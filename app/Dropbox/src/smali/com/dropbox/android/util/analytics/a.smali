.class public final Lcom/dropbox/android/util/analytics/a;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Ljava/util/TimerTask;

.field private static final c:Lcom/dropbox/android/util/analytics/j;

.field private static final d:Ljava/lang/Object;

.field private static e:Ljava/io/File;

.field private static f:Ljava/io/BufferedWriter;

.field private static g:Ljava/io/File;

.field private static volatile h:Z

.field private static final i:Ljava/util/concurrent/atomic/AtomicLong;

.field private static final j:Ljava/util/Timer;

.field private static k:I

.field private static final l:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 85
    const-class v0, Lcom/dropbox/android/util/analytics/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/util/analytics/a;->a:Ljava/lang/String;

    .line 96
    sput-object v2, Lcom/dropbox/android/util/analytics/a;->b:Ljava/util/TimerTask;

    .line 1534
    new-instance v0, Lcom/dropbox/android/util/analytics/j;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/j;-><init>(I)V

    sput-object v0, Lcom/dropbox/android/util/analytics/a;->c:Lcom/dropbox/android/util/analytics/j;

    .line 1540
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/analytics/a;->d:Ljava/lang/Object;

    .line 1546
    sput-object v2, Lcom/dropbox/android/util/analytics/a;->f:Ljava/io/BufferedWriter;

    .line 1551
    sput-object v2, Lcom/dropbox/android/util/analytics/a;->g:Ljava/io/File;

    .line 1558
    sput-boolean v3, Lcom/dropbox/android/util/analytics/a;->h:Z

    .line 1563
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/analytics/a;->i:Ljava/util/concurrent/atomic/AtomicLong;

    .line 1679
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/analytics/a;->j:Ljava/util/Timer;

    .line 1749
    sput v3, Lcom/dropbox/android/util/analytics/a;->k:I

    .line 2311
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/analytics/a;->l:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    return-void
.end method

.method public static A()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 233
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "accsync.unlink"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static B()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 237
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "accsync.unlink.done"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static C()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 241
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "accsync.unlink.failed_network"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static D()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 245
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "unlink.broadcast"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static E()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 249
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "battery.level"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static F()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 253
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "camera.upload.command"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static G()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 257
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "camera.upload.fullscan.event"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static H()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 265
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "video.start"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static I()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 273
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "video.prepared"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static J()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 277
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "video.completed"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static K()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 281
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "video.error"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static L()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 285
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "video.info"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static M()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 295
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "video.playing"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static N()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 303
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "video.size.mismatch"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static O()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 311
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "video.seek.limited"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static P()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 319
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "need.dotless.intent"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static Q()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 323
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "uncaught.exception"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static R()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 327
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "logged.exception"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static S()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 331
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "upload.queue.bump"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static T()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 338
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "upload.queue.schedule"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static U()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 342
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "camera.gallery.refresh.first_page"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static V()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 346
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "camera.gallery.refresh.first_page.request_and_parse_only"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static W()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 350
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "camera.gallery.refresh.request_and_parse_only"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static X()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 354
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "camera.gallery.refresh"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static Y()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 358
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "albums.delta.refresh"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static Z()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 362
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "unknown.file.extension"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 107
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "tour.view"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(J)Lcom/dropbox/android/util/analytics/l;
    .locals 1

    .prologue
    .line 505
    const-string v0, "login.sso"

    invoke-static {v0, p0, p1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/BroadcastReceiver;Landroid/content/Intent;)Lcom/dropbox/android/util/analytics/l;
    .locals 4

    .prologue
    .line 1186
    const/4 v0, 0x0

    .line 1187
    if-eqz p1, :cond_0

    .line 1188
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1190
    :cond_0
    new-instance v1, Lcom/dropbox/android/util/analytics/l;

    const-string v2, "broadcast.received"

    invoke-direct {v1, v2}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    const-string v2, "class"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "intent.action"

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;
    .locals 3

    .prologue
    .line 183
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "report.host.info."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 500
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "auth.success"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    const-string v1, "source"

    invoke-virtual {v0, v1, p0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "user"

    invoke-virtual {v0, v1, p1, p2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Landroid/app/Activity;)Lcom/dropbox/android/util/analytics/l;
    .locals 3

    .prologue
    .line 1145
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "act."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    const-string v1, "class"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    .line 1149
    instance-of v0, p1, Lcom/dropbox/android/activity/base/h;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 1150
    check-cast v0, Lcom/dropbox/android/activity/base/h;

    .line 1151
    invoke-interface {v0}, Lcom/dropbox/android/activity/base/h;->b()Ljava/lang/String;

    move-result-object v2

    .line 1152
    if-eqz v2, :cond_0

    .line 1153
    const-string v2, "id"

    invoke-interface {v0}, Lcom/dropbox/android/activity/base/h;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 1157
    :cond_0
    const-string v0, "create"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "network_state"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1158
    :cond_1
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1159
    if-eqz v0, :cond_2

    .line 1160
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1161
    if-eqz v0, :cond_2

    .line 1162
    const-string v2, "intent.action"

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 1165
    :cond_2
    invoke-virtual {p1}, Landroid/app/Activity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    .line 1166
    if-eqz v0, :cond_3

    .line 1167
    const-string v2, "caller"

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 1170
    :cond_3
    return-object v1
.end method

.method public static a(Ljava/lang/String;Landroid/app/Service;)Lcom/dropbox/android/util/analytics/l;
    .locals 3

    .prologue
    .line 1174
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "service."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    const-string v1, "class"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Landroid/support/v4/app/DialogFragment;)Lcom/dropbox/android/util/analytics/l;
    .locals 3

    .prologue
    .line 1326
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dialog."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    const-string v1, "class"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Landroid/support/v4/app/Fragment;)Lcom/dropbox/android/util/analytics/l;
    .locals 3

    .prologue
    .line 1178
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "frag."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    const-string v1, "class"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/util/analytics/l;
    .locals 4

    .prologue
    .line 1114
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "file."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    const-string v1, "mime"

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "extension"

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->q(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "size"

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->c()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    .line 1117
    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/u;)Lcom/dropbox/android/util/analytics/l;
    .locals 4

    .prologue
    .line 1128
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "download."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    const-string v1, "id"

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/u;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "class"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Lcom/dropbox/android/util/analytics/p;)Lcom/dropbox/android/util/analytics/l;
    .locals 4

    .prologue
    .line 1093
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "directory_space"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    const-string v1, "location"

    invoke-virtual {v0, v1, p0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "path"

    iget-object v2, p1, Lcom/dropbox/android/util/analytics/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "available"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "bytes_free"

    iget-wide v2, p1, Lcom/dropbox/android/util/analytics/p;->b:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "bytes_reserved"

    iget-wide v2, p1, Lcom/dropbox/android/util/analytics/p;->c:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "bytes_total"

    iget-wide v2, p1, Lcom/dropbox/android/util/analytics/p;->d:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ldbxyzptlk/db231222/z/K;Z)Lcom/dropbox/android/util/analytics/l;
    .locals 3

    .prologue
    .line 703
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "login.sso.initiated"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    const-string v1, "sso_state"

    invoke-virtual {p1}, Ldbxyzptlk/db231222/z/K;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "password_entered"

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "request_identifier"

    invoke-static {p0}, Lcom/dropbox/android/util/analytics/a;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;
    .locals 3

    .prologue
    .line 1197
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notification."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    const-string v1, "notification"

    invoke-virtual {v0, v1, p1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/dropbox/android/util/analytics/l;
    .locals 3

    .prologue
    .line 1103
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "directory_space"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    const-string v1, "location"

    invoke-virtual {v0, v1, p0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "path"

    invoke-virtual {v0, v1, p1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "available"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "failed"

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Z)Lcom/dropbox/android/util/analytics/l;
    .locals 3

    .prologue
    .line 710
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "login.sso.completed"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    const-string v1, "request_identifier"

    invoke-static {p0}, Lcom/dropbox/android/util/analytics/a;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "link_accepted"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ldbxyzptlk/db231222/i/c;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2514
    if-nez p1, :cond_0

    .line 2520
    const-string p1, "0"

    .line 2522
    :cond_0
    const-string v0, "|"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Ldbxyzptlk/db231222/i/c;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Ldbxyzptlk/db231222/i/c;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Ldbxyzptlk/db231222/i/c;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Ldbxyzptlk/db231222/i/c;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Ldbxyzptlk/db231222/i/c;->e:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Ldbxyzptlk/db231222/i/c;->f:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/io/BufferedReader;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/BufferedReader;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2280
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 2282
    new-instance v2, Ldbxyzptlk/db231222/ak/b;

    invoke-direct {v2}, Ldbxyzptlk/db231222/ak/b;-><init>()V

    .line 2284
    :cond_0
    :goto_0
    invoke-virtual {p0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 2285
    if-nez v0, :cond_1

    .line 2302
    return-object v1

    .line 2289
    :cond_1
    :try_start_0
    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/ak/b;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 2290
    instance-of v3, v0, Ljava/util/Map;

    if-eqz v3, :cond_0

    .line 2292
    check-cast v0, Ljava/util/Map;

    .line 2293
    const-string v3, "event"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2294
    if-eqz v0, :cond_0

    .line 2295
    invoke-static {v1, v0}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/util/Map;Ljava/lang/String;)V
    :try_end_0
    .catch Ldbxyzptlk/db231222/ak/c; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2298
    :catch_0
    move-exception v0

    .line 2299
    const-string v0, "skipped.log.parse.exception"

    invoke-static {v1, v0}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/util/Map;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Lcom/dropbox/android/util/analytics/k;)V
    .locals 6

    .prologue
    const/16 v4, 0xa

    .line 1756
    sget-boolean v0, Lcom/dropbox/android/util/analytics/a;->h:Z

    if-nez v0, :cond_1

    .line 1757
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 1793
    :cond_0
    :goto_0
    return-void

    .line 1771
    :cond_1
    :try_start_0
    invoke-interface {p0}, Lcom/dropbox/android/util/analytics/k;->f()Ljava/lang/String;

    move-result-object v0

    .line 1772
    if-eqz v0, :cond_0

    .line 1773
    sget-object v1, Lcom/dropbox/android/util/analytics/a;->d:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1774
    :try_start_1
    sget-object v2, Lcom/dropbox/android/util/analytics/a;->f:Ljava/io/BufferedWriter;

    invoke-virtual {v2, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 1775
    sget-object v2, Lcom/dropbox/android/util/analytics/a;->f:Ljava/io/BufferedWriter;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/io/BufferedWriter;->write(I)V

    .line 1776
    sget v2, Lcom/dropbox/android/util/analytics/a;->k:I

    add-int/lit8 v2, v2, 0x1

    sput v2, Lcom/dropbox/android/util/analytics/a;->k:I

    .line 1777
    sget v2, Lcom/dropbox/android/util/analytics/a;->k:I

    if-lt v2, v4, :cond_2

    .line 1778
    const/4 v2, 0x0

    sput v2, Lcom/dropbox/android/util/analytics/a;->k:I

    .line 1779
    sget-object v2, Lcom/dropbox/android/util/analytics/a;->f:Ljava/io/BufferedWriter;

    invoke-virtual {v2}, Ljava/io/BufferedWriter;->flush()V

    .line 1780
    sget-object v2, Lcom/dropbox/android/util/analytics/a;->e:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/32 v4, 0x80000

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    .line 1781
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cH()V

    .line 1784
    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1785
    :try_start_2
    sget-object v1, Lcom/dropbox/android/util/analytics/a;->c:Lcom/dropbox/android/util/analytics/j;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/util/analytics/j;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 1790
    :catch_0
    move-exception v0

    .line 1791
    sget-object v1, Lcom/dropbox/android/util/analytics/a;->a:Ljava/lang/String;

    const-string v2, "logEvent"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1784
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
.end method

.method private static a(Ldbxyzptlk/db231222/i/c;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    .line 1684
    sget-object v1, Lcom/dropbox/android/util/analytics/a;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 1686
    :try_start_0
    sget-object v2, Lcom/dropbox/android/util/analytics/a;->e:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1687
    :goto_0
    sget-object v2, Lcom/dropbox/android/util/analytics/a;->e:Ljava/io/File;

    invoke-static {v2}, Lcom/dropbox/android/util/v;->a(Ljava/io/File;)Ljava/io/OutputStream;

    move-result-object v2

    .line 1688
    new-instance v3, Ljava/io/BufferedWriter;

    new-instance v4, Ljava/io/OutputStreamWriter;

    invoke-direct {v4, v2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    const/16 v2, 0x2000

    invoke-direct {v3, v4, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V

    sput-object v3, Lcom/dropbox/android/util/analytics/a;->f:Ljava/io/BufferedWriter;

    .line 1696
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v2

    .line 1697
    new-instance v3, Lcom/dropbox/android/util/analytics/o;

    invoke-direct {v3, p0, v2}, Lcom/dropbox/android/util/analytics/o;-><init>(Ldbxyzptlk/db231222/i/c;Ldbxyzptlk/db231222/r/k;)V

    .line 1698
    sget-boolean v4, Lcom/dropbox/android/util/analytics/a;->h:Z

    if-nez v4, :cond_0

    .line 1699
    const-string v4, "early.event.count"

    sget-object v5, Lcom/dropbox/android/util/analytics/a;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    .line 1701
    :cond_0
    invoke-virtual {v3}, Lcom/dropbox/android/util/analytics/l;->f()Ljava/lang/String;

    move-result-object v3

    .line 1702
    sget-object v4, Lcom/dropbox/android/util/analytics/a;->f:Ljava/io/BufferedWriter;

    invoke-virtual {v4, v3}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 1703
    sget-object v3, Lcom/dropbox/android/util/analytics/a;->f:Ljava/io/BufferedWriter;

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Ljava/io/BufferedWriter;->write(I)V

    .line 1704
    sget-object v3, Lcom/dropbox/android/util/analytics/a;->f:Ljava/io/BufferedWriter;

    invoke-virtual {v3}, Ljava/io/BufferedWriter;->flush()V

    .line 1708
    const/4 v3, 0x1

    sput-boolean v3, Lcom/dropbox/android/util/analytics/a;->h:Z

    .line 1710
    if-eqz v0, :cond_1

    .line 1713
    if-nez v2, :cond_3

    const/4 v0, 0x0

    .line 1714
    :goto_1
    invoke-static {}, Ldbxyzptlk/db231222/n/k;->a()Ldbxyzptlk/db231222/n/k;

    move-result-object v2

    invoke-static {p0, v0}, Lcom/dropbox/android/util/analytics/a;->a(Ldbxyzptlk/db231222/i/c;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/n/k;->c(Ljava/lang/String;)V

    .line 1718
    new-instance v0, Ljava/lang/Thread;

    new-instance v2, Lcom/dropbox/android/util/analytics/f;

    invoke-direct {v2}, Lcom/dropbox/android/util/analytics/f;-><init>()V

    invoke-direct {v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1736
    :cond_1
    :goto_2
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1737
    return-void

    .line 1686
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1713
    :cond_3
    :try_start_2
    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/k;->a()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 1726
    :catch_0
    move-exception v0

    .line 1729
    :try_start_3
    sget-object v2, Lcom/dropbox/android/util/analytics/a;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "initWriter, sInitialized = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/dropbox/android/util/analytics/a;->h:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1730
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v2

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 1736
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0
.end method

.method public static a(Ljava/io/File;)V
    .locals 6

    .prologue
    .line 1571
    sget-object v1, Lcom/dropbox/android/util/analytics/a;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 1572
    :try_start_0
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->e:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 1573
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 1580
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1575
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Ljava/io/File;->mkdirs()Z

    .line 1576
    new-instance v0, Ljava/io/File;

    const-string v2, "dbl.dbl"

    invoke-direct {v0, p0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/analytics/a;->e:Ljava/io/File;

    .line 1577
    sput-object p0, Lcom/dropbox/android/util/analytics/a;->g:Ljava/io/File;

    .line 1579
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/i/d;->a()Ldbxyzptlk/db231222/i/c;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->a(Ldbxyzptlk/db231222/i/c;)V

    .line 1580
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1582
    sget-boolean v0, Lcom/dropbox/android/util/analytics/a;->h:Z

    if-nez v0, :cond_1

    .line 1608
    :goto_0
    return-void

    .line 1588
    :cond_1
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cI()V

    .line 1590
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cL()V

    .line 1600
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->j:Ljava/util/Timer;

    new-instance v1, Lcom/dropbox/android/util/analytics/b;

    invoke-direct {v1}, Lcom/dropbox/android/util/analytics/b;-><init>()V

    const-wide/16 v2, 0x1388

    const-wide/32 v4, 0x1808580

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    goto :goto_0
.end method

.method private static a(Ljava/util/Map;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2247
    const/4 v1, 0x1

    .line 2248
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2249
    if-eqz v0, :cond_0

    .line 2250
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v1

    .line 2252
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2253
    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private static a(Ljava/io/File;Ljava/util/Map;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2198
    invoke-static {p0}, Lcom/dropbox/android/util/analytics/a;->c(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    .line 2199
    if-nez v0, :cond_0

    move v0, v2

    .line 2241
    :goto_0
    return v0

    .line 2207
    :cond_0
    :try_start_0
    const-string v1, "skipped"

    const-string v3, "tmp"

    invoke-virtual {p0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v4

    invoke-static {v1, v3, v4}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 2215
    :try_start_1
    invoke-static {v3}, Lcom/dropbox/android/util/v;->a(Ljava/io/File;)Ljava/io/OutputStream;

    move-result-object v1

    .line 2216
    new-instance v4, Ljava/io/BufferedWriter;

    new-instance v5, Ljava/io/OutputStreamWriter;

    invoke-direct {v5, v1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    const/16 v1, 0x2000

    invoke-direct {v4, v5, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V

    .line 2217
    invoke-virtual {v4, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 2218
    const/16 v0, 0xa

    invoke-virtual {v4, v0}, Ljava/io/BufferedWriter;->write(I)V

    .line 2221
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cB()Lcom/dropbox/android/util/analytics/l;

    move-result-object v5

    .line 2222
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2223
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v7, v0

    invoke-virtual {v5, v1, v7, v8}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 2229
    :catch_0
    move-exception v0

    .line 2230
    sget-object v1, Lcom/dropbox/android/util/analytics/a;->a:Ljava/lang/String;

    const-string v3, "failedToReplaceContents"

    invoke-static {v1, v3}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2231
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    move v0, v2

    .line 2232
    goto :goto_0

    .line 2208
    :catch_1
    move-exception v0

    move v0, v2

    .line 2209
    goto :goto_0

    .line 2225
    :cond_1
    :try_start_2
    invoke-virtual {v5}, Lcom/dropbox/android/util/analytics/l;->f()Ljava/lang/String;

    move-result-object v0

    .line 2226
    invoke-virtual {v4, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 2227
    const/16 v0, 0xa

    invoke-virtual {v4, v0}, Ljava/io/BufferedWriter;->write(I)V

    .line 2228
    invoke-virtual {v4}, Ljava/io/BufferedWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 2236
    invoke-virtual {v3, p0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2237
    const/4 v0, 0x1

    goto :goto_0

    .line 2239
    :cond_2
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->a:Ljava/lang/String;

    const-string v1, "failedToRenameTempFile"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2240
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move v0, v2

    .line 2241
    goto/16 :goto_0
.end method

.method private static a(Ljava/util/Map;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 2155
    .line 2158
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "broadcast.received"

    aput-object v1, v0, v3

    const-string v1, "upload.start"

    aput-object v1, v0, v6

    const/4 v1, 0x2

    const-string v2, "upload.enqueue"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "upload.cancel"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "open.log"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "cameraupload.scan.finished"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "cache.cleanup.remove"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "camera.upload.fullscan.event"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "control.change"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "media.playbackstate.change"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "upload.queue.schedule"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "battery.level"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "download.start"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "reachability.change"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "upload.queue.bump"

    aput-object v2, v0, v1

    .line 2163
    new-instance v7, Ljava/util/HashSet;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 2168
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v3

    move v4, v3

    move v5, v3

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2169
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2170
    add-int/2addr v5, v1

    .line 2171
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2174
    if-le v1, v4, :cond_1

    move v0, v4

    :goto_1
    move v2, v0

    move v4, v1

    .line 2180
    goto :goto_0

    .line 2177
    :cond_1
    if-le v1, v2, :cond_4

    move v0, v2

    move v1, v4

    .line 2178
    goto :goto_1

    .line 2182
    :cond_2
    int-to-double v0, v5

    const-wide v7, 0x406f400000000000L    # 250.0

    cmpl-double v0, v0, v7

    if-ltz v0, :cond_3

    add-int v0, v4, v2

    int-to-double v0, v0

    int-to-double v4, v5

    div-double/2addr v0, v4

    const-wide v4, 0x3fefae147ae147aeL    # 0.99

    cmpl-double v0, v0, v4

    if-lez v0, :cond_3

    .line 2186
    :goto_2
    return v3

    :cond_3
    move v3, v6

    goto :goto_2

    :cond_4
    move v0, v2

    move v1, v4

    goto :goto_1
.end method

.method public static aA()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 539
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "photos_provider.cursor_load.more_check"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aB()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 545
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "app.link"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aC()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 549
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "app.unlink"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aD()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 557
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "login.flow.launch"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aE()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 565
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "login.page.launch"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aF()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 569
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "login.twofactor.prompted"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aG()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 577
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "login.twofactor.didntreceive"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aH()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 582
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "cu.turned_on"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aI()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 586
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "cu.turned_off"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aJ()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 590
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "cu.skipped"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aK()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 597
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "cu.videos_enabled_changed"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aL()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 601
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "share_link.generate"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aM()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 605
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "share_album_link.generate"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aN()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 609
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "share_lightweight_album_link.generate"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aO()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 617
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "send_to_contact"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aP()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 625
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "album_send_to_contact"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aQ()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 633
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "device.association"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aR()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 640
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "application.opened"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aS()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 649
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "intent.redirect"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aT()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 716
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "login.sso.optional_ignored"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aU()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 723
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "image.view"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aV()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 731
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "metadata.error"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aW()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 735
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "folder.rename"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aX()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 739
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "file.rename"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aY()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 743
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "folder.move"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aZ()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 747
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "file.move"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aa()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 366
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "gallery.showing.image.set"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ab()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 370
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "gallery.new.image.shown"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ac()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 374
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "gallery.pinch.start"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ad()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 378
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "gallery.pinch.end"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ae()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 386
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "gallery.video.play.tap.started"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static af()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 394
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "gallery.video.play.tap.confirmed"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ag()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 402
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "media.regular"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ah()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 409
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "media.transcode"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ai()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 413
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "multiselect.enter"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aj()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 417
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "multiselect.exit"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ak()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 421
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "album.renamemode.enter"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static al()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 425
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "album.renamemode.exit"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static am()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 429
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "bottommenu.click"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static an()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 436
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "expand.lightweight.shares"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ao()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 445
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "unexpected.file.root"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ap()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 454
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "up.history.stack.is.not.parent"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aq()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 458
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "get.content.request"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ar()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 462
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "get.content.result"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static as()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 466
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "chooser.request"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static at()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 470
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "chooser.result"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static au()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 475
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "dauth.success"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static av()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 480
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "dauth.failure"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aw()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 485
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "dauth.deny"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ax()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 490
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "dauth.allow"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ay()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 531
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "photos_provider.cursor_load"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static az()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 535
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "photos_provider.cursor_load.first_query"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static b()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 111
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "tour.back_from"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(J)Lcom/dropbox/android/util/analytics/l;
    .locals 1

    .prologue
    .line 510
    const-string v0, "login"

    invoke-static {v0, p0, p1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 699
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "login.sso.competing_scheme"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    const-string v1, "competing_package"

    invoke-virtual {v0, v1, p0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;Lcom/dropbox/android/taskqueue/u;)Lcom/dropbox/android/util/analytics/l;
    .locals 4

    .prologue
    .line 1137
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "upload."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    const-string v1, "id"

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/u;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "class"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1213
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "sort_changed"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    const-string v1, "old_sort"

    invoke-virtual {v0, v1, p0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "new_sort"

    invoke-virtual {v0, v1, p1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/io/File;)[Ljava/io/File;
    .locals 1

    .prologue
    .line 2001
    new-instance v0, Lcom/dropbox/android/util/analytics/i;

    invoke-direct {v0}, Lcom/dropbox/android/util/analytics/i;-><init>()V

    .line 2007
    invoke-virtual {p0, v0}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static bA()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 969
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "google_play.many_subs"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bB()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 977
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "google_play.server_failure"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bC()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 984
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "sdk.provider.query"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bD()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 996
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "app.create"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bE()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1004
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "gandalf.exposure"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bF()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1012
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "retrieve.gandalf.time"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bG()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1020
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "referrals.selected"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bH()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1027
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "referrals.contacts"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bI()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1031
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "misc.thumb.cache.init"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bJ()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1039
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "tab.selected"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bK()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1046
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "overquota.notification.refer_friends"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bL()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1053
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "overquota.notification.upgrade"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bM()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1060
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "dealexpirationwarning.notification.upgrade"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bN()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1067
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "dealexpirationwarning.notification.dismiss"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bO()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1074
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "dealexpirationwarning.notification.dismiss.error"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bP()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1082
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "dealexpirationwarning.notification.dismiss.success"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bQ()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1089
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "media.count"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bR()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1227
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "notification.feed.shmodel.click"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bS()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1234
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "notification.feed.shared.folder.invite.click"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bT()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1239
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "standard.oobe.sign.up"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bU()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1244
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "standard.oobe.sign.in"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bV()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1249
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "standard.oobe.no.thanks"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bW()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1254
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "standard.oobe.cancel"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bX()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1259
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "standard.obbe.success"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bY()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1264
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "standard.oobe.network.status"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bZ()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1269
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "shared.folder.invite.start"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ba()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 760
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "database.upgrade"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bb()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 769
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "database.migrate.one.version"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bc()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 795
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "help.view_TOS"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bd()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 803
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "help.view_privacy"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static be()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 811
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "help.send_feedback"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bf()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 818
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "new_text_file"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bg()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 825
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "edit_existing_text_file"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bh()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 829
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "refresh_file_browser"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bi()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 843
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "password.reset.sent"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bj()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 847
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "recover.account.request"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bk()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 856
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "card_scan.possible"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bl()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 863
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "card_scan.begin"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bm()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 870
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "card_scan.cancelled"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bn()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 877
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "card_scan.na"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bo()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 884
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "card_scan.confirmation_suppressed"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bp()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 891
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "card_scan.suppressed"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bq()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 898
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "card_scan.success"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static br()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 905
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "payment_selector.view"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bs()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 913
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "payment_credit_card.initiated"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bt()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 923
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "google_play.initiated"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bu()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 930
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "google_play.started"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bv()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 937
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "google_play.cancel"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bw()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 944
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "google_play.success"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bx()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 951
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "google_play.sku_data"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static by()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 958
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "google_play.upgrade_failure"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static bz()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 965
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "google_play.failure"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static c()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 115
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "current.settings.state"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static c(J)Lcom/dropbox/android/util/analytics/l;
    .locals 1

    .prologue
    .line 515
    const-string v0, "login.web_session"

    invoke-static {v0, p0, p1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;
    .locals 3

    .prologue
    .line 1201
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "gcm."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1346
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "intro.tour.dismissed"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    const-string v1, "page"

    invoke-virtual {v0, v1, p0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "method"

    invoke-virtual {v0, v1, p1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method private static c(Ljava/io/File;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2087
    .line 2089
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-static {p0}, Lcom/dropbox/android/util/analytics/a;->i(Ljava/io/File;)Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2090
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 2094
    invoke-static {v1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/Reader;)V

    :goto_0
    return-object v0

    .line 2091
    :catch_0
    move-exception v1

    move-object v1, v0

    .line 2094
    :goto_1
    invoke-static {v1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/Reader;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_2
    invoke-static {v1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/Reader;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 2091
    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method static synthetic cA()V
    .locals 0

    .prologue
    .line 84
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cI()V

    return-void
.end method

.method private static cB()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 103
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "skipped.log"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static cC()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 835
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "rotation.forced"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static cD()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 839
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "rotation.scheduled"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static cE()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 988
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "log.up"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static cF()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 992
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "deleted.big.log"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static cG()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1016
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "logseries.newUUID"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static cH()V
    .locals 4

    .prologue
    .line 1624
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->j:Ljava/util/Timer;

    new-instance v1, Lcom/dropbox/android/util/analytics/c;

    invoke-direct {v1}, Lcom/dropbox/android/util/analytics/c;-><init>()V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1638
    return-void
.end method

.method private static declared-synchronized cI()V
    .locals 14

    .prologue
    const-wide/32 v12, 0x15180

    const-wide/16 v10, 0x3e8

    .line 1826
    const-class v4, Lcom/dropbox/android/util/analytics/a;

    monitor-enter v4

    :try_start_0
    invoke-static {}, Ldbxyzptlk/db231222/n/k;->a()Ldbxyzptlk/db231222/n/k;

    move-result-object v5

    .line 1827
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long v6, v0, v2

    .line 1828
    invoke-virtual {v5}, Ldbxyzptlk/db231222/n/k;->j()J

    move-result-wide v0

    .line 1830
    sget-object v2, Lcom/dropbox/android/util/analytics/a;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "nextRotation: "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1831
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 1834
    add-long v0, v6, v12

    .line 1835
    invoke-virtual {v5, v0, v1}, Ldbxyzptlk/db231222/n/k;->a(J)V

    .line 1836
    sget-object v2, Lcom/dropbox/android/util/analytics/a;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "no scheduled rotation, scheduling it for "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v8, v0, v6

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " seconds from now."

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1839
    :cond_0
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v2

    .line 1840
    if-nez v2, :cond_4

    const/4 v2, 0x0

    .line 1841
    :goto_0
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/i/d;->a()Ldbxyzptlk/db231222/i/c;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/dropbox/android/util/analytics/a;->a(Ldbxyzptlk/db231222/i/c;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1843
    invoke-virtual {v5}, Ldbxyzptlk/db231222/n/k;->k()Ljava/lang/String;

    move-result-object v2

    .line 1844
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 1845
    invoke-virtual {v5, v3}, Ldbxyzptlk/db231222/n/k;->c(Ljava/lang/String;)V

    .line 1848
    const-string v5, ""

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    move-object v2, v3

    .line 1853
    :cond_1
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const/4 v2, 0x1

    .line 1855
    :goto_1
    cmp-long v5, v6, v0

    if-gez v5, :cond_2

    if-eqz v2, :cond_3

    .line 1856
    :cond_2
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Version changed to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1857
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cD()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 1858
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cJ()V

    .line 1859
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->j:Ljava/util/Timer;

    new-instance v1, Lcom/dropbox/android/util/analytics/g;

    invoke-direct {v1}, Lcom/dropbox/android/util/analytics/g;-><init>()V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1867
    add-long v0, v6, v12

    .line 1871
    :cond_3
    sub-long/2addr v0, v6

    mul-long/2addr v0, v10

    .line 1872
    sget-object v2, Lcom/dropbox/android/util/analytics/a;->j:Ljava/util/Timer;

    new-instance v3, Lcom/dropbox/android/util/analytics/h;

    invoke-direct {v3}, Lcom/dropbox/android/util/analytics/h;-><init>()V

    invoke-virtual {v2, v3, v0, v1}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1880
    monitor-exit v4

    return-void

    .line 1840
    :cond_4
    :try_start_1
    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/k;->a()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 1853
    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    .line 1826
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0
.end method

.method private static cJ()V
    .locals 6

    .prologue
    .line 1906
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->a:Ljava/lang/String;

    const-string v1, "Rotating."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1910
    sget-object v1, Lcom/dropbox/android/util/analytics/a;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 1912
    :try_start_0
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->f:Ljava/io/BufferedWriter;

    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1917
    :goto_0
    :try_start_1
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    .line 1918
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    new-instance v2, Ljava/lang/Throwable;

    const-string v3, "Zero-length file."

    invoke-direct {v2, v3}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 1919
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->e:Ljava/io/File;

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->g(Ljava/io/File;)Z

    move-result v0

    .line 1935
    :goto_1
    if-eqz v0, :cond_0

    .line 1936
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cM()V

    .line 1937
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/i/d;->a()Ldbxyzptlk/db231222/i/c;

    move-result-object v0

    .line 1938
    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->a(Ldbxyzptlk/db231222/i/c;)V

    .line 1940
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1943
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 1944
    invoke-static {}, Ldbxyzptlk/db231222/n/k;->a()Ldbxyzptlk/db231222/n/k;

    move-result-object v2

    const-wide/32 v3, 0x15180

    add-long/2addr v0, v3

    invoke-virtual {v2, v0, v1}, Ldbxyzptlk/db231222/n/k;->a(J)V

    .line 1945
    return-void

    .line 1913
    :catch_0
    move-exception v0

    .line 1914
    :try_start_2
    sget-object v2, Lcom/dropbox/android/util/analytics/a;->a:Ljava/lang/String;

    const-string v3, "rotateLog"

    invoke-static {v2, v3, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1940
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1927
    :cond_1
    :try_start_3
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/dropbox/android/util/analytics/a;->g:Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dbup-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".dbu"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1928
    sget-object v2, Lcom/dropbox/android/util/analytics/a;->e:Ljava/io/File;

    invoke-virtual {v2, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1929
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    new-instance v2, Ljava/lang/Throwable;

    const-string v3, "Failed to rename, deleting instead"

    invoke-direct {v2, v3}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 1930
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->e:Ljava/io/File;

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->f(Ljava/io/File;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    goto :goto_1

    .line 1932
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private static cK()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1974
    const-string v0, "internal"

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v3

    invoke-static {v3}, Lcom/dropbox/android/util/analytics/p;->a(Ljava/io/File;)Lcom/dropbox/android/util/analytics/p;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Lcom/dropbox/android/util/analytics/p;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 1977
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    const-string v3, "fake.db"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 1978
    const-string v3, "database_dir"

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/p;->a(Ljava/io/File;)Lcom/dropbox/android/util/analytics/p;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Lcom/dropbox/android/util/analytics/p;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 1981
    const/4 v3, 0x0

    .line 1982
    const-string v0, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "mounted_ro"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    .line 1984
    :goto_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    .line 1986
    if-eqz v0, :cond_3

    .line 1988
    :try_start_0
    invoke-static {v4}, Lcom/dropbox/android/util/analytics/p;->a(Ljava/io/File;)Lcom/dropbox/android/util/analytics/p;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1993
    :goto_1
    if-eqz v0, :cond_2

    .line 1994
    const-string v1, "external"

    invoke-static {v1, v0}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Lcom/dropbox/android/util/analytics/p;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 1998
    :goto_2
    return-void

    :cond_1
    move v0, v1

    .line 1982
    goto :goto_0

    .line 1989
    :catch_0
    move-exception v0

    move v1, v2

    move-object v0, v3

    .line 1990
    goto :goto_1

    .line 1996
    :cond_2
    const-string v0, "external"

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_2

    :cond_3
    move-object v0, v3

    goto :goto_1
.end method

.method private static cL()V
    .locals 1

    .prologue
    .line 2071
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v0

    .line 2072
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/k;->h()Ldbxyzptlk/db231222/r/o;

    move-result-object v0

    invoke-interface {v0}, Ldbxyzptlk/db231222/r/o;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2073
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->ct()V

    .line 2075
    :cond_0
    return-void
.end method

.method private static cM()V
    .locals 2

    .prologue
    .line 2078
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v0

    .line 2079
    if-eqz v0, :cond_0

    .line 2080
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/k;->h()Ldbxyzptlk/db231222/r/o;

    move-result-object v0

    .line 2081
    invoke-interface {v0}, Ldbxyzptlk/db231222/r/o;->f()I

    move-result v1

    .line 2082
    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Ldbxyzptlk/db231222/r/o;->a(I)V

    .line 2084
    :cond_0
    return-void
.end method

.method private static cN()V
    .locals 23

    .prologue
    .line 2356
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 2357
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cO()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2442
    :goto_0
    return-void

    .line 2360
    :cond_0
    sget-object v20, Lcom/dropbox/android/util/analytics/a;->l:Ljava/lang/Object;

    monitor-enter v20

    .line 2361
    :try_start_0
    sget-object v2, Lcom/dropbox/android/util/analytics/a;->g:Ljava/io/File;

    invoke-static {v2}, Lcom/dropbox/android/util/analytics/a;->b(Ljava/io/File;)[Ljava/io/File;

    move-result-object v21

    .line 2362
    sget-object v2, Lcom/dropbox/android/util/analytics/a;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Found "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v21

    array-length v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " matching files."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2363
    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v22, v0

    const/4 v2, 0x0

    move/from16 v19, v2

    :goto_1
    move/from16 v0, v19

    move/from16 v1, v22

    if-ge v0, v1, :cond_5

    aget-object v3, v21, v19

    .line 2364
    invoke-static {v3}, Lcom/dropbox/android/util/analytics/a;->d(Ljava/io/File;)Lcom/dropbox/android/util/analytics/n;

    move-result-object v2

    .line 2365
    if-nez v2, :cond_7

    .line 2366
    sget-object v2, Lcom/dropbox/android/util/analytics/a;->a:Ljava/lang/String;

    const-string v4, "Couldn\'t parse header, using current UserInfo."

    invoke-static {v2, v4}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2371
    new-instance v2, Lcom/dropbox/android/util/analytics/n;

    const/4 v4, 0x0

    invoke-direct {v2, v4}, Lcom/dropbox/android/util/analytics/n;-><init>(Lcom/dropbox/android/util/analytics/b;)V

    .line 2372
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v4

    invoke-virtual {v4}, Ldbxyzptlk/db231222/i/d;->a()Ldbxyzptlk/db231222/i/c;

    move-result-object v4

    .line 2373
    iget-object v5, v4, Ldbxyzptlk/db231222/i/c;->d:Ljava/lang/String;

    iput-object v5, v2, Lcom/dropbox/android/util/analytics/n;->e:Ljava/lang/String;

    .line 2374
    iget-object v5, v4, Ldbxyzptlk/db231222/i/c;->a:Ljava/lang/String;

    iput-object v5, v2, Lcom/dropbox/android/util/analytics/n;->b:Ljava/lang/String;

    .line 2375
    iget-object v5, v4, Ldbxyzptlk/db231222/i/c;->e:Ljava/lang/String;

    iput-object v5, v2, Lcom/dropbox/android/util/analytics/n;->c:Ljava/lang/String;

    .line 2376
    iget-object v4, v4, Ldbxyzptlk/db231222/i/c;->c:Ljava/lang/String;

    iput-object v4, v2, Lcom/dropbox/android/util/analytics/n;->d:Ljava/lang/String;

    .line 2377
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v4

    invoke-virtual {v4}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v4

    .line 2378
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Ldbxyzptlk/db231222/r/k;->f()Ldbxyzptlk/db231222/r/d;

    move-result-object v4

    invoke-virtual {v4}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v4

    :goto_2
    iput-object v4, v2, Lcom/dropbox/android/util/analytics/n;->a:Ljava/lang/String;

    move-object/from16 v18, v2

    .line 2383
    :goto_3
    invoke-static {v3}, Lcom/dropbox/android/util/analytics/a;->e(Ljava/io/File;)Ljava/util/Map;

    move-result-object v2

    .line 2384
    if-eqz v2, :cond_3

    invoke-static {v2}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/util/Map;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2385
    invoke-static {v3, v2}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/io/File;Ljava/util/Map;)Z

    move-result v2

    .line 2386
    if-nez v2, :cond_3

    .line 2363
    :cond_1
    :goto_4
    add-int/lit8 v2, v19, 0x1

    move/from16 v19, v2

    goto :goto_1

    .line 2378
    :cond_2
    const-string v4, "0"

    goto :goto_2

    .line 2391
    :cond_3
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v4, "dbup-"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2392
    invoke-static {v3}, Lcom/dropbox/android/util/analytics/a;->h(Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    .line 2393
    if-eqz v2, :cond_6

    move-object/from16 v17, v2

    .line 2397
    :goto_5
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "gzdbup-"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    .line 2400
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/32 v4, 0x400000

    cmp-long v2, v2, v4

    if-lez v2, :cond_4

    .line 2401
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cF()Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "size"

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 2402
    invoke-static/range {v17 .. v17}, Lcom/dropbox/android/util/analytics/a;->g(Ljava/io/File;)Z

    goto :goto_4

    .line 2441
    :catchall_0
    move-exception v2

    monitor-exit v20
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 2409
    :cond_4
    :try_start_1
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long v10, v2, v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2410
    const/4 v2, 0x0

    move/from16 v16, v2

    :goto_6
    const/4 v2, 0x2

    move/from16 v0, v16

    if-ge v0, v2, :cond_1

    .line 2411
    const/4 v12, 0x0

    .line 2413
    :try_start_2
    invoke-static/range {v17 .. v17}, Lcom/dropbox/android/util/v;->b(Ljava/io/File;)Ljava/io/InputStream;

    move-result-object v12

    .line 2414
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/a;->h()Ldbxyzptlk/db231222/z/F;

    move-result-object v2

    const-string v3, "android"

    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/dropbox/android/util/analytics/n;->b:Ljava/lang/String;

    sget-object v5, Ldbxyzptlk/db231222/z/J;->e:Ldbxyzptlk/db231222/z/J;

    move-object/from16 v0, v18

    iget-object v6, v0, Lcom/dropbox/android/util/analytics/n;->a:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/dropbox/android/util/analytics/n;->c:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v8, v0, Lcom/dropbox/android/util/analytics/n;->d:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v9, v0, Lcom/dropbox/android/util/analytics/n;->e:Ljava/lang/String;

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->length()J

    move-result-wide v13

    invoke-virtual/range {v2 .. v15}, Ldbxyzptlk/db231222/z/F;->a(Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/db231222/z/J;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/io/InputStream;JZ)V

    .line 2418
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cE()Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "size"

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "name"

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "attempt"

    move/from16 v0, v16

    int-to-long v4, v0

    invoke-virtual {v2, v3, v4, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "gzipped"

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 2419
    invoke-static/range {v17 .. v17}, Lcom/dropbox/android/util/analytics/a;->g(Ljava/io/File;)Z
    :try_end_2
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2437
    :try_start_3
    invoke-static {v12}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_4

    .line 2421
    :catch_0
    move-exception v2

    .line 2423
    :try_start_4
    sget-object v3, Lcom/dropbox/android/util/analytics/a;->a:Ljava/lang/String;

    const-string v4, "Failed to upload"

    invoke-static {v3, v4, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2437
    :try_start_5
    invoke-static {v12}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2410
    add-int/lit8 v2, v16, 0x1

    move/from16 v16, v2

    goto :goto_6

    .line 2424
    :catch_1
    move-exception v2

    .line 2427
    :try_start_6
    sget-object v3, Lcom/dropbox/android/util/analytics/a;->a:Ljava/lang/String;

    const-string v4, "Failed to upload"

    invoke-static {v3, v4, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2428
    invoke-static/range {v17 .. v17}, Lcom/dropbox/android/util/analytics/a;->g(Ljava/io/File;)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2437
    :try_start_7
    invoke-static {v12}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_4

    .line 2430
    :catch_2
    move-exception v2

    .line 2433
    :try_start_8
    sget-object v3, Lcom/dropbox/android/util/analytics/a;->a:Ljava/lang/String;

    const-string v4, "Failed to upload"

    invoke-static {v3, v4, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2434
    invoke-static/range {v17 .. v17}, Lcom/dropbox/android/util/analytics/a;->g(Ljava/io/File;)Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 2437
    :try_start_9
    invoke-static {v12}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto/16 :goto_4

    :catchall_1
    move-exception v2

    invoke-static {v12}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    throw v2

    .line 2441
    :cond_5
    monitor-exit v20
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    :cond_6
    move-object/from16 v17, v3

    goto/16 :goto_5

    :cond_7
    move-object/from16 v18, v2

    goto/16 :goto_3
.end method

.method private static cO()Z
    .locals 2

    .prologue
    .line 2501
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/G;->b()Lcom/dropbox/android/service/J;

    move-result-object v0

    .line 2502
    invoke-virtual {v0}, Lcom/dropbox/android/service/J;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/dropbox/android/service/J;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static ca()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1274
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "shared.folder.invite.success"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static cb()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1279
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "shared.folder.permissions.update"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static cc()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1284
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "shared.folder.manage.start"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static cd()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1289
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "shared.folder.kickout"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ce()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1294
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "shared.folder.promote"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static cf()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1299
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "shared.folder.leave"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static cg()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1304
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "shared.folder.unshare"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ch()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1309
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "shared.folder.email"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ci()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1314
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "shared.folder.uninvite"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static cj()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1319
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "shared.folder.reinvite"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ck()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1372
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "qr.auth.page"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static cl()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1382
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "qr.auth.connectivity.error"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static cm()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1390
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "qr.auth.code.sent"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static cn()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1397
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "qr.auth.code.response"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static co()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1405
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "qr.code.prefix.mismatch"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static cp()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1413
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "ri.entry.displayed"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static declared-synchronized cq()V
    .locals 5

    .prologue
    .line 1645
    const-class v1, Lcom/dropbox/android/util/analytics/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->b:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    .line 1646
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->b:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 1647
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->j:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 1649
    :cond_0
    new-instance v0, Lcom/dropbox/android/util/analytics/d;

    invoke-direct {v0}, Lcom/dropbox/android/util/analytics/d;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/analytics/a;->b:Ljava/util/TimerTask;

    .line 1655
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->j:Ljava/util/Timer;

    sget-object v2, Lcom/dropbox/android/util/analytics/a;->b:Ljava/util/TimerTask;

    const-wide/32 v3, 0x927c0

    invoke-virtual {v0, v2, v3, v4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1656
    monitor-exit v1

    return-void

    .line 1645
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static cr()V
    .locals 4

    .prologue
    .line 1799
    sget-boolean v0, Lcom/dropbox/android/util/analytics/a;->h:Z

    if-nez v0, :cond_0

    .line 1813
    :goto_0
    return-void

    .line 1802
    :cond_0
    sget-object v1, Lcom/dropbox/android/util/analytics/a;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 1804
    :try_start_0
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->f:Ljava/io/BufferedWriter;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 1805
    :try_start_1
    monitor-exit v1

    goto :goto_0

    .line 1812
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1807
    :cond_1
    :try_start_2
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->f:Ljava/io/BufferedWriter;

    invoke-virtual {v0}, Ljava/io/BufferedWriter;->flush()V

    .line 1808
    const/4 v0, 0x0

    sput v0, Lcom/dropbox/android/util/analytics/a;->k:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1812
    :goto_1
    :try_start_3
    monitor-exit v1

    goto :goto_0

    .line 1809
    :catch_0
    move-exception v0

    .line 1810
    sget-object v2, Lcom/dropbox/android/util/analytics/a;->a:Ljava/lang/String;

    const-string v3, "flushLog"

    invoke-static {v2, v3, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public static cs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1819
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->c:Lcom/dropbox/android/util/analytics/j;

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/j;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static ct()V
    .locals 3

    .prologue
    .line 2050
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v1

    .line 2051
    const/4 v0, 0x0

    .line 2052
    if-eqz v1, :cond_0

    .line 2053
    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/k;->h()Ldbxyzptlk/db231222/r/o;

    move-result-object v1

    .line 2056
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2057
    invoke-interface {v1, v0}, Ldbxyzptlk/db231222/r/o;->b(Ljava/lang/String;)V

    .line 2058
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ldbxyzptlk/db231222/r/o;->a(I)V

    .line 2060
    :cond_0
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cG()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "series.uuid"

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 2061
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/util/analytics/a;->g(J)V

    .line 2062
    return-void
.end method

.method static synthetic cu()V
    .locals 0

    .prologue
    .line 84
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cN()V

    return-void
.end method

.method static synthetic cv()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->d:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic cw()Ljava/io/File;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->e:Ljava/io/File;

    return-object v0
.end method

.method static synthetic cx()Lcom/dropbox/android/util/analytics/l;
    .locals 1

    .prologue
    .line 84
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cD()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method static synthetic cy()V
    .locals 0

    .prologue
    .line 84
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cJ()V

    return-void
.end method

.method static synthetic cz()V
    .locals 0

    .prologue
    .line 84
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cK()V

    return-void
.end method

.method public static d()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 119
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "camera.upload.tour.btn"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static d(J)Lcom/dropbox/android/util/analytics/l;
    .locals 1

    .prologue
    .line 520
    const-string v0, "login.htc"

    invoke-static {v0, p0, p1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method public static d(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;
    .locals 3

    .prologue
    .line 1205
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sf."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1357
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "intro.tour.display.type"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    const-string v1, "page"

    invoke-virtual {v0, v1, p0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "type"

    invoke-virtual {v0, v1, p1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method private static d(Ljava/io/File;)Lcom/dropbox/android/util/analytics/n;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 2115
    invoke-static {p0}, Lcom/dropbox/android/util/analytics/a;->c(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    .line 2116
    if-nez v0, :cond_0

    move-object v0, v2

    .line 2141
    :goto_0
    return-object v0

    .line 2120
    :cond_0
    :try_start_0
    new-instance v3, Ldbxyzptlk/db231222/ak/b;

    invoke-direct {v3}, Ldbxyzptlk/db231222/ak/b;-><init>()V

    invoke-virtual {v3, v0}, Ldbxyzptlk/db231222/ak/b;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 2121
    instance-of v3, v0, Ljava/util/Map;

    if-eqz v3, :cond_3

    .line 2122
    new-instance v3, Lcom/dropbox/android/util/analytics/n;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/dropbox/android/util/analytics/n;-><init>(Lcom/dropbox/android/util/analytics/b;)V

    .line 2124
    check-cast v0, Ljava/util/Map;

    .line 2125
    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "APP_VERSION"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "USER_ID"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "DEVICE_ID"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "PHONE_MODEL"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "ANDROID_VERSION"

    aput-object v6, v4, v5

    .line 2126
    array-length v5, v4

    :goto_1
    if-ge v1, v5, :cond_2

    aget-object v6, v4, v1

    .line 2127
    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_1

    .line 2128
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Required header field missing or null: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 2129
    goto :goto_0

    .line 2126
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2132
    :cond_2
    const-string v1, "USER_ID"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v3, Lcom/dropbox/android/util/analytics/n;->a:Ljava/lang/String;

    .line 2133
    const-string v1, "APP_VERSION"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v3, Lcom/dropbox/android/util/analytics/n;->b:Ljava/lang/String;

    .line 2134
    const-string v1, "DEVICE_ID"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v3, Lcom/dropbox/android/util/analytics/n;->c:Ljava/lang/String;

    .line 2135
    const-string v1, "PHONE_MODEL"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v3, Lcom/dropbox/android/util/analytics/n;->d:Ljava/lang/String;

    .line 2136
    const-string v1, "ANDROID_VERSION"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/dropbox/android/util/analytics/n;->e:Ljava/lang/String;
    :try_end_0
    .catch Ldbxyzptlk/db231222/ak/c; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v3

    .line 2137
    goto/16 :goto_0

    :cond_3
    move-object v0, v2

    .line 2139
    goto/16 :goto_0

    .line 2140
    :catch_0
    move-exception v0

    move-object v0, v2

    .line 2141
    goto/16 :goto_0
.end method

.method public static e()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 123
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "quickaction.btn.click"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static e(J)Lcom/dropbox/android/util/analytics/l;
    .locals 1

    .prologue
    .line 525
    const-string v0, "new_account"

    invoke-static {v0, p0, p1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method public static e(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;
    .locals 3

    .prologue
    .line 1209
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "early_update."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static e(Ljava/io/File;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2261
    .line 2264
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-static {p0}, Lcom/dropbox/android/util/analytics/a;->i(Ljava/io/File;)Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2265
    :try_start_1
    invoke-static {v1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/io/BufferedReader;)Ljava/util/Map;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 2269
    invoke-static {v1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/Reader;)V

    :goto_0
    return-object v0

    .line 2266
    :catch_0
    move-exception v1

    move-object v1, v0

    .line 2269
    :goto_1
    invoke-static {v1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/Reader;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_2
    invoke-static {v1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/Reader;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 2266
    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method public static f()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 127
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "quickaction.menu.click"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static f(J)Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 573
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "login.twofactor.success"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    const-string v1, "user"

    invoke-virtual {v0, v1, p0, p1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method public static f(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1334
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "intro.tour.page"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    const-string v1, "page"

    invoke-virtual {v0, v1, p0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method private static f(Ljava/io/File;)Z
    .locals 2

    .prologue
    .line 2320
    invoke-static {p0}, Lcom/dropbox/android/util/analytics/a;->g(Ljava/io/File;)Z

    move-result v0

    .line 2321
    if-nez v0, :cond_0

    .line 2322
    const/4 v1, 0x0

    sput-boolean v1, Lcom/dropbox/android/util/analytics/a;->h:Z

    .line 2324
    :cond_0
    return v0
.end method

.method public static g()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 131
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "quickaction.show"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static g(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1365
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "qr.entrypoint"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    const-string v1, "entrypoint"

    invoke-virtual {v0, v1, p0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    return-object v0
.end method

.method public static g(J)V
    .locals 2

    .prologue
    .line 1662
    sget-boolean v0, Lcom/dropbox/android/util/analytics/a;->h:Z

    if-nez v0, :cond_0

    .line 1674
    :goto_0
    return-void

    .line 1665
    :cond_0
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cC()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 1666
    sget-object v0, Lcom/dropbox/android/util/analytics/a;->j:Ljava/util/Timer;

    new-instance v1, Lcom/dropbox/android/util/analytics/e;

    invoke-direct {v1}, Lcom/dropbox/android/util/analytics/e;-><init>()V

    invoke-virtual {v0, v1, p0, p1}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0
.end method

.method private static g(Ljava/io/File;)Z
    .locals 5

    .prologue
    .line 2333
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2334
    const/4 v0, 0x1

    .line 2341
    :cond_0
    :goto_0
    return v0

    .line 2336
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v0

    .line 2337
    if-nez v0, :cond_0

    .line 2338
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    new-instance v2, Ljava/lang/Throwable;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to delete "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static h()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 135
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "quickaction.dismiss"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static h(Ljava/io/File;)Ljava/io/File;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2453
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 2454
    const-string v2, "dbup-"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2455
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Wrong prefix."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2458
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "gzdbup-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "dbup-"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2459
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2461
    invoke-static {v1}, Lcom/dropbox/android/util/analytics/a;->g(Ljava/io/File;)Z

    move-result v2

    .line 2462
    if-nez v2, :cond_1

    .line 2489
    :goto_0
    return-object v0

    .line 2469
    :cond_1
    :try_start_0
    invoke-static {p0}, Lcom/dropbox/android/util/v;->b(Ljava/io/File;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 2470
    :try_start_1
    new-instance v2, Ljava/io/FileOutputStream;

    const/4 v4, 0x0

    invoke-direct {v2, v1, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    invoke-static {v2}, Lcom/dropbox/android/util/v;->a(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v4

    .line 2471
    new-instance v2, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v2, v4}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2472
    :try_start_2
    invoke-static {v3, v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    .line 2474
    invoke-virtual {p0}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v4

    if-eqz v4, :cond_2

    .line 2485
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    .line 2486
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V

    move-object v0, v1

    goto :goto_0

    .line 2479
    :cond_2
    :try_start_3
    invoke-static {v1}, Lcom/dropbox/android/util/analytics/a;->g(Ljava/io/File;)Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 2485
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    .line 2486
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V

    goto :goto_0

    .line 2481
    :catch_0
    move-exception v2

    move-object v2, v0

    move-object v3, v0

    .line 2483
    :goto_1
    :try_start_4
    invoke-static {v1}, Lcom/dropbox/android/util/analytics/a;->g(Ljava/io/File;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 2485
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    .line 2486
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V

    goto :goto_0

    .line 2485
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v0, v1

    :goto_2
    invoke-static {v3}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    .line 2486
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V

    throw v0

    .line 2485
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_2

    :catchall_2
    move-exception v0

    goto :goto_2

    .line 2481
    :catch_1
    move-exception v2

    move-object v2, v0

    goto :goto_1

    :catch_2
    move-exception v4

    goto :goto_1
.end method

.method private static h(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 667
    :try_start_0
    const-string v0, "SHA-256"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 674
    :try_start_1
    const-string v1, "utf-8"

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->digest([B)[B
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 678
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 680
    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_1

    .line 681
    aget-byte v3, v1, v0

    and-int/lit16 v3, v3, 0xff

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    .line 684
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 685
    const/16 v4, 0x30

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 687
    :cond_0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 680
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 668
    :catch_0
    move-exception v0

    .line 669
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 675
    :catch_1
    move-exception v0

    .line 676
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 689
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static i()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 139
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "custom.intent.chooser"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static i(Ljava/io/File;)Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 2493
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "gzdbup-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2494
    new-instance v0, Ljava/util/zip/GZIPInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-static {v0}, Lcom/dropbox/android/util/v;->a(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v0

    .line 2496
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/dropbox/android/util/v;->b(Ljava/io/File;)Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0
.end method

.method public static j()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 143
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "cameraupload.scan.finished"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static k()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 147
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "userid.change"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static l()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 151
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "pref.changed"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static m()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 155
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "cameraupload.tour.pressed_tour_on"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static n()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 159
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "cameraupload.tour.pressed_skip_this"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static o()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 163
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "cameraupload.tour.pref.final.value"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static p()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 167
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "block.scan"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static q()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 171
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "textedit.open"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static r()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 175
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "textedit.save"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static s()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 179
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "user.clear.cache"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static t()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 190
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "user.unlink"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static u()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 197
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "email.auto_complete.add"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static v()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 204
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "email.auto_complete.accept"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static w()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 211
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "email.domain_suggestion.view"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static x()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 218
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "email.domain_suggestion.accept"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static y()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 225
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "user.unlink.done"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static z()Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 229
    new-instance v0, Lcom/dropbox/android/util/analytics/l;

    const-string v1, "user.unlink.failed_network"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
