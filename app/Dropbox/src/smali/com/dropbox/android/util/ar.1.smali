.class public final enum Lcom/dropbox/android/util/ar;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/util/ar;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/util/ar;

.field public static final enum b:Lcom/dropbox/android/util/ar;

.field public static final enum c:Lcom/dropbox/android/util/ar;

.field private static final synthetic f:[Lcom/dropbox/android/util/ar;


# instance fields
.field private d:Lcom/dropbox/android/util/bz;

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 11
    new-instance v0, Lcom/dropbox/android/util/ar;

    const-string v1, "TOS"

    sget-object v2, Lcom/dropbox/android/util/bz;->e:Lcom/dropbox/android/util/bz;

    const v3, 0x7f0d004c

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/dropbox/android/util/ar;-><init>(Ljava/lang/String;ILcom/dropbox/android/util/bz;I)V

    sput-object v0, Lcom/dropbox/android/util/ar;->a:Lcom/dropbox/android/util/ar;

    .line 12
    new-instance v0, Lcom/dropbox/android/util/ar;

    const-string v1, "PRIVACY"

    sget-object v2, Lcom/dropbox/android/util/bz;->f:Lcom/dropbox/android/util/bz;

    const v3, 0x7f0d0150

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/dropbox/android/util/ar;-><init>(Ljava/lang/String;ILcom/dropbox/android/util/bz;I)V

    sput-object v0, Lcom/dropbox/android/util/ar;->b:Lcom/dropbox/android/util/ar;

    .line 13
    new-instance v0, Lcom/dropbox/android/util/ar;

    const-string v1, "OPEN_SOURCE"

    sget-object v2, Lcom/dropbox/android/util/bz;->g:Lcom/dropbox/android/util/bz;

    const v3, 0x7f0d0151

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/dropbox/android/util/ar;-><init>(Ljava/lang/String;ILcom/dropbox/android/util/bz;I)V

    sput-object v0, Lcom/dropbox/android/util/ar;->c:Lcom/dropbox/android/util/ar;

    .line 10
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dropbox/android/util/ar;

    sget-object v1, Lcom/dropbox/android/util/ar;->a:Lcom/dropbox/android/util/ar;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/util/ar;->b:Lcom/dropbox/android/util/ar;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/android/util/ar;->c:Lcom/dropbox/android/util/ar;

    aput-object v1, v0, v6

    sput-object v0, Lcom/dropbox/android/util/ar;->f:[Lcom/dropbox/android/util/ar;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/dropbox/android/util/bz;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/util/bz;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 19
    iput-object p3, p0, Lcom/dropbox/android/util/ar;->d:Lcom/dropbox/android/util/bz;

    .line 20
    iput p4, p0, Lcom/dropbox/android/util/ar;->e:I

    .line 21
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/util/ar;
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/dropbox/android/util/ar;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/ar;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/util/ar;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/dropbox/android/util/ar;->f:[Lcom/dropbox/android/util/ar;

    invoke-virtual {v0}, [Lcom/dropbox/android/util/ar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/util/ar;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;ZZ)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 28
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/dropbox/android/activity/DropboxWebViewActivity;

    invoke-direct {v1, p1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 29
    const-string v0, "EXTRA_TITLE"

    iget v2, p0, Lcom/dropbox/android/util/ar;->e:I

    invoke-virtual {p1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 31
    iget-object v0, p0, Lcom/dropbox/android/util/ar;->d:Lcom/dropbox/android/util/bz;

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/dropbox/android/util/bz;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    .line 32
    invoke-static {}, Lcom/dropbox/android/util/E;->a()Ljava/lang/String;

    move-result-object v2

    .line 33
    if-eqz v2, :cond_0

    .line 34
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "&oem_info="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 36
    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 38
    if-eqz p2, :cond_1

    .line 39
    const-string v0, "EXTRA_HAS_BUTTONS"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 42
    :cond_1
    const-string v0, "EXTRA_REQUIRES_AUTH"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 44
    return-object v1
.end method
