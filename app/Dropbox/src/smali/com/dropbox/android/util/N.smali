.class final Lcom/dropbox/android/util/N;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/dropbox/android/util/L;

.field private b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/dropbox/android/util/L;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 140
    iput-object p1, p0, Lcom/dropbox/android/util/N;->a:Lcom/dropbox/android/util/L;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    invoke-virtual {p0, p2}, Lcom/dropbox/android/util/N;->a(Ljava/lang/Object;)V

    .line 142
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 145
    iput-object p1, p0, Lcom/dropbox/android/util/N;->b:Ljava/lang/Object;

    .line 146
    return-void
.end method

.method public final run()V
    .locals 2

    .prologue
    .line 150
    invoke-static {}, Lcom/dropbox/android/util/L;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ShowDataRunnable running"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/dropbox/android/util/N;->a:Lcom/dropbox/android/util/L;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/util/L;->a(Lcom/dropbox/android/util/L;Lcom/dropbox/android/util/N;)Lcom/dropbox/android/util/N;

    .line 152
    iget-object v0, p0, Lcom/dropbox/android/util/N;->a:Lcom/dropbox/android/util/L;

    invoke-static {v0}, Lcom/dropbox/android/util/L;->a(Lcom/dropbox/android/util/L;)Lcom/dropbox/android/util/P;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/util/P;->d:Lcom/dropbox/android/util/P;

    if-eq v0, v1, :cond_0

    .line 153
    iget-object v0, p0, Lcom/dropbox/android/util/N;->a:Lcom/dropbox/android/util/L;

    sget-object v1, Lcom/dropbox/android/util/P;->d:Lcom/dropbox/android/util/P;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/L;->a(Lcom/dropbox/android/util/L;Lcom/dropbox/android/util/P;)Lcom/dropbox/android/util/P;

    .line 154
    invoke-static {}, Lcom/dropbox/android/util/L;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ShowDataRunnable showing data"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/dropbox/android/util/N;->a:Lcom/dropbox/android/util/L;

    iget-object v1, p0, Lcom/dropbox/android/util/N;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/L;->a(Ljava/lang/Object;)V

    .line 157
    :cond_0
    return-void
.end method
