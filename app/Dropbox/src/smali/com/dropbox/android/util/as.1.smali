.class public final Lcom/dropbox/android/util/as;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/dropbox/android/util/at;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z


# direct methods
.method public varargs constructor <init>([Lcom/dropbox/android/util/at;)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/util/as;->b:Z

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/dropbox/android/util/as;->a:Ljava/util/ArrayList;

    .line 38
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/dropbox/android/util/as;->b:Z

    const-string v1, "onCreate must be called."

    invoke-static {v0, v1}, Lcom/dropbox/android/util/C;->a(ZLjava/lang/String;)V

    .line 55
    iget-object v0, p0, Lcom/dropbox/android/util/as;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/at;

    .line 56
    invoke-interface {v0}, Lcom/dropbox/android/util/at;->a()V

    goto :goto_0

    .line 58
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/util/as;->b:Z

    .line 45
    iget-object v0, p0, Lcom/dropbox/android/util/as;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/at;

    .line 46
    invoke-interface {v0, p1}, Lcom/dropbox/android/util/at;->a(Landroid/os/Bundle;)V

    goto :goto_0

    .line 48
    :cond_0
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/dropbox/android/util/as;->b:Z

    const-string v1, "oCreate must be called."

    invoke-static {v0, v1}, Lcom/dropbox/android/util/C;->a(ZLjava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/dropbox/android/util/as;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/at;

    .line 66
    invoke-interface {v0, p1}, Lcom/dropbox/android/util/at;->b(Landroid/os/Bundle;)V

    goto :goto_0

    .line 68
    :cond_0
    return-void
.end method
