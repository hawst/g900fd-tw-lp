.class public final Lcom/dropbox/android/util/w;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:[B

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>([B)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    const/16 v0, 0x100

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/dropbox/android/util/w;->a:[B

    .line 105
    iput v1, p0, Lcom/dropbox/android/util/w;->b:I

    .line 106
    iput v1, p0, Lcom/dropbox/android/util/w;->c:I

    .line 112
    invoke-direct {p0, p1}, Lcom/dropbox/android/util/w;->a([B)V

    .line 113
    return-void
.end method

.method private a([B)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 163
    array-length v0, p1

    const/4 v2, 0x1

    if-lt v0, v2, :cond_0

    array-length v0, p1

    const/16 v2, 0x100

    if-le v0, v2, :cond_1

    .line 164
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Key should be between 1 and 256 bytes in length."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167
    :cond_1
    iput v1, p0, Lcom/dropbox/android/util/w;->b:I

    .line 168
    iput v1, p0, Lcom/dropbox/android/util/w;->c:I

    move v0, v1

    .line 170
    :goto_0
    iget-object v2, p0, Lcom/dropbox/android/util/w;->a:[B

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 171
    iget-object v2, p0, Lcom/dropbox/android/util/w;->a:[B

    int-to-byte v3, v0

    aput-byte v3, v2, v0

    .line 170
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 175
    :goto_1
    iget-object v2, p0, Lcom/dropbox/android/util/w;->a:[B

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 176
    array-length v2, p1

    rem-int v2, v1, v2

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    iget-object v3, p0, Lcom/dropbox/android/util/w;->a:[B

    aget-byte v3, v3, v1

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    and-int/lit16 v0, v0, 0xff

    .line 178
    iget-object v2, p0, Lcom/dropbox/android/util/w;->a:[B

    aget-byte v2, v2, v1

    .line 179
    iget-object v3, p0, Lcom/dropbox/android/util/w;->a:[B

    iget-object v4, p0, Lcom/dropbox/android/util/w;->a:[B

    aget-byte v4, v4, v0

    aput-byte v4, v3, v1

    .line 180
    iget-object v3, p0, Lcom/dropbox/android/util/w;->a:[B

    aput-byte v2, v3, v0

    .line 175
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 182
    :cond_3
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 6

    .prologue
    .line 120
    const/4 v0, 0x0

    :goto_0
    int-to-long v1, v0

    cmp-long v1, v1, p1

    if-gez v1, :cond_0

    .line 121
    iget v1, p0, Lcom/dropbox/android/util/w;->b:I

    add-int/lit8 v1, v1, 0x1

    and-int/lit16 v1, v1, 0xff

    iput v1, p0, Lcom/dropbox/android/util/w;->b:I

    .line 122
    iget-object v1, p0, Lcom/dropbox/android/util/w;->a:[B

    iget v2, p0, Lcom/dropbox/android/util/w;->b:I

    aget-byte v1, v1, v2

    iget v2, p0, Lcom/dropbox/android/util/w;->c:I

    add-int/2addr v1, v2

    and-int/lit16 v1, v1, 0xff

    iput v1, p0, Lcom/dropbox/android/util/w;->c:I

    .line 124
    iget-object v1, p0, Lcom/dropbox/android/util/w;->a:[B

    iget v2, p0, Lcom/dropbox/android/util/w;->b:I

    aget-byte v1, v1, v2

    .line 125
    iget-object v2, p0, Lcom/dropbox/android/util/w;->a:[B

    iget v3, p0, Lcom/dropbox/android/util/w;->b:I

    iget-object v4, p0, Lcom/dropbox/android/util/w;->a:[B

    iget v5, p0, Lcom/dropbox/android/util/w;->c:I

    aget-byte v4, v4, v5

    aput-byte v4, v2, v3

    .line 126
    iget-object v2, p0, Lcom/dropbox/android/util/w;->a:[B

    iget v3, p0, Lcom/dropbox/android/util/w;->c:I

    aput-byte v1, v2, v3

    .line 120
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 128
    :cond_0
    return-void
.end method

.method public final a([BI[BII)V
    .locals 7

    .prologue
    .line 138
    add-int v0, p2, p5

    array-length v1, p1

    if-le v0, v1, :cond_0

    .line 139
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "input buffer too short"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 142
    :cond_0
    add-int v0, p4, p5

    array-length v1, p3

    if-le v0, v1, :cond_1

    .line 143
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "output buffer too short"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 146
    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p5, :cond_2

    .line 147
    iget v1, p0, Lcom/dropbox/android/util/w;->b:I

    add-int/lit8 v1, v1, 0x1

    and-int/lit16 v1, v1, 0xff

    iput v1, p0, Lcom/dropbox/android/util/w;->b:I

    .line 148
    iget-object v1, p0, Lcom/dropbox/android/util/w;->a:[B

    iget v2, p0, Lcom/dropbox/android/util/w;->b:I

    aget-byte v1, v1, v2

    iget v2, p0, Lcom/dropbox/android/util/w;->c:I

    add-int/2addr v1, v2

    and-int/lit16 v1, v1, 0xff

    iput v1, p0, Lcom/dropbox/android/util/w;->c:I

    .line 150
    iget-object v1, p0, Lcom/dropbox/android/util/w;->a:[B

    iget v2, p0, Lcom/dropbox/android/util/w;->b:I

    aget-byte v1, v1, v2

    .line 151
    iget-object v2, p0, Lcom/dropbox/android/util/w;->a:[B

    iget v3, p0, Lcom/dropbox/android/util/w;->b:I

    iget-object v4, p0, Lcom/dropbox/android/util/w;->a:[B

    iget v5, p0, Lcom/dropbox/android/util/w;->c:I

    aget-byte v4, v4, v5

    aput-byte v4, v2, v3

    .line 152
    iget-object v2, p0, Lcom/dropbox/android/util/w;->a:[B

    iget v3, p0, Lcom/dropbox/android/util/w;->c:I

    aput-byte v1, v2, v3

    .line 154
    add-int v1, v0, p4

    add-int v2, v0, p2

    aget-byte v2, p1, v2

    iget-object v3, p0, Lcom/dropbox/android/util/w;->a:[B

    iget-object v4, p0, Lcom/dropbox/android/util/w;->a:[B

    iget v5, p0, Lcom/dropbox/android/util/w;->b:I

    aget-byte v4, v4, v5

    iget-object v5, p0, Lcom/dropbox/android/util/w;->a:[B

    iget v6, p0, Lcom/dropbox/android/util/w;->c:I

    aget-byte v5, v5, v6

    add-int/2addr v4, v5

    and-int/lit16 v4, v4, 0xff

    aget-byte v3, v3, v4

    xor-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, p3, v1

    .line 146
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 156
    :cond_2
    return-void
.end method
