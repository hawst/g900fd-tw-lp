.class Lcom/dropbox/android/util/Activities$AlbumSharePickerSpec;
.super Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;
.source "panda.py"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dropbox/android/util/Activities$AlbumSharePickerSpec;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/dropbox/android/albums/Album;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 277
    new-instance v0, Lcom/dropbox/android/util/b;

    invoke-direct {v0}, Lcom/dropbox/android/util/b;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/Activities$AlbumSharePickerSpec;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 272
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;-><init>()V

    .line 273
    sget-object v0, Lcom/dropbox/android/albums/Album;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/albums/Album;

    iput-object v0, p0, Lcom/dropbox/android/util/Activities$AlbumSharePickerSpec;->a:Lcom/dropbox/android/albums/Album;

    .line 274
    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/albums/Album;)V
    .locals 0

    .prologue
    .line 236
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;-><init>()V

    .line 237
    iput-object p1, p0, Lcom/dropbox/android/util/Activities$AlbumSharePickerSpec;->a:Lcom/dropbox/android/albums/Album;

    .line 238
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 242
    const v0, 0x7f0d02c6

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Intent;Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 247
    iget-object v0, p0, Lcom/dropbox/android/util/Activities$AlbumSharePickerSpec;->a:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/Album;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 248
    new-instance v0, Ldbxyzptlk/db231222/g/ak;

    invoke-virtual {p3}, Ldbxyzptlk/db231222/r/d;->r()Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/util/Activities$AlbumSharePickerSpec;->a:Lcom/dropbox/android/albums/Album;

    invoke-direct {v0, p2, v1, v2, p1}, Ldbxyzptlk/db231222/g/ak;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/albums/Album;Landroid/content/Intent;)V

    .line 249
    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/ak;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 260
    :goto_0
    return-void

    .line 251
    :cond_0
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aM()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "id"

    iget-object v2, p0, Lcom/dropbox/android/util/Activities$AlbumSharePickerSpec;->a:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v2}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "num.items"

    iget-object v2, p0, Lcom/dropbox/android/util/Activities$AlbumSharePickerSpec;->a:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v2}, Lcom/dropbox/android/albums/Album;->c()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "component.shared.to"

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "create"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 257
    iget-object v0, p0, Lcom/dropbox/android/util/Activities$AlbumSharePickerSpec;->a:Lcom/dropbox/android/albums/Album;

    iget-object v1, p0, Lcom/dropbox/android/util/Activities$AlbumSharePickerSpec;->a:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v1}, Lcom/dropbox/android/albums/Album;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/util/Activities$AlbumSharePickerSpec;->a:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v2}, Lcom/dropbox/android/albums/Album;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, p1, v0, v1, v2}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->a(Landroid/content/Context;Landroid/content/Intent;Lcom/dropbox/android/albums/Album;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 264
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/dropbox/android/util/Activities$AlbumSharePickerSpec;->a:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v0, p1, p2}, Lcom/dropbox/android/albums/Album;->writeToParcel(Landroid/os/Parcel;I)V

    .line 270
    return-void
.end method
