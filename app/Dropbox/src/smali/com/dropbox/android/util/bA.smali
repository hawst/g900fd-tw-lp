.class public final Lcom/dropbox/android/util/bA;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static a:Lcom/dropbox/android/util/bA;


# instance fields
.field private final b:Lcom/dropbox/android/util/bB;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 41
    if-eqz v0, :cond_0

    .line 42
    const/4 v1, 0x3

    const-string v2, "Dropbox network manager wifi lock"

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    .line 44
    new-instance v1, Lcom/dropbox/android/util/bB;

    invoke-direct {v1, v0}, Lcom/dropbox/android/util/bB;-><init>(Landroid/net/wifi/WifiManager$WifiLock;)V

    iput-object v1, p0, Lcom/dropbox/android/util/bA;->b:Lcom/dropbox/android/util/bB;

    .line 48
    :goto_0
    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/util/bA;->b:Lcom/dropbox/android/util/bB;

    goto :goto_0
.end method

.method public static declared-synchronized a()Lcom/dropbox/android/util/bA;
    .locals 3

    .prologue
    .line 14
    const-class v1, Lcom/dropbox/android/util/bA;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/dropbox/android/util/bA;->a:Lcom/dropbox/android/util/bA;

    if-nez v0, :cond_0

    .line 15
    new-instance v0, Lcom/dropbox/android/util/bA;

    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/dropbox/android/util/bA;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/dropbox/android/util/bA;->a:Lcom/dropbox/android/util/bA;

    .line 17
    :cond_0
    sget-object v0, Lcom/dropbox/android/util/bA;->a:Lcom/dropbox/android/util/bA;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 14
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final b()V
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/dropbox/android/util/bA;->b:Lcom/dropbox/android/util/bB;

    if-eqz v0, :cond_0

    .line 27
    iget-object v0, p0, Lcom/dropbox/android/util/bA;->b:Lcom/dropbox/android/util/bB;

    invoke-virtual {v0}, Lcom/dropbox/android/util/bB;->a()V

    .line 29
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/dropbox/android/util/bA;->b:Lcom/dropbox/android/util/bB;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/dropbox/android/util/bA;->b:Lcom/dropbox/android/util/bB;

    invoke-virtual {v0}, Lcom/dropbox/android/util/bB;->b()V

    .line 36
    :cond_0
    return-void
.end method
