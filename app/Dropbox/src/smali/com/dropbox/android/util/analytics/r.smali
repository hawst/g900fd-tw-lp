.class public Lcom/dropbox/android/util/analytics/r;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ldbxyzptlk/db231222/t/a;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ldbxyzptlk/db231222/x/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/x/c",
            "<",
            "Ldbxyzptlk/db231222/t/e;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/lang/String;

.field private static d:Lcom/dropbox/android/util/analytics/r;

.field private static final i:Ldbxyzptlk/db231222/t/a;


# instance fields
.field private e:Ldbxyzptlk/db231222/t/e;

.field private final f:Ldbxyzptlk/db231222/n/k;

.field private final g:Lcom/dropbox/android/filemanager/au;

.field private final h:Ldbxyzptlk/db231222/z/F;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 48
    const-class v0, Lcom/dropbox/android/util/analytics/r;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/util/analytics/r;->c:Ljava/lang/String;

    .line 71
    invoke-static {}, Ldbxyzptlk/db231222/t/a;->o()Ldbxyzptlk/db231222/t/c;

    move-result-object v0

    invoke-virtual {v0, v6}, Ldbxyzptlk/db231222/t/c;->a(I)Ldbxyzptlk/db231222/t/c;

    move-result-object v0

    const-string v1, "CONTROL"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/t/c;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/t/c;

    move-result-object v0

    invoke-virtual {v0, v6}, Ldbxyzptlk/db231222/t/c;->b(I)Ldbxyzptlk/db231222/t/c;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/t/c;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/t/c;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/c;->b()Ldbxyzptlk/db231222/t/a;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/util/analytics/r;->i:Ldbxyzptlk/db231222/t/a;

    .line 142
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 144
    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "alt-variant"

    aput-object v2, v1, v8

    const-string v2, "CONTROL"

    aput-object v2, v1, v6

    .line 145
    new-array v2, v7, [D

    fill-array-data v2, :array_0

    .line 146
    const-string v3, "mobile-test"

    const-string v4, "mobile-test"

    invoke-static {v1, v2}, Lcom/dropbox/android/util/analytics/r;->a([Ljava/lang/String;[D)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dropbox/android/util/analytics/r;->b(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/t/a;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    const-string v3, "mobile-test2"

    const-string v4, "mobile-test2"

    invoke-static {v1, v2}, Lcom/dropbox/android/util/analytics/r;->a([Ljava/lang/String;[D)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/dropbox/android/util/analytics/r;->b(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/t/a;

    move-result-object v1

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "no-kite"

    aput-object v2, v1, v8

    const-string v2, "new-account"

    aput-object v2, v1, v6

    const-string v2, "new-account-no-kite"

    aput-object v2, v1, v7

    const-string v2, "CONTROL"

    aput-object v2, v1, v9

    .line 155
    new-array v2, v9, [D

    fill-array-data v2, :array_1

    .line 156
    const-string v3, "mobile-kite-tour"

    const-string v4, "mobile-kite-tour"

    invoke-static {v1, v2}, Lcom/dropbox/android/util/analytics/r;->a([Ljava/lang/String;[D)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/dropbox/android/util/analytics/r;->b(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/t/a;

    move-result-object v1

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    const-string v1, "mobile-contacts-local-upload"

    const-string v2, "mobile-contacts-local-upload"

    const-string v3, "CONTROL"

    invoke-static {v2, v3}, Lcom/dropbox/android/util/analytics/r;->b(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/t/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    const-string v1, "mobile-payment-selector-v1"

    const-string v2, "mobile-payment-selector-v1"

    const-string v3, "CONTROL"

    invoke-static {v2, v3}, Lcom/dropbox/android/util/analytics/r;->b(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/t/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    const-string v1, "mobile-payment-selector-enable"

    const-string v2, "mobile-payment-selector-enable"

    const-string v3, "CONTROL"

    invoke-static {v2, v3}, Lcom/dropbox/android/util/analytics/r;->b(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/t/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    const-string v1, "mobile-monsoon-upgrade-plan"

    const-string v2, "mobile-monsoon-upgrade-plan"

    const-string v3, "CONTROL"

    invoke-static {v2, v3}, Lcom/dropbox/android/util/analytics/r;->b(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/t/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    const-string v1, "mobile-remote-installer"

    const-string v2, "mobile-remote-installer"

    const-string v3, "NOT_IN_EXPERIMENT"

    invoke-static {v2, v3}, Lcom/dropbox/android/util/analytics/r;->b(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/t/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/util/analytics/r;->a:Ljava/util/Map;

    .line 405
    new-instance v0, Lcom/dropbox/android/util/analytics/t;

    invoke-direct {v0}, Lcom/dropbox/android/util/analytics/t;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/analytics/r;->b:Ldbxyzptlk/db231222/x/c;

    return-void

    .line 145
    :array_0
    .array-data 8
        0x3fe0000000000000L    # 0.5
        0x3fe0000000000000L    # 0.5
    .end array-data

    .line 155
    :array_1
    .array-data 8
        0x3f9eb851eb851eb8L    # 0.03
        0x3f9eb851eb851eb8L    # 0.03
        0x3f9eb851eb851eb8L    # 0.03
    .end array-data
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/n/k;Lcom/dropbox/android/filemanager/au;Ldbxyzptlk/db231222/z/F;)V
    .locals 1

    .prologue
    .line 201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 202
    iput-object p1, p0, Lcom/dropbox/android/util/analytics/r;->f:Ldbxyzptlk/db231222/n/k;

    .line 203
    iput-object p2, p0, Lcom/dropbox/android/util/analytics/r;->g:Lcom/dropbox/android/filemanager/au;

    .line 204
    iput-object p3, p0, Lcom/dropbox/android/util/analytics/r;->h:Ldbxyzptlk/db231222/z/F;

    .line 205
    invoke-virtual {p1}, Ldbxyzptlk/db231222/n/k;->e()Ldbxyzptlk/db231222/t/e;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/util/analytics/r;->e:Ldbxyzptlk/db231222/t/e;

    .line 206
    iget-object v0, p0, Lcom/dropbox/android/util/analytics/r;->e:Ldbxyzptlk/db231222/t/e;

    if-nez v0, :cond_0

    .line 207
    invoke-static {}, Ldbxyzptlk/db231222/t/e;->a()Ldbxyzptlk/db231222/t/e;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/util/analytics/r;->e:Ldbxyzptlk/db231222/t/e;

    .line 209
    :cond_0
    return-void
.end method

.method public static declared-synchronized a()Lcom/dropbox/android/util/analytics/r;
    .locals 2

    .prologue
    .line 195
    const-class v1, Lcom/dropbox/android/util/analytics/r;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/dropbox/android/util/analytics/r;->d:Lcom/dropbox/android/util/analytics/r;

    if-nez v0, :cond_0

    .line 196
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 195
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 198
    :cond_0
    :try_start_1
    sget-object v0, Lcom/dropbox/android/util/analytics/r;->d:Lcom/dropbox/android/util/analytics/r;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method public static declared-synchronized a(Ldbxyzptlk/db231222/n/k;Lcom/dropbox/android/filemanager/au;Ldbxyzptlk/db231222/z/F;)Lcom/dropbox/android/util/analytics/r;
    .locals 2

    .prologue
    .line 186
    const-class v1, Lcom/dropbox/android/util/analytics/r;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/dropbox/android/util/analytics/r;->d:Lcom/dropbox/android/util/analytics/r;

    if-nez v0, :cond_0

    .line 187
    new-instance v0, Lcom/dropbox/android/util/analytics/r;

    invoke-direct {v0, p0, p1, p2}, Lcom/dropbox/android/util/analytics/r;-><init>(Ldbxyzptlk/db231222/n/k;Lcom/dropbox/android/filemanager/au;Ldbxyzptlk/db231222/z/F;)V

    sput-object v0, Lcom/dropbox/android/util/analytics/r;->d:Lcom/dropbox/android/util/analytics/r;

    .line 188
    sget-object v0, Lcom/dropbox/android/util/analytics/r;->d:Lcom/dropbox/android/util/analytics/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 190
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 186
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/dropbox/android/util/analytics/r;)Ldbxyzptlk/db231222/z/F;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/dropbox/android/util/analytics/r;->h:Ldbxyzptlk/db231222/z/F;

    return-object v0
.end method

.method private static a([Ljava/lang/String;[D)Ljava/lang/String;
    .locals 5

    .prologue
    .line 243
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v1

    .line 246
    const/4 v0, 0x0

    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_1

    .line 247
    aget-wide v3, p1, v0

    cmpg-double v3, v1, v3

    if-gez v3, :cond_0

    .line 248
    aget-object v0, p0, v0

    .line 253
    :goto_1
    return-object v0

    .line 250
    :cond_0
    aget-wide v3, p1, v0

    sub-double/2addr v1, v3

    .line 246
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 253
    :cond_1
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    aget-object v0, p0, v0

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 333
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bE()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "feature"

    invoke-virtual {v0, v1, p1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "variant"

    invoke-virtual {v0, v1, p2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "used_default"

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 337
    return-void
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/t/a;
    .locals 1

    .prologue
    .line 374
    sget-object v0, Lcom/dropbox/android/util/analytics/r;->i:Ldbxyzptlk/db231222/t/a;

    invoke-static {v0}, Ldbxyzptlk/db231222/t/a;->a(Ldbxyzptlk/db231222/t/a;)Ldbxyzptlk/db231222/t/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Ldbxyzptlk/db231222/t/c;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/t/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/t/c;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/t/c;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/c;->b()Ldbxyzptlk/db231222/t/a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/dropbox/android/util/analytics/r;->c:Ljava/lang/String;

    return-object v0
.end method

.method private declared-synchronized c(Ljava/lang/String;)Ldbxyzptlk/db231222/t/a;
    .locals 3

    .prologue
    .line 213
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/util/analytics/r;->e:Ldbxyzptlk/db231222/t/e;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/e;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/a;

    .line 214
    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/a;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 218
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 213
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 226
    sget-object v0, Lcom/dropbox/android/util/analytics/r;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 227
    invoke-direct {p0, v0}, Lcom/dropbox/android/util/analytics/r;->c(Ljava/lang/String;)Ldbxyzptlk/db231222/t/a;

    move-result-object v0

    if-nez v0, :cond_0

    .line 228
    const/4 v0, 0x1

    .line 231
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Ljava/lang/String;)Ldbxyzptlk/db231222/t/a;
    .locals 2

    .prologue
    .line 257
    sget-object v0, Lcom/dropbox/android/util/analytics/r;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/a;

    .line 258
    const-string v1, "Gandalf feature with no default"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 259
    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 271
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/dropbox/android/util/analytics/r;->c(Ljava/lang/String;)Ldbxyzptlk/db231222/t/a;

    move-result-object v1

    .line 272
    const/4 v0, 0x0

    .line 273
    if-nez v1, :cond_0

    .line 274
    invoke-static {p1}, Lcom/dropbox/android/util/analytics/r;->d(Ljava/lang/String;)Ldbxyzptlk/db231222/t/a;

    move-result-object v1

    .line 275
    iget-object v0, p0, Lcom/dropbox/android/util/analytics/r;->e:Ldbxyzptlk/db231222/t/e;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/e;->j()Ldbxyzptlk/db231222/t/g;

    move-result-object v0

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/t/g;->a(Ldbxyzptlk/db231222/t/a;)Ldbxyzptlk/db231222/t/g;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/g;->b()Ldbxyzptlk/db231222/t/e;

    .line 276
    iget-object v0, p0, Lcom/dropbox/android/util/analytics/r;->f:Ldbxyzptlk/db231222/n/k;

    iget-object v2, p0, Lcom/dropbox/android/util/analytics/r;->e:Ldbxyzptlk/db231222/t/e;

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/n/k;->a(Ldbxyzptlk/db231222/t/e;)V

    .line 277
    const/4 v0, 0x1

    .line 279
    :cond_0
    invoke-virtual {v1}, Ldbxyzptlk/db231222/t/a;->i()Ljava/lang/String;

    move-result-object v1

    .line 280
    const-string v2, "NOT_IN_EXPERIMENT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 281
    invoke-direct {p0, p1, v1, v0}, Lcom/dropbox/android/util/analytics/r;->a(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283
    :cond_1
    monitor-exit p0

    return-object v1

    .line 271
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ldbxyzptlk/db231222/r/k;Ldbxyzptlk/db231222/i/c;Ljava/util/concurrent/Executor;)V
    .locals 3

    .prologue
    .line 347
    invoke-direct {p0}, Lcom/dropbox/android/util/analytics/r;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 371
    :goto_0
    return-void

    .line 351
    :cond_0
    if-eqz p1, :cond_1

    .line 352
    iget-object v0, p0, Lcom/dropbox/android/util/analytics/r;->g:Lcom/dropbox/android/filemanager/au;

    sget-object v1, Lcom/dropbox/android/filemanager/aC;->b:Lcom/dropbox/android/filemanager/aC;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/k;->f()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p0}, Lcom/dropbox/android/filemanager/au;->a(Lcom/dropbox/android/filemanager/aC;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/util/analytics/r;)V

    goto :goto_0

    .line 355
    :cond_1
    new-instance v0, Lcom/dropbox/android/util/analytics/s;

    invoke-direct {v0, p0, p2}, Lcom/dropbox/android/util/analytics/s;-><init>(Lcom/dropbox/android/util/analytics/r;Ldbxyzptlk/db231222/i/c;)V

    invoke-interface {p3, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Ldbxyzptlk/db231222/t/e;)V
    .locals 5

    .prologue
    .line 382
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 383
    iget-object v0, p0, Lcom/dropbox/android/util/analytics/r;->e:Ldbxyzptlk/db231222/t/e;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/e;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/a;

    .line 384
    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/a;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 382
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 386
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Ldbxyzptlk/db231222/t/e;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/a;

    .line 388
    sget-object v1, Lcom/dropbox/android/util/analytics/r;->a:Ljava/util/Map;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/a;->d()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 392
    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldbxyzptlk/db231222/t/a;

    .line 393
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ldbxyzptlk/db231222/t/a;->l()I

    move-result v1

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/a;->l()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 394
    :cond_2
    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 397
    :cond_3
    invoke-static {}, Ldbxyzptlk/db231222/t/e;->i()Ldbxyzptlk/db231222/t/g;

    move-result-object v0

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/t/g;->a(Ljava/lang/Iterable;)Ldbxyzptlk/db231222/t/g;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/t/g;->a(J)Ldbxyzptlk/db231222/t/g;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/g;->b()Ldbxyzptlk/db231222/t/e;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/util/analytics/r;->e:Ldbxyzptlk/db231222/t/e;

    .line 399
    iget-object v0, p0, Lcom/dropbox/android/util/analytics/r;->f:Ldbxyzptlk/db231222/n/k;

    iget-object v1, p0, Lcom/dropbox/android/util/analytics/r;->e:Ldbxyzptlk/db231222/t/e;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/k;->a(Ldbxyzptlk/db231222/t/e;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 400
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 300
    monitor-enter p0

    :try_start_0
    invoke-static {p1, p2}, Lcom/dropbox/android/util/analytics/r;->b(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/t/a;

    move-result-object v1

    .line 301
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 302
    iget-object v0, p0, Lcom/dropbox/android/util/analytics/r;->e:Ldbxyzptlk/db231222/t/e;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/e;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/a;

    .line 303
    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/a;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 304
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 300
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 307
    :cond_1
    :try_start_1
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 308
    iget-object v0, p0, Lcom/dropbox/android/util/analytics/r;->e:Ldbxyzptlk/db231222/t/e;

    invoke-static {v0}, Ldbxyzptlk/db231222/t/e;->a(Ldbxyzptlk/db231222/t/e;)Ldbxyzptlk/db231222/t/g;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/g;->f()Ldbxyzptlk/db231222/t/g;

    move-result-object v0

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/t/g;->a(Ljava/lang/Iterable;)Ldbxyzptlk/db231222/t/g;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/g;->b()Ldbxyzptlk/db231222/t/e;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/util/analytics/r;->e:Ldbxyzptlk/db231222/t/e;

    .line 312
    iget-object v0, p0, Lcom/dropbox/android/util/analytics/r;->f:Ldbxyzptlk/db231222/n/k;

    iget-object v1, p0, Lcom/dropbox/android/util/analytics/r;->e:Ldbxyzptlk/db231222/t/e;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/k;->a(Ldbxyzptlk/db231222/t/e;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 313
    monitor-exit p0

    return-void
.end method

.method public final varargs a(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 323
    invoke-virtual {p0, p1}, Lcom/dropbox/android/util/analytics/r;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 324
    array-length v3, p2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, p2, v1

    .line 325
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 326
    const/4 v0, 0x1

    .line 329
    :cond_0
    return v0

    .line 324
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 292
    invoke-direct {p0, p1}, Lcom/dropbox/android/util/analytics/r;->c(Ljava/lang/String;)Ldbxyzptlk/db231222/t/a;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
