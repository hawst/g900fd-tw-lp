.class public abstract Lcom/dropbox/android/util/L;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/os/Handler;

.field private c:Lcom/dropbox/android/util/P;

.field private d:Lcom/dropbox/android/util/O;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/util/L",
            "<TT;>.com/dropbox/android/util/O;"
        }
    .end annotation
.end field

.field private e:Lcom/dropbox/android/util/N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dropbox/android/util/L",
            "<TT;>.com/dropbox/android/util/N;"
        }
    .end annotation
.end field

.field private f:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/dropbox/android/util/L;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/util/L;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    sget-object v0, Lcom/dropbox/android/util/P;->d:Lcom/dropbox/android/util/P;

    iput-object v0, p0, Lcom/dropbox/android/util/L;->c:Lcom/dropbox/android/util/P;

    .line 37
    iput-object p1, p0, Lcom/dropbox/android/util/L;->b:Landroid/os/Handler;

    .line 38
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/util/L;J)J
    .locals 0

    .prologue
    .line 16
    iput-wide p1, p0, Lcom/dropbox/android/util/L;->f:J

    return-wide p1
.end method

.method static synthetic a(Lcom/dropbox/android/util/L;Lcom/dropbox/android/util/N;)Lcom/dropbox/android/util/N;
    .locals 0

    .prologue
    .line 16
    iput-object p1, p0, Lcom/dropbox/android/util/L;->e:Lcom/dropbox/android/util/N;

    return-object p1
.end method

.method static synthetic a(Lcom/dropbox/android/util/L;Lcom/dropbox/android/util/O;)Lcom/dropbox/android/util/O;
    .locals 0

    .prologue
    .line 16
    iput-object p1, p0, Lcom/dropbox/android/util/L;->d:Lcom/dropbox/android/util/O;

    return-object p1
.end method

.method static synthetic a(Lcom/dropbox/android/util/L;)Lcom/dropbox/android/util/P;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/dropbox/android/util/L;->c:Lcom/dropbox/android/util/P;

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/util/L;Lcom/dropbox/android/util/P;)Lcom/dropbox/android/util/P;
    .locals 0

    .prologue
    .line 16
    iput-object p1, p0, Lcom/dropbox/android/util/L;->c:Lcom/dropbox/android/util/P;

    return-object p1
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/dropbox/android/util/L;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected abstract a()V
.end method

.method protected abstract a(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 57
    sget-object v0, Lcom/dropbox/android/util/L;->a:Ljava/lang/String;

    const-string v1, "loadingStarted()"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 63
    invoke-virtual {p0}, Lcom/dropbox/android/util/L;->c()V

    .line 65
    sget-object v0, Lcom/dropbox/android/util/P;->a:Lcom/dropbox/android/util/P;

    iput-object v0, p0, Lcom/dropbox/android/util/L;->c:Lcom/dropbox/android/util/P;

    .line 66
    new-instance v0, Lcom/dropbox/android/util/O;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/util/O;-><init>(Lcom/dropbox/android/util/L;Lcom/dropbox/android/util/M;)V

    iput-object v0, p0, Lcom/dropbox/android/util/L;->d:Lcom/dropbox/android/util/O;

    .line 67
    iget-object v0, p0, Lcom/dropbox/android/util/L;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/dropbox/android/util/L;->d:Lcom/dropbox/android/util/O;

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 68
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 77
    sget-object v0, Lcom/dropbox/android/util/L;->a:Ljava/lang/String;

    const-string v1, "loadingFinished()"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 79
    iget-object v0, p0, Lcom/dropbox/android/util/L;->c:Lcom/dropbox/android/util/P;

    sget-object v1, Lcom/dropbox/android/util/P;->c:Lcom/dropbox/android/util/P;

    if-ne v0, v1, :cond_0

    .line 81
    sget-object v0, Lcom/dropbox/android/util/L;->a:Ljava/lang/String;

    const-string v1, "got data update while waiting to show previous data"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/dropbox/android/util/L;->e:Lcom/dropbox/android/util/N;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/N;->a(Ljava/lang/Object;)V

    .line 101
    :goto_0
    return-void

    .line 84
    :cond_0
    const-wide/16 v0, 0x1f4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/dropbox/android/util/L;->f:J

    sub-long/2addr v2, v4

    sub-long/2addr v0, v2

    .line 86
    iget-object v2, p0, Lcom/dropbox/android/util/L;->c:Lcom/dropbox/android/util/P;

    sget-object v3, Lcom/dropbox/android/util/P;->b:Lcom/dropbox/android/util/P;

    if-ne v2, v3, :cond_1

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 88
    sget-object v2, Lcom/dropbox/android/util/L;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "delayed showing data by "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " millis"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    sget-object v2, Lcom/dropbox/android/util/P;->c:Lcom/dropbox/android/util/P;

    iput-object v2, p0, Lcom/dropbox/android/util/L;->c:Lcom/dropbox/android/util/P;

    .line 90
    new-instance v2, Lcom/dropbox/android/util/N;

    invoke-direct {v2, p0, p1}, Lcom/dropbox/android/util/N;-><init>(Lcom/dropbox/android/util/L;Ljava/lang/Object;)V

    iput-object v2, p0, Lcom/dropbox/android/util/L;->e:Lcom/dropbox/android/util/N;

    .line 91
    iget-object v2, p0, Lcom/dropbox/android/util/L;->b:Landroid/os/Handler;

    iget-object v3, p0, Lcom/dropbox/android/util/L;->e:Lcom/dropbox/android/util/N;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 94
    :cond_1
    sget-object v0, Lcom/dropbox/android/util/L;->a:Ljava/lang/String;

    const-string v1, "showing data immediately"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    sget-object v0, Lcom/dropbox/android/util/P;->d:Lcom/dropbox/android/util/P;

    iput-object v0, p0, Lcom/dropbox/android/util/L;->c:Lcom/dropbox/android/util/P;

    .line 96
    iget-object v0, p0, Lcom/dropbox/android/util/L;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/dropbox/android/util/L;->d:Lcom/dropbox/android/util/O;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/util/L;->d:Lcom/dropbox/android/util/O;

    .line 98
    invoke-virtual {p0, p1}, Lcom/dropbox/android/util/L;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 108
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 109
    sget-object v0, Lcom/dropbox/android/util/P;->d:Lcom/dropbox/android/util/P;

    iput-object v0, p0, Lcom/dropbox/android/util/L;->c:Lcom/dropbox/android/util/P;

    .line 110
    iget-object v0, p0, Lcom/dropbox/android/util/L;->d:Lcom/dropbox/android/util/O;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/dropbox/android/util/L;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/dropbox/android/util/L;->d:Lcom/dropbox/android/util/O;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 112
    iput-object v2, p0, Lcom/dropbox/android/util/L;->d:Lcom/dropbox/android/util/O;

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/util/L;->e:Lcom/dropbox/android/util/N;

    if-eqz v0, :cond_1

    .line 115
    iget-object v0, p0, Lcom/dropbox/android/util/L;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/dropbox/android/util/L;->e:Lcom/dropbox/android/util/N;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 116
    iput-object v2, p0, Lcom/dropbox/android/util/L;->e:Lcom/dropbox/android/util/N;

    .line 118
    :cond_1
    return-void
.end method
