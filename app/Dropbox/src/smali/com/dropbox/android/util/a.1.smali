.class final Lcom/dropbox/android/util/a;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/widget/aX;


# instance fields
.field final synthetic a:Landroid/support/v4/app/FragmentActivity;

.field final synthetic b:Ldbxyzptlk/db231222/r/d;

.field final synthetic c:Lcom/dropbox/android/filemanager/LocalEntry;


# direct methods
.method constructor <init>(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;)V
    .locals 0

    .prologue
    .line 374
    iput-object p1, p0, Lcom/dropbox/android/util/a;->a:Landroid/support/v4/app/FragmentActivity;

    iput-object p2, p0, Lcom/dropbox/android/util/a;->b:Ldbxyzptlk/db231222/r/d;

    iput-object p3, p0, Lcom/dropbox/android/util/a;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 392
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 377
    iget-object v0, p0, Lcom/dropbox/android/util/a;->a:Landroid/support/v4/app/FragmentActivity;

    if-eqz v0, :cond_0

    .line 378
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/dropbox/android/activity/LocalFileBrowserActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 379
    iget-object v0, p0, Lcom/dropbox/android/util/a;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 389
    :cond_0
    :goto_0
    return-void

    .line 381
    :cond_1
    new-instance v0, Ldbxyzptlk/db231222/g/d;

    iget-object v1, p0, Lcom/dropbox/android/util/a;->a:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/dropbox/android/util/a;->b:Ldbxyzptlk/db231222/r/d;

    iget-object v3, p0, Lcom/dropbox/android/util/a;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-direct {v0, v1, v2, v3, p1}, Ldbxyzptlk/db231222/g/d;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/Intent;)V

    .line 383
    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->a(Ldbxyzptlk/db231222/g/i;)Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;

    move-result-object v1

    .line 384
    iget-object v2, p0, Lcom/dropbox/android/util/a;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 385
    invoke-virtual {v0}, Ldbxyzptlk/db231222/g/d;->f()V

    .line 386
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
