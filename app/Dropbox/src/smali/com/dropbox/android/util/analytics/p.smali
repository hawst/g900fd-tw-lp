.class final Lcom/dropbox/android/util/analytics/p;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field final a:Ljava/lang/String;

.field final b:J

.field final c:J

.field final d:J


# direct methods
.method private constructor <init>(Ljava/lang/String;JJJ)V
    .locals 0

    .prologue
    .line 1953
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1954
    iput-object p1, p0, Lcom/dropbox/android/util/analytics/p;->a:Ljava/lang/String;

    .line 1955
    iput-wide p2, p0, Lcom/dropbox/android/util/analytics/p;->b:J

    .line 1956
    iput-wide p4, p0, Lcom/dropbox/android/util/analytics/p;->c:J

    .line 1957
    iput-wide p6, p0, Lcom/dropbox/android/util/analytics/p;->d:J

    .line 1958
    return-void
.end method

.method public static a(Ljava/io/File;)Lcom/dropbox/android/util/analytics/p;
    .locals 10

    .prologue
    .line 1961
    new-instance v0, Landroid/os/StatFs;

    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1962
    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/StatFs;->restat(Ljava/lang/String;)V

    .line 1963
    invoke-static {v0}, Lcom/dropbox/android/util/t;->c(Landroid/os/StatFs;)J

    move-result-wide v4

    .line 1964
    invoke-static {v0}, Lcom/dropbox/android/util/t;->a(Landroid/os/StatFs;)J

    move-result-wide v1

    mul-long v2, v4, v1

    .line 1965
    invoke-static {v0}, Lcom/dropbox/android/util/t;->b(Landroid/os/StatFs;)J

    move-result-wide v6

    mul-long/2addr v6, v4

    .line 1966
    invoke-static {v0}, Lcom/dropbox/android/util/t;->d(Landroid/os/StatFs;)J

    move-result-wide v8

    invoke-static {v0}, Lcom/dropbox/android/util/t;->a(Landroid/os/StatFs;)J

    move-result-wide v0

    sub-long v0, v8, v0

    mul-long/2addr v4, v0

    .line 1968
    new-instance v0, Lcom/dropbox/android/util/analytics/p;

    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct/range {v0 .. v7}, Lcom/dropbox/android/util/analytics/p;-><init>(Ljava/lang/String;JJJ)V

    return-object v0
.end method
