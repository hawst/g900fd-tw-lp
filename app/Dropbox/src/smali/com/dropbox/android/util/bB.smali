.class final Lcom/dropbox/android/util/bB;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Landroid/net/wifi/WifiManager$WifiLock;

.field private b:I


# direct methods
.method public constructor <init>(Landroid/net/wifi/WifiManager$WifiLock;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput v0, p0, Lcom/dropbox/android/util/bB;->b:I

    .line 60
    invoke-virtual {p1, v0}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    .line 61
    iput-object p1, p0, Lcom/dropbox/android/util/bB;->a:Landroid/net/wifi/WifiManager$WifiLock;

    .line 62
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 65
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/dropbox/android/util/bB;->b:I

    if-nez v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/dropbox/android/util/bB;->a:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    .line 68
    :cond_0
    iget v0, p0, Lcom/dropbox/android/util/bB;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/util/bB;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    monitor-exit p0

    return-void

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 72
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/dropbox/android/util/bB;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/dropbox/android/util/bB;->b:I

    .line 73
    iget v0, p0, Lcom/dropbox/android/util/bB;->b:I

    if-nez v0, :cond_1

    .line 74
    iget-object v0, p0, Lcom/dropbox/android/util/bB;->a:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    :cond_0
    monitor-exit p0

    return-void

    .line 75
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/dropbox/android/util/bB;->b:I

    if-gez v0, :cond_0

    .line 76
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Wifi lock released more times than acquired."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
