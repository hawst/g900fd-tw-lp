.class public Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;
.super Lcom/dropbox/android/util/HistoryEntry;
.source "panda.py"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Lcom/dropbox/android/util/DropboxPath;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 149
    new-instance v0, Lcom/dropbox/android/util/ai;

    invoke-direct {v0}, Lcom/dropbox/android/util/ai;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 145
    invoke-direct {p0, p1}, Lcom/dropbox/android/util/HistoryEntry;-><init>(Landroid/os/Parcel;)V

    .line 146
    const-class v0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    iput-object v0, p0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->c:Lcom/dropbox/android/util/DropboxPath;

    .line 147
    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/util/DropboxPath;)V
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lcom/dropbox/android/util/al;->a:Lcom/dropbox/android/util/al;

    invoke-direct {p0, v0}, Lcom/dropbox/android/util/HistoryEntry;-><init>(Lcom/dropbox/android/util/al;)V

    .line 95
    iput-object p1, p0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->c:Lcom/dropbox/android/util/DropboxPath;

    .line 96
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->c:Lcom/dropbox/android/util/DropboxPath;

    invoke-static {p1, v0}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/content/res/Resources;Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 105
    const v0, 0x7f0d004e

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 110
    const v0, 0x7f0d0052

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->c:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/dropbox/android/util/HistoryEntry;
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->c:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 117
    new-instance v0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    iget-object v1, p0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->c:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->g()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    .line 119
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 129
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->c:Lcom/dropbox/android/util/DropboxPath;

    check-cast p1, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    iget-object v1, p1, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->c:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/DropboxPath;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/dropbox/android/util/DropboxPath;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->c:Lcom/dropbox/android/util/DropboxPath;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->c:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->hashCode()I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 140
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/HistoryEntry;->writeToParcel(Landroid/os/Parcel;I)V

    .line 141
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;->c:Lcom/dropbox/android/util/DropboxPath;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 142
    return-void
.end method
