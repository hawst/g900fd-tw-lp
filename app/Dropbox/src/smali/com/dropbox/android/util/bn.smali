.class public final Lcom/dropbox/android/util/bn;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static a:Ldbxyzptlk/db231222/v/n;


# direct methods
.method public static a(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 77
    const v0, 0x1010030

    invoke-static {p0, v0}, Lcom/dropbox/android/util/bn;->a(Landroid/content/Context;I)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;I)I
    .locals 1

    .prologue
    .line 81
    invoke-static {p0, p1}, Lcom/dropbox/android/util/bn;->b(Landroid/content/Context;I)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/res/Resources;)I
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    return v0
.end method

.method public static a(Landroid/content/res/Configuration;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 61
    iget v0, p0, Landroid/content/res/Configuration;->screenHeightDp:I

    iget v1, p0, Landroid/content/res/Configuration;->screenWidthDp:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 62
    iget v1, p0, Landroid/content/res/Configuration;->screenHeightDp:I

    iget v2, p0, Landroid/content/res/Configuration;->screenWidthDp:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 63
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "x"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 64
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/dropbox/android/util/bn;->b(Landroid/content/res/Configuration;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a()Z
    .locals 2

    .prologue
    .line 24
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 26
    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v1, 0x258

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;I)I
    .locals 3

    .prologue
    .line 85
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 86
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 87
    iget v0, v0, Landroid/util/TypedValue;->data:I

    return v0
.end method

.method public static b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 72
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 73
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getDeviceConfigurationInfo()Landroid/content/pm/ConfigurationInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/ConfigurationInfo;->getGlEsVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/content/res/Configuration;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    packed-switch v0, :pswitch_data_0

    .line 49
    iget v0, p0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 50
    const-string v0, "x-large"

    .line 52
    :goto_0
    return-object v0

    .line 41
    :pswitch_0
    const-string v0, "small"

    goto :goto_0

    .line 43
    :pswitch_1
    const-string v0, "normal"

    goto :goto_0

    .line 45
    :pswitch_2
    const-string v0, "large"

    goto :goto_0

    .line 47
    :pswitch_3
    const-string v0, "undefined"

    goto :goto_0

    .line 52
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 39
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static c()Z
    .locals 2

    .prologue
    .line 91
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 92
    const-string v1, "BNRV200"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "BNTV250"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "NOOKcolor"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "BNTV250A"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d()I
    .locals 1

    .prologue
    .line 97
    const v0, 0x1030128

    return v0
.end method

.method public static e()I
    .locals 1

    .prologue
    .line 101
    const v0, 0x103006e

    return v0
.end method

.method public static f()Ldbxyzptlk/db231222/v/n;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Ldbxyzptlk/db231222/v/n;->e:Ldbxyzptlk/db231222/v/n;

    return-object v0
.end method

.method public static g()Ldbxyzptlk/db231222/v/n;
    .locals 1

    .prologue
    .line 109
    sget-object v0, Ldbxyzptlk/db231222/v/n;->c:Ldbxyzptlk/db231222/v/n;

    return-object v0
.end method

.method public static h()Ldbxyzptlk/db231222/v/n;
    .locals 1

    .prologue
    .line 113
    sget-object v0, Ldbxyzptlk/db231222/v/n;->c:Ldbxyzptlk/db231222/v/n;

    return-object v0
.end method

.method public static i()I
    .locals 3

    .prologue
    .line 121
    invoke-static {}, Lcom/dropbox/android/util/S;->c()I

    move-result v0

    .line 122
    const/16 v1, 0x30

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    const/4 v2, 0x0

    add-int/lit8 v0, v0, -0x30

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    .line 125
    const/16 v1, 0x64

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public static j()I
    .locals 3

    .prologue
    .line 129
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 131
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 132
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 133
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 134
    iget v0, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 137
    div-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public static k()Ldbxyzptlk/db231222/v/n;
    .locals 5

    .prologue
    const/16 v3, 0x280

    .line 141
    sget-object v0, Lcom/dropbox/android/util/bn;->a:Ldbxyzptlk/db231222/v/n;

    if-nez v0, :cond_0

    .line 142
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 143
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 144
    iget v0, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 145
    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 146
    if-le v1, v0, :cond_3

    .line 158
    :goto_0
    if-gt v1, v3, :cond_1

    const/16 v2, 0x1e0

    if-gt v0, v2, :cond_1

    .line 159
    sget-object v0, Ldbxyzptlk/db231222/v/n;->h:Ldbxyzptlk/db231222/v/n;

    sput-object v0, Lcom/dropbox/android/util/bn;->a:Ldbxyzptlk/db231222/v/n;

    .line 167
    :cond_0
    :goto_1
    sget-object v0, Lcom/dropbox/android/util/bn;->a:Ldbxyzptlk/db231222/v/n;

    return-object v0

    .line 160
    :cond_1
    const/16 v2, 0x3c0

    if-gt v1, v2, :cond_2

    if-gt v0, v3, :cond_2

    .line 161
    sget-object v0, Ldbxyzptlk/db231222/v/n;->i:Ldbxyzptlk/db231222/v/n;

    sput-object v0, Lcom/dropbox/android/util/bn;->a:Ldbxyzptlk/db231222/v/n;

    goto :goto_1

    .line 163
    :cond_2
    sget-object v0, Ldbxyzptlk/db231222/v/n;->j:Ldbxyzptlk/db231222/v/n;

    sput-object v0, Lcom/dropbox/android/util/bn;->a:Ldbxyzptlk/db231222/v/n;

    goto :goto_1

    :cond_3
    move v4, v1

    move v1, v0

    move v0, v4

    goto :goto_0
.end method
