.class public abstract enum Lcom/dropbox/android/util/aH;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/util/aH;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/util/aH;

.field public static final enum b:Lcom/dropbox/android/util/aH;

.field public static final enum c:Lcom/dropbox/android/util/aH;

.field public static final enum d:Lcom/dropbox/android/util/aH;

.field private static final synthetic e:[Lcom/dropbox/android/util/aH;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 362
    new-instance v0, Lcom/dropbox/android/util/aI;

    const-string v1, "UPLOAD_FAILED"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/util/aI;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/aH;->a:Lcom/dropbox/android/util/aH;

    .line 394
    new-instance v0, Lcom/dropbox/android/util/aJ;

    const-string v1, "SHMODEL"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/util/aJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/aH;->b:Lcom/dropbox/android/util/aH;

    .line 491
    new-instance v0, Lcom/dropbox/android/util/aK;

    const-string v1, "SHARED_FOLDER"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/util/aK;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/aH;->c:Lcom/dropbox/android/util/aH;

    .line 526
    new-instance v0, Lcom/dropbox/android/util/aL;

    const-string v1, "CLIENT_INSTALL_REMINDER"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/android/util/aL;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/aH;->d:Lcom/dropbox/android/util/aH;

    .line 361
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/dropbox/android/util/aH;

    sget-object v1, Lcom/dropbox/android/util/aH;->a:Lcom/dropbox/android/util/aH;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/util/aH;->b:Lcom/dropbox/android/util/aH;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/util/aH;->c:Lcom/dropbox/android/util/aH;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/util/aH;->d:Lcom/dropbox/android/util/aH;

    aput-object v1, v0, v5

    sput-object v0, Lcom/dropbox/android/util/aH;->e:[Lcom/dropbox/android/util/aH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 361
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/dropbox/android/util/aG;)V
    .locals 0

    .prologue
    .line 361
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/util/aH;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/util/aH;
    .locals 1

    .prologue
    .line 361
    const-class v0, Lcom/dropbox/android/util/aH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/aH;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/util/aH;
    .locals 1

    .prologue
    .line 361
    sget-object v0, Lcom/dropbox/android/util/aH;->e:[Lcom/dropbox/android/util/aH;

    invoke-virtual {v0}, [Lcom/dropbox/android/util/aH;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/util/aH;

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/app/Notification;
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 557
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Plural."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/util/aH;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
