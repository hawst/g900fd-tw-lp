.class public final Lcom/dropbox/android/util/m;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Z

.field private final b:Z


# direct methods
.method public constructor <init>(ZZ)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-boolean p1, p0, Lcom/dropbox/android/util/m;->a:Z

    .line 26
    iput-boolean p2, p0, Lcom/dropbox/android/util/m;->b:Z

    .line 27
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/dropbox/android/util/m;->a:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/dropbox/android/util/m;->b:Z

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 54
    instance-of v1, p1, Lcom/dropbox/android/util/m;

    if-eqz v1, :cond_0

    .line 55
    check-cast p1, Lcom/dropbox/android/util/m;

    .line 56
    iget-boolean v1, p0, Lcom/dropbox/android/util/m;->a:Z

    iget-boolean v2, p1, Lcom/dropbox/android/util/m;->a:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/dropbox/android/util/m;->b:Z

    iget-boolean v2, p1, Lcom/dropbox/android/util/m;->b:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 58
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 49
    iget-boolean v0, p0, Lcom/dropbox/android/util/m;->a:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    :goto_0
    iget-boolean v2, p0, Lcom/dropbox/android/util/m;->b:Z

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    or-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
