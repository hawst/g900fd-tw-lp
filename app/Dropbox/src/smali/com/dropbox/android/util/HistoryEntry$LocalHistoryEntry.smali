.class public Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;
.super Lcom/dropbox/android/util/HistoryEntry;
.source "panda.py"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 355
    new-instance v0, Lcom/dropbox/android/util/ak;

    invoke-direct {v0}, Lcom/dropbox/android/util/ak;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 309
    sget-object v0, Lcom/dropbox/android/util/al;->d:Lcom/dropbox/android/util/al;

    invoke-direct {p0, v0}, Lcom/dropbox/android/util/HistoryEntry;-><init>(Lcom/dropbox/android/util/al;)V

    .line 310
    iput-object p1, p0, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;->c:Landroid/net/Uri;

    .line 311
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 351
    invoke-direct {p0, p1}, Lcom/dropbox/android/util/HistoryEntry;-><init>(Landroid/os/Parcel;)V

    .line 352
    const-class v0, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;->c:Landroid/net/Uri;

    .line 353
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;->c:Landroid/net/Uri;

    invoke-static {v0, p1}, Lcom/dropbox/android/provider/FileSystemProvider;->a(Landroid/net/Uri;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 320
    const v0, 0x7f0d004e

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 325
    const v0, 0x7f0d0052

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;->c:Landroid/net/Uri;

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 335
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;->c:Landroid/net/Uri;

    check-cast p1, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;

    iget-object v1, p1, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;->c:Landroid/net/Uri;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 346
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/HistoryEntry;->writeToParcel(Landroid/os/Parcel;I)V

    .line 347
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryEntry$LocalHistoryEntry;->c:Landroid/net/Uri;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 348
    return-void
.end method
