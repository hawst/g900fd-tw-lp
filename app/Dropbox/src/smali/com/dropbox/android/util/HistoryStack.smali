.class public Lcom/dropbox/android/util/HistoryStack;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dropbox/android/util/HistoryStack;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/dropbox/android/util/HistoryEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    new-instance v0, Lcom/dropbox/android/util/am;

    invoke-direct {v0}, Lcom/dropbox/android/util/am;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/HistoryStack;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/util/HistoryStack;->a:Ljava/util/Stack;

    .line 20
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/util/HistoryStack;->a:Ljava/util/Stack;

    .line 91
    const-class v0, Lcom/dropbox/android/util/HistoryStack;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v2

    .line 93
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 94
    check-cast v0, Lcom/dropbox/android/util/HistoryEntry;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/util/HistoryStack;->a(Lcom/dropbox/android/util/HistoryEntry;)Z

    .line 93
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 96
    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/dropbox/android/util/am;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/dropbox/android/util/HistoryStack;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public final a(I)Lcom/dropbox/android/util/HistoryEntry;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryStack;->a:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/HistoryEntry;

    return-object v0
.end method

.method public final a(Landroid/widget/ListView;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-virtual {p0}, Lcom/dropbox/android/util/HistoryStack;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 36
    invoke-virtual {p1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    .line 37
    invoke-virtual {p1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 38
    if-nez v2, :cond_1

    .line 40
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/util/HistoryStack;->b()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v2

    .line 41
    iput v1, v2, Lcom/dropbox/android/util/HistoryEntry;->a:I

    .line 42
    iput v0, v2, Lcom/dropbox/android/util/HistoryEntry;->b:I

    .line 44
    :cond_0
    return-void

    .line 38
    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v0

    goto :goto_0
.end method

.method public final a(Lcom/dropbox/android/widget/DropboxItemListView;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-virtual {p0}, Lcom/dropbox/android/util/HistoryStack;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 24
    invoke-virtual {p1}, Lcom/dropbox/android/widget/DropboxItemListView;->c()I

    move-result v1

    .line 25
    invoke-virtual {p1, v0}, Lcom/dropbox/android/widget/DropboxItemListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 26
    if-nez v2, :cond_1

    .line 28
    :goto_0
    invoke-virtual {p0}, Lcom/dropbox/android/util/HistoryStack;->b()Lcom/dropbox/android/util/HistoryEntry;

    move-result-object v2

    .line 29
    iput v1, v2, Lcom/dropbox/android/util/HistoryEntry;->a:I

    .line 30
    iput v0, v2, Lcom/dropbox/android/util/HistoryEntry;->b:I

    .line 32
    :cond_0
    return-void

    .line 26
    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v0

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryStack;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/dropbox/android/util/HistoryEntry;)Z
    .locals 1

    .prologue
    .line 70
    invoke-static {p1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 71
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryStack;->a:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Lcom/dropbox/android/util/HistoryEntry;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryStack;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/HistoryEntry;

    return-object v0
.end method

.method public final b(Lcom/dropbox/android/util/HistoryEntry;)Lcom/dropbox/android/util/HistoryEntry;
    .locals 1

    .prologue
    .line 75
    invoke-static {p1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 76
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryStack;->a:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/HistoryEntry;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryStack;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    return v0
.end method

.method public final d()Lcom/dropbox/android/util/HistoryEntry;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryStack;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/HistoryEntry;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryStack;->a:Ljava/util/Stack;

    iget-object v1, p0, Lcom/dropbox/android/util/HistoryStack;->a:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v1

    new-array v1, v1, [Lcom/dropbox/android/util/HistoryEntry;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/util/HistoryEntry;

    .line 87
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 88
    return-void
.end method
