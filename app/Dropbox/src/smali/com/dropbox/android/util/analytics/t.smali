.class final Lcom/dropbox/android/util/analytics/t;
.super Ldbxyzptlk/db231222/x/c;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/x/c",
        "<",
        "Ldbxyzptlk/db231222/t/e;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 405
    invoke-direct {p0}, Ldbxyzptlk/db231222/x/c;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/x/k;)Ldbxyzptlk/db231222/t/e;
    .locals 5

    .prologue
    .line 410
    invoke-virtual {p1}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v0

    .line 411
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 412
    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/g;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 413
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 414
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/x/k;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v0

    .line 415
    invoke-static {}, Ldbxyzptlk/db231222/t/a;->o()Ldbxyzptlk/db231222/t/c;

    move-result-object v4

    invoke-virtual {v4, v1}, Ldbxyzptlk/db231222/t/c;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/t/c;

    move-result-object v1

    const-string v4, "experiment_version"

    invoke-virtual {v0, v4}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v4

    invoke-virtual {v4}, Ldbxyzptlk/db231222/x/k;->f()I

    move-result v4

    invoke-virtual {v1, v4}, Ldbxyzptlk/db231222/t/c;->a(I)Ldbxyzptlk/db231222/t/c;

    move-result-object v1

    const-string v4, "version"

    invoke-virtual {v0, v4}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v4

    invoke-virtual {v4}, Ldbxyzptlk/db231222/x/k;->f()I

    move-result v4

    invoke-virtual {v1, v4}, Ldbxyzptlk/db231222/t/c;->b(I)Ldbxyzptlk/db231222/t/c;

    move-result-object v1

    const-string v4, "variant"

    invoke-virtual {v0, v4}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/t/c;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/t/c;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/c;->b()Ldbxyzptlk/db231222/t/a;

    move-result-object v0

    .line 420
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 422
    :cond_0
    invoke-static {}, Ldbxyzptlk/db231222/t/e;->i()Ldbxyzptlk/db231222/t/g;

    move-result-object v0

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/t/g;->a(Ljava/lang/Iterable;)Ldbxyzptlk/db231222/t/g;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/g;->b()Ldbxyzptlk/db231222/t/e;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 405
    invoke-virtual {p0, p1}, Lcom/dropbox/android/util/analytics/t;->a(Ldbxyzptlk/db231222/x/k;)Ldbxyzptlk/db231222/t/e;

    move-result-object v0

    return-object v0
.end method
