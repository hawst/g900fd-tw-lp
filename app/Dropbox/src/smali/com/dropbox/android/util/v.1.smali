.class public final Lcom/dropbox/android/util/v;
.super Ljava/lang/Object;
.source "panda.py"


# direct methods
.method public static a(Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 216
    new-instance v0, Lcom/dropbox/android/util/x;

    invoke-static {}, Lcom/dropbox/android/util/v;->a()[B

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/util/x;-><init>(Ljava/io/InputStream;[B)V

    return-object v0
.end method

.method public static a(Ljava/io/File;)Ljava/io/OutputStream;
    .locals 5

    .prologue
    .line 190
    const-wide/16 v0, 0x0

    .line 191
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 192
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 194
    :cond_0
    new-instance v2, Lcom/dropbox/android/util/y;

    new-instance v3, Ljava/io/FileOutputStream;

    const/4 v4, 0x1

    invoke-direct {v3, p0, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    invoke-static {}, Lcom/dropbox/android/util/v;->a()[B

    move-result-object v4

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/dropbox/android/util/y;-><init>(Ljava/io/OutputStream;[BJ)V

    return-object v2
.end method

.method public static a(Ljava/io/OutputStream;)Ljava/io/OutputStream;
    .locals 4

    .prologue
    .line 202
    new-instance v0, Lcom/dropbox/android/util/y;

    invoke-static {}, Lcom/dropbox/android/util/v;->a()[B

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/dropbox/android/util/y;-><init>(Ljava/io/OutputStream;[BJ)V

    return-object v0
.end method

.method private static a()[B
    .locals 10

    .prologue
    const/16 v9, 0x1a

    const/16 v2, 0xf

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 226
    const-string v0, "vlwr70ohoptcb6x9n71kd0ghf15ijzk68u6zew0amx3p3yld4odold5v8w8d06e28mleivtzq9nxspdzm004jnsdrb4r4noqudhbmmdm7lrxj1isl2rb1pqjg0i155zz8mbxsm0tcgd5hstwg028i4qatlvu4eonj8kf3gj29t39r5qeiso54e251h1r23ss25q6k2p90j3d4jfh5nrrfu80k3nkok7702vyzhb321xjtfanahyq2xnfkfn1h29ex0amds3zljmiii1atl6megqwshdumm95iqprqy2wqn63ir04hsjy71enazu5sunk26yepl600kftr3kt9zlbtbk3xbj090dme04fqwyzvdq19l94hk4sv7dgl5vd410tbbqhtk222731eaamkgs76os4eubd2su0bblx9p4mkz8s67w40r6wpt78vrm538fcab717l6fevhy32eftog1ykssm2sh0nmt3qr6x5vr1q96fkmg3fajf0lc0f5r3da1bb5mkon7a8a753jgij61vgpc33jed0i0jpeo2vgo3ter52zr4a134ppj8kh0hftxz8qeoaka2lr18i7btkhetnrpgaaxzf2aey9l1jwshtn3pm6l4csti60v71p5opkiafv2b3d0wvi1ikjer6llz7f8s10sk870ugunwgil0xdsth5cunt01xi4ygdwiooello96j46kdeuuvbzcrvc4fkyiqaumluwvd25977b1lxykubl3rfjs1744effxtun139li26uwz9j2ulaidymmsh8ko6pc1wapgpescm1vumbixkyouv81sgy0djdrtj85r0avdkza2cowmgcqin02t3qqahaupxihuqd3a3kb91v2fr1qmxskvt9ghgpbje9tcxat23htblookisjtr43vjs7tenfjapnlh23whwms5jnwgfy49csvpprxhxro5n0cwq19rcyhsnxmyaf9x0kpx3yju0fo9ufgrbjmbubghbzhhunea1lkrr3cyyq4ku"

    .line 228
    const/4 v0, 0x3

    new-array v3, v0, [[I

    new-array v0, v2, [I

    fill-array-data v0, :array_0

    aput-object v0, v3, v1

    new-array v0, v2, [I

    fill-array-data v0, :array_1

    aput-object v0, v3, v8

    const/4 v0, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_2

    aput-object v2, v3, v0

    .line 233
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 234
    aget-object v0, v3, v1

    const/4 v2, 0x5

    aget v5, v0, v2

    .line 235
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    move v2, v1

    .line 236
    :goto_0
    const/16 v0, 0x24

    if-ge v2, v0, :cond_2

    .line 237
    if-ge v2, v9, :cond_0

    add-int/lit8 v0, v2, 0x61

    int-to-char v0, v0

    :goto_1
    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v7

    sub-int v0, v2, v5

    add-int/lit8 v0, v0, 0x24

    rem-int/lit8 v0, v0, 0x24

    if-ge v0, v9, :cond_1

    sub-int v0, v2, v5

    add-int/lit8 v0, v0, 0x24

    rem-int/lit8 v0, v0, 0x24

    add-int/lit8 v0, v0, 0x61

    int-to-char v0, v0

    :goto_2
    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-interface {v6, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 237
    :cond_0
    add-int/lit8 v0, v2, -0x1a

    add-int/lit8 v0, v0, 0x30

    int-to-char v0, v0

    goto :goto_1

    :cond_1
    sub-int v0, v2, v5

    add-int/lit8 v0, v0, 0x24

    rem-int/lit8 v0, v0, 0x24

    add-int/lit8 v0, v0, -0x1a

    add-int/lit8 v0, v0, 0x30

    int-to-char v0, v0

    goto :goto_2

    :cond_2
    move v0, v1

    .line 240
    :goto_3
    aget-object v1, v3, v8

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 241
    const-string v1, "vlwr70ohoptcb6x9n71kd0ghf15ijzk68u6zew0amx3p3yld4odold5v8w8d06e28mleivtzq9nxspdzm004jnsdrb4r4noqudhbmmdm7lrxj1isl2rb1pqjg0i155zz8mbxsm0tcgd5hstwg028i4qatlvu4eonj8kf3gj29t39r5qeiso54e251h1r23ss25q6k2p90j3d4jfh5nrrfu80k3nkok7702vyzhb321xjtfanahyq2xnfkfn1h29ex0amds3zljmiii1atl6megqwshdumm95iqprqy2wqn63ir04hsjy71enazu5sunk26yepl600kftr3kt9zlbtbk3xbj090dme04fqwyzvdq19l94hk4sv7dgl5vd410tbbqhtk222731eaamkgs76os4eubd2su0bblx9p4mkz8s67w40r6wpt78vrm538fcab717l6fevhy32eftog1ykssm2sh0nmt3qr6x5vr1q96fkmg3fajf0lc0f5r3da1bb5mkon7a8a753jgij61vgpc33jed0i0jpeo2vgo3ter52zr4a134ppj8kh0hftxz8qeoaka2lr18i7btkhetnrpgaaxzf2aey9l1jwshtn3pm6l4csti60v71p5opkiafv2b3d0wvi1ikjer6llz7f8s10sk870ugunwgil0xdsth5cunt01xi4ygdwiooello96j46kdeuuvbzcrvc4fkyiqaumluwvd25977b1lxykubl3rfjs1744effxtun139li26uwz9j2ulaidymmsh8ko6pc1wapgpescm1vumbixkyouv81sgy0djdrtj85r0avdkza2cowmgcqin02t3qqahaupxihuqd3a3kb91v2fr1qmxskvt9ghgpbje9tcxat23htblookisjtr43vjs7tenfjapnlh23whwms5jnwgfy49csvpprxhxro5n0cwq19rcyhsnxmyaf9x0kpx3yju0fo9ufgrbjmbubghbzhhunea1lkrr3cyyq4ku"

    aget-object v2, v3, v8

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 242
    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 240
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 244
    :cond_3
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->reverse()Ljava/lang/StringBuffer;

    .line 245
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0

    .line 228
    :array_0
    .array-data 4
        0x88
        0x237
        0x15b
        0x54
        0x94
        0x15
        0x1a
        0x207
        0x2cb
        0x36f
        0x399
        0xe3
        0x3a7
        0x16
        0x234
    .end array-data

    :array_1
    .array-data 4
        0x186
        0x386
        0x1e
        0x82
        0x179
        0xe7
        0x153
        0xdc
        0x13a
        0x76
        0xda
        0x282
        0x1fd
        0x21b
        0x288
    .end array-data

    :array_2
    .array-data 4
        0x182
        0x1
        0x18c
        0x173
        0x3c1
        0x1d1
        0x2aa
        0x3d9
        0x242
        0x109
        0x167
        0x11d
        0x30d
        0x374
        0x113
    .end array-data
.end method

.method public static b(Ljava/io/File;)Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 209
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-static {v0}, Lcom/dropbox/android/util/v;->a(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method
