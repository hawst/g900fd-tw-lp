.class public final enum Lcom/dropbox/android/util/al;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/util/al;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/util/al;

.field public static final enum b:Lcom/dropbox/android/util/al;

.field public static final enum c:Lcom/dropbox/android/util/al;

.field public static final enum d:Lcom/dropbox/android/util/al;

.field private static final synthetic e:[Lcom/dropbox/android/util/al;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    new-instance v0, Lcom/dropbox/android/util/al;

    const-string v1, "DROPBOX_DIRECTORY"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/util/al;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/al;->a:Lcom/dropbox/android/util/al;

    new-instance v0, Lcom/dropbox/android/util/al;

    const-string v1, "DROPBOX_SEARCH"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/util/al;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/al;->b:Lcom/dropbox/android/util/al;

    new-instance v0, Lcom/dropbox/android/util/al;

    const-string v1, "DROPBOX_FAVORITES"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/util/al;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/al;->c:Lcom/dropbox/android/util/al;

    new-instance v0, Lcom/dropbox/android/util/al;

    const-string v1, "LOCAL_DIRECTORY"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/android/util/al;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/al;->d:Lcom/dropbox/android/util/al;

    .line 19
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/dropbox/android/util/al;

    sget-object v1, Lcom/dropbox/android/util/al;->a:Lcom/dropbox/android/util/al;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/util/al;->b:Lcom/dropbox/android/util/al;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/util/al;->c:Lcom/dropbox/android/util/al;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/util/al;->d:Lcom/dropbox/android/util/al;

    aput-object v1, v0, v5

    sput-object v0, Lcom/dropbox/android/util/al;->e:[Lcom/dropbox/android/util/al;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/util/al;
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/dropbox/android/util/al;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/al;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/util/al;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/dropbox/android/util/al;->e:[Lcom/dropbox/android/util/al;

    invoke-virtual {v0}, [Lcom/dropbox/android/util/al;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/util/al;

    return-object v0
.end method
