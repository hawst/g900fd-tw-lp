.class public Lcom/dropbox/android/util/ParcelablePair;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dropbox/android/util/ParcelablePair;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Landroid/os/Parcelable;

.field private final b:Landroid/os/Parcelable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    new-instance v0, Lcom/dropbox/android/util/aX;

    invoke-direct {v0}, Lcom/dropbox/android/util/aX;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/ParcelablePair;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object v0, p0, Lcom/dropbox/android/util/ParcelablePair;->a:Landroid/os/Parcelable;

    iput-object v0, p0, Lcom/dropbox/android/util/ParcelablePair;->b:Landroid/os/Parcelable;

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/dropbox/android/util/ParcelablePair;->a:Landroid/os/Parcelable;

    .line 32
    iput-object p2, p0, Lcom/dropbox/android/util/ParcelablePair;->b:Landroid/os/Parcelable;

    .line 33
    return-void
.end method


# virtual methods
.method public final a()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/dropbox/android/util/ParcelablePair;->a:Landroid/os/Parcelable;

    return-object v0
.end method

.method public final b()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/dropbox/android/util/ParcelablePair;->b:Landroid/os/Parcelable;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 57
    instance-of v1, p1, Lcom/dropbox/android/util/ParcelablePair;

    if-nez v1, :cond_1

    .line 61
    :cond_0
    :goto_0
    return v0

    .line 60
    :cond_1
    check-cast p1, Lcom/dropbox/android/util/ParcelablePair;

    .line 61
    iget-object v1, p1, Lcom/dropbox/android/util/ParcelablePair;->a:Landroid/os/Parcelable;

    iget-object v2, p0, Lcom/dropbox/android/util/ParcelablePair;->a:Landroid/os/Parcelable;

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/E/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/dropbox/android/util/ParcelablePair;->b:Landroid/os/Parcelable;

    iget-object v2, p0, Lcom/dropbox/android/util/ParcelablePair;->b:Landroid/os/Parcelable;

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/E/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 71
    iget-object v0, p0, Lcom/dropbox/android/util/ParcelablePair;->a:Landroid/os/Parcelable;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/dropbox/android/util/ParcelablePair;->b:Landroid/os/Parcelable;

    if-nez v2, :cond_1

    :goto_1
    xor-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/util/ParcelablePair;->a:Landroid/os/Parcelable;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/dropbox/android/util/ParcelablePair;->b:Landroid/os/Parcelable;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 82
    iget-object v0, p0, Lcom/dropbox/android/util/ParcelablePair;->a:Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 83
    iget-object v0, p0, Lcom/dropbox/android/util/ParcelablePair;->b:Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 84
    return-void
.end method
