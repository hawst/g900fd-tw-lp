.class public final Lcom/dropbox/android/util/s;
.super Ljava/lang/Object;
.source "panda.py"


# direct methods
.method public static declared-synchronized a(Lcom/dropbox/android/notifications/d;Ldbxyzptlk/db231222/n/P;Lcom/dropbox/android/taskqueue/U;)V
    .locals 6

    .prologue
    .line 15
    const-class v1, Lcom/dropbox/android/util/s;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p1}, Ldbxyzptlk/db231222/n/P;->s()Ldbxyzptlk/db231222/n/n;

    move-result-object v0

    sget-object v2, Ldbxyzptlk/db231222/n/n;->b:Ldbxyzptlk/db231222/n/n;

    if-ne v0, v2, :cond_0

    .line 16
    const-class v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    invoke-virtual {p2, v0}, Lcom/dropbox/android/taskqueue/U;->b(Ljava/lang/Class;)I

    move-result v0

    .line 17
    if-nez v0, :cond_1

    invoke-virtual {p1}, Ldbxyzptlk/db231222/n/P;->q()Z

    move-result v0

    if-nez v0, :cond_1

    .line 18
    invoke-virtual {p1}, Ldbxyzptlk/db231222/n/P;->t()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    invoke-virtual {p1}, Ldbxyzptlk/db231222/n/P;->w()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 19
    sget-object v0, Lcom/dropbox/android/util/aM;->d:Lcom/dropbox/android/util/aM;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;Landroid/os/Bundle;)Z

    .line 20
    sget-object v0, Ldbxyzptlk/db231222/n/n;->a:Ldbxyzptlk/db231222/n/n;

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/n/P;->a(Ldbxyzptlk/db231222/n/n;)V

    .line 34
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ldbxyzptlk/db231222/n/P;->s()Ldbxyzptlk/db231222/n/n;

    move-result-object v0

    sget-object v2, Ldbxyzptlk/db231222/n/n;->c:Ldbxyzptlk/db231222/n/n;

    if-ne v0, v2, :cond_1

    .line 35
    invoke-virtual {p1}, Ldbxyzptlk/db231222/n/P;->t()J

    move-result-wide v2

    const-wide/16 v4, 0xa

    cmp-long v0, v2, v4

    if-ltz v0, :cond_1

    .line 36
    sget-object v0, Lcom/dropbox/android/util/aM;->e:Lcom/dropbox/android/util/aM;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Lcom/dropbox/android/notifications/d;->a(Lcom/dropbox/android/util/aM;Landroid/os/Bundle;)Z

    .line 37
    sget-object v0, Ldbxyzptlk/db231222/n/n;->a:Ldbxyzptlk/db231222/n/n;

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/n/P;->a(Ldbxyzptlk/db231222/n/n;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    :cond_1
    monitor-exit v1

    return-void

    .line 26
    :cond_2
    :try_start_1
    sget-object v0, Ldbxyzptlk/db231222/n/n;->c:Ldbxyzptlk/db231222/n/n;

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/n/P;->a(Ldbxyzptlk/db231222/n/n;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 15
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
