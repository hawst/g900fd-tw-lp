.class public abstract Lcom/dropbox/android/util/HistoryEntry;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ParcelCreator"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field private final c:Lcom/dropbox/android/util/al;


# direct methods
.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/util/HistoryEntry;->a:I

    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/al;->valueOf(Ljava/lang/String;)Lcom/dropbox/android/util/al;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/util/HistoryEntry;->c:Lcom/dropbox/android/util/al;

    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/util/HistoryEntry;->a:I

    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/util/HistoryEntry;->b:I

    .line 86
    return-void
.end method

.method protected constructor <init>(Lcom/dropbox/android/util/al;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/util/HistoryEntry;->a:I

    .line 28
    iput-object p1, p0, Lcom/dropbox/android/util/HistoryEntry;->c:Lcom/dropbox/android/util/al;

    .line 29
    return-void
.end method


# virtual methods
.method public final a()Lcom/dropbox/android/util/al;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryEntry;->c:Lcom/dropbox/android/util/al;

    return-object v0
.end method

.method public abstract a(Landroid/content/res/Resources;)Ljava/lang/String;
.end method

.method public abstract b()I
.end method

.method public final b(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0, p1}, Lcom/dropbox/android/util/HistoryEntry;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract c()I
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method public e()Lcom/dropbox/android/util/HistoryEntry;
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract equals(Ljava/lang/Object;)Z
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryEntry;->c:Lcom/dropbox/android/util/al;

    invoke-virtual {v0}, Lcom/dropbox/android/util/al;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78
    iget v0, p0, Lcom/dropbox/android/util/HistoryEntry;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 79
    iget v0, p0, Lcom/dropbox/android/util/HistoryEntry;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 80
    return-void
.end method
