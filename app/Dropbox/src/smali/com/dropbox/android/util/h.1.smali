.class public Lcom/dropbox/android/util/h;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/dropbox/android/util/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/util/h;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;Ldbxyzptlk/db231222/n/w;)Ldbxyzptlk/db231222/n/a;
    .locals 3

    .prologue
    .line 191
    new-instance v0, Ldbxyzptlk/db231222/n/a;

    new-instance v1, Ldbxyzptlk/db231222/n/v;

    const-string v2, "prefs.db"

    invoke-direct {v1, p0, v2, p1}, Ldbxyzptlk/db231222/n/v;-><init>(Landroid/content/Context;Ljava/lang/String;Ldbxyzptlk/db231222/n/w;)V

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/n/a;-><init>(Ldbxyzptlk/db231222/n/v;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 37
    invoke-static {p0}, Lcom/dropbox/android/util/h;->e(Landroid/content/Context;)V

    .line 39
    invoke-static {p0}, Lcom/dropbox/android/util/h;->b(Landroid/content/Context;)V

    .line 40
    invoke-static {p0}, Lcom/dropbox/android/util/ba;->a(Landroid/content/Context;)V

    .line 41
    invoke-static {p0}, Lcom/dropbox/android/util/h;->c(Landroid/content/Context;)V

    .line 42
    invoke-static {p0}, Lcom/dropbox/android/util/h;->d(Landroid/content/Context;)V

    .line 43
    invoke-static {p0}, Lcom/dropbox/android/util/h;->f(Landroid/content/Context;)V

    .line 44
    return-void
.end method

.method private static b(Landroid/content/Context;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v9, 0x1

    .line 55
    sget-object v1, Ldbxyzptlk/db231222/n/w;->a:Ldbxyzptlk/db231222/n/w;

    invoke-static {p0, v1}, Lcom/dropbox/android/util/h;->a(Landroid/content/Context;Ldbxyzptlk/db231222/n/w;)Ldbxyzptlk/db231222/n/a;

    move-result-object v1

    .line 57
    :try_start_0
    const-string v2, "ACCESS_KEY"

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/n/a;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "ACCESS_SECRET"

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/n/a;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 58
    sget-object v2, Lcom/dropbox/android/util/h;->a:Ljava/lang/String;

    const-string v3, "Migrating Credentials"

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v2, "ACCESS_KEY"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ldbxyzptlk/db231222/n/a;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 60
    const-string v3, "ACCESS_SECRET"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Ldbxyzptlk/db231222/n/a;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 64
    const-string v4, "UID"

    const-wide/16 v5, 0x0

    invoke-virtual {v1, v4, v5, v6}, Ldbxyzptlk/db231222/n/a;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 65
    const-string v6, "EMAIL"

    const v7, 0x7f0d0012

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Ldbxyzptlk/db231222/n/a;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 67
    new-instance v7, Landroid/accounts/Account;

    const-string v8, "com.dropbox.android.account"

    invoke-direct {v7, v6, v8}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 69
    const-string v8, "KEY"

    invoke-virtual {v6, v8, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v2, "SECRET"

    invoke-virtual {v6, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v2, "USER_ID"

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 74
    const/4 v3, 0x0

    invoke-virtual {v2, v7, v3, v6}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 76
    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/a;->c()Ldbxyzptlk/db231222/n/h;

    move-result-object v2

    const-string v3, "ACCESS_KEY"

    invoke-virtual {v2, v3}, Ldbxyzptlk/db231222/n/h;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "ACCESS_SECRET"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 81
    :cond_0
    const-string v2, "USE_LOCK_CODE"

    .line 82
    const-string v2, "LOCK_CODE"

    .line 83
    const-string v2, "LOCK_CODE_ERASE"

    .line 84
    const-string v2, "LOCK_CODE_LOCKED_UNTIL"

    .line 85
    const-string v2, "USE_LOCK_CODE"

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/n/a;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 86
    const-string v2, "USE_LOCK_CODE"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ldbxyzptlk/db231222/n/a;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 88
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 89
    const-string v3, "com.dropbox.android.account"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    .line 90
    array-length v4, v3

    if-lt v4, v9, :cond_2

    .line 91
    array-length v0, v3

    if-eq v0, v9, :cond_1

    .line 92
    sget-object v0, Lcom/dropbox/android/util/h;->a:Ljava/lang/String;

    const-string v4, "More than one Dropbox account found in AccountManager during migration"

    invoke-static {v0, v4}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :cond_1
    const/4 v0, 0x0

    aget-object v0, v3, v0

    .line 96
    :cond_2
    if-eqz v0, :cond_3

    .line 97
    const-string v3, "USE_LOCK_CODE"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v3, v4}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string v3, "LOCK_CODE"

    const-string v4, "LOCK_CODE"

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Ldbxyzptlk/db231222/n/a;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v3, v4}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const-string v3, "LOCK_CODE_ERASE"

    const-string v4, "LOCK_CODE_ERASE"

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Ldbxyzptlk/db231222/n/a;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v3, v4}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string v3, "LOCK_CODE_LOCKED_UNTIL"

    const-string v4, "LOCK_CODE_LOCKED_UNTIL"

    const-wide/16 v5, 0x0

    invoke-virtual {v1, v4, v5, v6}, Ldbxyzptlk/db231222/n/a;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v3, v4}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :cond_3
    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/a;->c()Ldbxyzptlk/db231222/n/h;

    move-result-object v0

    const-string v2, "USE_LOCK_CODE"

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/n/h;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "LOCK_CODE"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "LOCK_CODE_ERASE"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "LOCK_CODE_LOCKED_UNTIL"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    :cond_4
    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/a;->b()V

    .line 111
    return-void

    .line 109
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/a;->b()V

    throw v0
.end method

.method private static c(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 122
    sget-object v1, Ldbxyzptlk/db231222/n/w;->b:Ldbxyzptlk/db231222/n/w;

    invoke-static {p0, v1}, Lcom/dropbox/android/util/h;->a(Landroid/content/Context;Ldbxyzptlk/db231222/n/w;)Ldbxyzptlk/db231222/n/a;

    move-result-object v1

    .line 124
    :try_start_0
    const-string v2, "PHOTO_SELECT_POPUP_DISMISSED"

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/n/a;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 125
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 126
    invoke-static {v2}, Lcom/dropbox/android/util/au;->a(Landroid/content/res/Resources;)Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "en"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    .line 128
    if-eqz v2, :cond_0

    .line 129
    const-string v0, "PHOTO_SELECT_POPUP_DISMISSED"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/db231222/n/a;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 134
    :cond_0
    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/a;->c()Ldbxyzptlk/db231222/n/h;

    move-result-object v2

    .line 135
    const-string v3, "PHOTO_SELECT_POPUP_DISMISSED"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 136
    const-string v3, "PHOTO_SELECT_POPUP_DISMISSED_NEW"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 137
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    :cond_1
    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/a;->b()V

    .line 142
    return-void

    .line 140
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/a;->b()V

    throw v0
.end method

.method private static d(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 149
    sget-object v0, Ldbxyzptlk/db231222/n/w;->a:Ldbxyzptlk/db231222/n/w;

    invoke-static {p0, v0}, Lcom/dropbox/android/util/h;->a(Landroid/content/Context;Ldbxyzptlk/db231222/n/w;)Ldbxyzptlk/db231222/n/a;

    move-result-object v1

    .line 151
    :try_start_0
    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/a;->c()Ldbxyzptlk/db231222/n/h;

    move-result-object v0

    .line 154
    const-string v2, "CAMERA_UPLOADS_ALBUM_CURSOR"

    .line 156
    const-string v2, "CAMERA_UPLOADS_ALBUM_CURSOR"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 158
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/a;->b()V

    .line 162
    return-void

    .line 160
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/a;->b()V

    throw v0
.end method

.method private static e(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 165
    const-string v0, "DropboxAccountPrefs"

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 167
    sget-object v1, Ldbxyzptlk/db231222/n/w;->a:Ldbxyzptlk/db231222/n/w;

    invoke-static {p0, v1}, Lcom/dropbox/android/util/h;->a(Landroid/content/Context;Ldbxyzptlk/db231222/n/w;)Ldbxyzptlk/db231222/n/a;

    move-result-object v1

    .line 169
    :try_start_0
    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/n/a;->a(Landroid/content/SharedPreferences;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/a;->b()V

    .line 174
    const-string v0, "DropboxPersistentPrefs"

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 176
    sget-object v1, Ldbxyzptlk/db231222/n/w;->b:Ldbxyzptlk/db231222/n/w;

    invoke-static {p0, v1}, Lcom/dropbox/android/util/h;->a(Landroid/content/Context;Ldbxyzptlk/db231222/n/w;)Ldbxyzptlk/db231222/n/a;

    move-result-object v1

    .line 178
    :try_start_1
    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/n/a;->a(Landroid/content/SharedPreferences;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 180
    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/a;->b()V

    .line 182
    return-void

    .line 171
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/a;->b()V

    throw v0

    .line 180
    :catchall_1
    move-exception v0

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/a;->b()V

    throw v0
.end method

.method private static f(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 185
    const-string v0, "bromo"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 186
    invoke-static {v0}, Ldbxyzptlk/db231222/X/b;->c(Ljava/io/File;)Z

    .line 187
    return-void
.end method
