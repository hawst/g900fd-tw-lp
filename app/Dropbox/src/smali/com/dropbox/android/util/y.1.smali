.class public final Lcom/dropbox/android/util/y;
.super Ljava/io/OutputStream;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/android/util/w;

.field private final b:Ljava/io/OutputStream;

.field private final c:[B


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;[BJ)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 65
    const/16 v0, 0x2000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/dropbox/android/util/y;->c:[B

    .line 68
    iput-object p1, p0, Lcom/dropbox/android/util/y;->b:Ljava/io/OutputStream;

    .line 69
    new-instance v0, Lcom/dropbox/android/util/w;

    invoke-direct {v0, p2}, Lcom/dropbox/android/util/w;-><init>([B)V

    iput-object v0, p0, Lcom/dropbox/android/util/y;->a:Lcom/dropbox/android/util/w;

    .line 70
    iget-object v0, p0, Lcom/dropbox/android/util/y;->a:Lcom/dropbox/android/util/w;

    invoke-virtual {v0, p3, p4}, Lcom/dropbox/android/util/w;->a(J)V

    .line 71
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/dropbox/android/util/y;->b:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 88
    return-void
.end method

.method public final flush()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/dropbox/android/util/y;->b:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 93
    return-void
.end method

.method public final write(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 97
    new-array v0, v3, [B

    .line 98
    int-to-byte v1, p1

    aput-byte v1, v0, v2

    .line 99
    invoke-virtual {p0, v0, v2, v3}, Lcom/dropbox/android/util/y;->write([BII)V

    .line 100
    return-void
.end method

.method public final write([BII)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 76
    move v2, p2

    :goto_0
    if-lez p3, :cond_0

    .line 77
    iget-object v0, p0, Lcom/dropbox/android/util/y;->c:[B

    array-length v0, v0

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 78
    iget-object v0, p0, Lcom/dropbox/android/util/y;->a:Lcom/dropbox/android/util/w;

    iget-object v3, p0, Lcom/dropbox/android/util/y;->c:[B

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/android/util/w;->a([BI[BII)V

    .line 79
    iget-object v0, p0, Lcom/dropbox/android/util/y;->b:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/dropbox/android/util/y;->c:[B

    invoke-virtual {v0, v1, v4, v5}, Ljava/io/OutputStream;->write([BII)V

    .line 80
    sub-int/2addr p3, v5

    .line 81
    add-int/2addr v2, v5

    .line 82
    goto :goto_0

    .line 83
    :cond_0
    return-void
.end method
