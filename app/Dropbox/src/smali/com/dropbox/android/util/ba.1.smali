.class public final Lcom/dropbox/android/util/ba;
.super Ljava/lang/Object;
.source "panda.py"


# direct methods
.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 48
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/dropbox/android/util/by;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    const-class v0, Lcom/dropbox/android/service/DropboxNetworkReceiver;

    invoke-static {p0, v0}, Lcom/dropbox/android/util/ba;->a(Landroid/content/Context;Ljava/lang/Class;)V

    .line 75
    :cond_0
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/Class;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 78
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 79
    new-instance v1, Landroid/content/ComponentName;

    invoke-direct {v1, p0, p1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 80
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 81
    invoke-virtual {v0, v1, v4, v4}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 84
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Class;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 35
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/dropbox/android/util/by;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    if-eqz p2, :cond_1

    move v0, v1

    .line 37
    :goto_0
    new-instance v2, Landroid/content/ComponentName;

    invoke-direct {v2, p0, p1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 38
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v2, v0, v1}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 40
    :cond_0
    return-void

    .line 36
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method
