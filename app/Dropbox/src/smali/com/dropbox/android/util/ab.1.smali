.class public final Lcom/dropbox/android/util/ab;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field static final synthetic a:Z

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 32
    const-class v0, Lcom/dropbox/android/util/ab;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/dropbox/android/util/ab;->a:Z

    .line 36
    const-class v0, Lcom/dropbox/android/util/ab;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/util/ab;->b:Ljava/lang/String;

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/ab;->c:Ljava/util/HashMap;

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/ab;->d:Ljava/util/HashMap;

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/ab;->e:Ljava/util/HashMap;

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/ab;->f:Ljava/util/HashMap;

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/ab;->g:Ljava/util/HashMap;

    .line 43
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/ab;->h:Ljava/util/Set;

    .line 47
    const-string v0, "htm"

    const-string v2, "text/html"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-string v0, "html"

    const-string v2, "text/html"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const-string v0, "asc"

    const-string v2, "text/plain"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const-string v0, "txt"

    const-string v2, "text/plain"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    const-string v0, "xsl"

    const-string v2, "text/xml"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const-string v0, "xml"

    const-string v2, "text/xml"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-string v0, "css"

    const-string v2, "text/css"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v0, "rtx"

    const-string v2, "text/richtext"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string v0, "rtf"

    const-string v2, "text/rtf"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v0, "sgm"

    const-string v2, "text/sgml"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-string v0, "sgml"

    const-string v2, "text/sgml"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const-string v0, "tsv"

    const-string v2, "text/tab-separated-values"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v0, "csv"

    const-string v2, "text/comma-separated-values"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v0, "vnt"

    const-string v2, "text/x-vnote"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v0, "doc"

    const-string v2, "application/msword"

    const-string v3, "page_white_word"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v0, "dot"

    const-string v2, "application/msword"

    const-string v3, "page_white_word"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v0, "docx"

    const-string v2, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    const-string v3, "page_white_word"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v0, "dotx"

    const-string v2, "application/vnd.openxmlformats-officedocument.wordprocessingml.template"

    const-string v3, "page_white_word"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v0, "docm"

    const-string v2, "application/vnd.ms-word.document.macroEnabled.12"

    const-string v3, "page_white_word"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v0, "dotm"

    const-string v2, "application/vnd.ms-word.template.macroEnabled.12"

    const-string v3, "page_white_word"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v0, "xls"

    const-string v2, "application/vnd.ms-excel"

    const-string v3, "page_white_excel"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const-string v0, "xlt"

    const-string v2, "application/vnd.ms-excel"

    const-string v3, "page_white_excel"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v0, "xla"

    const-string v2, "application/vnd.ms-excel"

    const-string v3, "page_white_excel"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v0, "xlsx"

    const-string v2, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    const-string v3, "page_white_excel"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v0, "xltx"

    const-string v2, "application/vnd.openxmlformats-officedocument.spreadsheetml.template"

    const-string v3, "page_white_excel"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v0, "xlsm"

    const-string v2, "application/vnd.ms-excel.sheet.macroEnabled.12"

    const-string v3, "page_white_excel"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v0, "xltm"

    const-string v2, "application/vnd.ms-excel.template.macroEnabled.12"

    const-string v3, "page_white_excel"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const-string v0, "xlam"

    const-string v2, "application/vnd.ms-excel.addin.macroEnabled.12"

    const-string v3, "page_white_excel"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const-string v0, "xlsb"

    const-string v2, "application/vnd.ms-excel.sheet.binary.macroEnabled.12"

    const-string v3, "page_white_excel"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v0, "ppt"

    const-string v2, "application/vnd.ms-powerpoint"

    const-string v3, "page_white_powerpoint"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const-string v0, "pot"

    const-string v2, "application/vnd.ms-powerpoint"

    const-string v3, "page_white_powerpoint"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string v0, "pps"

    const-string v2, "application/vnd.ms-powerpoint"

    const-string v3, "page_white_powerpoint"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v0, "ppa"

    const-string v2, "application/vnd.ms-powerpoint"

    const-string v3, "page_white_powerpoint"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v0, "pptx"

    const-string v2, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    const-string v3, "page_white_powerpoint"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v0, "potx"

    const-string v2, "application/vnd.openxmlformats-officedocument.presentationml.template"

    const-string v3, "page_white_powerpoint"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v0, "ppsx"

    const-string v2, "application/vnd.openxmlformats-officedocument.presentationml.slideshow"

    const-string v3, "page_white_powerpoint"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const-string v0, "ppam"

    const-string v2, "application/vnd.ms-powerpoint.addin.macroEnabled.12"

    const-string v3, "page_white_powerpoint"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const-string v0, "pptm"

    const-string v2, "application/vnd.ms-powerpoint.presentation.macroEnabled.12"

    const-string v3, "page_white_powerpoint"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v0, "potm"

    const-string v2, "application/vnd.ms-powerpoint.template.macroEnabled.12"

    const-string v3, "page_white_powerpoint"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string v0, "ppsm"

    const-string v2, "application/vnd.ms-powerpoint.slideshow.macroEnabled.12"

    const-string v3, "page_white_powerpoint"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const-string v0, "pdf"

    const-string v2, "application/pdf"

    const-string v3, "page_white_acrobat"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const-string v0, "ps"

    const-string v2, "application/postscript"

    const-string v3, "page_white_acrobat"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string v0, "ai"

    const-string v2, "application/postscript"

    const-string v3, "page_white_acrobat"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string v0, "eps"

    const-string v2, "application/postscript"

    const-string v3, "page_white_acrobat"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string v0, "xml"

    const-string v2, "application/xml"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3, v1}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 101
    const-string v0, "js"

    const-string v2, "application/javascript"

    const-string v3, "page_white_js"

    invoke-static {v0, v2, v3, v1}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 102
    const-string v0, "js"

    const-string v2, "application/x-javascript"

    const-string v3, "page_white_js"

    invoke-static {v0, v2, v3, v1}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 103
    const-string v0, "latex"

    const-string v2, "application/x-latex"

    const-string v3, "page_white_code"

    invoke-static {v0, v2, v3, v1}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 104
    const-string v0, "sh"

    const-string v2, "application/x-sh"

    const-string v3, "page_white_code"

    invoke-static {v0, v2, v3, v1}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 105
    const-string v0, "tex"

    const-string v2, "application/x-tex"

    const-string v3, "page_white_code"

    invoke-static {v0, v2, v3, v1}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 106
    const-string v0, "texi"

    const-string v2, "application/x-texinfo"

    const-string v3, "page_white_code"

    invoke-static {v0, v2, v3, v1}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 107
    const-string v0, "texinfo"

    const-string v2, "application/x-texinfo"

    const-string v3, "page_white_code"

    invoke-static {v0, v2, v3, v1}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 108
    const-string v0, "tcl"

    const-string v2, "application/x-tcl"

    const-string v3, "page_white_code"

    invoke-static {v0, v2, v3, v1}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 111
    const-string v0, "tar"

    const-string v1, "application/x-tar"

    const-string v2, "page_white_compressed"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string v0, "zip"

    const-string v1, "application/zip"

    const-string v2, "page_white_compressed"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const-string v0, "swf"

    const-string v1, "application/x-shockwave-flash"

    const-string v2, "page_white_flash"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v0, "bmp"

    const-string v1, "image/bmp"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v0, "gif"

    const-string v1, "image/gif"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v0, "jpg"

    const-string v1, "image/jpeg"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v0, "jpe"

    const-string v1, "image/jpeg"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v0, "jpeg"

    const-string v1, "image/jpeg"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string v0, "png"

    const-string v1, "image/png"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v0, "tif"

    const-string v1, "image/tiff"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const-string v0, "tiff"

    const-string v1, "image/tiff"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const-string v0, "pnm"

    const-string v1, "image/x-portable-anymap"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v0, "pbm"

    const-string v1, "image/x-portable-bitmap"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const-string v0, "pgm"

    const-string v1, "image/x-portable-graymap"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-string v0, "xbm"

    const-string v1, "image/x-xbitmap"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const-string v0, "xwd"

    const-string v1, "image/x-xwindowdump"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const-string v0, "wrl"

    const-string v1, "model/vrml"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const-string v0, "vrml"

    const-string v1, "model/vrml"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const-string v0, "mpg"

    const-string v1, "video/m3peg"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const-string v0, "mpe"

    const-string v1, "video/mpeg"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    const-string v0, "mpeg"

    const-string v1, "video/mpeg"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v0, "mov"

    const-string v1, "video/quicktime"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const-string v0, "qt"

    const-string v1, "video/quicktime"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v0, "mxu"

    const-string v1, "video/vnd.mpegurl"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const-string v0, "avi"

    const-string v1, "video/x-msvideo"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const-string v0, "movie"

    const-string v1, "video/x-sgi-movie"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    const-string v0, "3gp"

    const-string v1, "video/3gpp"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string v0, "3gpp"

    const-string v1, "video/3gpp"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const-string v0, "mkv"

    const-string v1, "video/x-matroska"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string v0, "au"

    const-string v1, "audio/basic"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string v0, "snd"

    const-string v1, "audio/basic"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const-string v0, "mid"

    const-string v1, "audio/midi"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string v0, "midi"

    const-string v1, "audio/midi"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    const-string v0, "kar"

    const-string v1, "audio/midi"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const-string v0, "aif"

    const-string v1, "audio/x-aiff"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const-string v0, "aiff"

    const-string v1, "audio/x-aiff"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string v0, "aifc"

    const-string v1, "audio/x-aiff"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string v0, "mpga"

    const-string v1, "audio/mpeg"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const-string v0, "mp2"

    const-string v1, "audio/mpeg"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    const-string v0, "mp3"

    const-string v1, "audio/mpeg"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    const-string v0, "mp4"

    const-string v1, "audio/mp4"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    const-string v0, "m3u"

    const-string v1, "audio/x-mpegurl"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const-string v0, "ram"

    const-string v1, "audio/x-pn-realaudio"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const-string v0, "rm"

    const-string v1, "audio/x-pn-realaudio"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const-string v0, "ra"

    const-string v1, "audio/x-realaudio"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const-string v0, "wav"

    const-string v1, "audio/x-wav"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const-string v0, "amr"

    const-string v1, "audio/3gpp"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const-string v0, "mka"

    const-string v1, "audio/x-matroska"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const-string v0, "3ga"

    const-string v1, "audio/3ga"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const-string v0, "wma"

    const-string v1, "audio/x-ms-wma"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    const-string v0, "wmv"

    const-string v1, "audio/x-ms-wmv"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const-string v0, "url"

    const-string v1, "text/url"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string v0, "apk"

    const-string v1, "application/vnd.android.package-archive"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    const-string v0, "epub"

    const-string v1, "application/epub+zip"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const-string v0, "prc"

    const-string v1, "application/x-pilot-prc"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const-string v0, "mobi"

    const-string v1, "application/x-mobipocket-ebook"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const-string v0, "cbr"

    const-string v1, "application/x-cbr"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const-string v0, "cbz"

    const-string v1, "application/x-cbr"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    const-string v0, "cbt"

    const-string v1, "application/x-cbr"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    const-string v0, "cba"

    const-string v1, "application/x-cbr"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const-string v0, "cb7"

    const-string v1, "application/x-cbr"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    sget-object v0, Lcom/dropbox/android/util/ab;->h:Ljava/util/Set;

    const-string v1, "image/bmp"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 192
    sget-object v0, Lcom/dropbox/android/util/ab;->h:Ljava/util/Set;

    const-string v1, "image/gif"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 193
    sget-object v0, Lcom/dropbox/android/util/ab;->h:Ljava/util/Set;

    const-string v1, "image/png"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 194
    sget-object v0, Lcom/dropbox/android/util/ab;->h:Ljava/util/Set;

    const-string v1, "image/tiff"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 195
    sget-object v0, Lcom/dropbox/android/util/ab;->h:Ljava/util/Set;

    const-string v1, "image/x-portable-anymap"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 196
    sget-object v0, Lcom/dropbox/android/util/ab;->h:Ljava/util/Set;

    const-string v1, "image/x-portable-bitmap"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 197
    sget-object v0, Lcom/dropbox/android/util/ab;->h:Ljava/util/Set;

    const-string v1, "image/x-portable-graymap"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 198
    sget-object v0, Lcom/dropbox/android/util/ab;->h:Ljava/util/Set;

    const-string v1, "image/x-xbitmap"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 199
    sget-object v0, Lcom/dropbox/android/util/ab;->h:Ljava/util/Set;

    const-string v1, "image/x-xwindowdump"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 202
    const-string v0, "apk"

    const-string v1, "application/vnd.android.package-archive"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    const-string v0, "wma"

    const-string v1, "audio/x-ms-wma"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    const-string v0, "url"

    const-string v1, "application/x-url"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const-string v0, "epub"

    const-string v1, "application/epub+zip"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    const-string v0, "prc"

    const-string v1, "application/x-pilot-prc"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    const-string v0, "mobi"

    const-string v1, "application/x-mobipocket-ebook"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    const-string v0, "psafe3"

    const-string v1, "application/application/x-psafe"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const-string v0, "cbr"

    const-string v1, "application/x-cbr"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    const-string v0, "cbz"

    const-string v1, "application/x-cbr"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    const-string v0, "cbt"

    const-string v1, "application/x-cbr"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const-string v0, "cba"

    const-string v1, "application/x-cbr"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    const-string v0, "cb7"

    const-string v1, "application/x-cbr"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const-string v0, "mmap"

    const-string v1, "application/vnd.mindjet.mindmanager"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    const-string v0, "xmmap"

    const-string v1, "application/vnd.mindjet.mindmanager"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    const-string v0, "mm"

    const-string v1, "application/x-freemind"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    const-string v0, "rmvb"

    const-string v1, "video/x-rmvb"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    const-string v0, "aa"

    const-string v1, "audio/audible"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const-string v0, "aax"

    const-string v1, "audio/vnd.audible.aax"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const-string v0, "bmml"

    const-string v1, "text/vnd.balsamiq.bmml"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const-string v0, "vnt"

    const-string v1, "text/x-vnote"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const-string v0, "hwp"

    const-string v1, "application/x-hwp"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    return-void

    .line 32
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ldbxyzptlk/db231222/k/h;Landroid/net/Uri;)Ljava/io/File;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 548
    .line 550
    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 551
    if-nez v0, :cond_0

    .line 553
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 588
    :goto_0
    if-nez v0, :cond_4

    .line 589
    sget-object v0, Lcom/dropbox/android/util/ab;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to determine source File from uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Lcom/dropbox/android/util/ab;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    :goto_1
    return-object v6

    .line 554
    :cond_0
    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 555
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 556
    :cond_1
    const-string v1, "content"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.dropbox.android.Dropbox"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 558
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v0, p2}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    .line 559
    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/DropboxPath;->a(Ldbxyzptlk/db231222/k/h;)Ldbxyzptlk/db231222/k/f;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/k/f;->a()Ljava/io/File;

    move-result-object v6

    goto :goto_1

    .line 562
    :cond_2
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 563
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 564
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 565
    invoke-static {p0, v1}, Lcom/dropbox/android/util/ab;->a(Landroid/content/Context;Ljava/io/File;)Ljava/io/File;

    move-result-object v6

    goto :goto_1

    .line 569
    :cond_3
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    .line 570
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 571
    if-eqz v1, :cond_7

    .line 572
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 573
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 575
    :goto_2
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_0

    .line 577
    :catch_0
    move-exception v0

    .line 579
    sget-object v1, Lcom/dropbox/android/util/ab;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Security exception, couldn\'t get filename from URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Lcom/dropbox/android/util/ab;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    throw v0

    .line 581
    :catch_1
    move-exception v0

    move-object v1, v6

    .line 582
    :goto_3
    sget-object v2, Lcom/dropbox/android/util/ab;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t get filename from content provider for: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p2}, Lcom/dropbox/android/util/ab;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 583
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v2

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    move-object v0, v1

    goto/16 :goto_0

    .line 591
    :cond_4
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 592
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 593
    invoke-static {p0, v1}, Lcom/dropbox/android/util/ab;->a(Landroid/content/Context;Ljava/io/File;)Ljava/io/File;

    move-result-object v6

    goto/16 :goto_1

    .line 595
    :cond_5
    sget-object v0, Lcom/dropbox/android/util/ab;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "File doesn\'t exist: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v1}, Lcom/dropbox/android/util/ab;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 581
    :catch_2
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_3

    :cond_6
    move-object v0, v6

    goto :goto_2

    :cond_7
    move-object v0, v6

    goto/16 :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/io/File;)Ljava/io/File;
    .locals 2

    .prologue
    .line 619
    invoke-static {p0, p1}, Lcom/dropbox/android/util/ab;->b(Landroid/content/Context;Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 620
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Security exception, a filename we don\'t allow was attempting to be uploaded: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/dropbox/android/util/ab;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 622
    sget-object v1, Lcom/dropbox/android/util/ab;->b:Ljava/lang/String;

    invoke-static {v1, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 626
    :cond_0
    return-object p1
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 284
    const-string v0, "thumb_exists = 1 and (mime_type like \'image/%\' or mime_type like \'video/%\')"

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 454
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 455
    if-nez v0, :cond_1

    .line 457
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 534
    :cond_0
    :goto_0
    return-object v0

    .line 458
    :cond_1
    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 460
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 461
    :cond_2
    const-string v1, "content"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.dropbox.android.Dropbox"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 463
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v0, p1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 465
    :cond_3
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 467
    if-eqz v0, :cond_4

    .line 468
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 469
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 470
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 484
    :cond_4
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 485
    if-eqz v1, :cond_a

    .line 486
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 487
    const-string v0, "_display_name"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 488
    if-ltz v0, :cond_5

    .line 489
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 492
    :cond_5
    :try_start_1
    const-string v0, "_data"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 493
    if-ltz v0, :cond_8

    .line 502
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 503
    if-eqz v0, :cond_8

    .line 504
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 505
    if-eqz v6, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v3

    if-le v2, v3, :cond_8

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v2

    if-eqz v2, :cond_8

    .line 513
    :cond_6
    :goto_1
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_3

    .line 526
    :goto_2
    invoke-static {v0}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 527
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 530
    :cond_7
    invoke-static {v0}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 531
    const-string v0, "unknown"

    goto/16 :goto_0

    .line 516
    :catch_0
    move-exception v0

    move-object v0, v6

    .line 518
    :goto_3
    sget-object v1, Lcom/dropbox/android/util/ab;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Security exception, couldn\'t get filename from URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Lcom/dropbox/android/util/ab;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 519
    :catch_1
    move-exception v0

    move-object v1, v0

    move-object v0, v6

    .line 521
    :goto_4
    sget-object v2, Lcom/dropbox/android/util/ab;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t get filename from URI: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lcom/dropbox/android/util/ab;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v2

    invoke-virtual {v2, v1}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 519
    :catch_2
    move-exception v0

    move-object v1, v0

    move-object v0, v6

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_4

    .line 516
    :catch_4
    move-exception v0

    move-object v0, v6

    goto :goto_3

    :catch_5
    move-exception v1

    goto :goto_3

    :cond_8
    move-object v0, v6

    goto :goto_1

    :cond_9
    move-object v0, v6

    goto :goto_1

    :cond_a
    move-object v0, v6

    goto :goto_2
.end method

.method public static a(Ljava/io/File;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 720
    const/4 v0, 0x0

    invoke-static {v0, p0}, Lcom/dropbox/android/util/ab;->a(Ljava/io/File;Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/io/File;Ljava/io/File;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 729
    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    .line 730
    if-eqz p0, :cond_0

    .line 732
    invoke-virtual {p0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    .line 733
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 734
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 737
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/dropbox/android/util/ab;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 246
    sget-object v0, Lcom/dropbox/android/util/ab;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 388
    const-string v0, "."

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 389
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 391
    sget-object v1, Lcom/dropbox/android/util/ab;->f:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 392
    if-nez v0, :cond_0

    .line 395
    :goto_0
    return-object p1

    :cond_0
    move-object p1, v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/util/Collection;)Ljava/util/HashMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 664
    new-instance v1, Ljava/util/HashMap;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 665
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 666
    invoke-static {p0, v0}, Lcom/dropbox/android/util/ab;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 668
    :cond_0
    return-object v1
.end method

.method public static a(Landroid/database/Cursor;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 673
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 675
    if-eqz p0, :cond_1

    :try_start_0
    invoke-interface {p0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 676
    const/4 v1, -0x1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 677
    :cond_0
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 678
    sget-object v1, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    invoke-static {p0, v1}, Lcom/dropbox/android/provider/Z;->a(Landroid/database/Cursor;Lcom/dropbox/android/provider/Z;)Lcom/dropbox/android/provider/Z;

    move-result-object v1

    .line 679
    sget-object v2, Lcom/dropbox/android/provider/Z;->a:Lcom/dropbox/android/provider/Z;

    if-ne v1, v2, :cond_0

    .line 680
    const-string v1, "_display_name"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 681
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 682
    if-eqz v1, :cond_0

    .line 683
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/database/StaleDataException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 688
    :catch_0
    move-exception v1

    .line 692
    :cond_1
    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 226
    const/4 v0, 0x0

    .line 227
    const-string v1, "text/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 228
    const/4 v0, 0x1

    .line 230
    :cond_0
    invoke-static {p0, p1, p2, v0}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 231
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 234
    sget-object v0, Lcom/dropbox/android/util/ab;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    sget-object v0, Lcom/dropbox/android/util/ab;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    sget-object v0, Lcom/dropbox/android/util/ab;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    sget-object v0, Lcom/dropbox/android/util/ab;->g:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    return-void
.end method

.method public static a(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 399
    invoke-static {p0}, Lcom/dropbox/android/util/ab;->d(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/dropbox/android/util/ab;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 411
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 415
    :cond_0
    :goto_0
    return v0

    .line 414
    :cond_1
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\\."

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 415
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    array-length v2, v1

    const/4 v3, 0x2

    if-lt v2, v3, :cond_0

    const-string v2, "dropbox"

    array-length v3, v1

    add-int/lit8 v3, v3, -0x2

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "com"

    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    aget-object v1, v1, v3

    invoke-static {v2, v1}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/dropbox/android/filemanager/LocalEntry;)Z
    .locals 2

    .prologue
    .line 278
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->e:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "image/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "video/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/dropbox/android/filemanager/LocalEntry;Z)Z
    .locals 2

    .prologue
    .line 308
    if-eqz p1, :cond_0

    .line 309
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/filemanager/LocalEntry;->a(Ljava/lang/String;)V

    .line 312
    :cond_0
    invoke-static {p0}, Lcom/dropbox/android/util/ab;->a(Lcom/dropbox/android/filemanager/LocalEntry;)Z

    move-result v0

    return v0
.end method

.method public static b()Lcom/dropbox/android/util/aY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/dropbox/android/util/aY",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 289
    new-instance v0, Lcom/dropbox/android/util/ac;

    invoke-direct {v0}, Lcom/dropbox/android/util/ac;-><init>()V

    return-object v0
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 241
    sget-object v0, Lcom/dropbox/android/util/ab;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    return-void
.end method

.method public static b(Landroid/content/Context;Ldbxyzptlk/db231222/k/h;Landroid/net/Uri;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 605
    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 606
    if-eqz v2, :cond_0

    const-string v3, "file"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 608
    :cond_0
    :try_start_0
    invoke-static {p0, p1, p2}, Lcom/dropbox/android/util/ab;->a(Landroid/content/Context;Ldbxyzptlk/db231222/k/h;Landroid/net/Uri;)Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    if-nez v2, :cond_1

    .line 615
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 608
    goto :goto_0

    :cond_2
    move v0, v1

    .line 615
    goto :goto_0

    .line 609
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;Ljava/io/File;)Z
    .locals 2

    .prologue
    .line 635
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v0

    .line 637
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 640
    :goto_0
    return v0

    .line 638
    :catch_0
    move-exception v0

    .line 640
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 407
    const-string v0, "https"

    invoke-static {p0, v0}, Lcom/dropbox/android/util/ab;->a(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 250
    sget-object v0, Lcom/dropbox/android/util/ab;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    sget-object v0, Lcom/dropbox/android/util/ab;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 253
    :goto_0
    return v0

    :cond_0
    const-string v0, "text/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static c(Landroid/net/Uri;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 758
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 759
    if-nez v0, :cond_2

    .line 760
    const-string v0, ""

    .line 764
    :goto_0
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 765
    if-nez v1, :cond_0

    .line 766
    const-string v1, ""

    .line 768
    :cond_0
    const-string v2, ""

    .line 769
    invoke-virtual {p0}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v3

    .line 770
    if-eqz v3, :cond_1

    .line 771
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "?"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v3}, Lcom/dropbox/android/util/ab;->t(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 773
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/dropbox/android/util/ab;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 762
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 258
    const-string v0, "text/html"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static d(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 403
    const-string v0, "http"

    invoke-static {p0, v0}, Lcom/dropbox/android/util/ab;->a(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 262
    const-string v0, "application/x-url"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static e(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 266
    const-string v0, ".webloc"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static f(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 317
    .line 318
    const-string v0, "."

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 319
    const/4 v0, -0x1

    if-le v1, v0, :cond_0

    .line 320
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    .line 322
    :cond_0
    sget-object v0, Lcom/dropbox/android/util/ab;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 323
    if-nez v0, :cond_1

    if-lez v1, :cond_1

    .line 324
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->Z()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "ext"

    invoke-virtual {v1, v2, p0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 326
    :cond_1
    return-object v0
.end method

.method public static g(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 330
    if-nez p0, :cond_1

    .line 334
    :cond_0
    :goto_0
    return v0

    .line 333
    :cond_1
    const-string v1, "/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 334
    aget-object v2, v1, v0

    const-string v3, "video"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    aget-object v1, v1, v0

    const-string v2, "image"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static h(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 339
    if-eqz p0, :cond_0

    const-string v0, "video/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 343
    if-eqz p0, :cond_0

    const-string v0, "image/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 347
    sget-object v0, Lcom/dropbox/android/util/ab;->h:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static k(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 351
    if-eqz p0, :cond_0

    const-string v0, "audio/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static l(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 355
    invoke-static {p0}, Lcom/dropbox/android/util/ab;->h(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/dropbox/android/util/ab;->k(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 362
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HTC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 363
    if-eqz v0, :cond_1

    .line 364
    sget-object v0, Lcom/dropbox/android/util/ab;->b:Ljava/lang/String;

    const-string v1, "Transcoding disabled; HTC."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    const/4 v0, 0x0

    .line 373
    :cond_0
    return v0

    .line 369
    :cond_1
    invoke-static {p0}, Lcom/dropbox/android/util/ab;->h(Ljava/lang/String;)Z

    move-result v0

    .line 370
    if-eqz v0, :cond_0

    .line 371
    sget-boolean v1, Lcom/dropbox/android/util/ab;->a:Z

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/dropbox/android/util/ab;->l(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static n(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 379
    const-string v0, "."

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 380
    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    .line 381
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 383
    :cond_0
    sget-object v0, Lcom/dropbox/android/util/ab;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static o(Ljava/lang/String;)Landroid/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 424
    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 425
    new-instance v1, Landroid/util/Pair;

    const/4 v2, 0x0

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method

.method public static p(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 434
    const-string v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 436
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 438
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static q(Ljava/lang/String;)Landroid/util/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 653
    const/16 v0, 0x2e

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 654
    if-gtz v1, :cond_0

    .line 655
    new-instance v0, Landroid/util/Pair;

    const-string v1, ""

    invoke-direct {v0, p0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 657
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/util/Pair;

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static r(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 696
    const-string v0, "[\\000-\\037]|\\\\|/|:|\\?|\\*|<|>|\"|\\|"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static s(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 707
    const-string v0, "/"

    invoke-static {p0, v0}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 708
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 709
    aget-object v2, v1, v0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 710
    aget-object v2, v1, v0

    invoke-static {v2}, Lcom/dropbox/android/util/ab;->t(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 708
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 713
    :cond_1
    const-string v0, "/"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static t(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 746
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%x"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
