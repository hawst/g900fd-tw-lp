.class public final Lcom/dropbox/android/util/analytics/u;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:Lcom/dropbox/android/util/az;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lcom/dropbox/android/util/aA;

    invoke-direct {v0}, Lcom/dropbox/android/util/aA;-><init>()V

    invoke-direct {p0, v0}, Lcom/dropbox/android/util/analytics/u;-><init>(Lcom/dropbox/android/util/az;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/util/az;)V
    .locals 0
    .annotation runtime Ldbxyzptlk/db231222/W/a;
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/dropbox/android/util/analytics/u;->a:Lcom/dropbox/android/util/az;

    .line 44
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/ContentResolver;[Lcom/dropbox/android/filemanager/ad;)Lcom/dropbox/android/util/analytics/v;
    .locals 7

    .prologue
    .line 56
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 58
    new-instance v1, Lcom/dropbox/android/util/analytics/v;

    invoke-direct {v1}, Lcom/dropbox/android/util/analytics/v;-><init>()V

    .line 60
    array-length v2, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, p2, v0

    .line 61
    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/ad;->b()Landroid/net/Uri;

    move-result-object v3

    .line 62
    iget-object v4, p0, Lcom/dropbox/android/util/analytics/u;->a:Lcom/dropbox/android/util/az;

    invoke-interface {v4, p1, v3}, Lcom/dropbox/android/util/az;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Lcom/dropbox/android/util/ax;

    move-result-object v3

    .line 63
    if-nez v3, :cond_0

    .line 60
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 67
    :cond_0
    :try_start_0
    const-string v4, "_data"

    invoke-virtual {v3, v4}, Lcom/dropbox/android/util/ax;->a(Ljava/lang/String;)I

    move-result v4

    .line 68
    :cond_1
    :goto_2
    invoke-virtual {v3}, Lcom/dropbox/android/util/ax;->a()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 69
    invoke-virtual {v3, v4}, Lcom/dropbox/android/util/ax;->b(I)Ljava/lang/String;

    move-result-object v5

    .line 70
    if-eqz v5, :cond_1

    .line 73
    invoke-static {v5}, Lcom/dropbox/android/util/av;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 74
    iget v5, v1, Lcom/dropbox/android/util/analytics/v;->c:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v1, Lcom/dropbox/android/util/analytics/v;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 86
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/dropbox/android/util/ax;->b()V

    throw v0

    .line 75
    :cond_2
    :try_start_1
    invoke-static {v5}, Lcom/dropbox/android/util/av;->b(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 76
    const-string v5, "mime_type"

    invoke-virtual {v3, v5}, Lcom/dropbox/android/util/ax;->a(Ljava/lang/String;)I

    move-result v5

    .line 77
    invoke-virtual {v3, v5}, Lcom/dropbox/android/util/ax;->b(I)Ljava/lang/String;

    move-result-object v5

    .line 78
    invoke-static {v5}, Lcom/dropbox/android/util/ab;->i(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 79
    iget v5, v1, Lcom/dropbox/android/util/analytics/v;->a:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v1, Lcom/dropbox/android/util/analytics/v;->a:I

    goto :goto_2

    .line 80
    :cond_3
    invoke-static {v5}, Lcom/dropbox/android/util/ab;->h(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 81
    iget v5, v1, Lcom/dropbox/android/util/analytics/v;->b:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v1, Lcom/dropbox/android/util/analytics/v;->b:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 86
    :cond_4
    invoke-virtual {v3}, Lcom/dropbox/android/util/ax;->b()V

    goto :goto_1

    .line 89
    :cond_5
    return-object v1
.end method
