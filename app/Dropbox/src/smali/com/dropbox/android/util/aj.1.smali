.class final Lcom/dropbox/android/util/aj;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Parcel;)Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;
    .locals 1

    .prologue
    .line 223
    new-instance v0, Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;

    invoke-direct {v0, p1}, Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final a(I)[Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;
    .locals 1

    .prologue
    .line 228
    new-array v0, p1, [Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;

    return-object v0
.end method

.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 220
    invoke-virtual {p0, p1}, Lcom/dropbox/android/util/aj;->a(Landroid/os/Parcel;)Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 220
    invoke-virtual {p0, p1}, Lcom/dropbox/android/util/aj;->a(I)[Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;

    move-result-object v0

    return-object v0
.end method
