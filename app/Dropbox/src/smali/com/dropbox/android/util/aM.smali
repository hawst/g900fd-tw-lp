.class public abstract enum Lcom/dropbox/android/util/aM;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/util/aM;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/util/aM;

.field public static final enum b:Lcom/dropbox/android/util/aM;

.field public static final enum c:Lcom/dropbox/android/util/aM;

.field public static final enum d:Lcom/dropbox/android/util/aM;

.field public static final enum e:Lcom/dropbox/android/util/aM;

.field public static final enum f:Lcom/dropbox/android/util/aM;

.field public static final enum g:Lcom/dropbox/android/util/aM;

.field public static final enum h:Lcom/dropbox/android/util/aM;

.field public static final enum i:Lcom/dropbox/android/util/aM;

.field private static final synthetic j:[Lcom/dropbox/android/util/aM;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 48
    new-instance v0, Lcom/dropbox/android/util/aN;

    const-string v1, "CAMERA_UPLOAD_STOPPED_QUOTA"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/util/aN;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/aM;->a:Lcom/dropbox/android/util/aM;

    .line 71
    new-instance v0, Lcom/dropbox/android/util/aO;

    const-string v1, "UPLOAD_STOPPED_QUOTA"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/util/aO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/aM;->b:Lcom/dropbox/android/util/aM;

    .line 95
    new-instance v0, Lcom/dropbox/android/util/aP;

    const-string v1, "CAMERA_UPLOAD_PAUSED_BATTERY"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/android/util/aP;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/aM;->c:Lcom/dropbox/android/util/aM;

    .line 119
    new-instance v0, Lcom/dropbox/android/util/aQ;

    const-string v1, "CAMERA_UPLOAD_BACKLOG_FINISHED"

    invoke-direct {v0, v1, v6}, Lcom/dropbox/android/util/aQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/aM;->d:Lcom/dropbox/android/util/aM;

    .line 139
    new-instance v0, Lcom/dropbox/android/util/aR;

    const-string v1, "CAMERA_UPLOAD_FIRST_BUNCH_FINISHED"

    invoke-direct {v0, v1, v7}, Lcom/dropbox/android/util/aR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/aM;->e:Lcom/dropbox/android/util/aM;

    .line 159
    new-instance v0, Lcom/dropbox/android/util/aS;

    const-string v1, "UPLOAD_PROGRESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/util/aS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/aM;->f:Lcom/dropbox/android/util/aM;

    .line 194
    new-instance v0, Lcom/dropbox/android/util/aT;

    const-string v1, "UPLOAD_DONE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/util/aT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/aM;->g:Lcom/dropbox/android/util/aM;

    .line 225
    new-instance v0, Lcom/dropbox/android/util/aU;

    const-string v1, "UPDATE_AVAILABLE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/util/aU;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/aM;->h:Lcom/dropbox/android/util/aM;

    .line 263
    new-instance v0, Lcom/dropbox/android/util/aV;

    const-string v1, "DEAL_EXPIRATION_WARNING"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/util/aV;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/aM;->i:Lcom/dropbox/android/util/aM;

    .line 47
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/dropbox/android/util/aM;

    sget-object v1, Lcom/dropbox/android/util/aM;->a:Lcom/dropbox/android/util/aM;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/util/aM;->b:Lcom/dropbox/android/util/aM;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/util/aM;->c:Lcom/dropbox/android/util/aM;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/android/util/aM;->d:Lcom/dropbox/android/util/aM;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dropbox/android/util/aM;->e:Lcom/dropbox/android/util/aM;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/dropbox/android/util/aM;->f:Lcom/dropbox/android/util/aM;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dropbox/android/util/aM;->g:Lcom/dropbox/android/util/aM;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/dropbox/android/util/aM;->h:Lcom/dropbox/android/util/aM;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dropbox/android/util/aM;->i:Lcom/dropbox/android/util/aM;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/util/aM;->j:[Lcom/dropbox/android/util/aM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/dropbox/android/util/aG;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/util/aM;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/util/aM;
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/dropbox/android/util/aM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/aM;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/util/aM;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/dropbox/android/util/aM;->j:[Lcom/dropbox/android/util/aM;

    invoke-virtual {v0}, [Lcom/dropbox/android/util/aM;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/util/aM;

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/app/Notification;
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 332
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Singular."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/util/aM;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 343
    const/4 v0, 0x0

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 350
    const/4 v0, 0x0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 357
    const/4 v0, 0x1

    return v0
.end method
