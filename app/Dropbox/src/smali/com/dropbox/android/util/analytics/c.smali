.class final Lcom/dropbox/android/util/analytics/c;
.super Ljava/util/TimerTask;
.source "panda.py"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1624
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1628
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cv()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 1629
    :try_start_0
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cw()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 1630
    const-wide/32 v4, 0x80000

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    .line 1631
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cx()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v4, "reason"

    const-string v5, "size"

    invoke-virtual {v0, v4, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v4, "size"

    invoke-virtual {v0, v4, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 1632
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cy()V

    .line 1634
    :cond_0
    monitor-exit v1

    .line 1635
    return-void

    .line 1634
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
