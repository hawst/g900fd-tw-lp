.class final Lcom/dropbox/android/util/ai;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Parcel;)Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;
    .locals 1

    .prologue
    .line 153
    new-instance v0, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    invoke-direct {v0, p1}, Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final a(I)[Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;
    .locals 1

    .prologue
    .line 158
    new-array v0, p1, [Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    return-object v0
.end method

.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 150
    invoke-virtual {p0, p1}, Lcom/dropbox/android/util/ai;->a(Landroid/os/Parcel;)Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 150
    invoke-virtual {p0, p1}, Lcom/dropbox/android/util/ai;->a(I)[Lcom/dropbox/android/util/HistoryEntry$DropboxHistoryEntry;

    move-result-object v0

    return-object v0
.end method
