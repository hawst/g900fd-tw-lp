.class public final Lcom/dropbox/android/util/ax;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/ContentResolver;

.field private final c:Landroid/net/Uri;

.field private d:Z

.field private e:J

.field private f:Landroid/database/Cursor;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 21
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "_size"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "date_modified"

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/util/ax;->a:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/util/ax;->d:Z

    .line 32
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/dropbox/android/util/ax;->e:J

    .line 36
    iput-object p1, p0, Lcom/dropbox/android/util/ax;->b:Landroid/content/ContentResolver;

    .line 37
    iput-object p2, p0, Lcom/dropbox/android/util/ax;->c:Landroid/net/Uri;

    .line 38
    invoke-direct {p0}, Lcom/dropbox/android/util/ax;->c()V

    .line 39
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/ContentResolver;Landroid/net/Uri;Lcom/dropbox/android/util/ay;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/util/ax;-><init>(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/util/ax;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/dropbox/android/util/ax;->f:Landroid/database/Cursor;

    return-object v0
.end method

.method private c()V
    .locals 12

    .prologue
    const/16 v11, 0x64

    const/4 v10, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 123
    iget-object v0, p0, Lcom/dropbox/android/util/ax;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "limit"

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 127
    iget-boolean v0, p0, Lcom/dropbox/android/util/ax;->d:Z

    if-eqz v0, :cond_1

    iget-wide v2, p0, Lcom/dropbox/android/util/ax;->e:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_1

    .line 128
    const-string v3, "(mime_type LIKE ? OR mime_type LIKE ?) AND _id < ?"

    .line 131
    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, "image/%"

    aput-object v0, v4, v7

    const-string v0, "video/%"

    aput-object v0, v4, v6

    iget-wide v8, p0, Lcom/dropbox/android/util/ax;->e:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v10

    .line 138
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/util/ax;->f:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/dropbox/android/util/ax;->f:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 143
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/util/ax;->b:Landroid/content/ContentResolver;

    sget-object v2, Lcom/dropbox/android/util/ax;->a:[Ljava/lang/String;

    const-string v5, "_id DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/util/ax;->f:Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    :goto_1
    iget-object v0, p0, Lcom/dropbox/android/util/ax;->f:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/dropbox/android/util/ax;->f:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v11, :cond_2

    move v0, v6

    :goto_2
    iput-boolean v0, p0, Lcom/dropbox/android/util/ax;->d:Z

    .line 166
    return-void

    .line 133
    :cond_1
    const-string v3, "mime_type LIKE ? OR mime_type LIKE ?"

    .line 135
    new-array v4, v10, [Ljava/lang/String;

    const-string v0, "image/%"

    aput-object v0, v4, v7

    const-string v0, "video/%"

    aput-object v0, v4, v6

    goto :goto_0

    .line 148
    :catch_0
    move-exception v0

    .line 154
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "uri: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/util/ax;->c:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 157
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/util/ax;->f:Landroid/database/Cursor;

    goto :goto_1

    :cond_2
    move v0, v7

    .line 165
    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/dropbox/android/util/ax;->f:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final a(I)J
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/dropbox/android/util/ax;->f:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 78
    iget-object v1, p0, Lcom/dropbox/android/util/ax;->f:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 80
    const/4 v0, 0x1

    .line 95
    :cond_0
    :goto_0
    return v0

    .line 83
    :cond_1
    iget-boolean v1, p0, Lcom/dropbox/android/util/ax;->d:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/dropbox/android/util/ax;->f:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToLast()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    invoke-virtual {p0, v0}, Lcom/dropbox/android/util/ax;->a(I)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/dropbox/android/util/ax;->e:J

    .line 86
    invoke-direct {p0}, Lcom/dropbox/android/util/ax;->c()V

    .line 87
    iget-object v1, p0, Lcom/dropbox/android/util/ax;->f:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 92
    iget-object v0, p0, Lcom/dropbox/android/util/ax;->f:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    goto :goto_0
.end method

.method public final b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/dropbox/android/util/ax;->f:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/dropbox/android/util/ax;->f:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/dropbox/android/util/ax;->f:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 116
    :cond_0
    return-void
.end method
