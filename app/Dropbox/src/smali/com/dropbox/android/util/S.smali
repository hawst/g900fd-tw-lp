.class public final Lcom/dropbox/android/util/S;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static a:Lcom/dropbox/android/util/T;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    sput-object v0, Lcom/dropbox/android/util/S;->a:Lcom/dropbox/android/util/T;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method

.method public static a()Z
    .locals 3

    .prologue
    .line 17
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    .line 18
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "android.hardware.camera"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.camera.front"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 28
    const-class v3, Lcom/dropbox/android/util/S;

    monitor-enter v3

    .line 29
    :try_start_0
    sget-object v0, Lcom/dropbox/android/util/S;->a:Lcom/dropbox/android/util/T;

    if-nez v0, :cond_0

    .line 30
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    const-string v4, "connectivity"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    :try_start_1
    const-class v4, Landroid/net/ConnectivityManager;

    const-string v5, "isNetworkSupported"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 34
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v4, v0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 35
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/dropbox/android/util/T;->a:Lcom/dropbox/android/util/T;

    :goto_0
    sput-object v0, Lcom/dropbox/android/util/S;->a:Lcom/dropbox/android/util/T;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 46
    :cond_0
    :goto_1
    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 48
    sget-object v0, Lcom/dropbox/android/util/S;->a:Lcom/dropbox/android/util/T;

    sget-object v3, Lcom/dropbox/android/util/T;->c:Lcom/dropbox/android/util/T;

    if-eq v0, v3, :cond_3

    .line 49
    sget-object v0, Lcom/dropbox/android/util/S;->a:Lcom/dropbox/android/util/T;

    sget-object v3, Lcom/dropbox/android/util/T;->a:Lcom/dropbox/android/util/T;

    if-ne v0, v3, :cond_2

    move v0, v1

    .line 54
    :goto_2
    return v0

    .line 35
    :cond_1
    :try_start_3
    sget-object v0, Lcom/dropbox/android/util/T;->b:Lcom/dropbox/android/util/T;
    :try_end_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 36
    :catch_0
    move-exception v0

    .line 37
    :try_start_4
    sget-object v0, Lcom/dropbox/android/util/T;->c:Lcom/dropbox/android/util/T;

    sput-object v0, Lcom/dropbox/android/util/S;->a:Lcom/dropbox/android/util/T;

    goto :goto_1

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 38
    :catch_1
    move-exception v0

    .line 39
    :try_start_5
    sget-object v0, Lcom/dropbox/android/util/T;->c:Lcom/dropbox/android/util/T;

    sput-object v0, Lcom/dropbox/android/util/S;->a:Lcom/dropbox/android/util/T;

    goto :goto_1

    .line 40
    :catch_2
    move-exception v0

    .line 41
    sget-object v0, Lcom/dropbox/android/util/T;->c:Lcom/dropbox/android/util/T;

    sput-object v0, Lcom/dropbox/android/util/S;->a:Lcom/dropbox/android/util/T;

    goto :goto_1

    .line 42
    :catch_3
    move-exception v0

    .line 43
    sget-object v0, Lcom/dropbox/android/util/T;->c:Lcom/dropbox/android/util/T;

    sput-object v0, Lcom/dropbox/android/util/S;->a:Lcom/dropbox/android/util/T;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 49
    goto :goto_2

    .line 53
    :cond_3
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.telephony"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    goto :goto_2
.end method

.method public static c()I
    .locals 2

    .prologue
    .line 59
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 60
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    return v0
.end method
