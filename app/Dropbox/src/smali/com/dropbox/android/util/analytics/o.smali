.class final Lcom/dropbox/android/util/analytics/o;
.super Lcom/dropbox/android/util/analytics/l;
.source "panda.py"


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/i/c;Ldbxyzptlk/db231222/r/k;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2018
    const-string v0, "open.log"

    invoke-direct {p0, v0}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    .line 2020
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ldbxyzptlk/db231222/r/k;->f()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 2021
    :goto_0
    const-string v2, "APP_VERSION"

    iget-object v3, p1, Ldbxyzptlk/db231222/i/c;->a:Ljava/lang/String;

    invoke-virtual {p0, v2, v3}, Lcom/dropbox/android/util/analytics/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 2022
    const-string v2, "USER_ID"

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p0, v2, v0}, Lcom/dropbox/android/util/analytics/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 2023
    const-string v0, "DEVICE_ID"

    iget-object v2, p1, Ldbxyzptlk/db231222/i/c;->e:Ljava/lang/String;

    invoke-virtual {p0, v0, v2}, Lcom/dropbox/android/util/analytics/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 2024
    const-string v0, "PHONE_MODEL"

    iget-object v2, p1, Ldbxyzptlk/db231222/i/c;->c:Ljava/lang/String;

    invoke-virtual {p0, v0, v2}, Lcom/dropbox/android/util/analytics/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 2025
    const-string v0, "ANDROID_VERSION"

    iget-object v2, p1, Ldbxyzptlk/db231222/i/c;->d:Ljava/lang/String;

    invoke-virtual {p0, v0, v2}, Lcom/dropbox/android/util/analytics/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 2026
    const-string v0, "MANUFACTURER"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {p0, v0, v2}, Lcom/dropbox/android/util/analytics/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 2029
    const/4 v0, -0x1

    .line 2030
    if-eqz p2, :cond_0

    .line 2031
    invoke-virtual {p2}, Ldbxyzptlk/db231222/r/k;->h()Ldbxyzptlk/db231222/r/o;

    move-result-object v0

    .line 2032
    invoke-interface {v0}, Ldbxyzptlk/db231222/r/o;->e()Ljava/lang/String;

    move-result-object v1

    .line 2033
    invoke-interface {v0}, Ldbxyzptlk/db231222/r/o;->f()I

    move-result v0

    .line 2035
    :cond_0
    const-string v2, "LOG_SERIES_UUID"

    invoke-virtual {p0, v2, v1}, Lcom/dropbox/android/util/analytics/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 2036
    const-string v1, "LOG_SEQUENCE_NUMBER"

    int-to-long v2, v0

    invoke-virtual {p0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/o;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    .line 2038
    const-string v0, "LOCALE"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/analytics/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 2042
    return-void

    :cond_1
    move-object v0, v1

    .line 2020
    goto :goto_0

    .line 2022
    :cond_2
    const-string v0, "0"

    goto :goto_1
.end method
