.class public final Lcom/dropbox/android/util/x;
.super Ljava/io/InputStream;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/android/util/w;

.field private final b:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;[B)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/dropbox/android/util/x;->b:Ljava/io/InputStream;

    .line 33
    new-instance v0, Lcom/dropbox/android/util/w;

    invoke-direct {v0, p2}, Lcom/dropbox/android/util/w;-><init>([B)V

    iput-object v0, p0, Lcom/dropbox/android/util/x;->a:Lcom/dropbox/android/util/w;

    .line 34
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/dropbox/android/util/x;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 56
    return-void
.end method

.method public final read()I
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x1

    new-array v0, v0, [B

    .line 50
    invoke-virtual {p0, v0}, Lcom/dropbox/android/util/x;->read([B)I

    move-result v0

    return v0
.end method

.method public final read([BII)I
    .locals 6

    .prologue
    const/4 v0, -0x1

    .line 39
    iget-object v1, p0, Lcom/dropbox/android/util/x;->b:Ljava/io/InputStream;

    invoke-virtual {v1, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v5

    .line 40
    if-ne v5, v0, :cond_0

    move v5, v0

    .line 44
    :goto_0
    return v5

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/util/x;->a:Lcom/dropbox/android/util/w;

    move-object v1, p1

    move v2, p2

    move-object v3, p1

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/android/util/w;->a([BI[BII)V

    goto :goto_0
.end method
