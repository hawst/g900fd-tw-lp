.class final Lcom/dropbox/android/util/ap;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/dropbox/android/util/ao;

.field private b:I

.field private c:I


# direct methods
.method constructor <init>(Lcom/dropbox/android/util/ao;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 83
    iput-object p1, p0, Lcom/dropbox/android/util/ap;->a:Lcom/dropbox/android/util/ao;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput v0, p0, Lcom/dropbox/android/util/ap;->b:I

    .line 86
    iput v0, p0, Lcom/dropbox/android/util/ap;->c:I

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Integer;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 101
    iget v0, p0, Lcom/dropbox/android/util/ap;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 102
    iput v2, p0, Lcom/dropbox/android/util/ap;->b:I

    .line 103
    iget-object v0, p0, Lcom/dropbox/android/util/ap;->a:Lcom/dropbox/android/util/ao;

    invoke-static {v0}, Lcom/dropbox/android/util/ao;->c(Lcom/dropbox/android/util/ao;)[I

    move-result-object v0

    aget v0, v0, v2

    iput v0, p0, Lcom/dropbox/android/util/ap;->c:I

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/util/ap;->a:Lcom/dropbox/android/util/ao;

    invoke-static {v0}, Lcom/dropbox/android/util/ao;->c(Lcom/dropbox/android/util/ao;)[I

    move-result-object v0

    iget v1, p0, Lcom/dropbox/android/util/ap;->b:I

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    aget v0, v0, v1

    .line 106
    iget v1, p0, Lcom/dropbox/android/util/ap;->c:I

    .line 107
    iget v2, p0, Lcom/dropbox/android/util/ap;->c:I

    if-ne v2, v0, :cond_2

    .line 108
    iget v0, p0, Lcom/dropbox/android/util/ap;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/util/ap;->b:I

    .line 109
    iget v0, p0, Lcom/dropbox/android/util/ap;->b:I

    iget-object v2, p0, Lcom/dropbox/android/util/ap;->a:Lcom/dropbox/android/util/ao;

    invoke-static {v2}, Lcom/dropbox/android/util/ao;->b(Lcom/dropbox/android/util/ao;)I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 110
    iget-object v0, p0, Lcom/dropbox/android/util/ap;->a:Lcom/dropbox/android/util/ao;

    iget v2, p0, Lcom/dropbox/android/util/ap;->b:I

    invoke-static {v0, v2}, Lcom/dropbox/android/util/ao;->b(Lcom/dropbox/android/util/ao;I)I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/util/ap;->c:I

    .line 115
    :cond_1
    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    .line 113
    :cond_2
    iget v0, p0, Lcom/dropbox/android/util/ap;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/util/ap;->c:I

    goto :goto_0
.end method

.method public final hasNext()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 90
    iget v2, p0, Lcom/dropbox/android/util/ap;->b:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 91
    iget-object v2, p0, Lcom/dropbox/android/util/ap;->a:Lcom/dropbox/android/util/ao;

    invoke-static {v2}, Lcom/dropbox/android/util/ao;->a(Lcom/dropbox/android/util/ao;)I

    move-result v2

    if-lez v2, :cond_1

    .line 96
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 91
    goto :goto_0

    .line 93
    :cond_2
    iget v2, p0, Lcom/dropbox/android/util/ap;->b:I

    iget-object v3, p0, Lcom/dropbox/android/util/ap;->a:Lcom/dropbox/android/util/ao;

    invoke-static {v3}, Lcom/dropbox/android/util/ao;->b(Lcom/dropbox/android/util/ao;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_3

    .line 94
    iget v2, p0, Lcom/dropbox/android/util/ap;->c:I

    iget-object v3, p0, Lcom/dropbox/android/util/ap;->a:Lcom/dropbox/android/util/ao;

    iget v4, p0, Lcom/dropbox/android/util/ap;->b:I

    invoke-static {v3, v4}, Lcom/dropbox/android/util/ao;->a(Lcom/dropbox/android/util/ao;I)I

    move-result v3

    if-le v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 96
    :cond_3
    iget v2, p0, Lcom/dropbox/android/util/ap;->b:I

    iget-object v3, p0, Lcom/dropbox/android/util/ap;->a:Lcom/dropbox/android/util/ao;

    invoke-static {v3}, Lcom/dropbox/android/util/ao;->b(Lcom/dropbox/android/util/ao;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-lt v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/dropbox/android/util/ap;->a()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 120
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
