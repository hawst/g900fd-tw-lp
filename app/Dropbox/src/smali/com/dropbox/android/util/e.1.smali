.class public Lcom/dropbox/android/util/e;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/app/Activity;

.field private c:Lcom/dropbox/libs/fileobserver/DbxFileObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/dropbox/android/util/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/util/e;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/dropbox/android/util/e;->b:Landroid/app/Activity;

    .line 46
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/util/e;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/dropbox/android/util/e;->b:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/dropbox/android/util/e;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/dropbox/android/util/e;->c:Lcom/dropbox/libs/fileobserver/DbxFileObserver;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/dropbox/android/util/e;->c:Lcom/dropbox/libs/fileobserver/DbxFileObserver;

    invoke-virtual {v0}, Lcom/dropbox/libs/fileobserver/DbxFileObserver;->a()V

    .line 116
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/util/e;->c:Lcom/dropbox/libs/fileobserver/DbxFileObserver;

    .line 118
    :cond_0
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/dropbox/android/util/e;->a()V

    .line 58
    if-eqz p1, :cond_0

    .line 60
    const-string v0, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 69
    :goto_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 70
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 105
    :cond_0
    :goto_1
    return-void

    .line 62
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 75
    :cond_2
    :try_start_0
    new-instance v1, Lcom/dropbox/android/util/f;

    invoke-direct {v1, p0}, Lcom/dropbox/android/util/f;-><init>(Lcom/dropbox/android/util/e;)V

    iput-object v1, p0, Lcom/dropbox/android/util/e;->c:Lcom/dropbox/libs/fileobserver/DbxFileObserver;

    .line 85
    iget-object v1, p0, Lcom/dropbox/android/util/e;->c:Lcom/dropbox/libs/fileobserver/DbxFileObserver;

    const/16 v2, 0xec0

    invoke-virtual {v1, v0, v2}, Lcom/dropbox/libs/fileobserver/DbxFileObserver;->a(Ljava/lang/String;I)I
    :try_end_0
    .catch Lcom/dropbox/libs/fileobserver/exceptions/UserInstanceLimitException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/dropbox/libs/fileobserver/exceptions/UserWatchLimitException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/dropbox/libs/fileobserver/exceptions/BadPathException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/dropbox/libs/fileobserver/exceptions/PermissionException; {:try_start_0 .. :try_end_0} :catch_3

    .line 99
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/dropbox/android/util/e;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_1

    .line 86
    :catch_0
    move-exception v0

    .line 87
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 88
    :catch_1
    move-exception v0

    .line 89
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 90
    :catch_2
    move-exception v0

    .line 92
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 93
    :catch_3
    move-exception v0

    .line 95
    sget-object v1, Lcom/dropbox/android/util/e;->a:Ljava/lang/String;

    const-string v2, "Failed to watch file"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
