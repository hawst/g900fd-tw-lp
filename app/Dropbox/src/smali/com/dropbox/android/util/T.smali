.class final enum Lcom/dropbox/android/util/T;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/util/T;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/util/T;

.field public static final enum b:Lcom/dropbox/android/util/T;

.field public static final enum c:Lcom/dropbox/android/util/T;

.field private static final synthetic d:[Lcom/dropbox/android/util/T;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23
    new-instance v0, Lcom/dropbox/android/util/T;

    const-string v1, "SUPPORTED"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/util/T;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/T;->a:Lcom/dropbox/android/util/T;

    new-instance v0, Lcom/dropbox/android/util/T;

    const-string v1, "UNSUPPORTED"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/util/T;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/T;->b:Lcom/dropbox/android/util/T;

    new-instance v0, Lcom/dropbox/android/util/T;

    const-string v1, "UNABLE_TO_CHECK"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/util/T;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/T;->c:Lcom/dropbox/android/util/T;

    .line 22
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dropbox/android/util/T;

    sget-object v1, Lcom/dropbox/android/util/T;->a:Lcom/dropbox/android/util/T;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/util/T;->b:Lcom/dropbox/android/util/T;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/util/T;->c:Lcom/dropbox/android/util/T;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dropbox/android/util/T;->d:[Lcom/dropbox/android/util/T;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/util/T;
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/dropbox/android/util/T;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/T;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/util/T;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/dropbox/android/util/T;->d:[Lcom/dropbox/android/util/T;

    invoke-virtual {v0}, [Lcom/dropbox/android/util/T;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/util/T;

    return-object v0
.end method
