.class final Lcom/dropbox/android/util/k;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:I

.field private b:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 183
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/util/k;->a:I

    .line 184
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/util/k;->b:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/android/util/j;)V
    .locals 0

    .prologue
    .line 181
    invoke-direct {p0}, Lcom/dropbox/android/util/k;-><init>()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(DZ)V
    .locals 5

    .prologue
    .line 191
    monitor-enter p0

    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, p1

    double-to-int v0, v0

    .line 192
    :try_start_0
    iget v1, p0, Lcom/dropbox/android/util/k;->a:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/dropbox/android/util/k;->b:Z

    if-ne v1, p3, :cond_0

    iget v1, p0, Lcom/dropbox/android/util/k;->a:I

    sub-int/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const/16 v2, 0xa

    if-lt v1, v2, :cond_1

    .line 198
    :cond_0
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->E()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "percent"

    int-to-long v3, v0

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "plugged"

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 199
    iput v0, p0, Lcom/dropbox/android/util/k;->a:I

    .line 200
    iput-boolean p3, p0, Lcom/dropbox/android/util/k;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    :cond_1
    monitor-exit p0

    return-void

    .line 191
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
