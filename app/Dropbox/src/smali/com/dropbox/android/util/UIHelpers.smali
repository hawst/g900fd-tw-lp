.class public final Lcom/dropbox/android/util/UIHelpers;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/dropbox/android/taskqueue/w;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const v3, 0x7f0d0088

    .line 100
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/dropbox/android/taskqueue/w;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/dropbox/android/util/UIHelpers;->a:Ljava/util/EnumMap;

    .line 103
    sget-object v0, Lcom/dropbox/android/util/UIHelpers;->a:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->a:Lcom/dropbox/android/taskqueue/w;

    const v2, 0x7f0d0086

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lcom/dropbox/android/util/UIHelpers;->a:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->b:Lcom/dropbox/android/taskqueue/w;

    const v2, 0x7f0d0087

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v0, Lcom/dropbox/android/util/UIHelpers;->a:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->d:Lcom/dropbox/android/taskqueue/w;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lcom/dropbox/android/util/UIHelpers;->a:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->e:Lcom/dropbox/android/taskqueue/w;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/dropbox/android/util/UIHelpers;->a:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->f:Lcom/dropbox/android/taskqueue/w;

    const v2, 0x7f0d0089

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/dropbox/android/util/UIHelpers;->a:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->h:Lcom/dropbox/android/taskqueue/w;

    const v2, 0x7f0d008a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/dropbox/android/util/UIHelpers;->a:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->i:Lcom/dropbox/android/taskqueue/w;

    const v2, 0x7f0d008b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lcom/dropbox/android/util/UIHelpers;->a:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->k:Lcom/dropbox/android/taskqueue/w;

    const v2, 0x7f0d008c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v0, Lcom/dropbox/android/util/UIHelpers;->a:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->l:Lcom/dropbox/android/taskqueue/w;

    const v2, 0x7f0d0076

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lcom/dropbox/android/util/UIHelpers;->a:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->m:Lcom/dropbox/android/taskqueue/w;

    const v2, 0x7f0d0192

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lcom/dropbox/android/util/UIHelpers;->a:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->n:Lcom/dropbox/android/taskqueue/w;

    const v2, 0x7f0d008d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v0, Lcom/dropbox/android/util/UIHelpers;->a:Ljava/util/EnumMap;

    sget-object v1, Lcom/dropbox/android/taskqueue/w;->o:Lcom/dropbox/android/taskqueue/w;

    const v2, 0x7f0d008e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 492
    const/4 v0, 0x0

    sput-boolean v0, Lcom/dropbox/android/util/UIHelpers;->b:Z

    return-void
.end method

.method public static a(Ljava/util/Date;)J
    .locals 2

    .prologue
    .line 90
    if-eqz p0, :cond_0

    .line 92
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 93
    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 94
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    .line 96
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;IIIZZ)Lcom/dropbox/android/util/UIHelpers$TextViewWithObservableAttach;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 390
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 391
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 392
    new-instance v1, Lcom/dropbox/android/util/UIHelpers$TextViewWithObservableAttach;

    const v2, 0x7f01006b

    invoke-direct {v1, p0, v3, v2}, Lcom/dropbox/android/util/UIHelpers$TextViewWithObservableAttach;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 393
    if-eqz p5, :cond_0

    .line 394
    invoke-virtual {v1, p1}, Lcom/dropbox/android/util/UIHelpers$TextViewWithObservableAttach;->setText(I)V

    .line 398
    :goto_0
    const v2, 0x7f010041

    invoke-static {p0, v2}, Lcom/dropbox/android/util/bn;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v1, p0, v2}, Lcom/dropbox/android/util/UIHelpers$TextViewWithObservableAttach;->setTextAppearance(Landroid/content/Context;I)V

    .line 399
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/dropbox/android/util/UIHelpers$TextViewWithObservableAttach;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 400
    invoke-virtual {v1, v0, v3, v3, v3}, Lcom/dropbox/android/util/UIHelpers$TextViewWithObservableAttach;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 401
    invoke-virtual {v1, p4}, Lcom/dropbox/android/util/UIHelpers$TextViewWithObservableAttach;->setEnabled(Z)V

    .line 403
    new-instance v0, Lcom/dropbox/android/util/bq;

    invoke-direct {v0, p1}, Lcom/dropbox/android/util/bq;-><init>(I)V

    invoke-virtual {v1, v0}, Lcom/dropbox/android/util/UIHelpers$TextViewWithObservableAttach;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 410
    return-object v1

    .line 396
    :cond_0
    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/dropbox/android/util/UIHelpers$TextViewWithObservableAttach;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/util/bt;
    .locals 4

    .prologue
    .line 264
    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v0

    .line 265
    invoke-static {v0}, Lcom/dropbox/android/util/ab;->m(Ljava/lang/String;)Z

    move-result v1

    .line 266
    invoke-static {v0}, Lcom/dropbox/android/util/ab;->l(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 267
    new-instance v0, Lcom/dropbox/android/util/bt;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/util/bt;-><init>(ZZLandroid/net/Uri;)V

    .line 272
    :goto_0
    return-object v0

    .line 270
    :cond_0
    const-string v2, "https://dl.dropbox.com/fake_streaming_file"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 271
    invoke-static {p0, v2, v0}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v3

    .line 272
    new-instance v0, Lcom/dropbox/android/util/bt;

    invoke-direct {v0, v1, v3, v2}, Lcom/dropbox/android/util/bt;-><init>(ZZLandroid/net/Uri;)V

    goto :goto_0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 326
    const v0, 0x7f0d00ea

    const v1, 0x7f0f0011

    invoke-static {p0, v0, v1}, Lcom/dropbox/android/util/UIHelpers;->a(III)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(III)Ljava/lang/String;
    .locals 4

    .prologue
    .line 331
    if-nez p0, :cond_0

    .line 332
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 334
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, p2, p0, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(J)Ljava/lang/String;
    .locals 9

    .prologue
    const-wide/32 v7, 0x36ee80

    const-wide/16 v5, 0x3c

    .line 81
    const-string v0, "%02d:%02d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-wide/32 v3, 0xea60

    div-long v3, p0, v3

    rem-long/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-wide/16 v3, 0x3e8

    div-long v3, p0, v3

    rem-long/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 83
    div-long v1, p0, v7

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    .line 84
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    div-long v2, p0, v7

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 86
    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    const v0, 0x7f0d0017

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 76
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/res/Resources;Lcom/dropbox/android/util/DropboxPath;I)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 315
    invoke-static {p0, p1}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/content/res/Resources;Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v0

    .line 317
    if-nez p2, :cond_0

    .line 318
    const v1, 0x7f0d00e9

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 320
    :goto_0
    return-object v0

    :cond_0
    const v1, 0x7f0f0010

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-virtual {p0, v1, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/dropbox/android/taskqueue/w;Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    if-nez p0, :cond_0

    .line 119
    const/4 v0, 0x0

    .line 121
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/dropbox/android/util/UIHelpers;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, p0}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 528
    const/4 v0, 0x0

    sput-boolean v0, Lcom/dropbox/android/util/UIHelpers;->b:Z

    .line 529
    return-void
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 6

    .prologue
    const/high16 v4, 0x42480000    # 50.0f

    .line 345
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 346
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 347
    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 350
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v4

    float-to-int v2, v2

    sub-int/2addr v1, v2

    const/high16 v2, 0x440c0000    # 560.0f

    iget v3, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 352
    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v3, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x19

    const/high16 v3, 0x442f0000    # 700.0f

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 355
    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int/2addr v3, v1

    div-int/lit8 v3, v3, 0x2

    .line 356
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x19

    .line 358
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v4

    .line 359
    invoke-virtual {v4}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    .line 360
    iput v3, v5, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 361
    iput v0, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 362
    iput v1, v5, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 363
    iput v2, v5, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 364
    invoke-virtual {v4, v5}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 365
    const/16 v0, 0x33

    invoke-virtual {v4, v0}, Landroid/view/Window;->setGravity(I)V

    .line 366
    return-void
.end method

.method public static a(Landroid/os/Handler;Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 444
    new-instance v0, Lcom/dropbox/android/util/br;

    invoke-direct {v0, p1}, Lcom/dropbox/android/util/br;-><init>(Landroid/app/Activity;)V

    invoke-virtual {p0, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 460
    return-void
.end method

.method private static a(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/filemanager/LocalEntry;)V
    .locals 2

    .prologue
    .line 279
    const-string v0, "no.viewer"

    invoke-static {v0, p1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 280
    iget-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/NoViewerDialogFrag;->a(Ljava/lang/String;)Lcom/dropbox/android/activity/dialog/NoViewerDialogFrag;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/NoViewerDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 281
    return-void
.end method

.method private static a(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;)V
    .locals 3

    .prologue
    .line 308
    new-instance v0, Ldbxyzptlk/db231222/g/d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Ldbxyzptlk/db231222/g/d;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/Intent;)V

    .line 309
    invoke-virtual {v0}, Ldbxyzptlk/db231222/g/d;->f()V

    .line 310
    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->a(Ldbxyzptlk/db231222/g/i;)Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 311
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 312
    return-void
.end method

.method public static a(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/util/bs;)V
    .locals 1

    .prologue
    .line 209
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/util/bs;Lcom/dropbox/android/util/analytics/ChainInfo;)V

    .line 210
    return-void
.end method

.method public static a(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/util/bs;Lcom/dropbox/android/util/analytics/ChainInfo;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 215
    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/dropbox/android/filemanager/LocalEntry;->a(Ljava/lang/String;)V

    .line 217
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file://"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 218
    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v0, v3}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    .line 221
    :goto_0
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->p()Ldbxyzptlk/db231222/k/h;

    move-result-object v3

    .line 222
    sget-object v4, Lcom/dropbox/android/util/bs;->a:Lcom/dropbox/android/util/bs;

    if-ne p3, v4, :cond_4

    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v4

    invoke-virtual {v3, v4}, Ldbxyzptlk/db231222/k/h;->a(Lcom/dropbox/android/util/DropboxPath;)Z

    move-result v3

    if-eqz v3, :cond_1

    if-nez v0, :cond_4

    .line 225
    :cond_1
    :goto_1
    if-eqz v2, :cond_5

    invoke-static {p0, p2}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/util/bt;

    move-result-object v1

    .line 227
    :goto_2
    if-eqz v2, :cond_6

    iget-boolean v2, v1, Lcom/dropbox/android/util/bt;->b:Z

    if-nez v2, :cond_2

    iget-boolean v2, v1, Lcom/dropbox/android/util/bt;->a:Z

    if-eqz v2, :cond_6

    .line 228
    :cond_2
    invoke-static {p0, p1, p2, v1, p4}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/util/bt;Lcom/dropbox/android/util/analytics/ChainInfo;)V

    .line 239
    :goto_3
    return-void

    :cond_3
    move v0, v1

    .line 218
    goto :goto_0

    :cond_4
    move v2, v1

    .line 222
    goto :goto_1

    .line 225
    :cond_5
    const/4 v1, 0x0

    goto :goto_2

    .line 229
    :cond_6
    if-eqz v0, :cond_7

    .line 230
    invoke-static {p0, p1, p2}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;)V

    goto :goto_3

    .line 232
    :cond_7
    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 233
    const-string v0, "text/plain"

    invoke-virtual {p2, v0}, Lcom/dropbox/android/filemanager/LocalEntry;->a(Ljava/lang/String;)V

    .line 234
    invoke-static {p0, p1, p2}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;)V

    goto :goto_3

    .line 236
    :cond_8
    invoke-static {p0, p2}, Lcom/dropbox/android/util/UIHelpers;->a(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/filemanager/LocalEntry;)V

    goto :goto_3
.end method

.method private static a(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/util/bt;Lcom/dropbox/android/util/analytics/ChainInfo;)V
    .locals 12

    .prologue
    .line 153
    new-instance v5, Lcom/dropbox/android/util/bo;

    invoke-direct {v5, p0, p1, p2}, Lcom/dropbox/android/util/bo;-><init>(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 168
    iget-boolean v0, p3, Lcom/dropbox/android/util/bt;->a:Z

    if-eqz v0, :cond_0

    .line 173
    new-instance v0, Ldbxyzptlk/db231222/g/as;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v2

    iget-boolean v4, p3, Lcom/dropbox/android/util/bt;->b:Z

    move-object v1, p0

    move-object v3, p2

    move-object/from16 v6, p4

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/g/as;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/filemanager/LocalEntry;ZLdbxyzptlk/db231222/g/ar;Lcom/dropbox/android/util/analytics/ChainInfo;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/as;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 201
    :goto_0
    return-void

    .line 175
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 176
    iget-object v1, p3, Lcom/dropbox/android/util/bt;->c:Landroid/net/Uri;

    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 178
    const v1, 0x7f0d009d

    invoke-virtual {p0, v1}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 179
    new-instance v6, Lcom/dropbox/android/widget/aU;

    const/4 v1, 0x1

    new-array v9, v1, [Landroid/content/Intent;

    const/4 v1, 0x0

    aput-object v0, v9, v1

    const/4 v10, 0x0

    sget-object v11, Lcom/dropbox/android/util/Activities;->a:Lcom/dropbox/android/util/c;

    move-object v7, p0

    invoke-direct/range {v6 .. v11}, Lcom/dropbox/android/widget/aU;-><init>(Landroid/content/Context;Ljava/lang/String;[Landroid/content/Intent;[Landroid/content/Intent;Lcom/dropbox/android/util/c;)V

    .line 181
    new-instance v0, Lcom/dropbox/android/util/bp;

    invoke-direct {v0, p0, p1, p2, v5}, Lcom/dropbox/android/util/bp;-><init>(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;Ldbxyzptlk/db231222/g/ar;)V

    invoke-virtual {v6, v0}, Lcom/dropbox/android/widget/aU;->a(Lcom/dropbox/android/widget/aX;)V

    .line 199
    invoke-virtual {v6}, Lcom/dropbox/android/widget/aU;->a()V

    goto :goto_0
.end method

.method public static a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 473
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/dropbox/android/util/by;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 474
    invoke-virtual {p0, p1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 478
    :goto_0
    return-void

    .line 476
    :cond_0
    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public static a(Landroid/widget/EditText;)V
    .locals 1

    .prologue
    .line 487
    const/16 v0, 0x81

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setInputType(I)V

    .line 488
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 489
    return-void
.end method

.method private static a(II)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 127
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 131
    :cond_0
    :goto_0
    return v0

    .line 130
    :cond_1
    int-to-float v1, p1

    int-to-float v2, p0

    div-float/2addr v1, v2

    .line 131
    const/high16 v2, 0x40400000    # 3.0f

    cmpl-float v2, v1, v2

    if-gez v2, :cond_2

    const v2, 0x3eaaaaab

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 288
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 289
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 290
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 291
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 294
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/high16 v2, 0x10000

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 298
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 299
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 300
    sget-object v3, Lcom/dropbox/android/util/Activities;->a:Lcom/dropbox/android/util/c;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v3, v0}, Lcom/dropbox/android/util/c;->b(Landroid/content/pm/ComponentInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 301
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 304
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Landroid/graphics/Bitmap;)Z
    .locals 2

    .prologue
    .line 146
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/dropbox/android/util/UIHelpers;->a(II)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/graphics/drawable/Drawable;)Z
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 136
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 137
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 138
    if-eq v0, v2, :cond_0

    if-ne v1, v2, :cond_1

    .line 139
    :cond_0
    const/4 v0, 0x0

    .line 141
    :goto_0
    return v0

    :cond_1
    invoke-static {v0, v1}, Lcom/dropbox/android/util/UIHelpers;->a(II)Z

    move-result v0

    goto :goto_0
.end method

.method public static b(Landroid/app/Activity;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 537
    sget-boolean v0, Lcom/dropbox/android/util/UIHelpers;->b:Z

    if-nez v0, :cond_0

    .line 538
    const/4 v0, 0x0

    .line 542
    :goto_0
    return-object v0

    .line 541
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/app/Activity;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "feedback_screenshot.png"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 542
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method
