.class public final Lcom/dropbox/android/util/Z;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 39
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "gmail.com"

    aput-object v1, v0, v3

    const-string v1, "hotmail.com"

    aput-object v1, v0, v4

    const-string v1, "yahoo.com"

    aput-object v1, v0, v5

    const-string v1, "aol.com"

    aput-object v1, v0, v6

    const-string v1, "web.de"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "gmx.de"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "googlemail.com"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "me.com"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "live.com"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "msn.com"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "mail.ru"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "comcast.net"

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/util/Z;->a:[Ljava/lang/String;

    .line 54
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "ymail\\.com"

    aput-object v1, v0, v3

    const-string v1, "yahoo\\.co\\."

    aput-object v1, v0, v4

    const-string v1, "yahoo\\.com\\."

    aput-object v1, v0, v5

    const-string v1, "hotmail\\.co\\."

    aput-object v1, v0, v6

    const-string v1, "hotmail\\.com\\."

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "gmx\\.net"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "gmx\\.at"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "gmx\\.ch"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "mail\\.com"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "web\\.com"

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/util/Z;->b:[Ljava/lang/String;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/dropbox/android/util/aa;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 109
    invoke-static {p0}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 136
    :cond_0
    :goto_0
    return-object v0

    .line 113
    :cond_1
    const-string v2, "@"

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 116
    array-length v3, v2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 120
    aget-object v3, v2, v1

    .line 121
    const/4 v4, 0x1

    aget-object v2, v2, v4

    .line 124
    sget-object v4, Lcom/dropbox/android/util/Z;->b:[Ljava/lang/String;

    array-length v5, v4

    :goto_1
    if-ge v1, v5, :cond_2

    aget-object v6, v4, v1

    .line 125
    invoke-virtual {v2, v6}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 124
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 130
    :cond_2
    invoke-static {v2}, Lcom/dropbox/android/util/Z;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 132
    if-eqz v1, :cond_0

    .line 133
    new-instance v0, Lcom/dropbox/android/util/aa;

    invoke-direct {v0, v3, v1}, Lcom/dropbox/android/util/aa;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v1

    .line 26
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 28
    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v1, v0

    .line 29
    sget-object v5, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    iget-object v6, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 30
    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 28
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 34
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v10, 0x2

    .line 81
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    .line 84
    const/16 v3, 0x64

    .line 87
    sget-object v7, Lcom/dropbox/android/util/Z;->a:[Ljava/lang/String;

    array-length v8, v7

    const/4 v0, 0x0

    move v5, v0

    move-object v1, v4

    :goto_0
    if-ge v5, v8, :cond_0

    aget-object v0, v7, v5

    .line 90
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    sub-int/2addr v2, v9

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-gt v2, v10, :cond_2

    .line 91
    invoke-static {v6, v0}, Ldbxyzptlk/db231222/aa/f;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v2

    .line 92
    if-ge v2, v3, :cond_2

    move v1, v2

    .line 87
    :goto_1
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v3, v1

    move-object v1, v0

    goto :goto_0

    .line 99
    :cond_0
    if-eqz v1, :cond_1

    if-lez v3, :cond_1

    if-gt v3, v10, :cond_1

    .line 103
    :goto_2
    return-object v1

    :cond_1
    move-object v1, v4

    goto :goto_2

    :cond_2
    move-object v0, v1

    move v1, v3

    goto :goto_1
.end method
