.class public final Lcom/dropbox/android/util/X;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final e:[Ljava/lang/String;

.field private static final g:[Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "+"

    aput-object v1, v0, v2

    const-string v1, "reply"

    aput-object v1, v0, v3

    const-string v1, "unsubscribe"

    aput-object v1, v0, v4

    sput-object v0, Lcom/dropbox/android/util/X;->a:[Ljava/lang/String;

    .line 28
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "craigslist"

    aput-object v1, v0, v2

    const-string v1, "jobvite"

    aput-object v1, v0, v3

    sput-object v0, Lcom/dropbox/android/util/X;->c:[Ljava/lang/String;

    .line 31
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "gmail.com"

    aput-object v1, v0, v2

    const-string v1, "yahoo.com"

    aput-object v1, v0, v3

    const-string v1, "hotmail.com"

    aput-object v1, v0, v4

    const-string v1, ".edu"

    aput-object v1, v0, v5

    sput-object v0, Lcom/dropbox/android/util/X;->e:[Ljava/lang/String;

    .line 37
    const/16 v0, 0x126

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "gmail.com"

    aput-object v1, v0, v2

    const-string v1, "hotmail.com"

    aput-object v1, v0, v3

    const-string v1, "yahoo.com"

    aput-object v1, v0, v4

    const-string v1, "aol.com"

    aput-object v1, v0, v5

    const-string v1, "gmx.de"

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string v2, "web.de"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "googlemail.com"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "me.com"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "dropboxanon.com"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "msn.com"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "live.com"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "hotmail.co.uk"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "comcast.net"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "mail.ru"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "yahoo.co.jp"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "163.com"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "hotmail.fr"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "yahoo.com.tw"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "yahoo.co.uk"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "naver.com"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "yahoo.fr"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "hotmail.it"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "yahoo.de"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "gmx.net"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "t-online.de"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "yahoo.es"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "mac.com"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "i.softbank.jp"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "ymail.com"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "sbcglobal.net"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "libero.it"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "hotmail.de"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "mailinator.com"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "btinternet.com"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "qq.com"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "yahoo.com.br"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "verizon.net"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "yandex.ru"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "hotmail.es"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "gmx.at"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "yahoo.com.hk"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "free.fr"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "orange.fr"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "cox.net"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "yahoo.it"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "att.net"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "bluewin.ch"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "live.co.uk"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "live.nl"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "wanadoo.fr"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "ezweb.ne.jp"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "bellsouth.net"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "docomo.ne.jp"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "126.com"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "freenet.de"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "hanmail.net"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "rocketmail.com"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "yahoo.ca"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "arcor.de"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "shaw.ca"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "bigpond.com"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "alice.it"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "yahoo.co.in"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "hotmail.co.jp"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, "wp.pl"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "ns-bbs.com"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, "seznam.cz"

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "live.fr"

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "telenet.be"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, "gmail.com#"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "earthlink.net"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "gmx.ch"

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, "nate.com"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, "softbank.ne.jp"

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "yahoo.com.mx"

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, "mail.com"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, "sina.cn"

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, "tiscali.it"

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "m3com.com.sa"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, "rogers.com"

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "yahoo.com.ar"

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, "live.de"

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "sky.com"

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, "rambler.ru"

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, "laposte.net"

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, "ntlworld.com"

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, "charter.net"

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, "yahoo.com.au"

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, "aim.com"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "yahoo.com.sg"

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, "o2.pl"

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, "skynet.be"

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, "online.de"

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, "uol.com.br"

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, "yopmail.com"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, "yahoo.gr"

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, "home.nl"

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, "live.it"

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, "optonline.net"

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "live.dk"

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, "sympatico.ca"

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, "sapo.pt"

    aput-object v2, v0, v1

    const/16 v1, 0x66

    const-string v2, "trashmail.net"

    aput-object v2, v0, v1

    const/16 v1, 0x67

    const-string v2, "yahoo.co.id"

    aput-object v2, v0, v1

    const/16 v1, 0x68

    const-string v2, "live.ca"

    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, "telia.com"

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    const-string v2, "nifty.com"

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, "planet.nl"

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    const-string v2, "walla.com"

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    const-string v2, "rediffmail.com"

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    const-string v2, "online.no"

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    const-string v2, "telefonica.net"

    aput-object v2, v0, v1

    const/16 v1, 0x70

    const-string v2, "optusnet.com.au"

    aput-object v2, v0, v1

    const/16 v1, 0x71

    const-string v2, "windowslive.com"

    aput-object v2, v0, v1

    const/16 v1, 0x72

    const-string v2, "videotron.ca"

    aput-object v2, v0, v1

    const/16 v1, 0x73

    const-string v2, "telus.net"

    aput-object v2, v0, v1

    const/16 v1, 0x74

    const-string v2, "aon.at"

    aput-object v2, v0, v1

    const/16 v1, 0x75

    const-string v2, "sina.com"

    aput-object v2, v0, v1

    const/16 v1, 0x76

    const-string v2, "gmx.com"

    aput-object v2, v0, v1

    const/16 v1, 0x77

    const-string v2, "live.se"

    aput-object v2, v0, v1

    const/16 v1, 0x78

    const-string v2, "bk.ru"

    aput-object v2, v0, v1

    const/16 v1, 0x79

    const-string v2, "freemail.hu"

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    const-string v2, "abv.bg"

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    const-string v2, "virgilio.it"

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    const-string v2, "terra.com.br"

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    const-string v2, "list.ru"

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    const-string v2, "blueyonder.co.uk"

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    const-string v2, "live.com.mx"

    aput-object v2, v0, v1

    const/16 v1, 0x80

    const-string v2, "yahoo.com.cn"

    aput-object v2, v0, v1

    const/16 v1, 0x81

    const-string v2, "live.com.au"

    aput-object v2, v0, v1

    const/16 v1, 0x82

    const-string v2, "sharklasers.com"

    aput-object v2, v0, v1

    const/16 v1, 0x83

    const-string v2, "xtra.co.nz"

    aput-object v2, v0, v1

    const/16 v1, 0x84

    const-string v2, "talktalk.net"

    aput-object v2, v0, v1

    const/16 v1, 0x85

    const-string v2, "hetnet.nl"

    aput-object v2, v0, v1

    const/16 v1, 0x86

    const-string v2, "yahoo.com.vn"

    aput-object v2, v0, v1

    const/16 v1, 0x87

    const-string v2, "sfr.fr"

    aput-object v2, v0, v1

    const/16 v1, 0x88

    const-string v2, "trash-mail.com"

    aput-object v2, v0, v1

    const/16 v1, 0x89

    const-string v2, "xs4all.nl"

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    const-string v2, "inbox.ru"

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    const-string v2, "terra.es"

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    const-string v2, "ukr.net"

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    const-string v2, "ig.com.br"

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    const-string v2, "ziggo.nl"

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    const-string v2, "tiscali.co.uk"

    aput-object v2, v0, v1

    const/16 v1, 0x90

    const-string v2, "live.jp"

    aput-object v2, v0, v1

    const/16 v1, 0x91

    const-string v2, "sohu.com"

    aput-object v2, v0, v1

    const/16 v1, 0x92

    const-string v2, "tin.it"

    aput-object v2, v0, v1

    const/16 v1, 0x93

    const-string v2, "ya.ru"

    aput-object v2, v0, v1

    const/16 v1, 0x94

    const-string v2, "juno.com"

    aput-object v2, v0, v1

    const/16 v1, 0x95

    const-string v2, "fastwebnet.it"

    aput-object v2, v0, v1

    const/16 v1, 0x96

    const-string v2, "bigpond.net.au"

    aput-object v2, v0, v1

    const/16 v1, 0x97

    const-string v2, "neuf.fr"

    aput-object v2, v0, v1

    const/16 v1, 0x98

    const-string v2, "ybb.ne.jp"

    aput-object v2, v0, v1

    const/16 v1, 0x99

    const-string v2, "kssync.co.cc"

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    const-string v2, "hotmail.ca"

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    const-string v2, "hotmail.com#"

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    const-string v2, "inbox.lv"

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    const-string v2, "msa.hinet.net"

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    const-string v2, "yahoo.dk"

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    const-string v2, "gmai.com"

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    const-string v2, "hotmail.com.tw"

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    const-string v2, "interia.pl"

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    const-string v2, "upcmail.nl"

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    const-string v2, "roadrunner.com"

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    const-string v2, "mail.dk"

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    const-string v2, "live.com.pt"

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    const-string v2, "chello.nl"

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    const-string v2, "yahoo.in"

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    const-string v2, "walla.co.il"

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    const-string v2, "ono.com"

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    const-string v2, "eircom.net"

    aput-object v2, v0, v1

    const/16 v1, 0xab

    const-string v2, "yahoo.com.ph"

    aput-object v2, v0, v1

    const/16 v1, 0xac

    const-string v2, "iinet.net.au"

    aput-object v2, v0, v1

    const/16 v1, 0xad

    const-string v2, "bol.com.br"

    aput-object v2, v0, v1

    const/16 v1, 0xae

    const-string v2, "gamil.com"

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    const-string v2, "hotmail.nl"

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    const-string v2, "umich.edu"

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    const-string v2, "email.it"

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    const-string v2, "op.pl"

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    const-string v2, "gmail.con"

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    const-string v2, "embarqmail.com"

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    const-string v2, "rppkn.com"

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    const-string v2, "nwldx.com"

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    const-string v2, "centrum.cz"

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    const-string v2, "prodigy.net.mx"

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    const-string v2, "rtrtr.com"

    aput-object v2, v0, v1

    const/16 v1, 0xba

    const-string v2, "119797.com"

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    const-string v2, "hp.com"

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    const-string v2, "virgin.net"

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    const-string v2, "kpnmail.nl"

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    const-string v2, "casema.nl"

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    const-string v2, "1yahoo.com"

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    const-string v2, "windstream.net"

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    const-string v2, "outlook.com"

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    const-string v2, "mweb.co.za"

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    const-string v2, "q.com"

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    const-string v2, "btopenworld.com"

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    const-string v2, "yahoo.se"

    aput-object v2, v0, v1

    const/16 v1, 0xc6

    const-string v2, "yahoo.cn"

    aput-object v2, v0, v1

    const/16 v1, 0xc7

    const-string v2, "umn.edu"

    aput-object v2, v0, v1

    const/16 v1, 0xc8

    const-string v2, "cornell.edu"

    aput-object v2, v0, v1

    const/16 v1, 0xc9

    const-string v2, "nepwk.com"

    aput-object v2, v0, v1

    const/16 v1, 0xca

    const-string v2, "google.com"

    aput-object v2, v0, v1

    const/16 v1, 0xcb

    const-string v2, "telkomsa.net"

    aput-object v2, v0, v1

    const/16 v1, 0xcc

    const-string v2, "btconnect.com"

    aput-object v2, v0, v1

    const/16 v1, 0xcd

    const-string v2, "mindspring.com"

    aput-object v2, v0, v1

    const/16 v1, 0xce

    const-string v2, "foxmail.com"

    aput-object v2, v0, v1

    const/16 v1, 0xcf

    const-string v2, "asu.edu"

    aput-object v2, v0, v1

    const/16 v1, 0xd0

    const-string v2, "zonnet.nl"

    aput-object v2, v0, v1

    const/16 v1, 0xd1

    const-string v2, "chello.at"

    aput-object v2, v0, v1

    const/16 v1, 0xd2

    const-string v2, "email.com"

    aput-object v2, v0, v1

    const/16 v1, 0xd3

    const-string v2, "live.no"

    aput-object v2, v0, v1

    const/16 v1, 0xd4

    const-string v2, "klzlk.com"

    aput-object v2, v0, v1

    const/16 v1, 0xd5

    const-string v2, "sunrise.ch"

    aput-object v2, v0, v1

    const/16 v1, 0xd6

    const-string v2, "mail.goo.ne.jp"

    aput-object v2, v0, v1

    const/16 v1, 0xd7

    const-string v2, "usc.edu"

    aput-object v2, v0, v1

    const/16 v1, 0xd8

    const-string v2, "hotmail.se"

    aput-object v2, v0, v1

    const/16 v1, 0xd9

    const-string v2, "live.be"

    aput-object v2, v0, v1

    const/16 v1, 0xda

    const-string v2, "live.cl"

    aput-object v2, v0, v1

    const/16 v1, 0xdb

    const-string v2, "frontier.com"

    aput-object v2, v0, v1

    const/16 v1, 0xdc

    const-string v2, "educities.edu.tw"

    aput-object v2, v0, v1

    const/16 v1, 0xdd

    const-string v2, "voila.fr"

    aput-object v2, v0, v1

    const/16 v1, 0xde

    const-string v2, "hispeed.ch"

    aput-object v2, v0, v1

    const/16 v1, 0xdf

    const-string v2, "cfl.rr.com"

    aput-object v2, v0, v1

    const/16 v1, 0xe0

    const-string v2, "globo.com"

    aput-object v2, v0, v1

    const/16 v1, 0xe1

    const-string v2, "ualberta.ca"

    aput-object v2, v0, v1

    const/16 v1, 0xe2

    const-string v2, "pacbell.net"

    aput-object v2, v0, v1

    const/16 v1, 0xe3

    const-string v2, "daum.net"

    aput-object v2, v0, v1

    const/16 v1, 0xe4

    const-string v2, "columbia.edu"

    aput-object v2, v0, v1

    const/16 v1, 0xe5

    const-string v2, "live.cn"

    aput-object v2, v0, v1

    const/16 v1, 0xe6

    const-string v2, "kpnplanet.nl"

    aput-object v2, v0, v1

    const/16 v1, 0xe7

    const-string v2, "uc.cl"

    aput-object v2, v0, v1

    const/16 v1, 0xe8

    const-string v2, "stanford.edu"

    aput-object v2, v0, v1

    const/16 v1, 0xe9

    const-string v2, "netcabo.pt"

    aput-object v2, v0, v1

    const/16 v1, 0xea

    const-string v2, "nyu.edu"

    aput-object v2, v0, v1

    const/16 v1, 0xeb

    const-string v2, "pchome.com.tw"

    aput-object v2, v0, v1

    const/16 v1, 0xec

    const-string v2, "ufl.edu"

    aput-object v2, v0, v1

    const/16 v1, 0xed

    const-string v2, "jcom.home.ne.jp"

    aput-object v2, v0, v1

    const/16 v1, 0xee

    const-string v2, "purdue.edu"

    aput-object v2, v0, v1

    const/16 v1, 0xef

    const-string v2, "cisco.com"

    aput-object v2, v0, v1

    const/16 v1, 0xf0

    const-string v2, "yahoo.co.kr"

    aput-object v2, v0, v1

    const/16 v1, 0xf1

    const-string v2, "yahoo.no"

    aput-object v2, v0, v1

    const/16 v1, 0xf2

    const-string v2, "tpg.com.au"

    aput-object v2, v0, v1

    const/16 v1, 0xf3

    const-string v2, "virginmedia.com"

    aput-object v2, v0, v1

    const/16 v1, 0xf4

    const-string v2, "email.cz"

    aput-object v2, v0, v1

    const/16 v1, 0xf5

    const-string v2, "quicknet.nl"

    aput-object v2, v0, v1

    const/16 v1, 0xf6

    const-string v2, "illinois.edu"

    aput-object v2, v0, v1

    const/16 v1, 0xf7

    const-string v2, "ovi.com"

    aput-object v2, v0, v1

    const/16 v1, 0xf8

    const-string v2, "psu.edu"

    aput-object v2, v0, v1

    const/16 v1, 0xf9

    const-string v2, "singnet.com.sg"

    aput-object v2, v0, v1

    const/16 v1, 0xfa

    const-string v2, "online.nl"

    aput-object v2, v0, v1

    const/16 v1, 0xfb

    const-string v2, "mit.edu"

    aput-object v2, v0, v1

    const/16 v1, 0xfc

    const-string v2, "tlen.pl"

    aput-object v2, v0, v1

    const/16 v1, 0xfd

    const-string v2, "tampabay.rr.com"

    aput-object v2, v0, v1

    const/16 v1, 0xfe

    const-string v2, "xtec.cat"

    aput-object v2, v0, v1

    const/16 v1, 0xff

    const-string v2, "hotmail.con"

    aput-object v2, v0, v1

    const/16 v1, 0x100

    const-string v2, "inwind.it"

    aput-object v2, v0, v1

    const/16 v1, 0x101

    const-string v2, "msu.edu"

    aput-object v2, v0, v1

    const/16 v1, 0x102

    const-string v2, "citromail.hu"

    aput-object v2, v0, v1

    const/16 v1, 0x103

    const-string v2, "utoronto.ca"

    aput-object v2, v0, v1

    const/16 v1, 0x104

    const-string v2, "cogeco.ca"

    aput-object v2, v0, v1

    const/16 v1, 0x105

    const-string v2, "live.com.ar"

    aput-object v2, v0, v1

    const/16 v1, 0x106

    const-string v2, "microsoft.com"

    aput-object v2, v0, v1

    const/16 v1, 0x107

    const-string v2, "ncsu.edu"

    aput-object v2, v0, v1

    const/16 v1, 0x108

    const-string v2, "indiana.edu"

    aput-object v2, v0, v1

    const/16 v1, 0x109

    const-string v2, "yeah.net"

    aput-object v2, v0, v1

    const/16 v1, 0x10a

    const-string v2, "yahoo.com#"

    aput-object v2, v0, v1

    const/16 v1, 0x10b

    const-string v2, "jnxjn.com"

    aput-object v2, v0, v1

    const/16 v1, 0x10c

    const-string v2, "paran.com"

    aput-object v2, v0, v1

    const/16 v1, 0x10d

    const-string v2, "insightbb.com"

    aput-object v2, v0, v1

    const/16 v1, 0x10e

    const-string v2, "mchsi.com"

    aput-object v2, v0, v1

    const/16 v1, 0x10f

    const-string v2, "vt.edu"

    aput-object v2, v0, v1

    const/16 v1, 0x110

    const-string v2, "colorado.edu"

    aput-object v2, v0, v1

    const/16 v1, 0x111

    const-string v2, "poczta.onet.pl"

    aput-object v2, v0, v1

    const/16 v1, 0x112

    const-string v2, "kokinama.ddo.jp"

    aput-object v2, v0, v1

    const/16 v1, 0x113

    const-string v2, "mail.mcgill.ca"

    aput-object v2, v0, v1

    const/16 v1, 0x114

    const-string v2, "berkeley.edu"

    aput-object v2, v0, v1

    const/16 v1, 0x115

    const-string v2, "ntu.edu.tw"

    aput-object v2, v0, v1

    const/16 v1, 0x116

    const-string v2, "lycos.com"

    aput-object v2, v0, v1

    const/16 v1, 0x117

    const-string v2, "netscape.net"

    aput-object v2, v0, v1

    const/16 v1, 0x118

    const-string v2, "talk21.com"

    aput-object v2, v0, v1

    const/16 v1, 0x119

    const-string v2, "inbox.com"

    aput-object v2, v0, v1

    const/16 v1, 0x11a

    const-string v2, "yahoo.ie"

    aput-object v2, v0, v1

    const/16 v1, 0x11b

    const-string v2, "hotmai.com"

    aput-object v2, v0, v1

    const/16 v1, 0x11c

    const-string v2, "wisc.edu"

    aput-object v2, v0, v1

    const/16 v1, 0x11d

    const-string v2, "suddenlink.net"

    aput-object v2, v0, v1

    const/16 v1, 0x11e

    const-string v2, "livemail.tw"

    aput-object v2, v0, v1

    const/16 v1, 0x11f

    const-string v2, "facebook.com"

    aput-object v2, v0, v1

    const/16 v1, 0x120

    const-string v2, "nc.rr.com"

    aput-object v2, v0, v1

    const/16 v1, 0x121

    const-string v2, "club-internet.fr"

    aput-object v2, v0, v1

    const/16 v1, 0x122

    const-string v2, "virginia.edu"

    aput-object v2, v0, v1

    const/16 v1, 0x123

    const-string v2, "bredband.net"

    aput-object v2, v0, v1

    const/16 v1, 0x124

    const-string v2, "hotmail.be"

    aput-object v2, v0, v1

    const/16 v1, 0x125

    const-string v2, "internode.on.net"

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/util/X;->g:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 335
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 336
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/util/X;->b:Ljava/util/Set;

    .line 337
    iget-object v0, p0, Lcom/dropbox/android/util/X;->b:Ljava/util/Set;

    sget-object v1, Lcom/dropbox/android/util/X;->a:[Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 338
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/util/X;->d:Ljava/util/Set;

    .line 339
    iget-object v0, p0, Lcom/dropbox/android/util/X;->d:Ljava/util/Set;

    sget-object v1, Lcom/dropbox/android/util/X;->c:[Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 340
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/util/X;->f:Ljava/util/Set;

    .line 341
    iget-object v0, p0, Lcom/dropbox/android/util/X;->f:Ljava/util/Set;

    sget-object v1, Lcom/dropbox/android/util/X;->e:[Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 342
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/util/X;->h:Ljava/util/Set;

    .line 343
    iget-object v0, p0, Lcom/dropbox/android/util/X;->h:Ljava/util/Set;

    sget-object v1, Lcom/dropbox/android/util/X;->g:[Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 344
    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/util/Y;)Z
    .locals 2

    .prologue
    .line 352
    iget-object v0, p0, Lcom/dropbox/android/util/X;->b:Ljava/util/Set;

    iget-object v1, p1, Lcom/dropbox/android/util/Y;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/util/X;->d:Ljava/util/Set;

    iget-object v1, p1, Lcom/dropbox/android/util/Y;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 347
    new-instance v0, Lcom/dropbox/android/util/Y;

    invoke-direct {v0, p1}, Lcom/dropbox/android/util/Y;-><init>(Ljava/lang/String;)V

    .line 348
    invoke-virtual {p0, v0}, Lcom/dropbox/android/util/X;->a(Lcom/dropbox/android/util/Y;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/dropbox/android/util/Y;)Z
    .locals 2

    .prologue
    .line 361
    iget-object v0, p0, Lcom/dropbox/android/util/X;->f:Ljava/util/Set;

    iget-object v1, p1, Lcom/dropbox/android/util/Y;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c(Lcom/dropbox/android/util/Y;)Z
    .locals 2

    .prologue
    .line 370
    iget-object v0, p0, Lcom/dropbox/android/util/X;->h:Ljava/util/Set;

    iget-object v1, p1, Lcom/dropbox/android/util/Y;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
