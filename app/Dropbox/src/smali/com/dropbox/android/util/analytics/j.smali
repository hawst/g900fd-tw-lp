.class final Lcom/dropbox/android/util/analytics/j;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:[Ljava/lang/String;

.field private b:I


# direct methods
.method constructor <init>(I)V
    .locals 1

    .prologue
    .line 1507
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1505
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/util/analytics/j;->b:I

    .line 1508
    new-array v0, p1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/dropbox/android/util/analytics/j;->a:[Ljava/lang/String;

    .line 1509
    return-void
.end method


# virtual methods
.method final a()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1519
    iget-object v2, p0, Lcom/dropbox/android/util/analytics/j;->a:[Ljava/lang/String;

    monitor-enter v2

    .line 1520
    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/dropbox/android/util/analytics/j;->a:[Ljava/lang/String;

    array-length v0, v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1521
    iget v1, p0, Lcom/dropbox/android/util/analytics/j;->b:I

    .line 1522
    const/4 v0, 0x0

    :goto_0
    iget-object v4, p0, Lcom/dropbox/android/util/analytics/j;->a:[Ljava/lang/String;

    array-length v4, v4

    if-ge v0, v4, :cond_1

    .line 1523
    iget-object v4, p0, Lcom/dropbox/android/util/analytics/j;->a:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 1524
    if-eqz v4, :cond_0

    .line 1525
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1527
    :cond_0
    add-int/lit8 v1, v1, 0x1

    iget-object v4, p0, Lcom/dropbox/android/util/analytics/j;->a:[Ljava/lang/String;

    array-length v4, v4

    rem-int/2addr v1, v4

    .line 1522
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1529
    :cond_1
    monitor-exit v2

    return-object v3

    .line 1530
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1512
    iget-object v1, p0, Lcom/dropbox/android/util/analytics/j;->a:[Ljava/lang/String;

    monitor-enter v1

    .line 1513
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/util/analytics/j;->a:[Ljava/lang/String;

    iget v2, p0, Lcom/dropbox/android/util/analytics/j;->b:I

    aput-object p1, v0, v2

    .line 1514
    iget v0, p0, Lcom/dropbox/android/util/analytics/j;->b:I

    add-int/lit8 v0, v0, 0x1

    iget-object v2, p0, Lcom/dropbox/android/util/analytics/j;->a:[Ljava/lang/String;

    array-length v2, v2

    rem-int/2addr v0, v2

    iput v0, p0, Lcom/dropbox/android/util/analytics/j;->b:I

    .line 1515
    monitor-exit v1

    .line 1516
    return-void

    .line 1515
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
