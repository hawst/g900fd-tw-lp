.class public Lcom/dropbox/android/util/q;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final i:Ljava/lang/Object;


# instance fields
.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private final f:[Ljava/lang/Object;

.field private final g:Lcom/dropbox/android/util/ao;

.field private final h:Lcom/dropbox/android/util/ao;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/dropbox/android/util/q;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/util/q;->a:Ljava/lang/String;

    .line 26
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/q;->i:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput v2, p0, Lcom/dropbox/android/util/q;->b:I

    .line 14
    iput v2, p0, Lcom/dropbox/android/util/q;->c:I

    .line 16
    iput v2, p0, Lcom/dropbox/android/util/q;->d:I

    .line 17
    iput v2, p0, Lcom/dropbox/android/util/q;->e:I

    .line 20
    new-instance v0, Lcom/dropbox/android/util/ao;

    invoke-direct {v0}, Lcom/dropbox/android/util/ao;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/util/q;->g:Lcom/dropbox/android/util/ao;

    .line 21
    new-instance v0, Lcom/dropbox/android/util/ao;

    invoke-direct {v0}, Lcom/dropbox/android/util/ao;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/util/q;->h:Lcom/dropbox/android/util/ao;

    .line 29
    iput p1, p0, Lcom/dropbox/android/util/q;->b:I

    .line 30
    iget v0, p0, Lcom/dropbox/android/util/q;->b:I

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    .line 31
    iput p2, p0, Lcom/dropbox/android/util/q;->c:I

    .line 34
    if-eqz p1, :cond_0

    .line 35
    iput v2, p0, Lcom/dropbox/android/util/q;->d:I

    .line 36
    iput v2, p0, Lcom/dropbox/android/util/q;->e:I

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/util/q;->h:Lcom/dropbox/android/util/ao;

    iget v1, p0, Lcom/dropbox/android/util/q;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v2, v1}, Lcom/dropbox/android/util/ao;->a(II)V

    .line 40
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 147
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/util/q;->g:Lcom/dropbox/android/util/ao;

    invoke-virtual {v0}, Lcom/dropbox/android/util/ao;->c()I

    move-result v0

    iget v1, p0, Lcom/dropbox/android/util/q;->c:I

    add-int/lit8 v1, v1, 0x1

    if-le v0, v1, :cond_3

    .line 148
    invoke-direct {p0}, Lcom/dropbox/android/util/q;->e()Lcom/dropbox/android/util/r;

    move-result-object v0

    .line 149
    iget v1, v0, Lcom/dropbox/android/util/r;->a:I

    iget-object v2, p0, Lcom/dropbox/android/util/q;->g:Lcom/dropbox/android/util/ao;

    invoke-virtual {v2}, Lcom/dropbox/android/util/ao;->a()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iget v0, v0, Lcom/dropbox/android/util/r;->b:I

    iget-object v2, p0, Lcom/dropbox/android/util/q;->g:Lcom/dropbox/android/util/ao;

    invoke-virtual {v2}, Lcom/dropbox/android/util/ao;->b()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-lt v1, v0, :cond_1

    const/4 v0, 0x1

    .line 150
    :goto_1
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/dropbox/android/util/q;->g:Lcom/dropbox/android/util/ao;

    invoke-virtual {v0}, Lcom/dropbox/android/util/ao;->a()I

    move-result v0

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 152
    iget-object v0, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aget-object v0, v0, v2

    instance-of v0, v0, Lcom/dropbox/android/util/n;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aget-object v0, v0, v2

    check-cast v0, Lcom/dropbox/android/util/n;

    invoke-virtual {v0}, Lcom/dropbox/android/util/n;->b()V

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x0

    aput-object v3, v0, v2

    .line 156
    iget-object v0, p0, Lcom/dropbox/android/util/q;->g:Lcom/dropbox/android/util/ao;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/dropbox/android/util/ao;->c(I)V

    .line 157
    iget-object v0, p0, Lcom/dropbox/android/util/q;->h:Lcom/dropbox/android/util/ao;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/ao;->d(I)V

    goto :goto_0

    .line 149
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 150
    :cond_2
    iget-object v0, p0, Lcom/dropbox/android/util/q;->g:Lcom/dropbox/android/util/ao;

    invoke-virtual {v0}, Lcom/dropbox/android/util/ao;->b()I

    move-result v0

    goto :goto_2

    .line 159
    :cond_3
    return-void
.end method

.method private e()Lcom/dropbox/android/util/r;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 216
    iget v0, p0, Lcom/dropbox/android/util/q;->d:I

    iget v1, p0, Lcom/dropbox/android/util/q;->c:I

    iget v2, p0, Lcom/dropbox/android/util/q;->e:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 217
    iget v1, p0, Lcom/dropbox/android/util/q;->c:I

    add-int/lit8 v1, v1, -0x1

    add-int/2addr v1, v0

    iget v2, p0, Lcom/dropbox/android/util/q;->b:I

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 218
    iget v2, p0, Lcom/dropbox/android/util/q;->b:I

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_0

    .line 222
    iget v0, p0, Lcom/dropbox/android/util/q;->c:I

    add-int/lit8 v0, v0, -0x1

    sub-int v0, v1, v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 225
    :cond_0
    new-instance v2, Lcom/dropbox/android/util/r;

    invoke-direct {v2, v0, v1}, Lcom/dropbox/android/util/r;-><init>(II)V

    return-object v2
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 58
    iget-object v0, p0, Lcom/dropbox/android/util/q;->g:Lcom/dropbox/android/util/ao;

    invoke-virtual {v0}, Lcom/dropbox/android/util/ao;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 59
    iget-object v1, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget-object v1, v1, v3

    instance-of v1, v1, Lcom/dropbox/android/util/n;

    if-eqz v1, :cond_0

    .line 60
    iget-object v1, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget-object v1, v1, v3

    check-cast v1, Lcom/dropbox/android/util/n;

    invoke-virtual {v1}, Lcom/dropbox/android/util/n;->b()V

    .line 62
    :cond_0
    iget-object v1, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v3, 0x0

    aput-object v3, v1, v0

    goto :goto_0

    .line 64
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/util/q;->h:Lcom/dropbox/android/util/ao;

    iget-object v1, p0, Lcom/dropbox/android/util/q;->g:Lcom/dropbox/android/util/ao;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/ao;->a(Ljava/lang/Iterable;)V

    .line 65
    iget-object v0, p0, Lcom/dropbox/android/util/q;->g:Lcom/dropbox/android/util/ao;

    invoke-virtual {v0}, Lcom/dropbox/android/util/ao;->d()V

    .line 66
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 51
    iput p1, p0, Lcom/dropbox/android/util/q;->c:I

    .line 52
    invoke-direct {p0}, Lcom/dropbox/android/util/q;->d()V

    .line 53
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/dropbox/android/util/q;->c:I

    if-le p2, v0, :cond_0

    .line 73
    iput p2, p0, Lcom/dropbox/android/util/q;->c:I

    .line 75
    :cond_0
    iput p1, p0, Lcom/dropbox/android/util/q;->d:I

    .line 76
    iput p2, p0, Lcom/dropbox/android/util/q;->e:I

    .line 77
    return-void
.end method

.method public final a(ILcom/dropbox/android/util/n;)V
    .locals 0

    .prologue
    .line 84
    invoke-virtual {p2}, Lcom/dropbox/android/util/n;->a()V

    .line 85
    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/util/q;->a(ILjava/lang/Object;)V

    .line 86
    return-void
.end method

.method protected final a(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    aput-object p2, v0, p1

    .line 91
    iget-object v0, p0, Lcom/dropbox/android/util/q;->g:Lcom/dropbox/android/util/ao;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/ao;->d(I)V

    .line 92
    iget-object v0, p0, Lcom/dropbox/android/util/q;->h:Lcom/dropbox/android/util/ao;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/ao;->c(I)V

    .line 104
    :goto_0
    invoke-direct {p0}, Lcom/dropbox/android/util/q;->d()V

    .line 105
    return-void

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    aget-object v0, v0, p1

    instance-of v0, v0, Lcom/dropbox/android/util/n;

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    aget-object v0, v0, p1

    check-cast v0, Lcom/dropbox/android/util/n;

    check-cast v0, Lcom/dropbox/android/util/n;

    invoke-virtual {v0}, Lcom/dropbox/android/util/n;->b()V

    .line 96
    iget-object v0, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    aput-object p2, v0, p1

    goto :goto_0

    .line 97
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    aget-object v0, v0, p1

    sget-object v1, Lcom/dropbox/android/util/q;->i:Ljava/lang/Object;

    if-ne v0, v1, :cond_2

    .line 99
    iget-object v0, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    aput-object p2, v0, p1

    goto :goto_0

    .line 101
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "how did a non node or bitmap get in here!?"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()I
    .locals 6

    .prologue
    const/4 v2, -0x1

    .line 163
    iget-object v0, p0, Lcom/dropbox/android/util/q;->h:Lcom/dropbox/android/util/ao;

    iget v1, p0, Lcom/dropbox/android/util/q;->d:I

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/ao;->a(I)Ljava/lang/Integer;

    move-result-object v0

    .line 164
    iget v1, p0, Lcom/dropbox/android/util/q;->d:I

    iget v3, p0, Lcom/dropbox/android/util/q;->e:I

    add-int/2addr v3, v1

    .line 166
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gt v1, v3, :cond_1

    .line 167
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 200
    :cond_0
    :goto_0
    return v2

    .line 171
    :cond_1
    iget-object v0, p0, Lcom/dropbox/android/util/q;->h:Lcom/dropbox/android/util/ao;

    iget v1, p0, Lcom/dropbox/android/util/q;->d:I

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/ao;->b(I)Ljava/lang/Integer;

    move-result-object v0

    .line 172
    iget-object v1, p0, Lcom/dropbox/android/util/q;->h:Lcom/dropbox/android/util/ao;

    invoke-virtual {v1, v3}, Lcom/dropbox/android/util/ao;->a(I)Ljava/lang/Integer;

    move-result-object v1

    .line 173
    iget-object v4, p0, Lcom/dropbox/android/util/q;->g:Lcom/dropbox/android/util/ao;

    invoke-virtual {v4}, Lcom/dropbox/android/util/ao;->c()I

    move-result v4

    iget v5, p0, Lcom/dropbox/android/util/q;->c:I

    if-ge v4, v5, :cond_5

    .line 174
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    .line 175
    iget v2, p0, Lcom/dropbox/android/util/q;->d:I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sub-int/2addr v2, v4

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sub-int v3, v4, v3

    if-ge v2, v3, :cond_2

    :goto_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1

    .line 176
    :cond_3
    if-eqz v0, :cond_4

    .line 177
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_0

    .line 178
    :cond_4
    if-eqz v1, :cond_0

    .line 179
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_0

    .line 185
    :cond_5
    if-eqz v0, :cond_9

    if-eqz v1, :cond_9

    .line 186
    iget v4, p0, Lcom/dropbox/android/util/q;->d:I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    sub-int v3, v5, v3

    if-ge v4, v3, :cond_8

    :goto_2
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 194
    :goto_3
    if-eq v0, v2, :cond_7

    .line 195
    invoke-direct {p0}, Lcom/dropbox/android/util/q;->e()Lcom/dropbox/android/util/r;

    move-result-object v1

    .line 196
    iget v3, v1, Lcom/dropbox/android/util/r;->a:I

    if-lt v0, v3, :cond_6

    iget v1, v1, Lcom/dropbox/android/util/r;->b:I

    if-le v0, v1, :cond_7

    :cond_6
    move v0, v2

    :cond_7
    move v2, v0

    .line 200
    goto :goto_0

    :cond_8
    move-object v0, v1

    .line 186
    goto :goto_2

    .line 187
    :cond_9
    if-eqz v0, :cond_a

    .line 188
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_3

    .line 189
    :cond_a
    if-eqz v1, :cond_b

    .line 190
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_3

    :cond_b
    move v0, v2

    .line 192
    goto :goto_3
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lcom/dropbox/android/util/q;->i:Ljava/lang/Object;

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/util/q;->a(ILjava/lang/Object;)V

    .line 81
    return-void
.end method

.method public final c()Landroid/util/SparseArray;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/dropbox/android/util/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 233
    new-instance v1, Landroid/util/SparseArray;

    iget-object v0, p0, Lcom/dropbox/android/util/q;->g:Lcom/dropbox/android/util/ao;

    invoke-virtual {v0}, Lcom/dropbox/android/util/ao;->c()I

    move-result v0

    invoke-direct {v1, v0}, Landroid/util/SparseArray;-><init>(I)V

    .line 234
    iget-object v0, p0, Lcom/dropbox/android/util/q;->g:Lcom/dropbox/android/util/ao;

    invoke-virtual {v0}, Lcom/dropbox/android/util/ao;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 235
    iget-object v0, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    aget-object v0, v0, v3

    instance-of v0, v0, Lcom/dropbox/android/util/n;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    aget-object v0, v0, v3

    check-cast v0, Lcom/dropbox/android/util/n;

    invoke-virtual {v0}, Lcom/dropbox/android/util/n;->a()V

    .line 237
    iget-object v0, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    aget-object v0, v0, v3

    check-cast v0, Lcom/dropbox/android/util/n;

    invoke-virtual {v1, v3, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_0

    .line 240
    :cond_1
    return-object v1
.end method

.method public final c(I)Lcom/dropbox/android/util/n;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    aget-object v0, v0, p1

    instance-of v0, v0, Lcom/dropbox/android/util/n;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    aget-object v0, v0, p1

    check-cast v0, Lcom/dropbox/android/util/n;

    .line 111
    invoke-virtual {v0}, Lcom/dropbox/android/util/n;->a()V

    .line 114
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(I)Z
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    aget-object v0, v0, p1

    instance-of v0, v0, Lcom/dropbox/android/util/n;

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 124
    const-string v1, "["

    .line 125
    const/4 v0, 0x0

    move v5, v0

    move-object v0, v1

    move v1, v5

    :goto_0
    iget-object v2, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 126
    if-lez v1, :cond_0

    .line 127
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 130
    :cond_0
    iget-object v2, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    aget-object v2, v2, v1

    if-nez v2, :cond_1

    .line 131
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": null"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 125
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 132
    :cond_1
    iget-object v2, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    aget-object v2, v2, v1

    instance-of v2, v2, Lcom/dropbox/android/util/n;

    if-eqz v2, :cond_2

    .line 133
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x40

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 135
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/dropbox/android/util/q;->f:[Ljava/lang/Object;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 138
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 139
    return-object v0
.end method
