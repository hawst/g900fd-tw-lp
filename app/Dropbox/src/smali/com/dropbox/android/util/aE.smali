.class public Lcom/dropbox/android/util/aE;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ldbxyzptlk/db231222/V/y;

.field private final c:Ljava/io/File;

.field private final d:Ldbxyzptlk/db231222/Q/r;

.field private e:Ldbxyzptlk/db231222/Q/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/dropbox/android/util/aE;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/util/aE;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/io/File;)V
    .locals 3

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ldbxyzptlk/db231222/Q/r;

    invoke-direct {v0}, Ldbxyzptlk/db231222/Q/r;-><init>()V

    const-string v1, "http/1.1"

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/Q/r;->a(Ljava/util/List;)Ldbxyzptlk/db231222/Q/r;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/util/aE;->d:Ldbxyzptlk/db231222/Q/r;

    .line 50
    iput-object p2, p0, Lcom/dropbox/android/util/aE;->c:Ljava/io/File;

    .line 51
    invoke-direct {p0}, Lcom/dropbox/android/util/aE;->f()V

    .line 52
    new-instance v0, Ldbxyzptlk/db231222/V/A;

    invoke-direct {v0, p1}, Ldbxyzptlk/db231222/V/A;-><init>(Landroid/content/Context;)V

    new-instance v1, Ldbxyzptlk/db231222/V/x;

    iget-object v2, p0, Lcom/dropbox/android/util/aE;->d:Ldbxyzptlk/db231222/Q/r;

    invoke-direct {v1, v2}, Ldbxyzptlk/db231222/V/x;-><init>(Ldbxyzptlk/db231222/Q/r;)V

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/V/A;->a(Ldbxyzptlk/db231222/V/q;)Ldbxyzptlk/db231222/V/A;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/V/A;->a()Ldbxyzptlk/db231222/V/y;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/util/aE;->b:Ldbxyzptlk/db231222/V/y;

    .line 55
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 88
    iget-object v0, p0, Lcom/dropbox/android/util/aE;->e:Ldbxyzptlk/db231222/Q/f;

    if-eqz v0, :cond_0

    .line 90
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/util/aE;->e:Ldbxyzptlk/db231222/Q/f;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/f;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 91
    :catch_0
    move-exception v0

    .line 92
    sget-object v1, Lcom/dropbox/android/util/aE;->a:Ljava/lang/String;

    const-string v2, "Failed to delete misc thumb cache"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 99
    :try_start_0
    new-instance v0, Ldbxyzptlk/db231222/Q/f;

    iget-object v1, p0, Lcom/dropbox/android/util/aE;->c:Ljava/io/File;

    const-wide/32 v2, 0x6400000

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/Q/f;-><init>(Ljava/io/File;J)V

    iput-object v0, p0, Lcom/dropbox/android/util/aE;->e:Ldbxyzptlk/db231222/Q/f;

    .line 100
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bI()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "success"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/util/aE;->d:Ldbxyzptlk/db231222/Q/r;

    iget-object v1, p0, Lcom/dropbox/android/util/aE;->e:Ldbxyzptlk/db231222/Q/f;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/Q/r;->a(Ljava/net/ResponseCache;)Ldbxyzptlk/db231222/Q/r;

    .line 108
    return-void

    .line 101
    :catch_0
    move-exception v0

    .line 103
    sget-object v1, Lcom/dropbox/android/util/aE;->a:Ljava/lang/String;

    const-string v2, "Failed to create misc thumb cache"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 104
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bI()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "success"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/util/aE;->e:Ldbxyzptlk/db231222/Q/f;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ldbxyzptlk/db231222/V/y;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/dropbox/android/util/aE;->b:Ldbxyzptlk/db231222/V/y;

    return-object v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/dropbox/android/util/aE;->c:Ljava/io/File;

    invoke-static {v0}, Ldbxyzptlk/db231222/k/a;->a(Ljava/io/File;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/dropbox/android/util/aE;->e()V

    .line 76
    invoke-direct {p0}, Lcom/dropbox/android/util/aE;->f()V

    .line 77
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/dropbox/android/util/aE;->e()V

    .line 84
    iget-object v0, p0, Lcom/dropbox/android/util/aE;->b:Ldbxyzptlk/db231222/V/y;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/V/y;->a()V

    .line 85
    return-void
.end method
