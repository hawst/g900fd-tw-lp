.class public final Lcom/dropbox/android/util/i;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static d:Lcom/dropbox/android/util/m;

.field private static e:J

.field private static final f:Ljava/lang/Object;

.field private static final g:Lcom/dropbox/android/util/k;


# instance fields
.field private a:Lcom/dropbox/android/util/m;

.field private final b:Lcom/dropbox/android/util/l;

.field private final c:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 119
    new-instance v0, Lcom/dropbox/android/util/m;

    invoke-direct {v0, v1, v1}, Lcom/dropbox/android/util/m;-><init>(ZZ)V

    sput-object v0, Lcom/dropbox/android/util/i;->d:Lcom/dropbox/android/util/m;

    .line 120
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/dropbox/android/util/i;->e:J

    .line 121
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/i;->f:Ljava/lang/Object;

    .line 205
    new-instance v0, Lcom/dropbox/android/util/k;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/k;-><init>(Lcom/dropbox/android/util/j;)V

    sput-object v0, Lcom/dropbox/android/util/i;->g:Lcom/dropbox/android/util/k;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/util/l;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    new-instance v0, Lcom/dropbox/android/util/m;

    invoke-direct {v0, v1, v1}, Lcom/dropbox/android/util/m;-><init>(ZZ)V

    iput-object v0, p0, Lcom/dropbox/android/util/i;->a:Lcom/dropbox/android/util/m;

    .line 86
    iput-object p2, p0, Lcom/dropbox/android/util/i;->b:Lcom/dropbox/android/util/l;

    .line 87
    new-instance v0, Lcom/dropbox/android/util/j;

    invoke-direct {v0, p0}, Lcom/dropbox/android/util/j;-><init>(Lcom/dropbox/android/util/i;)V

    iput-object v0, p0, Lcom/dropbox/android/util/i;->c:Landroid/content/BroadcastReceiver;

    .line 101
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/util/i;->c:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    .line 103
    if-eqz v0, :cond_0

    .line 104
    invoke-static {v0}, Lcom/dropbox/android/util/i;->b(Landroid/content/Intent;)Lcom/dropbox/android/util/m;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/util/i;->a(Lcom/dropbox/android/util/m;)V

    .line 106
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/dropbox/android/util/m;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 131
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 132
    sget-object v3, Lcom/dropbox/android/util/i;->f:Ljava/lang/Object;

    monitor-enter v3

    .line 133
    :try_start_0
    sget-wide v4, Lcom/dropbox/android/util/i;->e:J

    sub-long v4, v1, v4

    const-wide/16 v6, 0x64

    cmp-long v0, v4, v6

    if-gtz v0, :cond_0

    .line 134
    sget-object v0, Lcom/dropbox/android/util/i;->d:Lcom/dropbox/android/util/m;

    monitor-exit v3

    .line 149
    :goto_0
    return-object v0

    .line 136
    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 137
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v3, 0x0

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    .line 140
    if-eqz v0, :cond_1

    .line 141
    invoke-static {v0}, Lcom/dropbox/android/util/i;->b(Landroid/content/Intent;)Lcom/dropbox/android/util/m;

    move-result-object v0

    .line 145
    :goto_1
    sget-object v3, Lcom/dropbox/android/util/i;->f:Ljava/lang/Object;

    monitor-enter v3

    .line 146
    :try_start_1
    sput-object v0, Lcom/dropbox/android/util/i;->d:Lcom/dropbox/android/util/m;

    .line 147
    sput-wide v1, Lcom/dropbox/android/util/i;->e:J

    .line 148
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 136
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 143
    :cond_1
    new-instance v0, Lcom/dropbox/android/util/m;

    invoke-direct {v0, v8, v8}, Lcom/dropbox/android/util/m;-><init>(ZZ)V

    goto :goto_1
.end method

.method static synthetic a(Landroid/content/Intent;)Lcom/dropbox/android/util/m;
    .locals 1

    .prologue
    .line 17
    invoke-static {p0}, Lcom/dropbox/android/util/i;->b(Landroid/content/Intent;)Lcom/dropbox/android/util/m;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/util/i;)Lcom/dropbox/android/util/m;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/dropbox/android/util/i;->a:Lcom/dropbox/android/util/m;

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/util/i;Lcom/dropbox/android/util/m;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/dropbox/android/util/i;->a(Lcom/dropbox/android/util/m;)V

    return-void
.end method

.method private a(Lcom/dropbox/android/util/m;)V
    .locals 4

    .prologue
    .line 153
    monitor-enter p0

    .line 154
    :try_start_0
    iput-object p1, p0, Lcom/dropbox/android/util/i;->a:Lcom/dropbox/android/util/m;

    .line 155
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    sget-object v1, Lcom/dropbox/android/util/i;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 157
    :try_start_1
    sput-object p1, Lcom/dropbox/android/util/i;->d:Lcom/dropbox/android/util/m;

    .line 158
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sput-wide v2, Lcom/dropbox/android/util/i;->e:J

    .line 159
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 160
    return-void

    .line 155
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 159
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method static synthetic b(Lcom/dropbox/android/util/i;)Lcom/dropbox/android/util/l;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/dropbox/android/util/i;->b:Lcom/dropbox/android/util/l;

    return-object v0
.end method

.method private static b(Landroid/content/Intent;)Lcom/dropbox/android/util/m;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v4, -0x1

    const/4 v2, 0x0

    .line 163
    const-string v0, "level"

    invoke-virtual {p0, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 164
    const-string v3, "scale"

    invoke-virtual {p0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 165
    if-eq v0, v4, :cond_0

    if-ne v3, v4, :cond_1

    .line 166
    :cond_0
    new-instance v0, Lcom/dropbox/android/util/m;

    invoke-direct {v0, v2, v2}, Lcom/dropbox/android/util/m;-><init>(ZZ)V

    .line 174
    :goto_0
    return-object v0

    .line 168
    :cond_1
    int-to-double v4, v0

    int-to-double v6, v3

    div-double v6, v4, v6

    .line 169
    const-string v0, "plugged"

    invoke-virtual {p0, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 171
    :goto_1
    sget-object v3, Lcom/dropbox/android/util/i;->g:Lcom/dropbox/android/util/k;

    invoke-virtual {v3, v6, v7, v0}, Lcom/dropbox/android/util/k;->a(DZ)V

    .line 173
    if-eqz v0, :cond_3

    const-wide v3, 0x3fb999999999999aL    # 0.1

    .line 174
    :goto_2
    new-instance v5, Lcom/dropbox/android/util/m;

    cmpg-double v3, v6, v3

    if-gtz v3, :cond_4

    :goto_3
    invoke-direct {v5, v1, v0}, Lcom/dropbox/android/util/m;-><init>(ZZ)V

    move-object v0, v5

    goto :goto_0

    :cond_2
    move v0, v2

    .line 169
    goto :goto_1

    .line 173
    :cond_3
    const-wide v3, 0x3fd3333333333333L    # 0.3

    goto :goto_2

    :cond_4
    move v1, v2

    .line 174
    goto :goto_3
.end method
