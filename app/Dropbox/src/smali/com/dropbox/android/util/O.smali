.class final Lcom/dropbox/android/util/O;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/dropbox/android/util/L;


# direct methods
.method private constructor <init>(Lcom/dropbox/android/util/L;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/dropbox/android/util/O;->a:Lcom/dropbox/android/util/L;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/android/util/L;Lcom/dropbox/android/util/M;)V
    .locals 0

    .prologue
    .line 120
    invoke-direct {p0, p1}, Lcom/dropbox/android/util/O;-><init>(Lcom/dropbox/android/util/L;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 123
    invoke-static {}, Lcom/dropbox/android/util/L;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ShowLoadingUiRunnable running"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/dropbox/android/util/O;->a:Lcom/dropbox/android/util/L;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/util/L;->a(Lcom/dropbox/android/util/L;Lcom/dropbox/android/util/O;)Lcom/dropbox/android/util/O;

    .line 125
    iget-object v0, p0, Lcom/dropbox/android/util/O;->a:Lcom/dropbox/android/util/L;

    invoke-static {v0}, Lcom/dropbox/android/util/L;->a(Lcom/dropbox/android/util/L;)Lcom/dropbox/android/util/P;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/util/P;->a:Lcom/dropbox/android/util/P;

    if-ne v0, v1, :cond_0

    .line 128
    invoke-static {}, Lcom/dropbox/android/util/L;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ShowLoadingUiRunnable showing loading UI"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/dropbox/android/util/O;->a:Lcom/dropbox/android/util/L;

    sget-object v1, Lcom/dropbox/android/util/P;->b:Lcom/dropbox/android/util/P;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/L;->a(Lcom/dropbox/android/util/L;Lcom/dropbox/android/util/P;)Lcom/dropbox/android/util/P;

    .line 130
    iget-object v0, p0, Lcom/dropbox/android/util/O;->a:Lcom/dropbox/android/util/L;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/L;->a(Lcom/dropbox/android/util/L;J)J

    .line 131
    iget-object v0, p0, Lcom/dropbox/android/util/O;->a:Lcom/dropbox/android/util/L;

    invoke-virtual {v0}, Lcom/dropbox/android/util/L;->a()V

    .line 133
    :cond_0
    return-void
.end method
