.class public final enum Lcom/dropbox/android/util/bs;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/util/bs;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/util/bs;

.field public static final enum b:Lcom/dropbox/android/util/bs;

.field private static final synthetic c:[Lcom/dropbox/android/util/bs;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 204
    new-instance v0, Lcom/dropbox/android/util/bs;

    const-string v1, "STREAM_IF_NOT_DOWNLOADED"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/util/bs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/bs;->a:Lcom/dropbox/android/util/bs;

    .line 205
    new-instance v0, Lcom/dropbox/android/util/bs;

    const-string v1, "ALWAYS_DOWNLOAD"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/util/bs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/bs;->b:Lcom/dropbox/android/util/bs;

    .line 203
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/dropbox/android/util/bs;

    sget-object v1, Lcom/dropbox/android/util/bs;->a:Lcom/dropbox/android/util/bs;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/util/bs;->b:Lcom/dropbox/android/util/bs;

    aput-object v1, v0, v3

    sput-object v0, Lcom/dropbox/android/util/bs;->c:[Lcom/dropbox/android/util/bs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 203
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/util/bs;
    .locals 1

    .prologue
    .line 203
    const-class v0, Lcom/dropbox/android/util/bs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/bs;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/util/bs;
    .locals 1

    .prologue
    .line 203
    sget-object v0, Lcom/dropbox/android/util/bs;->c:[Lcom/dropbox/android/util/bs;

    invoke-virtual {v0}, [Lcom/dropbox/android/util/bs;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/util/bs;

    return-object v0
.end method
