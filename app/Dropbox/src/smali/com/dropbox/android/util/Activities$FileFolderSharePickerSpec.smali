.class Lcom/dropbox/android/util/Activities$FileFolderSharePickerSpec;
.super Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;
.source "panda.py"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dropbox/android/util/Activities$FileFolderSharePickerSpec;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/dropbox/android/filemanager/LocalEntry;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 192
    new-instance v0, Lcom/dropbox/android/util/d;

    invoke-direct {v0}, Lcom/dropbox/android/util/d;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/Activities$FileFolderSharePickerSpec;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 187
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;-><init>()V

    .line 188
    sget-object v0, Lcom/dropbox/android/filemanager/LocalEntry;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    iput-object v0, p0, Lcom/dropbox/android/util/Activities$FileFolderSharePickerSpec;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 189
    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .locals 0

    .prologue
    .line 156
    invoke-direct {p0}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment$SharePickerSpec;-><init>()V

    .line 157
    iput-object p1, p0, Lcom/dropbox/android/util/Activities$FileFolderSharePickerSpec;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 158
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/dropbox/android/util/Activities$FileFolderSharePickerSpec;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-boolean v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Z

    if-eqz v0, :cond_0

    .line 163
    const v0, 0x7f0d02c4

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 167
    :goto_0
    return-object v0

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/util/Activities$FileFolderSharePickerSpec;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->i(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 165
    const v0, 0x7f0d02c5

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 167
    :cond_1
    const v0, 0x7f0d02c3

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/r/d;)V
    .locals 3

    .prologue
    .line 173
    new-instance v0, Ldbxyzptlk/db231222/g/am;

    invoke-virtual {p3}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/util/Activities$FileFolderSharePickerSpec;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-direct {v0, p2, v1, v2, p1}, Ldbxyzptlk/db231222/g/am;-><init>(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/Intent;)V

    .line 174
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/am;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 175
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/dropbox/android/util/Activities$FileFolderSharePickerSpec;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v0, p1, p2}, Lcom/dropbox/android/filemanager/LocalEntry;->writeToParcel(Landroid/os/Parcel;I)V

    .line 185
    return-void
.end method
