.class public final Lcom/dropbox/android/util/ao;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private a:[I

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/dropbox/android/util/ao;-><init>(I)V

    .line 27
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput v0, p0, Lcom/dropbox/android/util/ao;->b:I

    .line 23
    iput v0, p0, Lcom/dropbox/android/util/ao;->c:I

    .line 30
    invoke-static {p1}, Lcom/dropbox/android/util/ao;->k(I)I

    move-result v0

    .line 31
    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/dropbox/android/util/ao;->a:[I

    .line 32
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/util/ao;)I
    .locals 1

    .prologue
    .line 9
    iget v0, p0, Lcom/dropbox/android/util/ao;->c:I

    return v0
.end method

.method static synthetic a(Lcom/dropbox/android/util/ao;I)I
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lcom/dropbox/android/util/ao;->h(I)I

    move-result v0

    return v0
.end method

.method private static a([IIII)I
    .locals 4

    .prologue
    .line 308
    add-int v2, p1, p2

    .line 309
    add-int/lit8 v0, p1, -0x1

    move v1, v0

    move v0, v2

    .line 311
    :goto_0
    sub-int v2, v0, v1

    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    .line 312
    add-int v2, v0, v1

    ushr-int/lit8 v2, v2, 0x1

    .line 313
    mul-int/lit8 v3, v2, 0x2

    add-int/lit8 v3, v3, 0x1

    aget v3, p0, v3

    if-ge v3, p3, :cond_0

    move v1, v2

    .line 314
    goto :goto_0

    :cond_0
    move v0, v2

    .line 316
    goto :goto_0

    .line 320
    :cond_1
    add-int v1, p1, p2

    if-ne v0, v1, :cond_3

    .line 321
    add-int v0, p1, p2

    xor-int/lit8 v0, v0, -0x1

    .line 325
    :cond_2
    :goto_1
    return v0

    .line 322
    :cond_3
    mul-int/lit8 v1, v0, 0x2

    aget v1, p0, v1

    if-gt v1, p3, :cond_4

    mul-int/lit8 v1, v0, 0x2

    add-int/lit8 v1, v1, 0x1

    aget v1, p0, v1

    if-le p3, v1, :cond_2

    .line 325
    :cond_4
    xor-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method static synthetic b(Lcom/dropbox/android/util/ao;)I
    .locals 1

    .prologue
    .line 9
    iget v0, p0, Lcom/dropbox/android/util/ao;->b:I

    return v0
.end method

.method static synthetic b(Lcom/dropbox/android/util/ao;I)I
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lcom/dropbox/android/util/ao;->g(I)I

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/dropbox/android/util/ao;)[I
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lcom/dropbox/android/util/ao;->a:[I

    return-object v0
.end method

.method private static e(I)I
    .locals 1

    .prologue
    .line 275
    mul-int/lit8 v0, p0, 0x2

    return v0
.end method

.method private static f(I)I
    .locals 1

    .prologue
    .line 282
    mul-int/lit8 v0, p0, 0x2

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private g(I)I
    .locals 2

    .prologue
    .line 289
    iget-object v0, p0, Lcom/dropbox/android/util/ao;->a:[I

    invoke-static {p1}, Lcom/dropbox/android/util/ao;->e(I)I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method private h(I)I
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lcom/dropbox/android/util/ao;->a:[I

    invoke-static {p1}, Lcom/dropbox/android/util/ao;->f(I)I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method private i(I)I
    .locals 3

    .prologue
    .line 304
    iget-object v0, p0, Lcom/dropbox/android/util/ao;->a:[I

    const/4 v1, 0x0

    iget v2, p0, Lcom/dropbox/android/util/ao;->b:I

    invoke-static {v0, v1, v2, p1}, Lcom/dropbox/android/util/ao;->a([IIII)I

    move-result v0

    return v0
.end method

.method private static j(I)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 335
    const/4 v0, 0x4

    :goto_0
    const/16 v1, 0x20

    if-ge v0, v1, :cond_0

    .line 336
    shl-int v1, v2, v0

    add-int/lit8 v1, v1, -0xc

    if-gt p0, v1, :cond_1

    .line 337
    shl-int v0, v2, v0

    add-int/lit8 p0, v0, -0xc

    .line 340
    :cond_0
    return p0

    .line 335
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static k(I)I
    .locals 1

    .prologue
    .line 344
    mul-int/lit8 v0, p0, 0x4

    invoke-static {v0}, Lcom/dropbox/android/util/ao;->j(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x4

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/dropbox/android/util/ao;->c:I

    if-nez v0, :cond_0

    .line 68
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 70
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dropbox/android/util/ao;->g(I)I

    move-result v0

    return v0
.end method

.method public final a(I)Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/dropbox/android/util/ao;->i(I)I

    move-result v0

    .line 36
    if-gez v0, :cond_0

    .line 37
    xor-int/lit8 v0, v0, -0x1

    .line 38
    iget v1, p0, Lcom/dropbox/android/util/ao;->b:I

    if-lt v0, v1, :cond_0

    .line 39
    const/4 v0, 0x0

    .line 44
    :goto_0
    return-object v0

    .line 42
    :cond_0
    invoke-direct {p0, v0}, Lcom/dropbox/android/util/ao;->g(I)I

    move-result v1

    .line 43
    invoke-direct {p0, v0}, Lcom/dropbox/android/util/ao;->h(I)I

    move-result v0

    .line 44
    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(II)V
    .locals 0

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/dropbox/android/util/ao;->d()V

    .line 251
    :goto_0
    if-gt p1, p2, :cond_0

    .line 252
    invoke-virtual {p0, p1}, Lcom/dropbox/android/util/ao;->d(I)V

    .line 251
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 254
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Iterable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 127
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 128
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/util/ao;->d(I)V

    goto :goto_0

    .line 130
    :cond_0
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/dropbox/android/util/ao;->c:I

    if-nez v0, :cond_0

    .line 75
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 77
    :cond_0
    iget v0, p0, Lcom/dropbox/android/util/ao;->b:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/dropbox/android/util/ao;->h(I)I

    move-result v0

    return v0
.end method

.method public final b(I)Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/dropbox/android/util/ao;->i(I)I

    move-result v0

    .line 49
    if-gez v0, :cond_1

    .line 50
    xor-int/lit8 v0, v0, -0x1

    .line 51
    if-nez v0, :cond_0

    .line 52
    const/4 v0, 0x0

    .line 63
    :goto_0
    return-object v0

    .line 54
    :cond_0
    iget v1, p0, Lcom/dropbox/android/util/ao;->b:I

    if-lt v0, v1, :cond_2

    .line 55
    iget v0, p0, Lcom/dropbox/android/util/ao;->b:I

    add-int/lit8 v0, v0, -0x1

    .line 61
    :cond_1
    :goto_1
    invoke-direct {p0, v0}, Lcom/dropbox/android/util/ao;->g(I)I

    move-result v1

    .line 62
    invoke-direct {p0, v0}, Lcom/dropbox/android/util/ao;->h(I)I

    move-result v0

    .line 63
    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 57
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 232
    iget v0, p0, Lcom/dropbox/android/util/ao;->c:I

    return v0
.end method

.method public final c(I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 137
    invoke-direct {p0, p1}, Lcom/dropbox/android/util/ao;->i(I)I

    move-result v3

    .line 138
    if-gez v3, :cond_0

    .line 169
    :goto_0
    return-void

    .line 142
    :cond_0
    iget v0, p0, Lcom/dropbox/android/util/ao;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/dropbox/android/util/ao;->c:I

    .line 143
    invoke-direct {p0, v3}, Lcom/dropbox/android/util/ao;->g(I)I

    move-result v0

    if-ne v0, p1, :cond_1

    move v0, v1

    .line 144
    :goto_1
    invoke-direct {p0, v3}, Lcom/dropbox/android/util/ao;->h(I)I

    move-result v4

    if-ne v4, p1, :cond_2

    .line 145
    :goto_2
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    .line 147
    iget-object v0, p0, Lcom/dropbox/android/util/ao;->a:[I

    add-int/lit8 v1, v3, 0x1

    mul-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/dropbox/android/util/ao;->a:[I

    mul-int/lit8 v4, v3, 0x2

    iget-object v5, p0, Lcom/dropbox/android/util/ao;->a:[I

    array-length v5, v5

    add-int/lit8 v3, v3, 0x1

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, v5, v3

    invoke-static {v0, v1, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 148
    iget v0, p0, Lcom/dropbox/android/util/ao;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/dropbox/android/util/ao;->b:I

    goto :goto_0

    :cond_1
    move v0, v2

    .line 143
    goto :goto_1

    :cond_2
    move v1, v2

    .line 144
    goto :goto_2

    .line 150
    :cond_3
    if-eqz v0, :cond_4

    .line 152
    iget-object v0, p0, Lcom/dropbox/android/util/ao;->a:[I

    invoke-static {v3}, Lcom/dropbox/android/util/ao;->e(I)I

    move-result v1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    goto :goto_0

    .line 153
    :cond_4
    if-eqz v1, :cond_5

    .line 155
    iget-object v0, p0, Lcom/dropbox/android/util/ao;->a:[I

    invoke-static {v3}, Lcom/dropbox/android/util/ao;->f(I)I

    move-result v1

    aget v2, v0, v1

    add-int/lit8 v2, v2, -0x1

    aput v2, v0, v1

    goto :goto_0

    .line 158
    :cond_5
    iget v0, p0, Lcom/dropbox/android/util/ao;->b:I

    mul-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/dropbox/android/util/ao;->a:[I

    array-length v1, v1

    if-lt v0, v1, :cond_6

    .line 159
    iget v0, p0, Lcom/dropbox/android/util/ao;->b:I

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/dropbox/android/util/ao;->k(I)I

    move-result v0

    .line 160
    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [I

    .line 161
    iget-object v1, p0, Lcom/dropbox/android/util/ao;->a:[I

    iget-object v4, p0, Lcom/dropbox/android/util/ao;->a:[I

    array-length v4, v4

    invoke-static {v1, v2, v0, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 162
    iput-object v0, p0, Lcom/dropbox/android/util/ao;->a:[I

    .line 164
    :cond_6
    iget-object v0, p0, Lcom/dropbox/android/util/ao;->a:[I

    invoke-static {v3}, Lcom/dropbox/android/util/ao;->e(I)I

    move-result v1

    iget-object v2, p0, Lcom/dropbox/android/util/ao;->a:[I

    add-int/lit8 v4, v3, 0x1

    invoke-static {v4}, Lcom/dropbox/android/util/ao;->e(I)I

    move-result v4

    iget v5, p0, Lcom/dropbox/android/util/ao;->b:I

    sub-int/2addr v5, v3

    mul-int/lit8 v5, v5, 0x2

    invoke-static {v0, v1, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 165
    iget-object v0, p0, Lcom/dropbox/android/util/ao;->a:[I

    invoke-static {v3}, Lcom/dropbox/android/util/ao;->f(I)I

    move-result v1

    add-int/lit8 v2, p1, -0x1

    aput v2, v0, v1

    .line 166
    iget-object v0, p0, Lcom/dropbox/android/util/ao;->a:[I

    add-int/lit8 v1, v3, 0x1

    invoke-static {v1}, Lcom/dropbox/android/util/ao;->e(I)I

    move-result v1

    add-int/lit8 v2, p1, 0x1

    aput v2, v0, v1

    .line 167
    iget v0, p0, Lcom/dropbox/android/util/ao;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/util/ao;->b:I

    goto/16 :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 240
    iput v0, p0, Lcom/dropbox/android/util/ao;->b:I

    .line 241
    iput v0, p0, Lcom/dropbox/android/util/ao;->c:I

    .line 242
    return-void
.end method

.method public final d(I)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 172
    invoke-direct {p0, p1}, Lcom/dropbox/android/util/ao;->i(I)I

    move-result v0

    .line 173
    if-ltz v0, :cond_1

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 177
    :cond_1
    iget v3, p0, Lcom/dropbox/android/util/ao;->c:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/dropbox/android/util/ao;->c:I

    .line 178
    xor-int/lit8 v3, v0, -0x1

    .line 179
    iget v0, p0, Lcom/dropbox/android/util/ao;->b:I

    if-ne v3, v0, :cond_2

    if-lez v3, :cond_2

    .line 180
    add-int/lit8 v0, v3, -0x1

    invoke-direct {p0, v0}, Lcom/dropbox/android/util/ao;->h(I)I

    move-result v0

    .line 183
    add-int/lit8 v0, v0, 0x1

    if-ne v0, p1, :cond_2

    .line 184
    iget-object v0, p0, Lcom/dropbox/android/util/ao;->a:[I

    add-int/lit8 v1, v3, -0x1

    invoke-static {v1}, Lcom/dropbox/android/util/ao;->f(I)I

    move-result v1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    goto :goto_0

    .line 188
    :cond_2
    iget v0, p0, Lcom/dropbox/android/util/ao;->b:I

    if-ge v3, v0, :cond_5

    if-lez v3, :cond_5

    .line 190
    add-int/lit8 v0, v3, -0x1

    invoke-static {v0}, Lcom/dropbox/android/util/ao;->f(I)I

    move-result v4

    .line 191
    add-int/lit8 v0, v3, -0x1

    invoke-direct {p0, v0}, Lcom/dropbox/android/util/ao;->h(I)I

    move-result v0

    .line 192
    add-int/lit8 v0, v0, 0x1

    if-ne p1, v0, :cond_8

    .line 193
    iget-object v0, p0, Lcom/dropbox/android/util/ao;->a:[I

    add-int/lit8 v5, v3, -0x1

    invoke-static {v5}, Lcom/dropbox/android/util/ao;->f(I)I

    move-result v5

    aget v6, v0, v5

    add-int/lit8 v6, v6, 0x1

    aput v6, v0, v5

    move v0, v1

    .line 196
    :goto_1
    invoke-static {v3}, Lcom/dropbox/android/util/ao;->e(I)I

    move-result v5

    .line 197
    invoke-static {v3}, Lcom/dropbox/android/util/ao;->f(I)I

    move-result v6

    .line 199
    iget-object v7, p0, Lcom/dropbox/android/util/ao;->a:[I

    aget v7, v7, v5

    add-int/lit8 v7, v7, -0x1

    if-ne p1, v7, :cond_3

    .line 200
    iget-object v0, p0, Lcom/dropbox/android/util/ao;->a:[I

    aget v7, v0, v5

    add-int/lit8 v7, v7, -0x1

    aput v7, v0, v5

    move v0, v1

    .line 204
    :cond_3
    iget-object v7, p0, Lcom/dropbox/android/util/ao;->a:[I

    aget v5, v7, v5

    iget-object v7, p0, Lcom/dropbox/android/util/ao;->a:[I

    aget v7, v7, v4

    sub-int/2addr v5, v7

    if-gt v5, v1, :cond_4

    .line 205
    iget-object v1, p0, Lcom/dropbox/android/util/ao;->a:[I

    iget-object v5, p0, Lcom/dropbox/android/util/ao;->a:[I

    aget v5, v5, v6

    aput v5, v1, v4

    .line 206
    iget-object v1, p0, Lcom/dropbox/android/util/ao;->a:[I

    add-int/lit8 v4, v3, 0x1

    invoke-static {v4}, Lcom/dropbox/android/util/ao;->e(I)I

    move-result v4

    iget-object v5, p0, Lcom/dropbox/android/util/ao;->a:[I

    invoke-static {v3}, Lcom/dropbox/android/util/ao;->e(I)I

    move-result v6

    iget-object v7, p0, Lcom/dropbox/android/util/ao;->a:[I

    array-length v7, v7

    add-int/lit8 v8, v3, 0x1

    invoke-static {v8}, Lcom/dropbox/android/util/ao;->e(I)I

    move-result v8

    sub-int/2addr v7, v8

    invoke-static {v1, v4, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 207
    iget v1, p0, Lcom/dropbox/android/util/ao;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/dropbox/android/util/ao;->b:I

    .line 209
    :cond_4
    if-nez v0, :cond_0

    .line 213
    :cond_5
    iget v0, p0, Lcom/dropbox/android/util/ao;->b:I

    invoke-static {v0}, Lcom/dropbox/android/util/ao;->e(I)I

    move-result v0

    iget-object v1, p0, Lcom/dropbox/android/util/ao;->a:[I

    array-length v1, v1

    if-lt v0, v1, :cond_6

    .line 214
    iget v0, p0, Lcom/dropbox/android/util/ao;->b:I

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/dropbox/android/util/ao;->k(I)I

    move-result v0

    .line 215
    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [I

    .line 216
    iget-object v1, p0, Lcom/dropbox/android/util/ao;->a:[I

    iget-object v4, p0, Lcom/dropbox/android/util/ao;->a:[I

    array-length v4, v4

    invoke-static {v1, v2, v0, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 217
    iput-object v0, p0, Lcom/dropbox/android/util/ao;->a:[I

    .line 220
    :cond_6
    iget v0, p0, Lcom/dropbox/android/util/ao;->b:I

    if-eq v0, v3, :cond_7

    .line 221
    iget-object v0, p0, Lcom/dropbox/android/util/ao;->a:[I

    invoke-static {v3}, Lcom/dropbox/android/util/ao;->e(I)I

    move-result v1

    iget-object v2, p0, Lcom/dropbox/android/util/ao;->a:[I

    add-int/lit8 v4, v3, 0x1

    invoke-static {v4}, Lcom/dropbox/android/util/ao;->e(I)I

    move-result v4

    iget v5, p0, Lcom/dropbox/android/util/ao;->b:I

    invoke-static {v5}, Lcom/dropbox/android/util/ao;->e(I)I

    move-result v5

    invoke-static {v3}, Lcom/dropbox/android/util/ao;->e(I)I

    move-result v6

    sub-int/2addr v5, v6

    invoke-static {v0, v1, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 223
    :cond_7
    iget-object v0, p0, Lcom/dropbox/android/util/ao;->a:[I

    invoke-static {v3}, Lcom/dropbox/android/util/ao;->e(I)I

    move-result v1

    aput p1, v0, v1

    .line 224
    iget-object v0, p0, Lcom/dropbox/android/util/ao;->a:[I

    invoke-static {v3}, Lcom/dropbox/android/util/ao;->f(I)I

    move-result v1

    aput p1, v0, v1

    .line 225
    iget v0, p0, Lcom/dropbox/android/util/ao;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/util/ao;->b:I

    goto/16 :goto_0

    :cond_8
    move v0, v2

    goto/16 :goto_1
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    new-instance v0, Lcom/dropbox/android/util/ap;

    invoke-direct {v0, p0}, Lcom/dropbox/android/util/ap;-><init>(Lcom/dropbox/android/util/ao;)V

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 258
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 259
    const-string v0, "{"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/dropbox/android/util/ao;->b:I

    if-ge v0, v2, :cond_0

    .line 261
    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    invoke-direct {p0, v0}, Lcom/dropbox/android/util/ao;->g(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 263
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    invoke-direct {p0, v0}, Lcom/dropbox/android/util/ao;->h(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 265
    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 267
    :cond_0
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
