.class public Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;
.super Lcom/dropbox/android/util/HistoryEntry;
.source "panda.py"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 219
    new-instance v0, Lcom/dropbox/android/util/aj;

    invoke-direct {v0}, Lcom/dropbox/android/util/aj;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 215
    invoke-direct {p0, p1}, Lcom/dropbox/android/util/HistoryEntry;-><init>(Landroid/os/Parcel;)V

    .line 216
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;->c:Ljava/lang/String;

    .line 217
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 174
    sget-object v0, Lcom/dropbox/android/util/al;->b:Lcom/dropbox/android/util/al;

    invoke-direct {p0, v0}, Lcom/dropbox/android/util/HistoryEntry;-><init>(Lcom/dropbox/android/util/al;)V

    .line 175
    iput-object p1, p0, Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;->c:Ljava/lang/String;

    .line 176
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 180
    const v0, 0x7f0d0018

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 185
    const v0, 0x7f0d004f

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 190
    const v0, 0x7f0d0055

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 199
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;->c:Ljava/lang/String;

    check-cast p1, Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;

    iget-object v1, p1, Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;->c:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 210
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/HistoryEntry;->writeToParcel(Landroid/os/Parcel;I)V

    .line 211
    iget-object v0, p0, Lcom/dropbox/android/util/HistoryEntry$DropboxSearchEntry;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 212
    return-void
.end method
