.class public Lcom/dropbox/android/util/analytics/l;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/util/analytics/k;


# instance fields
.field protected final a:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1427
    new-instance v0, Ljava/util/LinkedHashMap;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    iput-object v0, p0, Lcom/dropbox/android/util/analytics/l;->a:Ljava/util/LinkedHashMap;

    .line 1434
    const-string v0, "boot_ts"

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/dropbox/android/util/analytics/l;->d(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 1435
    const-string v0, "ts"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/dropbox/android/util/analytics/l;->d(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 1436
    const-string v0, "event"

    invoke-virtual {p0, v0, p1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 1437
    return-void
.end method

.method protected static d(J)Ljava/lang/String;
    .locals 8

    .prologue
    .line 1430
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%.02f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    long-to-double v4, p0

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;
    .locals 0

    .prologue
    .line 1473
    if-eqz p1, :cond_0

    .line 1474
    invoke-interface {p1, p0}, Lcom/dropbox/android/util/analytics/m;->a(Lcom/dropbox/android/util/analytics/l;)V

    .line 1476
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/String;D)Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1453
    iget-object v0, p0, Lcom/dropbox/android/util/analytics/l;->a:Ljava/util/LinkedHashMap;

    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1454
    return-object p0
.end method

.method public final a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;
    .locals 2

    .prologue
    .line 1458
    iget-object v0, p0, Lcom/dropbox/android/util/analytics/l;->a:Ljava/util/LinkedHashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1459
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;
    .locals 1

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/dropbox/android/util/analytics/l;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1449
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;
    .locals 1

    .prologue
    .line 1443
    iget-object v0, p0, Lcom/dropbox/android/util/analytics/l;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1444
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;)Lcom/dropbox/android/util/analytics/l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<*>;)",
            "Lcom/dropbox/android/util/analytics/l;"
        }
    .end annotation

    .prologue
    .line 1468
    iget-object v0, p0, Lcom/dropbox/android/util/analytics/l;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1469
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;)Lcom/dropbox/android/util/analytics/l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/dropbox/android/util/analytics/l;"
        }
    .end annotation

    .prologue
    .line 1463
    iget-object v0, p0, Lcom/dropbox/android/util/analytics/l;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1464
    return-object p0
.end method

.method public e()V
    .locals 0

    .prologue
    .line 1496
    invoke-static {p0}, Lcom/dropbox/android/util/analytics/a;->a(Lcom/dropbox/android/util/analytics/k;)V

    .line 1497
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1489
    iget-object v0, p0, Lcom/dropbox/android/util/analytics/l;->a:Ljava/util/LinkedHashMap;

    invoke-static {v0}, Ldbxyzptlk/db231222/aj/c;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
