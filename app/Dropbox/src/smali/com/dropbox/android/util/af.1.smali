.class final Lcom/dropbox/android/util/af;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:J

.field private final b:I


# direct methods
.method private constructor <init>(JI)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    iput-wide p1, p0, Lcom/dropbox/android/util/af;->a:J

    .line 103
    iput p3, p0, Lcom/dropbox/android/util/af;->b:I

    .line 104
    return-void
.end method

.method synthetic constructor <init>(JILcom/dropbox/android/util/ae;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/util/af;-><init>(JI)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 113
    iget-wide v0, p0, Lcom/dropbox/android/util/af;->a:J

    div-long v0, p2, v0

    long-to-int v0, v0

    .line 114
    iget v1, p0, Lcom/dropbox/android/util/af;->b:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p1, v1, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)Z
    .locals 2

    .prologue
    .line 108
    iget-wide v0, p0, Lcom/dropbox/android/util/af;->a:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
