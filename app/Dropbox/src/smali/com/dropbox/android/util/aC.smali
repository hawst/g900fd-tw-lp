.class public final Lcom/dropbox/android/util/aC;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static a:Lcom/dropbox/android/util/aC;


# instance fields
.field private final b:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/dropbox/android/util/aC;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 55
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/util/aC;)I
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/dropbox/android/util/aC;->d()I

    move-result v0

    return v0
.end method

.method public static declared-synchronized a()Lcom/dropbox/android/util/aC;
    .locals 2

    .prologue
    .line 35
    const-class v1, Lcom/dropbox/android/util/aC;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/dropbox/android/util/aC;->a:Lcom/dropbox/android/util/aC;

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Lcom/dropbox/android/util/aC;

    invoke-direct {v0}, Lcom/dropbox/android/util/aC;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/aC;->a:Lcom/dropbox/android/util/aC;

    .line 38
    :cond_0
    sget-object v0, Lcom/dropbox/android/util/aC;->a:Lcom/dropbox/android/util/aC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Lcom/dropbox/android/util/aC;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/dropbox/android/util/aC;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method private c()I
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/dropbox/android/util/aC;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method private d()I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 66
    const/4 v6, -0x1

    .line 71
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/ZipperedMediaProvider;->c:Landroid/net/Uri;

    const-string v5, "date_added LIMIT 20"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 74
    if-eqz v1, :cond_0

    .line 76
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 78
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 81
    :goto_0
    return v0

    .line 78
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    move v0, v6

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 27
    const/16 v0, 0x14

    if-gt p1, v0, :cond_0

    move v0, v1

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Must only check for up to MAX_PHOTO_COUNT=20 images.Checking for n="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " insead"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/dropbox/android/util/C;->a(ZLjava/lang/String;)V

    .line 30
    invoke-direct {p0}, Lcom/dropbox/android/util/aC;->c()I

    move-result v0

    if-lt v0, p1, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 27
    goto :goto_0

    :cond_1
    move v1, v2

    .line 30
    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/dropbox/android/util/aD;

    invoke-direct {v0, p0}, Lcom/dropbox/android/util/aD;-><init>(Lcom/dropbox/android/util/aC;)V

    invoke-virtual {v0}, Lcom/dropbox/android/util/aD;->start()V

    .line 52
    return-void
.end method
