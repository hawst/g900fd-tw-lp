.class public final Lcom/dropbox/android/util/ad;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:[Lcom/dropbox/android/util/af;

.field private static final b:Ljava/text/DateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 120
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/dropbox/android/util/af;

    const/4 v1, 0x0

    new-instance v2, Lcom/dropbox/android/util/af;

    const-wide v3, 0x757b12c00L

    const/high16 v5, 0x7f0f0000

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/dropbox/android/util/af;-><init>(JILcom/dropbox/android/util/ae;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/dropbox/android/util/af;

    const-wide v3, 0x9a7ec800L

    const v5, 0x7f0f0001

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/dropbox/android/util/af;-><init>(JILcom/dropbox/android/util/ae;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/dropbox/android/util/af;

    const-wide/32 v3, 0x240c8400

    const v5, 0x7f0f0002

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/dropbox/android/util/af;-><init>(JILcom/dropbox/android/util/ae;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/dropbox/android/util/af;

    const-wide/32 v3, 0x5265c00

    const v5, 0x7f0f0003

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/dropbox/android/util/af;-><init>(JILcom/dropbox/android/util/ae;)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcom/dropbox/android/util/af;

    const-wide/32 v3, 0x36ee80

    const v5, 0x7f0f0004

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/dropbox/android/util/af;-><init>(JILcom/dropbox/android/util/ae;)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lcom/dropbox/android/util/af;

    const-wide/32 v3, 0xea60

    const v5, 0x7f0f0005

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/dropbox/android/util/af;-><init>(JILcom/dropbox/android/util/ae;)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lcom/dropbox/android/util/af;

    const-wide/16 v3, 0x3e8

    const v5, 0x7f0f0006

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/dropbox/android/util/af;-><init>(JILcom/dropbox/android/util/ae;)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/util/ad;->a:[Lcom/dropbox/android/util/af;

    .line 139
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEE, dd MMM yyyy kk:mm:ss ZZZZZ"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/dropbox/android/util/ad;->b:Ljava/text/DateFormat;

    return-void
.end method

.method public static a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 152
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "ZZZZZ"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;J)Ljava/lang/String;
    .locals 9

    .prologue
    const-wide/16 v7, 0x0

    .line 161
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 162
    sub-long/2addr v0, p1

    invoke-static {v7, v8, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    .line 163
    sget-object v3, Lcom/dropbox/android/util/ad;->a:[Lcom/dropbox/android/util/af;

    array-length v4, v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    .line 164
    invoke-virtual {v5, v1, v2}, Lcom/dropbox/android/util/af;->a(J)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 165
    invoke-virtual {v5, p0, v1, v2}, Lcom/dropbox/android/util/af;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v0

    .line 170
    :goto_1
    return-object v0

    .line 163
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 170
    :cond_1
    sget-object v0, Lcom/dropbox/android/util/ad;->a:[Lcom/dropbox/android/util/af;

    sget-object v1, Lcom/dropbox/android/util/ad;->a:[Lcom/dropbox/android/util/af;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-virtual {v0, p0, v7, v8}, Lcom/dropbox/android/util/af;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Landroid/content/res/Resources;JJ)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 131
    long-to-double v0, p1

    long-to-double v2, p3

    div-double/2addr v0, v2

    .line 133
    invoke-static {p0}, Lcom/dropbox/android/util/au;->b(Landroid/content/res/Resources;)Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2}, Ljava/text/NumberFormat;->getPercentInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v2

    .line 134
    invoke-virtual {v2, v4}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 135
    invoke-virtual {v2, v4}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 136
    invoke-virtual {v2, v0, v1}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;JZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 48
    const-wide/high16 v0, 0xfa0000000000000L

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 49
    const v2, 0x7f0d0083

    .line 50
    const-wide/high16 v0, 0x1000000000000000L

    .line 73
    :goto_0
    long-to-double v3, p1

    long-to-double v0, v0

    div-double v0, v3, v0

    .line 75
    invoke-static {p0}, Lcom/dropbox/android/util/au;->b(Landroid/content/res/Resources;)Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v3

    .line 76
    if-eqz p3, :cond_6

    .line 77
    invoke-virtual {v3, v5}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 78
    invoke-virtual {v3, v5}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 83
    :goto_1
    invoke-virtual {v3, v0, v1}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    .line 85
    new-array v1, v5, [Ljava/lang/Object;

    aput-object v0, v1, v6

    invoke-virtual {p0, v2, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    return-object v0

    .line 51
    :cond_0
    const-wide v0, 0x3e80000000000L

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    .line 52
    const v2, 0x7f0d0082

    .line 53
    const-wide/high16 v0, 0x4000000000000L

    goto :goto_0

    .line 54
    :cond_1
    const-wide v0, 0xfa00000000L

    cmp-long v0, p1, v0

    if-lez v0, :cond_2

    .line 55
    const v2, 0x7f0d0081

    .line 56
    const-wide v0, 0x10000000000L

    goto :goto_0

    .line 57
    :cond_2
    const-wide/32 v0, 0x3e800000

    cmp-long v0, p1, v0

    if-lez v0, :cond_3

    .line 58
    const v2, 0x7f0d0080

    .line 59
    const-wide/32 v0, 0x40000000

    goto :goto_0

    .line 60
    :cond_3
    const-wide/32 v0, 0xfa000

    cmp-long v0, p1, v0

    if-lez v0, :cond_4

    .line 61
    const v2, 0x7f0d007f

    .line 62
    const-wide/32 v0, 0x100000

    goto :goto_0

    .line 63
    :cond_4
    const-wide/16 v0, 0x3e8

    cmp-long v0, p1, v0

    if-lez v0, :cond_5

    .line 64
    const v2, 0x7f0d007e

    .line 65
    const-wide/16 v0, 0x400

    goto :goto_0

    .line 69
    :cond_5
    long-to-int v0, p1

    .line 70
    const v1, 0x7f0f0007

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {p0, v1, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 80
    :cond_6
    invoke-virtual {v3, v6}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 81
    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    goto :goto_1
.end method

.method public static a(Landroid/content/res/Resources;Ldbxyzptlk/db231222/ac/b;Ldbxyzptlk/db231222/ac/b;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 189
    new-instance v0, Ldbxyzptlk/db231222/ac/v;

    invoke-static {}, Ldbxyzptlk/db231222/ac/w;->a()Ldbxyzptlk/db231222/ac/w;

    move-result-object v1

    invoke-direct {v0, p2, p1, v1}, Ldbxyzptlk/db231222/ac/v;-><init>(Ldbxyzptlk/db231222/ac/B;Ldbxyzptlk/db231222/ac/B;Ldbxyzptlk/db231222/ac/w;)V

    .line 191
    invoke-static {p2, p1}, Ldbxyzptlk/db231222/ac/s;->a(Ldbxyzptlk/db231222/ac/B;Ldbxyzptlk/db231222/ac/B;)Ldbxyzptlk/db231222/ac/s;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/ac/s;->c()I

    move-result v1

    if-gt v1, v5, :cond_0

    .line 192
    const v0, 0x7f0d0309

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 229
    :goto_0
    return-object v0

    .line 200
    :cond_0
    invoke-virtual {p1}, Ldbxyzptlk/db231222/ac/b;->l_()Ldbxyzptlk/db231222/ac/r;

    move-result-object v1

    .line 201
    invoke-virtual {p2}, Ldbxyzptlk/db231222/ac/b;->l_()Ldbxyzptlk/db231222/ac/r;

    move-result-object v2

    .line 202
    new-instance v3, Ldbxyzptlk/db231222/ac/v;

    invoke-static {}, Ldbxyzptlk/db231222/ac/w;->a()Ldbxyzptlk/db231222/ac/w;

    move-result-object v4

    invoke-direct {v3, v2, v1, v4}, Ldbxyzptlk/db231222/ac/v;-><init>(Ldbxyzptlk/db231222/ac/D;Ldbxyzptlk/db231222/ac/D;Ldbxyzptlk/db231222/ac/w;)V

    .line 204
    invoke-virtual {v3}, Ldbxyzptlk/db231222/ac/v;->a()I

    move-result v1

    if-lez v1, :cond_1

    .line 205
    const v0, 0x7f0f0034

    invoke-virtual {v3}, Ldbxyzptlk/db231222/ac/v;->a()I

    move-result v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/ac/v;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 209
    :cond_1
    invoke-virtual {v3}, Ldbxyzptlk/db231222/ac/v;->c()I

    move-result v1

    if-lez v1, :cond_2

    .line 210
    const v0, 0x7f0f0033

    invoke-virtual {v3}, Ldbxyzptlk/db231222/ac/v;->c()I

    move-result v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/ac/v;->c()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 214
    :cond_2
    invoke-virtual {v3}, Ldbxyzptlk/db231222/ac/v;->d()I

    move-result v1

    if-lez v1, :cond_3

    .line 215
    const v0, 0x7f0f0032

    invoke-virtual {v3}, Ldbxyzptlk/db231222/ac/v;->d()I

    move-result v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/ac/v;->d()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 219
    :cond_3
    invoke-virtual {v3}, Ldbxyzptlk/db231222/ac/v;->e()I

    move-result v1

    if-lez v1, :cond_4

    .line 220
    const v0, 0x7f0f0031

    invoke-virtual {v3}, Ldbxyzptlk/db231222/ac/v;->e()I

    move-result v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/ac/v;->e()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 224
    :cond_4
    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/v;->f()I

    move-result v1

    if-lez v1, :cond_5

    .line 225
    const v1, 0x7f0f0030

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/v;->f()I

    move-result v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/v;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-virtual {p0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 229
    :cond_5
    const v1, 0x7f0f002f

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/v;->g()I

    move-result v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/v;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-virtual {p0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/res/Resources;Ljava/util/Date;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 179
    new-instance v0, Ldbxyzptlk/db231222/ac/b;

    invoke-direct {v0}, Ldbxyzptlk/db231222/ac/b;-><init>()V

    .line 180
    new-instance v1, Ldbxyzptlk/db231222/ac/b;

    invoke-direct {v1, p1}, Ldbxyzptlk/db231222/ac/b;-><init>(Ljava/lang/Object;)V

    .line 181
    invoke-static {p0, v0, v1}, Lcom/dropbox/android/util/ad;->a(Landroid/content/res/Resources;Ldbxyzptlk/db231222/ac/b;Ldbxyzptlk/db231222/ac/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Date;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 145
    sget-object v1, Lcom/dropbox/android/util/ad;->b:Ljava/text/DateFormat;

    monitor-enter v1

    .line 146
    :try_start_0
    sget-object v0, Lcom/dropbox/android/util/ad;->b:Ljava/text/DateFormat;

    invoke-virtual {v0, p0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
