.class public final Lcom/dropbox/android/util/aW;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1::",
        "Ljava/io/Serializable;",
        "T2::",
        "Ljava/io/Serializable;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x4844e5fd8d4c8b89L


# instance fields
.field private final a:Ljava/io/Serializable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT1;"
        }
    .end annotation
.end field

.field private final b:Ljava/io/Serializable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT2;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object v0, p0, Lcom/dropbox/android/util/aW;->a:Ljava/io/Serializable;

    iput-object v0, p0, Lcom/dropbox/android/util/aW;->b:Ljava/io/Serializable;

    .line 38
    return-void
.end method

.method public constructor <init>(Ljava/io/Serializable;Ljava/io/Serializable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT1;TT2;)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/dropbox/android/util/aW;->a:Ljava/io/Serializable;

    .line 33
    iput-object p2, p0, Lcom/dropbox/android/util/aW;->b:Ljava/io/Serializable;

    .line 34
    return-void
.end method


# virtual methods
.method public final a()Ljava/io/Serializable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT1;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/dropbox/android/util/aW;->a:Ljava/io/Serializable;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 58
    instance-of v1, p1, Lcom/dropbox/android/util/aW;

    if-nez v1, :cond_1

    .line 62
    :cond_0
    :goto_0
    return v0

    .line 61
    :cond_1
    check-cast p1, Lcom/dropbox/android/util/aW;

    .line 62
    iget-object v1, p1, Lcom/dropbox/android/util/aW;->a:Ljava/io/Serializable;

    iget-object v2, p0, Lcom/dropbox/android/util/aW;->a:Ljava/io/Serializable;

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/E/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/dropbox/android/util/aW;->b:Ljava/io/Serializable;

    iget-object v2, p0, Lcom/dropbox/android/util/aW;->b:Ljava/io/Serializable;

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/E/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 72
    iget-object v0, p0, Lcom/dropbox/android/util/aW;->a:Ljava/io/Serializable;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/dropbox/android/util/aW;->b:Ljava/io/Serializable;

    if-nez v2, :cond_1

    :goto_1
    xor-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/dropbox/android/util/aW;->a:Ljava/io/Serializable;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/dropbox/android/util/aW;->b:Ljava/io/Serializable;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1
.end method
