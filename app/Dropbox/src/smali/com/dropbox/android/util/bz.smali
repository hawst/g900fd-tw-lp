.class public final enum Lcom/dropbox/android/util/bz;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dropbox/android/util/bz;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/dropbox/android/util/bz;

.field public static final enum b:Lcom/dropbox/android/util/bz;

.field public static final enum c:Lcom/dropbox/android/util/bz;

.field public static final enum d:Lcom/dropbox/android/util/bz;

.field public static final enum e:Lcom/dropbox/android/util/bz;

.field public static final enum f:Lcom/dropbox/android/util/bz;

.field public static final enum g:Lcom/dropbox/android/util/bz;

.field public static final enum h:Lcom/dropbox/android/util/bz;

.field public static final enum i:Lcom/dropbox/android/util/bz;

.field public static final enum j:Lcom/dropbox/android/util/bz;

.field public static final enum k:Lcom/dropbox/android/util/bz;

.field public static final enum l:Lcom/dropbox/android/util/bz;

.field public static final enum m:Lcom/dropbox/android/util/bz;

.field public static final enum n:Lcom/dropbox/android/util/bz;

.field public static final enum o:Lcom/dropbox/android/util/bz;

.field public static final enum p:Lcom/dropbox/android/util/bz;

.field public static final enum q:Lcom/dropbox/android/util/bz;

.field public static final enum r:Lcom/dropbox/android/util/bz;

.field public static final enum s:Lcom/dropbox/android/util/bz;

.field public static final enum t:Lcom/dropbox/android/util/bz;

.field public static final enum u:Lcom/dropbox/android/util/bz;

.field private static final synthetic w:[Lcom/dropbox/android/util/bz;


# instance fields
.field private v:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 15
    new-instance v0, Lcom/dropbox/android/util/bz;

    const-string v1, "HELP_HOME"

    const-string v2, "https://www.dropbox.com/help/category/Mobile?cl=%s#category:Mobile"

    invoke-direct {v0, v1, v4, v2}, Lcom/dropbox/android/util/bz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/bz;->a:Lcom/dropbox/android/util/bz;

    .line 16
    new-instance v0, Lcom/dropbox/android/util/bz;

    const-string v1, "HELP_FAVORITES"

    const-string v2, "https://www.dropbox.com/c/help/mobile_favorites?cl=%s&device=android"

    invoke-direct {v0, v1, v5, v2}, Lcom/dropbox/android/util/bz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/bz;->b:Lcom/dropbox/android/util/bz;

    .line 17
    new-instance v0, Lcom/dropbox/android/util/bz;

    const-string v1, "HELP_CAMERA_QUOTA"

    const-string v2, "https://www.dropbox.com/c/help/camera_upload_full?cl=%s&device=android"

    invoke-direct {v0, v1, v6, v2}, Lcom/dropbox/android/util/bz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/bz;->c:Lcom/dropbox/android/util/bz;

    .line 18
    new-instance v0, Lcom/dropbox/android/util/bz;

    const-string v1, "HELP_TWO_FACTOR"

    const-string v2, "https://www.dropbox.com/c/help/two_step?cl=%s&device=android"

    invoke-direct {v0, v1, v7, v2}, Lcom/dropbox/android/util/bz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/bz;->d:Lcom/dropbox/android/util/bz;

    .line 19
    new-instance v0, Lcom/dropbox/android/util/bz;

    const-string v1, "TOS"

    const-string v2, "https://www.dropbox.com/terms?cl=%s&mobile=1"

    invoke-direct {v0, v1, v8, v2}, Lcom/dropbox/android/util/bz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/bz;->e:Lcom/dropbox/android/util/bz;

    .line 20
    new-instance v0, Lcom/dropbox/android/util/bz;

    const-string v1, "PRIVACY"

    const/4 v2, 0x5

    const-string v3, "https://www.dropbox.com/privacy?cl=%s&mobile=1"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/util/bz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/bz;->f:Lcom/dropbox/android/util/bz;

    .line 21
    new-instance v0, Lcom/dropbox/android/util/bz;

    const-string v1, "OPEN_SOURCE"

    const/4 v2, 0x6

    const-string v3, "https://www.dropbox.com/android_opensource?cl=%s&mobile=1"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/util/bz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/bz;->g:Lcom/dropbox/android/util/bz;

    .line 24
    new-instance v0, Lcom/dropbox/android/util/bz;

    const-string v1, "UPGRADE_SETTINGS_UPGRADE_BUTTON"

    const/4 v2, 0x7

    const-string v3, "https://www.dropbox.com/upgrade?cl=%s&android_app=1&oqa=upsub"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/util/bz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/bz;->h:Lcom/dropbox/android/util/bz;

    .line 25
    new-instance v0, Lcom/dropbox/android/util/bz;

    const-string v1, "UPGRADE_SETTINGS_SPACE_BUTTON"

    const/16 v2, 0x8

    const-string v3, "https://www.dropbox.com/upgrade?cl=%s&android_app=1&oqa=upssb"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/util/bz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/bz;->i:Lcom/dropbox/android/util/bz;

    .line 26
    new-instance v0, Lcom/dropbox/android/util/bz;

    const-string v1, "UPGRADE_CONTEXT_MENU"

    const/16 v2, 0x9

    const-string v3, "https://www.dropbox.com/upgrade?cl=%s&android_app=1&oqa=upcm"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/util/bz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/bz;->j:Lcom/dropbox/android/util/bz;

    .line 27
    new-instance v0, Lcom/dropbox/android/util/bz;

    const-string v1, "UPGRADE_OVER_QUOTA_CAMERA_UPLOAD"

    const/16 v2, 0xa

    const-string v3, "https://www.dropbox.com/upgrade?cl=%s&android_app=1&oqa=upcuoq"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/util/bz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/bz;->k:Lcom/dropbox/android/util/bz;

    .line 28
    new-instance v0, Lcom/dropbox/android/util/bz;

    const-string v1, "UPGRADE_OVER_QUOTA_MANUAL_UPLOAD"

    const/16 v2, 0xb

    const-string v3, "https://www.dropbox.com/upgrade?cl=%s&android_app=1&oqa=upmuoq"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/util/bz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/bz;->l:Lcom/dropbox/android/util/bz;

    .line 29
    new-instance v0, Lcom/dropbox/android/util/bz;

    const-string v1, "UPGRADE_OVER_QUOTA_MOVE_FILE"

    const/16 v2, 0xc

    const-string v3, "https://www.dropbox.com/upgrade?cl=%s&android_app=1&oqa=upmfioq"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/util/bz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/bz;->m:Lcom/dropbox/android/util/bz;

    .line 30
    new-instance v0, Lcom/dropbox/android/util/bz;

    const-string v1, "UPGRADE_OVER_QUOTA_MOVE_FOLDER"

    const/16 v2, 0xd

    const-string v3, "https://www.dropbox.com/upgrade?cl=%s&android_app=1&oqa=upmfooq"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/util/bz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/bz;->n:Lcom/dropbox/android/util/bz;

    .line 31
    new-instance v0, Lcom/dropbox/android/util/bz;

    const-string v1, "UPGRADE_OVER_QUOTA_RENAME_FILE"

    const/16 v2, 0xe

    const-string v3, "https://www.dropbox.com/upgrade?cl=%s&android_app=1&oqa=uprfioq"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/util/bz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/bz;->o:Lcom/dropbox/android/util/bz;

    .line 32
    new-instance v0, Lcom/dropbox/android/util/bz;

    const-string v1, "UPGRADE_OVER_QUOTA_RENAME_FOLDER"

    const/16 v2, 0xf

    const-string v3, "https://www.dropbox.com/upgrade?cl=%s&android_app=1&oqa=uprfooq"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/util/bz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/bz;->p:Lcom/dropbox/android/util/bz;

    .line 33
    new-instance v0, Lcom/dropbox/android/util/bz;

    const-string v1, "UPGRADE_OVER_QUOTA_NEW_FOLDER"

    const/16 v2, 0x10

    const-string v3, "https://www.dropbox.com/upgrade?cl=%s&android_app=1&oqa=upnfoq"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/util/bz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/bz;->q:Lcom/dropbox/android/util/bz;

    .line 34
    new-instance v0, Lcom/dropbox/android/util/bz;

    const-string v1, "UPGRADE_OVER_QUOTA_SHARED_FOLDER"

    const/16 v2, 0x11

    const-string v3, "https://www.dropbox.com/upgrade?cl=%s&android_app=1&oqa=upsfoq"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/util/bz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/bz;->r:Lcom/dropbox/android/util/bz;

    .line 35
    new-instance v0, Lcom/dropbox/android/util/bz;

    const-string v1, "UPGRADE_NOTIFICATION_UPGRADE_BUTTON"

    const/16 v2, 0x12

    const-string v3, "https://www.dropbox.com/upgrade?cl=%s&android_app=1&oqa=upnotb"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/util/bz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/bz;->s:Lcom/dropbox/android/util/bz;

    .line 36
    new-instance v0, Lcom/dropbox/android/util/bz;

    const-string v1, "UPGRADE_DEAL_EXPIRATION_NOTIFICATION_UPGRADE_BUTTON"

    const/16 v2, 0x13

    const-string v3, "https://www.dropbox.com/upgrade?cl=%s&android_app=1&oqa=updenotb"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/util/bz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/bz;->t:Lcom/dropbox/android/util/bz;

    .line 37
    new-instance v0, Lcom/dropbox/android/util/bz;

    const-string v1, "UPGRADE_NOTIFICATION_BACKGROUND"

    const/16 v2, 0x14

    const-string v3, "https://www.dropbox.com/upgrade?cl=%s&android_app=1&oqa=upnot"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/util/bz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/bz;->u:Lcom/dropbox/android/util/bz;

    .line 14
    const/16 v0, 0x15

    new-array v0, v0, [Lcom/dropbox/android/util/bz;

    sget-object v1, Lcom/dropbox/android/util/bz;->a:Lcom/dropbox/android/util/bz;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/util/bz;->b:Lcom/dropbox/android/util/bz;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/android/util/bz;->c:Lcom/dropbox/android/util/bz;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dropbox/android/util/bz;->d:Lcom/dropbox/android/util/bz;

    aput-object v1, v0, v7

    sget-object v1, Lcom/dropbox/android/util/bz;->e:Lcom/dropbox/android/util/bz;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/dropbox/android/util/bz;->f:Lcom/dropbox/android/util/bz;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dropbox/android/util/bz;->g:Lcom/dropbox/android/util/bz;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/dropbox/android/util/bz;->h:Lcom/dropbox/android/util/bz;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dropbox/android/util/bz;->i:Lcom/dropbox/android/util/bz;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/dropbox/android/util/bz;->j:Lcom/dropbox/android/util/bz;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/dropbox/android/util/bz;->k:Lcom/dropbox/android/util/bz;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/dropbox/android/util/bz;->l:Lcom/dropbox/android/util/bz;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/dropbox/android/util/bz;->m:Lcom/dropbox/android/util/bz;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/dropbox/android/util/bz;->n:Lcom/dropbox/android/util/bz;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/dropbox/android/util/bz;->o:Lcom/dropbox/android/util/bz;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/dropbox/android/util/bz;->p:Lcom/dropbox/android/util/bz;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/dropbox/android/util/bz;->q:Lcom/dropbox/android/util/bz;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/dropbox/android/util/bz;->r:Lcom/dropbox/android/util/bz;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/dropbox/android/util/bz;->s:Lcom/dropbox/android/util/bz;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/dropbox/android/util/bz;->t:Lcom/dropbox/android/util/bz;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/dropbox/android/util/bz;->u:Lcom/dropbox/android/util/bz;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/util/bz;->w:[Lcom/dropbox/android/util/bz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 42
    iput-object p3, p0, Lcom/dropbox/android/util/bz;->v:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/util/bz;
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/dropbox/android/util/bz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/bz;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/util/bz;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/dropbox/android/util/bz;->w:[Lcom/dropbox/android/util/bz;

    invoke-virtual {v0}, [Lcom/dropbox/android/util/bz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/util/bz;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 50
    iget-object v0, p0, Lcom/dropbox/android/util/bz;->v:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Lcom/dropbox/android/util/au;->c(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
