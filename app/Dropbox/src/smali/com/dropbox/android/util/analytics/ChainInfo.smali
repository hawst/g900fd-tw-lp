.class public final Lcom/dropbox/android/util/analytics/ChainInfo;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/dropbox/android/util/analytics/m;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dropbox/android/util/analytics/ChainInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/Random;


# instance fields
.field private final a:J

.field private final b:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/analytics/ChainInfo;->c:Ljava/util/Random;

    .line 53
    new-instance v0, Lcom/dropbox/android/util/analytics/q;

    invoke-direct {v0}, Lcom/dropbox/android/util/analytics/q;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/analytics/ChainInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-object v0, Lcom/dropbox/android/util/analytics/ChainInfo;->c:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/util/analytics/ChainInfo;->a:J

    .line 23
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/util/analytics/ChainInfo;->b:J

    .line 24
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/util/analytics/ChainInfo;->a:J

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/util/analytics/ChainInfo;->b:J

    .line 68
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/dropbox/android/util/analytics/q;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/dropbox/android/util/analytics/ChainInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static a()Lcom/dropbox/android/util/analytics/ChainInfo;
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/dropbox/android/util/analytics/ChainInfo;

    invoke-direct {v0}, Lcom/dropbox/android/util/analytics/ChainInfo;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/util/analytics/l;)V
    .locals 6

    .prologue
    .line 39
    const-string v0, "chain_id"

    iget-wide v1, p0, Lcom/dropbox/android/util/analytics/ChainInfo;->a:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "chain_dur"

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/dropbox/android/util/analytics/ChainInfo;->b:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    .line 40
    return-void
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/dropbox/android/util/analytics/ChainInfo;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 50
    iget-wide v0, p0, Lcom/dropbox/android/util/analytics/ChainInfo;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 51
    return-void
.end method
