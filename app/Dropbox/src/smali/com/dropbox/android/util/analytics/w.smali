.class public final Lcom/dropbox/android/util/analytics/w;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/util/analytics/m;


# instance fields
.field private a:J


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/util/analytics/w;->a:J

    .line 17
    return-void
.end method

.method public static a()Lcom/dropbox/android/util/analytics/w;
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/dropbox/android/util/analytics/w;

    invoke-direct {v0}, Lcom/dropbox/android/util/analytics/w;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/util/analytics/l;)V
    .locals 5

    .prologue
    .line 31
    const-string v0, "dur"

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/dropbox/android/util/analytics/w;->a:J

    sub-long/2addr v1, v3

    invoke-virtual {p1, v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    .line 32
    return-void
.end method
