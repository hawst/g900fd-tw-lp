.class final Lcom/dropbox/android/util/W;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/dropbox/android/util/DropboxPath;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Parcel;)Lcom/dropbox/android/util/DropboxPath;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 333
    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-ne v3, v0, :cond_0

    :goto_0
    invoke-direct {v1, v2, v0}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;Z)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)[Lcom/dropbox/android/util/DropboxPath;
    .locals 1

    .prologue
    .line 338
    new-array v0, p1, [Lcom/dropbox/android/util/DropboxPath;

    return-object v0
.end method

.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 330
    invoke-virtual {p0, p1}, Lcom/dropbox/android/util/W;->a(Landroid/os/Parcel;)Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 330
    invoke-virtual {p0, p1}, Lcom/dropbox/android/util/W;->a(I)[Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    return-object v0
.end method
