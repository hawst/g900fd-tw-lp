.class public final Lcom/dropbox/android/util/bk;
.super Ljava/lang/Object;
.source "panda.py"


# direct methods
.method private static a(Landroid/content/Context;)Landroid/content/Context;
    .locals 2

    .prologue
    .line 31
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x1030128

    invoke-direct {v0, p0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 40
    invoke-static {p0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 41
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/View;I)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 72
    invoke-static {p0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 73
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 74
    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 75
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 76
    invoke-virtual {p1, v2}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 77
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 78
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    .line 79
    const/4 v5, 0x1

    aget v5, v1, v5

    div-int/lit8 v6, v4, 0x2

    add-int/2addr v5, v6

    .line 80
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v6, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 81
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    if-ge v5, v2, :cond_0

    .line 83
    const/16 v2, 0x35

    aget v1, v1, v7

    sub-int v1, v6, v1

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v1, v3

    invoke-virtual {v0, v2, v1, v4}, Landroid/widget/Toast;->setGravity(III)V

    .line 89
    :goto_0
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 90
    return-void

    .line 87
    :cond_0
    const/16 v2, 0x53

    aget v1, v1, v7

    invoke-virtual {v0, v2, v1, v4}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 36
    invoke-static {p0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 37
    return-void
.end method

.method private static a(Landroid/widget/Toast;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 63
    invoke-static {p0, v0, v0}, Lcom/dropbox/android/util/bk;->a(Landroid/widget/Toast;II)V

    .line 64
    return-void
.end method

.method private static a(Landroid/widget/Toast;II)V
    .locals 1

    .prologue
    .line 67
    const/16 v0, 0x31

    invoke-virtual {p0, v0, p1, p2}, Landroid/widget/Toast;->setGravity(III)V

    .line 68
    return-void
.end method

.method public static b(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 57
    invoke-static {p0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 58
    invoke-static {v0}, Lcom/dropbox/android/util/bk;->a(Landroid/widget/Toast;)V

    .line 59
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 60
    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 44
    invoke-static {p0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 45
    invoke-static {v0}, Lcom/dropbox/android/util/bk;->a(Landroid/widget/Toast;)V

    .line 46
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 47
    return-void
.end method
