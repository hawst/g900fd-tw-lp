.class public final Lcom/dropbox/android/util/bu;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/service/D;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/dropbox/android/service/D",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/dropbox/android/service/E;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:Ldbxyzptlk/db231222/s/a;

.field private c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/dropbox/android/util/X;

.field private final e:Ljava/text/Collator;


# direct methods
.method public constructor <init>(ILjava/util/Locale;Ldbxyzptlk/db231222/n/P;Ldbxyzptlk/db231222/n/K;)V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput p1, p0, Lcom/dropbox/android/util/bu;->a:I

    .line 35
    invoke-virtual {p3}, Ldbxyzptlk/db231222/n/P;->g()Ldbxyzptlk/db231222/s/a;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/util/bu;->b:Ldbxyzptlk/db231222/s/a;

    .line 36
    new-instance v0, Ljava/util/HashSet;

    invoke-virtual {p4}, Ldbxyzptlk/db231222/n/K;->h()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/dropbox/android/util/bu;->c:Ljava/util/Set;

    .line 37
    new-instance v0, Lcom/dropbox/android/util/X;

    invoke-direct {v0}, Lcom/dropbox/android/util/X;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/util/bu;->d:Lcom/dropbox/android/util/X;

    .line 38
    invoke-static {p2}, Lcom/dropbox/android/util/bh;->a(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/util/bu;->e:Ljava/text/Collator;

    .line 39
    return-void
.end method

.method protected static a(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/util/X;Lcom/dropbox/android/util/bx;Ljava/text/Collator;)I
    .locals 11

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 150
    .line 153
    iget-object v0, p3, Lcom/dropbox/android/util/bx;->a:Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p3, Lcom/dropbox/android/util/bx;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_b

    .line 157
    iget-object v0, p3, Lcom/dropbox/android/util/bx;->a:Ljava/lang/String;

    .line 158
    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 159
    const-string v4, "[a-zA-Z .]+"

    invoke-virtual {v0, v4}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    .line 160
    if-eqz v0, :cond_a

    array-length v0, v5

    if-lt v0, v1, :cond_a

    array-length v0, v5

    const/4 v4, 0x3

    if-gt v0, v4, :cond_a

    move v0, v1

    .line 166
    :goto_0
    const-string v4, " "

    invoke-virtual {p0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 167
    array-length v6, v4

    if-lt v6, v1, :cond_3

    .line 168
    aget-object v6, v4, v3

    .line 169
    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    aget-object v7, v4, v1

    .line 171
    array-length v8, v5

    move v4, v3

    move v1, v3

    :goto_1
    if-ge v4, v8, :cond_2

    aget-object v9, v5, v4

    .line 172
    invoke-virtual {p4, v9, v7}, Ljava/text/Collator;->equals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    move v1, v2

    .line 175
    :cond_0
    invoke-virtual {p4, v9, v6}, Ljava/text/Collator;->equals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    move v3, v2

    .line 171
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 179
    :cond_2
    if-eqz v1, :cond_3

    if-nez v3, :cond_3

    .line 180
    add-int/lit8 v0, v0, 0x2

    .line 184
    :cond_3
    new-instance v1, Lcom/dropbox/android/util/Y;

    invoke-direct {v1, p1}, Lcom/dropbox/android/util/Y;-><init>(Ljava/lang/String;)V

    .line 185
    new-instance v2, Lcom/dropbox/android/util/Y;

    iget-object v3, p3, Lcom/dropbox/android/util/bx;->b:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/dropbox/android/util/Y;-><init>(Ljava/lang/String;)V

    .line 188
    invoke-virtual {p2, v2}, Lcom/dropbox/android/util/X;->b(Lcom/dropbox/android/util/Y;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 189
    add-int/lit8 v0, v0, 0x1

    .line 194
    :cond_4
    invoke-virtual {p2, v2}, Lcom/dropbox/android/util/X;->c(Lcom/dropbox/android/util/Y;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 195
    add-int/lit8 v0, v0, 0x1

    .line 202
    :cond_5
    :goto_2
    iget-boolean v1, p3, Lcom/dropbox/android/util/bx;->c:Z

    if-eqz v1, :cond_6

    .line 203
    add-int/lit8 v0, v0, 0x2

    .line 206
    :cond_6
    iget-boolean v1, p3, Lcom/dropbox/android/util/bx;->d:Z

    if-eqz v1, :cond_7

    .line 207
    add-int/lit8 v0, v0, 0x1

    .line 210
    :cond_7
    iget-boolean v1, p3, Lcom/dropbox/android/util/bx;->e:Z

    if-eqz v1, :cond_8

    .line 211
    add-int/lit8 v0, v0, 0x5

    .line 214
    :cond_8
    iget v1, p3, Lcom/dropbox/android/util/bx;->f:I

    add-int/2addr v0, v1

    .line 217
    :goto_3
    return v0

    .line 196
    :cond_9
    iget-object v1, v1, Lcom/dropbox/android/util/Y;->b:Ljava/lang/String;

    iget-object v2, v2, Lcom/dropbox/android/util/Y;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 197
    add-int/lit8 v0, v0, 0x2

    goto :goto_2

    :cond_a
    move v0, v2

    goto :goto_0

    :cond_b
    move v0, v3

    goto :goto_3
.end method

.method static synthetic a(Lcom/dropbox/android/util/bu;)Ljava/text/Collator;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/dropbox/android/util/bu;->e:Ljava/text/Collator;

    return-object v0
.end method

.method private a(Lcom/dropbox/android/util/bx;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 226
    iget-object v1, p1, Lcom/dropbox/android/util/bx;->a:Ljava/lang/String;

    .line 227
    iget-object v2, p1, Lcom/dropbox/android/util/bx;->b:Ljava/lang/String;

    .line 228
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v3, 0x28

    if-le v1, v3, :cond_1

    .line 241
    :cond_0
    :goto_0
    return v0

    .line 234
    :cond_1
    iget-object v1, p0, Lcom/dropbox/android/util/bu;->d:Lcom/dropbox/android/util/X;

    invoke-virtual {v1, v2}, Lcom/dropbox/android/util/X;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 238
    iget-object v1, p0, Lcom/dropbox/android/util/bu;->c:Ljava/util/Set;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 241
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/util/bu;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/service/E;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/service/E;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, -0x1

    const/4 v3, 0x0

    .line 57
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 58
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v2, v3

    .line 61
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 62
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/service/E;

    .line 64
    invoke-virtual {v0}, Lcom/dropbox/android/service/E;->c()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 65
    invoke-virtual {v0}, Lcom/dropbox/android/service/E;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 66
    new-instance v7, Lcom/dropbox/android/util/bx;

    invoke-direct {v7}, Lcom/dropbox/android/util/bx;-><init>()V

    .line 67
    invoke-virtual {v0}, Lcom/dropbox/android/service/E;->b()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/dropbox/android/util/bx;->a:Ljava/lang/String;

    .line 68
    iput-object v1, v7, Lcom/dropbox/android/util/bx;->b:Ljava/lang/String;

    .line 69
    iput v2, v7, Lcom/dropbox/android/util/bx;->g:I

    .line 70
    invoke-virtual {v0}, Lcom/dropbox/android/service/E;->f()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_2
    iput-boolean v1, v7, Lcom/dropbox/android/util/bx;->c:Z

    .line 71
    invoke-virtual {v0}, Lcom/dropbox/android/service/E;->h()Z

    move-result v1

    iput-boolean v1, v7, Lcom/dropbox/android/util/bx;->d:Z

    .line 72
    invoke-virtual {v0}, Lcom/dropbox/android/service/E;->e()Z

    move-result v1

    iput-boolean v1, v7, Lcom/dropbox/android/util/bx;->e:Z

    .line 73
    invoke-virtual {v0}, Lcom/dropbox/android/service/E;->d()I

    move-result v1

    iput v1, v7, Lcom/dropbox/android/util/bx;->f:I

    .line 74
    invoke-direct {p0, v7}, Lcom/dropbox/android/util/bu;->a(Lcom/dropbox/android/util/bx;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75
    iget-object v1, p0, Lcom/dropbox/android/util/bu;->b:Ldbxyzptlk/db231222/s/a;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->l()Ljava/lang/String;

    move-result-object v1

    iget-object v8, p0, Lcom/dropbox/android/util/bu;->b:Ldbxyzptlk/db231222/s/a;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/s/a;->d()Ldbxyzptlk/db231222/s/q;

    move-result-object v8

    invoke-virtual {v8}, Ldbxyzptlk/db231222/s/q;->i()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/dropbox/android/util/bu;->d:Lcom/dropbox/android/util/X;

    iget-object v10, p0, Lcom/dropbox/android/util/bu;->e:Ljava/text/Collator;

    invoke-static {v1, v8, v9, v7, v10}, Lcom/dropbox/android/util/bu;->a(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/util/X;Lcom/dropbox/android/util/bx;Ljava/text/Collator;)I

    move-result v1

    iput v1, v7, Lcom/dropbox/android/util/bx;->h:I

    .line 77
    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move v1, v3

    .line 70
    goto :goto_2

    .line 61
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 83
    :cond_3
    new-instance v0, Lcom/dropbox/android/util/bv;

    invoke-direct {v0, p0}, Lcom/dropbox/android/util/bv;-><init>(Lcom/dropbox/android/util/bu;)V

    invoke-static {v5, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 96
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/service/E;

    .line 97
    :goto_3
    invoke-virtual {v0}, Lcom/dropbox/android/service/E;->c()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_4

    .line 98
    invoke-virtual {v0, v3}, Lcom/dropbox/android/service/E;->c(I)V

    goto :goto_3

    .line 103
    :cond_5
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [I

    .line 104
    invoke-static {v2, v11}, Ljava/util/Arrays;->fill([II)V

    .line 105
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/bx;

    .line 106
    iget v1, v0, Lcom/dropbox/android/util/bx;->g:I

    aget v1, v2, v1

    if-ne v1, v11, :cond_6

    .line 107
    iget v1, v0, Lcom/dropbox/android/util/bx;->g:I

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    aput v5, v2, v1

    .line 108
    iget v1, v0, Lcom/dropbox/android/util/bx;->g:I

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    :cond_6
    iget v1, v0, Lcom/dropbox/android/util/bx;->g:I

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/service/E;

    iget-object v0, v0, Lcom/dropbox/android/util/bx;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/service/E;->d(Ljava/lang/String;)V

    goto :goto_4

    .line 112
    :cond_7
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/dropbox/android/util/bu;->a:I

    if-lt v0, v1, :cond_8

    .line 113
    iget v0, p0, Lcom/dropbox/android/util/bu;->a:I

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v4, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/dropbox/android/util/bw;

    invoke-direct {v1, p0}, Lcom/dropbox/android/util/bw;-><init>(Lcom/dropbox/android/util/bu;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 120
    :cond_8
    return-object v4
.end method
