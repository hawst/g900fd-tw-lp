.class public Lcom/dropbox/android/util/V;
.super Lcom/dropbox/android/util/analytics/l;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/v/q;


# static fields
.field private static final b:Ljava/lang/String;

.field private static final c:[Ljava/lang/String;


# instance fields
.field private d:J

.field private final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/lang/Long;

.field private h:J

.field private i:J

.field private j:Z

.field private k:Lcom/dropbox/android/service/J;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 63
    const-class v0, Lcom/dropbox/android/util/V;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/util/V;->b:Ljava/lang/String;

    .line 66
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "event"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "request.method"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "request.startTime"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "response.wallTime"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "logging.ready"

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/util/V;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 125
    const-string v0, "network.log"

    invoke-direct {p0, v0}, Lcom/dropbox/android/util/analytics/l;-><init>(Ljava/lang/String;)V

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/util/V;->e:Ljava/util/ArrayList;

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/util/V;->f:Ljava/util/ArrayList;

    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/util/V;->g:Ljava/lang/Long;

    .line 116
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/util/V;->j:Z

    .line 127
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/i/d;->a()Ldbxyzptlk/db231222/i/c;

    move-result-object v0

    .line 128
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/G;->b()Lcom/dropbox/android/service/J;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/util/V;->k:Lcom/dropbox/android/service/J;

    .line 130
    const-string v1, "system.carrier"

    iget-object v0, v0, Ldbxyzptlk/db231222/i/c;->f:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcom/dropbox/android/util/V;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 131
    const-string v0, "system.manufacturer"

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/V;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 132
    const-string v0, "system.locale"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/V;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 134
    const-string v0, "network.connected"

    iget-object v1, p0, Lcom/dropbox/android/util/V;->k:Lcom/dropbox/android/service/J;

    invoke-virtual {v1}, Lcom/dropbox/android/service/J;->a()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/V;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 135
    const-string v0, "network.wifi"

    iget-object v1, p0, Lcom/dropbox/android/util/V;->k:Lcom/dropbox/android/service/J;

    invoke-virtual {v1}, Lcom/dropbox/android/service/J;->b()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/V;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 136
    const-string v0, "network.roaming"

    iget-object v1, p0, Lcom/dropbox/android/util/V;->k:Lcom/dropbox/android/service/J;

    invoke-virtual {v1}, Lcom/dropbox/android/service/J;->d()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/V;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 137
    const-string v0, "network.3g"

    iget-object v1, p0, Lcom/dropbox/android/util/V;->k:Lcom/dropbox/android/service/J;

    invoke-virtual {v1}, Lcom/dropbox/android/service/J;->c()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/V;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 140
    return-void
.end method

.method private a(JJ)V
    .locals 5

    .prologue
    .line 247
    iput-wide p3, p0, Lcom/dropbox/android/util/V;->i:J

    .line 248
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/util/V;->h:J

    .line 249
    iget-object v0, p0, Lcom/dropbox/android/util/V;->f:Ljava/util/ArrayList;

    iget-wide v1, p0, Lcom/dropbox/android/util/V;->h:J

    iget-wide v3, p0, Lcom/dropbox/android/util/V;->i:J

    sub-long/2addr v1, v3

    const-wide/32 v3, 0xf4240

    div-long/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 250
    iget-object v0, p0, Lcom/dropbox/android/util/V;->e:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 251
    return-void
.end method

.method public static a()Z
    .locals 4

    .prologue
    .line 149
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide v2, 0x3fa999999999999aL    # 0.05

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Z
    .locals 2

    .prologue
    .line 326
    iget-object v0, p0, Lcom/dropbox/android/util/V;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/util/V;->c:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(J)V
    .locals 3

    .prologue
    .line 172
    iput-wide p1, p0, Lcom/dropbox/android/util/V;->d:J

    .line 173
    const-string v0, "request.startTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/dropbox/android/util/V;->d(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/V;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 174
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/util/V;->g:Ljava/lang/Long;

    .line 175
    return-void
.end method

.method public final a(Lorg/apache/http/HttpResponse;Z)V
    .locals 3

    .prologue
    .line 190
    const-string v0, "response.wallTime"

    invoke-virtual {p0}, Lcom/dropbox/android/util/V;->c()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/dropbox/android/util/V;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    .line 191
    const-string v0, "response.status"

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/dropbox/android/util/V;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    .line 192
    const-string v0, "response.size"

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/dropbox/android/util/V;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    .line 194
    const-string v0, "x-dropbox-request-id"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 195
    if-eqz v0, :cond_0

    .line 196
    const-string v1, "request_id"

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/dropbox/android/util/V;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 198
    :cond_0
    const-string v0, "x-server-response-time"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 199
    if-eqz v0, :cond_1

    .line 200
    const-string v1, "server_response_time"

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/dropbox/android/util/V;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 203
    :cond_1
    if-nez p2, :cond_2

    .line 204
    const-string v0, "logging.ready"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/V;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 206
    :cond_2
    return-void
.end method

.method public final a(Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 2

    .prologue
    .line 160
    const-string v0, "request.method"

    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getMethod()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/V;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 161
    const-string v0, "request.path"

    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/V;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 162
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 179
    const-wide/16 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/V;->a(J)V

    .line 180
    return-void
.end method

.method public final declared-synchronized b(J)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x1

    .line 225
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/dropbox/android/util/V;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 244
    :goto_0
    monitor-exit p0

    return-void

    .line 229
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/util/V;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 230
    iget-object v0, p0, Lcom/dropbox/android/util/V;->g:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/dropbox/android/util/V;->a(JJ)V

    .line 231
    iget-wide v0, p0, Lcom/dropbox/android/util/V;->d:J

    sub-long/2addr v0, v8

    iput-wide v0, p0, Lcom/dropbox/android/util/V;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 225
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 233
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/dropbox/android/util/V;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    .line 234
    iget-object v0, p0, Lcom/dropbox/android/util/V;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 235
    add-long v4, v2, p1

    const-wide/16 v6, 0x4000

    cmp-long v0, v4, v6

    if-gtz v0, :cond_2

    .line 236
    iget-object v0, p0, Lcom/dropbox/android/util/V;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 237
    iget-object v0, p0, Lcom/dropbox/android/util/V;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 238
    add-long v0, v2, p1

    iget-wide v2, p0, Lcom/dropbox/android/util/V;->i:J

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/dropbox/android/util/V;->a(JJ)V

    goto :goto_0

    .line 240
    :cond_2
    iget-wide v0, p0, Lcom/dropbox/android/util/V;->h:J

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/dropbox/android/util/V;->a(JJ)V

    .line 241
    iget-wide v0, p0, Lcom/dropbox/android/util/V;->d:J

    sub-long/2addr v0, v8

    iput-wide v0, p0, Lcom/dropbox/android/util/V;->d:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final c()J
    .locals 4

    .prologue
    .line 283
    iget-object v0, p0, Lcom/dropbox/android/util/V;->g:Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 284
    invoke-virtual {p0}, Lcom/dropbox/android/util/V;->d()V

    .line 285
    const-wide/16 v0, -0x1

    .line 287
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/dropbox/android/util/V;->g:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    goto :goto_0
.end method

.method public final c(J)V
    .locals 3

    .prologue
    .line 260
    const-string v0, "stream.updates.bytes"

    iget-object v1, p0, Lcom/dropbox/android/util/V;->e:Ljava/util/ArrayList;

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/V;->a(Ljava/lang/String;Ljava/util/List;)Lcom/dropbox/android/util/analytics/l;

    .line 261
    const-string v0, "stream.updates.time"

    iget-object v1, p0, Lcom/dropbox/android/util/V;->f:Ljava/util/ArrayList;

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/V;->a(Ljava/lang/String;Ljava/util/List;)Lcom/dropbox/android/util/analytics/l;

    .line 263
    const-string v0, "stream.total.bytes"

    invoke-virtual {p0, v0, p1, p2}, Lcom/dropbox/android/util/V;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    .line 264
    const-string v0, "stream.total.time"

    invoke-virtual {p0}, Lcom/dropbox/android/util/V;->c()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/dropbox/android/util/V;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    .line 272
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/G;->b()Lcom/dropbox/android/service/J;

    move-result-object v0

    .line 273
    iget-object v1, p0, Lcom/dropbox/android/util/V;->k:Lcom/dropbox/android/service/J;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/J;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 274
    invoke-virtual {p0}, Lcom/dropbox/android/util/V;->d()V

    .line 279
    :goto_0
    return-void

    .line 278
    :cond_0
    const-string v0, "logging.ready"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/V;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 293
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/util/V;->j:Z

    .line 294
    return-void
.end method

.method public final e()V
    .locals 5

    .prologue
    .line 299
    const-string v0, "run a dev build for more debugging info"

    .line 304
    iget-boolean v1, p0, Lcom/dropbox/android/util/V;->j:Z

    if-eqz v1, :cond_0

    .line 305
    sget-object v1, Lcom/dropbox/android/util/V;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not logged; event was invalid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    :goto_0
    return-void

    .line 308
    :cond_0
    invoke-direct {p0}, Lcom/dropbox/android/util/V;->g()Z

    move-result v1

    if-nez v1, :cond_1

    .line 309
    sget-object v1, Lcom/dropbox/android/util/V;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not logged; missing keys: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 312
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/util/V;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    int-to-long v1, v1

    const-wide/32 v3, 0x380000

    cmp-long v1, v1, v3

    if-ltz v1, :cond_2

    .line 313
    sget-object v1, Lcom/dropbox/android/util/V;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not logged; too large: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 316
    :cond_2
    sget-object v0, Lcom/dropbox/android/util/V;->b:Ljava/lang/String;

    const-string v1, "Writing event to log."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    invoke-super {p0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_0
.end method
