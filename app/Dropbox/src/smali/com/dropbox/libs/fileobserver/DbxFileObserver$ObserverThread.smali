.class Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;
.super Ljava/lang/Thread;
.source "panda.py"


# instance fields
.field private final a:[I

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/dropbox/libs/fileobserver/DbxFileObserver;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method private constructor <init>(Lcom/dropbox/libs/fileobserver/DbxFileObserver;)V
    .locals 1

    .prologue
    .line 179
    const-string v0, "DbxFileObserverThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 168
    const/4 v0, 0x3

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->a:[I

    .line 171
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->b:Ljava/util/Set;

    .line 176
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 180
    iget-object v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->a:[I

    invoke-direct {p0, v0}, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->nativeInit([I)V

    .line 181
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->c:Ljava/lang/ref/WeakReference;

    .line 182
    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/libs/fileobserver/DbxFileObserver;Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;)V
    .locals 0

    .prologue
    .line 178
    invoke-direct {p0, p1}, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;-><init>(Lcom/dropbox/libs/fileobserver/DbxFileObserver;)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 226
    invoke-direct {p0, p1, p2}, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;I)I
    .locals 4

    .prologue
    .line 227
    iget-object v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->a:[I

    invoke-direct {p0, v0, p1, p2}, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->nativeAddOrUpdateWatch([ILjava/lang/String;I)I

    move-result v0

    .line 228
    iget-object v1, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->b:Ljava/util/Set;

    monitor-enter v1

    .line 229
    :try_start_0
    iget-object v2, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->b:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 228
    monitor-exit v1

    .line 231
    return v0

    .line 228
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 191
    iget-object v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    :goto_0
    return-void

    .line 196
    :cond_0
    iget-object v1, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->b:Ljava/util/Set;

    monitor-enter v1

    .line 197
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->b:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 196
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 204
    iget-object v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->a:[I

    invoke-direct {p0, v0}, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->nativeStopObserve([I)V

    goto :goto_0

    .line 196
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 200
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 201
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->a(I)Z

    goto :goto_1
.end method

.method private a(I)Z
    .locals 3

    .prologue
    .line 235
    iget-object v1, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->b:Ljava/util/Set;

    monitor-enter v1

    .line 236
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->b:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 235
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->a:[I

    invoke-direct {p0, v0, p1}, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->nativeRemoveWatch([II)V
    :try_end_1
    .catch Lcom/dropbox/libs/fileobserver/exceptions/InvalidWatchDescriptorException; {:try_start_1 .. :try_end_1} :catch_0

    .line 244
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 235
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 241
    :catch_0
    move-exception v0

    .line 242
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;I)Z
    .locals 1

    .prologue
    .line 234
    invoke-direct {p0, p1}, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->a(I)Z

    move-result v0

    return v0
.end method

.method private native nativeAddOrUpdateWatch([ILjava/lang/String;I)I
.end method

.method private native nativeInit([I)V
.end method

.method private native nativeObserve([I)V
.end method

.method private native nativeRemoveWatch([II)V
.end method

.method private native nativeStopObserve([I)V
.end method

.method private declared-synchronized onEvent(IILjava/lang/String;)V
    .locals 3

    .prologue
    .line 215
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/libs/fileobserver/DbxFileObserver;

    .line 216
    if-eqz v0, :cond_0

    .line 217
    invoke-virtual {v0, p1, p2, p3}, Lcom/dropbox/libs/fileobserver/DbxFileObserver;->a(IILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 219
    :catch_0
    move-exception v0

    .line 222
    :try_start_1
    sget-object v1, Lcom/dropbox/libs/fileobserver/DbxFileObserver;->b:Ljava/lang/String;

    const-string v2, "Exception in DbxFileObserver onEvent!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 215
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public interrupt()V
    .locals 0

    .prologue
    .line 209
    invoke-direct {p0}, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->a()V

    .line 210
    invoke-super {p0}, Ljava/lang/Thread;->interrupt()V

    .line 211
    return-void
.end method

.method public run()V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->a:[I

    invoke-direct {p0, v0}, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->nativeObserve([I)V

    .line 187
    return-void
.end method
