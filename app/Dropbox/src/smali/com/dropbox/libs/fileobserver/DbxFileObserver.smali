.class public abstract Lcom/dropbox/libs/fileobserver/DbxFileObserver;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field static final b:Ljava/lang/String;


# instance fields
.field private final a:Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;

.field private c:Z

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 103
    const-string v0, "DbxFileObserver"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 160
    const-class v0, Lcom/dropbox/libs/fileobserver/DbxFileObserver;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/libs/fileobserver/DbxFileObserver;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254
    iput-boolean v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver;->c:Z

    .line 255
    iput-boolean v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver;->d:Z

    .line 274
    new-instance v0, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;-><init>(Lcom/dropbox/libs/fileobserver/DbxFileObserver;Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;)V

    iput-object v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver;->a:Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;

    .line 275
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 299
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver;->c:Z

    if-eqz v0, :cond_0

    .line 300
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "observer is shut down"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 302
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver;->d:Z

    if-nez v0, :cond_1

    .line 303
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver;->d:Z

    .line 304
    iget-object v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver;->a:Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;

    invoke-virtual {v0}, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->start()V

    .line 306
    :cond_1
    iget-object v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver;->a:Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;

    invoke-static {v0, p1, p2}, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->a(Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;Ljava/lang/String;I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    monitor-exit p0

    return v0
.end method

.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 331
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver;->c:Z

    .line 332
    iget-boolean v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver;->a:Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;

    invoke-virtual {v0}, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    iget-object v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver;->a:Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;

    invoke-virtual {v0}, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->interrupt()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 335
    :cond_0
    monitor-exit p0

    return-void

    .line 331
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract a(IILjava/lang/String;)V
.end method

.method public final declared-synchronized a(I)Z
    .locals 2

    .prologue
    .line 319
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver;->c:Z

    if-eqz v0, :cond_0

    .line 320
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "observer is shut down"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 319
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 322
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver;->a:Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;

    invoke-static {v0, p1}, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->a(Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    monitor-exit p0

    return v0
.end method

.method protected finalize()V
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/dropbox/libs/fileobserver/DbxFileObserver;->a:Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;

    invoke-virtual {v0}, Lcom/dropbox/libs/fileobserver/DbxFileObserver$ObserverThread;->interrupt()V

    .line 361
    return-void
.end method
