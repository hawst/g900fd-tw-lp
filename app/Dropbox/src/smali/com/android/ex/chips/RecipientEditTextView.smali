.class public Lcom/android/ex/chips/RecipientEditTextView;
.super Landroid/widget/MultiAutoCompleteTextView;
.source "panda.py"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroid/view/ActionMode$Callback;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/android/ex/chips/y;


# static fields
.field private static final ac:Ljava/util/regex/Pattern;

.field private static ai:I

.field private static final c:Ljava/lang/String;

.field private static d:I

.field private static e:I

.field private static f:I

.field private static g:I


# instance fields
.field private A:Landroid/text/style/ImageSpan;

.field private B:Landroid/widget/TextView;

.field private C:Landroid/os/Handler;

.field private D:I

.field private E:Z

.field private F:Landroid/widget/ListPopupWindow;

.field private G:Landroid/widget/ListPopupWindow;

.field private H:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ldbxyzptlk/db231222/d/b;",
            ">;"
        }
    .end annotation
.end field

.field private I:Z

.field private J:Z

.field private K:Z

.field private L:Z

.field private M:Z

.field private N:Z

.field private O:Z

.field private P:Landroid/view/GestureDetector;

.field private Q:Landroid/app/Dialog;

.field private R:Ljava/lang/String;

.field private S:Landroid/widget/LinearLayout;

.field private T:Landroid/widget/AdapterView$OnItemClickListener;

.field private U:I

.field private V:Landroid/text/TextWatcher;

.field private W:Landroid/widget/ScrollView;

.field private Z:Z

.field final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private aa:Z

.field private final ab:Landroid/widget/ListView;

.field private final ad:Ljava/lang/Runnable;

.field private ae:Lcom/android/ex/chips/N;

.field private af:Ljava/lang/Runnable;

.field private ag:Ljava/lang/Runnable;

.field private ah:I

.field private aj:I

.field private ak:Lcom/android/ex/chips/X;

.field private al:Lcom/android/ex/chips/M;

.field private am:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/ex/chips/RecipientEditTextView$ChipState;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ldbxyzptlk/db231222/d/b;",
            ">;"
        }
    .end annotation
.end field

.field private h:Landroid/graphics/drawable/Drawable;

.field private i:Landroid/graphics/drawable/Drawable;

.field private j:Landroid/graphics/drawable/Drawable;

.field private k:Landroid/graphics/drawable/Drawable;

.field private l:Landroid/graphics/drawable/Drawable;

.field private m:Landroid/graphics/drawable/Drawable;

.field private n:Landroid/graphics/drawable/Drawable;

.field private o:Landroid/graphics/drawable/Drawable;

.field private p:Landroid/graphics/drawable/Drawable;

.field private q:F

.field private r:I

.field private s:F

.field private t:F

.field private u:I

.field private v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

.field private w:Landroid/widget/AutoCompleteTextView$Validator;

.field private x:Ldbxyzptlk/db231222/d/b;

.field private y:I

.field private z:Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x2c

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x20

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/ex/chips/RecipientEditTextView;->c:Ljava/lang/String;

    .line 128
    const-string v0, "dismiss"

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    sput v0, Lcom/android/ex/chips/RecipientEditTextView;->d:I

    .line 138
    sput v2, Lcom/android/ex/chips/RecipientEditTextView;->e:I

    .line 140
    const/16 v0, 0xa

    sput v0, Lcom/android/ex/chips/RecipientEditTextView;->f:I

    .line 142
    const/4 v0, 0x4

    sput v0, Lcom/android/ex/chips/RecipientEditTextView;->g:I

    .line 258
    const-string v0, "(\\+[0-9]+[\\- \\.]*)?(1?[ ]*\\([0-9]+\\)[\\- \\.]*)?([0-9][0-9\\- \\.][0-9\\- \\.]+[0-9])"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/ex/chips/RecipientEditTextView;->ac:Ljava/util/regex/Pattern;

    .line 296
    sput v2, Lcom/android/ex/chips/RecipientEditTextView;->ai:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 323
    invoke-direct {p0, p1, p2}, Landroid/widget/MultiAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 145
    iput-object v3, p0, Lcom/android/ex/chips/RecipientEditTextView;->h:Landroid/graphics/drawable/Drawable;

    .line 147
    iput-object v3, p0, Lcom/android/ex/chips/RecipientEditTextView;->i:Landroid/graphics/drawable/Drawable;

    .line 188
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->a:Ljava/util/ArrayList;

    .line 192
    iput v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->D:I

    .line 194
    iput-boolean v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->E:Z

    .line 207
    iput-boolean v2, p0, Lcom/android/ex/chips/RecipientEditTextView;->I:Z

    .line 209
    iput-boolean v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->J:Z

    .line 211
    iput-boolean v2, p0, Lcom/android/ex/chips/RecipientEditTextView;->K:Z

    .line 213
    iput-boolean v2, p0, Lcom/android/ex/chips/RecipientEditTextView;->L:Z

    .line 215
    iput-boolean v2, p0, Lcom/android/ex/chips/RecipientEditTextView;->M:Z

    .line 217
    iput-boolean v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->N:Z

    .line 219
    iput-boolean v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->O:Z

    .line 246
    iput-boolean v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->aa:Z

    .line 264
    new-instance v0, Lcom/android/ex/chips/A;

    invoke-direct {v0, p0}, Lcom/android/ex/chips/A;-><init>(Lcom/android/ex/chips/RecipientEditTextView;)V

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->ad:Ljava/lang/Runnable;

    .line 276
    new-instance v0, Lcom/android/ex/chips/B;

    invoke-direct {v0, p0}, Lcom/android/ex/chips/B;-><init>(Lcom/android/ex/chips/RecipientEditTextView;)V

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->af:Ljava/lang/Runnable;

    .line 285
    new-instance v0, Lcom/android/ex/chips/C;

    invoke-direct {v0, p0}, Lcom/android/ex/chips/C;-><init>(Lcom/android/ex/chips/RecipientEditTextView;)V

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->ag:Ljava/lang/Runnable;

    .line 324
    invoke-direct {p0, p1, p2}, Lcom/android/ex/chips/RecipientEditTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 325
    sget v0, Lcom/android/ex/chips/RecipientEditTextView;->e:I

    if-ne v0, v4, :cond_0

    .line 326
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x106000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/android/ex/chips/RecipientEditTextView;->e:I

    .line 328
    :cond_0
    new-instance v0, Landroid/widget/ListPopupWindow;

    invoke-direct {v0, p1}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->F:Landroid/widget/ListPopupWindow;

    .line 329
    new-instance v0, Landroid/widget/ListPopupWindow;

    invoke-direct {v0, p1}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->G:Landroid/widget/ListPopupWindow;

    .line 330
    new-instance v0, Landroid/app/Dialog;

    invoke-direct {v0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->Q:Landroid/app/Dialog;

    .line 331
    new-instance v0, Landroid/widget/ListView;

    invoke-direct {v0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->ab:Landroid/widget/ListView;

    .line 332
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 333
    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->ab:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 334
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->ab:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 335
    new-instance v0, Lcom/android/ex/chips/S;

    invoke-direct {v0, p0, v3}, Lcom/android/ex/chips/S;-><init>(Lcom/android/ex/chips/RecipientEditTextView;Lcom/android/ex/chips/A;)V

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->al:Lcom/android/ex/chips/M;

    .line 336
    new-instance v0, Lcom/android/ex/chips/D;

    invoke-direct {v0, p0}, Lcom/android/ex/chips/D;-><init>(Lcom/android/ex/chips/RecipientEditTextView;)V

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->T:Landroid/widget/AdapterView$OnItemClickListener;

    .line 349
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->A()V

    .line 350
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getInputType()I

    move-result v0

    const/high16 v1, 0x80000

    or-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->setInputType(I)V

    .line 351
    invoke-virtual {p0, p0}, Lcom/android/ex/chips/RecipientEditTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 352
    invoke-virtual {p0, p0}, Lcom/android/ex/chips/RecipientEditTextView;->setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V

    .line 353
    new-instance v0, Lcom/android/ex/chips/E;

    invoke-direct {v0, p0}, Lcom/android/ex/chips/E;-><init>(Lcom/android/ex/chips/RecipientEditTextView;)V

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->C:Landroid/os/Handler;

    .line 363
    new-instance v0, Lcom/android/ex/chips/W;

    invoke-direct {v0, p0, v3}, Lcom/android/ex/chips/W;-><init>(Lcom/android/ex/chips/RecipientEditTextView;Lcom/android/ex/chips/A;)V

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->V:Landroid/text/TextWatcher;

    .line 364
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->V:Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 365
    new-instance v0, Landroid/view/GestureDetector;

    invoke-direct {v0, p1, p0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->P:Landroid/view/GestureDetector;

    .line 366
    invoke-virtual {p0, p0}, Lcom/android/ex/chips/RecipientEditTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 367
    return-void
.end method

.method private A()V
    .locals 6

    .prologue
    const/4 v4, -0x2

    const/4 v5, 0x0

    .line 1212
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getMeasuredHeight()I

    move-result v0

    .line 1213
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getMeasuredWidth()I

    move-result v1

    .line 1214
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 1217
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v3}, Lcom/android/ex/chips/RecipientEditTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1219
    const-string v3, "a"

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/android/ex/chips/RecipientEntry;->a(Ljava/lang/String;Z)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v3

    invoke-virtual {p0, v3, v5}, Lcom/android/ex/chips/RecipientEditTextView;->a(Lcom/android/ex/chips/RecipientEntry;Z)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/ex/chips/RecipientEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1221
    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {p0, v3, v4}, Lcom/android/ex/chips/RecipientEditTextView;->measure(II)V

    .line 1223
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getMeasuredHeight()I

    move-result v3

    iput v3, p0, Lcom/android/ex/chips/RecipientEditTextView;->r:I

    .line 1225
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/android/ex/chips/RecipientEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1226
    if-eqz v2, :cond_0

    .line 1228
    invoke-virtual {p0, v2}, Lcom/android/ex/chips/RecipientEditTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1230
    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/android/ex/chips/RecipientEditTextView;->setMeasuredDimension(II)V

    .line 1231
    return-void
.end method

.method private B()V
    .locals 2

    .prologue
    .line 1290
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->C:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->af:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1291
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->C:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->af:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1292
    return-void
.end method

.method private C()V
    .locals 6

    .prologue
    .line 1296
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->k()[Ldbxyzptlk/db231222/d/b;

    move-result-object v1

    .line 1297
    if-eqz v1, :cond_1

    .line 1299
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1300
    invoke-interface {v3}, Ldbxyzptlk/db231222/d/b;->g()Landroid/graphics/Rect;

    move-result-object v4

    .line 1301
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getWidth()I

    move-result v5

    if-lez v5, :cond_0

    iget v5, v4, Landroid/graphics/Rect;->right:I

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int v4, v5, v4

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getWidth()I

    move-result v5

    if-le v4, v5, :cond_0

    .line 1303
    invoke-interface {v3}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/android/ex/chips/RecipientEditTextView;->a(Ldbxyzptlk/db231222/d/b;Lcom/android/ex/chips/RecipientEntry;)V

    .line 1299
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1307
    :cond_1
    return-void
.end method

.method private D()Z
    .locals 1

    .prologue
    .line 1604
    const/16 v0, 0x82

    invoke-virtual {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    .line 1605
    if-eqz v0, :cond_0

    .line 1606
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 1607
    const/4 v0, 0x1

    .line 1609
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private E()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1621
    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    if-nez v1, :cond_1

    .line 1639
    :cond_0
    :goto_0
    return v0

    .line 1624
    :cond_1
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 1625
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getSelectionEnd()I

    move-result v2

    .line 1626
    iget-object v3, p0, Lcom/android/ex/chips/RecipientEditTextView;->v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v3, v1, v2}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v3

    .line 1628
    invoke-direct {p0, v3, v2}, Lcom/android/ex/chips/RecipientEditTextView;->b(II)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1629
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v0, v4, v3}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenEnd(Ljava/lang/CharSequence;I)I

    move-result v0

    .line 1632
    invoke-virtual {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->b(I)I

    move-result v0

    .line 1633
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getSelectionEnd()I

    move-result v4

    if-eq v0, v4, :cond_2

    .line 1634
    invoke-direct {p0, v3, v0}, Lcom/android/ex/chips/RecipientEditTextView;->d(II)V

    .line 1635
    const/4 v0, 0x1

    goto :goto_0

    .line 1637
    :cond_2
    invoke-direct {p0, v3, v2, v1}, Lcom/android/ex/chips/RecipientEditTextView;->a(IILandroid/text/Editable;)Z

    move-result v0

    goto :goto_0
.end method

.method private F()Z
    .locals 2

    .prologue
    .line 1748
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Spannable;->length()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->c(II)Z

    move-result v0

    return v0
.end method

.method private G()V
    .locals 1

    .prologue
    .line 1897
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    if-eqz v0, :cond_0

    .line 1898
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    invoke-direct {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->j(Ldbxyzptlk/db231222/d/b;)V

    .line 1899
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    .line 1901
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->setCursorVisible(Z)V

    .line 1902
    return-void
.end method

.method private H()Z
    .locals 1

    .prologue
    .line 2711
    iget v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->D:I

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->H:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->H:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private I()V
    .locals 4

    .prologue
    .line 2886
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->p()Ljava/util/ArrayList;

    move-result-object v0

    .line 2887
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 2889
    new-instance v1, Lcom/android/ex/chips/N;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/ex/chips/N;-><init>(Lcom/android/ex/chips/RecipientEditTextView;Lcom/android/ex/chips/A;)V

    .line 2890
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/util/ArrayList;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/android/ex/chips/N;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2892
    :cond_0
    return-void
.end method

.method private static a(Landroid/text/TextPaint;I)F
    .locals 3

    .prologue
    .line 1061
    div-int/lit8 v0, p1, 0x2

    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/text/TextPaint;->descent()F

    move-result v1

    invoke-virtual {p0}, Landroid/text/TextPaint;->ascent()F

    move-result v2

    add-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    return v0
.end method

.method private static a(Landroid/text/Editable;I)I
    .locals 2

    .prologue
    .line 2043
    invoke-interface {p0, p1}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    .line 2046
    :goto_0
    return p1

    :cond_0
    const/4 p1, -0x1

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/ex/chips/RecipientEditTextView;I)I
    .locals 0

    .prologue
    .line 112
    iput p1, p0, Lcom/android/ex/chips/RecipientEditTextView;->U:I

    return p1
.end method

.method private a(Lcom/android/ex/chips/RecipientEntry;Landroid/text/TextPaint;)Landroid/graphics/Bitmap;
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 867
    iget v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->q:F

    float-to-int v9, v0

    .line 868
    sget v0, Lcom/android/ex/chips/RecipientEditTextView;->f:I

    sub-int v0, v9, v0

    .line 870
    const/4 v1, 0x1

    new-array v3, v1, [F

    .line 871
    iget v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->u:I

    .line 872
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->d(Lcom/android/ex/chips/RecipientEntry;)Z

    move-result v10

    .line 873
    if-eqz v10, :cond_2

    move v7, v0

    .line 878
    :goto_0
    iget-boolean v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->K:Z

    if-nez v1, :cond_4

    move v8, v2

    .line 882
    :goto_1
    const-string v1, " "

    invoke-virtual {p2, v1, v3}, Landroid/text/TextPaint;->getTextWidths(Ljava/lang/String;[F)I

    .line 883
    invoke-virtual {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->c(Lcom/android/ex/chips/RecipientEntry;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->z()F

    move-result v4

    int-to-float v5, v8

    sub-float/2addr v4, v5

    int-to-float v5, v0

    sub-float/2addr v4, v5

    aget v3, v3, v2

    sub-float v3, v4, v3

    invoke-direct {p0, v1, p2, v3}, Lcom/android/ex/chips/RecipientEditTextView;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;F)Ljava/lang/CharSequence;

    move-result-object v1

    .line 888
    mul-int/lit8 v3, v9, 0x2

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    invoke-virtual {p2, v1, v2, v4}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v4

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v4, v4

    iget v5, p0, Lcom/android/ex/chips/RecipientEditTextView;->u:I

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    add-int/2addr v4, v8

    add-int/2addr v0, v4

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 893
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v11, v9, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 894
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v12}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 895
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->e(Lcom/android/ex/chips/RecipientEntry;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 896
    if-eqz v3, :cond_3

    .line 897
    invoke-virtual {v3, v2, v2, v11, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 898
    invoke-virtual {v3, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 899
    sget v3, Lcom/android/ex/chips/RecipientEditTextView;->e:I

    invoke-virtual {p2, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 901
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    int-to-float v4, v7

    invoke-static {p2, v9}, Lcom/android/ex/chips/RecipientEditTextView;->a(Landroid/text/TextPaint;I)F

    move-result v5

    move-object v6, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 904
    if-eqz v10, :cond_0

    .line 905
    invoke-direct {p0, v9, v7}, Lcom/android/ex/chips/RecipientEditTextView;->a(II)Landroid/graphics/Rect;

    move-result-object v1

    .line 906
    iget-object v2, p0, Lcom/android/ex/chips/RecipientEditTextView;->k:Landroid/graphics/drawable/Drawable;

    iget v3, v1, Landroid/graphics/Rect;->left:I

    iget v4, v1, Landroid/graphics/Rect;->top:I

    iget v5, v1, Landroid/graphics/Rect;->right:I

    sub-int v5, v7, v5

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sub-int v1, v9, v1

    invoke-virtual {v2, v3, v4, v5, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 908
    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 910
    :cond_0
    iget-boolean v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->K:Z

    if-eqz v1, :cond_1

    .line 911
    div-int/lit8 v1, v8, 0x4

    .line 912
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 914
    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->i:Landroid/graphics/drawable/Drawable;

    sub-int v3, v11, v8

    iget v4, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v4

    iget v4, v2, Landroid/graphics/Rect;->top:I

    add-int/lit8 v4, v4, 0x0

    iget v5, v2, Landroid/graphics/Rect;->right:I

    sub-int v5, v11, v5

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int v2, v9, v2

    invoke-virtual {v1, v3, v4, v5, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 918
    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 923
    :cond_1
    :goto_2
    return-object v12

    :cond_2
    move v7, v1

    move v0, v2

    .line 876
    goto/16 :goto_0

    .line 921
    :cond_3
    const-string v0, "RecipientEditTextView"

    const-string v1, "Unable to draw a background for the chips as it was never set"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_4
    move v8, v9

    goto/16 :goto_1
.end method

.method private a(Lcom/android/ex/chips/RecipientEntry;Landroid/text/TextPaint;Z)Landroid/graphics/Bitmap;
    .locals 15

    .prologue
    .line 931
    iget v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->q:F

    float-to-int v6, v1

    .line 933
    sget v1, Lcom/android/ex/chips/RecipientEditTextView;->f:I

    sub-int v1, v6, v1

    .line 934
    const/4 v2, 0x1

    new-array v3, v2, [F

    .line 935
    iget v2, p0, Lcom/android/ex/chips/RecipientEditTextView;->u:I

    .line 936
    invoke-direct/range {p0 .. p1}, Lcom/android/ex/chips/RecipientEditTextView;->d(Lcom/android/ex/chips/RecipientEntry;)Z

    move-result v7

    .line 937
    if-eqz v7, :cond_4

    move v5, v1

    .line 942
    :goto_0
    iget-boolean v2, p0, Lcom/android/ex/chips/RecipientEditTextView;->K:Z

    if-nez v2, :cond_a

    .line 943
    const/4 v2, 0x0

    move v4, v2

    .line 946
    :goto_1
    const-string v2, " "

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Landroid/text/TextPaint;->getTextWidths(Ljava/lang/String;[F)I

    .line 947
    invoke-virtual/range {p0 .. p1}, Lcom/android/ex/chips/RecipientEditTextView;->c(Lcom/android/ex/chips/RecipientEntry;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->z()F

    move-result v8

    int-to-float v9, v4

    sub-float/2addr v8, v9

    int-to-float v9, v1

    sub-float/2addr v8, v9

    const/4 v9, 0x0

    aget v3, v3, v9

    sub-float v3, v8, v3

    move-object/from16 v0, p2

    invoke-direct {p0, v2, v0, v3}, Lcom/android/ex/chips/RecipientEditTextView;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;F)Ljava/lang/CharSequence;

    move-result-object v2

    .line 951
    mul-int/lit8 v3, v6, 0x2

    const/4 v8, 0x0

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v9

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v8, v9}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v8

    float-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    double-to-int v8, v8

    iget v9, p0, Lcom/android/ex/chips/RecipientEditTextView;->u:I

    mul-int/lit8 v9, v9, 0x2

    add-int/2addr v8, v9

    add-int/2addr v8, v4

    add-int/2addr v1, v8

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 956
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v6, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 957
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v9}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 958
    invoke-virtual/range {p0 .. p1}, Lcom/android/ex/chips/RecipientEditTextView;->a(Lcom/android/ex/chips/RecipientEntry;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 959
    if-eqz v3, :cond_9

    .line 960
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v3, v10, v11, v8, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 961
    invoke-virtual {v3, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 964
    invoke-virtual/range {p1 .. p1}, Lcom/android/ex/chips/RecipientEntry;->g()J

    move-result-wide v10

    .line 965
    iget-boolean v3, p0, Lcom/android/ex/chips/RecipientEditTextView;->K:Z

    if-eqz v3, :cond_6

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->q()Z

    move-result v3

    if-eqz v3, :cond_5

    const-wide/16 v12, -0x1

    cmp-long v3, v10, v12

    if-eqz v3, :cond_6

    :cond_0
    const/4 v3, 0x1

    .line 970
    :goto_2
    if-eqz v3, :cond_8

    .line 971
    invoke-virtual/range {p1 .. p1}, Lcom/android/ex/chips/RecipientEntry;->k()[B

    move-result-object v3

    .line 974
    if-nez v3, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/android/ex/chips/RecipientEntry;->j()Landroid/net/Uri;

    move-result-object v10

    if-eqz v10, :cond_1

    .line 976
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    check-cast v3, Lcom/android/ex/chips/a;

    invoke-virtual/range {p1 .. p1}, Lcom/android/ex/chips/RecipientEntry;->j()Landroid/net/Uri;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v10}, Lcom/android/ex/chips/a;->a(Lcom/android/ex/chips/RecipientEntry;Landroid/net/Uri;)V

    .line 978
    invoke-virtual/range {p1 .. p1}, Lcom/android/ex/chips/RecipientEntry;->k()[B

    move-result-object v3

    .line 982
    :cond_1
    if-eqz v3, :cond_7

    .line 983
    const/4 v10, 0x0

    array-length v11, v3

    invoke-static {v3, v10, v11}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 989
    :goto_3
    if-eqz v3, :cond_2

    .line 990
    new-instance v10, Landroid/graphics/RectF;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    int-to-float v13, v13

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    int-to-float v14, v14

    invoke-direct {v10, v11, v12, v13, v14}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 991
    new-instance v11, Landroid/graphics/Rect;

    invoke-direct {v11}, Landroid/graphics/Rect;-><init>()V

    .line 992
    iget-object v12, p0, Lcom/android/ex/chips/RecipientEditTextView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v12, v11}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 993
    new-instance v12, Landroid/graphics/RectF;

    sub-int v4, v8, v4

    iget v13, v11, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, v13

    int-to-float v4, v4

    iget v13, v11, Landroid/graphics/Rect;->top:I

    add-int/lit8 v13, v13, 0x0

    int-to-float v13, v13

    iget v14, v11, Landroid/graphics/Rect;->right:I

    sub-int/2addr v8, v14

    int-to-float v8, v8

    iget v11, v11, Landroid/graphics/Rect;->bottom:I

    sub-int v11, v6, v11

    int-to-float v11, v11

    invoke-direct {v12, v4, v13, v8, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 997
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 998
    sget-object v8, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v4, v10, v12, v8}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 999
    move-object/from16 v0, p2

    invoke-virtual {v1, v3, v4, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 1004
    :cond_2
    :goto_4
    if-eqz v7, :cond_3

    .line 1005
    invoke-direct {p0, v6, v5}, Lcom/android/ex/chips/RecipientEditTextView;->a(II)Landroid/graphics/Rect;

    move-result-object v3

    .line 1006
    iget-object v4, p0, Lcom/android/ex/chips/RecipientEditTextView;->j:Landroid/graphics/drawable/Drawable;

    iget v7, v3, Landroid/graphics/Rect;->left:I

    iget v8, v3, Landroid/graphics/Rect;->top:I

    iget v10, v3, Landroid/graphics/Rect;->right:I

    sub-int v10, v5, v10

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    sub-int v3, v6, v3

    invoke-virtual {v4, v7, v8, v10, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1008
    iget-object v3, p0, Lcom/android/ex/chips/RecipientEditTextView;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1010
    :cond_3
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x106000c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 1012
    const/4 v3, 0x0

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v4

    int-to-float v5, v5

    move-object/from16 v0, p2

    invoke-static {v0, v6}, Lcom/android/ex/chips/RecipientEditTextView;->a(Landroid/text/TextPaint;I)F

    move-result v6

    move-object/from16 v7, p2

    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 1017
    :goto_5
    return-object v9

    .line 940
    :cond_4
    const/4 v1, 0x0

    move v5, v2

    goto/16 :goto_0

    .line 965
    :cond_5
    const-wide/16 v12, -0x1

    cmp-long v3, v10, v12

    if-eqz v3, :cond_6

    const-wide/16 v12, -0x2

    cmp-long v3, v10, v12

    if-eqz v3, :cond_6

    invoke-virtual/range {p1 .. p1}, Lcom/android/ex/chips/RecipientEntry;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_6
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 986
    :cond_7
    iget-object v3, p0, Lcom/android/ex/chips/RecipientEditTextView;->z:Landroid/graphics/Bitmap;

    goto/16 :goto_3

    .line 1001
    :cond_8
    if-eqz p3, :cond_2

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->q()Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_4

    .line 1015
    :cond_9
    const-string v1, "RecipientEditTextView"

    const-string v2, "Unable to draw a background for the chips as it was never set"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    :cond_a
    move v4, v6

    goto/16 :goto_1
.end method

.method private a(II)Landroid/graphics/Rect;
    .locals 4

    .prologue
    .line 1021
    div-int/lit8 v0, p1, 0x4

    .line 1022
    sget v1, Lcom/android/ex/chips/RecipientEditTextView;->g:I

    sub-int v1, v0, v1

    .line 1023
    mul-int/lit8 v2, v0, 0x2

    sub-int v2, p2, v2

    sub-int/2addr v2, v1

    .line 1024
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3, v1, v0, v2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v3
.end method

.method static synthetic a(Lcom/android/ex/chips/RecipientEditTextView;)Landroid/text/TextWatcher;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->V:Landroid/text/TextWatcher;

    return-object v0
.end method

.method static synthetic a(Lcom/android/ex/chips/RecipientEditTextView;Landroid/text/TextWatcher;)Landroid/text/TextWatcher;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/android/ex/chips/RecipientEditTextView;->V:Landroid/text/TextWatcher;

    return-object p1
.end method

.method static synthetic a(Lcom/android/ex/chips/RecipientEditTextView;Ldbxyzptlk/db231222/d/b;)Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->f(Ldbxyzptlk/db231222/d/b;)Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/android/ex/chips/RecipientEditTextView;Lcom/android/ex/chips/RecipientEntry;)Lcom/android/ex/chips/RecipientEntry;
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->g(Lcom/android/ex/chips/RecipientEntry;)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v0

    return-object v0
.end method

.method private a(IFF)Ldbxyzptlk/db231222/d/b;
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 2054
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    const-class v4, Ldbxyzptlk/db231222/d/b;

    invoke-interface {v0, v1, v2, v4}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/d/b;

    .line 2057
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_4

    .line 2058
    aget-object v2, v0, v1

    .line 2059
    invoke-direct {p0, v2}, Lcom/android/ex/chips/RecipientEditTextView;->c(Ldbxyzptlk/db231222/d/b;)I

    move-result v4

    .line 2060
    invoke-direct {p0, v2}, Lcom/android/ex/chips/RecipientEditTextView;->d(Ldbxyzptlk/db231222/d/b;)I

    move-result v5

    .line 2061
    if-lt p1, v4, :cond_3

    if-gt p1, v5, :cond_3

    .line 2062
    cmpg-float v0, p2, v6

    if-ltz v0, :cond_0

    cmpg-float v0, p3, v6

    if-gez v0, :cond_1

    :cond_0
    move-object v0, v2

    .line 2077
    :goto_1
    return-object v0

    .line 2069
    :cond_1
    invoke-direct {p0, v4, v5}, Lcom/android/ex/chips/RecipientEditTextView;->e(II)Landroid/graphics/Rect;

    move-result-object v0

    .line 2070
    float-to-int v1, p2

    float-to-int v4, p3

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, v2

    .line 2071
    goto :goto_1

    :cond_2
    move-object v0, v3

    .line 2073
    goto :goto_1

    .line 2057
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    move-object v0, v3

    .line 2077
    goto :goto_1
.end method

.method static synthetic a(Lcom/android/ex/chips/RecipientEditTextView;Lcom/android/ex/chips/RecipientEntry;ZZ)Ldbxyzptlk/db231222/d/b;
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0, p1, p2, p3}, Lcom/android/ex/chips/RecipientEditTextView;->a(Lcom/android/ex/chips/RecipientEntry;ZZ)Ldbxyzptlk/db231222/d/b;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/android/ex/chips/RecipientEntry;ZZ)Ldbxyzptlk/db231222/d/b;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1066
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->h:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 1067
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Unable to render any chips as setChipDimensions was not called."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1071
    :cond_0
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    .line 1072
    invoke-virtual {v1}, Landroid/text/TextPaint;->getTextSize()F

    move-result v2

    .line 1073
    invoke-virtual {v1}, Landroid/text/TextPaint;->getColor()I

    move-result v3

    .line 1076
    if-eqz p2, :cond_1

    .line 1077
    invoke-direct {p0, p1, v1}, Lcom/android/ex/chips/RecipientEditTextView;->a(Lcom/android/ex/chips/RecipientEntry;Landroid/text/TextPaint;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1084
    :goto_0
    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v4, v5, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 1085
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {v4, v6, v6, v5, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1086
    new-instance v0, Ldbxyzptlk/db231222/d/e;

    invoke-direct {v0, v4, p1}, Ldbxyzptlk/db231222/d/e;-><init>(Landroid/graphics/drawable/Drawable;Lcom/android/ex/chips/RecipientEntry;)V

    .line 1088
    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1089
    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 1090
    return-object v0

    .line 1080
    :cond_1
    invoke-direct {p0, p1, v1, p3}, Lcom/android/ex/chips/RecipientEditTextView;->a(Lcom/android/ex/chips/RecipientEntry;Landroid/text/TextPaint;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ldbxyzptlk/db231222/d/b;Z)Ldbxyzptlk/db231222/d/b;
    .locals 10

    .prologue
    const/16 v9, 0x21

    const/4 v0, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 2474
    invoke-interface {p1}, Ldbxyzptlk/db231222/d/b;->c()J

    move-result-wide v1

    const-wide/16 v3, -0x2

    cmp-long v1, v1, v3

    if-nez v1, :cond_5

    .line 2475
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->c(Ldbxyzptlk/db231222/d/b;)I

    move-result v1

    .line 2476
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->d(Ldbxyzptlk/db231222/d/b;)I

    move-result v2

    .line 2477
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v3

    invoke-interface {v3, p1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 2480
    :try_start_0
    iget-boolean v3, p0, Lcom/android/ex/chips/RecipientEditTextView;->E:Z

    if-eqz v3, :cond_0

    .line 2530
    :goto_0
    return-object v0

    .line 2483
    :cond_0
    invoke-interface {p1}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, v3, v4, v5}, Lcom/android/ex/chips/RecipientEditTextView;->a(Lcom/android/ex/chips/RecipientEntry;ZZ)Ldbxyzptlk/db231222/d/b;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2488
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    .line 2489
    const-string v4, ""

    invoke-static {v3, v1, v2, v4}, Landroid/text/method/QwertyKeyListener;->markAsReplaced(Landroid/text/Spannable;IILjava/lang/String;)V

    .line 2490
    if-eq v1, v6, :cond_1

    if-ne v2, v6, :cond_4

    .line 2491
    :cond_1
    const-string v1, "RecipientEditTextView"

    const-string v2, "The chip being selected no longer exists but should."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2495
    :goto_1
    invoke-interface {v0, v8}, Ldbxyzptlk/db231222/d/b;->a(Z)V

    .line 2496
    invoke-direct {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->i(Ldbxyzptlk/db231222/d/b;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2497
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->c(Ldbxyzptlk/db231222/d/b;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->d(I)V

    .line 2499
    :cond_2
    if-eqz p2, :cond_3

    .line 2500
    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->G:Landroid/widget/ListPopupWindow;

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getWidth()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/android/ex/chips/RecipientEditTextView;->b(Ldbxyzptlk/db231222/d/b;Landroid/widget/ListPopupWindow;I)V

    .line 2502
    :cond_3
    invoke-virtual {p0, v7}, Lcom/android/ex/chips/RecipientEditTextView;->setCursorVisible(Z)V

    goto :goto_0

    .line 2484
    :catch_0
    move-exception v1

    .line 2485
    const-string v2, "RecipientEditTextView"

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 2493
    :cond_4
    invoke-interface {v3, v0, v1, v2, v9}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1

    .line 2505
    :cond_5
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->c(Ldbxyzptlk/db231222/d/b;)I

    move-result v1

    .line 2506
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->d(Ldbxyzptlk/db231222/d/b;)I

    move-result v2

    .line 2507
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v3

    invoke-interface {v3, p1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 2510
    :try_start_1
    invoke-interface {p1}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, v3, v4, v5}, Lcom/android/ex/chips/RecipientEditTextView;->a(Lcom/android/ex/chips/RecipientEntry;ZZ)Ldbxyzptlk/db231222/d/b;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 2515
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    .line 2516
    const-string v4, ""

    invoke-static {v3, v1, v2, v4}, Landroid/text/method/QwertyKeyListener;->markAsReplaced(Landroid/text/Spannable;IILjava/lang/String;)V

    .line 2517
    if-eq v1, v6, :cond_6

    if-ne v2, v6, :cond_9

    .line 2518
    :cond_6
    const-string v1, "RecipientEditTextView"

    const-string v2, "The chip being selected no longer exists but should."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2522
    :goto_2
    invoke-interface {v0, v8}, Ldbxyzptlk/db231222/d/b;->a(Z)V

    .line 2523
    invoke-direct {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->i(Ldbxyzptlk/db231222/d/b;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2524
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->c(Ldbxyzptlk/db231222/d/b;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->d(I)V

    .line 2526
    :cond_7
    if-eqz p2, :cond_8

    .line 2527
    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->F:Landroid/widget/ListPopupWindow;

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getWidth()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/android/ex/chips/RecipientEditTextView;->a(Ldbxyzptlk/db231222/d/b;Landroid/widget/ListPopupWindow;I)V

    .line 2529
    :cond_8
    invoke-virtual {p0, v7}, Lcom/android/ex/chips/RecipientEditTextView;->setCursorVisible(Z)V

    goto/16 :goto_0

    .line 2511
    :catch_1
    move-exception v1

    .line 2512
    const-string v2, "RecipientEditTextView"

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 2520
    :cond_9
    invoke-interface {v3, v0, v1, v2, v9}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_2
.end method

.method private a(Ljava/lang/CharSequence;Landroid/text/TextPaint;F)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 855
    iget v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->s:F

    invoke-virtual {p2, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 856
    const/4 v0, 0x0

    cmpg-float v0, p3, v0

    if-gtz v0, :cond_0

    const-string v0, "RecipientEditTextView"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 857
    const-string v0, "RecipientEditTextView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Max width is negative: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 859
    :cond_0
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {p1, p2, p3, v0}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/ClipData;)V
    .locals 5

    .prologue
    .line 2852
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->V:Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2854
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v0

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/ClipDescription;->hasMimeType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2855
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Landroid/content/ClipData;->getItemCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 2856
    invoke-virtual {p1, v0}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 2857
    if-eqz v1, :cond_0

    .line 2858
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getSelectionStart()I

    move-result v2

    .line 2859
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getSelectionEnd()I

    move-result v3

    .line 2860
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    .line 2861
    if-ltz v2, :cond_1

    if-ltz v3, :cond_1

    if-eq v2, v3, :cond_1

    .line 2862
    invoke-interface {v4, v1, v2, v3}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;II)Landroid/text/Editable;

    .line 2866
    :goto_1
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->I()V

    .line 2855
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2864
    :cond_1
    invoke-interface {v4, v3, v1}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_1

    .line 2871
    :cond_2
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->C:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->ad:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2872
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9

    .prologue
    const/high16 v8, -0x40800000    # -1.0f

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 1117
    sget-object v0, Lcom/android/ex/chips/w;->RecipientEditTextView:[I

    invoke-virtual {p1, p2, v0, v6, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1119
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1121
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->h:Landroid/graphics/drawable/Drawable;

    .line 1122
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->h:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 1123
    sget v0, Lcom/android/ex/chips/s;->chip_background:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->h:Landroid/graphics/drawable/Drawable;

    .line 1125
    :cond_0
    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->n:Landroid/graphics/drawable/Drawable;

    .line 1127
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->n:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    .line 1128
    sget v0, Lcom/android/ex/chips/s;->chip_background_pressed:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->n:Landroid/graphics/drawable/Drawable;

    .line 1130
    :cond_1
    const/4 v0, 0x6

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->i:Landroid/graphics/drawable/Drawable;

    .line 1131
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->i:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_2

    .line 1132
    sget v0, Lcom/android/ex/chips/s;->chip_delete:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->i:Landroid/graphics/drawable/Drawable;

    .line 1134
    :cond_2
    const/16 v0, 0xa

    invoke-virtual {v1, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->u:I

    .line 1135
    iget v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->u:I

    if-ne v0, v5, :cond_3

    .line 1136
    sget v0, Lcom/android/ex/chips/r;->chip_padding:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->u:I

    .line 1138
    :cond_3
    const/16 v0, 0x9

    invoke-virtual {v1, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->y:I

    .line 1140
    iget v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->y:I

    if-ne v0, v5, :cond_4

    .line 1141
    sget v0, Lcom/android/ex/chips/u;->chips_alternate_item:I

    iput v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->y:I

    .line 1144
    :cond_4
    sget v0, Lcom/android/ex/chips/s;->ic_contact_picture:I

    invoke-static {v2, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->z:Landroid/graphics/Bitmap;

    .line 1146
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v3, Lcom/android/ex/chips/u;->more_item:I

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->B:Landroid/widget/TextView;

    .line 1148
    const/16 v0, 0xb

    invoke-virtual {v1, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->q:F

    .line 1149
    iget v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->q:F

    cmpl-float v0, v0, v8

    if-nez v0, :cond_5

    .line 1150
    sget v0, Lcom/android/ex/chips/r;->chip_height:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->q:F

    .line 1153
    :cond_5
    const/16 v0, 0xd

    invoke-virtual {v1, v0, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->K:Z

    .line 1154
    const/16 v0, 0xe

    invoke-virtual {v1, v0, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->L:Z

    .line 1155
    const/16 v0, 0xf

    invoke-virtual {v1, v0, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->M:Z

    .line 1156
    const/16 v0, 0x10

    invoke-virtual {v1, v0, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->N:Z

    .line 1158
    const/16 v0, 0x11

    invoke-virtual {v1, v0, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->O:Z

    .line 1161
    const/16 v0, 0xc

    invoke-virtual {v1, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->s:F

    .line 1162
    iget v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->s:F

    cmpl-float v0, v0, v8

    if-nez v0, :cond_6

    .line 1163
    sget v0, Lcom/android/ex/chips/r;->chip_text_size:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->s:F

    .line 1165
    :cond_6
    invoke-virtual {v1, v7}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->l:Landroid/graphics/drawable/Drawable;

    .line 1167
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->l:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_7

    .line 1168
    sget v0, Lcom/android/ex/chips/s;->chip_background_invalid:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->l:Landroid/graphics/drawable/Drawable;

    .line 1171
    :cond_7
    invoke-virtual {v1, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->p:Landroid/graphics/drawable/Drawable;

    .line 1172
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->p:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_8

    .line 1173
    sget v0, Lcom/android/ex/chips/s;->chip_background_invalid_pressed:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->p:Landroid/graphics/drawable/Drawable;

    .line 1176
    :cond_8
    const/4 v0, 0x7

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->j:Landroid/graphics/drawable/Drawable;

    .line 1177
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->j:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_9

    .line 1178
    sget v0, Lcom/android/ex/chips/s;->warning_icon:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->j:Landroid/graphics/drawable/Drawable;

    .line 1181
    :cond_9
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->k:Landroid/graphics/drawable/Drawable;

    .line 1182
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->k:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_a

    .line 1183
    sget v0, Lcom/android/ex/chips/s;->warning_icon_white:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->k:Landroid/graphics/drawable/Drawable;

    .line 1186
    :cond_a
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->m:Landroid/graphics/drawable/Drawable;

    .line 1187
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->m:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_b

    .line 1188
    sget v0, Lcom/android/ex/chips/s;->chip_background_warn:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->m:Landroid/graphics/drawable/Drawable;

    .line 1191
    :cond_b
    const/4 v0, 0x5

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->o:Landroid/graphics/drawable/Drawable;

    .line 1192
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->o:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_c

    .line 1193
    sget v0, Lcom/android/ex/chips/s;->chip_background_warn_pressed:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->o:Landroid/graphics/drawable/Drawable;

    .line 1196
    :cond_c
    sget v0, Lcom/android/ex/chips/r;->line_spacing_extra:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->t:F

    .line 1197
    sget v0, Lcom/android/ex/chips/t;->chips_max_lines:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->ah:I

    .line 1198
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 1199
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x10102eb

    invoke-virtual {v2, v3, v0, v7}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1200
    iget v0, v0, Landroid/util/TypedValue;->data:I

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v0

    iput v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->aj:I

    .line 1203
    :cond_d
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1204
    return-void
.end method

.method private a(Ldbxyzptlk/db231222/d/b;Landroid/widget/ListPopupWindow;I)V
    .locals 2

    .prologue
    .line 1958
    new-instance v1, Lcom/android/ex/chips/G;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/ex/chips/G;-><init>(Lcom/android/ex/chips/RecipientEditTextView;Ldbxyzptlk/db231222/d/b;Landroid/widget/ListPopupWindow;I)V

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/android/ex/chips/G;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1991
    return-void
.end method

.method private a(IILandroid/text/Editable;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, -0x1

    const/4 v2, 0x0

    .line 1658
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 1659
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->enoughToFilter()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getSelectionEnd()I

    move-result v0

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1662
    invoke-direct {p0, v2}, Lcom/android/ex/chips/RecipientEditTextView;->g(I)V

    .line 1663
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->dismissDropDown()V

    move v0, v1

    .line 1695
    :goto_0
    return v0

    .line 1666
    :cond_0
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v0, p3, p1}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenEnd(Ljava/lang/CharSequence;I)I

    move-result v0

    .line 1667
    invoke-interface {p3}, Landroid/text/Editable;->length()I

    move-result v3

    add-int/lit8 v4, v0, 0x1

    if-le v3, v4, :cond_2

    .line 1668
    add-int/lit8 v3, v0, 0x1

    invoke-interface {p3, v3}, Landroid/text/Editable;->charAt(I)C

    move-result v3

    .line 1669
    const/16 v4, 0x2c

    if-eq v3, v4, :cond_1

    const/16 v4, 0x3b

    if-ne v3, v4, :cond_2

    .line 1670
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 1673
    :cond_2
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1674
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->clearComposingText()V

    .line 1675
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_5

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 1676
    invoke-virtual {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->a(Ljava/lang/String;)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v0

    .line 1677
    if-eqz v0, :cond_3

    .line 1678
    const-string v3, ""

    invoke-static {p3, p1, p2, v3}, Landroid/text/method/QwertyKeyListener;->markAsReplaced(Landroid/text/Spannable;IILjava/lang/String;)V

    .line 1679
    invoke-virtual {p0, v0, v2}, Lcom/android/ex/chips/RecipientEditTextView;->a(Lcom/android/ex/chips/RecipientEntry;Z)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1680
    if-eqz v0, :cond_3

    if-le p1, v5, :cond_3

    if-le p2, v5, :cond_3

    .line 1681
    invoke-interface {p3, p1, p2, v0}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 1688
    :cond_3
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getSelectionEnd()I

    move-result v0

    if-ne p2, v0, :cond_4

    .line 1689
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->dismissDropDown()V

    .line 1691
    :cond_4
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->i()V

    move v0, v1

    .line 1692
    goto :goto_0

    :cond_5
    move v0, v2

    .line 1695
    goto :goto_0
.end method

.method static synthetic b(Lcom/android/ex/chips/RecipientEditTextView;Ldbxyzptlk/db231222/d/b;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->e(Ldbxyzptlk/db231222/d/b;)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    invoke-static {p0}, Lcom/android/ex/chips/RecipientEditTextView;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/android/ex/chips/RecipientEditTextView;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->x()V

    return-void
.end method

.method private b(Ldbxyzptlk/db231222/d/b;Landroid/widget/ListPopupWindow;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2545
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->c(Ldbxyzptlk/db231222/d/b;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v0

    .line 2546
    invoke-direct {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->c(I)I

    move-result v0

    .line 2549
    invoke-virtual {p2, p3}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    .line 2550
    invoke-virtual {p2, p0}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 2551
    invoke-virtual {p2, v0}, Landroid/widget/ListPopupWindow;->setVerticalOffset(I)V

    .line 2552
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->g(Ldbxyzptlk/db231222/d/b;)Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2553
    new-instance v0, Lcom/android/ex/chips/I;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/ex/chips/I;-><init>(Lcom/android/ex/chips/RecipientEditTextView;Ldbxyzptlk/db231222/d/b;Landroid/widget/ListPopupWindow;)V

    invoke-virtual {p2, v0}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2560
    invoke-virtual {p2}, Landroid/widget/ListPopupWindow;->show()V

    .line 2561
    invoke-virtual {p2}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 2562
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 2563
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 2564
    return-void
.end method

.method private b(II)Z
    .locals 1

    .prologue
    .line 1732
    iget-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->E:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->enoughToFilter()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/android/ex/chips/RecipientEditTextView;->c(II)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ldbxyzptlk/db231222/d/b;IFF)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2610
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->s()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2625
    :cond_0
    :goto_0
    return v0

    .line 2617
    :cond_1
    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->copyBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 2620
    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 2621
    invoke-interface {p1}, Ldbxyzptlk/db231222/d/b;->g()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 2622
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->e(Ldbxyzptlk/db231222/d/b;)Landroid/graphics/Rect;

    move-result-object v2

    .line 2623
    iget v3, v2, Landroid/graphics/Rect;->left:I

    iget v4, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 2624
    iget v2, v2, Landroid/graphics/Rect;->right:I

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 2625
    invoke-interface {p1}, Ldbxyzptlk/db231222/d/b;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->d(Ldbxyzptlk/db231222/d/b;)I

    move-result v2

    if-ne p2, v2, :cond_0

    float-to-int v2, p3

    float-to-int v3, p4

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private c(I)I
    .locals 2

    .prologue
    .line 1101
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getLineCount()I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    sub-int/2addr v0, v1

    .line 1102
    iget v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->q:F

    float-to-int v1, v1

    mul-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    neg-int v0, v0

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getDropDownVerticalOffset()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private c(Ldbxyzptlk/db231222/d/b;)I
    .locals 1

    .prologue
    .line 1813
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/android/ex/chips/RecipientEditTextView;)Landroid/widget/AutoCompleteTextView$Validator;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->w:Landroid/widget/AutoCompleteTextView$Validator;

    return-object v0
.end method

.method static synthetic c(Lcom/android/ex/chips/RecipientEditTextView;Ldbxyzptlk/db231222/d/b;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->j(Ldbxyzptlk/db231222/d/b;)V

    return-void
.end method

.method private c(II)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1736
    iget-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->E:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 1744
    :goto_0
    return v0

    .line 1739
    :cond_0
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v0

    const-class v2, Ldbxyzptlk/db231222/d/b;

    invoke-interface {v0, p1, p2, v2}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/d/b;

    .line 1741
    if-eqz v0, :cond_1

    array-length v0, v0

    if-nez v0, :cond_2

    .line 1742
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1744
    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1468
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1469
    const/4 v0, 0x0

    .line 1473
    :goto_0
    return v0

    .line 1472
    :cond_0
    sget-object v0, Lcom/android/ex/chips/RecipientEditTextView;->ac:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 1473
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    goto :goto_0
.end method

.method private d(Ldbxyzptlk/db231222/d/b;)I
    .locals 1

    .prologue
    .line 1817
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/android/ex/chips/RecipientEditTextView;)Landroid/widget/ListPopupWindow;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->F:Landroid/widget/ListPopupWindow;

    return-object v0
.end method

.method private d(I)V
    .locals 3

    .prologue
    .line 1951
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->W:Landroid/widget/ScrollView;

    if-eqz v0, :cond_0

    .line 1952
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->W:Landroid/widget/ScrollView;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->c(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/ScrollView;->smoothScrollBy(II)V

    .line 1954
    :cond_0
    return-void
.end method

.method private d(II)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 1752
    if-eq p1, v3, :cond_0

    if-ne p2, v3, :cond_1

    .line 1754
    :cond_0
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->dismissDropDown()V

    .line 1772
    :goto_0
    return-void

    .line 1759
    :cond_1
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 1760
    invoke-virtual {p0, p2}, Lcom/android/ex/chips/RecipientEditTextView;->setSelection(I)V

    .line 1761
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1762
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1763
    invoke-direct {p0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->d(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v1, v2}, Lcom/android/ex/chips/RecipientEntry;->a(Ljava/lang/String;Z)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v1

    .line 1764
    const-string v2, ""

    invoke-static {v0, p1, p2, v2}, Landroid/text/method/QwertyKeyListener;->markAsReplaced(Landroid/text/Spannable;IILjava/lang/String;)V

    .line 1765
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/android/ex/chips/RecipientEditTextView;->a(Lcom/android/ex/chips/RecipientEntry;Z)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1766
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getSelectionEnd()I

    move-result v2

    .line 1767
    if-eqz v1, :cond_2

    if-le p1, v3, :cond_2

    if-le v2, v3, :cond_2

    .line 1768
    invoke-interface {v0, p1, v2, v1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 1771
    :cond_2
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->dismissDropDown()V

    goto :goto_0
.end method

.method private d(Lcom/android/ex/chips/RecipientEntry;)Z
    .locals 2

    .prologue
    .line 1039
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->f(Lcom/android/ex/chips/RecipientEntry;)Lcom/android/ex/chips/K;

    move-result-object v0

    .line 1040
    sget-object v1, Lcom/android/ex/chips/K;->a:Lcom/android/ex/chips/K;

    if-ne v0, v1, :cond_0

    .line 1041
    const/4 v0, 0x0

    .line 1043
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1534
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->w:Landroid/widget/AutoCompleteTextView$Validator;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->w:Landroid/widget/AutoCompleteTextView$Validator;

    invoke-interface {v0, p1}, Landroid/widget/AutoCompleteTextView$Validator;->isValid(Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method

.method private e(I)I
    .locals 5

    .prologue
    .line 2016
    .line 2017
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    .line 2018
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v1

    .line 2021
    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 2022
    invoke-interface {v2, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v3

    const/16 v4, 0x20

    if-ne v3, v4, :cond_0

    .line 2023
    add-int/lit8 v1, v1, -0x1

    .line 2021
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 2031
    :cond_0
    if-lt p1, v1, :cond_2

    .line 2039
    :cond_1
    return p1

    .line 2034
    :cond_2
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 2035
    :goto_1
    if-ltz p1, :cond_1

    invoke-static {v0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->a(Landroid/text/Editable;I)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->f(I)Ldbxyzptlk/db231222/d/b;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2037
    add-int/lit8 p1, p1, -0x1

    goto :goto_1
.end method

.method private e(II)Landroid/graphics/Rect;
    .locals 6

    .prologue
    .line 1825
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    .line 1826
    invoke-virtual {v2, p1}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v0

    .line 1827
    invoke-virtual {v2, p2}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v1

    .line 1828
    cmpl-float v3, v0, v1

    if-lez v3, :cond_1

    .line 1835
    :goto_0
    invoke-virtual {v2, p1}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v3

    .line 1836
    invoke-virtual {v2, p2}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v4

    .line 1837
    if-eq v3, v4, :cond_0

    .line 1838
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "A chip should be on one line."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1841
    :cond_0
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 1842
    invoke-virtual {v2, v3, v4}, Landroid/text/Layout;->getLineBounds(ILandroid/graphics/Rect;)I

    .line 1844
    float-to-int v1, v1

    iput v1, v4, Landroid/graphics/Rect;->left:I

    .line 1845
    float-to-int v0, v0

    iput v0, v4, Landroid/graphics/Rect;->right:I

    .line 1847
    return-object v4

    :cond_1
    move v5, v1

    move v1, v0

    move v0, v5

    goto :goto_0
.end method

.method private e(Ldbxyzptlk/db231222/d/b;)Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 1821
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->c(Ldbxyzptlk/db231222/d/b;)I

    move-result v0

    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->d(Ldbxyzptlk/db231222/d/b;)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->e(II)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method private e(Lcom/android/ex/chips/RecipientEntry;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1047
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->f(Lcom/android/ex/chips/RecipientEntry;)Lcom/android/ex/chips/K;

    move-result-object v0

    .line 1048
    sget-object v1, Lcom/android/ex/chips/K;->b:Lcom/android/ex/chips/K;

    if-ne v0, v1, :cond_0

    .line 1049
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->o:Landroid/graphics/drawable/Drawable;

    .line 1053
    :goto_0
    return-object v0

    .line 1050
    :cond_0
    sget-object v1, Lcom/android/ex/chips/K;->c:Lcom/android/ex/chips/K;

    if-ne v0, v1, :cond_1

    .line 1051
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->p:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 1053
    :cond_1
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->n:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method static synthetic e(Lcom/android/ex/chips/RecipientEditTextView;)Ldbxyzptlk/db231222/d/b;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    return-object v0
.end method

.method private static e(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1538
    invoke-static {p0}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v0

    .line 1539
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 1540
    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object p0

    .line 1542
    :cond_0
    return-object p0
.end method

.method static synthetic f(Lcom/android/ex/chips/RecipientEditTextView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->C:Landroid/os/Handler;

    return-object v0
.end method

.method private f(Ldbxyzptlk/db231222/d/b;)Landroid/widget/ListAdapter;
    .locals 8

    .prologue
    .line 1994
    new-instance v0, Lcom/android/ex/chips/x;

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p1}, Ldbxyzptlk/db231222/d/b;->c()J

    move-result-wide v2

    invoke-interface {p1}, Ldbxyzptlk/db231222/d/b;->d()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v6

    check-cast v6, Lcom/android/ex/chips/a;

    invoke-virtual {v6}, Lcom/android/ex/chips/a;->a()I

    move-result v6

    move-object v7, p0

    invoke-direct/range {v0 .. v7}, Lcom/android/ex/chips/x;-><init>(Landroid/content/Context;JJILcom/android/ex/chips/y;)V

    return-object v0
.end method

.method private f(Lcom/android/ex/chips/RecipientEntry;)Lcom/android/ex/chips/K;
    .locals 1

    .prologue
    .line 1057
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->al:Lcom/android/ex/chips/M;

    invoke-interface {v0, p1}, Lcom/android/ex/chips/M;->a(Lcom/android/ex/chips/RecipientEntry;)Lcom/android/ex/chips/K;

    move-result-object v0

    return-object v0
.end method

.method private f(I)Ldbxyzptlk/db231222/d/b;
    .locals 1

    .prologue
    const/high16 v0, -0x40800000    # -1.0f

    .line 2050
    invoke-direct {p0, p1, v0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->a(IFF)Ldbxyzptlk/db231222/d/b;

    move-result-object v0

    return-object v0
.end method

.method private f(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 3309
    iput-object p1, p0, Lcom/android/ex/chips/RecipientEditTextView;->R:Ljava/lang/String;

    .line 3310
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->Q:Landroid/app/Dialog;

    invoke-virtual {v0, p1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 3311
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->Q:Landroid/app/Dialog;

    sget v1, Lcom/android/ex/chips/u;->copy_chip_dialog_layout:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(I)V

    .line 3312
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->Q:Landroid/app/Dialog;

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 3313
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->Q:Landroid/app/Dialog;

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 3314
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->Q:Landroid/app/Dialog;

    const v1, 0x1020019

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 3315
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3317
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->q()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3318
    sget v1, Lcom/android/ex/chips/v;->copy_number:I

    .line 3322
    :goto_0
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 3323
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3324
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->Q:Landroid/app/Dialog;

    invoke-virtual {v0, p0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 3325
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->Q:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 3326
    return-void

    .line 3320
    :cond_0
    sget v1, Lcom/android/ex/chips/v;->copy_email:I

    goto :goto_0
.end method

.method private g(Ldbxyzptlk/db231222/d/b;)Landroid/widget/ListAdapter;
    .locals 4

    .prologue
    .line 1999
    new-instance v0, Lcom/android/ex/chips/Z;

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/android/ex/chips/RecipientEditTextView;->y:I

    invoke-interface {p1}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/android/ex/chips/Z;-><init>(Landroid/content/Context;ILcom/android/ex/chips/RecipientEntry;)V

    return-object v0
.end method

.method private g(Lcom/android/ex/chips/RecipientEntry;)Lcom/android/ex/chips/RecipientEntry;
    .locals 5

    .prologue
    .line 2183
    if-nez p1, :cond_1

    .line 2184
    const/4 p1, 0x0

    .line 2202
    :cond_0
    :goto_0
    return-object p1

    .line 2190
    :cond_1
    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->d()Ljava/lang/String;

    move-result-object v0

    .line 2191
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->q()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->g()J

    move-result-wide v1

    const-wide/16 v3, -0x2

    cmp-long v1, v1, v3

    if-nez v1, :cond_2

    .line 2192
    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->a()Z

    move-result v2

    invoke-static {v1, v0, v2}, Lcom/android/ex/chips/RecipientEntry;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/android/ex/chips/RecipientEntry;

    move-result-object p1

    goto :goto_0

    .line 2194
    :cond_2
    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->g()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/android/ex/chips/RecipientEntry;->a(J)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->w:Landroid/widget/AutoCompleteTextView$Validator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->w:Landroid/widget/AutoCompleteTextView$Validator;

    invoke-interface {v1, v0}, Landroid/widget/AutoCompleteTextView$Validator;->isValid(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2198
    :cond_3
    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->a()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/ex/chips/RecipientEntry;->a(Ljava/lang/String;Z)Lcom/android/ex/chips/RecipientEntry;

    move-result-object p1

    goto :goto_0
.end method

.method private g(I)V
    .locals 5

    .prologue
    .line 2163
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/ex/chips/RecipientEntry;

    invoke-direct {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->g(Lcom/android/ex/chips/RecipientEntry;)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v0

    .line 2165
    if-nez v0, :cond_0

    .line 2180
    :goto_0
    return-void

    .line 2168
    :cond_0
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->clearComposingText()V

    .line 2170
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getSelectionEnd()I

    move-result v1

    .line 2171
    iget-object v2, p0, Lcom/android/ex/chips/RecipientEditTextView;->v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v2

    .line 2173
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    .line 2174
    const-string v4, ""

    invoke-static {v3, v2, v1, v4}, Landroid/text/method/QwertyKeyListener;->markAsReplaced(Landroid/text/Spannable;IILjava/lang/String;)V

    .line 2175
    const/4 v4, 0x0

    invoke-virtual {p0, v0, v4}, Lcom/android/ex/chips/RecipientEditTextView;->a(Lcom/android/ex/chips/RecipientEntry;Z)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2176
    if-eqz v0, :cond_1

    if-ltz v2, :cond_1

    if-ltz v1, :cond_1

    .line 2177
    invoke-interface {v3, v2, v1, v0}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 2179
    :cond_1
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->i()V

    goto :goto_0
.end method

.method static synthetic g(Lcom/android/ex/chips/RecipientEditTextView;)Z
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->u()Z

    move-result v0

    return v0
.end method

.method private h(I)Lcom/android/ex/chips/Q;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 2285
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->B:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2286
    new-instance v6, Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-direct {v6, v0}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 2287
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->B:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextSize()F

    move-result v0

    invoke-virtual {v6, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 2288
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->B:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v0

    invoke-virtual {v6, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2289
    invoke-virtual {v6, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    iget-object v3, p0, Lcom/android/ex/chips/RecipientEditTextView;->B:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v3

    add-int/2addr v0, v3

    iget-object v3, p0, Lcom/android/ex/chips/RecipientEditTextView;->B:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v3

    add-int v8, v0, v3

    .line 2291
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getLineHeight()I

    move-result v7

    .line 2292
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v7, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 2293
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v9}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2295
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getLayout()Landroid/text/Layout;

    move-result-object v3

    .line 2296
    if-eqz v3, :cond_0

    .line 2297
    invoke-virtual {v3, v2}, Landroid/text/Layout;->getLineDescent(I)I

    move-result v3

    sub-int v3, v7, v3

    move v5, v3

    .line 2299
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x0

    int-to-float v5, v5

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    .line 2301
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, v9}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 2302
    invoke-virtual {v0, v2, v2, v8, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2303
    new-instance v1, Lcom/android/ex/chips/Q;

    invoke-direct {v1, p0, v0}, Lcom/android/ex/chips/Q;-><init>(Lcom/android/ex/chips/RecipientEditTextView;Landroid/graphics/drawable/Drawable;)V

    return-object v1

    :cond_0
    move v5, v7

    goto :goto_0
.end method

.method private h(Ldbxyzptlk/db231222/d/b;)Ldbxyzptlk/db231222/d/b;
    .locals 3

    .prologue
    .line 2460
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->i(Ldbxyzptlk/db231222/d/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2461
    invoke-interface {p1}, Ldbxyzptlk/db231222/d/b;->b()Ljava/lang/CharSequence;

    move-result-object v0

    .line 2462
    invoke-virtual {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->a(Ldbxyzptlk/db231222/d/b;)V

    .line 2463
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->setCursorVisible(Z)V

    .line 2464
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 2465
    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/android/ex/chips/RecipientEditTextView;->setSelection(I)V

    .line 2466
    invoke-interface {v1, v0}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    .line 2467
    const/4 v0, 0x0

    .line 2469
    :goto_0
    return-object v0

    :cond_0
    iget-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->L:Z

    invoke-direct {p0, p1, v0}, Lcom/android/ex/chips/RecipientEditTextView;->a(Ldbxyzptlk/db231222/d/b;Z)Ldbxyzptlk/db231222/d/b;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic h(Lcom/android/ex/chips/RecipientEditTextView;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->w()V

    return-void
.end method

.method static synthetic i(Lcom/android/ex/chips/RecipientEditTextView;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->T:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method private i(Ldbxyzptlk/db231222/d/b;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2535
    iget-boolean v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->M:Z

    if-nez v1, :cond_1

    .line 2539
    :cond_0
    :goto_0
    return v0

    .line 2538
    :cond_1
    invoke-interface {p1}, Ldbxyzptlk/db231222/d/b;->c()J

    move-result-wide v1

    .line 2539
    const-wide/16 v3, -0x1

    cmp-long v3, v1, v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->q()Z

    move-result v3

    if-nez v3, :cond_0

    const-wide/16 v3, -0x2

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic j(Lcom/android/ex/chips/RecipientEditTextView;)I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->U:I

    return v0
.end method

.method private j(Ldbxyzptlk/db231222/d/b;)V
    .locals 6

    .prologue
    const/4 v4, -0x1

    .line 2572
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->c(Ldbxyzptlk/db231222/d/b;)I

    move-result v0

    .line 2573
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->d(Ldbxyzptlk/db231222/d/b;)I

    move-result v1

    .line 2574
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    .line 2575
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    .line 2576
    if-eq v0, v4, :cond_0

    if-ne v1, v4, :cond_3

    .line 2577
    :cond_0
    const-string v0, "RecipientEditTextView"

    const-string v1, "The chip doesn\'t exist or may be a chip a user was editing"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2578
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->setSelection(I)V

    .line 2579
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->E()Z

    .line 2593
    :cond_1
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->setCursorVisible(Z)V

    .line 2594
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->setSelection(I)V

    .line 2595
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->F:Landroid/widget/ListPopupWindow;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->F:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2596
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->F:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 2598
    :cond_2
    return-void

    .line 2581
    :cond_3
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v3

    invoke-interface {v3, p1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 2582
    const-string v3, ""

    invoke-static {v2, v0, v1, v3}, Landroid/text/method/QwertyKeyListener;->markAsReplaced(Landroid/text/Spannable;IILjava/lang/String;)V

    .line 2583
    invoke-interface {v2, p1}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 2585
    :try_start_0
    iget-boolean v3, p0, Lcom/android/ex/chips/RecipientEditTextView;->E:Z

    if-nez v3, :cond_1

    .line 2586
    invoke-interface {p1}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {p0, v3, v4, v5}, Lcom/android/ex/chips/RecipientEditTextView;->a(Lcom/android/ex/chips/RecipientEntry;ZZ)Ldbxyzptlk/db231222/d/b;

    move-result-object v3

    const/16 v4, 0x21

    invoke-interface {v2, v3, v0, v1, v4}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2589
    :catch_0
    move-exception v0

    .line 2590
    const-string v1, "RecipientEditTextView"

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic k(Lcom/android/ex/chips/RecipientEditTextView;)Landroid/text/style/ImageSpan;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->A:Landroid/text/style/ImageSpan;

    return-object v0
.end method

.method private k(Ldbxyzptlk/db231222/d/b;)V
    .locals 4

    .prologue
    .line 3255
    invoke-interface {p1}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEntry;->d()Ljava/lang/String;

    move-result-object v0

    .line 3256
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v0

    .line 3259
    new-instance v1, Lcom/android/ex/chips/R;

    invoke-direct {v1, p0, p1}, Lcom/android/ex/chips/R;-><init>(Lcom/android/ex/chips/RecipientEditTextView;Ldbxyzptlk/db231222/d/b;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/ex/chips/RecipientEditTextView;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 3263
    invoke-virtual {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->a(Ldbxyzptlk/db231222/d/b;)V

    .line 3264
    return-void
.end method

.method static synthetic l(Lcom/android/ex/chips/RecipientEditTextView;)Z
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->H()Z

    move-result v0

    return v0
.end method

.method static synthetic m(Lcom/android/ex/chips/RecipientEditTextView;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->G()V

    return-void
.end method

.method static synthetic n(Lcom/android/ex/chips/RecipientEditTextView;)Landroid/widget/MultiAutoCompleteTextView$Tokenizer;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    return-object v0
.end method

.method static synthetic o(Lcom/android/ex/chips/RecipientEditTextView;)Z
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->E:Z

    return v0
.end method

.method static synthetic p(Lcom/android/ex/chips/RecipientEditTextView;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->H:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic q(Lcom/android/ex/chips/RecipientEditTextView;)Lcom/android/ex/chips/N;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->ae:Lcom/android/ex/chips/N;

    return-object v0
.end method

.method static synthetic t()I
    .locals 1

    .prologue
    .line 112
    sget v0, Lcom/android/ex/chips/RecipientEditTextView;->d:I

    return v0
.end method

.method private u()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 401
    iget-object v2, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    if-nez v2, :cond_0

    .line 405
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getSelectionStart()I

    move-result v2

    .line 406
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getSelectionEnd()I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 407
    add-int/lit8 v2, v2, -0x1

    .line 408
    invoke-direct {p0, v2}, Lcom/android/ex/chips/RecipientEditTextView;->f(I)Ldbxyzptlk/db231222/d/b;

    move-result-object v2

    .line 409
    if-eqz v2, :cond_2

    .line 411
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/android/ex/chips/RecipientEditTextView;->setSelection(I)V

    .line 412
    invoke-direct {p0, v2, v1}, Lcom/android/ex/chips/RecipientEditTextView;->a(Ldbxyzptlk/db231222/d/b;Z)Ldbxyzptlk/db231222/d/b;

    move-result-object v1

    iput-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    .line 424
    :goto_0
    return v0

    .line 418
    :cond_0
    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->F:Landroid/widget/ListPopupWindow;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->F:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1}, Landroid/widget/ListPopupWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 419
    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->F:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 421
    :cond_1
    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    invoke-virtual {p0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->a(Ldbxyzptlk/db231222/d/b;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 424
    goto :goto_0
.end method

.method private v()I
    .locals 2

    .prologue
    .line 712
    sget v0, Lcom/android/ex/chips/RecipientEditTextView;->ai:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 713
    iget v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->q:F

    iget v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->t:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lcom/android/ex/chips/RecipientEditTextView;->ai:I

    .line 715
    :cond_0
    sget v0, Lcom/android/ex/chips/RecipientEditTextView;->ai:I

    return v0
.end method

.method private w()V
    .locals 4

    .prologue
    .line 742
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->W:Landroid/widget/ScrollView;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->I:Z

    if-eqz v0, :cond_0

    .line 743
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 744
    invoke-virtual {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->getLocationOnScreen([I)V

    .line 745
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getHeight()I

    move-result v1

    .line 746
    const/4 v2, 0x1

    aget v0, v0, v2

    add-int/2addr v0, v1

    .line 750
    iget v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->q:F

    float-to-int v1, v1

    iget v2, p0, Lcom/android/ex/chips/RecipientEditTextView;->aj:I

    add-int/2addr v1, v2

    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->v()I

    move-result v2

    add-int/2addr v1, v2

    .line 751
    if-le v0, v1, :cond_0

    .line 752
    iget-object v2, p0, Lcom/android/ex/chips/RecipientEditTextView;->W:Landroid/widget/ScrollView;

    const/4 v3, 0x0

    sub-int/2addr v0, v1

    invoke-virtual {v2, v3, v0}, Landroid/widget/ScrollView;->scrollBy(II)V

    .line 755
    :cond_0
    return-void
.end method

.method private x()V
    .locals 6

    .prologue
    const-wide/16 v2, -0x1

    .line 788
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    if-nez v0, :cond_0

    .line 836
    :goto_0
    return-void

    .line 791
    :cond_0
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    invoke-interface {v0}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEntry;->g()J

    move-result-wide v0

    .line 792
    :goto_1
    iget-object v4, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    if-eqz v4, :cond_2

    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->q()Z

    move-result v2

    if-nez v2, :cond_2

    const-wide/16 v2, -0x2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 794
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->G()V

    .line 835
    :goto_2
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->n()V

    goto :goto_0

    :cond_1
    move-wide v0, v2

    .line 791
    goto :goto_1

    .line 796
    :cond_2
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getWidth()I

    move-result v0

    if-gtz v0, :cond_3

    .line 802
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->C:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->ag:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 803
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->C:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->ag:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 808
    :cond_3
    iget v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->D:I

    if-lez v0, :cond_5

    .line 809
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->B()V

    .line 833
    :cond_4
    :goto_3
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->C:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->ad:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    .line 811
    :cond_5
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 812
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getSelectionEnd()I

    move-result v2

    .line 813
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v0, v1, v2}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v3

    .line 814
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v0

    const-class v4, Ldbxyzptlk/db231222/d/b;

    invoke-interface {v0, v3, v2, v4}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/d/b;

    .line 816
    if-eqz v0, :cond_6

    array-length v0, v0

    if-nez v0, :cond_4

    .line 817
    :cond_6
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    .line 818
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v0, v4, v3}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenEnd(Ljava/lang/CharSequence;I)I

    move-result v0

    .line 820
    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v5

    if-ge v0, v5, :cond_7

    invoke-interface {v4, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v4

    const/16 v5, 0x2c

    if-ne v4, v5, :cond_7

    .line 821
    invoke-virtual {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->b(I)I

    move-result v0

    .line 825
    :cond_7
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getSelectionEnd()I

    move-result v4

    .line 826
    if-eq v0, v4, :cond_8

    .line 827
    invoke-direct {p0, v3, v0}, Lcom/android/ex/chips/RecipientEditTextView;->d(II)V

    goto :goto_3

    .line 829
    :cond_8
    invoke-direct {p0, v3, v2, v1}, Lcom/android/ex/chips/RecipientEditTextView;->a(IILandroid/text/Editable;)Z

    goto :goto_3
.end method

.method private y()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 839
    iget-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->I:Z

    if-eqz v0, :cond_0

    .line 840
    const v0, 0x7fffffff

    invoke-virtual {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->setMaxLines(I)V

    .line 842
    :cond_0
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->o()V

    .line 843
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->setCursorVisible(Z)V

    .line 844
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 845
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v2

    if-lez v2, :cond_2

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->setSelection(I)V

    .line 848
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 849
    new-instance v0, Lcom/android/ex/chips/T;

    invoke-direct {v0, p0, v3}, Lcom/android/ex/chips/T;-><init>(Lcom/android/ex/chips/RecipientEditTextView;Lcom/android/ex/chips/A;)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/ex/chips/T;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 850
    iput-object v3, p0, Lcom/android/ex/chips/RecipientEditTextView;->b:Ljava/util/ArrayList;

    .line 852
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 845
    goto :goto_0
.end method

.method private z()F
    .locals 2

    .prologue
    .line 1112
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->u:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    return v0
.end method


# virtual methods
.method final a(Landroid/text/Editable;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2328
    move v1, v0

    .line 2330
    :cond_0
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 2331
    iget-object v2, p0, Lcom/android/ex/chips/RecipientEditTextView;->v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v2, p1, v0}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenEnd(Ljava/lang/CharSequence;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->b(I)I

    move-result v0

    .line 2332
    add-int/lit8 v1, v1, 0x1

    .line 2333
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 2337
    :cond_1
    return v1
.end method

.method final a(Lcom/android/ex/chips/RecipientEntry;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1029
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->f(Lcom/android/ex/chips/RecipientEntry;)Lcom/android/ex/chips/K;

    move-result-object v0

    .line 1030
    sget-object v1, Lcom/android/ex/chips/K;->b:Lcom/android/ex/chips/K;

    if-ne v0, v1, :cond_0

    .line 1031
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->m:Landroid/graphics/drawable/Drawable;

    .line 1035
    :goto_0
    return-object v0

    .line 1032
    :cond_0
    sget-object v1, Lcom/android/ex/chips/K;->c:Lcom/android/ex/chips/K;

    if-ne v0, v1, :cond_1

    .line 1033
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->l:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 1035
    :cond_1
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->h:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method final a(Ljava/lang/String;)Lcom/android/ex/chips/RecipientEntry;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1478
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1529
    :goto_0
    return-object v3

    .line 1481
    :cond_0
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/android/ex/chips/RecipientEditTextView;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1482
    invoke-static {p1, v1}, Lcom/android/ex/chips/RecipientEntry;->b(Ljava/lang/String;Z)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v3

    goto :goto_0

    .line 1484
    :cond_1
    invoke-static {p1}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v0

    .line 1486
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->d(Ljava/lang/String;)Z

    move-result v2

    .line 1487
    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    array-length v5, v0

    if-lez v5, :cond_3

    .line 1490
    aget-object v5, v0, v4

    invoke-virtual {v5}, Landroid/text/util/Rfc822Token;->getName()Ljava/lang/String;

    move-result-object v5

    .line 1491
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1492
    aget-object v0, v0, v4

    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0, v2}, Lcom/android/ex/chips/RecipientEntry;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v3

    goto :goto_0

    .line 1495
    :cond_2
    aget-object v0, v0, v4

    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 1496
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 1497
    invoke-static {v0, v2}, Lcom/android/ex/chips/RecipientEntry;->a(Ljava/lang/String;Z)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v3

    goto :goto_0

    .line 1504
    :cond_3
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->w:Landroid/widget/AutoCompleteTextView$Validator;

    if-eqz v0, :cond_8

    if-nez v2, :cond_8

    .line 1506
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->w:Landroid/widget/AutoCompleteTextView$Validator;

    invoke-interface {v0, p1}, Landroid/widget/AutoCompleteTextView$Validator;->fixText(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1507
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 1508
    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1512
    invoke-static {v0}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v3

    .line 1513
    array-length v5, v3

    if-lez v5, :cond_7

    .line 1514
    aget-object v0, v3, v4

    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v0

    :goto_1
    move v2, v1

    .line 1529
    :cond_4
    :goto_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    :goto_3
    invoke-static {v0, v2}, Lcom/android/ex/chips/RecipientEntry;->a(Ljava/lang/String;Z)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v3

    goto :goto_0

    :cond_5
    move-object v0, v3

    move v2, v4

    .line 1524
    goto :goto_2

    :cond_6
    move-object v0, p1

    .line 1529
    goto :goto_3

    :cond_7
    move v1, v2

    goto :goto_1

    :cond_8
    move-object v0, v3

    goto :goto_2
.end method

.method protected final a(Lcom/android/ex/chips/RecipientEntry;Z)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2127
    invoke-virtual {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->b(Lcom/android/ex/chips/RecipientEntry;)Ljava/lang/String;

    move-result-object v2

    .line 2128
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2147
    :goto_0
    return-object v0

    .line 2133
    :cond_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v3, v1, -0x1

    .line 2134
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2135
    iget-boolean v2, p0, Lcom/android/ex/chips/RecipientEditTextView;->E:Z

    if-nez v2, :cond_1

    .line 2137
    const/4 v2, 0x0

    :try_start_0
    invoke-direct {p0, p1, p2, v2}, Lcom/android/ex/chips/RecipientEditTextView;->a(Lcom/android/ex/chips/RecipientEntry;ZZ)Ldbxyzptlk/db231222/d/b;

    move-result-object v2

    .line 2139
    const/4 v4, 0x0

    const/16 v5, 0x21

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2141
    invoke-virtual {v1}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ldbxyzptlk/db231222/d/b;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    move-object v0, v1

    .line 2147
    goto :goto_0

    .line 2142
    :catch_0
    move-exception v1

    .line 2143
    const-string v2, "RecipientEditTextView"

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final a()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/d/b;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 371
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 372
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    const-class v4, Ldbxyzptlk/db231222/d/b;

    invoke-interface {v0, v1, v3, v4}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/d/b;

    .line 373
    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 374
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 373
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->H:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 377
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->H:Ljava/util/ArrayList;

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 379
    :cond_1
    return-object v2
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2005
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->F:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 2006
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 2007
    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 2009
    :cond_0
    iput p1, p0, Lcom/android/ex/chips/RecipientEditTextView;->U:I

    .line 2010
    return-void
.end method

.method final a(IILandroid/text/Editable;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1422
    invoke-direct {p0, p1, p2}, Lcom/android/ex/chips/RecipientEditTextView;->c(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1462
    :cond_0
    :goto_0
    return-void

    .line 1427
    :cond_1
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1428
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 1429
    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 1430
    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v3, v4, :cond_2

    .line 1431
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1433
    :cond_2
    invoke-virtual {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->a(Ljava/lang/String;)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v3

    .line 1434
    if-eqz v3, :cond_0

    .line 1435
    const/4 v2, 0x0

    .line 1437
    :try_start_0
    iget-boolean v4, p0, Lcom/android/ex/chips/RecipientEditTextView;->E:Z

    if-nez v4, :cond_7

    .line 1442
    invoke-virtual {v3}, Lcom/android/ex/chips/RecipientEntry;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {v3}, Lcom/android/ex/chips/RecipientEntry;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/android/ex/chips/RecipientEntry;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_3
    const/4 v1, 0x1

    .line 1445
    :cond_4
    if-eqz p4, :cond_6

    const/4 v4, 0x0

    invoke-direct {p0, v3, v4, v1}, Lcom/android/ex/chips/RecipientEditTextView;->a(Lcom/android/ex/chips/RecipientEntry;ZZ)Ldbxyzptlk/db231222/d/b;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1452
    :goto_1
    const/16 v2, 0x21

    invoke-interface {p3, v1, p1, p2, v2}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 1454
    if-eqz v1, :cond_0

    .line 1455
    iget-object v2, p0, Lcom/android/ex/chips/RecipientEditTextView;->b:Ljava/util/ArrayList;

    if-nez v2, :cond_5

    .line 1456
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/android/ex/chips/RecipientEditTextView;->b:Ljava/util/ArrayList;

    .line 1458
    :cond_5
    invoke-interface {v1, v0}, Ldbxyzptlk/db231222/d/b;->a(Ljava/lang/String;)V

    .line 1459
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1445
    :cond_6
    :try_start_1
    new-instance v1, Ldbxyzptlk/db231222/d/c;

    invoke-direct {v1, v3}, Ldbxyzptlk/db231222/d/c;-><init>(Lcom/android/ex/chips/RecipientEntry;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1449
    :catch_0
    move-exception v1

    .line 1450
    const-string v3, "RecipientEditTextView"

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_7
    move-object v1, v2

    goto :goto_1
.end method

.method protected final a(Ldbxyzptlk/db231222/d/b;)V
    .locals 7

    .prologue
    .line 2634
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v2

    .line 2635
    invoke-interface {v2, p1}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v3

    .line 2636
    invoke-interface {v2, p1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    .line 2637
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    .line 2639
    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    if-ne p1, v1, :cond_1

    const/4 v1, 0x1

    .line 2641
    :goto_0
    if-eqz v1, :cond_0

    .line 2642
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    .line 2645
    :cond_0
    :goto_1
    if-ltz v0, :cond_2

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v5

    if-ge v0, v5, :cond_2

    invoke-interface {v4, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v5

    const/16 v6, 0x20

    if-ne v5, v6, :cond_2

    .line 2646
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2639
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 2648
    :cond_2
    invoke-interface {v2, p1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 2649
    if-ltz v3, :cond_3

    if-lez v0, :cond_3

    .line 2650
    invoke-interface {v4, v3, v0}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 2652
    :cond_3
    if-eqz v1, :cond_4

    .line 2653
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->G()V

    .line 2655
    :cond_4
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/d/b;IFF)V
    .locals 1

    .prologue
    .line 2701
    invoke-interface {p1}, Ldbxyzptlk/db231222/d/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2702
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/ex/chips/RecipientEditTextView;->b(Ldbxyzptlk/db231222/d/b;IFF)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2703
    invoke-virtual {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->a(Ldbxyzptlk/db231222/d/b;)V

    .line 2708
    :cond_0
    :goto_0
    return-void

    .line 2705
    :cond_1
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->G()V

    goto :goto_0
.end method

.method final a(Ldbxyzptlk/db231222/d/b;Lcom/android/ex/chips/RecipientEntry;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v7, -0x1

    const/4 v3, 0x0

    .line 2663
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    if-ne p1, v0, :cond_4

    move v1, v2

    .line 2664
    :goto_0
    if-eqz v1, :cond_0

    .line 2665
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    .line 2667
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->c(Ldbxyzptlk/db231222/d/b;)I

    move-result v4

    .line 2668
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->d(Ldbxyzptlk/db231222/d/b;)I

    move-result v0

    .line 2669
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v5

    invoke-interface {v5, p1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 2670
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v5

    .line 2671
    invoke-virtual {p0, p2, v3}, Lcom/android/ex/chips/RecipientEditTextView;->a(Lcom/android/ex/chips/RecipientEntry;Z)Ljava/lang/CharSequence;

    move-result-object v6

    .line 2672
    if-eqz v6, :cond_2

    .line 2673
    if-eq v4, v7, :cond_1

    if-ne v0, v7, :cond_5

    .line 2674
    :cond_1
    const-string v0, "RecipientEditTextView"

    const-string v4, "The chip to replace does not exist but should."

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2675
    invoke-interface {v5, v3, v6}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 2689
    :cond_2
    :goto_1
    invoke-virtual {p0, v2}, Lcom/android/ex/chips/RecipientEditTextView;->setCursorVisible(Z)V

    .line 2690
    if-eqz v1, :cond_3

    .line 2691
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->G()V

    .line 2693
    :cond_3
    return-void

    :cond_4
    move v1, v3

    .line 2663
    goto :goto_0

    .line 2677
    :cond_5
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2682
    :goto_2
    if-ltz v0, :cond_6

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v3

    if-ge v0, v3, :cond_6

    invoke-interface {v5, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v3

    const/16 v7, 0x20

    if-ne v3, v7, :cond_6

    .line 2683
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2685
    :cond_6
    invoke-interface {v5, v4, v0, v6}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_1
.end method

.method final a(Ljava/lang/CharSequence;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1882
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1893
    :cond_0
    :goto_0
    return v0

    .line 1886
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 1887
    iget-object v2, p0, Lcom/android/ex/chips/RecipientEditTextView;->v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v2, p1, v1}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v2

    .line 1888
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 1889
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1890
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 1891
    const/16 v2, 0x2c

    if-eq v1, v2, :cond_2

    const/16 v2, 0x3b

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public append(Ljava/lang/CharSequence;II)V
    .locals 4

    .prologue
    .line 628
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->V:Landroid/text/TextWatcher;

    if-eqz v0, :cond_0

    .line 629
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->V:Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 631
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/MultiAutoCompleteTextView;->append(Ljava/lang/CharSequence;II)V

    .line 632
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p1}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v0

    if-lez v0, :cond_2

    .line 633
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 635
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x2c

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 637
    sget-object v1, Lcom/android/ex/chips/RecipientEditTextView;->c:Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, Lcom/android/ex/chips/RecipientEditTextView;->c:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-super {p0, v1, v2, v3}, Landroid/widget/MultiAutoCompleteTextView;->append(Ljava/lang/CharSequence;II)V

    .line 638
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/ex/chips/RecipientEditTextView;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 641
    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {v0}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v1

    if-lez v1, :cond_2

    .line 643
    iget v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->D:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->D:I

    .line 644
    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 649
    :cond_2
    iget v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->D:I

    if-lez v0, :cond_3

    .line 650
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->B()V

    .line 652
    :cond_3
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->C:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->ad:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 653
    return-void
.end method

.method final b(I)I
    .locals 2

    .prologue
    .line 2946
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->length()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 2958
    :cond_0
    :goto_0
    return p1

    .line 2949
    :cond_1
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 2950
    const/16 v1, 0x2c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3b

    if-ne v0, v1, :cond_3

    .line 2951
    :cond_2
    add-int/lit8 p1, p1, 0x1

    .line 2955
    :cond_3
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->length()I

    move-result v0

    if-ge p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    .line 2956
    add-int/lit8 p1, p1, 0x1

    goto :goto_0
.end method

.method final b()Ldbxyzptlk/db231222/d/b;
    .locals 3

    .prologue
    .line 469
    const/4 v0, 0x0

    .line 470
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->k()[Ldbxyzptlk/db231222/d/b;

    move-result-object v1

    .line 471
    if-eqz v1, :cond_0

    array-length v2, v1

    if-lez v2, :cond_0

    .line 472
    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    aget-object v0, v1, v0

    .line 474
    :cond_0
    return-object v0
.end method

.method final b(Lcom/android/ex/chips/RecipientEntry;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2083
    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->c()Ljava/lang/String;

    move-result-object v0

    .line 2084
    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->d()Ljava/lang/String;

    move-result-object v2

    .line 2085
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move-object v0, v1

    .line 2089
    :cond_1
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->q()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {v2}, Lcom/android/ex/chips/RecipientEditTextView;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2090
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 2103
    :goto_0
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 2104
    iget-object v2, p0, Lcom/android/ex/chips/RecipientEditTextView;->v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    if-eqz v2, :cond_2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v1, v0}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->terminateToken(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_2
    return-object v0

    .line 2092
    :cond_3
    if-eqz v2, :cond_4

    .line 2095
    invoke-static {v2}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v3

    .line 2096
    if-eqz v3, :cond_4

    array-length v4, v3

    if-lez v4, :cond_4

    .line 2097
    const/4 v2, 0x0

    aget-object v2, v3, v2

    invoke-virtual {v2}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v2

    .line 2100
    :cond_4
    new-instance v3, Landroid/text/util/Rfc822Token;

    invoke-direct {v3, v0, v2, v1}, Landroid/text/util/Rfc822Token;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2101
    invoke-virtual {v3}, Landroid/text/util/Rfc822Token;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Ldbxyzptlk/db231222/d/b;)Z
    .locals 4

    .prologue
    .line 2843
    invoke-interface {p1}, Ldbxyzptlk/db231222/d/b;->c()J

    move-result-wide v0

    .line 2844
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->q()Z

    move-result v2

    if-nez v2, :cond_1

    const-wide/16 v2, -0x2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/CharSequence;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2832
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getSelectionEnd()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 2833
    :goto_0
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .line 2834
    if-eq v0, v2, :cond_3

    .line 2835
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 2839
    :goto_1
    const/16 v2, 0x2c

    if-eq v0, v2, :cond_0

    const/16 v2, 0x3b

    if-ne v0, v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    return v1

    .line 2832
    :cond_2
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getSelectionEnd()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 2837
    :cond_3
    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    goto :goto_1
.end method

.method final c(Lcom/android/ex/chips/RecipientEntry;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2112
    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->c()Ljava/lang/String;

    move-result-object v0

    .line 2113
    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->d()Ljava/lang/String;

    move-result-object v2

    .line 2114
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move-object v0, v1

    .line 2117
    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2122
    :goto_0
    return-object v0

    .line 2119
    :cond_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    move-object v0, v2

    .line 2120
    goto :goto_0

    .line 2122
    :cond_3
    new-instance v3, Landroid/text/util/Rfc822Token;

    invoke-direct {v3, v0, v2, v1}, Landroid/text/util/Rfc822Token;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/text/util/Rfc822Token;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 767
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    const-class v3, Ldbxyzptlk/db231222/d/b;

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/d/b;

    .line 768
    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 769
    invoke-direct {p0, v3}, Lcom/android/ex/chips/RecipientEditTextView;->j(Ldbxyzptlk/db231222/d/b;)V

    .line 768
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 771
    :cond_0
    return-void
.end method

.method public final d()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 778
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    const-class v3, Ldbxyzptlk/db231222/d/b;

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/d/b;

    .line 779
    array-length v3, v0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    .line 780
    invoke-interface {v4}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/ex/chips/RecipientEditTextView;->f(Lcom/android/ex/chips/RecipientEntry;)Lcom/android/ex/chips/K;

    move-result-object v4

    sget-object v5, Lcom/android/ex/chips/K;->c:Lcom/android/ex/chips/K;

    if-ne v4, v5, :cond_0

    .line 781
    const/4 v0, 0x1

    .line 784
    :goto_1
    return v0

    .line 779
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 784
    goto :goto_1
.end method

.method public dismissDropDown()V
    .locals 2

    .prologue
    .line 689
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->S:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    .line 690
    invoke-super {p0}, Landroid/widget/MultiAutoCompleteTextView;->dismissDropDown()V

    .line 694
    :goto_0
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->ak:Lcom/android/ex/chips/X;

    if-eqz v0, :cond_0

    .line 695
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->ak:Lcom/android/ex/chips/X;

    invoke-interface {v0, p0}, Lcom/android/ex/chips/X;->a(Lcom/android/ex/chips/RecipientEditTextView;)V

    .line 697
    :cond_0
    return-void

    .line 692
    :cond_1
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->ab:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0
.end method

.method final e()V
    .locals 11

    .prologue
    const/16 v10, 0x32

    const/4 v2, 0x1

    const/4 v9, 0x2

    const/4 v1, 0x0

    .line 1311
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->f()I

    move-result v0

    if-gtz v0, :cond_1

    .line 1375
    :cond_0
    :goto_0
    return-void

    .line 1317
    :cond_1
    iget v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->D:I

    if-lez v0, :cond_0

    .line 1321
    iget-object v5, p0, Lcom/android/ex/chips/RecipientEditTextView;->a:Ljava/util/ArrayList;

    monitor-enter v5

    .line 1322
    :try_start_0
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v6

    .line 1324
    iget v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->D:I

    if-gt v0, v10, :cond_7

    move v4, v1

    .line 1325
    :goto_1
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_5

    .line 1326
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1327
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 1329
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v7

    add-int/lit8 v0, v0, -0x1

    .line 1330
    if-ltz v7, :cond_3

    .line 1333
    invoke-interface {v6}, Landroid/text/Editable;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    if-ge v0, v3, :cond_b

    invoke-interface {v6, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v3

    const/16 v8, 0x2c

    if-ne v3, v8, :cond_b

    .line 1335
    add-int/lit8 v0, v0, 0x1

    move v3, v0

    .line 1337
    :goto_2
    if-lt v4, v9, :cond_2

    iget-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->I:Z

    if-nez v0, :cond_4

    :cond_2
    move v0, v2

    :goto_3
    invoke-virtual {p0, v7, v3, v6, v0}, Lcom/android/ex/chips/RecipientEditTextView;->a(IILandroid/text/Editable;Z)V

    .line 1340
    :cond_3
    iget v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->D:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->D:I

    .line 1325
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_4
    move v0, v1

    .line 1337
    goto :goto_3

    .line 1342
    :cond_5
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->g()V

    .line 1347
    :goto_4
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_a

    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, v10, :cond_a

    .line 1349
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v9, :cond_8

    .line 1350
    :cond_6
    new-instance v0, Lcom/android/ex/chips/T;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/ex/chips/T;-><init>(Lcom/android/ex/chips/RecipientEditTextView;Lcom/android/ex/chips/A;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/ex/chips/T;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1351
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->b:Ljava/util/ArrayList;

    .line 1372
    :goto_5
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->D:I

    .line 1373
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1374
    monitor-exit v5

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1344
    :cond_7
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->E:Z

    goto :goto_4

    .line 1354
    :cond_8
    new-instance v0, Lcom/android/ex/chips/N;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/ex/chips/N;-><init>(Lcom/android/ex/chips/RecipientEditTextView;Lcom/android/ex/chips/A;)V

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->ae:Lcom/android/ex/chips/N;

    .line 1355
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->ae:Lcom/android/ex/chips/N;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/util/ArrayList;

    const/4 v2, 0x0

    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/ex/chips/RecipientEditTextView;->b:Ljava/util/ArrayList;

    const/4 v6, 0x0

    const/4 v7, 0x2

    invoke-virtual {v4, v6, v7}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/ex/chips/N;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1357
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v9, :cond_9

    .line 1358
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->b:Ljava/util/ArrayList;

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/android/ex/chips/RecipientEditTextView;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->b:Ljava/util/ArrayList;

    .line 1364
    :goto_6
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->n()V

    goto :goto_5

    .line 1362
    :cond_9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->b:Ljava/util/ArrayList;

    goto :goto_6

    .line 1369
    :cond_a
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->b:Ljava/util/ArrayList;

    .line 1370
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->n()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    :cond_b
    move v3, v0

    goto/16 :goto_2
.end method

.method final f()I
    .locals 1

    .prologue
    .line 1379
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getWidth()I

    move-result v0

    return v0
.end method

.method final g()V
    .locals 6

    .prologue
    .line 1388
    iget v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->D:I

    if-lez v0, :cond_1

    .line 1413
    :cond_0
    :goto_0
    return-void

    .line 1392
    :cond_1
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->k()[Ldbxyzptlk/db231222/d/b;

    move-result-object v0

    .line 1393
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v1

    .line 1394
    if-eqz v0, :cond_0

    array-length v0, v0

    if-lez v0, :cond_0

    .line 1396
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->l()Landroid/text/style/ImageSpan;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->A:Landroid/text/style/ImageSpan;

    .line 1397
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->A:Landroid/text/style/ImageSpan;

    if-eqz v0, :cond_3

    .line 1398
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->A:Landroid/text/style/ImageSpan;

    invoke-interface {v1, v0}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    .line 1402
    :goto_1
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 1403
    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v2

    .line 1404
    if-le v2, v0, :cond_0

    .line 1406
    const-string v3, "RecipientEditTextView"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1407
    const-string v3, "RecipientEditTextView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "There were extra characters after the last tokenizable entry."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1410
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-interface {v1, v0, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_0

    .line 1400
    :cond_3
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->b()Ldbxyzptlk/db231222/d/b;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    goto :goto_1
.end method

.method public final h()V
    .locals 4

    .prologue
    .line 1645
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    if-nez v0, :cond_0

    .line 1655
    :goto_0
    return-void

    .line 1648
    :cond_0
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 1649
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getSelectionEnd()I

    move-result v1

    .line 1650
    iget-object v2, p0, Lcom/android/ex/chips/RecipientEditTextView;->v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v2, v0, v1}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v2

    .line 1651
    invoke-direct {p0, v2, v1}, Lcom/android/ex/chips/RecipientEditTextView;->b(II)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1652
    invoke-direct {p0, v2, v1, v0}, Lcom/android/ex/chips/RecipientEditTextView;->a(IILandroid/text/Editable;)Z

    .line 1654
    :cond_1
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->setSelection(I)V

    goto :goto_0
.end method

.method final i()V
    .locals 5

    .prologue
    .line 1701
    iget v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->D:I

    if-lez v0, :cond_1

    .line 1729
    :cond_0
    :goto_0
    return-void

    .line 1705
    :cond_1
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->k()[Ldbxyzptlk/db231222/d/b;

    move-result-object v1

    .line 1706
    if-eqz v1, :cond_0

    array-length v0, v1

    if-lez v0, :cond_0

    .line 1707
    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    aget-object v2, v1, v0

    .line 1708
    const/4 v0, 0x0

    .line 1709
    array-length v3, v1

    const/4 v4, 0x1

    if-le v3, v4, :cond_2

    .line 1710
    array-length v0, v1

    add-int/lit8 v0, v0, -0x2

    aget-object v0, v1, v0

    .line 1712
    :cond_2
    const/4 v1, 0x0

    .line 1713
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v3

    invoke-interface {v3, v2}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v2

    .line 1714
    if-eqz v0, :cond_4

    .line 1715
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    .line 1716
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 1717
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-gt v0, v3, :cond_0

    .line 1721
    invoke-interface {v1, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v1

    const/16 v3, 0x20

    if-ne v1, v3, :cond_3

    .line 1722
    add-int/lit8 v0, v0, 0x1

    .line 1725
    :cond_3
    :goto_1
    if-ltz v0, :cond_0

    if-ltz v2, :cond_0

    if-ge v0, v2, :cond_0

    .line 1726
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1, v0, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method final j()Landroid/text/Spannable;
    .locals 1

    .prologue
    .line 1809
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method final k()[Ldbxyzptlk/db231222/d/b;
    .locals 4

    .prologue
    .line 2232
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    const-class v3, Ldbxyzptlk/db231222/d/b;

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/d/b;

    .line 2234
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2236
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v0

    .line 2237
    new-instance v2, Lcom/android/ex/chips/H;

    invoke-direct {v2, p0, v0}, Lcom/android/ex/chips/H;-><init>(Lcom/android/ex/chips/RecipientEditTextView;Landroid/text/Spannable;)V

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2252
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ldbxyzptlk/db231222/d/b;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/d/b;

    return-object v0
.end method

.method final l()Landroid/text/style/ImageSpan;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2279
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    const-class v2, Lcom/android/ex/chips/Q;

    invoke-interface {v0, v3, v1, v2}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/ex/chips/Q;

    .line 2281
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    aget-object v0, v0, v3

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final m()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2309
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    move v0, v1

    move v2, v1

    move v3, v1

    .line 2312
    :goto_0
    const/4 v5, 0x2

    if-ge v0, v5, :cond_0

    .line 2313
    iget-object v2, p0, Lcom/android/ex/chips/RecipientEditTextView;->v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v2, v4, v3}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenEnd(Ljava/lang/CharSequence;I)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/android/ex/chips/RecipientEditTextView;->b(I)I

    move-result v2

    .line 2312
    add-int/lit8 v0, v0, 0x1

    move v3, v2

    goto :goto_0

    .line 2318
    :cond_0
    invoke-virtual {p0, v4}, Lcom/android/ex/chips/RecipientEditTextView;->a(Landroid/text/Editable;)I

    move-result v0

    .line 2319
    add-int/lit8 v0, v0, -0x2

    invoke-direct {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->h(I)Lcom/android/ex/chips/Q;

    move-result-object v0

    .line 2320
    new-instance v3, Landroid/text/SpannableString;

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v5

    invoke-interface {v4, v2, v5}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-direct {v3, v5}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2321
    invoke-virtual {v3}, Landroid/text/SpannableString;->length()I

    move-result v5

    const/16 v6, 0x21

    invoke-virtual {v3, v0, v1, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2322
    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-interface {v4, v2, v1, v3}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 2323
    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->A:Landroid/text/style/ImageSpan;

    .line 2324
    return-void
.end method

.method final n()V
    .locals 14

    .prologue
    const/4 v2, 0x0

    .line 2347
    iget-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->E:Z

    if-eqz v0, :cond_1

    .line 2348
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->m()V

    .line 2401
    :cond_0
    :goto_0
    return-void

    .line 2352
    :cond_1
    iget-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->I:Z

    if-eqz v0, :cond_0

    .line 2355
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    const-class v3, Lcom/android/ex/chips/Q;

    invoke-interface {v0, v2, v1, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/ImageSpan;

    .line 2356
    array-length v1, v0

    if-lez v1, :cond_2

    .line 2357
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v1

    aget-object v0, v0, v2

    invoke-interface {v1, v0}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 2359
    :cond_2
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->k()[Ldbxyzptlk/db231222/d/b;

    move-result-object v4

    .line 2361
    if-eqz v4, :cond_3

    array-length v0, v4

    const/4 v1, 0x2

    if-gt v0, v1, :cond_4

    .line 2362
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->A:Landroid/text/style/ImageSpan;

    goto :goto_0

    .line 2365
    :cond_4
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v5

    .line 2366
    array-length v6, v4

    .line 2367
    add-int/lit8 v7, v6, -0x2

    .line 2368
    invoke-direct {p0, v7}, Lcom/android/ex/chips/RecipientEditTextView;->h(I)Lcom/android/ex/chips/Q;

    move-result-object v8

    .line 2369
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->H:Ljava/util/ArrayList;

    .line 2372
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v9

    .line 2373
    sub-int v0, v6, v7

    move v1, v2

    move v3, v2

    :goto_1
    array-length v10, v4

    if-ge v0, v10, :cond_9

    .line 2374
    iget-object v10, p0, Lcom/android/ex/chips/RecipientEditTextView;->H:Ljava/util/ArrayList;

    aget-object v11, v4, v0

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2375
    sub-int v10, v6, v7

    if-ne v0, v10, :cond_5

    .line 2376
    aget-object v3, v4, v0

    invoke-interface {v5, v3}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v3

    .line 2378
    :cond_5
    array-length v10, v4

    add-int/lit8 v10, v10, -0x1

    if-ne v0, v10, :cond_6

    .line 2379
    aget-object v1, v4, v0

    invoke-interface {v5, v1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v1

    .line 2381
    :cond_6
    iget-object v10, p0, Lcom/android/ex/chips/RecipientEditTextView;->b:Ljava/util/ArrayList;

    if-eqz v10, :cond_7

    iget-object v10, p0, Lcom/android/ex/chips/RecipientEditTextView;->b:Ljava/util/ArrayList;

    aget-object v11, v4, v0

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_8

    .line 2382
    :cond_7
    aget-object v10, v4, v0

    invoke-interface {v5, v10}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v10

    .line 2383
    aget-object v11, v4, v0

    invoke-interface {v5, v11}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v11

    .line 2384
    aget-object v12, v4, v0

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v12, v10}, Ldbxyzptlk/db231222/d/b;->a(Ljava/lang/String;)V

    .line 2386
    :cond_8
    aget-object v10, v4, v0

    invoke-interface {v5, v10}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 2373
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2388
    :cond_9
    invoke-interface {v9}, Landroid/text/Editable;->length()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 2389
    invoke-interface {v9}, Landroid/text/Editable;->length()I

    move-result v1

    .line 2391
    :cond_a
    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2392
    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2393
    new-instance v3, Landroid/text/SpannableString;

    invoke-interface {v9, v1, v0}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2394
    invoke-virtual {v3}, Landroid/text/SpannableString;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v3, v8, v2, v4, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2395
    invoke-interface {v9, v1, v0, v3}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 2396
    iput-object v8, p0, Lcom/android/ex/chips/RecipientEditTextView;->A:Landroid/text/style/ImageSpan;

    .line 2398
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->q()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getLineCount()I

    move-result v0

    iget v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->ah:I

    if-le v0, v1, :cond_0

    .line 2399
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getLineCount()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->setMaxLines(I)V

    goto/16 :goto_0
.end method

.method final o()V
    .locals 6

    .prologue
    .line 2409
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->A:Landroid/text/style/ImageSpan;

    if-eqz v0, :cond_0

    .line 2410
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v0

    .line 2411
    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->A:Landroid/text/style/ImageSpan;

    invoke-interface {v0, v1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 2412
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->A:Landroid/text/style/ImageSpan;

    .line 2414
    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->H:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->H:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 2416
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->k()[Ldbxyzptlk/db231222/d/b;

    move-result-object v1

    .line 2419
    if-eqz v1, :cond_0

    array-length v2, v1

    if-nez v2, :cond_1

    .line 2445
    :cond_0
    :goto_0
    return-void

    .line 2422
    :cond_1
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    invoke-interface {v0, v1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    .line 2423
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    .line 2424
    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->H:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/d/b;

    .line 2429
    invoke-interface {v0}, Ldbxyzptlk/db231222/d/b;->f()Ljava/lang/String;

    move-result-object v4

    .line 2434
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v4, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v5

    .line 2435
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, v5

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2437
    const/4 v4, -0x1

    if-eq v5, v4, :cond_2

    .line 2438
    const/16 v4, 0x21

    invoke-interface {v2, v0, v5, v1, v4}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1

    .line 2442
    :cond_3
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->H:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 2257
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 3353
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 3355
    const-string v1, ""

    iget-object v2, p0, Lcom/android/ex/chips/RecipientEditTextView;->R:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 3356
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->Q:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 3357
    return-void
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 2274
    const/4 v0, 0x0

    return v0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 3

    .prologue
    .line 447
    invoke-super {p0, p1}, Landroid/widget/MultiAutoCompleteTextView;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    .line 448
    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    and-int/lit16 v0, v0, 0xff

    .line 449
    and-int/lit8 v2, v0, 0x6

    if-eqz v2, :cond_0

    .line 451
    iget v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    xor-int/2addr v0, v2

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 453
    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/lit8 v0, v0, 0x6

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 455
    :cond_0
    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const/high16 v2, 0x40000000    # 2.0f

    and-int/2addr v0, v2

    if-eqz v0, :cond_1

    .line 456
    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const v2, -0x40000001    # -1.9999999f

    and-int/2addr v0, v2

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 458
    :cond_1
    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const/high16 v2, 0x10000000

    or-int/2addr v0, v2

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 460
    const/4 v0, 0x6

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->actionId:I

    .line 461
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v2, Lcom/android/ex/chips/v;->done:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Landroid/view/inputmethod/EditorInfo;->actionLabel:Ljava/lang/CharSequence;

    .line 462
    if-eqz v1, :cond_2

    .line 463
    new-instance v0, Lcom/android/ex/chips/L;

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Lcom/android/ex/chips/L;-><init>(Lcom/android/ex/chips/RecipientEditTextView;Landroid/view/inputmethod/InputConnection;Z)V

    .line 465
    :goto_0
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 0

    .prologue
    .line 2262
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 3347
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->R:Ljava/lang/String;

    .line 3348
    return-void
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 3215
    const/4 v0, 0x0

    return v0
.end method

.method public onDragEvent(Landroid/view/DragEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3271
    invoke-virtual {p1}, Landroid/view/DragEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 3282
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 3274
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/DragEvent;->getClipDescription()Landroid/content/ClipDescription;

    move-result-object v0

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/ClipDescription;->hasMimeType(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 3276
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->requestFocus()Z

    goto :goto_0

    .line 3279
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/DragEvent;->getClipData()Landroid/content/ClipData;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->a(Landroid/content/ClipData;)V

    goto :goto_0

    .line 3271
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 384
    const/4 v1, 0x6

    if-ne p2, v1, :cond_3

    .line 385
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->E()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 395
    :cond_0
    :goto_0
    return v0

    .line 388
    :cond_1
    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    if-eqz v1, :cond_2

    .line 389
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->G()V

    goto :goto_0

    .line 391
    :cond_2
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->D()Z

    move-result v1

    if-nez v1, :cond_0

    .line 395
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 707
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->J:Z

    .line 708
    invoke-super {p0}, Landroid/widget/MultiAutoCompleteTextView;->onFinishTemporaryDetach()V

    .line 709
    return-void
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .prologue
    .line 3221
    const/4 v0, 0x0

    return v0
.end method

.method public onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 657
    invoke-super {p0, p1, p2, p3}, Landroid/widget/MultiAutoCompleteTextView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 666
    iget-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->J:Z

    if-nez v0, :cond_0

    .line 667
    if-nez p1, :cond_1

    .line 668
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->x()V

    .line 673
    :cond_0
    :goto_0
    return-void

    .line 670
    :cond_1
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->y()V

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2156
    if-gez p3, :cond_0

    .line 2160
    :goto_0
    return-void

    .line 2159
    :cond_0
    invoke-direct {p0, p3}, Lcom/android/ex/chips/RecipientEditTextView;->g(I)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1780
    sparse-switch p1, :sswitch_data_0

    .line 1804
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/MultiAutoCompleteTextView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :cond_1
    :goto_0
    return v0

    .line 1782
    :sswitch_0
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->u()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 1790
    :sswitch_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1791
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->E()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1794
    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    if-eqz v1, :cond_2

    .line 1795
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->G()V

    goto :goto_0

    .line 1797
    :cond_2
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->D()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 1780
    nop

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_1
        0x42 -> :sswitch_1
        0x43 -> :sswitch_0
    .end sparse-switch
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1572
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    if-eqz v0, :cond_0

    .line 1573
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->G()V

    .line 1574
    const/4 v0, 0x1

    .line 1576
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/MultiAutoCompleteTextView;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1589
    packed-switch p1, :pswitch_data_0

    .line 1600
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/widget/MultiAutoCompleteTextView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 1591
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1592
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    if-eqz v0, :cond_1

    .line 1593
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->G()V

    goto :goto_0

    .line 1595
    :cond_1
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->E()Z

    goto :goto_0

    .line 1589
    :pswitch_data_0
    .packed-switch 0x3d
        :pswitch_0
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 543
    invoke-super/range {p0 .. p5}, Landroid/widget/MultiAutoCompleteTextView;->onLayout(ZIIII)V

    .line 544
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->am:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 545
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 546
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->am:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/ex/chips/RecipientEditTextView$ChipState;

    .line 547
    iget-object v3, v0, Lcom/android/ex/chips/RecipientEditTextView$ChipState;->d:Lcom/android/ex/chips/RecipientEntry;

    invoke-direct {p0, v3, v6, v6}, Lcom/android/ex/chips/RecipientEditTextView;->a(Lcom/android/ex/chips/RecipientEntry;ZZ)Ldbxyzptlk/db231222/d/b;

    move-result-object v3

    .line 548
    iget-object v4, v0, Lcom/android/ex/chips/RecipientEditTextView$ChipState;->a:Ljava/lang/String;

    invoke-interface {v3, v4}, Ldbxyzptlk/db231222/d/b;->a(Ljava/lang/String;)V

    .line 549
    iget v4, v0, Lcom/android/ex/chips/RecipientEditTextView$ChipState;->b:I

    iget v0, v0, Lcom/android/ex/chips/RecipientEditTextView$ChipState;->c:I

    const/16 v5, 0x21

    invoke-interface {v1, v3, v4, v0, v5}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 553
    :cond_0
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_1

    .line 554
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->x()V

    .line 557
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->am:Ljava/util/ArrayList;

    .line 559
    :cond_2
    return-void
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 3226
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    if-eqz v0, :cond_1

    .line 3242
    :cond_0
    :goto_0
    return-void

    .line 3229
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 3230
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 3231
    invoke-virtual {p0, v0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->getOffsetForPosition(FF)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/android/ex/chips/RecipientEditTextView;->e(I)I

    move-result v2

    .line 3232
    invoke-direct {p0, v2, v0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->a(IFF)Ldbxyzptlk/db231222/d/b;

    move-result-object v0

    .line 3233
    if-eqz v0, :cond_0

    .line 3234
    iget-boolean v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->aa:Z

    if-eqz v1, :cond_2

    .line 3236
    invoke-direct {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->k(Ldbxyzptlk/db231222/d/b;)V

    goto :goto_0

    .line 3239
    :cond_2
    invoke-interface {v0}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEntry;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->f(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 3361
    invoke-super {p0, p1, p2}, Landroid/widget/MultiAutoCompleteTextView;->onMeasure(II)V

    .line 3362
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->F()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3363
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 3364
    const/high16 v1, 0x40000000    # 2.0f

    if-eq v0, v1, :cond_0

    .line 3365
    iget v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->r:I

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getMeasuredHeight()I

    move-result v2

    if-le v1, v2, :cond_0

    .line 3367
    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_1

    .line 3368
    iget v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->r:I

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 3372
    :goto_0
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/android/ex/chips/RecipientEditTextView;->setMeasuredDimension(II)V

    .line 3376
    :cond_0
    return-void

    .line 3370
    :cond_1
    iget v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->r:I

    goto :goto_0
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 2266
    const/4 v0, 0x0

    return v0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 563
    check-cast p1, Landroid/os/Bundle;

    .line 564
    const-string v0, "super"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/MultiAutoCompleteTextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 572
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->y()V

    .line 573
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    .line 574
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v0

    const-class v3, Ldbxyzptlk/db231222/d/b;

    invoke-interface {v2, v1, v0, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/d/b;

    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 575
    invoke-interface {v2, v4}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 574
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 580
    :cond_0
    const-string v0, "chips"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->am:Ljava/util/ArrayList;

    .line 581
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 586
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->G()V

    .line 588
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->A:Landroid/text/style/ImageSpan;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    move v2, v0

    .line 589
    :goto_0
    if-eqz v2, :cond_0

    .line 590
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->y()V

    .line 593
    :cond_0
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 594
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    .line 596
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 597
    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v0

    const-class v6, Ldbxyzptlk/db231222/d/b;

    invoke-interface {v4, v1, v0, v6}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/d/b;

    .line 598
    array-length v6, v0

    :goto_1
    if-ge v1, v6, :cond_2

    aget-object v7, v0, v1

    .line 602
    invoke-interface {v7}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v8

    .line 603
    invoke-interface {v7}, Ldbxyzptlk/db231222/d/b;->f()Ljava/lang/String;

    move-result-object v9

    .line 604
    invoke-interface {v4, v7}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v10

    .line 605
    invoke-interface {v4, v7}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    .line 606
    new-instance v11, Lcom/android/ex/chips/RecipientEditTextView$ChipState;

    invoke-direct {v11, v9, v10, v7, v8}, Lcom/android/ex/chips/RecipientEditTextView$ChipState;-><init>(Ljava/lang/String;IILcom/android/ex/chips/RecipientEntry;)V

    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 598
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    move v2, v1

    .line 588
    goto :goto_0

    .line 609
    :cond_2
    const-string v0, "chips"

    invoke-virtual {v3, v0, v5}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 610
    const-string v0, "super"

    invoke-super {p0}, Landroid/widget/MultiAutoCompleteTextView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 612
    if-eqz v2, :cond_3

    .line 613
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->x()V

    .line 616
    :cond_3
    return-object v3
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .prologue
    .line 3331
    const/4 v0, 0x0

    return v0
.end method

.method public onSelectionChanged(II)V
    .locals 2

    .prologue
    .line 481
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->b()Ldbxyzptlk/db231222/d/b;

    move-result-object v0

    .line 482
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 484
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->setSelection(I)V

    .line 486
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/MultiAutoCompleteTextView;->onSelectionChanged(II)V

    .line 487
    return-void
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 3337
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 3342
    const/4 v0, 0x0

    return v0
.end method

.method public onSizeChanged(IIII)V
    .locals 2

    .prologue
    .line 1268
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/MultiAutoCompleteTextView;->onSizeChanged(IIII)V

    .line 1269
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 1270
    iget v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->D:I

    if-lez v0, :cond_1

    .line 1271
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->B()V

    .line 1277
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->W:Landroid/widget/ScrollView;

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->Z:Z

    if-nez v0, :cond_4

    .line 1278
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1279
    :goto_1
    if-eqz v0, :cond_2

    instance-of v1, v0, Landroid/widget/ScrollView;

    if-nez v1, :cond_2

    .line 1280
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_1

    .line 1273
    :cond_1
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->C()V

    goto :goto_0

    .line 1282
    :cond_2
    if-eqz v0, :cond_3

    .line 1283
    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->W:Landroid/widget/ScrollView;

    .line 1285
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->Z:Z

    .line 1287
    :cond_4
    return-void
.end method

.method public onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 701
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->J:Z

    .line 702
    invoke-super {p0}, Landroid/widget/MultiAutoCompleteTextView;->onStartTemporaryDetach()V

    .line 703
    return-void
.end method

.method public onTextContextMenuItem(I)Z
    .locals 2

    .prologue
    .line 2876
    const v0, 0x1020022

    if-ne p1, v0, :cond_0

    .line 2877
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 2879
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->a(Landroid/content/ClipData;)V

    .line 2880
    const/4 v0, 0x1

    .line 2882
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/MultiAutoCompleteTextView;->onTextContextMenuItem(I)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    .line 1914
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1916
    invoke-super {p0, p1}, Landroid/widget/MultiAutoCompleteTextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 1947
    :cond_0
    :goto_0
    return v0

    .line 1918
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/MultiAutoCompleteTextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 1919
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    .line 1920
    const/4 v0, 0x0

    .line 1921
    iget-object v4, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    if-nez v4, :cond_2

    .line 1922
    iget-object v4, p0, Lcom/android/ex/chips/RecipientEditTextView;->P:Landroid/view/GestureDetector;

    invoke-virtual {v4, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1924
    :cond_2
    iget-object v4, p0, Lcom/android/ex/chips/RecipientEditTextView;->R:Ljava/lang/String;

    if-nez v4, :cond_5

    if-ne v3, v2, :cond_5

    .line 1925
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 1926
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    .line 1927
    invoke-virtual {p0, v4, v5}, Lcom/android/ex/chips/RecipientEditTextView;->getOffsetForPosition(FF)I

    move-result v6

    invoke-direct {p0, v6}, Lcom/android/ex/chips/RecipientEditTextView;->e(I)I

    move-result v6

    .line 1928
    invoke-direct {p0, v6, v4, v5}, Lcom/android/ex/chips/RecipientEditTextView;->a(IFF)Ldbxyzptlk/db231222/d/b;

    move-result-object v7

    .line 1929
    if-eqz v7, :cond_5

    .line 1930
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    if-eq v0, v7, :cond_3

    .line 1931
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->G()V

    .line 1932
    invoke-direct {p0, v7}, Lcom/android/ex/chips/RecipientEditTextView;->h(Ldbxyzptlk/db231222/d/b;)Ldbxyzptlk/db231222/d/b;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    :goto_1
    move v1, v2

    move v0, v2

    .line 1944
    :goto_2
    if-ne v3, v2, :cond_0

    if-nez v1, :cond_0

    .line 1945
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->G()V

    goto :goto_0

    .line 1933
    :cond_3
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    if-nez v0, :cond_4

    .line 1934
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->setSelection(I)V

    .line 1935
    invoke-direct {p0}, Lcom/android/ex/chips/RecipientEditTextView;->E()Z

    .line 1936
    invoke-direct {p0, v7}, Lcom/android/ex/chips/RecipientEditTextView;->h(Ldbxyzptlk/db231222/d/b;)Ldbxyzptlk/db231222/d/b;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    goto :goto_1

    .line 1938
    :cond_4
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->x:Ldbxyzptlk/db231222/d/b;

    invoke-virtual {p0, v0, v6, v4, v5}, Lcom/android/ex/chips/RecipientEditTextView;->a(Ldbxyzptlk/db231222/d/b;IFF)V

    goto :goto_1

    :cond_5
    move v8, v0

    move v0, v1

    move v1, v8

    goto :goto_2
.end method

.method final p()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ldbxyzptlk/db231222/d/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2896
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2897
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getSelectionEnd()I

    move-result v1

    invoke-interface {v0, v4, v1}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v3

    .line 2898
    invoke-virtual {v4, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2900
    const/4 v1, 0x0

    .line 2901
    const/4 v0, 0x0

    .line 2902
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2903
    if-eqz v3, :cond_0

    move v2, v1

    move v1, v3

    .line 2905
    :goto_0
    if-eqz v1, :cond_5

    if-nez v0, :cond_5

    if-eq v1, v2, :cond_5

    .line 2907
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v0, v4, v1}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v2

    .line 2908
    invoke-direct {p0, v2}, Lcom/android/ex/chips/RecipientEditTextView;->f(I)Ldbxyzptlk/db231222/d/b;

    move-result-object v0

    .line 2909
    if-ne v2, v3, :cond_4

    if-nez v0, :cond_4

    move-object v7, v0

    move v0, v1

    move v1, v2

    move-object v2, v7

    .line 2913
    :goto_1
    if-eq v1, v3, :cond_0

    .line 2914
    if-eqz v2, :cond_3

    .line 2919
    :goto_2
    if-ge v0, v3, :cond_0

    .line 2920
    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenEnd(Ljava/lang/CharSequence;I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->b(I)I

    move-result v1

    .line 2922
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/android/ex/chips/RecipientEditTextView;->a(IILandroid/text/Editable;)Z

    .line 2923
    invoke-direct {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->f(I)Ldbxyzptlk/db231222/d/b;

    move-result-object v1

    .line 2924
    if-nez v1, :cond_2

    .line 2935
    :cond_0
    invoke-virtual {p0, v5}, Lcom/android/ex/chips/RecipientEditTextView;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2936
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 2937
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v5, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 2938
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-direct {p0, v1, v2, v0}, Lcom/android/ex/chips/RecipientEditTextView;->a(IILandroid/text/Editable;)Z

    .line 2939
    invoke-direct {p0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->f(I)Ldbxyzptlk/db231222/d/b;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2941
    :cond_1
    return-object v6

    .line 2928
    :cond_2
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 2929
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v7, v1

    move v1, v2

    move v2, v7

    goto :goto_0

    :cond_5
    move-object v7, v0

    move v0, v2

    move-object v2, v7

    goto :goto_1
.end method

.method protected performFiltering(Ljava/lang/CharSequence;I)V
    .locals 4

    .prologue
    .line 1859
    invoke-virtual {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 1860
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->enoughToFilter()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 1861
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getSelectionEnd()I

    move-result v0

    .line 1862
    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v1, p1, v0}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v1

    .line 1865
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v2

    .line 1866
    const-class v3, Ldbxyzptlk/db231222/d/b;

    invoke-interface {v2, v1, v0, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/d/b;

    .line 1867
    if-eqz v0, :cond_1

    array-length v0, v0

    if-lez v0, :cond_1

    .line 1878
    :goto_0
    return-void

    .line 1870
    :cond_0
    if-eqz v0, :cond_1

    .line 1874
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->dismissDropDown()V

    goto :goto_0

    .line 1877
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/MultiAutoCompleteTextView;->performFiltering(Ljava/lang/CharSequence;I)V

    goto :goto_0
.end method

.method public performValidation()V
    .locals 0

    .prologue
    .line 760
    return-void
.end method

.method protected final q()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 3379
    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/ex/chips/RecipientEditTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/ex/chips/a;

    invoke-virtual {v0}, Lcom/android/ex/chips/a;->a()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 3385
    const/4 v0, -0x1

    return v0
.end method

.method public removeTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 2716
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->V:Landroid/text/TextWatcher;

    .line 2717
    invoke-super {p0, p1}, Landroid/widget/MultiAutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2718
    return-void
.end method

.method protected replaceText(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 1564
    return-void
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 3389
    iget-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->K:Z

    return v0
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/widget/ListAdapter;",
            ":",
            "Landroid/widget/Filterable;",
            ">(TT;)V"
        }
    .end annotation

    .prologue
    .line 720
    invoke-super {p0, p1}, Landroid/widget/MultiAutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 721
    check-cast p1, Lcom/android/ex/chips/a;

    .line 722
    new-instance v0, Lcom/android/ex/chips/F;

    invoke-direct {v0, p0}, Lcom/android/ex/chips/F;-><init>(Lcom/android/ex/chips/RecipientEditTextView;)V

    invoke-virtual {p1, v0}, Lcom/android/ex/chips/a;->a(Lcom/android/ex/chips/j;)V

    .line 735
    iget-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->K:Z

    invoke-virtual {p1, v0}, Lcom/android/ex/chips/a;->a(Z)V

    .line 736
    iget-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->N:Z

    invoke-virtual {p1, v0}, Lcom/android/ex/chips/a;->c(Z)V

    .line 737
    iget-boolean v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->O:Z

    invoke-virtual {p1, v0}, Lcom/android/ex/chips/a;->b(Z)V

    .line 738
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->ab:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 739
    return-void
.end method

.method public setAllowsEditingOfInvalidTokens(Z)V
    .locals 0

    .prologue
    .line 3410
    iput-boolean p1, p0, Lcom/android/ex/chips/RecipientEditTextView;->M:Z

    .line 3411
    return-void
.end method

.method public setContactStatusResolver(Lcom/android/ex/chips/M;)V
    .locals 0

    .prologue
    .line 3427
    iput-object p1, p0, Lcom/android/ex/chips/RecipientEditTextView;->al:Lcom/android/ex/chips/M;

    .line 3428
    return-void
.end method

.method public setLayoutToShowSuggestionsIn(Landroid/widget/LinearLayout;)V
    .locals 2

    .prologue
    .line 3439
    iput-object p1, p0, Lcom/android/ex/chips/RecipientEditTextView;->S:Landroid/widget/LinearLayout;

    .line 3440
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->S:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/android/ex/chips/RecipientEditTextView;->ab:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 3441
    return-void
.end method

.method public setOnFocusListShrinkRecipients(Z)V
    .locals 0

    .prologue
    .line 1263
    iput-boolean p1, p0, Lcom/android/ex/chips/RecipientEditTextView;->I:Z

    .line 1264
    return-void
.end method

.method public setShowsAlternatesPopover(Z)V
    .locals 0

    .prologue
    .line 3401
    iput-boolean p1, p0, Lcom/android/ex/chips/RecipientEditTextView;->L:Z

    .line 3402
    return-void
.end method

.method public setShowsContactMethodIconForRecipients(Z)V
    .locals 0

    .prologue
    .line 3419
    iput-boolean p1, p0, Lcom/android/ex/chips/RecipientEditTextView;->O:Z

    .line 3420
    return-void
.end method

.method public setShowsContactPhoto(Z)V
    .locals 0

    .prologue
    .line 3393
    iput-boolean p1, p0, Lcom/android/ex/chips/RecipientEditTextView;->K:Z

    .line 3394
    return-void
.end method

.method public setSuggestionListener(Lcom/android/ex/chips/X;)V
    .locals 0

    .prologue
    .line 3431
    iput-object p1, p0, Lcom/android/ex/chips/RecipientEditTextView;->ak:Lcom/android/ex/chips/X;

    .line 3432
    return-void
.end method

.method public setTokenizer(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;)V
    .locals 1

    .prologue
    .line 1547
    iput-object p1, p0, Lcom/android/ex/chips/RecipientEditTextView;->v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    .line 1548
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->v:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-super {p0, v0}, Landroid/widget/MultiAutoCompleteTextView;->setTokenizer(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;)V

    .line 1549
    return-void
.end method

.method public setValidator(Landroid/widget/AutoCompleteTextView$Validator;)V
    .locals 0

    .prologue
    .line 1553
    iput-object p1, p0, Lcom/android/ex/chips/RecipientEditTextView;->w:Landroid/widget/AutoCompleteTextView$Validator;

    .line 1554
    invoke-super {p0, p1}, Landroid/widget/MultiAutoCompleteTextView;->setValidator(Landroid/widget/AutoCompleteTextView$Validator;)V

    .line 1555
    return-void
.end method

.method public showDropDown()V
    .locals 2

    .prologue
    .line 677
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->S:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    .line 678
    invoke-super {p0}, Landroid/widget/MultiAutoCompleteTextView;->showDropDown()V

    .line 682
    :goto_0
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->ak:Lcom/android/ex/chips/X;

    if-eqz v0, :cond_0

    .line 683
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->ak:Lcom/android/ex/chips/X;

    invoke-interface {v0, p0}, Lcom/android/ex/chips/X;->b(Lcom/android/ex/chips/RecipientEditTextView;)V

    .line 685
    :cond_0
    return-void

    .line 680
    :cond_1
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView;->ab:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0
.end method
