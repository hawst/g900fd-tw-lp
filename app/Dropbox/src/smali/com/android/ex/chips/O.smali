.class final Lcom/android/ex/chips/O;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/android/ex/chips/z;


# instance fields
.field final synthetic a:Ljava/util/ArrayList;

.field final synthetic b:Lcom/android/ex/chips/N;


# direct methods
.method constructor <init>(Lcom/android/ex/chips/N;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 3162
    iput-object p1, p0, Lcom/android/ex/chips/O;->b:Lcom/android/ex/chips/N;

    iput-object p2, p0, Lcom/android/ex/chips/O;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/ex/chips/RecipientEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3166
    iget-object v0, p0, Lcom/android/ex/chips/O;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/d/b;

    .line 3167
    invoke-interface {v0}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/ex/chips/RecipientEntry;->g()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/android/ex/chips/RecipientEntry;->a(J)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/ex/chips/O;->b:Lcom/android/ex/chips/N;

    iget-object v1, v1, Lcom/android/ex/chips/N;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v1}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    .line 3171
    iget-object v1, p0, Lcom/android/ex/chips/O;->b:Lcom/android/ex/chips/N;

    iget-object v3, v1, Lcom/android/ex/chips/N;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-interface {v0}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/ex/chips/RecipientEntry;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/ex/chips/RecipientEditTextView;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/ex/chips/RecipientEntry;

    invoke-static {v3, v1}, Lcom/android/ex/chips/RecipientEditTextView;->a(Lcom/android/ex/chips/RecipientEditTextView;Lcom/android/ex/chips/RecipientEntry;)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v1

    .line 3177
    if-nez v1, :cond_1

    iget-object v3, p0, Lcom/android/ex/chips/O;->b:Lcom/android/ex/chips/N;

    iget-object v3, v3, Lcom/android/ex/chips/N;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v3}, Lcom/android/ex/chips/RecipientEditTextView;->q()Z

    move-result v3

    if-nez v3, :cond_1

    .line 3178
    invoke-interface {v0}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v1

    .line 3181
    :cond_1
    if-eqz v1, :cond_0

    .line 3182
    iget-object v3, p0, Lcom/android/ex/chips/O;->b:Lcom/android/ex/chips/N;

    iget-object v3, v3, Lcom/android/ex/chips/N;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-static {v3}, Lcom/android/ex/chips/RecipientEditTextView;->f(Lcom/android/ex/chips/RecipientEditTextView;)Landroid/os/Handler;

    move-result-object v3

    new-instance v4, Lcom/android/ex/chips/P;

    invoke-direct {v4, p0, v0, v1}, Lcom/android/ex/chips/P;-><init>(Lcom/android/ex/chips/O;Ldbxyzptlk/db231222/d/b;Lcom/android/ex/chips/RecipientEntry;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 3191
    :cond_2
    return-void
.end method

.method public final a(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3196
    return-void
.end method
