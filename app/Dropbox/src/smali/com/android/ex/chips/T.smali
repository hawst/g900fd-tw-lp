.class final Lcom/android/ex/chips/T;
.super Landroid/os/AsyncTask;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/android/ex/chips/RecipientEditTextView;


# direct methods
.method private constructor <init>(Lcom/android/ex/chips/RecipientEditTextView;)V
    .locals 0

    .prologue
    .line 2961
    iput-object p1, p0, Lcom/android/ex/chips/T;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/ex/chips/RecipientEditTextView;Lcom/android/ex/chips/A;)V
    .locals 0

    .prologue
    .line 2961
    invoke-direct {p0, p1}, Lcom/android/ex/chips/T;-><init>(Lcom/android/ex/chips/RecipientEditTextView;)V

    return-void
.end method

.method private a(Lcom/android/ex/chips/RecipientEntry;)Ldbxyzptlk/db231222/d/b;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2964
    :try_start_0
    iget-object v1, p0, Lcom/android/ex/chips/T;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-static {v1}, Lcom/android/ex/chips/RecipientEditTextView;->o(Lcom/android/ex/chips/RecipientEditTextView;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2971
    :goto_0
    return-object v0

    .line 2967
    :cond_0
    iget-object v1, p0, Lcom/android/ex/chips/T;->a:Lcom/android/ex/chips/RecipientEditTextView;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, p1, v2, v3}, Lcom/android/ex/chips/RecipientEditTextView;->a(Lcom/android/ex/chips/RecipientEditTextView;Lcom/android/ex/chips/RecipientEntry;ZZ)Ldbxyzptlk/db231222/d/b;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 2969
    :catch_0
    move-exception v1

    .line 2970
    const-string v2, "RecipientEditTextView"

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/ex/chips/T;Lcom/android/ex/chips/RecipientEntry;)Ldbxyzptlk/db231222/d/b;
    .locals 1

    .prologue
    .line 2961
    invoke-direct {p0, p1}, Lcom/android/ex/chips/T;->a(Lcom/android/ex/chips/RecipientEntry;)Ldbxyzptlk/db231222/d/b;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/android/ex/chips/T;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 2961
    invoke-direct {p0, p1, p2}, Lcom/android/ex/chips/T;->a(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method private a(Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/d/b;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/d/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3086
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 3087
    new-instance v0, Lcom/android/ex/chips/V;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/ex/chips/V;-><init>(Lcom/android/ex/chips/T;Ljava/util/List;Ljava/util/List;)V

    .line 3135
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-ne v1, v2, :cond_1

    .line 3136
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 3141
    :cond_0
    :goto_0
    return-void

    .line 3138
    :cond_1
    iget-object v1, p0, Lcom/android/ex/chips/T;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-static {v1}, Lcom/android/ex/chips/RecipientEditTextView;->f(Lcom/android/ex/chips/RecipientEditTextView;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method


# virtual methods
.method protected final varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 3006
    iget-object v0, p0, Lcom/android/ex/chips/T;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-static {v0}, Lcom/android/ex/chips/RecipientEditTextView;->q(Lcom/android/ex/chips/RecipientEditTextView;)Lcom/android/ex/chips/N;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3007
    iget-object v0, p0, Lcom/android/ex/chips/T;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-static {v0}, Lcom/android/ex/chips/RecipientEditTextView;->q(Lcom/android/ex/chips/RecipientEditTextView;)Lcom/android/ex/chips/N;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/android/ex/chips/N;->cancel(Z)Z

    .line 3012
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 3014
    iget-object v0, p0, Lcom/android/ex/chips/T;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEditTextView;->k()[Ldbxyzptlk/db231222/d/b;

    move-result-object v3

    move v0, v1

    .line 3015
    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_1

    .line 3016
    aget-object v4, v3, v0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3015
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3018
    :cond_1
    iget-object v0, p0, Lcom/android/ex/chips/T;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-static {v0}, Lcom/android/ex/chips/RecipientEditTextView;->p(Lcom/android/ex/chips/RecipientEditTextView;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 3019
    iget-object v0, p0, Lcom/android/ex/chips/T;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-static {v0}, Lcom/android/ex/chips/RecipientEditTextView;->p(Lcom/android/ex/chips/RecipientEditTextView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 3021
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 3023
    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 3024
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/d/b;

    .line 3025
    if-eqz v0, :cond_3

    .line 3026
    iget-object v4, p0, Lcom/android/ex/chips/T;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-interface {v0}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/android/ex/chips/RecipientEditTextView;->b(Lcom/android/ex/chips/RecipientEntry;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3023
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3029
    :cond_4
    iget-object v0, p0, Lcom/android/ex/chips/T;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEditTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/ex/chips/a;

    .line 3030
    if-nez v0, :cond_5

    .line 3081
    :goto_2
    return-object v5

    .line 3033
    :cond_5
    iget-object v1, p0, Lcom/android/ex/chips/T;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v1}, Lcom/android/ex/chips/RecipientEditTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/ex/chips/a;->i()Landroid/accounts/Account;

    move-result-object v0

    new-instance v4, Lcom/android/ex/chips/U;

    invoke-direct {v4, p0, v2}, Lcom/android/ex/chips/U;-><init>(Lcom/android/ex/chips/T;Ljava/util/ArrayList;)V

    invoke-static {v1, v3, v0, v4}, Lcom/android/ex/chips/x;->a(Landroid/content/Context;Ljava/util/ArrayList;Landroid/accounts/Account;Lcom/android/ex/chips/z;)V

    goto :goto_2
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2961
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/ex/chips/T;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final onPreExecute()V
    .locals 6

    .prologue
    .line 2979
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2981
    iget-object v0, p0, Lcom/android/ex/chips/T;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEditTextView;->k()[Ldbxyzptlk/db231222/d/b;

    move-result-object v2

    .line 2982
    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 2983
    aget-object v3, v2, v0

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2982
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2985
    :cond_0
    iget-object v0, p0, Lcom/android/ex/chips/T;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-static {v0}, Lcom/android/ex/chips/RecipientEditTextView;->p(Lcom/android/ex/chips/RecipientEditTextView;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2986
    iget-object v0, p0, Lcom/android/ex/chips/T;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-static {v0}, Lcom/android/ex/chips/RecipientEditTextView;->p(Lcom/android/ex/chips/RecipientEditTextView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2989
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2992
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/d/b;

    .line 2993
    invoke-interface {v0}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/ex/chips/RecipientEntry;->g()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/android/ex/chips/RecipientEntry;->a(J)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/ex/chips/T;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v4}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v4

    invoke-interface {v4, v0}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2

    .line 2995
    invoke-interface {v0}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/ex/chips/T;->a(Lcom/android/ex/chips/RecipientEntry;)Ldbxyzptlk/db231222/d/b;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2997
    :cond_2
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 3001
    :cond_3
    invoke-direct {p0, v1, v2}, Lcom/android/ex/chips/T;->a(Ljava/util/List;Ljava/util/List;)V

    .line 3002
    return-void
.end method
