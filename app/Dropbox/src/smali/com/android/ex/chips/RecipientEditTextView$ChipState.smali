.class public Lcom/android/ex/chips/RecipientEditTextView$ChipState;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/ex/chips/RecipientEditTextView$ChipState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:I

.field public final d:Lcom/android/ex/chips/RecipientEntry;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 523
    new-instance v0, Lcom/android/ex/chips/J;

    invoke-direct {v0}, Lcom/android/ex/chips/J;-><init>()V

    sput-object v0, Lcom/android/ex/chips/RecipientEditTextView$ChipState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 516
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 517
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView$ChipState;->a:Ljava/lang/String;

    .line 518
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/ex/chips/RecipientEditTextView$ChipState;->b:I

    .line 519
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/ex/chips/RecipientEditTextView$ChipState;->c:I

    .line 520
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/ex/chips/RecipientEntry;

    iput-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView$ChipState;->d:Lcom/android/ex/chips/RecipientEntry;

    .line 521
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/android/ex/chips/A;)V
    .locals 0

    .prologue
    .line 489
    invoke-direct {p0, p1}, Lcom/android/ex/chips/RecipientEditTextView$ChipState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IILcom/android/ex/chips/RecipientEntry;)V
    .locals 0

    .prologue
    .line 496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 497
    iput-object p1, p0, Lcom/android/ex/chips/RecipientEditTextView$ChipState;->a:Ljava/lang/String;

    .line 498
    iput p2, p0, Lcom/android/ex/chips/RecipientEditTextView$ChipState;->b:I

    .line 499
    iput p3, p0, Lcom/android/ex/chips/RecipientEditTextView$ChipState;->c:I

    .line 500
    iput-object p4, p0, Lcom/android/ex/chips/RecipientEditTextView$ChipState;->d:Lcom/android/ex/chips/RecipientEntry;

    .line 501
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 505
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 510
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView$ChipState;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 511
    iget v0, p0, Lcom/android/ex/chips/RecipientEditTextView$ChipState;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 512
    iget v0, p0, Lcom/android/ex/chips/RecipientEditTextView$ChipState;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 513
    iget-object v0, p0, Lcom/android/ex/chips/RecipientEditTextView$ChipState;->d:Lcom/android/ex/chips/RecipientEntry;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 514
    return-void
.end method
