.class final Lcom/android/ex/chips/g;
.super Landroid/widget/Filter;
.source "panda.py"


# instance fields
.field final synthetic a:Lcom/android/ex/chips/a;

.field private final b:Lcom/android/ex/chips/i;

.field private c:I


# direct methods
.method public constructor <init>(Lcom/android/ex/chips/a;Lcom/android/ex/chips/i;)V
    .locals 0

    .prologue
    .line 340
    iput-object p1, p0, Lcom/android/ex/chips/g;->a:Lcom/android/ex/chips/a;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    .line 341
    iput-object p2, p0, Lcom/android/ex/chips/g;->b:Lcom/android/ex/chips/i;

    .line 342
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()I
    .locals 1

    .prologue
    .line 349
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/ex/chips/g;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)V
    .locals 1

    .prologue
    .line 345
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/android/ex/chips/g;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346
    monitor-exit p0

    return-void

    .line 345
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 358
    new-instance v0, Landroid/widget/Filter$FilterResults;

    invoke-direct {v0}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 359
    iput-object v1, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 360
    const/4 v2, 0x0

    iput v2, v0, Landroid/widget/Filter$FilterResults;->count:I

    .line 362
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 363
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 370
    :try_start_0
    iget-object v3, p0, Lcom/android/ex/chips/g;->a:Lcom/android/ex/chips/a;

    invoke-virtual {p0}, Lcom/android/ex/chips/g;->a()I

    move-result v4

    iget-object v5, p0, Lcom/android/ex/chips/g;->b:Lcom/android/ex/chips/i;

    iget-wide v5, v5, Lcom/android/ex/chips/i;->a:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Lcom/android/ex/chips/a;->a(Ljava/lang/CharSequence;ILjava/lang/Long;)Landroid/database/Cursor;

    move-result-object v1

    .line 372
    if-eqz v1, :cond_1

    .line 373
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 374
    new-instance v3, Lcom/android/ex/chips/l;

    invoke-direct {v3, v1}, Lcom/android/ex/chips/l;-><init>(Landroid/database/Cursor;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 378
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_0

    .line 379
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 378
    :cond_1
    if-eqz v1, :cond_2

    .line 379
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 382
    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 383
    iput-object v2, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 384
    const/4 v1, 0x1

    iput v1, v0, Landroid/widget/Filter$FilterResults;->count:I

    .line 393
    :cond_3
    return-object v0
.end method

.method protected final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 7

    .prologue
    .line 402
    iget-object v0, p0, Lcom/android/ex/chips/g;->a:Lcom/android/ex/chips/a;

    invoke-static {v0}, Lcom/android/ex/chips/a;->g(Lcom/android/ex/chips/a;)Lcom/android/ex/chips/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/ex/chips/f;->b()V

    .line 407
    iget-object v0, p0, Lcom/android/ex/chips/g;->a:Lcom/android/ex/chips/a;

    invoke-static {v0}, Lcom/android/ex/chips/a;->h(Lcom/android/ex/chips/a;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 408
    iget v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-lez v0, :cond_1

    .line 410
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    .line 413
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/ex/chips/l;

    .line 414
    iget-object v1, p0, Lcom/android/ex/chips/g;->b:Lcom/android/ex/chips/i;

    iget-wide v3, v1, Lcom/android/ex/chips/i;->a:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    iget-object v3, p0, Lcom/android/ex/chips/g;->a:Lcom/android/ex/chips/a;

    invoke-static {v3}, Lcom/android/ex/chips/a;->i(Lcom/android/ex/chips/a;)Ljava/util/LinkedHashMap;

    move-result-object v3

    iget-object v4, p0, Lcom/android/ex/chips/g;->a:Lcom/android/ex/chips/a;

    invoke-static {v4}, Lcom/android/ex/chips/a;->j(Lcom/android/ex/chips/a;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/android/ex/chips/g;->a:Lcom/android/ex/chips/a;

    invoke-static {v5}, Lcom/android/ex/chips/a;->k(Lcom/android/ex/chips/a;)Ljava/util/Set;

    move-result-object v5

    invoke-static {v0, v1, v3, v4, v5}, Lcom/android/ex/chips/a;->a(Lcom/android/ex/chips/l;ZLjava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Set;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 420
    :cond_1
    iget-object v0, p0, Lcom/android/ex/chips/g;->a:Lcom/android/ex/chips/a;

    invoke-static {v0}, Lcom/android/ex/chips/a;->l(Lcom/android/ex/chips/a;)I

    .line 421
    iget-object v0, p0, Lcom/android/ex/chips/g;->a:Lcom/android/ex/chips/a;

    invoke-static {v0}, Lcom/android/ex/chips/a;->m(Lcom/android/ex/chips/a;)I

    move-result v0

    if-lez v0, :cond_2

    .line 426
    iget-object v0, p0, Lcom/android/ex/chips/g;->a:Lcom/android/ex/chips/a;

    invoke-static {v0}, Lcom/android/ex/chips/a;->g(Lcom/android/ex/chips/a;)Lcom/android/ex/chips/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/ex/chips/f;->a()V

    .line 431
    :cond_2
    iget v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-gtz v0, :cond_3

    iget-object v0, p0, Lcom/android/ex/chips/g;->a:Lcom/android/ex/chips/a;

    invoke-static {v0}, Lcom/android/ex/chips/a;->m(Lcom/android/ex/chips/a;)I

    move-result v0

    if-nez v0, :cond_4

    .line 433
    :cond_3
    iget-object v0, p0, Lcom/android/ex/chips/g;->a:Lcom/android/ex/chips/a;

    invoke-static {v0}, Lcom/android/ex/chips/a;->a(Lcom/android/ex/chips/a;)V

    .line 438
    :cond_4
    iget-object v0, p0, Lcom/android/ex/chips/g;->a:Lcom/android/ex/chips/a;

    iget-object v1, p0, Lcom/android/ex/chips/g;->a:Lcom/android/ex/chips/a;

    iget-object v2, p0, Lcom/android/ex/chips/g;->a:Lcom/android/ex/chips/a;

    invoke-static {v2}, Lcom/android/ex/chips/a;->i(Lcom/android/ex/chips/a;)Ljava/util/LinkedHashMap;

    move-result-object v2

    iget-object v3, p0, Lcom/android/ex/chips/g;->a:Lcom/android/ex/chips/a;

    invoke-static {v3}, Lcom/android/ex/chips/a;->j(Lcom/android/ex/chips/a;)Ljava/util/List;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/android/ex/chips/a;->a(Lcom/android/ex/chips/a;Ljava/util/LinkedHashMap;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/ex/chips/a;->b(Lcom/android/ex/chips/a;Ljava/util/List;)V

    .line 439
    return-void
.end method
