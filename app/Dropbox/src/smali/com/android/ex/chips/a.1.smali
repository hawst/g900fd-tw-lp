.class public abstract Lcom/android/ex/chips/a;
.super Landroid/widget/BaseAdapter;
.source "panda.py"

# interfaces
.implements Landroid/widget/Filterable;


# instance fields
.field private final a:Lcom/android/ex/chips/p;

.field private final b:I

.field private c:Z

.field private d:Z

.field private e:Z

.field private final f:Landroid/content/Context;

.field private final g:Landroid/content/ContentResolver;

.field private final h:Landroid/view/LayoutInflater;

.field private i:Landroid/accounts/Account;

.field private final j:I

.field private final k:Landroid/os/Handler;

.field private l:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lcom/android/ex/chips/RecipientEntry;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/ex/chips/RecipientEntry;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/ex/chips/RecipientEntry;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/ex/chips/RecipientEntry;",
            ">;"
        }
    .end annotation
.end field

.field private q:I

.field private r:Ljava/lang/CharSequence;

.field private final s:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Landroid/net/Uri;",
            "[B>;"
        }
    .end annotation
.end field

.field private final t:Lcom/android/ex/chips/f;

.field private u:Lcom/android/ex/chips/j;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 517
    const/16 v0, 0xa

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/ex/chips/a;-><init>(Landroid/content/Context;II)V

    .line 518
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 532
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 100
    iput-boolean v2, p0, Lcom/android/ex/chips/a;->c:Z

    .line 101
    iput-boolean v0, p0, Lcom/android/ex/chips/a;->d:Z

    .line 102
    iput-boolean v0, p0, Lcom/android/ex/chips/a;->e:Z

    .line 447
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/ex/chips/a;->k:Landroid/os/Handler;

    .line 509
    new-instance v0, Lcom/android/ex/chips/f;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/ex/chips/f;-><init>(Lcom/android/ex/chips/a;Lcom/android/ex/chips/b;)V

    iput-object v0, p0, Lcom/android/ex/chips/a;->t:Lcom/android/ex/chips/f;

    .line 533
    iput-object p1, p0, Lcom/android/ex/chips/a;->f:Landroid/content/Context;

    .line 534
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/a;->g:Landroid/content/ContentResolver;

    .line 535
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/chips/a;->h:Landroid/view/LayoutInflater;

    .line 536
    iput p2, p0, Lcom/android/ex/chips/a;->j:I

    .line 537
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/android/ex/chips/a;->s:Landroid/util/LruCache;

    .line 538
    iput p3, p0, Lcom/android/ex/chips/a;->b:I

    .line 540
    if-nez p3, :cond_0

    .line 541
    sget-object v0, Lcom/android/ex/chips/m;->b:Lcom/android/ex/chips/p;

    iput-object v0, p0, Lcom/android/ex/chips/a;->a:Lcom/android/ex/chips/p;

    .line 548
    :goto_0
    return-void

    .line 542
    :cond_0
    if-ne p3, v2, :cond_1

    .line 543
    sget-object v0, Lcom/android/ex/chips/m;->a:Lcom/android/ex/chips/p;

    iput-object v0, p0, Lcom/android/ex/chips/a;->a:Lcom/android/ex/chips/p;

    goto :goto_0

    .line 545
    :cond_1
    sget-object v0, Lcom/android/ex/chips/m;->b:Lcom/android/ex/chips/p;

    iput-object v0, p0, Lcom/android/ex/chips/a;->a:Lcom/android/ex/chips/p;

    .line 546
    const-string v0, "BaseRecipientAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported query type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/ex/chips/a;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/android/ex/chips/a;->r:Ljava/lang/CharSequence;

    return-object p1
.end method

.method static synthetic a(Lcom/android/ex/chips/a;Ljava/util/LinkedHashMap;)Ljava/util/LinkedHashMap;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/android/ex/chips/a;->l:Ljava/util/LinkedHashMap;

    return-object p1
.end method

.method public static a(Landroid/content/Context;Landroid/database/Cursor;Landroid/accounts/Account;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/database/Cursor;",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/android/ex/chips/i;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    .line 570
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 571
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 572
    const/4 v1, 0x0

    .line 573
    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 574
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    .line 578
    const-wide/16 v7, 0x1

    cmp-long v0, v5, v7

    if-eqz v0, :cond_0

    .line 582
    new-instance v0, Lcom/android/ex/chips/i;

    invoke-direct {v0}, Lcom/android/ex/chips/i;-><init>()V

    .line 583
    const/4 v2, 0x4

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 584
    const/4 v2, 0x5

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 585
    iput-wide v5, v0, Lcom/android/ex/chips/i;->a:J

    .line 586
    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/android/ex/chips/i;->c:Ljava/lang/String;

    .line 587
    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/android/ex/chips/i;->d:Ljava/lang/String;

    .line 588
    const/4 v2, 0x2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/android/ex/chips/i;->e:Ljava/lang/String;

    .line 589
    if-eqz v7, :cond_1

    if-eqz v8, :cond_1

    .line 591
    :try_start_0
    invoke-virtual {v3, v7}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v2

    .line 593
    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/android/ex/chips/i;->b:Ljava/lang/String;

    .line 594
    iget-object v2, v0, Lcom/android/ex/chips/i;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 595
    const-string v2, "BaseRecipientAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cannot resolve directory name: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "@"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 607
    :cond_1
    :goto_1
    if-eqz p2, :cond_2

    iget-object v2, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v5, v0, Lcom/android/ex/chips/i;->d:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    iget-object v5, v0, Lcom/android/ex/chips/i;->e:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_2
    move-object v1, v0

    .line 613
    goto/16 :goto_0

    .line 598
    :catch_0
    move-exception v2

    .line 599
    const-string v5, "BaseRecipientAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Cannot resolve directory name: "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "@"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 611
    :cond_2
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    goto :goto_2

    .line 615
    :cond_3
    if-eqz v1, :cond_4

    .line 616
    invoke-interface {v4, v10, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 619
    :cond_4
    return-object v4
.end method

.method static synthetic a(Lcom/android/ex/chips/a;Ljava/util/LinkedHashMap;Ljava/util/List;)Ljava/util/List;
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/android/ex/chips/a;->a(Ljava/util/LinkedHashMap;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/android/ex/chips/a;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/android/ex/chips/a;->m:Ljava/util/List;

    return-object p1
.end method

.method private a(Ljava/util/LinkedHashMap;Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lcom/android/ex/chips/RecipientEntry;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Lcom/android/ex/chips/RecipientEntry;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/android/ex/chips/RecipientEntry;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 689
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 691
    invoke-virtual {p1}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 692
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 693
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    move v4, v2

    move v3, v1

    .line 694
    :goto_1
    if-ge v4, v7, :cond_0

    .line 695
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/ex/chips/RecipientEntry;

    .line 696
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 697
    invoke-direct {p0, v1}, Lcom/android/ex/chips/a;->a(Lcom/android/ex/chips/RecipientEntry;)V

    .line 698
    add-int/lit8 v3, v3, 0x1

    .line 694
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 700
    :cond_0
    iget v0, p0, Lcom/android/ex/chips/a;->j:I

    if-le v3, v0, :cond_2

    .line 704
    :goto_2
    iget v0, p0, Lcom/android/ex/chips/a;->j:I

    if-gt v3, v0, :cond_1

    .line 705
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/ex/chips/RecipientEntry;

    .line 706
    iget v2, p0, Lcom/android/ex/chips/a;->j:I

    if-le v3, v2, :cond_3

    .line 716
    :cond_1
    return-object v5

    :cond_2
    move v1, v3

    .line 703
    goto :goto_0

    .line 709
    :cond_3
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 710
    invoke-direct {p0, v0}, Lcom/android/ex/chips/a;->a(Lcom/android/ex/chips/RecipientEntry;)V

    .line 712
    add-int/lit8 v3, v3, 0x1

    .line 713
    goto :goto_3

    :cond_4
    move v3, v1

    goto :goto_2
.end method

.method static synthetic a(Lcom/android/ex/chips/a;Ljava/util/Set;)Ljava/util/Set;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/android/ex/chips/a;->n:Ljava/util/Set;

    return-object p1
.end method

.method private a(Lcom/android/ex/chips/RecipientEntry;)V
    .locals 2

    .prologue
    .line 748
    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->j()Landroid/net/Uri;

    move-result-object v1

    .line 749
    if-eqz v1, :cond_0

    .line 750
    iget-object v0, p0, Lcom/android/ex/chips/a;->s:Landroid/util/LruCache;

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 751
    if-eqz v0, :cond_1

    .line 752
    invoke-virtual {p1, v0}, Lcom/android/ex/chips/RecipientEntry;->a([B)V

    .line 762
    :cond_0
    :goto_0
    return-void

    .line 759
    :cond_1
    invoke-direct {p0, p1, v1}, Lcom/android/ex/chips/a;->b(Lcom/android/ex/chips/RecipientEntry;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/ex/chips/a;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/android/ex/chips/a;->k()V

    return-void
.end method

.method static synthetic a(Lcom/android/ex/chips/a;Ljava/lang/CharSequence;Ljava/util/List;I)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lcom/android/ex/chips/a;->a(Ljava/lang/CharSequence;Ljava/util/List;I)V

    return-void
.end method

.method static synthetic a(Lcom/android/ex/chips/l;ZLjava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 59
    invoke-static {p0, p1, p2, p3, p4}, Lcom/android/ex/chips/a;->b(Lcom/android/ex/chips/l;ZLjava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Set;)V

    return-void
.end method

.method private a(Ljava/lang/CharSequence;Ljava/util/List;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/List",
            "<",
            "Lcom/android/ex/chips/i;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 628
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    .line 630
    const/4 v0, 0x1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 631
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/ex/chips/i;

    .line 632
    iput-object p1, v0, Lcom/android/ex/chips/i;->f:Ljava/lang/CharSequence;

    .line 633
    iget-object v3, v0, Lcom/android/ex/chips/i;->g:Lcom/android/ex/chips/g;

    if-nez v3, :cond_0

    .line 634
    new-instance v3, Lcom/android/ex/chips/g;

    invoke-direct {v3, p0, v0}, Lcom/android/ex/chips/g;-><init>(Lcom/android/ex/chips/a;Lcom/android/ex/chips/i;)V

    iput-object v3, v0, Lcom/android/ex/chips/i;->g:Lcom/android/ex/chips/g;

    .line 636
    :cond_0
    iget-object v3, v0, Lcom/android/ex/chips/i;->g:Lcom/android/ex/chips/g;

    invoke-virtual {v3, p3}, Lcom/android/ex/chips/g;->a(I)V

    .line 637
    iget-object v0, v0, Lcom/android/ex/chips/i;->g:Lcom/android/ex/chips/g;

    invoke-virtual {v0, p1}, Lcom/android/ex/chips/g;->filter(Ljava/lang/CharSequence;)V

    .line 630
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 642
    :cond_1
    add-int/lit8 v0, v2, -0x1

    iput v0, p0, Lcom/android/ex/chips/a;->q:I

    .line 643
    iget-object v0, p0, Lcom/android/ex/chips/a;->t:Lcom/android/ex/chips/f;

    invoke-virtual {v0}, Lcom/android/ex/chips/f;->a()V

    .line 644
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/ex/chips/RecipientEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 730
    iput-object p1, p0, Lcom/android/ex/chips/a;->o:Ljava/util/List;

    .line 731
    iget-object v0, p0, Lcom/android/ex/chips/a;->u:Lcom/android/ex/chips/j;

    invoke-interface {v0, p1}, Lcom/android/ex/chips/j;->a(Ljava/util/List;)V

    .line 732
    invoke-virtual {p0}, Lcom/android/ex/chips/a;->notifyDataSetChanged()V

    .line 733
    return-void
.end method

.method static synthetic b(Lcom/android/ex/chips/a;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/android/ex/chips/a;->j:I

    return v0
.end method

.method private b(Lcom/android/ex/chips/RecipientEntry;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 765
    new-instance v0, Lcom/android/ex/chips/b;

    invoke-direct {v0, p0, p2, p1}, Lcom/android/ex/chips/b;-><init>(Lcom/android/ex/chips/a;Landroid/net/Uri;Lcom/android/ex/chips/RecipientEntry;)V

    .line 791
    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 792
    return-void
.end method

.method static synthetic b(Lcom/android/ex/chips/a;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/android/ex/chips/a;->a(Ljava/util/List;)V

    return-void
.end method

.method private static b(Lcom/android/ex/chips/l;ZLjava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Set;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/ex/chips/l;",
            "Z",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lcom/android/ex/chips/RecipientEntry;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Lcom/android/ex/chips/RecipientEntry;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 650
    iget-object v1, p0, Lcom/android/ex/chips/l;->b:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 679
    :goto_0
    return-void

    .line 654
    :cond_0
    iget-object v1, p0, Lcom/android/ex/chips/l;->b:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 656
    if-nez p1, :cond_1

    .line 657
    iget-object v1, p0, Lcom/android/ex/chips/l;->a:Ljava/lang/String;

    iget v2, p0, Lcom/android/ex/chips/l;->h:I

    iget-object v3, p0, Lcom/android/ex/chips/l;->b:Ljava/lang/String;

    iget v4, p0, Lcom/android/ex/chips/l;->c:I

    iget-object v5, p0, Lcom/android/ex/chips/l;->d:Ljava/lang/String;

    iget-wide v6, p0, Lcom/android/ex/chips/l;->e:J

    iget-wide v8, p0, Lcom/android/ex/chips/l;->f:J

    iget-object v10, p0, Lcom/android/ex/chips/l;->g:Ljava/lang/String;

    const/4 v11, 0x1

    invoke-static/range {v1 .. v11}, Lcom/android/ex/chips/RecipientEntry;->a(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;JJLjava/lang/String;Z)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 662
    :cond_1
    iget-wide v1, p0, Lcom/android/ex/chips/l;->e:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 664
    iget-wide v1, p0, Lcom/android/ex/chips/l;->e:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Ljava/util/List;

    .line 665
    iget-object v1, p0, Lcom/android/ex/chips/l;->a:Ljava/lang/String;

    iget v2, p0, Lcom/android/ex/chips/l;->h:I

    iget-object v3, p0, Lcom/android/ex/chips/l;->b:Ljava/lang/String;

    iget v4, p0, Lcom/android/ex/chips/l;->c:I

    iget-object v5, p0, Lcom/android/ex/chips/l;->d:Ljava/lang/String;

    iget-wide v6, p0, Lcom/android/ex/chips/l;->e:J

    iget-wide v8, p0, Lcom/android/ex/chips/l;->f:J

    iget-object v10, p0, Lcom/android/ex/chips/l;->g:Ljava/lang/String;

    const/4 v11, 0x1

    invoke-static/range {v1 .. v11}, Lcom/android/ex/chips/RecipientEntry;->b(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;JJLjava/lang/String;Z)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v1

    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 671
    :cond_2
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 672
    iget-object v1, p0, Lcom/android/ex/chips/l;->a:Ljava/lang/String;

    iget v2, p0, Lcom/android/ex/chips/l;->h:I

    iget-object v3, p0, Lcom/android/ex/chips/l;->b:Ljava/lang/String;

    iget v4, p0, Lcom/android/ex/chips/l;->c:I

    iget-object v5, p0, Lcom/android/ex/chips/l;->d:Ljava/lang/String;

    iget-wide v6, p0, Lcom/android/ex/chips/l;->e:J

    iget-wide v8, p0, Lcom/android/ex/chips/l;->f:J

    iget-object v10, p0, Lcom/android/ex/chips/l;->g:Ljava/lang/String;

    const/4 v11, 0x1

    invoke-static/range {v1 .. v11}, Lcom/android/ex/chips/RecipientEntry;->a(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;JJLjava/lang/String;Z)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v1

    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 677
    iget-wide v1, p0, Lcom/android/ex/chips/l;->e:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2, v1, v12}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method static synthetic c(Lcom/android/ex/chips/a;)Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/ex/chips/a;->g:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic d(Lcom/android/ex/chips/a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/ex/chips/a;->f:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e(Lcom/android/ex/chips/a;)Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/ex/chips/a;->i:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic f(Lcom/android/ex/chips/a;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/android/ex/chips/a;->j()V

    return-void
.end method

.method static synthetic g(Lcom/android/ex/chips/a;)Lcom/android/ex/chips/f;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/ex/chips/a;->t:Lcom/android/ex/chips/f;

    return-object v0
.end method

.method static synthetic h(Lcom/android/ex/chips/a;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/ex/chips/a;->r:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic i(Lcom/android/ex/chips/a;)Ljava/util/LinkedHashMap;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/ex/chips/a;->l:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method static synthetic j(Lcom/android/ex/chips/a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/ex/chips/a;->m:Ljava/util/List;

    return-object v0
.end method

.method private j()V
    .locals 1

    .prologue
    .line 736
    iget-object v0, p0, Lcom/android/ex/chips/a;->o:Ljava/util/List;

    iput-object v0, p0, Lcom/android/ex/chips/a;->p:Ljava/util/List;

    .line 737
    return-void
.end method

.method static synthetic k(Lcom/android/ex/chips/a;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/ex/chips/a;->n:Ljava/util/Set;

    return-object v0
.end method

.method private k()V
    .locals 1

    .prologue
    .line 740
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ex/chips/a;->p:Ljava/util/List;

    .line 741
    return-void
.end method

.method static synthetic l(Lcom/android/ex/chips/a;)I
    .locals 2

    .prologue
    .line 59
    iget v0, p0, Lcom/android/ex/chips/a;->q:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/android/ex/chips/a;->q:I

    return v0
.end method

.method private l()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/ex/chips/RecipientEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 744
    iget-object v0, p0, Lcom/android/ex/chips/a;->p:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/ex/chips/a;->p:Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/ex/chips/a;->o:Ljava/util/List;

    goto :goto_0
.end method

.method static synthetic m(Lcom/android/ex/chips/a;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/android/ex/chips/a;->q:I

    return v0
.end method

.method static synthetic n(Lcom/android/ex/chips/a;)Landroid/util/LruCache;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/ex/chips/a;->s:Landroid/util/LruCache;

    return-object v0
.end method

.method static synthetic o(Lcom/android/ex/chips/a;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/ex/chips/a;->k:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 551
    iget v0, p0, Lcom/android/ex/chips/a;->b:I

    return v0
.end method

.method protected a(Ljava/lang/CharSequence;ILjava/lang/Long;)Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 816
    iget-object v0, p0, Lcom/android/ex/chips/a;->a:Lcom/android/ex/chips/p;

    invoke-virtual {v0}, Lcom/android/ex/chips/p;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "limit"

    add-int/lit8 v2, p2, 0x5

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 820
    if-eqz p3, :cond_0

    .line 821
    const-string v0, "directory"

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 824
    :cond_0
    iget-object v0, p0, Lcom/android/ex/chips/a;->i:Landroid/accounts/Account;

    if-eqz v0, :cond_1

    .line 825
    const-string v0, "name_for_primary_account"

    iget-object v2, p0, Lcom/android/ex/chips/a;->i:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 826
    const-string v0, "type_for_primary_account"

    iget-object v2, p0, Lcom/android/ex/chips/a;->i:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 828
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 829
    iget-object v0, p0, Lcom/android/ex/chips/a;->g:Landroid/content/ContentResolver;

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/ex/chips/a;->a:Lcom/android/ex/chips/p;

    invoke-virtual {v2}, Lcom/android/ex/chips/p;->a()[Ljava/lang/String;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 831
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 838
    return-object v0
.end method

.method protected final a(Lcom/android/ex/chips/RecipientEntry;Landroid/net/Uri;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 795
    iget-object v0, p0, Lcom/android/ex/chips/a;->s:Landroid/util/LruCache;

    invoke-virtual {v0, p2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 796
    if-eqz v0, :cond_1

    .line 797
    invoke-virtual {p1, v0}, Lcom/android/ex/chips/RecipientEntry;->a([B)V

    .line 813
    :cond_0
    :goto_0
    return-void

    .line 800
    :cond_1
    iget-object v0, p0, Lcom/android/ex/chips/a;->g:Landroid/content/ContentResolver;

    sget-object v2, Lcom/android/ex/chips/k;->a:[Ljava/lang/String;

    move-object v1, p2

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 802
    if-eqz v1, :cond_0

    .line 804
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 805
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 806
    invoke-virtual {p1, v0}, Lcom/android/ex/chips/RecipientEntry;->a([B)V

    .line 807
    iget-object v2, p0, Lcom/android/ex/chips/a;->s:Landroid/util/LruCache;

    invoke-virtual {v2, p2, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 810
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final a(Lcom/android/ex/chips/j;)V
    .locals 0

    .prologue
    .line 725
    iput-object p1, p0, Lcom/android/ex/chips/a;->u:Lcom/android/ex/chips/j;

    .line 726
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1020
    iput-boolean p1, p0, Lcom/android/ex/chips/a;->c:Z

    .line 1021
    return-void
.end method

.method protected final b()I
    .locals 1

    .prologue
    .line 958
    sget v0, Lcom/android/ex/chips/u;->chips_recipient_dropdown_item:I

    return v0
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 1028
    iput-boolean p1, p0, Lcom/android/ex/chips/a;->d:Z

    .line 1029
    return-void
.end method

.method protected final c()I
    .locals 1

    .prologue
    .line 966
    sget v0, Lcom/android/ex/chips/s;->ic_contact_picture:I

    return v0
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 1037
    iput-boolean p1, p0, Lcom/android/ex/chips/a;->e:Z

    .line 1038
    return-void
.end method

.method protected final d()I
    .locals 1

    .prologue
    .line 974
    const v0, 0x1020016

    return v0
.end method

.method protected final e()I
    .locals 1

    .prologue
    .line 983
    const v0, 0x1020014

    return v0
.end method

.method protected final f()I
    .locals 1

    .prologue
    .line 991
    const v0, 0x1020015

    return v0
.end method

.method protected final g()I
    .locals 1

    .prologue
    .line 999
    const v0, 0x1020006

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 852
    invoke-direct {p0}, Lcom/android/ex/chips/a;->l()Ljava/util/List;

    move-result-object v0

    .line 853
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 2

    .prologue
    .line 565
    new-instance v0, Lcom/android/ex/chips/d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/ex/chips/d;-><init>(Lcom/android/ex/chips/a;Lcom/android/ex/chips/b;)V

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 858
    invoke-direct {p0}, Lcom/android/ex/chips/a;->l()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 863
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 873
    invoke-direct {p0}, Lcom/android/ex/chips/a;->l()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/ex/chips/RecipientEntry;

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEntry;->b()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v7, 0x0

    const/16 v11, 0x8

    const/4 v10, 0x0

    .line 883
    invoke-direct {p0}, Lcom/android/ex/chips/a;->l()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/ex/chips/RecipientEntry;

    .line 884
    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEntry;->c()Ljava/lang/String;

    move-result-object v1

    .line 885
    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEntry;->d()Ljava/lang/String;

    move-result-object v8

    .line 886
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c

    :cond_0
    move-object v6, v7

    .line 890
    :goto_0
    if-eqz p2, :cond_5

    .line 892
    :goto_1
    invoke-virtual {p0}, Lcom/android/ex/chips/a;->d()I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 893
    invoke-virtual {p0}, Lcom/android/ex/chips/a;->e()I

    move-result v2

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 894
    invoke-virtual {p0}, Lcom/android/ex/chips/a;->f()I

    move-result v3

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 896
    invoke-virtual {p0}, Lcom/android/ex/chips/a;->g()I

    move-result v4

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 897
    invoke-virtual {p0}, Lcom/android/ex/chips/a;->h()I

    move-result v5

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 898
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 899
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 904
    :goto_2
    if-eqz v3, :cond_1

    .line 905
    iget-object v2, p0, Lcom/android/ex/chips/a;->a:Lcom/android/ex/chips/p;

    iget-object v7, p0, Lcom/android/ex/chips/a;->f:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEntry;->e()I

    move-result v8

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEntry;->f()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v7, v8, v9}, Lcom/android/ex/chips/p;->a(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 909
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 912
    :cond_1
    iget-boolean v2, p0, Lcom/android/ex/chips/a;->e:Z

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEntry;->i()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 913
    :cond_2
    if-eqz v6, :cond_7

    .line 914
    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 915
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 919
    :goto_3
    if-eqz v4, :cond_3

    .line 920
    iget-boolean v1, p0, Lcom/android/ex/chips/a;->c:Z

    if-eqz v1, :cond_9

    .line 921
    invoke-virtual {v4, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 922
    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEntry;->k()[B

    move-result-object v0

    .line 923
    if-eqz v0, :cond_8

    .line 924
    array-length v1, v0

    invoke-static {v0, v10, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 926
    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 940
    :cond_3
    :goto_4
    if-eqz v5, :cond_4

    .line 941
    iget-boolean v0, p0, Lcom/android/ex/chips/a;->d:Z

    if-eqz v0, :cond_b

    .line 942
    invoke-virtual {v5, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 947
    :cond_4
    :goto_5
    return-object p2

    .line 890
    :cond_5
    iget-object v1, p0, Lcom/android/ex/chips/a;->h:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcom/android/ex/chips/a;->b()I

    move-result v2

    invoke-virtual {v1, v2, p3, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_1

    .line 901
    :cond_6
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 917
    :cond_7
    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 928
    :cond_8
    invoke-virtual {p0}, Lcom/android/ex/chips/a;->c()I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_4

    .line 931
    :cond_9
    invoke-virtual {v4, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4

    .line 935
    :cond_a
    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 936
    if-eqz v4, :cond_3

    .line 937
    const/4 v0, 0x4

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4

    .line 944
    :cond_b
    invoke-virtual {v5, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_5

    :cond_c
    move-object v6, v1

    goto/16 :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 868
    const/4 v0, 0x1

    return v0
.end method

.method protected final h()I
    .locals 1

    .prologue
    .line 1008
    const v0, 0x1020008

    return v0
.end method

.method public final i()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 1012
    iget-object v0, p0, Lcom/android/ex/chips/a;->i:Landroid/accounts/Account;

    return-object v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 878
    invoke-direct {p0}, Lcom/android/ex/chips/a;->l()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/ex/chips/RecipientEntry;

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEntry;->l()Z

    move-result v0

    return v0
.end method
