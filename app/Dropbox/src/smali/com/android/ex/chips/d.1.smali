.class final Lcom/android/ex/chips/d;
.super Landroid/widget/Filter;
.source "panda.py"


# instance fields
.field final synthetic a:Lcom/android/ex/chips/a;


# direct methods
.method private constructor <init>(Lcom/android/ex/chips/a;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lcom/android/ex/chips/d;->a:Lcom/android/ex/chips/a;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/ex/chips/a;Lcom/android/ex/chips/b;)V
    .locals 0

    .prologue
    .line 197
    invoke-direct {p0, p1}, Lcom/android/ex/chips/d;-><init>(Lcom/android/ex/chips/a;)V

    return-void
.end method


# virtual methods
.method public final convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 322
    check-cast p1, Lcom/android/ex/chips/RecipientEntry;

    .line 323
    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->c()Ljava/lang/String;

    move-result-object v1

    .line 324
    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->d()Ljava/lang/String;

    move-result-object v0

    .line 325
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 328
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v2, Landroid/text/util/Rfc822Token;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v0, v3}, Landroid/text/util/Rfc822Token;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/text/util/Rfc822Token;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 13

    .prologue
    const/4 v7, 0x0

    .line 206
    new-instance v6, Landroid/widget/Filter$FilterResults;

    invoke-direct {v6}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 210
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/android/ex/chips/d;->a:Lcom/android/ex/chips/a;

    invoke-static {v0}, Lcom/android/ex/chips/a;->a(Lcom/android/ex/chips/a;)V

    move-object v0, v6

    .line 281
    :goto_0
    return-object v0

    .line 217
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/android/ex/chips/d;->a:Lcom/android/ex/chips/a;

    iget-object v1, p0, Lcom/android/ex/chips/d;->a:Lcom/android/ex/chips/a;

    invoke-static {v1}, Lcom/android/ex/chips/a;->b(Lcom/android/ex/chips/a;)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/android/ex/chips/a;->a(Ljava/lang/CharSequence;ILjava/lang/Long;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v8

    .line 219
    if-nez v8, :cond_3

    .line 274
    :goto_1
    if-eqz v8, :cond_1

    .line 275
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 277
    :cond_1
    if-eqz v7, :cond_2

    .line 278
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v0, v6

    .line 281
    goto :goto_0

    .line 227
    :cond_3
    :try_start_1
    new-instance v9, Ljava/util/LinkedHashMap;

    invoke-direct {v9}, Ljava/util/LinkedHashMap;-><init>()V

    .line 229
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 231
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    .line 233
    :goto_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 236
    new-instance v0, Lcom/android/ex/chips/l;

    invoke-direct {v0, v8}, Lcom/android/ex/chips/l;-><init>(Landroid/database/Cursor;)V

    const/4 v1, 0x1

    invoke-static {v0, v1, v9, v10, v11}, Lcom/android/ex/chips/a;->a(Lcom/android/ex/chips/l;ZLjava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Set;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 274
    :catchall_0
    move-exception v0

    move-object v1, v7

    move-object v7, v8

    :goto_3
    if-eqz v7, :cond_4

    .line 275
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 277
    :cond_4
    if-eqz v1, :cond_5

    .line 278
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 241
    :cond_6
    :try_start_2
    iget-object v0, p0, Lcom/android/ex/chips/d;->a:Lcom/android/ex/chips/a;

    invoke-static {v0, v9, v10}, Lcom/android/ex/chips/a;->a(Lcom/android/ex/chips/a;Ljava/util/LinkedHashMap;Ljava/util/List;)Ljava/util/List;

    move-result-object v12

    .line 245
    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-eqz v0, :cond_9

    .line 274
    if-eqz v8, :cond_7

    .line 275
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 277
    :cond_7
    if-eqz v7, :cond_8

    .line 278
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_8
    move-object v0, v6

    goto :goto_0

    .line 251
    :cond_9
    :try_start_3
    iget-object v0, p0, Lcom/android/ex/chips/d;->a:Lcom/android/ex/chips/a;

    invoke-static {v0}, Lcom/android/ex/chips/a;->b(Lcom/android/ex/chips/a;)I

    move-result v0

    invoke-interface {v11}, Ljava/util/Set;->size()I

    move-result v1

    sub-int/2addr v0, v1

    .line 253
    if-lez v0, :cond_a

    .line 259
    iget-object v0, p0, Lcom/android/ex/chips/d;->a:Lcom/android/ex/chips/a;

    invoke-static {v0}, Lcom/android/ex/chips/a;->c(Lcom/android/ex/chips/a;)Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/ex/chips/h;->a:Landroid/net/Uri;

    sget-object v2, Lcom/android/ex/chips/h;->b:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 262
    :try_start_4
    iget-object v0, p0, Lcom/android/ex/chips/d;->a:Lcom/android/ex/chips/a;

    invoke-static {v0}, Lcom/android/ex/chips/a;->d(Lcom/android/ex/chips/a;)Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/android/ex/chips/d;->a:Lcom/android/ex/chips/a;

    invoke-static {v2}, Lcom/android/ex/chips/a;->e(Lcom/android/ex/chips/a;)Landroid/accounts/Account;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/ex/chips/a;->a(Landroid/content/Context;Landroid/database/Cursor;Landroid/accounts/Account;)Ljava/util/List;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    move-result-object v5

    move-object v7, v1

    .line 268
    :goto_4
    :try_start_5
    new-instance v0, Lcom/android/ex/chips/e;

    move-object v1, v12

    move-object v2, v9

    move-object v3, v10

    move-object v4, v11

    invoke-direct/range {v0 .. v5}, Lcom/android/ex/chips/e;-><init>(Ljava/util/List;Ljava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Set;Ljava/util/List;)V

    iput-object v0, v6, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 271
    const/4 v0, 0x1

    iput v0, v6, Landroid/widget/Filter$FilterResults;->count:I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_1

    .line 274
    :catchall_1
    move-exception v0

    move-object v1, v7

    move-object v7, v8

    goto :goto_3

    :cond_a
    move-object v5, v7

    .line 265
    goto :goto_4

    .line 274
    :catchall_2
    move-exception v0

    move-object v1, v7

    goto :goto_3

    :catchall_3
    move-exception v0

    move-object v7, v8

    goto :goto_3
.end method

.method protected final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 3

    .prologue
    .line 289
    iget-object v0, p0, Lcom/android/ex/chips/d;->a:Lcom/android/ex/chips/a;

    invoke-static {v0, p1}, Lcom/android/ex/chips/a;->a(Lcom/android/ex/chips/a;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 291
    iget-object v0, p0, Lcom/android/ex/chips/d;->a:Lcom/android/ex/chips/a;

    invoke-static {v0}, Lcom/android/ex/chips/a;->a(Lcom/android/ex/chips/a;)V

    .line 293
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 294
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Lcom/android/ex/chips/e;

    .line 295
    iget-object v1, p0, Lcom/android/ex/chips/d;->a:Lcom/android/ex/chips/a;

    iget-object v2, v0, Lcom/android/ex/chips/e;->b:Ljava/util/LinkedHashMap;

    invoke-static {v1, v2}, Lcom/android/ex/chips/a;->a(Lcom/android/ex/chips/a;Ljava/util/LinkedHashMap;)Ljava/util/LinkedHashMap;

    .line 296
    iget-object v1, p0, Lcom/android/ex/chips/d;->a:Lcom/android/ex/chips/a;

    iget-object v2, v0, Lcom/android/ex/chips/e;->c:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/android/ex/chips/a;->a(Lcom/android/ex/chips/a;Ljava/util/List;)Ljava/util/List;

    .line 297
    iget-object v1, p0, Lcom/android/ex/chips/d;->a:Lcom/android/ex/chips/a;

    iget-object v2, v0, Lcom/android/ex/chips/e;->d:Ljava/util/Set;

    invoke-static {v1, v2}, Lcom/android/ex/chips/a;->a(Lcom/android/ex/chips/a;Ljava/util/Set;)Ljava/util/Set;

    .line 301
    iget-object v1, v0, Lcom/android/ex/chips/e;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/android/ex/chips/e;->e:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 303
    iget-object v1, p0, Lcom/android/ex/chips/d;->a:Lcom/android/ex/chips/a;

    invoke-static {v1}, Lcom/android/ex/chips/a;->f(Lcom/android/ex/chips/a;)V

    .line 306
    :cond_0
    iget-object v1, p0, Lcom/android/ex/chips/d;->a:Lcom/android/ex/chips/a;

    iget-object v2, v0, Lcom/android/ex/chips/e;->a:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/android/ex/chips/a;->b(Lcom/android/ex/chips/a;Ljava/util/List;)V

    .line 309
    iget-object v1, v0, Lcom/android/ex/chips/e;->e:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 310
    iget-object v1, p0, Lcom/android/ex/chips/d;->a:Lcom/android/ex/chips/a;

    invoke-static {v1}, Lcom/android/ex/chips/a;->b(Lcom/android/ex/chips/a;)I

    move-result v1

    iget-object v2, v0, Lcom/android/ex/chips/e;->d:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    sub-int/2addr v1, v2

    .line 312
    iget-object v2, p0, Lcom/android/ex/chips/d;->a:Lcom/android/ex/chips/a;

    iget-object v0, v0, Lcom/android/ex/chips/e;->e:Ljava/util/List;

    invoke-static {v2, p1, v0, v1}, Lcom/android/ex/chips/a;->a(Lcom/android/ex/chips/a;Ljava/lang/CharSequence;Ljava/util/List;I)V

    .line 318
    :cond_1
    :goto_0
    return-void

    .line 315
    :cond_2
    iget-object v0, p0, Lcom/android/ex/chips/d;->a:Lcom/android/ex/chips/a;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/ex/chips/a;->b(Lcom/android/ex/chips/a;Ljava/util/List;)V

    goto :goto_0
.end method
