.class final Lcom/android/ex/chips/b;
.super Landroid/os/AsyncTask;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/net/Uri;

.field final synthetic b:Lcom/android/ex/chips/RecipientEntry;

.field final synthetic c:Lcom/android/ex/chips/a;


# direct methods
.method constructor <init>(Lcom/android/ex/chips/a;Landroid/net/Uri;Lcom/android/ex/chips/RecipientEntry;)V
    .locals 0

    .prologue
    .line 765
    iput-object p1, p0, Lcom/android/ex/chips/b;->c:Lcom/android/ex/chips/a;

    iput-object p2, p0, Lcom/android/ex/chips/b;->a:Landroid/net/Uri;

    iput-object p3, p0, Lcom/android/ex/chips/b;->b:Lcom/android/ex/chips/RecipientEntry;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 768
    iget-object v0, p0, Lcom/android/ex/chips/b;->c:Lcom/android/ex/chips/a;

    invoke-static {v0}, Lcom/android/ex/chips/a;->c(Lcom/android/ex/chips/a;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/ex/chips/b;->a:Landroid/net/Uri;

    sget-object v2, Lcom/android/ex/chips/k;->a:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 770
    if-eqz v1, :cond_1

    .line 772
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 773
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 774
    iget-object v2, p0, Lcom/android/ex/chips/b;->b:Lcom/android/ex/chips/RecipientEntry;

    invoke-virtual {v2, v0}, Lcom/android/ex/chips/RecipientEntry;->a([B)V

    .line 776
    iget-object v2, p0, Lcom/android/ex/chips/b;->c:Lcom/android/ex/chips/a;

    invoke-static {v2}, Lcom/android/ex/chips/a;->o(Lcom/android/ex/chips/a;)Landroid/os/Handler;

    move-result-object v2

    new-instance v4, Lcom/android/ex/chips/c;

    invoke-direct {v4, p0, v0}, Lcom/android/ex/chips/c;-><init>(Lcom/android/ex/chips/b;[B)V

    invoke-virtual {v2, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 785
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 788
    :cond_1
    return-object v3

    .line 785
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 765
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/ex/chips/b;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
