.class final Lcom/android/ex/chips/U;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/android/ex/chips/z;


# instance fields
.field final synthetic a:Ljava/util/ArrayList;

.field final synthetic b:Lcom/android/ex/chips/T;


# direct methods
.method constructor <init>(Lcom/android/ex/chips/T;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 3034
    iput-object p1, p0, Lcom/android/ex/chips/U;->b:Lcom/android/ex/chips/T;

    iput-object p2, p0, Lcom/android/ex/chips/U;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/ex/chips/RecipientEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 3037
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 3039
    iget-object v0, p0, Lcom/android/ex/chips/U;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/d/b;

    .line 3041
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/ex/chips/RecipientEntry;->g()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/android/ex/chips/RecipientEntry;->a(J)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/ex/chips/U;->b:Lcom/android/ex/chips/T;

    iget-object v4, v4, Lcom/android/ex/chips/T;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v4}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v4

    invoke-interface {v4, v0}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2

    .line 3045
    iget-object v4, p0, Lcom/android/ex/chips/U;->b:Lcom/android/ex/chips/T;

    iget-object v4, v4, Lcom/android/ex/chips/T;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-interface {v0}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEntry;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/ex/chips/RecipientEditTextView;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/ex/chips/RecipientEntry;

    invoke-static {v4, v0}, Lcom/android/ex/chips/RecipientEditTextView;->a(Lcom/android/ex/chips/RecipientEditTextView;Lcom/android/ex/chips/RecipientEntry;)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v0

    .line 3049
    :goto_1
    if-eqz v0, :cond_0

    .line 3050
    iget-object v4, p0, Lcom/android/ex/chips/U;->b:Lcom/android/ex/chips/T;

    invoke-static {v4, v0}, Lcom/android/ex/chips/T;->a(Lcom/android/ex/chips/T;Lcom/android/ex/chips/RecipientEntry;)Ldbxyzptlk/db231222/d/b;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3052
    :cond_0
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3055
    :cond_1
    iget-object v0, p0, Lcom/android/ex/chips/U;->b:Lcom/android/ex/chips/T;

    iget-object v1, p0, Lcom/android/ex/chips/U;->a:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lcom/android/ex/chips/T;->a(Lcom/android/ex/chips/T;Ljava/util/List;Ljava/util/List;)V

    .line 3056
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(Ljava/util/Set;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 3060
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 3063
    iget-object v0, p0, Lcom/android/ex/chips/U;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/d/b;

    .line 3064
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/ex/chips/RecipientEntry;->g()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/android/ex/chips/RecipientEntry;->a(J)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/ex/chips/U;->b:Lcom/android/ex/chips/T;

    iget-object v3, v3, Lcom/android/ex/chips/T;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v3}, Lcom/android/ex/chips/RecipientEditTextView;->j()Landroid/text/Spannable;

    move-result-object v3

    invoke-interface {v3, v0}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 3067
    invoke-interface {v0}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/ex/chips/RecipientEntry;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3069
    iget-object v3, p0, Lcom/android/ex/chips/U;->b:Lcom/android/ex/chips/T;

    invoke-interface {v0}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/android/ex/chips/T;->a(Lcom/android/ex/chips/T;Lcom/android/ex/chips/RecipientEntry;)Ldbxyzptlk/db231222/d/b;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3071
    :cond_0
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3074
    :cond_1
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3078
    :cond_2
    iget-object v0, p0, Lcom/android/ex/chips/U;->b:Lcom/android/ex/chips/T;

    iget-object v2, p0, Lcom/android/ex/chips/U;->a:Ljava/util/ArrayList;

    invoke-static {v0, v2, v1}, Lcom/android/ex/chips/T;->a(Lcom/android/ex/chips/T;Ljava/util/List;Ljava/util/List;)V

    .line 3079
    return-void
.end method
