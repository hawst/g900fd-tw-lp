.class final Lcom/android/ex/chips/V;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Lcom/android/ex/chips/T;


# direct methods
.method constructor <init>(Lcom/android/ex/chips/T;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 3087
    iput-object p1, p0, Lcom/android/ex/chips/V;->c:Lcom/android/ex/chips/T;

    iput-object p2, p0, Lcom/android/ex/chips/V;->a:Ljava/util/List;

    iput-object p3, p0, Lcom/android/ex/chips/V;->b:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 3090
    new-instance v5, Landroid/text/SpannableStringBuilder;

    iget-object v0, p0, Lcom/android/ex/chips/V;->c:Lcom/android/ex/chips/T;

    iget-object v0, v0, Lcom/android/ex/chips/T;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-direct {v5, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 3092
    iget-object v0, p0, Lcom/android/ex/chips/V;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v3

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/d/b;

    .line 3093
    iget-object v1, p0, Lcom/android/ex/chips/V;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldbxyzptlk/db231222/d/b;

    .line 3094
    if-eqz v1, :cond_0

    .line 3095
    invoke-interface {v0}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v4

    .line 3096
    invoke-interface {v1}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v7

    .line 3097
    invoke-static {v4, v7}, Lcom/android/ex/chips/x;->a(Lcom/android/ex/chips/RecipientEntry;Lcom/android/ex/chips/RecipientEntry;)Lcom/android/ex/chips/RecipientEntry;

    move-result-object v4

    if-ne v4, v7, :cond_1

    const/4 v4, 0x1

    .line 3101
    :goto_1
    if-eqz v4, :cond_0

    .line 3103
    invoke-interface {v5, v0}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    .line 3104
    const/4 v7, -0x1

    if-eq v4, v7, :cond_0

    .line 3107
    invoke-interface {v5, v0}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 3109
    invoke-interface {v5, v0}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 3112
    new-instance v0, Landroid/text/SpannableString;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/android/ex/chips/V;->c:Lcom/android/ex/chips/T;

    iget-object v9, v9, Lcom/android/ex/chips/T;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-interface {v1}, Ldbxyzptlk/db231222/d/b;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/android/ex/chips/RecipientEditTextView;->b(Lcom/android/ex/chips/RecipientEntry;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 3115
    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    const/16 v9, 0x21

    invoke-virtual {v0, v1, v3, v8, v9}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 3121
    invoke-interface {v5, v4, v7, v0}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 3122
    invoke-virtual {v0}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ldbxyzptlk/db231222/d/b;->a(Ljava/lang/String;)V

    .line 3123
    iget-object v0, p0, Lcom/android/ex/chips/V;->b:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v0, v2, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3125
    iget-object v0, p0, Lcom/android/ex/chips/V;->a:Ljava/util/List;

    invoke-interface {v0, v2, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3129
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 3130
    goto/16 :goto_0

    :cond_1
    move v4, v3

    .line 3097
    goto :goto_1

    .line 3131
    :cond_2
    iget-object v0, p0, Lcom/android/ex/chips/V;->c:Lcom/android/ex/chips/T;

    iget-object v0, v0, Lcom/android/ex/chips/T;->a:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v0, v5}, Lcom/android/ex/chips/RecipientEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 3132
    return-void
.end method
