.class public final Lcom/android/ex/chips/w;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final RecipientEditTextView:[I

.field public static final RecipientEditTextView_allowsEditingOfInvalidToken:I = 0xf

.field public static final RecipientEditTextView_chipAlternatesLayout:I = 0x9

.field public static final RecipientEditTextView_chipBackground:I = 0x2

.field public static final RecipientEditTextView_chipBackgroundPressed:I = 0x3

.field public static final RecipientEditTextView_chipDelete:I = 0x6

.field public static final RecipientEditTextView_chipFontSize:I = 0xc

.field public static final RecipientEditTextView_chipHeight:I = 0xb

.field public static final RecipientEditTextView_chipPadding:I = 0xa

.field public static final RecipientEditTextView_chipWarn:I = 0x7

.field public static final RecipientEditTextView_chipWarnPressed:I = 0x8

.field public static final RecipientEditTextView_invalidChipBackground:I = 0x1

.field public static final RecipientEditTextView_invalidChipBackgroundPressed:I = 0x0

.field public static final RecipientEditTextView_showsAlternatesPopover:I = 0xe

.field public static final RecipientEditTextView_showsContactMethodIconForRecipients:I = 0x11

.field public static final RecipientEditTextView_showsContactPhoto:I = 0xd

.field public static final RecipientEditTextView_showsNameAndDestinationForAllRecipients:I = 0x10

.field public static final RecipientEditTextView_warnChipBackground:I = 0x4

.field public static final RecipientEditTextView_warnChipBackgroundPressed:I = 0x5


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const/16 v0, 0x12

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/ex/chips/w;->RecipientEditTextView:[I

    return-void

    :array_0
    .array-data 4
        0x7f01001f
        0x7f010020
        0x7f010021
        0x7f010022
        0x7f010023
        0x7f010024
        0x7f010025
        0x7f010026
        0x7f010027
        0x7f010028
        0x7f010029
        0x7f01002a
        0x7f01002b
        0x7f01002c
        0x7f01002d
        0x7f01002e
        0x7f01002f
        0x7f010030
    .end array-data
.end method
