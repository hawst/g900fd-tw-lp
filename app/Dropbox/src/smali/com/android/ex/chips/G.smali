.class final Lcom/android/ex/chips/G;
.super Landroid/os/AsyncTask;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/widget/ListAdapter;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ldbxyzptlk/db231222/d/b;

.field final synthetic b:Landroid/widget/ListPopupWindow;

.field final synthetic c:I

.field final synthetic d:Lcom/android/ex/chips/RecipientEditTextView;


# direct methods
.method constructor <init>(Lcom/android/ex/chips/RecipientEditTextView;Ldbxyzptlk/db231222/d/b;Landroid/widget/ListPopupWindow;I)V
    .locals 0

    .prologue
    .line 1958
    iput-object p1, p0, Lcom/android/ex/chips/G;->d:Lcom/android/ex/chips/RecipientEditTextView;

    iput-object p2, p0, Lcom/android/ex/chips/G;->a:Ldbxyzptlk/db231222/d/b;

    iput-object p3, p0, Lcom/android/ex/chips/G;->b:Landroid/widget/ListPopupWindow;

    iput p4, p0, Lcom/android/ex/chips/G;->c:I

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final varargs a([Ljava/lang/Void;)Landroid/widget/ListAdapter;
    .locals 2

    .prologue
    .line 1961
    iget-object v0, p0, Lcom/android/ex/chips/G;->d:Lcom/android/ex/chips/RecipientEditTextView;

    iget-object v1, p0, Lcom/android/ex/chips/G;->a:Ldbxyzptlk/db231222/d/b;

    invoke-static {v0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->a(Lcom/android/ex/chips/RecipientEditTextView;Ldbxyzptlk/db231222/d/b;)Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/widget/ListAdapter;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 1966
    iget-object v0, p0, Lcom/android/ex/chips/G;->d:Lcom/android/ex/chips/RecipientEditTextView;

    iget-object v1, p0, Lcom/android/ex/chips/G;->a:Ldbxyzptlk/db231222/d/b;

    invoke-static {v0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->b(Lcom/android/ex/chips/RecipientEditTextView;Ldbxyzptlk/db231222/d/b;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1967
    iget-object v1, p0, Lcom/android/ex/chips/G;->d:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v1}, Lcom/android/ex/chips/RecipientEditTextView;->getHeight()I

    move-result v1

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v1, v0

    neg-int v0, v0

    .line 1971
    iget-object v1, p0, Lcom/android/ex/chips/G;->b:Landroid/widget/ListPopupWindow;

    iget v2, p0, Lcom/android/ex/chips/G;->c:I

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    .line 1972
    iget-object v1, p0, Lcom/android/ex/chips/G;->b:Landroid/widget/ListPopupWindow;

    iget-object v2, p0, Lcom/android/ex/chips/G;->d:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 1973
    iget-object v1, p0, Lcom/android/ex/chips/G;->b:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1, v0}, Landroid/widget/ListPopupWindow;->setVerticalOffset(I)V

    .line 1974
    iget-object v0, p0, Lcom/android/ex/chips/G;->b:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1975
    iget-object v0, p0, Lcom/android/ex/chips/G;->b:Landroid/widget/ListPopupWindow;

    iget-object v1, p0, Lcom/android/ex/chips/G;->d:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-static {v1}, Lcom/android/ex/chips/RecipientEditTextView;->i(Lcom/android/ex/chips/RecipientEditTextView;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1977
    iget-object v0, p0, Lcom/android/ex/chips/G;->d:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-static {v0, v3}, Lcom/android/ex/chips/RecipientEditTextView;->a(Lcom/android/ex/chips/RecipientEditTextView;I)I

    .line 1978
    iget-object v0, p0, Lcom/android/ex/chips/G;->b:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->show()V

    .line 1979
    iget-object v0, p0, Lcom/android/ex/chips/G;->b:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 1980
    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 1985
    iget-object v1, p0, Lcom/android/ex/chips/G;->d:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-static {v1}, Lcom/android/ex/chips/RecipientEditTextView;->j(Lcom/android/ex/chips/RecipientEditTextView;)I

    move-result v1

    if-eq v1, v3, :cond_0

    .line 1986
    iget-object v1, p0, Lcom/android/ex/chips/G;->d:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-static {v1}, Lcom/android/ex/chips/RecipientEditTextView;->j(Lcom/android/ex/chips/RecipientEditTextView;)I

    move-result v1

    invoke-virtual {v0, v1, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 1987
    iget-object v0, p0, Lcom/android/ex/chips/G;->d:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-static {v0, v3}, Lcom/android/ex/chips/RecipientEditTextView;->a(Lcom/android/ex/chips/RecipientEditTextView;I)I

    .line 1989
    :cond_0
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1958
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/ex/chips/G;->a([Ljava/lang/Void;)Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1958
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/android/ex/chips/G;->a(Landroid/widget/ListAdapter;)V

    return-void
.end method
