.class public Lcom/centrify/auth/aidl/AuthUserInformation;
.super Lcom/centrify/auth/aidl/ServiceResponseBase;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/centrify/auth/aidl/AuthUserInformation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:Ljava/lang/String;

.field private d:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    new-instance v0, Lcom/centrify/auth/aidl/a;

    invoke-direct {v0}, Lcom/centrify/auth/aidl/a;-><init>()V

    .line 7
    sput-object v0, Lcom/centrify/auth/aidl/AuthUserInformation;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 6
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/centrify/auth/aidl/ServiceResponseBase;-><init>()V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/centrify/auth/aidl/ServiceResponseBase;-><init>()V

    .line 24
    invoke-virtual {p0, p1}, Lcom/centrify/auth/aidl/AuthUserInformation;->a(Landroid/os/Parcel;)V

    .line 25
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/centrify/auth/aidl/AuthUserInformation;->c:Ljava/lang/String;

    return-object v0
.end method

.method protected final a(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 28
    invoke-super {p0, p1}, Lcom/centrify/auth/aidl/ServiceResponseBase;->a(Landroid/os/Parcel;)V

    .line 29
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/centrify/auth/aidl/AuthUserInformation;->c:Ljava/lang/String;

    .line 30
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/centrify/auth/aidl/AuthUserInformation;->d:J

    .line 31
    return-void
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/centrify/auth/aidl/AuthUserInformation;->d:J

    return-wide v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/centrify/auth/aidl/AuthUserInformation;->a:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 38
    invoke-super {p0, p1, p2}, Lcom/centrify/auth/aidl/ServiceResponseBase;->writeToParcel(Landroid/os/Parcel;I)V

    .line 39
    iget-object v0, p0, Lcom/centrify/auth/aidl/AuthUserInformation;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 40
    iget-wide v0, p0, Lcom/centrify/auth/aidl/AuthUserInformation;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 41
    return-void
.end method
