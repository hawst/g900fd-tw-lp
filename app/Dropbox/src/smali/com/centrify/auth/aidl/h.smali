.class final Lcom/centrify/auth/aidl/h;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/centrify/auth/aidl/UserLookupResult;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Parcel;)Lcom/centrify/auth/aidl/UserLookupResult;
    .locals 1

    .prologue
    .line 10
    new-instance v0, Lcom/centrify/auth/aidl/UserLookupResult;

    invoke-direct {v0, p1}, Lcom/centrify/auth/aidl/UserLookupResult;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final a(I)[Lcom/centrify/auth/aidl/UserLookupResult;
    .locals 1

    .prologue
    .line 14
    new-array v0, p1, [Lcom/centrify/auth/aidl/UserLookupResult;

    return-object v0
.end method

.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/centrify/auth/aidl/h;->a(Landroid/os/Parcel;)Lcom/centrify/auth/aidl/UserLookupResult;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/centrify/auth/aidl/h;->a(I)[Lcom/centrify/auth/aidl/UserLookupResult;

    move-result-object v0

    return-object v0
.end method
