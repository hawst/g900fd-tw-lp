.class public Lcom/centrify/auth/aidl/ServiceResponseBase;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/centrify/auth/aidl/ServiceResponseBase;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected a:I

.field protected b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/centrify/auth/aidl/g;

    invoke-direct {v0}, Lcom/centrify/auth/aidl/g;-><init>()V

    .line 20
    sput-object v0, Lcom/centrify/auth/aidl/ServiceResponseBase;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 11
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-virtual {p0, p1}, Lcom/centrify/auth/aidl/ServiceResponseBase;->a(Landroid/os/Parcel;)V

    .line 37
    return-void
.end method


# virtual methods
.method protected a(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/centrify/auth/aidl/ServiceResponseBase;->a:I

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/centrify/auth/aidl/ServiceResponseBase;->b:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/centrify/auth/aidl/ServiceResponseBase;->a:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/centrify/auth/aidl/ServiceResponseBase;->b:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/centrify/auth/aidl/ServiceResponseBase;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 50
    iget-object v0, p0, Lcom/centrify/auth/aidl/ServiceResponseBase;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 51
    return-void
.end method
