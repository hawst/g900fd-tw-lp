.class public abstract Lcom/centrify/auth/aidl/c;
.super Landroid/os/Binder;
.source "panda.py"

# interfaces
.implements Lcom/centrify/auth/aidl/b;


# direct methods
.method public static a(Landroid/os/IBinder;)Lcom/centrify/auth/aidl/b;
    .locals 2

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    .line 28
    :cond_0
    const-string v0, "com.centrify.auth.aidl.IEnterpriseSecurityProvider"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 29
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/centrify/auth/aidl/b;

    if-eqz v1, :cond_1

    .line 30
    check-cast v0, Lcom/centrify/auth/aidl/b;

    goto :goto_0

    .line 32
    :cond_1
    new-instance v0, Lcom/centrify/auth/aidl/d;

    invoke-direct {v0, p0}, Lcom/centrify/auth/aidl/d;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 40
    sparse-switch p1, :sswitch_data_0

    .line 114
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    :goto_0
    return v1

    .line 44
    :sswitch_0
    const-string v0, "com.centrify.auth.aidl.IEnterpriseSecurityProvider"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :sswitch_1
    const-string v0, "com.centrify.auth.aidl.IEnterpriseSecurityProvider"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 50
    invoke-virtual {p0}, Lcom/centrify/auth/aidl/c;->a()Lcom/centrify/auth/aidl/AuthUserInformation;

    move-result-object v0

    .line 51
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 52
    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 54
    invoke-virtual {v0, p3, v1}, Lcom/centrify/auth/aidl/AuthUserInformation;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 57
    :cond_0
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 63
    :sswitch_2
    const-string v0, "com.centrify.auth.aidl.IEnterpriseSecurityProvider"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 65
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v0

    .line 66
    invoke-virtual {p0, v0}, Lcom/centrify/auth/aidl/c;->a([Ljava/lang/String;)Lcom/centrify/auth/aidl/UserLookupResult;

    move-result-object v0

    .line 67
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 68
    if-eqz v0, :cond_1

    .line 69
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    invoke-virtual {v0, p3, v1}, Lcom/centrify/auth/aidl/UserLookupResult;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 73
    :cond_1
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 79
    :sswitch_3
    const-string v0, "com.centrify.auth.aidl.IEnterpriseSecurityProvider"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 83
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 84
    :goto_1
    invoke-virtual {p0, v3, v0}, Lcom/centrify/auth/aidl/c;->a(Ljava/lang/String;Z)Lcom/centrify/auth/aidl/SecurityTokenResponse;

    move-result-object v0

    .line 85
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 86
    if-eqz v0, :cond_3

    .line 87
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 88
    invoke-virtual {v0, p3, v1}, Lcom/centrify/auth/aidl/SecurityTokenResponse;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 83
    goto :goto_1

    .line 91
    :cond_3
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 97
    :sswitch_4
    const-string v0, "com.centrify.auth.aidl.IEnterpriseSecurityProvider"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 99
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 101
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 102
    :goto_2
    invoke-virtual {p0, v3, v0}, Lcom/centrify/auth/aidl/c;->b(Ljava/lang/String;Z)Lcom/centrify/auth/aidl/SecurityTokenDetailsResponse;

    move-result-object v0

    .line 103
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 104
    if-eqz v0, :cond_5

    .line 105
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 106
    invoke-virtual {v0, p3, v1}, Lcom/centrify/auth/aidl/SecurityTokenDetailsResponse;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 101
    goto :goto_2

    .line 109
    :cond_5
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
