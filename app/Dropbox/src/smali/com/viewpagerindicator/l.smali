.class public final Lcom/viewpagerindicator/l;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final default_circle_indicator_fill_color:I = 0x7f080008

.field public static final default_circle_indicator_page_color:I = 0x7f080009

.field public static final default_circle_indicator_stroke_color:I = 0x7f08000a

.field public static final default_line_indicator_selected_color:I = 0x7f08000b

.field public static final default_line_indicator_unselected_color:I = 0x7f08000c

.field public static final default_title_indicator_footer_color:I = 0x7f08000d

.field public static final default_title_indicator_selected_color:I = 0x7f08000e

.field public static final default_title_indicator_text_color:I = 0x7f08000f

.field public static final default_underline_indicator_selected_color:I = 0x7f080010

.field public static final vpi__background_holo_dark:I = 0x7f080000

.field public static final vpi__background_holo_light:I = 0x7f080001

.field public static final vpi__bright_foreground_disabled_holo_dark:I = 0x7f080004

.field public static final vpi__bright_foreground_disabled_holo_light:I = 0x7f080005

.field public static final vpi__bright_foreground_holo_dark:I = 0x7f080002

.field public static final vpi__bright_foreground_holo_light:I = 0x7f080003

.field public static final vpi__bright_foreground_inverse_holo_dark:I = 0x7f080006

.field public static final vpi__bright_foreground_inverse_holo_light:I = 0x7f080007

.field public static final vpi__dark_theme:I = 0x7f0800a1

.field public static final vpi__light_theme:I = 0x7f0800a2
