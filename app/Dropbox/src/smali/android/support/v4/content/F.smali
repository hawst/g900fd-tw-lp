.class public Landroid/support/v4/content/F;
.super Landroid/support/v4/content/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/content/a",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field protected final f:Landroid/support/v4/content/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/p",
            "<",
            "Landroid/database/Cursor;",
            ">.android/support/v4/content/q;"
        }
    .end annotation
.end field

.field protected g:Landroid/net/Uri;

.field protected h:[Ljava/lang/String;

.field protected i:Ljava/lang/String;

.field protected j:[Ljava/lang/String;

.field protected k:Ljava/lang/String;

.field private l:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0, p1}, Landroid/support/v4/content/a;-><init>(Landroid/content/Context;)V

    .line 110
    new-instance v0, Landroid/support/v4/content/q;

    invoke-direct {v0, p0}, Landroid/support/v4/content/q;-><init>(Landroid/support/v4/content/p;)V

    iput-object v0, p0, Landroid/support/v4/content/F;->f:Landroid/support/v4/content/q;

    .line 111
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0, p1}, Landroid/support/v4/content/a;-><init>(Landroid/content/Context;)V

    .line 122
    new-instance v0, Landroid/support/v4/content/q;

    invoke-direct {v0, p0}, Landroid/support/v4/content/q;-><init>(Landroid/support/v4/content/p;)V

    iput-object v0, p0, Landroid/support/v4/content/F;->f:Landroid/support/v4/content/q;

    .line 123
    iput-object p2, p0, Landroid/support/v4/content/F;->g:Landroid/net/Uri;

    .line 124
    iput-object p3, p0, Landroid/support/v4/content/F;->h:[Ljava/lang/String;

    .line 125
    iput-object p4, p0, Landroid/support/v4/content/F;->i:Ljava/lang/String;

    .line 126
    iput-object p5, p0, Landroid/support/v4/content/F;->j:[Ljava/lang/String;

    .line 127
    iput-object p6, p0, Landroid/support/v4/content/F;->k:Ljava/lang/String;

    .line 128
    return-void
.end method


# virtual methods
.method protected final a(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Landroid/support/v4/content/F;->f:Landroid/support/v4/content/q;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 79
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 49
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Landroid/support/v4/content/F;->c(Landroid/database/Cursor;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 244
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v4/content/a;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 245
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mUri="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/content/F;->g:Landroid/net/Uri;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 246
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mProjection="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 247
    iget-object v0, p0, Landroid/support/v4/content/F;->h:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 248
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mSelection="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/content/F;->i:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 249
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mSelectionArgs="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 250
    iget-object v0, p0, Landroid/support/v4/content/F;->j:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 251
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mSortOrder="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/content/F;->k:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 252
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mCursor="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/content/F;->l:Landroid/database/Cursor;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 256
    return-void
.end method

.method public final b(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 84
    invoke-virtual {p0}, Landroid/support/v4/content/F;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    if-eqz p1, :cond_0

    .line 87
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    iget-object v0, p0, Landroid/support/v4/content/F;->l:Landroid/database/Cursor;

    .line 92
    iput-object p1, p0, Landroid/support/v4/content/F;->l:Landroid/database/Cursor;

    .line 94
    invoke-virtual {p0}, Landroid/support/v4/content/F;->l()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 95
    invoke-super {p0, p1}, Landroid/support/v4/content/a;->b(Ljava/lang/Object;)V

    .line 98
    :cond_2
    if-eqz v0, :cond_0

    if-eq v0, p1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 99
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 49
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Landroid/support/v4/content/F;->b(Landroid/database/Cursor;)V

    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 171
    invoke-super {p0}, Landroid/support/v4/content/a;->b()Z

    move-result v0

    .line 172
    if-eqz v0, :cond_0

    .line 173
    invoke-virtual {p0}, Landroid/support/v4/content/F;->x()V

    .line 175
    :cond_0
    return v0
.end method

.method public final c(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 184
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 187
    :cond_0
    return-void
.end method

.method public synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Landroid/support/v4/content/F;->j()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected final f()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Landroid/support/v4/content/F;->l:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Landroid/support/v4/content/F;->l:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Landroid/support/v4/content/F;->b(Landroid/database/Cursor;)V

    .line 142
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/content/F;->u()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/content/F;->l:Landroid/database/Cursor;

    if-nez v0, :cond_2

    .line 143
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/content/F;->p()V

    .line 145
    :cond_2
    return-void
.end method

.method protected final g()V
    .locals 0

    .prologue
    .line 153
    invoke-virtual {p0}, Landroid/support/v4/content/F;->b()Z

    .line 154
    return-void
.end method

.method protected final h()V
    .locals 1

    .prologue
    .line 191
    invoke-super {p0}, Landroid/support/v4/content/a;->h()V

    .line 194
    invoke-virtual {p0}, Landroid/support/v4/content/F;->g()V

    .line 196
    iget-object v0, p0, Landroid/support/v4/content/F;->l:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/content/F;->l:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 197
    iget-object v0, p0, Landroid/support/v4/content/F;->l:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 199
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/content/F;->l:Landroid/database/Cursor;

    .line 200
    return-void
.end method

.method public j()Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 63
    invoke-virtual {p0}, Landroid/support/v4/content/F;->k()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/content/F;->g:Landroid/net/Uri;

    iget-object v2, p0, Landroid/support/v4/content/F;->h:[Ljava/lang/String;

    iget-object v3, p0, Landroid/support/v4/content/F;->i:Ljava/lang/String;

    iget-object v4, p0, Landroid/support/v4/content/F;->j:[Ljava/lang/String;

    iget-object v5, p0, Landroid/support/v4/content/F;->k:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 65
    if-eqz v0, :cond_0

    .line 67
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 68
    invoke-virtual {p0, v0}, Landroid/support/v4/content/F;->a(Landroid/database/Cursor;)V

    .line 70
    :cond_0
    return-object v0
.end method

.method final x()V
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/content/F;->s:Z

    .line 180
    return-void
.end method
