.class public abstract Landroid/support/v4/content/c;
.super Landroid/support/v4/content/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/support/v4/content/a",
        "<TD;>;"
    }
.end annotation


# instance fields
.field private f:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TD;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/support/v4/content/a;-><init>(Landroid/content/Context;)V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/content/c;->f:Ljava/lang/Object;

    .line 23
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 36
    invoke-virtual {p0}, Landroid/support/v4/content/c;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 39
    :cond_1
    iput-object p1, p0, Landroid/support/v4/content/c;->f:Ljava/lang/Object;

    .line 41
    invoke-virtual {p0}, Landroid/support/v4/content/c;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    invoke-super {p0, p1}, Landroid/support/v4/content/a;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected final f()V
    .locals 1

    .prologue
    .line 53
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 54
    iget-object v0, p0, Landroid/support/v4/content/c;->f:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Landroid/support/v4/content/c;->f:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Landroid/support/v4/content/c;->b(Ljava/lang/Object;)V

    .line 57
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/content/c;->u()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/content/c;->f:Ljava/lang/Object;

    if-nez v0, :cond_2

    .line 58
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/content/c;->p()V

    .line 60
    :cond_2
    return-void
.end method

.method protected final g()V
    .locals 0

    .prologue
    .line 69
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 70
    invoke-super {p0}, Landroid/support/v4/content/a;->g()V

    .line 71
    invoke-virtual {p0}, Landroid/support/v4/content/c;->b()Z

    .line 72
    return-void
.end method

.method protected final h()V
    .locals 1

    .prologue
    .line 81
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 82
    invoke-super {p0}, Landroid/support/v4/content/a;->h()V

    .line 83
    invoke-virtual {p0}, Landroid/support/v4/content/c;->g()V

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/content/c;->f:Ljava/lang/Object;

    .line 85
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/content/c;->s:Z

    .line 96
    invoke-super {p0}, Landroid/support/v4/content/a;->i()V

    .line 97
    return-void
.end method
