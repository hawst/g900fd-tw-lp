.class public abstract Ldbxyzptlk/db231222/ad/e;
.super Ldbxyzptlk/db231222/ad/a;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/ac/z;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x61eb0a2d15dL


# instance fields
.field private volatile a:J

.field private volatile b:Ldbxyzptlk/db231222/ac/a;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 61
    invoke-static {}, Ldbxyzptlk/db231222/ac/f;->a()J

    move-result-wide v0

    invoke-static {}, Ldbxyzptlk/db231222/ae/z;->O()Ldbxyzptlk/db231222/ae/z;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Ldbxyzptlk/db231222/ad/e;-><init>(JLdbxyzptlk/db231222/ac/a;)V

    .line 62
    return-void
.end method

.method public constructor <init>(IIIIIIILdbxyzptlk/db231222/ac/a;)V
    .locals 9

    .prologue
    .line 252
    invoke-direct {p0}, Ldbxyzptlk/db231222/ad/a;-><init>()V

    .line 253
    move-object/from16 v0, p8

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/ad/e;->b(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/a;

    move-result-object v1

    iput-object v1, p0, Ldbxyzptlk/db231222/ad/e;->b:Ldbxyzptlk/db231222/ac/a;

    .line 254
    iget-object v1, p0, Ldbxyzptlk/db231222/ad/e;->b:Ldbxyzptlk/db231222/ac/a;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    move/from16 v8, p7

    invoke-virtual/range {v1 .. v8}, Ldbxyzptlk/db231222/ac/a;->a(IIIIIII)J

    move-result-wide v1

    .line 256
    iget-object v3, p0, Ldbxyzptlk/db231222/ad/e;->b:Ldbxyzptlk/db231222/ac/a;

    invoke-virtual {p0, v1, v2, v3}, Ldbxyzptlk/db231222/ad/e;->a(JLdbxyzptlk/db231222/ac/a;)J

    move-result-wide v1

    iput-wide v1, p0, Ldbxyzptlk/db231222/ad/e;->a:J

    .line 257
    return-void
.end method

.method public constructor <init>(JLdbxyzptlk/db231222/ac/a;)V
    .locals 2

    .prologue
    .line 124
    invoke-direct {p0}, Ldbxyzptlk/db231222/ad/a;-><init>()V

    .line 125
    invoke-virtual {p0, p3}, Ldbxyzptlk/db231222/ad/e;->b(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/ad/e;->b:Ldbxyzptlk/db231222/ac/a;

    .line 126
    iget-object v0, p0, Ldbxyzptlk/db231222/ad/e;->b:Ldbxyzptlk/db231222/ac/a;

    invoke-virtual {p0, p1, p2, v0}, Ldbxyzptlk/db231222/ad/e;->a(JLdbxyzptlk/db231222/ac/a;)J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/ad/e;->a:J

    .line 127
    return-void
.end method

.method public constructor <init>(JLdbxyzptlk/db231222/ac/i;)V
    .locals 1

    .prologue
    .line 110
    invoke-static {p3}, Ldbxyzptlk/db231222/ae/z;->b(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ae/z;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Ldbxyzptlk/db231222/ad/e;-><init>(JLdbxyzptlk/db231222/ac/a;)V

    .line 111
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Ldbxyzptlk/db231222/ac/a;)V
    .locals 3

    .prologue
    .line 168
    invoke-direct {p0}, Ldbxyzptlk/db231222/ad/a;-><init>()V

    .line 169
    invoke-static {}, Ldbxyzptlk/db231222/af/d;->a()Ldbxyzptlk/db231222/af/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/af/d;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/af/i;

    move-result-object v0

    .line 170
    invoke-interface {v0, p1, p2}, Ldbxyzptlk/db231222/af/i;->b(Ljava/lang/Object;Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/a;

    move-result-object v1

    invoke-virtual {p0, v1}, Ldbxyzptlk/db231222/ad/e;->b(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/a;

    move-result-object v1

    iput-object v1, p0, Ldbxyzptlk/db231222/ad/e;->b:Ldbxyzptlk/db231222/ac/a;

    .line 171
    invoke-interface {v0, p1, p2}, Ldbxyzptlk/db231222/af/i;->a(Ljava/lang/Object;Ldbxyzptlk/db231222/ac/a;)J

    move-result-wide v0

    iget-object v2, p0, Ldbxyzptlk/db231222/ad/e;->b:Ldbxyzptlk/db231222/ac/a;

    invoke-virtual {p0, v0, v1, v2}, Ldbxyzptlk/db231222/ad/e;->a(JLdbxyzptlk/db231222/ac/a;)J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/ad/e;->a:J

    .line 172
    return-void
.end method


# virtual methods
.method protected final a(JLdbxyzptlk/db231222/ac/a;)J
    .locals 0

    .prologue
    .line 284
    return-wide p1
.end method

.method protected a(J)V
    .locals 2

    .prologue
    .line 317
    iget-object v0, p0, Ldbxyzptlk/db231222/ad/e;->b:Ldbxyzptlk/db231222/ac/a;

    invoke-virtual {p0, p1, p2, v0}, Ldbxyzptlk/db231222/ad/e;->a(JLdbxyzptlk/db231222/ac/a;)J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/ad/e;->a:J

    .line 318
    return-void
.end method

.method protected a(Ldbxyzptlk/db231222/ac/a;)V
    .locals 1

    .prologue
    .line 329
    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/ad/e;->b(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/ad/e;->b:Ldbxyzptlk/db231222/ac/a;

    .line 330
    return-void
.end method

.method protected final b(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/a;
    .locals 1

    .prologue
    .line 270
    invoke-static {p1}, Ldbxyzptlk/db231222/ac/f;->a(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 295
    iget-wide v0, p0, Ldbxyzptlk/db231222/ad/e;->a:J

    return-wide v0
.end method

.method public final d()Ldbxyzptlk/db231222/ac/a;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Ldbxyzptlk/db231222/ad/e;->b:Ldbxyzptlk/db231222/ac/a;

    return-object v0
.end method
