.class public abstract Ldbxyzptlk/db231222/ad/g;
.super Ldbxyzptlk/db231222/ad/d;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/ac/E;
.implements Ljava/io/Serializable;


# static fields
.field private static final a:Ldbxyzptlk/db231222/ac/E;

.field private static final serialVersionUID:J = -0x1d4b9cd3d9d73379L


# instance fields
.field private final b:Ldbxyzptlk/db231222/ac/w;

.field private final c:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Ldbxyzptlk/db231222/ad/h;

    invoke-direct {v0}, Ldbxyzptlk/db231222/ad/h;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/ad/g;->a:Ldbxyzptlk/db231222/ac/E;

    return-void
.end method

.method protected constructor <init>(JLdbxyzptlk/db231222/ac/w;Ldbxyzptlk/db231222/ac/a;)V
    .locals 2

    .prologue
    .line 254
    invoke-direct {p0}, Ldbxyzptlk/db231222/ad/d;-><init>()V

    .line 255
    invoke-virtual {p0, p3}, Ldbxyzptlk/db231222/ad/g;->a(Ldbxyzptlk/db231222/ac/w;)Ldbxyzptlk/db231222/ac/w;

    move-result-object v0

    .line 256
    invoke-static {p4}, Ldbxyzptlk/db231222/ac/f;->a(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/a;

    move-result-object v1

    .line 257
    iput-object v0, p0, Ldbxyzptlk/db231222/ad/g;->b:Ldbxyzptlk/db231222/ac/w;

    .line 258
    invoke-virtual {v1, p0, p1, p2}, Ldbxyzptlk/db231222/ac/a;->a(Ldbxyzptlk/db231222/ac/E;J)[I

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/ad/g;->c:[I

    .line 259
    return-void
.end method

.method protected constructor <init>(Ldbxyzptlk/db231222/ac/B;Ldbxyzptlk/db231222/ac/B;Ldbxyzptlk/db231222/ac/w;)V
    .locals 6

    .prologue
    .line 122
    invoke-direct {p0}, Ldbxyzptlk/db231222/ad/d;-><init>()V

    .line 123
    invoke-virtual {p0, p3}, Ldbxyzptlk/db231222/ad/g;->a(Ldbxyzptlk/db231222/ac/w;)Ldbxyzptlk/db231222/ac/w;

    move-result-object v1

    .line 124
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 125
    iput-object v1, p0, Ldbxyzptlk/db231222/ad/g;->b:Ldbxyzptlk/db231222/ac/w;

    .line 126
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ad/g;->h()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Ldbxyzptlk/db231222/ad/g;->c:[I

    .line 134
    :goto_0
    return-void

    .line 128
    :cond_0
    invoke-static {p1}, Ldbxyzptlk/db231222/ac/f;->a(Ldbxyzptlk/db231222/ac/B;)J

    move-result-wide v2

    .line 129
    invoke-static {p2}, Ldbxyzptlk/db231222/ac/f;->a(Ldbxyzptlk/db231222/ac/B;)J

    move-result-wide v4

    .line 130
    invoke-static {p1, p2}, Ldbxyzptlk/db231222/ac/f;->a(Ldbxyzptlk/db231222/ac/B;Ldbxyzptlk/db231222/ac/B;)Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    .line 131
    iput-object v1, p0, Ldbxyzptlk/db231222/ad/g;->b:Ldbxyzptlk/db231222/ac/w;

    move-object v1, p0

    .line 132
    invoke-virtual/range {v0 .. v5}, Ldbxyzptlk/db231222/ac/a;->a(Ldbxyzptlk/db231222/ac/E;JJ)[I

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/ad/g;->c:[I

    goto :goto_0
.end method

.method protected constructor <init>(Ldbxyzptlk/db231222/ac/D;Ldbxyzptlk/db231222/ac/D;Ldbxyzptlk/db231222/ac/w;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 155
    invoke-direct {p0}, Ldbxyzptlk/db231222/ad/d;-><init>()V

    .line 156
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 157
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ReadablePartial objects must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_1
    instance-of v0, p1, Ldbxyzptlk/db231222/ad/f;

    if-eqz v0, :cond_2

    instance-of v0, p2, Ldbxyzptlk/db231222/ad/f;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_2

    .line 161
    invoke-virtual {p0, p3}, Ldbxyzptlk/db231222/ad/g;->a(Ldbxyzptlk/db231222/ac/w;)Ldbxyzptlk/db231222/ac/w;

    move-result-object v1

    move-object v0, p1

    .line 162
    check-cast v0, Ldbxyzptlk/db231222/ad/f;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ad/f;->b()J

    move-result-wide v2

    .line 163
    check-cast p2, Ldbxyzptlk/db231222/ad/f;

    invoke-virtual {p2}, Ldbxyzptlk/db231222/ad/f;->b()J

    move-result-wide v4

    .line 164
    invoke-interface {p1}, Ldbxyzptlk/db231222/ac/D;->c()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    .line 165
    invoke-static {v0}, Ldbxyzptlk/db231222/ac/f;->a(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    .line 166
    iput-object v1, p0, Ldbxyzptlk/db231222/ad/g;->b:Ldbxyzptlk/db231222/ac/w;

    move-object v1, p0

    .line 167
    invoke-virtual/range {v0 .. v5}, Ldbxyzptlk/db231222/ac/a;->a(Ldbxyzptlk/db231222/ac/E;JJ)[I

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/ad/g;->c:[I

    .line 184
    :goto_0
    return-void

    .line 169
    :cond_2
    invoke-interface {p1}, Ldbxyzptlk/db231222/ac/D;->a()I

    move-result v0

    invoke-interface {p2}, Ldbxyzptlk/db231222/ac/D;->a()I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 170
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ReadablePartial objects must have the same set of fields"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172
    :cond_3
    const/4 v0, 0x0

    invoke-interface {p1}, Ldbxyzptlk/db231222/ac/D;->a()I

    move-result v1

    :goto_1
    if-ge v0, v1, :cond_5

    .line 173
    invoke-interface {p1, v0}, Ldbxyzptlk/db231222/ac/D;->b(I)Ldbxyzptlk/db231222/ac/d;

    move-result-object v2

    invoke-interface {p2, v0}, Ldbxyzptlk/db231222/ac/D;->b(I)Ldbxyzptlk/db231222/ac/d;

    move-result-object v3

    if-eq v2, v3, :cond_4

    .line 174
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ReadablePartial objects must have the same set of fields"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 177
    :cond_5
    invoke-static {p1}, Ldbxyzptlk/db231222/ac/f;->a(Ldbxyzptlk/db231222/ac/D;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 178
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ReadablePartial objects must be contiguous"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 180
    :cond_6
    invoke-virtual {p0, p3}, Ldbxyzptlk/db231222/ad/g;->a(Ldbxyzptlk/db231222/ac/w;)Ldbxyzptlk/db231222/ac/w;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/ad/g;->b:Ldbxyzptlk/db231222/ac/w;

    .line 181
    invoke-interface {p1}, Ldbxyzptlk/db231222/ac/D;->c()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/ac/f;->a(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/a;->b()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    .line 182
    invoke-virtual {v0, p1, v4, v5}, Ldbxyzptlk/db231222/ac/a;->a(Ldbxyzptlk/db231222/ac/D;J)J

    move-result-wide v2

    invoke-virtual {v0, p2, v4, v5}, Ldbxyzptlk/db231222/ac/a;->a(Ldbxyzptlk/db231222/ac/D;J)J

    move-result-wide v4

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Ldbxyzptlk/db231222/ac/a;->a(Ldbxyzptlk/db231222/ac/E;JJ)[I

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/ad/g;->c:[I

    goto :goto_0
.end method


# virtual methods
.method protected final a(Ldbxyzptlk/db231222/ac/w;)Ldbxyzptlk/db231222/ac/w;
    .locals 1

    .prologue
    .line 308
    invoke-static {p1}, Ldbxyzptlk/db231222/ac/f;->a(Ldbxyzptlk/db231222/ac/w;)Ldbxyzptlk/db231222/ac/w;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ldbxyzptlk/db231222/ac/w;
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Ldbxyzptlk/db231222/ad/g;->b:Ldbxyzptlk/db231222/ac/w;

    return-object v0
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Ldbxyzptlk/db231222/ad/g;->c:[I

    aget v0, v0, p1

    return v0
.end method
