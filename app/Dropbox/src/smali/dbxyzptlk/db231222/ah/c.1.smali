.class public final Ldbxyzptlk/db231222/ah/c;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Ldbxyzptlk/db231222/ah/w;

.field private final b:Ldbxyzptlk/db231222/ah/s;

.field private final c:Ljava/util/Locale;

.field private final d:Z

.field private final e:Ldbxyzptlk/db231222/ac/a;

.field private final f:Ldbxyzptlk/db231222/ac/i;

.field private final g:Ljava/lang/Integer;

.field private final h:I


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/ah/w;Ldbxyzptlk/db231222/ah/s;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput-object p1, p0, Ldbxyzptlk/db231222/ah/c;->a:Ldbxyzptlk/db231222/ah/w;

    .line 111
    iput-object p2, p0, Ldbxyzptlk/db231222/ah/c;->b:Ldbxyzptlk/db231222/ah/s;

    .line 112
    iput-object v1, p0, Ldbxyzptlk/db231222/ah/c;->c:Ljava/util/Locale;

    .line 113
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/ah/c;->d:Z

    .line 114
    iput-object v1, p0, Ldbxyzptlk/db231222/ah/c;->e:Ldbxyzptlk/db231222/ac/a;

    .line 115
    iput-object v1, p0, Ldbxyzptlk/db231222/ah/c;->f:Ldbxyzptlk/db231222/ac/i;

    .line 116
    iput-object v1, p0, Ldbxyzptlk/db231222/ah/c;->g:Ljava/lang/Integer;

    .line 117
    const/16 v0, 0x7d0

    iput v0, p0, Ldbxyzptlk/db231222/ah/c;->h:I

    .line 118
    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/ah/w;Ldbxyzptlk/db231222/ah/s;Ljava/util/Locale;ZLdbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/i;Ljava/lang/Integer;I)V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput-object p1, p0, Ldbxyzptlk/db231222/ah/c;->a:Ldbxyzptlk/db231222/ah/w;

    .line 130
    iput-object p2, p0, Ldbxyzptlk/db231222/ah/c;->b:Ldbxyzptlk/db231222/ah/s;

    .line 131
    iput-object p3, p0, Ldbxyzptlk/db231222/ah/c;->c:Ljava/util/Locale;

    .line 132
    iput-boolean p4, p0, Ldbxyzptlk/db231222/ah/c;->d:Z

    .line 133
    iput-object p5, p0, Ldbxyzptlk/db231222/ah/c;->e:Ldbxyzptlk/db231222/ac/a;

    .line 134
    iput-object p6, p0, Ldbxyzptlk/db231222/ah/c;->f:Ldbxyzptlk/db231222/ac/i;

    .line 135
    iput-object p7, p0, Ldbxyzptlk/db231222/ah/c;->g:Ljava/lang/Integer;

    .line 136
    iput p8, p0, Ldbxyzptlk/db231222/ah/c;->h:I

    .line 137
    return-void
.end method

.method private a(Ljava/lang/StringBuffer;JLdbxyzptlk/db231222/ac/a;)V
    .locals 11

    .prologue
    const-wide/16 v9, 0x0

    .line 619
    invoke-direct {p0}, Ldbxyzptlk/db231222/ah/c;->d()Ldbxyzptlk/db231222/ah/w;

    move-result-object v0

    .line 620
    invoke-direct {p0, p4}, Ldbxyzptlk/db231222/ah/c;->b(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/a;

    move-result-object v1

    .line 623
    invoke-virtual {v1}, Ldbxyzptlk/db231222/ac/a;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v6

    .line 624
    invoke-virtual {v6, p2, p3}, Ldbxyzptlk/db231222/ac/i;->b(J)I

    move-result v5

    .line 625
    int-to-long v2, v5

    add-long/2addr v2, p2

    .line 626
    xor-long v7, p2, v2

    cmp-long v4, v7, v9

    if-gez v4, :cond_0

    int-to-long v7, v5

    xor-long/2addr v7, p2

    cmp-long v4, v7, v9

    if-ltz v4, :cond_0

    .line 628
    sget-object v6, Ldbxyzptlk/db231222/ac/i;->a:Ldbxyzptlk/db231222/ac/i;

    .line 629
    const/4 v5, 0x0

    move-wide v2, p2

    .line 632
    :cond_0
    invoke-virtual {v1}, Ldbxyzptlk/db231222/ac/a;->b()Ldbxyzptlk/db231222/ac/a;

    move-result-object v4

    iget-object v7, p0, Ldbxyzptlk/db231222/ah/c;->c:Ljava/util/Locale;

    move-object v1, p1

    invoke-interface/range {v0 .. v7}, Ldbxyzptlk/db231222/ah/w;->a(Ljava/lang/StringBuffer;JLdbxyzptlk/db231222/ac/a;ILdbxyzptlk/db231222/ac/i;Ljava/util/Locale;)V

    .line 633
    return-void
.end method

.method private b(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/a;
    .locals 2

    .prologue
    .line 942
    invoke-static {p1}, Ldbxyzptlk/db231222/ac/f;->a(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    .line 943
    iget-object v1, p0, Ldbxyzptlk/db231222/ah/c;->e:Ldbxyzptlk/db231222/ac/a;

    if-eqz v1, :cond_0

    .line 944
    iget-object v0, p0, Ldbxyzptlk/db231222/ah/c;->e:Ldbxyzptlk/db231222/ac/a;

    .line 946
    :cond_0
    iget-object v1, p0, Ldbxyzptlk/db231222/ah/c;->f:Ldbxyzptlk/db231222/ac/i;

    if-eqz v1, :cond_1

    .line 947
    iget-object v1, p0, Ldbxyzptlk/db231222/ah/c;->f:Ldbxyzptlk/db231222/ac/i;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/ac/a;->a(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    .line 949
    :cond_1
    return-object v0
.end method

.method private d()Ldbxyzptlk/db231222/ah/w;
    .locals 2

    .prologue
    .line 658
    iget-object v0, p0, Ldbxyzptlk/db231222/ah/c;->a:Ldbxyzptlk/db231222/ah/w;

    .line 659
    if-nez v0, :cond_0

    .line 660
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Printing not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 662
    :cond_0
    return-object v0
.end method

.method private e()Ldbxyzptlk/db231222/ah/s;
    .locals 2

    .prologue
    .line 927
    iget-object v0, p0, Ldbxyzptlk/db231222/ah/c;->b:Ldbxyzptlk/db231222/ah/s;

    .line 928
    if-nez v0, :cond_0

    .line 929
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Parsing not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 931
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)J
    .locals 8

    .prologue
    .line 742
    invoke-direct {p0}, Ldbxyzptlk/db231222/ah/c;->e()Ldbxyzptlk/db231222/ah/s;

    move-result-object v7

    .line 744
    iget-object v0, p0, Ldbxyzptlk/db231222/ah/c;->e:Ldbxyzptlk/db231222/ac/a;

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/ah/c;->b(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/a;

    move-result-object v3

    .line 745
    new-instance v0, Ldbxyzptlk/db231222/ah/t;

    const-wide/16 v1, 0x0

    iget-object v4, p0, Ldbxyzptlk/db231222/ah/c;->c:Ljava/util/Locale;

    iget-object v5, p0, Ldbxyzptlk/db231222/ah/c;->g:Ljava/lang/Integer;

    iget v6, p0, Ldbxyzptlk/db231222/ah/c;->h:I

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/ah/t;-><init>(JLdbxyzptlk/db231222/ac/a;Ljava/util/Locale;Ljava/lang/Integer;I)V

    .line 746
    const/4 v1, 0x0

    invoke-interface {v7, v0, p1, v1}, Ldbxyzptlk/db231222/ah/s;->a(Ldbxyzptlk/db231222/ah/t;Ljava/lang/String;I)I

    move-result v1

    .line 747
    if-ltz v1, :cond_0

    .line 748
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 749
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ldbxyzptlk/db231222/ah/t;->a(ZLjava/lang/String;)J

    move-result-wide v0

    return-wide v0

    .line 752
    :cond_0
    xor-int/lit8 v0, v1, -0x1

    .line 754
    :goto_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static {p1, v0}, Ldbxyzptlk/db231222/ah/x;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final a(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ah/c;
    .locals 9

    .prologue
    .line 257
    iget-object v0, p0, Ldbxyzptlk/db231222/ah/c;->e:Ldbxyzptlk/db231222/ac/a;

    if-ne v0, p1, :cond_0

    .line 260
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Ldbxyzptlk/db231222/ah/c;

    iget-object v1, p0, Ldbxyzptlk/db231222/ah/c;->a:Ldbxyzptlk/db231222/ah/w;

    iget-object v2, p0, Ldbxyzptlk/db231222/ah/c;->b:Ldbxyzptlk/db231222/ah/s;

    iget-object v3, p0, Ldbxyzptlk/db231222/ah/c;->c:Ljava/util/Locale;

    iget-boolean v4, p0, Ldbxyzptlk/db231222/ah/c;->d:Z

    iget-object v6, p0, Ldbxyzptlk/db231222/ah/c;->f:Ldbxyzptlk/db231222/ac/i;

    iget-object v7, p0, Ldbxyzptlk/db231222/ah/c;->g:Ljava/lang/Integer;

    iget v8, p0, Ldbxyzptlk/db231222/ah/c;->h:I

    move-object v5, p1

    invoke-direct/range {v0 .. v8}, Ldbxyzptlk/db231222/ah/c;-><init>(Ldbxyzptlk/db231222/ah/w;Ldbxyzptlk/db231222/ah/s;Ljava/util/Locale;ZLdbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/i;Ljava/lang/Integer;I)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final a(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ah/c;
    .locals 9

    .prologue
    .line 321
    iget-object v0, p0, Ldbxyzptlk/db231222/ah/c;->f:Ldbxyzptlk/db231222/ac/i;

    if-ne v0, p1, :cond_0

    .line 324
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Ldbxyzptlk/db231222/ah/c;

    iget-object v1, p0, Ldbxyzptlk/db231222/ah/c;->a:Ldbxyzptlk/db231222/ah/w;

    iget-object v2, p0, Ldbxyzptlk/db231222/ah/c;->b:Ldbxyzptlk/db231222/ah/s;

    iget-object v3, p0, Ldbxyzptlk/db231222/ah/c;->c:Ljava/util/Locale;

    const/4 v4, 0x0

    iget-object v5, p0, Ldbxyzptlk/db231222/ah/c;->e:Ldbxyzptlk/db231222/ac/a;

    iget-object v7, p0, Ldbxyzptlk/db231222/ah/c;->g:Ljava/lang/Integer;

    iget v8, p0, Ldbxyzptlk/db231222/ah/c;->h:I

    move-object v6, p1

    invoke-direct/range {v0 .. v8}, Ldbxyzptlk/db231222/ah/c;-><init>(Ldbxyzptlk/db231222/ah/w;Ldbxyzptlk/db231222/ah/s;Ljava/util/Locale;ZLdbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/i;Ljava/lang/Integer;I)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Ldbxyzptlk/db231222/ah/w;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Ldbxyzptlk/db231222/ah/c;->a:Ldbxyzptlk/db231222/ah/w;

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/ac/B;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 583
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {p0}, Ldbxyzptlk/db231222/ah/c;->d()Ldbxyzptlk/db231222/ah/w;

    move-result-object v1

    invoke-interface {v1}, Ldbxyzptlk/db231222/ah/w;->a()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 584
    invoke-virtual {p0, v0, p1}, Ldbxyzptlk/db231222/ah/c;->a(Ljava/lang/StringBuffer;Ldbxyzptlk/db231222/ac/B;)V

    .line 585
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/ac/D;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 613
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {p0}, Ldbxyzptlk/db231222/ah/c;->d()Ldbxyzptlk/db231222/ah/w;

    move-result-object v1

    invoke-interface {v1}, Ldbxyzptlk/db231222/ah/w;->a()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 614
    invoke-virtual {p0, v0, p1}, Ldbxyzptlk/db231222/ah/c;->a(Ljava/lang/StringBuffer;Ldbxyzptlk/db231222/ac/D;)V

    .line 615
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/StringBuffer;J)V
    .locals 1

    .prologue
    .line 497
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Ldbxyzptlk/db231222/ah/c;->a(Ljava/lang/StringBuffer;JLdbxyzptlk/db231222/ac/a;)V

    .line 498
    return-void
.end method

.method public final a(Ljava/lang/StringBuffer;Ldbxyzptlk/db231222/ac/B;)V
    .locals 3

    .prologue
    .line 460
    invoke-static {p2}, Ldbxyzptlk/db231222/ac/f;->a(Ldbxyzptlk/db231222/ac/B;)J

    move-result-wide v0

    .line 461
    invoke-static {p2}, Ldbxyzptlk/db231222/ac/f;->b(Ldbxyzptlk/db231222/ac/B;)Ldbxyzptlk/db231222/ac/a;

    move-result-object v2

    .line 462
    invoke-direct {p0, p1, v0, v1, v2}, Ldbxyzptlk/db231222/ah/c;->a(Ljava/lang/StringBuffer;JLdbxyzptlk/db231222/ac/a;)V

    .line 463
    return-void
.end method

.method public final a(Ljava/lang/StringBuffer;Ldbxyzptlk/db231222/ac/D;)V
    .locals 2

    .prologue
    .line 534
    invoke-direct {p0}, Ldbxyzptlk/db231222/ah/c;->d()Ldbxyzptlk/db231222/ah/w;

    move-result-object v0

    .line 535
    if-nez p2, :cond_0

    .line 536
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The partial must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 538
    :cond_0
    iget-object v1, p0, Ldbxyzptlk/db231222/ah/c;->c:Ljava/util/Locale;

    invoke-interface {v0, p1, p2, v1}, Ldbxyzptlk/db231222/ah/w;->a(Ljava/lang/StringBuffer;Ldbxyzptlk/db231222/ac/D;Ljava/util/Locale;)V

    .line 539
    return-void
.end method

.method public final b()Ldbxyzptlk/db231222/ah/s;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Ldbxyzptlk/db231222/ah/c;->b:Ldbxyzptlk/db231222/ah/s;

    return-object v0
.end method

.method public final c()Ldbxyzptlk/db231222/ah/c;
    .locals 1

    .prologue
    .line 301
    sget-object v0, Ldbxyzptlk/db231222/ac/i;->a:Ldbxyzptlk/db231222/ac/i;

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/ah/c;->a(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ah/c;

    move-result-object v0

    return-object v0
.end method
