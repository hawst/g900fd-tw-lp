.class public final Ldbxyzptlk/db231222/ah/B;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Ldbxyzptlk/db231222/ah/L;

.field private final b:Ldbxyzptlk/db231222/ah/K;

.field private final c:Ljava/util/Locale;

.field private final d:Ldbxyzptlk/db231222/ac/w;


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/ah/L;Ldbxyzptlk/db231222/ah/K;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object p1, p0, Ldbxyzptlk/db231222/ah/B;->a:Ldbxyzptlk/db231222/ah/L;

    .line 89
    iput-object p2, p0, Ldbxyzptlk/db231222/ah/B;->b:Ldbxyzptlk/db231222/ah/K;

    .line 90
    iput-object v0, p0, Ldbxyzptlk/db231222/ah/B;->c:Ljava/util/Locale;

    .line 91
    iput-object v0, p0, Ldbxyzptlk/db231222/ah/B;->d:Ldbxyzptlk/db231222/ac/w;

    .line 92
    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/ah/L;Ldbxyzptlk/db231222/ah/K;Ljava/util/Locale;Ldbxyzptlk/db231222/ac/w;)V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    iput-object p1, p0, Ldbxyzptlk/db231222/ah/B;->a:Ldbxyzptlk/db231222/ah/L;

    .line 107
    iput-object p2, p0, Ldbxyzptlk/db231222/ah/B;->b:Ldbxyzptlk/db231222/ah/K;

    .line 108
    iput-object p3, p0, Ldbxyzptlk/db231222/ah/B;->c:Ljava/util/Locale;

    .line 109
    iput-object p4, p0, Ldbxyzptlk/db231222/ah/B;->d:Ldbxyzptlk/db231222/ac/w;

    .line 110
    return-void
.end method

.method private b(Ldbxyzptlk/db231222/ac/E;)V
    .locals 2

    .prologue
    .line 262
    if-nez p1, :cond_0

    .line 263
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Period must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 265
    :cond_0
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Ldbxyzptlk/db231222/ah/B;->a:Ldbxyzptlk/db231222/ah/L;

    if-nez v0, :cond_0

    .line 252
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Printing not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 254
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/ac/w;)Ldbxyzptlk/db231222/ah/B;
    .locals 4

    .prologue
    .line 187
    iget-object v0, p0, Ldbxyzptlk/db231222/ah/B;->d:Ldbxyzptlk/db231222/ac/w;

    if-ne p1, v0, :cond_0

    .line 190
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Ldbxyzptlk/db231222/ah/B;

    iget-object v1, p0, Ldbxyzptlk/db231222/ah/B;->a:Ldbxyzptlk/db231222/ah/L;

    iget-object v2, p0, Ldbxyzptlk/db231222/ah/B;->b:Ldbxyzptlk/db231222/ah/K;

    iget-object v3, p0, Ldbxyzptlk/db231222/ah/B;->c:Ljava/util/Locale;

    invoke-direct {v0, v1, v2, v3, p1}, Ldbxyzptlk/db231222/ah/B;-><init>(Ldbxyzptlk/db231222/ah/L;Ldbxyzptlk/db231222/ah/K;Ljava/util/Locale;Ldbxyzptlk/db231222/ac/w;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Ldbxyzptlk/db231222/ah/L;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Ldbxyzptlk/db231222/ah/B;->a:Ldbxyzptlk/db231222/ah/L;

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/ac/E;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 236
    invoke-direct {p0}, Ldbxyzptlk/db231222/ah/B;->c()V

    .line 237
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/ah/B;->b(Ldbxyzptlk/db231222/ac/E;)V

    .line 239
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ah/B;->a()Ldbxyzptlk/db231222/ah/L;

    move-result-object v0

    .line 240
    new-instance v1, Ljava/lang/StringBuffer;

    iget-object v2, p0, Ldbxyzptlk/db231222/ah/B;->c:Ljava/util/Locale;

    invoke-interface {v0, p1, v2}, Ldbxyzptlk/db231222/ah/L;->a(Ldbxyzptlk/db231222/ac/E;Ljava/util/Locale;)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 241
    iget-object v2, p0, Ldbxyzptlk/db231222/ah/B;->c:Ljava/util/Locale;

    invoke-interface {v0, v1, p1, v2}, Ldbxyzptlk/db231222/ah/L;->a(Ljava/lang/StringBuffer;Ldbxyzptlk/db231222/ac/E;Ljava/util/Locale;)V

    .line 242
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ldbxyzptlk/db231222/ah/K;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Ldbxyzptlk/db231222/ah/B;->b:Ldbxyzptlk/db231222/ah/K;

    return-object v0
.end method
