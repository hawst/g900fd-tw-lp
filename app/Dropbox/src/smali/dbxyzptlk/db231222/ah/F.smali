.class final Ldbxyzptlk/db231222/ah/F;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/ah/K;
.implements Ldbxyzptlk/db231222/ah/L;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:Z

.field private final e:I

.field private final f:[Ldbxyzptlk/db231222/ah/F;

.field private final g:Ldbxyzptlk/db231222/ah/H;

.field private final h:Ldbxyzptlk/db231222/ah/H;


# direct methods
.method constructor <init>(IIIZI[Ldbxyzptlk/db231222/ah/F;Ldbxyzptlk/db231222/ah/H;Ldbxyzptlk/db231222/ah/H;)V
    .locals 0

    .prologue
    .line 1053
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1054
    iput p1, p0, Ldbxyzptlk/db231222/ah/F;->a:I

    .line 1055
    iput p2, p0, Ldbxyzptlk/db231222/ah/F;->b:I

    .line 1056
    iput p3, p0, Ldbxyzptlk/db231222/ah/F;->c:I

    .line 1057
    iput-boolean p4, p0, Ldbxyzptlk/db231222/ah/F;->d:Z

    .line 1058
    iput p5, p0, Ldbxyzptlk/db231222/ah/F;->e:I

    .line 1059
    iput-object p6, p0, Ldbxyzptlk/db231222/ah/F;->f:[Ldbxyzptlk/db231222/ah/F;

    .line 1060
    iput-object p7, p0, Ldbxyzptlk/db231222/ah/F;->g:Ldbxyzptlk/db231222/ah/H;

    .line 1061
    iput-object p8, p0, Ldbxyzptlk/db231222/ah/F;->h:Ldbxyzptlk/db231222/ah/H;

    .line 1062
    return-void
.end method

.method constructor <init>(Ldbxyzptlk/db231222/ah/F;Ldbxyzptlk/db231222/ah/H;)V
    .locals 2

    .prologue
    .line 1064
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1065
    iget v0, p1, Ldbxyzptlk/db231222/ah/F;->a:I

    iput v0, p0, Ldbxyzptlk/db231222/ah/F;->a:I

    .line 1066
    iget v0, p1, Ldbxyzptlk/db231222/ah/F;->b:I

    iput v0, p0, Ldbxyzptlk/db231222/ah/F;->b:I

    .line 1067
    iget v0, p1, Ldbxyzptlk/db231222/ah/F;->c:I

    iput v0, p0, Ldbxyzptlk/db231222/ah/F;->c:I

    .line 1068
    iget-boolean v0, p1, Ldbxyzptlk/db231222/ah/F;->d:Z

    iput-boolean v0, p0, Ldbxyzptlk/db231222/ah/F;->d:Z

    .line 1069
    iget v0, p1, Ldbxyzptlk/db231222/ah/F;->e:I

    iput v0, p0, Ldbxyzptlk/db231222/ah/F;->e:I

    .line 1070
    iget-object v0, p1, Ldbxyzptlk/db231222/ah/F;->f:[Ldbxyzptlk/db231222/ah/F;

    iput-object v0, p0, Ldbxyzptlk/db231222/ah/F;->f:[Ldbxyzptlk/db231222/ah/F;

    .line 1071
    iget-object v0, p1, Ldbxyzptlk/db231222/ah/F;->g:Ldbxyzptlk/db231222/ah/H;

    iput-object v0, p0, Ldbxyzptlk/db231222/ah/F;->g:Ldbxyzptlk/db231222/ah/H;

    .line 1072
    iget-object v0, p1, Ldbxyzptlk/db231222/ah/F;->h:Ldbxyzptlk/db231222/ah/H;

    if-eqz v0, :cond_0

    .line 1073
    new-instance v0, Ldbxyzptlk/db231222/ah/E;

    iget-object v1, p1, Ldbxyzptlk/db231222/ah/F;->h:Ldbxyzptlk/db231222/ah/H;

    invoke-direct {v0, v1, p2}, Ldbxyzptlk/db231222/ah/E;-><init>(Ldbxyzptlk/db231222/ah/H;Ldbxyzptlk/db231222/ah/H;)V

    move-object p2, v0

    .line 1075
    :cond_0
    iput-object p2, p0, Ldbxyzptlk/db231222/ah/F;->h:Ldbxyzptlk/db231222/ah/H;

    .line 1076
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 1535
    iget v0, p0, Ldbxyzptlk/db231222/ah/F;->e:I

    return v0
.end method

.method public final a(Ldbxyzptlk/db231222/ac/E;ILjava/util/Locale;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1079
    if-gtz p2, :cond_1

    .line 1085
    :cond_0
    :goto_0
    return v0

    .line 1082
    :cond_1
    iget v1, p0, Ldbxyzptlk/db231222/ah/F;->b:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_2

    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/ah/F;->a(Ldbxyzptlk/db231222/ac/E;)J

    move-result-wide v1

    const-wide v3, 0x7fffffffffffffffL

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    .line 1083
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Ldbxyzptlk/db231222/ac/E;Ljava/util/Locale;)I
    .locals 9

    .prologue
    const-wide/16 v7, 0x3e8

    const-wide/16 v5, 0x0

    .line 1089
    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/ah/F;->a(Ldbxyzptlk/db231222/ac/E;)J

    move-result-wide v1

    .line 1090
    const-wide v3, 0x7fffffffffffffffL

    cmp-long v0, v1, v3

    if-nez v0, :cond_1

    .line 1091
    const/4 v0, 0x0

    .line 1117
    :cond_0
    :goto_0
    return v0

    .line 1094
    :cond_1
    invoke-static {v1, v2}, Ldbxyzptlk/db231222/ah/x;->a(J)I

    move-result v0

    iget v3, p0, Ldbxyzptlk/db231222/ah/F;->a:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1095
    iget v3, p0, Ldbxyzptlk/db231222/ah/F;->e:I

    const/16 v4, 0x8

    if-lt v3, v4, :cond_3

    .line 1098
    cmp-long v3, v1, v5

    if-gez v3, :cond_5

    const/4 v3, 0x5

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1100
    :goto_1
    add-int/lit8 v0, v0, 0x1

    .line 1101
    iget v3, p0, Ldbxyzptlk/db231222/ah/F;->e:I

    const/16 v4, 0x9

    if-ne v3, v4, :cond_2

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(J)J

    move-result-wide v3

    rem-long/2addr v3, v7

    cmp-long v3, v3, v5

    if-nez v3, :cond_2

    .line 1103
    add-int/lit8 v0, v0, -0x4

    .line 1106
    :cond_2
    div-long/2addr v1, v7

    .line 1108
    :cond_3
    long-to-int v1, v1

    .line 1110
    iget-object v2, p0, Ldbxyzptlk/db231222/ah/F;->g:Ldbxyzptlk/db231222/ah/H;

    if-eqz v2, :cond_4

    .line 1111
    iget-object v2, p0, Ldbxyzptlk/db231222/ah/F;->g:Ldbxyzptlk/db231222/ah/H;

    invoke-interface {v2, v1}, Ldbxyzptlk/db231222/ah/H;->a(I)I

    move-result v2

    add-int/2addr v0, v2

    .line 1113
    :cond_4
    iget-object v2, p0, Ldbxyzptlk/db231222/ah/F;->h:Ldbxyzptlk/db231222/ah/H;

    if-eqz v2, :cond_0

    .line 1114
    iget-object v2, p0, Ldbxyzptlk/db231222/ah/F;->h:Ldbxyzptlk/db231222/ah/H;

    invoke-interface {v2, v1}, Ldbxyzptlk/db231222/ah/H;->a(I)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0

    .line 1098
    :cond_5
    const/4 v3, 0x4

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_1
.end method

.method final a(Ldbxyzptlk/db231222/ac/E;)J
    .locals 11

    .prologue
    const/16 v10, 0x9

    const-wide v0, 0x7fffffffffffffffL

    .line 1386
    iget v2, p0, Ldbxyzptlk/db231222/ah/F;->b:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    .line 1387
    const/4 v2, 0x0

    move-object v5, v2

    .line 1391
    :goto_0
    if-eqz v5, :cond_2

    iget v2, p0, Ldbxyzptlk/db231222/ah/F;->e:I

    invoke-virtual {p0, v5, v2}, Ldbxyzptlk/db231222/ah/F;->a(Ldbxyzptlk/db231222/ac/w;I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1464
    :cond_0
    :goto_1
    :pswitch_0
    return-wide v0

    .line 1389
    :cond_1
    invoke-interface {p1}, Ldbxyzptlk/db231222/ac/E;->b()Ldbxyzptlk/db231222/ac/w;

    move-result-object v2

    move-object v5, v2

    goto :goto_0

    .line 1397
    :cond_2
    iget v2, p0, Ldbxyzptlk/db231222/ah/F;->e:I

    packed-switch v2, :pswitch_data_0

    goto :goto_1

    .line 1401
    :pswitch_1
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->j()Ldbxyzptlk/db231222/ac/m;

    move-result-object v2

    invoke-interface {p1, v2}, Ldbxyzptlk/db231222/ac/E;->a(Ldbxyzptlk/db231222/ac/m;)I

    move-result v2

    int-to-long v2, v2

    .line 1433
    :goto_2
    const-wide/16 v6, 0x0

    cmp-long v4, v2, v6

    if-nez v4, :cond_3

    .line 1434
    iget v4, p0, Ldbxyzptlk/db231222/ah/F;->b:I

    packed-switch v4, :pswitch_data_1

    :cond_3
    :pswitch_2
    move-wide v0, v2

    .line 1464
    goto :goto_1

    .line 1404
    :pswitch_3
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->i()Ldbxyzptlk/db231222/ac/m;

    move-result-object v2

    invoke-interface {p1, v2}, Ldbxyzptlk/db231222/ac/E;->a(Ldbxyzptlk/db231222/ac/m;)I

    move-result v2

    int-to-long v2, v2

    .line 1405
    goto :goto_2

    .line 1407
    :pswitch_4
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->g()Ldbxyzptlk/db231222/ac/m;

    move-result-object v2

    invoke-interface {p1, v2}, Ldbxyzptlk/db231222/ac/E;->a(Ldbxyzptlk/db231222/ac/m;)I

    move-result v2

    int-to-long v2, v2

    .line 1408
    goto :goto_2

    .line 1410
    :pswitch_5
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->f()Ldbxyzptlk/db231222/ac/m;

    move-result-object v2

    invoke-interface {p1, v2}, Ldbxyzptlk/db231222/ac/E;->a(Ldbxyzptlk/db231222/ac/m;)I

    move-result v2

    int-to-long v2, v2

    .line 1411
    goto :goto_2

    .line 1413
    :pswitch_6
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->d()Ldbxyzptlk/db231222/ac/m;

    move-result-object v2

    invoke-interface {p1, v2}, Ldbxyzptlk/db231222/ac/E;->a(Ldbxyzptlk/db231222/ac/m;)I

    move-result v2

    int-to-long v2, v2

    .line 1414
    goto :goto_2

    .line 1416
    :pswitch_7
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->c()Ldbxyzptlk/db231222/ac/m;

    move-result-object v2

    invoke-interface {p1, v2}, Ldbxyzptlk/db231222/ac/E;->a(Ldbxyzptlk/db231222/ac/m;)I

    move-result v2

    int-to-long v2, v2

    .line 1417
    goto :goto_2

    .line 1419
    :pswitch_8
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->b()Ldbxyzptlk/db231222/ac/m;

    move-result-object v2

    invoke-interface {p1, v2}, Ldbxyzptlk/db231222/ac/E;->a(Ldbxyzptlk/db231222/ac/m;)I

    move-result v2

    int-to-long v2, v2

    .line 1420
    goto :goto_2

    .line 1422
    :pswitch_9
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->a()Ldbxyzptlk/db231222/ac/m;

    move-result-object v2

    invoke-interface {p1, v2}, Ldbxyzptlk/db231222/ac/E;->a(Ldbxyzptlk/db231222/ac/m;)I

    move-result v2

    int-to-long v2, v2

    .line 1423
    goto :goto_2

    .line 1426
    :pswitch_a
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->b()Ldbxyzptlk/db231222/ac/m;

    move-result-object v2

    invoke-interface {p1, v2}, Ldbxyzptlk/db231222/ac/E;->a(Ldbxyzptlk/db231222/ac/m;)I

    move-result v2

    .line 1427
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->a()Ldbxyzptlk/db231222/ac/m;

    move-result-object v3

    invoke-interface {p1, v3}, Ldbxyzptlk/db231222/ac/E;->a(Ldbxyzptlk/db231222/ac/m;)I

    move-result v3

    .line 1428
    int-to-long v6, v2

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    int-to-long v2, v3

    add-long/2addr v2, v6

    goto :goto_2

    .line 1438
    :pswitch_b
    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/ah/F;->b(Ldbxyzptlk/db231222/ac/E;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Ldbxyzptlk/db231222/ah/F;->f:[Ldbxyzptlk/db231222/ah/F;

    iget v6, p0, Ldbxyzptlk/db231222/ah/F;->e:I

    aget-object v4, v4, v6

    if-ne v4, p0, :cond_0

    .line 1439
    iget v4, p0, Ldbxyzptlk/db231222/ah/F;->e:I

    add-int/lit8 v4, v4, 0x1

    :goto_3
    if-gt v4, v10, :cond_3

    .line 1440
    invoke-virtual {p0, v5, v4}, Ldbxyzptlk/db231222/ah/F;->a(Ldbxyzptlk/db231222/ac/w;I)Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Ldbxyzptlk/db231222/ah/F;->f:[Ldbxyzptlk/db231222/ah/F;

    aget-object v6, v6, v4

    if-nez v6, :cond_0

    .line 1439
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 1449
    :pswitch_c
    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/ah/F;->b(Ldbxyzptlk/db231222/ac/E;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Ldbxyzptlk/db231222/ah/F;->f:[Ldbxyzptlk/db231222/ah/F;

    iget v6, p0, Ldbxyzptlk/db231222/ah/F;->e:I

    aget-object v4, v4, v6

    if-ne v4, p0, :cond_0

    .line 1450
    iget v4, p0, Ldbxyzptlk/db231222/ah/F;->e:I

    const/16 v6, 0x8

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 1451
    add-int/lit8 v4, v4, -0x1

    .line 1452
    :goto_4
    if-ltz v4, :cond_3

    if-gt v4, v10, :cond_3

    .line 1453
    invoke-virtual {p0, v5, v4}, Ldbxyzptlk/db231222/ah/F;->a(Ldbxyzptlk/db231222/ac/w;I)Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Ldbxyzptlk/db231222/ah/F;->f:[Ldbxyzptlk/db231222/ah/F;

    aget-object v6, v6, v4

    if-nez v6, :cond_0

    .line 1452
    :cond_5
    add-int/lit8 v4, v4, -0x1

    goto :goto_4

    .line 1397
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_a
    .end packed-switch

    .line 1434
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_c
        :pswitch_b
        :pswitch_2
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/StringBuffer;Ldbxyzptlk/db231222/ac/E;Ljava/util/Locale;)V
    .locals 9

    .prologue
    const-wide/16 v7, 0x3e8

    const/16 v6, 0x8

    .line 1121
    invoke-virtual {p0, p2}, Ldbxyzptlk/db231222/ah/F;->a(Ldbxyzptlk/db231222/ac/E;)J

    move-result-wide v1

    .line 1122
    const-wide v3, 0x7fffffffffffffffL

    cmp-long v0, v1, v3

    if-nez v0, :cond_1

    .line 1153
    :cond_0
    :goto_0
    return-void

    .line 1125
    :cond_1
    long-to-int v0, v1

    .line 1126
    iget v3, p0, Ldbxyzptlk/db231222/ah/F;->e:I

    if-lt v3, v6, :cond_2

    .line 1127
    div-long v3, v1, v7

    long-to-int v0, v3

    .line 1130
    :cond_2
    iget-object v3, p0, Ldbxyzptlk/db231222/ah/F;->g:Ldbxyzptlk/db231222/ah/H;

    if-eqz v3, :cond_3

    .line 1131
    iget-object v3, p0, Ldbxyzptlk/db231222/ah/F;->g:Ldbxyzptlk/db231222/ah/H;

    invoke-interface {v3, p1, v0}, Ldbxyzptlk/db231222/ah/H;->a(Ljava/lang/StringBuffer;I)V

    .line 1133
    :cond_3
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    .line 1134
    iget v4, p0, Ldbxyzptlk/db231222/ah/F;->a:I

    .line 1135
    const/4 v5, 0x1

    if-gt v4, v5, :cond_7

    .line 1136
    invoke-static {p1, v0}, Ldbxyzptlk/db231222/ah/x;->a(Ljava/lang/StringBuffer;I)V

    .line 1140
    :goto_1
    iget v4, p0, Ldbxyzptlk/db231222/ah/F;->e:I

    if-lt v4, v6, :cond_6

    .line 1141
    invoke-static {v1, v2}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    rem-long/2addr v4, v7

    long-to-int v4, v4

    .line 1142
    iget v5, p0, Ldbxyzptlk/db231222/ah/F;->e:I

    if-eq v5, v6, :cond_4

    if-lez v4, :cond_6

    .line 1143
    :cond_4
    const-wide/16 v5, 0x0

    cmp-long v5, v1, v5

    if-gez v5, :cond_5

    const-wide/16 v5, -0x3e8

    cmp-long v1, v1, v5

    if-lez v1, :cond_5

    .line 1144
    const/16 v1, 0x2d

    invoke-virtual {p1, v3, v1}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 1146
    :cond_5
    const/16 v1, 0x2e

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1147
    const/4 v1, 0x3

    invoke-static {p1, v4, v1}, Ldbxyzptlk/db231222/ah/x;->a(Ljava/lang/StringBuffer;II)V

    .line 1150
    :cond_6
    iget-object v1, p0, Ldbxyzptlk/db231222/ah/F;->h:Ldbxyzptlk/db231222/ah/H;

    if-eqz v1, :cond_0

    .line 1151
    iget-object v1, p0, Ldbxyzptlk/db231222/ah/F;->h:Ldbxyzptlk/db231222/ah/H;

    invoke-interface {v1, p1, v0}, Ldbxyzptlk/db231222/ah/H;->a(Ljava/lang/StringBuffer;I)V

    goto :goto_0

    .line 1138
    :cond_7
    invoke-static {p1, v0, v4}, Ldbxyzptlk/db231222/ah/x;->a(Ljava/lang/StringBuffer;II)V

    goto :goto_1
.end method

.method final a(Ldbxyzptlk/db231222/ac/w;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1477
    packed-switch p2, :pswitch_data_0

    .line 1498
    :cond_0
    :goto_0
    return v0

    .line 1481
    :pswitch_0
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->j()Ldbxyzptlk/db231222/ac/m;

    move-result-object v0

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/ac/w;->a(Ldbxyzptlk/db231222/ac/m;)Z

    move-result v0

    goto :goto_0

    .line 1483
    :pswitch_1
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->i()Ldbxyzptlk/db231222/ac/m;

    move-result-object v0

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/ac/w;->a(Ldbxyzptlk/db231222/ac/m;)Z

    move-result v0

    goto :goto_0

    .line 1485
    :pswitch_2
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->g()Ldbxyzptlk/db231222/ac/m;

    move-result-object v0

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/ac/w;->a(Ldbxyzptlk/db231222/ac/m;)Z

    move-result v0

    goto :goto_0

    .line 1487
    :pswitch_3
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->f()Ldbxyzptlk/db231222/ac/m;

    move-result-object v0

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/ac/w;->a(Ldbxyzptlk/db231222/ac/m;)Z

    move-result v0

    goto :goto_0

    .line 1489
    :pswitch_4
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->d()Ldbxyzptlk/db231222/ac/m;

    move-result-object v0

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/ac/w;->a(Ldbxyzptlk/db231222/ac/m;)Z

    move-result v0

    goto :goto_0

    .line 1491
    :pswitch_5
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->c()Ldbxyzptlk/db231222/ac/m;

    move-result-object v0

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/ac/w;->a(Ldbxyzptlk/db231222/ac/m;)Z

    move-result v0

    goto :goto_0

    .line 1493
    :pswitch_6
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->b()Ldbxyzptlk/db231222/ac/m;

    move-result-object v0

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/ac/w;->a(Ldbxyzptlk/db231222/ac/m;)Z

    move-result v0

    goto :goto_0

    .line 1495
    :pswitch_7
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->a()Ldbxyzptlk/db231222/ac/m;

    move-result-object v0

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/ac/w;->a(Ldbxyzptlk/db231222/ac/m;)Z

    move-result v0

    goto :goto_0

    .line 1498
    :pswitch_8
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->b()Ldbxyzptlk/db231222/ac/m;

    move-result-object v1

    invoke-virtual {p1, v1}, Ldbxyzptlk/db231222/ac/w;->a(Ldbxyzptlk/db231222/ac/m;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->a()Ldbxyzptlk/db231222/ac/m;

    move-result-object v1

    invoke-virtual {p1, v1}, Ldbxyzptlk/db231222/ac/w;->a(Ldbxyzptlk/db231222/ac/m;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1477
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method final b(Ldbxyzptlk/db231222/ac/E;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1468
    invoke-interface {p1}, Ldbxyzptlk/db231222/ac/E;->h()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1469
    invoke-interface {p1, v1}, Ldbxyzptlk/db231222/ac/E;->c(I)I

    move-result v3

    if-eqz v3, :cond_0

    .line 1473
    :goto_1
    return v0

    .line 1468
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1473
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method
