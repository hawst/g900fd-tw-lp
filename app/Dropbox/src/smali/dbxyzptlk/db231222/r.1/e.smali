.class public Ldbxyzptlk/db231222/r/e;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ldbxyzptlk/db231222/r/e;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Landroid/accounts/AccountManager;

.field private final d:Ldbxyzptlk/db231222/r/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/r/j",
            "<",
            "Ldbxyzptlk/db231222/r/k;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ldbxyzptlk/db231222/r/h;

.field private final f:Lcom/dropbox/sync/android/bz;

.field private g:Lcom/dropbox/sync/android/ac;

.field private final h:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Ldbxyzptlk/db231222/r/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/r/e;->a:Ljava/lang/String;

    .line 55
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/r/e;->b:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/r/h;Landroid/accounts/AccountManager;Lcom/dropbox/sync/android/bz;)V
    .locals 2

    .prologue
    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/r/e;->h:Ljava/lang/Object;

    .line 190
    iput-object p1, p0, Ldbxyzptlk/db231222/r/e;->e:Ldbxyzptlk/db231222/r/h;

    .line 191
    iput-object p2, p0, Ldbxyzptlk/db231222/r/e;->c:Landroid/accounts/AccountManager;

    .line 192
    iput-object p3, p0, Ldbxyzptlk/db231222/r/e;->f:Lcom/dropbox/sync/android/bz;

    .line 193
    new-instance v0, Ldbxyzptlk/db231222/r/j;

    invoke-direct {p0}, Ldbxyzptlk/db231222/r/e;->g()Ldbxyzptlk/db231222/r/k;

    move-result-object v1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/r/j;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/r/e;->d:Ldbxyzptlk/db231222/r/j;

    .line 194
    invoke-direct {p0}, Ldbxyzptlk/db231222/r/e;->f()V

    .line 195
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/accounts/Account;
    .locals 6

    .prologue
    .line 506
    invoke-direct {p0}, Ldbxyzptlk/db231222/r/e;->h()[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 507
    iget-object v4, p0, Ldbxyzptlk/db231222/r/e;->c:Landroid/accounts/AccountManager;

    const-string v5, "USER_ID"

    invoke-virtual {v4, v0, v5}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 508
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 513
    :goto_1
    return-object v0

    .line 506
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 513
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Ldbxyzptlk/db231222/s/q;Z)Ldbxyzptlk/db231222/r/d;
    .locals 4

    .prologue
    .line 481
    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/q;->d()Ljava/lang/String;

    move-result-object v1

    .line 482
    invoke-direct {p0, v1}, Ldbxyzptlk/db231222/r/e;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v2

    .line 483
    const/4 v0, 0x0

    .line 484
    if-eqz v2, :cond_0

    .line 485
    new-instance v0, Ldbxyzptlk/db231222/r/g;

    iget-object v3, p0, Ldbxyzptlk/db231222/r/e;->c:Landroid/accounts/AccountManager;

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/r/g;-><init>(Ljava/lang/String;Landroid/accounts/Account;Landroid/accounts/AccountManager;)V

    .line 489
    :cond_0
    iget-object v2, p0, Ldbxyzptlk/db231222/r/e;->e:Ldbxyzptlk/db231222/r/h;

    invoke-interface {v2, v1}, Ldbxyzptlk/db231222/r/h;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/n/P;

    move-result-object v1

    .line 492
    if-eqz p2, :cond_1

    .line 493
    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/P;->g()Ldbxyzptlk/db231222/s/a;

    move-result-object v2

    .line 494
    if-eqz v2, :cond_1

    .line 495
    invoke-static {p1}, Ldbxyzptlk/db231222/s/q;->a(Ldbxyzptlk/db231222/s/q;)Ldbxyzptlk/db231222/s/s;

    move-result-object v3

    .line 496
    invoke-virtual {v2}, Ldbxyzptlk/db231222/s/a;->d()Ldbxyzptlk/db231222/s/q;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/s/q;->l()Ldbxyzptlk/db231222/s/k;

    move-result-object v2

    invoke-virtual {v3, v2}, Ldbxyzptlk/db231222/s/s;->a(Ldbxyzptlk/db231222/s/k;)Ldbxyzptlk/db231222/s/s;

    .line 497
    invoke-virtual {v3}, Ldbxyzptlk/db231222/s/s;->b()Ldbxyzptlk/db231222/s/q;

    move-result-object p1

    .line 501
    :cond_1
    iget-object v2, p0, Ldbxyzptlk/db231222/r/e;->e:Ldbxyzptlk/db231222/r/h;

    invoke-interface {v2, p1, v0, v1}, Ldbxyzptlk/db231222/r/h;->a(Ldbxyzptlk/db231222/s/q;Ldbxyzptlk/db231222/r/g;Ldbxyzptlk/db231222/n/P;)Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    return-object v0
.end method

.method public static a()Ldbxyzptlk/db231222/r/e;
    .locals 2

    .prologue
    .line 210
    sget-object v0, Ldbxyzptlk/db231222/r/e;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/r/e;

    .line 211
    if-nez v0, :cond_0

    .line 212
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Instance has not yet been created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 215
    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ldbxyzptlk/db231222/r/h;Lcom/dropbox/sync/android/bz;)Ldbxyzptlk/db231222/r/e;
    .locals 3

    .prologue
    .line 198
    sget-object v1, Ldbxyzptlk/db231222/r/e;->b:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v1

    .line 199
    :try_start_0
    sget-object v0, Ldbxyzptlk/db231222/r/e;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 200
    new-instance v0, Ldbxyzptlk/db231222/r/e;

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    invoke-direct {v0, p1, v2, p2}, Ldbxyzptlk/db231222/r/e;-><init>(Ldbxyzptlk/db231222/r/h;Landroid/accounts/AccountManager;Lcom/dropbox/sync/android/bz;)V

    .line 201
    sget-object v2, Ldbxyzptlk/db231222/r/e;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 202
    monitor-exit v1

    return-object v0

    .line 204
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 206
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This method should only be invoked once"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 204
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/r/e;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Ldbxyzptlk/db231222/r/e;->h:Ljava/lang/Object;

    return-object v0
.end method

.method private a(Ldbxyzptlk/db231222/r/d;Ldbxyzptlk/db231222/y/k;)V
    .locals 3

    .prologue
    .line 276
    iget-object v1, p0, Ldbxyzptlk/db231222/r/e;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 277
    :try_start_0
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->w()Lcom/dropbox/sync/android/V;

    move-result-object v0

    if-nez v0, :cond_0

    .line 278
    iget-object v0, p0, Ldbxyzptlk/db231222/r/e;->f:Lcom/dropbox/sync/android/bz;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, p2}, Lcom/dropbox/sync/android/bz;->a(Ljava/lang/String;Ldbxyzptlk/db231222/y/k;)V

    .line 279
    iget-object v0, p0, Ldbxyzptlk/db231222/r/e;->f:Lcom/dropbox/sync/android/bz;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/dropbox/sync/android/bz;->a(Ljava/lang/String;)Lcom/dropbox/sync/android/V;

    move-result-object v0

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/r/d;->a(Lcom/dropbox/sync/android/V;)V

    .line 280
    iget-object v0, p0, Ldbxyzptlk/db231222/r/e;->f:Lcom/dropbox/sync/android/bz;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->w()Lcom/dropbox/sync/android/V;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/dropbox/sync/android/bz;->a(Lcom/dropbox/sync/android/V;)Lcom/dropbox/sync/android/ai;

    move-result-object v0

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/r/d;->a(Lcom/dropbox/sync/android/ai;)V

    .line 282
    :cond_0
    monitor-exit v1

    .line 283
    return-void

    .line 282
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private b(Ldbxyzptlk/db231222/z/R;Ldbxyzptlk/db231222/y/k;)Landroid/accounts/Account;
    .locals 12

    .prologue
    const/4 v4, 0x0

    .line 387
    iget-object v0, p1, Ldbxyzptlk/db231222/z/R;->a:Ldbxyzptlk/db231222/s/a;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/a;->d()Ldbxyzptlk/db231222/s/q;

    move-result-object v0

    .line 388
    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/q;->d()Ljava/lang/String;

    move-result-object v5

    .line 389
    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/q;->i()Ljava/lang/String;

    move-result-object v6

    .line 394
    invoke-direct {p0}, Ldbxyzptlk/db231222/r/e;->h()[Landroid/accounts/Account;

    move-result-object v7

    array-length v8, v7

    const/4 v0, 0x0

    move v3, v0

    move-object v2, v4

    move-object v0, v4

    :goto_0
    if-ge v3, v8, :cond_3

    aget-object v1, v7, v3

    .line 396
    iget-object v9, p0, Ldbxyzptlk/db231222/r/e;->c:Landroid/accounts/AccountManager;

    const-string v10, "USER_ID"

    invoke-virtual {v9, v1, v10}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 397
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    iget-object v10, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    if-nez v2, :cond_0

    .line 394
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    goto :goto_0

    .line 403
    :cond_0
    if-nez v0, :cond_1

    .line 404
    iget-object v0, p0, Ldbxyzptlk/db231222/r/e;->c:Landroid/accounts/AccountManager;

    invoke-static {v1, v0}, Ldbxyzptlk/db231222/r/g;->a(Landroid/accounts/Account;Landroid/accounts/AccountManager;)Landroid/os/Bundle;

    move-result-object v0

    .line 408
    :cond_1
    iget-object v10, p1, Ldbxyzptlk/db231222/z/R;->b:Ldbxyzptlk/db231222/s/m;

    .line 409
    invoke-virtual {v10}, Ldbxyzptlk/db231222/s/m;->c()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-virtual {v10}, Ldbxyzptlk/db231222/s/m;->d()Ldbxyzptlk/db231222/s/q;

    move-result-object v10

    invoke-virtual {v10}, Ldbxyzptlk/db231222/s/q;->d()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    move-object v1, v2

    .line 410
    goto :goto_1

    .line 414
    :cond_2
    iget-object v9, p0, Ldbxyzptlk/db231222/r/e;->c:Landroid/accounts/AccountManager;

    invoke-virtual {v9, v1, v4, v4}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-object v1, v2

    goto :goto_1

    .line 418
    :cond_3
    if-eqz v2, :cond_5

    .line 419
    iget-object v0, p0, Ldbxyzptlk/db231222/r/e;->c:Landroid/accounts/AccountManager;

    const-string v1, "KEY"

    iget-object v3, p2, Ldbxyzptlk/db231222/y/k;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v1, v3}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    iget-object v0, p0, Ldbxyzptlk/db231222/r/e;->c:Landroid/accounts/AccountManager;

    const-string v1, "SECRET"

    iget-object v3, p2, Ldbxyzptlk/db231222/y/k;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v1, v3}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    :cond_4
    :goto_2
    return-object v2

    .line 426
    :cond_5
    if-nez v0, :cond_6

    .line 427
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 431
    :cond_6
    const-string v1, "USER_ID"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    const-string v1, "KEY"

    iget-object v2, p2, Ldbxyzptlk/db231222/y/k;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    const-string v1, "SECRET"

    iget-object v2, p2, Ldbxyzptlk/db231222/y/k;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    new-instance v2, Landroid/accounts/Account;

    const-string v1, "com.dropbox.android.account"

    invoke-direct {v2, v6, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    iget-object v1, p0, Ldbxyzptlk/db231222/r/e;->c:Landroid/accounts/AccountManager;

    invoke-virtual {v1, v2, v4, v0}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 440
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Failed to add Account."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 441
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->c(Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method static synthetic b(Ldbxyzptlk/db231222/r/e;)Lcom/dropbox/sync/android/ac;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Ldbxyzptlk/db231222/r/e;->g:Lcom/dropbox/sync/android/ac;

    return-object v0
.end method

.method private declared-synchronized f()V
    .locals 8

    .prologue
    .line 234
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-static {v0}, Lcom/dropbox/android/filemanager/a;->a(Ldbxyzptlk/db231222/r/d;)Ldbxyzptlk/db231222/z/D;

    move-result-object v5

    .line 235
    iget-object v7, p0, Ldbxyzptlk/db231222/r/e;->h:Ljava/lang/Object;

    monitor-enter v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    :try_start_1
    iget-object v0, p0, Ldbxyzptlk/db231222/r/e;->f:Lcom/dropbox/sync/android/bz;

    invoke-virtual {v5}, Ldbxyzptlk/db231222/z/D;->f()Ldbxyzptlk/db231222/y/l;

    move-result-object v1

    invoke-virtual {v5}, Ldbxyzptlk/db231222/z/D;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Ldbxyzptlk/db231222/z/D;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, Ldbxyzptlk/db231222/z/D;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, Ldbxyzptlk/db231222/z/D;->e()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ldbxyzptlk/db231222/r/f;

    invoke-direct {v6, p0}, Ldbxyzptlk/db231222/r/f;-><init>(Ldbxyzptlk/db231222/r/e;)V

    invoke-virtual/range {v0 .. v6}, Lcom/dropbox/sync/android/bz;->a(Ldbxyzptlk/db231222/y/l;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/sync/android/ac;)V

    .line 252
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 255
    :try_start_2
    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v0

    .line 256
    if-eqz v0, :cond_1

    .line 257
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/k;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/r/d;

    .line 258
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->m()Ldbxyzptlk/db231222/r/g;

    move-result-object v2

    .line 259
    if-eqz v2, :cond_0

    .line 260
    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/g;->g()Ldbxyzptlk/db231222/y/k;

    move-result-object v2

    .line 261
    if-eqz v2, :cond_0

    .line 262
    invoke-direct {p0, v0, v2}, Ldbxyzptlk/db231222/r/e;->a(Ldbxyzptlk/db231222/r/d;Ldbxyzptlk/db231222/y/k;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 234
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 252
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 267
    :cond_1
    monitor-exit p0

    return-void
.end method

.method private g()Ldbxyzptlk/db231222/r/k;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 454
    invoke-direct {p0}, Ldbxyzptlk/db231222/r/e;->h()[Landroid/accounts/Account;

    move-result-object v1

    .line 455
    array-length v2, v1

    if-nez v2, :cond_0

    .line 475
    :goto_0
    return-object v0

    .line 462
    :cond_0
    invoke-static {}, Ldbxyzptlk/db231222/s/q;->m()Ldbxyzptlk/db231222/s/s;

    move-result-object v2

    .line 463
    iget-object v3, p0, Ldbxyzptlk/db231222/r/e;->c:Landroid/accounts/AccountManager;

    aget-object v4, v1, v6

    const-string v5, "USER_ID"

    invoke-virtual {v3, v4, v5}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ldbxyzptlk/db231222/s/s;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/s/s;

    .line 464
    aget-object v1, v1, v6

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ldbxyzptlk/db231222/s/s;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/s/s;

    .line 465
    invoke-virtual {v2}, Ldbxyzptlk/db231222/s/s;->b()Ldbxyzptlk/db231222/s/q;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Ldbxyzptlk/db231222/r/e;->a(Ldbxyzptlk/db231222/s/q;Z)Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    .line 469
    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/P;->h()Ldbxyzptlk/db231222/s/m;

    move-result-object v1

    .line 470
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/m;->c()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 471
    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/m;->d()Ldbxyzptlk/db231222/s/q;

    move-result-object v0

    invoke-direct {p0, v0, v6}, Ldbxyzptlk/db231222/r/e;->a(Ldbxyzptlk/db231222/s/q;Z)Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 475
    :cond_1
    new-instance v1, Ldbxyzptlk/db231222/r/k;

    invoke-direct {v1, v2, v0}, Ldbxyzptlk/db231222/r/k;-><init>(Ldbxyzptlk/db231222/r/d;Ldbxyzptlk/db231222/r/d;)V

    move-object v0, v1

    goto :goto_0
.end method

.method private h()[Landroid/accounts/Account;
    .locals 2

    .prologue
    .line 517
    iget-object v0, p0, Ldbxyzptlk/db231222/r/e;->c:Landroid/accounts/AccountManager;

    const-string v1, "com.dropbox.android.account"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/z/R;Ldbxyzptlk/db231222/y/k;)Ldbxyzptlk/db231222/r/d;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 315
    iget-object v5, p0, Ldbxyzptlk/db231222/r/e;->d:Ldbxyzptlk/db231222/r/j;

    monitor-enter v5

    .line 317
    :try_start_0
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/r/e;->b(Ldbxyzptlk/db231222/z/R;Ldbxyzptlk/db231222/y/k;)Landroid/accounts/Account;

    .line 320
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 321
    iget-object v0, p1, Ldbxyzptlk/db231222/z/R;->a:Ldbxyzptlk/db231222/s/a;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/a;->d()Ldbxyzptlk/db231222/s/q;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/q;->d()Ljava/lang/String;

    move-result-object v3

    .line 322
    invoke-interface {v6, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 325
    iget-object v0, p1, Ldbxyzptlk/db231222/z/R;->b:Ldbxyzptlk/db231222/s/m;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/m;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 326
    iget-object v0, p1, Ldbxyzptlk/db231222/z/R;->b:Ldbxyzptlk/db231222/s/m;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/m;->d()Ldbxyzptlk/db231222/s/q;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/q;->d()Ljava/lang/String;

    move-result-object v0

    .line 327
    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object v4, v0

    .line 332
    :goto_0
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 333
    iget-object v0, p0, Ldbxyzptlk/db231222/r/e;->d:Ldbxyzptlk/db231222/r/j;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/j;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/r/k;

    .line 334
    if-eqz v0, :cond_0

    .line 335
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/k;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldbxyzptlk/db231222/r/d;

    .line 336
    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v9, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 382
    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 341
    :cond_0
    :try_start_1
    invoke-interface {v7, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldbxyzptlk/db231222/r/d;

    .line 342
    if-nez v1, :cond_3

    .line 344
    iget-object v1, p1, Ldbxyzptlk/db231222/z/R;->a:Ldbxyzptlk/db231222/s/a;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->d()Ldbxyzptlk/db231222/s/q;

    move-result-object v1

    const/4 v3, 0x0

    invoke-direct {p0, v1, v3}, Ldbxyzptlk/db231222/r/e;->a(Ldbxyzptlk/db231222/s/q;Z)Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    move-object v3, v1

    .line 354
    :goto_2
    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->k()Ldbxyzptlk/db231222/y/k;

    move-result-object v1

    invoke-direct {p0, v3, v1}, Ldbxyzptlk/db231222/r/e;->a(Ldbxyzptlk/db231222/r/d;Ldbxyzptlk/db231222/y/k;)V

    .line 357
    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v1

    .line 358
    iget-object v8, p1, Ldbxyzptlk/db231222/z/R;->a:Ldbxyzptlk/db231222/s/a;

    invoke-virtual {v1, v8}, Ldbxyzptlk/db231222/n/P;->a(Ldbxyzptlk/db231222/s/a;)V

    .line 359
    iget-object v8, p1, Ldbxyzptlk/db231222/z/R;->b:Ldbxyzptlk/db231222/s/m;

    invoke-virtual {v1, v8}, Ldbxyzptlk/db231222/n/P;->a(Ldbxyzptlk/db231222/s/m;)V

    .line 362
    invoke-interface {v7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v6}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 365
    if-eqz v4, :cond_4

    .line 366
    invoke-interface {v7, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldbxyzptlk/db231222/r/d;

    .line 367
    if-nez v1, :cond_1

    .line 368
    iget-object v1, p1, Ldbxyzptlk/db231222/z/R;->b:Ldbxyzptlk/db231222/s/m;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/m;->d()Ldbxyzptlk/db231222/s/q;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Ldbxyzptlk/db231222/r/e;->a(Ldbxyzptlk/db231222/s/q;Z)Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    .line 375
    :cond_1
    :goto_3
    new-instance v2, Ldbxyzptlk/db231222/r/k;

    invoke-direct {v2, v3, v1}, Ldbxyzptlk/db231222/r/k;-><init>(Ldbxyzptlk/db231222/r/d;Ldbxyzptlk/db231222/r/d;)V

    .line 376
    iget-object v1, p0, Ldbxyzptlk/db231222/r/e;->d:Ldbxyzptlk/db231222/r/j;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/db231222/r/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 377
    if-eqz v0, :cond_2

    .line 378
    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/r/k;->a(Ldbxyzptlk/db231222/r/k;)V

    .line 381
    :cond_2
    monitor-exit v5

    return-object v3

    .line 349
    :cond_3
    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->l()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v3, v1

    goto :goto_2

    :cond_4
    move-object v1, v2

    goto :goto_3

    :cond_5
    move-object v4, v2

    goto/16 :goto_0
.end method

.method public final a(Lcom/dropbox/sync/android/ac;)V
    .locals 2

    .prologue
    .line 290
    iget-object v1, p0, Ldbxyzptlk/db231222/r/e;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 291
    :try_start_0
    iput-object p1, p0, Ldbxyzptlk/db231222/r/e;->g:Lcom/dropbox/sync/android/ac;

    .line 292
    monitor-exit v1

    .line 293
    return-void

    .line 292
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()Ldbxyzptlk/db231222/r/k;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Ldbxyzptlk/db231222/r/e;->d:Ldbxyzptlk/db231222/r/j;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/j;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/r/k;

    return-object v0
.end method

.method public final c()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x0

    .line 525
    sget-object v1, Ldbxyzptlk/db231222/r/e;->a:Ljava/lang/String;

    const-string v2, "clearAllCredentials()"

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 528
    invoke-direct {p0}, Ldbxyzptlk/db231222/r/e;->h()[Landroid/accounts/Account;

    move-result-object v2

    .line 529
    array-length v1, v2

    if-lez v1, :cond_2

    .line 530
    new-instance v3, Ljava/util/ArrayList;

    array-length v1, v2

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 531
    array-length v4, v2

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v2, v1

    .line 532
    iget-object v6, p0, Ldbxyzptlk/db231222/r/e;->c:Landroid/accounts/AccountManager;

    invoke-virtual {v6, v5, v7, v7}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 531
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v0

    .line 536
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 538
    aget-object v4, v2, v1

    .line 540
    :try_start_0
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountManagerFuture;

    invoke-interface {v0}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 541
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Did not remove credentials: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/accounts/AccountsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 543
    :catch_0
    move-exception v0

    .line 544
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to clear credentials: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 545
    :catch_1
    move-exception v0

    .line 546
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to clear credentials: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 536
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 552
    :cond_2
    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/e;->d()V

    .line 553
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 560
    sget-object v0, Ldbxyzptlk/db231222/r/e;->a:Ljava/lang/String;

    const-string v1, "clearUsers()"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    iget-object v1, p0, Ldbxyzptlk/db231222/r/e;->d:Ldbxyzptlk/db231222/r/j;

    monitor-enter v1

    .line 562
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/r/e;->d:Ldbxyzptlk/db231222/r/j;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/r/j;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/r/k;

    .line 563
    if-eqz v0, :cond_0

    .line 564
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/r/k;->a(Ldbxyzptlk/db231222/r/k;)V

    .line 566
    :cond_0
    monitor-exit v1

    .line 567
    return-void

    .line 566
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 596
    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
