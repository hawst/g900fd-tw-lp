.class public Ldbxyzptlk/db231222/r/a;
.super Landroid/support/v4/content/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/content/a",
        "<",
        "Ldbxyzptlk/db231222/r/c;",
        ">;"
    }
.end annotation


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field private final g:Lcom/dropbox/android/service/a;

.field private final h:Lcom/dropbox/android/service/e;

.field private final i:Lcom/dropbox/android/service/e;

.field private final j:Lcom/dropbox/android/service/d;

.field private k:Z

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Ldbxyzptlk/db231222/r/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/r/a;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/service/a;Lcom/dropbox/android/service/e;Lcom/dropbox/android/service/e;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-direct {p0, p1}, Landroid/support/v4/content/a;-><init>(Landroid/content/Context;)V

    .line 25
    new-instance v0, Ldbxyzptlk/db231222/r/b;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/r/b;-><init>(Ldbxyzptlk/db231222/r/a;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/r/a;->j:Lcom/dropbox/android/service/d;

    .line 32
    iput-boolean v1, p0, Ldbxyzptlk/db231222/r/a;->k:Z

    .line 33
    iput-boolean v1, p0, Ldbxyzptlk/db231222/r/a;->l:Z

    .line 76
    iput-object p2, p0, Ldbxyzptlk/db231222/r/a;->g:Lcom/dropbox/android/service/a;

    .line 77
    iput-object p3, p0, Ldbxyzptlk/db231222/r/a;->h:Lcom/dropbox/android/service/e;

    .line 78
    iput-object p4, p0, Ldbxyzptlk/db231222/r/a;->i:Lcom/dropbox/android/service/e;

    .line 79
    return-void
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/r/c;)V
    .locals 1

    .prologue
    .line 118
    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/a;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    invoke-super {p0, p1}, Landroid/support/v4/content/a;->b(Ljava/lang/Object;)V

    .line 121
    :cond_0
    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Ldbxyzptlk/db231222/r/c;

    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/r/a;->a(Ldbxyzptlk/db231222/r/c;)V

    return-void
.end method

.method public synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/a;->j()Ldbxyzptlk/db231222/r/c;

    move-result-object v0

    return-object v0
.end method

.method protected final f()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 83
    iget-object v1, p0, Ldbxyzptlk/db231222/r/a;->g:Lcom/dropbox/android/service/a;

    iget-object v2, p0, Ldbxyzptlk/db231222/r/a;->i:Lcom/dropbox/android/service/e;

    iget-object v3, p0, Ldbxyzptlk/db231222/r/a;->j:Lcom/dropbox/android/service/d;

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/service/a;->a(Lcom/dropbox/android/service/e;Lcom/dropbox/android/service/d;)V

    .line 84
    iput-boolean v0, p0, Ldbxyzptlk/db231222/r/a;->l:Z

    .line 86
    iget-object v1, p0, Ldbxyzptlk/db231222/r/a;->g:Lcom/dropbox/android/service/a;

    invoke-virtual {v1}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v1

    .line 90
    if-eqz v1, :cond_0

    iget-boolean v2, p0, Ldbxyzptlk/db231222/r/a;->k:Z

    if-nez v2, :cond_3

    .line 92
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 93
    new-instance v2, Ldbxyzptlk/db231222/r/c;

    invoke-direct {v2, v1, v0}, Ldbxyzptlk/db231222/r/c;-><init>(Ldbxyzptlk/db231222/s/a;Z)V

    invoke-virtual {p0, v2}, Ldbxyzptlk/db231222/r/a;->a(Ldbxyzptlk/db231222/r/c;)V

    .line 96
    :cond_1
    if-eqz v0, :cond_2

    .line 97
    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/a;->p()V

    .line 99
    :cond_2
    return-void

    .line 90
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final g()V
    .locals 2

    .prologue
    .line 103
    iget-boolean v0, p0, Ldbxyzptlk/db231222/r/a;->l:Z

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Ldbxyzptlk/db231222/r/a;->g:Lcom/dropbox/android/service/a;

    iget-object v1, p0, Ldbxyzptlk/db231222/r/a;->j:Lcom/dropbox/android/service/d;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/a;->a(Lcom/dropbox/android/service/d;)V

    .line 105
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/r/a;->l:Z

    .line 106
    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/a;->b()Z

    .line 108
    :cond_0
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/a;->g()V

    .line 113
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/r/a;->k:Z

    .line 114
    return-void
.end method

.method public j()Ldbxyzptlk/db231222/r/c;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 125
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/db231222/r/a;->k:Z

    .line 127
    :try_start_0
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v1

    .line 128
    new-instance v0, Ldbxyzptlk/db231222/r/c;

    iget-object v2, p0, Ldbxyzptlk/db231222/r/a;->g:Lcom/dropbox/android/service/a;

    iget-object v3, p0, Ldbxyzptlk/db231222/r/a;->h:Lcom/dropbox/android/service/e;

    invoke-virtual {v2, v3, v1}, Lcom/dropbox/android/service/a;->a(Lcom/dropbox/android/service/e;Ldbxyzptlk/db231222/r/e;)Ldbxyzptlk/db231222/s/a;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/r/c;-><init>(Ldbxyzptlk/db231222/s/a;Z)V
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    :goto_0
    return-object v0

    .line 130
    :catch_0
    move-exception v0

    .line 131
    sget-object v1, Ldbxyzptlk/db231222/r/a;->f:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to load account info: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ldbxyzptlk/db231222/w/a;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    new-instance v0, Ldbxyzptlk/db231222/r/c;

    iget-object v1, p0, Ldbxyzptlk/db231222/r/a;->g:Lcom/dropbox/android/service/a;

    invoke-virtual {v1}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v1

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/db231222/r/c;-><init>(Ldbxyzptlk/db231222/s/a;Z)V

    goto :goto_0
.end method
