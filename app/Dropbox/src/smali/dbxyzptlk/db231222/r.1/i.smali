.class public final Ldbxyzptlk/db231222/r/i;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/r/h;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ldbxyzptlk/db231222/n/k;

.field private final c:Ldbxyzptlk/db231222/n/K;

.field private final d:Lcom/dropbox/android/provider/m;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/n/k;Ldbxyzptlk/db231222/n/K;Lcom/dropbox/android/provider/m;)V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    iput-object p1, p0, Ldbxyzptlk/db231222/r/i;->a:Landroid/content/Context;

    .line 146
    iput-object p2, p0, Ldbxyzptlk/db231222/r/i;->b:Ldbxyzptlk/db231222/n/k;

    .line 147
    iput-object p3, p0, Ldbxyzptlk/db231222/r/i;->c:Ldbxyzptlk/db231222/n/K;

    .line 148
    iput-object p4, p0, Ldbxyzptlk/db231222/r/i;->d:Lcom/dropbox/android/provider/m;

    .line 149
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ldbxyzptlk/db231222/n/P;
    .locals 4

    .prologue
    .line 162
    new-instance v0, Ldbxyzptlk/db231222/n/P;

    iget-object v1, p0, Ldbxyzptlk/db231222/r/i;->a:Landroid/content/Context;

    invoke-static {p1}, Ldbxyzptlk/db231222/n/P;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ldbxyzptlk/db231222/r/i;->b:Ldbxyzptlk/db231222/n/k;

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/n/P;-><init>(Landroid/content/Context;Ljava/lang/String;Ldbxyzptlk/db231222/n/k;)V

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/s/q;Ldbxyzptlk/db231222/r/g;Ldbxyzptlk/db231222/n/P;)Ldbxyzptlk/db231222/r/d;
    .locals 7

    .prologue
    .line 154
    iget-object v0, p0, Ldbxyzptlk/db231222/r/i;->d:Lcom/dropbox/android/provider/m;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/m;->a()Lcom/dropbox/android/provider/j;

    move-result-object v5

    .line 155
    new-instance v0, Ldbxyzptlk/db231222/r/d;

    iget-object v4, p0, Ldbxyzptlk/db231222/r/i;->c:Ldbxyzptlk/db231222/n/K;

    iget-object v6, p0, Ldbxyzptlk/db231222/r/i;->a:Landroid/content/Context;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/r/d;-><init>(Ldbxyzptlk/db231222/s/q;Ldbxyzptlk/db231222/r/g;Ldbxyzptlk/db231222/n/P;Ldbxyzptlk/db231222/n/K;Lcom/dropbox/android/provider/j;Landroid/content/Context;)V

    .line 156
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->a()V

    .line 157
    return-object v0
.end method
