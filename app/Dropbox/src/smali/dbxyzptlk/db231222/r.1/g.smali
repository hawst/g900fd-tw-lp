.class public final Ldbxyzptlk/db231222/r/g;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:Z

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:J

.field private f:Ldbxyzptlk/db231222/y/k;

.field private g:Landroid/accounts/AccountManager;

.field private final h:Landroid/accounts/Account;

.field private final i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 615
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "USE_LOCK_CODE"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "LOCK_CODE"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "LOCK_CODE_ERASE"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "LOCK_CODE_LOCKED_UNTIL"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "ANALYTICS_SERIES_UUID"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "ANALYTICS_SEQUENCE_NUMBER"

    aput-object v2, v0, v1

    sput-object v0, Ldbxyzptlk/db231222/r/g;->a:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Landroid/accounts/Account;Landroid/accounts/AccountManager;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 620
    iput-boolean v0, p0, Ldbxyzptlk/db231222/r/g;->b:Z

    .line 622
    iput-boolean v0, p0, Ldbxyzptlk/db231222/r/g;->d:Z

    .line 635
    invoke-static {p2}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 636
    iput-object p2, p0, Ldbxyzptlk/db231222/r/g;->h:Landroid/accounts/Account;

    .line 637
    iput-object p3, p0, Ldbxyzptlk/db231222/r/g;->g:Landroid/accounts/AccountManager;

    .line 638
    iput-object p1, p0, Ldbxyzptlk/db231222/r/g;->i:Ljava/lang/String;

    .line 639
    invoke-direct {p0}, Ldbxyzptlk/db231222/r/g;->k()V

    .line 640
    return-void
.end method

.method static synthetic a(Landroid/accounts/Account;Landroid/accounts/AccountManager;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 604
    invoke-static {p0, p1}, Ldbxyzptlk/db231222/r/g;->b(Landroid/accounts/Account;Landroid/accounts/AccountManager;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 779
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/r/g;->g:Landroid/accounts/AccountManager;

    if-eqz v0, :cond_0

    .line 780
    iget-object v0, p0, Ldbxyzptlk/db231222/r/g;->g:Landroid/accounts/AccountManager;

    iget-object v1, p0, Ldbxyzptlk/db231222/r/g;->h:Landroid/accounts/Account;

    invoke-virtual {v0, v1, p1, p2}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 782
    :cond_0
    monitor-exit p0

    return-void

    .line 779
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static b(Landroid/accounts/Account;Landroid/accounts/AccountManager;)Landroid/os/Bundle;
    .locals 6

    .prologue
    .line 795
    new-instance v1, Landroid/os/Bundle;

    sget-object v0, Ldbxyzptlk/db231222/r/g;->a:[Ljava/lang/String;

    array-length v0, v0

    invoke-direct {v1, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 796
    sget-object v2, Ldbxyzptlk/db231222/r/g;->a:[Ljava/lang/String;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 797
    invoke-virtual {p1, p0, v4}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 798
    if-eqz v5, :cond_0

    .line 799
    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 796
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 803
    :cond_1
    return-object v1
.end method

.method private declared-synchronized c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 786
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/r/g;->g:Landroid/accounts/AccountManager;

    if-eqz v0, :cond_0

    .line 787
    iget-object v0, p0, Ldbxyzptlk/db231222/r/g;->g:Landroid/accounts/AccountManager;

    iget-object v1, p0, Ldbxyzptlk/db231222/r/g;->h:Landroid/accounts/Account;

    invoke-virtual {v0, v1, p1}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 789
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 786
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized k()V
    .locals 2

    .prologue
    .line 693
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Ldbxyzptlk/db231222/r/g;->l()Ldbxyzptlk/db231222/y/k;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/r/g;->f:Ldbxyzptlk/db231222/y/k;

    .line 695
    const-string v0, "USE_LOCK_CODE"

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/r/g;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/r/g;->b:Z

    .line 696
    const-string v0, "LOCK_CODE"

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/r/g;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/r/g;->c:Ljava/lang/String;

    .line 697
    const-string v0, "LOCK_CODE_ERASE"

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/r/g;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/r/g;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 699
    :try_start_1
    const-string v0, "LOCK_CODE_LOCKED_UNTIL"

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/r/g;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/r/g;->e:J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 703
    :goto_0
    monitor-exit p0

    return-void

    .line 700
    :catch_0
    move-exception v0

    .line 701
    const-wide/16 v0, 0x0

    :try_start_2
    iput-wide v0, p0, Ldbxyzptlk/db231222/r/g;->e:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 693
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized l()Ldbxyzptlk/db231222/y/k;
    .locals 3

    .prologue
    .line 707
    monitor-enter p0

    :try_start_0
    const-string v0, "KEY"

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/r/g;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 708
    const-string v0, "SECRET"

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/r/g;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 709
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 710
    new-instance v0, Ldbxyzptlk/db231222/y/k;

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/y/k;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 713
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 707
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method final a()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 643
    iget-object v0, p0, Ldbxyzptlk/db231222/r/g;->h:Landroid/accounts/Account;

    return-object v0
.end method

.method final a(I)V
    .locals 2

    .prologue
    .line 761
    const-string v0, "ANALYTICS_SEQUENCE_NUMBER"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/r/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    return-void
.end method

.method final declared-synchronized a(J)V
    .locals 2

    .prologue
    .line 687
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Ldbxyzptlk/db231222/r/g;->e:J

    .line 688
    const-string v0, "LOCK_CODE_LOCKED_UNTIL"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/r/g;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 689
    monitor-exit p0

    return-void

    .line 687
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 667
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Ldbxyzptlk/db231222/r/g;->c:Ljava/lang/String;

    .line 668
    iget-object v0, p0, Ldbxyzptlk/db231222/r/g;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Ldbxyzptlk/db231222/r/g;->b:Z

    .line 669
    const-string v0, "USE_LOCK_CODE"

    iget-boolean v1, p0, Ldbxyzptlk/db231222/r/g;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/r/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 670
    const-string v0, "LOCK_CODE"

    iget-object v1, p0, Ldbxyzptlk/db231222/r/g;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/r/g;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 671
    monitor-exit p0

    return-void

    .line 668
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 667
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Z)V
    .locals 2

    .prologue
    .line 678
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Ldbxyzptlk/db231222/r/g;->d:Z

    .line 679
    const-string v0, "LOCK_CODE_ERASE"

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/r/g;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 680
    monitor-exit p0

    return-void

    .line 678
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 742
    const-string v0, "ANALYTICS_SERIES_UUID"

    invoke-direct {p0, v0, p1}, Ldbxyzptlk/db231222/r/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 743
    return-void
.end method

.method final declared-synchronized b()Z
    .locals 1

    .prologue
    .line 652
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/g;->c()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 657
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Ldbxyzptlk/db231222/r/g;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 658
    const/4 v0, 0x0

    .line 660
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Ldbxyzptlk/db231222/r/g;->c:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 657
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized d()Z
    .locals 1

    .prologue
    .line 674
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Ldbxyzptlk/db231222/r/g;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized e()J
    .locals 2

    .prologue
    .line 683
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Ldbxyzptlk/db231222/r/g;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized f()V
    .locals 1

    .prologue
    .line 722
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Ldbxyzptlk/db231222/r/g;->l()Ldbxyzptlk/db231222/y/k;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/r/g;->f:Ldbxyzptlk/db231222/y/k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 723
    monitor-exit p0

    return-void

    .line 722
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized g()Ldbxyzptlk/db231222/y/k;
    .locals 1

    .prologue
    .line 727
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/r/g;->f:Ldbxyzptlk/db231222/y/k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 735
    const-string v0, "ANALYTICS_SERIES_UUID"

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/r/g;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final i()I
    .locals 1

    .prologue
    .line 749
    const-string v0, "ANALYTICS_SEQUENCE_NUMBER"

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/r/g;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 750
    if-eqz v0, :cond_0

    .line 751
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 753
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final declared-synchronized j()V
    .locals 2

    .prologue
    .line 770
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Ldbxyzptlk/db231222/r/g;->b:Z

    .line 771
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/r/g;->c:Ljava/lang/String;

    .line 772
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/r/g;->d:Z

    .line 773
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ldbxyzptlk/db231222/r/g;->e:J

    .line 774
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/r/g;->f:Ldbxyzptlk/db231222/y/k;

    .line 775
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/r/g;->g:Landroid/accounts/AccountManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 776
    monitor-exit p0

    return-void

    .line 770
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
