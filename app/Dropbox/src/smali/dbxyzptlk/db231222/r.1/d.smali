.class public final Ldbxyzptlk/db231222/r/d;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:Ldbxyzptlk/db231222/r/g;

.field private final b:Ldbxyzptlk/db231222/s/q;

.field private final c:Ldbxyzptlk/db231222/n/P;

.field private final d:Ldbxyzptlk/db231222/n/K;

.field private final e:Ldbxyzptlk/db231222/k/h;

.field private final f:Lcom/dropbox/android/provider/j;

.field private final g:Lcom/dropbox/android/notifications/y;

.field private final h:Lcom/dropbox/android/util/aE;

.field private i:Lcom/dropbox/android/provider/MetadataManager;

.field private j:Lcom/dropbox/android/filemanager/E;

.field private final k:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final l:Lcom/dropbox/android/service/a;

.field private final m:Lcom/dropbox/android/service/i;

.field private final n:Lcom/dropbox/android/notifications/d;

.field private final o:Ldbxyzptlk/db231222/z/M;

.field private final p:Lcom/dropbox/android/filemanager/I;

.field private final q:Lcom/dropbox/android/albums/PhotosModel;

.field private final r:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/dropbox/sync/android/V;",
            ">;"
        }
    .end annotation
.end field

.field private final s:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/dropbox/sync/android/ai;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Ldbxyzptlk/db231222/l/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/l/k",
            "<",
            "Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;",
            "Ldbxyzptlk/db231222/z/aB;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final u:Ldbxyzptlk/db231222/l/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/l/k",
            "<",
            "Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;",
            "Ldbxyzptlk/db231222/z/ag;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final v:Ljava/lang/Object;

.field private final w:Ldbxyzptlk/db231222/l/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/l/k",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/s/q;Ldbxyzptlk/db231222/r/g;Ldbxyzptlk/db231222/n/P;Ldbxyzptlk/db231222/n/K;Lcom/dropbox/android/provider/j;Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/r/d;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 89
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/r/d;->r:Ljava/util/concurrent/atomic/AtomicReference;

    .line 90
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/r/d;->s:Ljava/util/concurrent/atomic/AtomicReference;

    .line 94
    invoke-static {}, Ldbxyzptlk/db231222/l/k;->a()Ldbxyzptlk/db231222/l/k;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/r/d;->t:Ldbxyzptlk/db231222/l/k;

    .line 97
    invoke-static {}, Ldbxyzptlk/db231222/l/k;->a()Ldbxyzptlk/db231222/l/k;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/r/d;->u:Ldbxyzptlk/db231222/l/k;

    .line 340
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/r/d;->v:Ljava/lang/Object;

    .line 383
    invoke-static {}, Ldbxyzptlk/db231222/l/k;->a()Ldbxyzptlk/db231222/l/k;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/r/d;->w:Ldbxyzptlk/db231222/l/k;

    .line 103
    iput-object p1, p0, Ldbxyzptlk/db231222/r/d;->b:Ldbxyzptlk/db231222/s/q;

    .line 104
    iput-object p2, p0, Ldbxyzptlk/db231222/r/d;->a:Ldbxyzptlk/db231222/r/g;

    .line 105
    iput-object p3, p0, Ldbxyzptlk/db231222/r/d;->c:Ldbxyzptlk/db231222/n/P;

    .line 106
    iput-object p4, p0, Ldbxyzptlk/db231222/r/d;->d:Ldbxyzptlk/db231222/n/K;

    .line 107
    iput-object p5, p0, Ldbxyzptlk/db231222/r/d;->f:Lcom/dropbox/android/provider/j;

    .line 108
    new-instance v0, Lcom/dropbox/android/service/a;

    invoke-direct {v0, p0, p6}, Lcom/dropbox/android/service/a;-><init>(Ldbxyzptlk/db231222/r/d;Landroid/content/Context;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/r/d;->l:Lcom/dropbox/android/service/a;

    .line 110
    invoke-static {}, Ldbxyzptlk/db231222/k/h;->a()Ldbxyzptlk/db231222/k/h;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/r/d;->e:Ldbxyzptlk/db231222/k/h;

    .line 112
    new-instance v0, Lcom/dropbox/android/notifications/d;

    invoke-direct {v0, p6, p0}, Lcom/dropbox/android/notifications/d;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/r/d;->n:Lcom/dropbox/android/notifications/d;

    .line 113
    new-instance v0, Lcom/dropbox/android/filemanager/I;

    iget-object v1, p0, Ldbxyzptlk/db231222/r/d;->f:Lcom/dropbox/android/provider/j;

    iget-object v2, p0, Ldbxyzptlk/db231222/r/d;->e:Ldbxyzptlk/db231222/k/h;

    invoke-direct {v0, p6, p0, v1, v2}, Lcom/dropbox/android/filemanager/I;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/k/h;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/r/d;->p:Lcom/dropbox/android/filemanager/I;

    .line 114
    new-instance v0, Lcom/dropbox/android/notifications/y;

    iget-object v1, p0, Ldbxyzptlk/db231222/r/d;->n:Lcom/dropbox/android/notifications/d;

    invoke-direct {v0, v1}, Lcom/dropbox/android/notifications/y;-><init>(Lcom/dropbox/android/notifications/d;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/r/d;->g:Lcom/dropbox/android/notifications/y;

    .line 116
    invoke-static {p0}, Lcom/dropbox/android/filemanager/a;->a(Ldbxyzptlk/db231222/r/d;)Ldbxyzptlk/db231222/z/D;

    move-result-object v0

    .line 117
    new-instance v1, Ldbxyzptlk/db231222/z/M;

    invoke-static {}, Lcom/dropbox/android/filemanager/a;->c()Ldbxyzptlk/db231222/v/r;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ldbxyzptlk/db231222/z/M;-><init>(Ldbxyzptlk/db231222/z/D;Ldbxyzptlk/db231222/v/r;)V

    iput-object v1, p0, Ldbxyzptlk/db231222/r/d;->o:Ldbxyzptlk/db231222/z/M;

    .line 118
    new-instance v0, Lcom/dropbox/android/albums/PhotosModel;

    invoke-virtual {p6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/db231222/r/d;->o:Ldbxyzptlk/db231222/z/M;

    iget-object v3, p0, Ldbxyzptlk/db231222/r/d;->f:Lcom/dropbox/android/provider/j;

    iget-object v4, p0, Ldbxyzptlk/db231222/r/d;->c:Ldbxyzptlk/db231222/n/P;

    iget-object v5, p0, Ldbxyzptlk/db231222/r/d;->p:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v5}, Lcom/dropbox/android/filemanager/I;->b()Ldbxyzptlk/db231222/j/i;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/albums/PhotosModel;-><init>(Landroid/content/ContentResolver;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/n/P;Ldbxyzptlk/db231222/j/i;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/r/d;->q:Lcom/dropbox/android/albums/PhotosModel;

    .line 121
    new-instance v0, Lcom/dropbox/android/util/aE;

    iget-object v1, p0, Ldbxyzptlk/db231222/r/d;->e:Ldbxyzptlk/db231222/k/h;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/k/h;->h()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, p6, v1}, Lcom/dropbox/android/util/aE;-><init>(Landroid/content/Context;Ljava/io/File;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/r/d;->h:Lcom/dropbox/android/util/aE;

    .line 122
    new-instance v0, Lcom/dropbox/android/service/i;

    iget-object v3, p0, Ldbxyzptlk/db231222/r/d;->o:Ldbxyzptlk/db231222/z/M;

    iget-object v4, p0, Ldbxyzptlk/db231222/r/d;->p:Lcom/dropbox/android/filemanager/I;

    iget-object v5, p0, Ldbxyzptlk/db231222/r/d;->n:Lcom/dropbox/android/notifications/d;

    move-object v1, p6

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/service/i;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/n/P;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/notifications/d;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/r/d;->m:Lcom/dropbox/android/service/i;

    .line 123
    return-void
.end method


# virtual methods
.method public final A()Lcom/dropbox/android/notifications/y;
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->g:Lcom/dropbox/android/notifications/y;

    return-object v0
.end method

.method public final B()Lcom/dropbox/android/util/aE;
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->h:Lcom/dropbox/android/util/aE;

    return-object v0
.end method

.method public final C()Ldbxyzptlk/db231222/l/k;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldbxyzptlk/db231222/l/k",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 391
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->w:Ldbxyzptlk/db231222/l/k;

    return-object v0
.end method

.method final a()V
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->a()V

    .line 139
    return-void
.end method

.method final a(Lcom/dropbox/sync/android/V;)V
    .locals 2

    .prologue
    .line 304
    invoke-static {p1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 305
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->r:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 306
    return-void
.end method

.method final a(Lcom/dropbox/sync/android/ai;)V
    .locals 2

    .prologue
    .line 314
    invoke-static {p1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 315
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->s:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 316
    return-void
.end method

.method public final b()Ldbxyzptlk/db231222/l/k;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldbxyzptlk/db231222/l/k",
            "<",
            "Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;",
            "Ldbxyzptlk/db231222/z/aB;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->t:Ldbxyzptlk/db231222/l/k;

    return-object v0
.end method

.method public final c()Ldbxyzptlk/db231222/l/k;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldbxyzptlk/db231222/l/k",
            "<",
            "Lcom/dropbox/android/payments/DbxSubscriptions$UpgradeActionKey;",
            "Ldbxyzptlk/db231222/z/ag;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 146
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->u:Ldbxyzptlk/db231222/l/k;

    return-object v0
.end method

.method public final d()Lcom/dropbox/android/service/a;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->l:Lcom/dropbox/android/service/a;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->b:Ldbxyzptlk/db231222/s/q;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/q;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->b:Ldbxyzptlk/db231222/s/q;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/q;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ldbxyzptlk/db231222/s/k;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->b:Ldbxyzptlk/db231222/s/q;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/q;->l()Ldbxyzptlk/db231222/s/k;

    move-result-object v0

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final i()V
    .locals 2

    .prologue
    .line 197
    monitor-enter p0

    .line 198
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 199
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->a:Ldbxyzptlk/db231222/r/g;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->a:Ldbxyzptlk/db231222/r/g;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/g;->j()V

    .line 201
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/r/d;->a:Ldbxyzptlk/db231222/r/g;

    .line 203
    :cond_0
    monitor-exit p0

    .line 204
    return-void

    .line 203
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 211
    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/a;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v0

    .line 212
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/a;->v()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Ldbxyzptlk/db231222/y/k;
    .locals 1

    .prologue
    .line 223
    monitor-enter p0

    .line 224
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->a:Ldbxyzptlk/db231222/r/g;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->a:Ldbxyzptlk/db231222/r/g;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/g;->g()Ldbxyzptlk/db231222/y/k;

    move-result-object v0

    monitor-exit p0

    .line 229
    :goto_0
    return-object v0

    .line 227
    :cond_0
    monitor-exit p0

    .line 229
    const/4 v0, 0x0

    goto :goto_0

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final l()V
    .locals 1

    .prologue
    .line 238
    monitor-enter p0

    .line 239
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->a:Ldbxyzptlk/db231222/r/g;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->a:Ldbxyzptlk/db231222/r/g;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/g;->f()V

    .line 242
    :cond_0
    monitor-exit p0

    .line 243
    return-void

    .line 242
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final m()Ldbxyzptlk/db231222/r/g;
    .locals 1

    .prologue
    .line 247
    monitor-enter p0

    .line 248
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->a:Ldbxyzptlk/db231222/r/g;

    monitor-exit p0

    return-object v0

    .line 249
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final n()Ldbxyzptlk/db231222/n/P;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->c:Ldbxyzptlk/db231222/n/P;

    return-object v0
.end method

.method public final o()Ldbxyzptlk/db231222/n/K;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->d:Ldbxyzptlk/db231222/n/K;

    return-object v0
.end method

.method public final p()Ldbxyzptlk/db231222/k/h;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->e:Ldbxyzptlk/db231222/k/h;

    return-object v0
.end method

.method public final q()Lcom/dropbox/android/provider/j;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->f:Lcom/dropbox/android/provider/j;

    return-object v0
.end method

.method public final r()Lcom/dropbox/android/albums/PhotosModel;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->q:Lcom/dropbox/android/albums/PhotosModel;

    return-object v0
.end method

.method public final s()Lcom/dropbox/android/service/i;
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->m:Lcom/dropbox/android/service/i;

    return-object v0
.end method

.method public final t()Lcom/dropbox/android/notifications/d;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->n:Lcom/dropbox/android/notifications/d;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 396
    new-instance v0, Ldbxyzptlk/db231222/ab/a;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/ab/a;-><init>(Ljava/lang/Object;)V

    .line 397
    const-string v1, "id"

    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/ab/a;->a(Ljava/lang/String;Ljava/lang/Object;)Ldbxyzptlk/db231222/ab/a;

    move-result-object v1

    const-string v2, "isValid"

    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/d;->h()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Ldbxyzptlk/db231222/ab/a;->a(Ljava/lang/String;Z)Ldbxyzptlk/db231222/ab/a;

    .line 398
    monitor-enter p0

    .line 399
    :try_start_0
    iget-object v1, p0, Ldbxyzptlk/db231222/r/d;->a:Ldbxyzptlk/db231222/r/g;

    if-eqz v1, :cond_0

    .line 400
    const-string v1, "account"

    iget-object v2, p0, Ldbxyzptlk/db231222/r/d;->a:Ldbxyzptlk/db231222/r/g;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/g;->a()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/ab/a;->a(Ljava/lang/String;Ljava/lang/Object;)Ldbxyzptlk/db231222/ab/a;

    .line 402
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 404
    invoke-virtual {v0}, Ldbxyzptlk/db231222/ab/a;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 402
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final u()Ldbxyzptlk/db231222/z/M;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->o:Ldbxyzptlk/db231222/z/M;

    return-object v0
.end method

.method public final v()Lcom/dropbox/sync/android/ai;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->s:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/ai;

    return-object v0
.end method

.method public final w()Lcom/dropbox/sync/android/V;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->r:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/sync/android/V;

    return-object v0
.end method

.method public final x()Lcom/dropbox/android/filemanager/I;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->p:Lcom/dropbox/android/filemanager/I;

    return-object v0
.end method

.method public final y()Lcom/dropbox/android/provider/MetadataManager;
    .locals 4

    .prologue
    .line 352
    iget-object v1, p0, Ldbxyzptlk/db231222/r/d;->v:Ljava/lang/Object;

    monitor-enter v1

    .line 353
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->i:Lcom/dropbox/android/provider/MetadataManager;

    if-nez v0, :cond_0

    .line 355
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 356
    new-instance v2, Lcom/dropbox/android/provider/MetadataManager;

    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v3

    invoke-direct {v2, p0, v3, v0}, Lcom/dropbox/android/provider/MetadataManager;-><init>(Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/a;Landroid/content/ContentResolver;)V

    iput-object v2, p0, Ldbxyzptlk/db231222/r/d;->i:Lcom/dropbox/android/provider/MetadataManager;

    .line 359
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->i:Lcom/dropbox/android/provider/MetadataManager;

    monitor-exit v1

    return-object v0

    .line 360
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final z()Lcom/dropbox/android/filemanager/E;
    .locals 3

    .prologue
    .line 366
    iget-object v1, p0, Ldbxyzptlk/db231222/r/d;->v:Ljava/lang/Object;

    monitor-enter v1

    .line 367
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->j:Lcom/dropbox/android/filemanager/E;

    if-nez v0, :cond_0

    .line 368
    new-instance v0, Lcom/dropbox/android/filemanager/E;

    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/d;->y()Lcom/dropbox/android/provider/MetadataManager;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/dropbox/android/filemanager/E;-><init>(Lcom/dropbox/android/provider/MetadataManager;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/r/d;->j:Lcom/dropbox/android/filemanager/E;

    .line 370
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/r/d;->j:Lcom/dropbox/android/filemanager/E;

    monitor-exit v1

    return-object v0

    .line 371
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
