.class public final Ldbxyzptlk/db231222/r/k;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Ldbxyzptlk/db231222/r/d;",
        ">;"
    }
.end annotation


# instance fields
.field private volatile a:Z

.field private final b:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Ldbxyzptlk/db231222/r/d;",
            "Ldbxyzptlk/db231222/r/d;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ldbxyzptlk/db231222/n/K;


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/r/d;Ldbxyzptlk/db231222/r/d;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-boolean v0, p0, Ldbxyzptlk/db231222/r/k;->a:Z

    .line 38
    invoke-static {p1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 40
    if-eqz p2, :cond_1

    .line 41
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    .line 42
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 43
    if-lez v1, :cond_1

    move-object v3, p1

    move-object p1, p2

    move-object p2, v3

    .line 50
    :cond_1
    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/r/k;->b:Landroid/util/Pair;

    .line 51
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/d;->o()Ldbxyzptlk/db231222/n/K;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/r/k;->c:Ldbxyzptlk/db231222/n/K;

    .line 52
    return-void
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;)Ldbxyzptlk/db231222/r/d;
    .locals 3

    .prologue
    .line 179
    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/k;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/r/d;

    .line 180
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->m()Ldbxyzptlk/db231222/r/g;

    move-result-object v2

    .line 181
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/g;->a()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 185
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ldbxyzptlk/db231222/s/k;)Ldbxyzptlk/db231222/r/d;
    .locals 3

    .prologue
    .line 169
    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/k;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/r/d;

    .line 170
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->g()Ldbxyzptlk/db231222/s/k;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 174
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Ldbxyzptlk/db231222/r/k;->b:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 63
    iget-object v0, p0, Ldbxyzptlk/db231222/r/k;->b:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v0

    .line 65
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Ldbxyzptlk/db231222/r/k;->b:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Ldbxyzptlk/db231222/r/k;->b:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method final a(Ldbxyzptlk/db231222/r/k;)V
    .locals 4

    .prologue
    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/db231222/r/k;->a:Z

    .line 96
    new-instance v1, Ljava/util/HashSet;

    const/4 v0, 0x2

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 97
    if-eqz p1, :cond_0

    .line 98
    invoke-virtual {p1}, Ldbxyzptlk/db231222/r/k;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/r/d;

    .line 99
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 102
    :cond_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/k;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/r/d;

    .line 103
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 104
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->i()V

    goto :goto_1

    .line 107
    :cond_2
    return-void
.end method

.method final b()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Ldbxyzptlk/db231222/r/k;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Ldbxyzptlk/db231222/r/k;->b:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ldbxyzptlk/db231222/r/d;
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Ldbxyzptlk/db231222/r/k;->b:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 125
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Expected a single user"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/r/k;->b:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ldbxyzptlk/db231222/r/d;

    return-object v0
.end method

.method public final e()Ldbxyzptlk/db231222/r/d;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 137
    const/4 v1, 0x3

    new-array v2, v1, [Ldbxyzptlk/db231222/s/k;

    sget-object v1, Ldbxyzptlk/db231222/s/k;->b:Ldbxyzptlk/db231222/s/k;

    aput-object v1, v2, v0

    const/4 v1, 0x1

    sget-object v3, Ldbxyzptlk/db231222/s/k;->a:Ldbxyzptlk/db231222/s/k;

    aput-object v3, v2, v1

    const/4 v1, 0x2

    sget-object v3, Ldbxyzptlk/db231222/s/k;->c:Ldbxyzptlk/db231222/s/k;

    aput-object v3, v2, v1

    .line 142
    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 143
    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/r/k;->a(Ldbxyzptlk/db231222/s/k;)Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 144
    if-eqz v0, :cond_0

    .line 148
    :goto_1
    return-object v0

    .line 142
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 148
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final f()Ldbxyzptlk/db231222/r/d;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Ldbxyzptlk/db231222/r/k;->b:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ldbxyzptlk/db231222/r/d;

    return-object v0
.end method

.method public final g()Ldbxyzptlk/db231222/n/K;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Ldbxyzptlk/db231222/r/k;->c:Ldbxyzptlk/db231222/n/K;

    return-object v0
.end method

.method public final h()Ldbxyzptlk/db231222/r/o;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 195
    iget-object v0, p0, Ldbxyzptlk/db231222/r/k;->b:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->m()Ldbxyzptlk/db231222/r/g;

    move-result-object v2

    .line 197
    iget-object v0, p0, Ldbxyzptlk/db231222/r/k;->b:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v0, :cond_3

    .line 198
    iget-object v0, p0, Ldbxyzptlk/db231222/r/k;->b:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->m()Ldbxyzptlk/db231222/r/g;

    move-result-object v0

    .line 201
    :goto_0
    if-nez v2, :cond_2

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    .line 206
    :goto_1
    if-nez v1, :cond_0

    .line 207
    invoke-static {}, Ldbxyzptlk/db231222/r/m;->g()Ldbxyzptlk/db231222/r/m;

    move-result-object v0

    .line 216
    :goto_2
    return-object v0

    .line 210
    :cond_0
    if-nez v0, :cond_1

    .line 211
    new-instance v0, Ldbxyzptlk/db231222/r/l;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/r/l;-><init>(Ldbxyzptlk/db231222/r/g;)V

    goto :goto_2

    .line 214
    :cond_1
    new-instance v2, Ldbxyzptlk/db231222/r/l;

    invoke-direct {v2, v1}, Ldbxyzptlk/db231222/r/l;-><init>(Ldbxyzptlk/db231222/r/g;)V

    .line 215
    new-instance v1, Ldbxyzptlk/db231222/r/l;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/r/l;-><init>(Ldbxyzptlk/db231222/r/g;)V

    .line 216
    new-instance v0, Ldbxyzptlk/db231222/r/n;

    invoke-direct {v0, v2, v1}, Ldbxyzptlk/db231222/r/n;-><init>(Ldbxyzptlk/db231222/r/o;Ldbxyzptlk/db231222/r/o;)V

    goto :goto_2

    :cond_2
    move-object v1, v2

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ldbxyzptlk/db231222/r/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Ldbxyzptlk/db231222/r/k;->b:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 76
    iget-object v0, p0, Ldbxyzptlk/db231222/r/k;->b:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 78
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x2

    new-array v1, v0, [Ldbxyzptlk/db231222/r/d;

    const/4 v2, 0x0

    iget-object v0, p0, Ldbxyzptlk/db231222/r/k;->b:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ldbxyzptlk/db231222/r/d;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    iget-object v0, p0, Ldbxyzptlk/db231222/r/k;->b:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ldbxyzptlk/db231222/r/d;

    aput-object v0, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 221
    new-instance v0, Ldbxyzptlk/db231222/ab/a;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/ab/a;-><init>(Ljava/lang/Object;)V

    const-string v1, "id"

    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/k;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/ab/a;->a(Ljava/lang/String;Ljava/lang/Object;)Ldbxyzptlk/db231222/ab/a;

    move-result-object v0

    const-string v1, "isValid"

    invoke-virtual {p0}, Ldbxyzptlk/db231222/r/k;->b()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/ab/a;->a(Ljava/lang/String;Z)Ldbxyzptlk/db231222/ab/a;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ab/a;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
