.class public Ldbxyzptlk/db231222/y/r;
.super Ldbxyzptlk/db231222/y/a;
.source "panda.py"


# instance fields
.field private a:Ldbxyzptlk/db231222/y/m;


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/y/l;Ldbxyzptlk/db231222/y/o;)V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/y/a;-><init>(Ldbxyzptlk/db231222/y/l;Ldbxyzptlk/db231222/y/o;)V

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/y/r;->a:Ldbxyzptlk/db231222/y/m;

    .line 91
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 215
    const/16 v0, 0x8

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    invoke-virtual {p0}, Ldbxyzptlk/db231222/y/r;->h()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "oauth_callback"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    aput-object p2, v4, v0

    const/4 v0, 0x4

    const-string v1, "oauth_verifier"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    aput-object p3, v4, v0

    const/4 v0, 0x6

    const-string v1, "use_oauth1a"

    aput-object v1, v4, v0

    const/4 v1, 0x7

    if-eqz p2, :cond_1

    const-string v0, "true"

    :goto_0
    aput-object v0, v4, v1

    .line 221
    sget-object v0, Ldbxyzptlk/db231222/v/w;->a:Ldbxyzptlk/db231222/v/w;

    invoke-virtual {p0}, Ldbxyzptlk/db231222/y/r;->b()Ljava/lang/String;

    move-result-object v1

    const-string v3, "r6"

    move-object v2, p1

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/db231222/v/v;->b(Ldbxyzptlk/db231222/v/w;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/db231222/y/n;)Ldbxyzptlk/db231222/v/l;

    move-result-object v0

    iget-object v0, v0, Ldbxyzptlk/db231222/v/l;->b:Lorg/apache/http/HttpResponse;

    .line 226
    invoke-static {v0}, Ldbxyzptlk/db231222/v/v;->b(Lorg/apache/http/HttpResponse;)Ljava/util/Map;

    move-result-object v2

    .line 228
    const-string v0, "oauth_token"

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "oauth_token_secret"

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 230
    :cond_0
    new-instance v0, Ldbxyzptlk/db231222/w/f;

    const-string v1, "Did not get tokens from Dropbox"

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/w/f;-><init>(Ljava/lang/String;)V

    throw v0

    .line 215
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 233
    :cond_2
    if-eqz p3, :cond_3

    .line 234
    new-instance v3, Ldbxyzptlk/db231222/y/k;

    const-string v0, "oauth_token"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "oauth_token_secret"

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v3, v0, v1}, Ldbxyzptlk/db231222/y/k;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Ldbxyzptlk/db231222/y/r;->a(Ldbxyzptlk/db231222/y/k;)V

    .line 238
    :cond_3
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ldbxyzptlk/db231222/y/s;
    .locals 5

    .prologue
    .line 130
    const-string v0, "/oauth/request_token"

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Ldbxyzptlk/db231222/y/r;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 133
    new-instance v2, Ldbxyzptlk/db231222/y/m;

    const-string v0, "oauth_token"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "oauth_token_secret"

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v2, v0, v1}, Ldbxyzptlk/db231222/y/m;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "oauth_token"

    aput-object v3, v0, v1

    const/4 v1, 0x1

    iget-object v3, v2, Ldbxyzptlk/db231222/y/m;->a:Ljava/lang/String;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    const-string v3, "locale"

    aput-object v3, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p0}, Ldbxyzptlk/db231222/y/r;->h()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    .line 141
    invoke-virtual {p0}, Ldbxyzptlk/db231222/y/r;->d()Ljava/lang/String;

    move-result-object v1

    const-string v3, "r6"

    const-string v4, "/oauth/authorize"

    invoke-static {v1, v3, v4, v0}, Ldbxyzptlk/db231222/v/v;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 144
    new-instance v1, Ldbxyzptlk/db231222/y/s;

    invoke-direct {v1, v0, v2}, Ldbxyzptlk/db231222/y/s;-><init>(Ljava/lang/String;Ldbxyzptlk/db231222/y/m;)V

    return-object v1
.end method

.method public final a(Ldbxyzptlk/db231222/y/m;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 181
    iput-object p1, p0, Ldbxyzptlk/db231222/y/r;->a:Ldbxyzptlk/db231222/y/m;

    .line 183
    :try_start_0
    const-string v0, "/oauth/access_token"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p2}, Ldbxyzptlk/db231222/y/r;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 185
    const-string v1, "uid"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    iput-object v2, p0, Ldbxyzptlk/db231222/y/r;->a:Ldbxyzptlk/db231222/y/m;

    return-object v0

    :catchall_0
    move-exception v0

    iput-object v2, p0, Ldbxyzptlk/db231222/y/r;->a:Ldbxyzptlk/db231222/y/m;

    throw v0
.end method

.method public final a(Lorg/apache/http/HttpRequest;)V
    .locals 3

    .prologue
    .line 204
    iget-object v0, p0, Ldbxyzptlk/db231222/y/r;->a:Ldbxyzptlk/db231222/y/m;

    if-eqz v0, :cond_0

    .line 205
    const-string v0, "Authorization"

    invoke-virtual {p0}, Ldbxyzptlk/db231222/y/r;->f()Ldbxyzptlk/db231222/y/l;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/db231222/y/r;->a:Ldbxyzptlk/db231222/y/m;

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/y/r;->a(Ldbxyzptlk/db231222/y/l;Ldbxyzptlk/db231222/y/k;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    :goto_0
    return-void

    .line 209
    :cond_0
    invoke-super {p0, p1}, Ldbxyzptlk/db231222/y/a;->a(Lorg/apache/http/HttpRequest;)V

    goto :goto_0
.end method
