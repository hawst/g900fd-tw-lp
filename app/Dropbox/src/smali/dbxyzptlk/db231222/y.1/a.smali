.class public abstract Ldbxyzptlk/db231222/y/a;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/y/n;


# instance fields
.field private final a:Ldbxyzptlk/db231222/y/o;

.field private final b:Ldbxyzptlk/db231222/y/l;

.field private c:Ldbxyzptlk/db231222/y/k;

.field private d:Lorg/apache/http/client/HttpClient;


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/y/l;Ldbxyzptlk/db231222/y/o;)V
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Ldbxyzptlk/db231222/y/a;-><init>(Ldbxyzptlk/db231222/y/l;Ldbxyzptlk/db231222/y/o;Ldbxyzptlk/db231222/y/k;)V

    .line 107
    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/y/l;Ldbxyzptlk/db231222/y/o;Ldbxyzptlk/db231222/y/k;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-object v0, p0, Ldbxyzptlk/db231222/y/a;->c:Ldbxyzptlk/db231222/y/k;

    .line 99
    iput-object v0, p0, Ldbxyzptlk/db231222/y/a;->d:Lorg/apache/http/client/HttpClient;

    .line 116
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'appKeyPair\' must be non-null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'type\' must be non-null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :cond_1
    iput-object p1, p0, Ldbxyzptlk/db231222/y/a;->b:Ldbxyzptlk/db231222/y/l;

    .line 120
    iput-object p2, p0, Ldbxyzptlk/db231222/y/a;->a:Ldbxyzptlk/db231222/y/o;

    .line 121
    iput-object p3, p0, Ldbxyzptlk/db231222/y/a;->c:Ldbxyzptlk/db231222/y/k;

    .line 122
    return-void
.end method

.method protected static a(Ldbxyzptlk/db231222/y/l;Ldbxyzptlk/db231222/y/k;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 189
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 190
    const-string v0, "OAuth oauth_version=\"1.0\""

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    const-string v0, ", oauth_signature_method=\"PLAINTEXT\""

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    const-string v0, ", oauth_consumer_key=\""

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Ldbxyzptlk/db231222/y/l;->a:Ljava/lang/String;

    invoke-static {v2}, Ldbxyzptlk/db231222/y/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    if-eqz p1, :cond_0

    .line 203
    const-string v0, ", oauth_token=\""

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p1, Ldbxyzptlk/db231222/y/k;->a:Ljava/lang/String;

    invoke-static {v2}, Ldbxyzptlk/db231222/y/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Ldbxyzptlk/db231222/y/l;->b:Ljava/lang/String;

    invoke-static {v2}, Ldbxyzptlk/db231222/y/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "&"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p1, Ldbxyzptlk/db231222/y/k;->b:Ljava/lang/String;

    invoke-static {v2}, Ldbxyzptlk/db231222/y/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 210
    :goto_0
    const-string v2, ", oauth_signature=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 208
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Ldbxyzptlk/db231222/y/l;->b:Ljava/lang/String;

    invoke-static {v2}, Ldbxyzptlk/db231222/y/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "&"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 218
    :try_start_0
    const-string v0, "UTF-8"

    invoke-static {p0, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 219
    :catch_0
    move-exception v0

    .line 220
    new-instance v1, Ljava/lang/AssertionError;

    const-string v2, "UTF-8 isn\'t available"

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    .line 221
    invoke-virtual {v1, v0}, Ljava/lang/AssertionError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 222
    throw v1
.end method


# virtual methods
.method public a()Ldbxyzptlk/db231222/y/k;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Ldbxyzptlk/db231222/y/a;->c:Ldbxyzptlk/db231222/y/k;

    return-object v0
.end method

.method public a(Ldbxyzptlk/db231222/y/k;)V
    .locals 2

    .prologue
    .line 128
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'accessTokenPair\' must be non-null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129
    :cond_0
    iput-object p1, p0, Ldbxyzptlk/db231222/y/a;->c:Ldbxyzptlk/db231222/y/k;

    .line 130
    return-void
.end method

.method public a(Lorg/apache/http/HttpRequest;)V
    .locals 3

    .prologue
    .line 183
    const-string v0, "Authorization"

    iget-object v1, p0, Ldbxyzptlk/db231222/y/a;->b:Ldbxyzptlk/db231222/y/l;

    invoke-virtual {p0}, Ldbxyzptlk/db231222/y/a;->a()Ldbxyzptlk/db231222/y/k;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/y/a;->a(Ldbxyzptlk/db231222/y/l;Ldbxyzptlk/db231222/y/k;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    return-void
.end method

.method public final a(Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 2

    .prologue
    const/16 v1, 0x7530

    .line 334
    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    .line 335
    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 336
    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 337
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 341
    const-string v0, "api.dropbox.com"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 346
    const-string v0, "api-content.dropbox.com"

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 351
    const-string v0, "www.dropbox.com"

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 355
    const-string v0, "api-notify.dropbox.com"

    return-object v0
.end method

.method public final f()Ldbxyzptlk/db231222/y/l;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Ldbxyzptlk/db231222/y/a;->b:Ldbxyzptlk/db231222/y/l;

    return-object v0
.end method

.method public final g()Ldbxyzptlk/db231222/y/o;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Ldbxyzptlk/db231222/y/a;->a:Ldbxyzptlk/db231222/y/o;

    return-object v0
.end method

.method public h()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 160
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    return-object v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0}, Ldbxyzptlk/db231222/y/a;->a()Ldbxyzptlk/db231222/y/k;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized j()Ldbxyzptlk/db231222/y/p;
    .locals 1

    .prologue
    .line 233
    monitor-enter p0

    const/4 v0, 0x0

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized k()Lorg/apache/http/client/HttpClient;
    .locals 6

    .prologue
    .line 246
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/y/a;->d:Lorg/apache/http/client/HttpClient;

    if-nez v0, :cond_0

    .line 249
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 250
    new-instance v1, Ldbxyzptlk/db231222/y/b;

    invoke-direct {v1, p0}, Ldbxyzptlk/db231222/y/b;-><init>(Ldbxyzptlk/db231222/y/a;)V

    invoke-static {v0, v1}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxConnectionsPerRoute(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/params/ConnPerRoute;)V

    .line 256
    const/16 v1, 0x14

    invoke-static {v0, v1}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxTotalConnections(Lorg/apache/http/params/HttpParams;I)V

    .line 259
    new-instance v1, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 260
    new-instance v2, Lorg/apache/http/conn/scheme/Scheme;

    const-string v3, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v4

    const/16 v5, 0x50

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v1, v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 262
    new-instance v2, Lorg/apache/http/conn/scheme/Scheme;

    const-string v3, "https"

    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v4

    const/16 v5, 0x1bb

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v1, v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 265
    new-instance v2, Ldbxyzptlk/db231222/y/f;

    invoke-direct {v2, v0, v1}, Ldbxyzptlk/db231222/y/f;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 269
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 270
    const/16 v1, 0x7530

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 271
    const/16 v1, 0x7530

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 272
    const/16 v1, 0x2000

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 273
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "OfficialDropboxJavaSDK/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Ldbxyzptlk/db231222/v/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpProtocolParams;->setUserAgent(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 276
    new-instance v1, Ldbxyzptlk/db231222/y/c;

    invoke-direct {v1, p0, v2, v0}, Ldbxyzptlk/db231222/y/c;-><init>(Ldbxyzptlk/db231222/y/a;Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    .line 288
    new-instance v0, Ldbxyzptlk/db231222/y/d;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/y/d;-><init>(Ldbxyzptlk/db231222/y/a;)V

    invoke-virtual {v1, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->addRequestInterceptor(Lorg/apache/http/HttpRequestInterceptor;)V

    .line 299
    new-instance v0, Ldbxyzptlk/db231222/y/e;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/y/e;-><init>(Ldbxyzptlk/db231222/y/a;)V

    invoke-virtual {v1, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->addResponseInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 321
    iput-object v1, p0, Ldbxyzptlk/db231222/y/a;->d:Lorg/apache/http/client/HttpClient;

    .line 324
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/y/a;->d:Lorg/apache/http/client/HttpClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 246
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
