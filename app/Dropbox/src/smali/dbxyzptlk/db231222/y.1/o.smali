.class public final enum Ldbxyzptlk/db231222/y/o;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldbxyzptlk/db231222/y/o;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ldbxyzptlk/db231222/y/o;

.field public static final enum b:Ldbxyzptlk/db231222/y/o;

.field private static final synthetic d:[Ldbxyzptlk/db231222/y/o;


# instance fields
.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 121
    new-instance v0, Ldbxyzptlk/db231222/y/o;

    const-string v1, "DROPBOX"

    const-string v2, "dropbox"

    invoke-direct {v0, v1, v3, v2}, Ldbxyzptlk/db231222/y/o;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/y/o;->a:Ldbxyzptlk/db231222/y/o;

    new-instance v0, Ldbxyzptlk/db231222/y/o;

    const-string v1, "APP_FOLDER"

    const-string v2, "sandbox"

    invoke-direct {v0, v1, v4, v2}, Ldbxyzptlk/db231222/y/o;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/y/o;->b:Ldbxyzptlk/db231222/y/o;

    .line 120
    const/4 v0, 0x2

    new-array v0, v0, [Ldbxyzptlk/db231222/y/o;

    sget-object v1, Ldbxyzptlk/db231222/y/o;->a:Ldbxyzptlk/db231222/y/o;

    aput-object v1, v0, v3

    sget-object v1, Ldbxyzptlk/db231222/y/o;->b:Ldbxyzptlk/db231222/y/o;

    aput-object v1, v0, v4

    sput-object v0, Ldbxyzptlk/db231222/y/o;->d:[Ldbxyzptlk/db231222/y/o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 125
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 126
    iput-object p3, p0, Ldbxyzptlk/db231222/y/o;->c:Ljava/lang/String;

    .line 127
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldbxyzptlk/db231222/y/o;
    .locals 1

    .prologue
    .line 120
    const-class v0, Ldbxyzptlk/db231222/y/o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/y/o;

    return-object v0
.end method

.method public static values()[Ldbxyzptlk/db231222/y/o;
    .locals 1

    .prologue
    .line 120
    sget-object v0, Ldbxyzptlk/db231222/y/o;->d:[Ldbxyzptlk/db231222/y/o;

    invoke-virtual {v0}, [Ldbxyzptlk/db231222/y/o;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/y/o;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Ldbxyzptlk/db231222/y/o;->c:Ljava/lang/String;

    return-object v0
.end method
