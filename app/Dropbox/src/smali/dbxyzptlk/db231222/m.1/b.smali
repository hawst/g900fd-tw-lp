.class public final Ldbxyzptlk/db231222/m/b;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/m/a;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Z


# direct methods
.method private constructor <init>(ILjava/lang/String;Z)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput p1, p0, Ldbxyzptlk/db231222/m/b;->a:I

    .line 26
    iput-object p2, p0, Ldbxyzptlk/db231222/m/b;->b:Ljava/lang/String;

    .line 27
    iput-boolean p3, p0, Ldbxyzptlk/db231222/m/b;->c:Z

    .line 28
    return-void
.end method

.method public static a(ILjava/lang/String;)Ldbxyzptlk/db231222/m/a;
    .locals 2

    .prologue
    .line 32
    new-instance v0, Ldbxyzptlk/db231222/m/b;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Ldbxyzptlk/db231222/m/b;-><init>(ILjava/lang/String;Z)V

    return-object v0
.end method

.method private a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Ldbxyzptlk/db231222/m/b;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Ldbxyzptlk/db231222/m/b;->b:Ljava/lang/String;

    .line 44
    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Ldbxyzptlk/db231222/m/b;->a:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(ILjava/lang/String;)Ldbxyzptlk/db231222/m/a;
    .locals 2

    .prologue
    .line 37
    new-instance v0, Ldbxyzptlk/db231222/m/b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Ldbxyzptlk/db231222/m/b;-><init>(ILjava/lang/String;Z)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;)V
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Ldbxyzptlk/db231222/m/b;->c:Z

    if-eqz v0, :cond_0

    .line 51
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/m/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/notifications/NotificationsFeedFragment$ErrorDialogFrag;->a(Ljava/lang/String;)Lcom/dropbox/android/notifications/NotificationsFeedFragment$ErrorDialogFrag;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/dropbox/android/notifications/NotificationsFeedFragment$ErrorDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 55
    :goto_0
    return-void

    .line 53
    :cond_0
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/m/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
