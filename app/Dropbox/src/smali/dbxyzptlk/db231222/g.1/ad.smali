.class public Ldbxyzptlk/db231222/g/ad;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ldbxyzptlk/db231222/g/a;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Ldbxyzptlk/db231222/g/ad;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/g/ad;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 37
    return-void
.end method


# virtual methods
.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/db231222/g/a;
    .locals 6

    .prologue
    const v5, 0x7f0d005d

    const/4 v4, 0x0

    .line 48
    :try_start_0
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/a;->d()Ljava/lang/String;

    .line 50
    new-instance v0, Ldbxyzptlk/db231222/g/ag;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/g/ag;-><init>(Ldbxyzptlk/db231222/g/ae;)V
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_3

    .line 67
    :goto_0
    return-object v0

    .line 51
    :catch_0
    move-exception v0

    .line 53
    sget-object v1, Ldbxyzptlk/db231222/g/ad;->a:Ljava/lang/String;

    const-string v2, "Error resending twofactor code"

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const v1, 0x7f0d0240

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/w/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 55
    new-instance v0, Ldbxyzptlk/db231222/g/H;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/g/H;-><init>(Ljava/lang/String;Z)V

    goto :goto_0

    .line 56
    :catch_1
    move-exception v0

    .line 57
    sget-object v1, Ldbxyzptlk/db231222/g/ad;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error resending twofactor code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 59
    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/w/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 60
    new-instance v0, Ldbxyzptlk/db231222/g/H;

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/db231222/g/H;-><init>(Ljava/lang/String;Z)V

    goto :goto_0

    .line 61
    :catch_2
    move-exception v0

    .line 62
    const v0, 0x7f0d005b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 63
    new-instance v0, Ldbxyzptlk/db231222/g/H;

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/db231222/g/H;-><init>(Ljava/lang/String;Z)V

    goto :goto_0

    .line 64
    :catch_3
    move-exception v0

    .line 65
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 66
    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 67
    new-instance v0, Ldbxyzptlk/db231222/g/H;

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/db231222/g/H;-><init>(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/ad;->a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/db231222/g/a;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Ldbxyzptlk/db231222/g/a;)V
    .locals 0

    .prologue
    .line 73
    invoke-interface {p2, p1}, Ldbxyzptlk/db231222/g/a;->a(Landroid/content/Context;)V

    .line 74
    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 41
    sget-object v0, Ldbxyzptlk/db231222/g/ad;->a:Ljava/lang/String;

    const-string v1, "Error resending twofactor code."

    invoke-static {v0, v1, p2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 42
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 43
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p2, Ldbxyzptlk/db231222/g/a;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/ad;->a(Landroid/content/Context;Ldbxyzptlk/db231222/g/a;)V

    return-void
.end method
