.class public final Ldbxyzptlk/db231222/g/ah;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Ljava/lang/Void;",
        "Ldbxyzptlk/db231222/z/as;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ldbxyzptlk/db231222/r/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 61
    iput-object p2, p0, Ldbxyzptlk/db231222/g/ah;->b:Ldbxyzptlk/db231222/r/d;

    .line 62
    return-void
.end method


# virtual methods
.method protected final varargs a(Landroid/content/Context;[Ljava/util/List;)Ldbxyzptlk/db231222/z/as;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "[",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ldbxyzptlk/db231222/z/as;"
        }
    .end annotation

    .prologue
    .line 66
    const/4 v0, 0x0

    aget-object v0, p2, v0

    iput-object v0, p0, Ldbxyzptlk/db231222/g/ah;->a:Ljava/util/List;

    .line 68
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/g/ah;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/g/ah;->a:Ljava/util/List;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/z/M;->b(Ljava/util/List;)Ldbxyzptlk/db231222/z/as;
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 73
    :goto_0
    return-object v0

    .line 69
    :catch_0
    move-exception v0

    .line 70
    instance-of v1, v0, Ldbxyzptlk/db231222/w/d;

    if-nez v1, :cond_0

    instance-of v1, v0, Ldbxyzptlk/db231222/w/j;

    if-nez v1, :cond_0

    .line 71
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 73
    :cond_0
    new-instance v0, Ldbxyzptlk/db231222/z/as;

    sget-object v1, Ldbxyzptlk/db231222/z/au;->f:Ldbxyzptlk/db231222/z/au;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/z/as;-><init>(Ldbxyzptlk/db231222/z/au;)V

    goto :goto_0
.end method

.method protected final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    check-cast p2, [Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/ah;->a(Landroid/content/Context;[Ljava/util/List;)Ldbxyzptlk/db231222/z/as;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Ldbxyzptlk/db231222/z/as;)V
    .locals 3

    .prologue
    .line 29
    sget-object v0, Ldbxyzptlk/db231222/g/ai;->a:[I

    invoke-virtual {p2}, Ldbxyzptlk/db231222/z/as;->a()Ldbxyzptlk/db231222/z/au;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/z/au;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 51
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 32
    :pswitch_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0027

    iget-object v2, p0, Ldbxyzptlk/db231222/g/ah;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    .line 33
    iget-object v0, p0, Ldbxyzptlk/db231222/g/ah;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->o()Ldbxyzptlk/db231222/n/K;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/K;->h()Ljava/util/List;

    move-result-object v0

    .line 34
    iget-object v2, p0, Ldbxyzptlk/db231222/g/ah;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 35
    iget-object v2, p0, Ldbxyzptlk/db231222/g/ah;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->o()Ldbxyzptlk/db231222/n/K;

    move-result-object v2

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/n/K;->a(Ljava/util/List;)V

    move-object v0, p1

    .line 36
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    move-object v0, v1

    .line 53
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 54
    return-void

    .line 39
    :pswitch_1
    const v0, 0x7f0d02b4

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 42
    :pswitch_2
    const v0, 0x7f0d02b6

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 45
    :pswitch_3
    const v0, 0x7f0d02b5

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 48
    :pswitch_4
    const v0, 0x7f0d02b7

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 29
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p2, Ldbxyzptlk/db231222/z/as;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/ah;->a(Landroid/content/Context;Ldbxyzptlk/db231222/z/as;)V

    return-void
.end method
