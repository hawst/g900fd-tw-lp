.class public Ldbxyzptlk/db231222/g/as;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Long;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ldbxyzptlk/db231222/z/M;

.field private final c:Lcom/dropbox/android/filemanager/LocalEntry;

.field private final d:Z

.field private final e:Ldbxyzptlk/db231222/g/ar;

.field private final f:Lcom/dropbox/android/util/analytics/ChainInfo;

.field private g:Ldbxyzptlk/db231222/z/ai;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Ldbxyzptlk/db231222/g/as;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/g/as;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/filemanager/LocalEntry;ZLdbxyzptlk/db231222/g/ar;Lcom/dropbox/android/util/analytics/ChainInfo;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 91
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/g/as;->g:Ldbxyzptlk/db231222/z/ai;

    .line 54
    iput-object p2, p0, Ldbxyzptlk/db231222/g/as;->b:Ldbxyzptlk/db231222/z/M;

    .line 55
    iput-object p3, p0, Ldbxyzptlk/db231222/g/as;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 56
    iput-boolean p4, p0, Ldbxyzptlk/db231222/g/as;->d:Z

    .line 57
    iput-object p5, p0, Ldbxyzptlk/db231222/g/as;->e:Ldbxyzptlk/db231222/g/ar;

    .line 58
    iput-object p6, p0, Ldbxyzptlk/db231222/g/as;->f:Lcom/dropbox/android/util/analytics/ChainInfo;

    .line 60
    invoke-virtual {p0}, Ldbxyzptlk/db231222/g/as;->f()V

    .line 61
    const v0, 0x7f0d00c7

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(I)Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;

    move-result-object v0

    .line 62
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 63
    return-void
.end method

.method private static a(Lcom/dropbox/android/service/J;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/dropbox/android/service/J;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    const-string v0, "wifi"

    .line 88
    :goto_0
    return-object v0

    .line 83
    :cond_0
    invoke-virtual {p0}, Lcom/dropbox/android/service/J;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    const-string v0, "3g"

    goto :goto_0

    .line 85
    :cond_1
    invoke-virtual {p0}, Lcom/dropbox/android/service/J;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 86
    const-string v0, "2g"

    goto :goto_0

    .line 88
    :cond_2
    const-string v0, "none"

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/as;->a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 96
    .line 97
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 98
    if-eqz v0, :cond_1

    .line 99
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 100
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "x"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 104
    :goto_0
    invoke-static {}, Lcom/dropbox/android/service/G;->a()Lcom/dropbox/android/service/G;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/service/G;->b()Lcom/dropbox/android/service/J;

    move-result-object v2

    .line 105
    invoke-static {v2}, Ldbxyzptlk/db231222/g/as;->a(Lcom/dropbox/android/service/J;)Ljava/lang/String;

    move-result-object v3

    .line 106
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v4

    invoke-virtual {v4}, Ldbxyzptlk/db231222/i/d;->a()Ldbxyzptlk/db231222/i/c;

    move-result-object v4

    .line 108
    :try_start_0
    invoke-static {}, Lcom/dropbox/android/util/analytics/w;->a()Lcom/dropbox/android/util/analytics/w;

    move-result-object v5

    .line 109
    iget-object v6, p0, Ldbxyzptlk/db231222/g/as;->b:Ldbxyzptlk/db231222/z/M;

    iget-object v7, p0, Ldbxyzptlk/db231222/g/as;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v7}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v7

    iget-object v4, v4, Ldbxyzptlk/db231222/i/c;->a:Ljava/lang/String;

    invoke-virtual {v6, v7, v0, v3, v4}, Ldbxyzptlk/db231222/z/M;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/z/ai;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/g/as;->g:Ldbxyzptlk/db231222/z/ai;

    .line 111
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->ah()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    iget-object v3, p0, Ldbxyzptlk/db231222/g/as;->f:Lcom/dropbox/android/util/analytics/ChainInfo;

    invoke-virtual {v0, v3}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 116
    sget-object v0, Ldbxyzptlk/db231222/g/as;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Stream URL: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Ldbxyzptlk/db231222/g/as;->g:Ldbxyzptlk/db231222/z/ai;

    iget-object v4, v4, Ldbxyzptlk/db231222/z/ai;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Ldbxyzptlk/db231222/g/as;->g:Ldbxyzptlk/db231222/z/ai;

    iget-object v1, v0, Ldbxyzptlk/db231222/z/ai;->a:Ljava/lang/String;
    :try_end_0
    .catch Ldbxyzptlk/db231222/z/az; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    :cond_0
    :goto_1
    return-object v1

    .line 118
    :catch_0
    move-exception v0

    .line 119
    sget-object v0, Ldbxyzptlk/db231222/g/as;->a:Ljava/lang/String;

    const-string v3, "Transcoding failed on the server, falling back."

    invoke-static {v0, v3}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    iput-object v1, p0, Ldbxyzptlk/db231222/g/as;->g:Ldbxyzptlk/db231222/z/ai;

    .line 124
    iget-boolean v0, p0, Ldbxyzptlk/db231222/g/as;->d:Z

    if-eqz v0, :cond_0

    .line 125
    invoke-static {}, Lcom/dropbox/android/util/analytics/w;->a()Lcom/dropbox/android/util/analytics/w;

    move-result-object v0

    .line 126
    iget-object v1, p0, Ldbxyzptlk/db231222/g/as;->b:Ldbxyzptlk/db231222/z/M;

    iget-object v3, p0, Ldbxyzptlk/db231222/g/as;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Ldbxyzptlk/db231222/z/M;->a(Lcom/dropbox/android/util/DropboxPath;Z)Ldbxyzptlk/db231222/v/i;

    move-result-object v1

    iget-object v1, v1, Ldbxyzptlk/db231222/v/i;->a:Ljava/lang/String;

    .line 127
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->ag()Lcom/dropbox/android/util/analytics/l;

    move-result-object v3

    iget-object v4, p0, Ldbxyzptlk/db231222/g/as;->f:Lcom/dropbox/android/util/analytics/ChainInfo;

    invoke-virtual {v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto/16 :goto_0
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 67
    move-object v0, p1

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->b(Landroid/support/v4/app/FragmentManager;)V

    .line 69
    const v0, 0x7f0d00c8

    invoke-static {p1, v0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    .line 72
    sget-object v0, Ldbxyzptlk/db231222/g/as;->a:Ljava/lang/String;

    const-string v1, "Error in TranscodeAsyncTask"

    invoke-static {v0, v1, p2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 73
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 74
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 38
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/as;->a(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 139
    move-object v0, p1

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->b(Landroid/support/v4/app/FragmentManager;)V

    .line 140
    if-eqz p2, :cond_4

    .line 142
    iget-object v0, p0, Ldbxyzptlk/db231222/g/as;->g:Ldbxyzptlk/db231222/z/ai;

    if-eqz v0, :cond_3

    .line 143
    sget-object v0, Ldbxyzptlk/db231222/g/as;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Container="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/db231222/g/as;->g:Ldbxyzptlk/db231222/z/ai;

    iget-object v2, v2, Ldbxyzptlk/db231222/z/ai;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", canSeek="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/db231222/g/as;->g:Ldbxyzptlk/db231222/z/ai;

    iget-boolean v2, v2, Ldbxyzptlk/db231222/z/ai;->e:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 145
    const-string v1, "EXTRA_CONTAINER"

    iget-object v2, p0, Ldbxyzptlk/db231222/g/as;->g:Ldbxyzptlk/db231222/z/ai;

    iget-object v2, v2, Ldbxyzptlk/db231222/z/ai;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 146
    const-string v1, "EXTRA_CAN_SEEK"

    iget-object v2, p0, Ldbxyzptlk/db231222/g/as;->g:Ldbxyzptlk/db231222/z/ai;

    iget-boolean v2, v2, Ldbxyzptlk/db231222/z/ai;->e:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 147
    iget-object v1, p0, Ldbxyzptlk/db231222/g/as;->g:Ldbxyzptlk/db231222/z/ai;

    iget-object v1, v1, Ldbxyzptlk/db231222/z/ai;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 148
    const-string v1, "EXTRA_METADATA_URL"

    iget-object v2, p0, Ldbxyzptlk/db231222/g/as;->g:Ldbxyzptlk/db231222/z/ai;

    iget-object v2, v2, Ldbxyzptlk/db231222/z/ai;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    :goto_0
    iget-object v1, p0, Ldbxyzptlk/db231222/g/as;->g:Ldbxyzptlk/db231222/z/ai;

    iget-object v1, v1, Ldbxyzptlk/db231222/z/ai;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 153
    const-string v1, "EXTRA_PROGRESS_URL"

    iget-object v2, p0, Ldbxyzptlk/db231222/g/as;->g:Ldbxyzptlk/db231222/z/ai;

    iget-object v2, v2, Ldbxyzptlk/db231222/z/ai;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 157
    :goto_1
    iget-object v1, p0, Ldbxyzptlk/db231222/g/as;->f:Lcom/dropbox/android/util/analytics/ChainInfo;

    if-eqz v1, :cond_0

    .line 158
    const-string v1, "EXTRA_ANALYTICS_CHAIN_ID"

    iget-object v2, p0, Ldbxyzptlk/db231222/g/as;->f:Lcom/dropbox/android/util/analytics/ChainInfo;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 166
    :cond_0
    :goto_2
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/db231222/g/as;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 169
    :try_start_0
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    :goto_3
    return-void

    .line 150
    :cond_1
    sget-object v1, Ldbxyzptlk/db231222/g/as;->a:Ljava/lang/String;

    const-string v2, "No Metadata URL."

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 155
    :cond_2
    sget-object v1, Ldbxyzptlk/db231222/g/as;->a:Ljava/lang/String;

    const-string v2, "No progress URL."

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 161
    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 170
    :catch_0
    move-exception v0

    .line 171
    iget-object v0, p0, Ldbxyzptlk/db231222/g/as;->e:Ldbxyzptlk/db231222/g/ar;

    invoke-interface {v0}, Ldbxyzptlk/db231222/g/ar;->a()V

    goto :goto_3

    .line 174
    :cond_4
    iget-object v0, p0, Ldbxyzptlk/db231222/g/as;->e:Ldbxyzptlk/db231222/g/ar;

    invoke-interface {v0}, Ldbxyzptlk/db231222/g/ar;->a()V

    goto :goto_3
.end method
