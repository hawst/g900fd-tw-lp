.class public final Ldbxyzptlk/db231222/g/B;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ldbxyzptlk/db231222/g/E;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ldbxyzptlk/db231222/g/F;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ldbxyzptlk/db231222/g/F;)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 71
    iput-object p2, p0, Ldbxyzptlk/db231222/g/B;->a:Ljava/lang/String;

    .line 72
    iput-object p3, p0, Ldbxyzptlk/db231222/g/B;->b:Ldbxyzptlk/db231222/g/F;

    .line 73
    return-void
.end method


# virtual methods
.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/db231222/g/E;
    .locals 3

    .prologue
    .line 78
    :try_start_0
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    .line 80
    iget-object v1, p0, Ldbxyzptlk/db231222/g/B;->b:Ldbxyzptlk/db231222/g/F;

    sget-object v2, Ldbxyzptlk/db231222/g/F;->a:Ldbxyzptlk/db231222/g/F;

    if-ne v1, v2, :cond_0

    .line 81
    iget-object v1, p0, Ldbxyzptlk/db231222/g/B;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/a;->d(Ljava/lang/String;)Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    move-object v1, v0

    .line 86
    :goto_0
    invoke-static {v1}, Ldbxyzptlk/db231222/g/r;->a(Ldbxyzptlk/db231222/r/d;)V

    .line 88
    new-instance v0, Ldbxyzptlk/db231222/g/E;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/g/E;-><init>(Ldbxyzptlk/db231222/r/d;)V

    .line 100
    :goto_1
    return-object v0

    .line 83
    :cond_0
    iget-object v1, p0, Ldbxyzptlk/db231222/g/B;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/a;->e(Ljava/lang/String;)Ldbxyzptlk/db231222/r/d;
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    new-instance v0, Ldbxyzptlk/db231222/g/E;

    sget-object v1, Ldbxyzptlk/db231222/g/D;->b:Ldbxyzptlk/db231222/g/D;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/g/E;-><init>(Ldbxyzptlk/db231222/g/D;)V

    goto :goto_1

    .line 91
    :catch_1
    move-exception v0

    .line 92
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 93
    iget v0, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v1, 0x190

    if-ne v0, v1, :cond_1

    .line 94
    new-instance v0, Ldbxyzptlk/db231222/g/E;

    sget-object v1, Ldbxyzptlk/db231222/g/D;->a:Ldbxyzptlk/db231222/g/D;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/g/E;-><init>(Ldbxyzptlk/db231222/g/D;)V

    goto :goto_1

    .line 96
    :cond_1
    new-instance v0, Ldbxyzptlk/db231222/g/E;

    sget-object v1, Ldbxyzptlk/db231222/g/D;->c:Ldbxyzptlk/db231222/g/D;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/g/E;-><init>(Ldbxyzptlk/db231222/g/D;)V

    goto :goto_1

    .line 98
    :catch_2
    move-exception v0

    .line 99
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 100
    new-instance v0, Ldbxyzptlk/db231222/g/E;

    sget-object v1, Ldbxyzptlk/db231222/g/D;->c:Ldbxyzptlk/db231222/g/D;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/g/E;-><init>(Ldbxyzptlk/db231222/g/D;)V

    goto :goto_1
.end method

.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/B;->a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/db231222/g/E;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Ldbxyzptlk/db231222/g/E;)V
    .locals 1

    .prologue
    .line 106
    instance-of v0, p1, Ldbxyzptlk/db231222/g/C;

    if-nez v0, :cond_0

    .line 116
    :goto_0
    return-void

    .line 110
    :cond_0
    check-cast p1, Ldbxyzptlk/db231222/g/C;

    .line 111
    iget-object v0, p2, Ldbxyzptlk/db231222/g/E;->a:Ldbxyzptlk/db231222/r/d;

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p2, Ldbxyzptlk/db231222/g/E;->a:Ldbxyzptlk/db231222/r/d;

    invoke-interface {p1, v0}, Ldbxyzptlk/db231222/g/C;->b(Ldbxyzptlk/db231222/r/d;)V

    goto :goto_0

    .line 114
    :cond_1
    iget-object v0, p2, Ldbxyzptlk/db231222/g/E;->b:Ldbxyzptlk/db231222/g/D;

    invoke-interface {p1, v0}, Ldbxyzptlk/db231222/g/C;->a(Ldbxyzptlk/db231222/g/D;)V

    goto :goto_0
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 121
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 122
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p2, Ldbxyzptlk/db231222/g/E;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/B;->a(Landroid/content/Context;Ldbxyzptlk/db231222/g/E;)V

    return-void
.end method
