.class public Ldbxyzptlk/db231222/g/d;
.super Ldbxyzptlk/db231222/g/i;
.source "panda.py"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Ldbxyzptlk/db231222/g/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/g/d;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Ldbxyzptlk/db231222/g/i;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 49
    iput-object p4, p0, Ldbxyzptlk/db231222/g/d;->c:Landroid/content/Intent;

    .line 50
    return-void
.end method

.method private static a(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 117
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 118
    if-nez v1, :cond_1

    .line 127
    :cond_0
    :goto_0
    return-object v0

    .line 121
    :cond_1
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ldbxyzptlk/db231222/g/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 122
    if-eqz v1, :cond_0

    .line 125
    invoke-virtual {p0}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 126
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private static a(Ljava/io/File;Lcom/dropbox/android/filemanager/LocalEntry;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 319
    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 320
    invoke-static {p0}, Ldbxyzptlk/db231222/g/d;->b(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 321
    if-eqz v0, :cond_1

    .line 336
    :cond_0
    :goto_0
    return-object v0

    .line 324
    :cond_1
    sget-object v0, Ldbxyzptlk/db231222/g/d;->b:Ljava/lang/String;

    const-string v1, "Url file didn\'t have url"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    :cond_2
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 328
    :cond_3
    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 329
    invoke-static {p0}, Ldbxyzptlk/db231222/g/d;->c(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 330
    if-nez v0, :cond_0

    .line 334
    sget-object v0, Ldbxyzptlk/db231222/g/d;->b:Ljava/lang/String;

    const-string v1, "Webloc file didn\'t have url"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Ldbxyzptlk/db231222/g/d;->b:Ljava/lang/String;

    return-object v0
.end method

.method private static a(Landroid/content/pm/ResolveInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v5, -0x1

    const/16 v4, 0x2e

    .line 83
    invoke-virtual {p0, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 84
    if-ne v2, v5, :cond_1

    .line 102
    :cond_0
    :goto_0
    return-object v0

    .line 87
    :cond_1
    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-eq v3, v5, :cond_0

    .line 90
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move v0, v1

    .line 91
    :goto_1
    if-ge v0, v2, :cond_3

    .line 92
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v4, :cond_2

    .line 99
    const/16 v1, 0x5f

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 91
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 102
    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Landroid/content/pm/PackageManager;Landroid/content/Intent;Landroid/content/Intent;)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/PackageManager;",
            "Landroid/content/Intent;",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/high16 v0, 0x10000

    .line 63
    invoke-virtual {p0, p2, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 65
    invoke-virtual {p0, p1, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 67
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 68
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 69
    invoke-static {v0}, Ldbxyzptlk/db231222/g/d;->a(Landroid/content/pm/ResolveInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 71
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 72
    invoke-static {v0}, Ldbxyzptlk/db231222/g/d;->a(Landroid/content/pm/ResolveInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 74
    :cond_1
    return-object v2
.end method

.method static synthetic a(Lcom/dropbox/android/activity/lock/LockReceiver;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 40
    invoke-static {p0, p1, p2}, Ldbxyzptlk/db231222/g/d;->d(Lcom/dropbox/android/activity/lock/LockReceiver;Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method private static b(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 141
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "file"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {p0}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 146
    const-string v1, "http://www.example.com/example"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 149
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/io/File;)Landroid/net/Uri;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 341
    .line 343
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 344
    new-instance v3, Ljava/io/InputStreamReader;

    invoke-direct {v3, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 345
    new-instance v2, Ljava/io/BufferedReader;

    const/16 v1, 0x2000

    invoke-direct {v2, v3, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 348
    :cond_0
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 349
    const-string v3, "URL"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 350
    const-string v3, "="

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 351
    const/4 v4, -0x1

    if-le v3, v4, :cond_0

    .line 352
    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 353
    if-eqz v1, :cond_1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 360
    :cond_1
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/Reader;)V

    .line 362
    :goto_0
    return-object v0

    .line 360
    :cond_2
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/Reader;)V

    goto :goto_0

    .line 357
    :catch_0
    move-exception v1

    move-object v2, v0

    .line 358
    :goto_1
    :try_start_2
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v3

    invoke-virtual {v3, v1}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 360
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/Reader;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_2
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/Reader;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 357
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method static synthetic b(Lcom/dropbox/android/activity/lock/LockReceiver;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 40
    invoke-static {p0, p1, p2}, Ldbxyzptlk/db231222/g/d;->c(Lcom/dropbox/android/activity/lock/LockReceiver;Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method private static c(Ljava/io/File;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 368
    :try_start_0
    invoke-static {p0}, Ldbxyzptlk/db231222/f/l;->a(Ljava/io/File;)Ldbxyzptlk/db231222/f/i;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/f/g;

    .line 369
    const-string v1, "URL"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/f/g;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/f/i;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/f/k;

    .line 370
    invoke-virtual {v0}, Ldbxyzptlk/db231222/f/k;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 374
    :goto_0
    return-object v0

    .line 371
    :catch_0
    move-exception v0

    .line 372
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 374
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Lcom/dropbox/android/activity/lock/LockReceiver;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 153
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 154
    new-instance v1, Ldbxyzptlk/db231222/g/e;

    invoke-direct {v1, p2, p0, v0}, Ldbxyzptlk/db231222/g/e;-><init>(Landroid/content/Intent;Lcom/dropbox/android/activity/lock/LockReceiver;Landroid/content/Context;)V

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/lock/LockReceiver;->a(Ljava/lang/Runnable;)V

    .line 163
    return-void
.end method

.method private static d(Lcom/dropbox/android/activity/lock/LockReceiver;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 173
    :try_start_0
    invoke-virtual {p0, p2}, Lcom/dropbox/android/activity/lock/LockReceiver;->a(Landroid/content/Intent;)V

    .line 174
    invoke-virtual {p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 180
    :goto_0
    return-void

    .line 175
    :catch_0
    move-exception v0

    .line 176
    const v0, 0x7f0d005f

    invoke-static {p1, v0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 177
    :catch_1
    move-exception v0

    .line 178
    const v0, 0x7f0d0084

    invoke-static {p1, v0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method private static e(Lcom/dropbox/android/activity/lock/LockReceiver;Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 210
    invoke-static {p2}, Ldbxyzptlk/db231222/g/d;->b(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v2

    .line 214
    invoke-static {p2}, Ldbxyzptlk/db231222/g/d;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v4

    .line 215
    if-eqz v4, :cond_3

    .line 217
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, v4, p2}, Ldbxyzptlk/db231222/g/d;->a(Landroid/content/pm/PackageManager;Landroid/content/Intent;Landroid/content/Intent;)Ljava/util/Set;

    move-result-object v0

    .line 219
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v3

    if-lez v3, :cond_3

    .line 221
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->i()Lcom/dropbox/android/util/analytics/l;

    move-result-object v3

    const-string v5, "mime"

    invoke-virtual {p2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v5, v7}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 222
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 225
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->P()Lcom/dropbox/android/util/analytics/l;

    move-result-object v5

    const-string v7, "package.name"

    invoke-virtual {v5, v7, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v5, "mime"

    invoke-virtual {p2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v5, v7}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v5

    const-string v7, "extension"

    invoke-virtual {p2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->q(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v7, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_0

    :cond_0
    move v0, v6

    .line 233
    :goto_1
    if-nez v0, :cond_1

    if-nez v2, :cond_1

    move v0, v1

    .line 276
    :goto_2
    return v0

    .line 238
    :cond_1
    if-eqz v2, :cond_2

    .line 244
    const/4 v0, 0x2

    new-array v3, v0, [Landroid/content/Intent;

    aput-object v2, v3, v1

    aput-object v4, v3, v6

    .line 249
    :goto_3
    new-instance v0, Lcom/dropbox/android/widget/aU;

    const v1, 0x7f0d009c

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    sget-object v5, Lcom/dropbox/android/util/Activities;->a:Lcom/dropbox/android/util/c;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/widget/aU;-><init>(Landroid/content/Context;Ljava/lang/String;[Landroid/content/Intent;[Landroid/content/Intent;Lcom/dropbox/android/util/c;)V

    .line 252
    new-instance v1, Ldbxyzptlk/db231222/g/f;

    invoke-direct {v1, p2, p0, p1}, Ldbxyzptlk/db231222/g/f;-><init>(Landroid/content/Intent;Lcom/dropbox/android/activity/lock/LockReceiver;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aU;->a(Lcom/dropbox/android/widget/aX;)V

    .line 269
    new-instance v1, Ldbxyzptlk/db231222/g/g;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/g/g;-><init>(Lcom/dropbox/android/widget/aU;)V

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/lock/LockReceiver;->a(Ljava/lang/Runnable;)V

    move v0, v6

    .line 276
    goto :goto_2

    .line 246
    :cond_2
    new-array v3, v6, [Landroid/content/Intent;

    aput-object v4, v3, v1

    goto :goto_3

    :cond_3
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/io/File;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 282
    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/dropbox/android/filemanager/LocalEntry;->a(Ljava/lang/String;)V

    .line 283
    invoke-static {p1, p2}, Ldbxyzptlk/db231222/g/d;->a(Ljava/io/File;Lcom/dropbox/android/filemanager/LocalEntry;)Landroid/net/Uri;

    move-result-object v0

    .line 285
    invoke-static {}, Lcom/dropbox/android/activity/lock/LockReceiver;->a()Lcom/dropbox/android/activity/lock/LockReceiver;

    move-result-object v1

    .line 286
    if-eqz v0, :cond_0

    .line 287
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 288
    invoke-static {v1, p3, v2}, Ldbxyzptlk/db231222/g/d;->c(Lcom/dropbox/android/activity/lock/LockReceiver;Landroid/content/Context;Landroid/content/Intent;)V

    .line 289
    const-string v0, "view.success"

    invoke-static {v0, p2}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 309
    :goto_0
    return-void

    .line 291
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/g/d;->c:Landroid/content/Intent;

    .line 292
    if-nez v0, :cond_1

    .line 293
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 294
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 295
    const v2, 0x10000003

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 297
    const-string v2, "android.intent.extra.TITLE"

    const v3, 0x7f0d009c

    invoke-virtual {p3, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 299
    :cond_1
    invoke-static {v0, p2}, Lcom/dropbox/android/util/Activities;->a(Landroid/content/Intent;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 300
    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->g()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 301
    const-string v2, "CHARACTER_SET"

    invoke-virtual {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 304
    :cond_2
    invoke-static {v1, p3, v0}, Ldbxyzptlk/db231222/g/d;->e(Lcom/dropbox/android/activity/lock/LockReceiver;Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 305
    invoke-static {v1, p3, v0}, Ldbxyzptlk/db231222/g/d;->c(Lcom/dropbox/android/activity/lock/LockReceiver;Landroid/content/Context;Landroid/content/Intent;)V

    .line 307
    :cond_3
    const-string v0, "view.success"

    invoke-static {v0, p2}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_0
.end method
