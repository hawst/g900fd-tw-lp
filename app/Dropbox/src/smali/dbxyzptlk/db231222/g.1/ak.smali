.class public Ldbxyzptlk/db231222/g/ak;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Long;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/dropbox/android/albums/PhotosModel;

.field private final c:Lcom/dropbox/android/albums/Album;

.field private final d:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Ldbxyzptlk/db231222/g/ak;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/g/ak;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/albums/Album;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 32
    iput-object p2, p0, Ldbxyzptlk/db231222/g/ak;->b:Lcom/dropbox/android/albums/PhotosModel;

    .line 33
    iput-object p3, p0, Ldbxyzptlk/db231222/g/ak;->c:Lcom/dropbox/android/albums/Album;

    .line 34
    iput-object p4, p0, Ldbxyzptlk/db231222/g/ak;->d:Landroid/content/Intent;

    .line 36
    invoke-virtual {p0}, Ldbxyzptlk/db231222/g/ak;->f()V

    .line 37
    const v0, 0x7f0d0280

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(I)Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;

    move-result-object v0

    .line 38
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 39
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/ak;->a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 53
    iget-object v0, p0, Ldbxyzptlk/db231222/g/ak;->c:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/Album;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Ldbxyzptlk/db231222/g/ak;->c:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/Album;->g()Ljava/lang/String;

    move-result-object v0

    .line 81
    :goto_0
    return-object v0

    .line 57
    :cond_0
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    .line 59
    iget-object v1, p0, Ldbxyzptlk/db231222/g/ak;->b:Lcom/dropbox/android/albums/PhotosModel;

    iget-object v2, p0, Ldbxyzptlk/db231222/g/ak;->c:Lcom/dropbox/android/albums/Album;

    new-instance v3, Ldbxyzptlk/db231222/g/al;

    invoke-direct {v3, p0, v0}, Ldbxyzptlk/db231222/g/al;-><init>(Ldbxyzptlk/db231222/g/ak;Ljava/util/concurrent/LinkedBlockingQueue;)V

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/albums/PhotosModel;->a(Lcom/dropbox/android/albums/Album;Lcom/dropbox/android/albums/s;)V

    .line 74
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->take()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 78
    instance-of v1, v0, Lcom/dropbox/android/taskqueue/w;

    if-eqz v1, :cond_1

    .line 79
    new-instance v1, Ldbxyzptlk/db231222/w/a;

    check-cast v0, Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/w;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/w/a;-><init>(Ljava/lang/String;)V

    throw v1

    .line 75
    :catch_0
    move-exception v0

    .line 76
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Result polling interrupted!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_1
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 43
    move-object v0, p1

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->b(Landroid/support/v4/app/FragmentManager;)V

    .line 45
    const v0, 0x7f0d0281

    invoke-static {p1, v0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    .line 48
    sget-object v0, Ldbxyzptlk/db231222/g/ak;->a:Ljava/lang/String;

    const-string v1, "Error in ShareAlbumAsyncTask"

    invoke-static {v0, v1, p2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 49
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/ak;->a(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 86
    move-object v0, p1

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->b(Landroid/support/v4/app/FragmentManager;)V

    .line 87
    if-eqz p2, :cond_0

    .line 89
    iget-object v0, p0, Ldbxyzptlk/db231222/g/ak;->d:Landroid/content/Intent;

    iget-object v1, p0, Ldbxyzptlk/db231222/g/ak;->c:Lcom/dropbox/android/albums/Album;

    iget-object v2, p0, Ldbxyzptlk/db231222/g/ak;->c:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v2}, Lcom/dropbox/android/albums/Album;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, p2, v2}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->a(Landroid/content/Context;Landroid/content/Intent;Lcom/dropbox/android/albums/Album;Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Ldbxyzptlk/db231222/g/ak;->c:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/Album;->h()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 95
    :goto_0
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aM()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "id"

    iget-object v3, p0, Ldbxyzptlk/db231222/g/ak;->c:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v3}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "num.items"

    iget-object v3, p0, Ldbxyzptlk/db231222/g/ak;->c:Lcom/dropbox/android/albums/Album;

    invoke-virtual {v3}, Lcom/dropbox/android/albums/Album;->c()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "component.shared.to"

    iget-object v3, p0, Ldbxyzptlk/db231222/g/ak;->d:Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "create"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 102
    :cond_0
    return-void

    .line 94
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
