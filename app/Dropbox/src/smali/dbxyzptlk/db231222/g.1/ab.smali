.class public Ldbxyzptlk/db231222/g/ab;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ldbxyzptlk/db231222/z/aA;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ldbxyzptlk/db231222/r/d;

.field private final c:Ldbxyzptlk/db231222/g/ac;

.field private final d:Lcom/dropbox/android/util/analytics/r;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Ldbxyzptlk/db231222/g/ab;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/g/ab;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Ldbxyzptlk/db231222/g/ac;Lcom/dropbox/android/util/analytics/r;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 37
    iput-object p3, p0, Ldbxyzptlk/db231222/g/ab;->c:Ldbxyzptlk/db231222/g/ac;

    .line 38
    iput-object p2, p0, Ldbxyzptlk/db231222/g/ab;->b:Ldbxyzptlk/db231222/r/d;

    .line 39
    iput-object p4, p0, Ldbxyzptlk/db231222/g/ab;->d:Lcom/dropbox/android/util/analytics/r;

    .line 40
    invoke-virtual {p0}, Ldbxyzptlk/db231222/g/ab;->f()V

    .line 41
    return-void
.end method


# virtual methods
.method protected final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/db231222/z/aA;
    .locals 3

    .prologue
    .line 45
    iget-object v0, p0, Ldbxyzptlk/db231222/g/ab;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 49
    :try_start_0
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    .line 50
    iget-object v1, p0, Ldbxyzptlk/db231222/g/ab;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/service/e;->a:Lcom/dropbox/android/service/e;

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/service/a;->a(Lcom/dropbox/android/service/e;Ldbxyzptlk/db231222/r/e;)Ldbxyzptlk/db231222/s/a;
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    :cond_0
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/i/d;->a()Ldbxyzptlk/db231222/i/c;

    move-result-object v0

    .line 58
    iget-object v1, p0, Ldbxyzptlk/db231222/g/ab;->b:Ldbxyzptlk/db231222/r/d;

    invoke-static {p1, v0, v1}, Ldbxyzptlk/db231222/z/B;->a(Landroid/content/Context;Ldbxyzptlk/db231222/i/c;Ldbxyzptlk/db231222/r/d;)Ldbxyzptlk/db231222/z/B;

    move-result-object v0

    .line 60
    const-string v1, "start"

    invoke-static {v1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 61
    iget-object v1, p0, Ldbxyzptlk/db231222/g/ab;->b:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->u()Ldbxyzptlk/db231222/z/M;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/z/M;->a(Ldbxyzptlk/db231222/z/B;)Ldbxyzptlk/db231222/z/av;

    move-result-object v0

    .line 62
    iget-object v1, p0, Ldbxyzptlk/db231222/g/ab;->d:Lcom/dropbox/android/util/analytics/r;

    iget-object v2, v0, Ldbxyzptlk/db231222/z/av;->b:Ldbxyzptlk/db231222/t/e;

    invoke-virtual {v1, v2}, Lcom/dropbox/android/util/analytics/r;->a(Ldbxyzptlk/db231222/t/e;)V

    .line 63
    const-string v1, "success"

    invoke-static {v1}, Lcom/dropbox/android/util/analytics/a;->a(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 65
    iget-object v0, v0, Ldbxyzptlk/db231222/z/av;->a:Ldbxyzptlk/db231222/z/aA;

    :goto_0
    return-object v0

    .line 51
    :catch_0
    move-exception v0

    .line 52
    iget-object v0, p0, Ldbxyzptlk/db231222/g/ab;->b:Ldbxyzptlk/db231222/r/d;

    invoke-static {v0}, Lcom/dropbox/android/util/Activities;->a(Ldbxyzptlk/db231222/r/d;)V

    .line 53
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/ab;->a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/db231222/z/aA;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Ldbxyzptlk/db231222/z/aA;)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Ldbxyzptlk/db231222/g/ab;->c:Ldbxyzptlk/db231222/g/ac;

    if-eqz v0, :cond_0

    .line 78
    if-nez p2, :cond_1

    .line 79
    iget-object v0, p0, Ldbxyzptlk/db231222/g/ab;->c:Ldbxyzptlk/db231222/g/ac;

    invoke-interface {v0}, Ldbxyzptlk/db231222/g/ac;->a()V

    .line 84
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/g/ab;->c:Ldbxyzptlk/db231222/g/ac;

    invoke-interface {v0, p2}, Ldbxyzptlk/db231222/g/ac;->a(Ldbxyzptlk/db231222/z/aA;)V

    goto :goto_0
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Ldbxyzptlk/db231222/g/ab;->c:Ldbxyzptlk/db231222/g/ac;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Ldbxyzptlk/db231222/g/ab;->c:Ldbxyzptlk/db231222/g/ac;

    invoke-interface {v0, p2}, Ldbxyzptlk/db231222/g/ac;->a(Ljava/lang/Exception;)V

    .line 73
    :cond_0
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 20
    check-cast p2, Ldbxyzptlk/db231222/z/aA;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/ab;->a(Landroid/content/Context;Ldbxyzptlk/db231222/z/aA;)V

    return-void
.end method
