.class public Ldbxyzptlk/db231222/g/r;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ldbxyzptlk/db231222/g/a;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Z

.field private final g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Ldbxyzptlk/db231222/g/r;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/g/r;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 173
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 175
    iput-boolean p7, p0, Ldbxyzptlk/db231222/g/r;->f:Z

    .line 176
    iput-object p2, p0, Ldbxyzptlk/db231222/g/r;->b:Ljava/lang/String;

    .line 177
    iput-object p3, p0, Ldbxyzptlk/db231222/g/r;->c:Ljava/lang/String;

    .line 178
    iput-object p4, p0, Ldbxyzptlk/db231222/g/r;->d:Ljava/lang/String;

    .line 179
    iput-object p5, p0, Ldbxyzptlk/db231222/g/r;->e:Ljava/lang/String;

    .line 180
    iput-boolean p6, p0, Ldbxyzptlk/db231222/g/r;->g:Z

    .line 181
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 160
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 162
    iput-boolean p5, p0, Ldbxyzptlk/db231222/g/r;->f:Z

    .line 163
    iput-object p2, p0, Ldbxyzptlk/db231222/g/r;->b:Ljava/lang/String;

    .line 164
    iput-object p3, p0, Ldbxyzptlk/db231222/g/r;->c:Ljava/lang/String;

    .line 165
    iput-boolean p4, p0, Ldbxyzptlk/db231222/g/r;->g:Z

    .line 167
    iput-object v0, p0, Ldbxyzptlk/db231222/g/r;->d:Ljava/lang/String;

    .line 168
    iput-object v0, p0, Ldbxyzptlk/db231222/g/r;->e:Ljava/lang/String;

    .line 169
    return-void
.end method

.method private a(Landroid/content/Context;Z)Ldbxyzptlk/db231222/g/a;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const v5, 0x7f0d005d

    .line 215
    :try_start_0
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    .line 217
    if-eqz p2, :cond_1

    .line 218
    iget-object v1, p0, Ldbxyzptlk/db231222/g/r;->b:Ljava/lang/String;

    iget-object v2, p0, Ldbxyzptlk/db231222/g/r;->c:Ljava/lang/String;

    iget-object v3, p0, Ldbxyzptlk/db231222/g/r;->d:Ljava/lang/String;

    iget-object v4, p0, Ldbxyzptlk/db231222/g/r;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/dropbox/android/filemanager/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    move-object v1, v0

    .line 223
    :goto_0
    invoke-static {v1}, Ldbxyzptlk/db231222/g/r;->a(Ldbxyzptlk/db231222/r/d;)V

    .line 225
    iget-boolean v0, p0, Ldbxyzptlk/db231222/g/r;->g:Z

    if-eqz v0, :cond_0

    .line 228
    invoke-virtual {p0}, Ldbxyzptlk/db231222/g/r;->f()V

    .line 231
    :cond_0
    new-instance v0, Ldbxyzptlk/db231222/g/A;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/g/A;-><init>(Ldbxyzptlk/db231222/r/d;)V

    .line 266
    :goto_1
    return-object v0

    .line 220
    :cond_1
    iget-object v1, p0, Ldbxyzptlk/db231222/g/r;->b:Ljava/lang/String;

    iget-object v2, p0, Ldbxyzptlk/db231222/g/r;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/filemanager/a;->a(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/r/d;
    :try_end_0
    .catch Lcom/dropbox/android/filemanager/f; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/dropbox/android/filemanager/g; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_5

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 232
    :catch_0
    move-exception v0

    .line 233
    new-instance v0, Ldbxyzptlk/db231222/g/x;

    invoke-direct {v0, v6}, Ldbxyzptlk/db231222/g/x;-><init>(Ldbxyzptlk/db231222/g/s;)V

    goto :goto_1

    .line 234
    :catch_1
    move-exception v0

    .line 235
    new-instance v1, Ldbxyzptlk/db231222/g/y;

    iget-object v2, p0, Ldbxyzptlk/db231222/g/r;->b:Ljava/lang/String;

    iget-object v3, v0, Lcom/dropbox/android/filemanager/g;->a:Ldbxyzptlk/db231222/z/K;

    iget-object v0, p0, Ldbxyzptlk/db231222/g/r;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-direct {v1, v2, v3, v0}, Ldbxyzptlk/db231222/g/y;-><init>(Ljava/lang/String;Ldbxyzptlk/db231222/z/K;Z)V

    move-object v0, v1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 236
    :catch_2
    move-exception v0

    .line 238
    sget-object v1, Ldbxyzptlk/db231222/g/r;->a:Ljava/lang/String;

    const-string v2, "Error logging in"

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    iget-boolean v1, p0, Ldbxyzptlk/db231222/g/r;->f:Z

    if-eqz v1, :cond_3

    .line 240
    const v0, 0x7f0d002b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 241
    new-instance v0, Ldbxyzptlk/db231222/g/u;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/g/u;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 243
    :cond_3
    const v1, 0x7f0d002f

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/w/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 244
    new-instance v0, Ldbxyzptlk/db231222/g/v;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/g/v;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 246
    :catch_3
    move-exception v0

    .line 247
    sget-object v1, Ldbxyzptlk/db231222/g/r;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error logging in or creating new account: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x190

    if-ne v1, v2, :cond_5

    .line 250
    invoke-virtual {v0}, Ldbxyzptlk/db231222/w/i;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 251
    new-instance v0, Ldbxyzptlk/db231222/g/t;

    invoke-direct {v0, v6}, Ldbxyzptlk/db231222/g/t;-><init>(Ldbxyzptlk/db231222/g/s;)V

    goto :goto_1

    .line 253
    :cond_4
    const v0, 0x7f0d0043

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 259
    :goto_3
    new-instance v1, Ldbxyzptlk/db231222/g/u;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/g/u;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    goto/16 :goto_1

    .line 256
    :cond_5
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 257
    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/w/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 260
    :catch_4
    move-exception v0

    .line 261
    const v0, 0x7f0d005b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 262
    new-instance v0, Ldbxyzptlk/db231222/g/u;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/g/u;-><init>(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 263
    :catch_5
    move-exception v0

    .line 264
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 265
    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 266
    new-instance v0, Ldbxyzptlk/db231222/g/u;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/g/u;-><init>(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method static a(Ldbxyzptlk/db231222/r/d;)V
    .locals 1

    .prologue
    .line 283
    invoke-static {}, Lcom/dropbox/android/gcm/GcmSubscriber;->a()Lcom/dropbox/android/gcm/GcmSubscriber;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/dropbox/android/gcm/GcmSubscriber;->a(Ldbxyzptlk/db231222/r/d;)V

    .line 284
    return-void
.end method


# virtual methods
.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/db231222/g/a;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 191
    iget-object v0, p0, Ldbxyzptlk/db231222/g/r;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 192
    :goto_0
    invoke-direct {p0, p1, v0}, Ldbxyzptlk/db231222/g/r;->a(Landroid/content/Context;Z)Ldbxyzptlk/db231222/g/a;

    move-result-object v2

    .line 197
    if-eqz v0, :cond_2

    instance-of v0, v2, Ldbxyzptlk/db231222/g/t;

    if-eqz v0, :cond_2

    .line 198
    invoke-direct {p0, p1, v1}, Ldbxyzptlk/db231222/g/r;->a(Landroid/content/Context;Z)Ldbxyzptlk/db231222/g/a;

    move-result-object v0

    .line 199
    instance-of v1, v0, Ldbxyzptlk/db231222/g/v;

    if-eqz v1, :cond_0

    .line 203
    new-instance v0, Ldbxyzptlk/db231222/g/z;

    iget-object v1, p0, Ldbxyzptlk/db231222/g/r;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/g/z;-><init>(Ljava/lang/String;)V

    .line 210
    :cond_0
    :goto_1
    return-object v0

    :cond_1
    move v0, v1

    .line 191
    goto :goto_0

    :cond_2
    move-object v0, v2

    goto :goto_1
.end method

.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/r;->a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/db231222/g/a;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Ldbxyzptlk/db231222/g/a;)V
    .locals 0

    .prologue
    .line 273
    invoke-interface {p2, p1}, Ldbxyzptlk/db231222/g/a;->a(Landroid/content/Context;)V

    .line 274
    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 185
    sget-object v0, Ldbxyzptlk/db231222/g/r;->a:Ljava/lang/String;

    const-string v1, "Error in Logging in."

    invoke-static {v0, v1, p2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 186
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 187
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 26
    check-cast p2, Ldbxyzptlk/db231222/g/a;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/r;->a(Landroid/content/Context;Ldbxyzptlk/db231222/g/a;)V

    return-void
.end method
