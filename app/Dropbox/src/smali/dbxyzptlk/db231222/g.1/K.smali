.class public final Ldbxyzptlk/db231222/g/K;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Lcom/dropbox/android/taskqueue/w;

.field private final b:Lcom/dropbox/android/util/DropboxPath;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/taskqueue/w;Lcom/dropbox/android/util/DropboxPath;)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {p1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 25
    iput-object p1, p0, Ldbxyzptlk/db231222/g/K;->a:Lcom/dropbox/android/taskqueue/w;

    .line 26
    iget-object v0, p0, Ldbxyzptlk/db231222/g/K;->a:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/w;->c()Lcom/dropbox/android/taskqueue/y;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/y;->a:Lcom/dropbox/android/taskqueue/y;

    if-ne v0, v1, :cond_0

    .line 27
    invoke-static {p2}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 29
    :cond_0
    iput-object p2, p0, Ldbxyzptlk/db231222/g/K;->b:Lcom/dropbox/android/util/DropboxPath;

    .line 30
    return-void
.end method


# virtual methods
.method public final a()Lcom/dropbox/android/taskqueue/w;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Ldbxyzptlk/db231222/g/K;->a:Lcom/dropbox/android/taskqueue/w;

    return-object v0
.end method

.method public final b()Lcom/dropbox/android/util/DropboxPath;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Ldbxyzptlk/db231222/g/K;->b:Lcom/dropbox/android/util/DropboxPath;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "status: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/g/K;->a:Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " dest: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/g/K;->b:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
