.class public Ldbxyzptlk/db231222/g/b;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/dropbox/android/filemanager/I;

.field private final c:Lcom/dropbox/android/util/aE;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Ldbxyzptlk/db231222/g/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/g/b;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/util/aE;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 21
    iput-object p2, p0, Ldbxyzptlk/db231222/g/b;->b:Lcom/dropbox/android/filemanager/I;

    .line 22
    iput-object p3, p0, Ldbxyzptlk/db231222/g/b;->c:Lcom/dropbox/android/util/aE;

    .line 23
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/b;->a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1

    .prologue
    .line 27
    if-eqz p1, :cond_0

    .line 28
    iget-object v0, p0, Ldbxyzptlk/db231222/g/b;->b:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->c()V

    .line 30
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/g/b;->c:Lcom/dropbox/android/util/aE;

    invoke-virtual {v0}, Lcom/dropbox/android/util/aE;->c()V

    .line 31
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 48
    sget-object v0, Ldbxyzptlk/db231222/g/b;->a:Ljava/lang/String;

    const-string v1, "Error in clearing cache"

    invoke-static {v0, v1, p2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 49
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 50
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 13
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/b;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 3

    .prologue
    .line 36
    invoke-virtual {p0}, Ldbxyzptlk/db231222/g/b;->d()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/PrefsActivity;

    .line 38
    if-eqz v0, :cond_0

    .line 39
    const v1, 0x7f0d0156

    invoke-static {p1, v1}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    .line 41
    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/activity/PrefsActivity;->a(J)V

    .line 42
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/PrefsActivity;->setSupportProgressBarIndeterminateVisibility(Z)V

    .line 44
    :cond_0
    return-void
.end method
