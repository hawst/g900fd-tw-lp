.class public Ldbxyzptlk/db231222/g/J;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ldbxyzptlk/db231222/g/K;",
        ">;"
    }
.end annotation


# instance fields
.field protected a:Lcom/dropbox/android/filemanager/LocalEntry;

.field protected b:Lcom/dropbox/android/util/DropboxPath;

.field private final c:Lcom/dropbox/android/filemanager/I;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/util/DropboxPath;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 25
    invoke-virtual {p4}, Lcom/dropbox/android/util/DropboxPath;->e()Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 26
    iput-object p3, p0, Ldbxyzptlk/db231222/g/J;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 27
    iput-object p4, p0, Ldbxyzptlk/db231222/g/J;->b:Lcom/dropbox/android/util/DropboxPath;

    .line 28
    iput-object p2, p0, Ldbxyzptlk/db231222/g/J;->c:Lcom/dropbox/android/filemanager/I;

    .line 29
    return-void
.end method


# virtual methods
.method protected final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/db231222/g/K;
    .locals 3

    .prologue
    .line 33
    iget-object v0, p0, Ldbxyzptlk/db231222/g/J;->c:Lcom/dropbox/android/filemanager/I;

    iget-object v1, p0, Ldbxyzptlk/db231222/g/J;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v2, p0, Ldbxyzptlk/db231222/g/J;->b:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/util/DropboxPath;)Ldbxyzptlk/db231222/g/K;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/J;->a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/db231222/g/K;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/content/Context;Ldbxyzptlk/db231222/g/K;)V
    .locals 0

    .prologue
    .line 40
    return-void
.end method

.method protected a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 45
    return-void
.end method

.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 16
    check-cast p2, Ldbxyzptlk/db231222/g/K;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/J;->a(Landroid/content/Context;Ldbxyzptlk/db231222/g/K;)V

    return-void
.end method
