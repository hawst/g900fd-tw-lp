.class public final Ldbxyzptlk/db231222/g/X;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/dropbox/android/util/DropboxPath;

.field private final c:Z

.field private final d:Lcom/dropbox/android/filemanager/I;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLjava/util/Collection;Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/filemanager/I;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Z",
            "Ljava/util/Collection",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Lcom/dropbox/android/filemanager/I;",
            ")V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 34
    iput-object p3, p0, Ldbxyzptlk/db231222/g/X;->a:Ljava/util/Collection;

    .line 35
    iput-object p4, p0, Ldbxyzptlk/db231222/g/X;->b:Lcom/dropbox/android/util/DropboxPath;

    .line 36
    iput-boolean p2, p0, Ldbxyzptlk/db231222/g/X;->c:Z

    .line 37
    iput-object p5, p0, Ldbxyzptlk/db231222/g/X;->d:Lcom/dropbox/android/filemanager/I;

    .line 38
    instance-of v0, p1, Ldbxyzptlk/db231222/g/Z;

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "QueueUserUploadsAsyncTask context must implment QueueUserUploadsAsyncTask"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/g/X;->f()V

    .line 42
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/X;->a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5

    .prologue
    .line 51
    iget-object v0, p0, Ldbxyzptlk/db231222/g/X;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Ldbxyzptlk/db231222/g/X;->a:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 56
    iget-object v1, p0, Ldbxyzptlk/db231222/g/X;->a:Ljava/util/Collection;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 57
    new-instance v1, Ldbxyzptlk/db231222/g/Y;

    invoke-direct {v1, p0}, Ldbxyzptlk/db231222/g/Y;-><init>(Ldbxyzptlk/db231222/g/X;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 63
    iget-object v1, p0, Ldbxyzptlk/db231222/g/X;->d:Lcom/dropbox/android/filemanager/I;

    iget-object v2, p0, Ldbxyzptlk/db231222/g/X;->b:Lcom/dropbox/android/util/DropboxPath;

    iget-boolean v3, p0, Ldbxyzptlk/db231222/g/X;->c:Z

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/util/Collection;ZZ)V

    .line 68
    iput-object v0, p0, Ldbxyzptlk/db231222/g/X;->e:Ljava/util/List;

    .line 70
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 46
    invoke-static {}, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;->a()Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;

    move-result-object v0

    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 47
    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/X;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 2

    .prologue
    .line 75
    move-object v0, p1

    check-cast v0, Ldbxyzptlk/db231222/g/Z;

    .line 76
    iget-object v1, p0, Ldbxyzptlk/db231222/g/X;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ldbxyzptlk/db231222/g/Z;->a(Ljava/util/List;)V

    .line 77
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;->b(Landroid/support/v4/app/FragmentManager;)V

    .line 78
    return-void
.end method
