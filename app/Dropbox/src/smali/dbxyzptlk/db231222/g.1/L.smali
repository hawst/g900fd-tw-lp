.class public final Ldbxyzptlk/db231222/g/L;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/dropbox/android/taskqueue/w;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/dropbox/android/util/DropboxPath;

.field private final b:Ldbxyzptlk/db231222/g/N;

.field private final c:Lcom/dropbox/android/filemanager/I;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/I;Lcom/dropbox/android/util/DropboxPath;Ldbxyzptlk/db231222/g/N;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 32
    iput-object p3, p0, Ldbxyzptlk/db231222/g/L;->a:Lcom/dropbox/android/util/DropboxPath;

    .line 33
    iput-object p4, p0, Ldbxyzptlk/db231222/g/L;->b:Ldbxyzptlk/db231222/g/N;

    .line 34
    iput-object p2, p0, Ldbxyzptlk/db231222/g/L;->c:Lcom/dropbox/android/filemanager/I;

    .line 35
    return-void
.end method

.method private a(Lcom/dropbox/android/taskqueue/w;)Ldbxyzptlk/db231222/g/O;
    .locals 2

    .prologue
    .line 43
    sget-object v0, Ldbxyzptlk/db231222/g/M;->a:[I

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/w;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 52
    sget-object v0, Ldbxyzptlk/db231222/g/O;->d:Ldbxyzptlk/db231222/g/O;

    :goto_0
    return-object v0

    .line 45
    :pswitch_0
    sget-object v0, Ldbxyzptlk/db231222/g/O;->b:Ldbxyzptlk/db231222/g/O;

    goto :goto_0

    .line 48
    :pswitch_1
    sget-object v0, Ldbxyzptlk/db231222/g/O;->a:Ldbxyzptlk/db231222/g/O;

    goto :goto_0

    .line 50
    :pswitch_2
    sget-object v0, Ldbxyzptlk/db231222/g/O;->c:Ldbxyzptlk/db231222/g/O;

    goto :goto_0

    .line 43
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Lcom/dropbox/android/taskqueue/w;
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Ldbxyzptlk/db231222/g/L;->c:Lcom/dropbox/android/filemanager/I;

    iget-object v1, p0, Ldbxyzptlk/db231222/g/L;->a:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/L;->a(Landroid/content/Context;[Ljava/lang/Void;)Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Ldbxyzptlk/db231222/g/L;->b:Ldbxyzptlk/db231222/g/N;

    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    invoke-interface {v0, p1}, Ldbxyzptlk/db231222/g/N;->a(Landroid/support/v4/app/FragmentActivity;)V

    .line 59
    return-void
.end method

.method protected final a(Landroid/content/Context;Lcom/dropbox/android/taskqueue/w;)V
    .locals 2

    .prologue
    .line 63
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    .line 65
    iget-object v0, p0, Ldbxyzptlk/db231222/g/L;->b:Ldbxyzptlk/db231222/g/N;

    invoke-interface {v0, p1}, Ldbxyzptlk/db231222/g/N;->b(Landroid/support/v4/app/FragmentActivity;)V

    .line 67
    invoke-virtual {p2}, Lcom/dropbox/android/taskqueue/w;->c()Lcom/dropbox/android/taskqueue/y;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/y;->a:Lcom/dropbox/android/taskqueue/y;

    if-ne v0, v1, :cond_0

    .line 68
    iget-object v0, p0, Ldbxyzptlk/db231222/g/L;->b:Ldbxyzptlk/db231222/g/N;

    iget-object v1, p0, Ldbxyzptlk/db231222/g/L;->a:Lcom/dropbox/android/util/DropboxPath;

    invoke-interface {v0, p1, v1}, Ldbxyzptlk/db231222/g/N;->a(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/util/DropboxPath;)V

    .line 72
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/g/L;->b:Ldbxyzptlk/db231222/g/N;

    invoke-direct {p0, p2}, Ldbxyzptlk/db231222/g/L;->a(Lcom/dropbox/android/taskqueue/w;)Ldbxyzptlk/db231222/g/O;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ldbxyzptlk/db231222/g/N;->a(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/g/O;)V

    goto :goto_0
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Ldbxyzptlk/db231222/g/L;->b:Ldbxyzptlk/db231222/g/N;

    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    invoke-interface {v0, p1}, Ldbxyzptlk/db231222/g/N;->b(Landroid/support/v4/app/FragmentActivity;)V

    .line 78
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 11
    check-cast p2, Lcom/dropbox/android/taskqueue/w;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/L;->a(Landroid/content/Context;Lcom/dropbox/android/taskqueue/w;)V

    return-void
.end method
