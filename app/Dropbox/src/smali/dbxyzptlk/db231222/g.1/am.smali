.class public Ldbxyzptlk/db231222/g/am;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Long;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ldbxyzptlk/db231222/z/M;

.field private c:Lcom/dropbox/android/filemanager/LocalEntry;

.field private d:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Ldbxyzptlk/db231222/g/am;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/g/am;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 29
    iput-object p2, p0, Ldbxyzptlk/db231222/g/am;->b:Ldbxyzptlk/db231222/z/M;

    .line 30
    iput-object p3, p0, Ldbxyzptlk/db231222/g/am;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 31
    iput-object p4, p0, Ldbxyzptlk/db231222/g/am;->d:Landroid/content/Intent;

    .line 33
    invoke-virtual {p0}, Ldbxyzptlk/db231222/g/am;->f()V

    .line 34
    const v0, 0x7f0d00c6

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(I)Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;

    move-result-object v0

    .line 35
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 36
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/am;->a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Ldbxyzptlk/db231222/g/am;->b:Ldbxyzptlk/db231222/z/M;

    iget-object v1, p0, Ldbxyzptlk/db231222/g/am;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/z/M;->c(Lcom/dropbox/android/util/DropboxPath;)Ldbxyzptlk/db231222/v/i;

    move-result-object v0

    iget-object v0, v0, Ldbxyzptlk/db231222/v/i;->a:Ljava/lang/String;

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 40
    move-object v0, p1

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->b(Landroid/support/v4/app/FragmentManager;)V

    .line 42
    const v0, 0x7f0d00a0

    invoke-static {p1, v0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;I)V

    .line 45
    sget-object v0, Ldbxyzptlk/db231222/g/am;->a:Ljava/lang/String;

    const-string v1, "Error in SharingFileAsyncTask"

    invoke-static {v0, v1, p2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 46
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/am;->a(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 55
    move-object v0, p1

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->b(Landroid/support/v4/app/FragmentManager;)V

    .line 56
    if-eqz p2, :cond_0

    .line 57
    iget-object v0, p0, Ldbxyzptlk/db231222/g/am;->d:Landroid/content/Intent;

    iget-object v1, p0, Ldbxyzptlk/db231222/g/am;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, p2, v2}, Lcom/dropbox/android/activity/dialog/SharePickerDialogFragment;->a(Landroid/content/Context;Landroid/content/Intent;Lcom/dropbox/android/filemanager/LocalEntry;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aL()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "component.shared.to"

    iget-object v2, p0, Ldbxyzptlk/db231222/g/am;->d:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "mime"

    iget-object v2, p0, Ldbxyzptlk/db231222/g/am;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/LocalEntry;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "extension"

    iget-object v0, p0, Ldbxyzptlk/db231222/g/am;->c:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/ab;->q(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 64
    :cond_0
    return-void
.end method
