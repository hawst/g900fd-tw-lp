.class public abstract Ldbxyzptlk/db231222/g/i;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/taskqueue/v;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Long;",
        "Ljava/io/File;",
        ">;",
        "Lcom/dropbox/android/taskqueue/v;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field protected volatile a:Lcom/dropbox/android/taskqueue/w;

.field private volatile c:Z

.field private d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/dropbox/android/widget/ab;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private final f:Lcom/dropbox/android/taskqueue/DownloadTask;

.field private final g:Lcom/dropbox/android/filemanager/I;

.field private h:Z

.field private i:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Ldbxyzptlk/db231222/g/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/g/i;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 25
    iput-boolean v1, p0, Ldbxyzptlk/db231222/g/i;->c:Z

    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/db231222/g/i;->e:Z

    .line 38
    iput-boolean v1, p0, Ldbxyzptlk/db231222/g/i;->h:Z

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/g/i;->a:Lcom/dropbox/android/taskqueue/w;

    .line 50
    new-instance v0, Lcom/dropbox/android/taskqueue/DownloadTask;

    invoke-direct {v0, p2, p3}, Lcom/dropbox/android/taskqueue/DownloadTask;-><init>(Ldbxyzptlk/db231222/r/d;Lcom/dropbox/android/filemanager/LocalEntry;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/g/i;->f:Lcom/dropbox/android/taskqueue/DownloadTask;

    .line 51
    invoke-virtual {p2}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/g/i;->g:Lcom/dropbox/android/filemanager/I;

    .line 52
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Ldbxyzptlk/db231222/g/i;->d:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Ldbxyzptlk/db231222/g/i;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ab;

    .line 147
    if-eqz v0, :cond_0

    .line 148
    invoke-virtual {v0}, Lcom/dropbox/android/widget/ab;->dismiss()V

    .line 151
    :cond_0
    return-void
.end method


# virtual methods
.method protected varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/io/File;
    .locals 3

    .prologue
    .line 85
    iget-object v0, p0, Ldbxyzptlk/db231222/g/i;->f:Lcom/dropbox/android/taskqueue/DownloadTask;

    invoke-virtual {v0, p0}, Lcom/dropbox/android/taskqueue/DownloadTask;->a(Lcom/dropbox/android/taskqueue/v;)V

    .line 86
    iget-object v0, p0, Ldbxyzptlk/db231222/g/i;->f:Lcom/dropbox/android/taskqueue/DownloadTask;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->c()Lcom/dropbox/android/taskqueue/w;

    move-result-object v0

    .line 87
    iget-object v1, p0, Ldbxyzptlk/db231222/g/i;->f:Lcom/dropbox/android/taskqueue/DownloadTask;

    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/DownloadTask;->h()Ljava/io/File;

    move-result-object v1

    .line 88
    sget-object v2, Lcom/dropbox/android/taskqueue/w;->b:Lcom/dropbox/android/taskqueue/w;

    if-ne v0, v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Ldbxyzptlk/db231222/g/i;->g:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->j()Ldbxyzptlk/db231222/k/c;

    move-result-object v0

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/k/c;->b(Ljava/io/File;)V

    .line 92
    invoke-virtual {p0, v1}, Ldbxyzptlk/db231222/g/i;->a(Ljava/io/File;)V

    .line 94
    :cond_0
    return-object v1
.end method

.method protected bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/i;->a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Ljava/io/File;)V
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Ldbxyzptlk/db231222/g/i;->c:Z

    if-eqz v0, :cond_1

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 130
    iget-object v0, p0, Ldbxyzptlk/db231222/g/i;->f:Lcom/dropbox/android/taskqueue/DownloadTask;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->g()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    invoke-virtual {p0, p2, v0, p1}, Ldbxyzptlk/db231222/g/i;->a(Ljava/io/File;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/Context;)V

    .line 132
    iget-boolean v0, p0, Ldbxyzptlk/db231222/g/i;->e:Z

    if-eqz v0, :cond_0

    .line 133
    invoke-direct {p0}, Ldbxyzptlk/db231222/g/i;->a()V

    goto :goto_0

    .line 136
    :cond_2
    iget-object v0, p0, Ldbxyzptlk/db231222/g/i;->a:Lcom/dropbox/android/taskqueue/w;

    invoke-static {v0, p1}, Lcom/dropbox/android/util/UIHelpers;->a(Lcom/dropbox/android/taskqueue/w;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_3

    .line 138
    invoke-static {p1, v0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 140
    :cond_3
    invoke-direct {p0}, Ldbxyzptlk/db231222/g/i;->a()V

    goto :goto_0
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 68
    iget-object v0, p0, Ldbxyzptlk/db231222/g/i;->f:Lcom/dropbox/android/taskqueue/DownloadTask;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->g()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    .line 69
    if-eqz v0, :cond_0

    .line 70
    sget-object v1, Ldbxyzptlk/db231222/g/i;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Downloading file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " exception: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :goto_0
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 76
    return-void

    .line 73
    :cond_0
    sget-object v0, Ldbxyzptlk/db231222/g/i;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Downloading file, exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p2, Ljava/io/File;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/i;->a(Landroid/content/Context;Ljava/io/File;)V

    return-void
.end method

.method public final a(Lcom/dropbox/android/taskqueue/u;JJ)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 175
    iput-boolean v0, p0, Ldbxyzptlk/db231222/g/i;->h:Z

    .line 176
    new-array v0, v0, [Ljava/lang/Long;

    const/4 v1, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/g/i;->publishProgress([Ljava/lang/Object;)V

    .line 177
    iput-wide p4, p0, Ldbxyzptlk/db231222/g/i;->i:J

    .line 178
    return-void
.end method

.method public final a(Lcom/dropbox/android/widget/ab;)V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/g/i;->d:Ljava/lang/ref/WeakReference;

    .line 60
    return-void
.end method

.method protected a(Ljava/io/File;)V
    .locals 0

    .prologue
    .line 102
    return-void
.end method

.method protected abstract a(Ljava/io/File;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/Context;)V
.end method

.method protected final varargs a([Ljava/lang/Long;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const-wide/16 v4, 0x3e8

    .line 106
    iget-object v0, p0, Ldbxyzptlk/db231222/g/i;->d:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    .line 107
    iget-object v0, p0, Ldbxyzptlk/db231222/g/i;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ab;

    .line 108
    if-eqz v0, :cond_1

    .line 109
    iget-boolean v1, p0, Ldbxyzptlk/db231222/g/i;->h:Z

    if-eqz v1, :cond_0

    .line 110
    iput-boolean v6, p0, Ldbxyzptlk/db231222/g/i;->h:Z

    .line 111
    iget-object v1, p0, Ldbxyzptlk/db231222/g/i;->f:Lcom/dropbox/android/taskqueue/DownloadTask;

    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/DownloadTask;->g()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v1

    .line 113
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/LocalEntry;->c()J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 114
    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/LocalEntry;->c()J

    move-result-wide v1

    div-long/2addr v1, v4

    long-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/ab;->b(I)V

    .line 118
    :cond_0
    aget-object v1, p1, v6

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    div-long/2addr v1, v4

    long-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/ab;->a(I)V

    .line 121
    :cond_1
    return-void
.end method

.method public final b()Lcom/dropbox/android/filemanager/LocalEntry;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Ldbxyzptlk/db231222/g/i;->f:Lcom/dropbox/android/taskqueue/DownloadTask;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->g()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/dropbox/android/taskqueue/u;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 157
    iput-boolean v0, p0, Ldbxyzptlk/db231222/g/i;->h:Z

    .line 158
    new-array v0, v0, [Ljava/lang/Long;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/g/i;->publishProgress([Ljava/lang/Object;)V

    .line 159
    return-void
.end method

.method public final b(Lcom/dropbox/android/taskqueue/u;Lcom/dropbox/android/taskqueue/w;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 163
    iput-boolean v0, p0, Ldbxyzptlk/db231222/g/i;->h:Z

    .line 164
    iput-object p2, p0, Ldbxyzptlk/db231222/g/i;->a:Lcom/dropbox/android/taskqueue/w;

    .line 165
    new-array v0, v0, [Ljava/lang/Long;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/g/i;->publishProgress([Ljava/lang/Object;)V

    .line 166
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/db231222/g/i;->c:Z

    .line 80
    iget-object v0, p0, Ldbxyzptlk/db231222/g/i;->f:Lcom/dropbox/android/taskqueue/DownloadTask;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->f()V

    .line 81
    return-void
.end method

.method public final c(Lcom/dropbox/android/taskqueue/u;)V
    .locals 4

    .prologue
    .line 182
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Long;

    const/4 v1, 0x0

    iget-wide v2, p0, Ldbxyzptlk/db231222/g/i;->i:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/g/i;->publishProgress([Ljava/lang/Object;)V

    .line 183
    return-void
.end method

.method public final d(Lcom/dropbox/android/taskqueue/u;)V
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/db231222/g/i;->c:Z

    .line 171
    return-void
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/g/i;->a([Ljava/lang/Long;)V

    return-void
.end method
