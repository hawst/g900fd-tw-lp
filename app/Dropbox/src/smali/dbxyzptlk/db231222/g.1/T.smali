.class final Ldbxyzptlk/db231222/g/T;
.super Ldbxyzptlk/db231222/g/V;
.source "panda.py"


# instance fields
.field final synthetic a:Ldbxyzptlk/db231222/g/R;

.field private final c:Z

.field private final d:Ldbxyzptlk/db231222/n/P;

.field private final e:Lcom/dropbox/android/albums/PhotosModel;

.field private final f:Lcom/dropbox/android/provider/j;

.field private final g:Ldbxyzptlk/db231222/z/M;


# direct methods
.method constructor <init>(Ldbxyzptlk/db231222/g/R;ZLdbxyzptlk/db231222/n/P;Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;)V
    .locals 1

    .prologue
    .line 205
    iput-object p1, p0, Ldbxyzptlk/db231222/g/T;->a:Ldbxyzptlk/db231222/g/R;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ldbxyzptlk/db231222/g/V;-><init>(Ldbxyzptlk/db231222/g/R;Ldbxyzptlk/db231222/g/S;)V

    .line 206
    iput-boolean p2, p0, Ldbxyzptlk/db231222/g/T;->c:Z

    .line 207
    iput-object p3, p0, Ldbxyzptlk/db231222/g/T;->d:Ldbxyzptlk/db231222/n/P;

    .line 208
    iput-object p4, p0, Ldbxyzptlk/db231222/g/T;->e:Lcom/dropbox/android/albums/PhotosModel;

    .line 209
    iput-object p5, p0, Ldbxyzptlk/db231222/g/T;->f:Lcom/dropbox/android/provider/j;

    .line 210
    iput-object p6, p0, Ldbxyzptlk/db231222/g/T;->g:Ldbxyzptlk/db231222/z/M;

    .line 211
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 20

    .prologue
    .line 215
    move-object/from16 v0, p0

    iget-object v2, v0, Ldbxyzptlk/db231222/g/T;->d:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/n/P;->E()Ljava/lang/String;

    move-result-object v12

    .line 217
    const-wide/16 v10, 0x0

    .line 218
    const-wide/16 v8, 0x0

    .line 219
    const-wide/16 v6, 0x0

    .line 221
    invoke-static {}, Lcom/dropbox/android/util/analytics/w;->a()Lcom/dropbox/android/util/analytics/w;

    move-result-object v13

    .line 222
    const-wide/16 v4, 0x0

    .line 223
    const/4 v3, 0x1

    .line 224
    move-object/from16 v0, p0

    iget-boolean v2, v0, Ldbxyzptlk/db231222/g/T;->c:Z

    move/from16 v19, v2

    move v2, v3

    move/from16 v3, v19

    .line 225
    :cond_0
    :goto_0
    if-eqz v2, :cond_f

    .line 226
    invoke-static {}, Ldbxyzptlk/db231222/g/R;->b()Ljava/lang/String;

    move-result-object v2

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Requesting page: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-wide/16 v15, 0x1

    add-long/2addr v15, v6

    invoke-virtual/range {v14 .. v16}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v2, v14}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    if-eqz v12, :cond_1

    invoke-static {}, Lcom/dropbox/android/util/S;->c()I

    move-result v2

    const/16 v14, 0x30

    if-ge v2, v14, :cond_4

    :cond_1
    const/4 v2, 0x1

    .line 228
    :goto_1
    if-eqz v2, :cond_5

    const/16 v2, 0x1f4

    .line 229
    :goto_2
    invoke-static {}, Ldbxyzptlk/db231222/g/R;->b()Ljava/lang/String;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Blocking: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    if-nez v3, :cond_2

    if-nez v12, :cond_3

    .line 233
    :cond_2
    invoke-virtual/range {p0 .. p0}, Ldbxyzptlk/db231222/g/T;->d()V

    .line 236
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    .line 237
    move-object/from16 v0, p0

    iget-object v0, v0, Ldbxyzptlk/db231222/g/T;->g:Ldbxyzptlk/db231222/z/M;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v12, v2, v3}, Ldbxyzptlk/db231222/z/M;->a(Ljava/lang/String;IZ)Ldbxyzptlk/db231222/v/f;

    move-result-object v16

    .line 239
    const/4 v3, 0x0

    .line 240
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v17

    sub-long v14, v17, v14

    add-long/2addr v4, v14

    .line 241
    const-wide/16 v14, 0x1

    add-long/2addr v6, v14

    .line 243
    invoke-static {}, Ldbxyzptlk/db231222/g/R;->b()Ljava/lang/String;

    move-result-object v2

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Processing page: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v2, v14}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 247
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 248
    move-object/from16 v0, v16

    iget-object v2, v0, Ldbxyzptlk/db231222/v/f;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_3
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldbxyzptlk/db231222/v/d;

    .line 249
    iget-object v0, v2, Ldbxyzptlk/db231222/v/d;->b:Ljava/lang/Object;

    move-object/from16 v18, v0

    if-eqz v18, :cond_6

    .line 250
    iget-object v2, v2, Ldbxyzptlk/db231222/v/d;->b:Ljava/lang/Object;

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 227
    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 228
    :cond_5
    const/16 v2, 0x7d0

    goto/16 :goto_2

    .line 253
    :cond_6
    iget-object v2, v2, Ldbxyzptlk/db231222/v/d;->a:Ljava/lang/String;

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 256
    :cond_7
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v17, v0

    add-long v10, v10, v17

    .line 257
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v17, v0

    add-long v8, v8, v17

    .line 259
    invoke-virtual/range {p0 .. p0}, Ldbxyzptlk/db231222/g/T;->c()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 303
    :cond_8
    :goto_4
    return-void

    .line 263
    :cond_9
    move-object/from16 v0, v16

    iget-boolean v2, v0, Ldbxyzptlk/db231222/v/f;->d:Z

    if-eqz v2, :cond_a

    .line 264
    invoke-virtual/range {p0 .. p0}, Ldbxyzptlk/db231222/g/T;->d()V

    .line 270
    :cond_a
    if-eqz v12, :cond_b

    move-object/from16 v0, v16

    iget-boolean v2, v0, Ldbxyzptlk/db231222/v/f;->a:Z

    if-eqz v2, :cond_e

    :cond_b
    const/4 v2, 0x1

    .line 271
    :goto_5
    move-object/from16 v0, p0

    iget-object v12, v0, Ldbxyzptlk/db231222/g/T;->f:Lcom/dropbox/android/provider/j;

    invoke-static {v12, v15, v14, v2}, Lcom/dropbox/android/provider/PhotosProvider;->a(Lcom/dropbox/android/provider/j;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 272
    move-object/from16 v0, p0

    iget-object v2, v0, Ldbxyzptlk/db231222/g/T;->a:Ldbxyzptlk/db231222/g/R;

    invoke-static {v2}, Ldbxyzptlk/db231222/g/R;->c(Ldbxyzptlk/db231222/g/R;)Lcom/dropbox/android/albums/PhotosModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/albums/PhotosModel;->i()V

    .line 275
    :cond_c
    move-object/from16 v0, v16

    iget-boolean v2, v0, Ldbxyzptlk/db231222/v/f;->d:Z

    .line 276
    move-object/from16 v0, v16

    iget-object v12, v0, Ldbxyzptlk/db231222/v/f;->b:Ljava/lang/String;

    .line 278
    invoke-virtual/range {p0 .. p0}, Ldbxyzptlk/db231222/g/T;->c()Z

    move-result v14

    if-nez v14, :cond_8

    .line 283
    move-object/from16 v0, p0

    iget-object v14, v0, Ldbxyzptlk/db231222/g/T;->d:Ldbxyzptlk/db231222/n/P;

    invoke-virtual {v14, v12}, Ldbxyzptlk/db231222/n/P;->d(Ljava/lang/String;)V

    .line 288
    invoke-virtual {v15}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_d

    .line 289
    move-object/from16 v0, p0

    iget-object v14, v0, Ldbxyzptlk/db231222/g/T;->e:Lcom/dropbox/android/albums/PhotosModel;

    invoke-virtual {v14}, Lcom/dropbox/android/albums/PhotosModel;->g()V

    .line 292
    move-object/from16 v0, p0

    iget-object v14, v0, Ldbxyzptlk/db231222/g/T;->e:Lcom/dropbox/android/albums/PhotosModel;

    invoke-virtual {v14}, Lcom/dropbox/android/albums/PhotosModel;->h()V

    .line 295
    :cond_d
    const-wide/16 v14, 0x1

    cmp-long v14, v6, v14

    if-nez v14, :cond_0

    .line 296
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->V()Lcom/dropbox/android/util/analytics/l;

    move-result-object v14

    const-string v15, "added"

    invoke-virtual {v14, v15, v10, v11}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v14

    const-string v15, "removed"

    invoke-virtual {v14, v15, v8, v9}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v14

    const-string v15, "dur"

    invoke-virtual {v14, v15, v4, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v14

    invoke-virtual {v14}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 297
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->U()Lcom/dropbox/android/util/analytics/l;

    move-result-object v14

    const-string v15, "added"

    invoke-virtual {v14, v15, v10, v11}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v14

    const-string v15, "removed"

    invoke-virtual {v14, v15, v8, v9}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v14

    invoke-virtual {v14, v13}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v14

    invoke-virtual {v14}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto/16 :goto_0

    .line 270
    :cond_e
    const/4 v2, 0x0

    goto :goto_5

    .line 301
    :cond_f
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->W()Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "added"

    invoke-virtual {v2, v3, v10, v11}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "removed"

    invoke-virtual {v2, v3, v8, v9}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "dur"

    invoke-virtual {v2, v3, v4, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 302
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->X()Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "added"

    invoke-virtual {v2, v3, v10, v11}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "removed"

    invoke-virtual {v2, v3, v8, v9}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    const-string v3, "pages"

    invoke-virtual {v2, v3, v6, v7}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    invoke-virtual {v2, v13}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto/16 :goto_4
.end method
