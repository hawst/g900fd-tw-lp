.class abstract Ldbxyzptlk/db231222/g/V;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final synthetic b:Ldbxyzptlk/db231222/g/R;


# direct methods
.method private constructor <init>(Ldbxyzptlk/db231222/g/R;)V
    .locals 2

    .prologue
    .line 144
    iput-object p1, p0, Ldbxyzptlk/db231222/g/V;->b:Ldbxyzptlk/db231222/g/R;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Ldbxyzptlk/db231222/g/V;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method synthetic constructor <init>(Ldbxyzptlk/db231222/g/R;Ldbxyzptlk/db231222/g/S;)V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/g/V;-><init>(Ldbxyzptlk/db231222/g/R;)V

    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Ldbxyzptlk/db231222/g/V;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 171
    return-void
.end method

.method protected final c()Z
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Ldbxyzptlk/db231222/g/V;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Ldbxyzptlk/db231222/g/V;->b:Ldbxyzptlk/db231222/g/R;

    invoke-static {v0}, Ldbxyzptlk/db231222/g/R;->a(Ldbxyzptlk/db231222/g/R;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Ldbxyzptlk/db231222/g/W;->a:Ldbxyzptlk/db231222/g/W;

    if-eq v0, v1, :cond_0

    .line 183
    iget-object v0, p0, Ldbxyzptlk/db231222/g/V;->b:Ldbxyzptlk/db231222/g/R;

    sget-object v1, Ldbxyzptlk/db231222/g/W;->a:Ldbxyzptlk/db231222/g/W;

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/g/R;->a(Ldbxyzptlk/db231222/g/R;Ldbxyzptlk/db231222/g/W;)V

    .line 185
    :cond_0
    return-void
.end method

.method public final run()V
    .locals 4

    .prologue
    .line 151
    :try_start_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/g/V;->a()V
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 157
    iget-object v0, p0, Ldbxyzptlk/db231222/g/V;->b:Ldbxyzptlk/db231222/g/R;

    invoke-static {v0}, Ldbxyzptlk/db231222/g/R;->a(Ldbxyzptlk/db231222/g/R;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Ldbxyzptlk/db231222/g/W;->c:Ldbxyzptlk/db231222/g/W;

    if-eq v0, v1, :cond_0

    .line 158
    iget-object v0, p0, Ldbxyzptlk/db231222/g/V;->b:Ldbxyzptlk/db231222/g/R;

    sget-object v1, Ldbxyzptlk/db231222/g/W;->b:Ldbxyzptlk/db231222/g/W;

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/g/R;->a(Ldbxyzptlk/db231222/g/R;Ldbxyzptlk/db231222/g/W;)V

    .line 160
    :cond_0
    iget-object v1, p0, Ldbxyzptlk/db231222/g/V;->b:Ldbxyzptlk/db231222/g/R;

    monitor-enter v1

    .line 161
    :try_start_1
    iget-object v0, p0, Ldbxyzptlk/db231222/g/V;->b:Ldbxyzptlk/db231222/g/R;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ldbxyzptlk/db231222/g/R;->a(Ldbxyzptlk/db231222/g/R;Ldbxyzptlk/db231222/g/V;)Ldbxyzptlk/db231222/g/V;

    .line 162
    iget-object v0, p0, Ldbxyzptlk/db231222/g/V;->b:Ldbxyzptlk/db231222/g/R;

    invoke-static {v0}, Ldbxyzptlk/db231222/g/R;->b(Ldbxyzptlk/db231222/g/R;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    iget-object v0, p0, Ldbxyzptlk/db231222/g/V;->b:Ldbxyzptlk/db231222/g/R;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/g/R;->a(Z)V

    .line 165
    :cond_1
    monitor-exit v1

    .line 167
    :goto_0
    return-void

    .line 165
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 152
    :catch_0
    move-exception v0

    .line 153
    :try_start_2
    invoke-static {}, Ldbxyzptlk/db231222/g/R;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "UpdateTask error"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 154
    iget-object v0, p0, Ldbxyzptlk/db231222/g/V;->b:Ldbxyzptlk/db231222/g/R;

    sget-object v1, Ldbxyzptlk/db231222/g/W;->c:Ldbxyzptlk/db231222/g/W;

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/g/R;->a(Ldbxyzptlk/db231222/g/R;Ldbxyzptlk/db231222/g/W;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 157
    iget-object v0, p0, Ldbxyzptlk/db231222/g/V;->b:Ldbxyzptlk/db231222/g/R;

    invoke-static {v0}, Ldbxyzptlk/db231222/g/R;->a(Ldbxyzptlk/db231222/g/R;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Ldbxyzptlk/db231222/g/W;->c:Ldbxyzptlk/db231222/g/W;

    if-eq v0, v1, :cond_2

    .line 158
    iget-object v0, p0, Ldbxyzptlk/db231222/g/V;->b:Ldbxyzptlk/db231222/g/R;

    sget-object v1, Ldbxyzptlk/db231222/g/W;->b:Ldbxyzptlk/db231222/g/W;

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/g/R;->a(Ldbxyzptlk/db231222/g/R;Ldbxyzptlk/db231222/g/W;)V

    .line 160
    :cond_2
    iget-object v1, p0, Ldbxyzptlk/db231222/g/V;->b:Ldbxyzptlk/db231222/g/R;

    monitor-enter v1

    .line 161
    :try_start_3
    iget-object v0, p0, Ldbxyzptlk/db231222/g/V;->b:Ldbxyzptlk/db231222/g/R;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ldbxyzptlk/db231222/g/R;->a(Ldbxyzptlk/db231222/g/R;Ldbxyzptlk/db231222/g/V;)Ldbxyzptlk/db231222/g/V;

    .line 162
    iget-object v0, p0, Ldbxyzptlk/db231222/g/V;->b:Ldbxyzptlk/db231222/g/R;

    invoke-static {v0}, Ldbxyzptlk/db231222/g/R;->b(Ldbxyzptlk/db231222/g/R;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 163
    iget-object v0, p0, Ldbxyzptlk/db231222/g/V;->b:Ldbxyzptlk/db231222/g/R;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/g/R;->a(Z)V

    .line 165
    :cond_3
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 157
    :catchall_2
    move-exception v0

    iget-object v1, p0, Ldbxyzptlk/db231222/g/V;->b:Ldbxyzptlk/db231222/g/R;

    invoke-static {v1}, Ldbxyzptlk/db231222/g/R;->a(Ldbxyzptlk/db231222/g/R;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/db231222/g/W;->c:Ldbxyzptlk/db231222/g/W;

    if-eq v1, v2, :cond_4

    .line 158
    iget-object v1, p0, Ldbxyzptlk/db231222/g/V;->b:Ldbxyzptlk/db231222/g/R;

    sget-object v2, Ldbxyzptlk/db231222/g/W;->b:Ldbxyzptlk/db231222/g/W;

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/g/R;->a(Ldbxyzptlk/db231222/g/R;Ldbxyzptlk/db231222/g/W;)V

    .line 160
    :cond_4
    iget-object v1, p0, Ldbxyzptlk/db231222/g/V;->b:Ldbxyzptlk/db231222/g/R;

    monitor-enter v1

    .line 161
    :try_start_4
    iget-object v2, p0, Ldbxyzptlk/db231222/g/V;->b:Ldbxyzptlk/db231222/g/R;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/g/R;->a(Ldbxyzptlk/db231222/g/R;Ldbxyzptlk/db231222/g/V;)Ldbxyzptlk/db231222/g/V;

    .line 162
    iget-object v2, p0, Ldbxyzptlk/db231222/g/V;->b:Ldbxyzptlk/db231222/g/R;

    invoke-static {v2}, Ldbxyzptlk/db231222/g/R;->b(Ldbxyzptlk/db231222/g/R;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 163
    iget-object v2, p0, Ldbxyzptlk/db231222/g/V;->b:Ldbxyzptlk/db231222/g/R;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ldbxyzptlk/db231222/g/R;->a(Z)V

    .line 165
    :cond_5
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    throw v0

    :catchall_3
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    throw v0
.end method
