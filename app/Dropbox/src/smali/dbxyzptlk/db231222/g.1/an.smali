.class public Ldbxyzptlk/db231222/g/an;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ldbxyzptlk/db231222/g/a;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ldbxyzptlk/db231222/z/K;

.field private final d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Ldbxyzptlk/db231222/g/an;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/g/an;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ldbxyzptlk/db231222/z/K;Z)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 63
    iput-object p2, p0, Ldbxyzptlk/db231222/g/an;->b:Ljava/lang/String;

    .line 64
    iput-object p3, p0, Ldbxyzptlk/db231222/g/an;->c:Ldbxyzptlk/db231222/z/K;

    .line 65
    iput-boolean p4, p0, Ldbxyzptlk/db231222/g/an;->d:Z

    .line 66
    return-void
.end method


# virtual methods
.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/db231222/g/a;
    .locals 5

    .prologue
    const v0, 0x7f0d005d

    .line 70
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v2

    .line 72
    :try_start_0
    new-instance v1, Ldbxyzptlk/db231222/g/ap;

    iget-object v3, p0, Ldbxyzptlk/db231222/g/an;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/dropbox/android/filemanager/a;->a(Ljava/lang/String;)Lcom/dropbox/android/filemanager/h;

    move-result-object v2

    iget-object v3, p0, Ldbxyzptlk/db231222/g/an;->c:Ldbxyzptlk/db231222/z/K;

    iget-boolean v4, p0, Ldbxyzptlk/db231222/g/an;->d:Z

    invoke-direct {v1, v2, v3, v4}, Ldbxyzptlk/db231222/g/ap;-><init>(Lcom/dropbox/android/filemanager/h;Ldbxyzptlk/db231222/z/K;Z)V
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_2

    move-object v0, v1

    .line 90
    :goto_0
    return-object v0

    .line 73
    :catch_0
    move-exception v1

    move-object v2, v1

    .line 75
    iget v1, v2, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v3, 0x1f4

    if-lt v1, v3, :cond_0

    .line 76
    sget-object v0, Ldbxyzptlk/db231222/g/an;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error starting  SSO link: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const v0, 0x7f0d005c

    .line 83
    :goto_1
    new-instance v1, Ldbxyzptlk/db231222/g/u;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/w/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/g/u;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    .line 80
    :cond_0
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 84
    :catch_1
    move-exception v0

    .line 85
    const v0, 0x7f0d005b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 86
    new-instance v0, Ldbxyzptlk/db231222/g/u;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/g/u;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 87
    :catch_2
    move-exception v1

    .line 88
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v2

    invoke-virtual {v2, v1}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 89
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 90
    new-instance v0, Ldbxyzptlk/db231222/g/u;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/g/u;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/an;->a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/db231222/g/a;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Ldbxyzptlk/db231222/g/a;)V
    .locals 0

    .prologue
    .line 96
    invoke-interface {p2, p1}, Ldbxyzptlk/db231222/g/a;->a(Landroid/content/Context;)V

    .line 97
    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 101
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 102
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 28
    check-cast p2, Ldbxyzptlk/db231222/g/a;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/an;->a(Landroid/content/Context;Ldbxyzptlk/db231222/g/a;)V

    return-void
.end method
