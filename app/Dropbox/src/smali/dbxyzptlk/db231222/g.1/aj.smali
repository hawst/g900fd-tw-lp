.class public Ldbxyzptlk/db231222/g/aj;
.super Landroid/os/AsyncTask;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/github/droidfu/DroidFuApplication;

.field private final c:Ljava/lang/String;

.field private final d:Ldbxyzptlk/db231222/r/d;

.field private e:Ldbxyzptlk/db231222/s/a;

.field private f:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Ldbxyzptlk/db231222/g/aj;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/g/aj;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/g/aj;->e:Ldbxyzptlk/db231222/s/a;

    .line 24
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ldbxyzptlk/db231222/g/aj;->f:J

    .line 27
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/github/droidfu/DroidFuApplication;

    iput-object v0, p0, Ldbxyzptlk/db231222/g/aj;->b:Lcom/github/droidfu/DroidFuApplication;

    .line 28
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/g/aj;->c:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Ldbxyzptlk/db231222/g/aj;->d:Ldbxyzptlk/db231222/r/d;

    .line 30
    return-void
.end method


# virtual methods
.method protected final a()Landroid/content/Context;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 34
    :try_start_0
    iget-object v1, p0, Ldbxyzptlk/db231222/g/aj;->b:Lcom/github/droidfu/DroidFuApplication;

    iget-object v2, p0, Ldbxyzptlk/db231222/g/aj;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/github/droidfu/DroidFuApplication;->a(Ljava/lang/String;)Landroid/content/Context;

    move-result-object v2

    .line 35
    if-eqz v2, :cond_0

    iget-object v1, p0, Ldbxyzptlk/db231222/g/aj;->c:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    instance-of v1, v2, Landroid/app/Activity;

    if-eqz v1, :cond_1

    move-object v0, v2

    check-cast v0, Landroid/app/Activity;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move-object v2, v3

    .line 44
    :cond_1
    :goto_0
    return-object v2

    .line 42
    :catch_0
    move-exception v1

    .line 43
    sget-object v2, Ldbxyzptlk/db231222/g/aj;->a:Ljava/lang/String;

    const-string v4, "Error in getCallingContext()"

    invoke-static {v2, v4, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v2, v3

    .line 44
    goto :goto_0
.end method

.method public final varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4

    .prologue
    .line 51
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/g/aj;->b:Lcom/github/droidfu/DroidFuApplication;

    if-eqz v0, :cond_0

    .line 52
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    .line 53
    iget-object v1, p0, Ldbxyzptlk/db231222/g/aj;->d:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->d()Lcom/dropbox/android/service/a;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/service/e;->a:Lcom/dropbox/android/service/e;

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/service/a;->a(Lcom/dropbox/android/service/e;Ldbxyzptlk/db231222/r/e;)Ldbxyzptlk/db231222/s/a;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/g/aj;->e:Ldbxyzptlk/db231222/s/a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :cond_0
    :goto_0
    iget-object v0, p0, Ldbxyzptlk/db231222/g/aj;->d:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->x()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/I;->e()J

    move-result-wide v0

    iget-object v2, p0, Ldbxyzptlk/db231222/g/aj;->d:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->B()Lcom/dropbox/android/util/aE;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/aE;->b()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Ldbxyzptlk/db231222/g/aj;->f:J

    .line 61
    const/4 v0, 0x0

    return-object v0

    .line 55
    :catch_0
    move-exception v0

    .line 56
    sget-object v1, Ldbxyzptlk/db231222/g/aj;->a:Ljava/lang/String;

    const-string v2, "Error in getting settings"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected final a(Ljava/lang/Void;)V
    .locals 4

    .prologue
    .line 67
    invoke-virtual {p0}, Ldbxyzptlk/db231222/g/aj;->a()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/base/BasePreferenceActivity;

    .line 69
    if-eqz v0, :cond_2

    .line 70
    iget-object v1, p0, Ldbxyzptlk/db231222/g/aj;->e:Ldbxyzptlk/db231222/s/a;

    if-eqz v1, :cond_0

    .line 71
    iget-object v1, p0, Ldbxyzptlk/db231222/g/aj;->e:Ldbxyzptlk/db231222/s/a;

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/UserPrefsActivity;->a(Lcom/dropbox/android/activity/base/BasePreferenceActivity;Ldbxyzptlk/db231222/s/a;)V

    .line 74
    :cond_0
    instance-of v1, v0, Lcom/dropbox/android/activity/PrefsActivity;

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 75
    check-cast v1, Lcom/dropbox/android/activity/PrefsActivity;

    iget-wide v2, p0, Ldbxyzptlk/db231222/g/aj;->f:J

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/activity/PrefsActivity;->a(J)V

    .line 78
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/base/BasePreferenceActivity;->setSupportProgressBarIndeterminateVisibility(Z)V

    .line 80
    :cond_2
    return-void
.end method

.method public synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/g/aj;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/g/aj;->a(Ljava/lang/Void;)V

    return-void
.end method
