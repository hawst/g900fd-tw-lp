.class public Ldbxyzptlk/db231222/g/o;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ldbxyzptlk/db231222/g/a;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/dropbox/android/filemanager/h;

.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Ldbxyzptlk/db231222/g/o;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/g/o;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/h;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 61
    iput-object p2, p0, Ldbxyzptlk/db231222/g/o;->b:Lcom/dropbox/android/filemanager/h;

    .line 62
    iput-object p3, p0, Ldbxyzptlk/db231222/g/o;->c:Ljava/lang/String;

    .line 63
    return-void
.end method


# virtual methods
.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/db231222/g/a;
    .locals 4

    .prologue
    const v0, 0x7f0d005d

    .line 67
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v1

    .line 69
    :try_start_0
    iget-object v2, p0, Ldbxyzptlk/db231222/g/o;->b:Lcom/dropbox/android/filemanager/h;

    iget-object v3, p0, Ldbxyzptlk/db231222/g/o;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/filemanager/a;->a(Lcom/dropbox/android/filemanager/h;Ljava/lang/String;)Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    .line 70
    invoke-static {v2}, Ldbxyzptlk/db231222/g/r;->a(Ldbxyzptlk/db231222/r/d;)V

    .line 76
    invoke-virtual {p0}, Ldbxyzptlk/db231222/g/o;->f()V

    .line 77
    new-instance v1, Ldbxyzptlk/db231222/g/q;

    invoke-direct {v1, v2}, Ldbxyzptlk/db231222/g/q;-><init>(Ldbxyzptlk/db231222/r/d;)V
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_3

    move-object v0, v1

    .line 100
    :goto_0
    return-object v0

    .line 78
    :catch_0
    move-exception v0

    .line 80
    sget-object v1, Ldbxyzptlk/db231222/g/o;->a:Ljava/lang/String;

    const-string v2, "Bad verifier code or invalid/expired oauth request token"

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const v1, 0x7f0d0030

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/w/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 82
    new-instance v0, Ldbxyzptlk/db231222/g/u;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/g/u;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 83
    :catch_1
    move-exception v1

    move-object v2, v1

    .line 85
    iget v1, v2, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v3, 0x1f4

    if-lt v1, v3, :cond_0

    .line 86
    sget-object v0, Ldbxyzptlk/db231222/g/o;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error logging in via SSO: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const v0, 0x7f0d005c

    .line 93
    :goto_1
    new-instance v1, Ldbxyzptlk/db231222/g/u;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/w/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/g/u;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    .line 90
    :cond_0
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 94
    :catch_2
    move-exception v0

    .line 95
    const v0, 0x7f0d005b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 96
    new-instance v0, Ldbxyzptlk/db231222/g/u;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/g/u;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 97
    :catch_3
    move-exception v1

    .line 98
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v2

    invoke-virtual {v2, v1}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 99
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 100
    new-instance v0, Ldbxyzptlk/db231222/g/u;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/g/u;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/o;->a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/db231222/g/a;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Ldbxyzptlk/db231222/g/a;)V
    .locals 0

    .prologue
    .line 106
    invoke-interface {p2, p1}, Ldbxyzptlk/db231222/g/a;->a(Landroid/content/Context;)V

    .line 107
    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 111
    sget-object v0, Ldbxyzptlk/db231222/g/o;->a:Ljava/lang/String;

    const-string v1, "Error with single-sign on"

    invoke-static {v0, v1, p2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 112
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 113
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 30
    check-cast p2, Ldbxyzptlk/db231222/g/a;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/o;->a(Landroid/content/Context;Ldbxyzptlk/db231222/g/a;)V

    return-void
.end method
