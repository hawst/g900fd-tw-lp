.class public Ldbxyzptlk/db231222/g/P;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Ldbxyzptlk/db231222/g/P;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/g/P;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 32
    iput-object p2, p0, Ldbxyzptlk/db231222/g/P;->b:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 57
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    .line 58
    iget-object v1, p0, Ldbxyzptlk/db231222/g/P;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/a;->c(Ljava/lang/String;)V

    .line 59
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/P;->a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    check-cast p1, Ldbxyzptlk/db231222/g/Q;

    .line 66
    iget-object v0, p0, Ldbxyzptlk/db231222/g/P;->b:Ljava/lang/String;

    invoke-interface {p1, v0}, Ldbxyzptlk/db231222/g/Q;->i(Ljava/lang/String;)V

    .line 67
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bi()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 69
    :cond_0
    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 37
    const v0, 0x7f0d005d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 39
    instance-of v1, p2, Ldbxyzptlk/db231222/w/i;

    if-eqz v1, :cond_1

    .line 40
    check-cast p2, Ldbxyzptlk/db231222/w/i;

    .line 41
    iget v1, p2, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x190

    if-ne v1, v2, :cond_0

    .line 42
    const v0, 0x7f0d0229

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 52
    :cond_0
    :goto_0
    invoke-static {p1, v0}, Lcom/dropbox/android/util/bk;->b(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 53
    return-void

    .line 44
    :cond_1
    instance-of v1, p2, Lcom/dropbox/android/filemanager/g;

    if-eqz v1, :cond_2

    .line 45
    const v0, 0x7f0d022a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v0, p1

    .line 46
    check-cast v0, Ldbxyzptlk/db231222/g/Q;

    .line 47
    iget-object v2, p0, Ldbxyzptlk/db231222/g/P;->b:Ljava/lang/String;

    invoke-interface {v0, v2}, Ldbxyzptlk/db231222/g/Q;->h(Ljava/lang/String;)V

    move-object v0, v1

    .line 48
    goto :goto_0

    .line 49
    :cond_2
    sget-object v1, Ldbxyzptlk/db231222/g/P;->a:Ljava/lang/String;

    const-string v2, "Error in sending password reset email."

    invoke-static {v1, v2, p2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 20
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/P;->a(Landroid/content/Context;Ljava/lang/Boolean;)V

    return-void
.end method
