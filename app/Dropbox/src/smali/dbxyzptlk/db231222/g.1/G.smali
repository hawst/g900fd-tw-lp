.class public Ldbxyzptlk/db231222/g/G;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ldbxyzptlk/db231222/g/a;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Ldbxyzptlk/db231222/g/G;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/g/G;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 78
    iput-object p2, p0, Ldbxyzptlk/db231222/g/G;->b:Ljava/lang/String;

    .line 79
    iput-boolean p3, p0, Ldbxyzptlk/db231222/g/G;->c:Z

    .line 80
    return-void
.end method


# virtual methods
.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/db231222/g/a;
    .locals 6

    .prologue
    const v5, 0x7f0d005d

    const/4 v4, 0x0

    .line 91
    :try_start_0
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    .line 92
    iget-object v1, p0, Ldbxyzptlk/db231222/g/G;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/a;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    .line 94
    invoke-static {v1}, Ldbxyzptlk/db231222/g/r;->a(Ldbxyzptlk/db231222/r/d;)V

    .line 96
    iget-boolean v0, p0, Ldbxyzptlk/db231222/g/G;->c:Z

    if-eqz v0, :cond_0

    .line 99
    invoke-virtual {p0}, Ldbxyzptlk/db231222/g/G;->f()V

    .line 102
    :cond_0
    new-instance v0, Ldbxyzptlk/db231222/g/I;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/g/I;-><init>(Ldbxyzptlk/db231222/r/d;)V
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_3

    .line 122
    :goto_0
    return-object v0

    .line 103
    :catch_0
    move-exception v0

    .line 105
    sget-object v1, Ldbxyzptlk/db231222/g/G;->a:Ljava/lang/String;

    const-string v2, "Error logging in"

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const v1, 0x7f0d0240

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/w/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 107
    new-instance v0, Ldbxyzptlk/db231222/g/H;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/g/H;-><init>(Ljava/lang/String;Z)V

    goto :goto_0

    .line 108
    :catch_1
    move-exception v0

    .line 109
    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x190

    if-eq v1, v2, :cond_1

    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x193

    if-eq v1, v2, :cond_1

    .line 111
    sget-object v1, Ldbxyzptlk/db231222/g/G;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error verifying twofactor: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 114
    :cond_1
    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/w/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 115
    new-instance v0, Ldbxyzptlk/db231222/g/H;

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/db231222/g/H;-><init>(Ljava/lang/String;Z)V

    goto :goto_0

    .line 116
    :catch_2
    move-exception v0

    .line 117
    const v0, 0x7f0d005b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 118
    new-instance v0, Ldbxyzptlk/db231222/g/H;

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/db231222/g/H;-><init>(Ljava/lang/String;Z)V

    goto :goto_0

    .line 119
    :catch_3
    move-exception v0

    .line 120
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 121
    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 122
    new-instance v0, Ldbxyzptlk/db231222/g/H;

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/db231222/g/H;-><init>(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/G;->a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/db231222/g/a;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Ldbxyzptlk/db231222/g/a;)V
    .locals 0

    .prologue
    .line 128
    invoke-interface {p2, p1}, Ldbxyzptlk/db231222/g/a;->a(Landroid/content/Context;)V

    .line 129
    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 84
    sget-object v0, Ldbxyzptlk/db231222/g/G;->a:Ljava/lang/String;

    const-string v1, "Error in Logging in."

    invoke-static {v0, v1, p2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 85
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Ldbxyzptlk/db231222/i/d;->b(Ljava/lang/Throwable;)V

    .line 86
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 20
    check-cast p2, Ldbxyzptlk/db231222/g/a;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/G;->a(Landroid/content/Context;Ldbxyzptlk/db231222/g/a;)V

    return-void
.end method
