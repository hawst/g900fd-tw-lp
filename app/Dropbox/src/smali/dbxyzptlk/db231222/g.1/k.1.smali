.class public final Ldbxyzptlk/db231222/g/k;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ldbxyzptlk/db231222/g/n;",
        ">;"
    }
.end annotation


# static fields
.field private static a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static final f:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ldbxyzptlk/db231222/g/m;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ldbxyzptlk/db231222/g/l;

.field private final c:Ldbxyzptlk/db231222/z/aA;

.field private d:Ljava/io/File;

.field private e:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 63
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Ldbxyzptlk/db231222/g/k;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 157
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/g/k;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/g/l;Ldbxyzptlk/db231222/z/aA;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 86
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 70
    iput-object v0, p0, Ldbxyzptlk/db231222/g/k;->d:Ljava/io/File;

    .line 72
    iput-object v0, p0, Ldbxyzptlk/db231222/g/k;->e:Ljava/io/File;

    .line 87
    iput-object p2, p0, Ldbxyzptlk/db231222/g/k;->b:Ldbxyzptlk/db231222/g/l;

    .line 88
    iput-object p3, p0, Ldbxyzptlk/db231222/g/k;->c:Ldbxyzptlk/db231222/z/aA;

    .line 89
    return-void
.end method

.method public static a(Ldbxyzptlk/db231222/g/l;Ldbxyzptlk/db231222/z/aA;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 80
    sget-object v0, Ldbxyzptlk/db231222/g/k;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    new-instance v0, Ldbxyzptlk/db231222/g/k;

    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1}, Ldbxyzptlk/db231222/g/k;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/g/l;Ldbxyzptlk/db231222/z/aA;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/g/k;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 83
    :cond_0
    return-void
.end method

.method public static a(Ldbxyzptlk/db231222/g/m;)V
    .locals 1

    .prologue
    .line 160
    sget-object v0, Ldbxyzptlk/db231222/g/k;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    return-void
.end method

.method private static a(Z)V
    .locals 2

    .prologue
    .line 172
    sget-object v0, Ldbxyzptlk/db231222/g/k;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/g/m;

    .line 173
    invoke-interface {v0, p0}, Ldbxyzptlk/db231222/g/m;->a(Z)V

    goto :goto_0

    .line 175
    :cond_0
    return-void
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 168
    sget-object v0, Ldbxyzptlk/db231222/g/k;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public static b(Ldbxyzptlk/db231222/g/m;)V
    .locals 1

    .prologue
    .line 164
    sget-object v0, Ldbxyzptlk/db231222/g/k;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 165
    return-void
.end method


# virtual methods
.method protected final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/db231222/g/n;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 99
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/a;->h()Ldbxyzptlk/db231222/z/F;

    move-result-object v0

    .line 101
    invoke-static {}, Ldbxyzptlk/db231222/k/h;->a()Ldbxyzptlk/db231222/k/h;

    move-result-object v1

    .line 102
    iget-object v2, p0, Ldbxyzptlk/db231222/g/k;->c:Ldbxyzptlk/db231222/z/aA;

    iget-object v2, v2, Ldbxyzptlk/db231222/z/aA;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/k/h;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Ldbxyzptlk/db231222/g/k;->d:Ljava/io/File;

    .line 103
    new-instance v2, Ljava/io/BufferedOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    iget-object v4, p0, Ldbxyzptlk/db231222/g/k;->d:Ljava/io/File;

    invoke-direct {v3, v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    invoke-direct {v2, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 106
    :try_start_0
    iget-object v3, p0, Ldbxyzptlk/db231222/g/k;->c:Ldbxyzptlk/db231222/z/aA;

    iget-object v3, v3, Ldbxyzptlk/db231222/z/aA;->d:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Ldbxyzptlk/db231222/z/F;->a(Ljava/lang/String;Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V

    .line 111
    iget-object v2, p0, Ldbxyzptlk/db231222/g/k;->c:Ldbxyzptlk/db231222/z/aA;

    iget-object v2, v2, Ldbxyzptlk/db231222/z/aA;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 112
    iget-object v2, p0, Ldbxyzptlk/db231222/g/k;->c:Ldbxyzptlk/db231222/z/aA;

    iget-object v2, v2, Ldbxyzptlk/db231222/z/aA;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/k/h;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    iput-object v1, p0, Ldbxyzptlk/db231222/g/k;->e:Ljava/io/File;

    .line 114
    new-instance v1, Ljava/io/BufferedOutputStream;

    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v3, p0, Ldbxyzptlk/db231222/g/k;->e:Ljava/io/File;

    invoke-direct {v2, v3, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    invoke-direct {v1, v2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 118
    :try_start_1
    iget-object v2, p0, Ldbxyzptlk/db231222/g/k;->c:Ldbxyzptlk/db231222/z/aA;

    iget-object v2, v2, Ldbxyzptlk/db231222/z/aA;->f:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ldbxyzptlk/db231222/z/F;->b(Ljava/lang/String;Ljava/io/OutputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 120
    invoke-static {v1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V

    .line 124
    :cond_0
    new-instance v0, Ldbxyzptlk/db231222/g/n;

    iget-object v1, p0, Ldbxyzptlk/db231222/g/k;->d:Ljava/io/File;

    iget-object v2, p0, Ldbxyzptlk/db231222/g/k;->e:Ljava/io/File;

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/g/n;-><init>(Ljava/io/File;Ljava/io/File;)V

    return-object v0

    .line 108
    :catchall_0
    move-exception v0

    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V

    throw v0

    .line 120
    :catchall_1
    move-exception v0

    invoke-static {v1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V

    throw v0
.end method

.method protected final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/k;->a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/db231222/g/n;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x1

    invoke-static {v0}, Ldbxyzptlk/db231222/g/k;->a(Z)V

    .line 94
    return-void
.end method

.method protected final a(Landroid/content/Context;Ldbxyzptlk/db231222/g/n;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 129
    iget-object v0, p0, Ldbxyzptlk/db231222/g/k;->b:Ldbxyzptlk/db231222/g/l;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Ldbxyzptlk/db231222/g/k;->b:Ldbxyzptlk/db231222/g/l;

    iget-object v1, p0, Ldbxyzptlk/db231222/g/k;->c:Ldbxyzptlk/db231222/z/aA;

    invoke-interface {v0, v1, p2}, Ldbxyzptlk/db231222/g/l;->a(Ldbxyzptlk/db231222/z/aA;Ldbxyzptlk/db231222/g/n;)V

    .line 132
    :cond_0
    sget-object v0, Ldbxyzptlk/db231222/g/k;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 133
    invoke-static {v2}, Ldbxyzptlk/db231222/g/k;->a(Z)V

    .line 134
    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 138
    iget-object v0, p0, Ldbxyzptlk/db231222/g/k;->d:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/g/k;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Ldbxyzptlk/db231222/g/k;->d:Ljava/io/File;

    invoke-static {v0}, Ldbxyzptlk/db231222/X/b;->c(Ljava/io/File;)Z

    .line 140
    iput-object v2, p0, Ldbxyzptlk/db231222/g/k;->d:Ljava/io/File;

    .line 142
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/g/k;->e:Ljava/io/File;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldbxyzptlk/db231222/g/k;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 143
    iget-object v0, p0, Ldbxyzptlk/db231222/g/k;->e:Ljava/io/File;

    invoke-static {v0}, Ldbxyzptlk/db231222/X/b;->c(Ljava/io/File;)Z

    .line 144
    iput-object v2, p0, Ldbxyzptlk/db231222/g/k;->e:Ljava/io/File;

    .line 146
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/g/k;->b:Ldbxyzptlk/db231222/g/l;

    if-eqz v0, :cond_2

    .line 147
    iget-object v0, p0, Ldbxyzptlk/db231222/g/k;->b:Ldbxyzptlk/db231222/g/l;

    invoke-interface {v0, p2}, Ldbxyzptlk/db231222/g/l;->a(Ljava/lang/Exception;)V

    .line 149
    :cond_2
    sget-object v0, Ldbxyzptlk/db231222/g/k;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 150
    invoke-static {v1}, Ldbxyzptlk/db231222/g/k;->a(Z)V

    .line 151
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 25
    check-cast p2, Ldbxyzptlk/db231222/g/n;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/g/k;->a(Landroid/content/Context;Ldbxyzptlk/db231222/g/n;)V

    return-void
.end method
