.class public Ldbxyzptlk/db231222/g/R;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ldbxyzptlk/db231222/g/V;

.field private c:Z

.field private d:Z

.field private e:Z

.field private final f:Lcom/dropbox/android/albums/PhotosModel;

.field private final g:Ldbxyzptlk/db231222/z/M;

.field private final h:Lcom/dropbox/android/provider/j;

.field private final i:Ldbxyzptlk/db231222/n/P;

.field private final j:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ldbxyzptlk/db231222/g/U;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ldbxyzptlk/db231222/g/W;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Ldbxyzptlk/db231222/g/R;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/g/R;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/albums/PhotosModel;Ldbxyzptlk/db231222/n/P;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/g/R;->b:Ldbxyzptlk/db231222/g/V;

    .line 45
    iput-boolean v1, p0, Ldbxyzptlk/db231222/g/R;->c:Z

    .line 50
    iput-boolean v1, p0, Ldbxyzptlk/db231222/g/R;->d:Z

    .line 52
    iput-boolean v1, p0, Ldbxyzptlk/db231222/g/R;->e:Z

    .line 59
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/g/R;->j:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 64
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, Ldbxyzptlk/db231222/g/W;->b:Ldbxyzptlk/db231222/g/W;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/g/R;->k:Ljava/util/concurrent/atomic/AtomicReference;

    .line 71
    iput-object p1, p0, Ldbxyzptlk/db231222/g/R;->f:Lcom/dropbox/android/albums/PhotosModel;

    .line 72
    iput-object p2, p0, Ldbxyzptlk/db231222/g/R;->i:Ldbxyzptlk/db231222/n/P;

    .line 73
    iput-object p3, p0, Ldbxyzptlk/db231222/g/R;->h:Lcom/dropbox/android/provider/j;

    .line 74
    iput-object p4, p0, Ldbxyzptlk/db231222/g/R;->g:Ldbxyzptlk/db231222/z/M;

    .line 75
    return-void
.end method

.method static synthetic a(Ldbxyzptlk/db231222/g/R;Ldbxyzptlk/db231222/g/V;)Ldbxyzptlk/db231222/g/V;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Ldbxyzptlk/db231222/g/R;->b:Ldbxyzptlk/db231222/g/V;

    return-object p1
.end method

.method static synthetic a(Ldbxyzptlk/db231222/g/R;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Ldbxyzptlk/db231222/g/R;->k:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/g/R;Ldbxyzptlk/db231222/g/W;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/g/R;->a(Ldbxyzptlk/db231222/g/W;)V

    return-void
.end method

.method private a(Ldbxyzptlk/db231222/g/W;)V
    .locals 3

    .prologue
    .line 132
    iget-object v0, p0, Ldbxyzptlk/db231222/g/R;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 133
    iget-object v0, p0, Ldbxyzptlk/db231222/g/R;->j:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/g/U;

    .line 139
    iget-object v1, p0, Ldbxyzptlk/db231222/g/R;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldbxyzptlk/db231222/g/W;

    invoke-interface {v0, v1}, Ldbxyzptlk/db231222/g/U;->a(Ldbxyzptlk/db231222/g/W;)V

    goto :goto_0

    .line 142
    :cond_0
    return-void
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Ldbxyzptlk/db231222/g/R;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Ldbxyzptlk/db231222/g/R;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Ldbxyzptlk/db231222/g/R;->c:Z

    return v0
.end method

.method static synthetic c(Ldbxyzptlk/db231222/g/R;)Lcom/dropbox/android/albums/PhotosModel;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Ldbxyzptlk/db231222/g/R;->f:Lcom/dropbox/android/albums/PhotosModel;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/g/R;->b:Ldbxyzptlk/db231222/g/V;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Ldbxyzptlk/db231222/g/R;->b:Ldbxyzptlk/db231222/g/V;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/g/V;->b()V

    .line 85
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/g/R;->c:Z

    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/db231222/g/R;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    monitor-exit p0

    return-void

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ldbxyzptlk/db231222/g/U;)V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Ldbxyzptlk/db231222/g/R;->j:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    iget-object v0, p0, Ldbxyzptlk/db231222/g/R;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/g/W;

    invoke-interface {p1, v0}, Ldbxyzptlk/db231222/g/U;->a(Ldbxyzptlk/db231222/g/W;)V

    .line 125
    return-void
.end method

.method public final declared-synchronized a(Z)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 99
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Ldbxyzptlk/db231222/g/R;->e:Z

    if-eqz v2, :cond_0

    .line 100
    sget-object v0, Ldbxyzptlk/db231222/g/R;->a:Ljava/lang/String;

    const-string v1, "refreshPhotos called after unlink"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    :goto_0
    monitor-exit p0

    return-void

    .line 103
    :cond_0
    :try_start_1
    iget-boolean v2, p0, Ldbxyzptlk/db231222/g/R;->d:Z

    if-nez v2, :cond_1

    if-eqz p1, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    iput-boolean v0, p0, Ldbxyzptlk/db231222/g/R;->d:Z

    .line 104
    iget-object v0, p0, Ldbxyzptlk/db231222/g/R;->b:Ldbxyzptlk/db231222/g/V;

    if-nez v0, :cond_3

    .line 105
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/g/R;->c:Z

    .line 106
    new-instance v0, Ldbxyzptlk/db231222/g/T;

    iget-boolean v2, p0, Ldbxyzptlk/db231222/g/R;->d:Z

    iget-object v3, p0, Ldbxyzptlk/db231222/g/R;->i:Ldbxyzptlk/db231222/n/P;

    iget-object v4, p0, Ldbxyzptlk/db231222/g/R;->f:Lcom/dropbox/android/albums/PhotosModel;

    iget-object v5, p0, Ldbxyzptlk/db231222/g/R;->h:Lcom/dropbox/android/provider/j;

    iget-object v6, p0, Ldbxyzptlk/db231222/g/R;->g:Ldbxyzptlk/db231222/z/M;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/g/T;-><init>(Ldbxyzptlk/db231222/g/R;ZLdbxyzptlk/db231222/n/P;Lcom/dropbox/android/albums/PhotosModel;Lcom/dropbox/android/provider/j;Ldbxyzptlk/db231222/z/M;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/g/R;->b:Ldbxyzptlk/db231222/g/V;

    .line 109
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/g/R;->d:Z

    .line 110
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Ldbxyzptlk/db231222/g/R;->b:Ldbxyzptlk/db231222/g/V;

    const-string v2, "PhotoGalleryMetadataThread"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 111
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 112
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 114
    :cond_3
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Ldbxyzptlk/db231222/g/R;->c:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final b(Ldbxyzptlk/db231222/g/U;)V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Ldbxyzptlk/db231222/g/R;->j:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 129
    return-void
.end method
