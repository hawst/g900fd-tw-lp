.class public final Ldbxyzptlk/db231222/X/f;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final a:C

.field public static final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 100
    sget-char v0, Ljava/io/File;->separatorChar:C

    sput-char v0, Ldbxyzptlk/db231222/X/f;->a:C

    .line 115
    new-instance v0, Ldbxyzptlk/db231222/Z/a;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/Z/a;-><init>(I)V

    .line 116
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 117
    invoke-virtual {v1}, Ljava/io/PrintWriter;->println()V

    .line 118
    invoke-virtual {v0}, Ldbxyzptlk/db231222/Z/a;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/X/f;->b:Ljava/lang/String;

    .line 119
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    .line 120
    return-void
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/OutputStream;)I
    .locals 4

    .prologue
    .line 1357
    invoke-static {p0, p1}, Ldbxyzptlk/db231222/X/f;->b(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    move-result-wide v0

    .line 1358
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 1359
    const/4 v0, -0x1

    .line 1361
    :goto_0
    return v0

    :cond_0
    long-to-int v0, v0

    goto :goto_0
.end method

.method public static a(Ljava/io/Reader;Ljava/io/Writer;)I
    .locals 4

    .prologue
    .line 1461
    invoke-static {p0, p1}, Ldbxyzptlk/db231222/X/f;->b(Ljava/io/Reader;Ljava/io/Writer;)J

    move-result-wide v0

    .line 1462
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 1463
    const/4 v0, -0x1

    .line 1465
    :goto_0
    return v0

    :cond_0
    long-to-int v0, v0

    goto :goto_0
.end method

.method public static a(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 584
    new-instance v0, Ldbxyzptlk/db231222/Z/a;

    invoke-direct {v0}, Ldbxyzptlk/db231222/Z/a;-><init>()V

    .line 585
    invoke-static {p0, v0, p1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;Ljava/io/Writer;Ljava/lang/String;)V

    .line 586
    invoke-virtual {v0}, Ldbxyzptlk/db231222/Z/a;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/io/Closeable;)V
    .locals 1

    .prologue
    .line 280
    if-eqz p0, :cond_0

    .line 281
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 286
    :cond_0
    :goto_0
    return-void

    .line 283
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static a(Ljava/io/InputStream;)V
    .locals 0

    .prologue
    .line 224
    invoke-static {p0}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/Closeable;)V

    .line 225
    return-void
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/Writer;)V
    .locals 1

    .prologue
    .line 1407
    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, p0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 1408
    invoke-static {v0, p1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/Reader;Ljava/io/Writer;)I

    .line 1409
    return-void
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/Writer;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1432
    if-nez p2, :cond_0

    .line 1433
    invoke-static {p0, p1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;Ljava/io/Writer;)V

    .line 1438
    :goto_0
    return-void

    .line 1435
    :cond_0
    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, p0, p2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 1436
    invoke-static {v0, p1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/Reader;Ljava/io/Writer;)I

    goto :goto_0
.end method

.method public static a(Ljava/io/OutputStream;)V
    .locals 0

    .prologue
    .line 252
    invoke-static {p0}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/Closeable;)V

    .line 253
    return-void
.end method

.method public static a(Ljava/io/Reader;)V
    .locals 0

    .prologue
    .line 171
    invoke-static {p0}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/Closeable;)V

    .line 172
    return-void
.end method

.method public static a(Ljava/io/Writer;)V
    .locals 0

    .prologue
    .line 197
    invoke-static {p0}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/Closeable;)V

    .line 198
    return-void
.end method

.method public static a(Ljava/net/Socket;)V
    .locals 1

    .prologue
    .line 312
    if-eqz p0, :cond_0

    .line 314
    :try_start_0
    invoke-virtual {p0}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 319
    :cond_0
    :goto_0
    return-void

    .line 315
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static a(Ljava/io/InputStream;I)[B
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 403
    if-gez p1, :cond_0

    .line 404
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Size must be equal or greater than zero: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 407
    :cond_0
    if-nez p1, :cond_2

    .line 408
    new-array v0, v1, [B

    .line 423
    :cond_1
    return-object v0

    .line 411
    :cond_2
    new-array v0, p1, [B

    .line 415
    :goto_0
    if-ge v1, p1, :cond_3

    sub-int v2, p1, v1

    invoke-virtual {p0, v0, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    .line 416
    add-int/2addr v1, v2

    goto :goto_0

    .line 419
    :cond_3
    if-eq v1, p1, :cond_1

    .line 420
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected readed size. current: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", excepted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Ljava/io/InputStream;J)[B
    .locals 3

    .prologue
    .line 383
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 384
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Size cannot be greater than Integer max value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 387
    :cond_0
    long-to-int v0, p1

    invoke-static {p0, v0}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;I)[B

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    .locals 5

    .prologue
    .line 1380
    const/16 v0, 0x1000

    new-array v2, v0, [B

    .line 1381
    const-wide/16 v0, 0x0

    .line 1383
    :goto_0
    const/4 v3, -0x1

    invoke-virtual {p0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v4

    if-eq v3, v4, :cond_0

    .line 1384
    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3, v4}, Ljava/io/OutputStream;->write([BII)V

    .line 1385
    int-to-long v3, v4

    add-long/2addr v0, v3

    goto :goto_0

    .line 1387
    :cond_0
    return-wide v0
.end method

.method public static b(Ljava/io/Reader;Ljava/io/Writer;)J
    .locals 5

    .prologue
    .line 1482
    const/16 v0, 0x1000

    new-array v2, v0, [C

    .line 1483
    const-wide/16 v0, 0x0

    .line 1485
    :goto_0
    const/4 v3, -0x1

    invoke-virtual {p0, v2}, Ljava/io/Reader;->read([C)I

    move-result v4

    if-eq v3, v4, :cond_0

    .line 1486
    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3, v4}, Ljava/io/Writer;->write([CII)V

    .line 1487
    int-to-long v3, v4

    add-long/2addr v0, v3

    goto :goto_0

    .line 1489
    :cond_0
    return-wide v0
.end method
