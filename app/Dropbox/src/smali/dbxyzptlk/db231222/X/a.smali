.class public final Ldbxyzptlk/db231222/X/a;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Ldbxyzptlk/db231222/X/a;

.field public static final b:Ldbxyzptlk/db231222/X/a;

.field public static final c:Ldbxyzptlk/db231222/X/a;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final d:Ljava/lang/String;

.field private final e:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 35
    new-instance v0, Ldbxyzptlk/db231222/X/a;

    const-string v1, "UTF-8"

    const/4 v2, 0x3

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/X/a;-><init>(Ljava/lang/String;[I)V

    sput-object v0, Ldbxyzptlk/db231222/X/a;->a:Ldbxyzptlk/db231222/X/a;

    .line 37
    new-instance v0, Ldbxyzptlk/db231222/X/a;

    const-string v1, "UTF-16BE"

    new-array v2, v3, [I

    fill-array-data v2, :array_1

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/X/a;-><init>(Ljava/lang/String;[I)V

    sput-object v0, Ldbxyzptlk/db231222/X/a;->b:Ldbxyzptlk/db231222/X/a;

    .line 39
    new-instance v0, Ldbxyzptlk/db231222/X/a;

    const-string v1, "UTF-16LE"

    new-array v2, v3, [I

    fill-array-data v2, :array_2

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/X/a;-><init>(Ljava/lang/String;[I)V

    sput-object v0, Ldbxyzptlk/db231222/X/a;->c:Ldbxyzptlk/db231222/X/a;

    return-void

    .line 35
    nop

    :array_0
    .array-data 4
        0xef
        0xbb
        0xbf
    .end array-data

    .line 37
    :array_1
    .array-data 4
        0xfe
        0xff
    .end array-data

    .line 39
    :array_2
    .array-data 4
        0xff
        0xfe
    .end array-data
.end method

.method public varargs constructor <init>(Ljava/lang/String;[I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 56
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No charsetName specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_1
    if-eqz p2, :cond_2

    array-length v0, p2

    if-nez v0, :cond_3

    .line 59
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No bytes specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_3
    iput-object p1, p0, Ldbxyzptlk/db231222/X/a;->d:Ljava/lang/String;

    .line 62
    array-length v0, p2

    new-array v0, v0, [I

    iput-object v0, p0, Ldbxyzptlk/db231222/X/a;->e:[I

    .line 63
    iget-object v0, p0, Ldbxyzptlk/db231222/X/a;->e:[I

    array-length v1, p2

    invoke-static {p2, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 64
    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Ldbxyzptlk/db231222/X/a;->e:[I

    aget v0, v0, p1

    return v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Ldbxyzptlk/db231222/X/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Ldbxyzptlk/db231222/X/a;->e:[I

    array-length v0, v0

    return v0
.end method

.method public final c()[B
    .locals 3

    .prologue
    .line 100
    iget-object v0, p0, Ldbxyzptlk/db231222/X/a;->e:[I

    array-length v0, v0

    new-array v1, v0, [B

    .line 101
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ldbxyzptlk/db231222/X/a;->e:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 102
    iget-object v2, p0, Ldbxyzptlk/db231222/X/a;->e:[I

    aget v2, v2, v0

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 101
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 104
    :cond_0
    return-object v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 116
    instance-of v0, p1, Ldbxyzptlk/db231222/X/a;

    if-nez v0, :cond_1

    .line 128
    :cond_0
    :goto_0
    return v1

    .line 119
    :cond_1
    check-cast p1, Ldbxyzptlk/db231222/X/a;

    .line 120
    iget-object v0, p0, Ldbxyzptlk/db231222/X/a;->e:[I

    array-length v0, v0

    invoke-virtual {p1}, Ldbxyzptlk/db231222/X/a;->b()I

    move-result v2

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 123
    :goto_1
    iget-object v2, p0, Ldbxyzptlk/db231222/X/a;->e:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 124
    iget-object v2, p0, Ldbxyzptlk/db231222/X/a;->e:[I

    aget v2, v2, v0

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/X/a;->a(I)I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 123
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 128
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    .line 139
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    .line 140
    iget-object v2, p0, Ldbxyzptlk/db231222/X/a;->e:[I

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    .line 141
    add-int/2addr v1, v4

    .line 140
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 143
    :cond_0
    return v1
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 153
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 154
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    const/16 v0, 0x5b

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 156
    iget-object v0, p0, Ldbxyzptlk/db231222/X/a;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    const-string v0, ": "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ldbxyzptlk/db231222/X/a;->e:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 159
    if-lez v0, :cond_0

    .line 160
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    :cond_0
    const-string v2, "0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    iget-object v2, p0, Ldbxyzptlk/db231222/X/a;->e:[I

    aget v2, v2, v0

    and-int/lit16 v2, v2, 0xff

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 165
    :cond_1
    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 166
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
