.class public final Ldbxyzptlk/db231222/X/d;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Ldbxyzptlk/db231222/X/d;

.field public static final b:Ldbxyzptlk/db231222/X/d;

.field public static final c:Ldbxyzptlk/db231222/X/d;

.field private static final serialVersionUID:J = -0x580776bc651386cfL


# instance fields
.field private final d:Ljava/lang/String;

.field private final transient e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 43
    new-instance v2, Ldbxyzptlk/db231222/X/d;

    const-string v3, "Sensitive"

    invoke-direct {v2, v3, v0}, Ldbxyzptlk/db231222/X/d;-><init>(Ljava/lang/String;Z)V

    sput-object v2, Ldbxyzptlk/db231222/X/d;->a:Ldbxyzptlk/db231222/X/d;

    .line 48
    new-instance v2, Ldbxyzptlk/db231222/X/d;

    const-string v3, "Insensitive"

    invoke-direct {v2, v3, v1}, Ldbxyzptlk/db231222/X/d;-><init>(Ljava/lang/String;Z)V

    sput-object v2, Ldbxyzptlk/db231222/X/d;->b:Ldbxyzptlk/db231222/X/d;

    .line 62
    new-instance v2, Ldbxyzptlk/db231222/X/d;

    const-string v3, "System"

    invoke-static {}, Ldbxyzptlk/db231222/X/c;->a()Z

    move-result v4

    if-nez v4, :cond_0

    :goto_0
    invoke-direct {v2, v3, v0}, Ldbxyzptlk/db231222/X/d;-><init>(Ljava/lang/String;Z)V

    sput-object v2, Ldbxyzptlk/db231222/X/d;->c:Ldbxyzptlk/db231222/X/d;

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    iput-object p1, p0, Ldbxyzptlk/db231222/X/d;->d:Ljava/lang/String;

    .line 103
    iput-boolean p2, p0, Ldbxyzptlk/db231222/X/d;->e:Z

    .line 104
    return-void
.end method

.method public static a(Ljava/lang/String;)Ldbxyzptlk/db231222/X/d;
    .locals 3

    .prologue
    .line 82
    sget-object v0, Ldbxyzptlk/db231222/X/d;->a:Ldbxyzptlk/db231222/X/d;

    iget-object v0, v0, Ldbxyzptlk/db231222/X/d;->d:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    sget-object v0, Ldbxyzptlk/db231222/X/d;->a:Ldbxyzptlk/db231222/X/d;

    .line 89
    :goto_0
    return-object v0

    .line 85
    :cond_0
    sget-object v0, Ldbxyzptlk/db231222/X/d;->b:Ldbxyzptlk/db231222/X/d;

    iget-object v0, v0, Ldbxyzptlk/db231222/X/d;->d:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    sget-object v0, Ldbxyzptlk/db231222/X/d;->b:Ldbxyzptlk/db231222/X/d;

    goto :goto_0

    .line 88
    :cond_1
    sget-object v0, Ldbxyzptlk/db231222/X/d;->c:Ldbxyzptlk/db231222/X/d;

    iget-object v0, v0, Ldbxyzptlk/db231222/X/d;->d:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 89
    sget-object v0, Ldbxyzptlk/db231222/X/d;->c:Ldbxyzptlk/db231222/X/d;

    goto :goto_0

    .line 91
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid IOCase name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Ldbxyzptlk/db231222/X/d;->d:Ljava/lang/String;

    invoke-static {v0}, Ldbxyzptlk/db231222/X/d;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/X/d;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 166
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 167
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "The strings must not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :cond_1
    iget-boolean v0, p0, Ldbxyzptlk/db231222/X/d;->e:Z

    if-eqz v0, :cond_2

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_2
    invoke-virtual {p1, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Ldbxyzptlk/db231222/X/d;->d:Ljava/lang/String;

    return-object v0
.end method
