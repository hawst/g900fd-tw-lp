.class public final Ldbxyzptlk/db231222/z/d;
.super Ljava/io/OutputStream;
.source "panda.py"


# instance fields
.field private final a:Ljava/security/MessageDigest;

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ldbxyzptlk/db231222/z/c;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:J

.field private e:Ljava/security/MessageDigest;

.field private f:Ljava/security/MessageDigest;

.field private g:I

.field private final h:[B


# direct methods
.method public constructor <init>(J)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 314
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 300
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/z/d;->b:Ljava/util/ArrayList;

    .line 301
    iput v2, p0, Ldbxyzptlk/db231222/z/d;->c:I

    .line 302
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ldbxyzptlk/db231222/z/d;->d:J

    .line 304
    iput-object v3, p0, Ldbxyzptlk/db231222/z/d;->e:Ljava/security/MessageDigest;

    .line 305
    iput-object v3, p0, Ldbxyzptlk/db231222/z/d;->f:Ljava/security/MessageDigest;

    .line 306
    iput v2, p0, Ldbxyzptlk/db231222/z/d;->g:I

    .line 367
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Ldbxyzptlk/db231222/z/d;->h:[B

    .line 316
    :try_start_0
    const-string v0, "SHA-256"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/z/d;->a:Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 320
    iput-wide p1, p0, Ldbxyzptlk/db231222/z/d;->d:J

    .line 321
    return-void

    .line 317
    :catch_0
    move-exception v0

    .line 318
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static b(Ljava/security/MessageDigest;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 376
    invoke-virtual {p0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    const/16 v1, 0xb

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 380
    iget-object v0, p0, Ldbxyzptlk/db231222/z/d;->a:Ljava/security/MessageDigest;

    invoke-static {v0}, Ldbxyzptlk/db231222/z/d;->b(Ljava/security/MessageDigest;)Ljava/lang/String;

    move-result-object v1

    .line 381
    iget-object v7, p0, Ldbxyzptlk/db231222/z/d;->b:Ljava/util/ArrayList;

    new-instance v0, Ldbxyzptlk/db231222/z/c;

    iget-wide v2, p0, Ldbxyzptlk/db231222/z/d;->d:J

    iget v4, p0, Ldbxyzptlk/db231222/z/d;->c:I

    int-to-long v4, v4

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/z/c;-><init>(Ljava/lang/String;JJZ)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 382
    invoke-static {}, Ldbxyzptlk/db231222/z/a;->e()Ljava/lang/String;

    move-result-object v0

    const-string v2, "%d: %s %d %d"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Ldbxyzptlk/db231222/z/d;->b:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    aput-object v1, v3, v6

    const/4 v1, 0x2

    iget-wide v4, p0, Ldbxyzptlk/db231222/z/d;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x3

    iget v4, p0, Ldbxyzptlk/db231222/z/d;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    iget-object v0, p0, Ldbxyzptlk/db231222/z/d;->a:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->reset()V

    .line 384
    iget-wide v0, p0, Ldbxyzptlk/db231222/z/d;->d:J

    iget v2, p0, Ldbxyzptlk/db231222/z/d;->c:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Ldbxyzptlk/db231222/z/d;->d:J

    .line 385
    iput v8, p0, Ldbxyzptlk/db231222/z/d;->c:I

    .line 386
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ldbxyzptlk/db231222/z/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 340
    iget-object v0, p0, Ldbxyzptlk/db231222/z/d;->b:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final a(Ljava/io/FileInputStream;JLdbxyzptlk/db231222/v/s;)V
    .locals 16

    .prologue
    .line 397
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v11

    .line 399
    const/16 v3, 0x1000

    new-array v13, v3, [B

    .line 401
    if-eqz p4, :cond_1

    invoke-virtual/range {p4 .. p4}, Ldbxyzptlk/db231222/v/s;->a()J

    move-result-wide v3

    .line 402
    :goto_0
    const-wide/16 v5, 0x0

    move-wide/from16 v7, p2

    .line 403
    :cond_0
    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/io/FileInputStream;->read([B)I

    move-result v9

    if-ltz v9, :cond_2

    .line 404
    const/4 v10, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v10, v9}, Ldbxyzptlk/db231222/z/d;->write([BII)V

    .line 405
    int-to-long v9, v9

    sub-long/2addr v7, v9

    .line 406
    if-eqz p4, :cond_0

    .line 407
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    .line 408
    sub-long v14, v9, v5

    cmp-long v14, v14, v3

    if-lez v14, :cond_0

    .line 410
    sub-long v5, p2, v7

    move-object/from16 v0, p4

    move-wide/from16 v1, p2

    invoke-virtual {v0, v5, v6, v1, v2}, Ldbxyzptlk/db231222/v/s;->a(JJ)V

    move-wide v5, v9

    goto :goto_1

    .line 401
    :cond_1
    const-wide/16 v3, 0x0

    goto :goto_0

    .line 415
    :cond_2
    invoke-virtual/range {p0 .. p0}, Ldbxyzptlk/db231222/z/d;->flush()V

    .line 416
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v3

    .line 417
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->p()Lcom/dropbox/android/util/analytics/l;

    move-result-object v5

    const-string v6, "blocks"

    move-object/from16 v0, p0

    iget-object v7, v0, Ldbxyzptlk/db231222/z/d;->b:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    int-to-long v7, v7

    invoke-virtual {v5, v6, v7, v8}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v5

    const-string v6, "time"

    sub-long/2addr v3, v11

    long-to-double v3, v3

    const-wide v7, 0x41cdcd6500000000L    # 1.0E9

    div-double/2addr v3, v7

    invoke-virtual {v5, v6, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;D)Lcom/dropbox/android/util/analytics/l;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 418
    return-void
.end method

.method public final a(Ljava/security/MessageDigest;)V
    .locals 0

    .prologue
    .line 324
    iput-object p1, p0, Ldbxyzptlk/db231222/z/d;->e:Ljava/security/MessageDigest;

    .line 325
    return-void
.end method

.method public final a(Ljava/security/MessageDigest;J)V
    .locals 3

    .prologue
    .line 328
    iput-object p1, p0, Ldbxyzptlk/db231222/z/d;->f:Ljava/security/MessageDigest;

    .line 329
    const-wide/16 v0, 0x2000

    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Ldbxyzptlk/db231222/z/d;->g:I

    .line 332
    long-to-int v0, p2

    .line 333
    iget-object v1, p0, Ldbxyzptlk/db231222/z/d;->f:Ljava/security/MessageDigest;

    ushr-int/lit8 v2, v0, 0x18

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Ljava/security/MessageDigest;->update(B)V

    .line 334
    iget-object v1, p0, Ldbxyzptlk/db231222/z/d;->f:Ljava/security/MessageDigest;

    ushr-int/lit8 v2, v0, 0x10

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Ljava/security/MessageDigest;->update(B)V

    .line 335
    iget-object v1, p0, Ldbxyzptlk/db231222/z/d;->f:Ljava/security/MessageDigest;

    ushr-int/lit8 v2, v0, 0x8

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Ljava/security/MessageDigest;->update(B)V

    .line 336
    iget-object v1, p0, Ldbxyzptlk/db231222/z/d;->f:Ljava/security/MessageDigest;

    int-to-byte v0, v0

    invoke-virtual {v1, v0}, Ljava/security/MessageDigest;->update(B)V

    .line 337
    return-void
.end method

.method public final flush()V
    .locals 1

    .prologue
    .line 390
    iget v0, p0, Ldbxyzptlk/db231222/z/d;->c:I

    if-lez v0, :cond_0

    .line 391
    invoke-direct {p0}, Ldbxyzptlk/db231222/z/d;->b()V

    .line 393
    :cond_0
    return-void
.end method

.method public final write(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 371
    iget-object v0, p0, Ldbxyzptlk/db231222/z/d;->h:[B

    int-to-byte v1, p1

    aput-byte v1, v0, v2

    .line 372
    iget-object v0, p0, Ldbxyzptlk/db231222/z/d;->h:[B

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v2, v1}, Ldbxyzptlk/db231222/z/d;->write([BII)V

    .line 373
    return-void
.end method

.method public final write([BII)V
    .locals 3

    .prologue
    const/high16 v2, 0x400000

    .line 345
    iget-object v0, p0, Ldbxyzptlk/db231222/z/d;->e:Ljava/security/MessageDigest;

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Ldbxyzptlk/db231222/z/d;->e:Ljava/security/MessageDigest;

    invoke-virtual {v0, p1, p2, p3}, Ljava/security/MessageDigest;->update([BII)V

    .line 348
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/z/d;->f:Ljava/security/MessageDigest;

    if-eqz v0, :cond_1

    iget v0, p0, Ldbxyzptlk/db231222/z/d;->g:I

    if-lez v0, :cond_1

    .line 349
    iget v0, p0, Ldbxyzptlk/db231222/z/d;->g:I

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 350
    iget-object v1, p0, Ldbxyzptlk/db231222/z/d;->f:Ljava/security/MessageDigest;

    invoke-virtual {v1, p1, p2, v0}, Ljava/security/MessageDigest;->update([BII)V

    .line 351
    iget v1, p0, Ldbxyzptlk/db231222/z/d;->g:I

    sub-int v0, v1, v0

    iput v0, p0, Ldbxyzptlk/db231222/z/d;->g:I

    .line 354
    :cond_1
    :goto_0
    if-lez p3, :cond_2

    .line 355
    iget v0, p0, Ldbxyzptlk/db231222/z/d;->c:I

    sub-int v0, v2, v0

    .line 356
    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 357
    iget-object v1, p0, Ldbxyzptlk/db231222/z/d;->a:Ljava/security/MessageDigest;

    invoke-virtual {v1, p1, p2, v0}, Ljava/security/MessageDigest;->update([BII)V

    .line 358
    sub-int/2addr p3, v0

    .line 359
    add-int/2addr p2, v0

    .line 360
    iget v1, p0, Ldbxyzptlk/db231222/z/d;->c:I

    add-int/2addr v0, v1

    iput v0, p0, Ldbxyzptlk/db231222/z/d;->c:I

    .line 361
    iget v0, p0, Ldbxyzptlk/db231222/z/d;->c:I

    if-ne v0, v2, :cond_1

    .line 362
    invoke-direct {p0}, Ldbxyzptlk/db231222/z/d;->b()V

    goto :goto_0

    .line 365
    :cond_2
    return-void
.end method
