.class final Ldbxyzptlk/db231222/z/al;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/z/aE;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RETURN_T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ldbxyzptlk/db231222/z/aE",
        "<TRETURN_T;>;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/io/FileInputStream;

.field private final c:Ldbxyzptlk/db231222/v/s;

.field private final d:Ldbxyzptlk/db231222/z/a;

.field private final e:Ldbxyzptlk/db231222/z/M;

.field private final f:Ldbxyzptlk/db231222/z/am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/z/am",
            "<TRETURN_T;>;"
        }
    .end annotation
.end field

.field private final g:Ldbxyzptlk/db231222/z/Q;

.field private h:Ldbxyzptlk/db231222/z/ab;

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1282
    const-class v0, Ldbxyzptlk/db231222/z/al;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/z/al;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/FileInputStream;Ldbxyzptlk/db231222/z/g;Ldbxyzptlk/db231222/z/M;Ldbxyzptlk/db231222/z/am;Ldbxyzptlk/db231222/z/Q;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/FileInputStream;",
            "Ldbxyzptlk/db231222/z/g;",
            "Ldbxyzptlk/db231222/z/M;",
            "Ldbxyzptlk/db231222/z/am",
            "<TRETURN_T;>;",
            "Ldbxyzptlk/db231222/z/Q;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1309
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/z/al;->h:Ldbxyzptlk/db231222/z/ab;

    .line 1310
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/z/al;->i:Z

    .line 1300
    iput-object p1, p0, Ldbxyzptlk/db231222/z/al;->b:Ljava/io/FileInputStream;

    .line 1301
    iget-object v0, p2, Ldbxyzptlk/db231222/z/g;->d:Ldbxyzptlk/db231222/v/s;

    iput-object v0, p0, Ldbxyzptlk/db231222/z/al;->c:Ldbxyzptlk/db231222/v/s;

    .line 1302
    iget-object v0, p2, Ldbxyzptlk/db231222/z/g;->a:Ldbxyzptlk/db231222/z/a;

    iput-object v0, p0, Ldbxyzptlk/db231222/z/al;->d:Ldbxyzptlk/db231222/z/a;

    .line 1303
    iput-object p3, p0, Ldbxyzptlk/db231222/z/al;->e:Ldbxyzptlk/db231222/z/M;

    .line 1304
    iput-object p4, p0, Ldbxyzptlk/db231222/z/al;->f:Ldbxyzptlk/db231222/z/am;

    .line 1305
    iput-object p5, p0, Ldbxyzptlk/db231222/z/al;->g:Ldbxyzptlk/db231222/z/Q;

    .line 1306
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const-wide/16 v1, -0x1

    .line 1313
    monitor-enter p0

    .line 1314
    :try_start_0
    iget-boolean v0, p0, Ldbxyzptlk/db231222/z/al;->i:Z

    if-eqz v0, :cond_0

    .line 1315
    new-instance v0, Ldbxyzptlk/db231222/w/g;

    const-wide/16 v1, -0x1

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/w/g;-><init>(J)V

    throw v0

    .line 1317
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1318
    iget-object v0, p0, Ldbxyzptlk/db231222/z/al;->g:Ldbxyzptlk/db231222/z/Q;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldbxyzptlk/db231222/z/al;->g:Ldbxyzptlk/db231222/z/Q;

    invoke-interface {v0}, Ldbxyzptlk/db231222/z/Q;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1319
    new-instance v0, Ldbxyzptlk/db231222/w/g;

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/w/g;-><init>(J)V

    throw v0

    .line 1321
    :cond_1
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 1376
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/z/al;->h:Ldbxyzptlk/db231222/z/ab;

    if-eqz v0, :cond_0

    .line 1377
    iget-object v0, p0, Ldbxyzptlk/db231222/z/al;->h:Ldbxyzptlk/db231222/z/ab;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/ab;->a()V

    .line 1379
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/db231222/z/al;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1380
    monitor-exit p0

    return-void

    .line 1376
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TRETURN_T;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 1325
    invoke-direct {p0}, Ldbxyzptlk/db231222/z/al;->b()V

    .line 1327
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/z/al;->f:Ldbxyzptlk/db231222/z/am;

    iget-object v1, p0, Ldbxyzptlk/db231222/z/al;->d:Ldbxyzptlk/db231222/z/a;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/z/a;->d()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/db231222/z/al;->d:Ldbxyzptlk/db231222/z/a;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/z/a;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Ldbxyzptlk/db231222/z/am;->b(Ljava/util/List;J)Ljava/lang/Object;
    :try_end_0
    .catch Ldbxyzptlk/db231222/z/ao; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1362
    :goto_0
    return-object v0

    .line 1328
    :catch_0
    move-exception v0

    .line 1329
    iget-object v1, p0, Ldbxyzptlk/db231222/z/al;->d:Ldbxyzptlk/db231222/z/a;

    iget-object v0, v0, Ldbxyzptlk/db231222/z/ao;->a:Ljava/util/List;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/z/a;->a(Ljava/util/Collection;)V

    .line 1333
    :try_start_1
    iget-object v0, p0, Ldbxyzptlk/db231222/z/al;->d:Ldbxyzptlk/db231222/z/a;

    iget-object v1, p0, Ldbxyzptlk/db231222/z/al;->c:Ldbxyzptlk/db231222/v/s;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/z/a;->a(Ldbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/v/s;

    move-result-object v5

    move v6, v7

    .line 1335
    :goto_1
    iget-object v0, p0, Ldbxyzptlk/db231222/z/al;->d:Ldbxyzptlk/db231222/z/a;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/a;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1336
    iget-object v0, p0, Ldbxyzptlk/db231222/z/al;->d:Ldbxyzptlk/db231222/z/a;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/a;->b()Ldbxyzptlk/db231222/z/c;

    move-result-object v4

    .line 1337
    iget-object v0, p0, Ldbxyzptlk/db231222/z/al;->b:Ljava/io/FileInputStream;

    invoke-virtual {v0}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    iget-wide v1, v4, Ldbxyzptlk/db231222/z/c;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 1338
    monitor-enter p0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1339
    :try_start_2
    invoke-direct {p0}, Ldbxyzptlk/db231222/z/al;->b()V

    .line 1340
    iget-object v0, p0, Ldbxyzptlk/db231222/z/al;->e:Ldbxyzptlk/db231222/z/M;

    iget-object v1, p0, Ldbxyzptlk/db231222/z/al;->b:Ljava/io/FileInputStream;

    iget-wide v2, v4, Ldbxyzptlk/db231222/z/c;->c:J

    iget-object v4, v4, Ldbxyzptlk/db231222/z/c;->a:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/db231222/z/M;->a(Ldbxyzptlk/db231222/z/M;Ljava/io/InputStream;JLjava/lang/String;Ldbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/z/ab;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/z/al;->h:Ldbxyzptlk/db231222/z/ab;

    .line 1341
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1342
    :try_start_3
    sget-object v0, Ldbxyzptlk/db231222/z/al;->a:Ljava/lang/String;

    const-string v1, "Starting block upload.."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 1344
    :try_start_4
    iget-object v0, p0, Ldbxyzptlk/db231222/z/al;->h:Ldbxyzptlk/db231222/z/ab;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/ab;->b()Ljava/lang/String;

    move-result-object v0

    .line 1345
    iget-object v1, p0, Ldbxyzptlk/db231222/z/al;->d:Ldbxyzptlk/db231222/z/a;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/z/a;->a(Ljava/lang/String;)V

    .line 1346
    monitor-enter p0
    :try_end_4
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 1347
    const/4 v0, 0x0

    :try_start_5
    iput-object v0, p0, Ldbxyzptlk/db231222/z/al;->h:Ldbxyzptlk/db231222/z/ab;

    .line 1348
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move v6, v7

    .line 1359
    goto :goto_1

    .line 1341
    :catchall_0
    move-exception v0

    :try_start_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    .line 1369
    :catch_1
    move-exception v0

    .line 1370
    new-instance v1, Ldbxyzptlk/db231222/w/d;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/w/d;-><init>(Ljava/io/IOException;)V

    throw v1

    .line 1348
    :catchall_1
    move-exception v0

    :try_start_8
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    throw v0
    :try_end_9
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1

    .line 1349
    :catch_2
    move-exception v0

    .line 1350
    :try_start_a
    sget-object v1, Ldbxyzptlk/db231222/z/al;->a:Ljava/lang/String;

    const-string v2, "Block upload error"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1351
    add-int/lit8 v1, v6, 0x1

    .line 1352
    const/4 v2, 0x3

    if-ge v1, v2, :cond_0

    .line 1353
    sget-object v0, Ldbxyzptlk/db231222/z/al;->a:Ljava/lang/String;

    const-string v2, "Trying again.."

    invoke-static {v0, v2}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v1

    .line 1354
    goto :goto_1

    .line 1356
    :cond_0
    throw v0

    .line 1360
    :cond_1
    invoke-direct {p0}, Ldbxyzptlk/db231222/z/al;->b()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1

    .line 1362
    :try_start_b
    iget-object v0, p0, Ldbxyzptlk/db231222/z/al;->f:Ldbxyzptlk/db231222/z/am;

    iget-object v1, p0, Ldbxyzptlk/db231222/z/al;->d:Ldbxyzptlk/db231222/z/a;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/z/a;->d()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/db231222/z/al;->d:Ldbxyzptlk/db231222/z/a;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/z/a;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Ldbxyzptlk/db231222/z/am;->b(Ljava/util/List;J)Ljava/lang/Object;
    :try_end_b
    .catch Ldbxyzptlk/db231222/z/ao; {:try_start_b .. :try_end_b} :catch_3
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1

    move-result-object v0

    goto/16 :goto_0

    .line 1363
    :catch_3
    move-exception v0

    .line 1366
    :try_start_c
    sget-object v1, Ldbxyzptlk/db231222/z/al;->a:Ljava/lang/String;

    const-string v2, "NeedBlocksException when we just uploaded everything"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1367
    new-instance v0, Ldbxyzptlk/db231222/w/a;

    const-string v1, "NeedBlocksException when we just uploaded everything"

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/w/a;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1
.end method
