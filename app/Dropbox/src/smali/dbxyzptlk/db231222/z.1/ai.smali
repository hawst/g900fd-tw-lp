.class public final Ldbxyzptlk/db231222/z/ai;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final g:Ldbxyzptlk/db231222/x/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/x/c",
            "<",
            "Ldbxyzptlk/db231222/z/ai;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/util/Date;

.field public final d:Ljava/lang/String;

.field public final e:Z

.field public final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 459
    new-instance v0, Ldbxyzptlk/db231222/z/aj;

    invoke-direct {v0}, Ldbxyzptlk/db231222/z/aj;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/z/ai;->g:Ldbxyzptlk/db231222/x/c;

    return-void
.end method

.method private constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 439
    const-string v0, "url"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/db231222/z/ai;->a:Ljava/lang/String;

    .line 440
    const-string v0, "container"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/db231222/z/ai;->d:Ljava/lang/String;

    .line 441
    const-string v0, "metadata_url"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/db231222/z/ai;->b:Ljava/lang/String;

    .line 442
    const-string v0, "progress_url"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/db231222/z/ai;->f:Ljava/lang/String;

    .line 444
    const-string v0, "expires"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 445
    if-eqz v0, :cond_0

    .line 446
    invoke-static {v0}, Ldbxyzptlk/db231222/v/v;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/z/ai;->c:Ljava/util/Date;

    .line 451
    :goto_0
    const-string v0, "can_seek"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 452
    if-eqz v0, :cond_1

    .line 453
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/z/ai;->e:Z

    .line 457
    :goto_1
    return-void

    .line 448
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/z/ai;->c:Ljava/util/Date;

    goto :goto_0

    .line 455
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/z/ai;->e:Z

    goto :goto_1
.end method

.method synthetic constructor <init>(Ljava/util/Map;Ldbxyzptlk/db231222/z/N;)V
    .locals 0

    .prologue
    .line 401
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/z/ai;-><init>(Ljava/util/Map;)V

    return-void
.end method
