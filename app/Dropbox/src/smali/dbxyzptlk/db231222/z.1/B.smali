.class public final Ldbxyzptlk/db231222/z/B;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static u:Ljava/lang/Object;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Z

.field public t:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/dropbox/android/util/bd;->M:Ljava/lang/Object;

    sput-object v0, Ldbxyzptlk/db231222/z/B;->u:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-string v0, ""

    iput-object v0, p0, Ldbxyzptlk/db231222/z/B;->c:Ljava/lang/String;

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/z/B;->t:Ljava/util/Map;

    return-void
.end method

.method public static a(Landroid/content/Context;Ldbxyzptlk/db231222/i/c;Ldbxyzptlk/db231222/r/d;)Ldbxyzptlk/db231222/z/B;
    .locals 5

    .prologue
    .line 114
    new-instance v1, Ldbxyzptlk/db231222/z/B;

    invoke-direct {v1}, Ldbxyzptlk/db231222/z/B;-><init>()V

    .line 115
    invoke-virtual {p2}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Ldbxyzptlk/db231222/z/B;->a:Ljava/lang/String;

    .line 116
    iget-object v0, p1, Ldbxyzptlk/db231222/i/c;->e:Ljava/lang/String;

    iput-object v0, v1, Ldbxyzptlk/db231222/z/B;->b:Ljava/lang/String;

    .line 117
    iget-object v0, p1, Ldbxyzptlk/db231222/i/c;->a:Ljava/lang/String;

    iput-object v0, v1, Ldbxyzptlk/db231222/z/B;->e:Ljava/lang/String;

    .line 118
    iget-object v0, p1, Ldbxyzptlk/db231222/i/c;->c:Ljava/lang/String;

    iput-object v0, v1, Ldbxyzptlk/db231222/z/B;->g:Ljava/lang/String;

    .line 119
    iget-object v0, p1, Ldbxyzptlk/db231222/i/c;->d:Ljava/lang/String;

    iput-object v0, v1, Ldbxyzptlk/db231222/z/B;->h:Ljava/lang/String;

    .line 120
    iget-object v0, p1, Ldbxyzptlk/db231222/i/c;->f:Ljava/lang/String;

    iput-object v0, v1, Ldbxyzptlk/db231222/z/B;->d:Ljava/lang/String;

    .line 121
    invoke-static {}, Lcom/dropbox/android/util/E;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Ldbxyzptlk/db231222/z/B;->f:Ljava/lang/String;

    .line 122
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Ldbxyzptlk/db231222/z/B;->n:Ljava/lang/String;

    .line 123
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v0, v1, Ldbxyzptlk/db231222/z/B;->k:Ljava/lang/String;

    .line 125
    sget-object v0, Landroid/os/Build;->BRAND:Ljava/lang/String;

    iput-object v0, v1, Ldbxyzptlk/db231222/z/B;->o:Ljava/lang/String;

    .line 126
    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    iput-object v0, v1, Ldbxyzptlk/db231222/z/B;->p:Ljava/lang/String;

    .line 127
    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    iput-object v0, v1, Ldbxyzptlk/db231222/z/B;->q:Ljava/lang/String;

    .line 128
    invoke-static {}, Ldbxyzptlk/db231222/n/k;->a()Ldbxyzptlk/db231222/n/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/k;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Ldbxyzptlk/db231222/z/B;->r:Ljava/lang/String;

    .line 129
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "android_id"

    invoke-static {v0, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 131
    if-eqz v0, :cond_0

    .line 132
    iput-object v0, v1, Ldbxyzptlk/db231222/z/B;->l:Ljava/lang/String;

    .line 134
    :cond_0
    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 135
    if-eqz v0, :cond_1

    .line 136
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_1

    .line 138
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    .line 139
    if-eqz v0, :cond_1

    .line 141
    const-string v2, "\\W"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Ldbxyzptlk/db231222/z/B;->m:Ljava/lang/String;

    .line 146
    :cond_1
    invoke-virtual {p2}, Ldbxyzptlk/db231222/r/d;->o()Ldbxyzptlk/db231222/n/K;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/K;->d()Z

    move-result v0

    iput-boolean v0, v1, Ldbxyzptlk/db231222/z/B;->s:Z

    .line 148
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 149
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-static {v2}, Lcom/dropbox/android/util/bn;->a(Landroid/content/res/Configuration;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Ldbxyzptlk/db231222/z/B;->j:Ljava/lang/String;

    .line 150
    iget-object v2, v1, Ldbxyzptlk/db231222/z/B;->t:Ljava/util/Map;

    const-string v3, "opengl_version"

    invoke-static {}, Lcom/dropbox/android/util/bn;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    iget-object v2, v1, Ldbxyzptlk/db231222/z/B;->t:Ljava/util/Map;

    const-string v3, "density"

    invoke-static {v0}, Lcom/dropbox/android/util/bn;->a(Landroid/content/res/Resources;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    iget-object v0, v1, Ldbxyzptlk/db231222/z/B;->t:Ljava/util/Map;

    const-string v2, "api_level"

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    iget-object v0, v1, Ldbxyzptlk/db231222/z/B;->t:Ljava/util/Map;

    const-string v2, "arch"

    const-string v3, "os.arch"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    instance-of v0, p0, Landroid/content/ContextWrapper;

    if-eqz v0, :cond_2

    .line 156
    check-cast p0, Landroid/content/ContextWrapper;

    invoke-static {p0}, Ldbxyzptlk/db231222/z/B;->a(Landroid/content/ContextWrapper;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Ldbxyzptlk/db231222/z/B;->i:Ljava/lang/String;

    .line 159
    :cond_2
    return-object v1
.end method

.method private static a(Landroid/content/ContextWrapper;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 96
    :try_start_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x40

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 100
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v1, v0

    const/4 v2, 0x0

    if-ge v2, v1, :cond_0

    aget-object v0, v0, v2

    .line 103
    invoke-virtual {v0}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/z/B;->a([B)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 106
    :goto_0
    return-object v0

    .line 105
    :catch_0
    move-exception v0

    .line 106
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private static a([B)Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    :try_start_0
    sget-object v0, Ldbxyzptlk/db231222/z/B;->u:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 87
    invoke-virtual {v0, p0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/z/a;->a([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 90
    :goto_0
    return-object v0

    .line 88
    :catch_0
    move-exception v0

    .line 90
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Ldbxyzptlk/db231222/z/B;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/z/B;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/z/B;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/z/B;->a([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
