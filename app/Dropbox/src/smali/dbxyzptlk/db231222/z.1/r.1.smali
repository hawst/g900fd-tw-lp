.class public final Ldbxyzptlk/db231222/z/r;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final f:Ldbxyzptlk/db231222/x/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/x/c",
            "<",
            "Ldbxyzptlk/db231222/z/r;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ldbxyzptlk/db231222/z/x;

.field public final d:Ldbxyzptlk/db231222/z/t;

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/z/t;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 351
    new-instance v0, Ldbxyzptlk/db231222/z/s;

    invoke-direct {v0}, Ldbxyzptlk/db231222/z/s;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/z/r;->f:Ldbxyzptlk/db231222/x/c;

    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/x/k;)V
    .locals 3

    .prologue
    .line 332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333
    invoke-virtual {p1}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v0

    .line 335
    const-string v1, "id"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/x/k;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Ldbxyzptlk/db231222/z/r;->a:Ljava/lang/String;

    .line 337
    const-string v1, "sort_key"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/x/k;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Ldbxyzptlk/db231222/z/r;->b:Ljava/lang/String;

    .line 339
    const-string v1, "shared_folder_status"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->c(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v1

    invoke-static {v1}, Ldbxyzptlk/db231222/z/x;->a(Ldbxyzptlk/db231222/x/k;)Ldbxyzptlk/db231222/z/x;

    move-result-object v1

    iput-object v1, p0, Ldbxyzptlk/db231222/z/r;->c:Ldbxyzptlk/db231222/z/x;

    .line 341
    new-instance v1, Ldbxyzptlk/db231222/z/t;

    const-string v2, "cover_file"

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v2

    invoke-direct {v1, v2}, Ldbxyzptlk/db231222/z/t;-><init>(Ldbxyzptlk/db231222/x/k;)V

    iput-object v1, p0, Ldbxyzptlk/db231222/z/r;->d:Ldbxyzptlk/db231222/z/t;

    .line 343
    const-string v1, "other_files"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->c()Ldbxyzptlk/db231222/x/d;

    move-result-object v0

    sget-object v1, Ldbxyzptlk/db231222/z/t;->d:Ldbxyzptlk/db231222/x/c;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/d;->a(Ldbxyzptlk/db231222/x/c;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/z/r;->e:Ljava/util/List;

    .line 344
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 348
    invoke-static {p0}, Ldbxyzptlk/db231222/z/h;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
