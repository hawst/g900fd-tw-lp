.class public final enum Ldbxyzptlk/db231222/z/x;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldbxyzptlk/db231222/z/x;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ldbxyzptlk/db231222/z/x;

.field public static final enum b:Ldbxyzptlk/db231222/z/x;

.field public static final enum c:Ldbxyzptlk/db231222/z/x;

.field private static final synthetic e:[Ldbxyzptlk/db231222/z/x;


# instance fields
.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 273
    new-instance v0, Ldbxyzptlk/db231222/z/x;

    const-string v1, "ROOT"

    invoke-direct {v0, v1, v4, v2}, Ldbxyzptlk/db231222/z/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldbxyzptlk/db231222/z/x;->a:Ldbxyzptlk/db231222/z/x;

    .line 275
    new-instance v0, Ldbxyzptlk/db231222/z/x;

    const-string v1, "JOINED"

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/z/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldbxyzptlk/db231222/z/x;->b:Ldbxyzptlk/db231222/z/x;

    .line 277
    new-instance v0, Ldbxyzptlk/db231222/z/x;

    const-string v1, "OWNED"

    invoke-direct {v0, v1, v3, v5}, Ldbxyzptlk/db231222/z/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldbxyzptlk/db231222/z/x;->c:Ldbxyzptlk/db231222/z/x;

    .line 271
    new-array v0, v5, [Ldbxyzptlk/db231222/z/x;

    sget-object v1, Ldbxyzptlk/db231222/z/x;->a:Ldbxyzptlk/db231222/z/x;

    aput-object v1, v0, v4

    sget-object v1, Ldbxyzptlk/db231222/z/x;->b:Ldbxyzptlk/db231222/z/x;

    aput-object v1, v0, v2

    sget-object v1, Ldbxyzptlk/db231222/z/x;->c:Ldbxyzptlk/db231222/z/x;

    aput-object v1, v0, v3

    sput-object v0, Ldbxyzptlk/db231222/z/x;->e:[Ldbxyzptlk/db231222/z/x;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 285
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 286
    iput p3, p0, Ldbxyzptlk/db231222/z/x;->d:I

    .line 287
    return-void
.end method

.method static synthetic a(Ldbxyzptlk/db231222/x/k;)Ldbxyzptlk/db231222/z/x;
    .locals 1

    .prologue
    .line 271
    invoke-static {p0}, Ldbxyzptlk/db231222/z/x;->b(Ldbxyzptlk/db231222/x/k;)Ldbxyzptlk/db231222/z/x;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ldbxyzptlk/db231222/x/k;)Ldbxyzptlk/db231222/z/x;
    .locals 2

    .prologue
    .line 297
    invoke-virtual {p0}, Ldbxyzptlk/db231222/x/k;->h()Ljava/lang/String;

    move-result-object v0

    .line 298
    if-nez v0, :cond_0

    .line 299
    const/4 v0, 0x0

    .line 305
    :goto_0
    return-object v0

    .line 300
    :cond_0
    const-string v1, "root"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 301
    sget-object v0, Ldbxyzptlk/db231222/z/x;->a:Ldbxyzptlk/db231222/z/x;

    goto :goto_0

    .line 302
    :cond_1
    const-string v1, "joined"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 303
    sget-object v0, Ldbxyzptlk/db231222/z/x;->b:Ldbxyzptlk/db231222/z/x;

    goto :goto_0

    .line 304
    :cond_2
    const-string v1, "owned"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 305
    sget-object v0, Ldbxyzptlk/db231222/z/x;->c:Ldbxyzptlk/db231222/z/x;

    goto :goto_0

    .line 307
    :cond_3
    const-string v0, "Invalid value"

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/x/k;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/x/b;

    move-result-object v0

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Ldbxyzptlk/db231222/z/x;
    .locals 1

    .prologue
    .line 271
    const-class v0, Ldbxyzptlk/db231222/z/x;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/x;

    return-object v0
.end method

.method public static values()[Ldbxyzptlk/db231222/z/x;
    .locals 1

    .prologue
    .line 271
    sget-object v0, Ldbxyzptlk/db231222/z/x;->e:[Ldbxyzptlk/db231222/z/x;

    invoke-virtual {v0}, [Ldbxyzptlk/db231222/z/x;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/z/x;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 293
    iget v0, p0, Ldbxyzptlk/db231222/z/x;->d:I

    return v0
.end method
