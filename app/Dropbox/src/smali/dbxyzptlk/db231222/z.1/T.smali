.class final Ldbxyzptlk/db231222/z/T;
.super Ldbxyzptlk/db231222/x/c;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/x/c",
        "<",
        "Ldbxyzptlk/db231222/s/a;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Ldbxyzptlk/db231222/x/c;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/x/k;)Ldbxyzptlk/db231222/s/a;
    .locals 4

    .prologue
    .line 145
    invoke-virtual {p1}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v1

    .line 146
    invoke-static {}, Ldbxyzptlk/db231222/s/a;->I()Ldbxyzptlk/db231222/s/c;

    move-result-object v2

    .line 147
    sget-object v0, Ldbxyzptlk/db231222/z/R;->c:Ldbxyzptlk/db231222/x/c;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/x/c;->b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/s/q;

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/s/c;->a(Ldbxyzptlk/db231222/s/q;)Ldbxyzptlk/db231222/s/c;

    .line 148
    const-string v0, "user_name"

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/s/c;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/s/c;

    .line 149
    const-string v0, "org_name"

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->h()Ljava/lang/String;

    move-result-object v0

    .line 150
    if-eqz v0, :cond_0

    .line 151
    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/s/c;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/s/c;

    .line 154
    :cond_0
    const-string v0, "display_name"

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/s/c;->c(Ljava/lang/String;)Ldbxyzptlk/db231222/s/c;

    .line 155
    const-string v0, "country"

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/s/c;->d(Ljava/lang/String;)Ldbxyzptlk/db231222/s/c;

    .line 156
    const-string v0, "referral_link"

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/s/c;->e(Ljava/lang/String;)Ldbxyzptlk/db231222/s/c;

    .line 157
    invoke-static {}, Ldbxyzptlk/db231222/z/R;->c()Ldbxyzptlk/db231222/x/c;

    move-result-object v0

    const-string v3, "quota_info"

    invoke-virtual {v1, v3}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v3

    invoke-virtual {v0, v3}, Ldbxyzptlk/db231222/x/c;->b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/s/d;

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/s/c;->a(Ldbxyzptlk/db231222/s/d;)Ldbxyzptlk/db231222/s/c;

    .line 158
    const-string v0, "teams_member"

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->i()Z

    move-result v0

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/s/c;->a(Z)Ldbxyzptlk/db231222/s/c;

    .line 159
    const-string v0, "email_verified"

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->i()Z

    move-result v0

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/s/c;->b(Z)Ldbxyzptlk/db231222/s/c;

    .line 160
    const-string v0, "deal_eligible"

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->i()Z

    move-result v0

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/s/c;->e(Z)Ldbxyzptlk/db231222/s/c;

    .line 162
    const-string v0, "video_uploads_enabled_default"

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/g;->c(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 163
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->a()Z

    move-result v3

    if-nez v3, :cond_1

    .line 164
    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->i()Z

    move-result v0

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/s/c;->f(Z)Ldbxyzptlk/db231222/s/c;

    .line 167
    :cond_1
    invoke-static {}, Ldbxyzptlk/db231222/z/R;->d()Ldbxyzptlk/db231222/x/c;

    move-result-object v0

    const-string v3, "can_share_folders_outside_team"

    invoke-virtual {v1, v3}, Ldbxyzptlk/db231222/x/g;->c(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v3

    invoke-virtual {v0, v3}, Ldbxyzptlk/db231222/x/c;->b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/s/h;

    .line 168
    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/s/c;->a(Ldbxyzptlk/db231222/s/h;)Ldbxyzptlk/db231222/s/c;

    .line 170
    const-string v0, "has_user_added_files"

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/g;->c(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 171
    if-eqz v0, :cond_2

    .line 172
    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->i()Z

    move-result v0

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/s/c;->c(Z)Ldbxyzptlk/db231222/s/c;

    .line 174
    :cond_2
    const-string v0, "has_ever_linked_desktop"

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/g;->c(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 175
    if-eqz v0, :cond_3

    .line 176
    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->i()Z

    move-result v0

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/s/c;->d(Z)Ldbxyzptlk/db231222/s/c;

    .line 178
    :cond_3
    invoke-virtual {v2}, Ldbxyzptlk/db231222/s/c;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 142
    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/z/T;->a(Ldbxyzptlk/db231222/x/k;)Ldbxyzptlk/db231222/s/a;

    move-result-object v0

    return-object v0
.end method
