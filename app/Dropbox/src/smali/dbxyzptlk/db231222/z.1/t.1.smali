.class public final Ldbxyzptlk/db231222/z/t;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final d:Ldbxyzptlk/db231222/x/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/x/c",
            "<",
            "Ldbxyzptlk/db231222/z/t;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ldbxyzptlk/db231222/z/n;

.field public final b:Ldbxyzptlk/db231222/v/j;

.field public final c:Ldbxyzptlk/db231222/z/v;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 261
    new-instance v0, Ldbxyzptlk/db231222/z/u;

    invoke-direct {v0}, Ldbxyzptlk/db231222/z/u;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/z/t;->d:Ldbxyzptlk/db231222/x/c;

    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/x/k;)V
    .locals 3

    .prologue
    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247
    invoke-virtual {p1}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v1

    .line 249
    new-instance v0, Ldbxyzptlk/db231222/z/n;

    const-string v2, "item_file_metadata"

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v2

    invoke-direct {v0, v2}, Ldbxyzptlk/db231222/z/n;-><init>(Ldbxyzptlk/db231222/x/k;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/z/t;->a:Ldbxyzptlk/db231222/z/n;

    .line 251
    sget-object v0, Ldbxyzptlk/db231222/v/j;->r:Ldbxyzptlk/db231222/x/c;

    const-string v2, "api_file_metadata"

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v2

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/x/c;->b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/v/j;

    iput-object v0, p0, Ldbxyzptlk/db231222/z/t;->b:Ldbxyzptlk/db231222/v/j;

    .line 253
    new-instance v0, Ldbxyzptlk/db231222/z/v;

    const-string v2, "photo_metadata"

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/z/v;-><init>(Ldbxyzptlk/db231222/x/k;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/z/t;->c:Ldbxyzptlk/db231222/z/v;

    .line 254
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 258
    invoke-static {p0}, Ldbxyzptlk/db231222/z/h;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
