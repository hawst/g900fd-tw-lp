.class public final Ldbxyzptlk/db231222/z/M;
.super Ldbxyzptlk/db231222/z/C;
.source "panda.py"


# static fields
.field private static final b:Ldbxyzptlk/db231222/x/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/x/c",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ldbxyzptlk/db231222/ac/q;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1819
    new-instance v0, Ldbxyzptlk/db231222/z/P;

    invoke-direct {v0}, Ldbxyzptlk/db231222/z/P;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/z/M;->b:Ldbxyzptlk/db231222/x/c;

    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/z/D;Ldbxyzptlk/db231222/v/r;)V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/z/C;-><init>(Ldbxyzptlk/db231222/z/D;Ldbxyzptlk/db231222/v/r;)V

    .line 105
    return-void
.end method

.method static synthetic a(Lorg/apache/http/HttpResponse;Ljava/lang/String;F)F
    .locals 1

    .prologue
    .line 87
    invoke-static {p0, p1, p2}, Ldbxyzptlk/db231222/z/M;->b(Lorg/apache/http/HttpResponse;Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method private a(Lcom/dropbox/android/util/DropboxPath;Ljava/util/List;JZLjava/lang/String;)Ldbxyzptlk/db231222/v/j;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;JZ",
            "Ljava/lang/String;",
            ")",
            "Ldbxyzptlk/db231222/v/j;"
        }
    .end annotation

    .prologue
    .line 1246
    invoke-static {p1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;)V

    .line 1248
    invoke-direct {p0}, Ldbxyzptlk/db231222/z/M;->i()V

    .line 1250
    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->b()Ldbxyzptlk/db231222/z/D;

    move-result-object v5

    .line 1252
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/commit_file/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v5}, Ldbxyzptlk/db231222/y/n;->g()Ldbxyzptlk/db231222/y/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ldbxyzptlk/db231222/z/M;->d(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1253
    const/16 v0, 0x8

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "block_hashes"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    invoke-static {p2}, Ldbxyzptlk/db231222/aj/a;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "overwrite"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    invoke-static {p5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string v1, "parent_rev"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    if-eqz p6, :cond_0

    :goto_0
    aput-object p6, v4, v0

    const/4 v0, 0x6

    const-string v1, "size"

    aput-object v1, v4, v0

    const/4 v0, 0x7

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 1261
    :try_start_0
    sget-object v0, Ldbxyzptlk/db231222/v/w;->b:Ldbxyzptlk/db231222/v/w;

    invoke-interface {v5}, Ldbxyzptlk/db231222/y/n;->c()Ljava/lang/String;

    move-result-object v1

    const-string v3, "r6"

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/db231222/v/v;->b(Ldbxyzptlk/db231222/v/w;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/db231222/y/n;)Ldbxyzptlk/db231222/v/l;

    move-result-object v0

    iget-object v0, v0, Ldbxyzptlk/db231222/v/l;->b:Lorg/apache/http/HttpResponse;

    .line 1266
    new-instance v1, Ldbxyzptlk/db231222/v/j;

    invoke-static {v0}, Ldbxyzptlk/db231222/v/v;->a(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/v/j;-><init>(Ljava/util/Map;)V
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 1270
    :goto_1
    return-object v0

    .line 1253
    :cond_0
    const-string p6, ""

    goto :goto_0

    .line 1268
    :catch_0
    move-exception v0

    .line 1269
    invoke-static {v0}, Ldbxyzptlk/db231222/z/M;->a(Ldbxyzptlk/db231222/w/i;)V

    .line 1270
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic a(Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/util/DropboxPath;Ljava/util/List;JZLjava/lang/String;)Ldbxyzptlk/db231222/v/j;
    .locals 1

    .prologue
    .line 87
    invoke-direct/range {p0 .. p6}, Ldbxyzptlk/db231222/z/M;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/util/List;JZLjava/lang/String;)Ldbxyzptlk/db231222/v/j;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/z/M;Ljava/io/InputStream;JLjava/lang/String;Ldbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/z/ab;
    .locals 1

    .prologue
    .line 87
    invoke-direct/range {p0 .. p5}, Ldbxyzptlk/db231222/z/M;->a(Ljava/io/InputStream;JLjava/lang/String;Ldbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/z/ab;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/io/InputStream;JLjava/lang/String;Ldbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/z/ab;
    .locals 5

    .prologue
    .line 1146
    invoke-direct {p0}, Ldbxyzptlk/db231222/z/M;->i()V

    .line 1147
    const-wide/32 v0, 0x400000

    cmp-long v0, p2, v0

    if-lez v0, :cond_0

    .line 1148
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Block length too large"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1151
    :cond_0
    const-string v0, "/upload_block"

    .line 1152
    if-eqz p4, :cond_1

    .line 1153
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1156
    :cond_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->b()Ldbxyzptlk/db231222/z/D;

    move-result-object v2

    .line 1157
    invoke-interface {v2}, Ldbxyzptlk/db231222/y/n;->c()Ljava/lang/String;

    move-result-object v1

    const-string v3, "r6"

    const/4 v4, 0x0

    invoke-static {v1, v3, v0, v4}, Ldbxyzptlk/db231222/v/v;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1159
    new-instance v3, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v3, v0}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    .line 1160
    invoke-interface {v2, v3}, Ldbxyzptlk/db231222/y/n;->a(Lorg/apache/http/HttpRequest;)V

    .line 1161
    new-instance v1, Lorg/apache/http/entity/InputStreamEntity;

    invoke-direct {v1, p1, p2, p3}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    .line 1162
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lorg/apache/http/entity/InputStreamEntity;->setChunked(Z)V

    .line 1165
    if-eqz p5, :cond_2

    .line 1166
    new-instance v0, Ldbxyzptlk/db231222/v/t;

    invoke-direct {v0, v1, p5}, Ldbxyzptlk/db231222/v/t;-><init>(Lorg/apache/http/HttpEntity;Ldbxyzptlk/db231222/v/s;)V

    .line 1169
    :goto_0
    invoke-virtual {v3, v0}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 1170
    new-instance v0, Ldbxyzptlk/db231222/z/ab;

    invoke-direct {v0, v3, v2}, Ldbxyzptlk/db231222/z/ab;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Ldbxyzptlk/db231222/y/n;)V

    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/z/M;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;Ljava/util/List;JLjava/lang/String;Ljava/lang/String;I)Ldbxyzptlk/db231222/z/ad;
    .locals 1

    .prologue
    .line 87
    invoke-direct/range {p0 .. p13}, Ldbxyzptlk/db231222/z/M;->a(Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;Ljava/util/List;JLjava/lang/String;Ljava/lang/String;I)Ldbxyzptlk/db231222/z/ad;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;Ljava/util/List;JLjava/lang/String;Ljava/lang/String;I)Ldbxyzptlk/db231222/z/ad;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "JJ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)",
            "Ldbxyzptlk/db231222/z/ad;"
        }
    .end annotation

    .prologue
    .line 1211
    invoke-static/range {p7 .. p7}, Lcom/dropbox/android/util/C;->a(Ljava/lang/String;)V

    .line 1212
    invoke-static/range {p11 .. p11}, Lcom/dropbox/android/util/C;->a(Ljava/lang/String;)V

    .line 1213
    invoke-static/range {p12 .. p12}, Lcom/dropbox/android/util/C;->a(Ljava/lang/String;)V

    .line 1215
    invoke-direct {p0}, Ldbxyzptlk/db231222/z/M;->i()V

    .line 1217
    const-string v1, "/"

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1218
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p7

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p7

    .line 1221
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/commit_camera_upload/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p11

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p7

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object v1, p0

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    move/from16 v7, p13

    .line 1222
    invoke-direct/range {v1 .. v7}, Ldbxyzptlk/db231222/z/M;->a(JJLjava/lang/String;I)Ljava/util/HashMap;

    move-result-object v3

    .line 1223
    const-string v1, "block_hashes"

    invoke-static/range {p8 .. p8}, Ldbxyzptlk/db231222/aj/a;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1224
    const-string v1, "size"

    invoke-static/range {p9 .. p10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1225
    const-string v1, "cu_hash_full"

    move-object/from16 v0, p12

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1226
    const-string v1, "mime_type"

    invoke-interface {v3, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1229
    :try_start_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->b()Ldbxyzptlk/db231222/z/D;

    move-result-object v6

    .line 1230
    sget-object v1, Ldbxyzptlk/db231222/v/w;->b:Ldbxyzptlk/db231222/v/w;

    invoke-interface {v6}, Ldbxyzptlk/db231222/y/n;->c()Ljava/lang/String;

    move-result-object v2

    const-string v4, "r6"

    invoke-static {v3}, Ldbxyzptlk/db231222/z/M;->a(Ljava/util/Map;)[Ljava/lang/String;

    move-result-object v5

    move-object v3, v8

    invoke-static/range {v1 .. v6}, Ldbxyzptlk/db231222/v/v;->b(Ldbxyzptlk/db231222/v/w;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/db231222/y/n;)Ldbxyzptlk/db231222/v/l;

    move-result-object v1

    iget-object v2, v1, Ldbxyzptlk/db231222/v/l;->b:Lorg/apache/http/HttpResponse;

    .line 1235
    new-instance v3, Ldbxyzptlk/db231222/v/j;

    invoke-static {v2}, Ldbxyzptlk/db231222/v/v;->a(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-direct {v3, v1}, Ldbxyzptlk/db231222/v/j;-><init>(Ljava/util/Map;)V

    .line 1236
    const-string v1, "dropbox-chillout"

    const/4 v4, 0x0

    invoke-static {v2, v1, v4}, Ldbxyzptlk/db231222/z/M;->b(Lorg/apache/http/HttpResponse;Ljava/lang/String;F)F

    move-result v2

    .line 1237
    new-instance v1, Ldbxyzptlk/db231222/z/ad;

    invoke-direct {v1, v3, v2}, Ldbxyzptlk/db231222/z/ad;-><init>(Ldbxyzptlk/db231222/v/j;F)V
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_0 .. :try_end_0} :catch_0

    .line 1240
    :goto_0
    return-object v1

    .line 1238
    :catch_0
    move-exception v1

    .line 1239
    invoke-static {v1}, Ldbxyzptlk/db231222/z/M;->a(Ldbxyzptlk/db231222/w/i;)V

    .line 1240
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/util/Collection;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 2115
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2116
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    .line 2117
    invoke-static {v0}, Ldbxyzptlk/db231222/z/M;->d(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2119
    :cond_0
    invoke-static {v1}, Ldbxyzptlk/db231222/aj/a;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(JJLjava/lang/String;I)Ljava/util/HashMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 898
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 899
    const-string v0, "device_manufacturer"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 900
    const-string v0, "device_model"

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 901
    const-string v0, "device_uid"

    invoke-static {}, Ldbxyzptlk/db231222/n/k;->a()Ldbxyzptlk/db231222/n/k;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/n/k;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 902
    const-string v0, "client_platform"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Android "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 903
    const-string v0, "client_buildstring"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Android "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/i/d;->a()Ldbxyzptlk/db231222/i/c;

    move-result-object v3

    iget-object v3, v3, Ldbxyzptlk/db231222/i/c;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 904
    const-string v2, "file_mtime"

    const-wide/16 v3, 0x0

    cmp-long v0, p1, v3

    if-lez v0, :cond_1

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 905
    const-string v0, "client_timeoffset"

    invoke-virtual {v1, v0, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 906
    const-string v0, "client_import_time"

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 907
    if-lez p6, :cond_0

    .line 908
    const-string v0, "file_number"

    invoke-static {p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 910
    :cond_0
    const-string v0, "locale"

    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 911
    return-object v1

    .line 904
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method private static a(Ldbxyzptlk/db231222/w/i;)V
    .locals 2

    .prologue
    .line 1193
    .line 1194
    iget v0, p0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v1, 0x19c

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/w/i;->f:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 1195
    iget-object v0, p0, Ldbxyzptlk/db231222/w/i;->f:Ljava/util/Map;

    const-string v1, "need_blocks"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1196
    if-eqz v0, :cond_0

    .line 1198
    check-cast v0, Ljava/util/List;

    .line 1199
    new-instance v1, Ldbxyzptlk/db231222/z/ao;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/z/ao;-><init>(Ljava/util/List;)V

    throw v1

    .line 1202
    :cond_0
    throw p0
.end method

.method private static a(Ljava/util/Map;)[Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 915
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    new-array v3, v0, [Ljava/lang/String;

    .line 916
    const/4 v0, 0x0

    .line 917
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 918
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v3, v2

    .line 919
    add-int/lit8 v1, v2, 0x1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v3, v1

    .line 920
    add-int/lit8 v0, v2, 0x2

    move v2, v0

    .line 921
    goto :goto_0

    .line 922
    :cond_0
    return-object v3
.end method

.method private static b(Lorg/apache/http/HttpResponse;Ljava/lang/String;F)F
    .locals 1

    .prologue
    .line 1065
    invoke-interface {p0, p1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 1066
    if-eqz v0, :cond_0

    .line 1068
    :try_start_0
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 1071
    :cond_0
    :goto_0
    return p2

    .line 1069
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private varargs b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;
    .locals 1

    .prologue
    .line 1541
    invoke-direct {p0}, Ldbxyzptlk/db231222/z/M;->i()V

    .line 1542
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/z/M;->a(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    return-object v0
.end method

.method private static d(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2111
    invoke-virtual {p0}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 584
    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->b()Ldbxyzptlk/db231222/z/D;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/D;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 585
    new-instance v0, Ldbxyzptlk/db231222/w/j;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/w/j;-><init>(Lorg/apache/http/HttpResponse;)V

    throw v0

    .line 587
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/z/y;Ljava/lang/String;)Ldbxyzptlk/db231222/v/f;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/z/y;",
            "Ljava/lang/String;",
            ")",
            "Ldbxyzptlk/db231222/v/f",
            "<",
            "Ldbxyzptlk/db231222/z/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1521
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "collection_type"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p1}, Ldbxyzptlk/db231222/z/y;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "cursor"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    aput-object p2, v0, v1

    .line 1526
    const-string v1, "/collections_delta"

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 1528
    :try_start_0
    sget-object v1, Ldbxyzptlk/db231222/z/j;->c:Ldbxyzptlk/db231222/x/c;

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/v/f;->a(Ldbxyzptlk/db231222/x/k;Ldbxyzptlk/db231222/x/c;)Ldbxyzptlk/db231222/v/f;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 1529
    :catch_0
    move-exception v0

    .line 1530
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Ljava/lang/String;IZ)Ldbxyzptlk/db231222/v/f;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IZ)",
            "Ldbxyzptlk/db231222/v/f",
            "<",
            "Ldbxyzptlk/db231222/z/r;",
            ">;"
        }
    .end annotation

    .prologue
    .line 855
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "cursor"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    const-string v2, "locale"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "limit"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "blocking"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    invoke-static {p3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 861
    const-string v1, "/collection_all_photos_delta"

    .line 863
    const-string v1, "/collection_all_photos_delta"

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 865
    :try_start_0
    sget-object v1, Ldbxyzptlk/db231222/z/r;->f:Ldbxyzptlk/db231222/x/c;

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/v/f;->a(Ldbxyzptlk/db231222/x/k;Ldbxyzptlk/db231222/x/c;)Ldbxyzptlk/db231222/v/f;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 866
    :catch_0
    move-exception v0

    .line 867
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ljava/io/OutputStream;Ldbxyzptlk/db231222/v/n;Ldbxyzptlk/db231222/v/m;Ldbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/v/g;
    .locals 7

    .prologue
    .line 1716
    iget-object v0, p0, Ldbxyzptlk/db231222/z/M;->a:Ldbxyzptlk/db231222/v/a;

    invoke-static {p1}, Ldbxyzptlk/db231222/z/M;->d(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Ldbxyzptlk/db231222/v/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/io/OutputStream;Ldbxyzptlk/db231222/v/n;Ldbxyzptlk/db231222/v/m;Ldbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/v/g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ljava/io/OutputStream;Ldbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/v/g;
    .locals 2

    .prologue
    .line 1686
    iget-object v0, p0, Ldbxyzptlk/db231222/z/M;->a:Ldbxyzptlk/db231222/v/a;

    invoke-static {p1}, Ldbxyzptlk/db231222/z/M;->d(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Ldbxyzptlk/db231222/v/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/io/OutputStream;Ldbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/v/g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Z)Ldbxyzptlk/db231222/v/i;
    .locals 2

    .prologue
    .line 1693
    iget-object v0, p0, Ldbxyzptlk/db231222/z/M;->a:Ldbxyzptlk/db231222/v/a;

    invoke-static {p1}, Ldbxyzptlk/db231222/z/M;->d(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ldbxyzptlk/db231222/v/a;->a(Ljava/lang/String;Z)Ldbxyzptlk/db231222/v/i;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)Ldbxyzptlk/db231222/v/j;
    .locals 4

    .prologue
    .line 736
    const-string v0, "/shared_folder/accept"

    .line 737
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "invitation_id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "locale"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->a()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 742
    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 745
    :try_start_0
    sget-object v1, Ldbxyzptlk/db231222/v/j;->r:Ldbxyzptlk/db231222/x/c;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/c;->b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/v/j;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 746
    :catch_0
    move-exception v0

    .line 747
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;)Ldbxyzptlk/db231222/v/j;
    .locals 2

    .prologue
    .line 1657
    iget-object v0, p0, Ldbxyzptlk/db231222/z/M;->a:Ldbxyzptlk/db231222/v/a;

    invoke-static {p1}, Ldbxyzptlk/db231222/z/M;->d(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/v/a;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/v/j;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;ILjava/lang/String;ZLjava/lang/String;)Ldbxyzptlk/db231222/v/j;
    .locals 6

    .prologue
    .line 1664
    iget-object v0, p0, Ldbxyzptlk/db231222/z/M;->a:Ldbxyzptlk/db231222/v/a;

    invoke-static {p1}, Ldbxyzptlk/db231222/z/M;->d(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Ldbxyzptlk/db231222/v/a;->a(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;)Ldbxyzptlk/db231222/v/j;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/util/DropboxPath;)Ldbxyzptlk/db231222/v/j;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1390
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "root"

    aput-object v2, v0, v1

    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->b()Ldbxyzptlk/db231222/z/D;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/z/D;->g()Ldbxyzptlk/db231222/y/o;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/y/o;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, "from_path"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p1}, Ldbxyzptlk/db231222/z/M;->d(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "to_path"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    invoke-static {p2}, Ldbxyzptlk/db231222/z/M;->d(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "locale"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "rename_on_conflict"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1396
    const-string v1, "/fileops/move"

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 1399
    :try_start_0
    sget-object v1, Ldbxyzptlk/db231222/v/j;->r:Ldbxyzptlk/db231222/x/c;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/c;->b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/v/j;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 1400
    :catch_0
    move-exception v0

    .line 1401
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Ljava/io/InputStream;JLdbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/v/o;
    .locals 6

    .prologue
    .line 1724
    iget-object v0, p0, Ldbxyzptlk/db231222/z/M;->a:Ldbxyzptlk/db231222/v/a;

    invoke-static {p1}, Ldbxyzptlk/db231222/z/M;->d(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v1

    move-object v2, p2

    move-wide v3, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Ldbxyzptlk/db231222/v/a;->a(Ljava/lang/String;Ljava/io/InputStream;JLdbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/v/o;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Ljava/io/InputStream;JLjava/lang/String;Ldbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/v/o;
    .locals 7

    .prologue
    .line 1732
    iget-object v0, p0, Ldbxyzptlk/db231222/z/M;->a:Ldbxyzptlk/db231222/v/a;

    invoke-static {p1}, Ldbxyzptlk/db231222/z/M;->d(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v1

    move-object v2, p2

    move-wide v3, p3

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Ldbxyzptlk/db231222/v/a;->a(Ljava/lang/String;Ljava/io/InputStream;JLjava/lang/String;Ldbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/v/o;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/z/Z;
    .locals 3

    .prologue
    .line 722
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "consumer_key"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    const-string v2, "locale"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "state"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    aput-object p2, v0, v1

    .line 727
    const-string v1, "/dtoken"

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 729
    :try_start_0
    new-instance v1, Ldbxyzptlk/db231222/z/Z;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v0

    iget-object v0, v0, Ldbxyzptlk/db231222/x/g;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/Map;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/z/Z;-><init>(Ljava/util/Map;)V
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 730
    :catch_0
    move-exception v0

    .line 731
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/dropbox/android/payments/x;)Ldbxyzptlk/db231222/z/aB;
    .locals 3

    .prologue
    .line 2084
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "package_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/dropbox/android/payments/x;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "subscription_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p1}, Lcom/dropbox/android/payments/x;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "token"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    invoke-virtual {p1}, Lcom/dropbox/android/payments/x;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "payload"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    invoke-virtual {p1}, Lcom/dropbox/android/payments/x;->e()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "android_iap_raw_response"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    invoke-virtual {p1}, Lcom/dropbox/android/payments/x;->d()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 2091
    const-string v1, "/android/google_play_subscription_upgrade"

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 2093
    :try_start_0
    sget-object v1, Ldbxyzptlk/db231222/z/aB;->a:Ldbxyzptlk/db231222/x/c;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/c;->b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/aB;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 2094
    :catch_0
    move-exception v0

    .line 2095
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Ljava/io/FileInputStream;ZLjava/lang/String;Ldbxyzptlk/db231222/z/g;)Ldbxyzptlk/db231222/z/aE;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Ljava/io/FileInputStream;",
            "Z",
            "Ljava/lang/String;",
            "Ldbxyzptlk/db231222/z/g;",
            ")",
            "Ldbxyzptlk/db231222/z/aE",
            "<",
            "Ldbxyzptlk/db231222/v/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1099
    .line 1100
    new-instance v7, Ldbxyzptlk/db231222/z/al;

    new-instance v0, Ldbxyzptlk/db231222/z/O;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/z/O;-><init>(Ldbxyzptlk/db231222/z/M;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/util/DropboxPath;ZLjava/lang/String;)V

    const/4 v6, 0x0

    move-object v1, v7

    move-object v2, p2

    move-object v3, p5

    move-object v4, p0

    move-object v5, v0

    invoke-direct/range {v1 .. v6}, Ldbxyzptlk/db231222/z/al;-><init>(Ljava/io/FileInputStream;Ldbxyzptlk/db231222/z/g;Ldbxyzptlk/db231222/z/M;Ldbxyzptlk/db231222/z/am;Ldbxyzptlk/db231222/z/Q;)V

    return-object v7
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;ILjava/io/FileInputStream;Ldbxyzptlk/db231222/z/g;Ldbxyzptlk/db231222/z/Q;)Ldbxyzptlk/db231222/z/aE;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JJ",
            "Ljava/lang/String;",
            "I",
            "Ljava/io/FileInputStream;",
            "Ldbxyzptlk/db231222/z/g;",
            "Ldbxyzptlk/db231222/z/Q;",
            ")",
            "Ldbxyzptlk/db231222/z/aE",
            "<",
            "Ldbxyzptlk/db231222/z/ad;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1078
    new-instance v13, Ldbxyzptlk/db231222/z/al;

    new-instance v0, Ldbxyzptlk/db231222/z/N;

    move-object v1, p0

    move-object/from16 v2, p10

    move-object/from16 v3, p2

    move-wide/from16 v4, p4

    move-wide/from16 v6, p6

    move-object/from16 v8, p8

    move-object/from16 v9, p3

    move-object v10, p1

    move-object/from16 v11, p11

    move/from16 v12, p9

    invoke-direct/range {v0 .. v12}, Ldbxyzptlk/db231222/z/N;-><init>(Ldbxyzptlk/db231222/z/M;Ljava/io/FileInputStream;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/db231222/z/g;I)V

    move-object v1, v13

    move-object/from16 v2, p10

    move-object/from16 v3, p11

    move-object v4, p0

    move-object v5, v0

    move-object/from16 v6, p12

    invoke-direct/range {v1 .. v6}, Ldbxyzptlk/db231222/z/al;-><init>(Ljava/io/FileInputStream;Ldbxyzptlk/db231222/z/g;Ldbxyzptlk/db231222/z/M;Ldbxyzptlk/db231222/z/am;Ldbxyzptlk/db231222/z/Q;)V

    return-object v13
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;ILjava/io/InputStream;JLdbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/z/aE;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JJ",
            "Ljava/lang/String;",
            "I",
            "Ljava/io/InputStream;",
            "J",
            "Ldbxyzptlk/db231222/v/s;",
            ")",
            "Ldbxyzptlk/db231222/z/aE",
            "<",
            "Ldbxyzptlk/db231222/z/ad;",
            ">;"
        }
    .end annotation

    .prologue
    .line 932
    invoke-direct {p0}, Ldbxyzptlk/db231222/z/M;->i()V

    .line 934
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/camera_upload/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    move-object v3, p0

    move-wide v4, p4

    move-wide/from16 v6, p6

    move-object/from16 v8, p8

    move/from16 v9, p9

    .line 936
    invoke-direct/range {v3 .. v9}, Ldbxyzptlk/db231222/z/M;->a(JJLjava/lang/String;I)Ljava/util/HashMap;

    move-result-object v3

    invoke-static {v3}, Ldbxyzptlk/db231222/z/M;->a(Ljava/util/Map;)[Ljava/lang/String;

    move-result-object v3

    .line 938
    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->b()Ldbxyzptlk/db231222/z/D;

    move-result-object v5

    .line 939
    invoke-interface {v5}, Ldbxyzptlk/db231222/y/n;->c()Ljava/lang/String;

    move-result-object v4

    const-string v6, "r6"

    invoke-static {v4, v6, v10, v3}, Ldbxyzptlk/db231222/v/v;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 943
    new-instance v6, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v6, v3}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    .line 944
    invoke-interface {v5, v6}, Ldbxyzptlk/db231222/y/n;->a(Lorg/apache/http/HttpRequest;)V

    .line 946
    const-string v3, "Content-Type"

    invoke-virtual {v6, v3, p2}, Lorg/apache/http/client/methods/HttpPut;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 948
    new-instance v4, Lorg/apache/http/entity/InputStreamEntity;

    move-object/from16 v0, p10

    move-wide/from16 v1, p11

    invoke-direct {v4, v0, v1, v2}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    .line 949
    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Lorg/apache/http/entity/InputStreamEntity;->setChunked(Z)V

    .line 953
    if-eqz p13, :cond_0

    .line 954
    new-instance v3, Ldbxyzptlk/db231222/v/t;

    move-object/from16 v0, p13

    invoke-direct {v3, v4, v0}, Ldbxyzptlk/db231222/v/t;-><init>(Lorg/apache/http/HttpEntity;Ldbxyzptlk/db231222/v/s;)V

    .line 957
    :goto_0
    invoke-virtual {v6, v3}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 959
    new-instance v3, Ldbxyzptlk/db231222/z/ac;

    invoke-direct {v3, p0, v6, v5}, Ldbxyzptlk/db231222/z/ac;-><init>(Ldbxyzptlk/db231222/z/M;Lorg/apache/http/client/methods/HttpUriRequest;Ldbxyzptlk/db231222/y/n;)V

    return-object v3

    :cond_0
    move-object v3, v4

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Ldbxyzptlk/db231222/z/aF;
    .locals 2

    .prologue
    .line 591
    invoke-direct {p0}, Ldbxyzptlk/db231222/z/M;->i()V

    .line 592
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 593
    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->b()Ldbxyzptlk/db231222/z/D;

    move-result-object v1

    invoke-static {v1, v0}, Ldbxyzptlk/db231222/v/v;->a(Ldbxyzptlk/db231222/y/n;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 596
    invoke-static {v0}, Ldbxyzptlk/db231222/v/v;->a(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 597
    if-nez v0, :cond_0

    .line 598
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected null video metadata"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 600
    :cond_0
    new-instance v1, Ldbxyzptlk/db231222/z/aF;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/z/aF;-><init>(Ljava/util/Map;)V

    return-object v1
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/z/ai;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 504
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/media_transcode/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->b()Ldbxyzptlk/db231222/z/D;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/z/D;->g()Ldbxyzptlk/db231222/y/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 507
    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "hls"

    aput-object v2, v1, v4

    const-string v2, "mpegts"

    aput-object v2, v1, v5

    .line 511
    const/16 v2, 0x12

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "locale"

    aput-object v3, v2, v4

    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->a()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const-string v3, "screen_resolution"

    aput-object v3, v2, v6

    const/4 v3, 0x3

    aput-object p2, v2, v3

    const/4 v3, 0x4

    const-string v4, "connection_type"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    aput-object p3, v2, v3

    const/4 v3, 0x6

    const-string v4, "container"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, ","

    invoke-static {v4, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    const/16 v1, 0x8

    const-string v3, "model"

    aput-object v3, v2, v1

    const/16 v1, 0x9

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v3, v2, v1

    const/16 v1, 0xa

    const-string v3, "app_version"

    aput-object v3, v2, v1

    const/16 v1, 0xb

    aput-object p4, v2, v1

    const/16 v1, 0xc

    const-string v3, "sys_version"

    aput-object v3, v2, v1

    const/16 v1, 0xd

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    aput-object v3, v2, v1

    const/16 v1, 0xe

    const-string v3, "platform"

    aput-object v3, v2, v1

    const/16 v1, 0xf

    const-string v3, "android"

    aput-object v3, v2, v1

    const/16 v1, 0x10

    const-string v3, "manufacturer"

    aput-object v3, v2, v1

    const/16 v1, 0x11

    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    aput-object v3, v2, v1

    .line 523
    :try_start_0
    invoke-direct {p0, v0, v2}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 524
    sget-object v1, Ldbxyzptlk/db231222/z/ai;->g:Ldbxyzptlk/db231222/x/c;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/c;->b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/ai;
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 525
    :catch_0
    move-exception v0

    .line 526
    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x19f

    if-ne v1, v2, :cond_0

    .line 528
    new-instance v0, Ldbxyzptlk/db231222/z/az;

    invoke-direct {v0}, Ldbxyzptlk/db231222/z/az;-><init>()V

    throw v0

    .line 530
    :cond_0
    throw v0

    .line 531
    :catch_1
    move-exception v0

    .line 532
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Ldbxyzptlk/db231222/z/B;)Ldbxyzptlk/db231222/z/av;
    .locals 3

    .prologue
    .line 659
    const/16 v0, 0x2e

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, Lcom/dropbox/android/util/bd;->j:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/dropbox/android/util/bd;->h:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Ldbxyzptlk/db231222/z/B;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/dropbox/android/util/bd;->O:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Ldbxyzptlk/db231222/z/B;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dropbox/android/util/bd;->f:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dropbox/android/util/bd;->q:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Ldbxyzptlk/db231222/z/B;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/dropbox/android/util/bd;->r:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Ldbxyzptlk/db231222/z/B;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/dropbox/android/util/bd;->i:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Ldbxyzptlk/db231222/z/B;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/dropbox/android/util/bd;->N:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p1, Ldbxyzptlk/db231222/z/B;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/dropbox/android/util/bd;->v:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-object v2, p1, Ldbxyzptlk/db231222/z/B;->n:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/dropbox/android/util/bd;->z:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget-object v2, p1, Ldbxyzptlk/db231222/z/B;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/dropbox/android/util/bd;->H:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x15

    iget-object v2, p1, Ldbxyzptlk/db231222/z/B;->k:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/dropbox/android/util/bd;->C:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x17

    iget-object v2, p1, Ldbxyzptlk/db231222/z/B;->m:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/dropbox/android/util/bd;->g:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x19

    iget-object v2, p1, Ldbxyzptlk/db231222/z/B;->l:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/dropbox/android/util/bd;->K:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    iget-object v2, p1, Ldbxyzptlk/db231222/z/B;->i:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/dropbox/android/util/bd;->y:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    iget-object v2, p1, Ldbxyzptlk/db231222/z/B;->j:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/dropbox/android/util/bd;->c:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    invoke-virtual {p1}, Ldbxyzptlk/db231222/z/B;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/dropbox/android/util/bd;->L:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x21

    iget-boolean v2, p1, Ldbxyzptlk/db231222/z/B;->s:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/dropbox/android/util/bd;->b:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x23

    iget-object v2, p1, Ldbxyzptlk/db231222/z/B;->t:Ljava/util/Map;

    invoke-static {v2}, Ldbxyzptlk/db231222/aj/c;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/dropbox/android/util/bd;->E:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x25

    iget-object v2, p1, Ldbxyzptlk/db231222/z/B;->o:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/dropbox/android/util/bd;->o:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x27

    iget-object v2, p1, Ldbxyzptlk/db231222/z/B;->p:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/dropbox/android/util/bd;->I:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x29

    iget-object v2, p1, Ldbxyzptlk/db231222/z/B;->q:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/dropbox/android/util/bd;->d:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    iget-object v2, p1, Ldbxyzptlk/db231222/z/B;->r:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "locale"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 685
    sget-object v1, Lcom/dropbox/android/util/bd;->t:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 687
    :try_start_0
    sget-object v1, Ldbxyzptlk/db231222/z/av;->c:Ldbxyzptlk/db231222/x/c;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/c;->b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/av;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 688
    :catch_0
    move-exception v0

    .line 689
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Ldbxyzptlk/db231222/z/y;Ljava/lang/String;Ljava/util/List;ZLjava/util/Date;)Ldbxyzptlk/db231222/z/h;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/z/y;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;Z",
            "Ljava/util/Date;",
            ")",
            "Ldbxyzptlk/db231222/z/h;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1425
    sget-object v0, Ldbxyzptlk/db231222/z/y;->b:Ldbxyzptlk/db231222/z/y;

    if-ne p1, v0, :cond_0

    if-nez p5, :cond_0

    .line 1426
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Lightweight collections require a localtime."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1428
    :cond_0
    const-string v0, "/collection_create"

    .line 1429
    const/16 v0, 0xa

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "collection_type"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    invoke-virtual {p1}, Ldbxyzptlk/db231222/z/y;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string v3, "name"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    aput-object p2, v2, v0

    const/4 v0, 0x4

    const-string v3, "shared"

    aput-object v3, v2, v0

    const/4 v3, 0x5

    if-eqz p4, :cond_2

    const-string v0, "true"

    :goto_0
    aput-object v0, v2, v3

    const/4 v0, 0x6

    const-string v3, "paths"

    aput-object v3, v2, v0

    const/4 v0, 0x7

    invoke-static {p3}, Ldbxyzptlk/db231222/z/M;->a(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0x8

    const-string v3, "localtime"

    aput-object v3, v2, v0

    const/16 v0, 0x9

    if-eqz p5, :cond_1

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-static {p5, v1}, Ldbxyzptlk/db231222/z/h;->a(Ljava/util/Date;Ljava/util/TimeZone;)Ljava/lang/String;

    move-result-object v1

    :cond_1
    aput-object v1, v2, v0

    .line 1441
    const-string v0, "/collection_create"

    invoke-direct {p0, v0, v2}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 1443
    :try_start_0
    new-instance v1, Ldbxyzptlk/db231222/z/h;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/z/h;-><init>(Ldbxyzptlk/db231222/x/k;)V
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :cond_2
    move-object v0, v1

    .line 1429
    goto :goto_0

    .line 1444
    :catch_0
    move-exception v0

    .line 1445
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;)Ldbxyzptlk/db231222/z/h;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;)",
            "Ldbxyzptlk/db231222/z/h;"
        }
    .end annotation

    .prologue
    .line 1453
    const-string v0, "/collection_items_add"

    .line 1454
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "paths"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p2}, Ldbxyzptlk/db231222/z/M;->a(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    aput-object p1, v0, v1

    .line 1459
    const-string v1, "/collection_items_add"

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 1461
    :try_start_0
    new-instance v1, Ldbxyzptlk/db231222/z/h;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/z/h;-><init>(Ldbxyzptlk/db231222/x/k;)V
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 1462
    :catch_0
    move-exception v0

    .line 1463
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;IZ)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Ljava/lang/String;",
            "IZ)",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/v/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1708
    iget-object v0, p0, Ldbxyzptlk/db231222/z/M;->a:Ldbxyzptlk/db231222/v/a;

    invoke-static {p1}, Ldbxyzptlk/db231222/z/M;->d(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Ldbxyzptlk/db231222/v/a;->a(Ljava/lang/String;Ljava/lang/String;IZ)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/v/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1546
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "root"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->b()Ldbxyzptlk/db231222/z/D;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/z/D;->g()Ldbxyzptlk/db231222/y/o;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/y/o;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "paths"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p1}, Ldbxyzptlk/db231222/z/M;->a(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1551
    const-string v1, "/fileops/delete_batch"

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 1553
    :try_start_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->c()Ldbxyzptlk/db231222/x/d;

    move-result-object v0

    sget-object v1, Ldbxyzptlk/db231222/v/j;->r:Ldbxyzptlk/db231222/x/c;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/d;->a(Ldbxyzptlk/db231222/x/c;)Ljava/util/ArrayList;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 1554
    :catch_0
    move-exception v0

    .line 1555
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Ljava/util/List;I)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;I)",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/v/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 797
    if-gtz p2, :cond_0

    .line 798
    const/16 p2, 0x61a8

    .line 801
    :cond_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->b()Ldbxyzptlk/db231222/z/D;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/D;->g()Ldbxyzptlk/db231222/y/o;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/y/o;->toString()Ljava/lang/String;

    move-result-object v1

    .line 802
    new-instance v2, Ldbxyzptlk/db231222/aj/a;

    invoke-direct {v2}, Ldbxyzptlk/db231222/aj/a;-><init>()V

    .line 803
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    .line 804
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Ldbxyzptlk/db231222/z/M;->d(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/aj/a;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 807
    :cond_1
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "paths"

    aput-object v3, v0, v1

    const/4 v1, 0x1

    invoke-virtual {v2}, Ldbxyzptlk/db231222/aj/a;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "file_limit"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "locale"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 813
    const-string v1, "/iphone/files_batch/"

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 815
    :try_start_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->c()Ldbxyzptlk/db231222/x/d;

    move-result-object v0

    sget-object v1, Ldbxyzptlk/db231222/v/j;->r:Ldbxyzptlk/db231222/x/c;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/d;->a(Ldbxyzptlk/db231222/x/c;)Ljava/util/ArrayList;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 816
    :catch_0
    move-exception v0

    .line 817
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/z/ae;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1637
    const-string v0, "direct"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "preview"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 1638
    const-string v0, "/chooser_share"

    .line 1639
    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "paths"

    aput-object v4, v3, v1

    invoke-static {p1}, Ldbxyzptlk/db231222/z/M;->a(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v2

    const/4 v1, 0x2

    const-string v2, "app_key"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    aput-object p2, v3, v1

    const/4 v1, 0x4

    const-string v2, "link_type"

    aput-object v2, v3, v1

    const/4 v1, 0x5

    aput-object p3, v3, v1

    .line 1644
    invoke-direct {p0, v0, v3}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 1646
    :try_start_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->c()Ldbxyzptlk/db231222/x/d;

    move-result-object v0

    .line 1647
    sget-object v1, Ldbxyzptlk/db231222/z/ae;->f:Ldbxyzptlk/db231222/x/c;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/d;->a(Ldbxyzptlk/db231222/x/c;)Ljava/util/ArrayList;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v1

    .line 1637
    goto :goto_0

    .line 1648
    :catch_0
    move-exception v0

    .line 1649
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 697
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, Lcom/dropbox/android/util/bd;->j:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/dropbox/android/util/bd;->q:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    aput-object p2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/dropbox/android/util/bd;->r:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    aput-object p3, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dropbox/android/util/bd;->i:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    aput-object p4, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dropbox/android/util/bd;->a:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    aput-object p5, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/dropbox/android/util/bd;->x:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    aput-object p6, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/dropbox/android/util/bd;->l:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    aput-object p7, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/dropbox/android/util/bd;->A:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf

    aput-object p8, v0, v1

    .line 707
    const-string v1, "/feedback"

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    .line 708
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1580
    const-string v0, "/share/default"

    .line 1581
    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "shmodel_url"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    const-string v3, "who"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {p2}, Ldbxyzptlk/db231222/aj/a;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "custom_message"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    aput-object p3, v1, v2

    .line 1587
    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    .line 1588
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 835
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "enabled"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 838
    const-string v1, "/set_video_uploads_enabled_default"

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    .line 839
    return-void
.end method

.method public final b(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 608
    invoke-direct {p0}, Ldbxyzptlk/db231222/z/M;->i()V

    .line 609
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 610
    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->b()Ldbxyzptlk/db231222/z/D;

    move-result-object v1

    invoke-static {v1, v0}, Ldbxyzptlk/db231222/v/v;->a(Ldbxyzptlk/db231222/y/n;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 613
    invoke-static {v0}, Ldbxyzptlk/db231222/v/v;->a(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 614
    const-string v1, "progress"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;Ljava/util/List;)Landroid/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/util/Pair",
            "<",
            "Ldbxyzptlk/db231222/z/p;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1468
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "item_ids"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ldbxyzptlk/db231222/aj/a;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "id"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object p1, v1, v2

    .line 1473
    const-string v2, "/collection_items_remove"

    invoke-direct {p0, v2, v1}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v1

    .line 1475
    :try_start_0
    invoke-virtual {v1}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v1

    .line 1476
    const-string v2, "metadata"

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/x/g;->c(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v2

    .line 1478
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ldbxyzptlk/db231222/x/k;->a()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1479
    new-instance v0, Ldbxyzptlk/db231222/z/p;

    invoke-direct {v0, v2}, Ldbxyzptlk/db231222/z/p;-><init>(Ldbxyzptlk/db231222/x/k;)V

    .line 1482
    :cond_0
    const-string v2, "removed"

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/x/k;->c()Ldbxyzptlk/db231222/x/d;

    move-result-object v1

    new-instance v2, Ldbxyzptlk/db231222/z/ay;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ldbxyzptlk/db231222/z/ay;-><init>(Ldbxyzptlk/db231222/z/N;)V

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/x/d;->a(Ldbxyzptlk/db231222/x/c;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1484
    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 1485
    :catch_0
    move-exception v0

    .line 1486
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/android/util/DropboxPath;)Ldbxyzptlk/db231222/v/j;
    .locals 3

    .prologue
    .line 1700
    iget-object v0, p0, Ldbxyzptlk/db231222/z/M;->a:Ldbxyzptlk/db231222/v/a;

    invoke-static {p1}, Ldbxyzptlk/db231222/z/M;->d(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ldbxyzptlk/db231222/z/M;->d(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/v/a;->a(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/v/j;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/util/List;)Ldbxyzptlk/db231222/z/as;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ldbxyzptlk/db231222/z/as;"
        }
    .end annotation

    .prologue
    .line 1805
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "emails"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ldbxyzptlk/db231222/aj/a;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "referral_src"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "android"

    aput-object v2, v0, v1

    .line 1810
    const-string v1, "/send_invite"

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 1813
    :try_start_0
    sget-object v1, Ldbxyzptlk/db231222/z/as;->a:Ldbxyzptlk/db231222/x/c;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/c;->b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/as;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 1814
    :catch_0
    move-exception v0

    .line 1815
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/z/p;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1499
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    const-string v3, "name"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object p2, v1, v2

    .line 1503
    const-string v2, "/collection_update"

    .line 1506
    :try_start_0
    const-string v2, "/collection_update"

    invoke-direct {p0, v2, v1}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v1

    .line 1507
    const-string v2, "success"

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/x/k;->i()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1514
    :cond_0
    :goto_0
    return-object v0

    .line 1510
    :cond_1
    const-string v2, "metadata"

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/x/g;->c(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v1

    .line 1511
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ldbxyzptlk/db231222/x/k;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1512
    new-instance v0, Ldbxyzptlk/db231222/z/p;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/z/p;-><init>(Ldbxyzptlk/db231222/x/k;)V
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1515
    :catch_0
    move-exception v0

    .line 1516
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(J)V
    .locals 4

    .prologue
    .line 752
    const-string v0, "/shared_folder/decline"

    .line 753
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "invitation_id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "locale"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->a()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 758
    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    .line 759
    return-void
.end method

.method public final b(Lcom/dropbox/android/util/DropboxPath;)V
    .locals 2

    .prologue
    .line 1671
    iget-object v0, p0, Ldbxyzptlk/db231222/z/M;->a:Ldbxyzptlk/db231222/v/a;

    invoke-static {p1}, Ldbxyzptlk/db231222/z/M;->d(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/v/a;->b(Ljava/lang/String;)V

    .line 1672
    return-void
.end method

.method public final c(Lcom/dropbox/android/util/DropboxPath;)Ldbxyzptlk/db231222/v/i;
    .locals 2

    .prologue
    .line 1678
    iget-object v0, p0, Ldbxyzptlk/db231222/z/M;->a:Ldbxyzptlk/db231222/v/a;

    invoke-static {p1}, Ldbxyzptlk/db231222/z/M;->d(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/v/a;->c(Ljava/lang/String;)Ldbxyzptlk/db231222/v/i;

    move-result-object v0

    return-object v0
.end method

.method public final c(J)Ldbxyzptlk/db231222/v/j;
    .locals 4

    .prologue
    .line 779
    const-string v0, "/shared_folder/info"

    .line 780
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "shared_folder_id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "locale"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->a()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 785
    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 788
    :try_start_0
    sget-object v1, Ldbxyzptlk/db231222/v/j;->r:Ldbxyzptlk/db231222/x/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v0

    const-string v2, "metadata"

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/c;->b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/v/j;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 789
    :catch_0
    move-exception v0

    .line 790
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c(Ljava/lang/String;)[B
    .locals 5

    .prologue
    .line 626
    const/4 v0, 0x0

    .line 627
    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->b()Ldbxyzptlk/db231222/z/D;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/z/D;->a()Ldbxyzptlk/db231222/y/k;

    move-result-object v1

    .line 628
    if-eqz v1, :cond_1

    .line 629
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 630
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "web_session_token"

    iget-object v1, v1, Ldbxyzptlk/db231222/y/k;->a:Ljava/lang/String;

    invoke-direct {v3, v4, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 631
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "platform"

    const-string v4, "android"

    invoke-direct {v1, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 632
    if-eqz p1, :cond_0

    .line 633
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "redirect"

    invoke-direct {v1, v3, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 636
    :cond_0
    :try_start_0
    new-instance v1, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    invoke-direct {v1, v2}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;)V

    .line 638
    invoke-virtual {v1}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {v1}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;->getContentLength()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;J)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 644
    :cond_1
    :goto_0
    return-object v0

    .line 640
    :catch_0
    move-exception v1

    goto :goto_0

    .line 639
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public final d()Ldbxyzptlk/db231222/z/R;
    .locals 4

    .prologue
    .line 262
    :try_start_0
    const-string v0, "/account/info"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "locale"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->a()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 263
    invoke-static {}, Ldbxyzptlk/db231222/z/R;->f()Ldbxyzptlk/db231222/x/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/c;->b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/R;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 264
    :catch_0
    move-exception v0

    .line 265
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final d(Ljava/lang/String;)Ldbxyzptlk/db231222/z/p;
    .locals 4

    .prologue
    .line 1406
    const-string v0, "/collection_share"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 1408
    :try_start_0
    new-instance v1, Ldbxyzptlk/db231222/z/p;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v0

    const-string v2, "metadata"

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/z/p;-><init>(Ldbxyzptlk/db231222/x/k;)V
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 1409
    :catch_0
    move-exception v0

    .line 1410
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final e(Ljava/lang/String;)Ldbxyzptlk/db231222/z/p;
    .locals 4

    .prologue
    .line 1415
    const-string v0, "/collection_unshare"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 1417
    :try_start_0
    new-instance v1, Ldbxyzptlk/db231222/z/p;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v0

    const-string v2, "metadata"

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/z/p;-><init>(Ldbxyzptlk/db231222/x/k;)V
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 1418
    :catch_0
    move-exception v0

    .line 1419
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 652
    :try_start_0
    const-string v0, "/unlink_access_token"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_0 .. :try_end_0} :catch_0

    .line 656
    :goto_0
    return-void

    .line 653
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 766
    const-string v0, "/deal_expiration_notification/dismiss"

    .line 767
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    .line 769
    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    .line 770
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1491
    const-string v0, "/collection_delete"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    .line 1492
    return-void
.end method

.method public final g(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1560
    const-string v0, "/notifications/user/subscribe"

    .line 1561
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "registration_id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    const-string v3, "locale"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/M;->a()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1566
    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 1572
    :try_start_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v0

    const-string v1, "subscription_id"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    iget-object v0, v0, Ldbxyzptlk/db231222/x/k;->a:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 1573
    :catch_0
    move-exception v0

    .line 1574
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final g()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 842
    const-string v0, "/camera_upload_hashes"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 844
    :try_start_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v0

    const-string v1, "hashes_8"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->c()Ldbxyzptlk/db231222/x/d;

    move-result-object v0

    new-instance v1, Ldbxyzptlk/db231222/z/ay;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ldbxyzptlk/db231222/z/ay;-><init>(Ldbxyzptlk/db231222/z/N;)V

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/d;->a(Ldbxyzptlk/db231222/x/c;)Ljava/util/ArrayList;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 845
    :catch_0
    move-exception v0

    .line 846
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final h()Ldbxyzptlk/db231222/z/ag;
    .locals 2

    .prologue
    .line 2100
    const-string v0, "/android/google_play_subscription_payload"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 2103
    :try_start_0
    sget-object v1, Ldbxyzptlk/db231222/z/ag;->a:Ldbxyzptlk/db231222/x/c;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/c;->b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/ag;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 2104
    :catch_0
    move-exception v0

    .line 2105
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final h(Ljava/lang/String;)Ldbxyzptlk/db231222/z/ap;
    .locals 4

    .prologue
    .line 1926
    const-string v0, "/qr_link"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "token"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    const-string v3, "platform"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "android"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/z/M;->b(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 1929
    :try_start_0
    sget-object v1, Ldbxyzptlk/db231222/z/ap;->a:Ldbxyzptlk/db231222/x/c;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/c;->b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/ap;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 1930
    :catch_0
    move-exception v0

    .line 1931
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
