.class public final Ldbxyzptlk/db231222/z/l;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final e:Ldbxyzptlk/db231222/x/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/x/c",
            "<",
            "Ldbxyzptlk/db231222/z/l;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ldbxyzptlk/db231222/z/n;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/z/n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 198
    new-instance v0, Ldbxyzptlk/db231222/z/m;

    invoke-direct {v0}, Ldbxyzptlk/db231222/z/m;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/z/l;->e:Ldbxyzptlk/db231222/x/c;

    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/x/k;)V
    .locals 3

    .prologue
    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    invoke-virtual {p1}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v0

    .line 187
    const-string v1, "id"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/x/k;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Ldbxyzptlk/db231222/z/l;->a:Ljava/lang/String;

    .line 188
    const-string v1, "sort_key"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/x/k;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Ldbxyzptlk/db231222/z/l;->b:Ljava/lang/String;

    .line 189
    new-instance v1, Ldbxyzptlk/db231222/z/n;

    const-string v2, "cover_file"

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v2

    invoke-direct {v1, v2}, Ldbxyzptlk/db231222/z/n;-><init>(Ldbxyzptlk/db231222/x/k;)V

    iput-object v1, p0, Ldbxyzptlk/db231222/z/l;->c:Ldbxyzptlk/db231222/z/n;

    .line 190
    const-string v1, "other_files"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->c()Ldbxyzptlk/db231222/x/d;

    move-result-object v0

    sget-object v1, Ldbxyzptlk/db231222/z/n;->c:Ldbxyzptlk/db231222/x/c;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/d;->a(Ldbxyzptlk/db231222/x/c;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/z/l;->d:Ljava/util/List;

    .line 191
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    invoke-static {p0}, Ldbxyzptlk/db231222/z/h;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
