.class public final Ldbxyzptlk/db231222/z/F;
.super Ldbxyzptlk/db231222/z/C;
.source "panda.py"


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/z/D;Ldbxyzptlk/db231222/v/r;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/z/C;-><init>(Ldbxyzptlk/db231222/z/D;Ldbxyzptlk/db231222/v/r;)V

    .line 69
    return-void
.end method

.method private c(Ljava/lang/String;Ljava/io/OutputStream;)V
    .locals 6

    .prologue
    .line 475
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 476
    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/F;->b()Ldbxyzptlk/db231222/z/D;

    move-result-object v1

    invoke-static {v1, v0}, Ldbxyzptlk/db231222/v/v;->a(Ldbxyzptlk/db231222/y/n;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 479
    :try_start_0
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    .line 480
    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    const/16 v2, 0xc8

    if-ne v1, v2, :cond_1

    .line 481
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 482
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentLength()J
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 483
    const/4 v1, 0x0

    .line 486
    :try_start_1
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 487
    invoke-static {v1, p2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    move-result v0

    .line 488
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-ltz v4, :cond_0

    int-to-long v4, v0

    cmp-long v0, v4, v2

    if-eqz v0, :cond_0

    .line 489
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 492
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-static {v1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    throw v0
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    .line 497
    :catch_0
    move-exception v0

    .line 498
    new-instance v1, Ldbxyzptlk/db231222/X/e;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/X/e;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 492
    :cond_0
    :try_start_3
    invoke-static {v1}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    .line 501
    return-void

    .line 495
    :cond_1
    new-instance v1, Ldbxyzptlk/db231222/w/i;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/w/i;-><init>(Lorg/apache/http/HttpResponse;)V

    throw v1
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;)Lcom/dropbox/android/filemanager/h;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 229
    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/F;->b()Ldbxyzptlk/db231222/z/D;

    move-result-object v3

    .line 230
    invoke-virtual {v3}, Ldbxyzptlk/db231222/z/D;->l()Ldbxyzptlk/db231222/y/r;

    move-result-object v1

    const-string v2, "dbx-sso://"

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/y/r;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/y/s;

    move-result-object v4

    .line 231
    new-instance v2, Ldbxyzptlk/db231222/P/a;

    invoke-direct {v2}, Ldbxyzptlk/db231222/P/a;-><init>()V

    .line 235
    const v1, 0x7f0d0012

    :try_start_0
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x0

    invoke-virtual {v2, v1, v5, p1}, Ldbxyzptlk/db231222/P/a;->a(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ldbxyzptlk/db231222/P/d; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldbxyzptlk/db231222/P/f; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldbxyzptlk/db231222/P/e; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ldbxyzptlk/db231222/P/c; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v1

    .line 237
    if-eqz v1, :cond_0

    .line 238
    :try_start_1
    invoke-virtual {v2, p1}, Ldbxyzptlk/db231222/P/a;->a(Landroid/content/Context;)Ldbxyzptlk/db231222/P/g;

    move-result-object v2

    .line 239
    invoke-virtual {v2}, Ldbxyzptlk/db231222/P/g;->a()Ljava/lang/String;

    move-result-object v0

    .line 241
    :cond_0
    const-string v2, "KNOX"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Got SAML assertion for user "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ldbxyzptlk/db231222/P/d; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ldbxyzptlk/db231222/P/f; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ldbxyzptlk/db231222/P/e; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ldbxyzptlk/db231222/P/c; {:try_start_1 .. :try_end_1} :catch_5

    .line 254
    :goto_0
    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 255
    invoke-virtual {p0, v1}, Ldbxyzptlk/db231222/z/F;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 256
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v4, Ldbxyzptlk/db231222/y/s;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&saml_token="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 267
    :goto_1
    new-instance v1, Lcom/dropbox/android/filemanager/h;

    iget-object v2, v4, Ldbxyzptlk/db231222/y/s;->b:Ldbxyzptlk/db231222/y/m;

    iget-object v2, v2, Ldbxyzptlk/db231222/y/m;->a:Ljava/lang/String;

    iget-object v3, v4, Ldbxyzptlk/db231222/y/s;->b:Ldbxyzptlk/db231222/y/m;

    iget-object v3, v3, Ldbxyzptlk/db231222/y/m;->b:Ljava/lang/String;

    invoke-direct {v1, p2, v0, v2, v3}, Lcom/dropbox/android/filemanager/h;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    .line 242
    :catch_0
    move-exception v1

    move-object v1, v0

    .line 243
    :goto_2
    const-string v2, "KNOX"

    const-string v5, "Couldn\'t find KNOX--skipping."

    invoke-static {v2, v5}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 244
    :catch_1
    move-exception v1

    move-object v2, v1

    move-object v1, v0

    .line 245
    :goto_3
    const-string v5, "KNOX"

    const-string v6, "Dropbox application not supported for KNOX"

    invoke-static {v5, v6, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 246
    :catch_2
    move-exception v1

    move-object v1, v0

    .line 247
    :goto_4
    const-string v2, "KNOX"

    const-string v5, "KNOX not provisioned--skipping."

    invoke-static {v2, v5}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 248
    :catch_3
    move-exception v1

    move-object v1, v0

    .line 251
    :goto_5
    const-string v2, "KNOX"

    const-string v5, "KNOX user not authenticated--skipping."

    invoke-static {v2, v5}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 260
    :cond_1
    :try_start_2
    iget-object v0, v4, Ldbxyzptlk/db231222/y/s;->a:Ljava/lang/String;

    const-string v1, "utf-8"

    invoke-static {v0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_4

    move-result-object v0

    .line 264
    const-string v1, "Expected an authorization URL"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 265
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Ldbxyzptlk/db231222/z/D;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/sso?from_mobile=true&login_email="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&cont="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 261
    :catch_4
    move-exception v0

    .line 262
    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 248
    :catch_5
    move-exception v2

    goto :goto_5

    .line 246
    :catch_6
    move-exception v2

    goto :goto_4

    .line 244
    :catch_7
    move-exception v2

    goto :goto_3

    .line 242
    :catch_8
    move-exception v2

    goto :goto_2
.end method

.method public final a(Ldbxyzptlk/db231222/i/c;)Ldbxyzptlk/db231222/t/e;
    .locals 3

    .prologue
    .line 418
    invoke-static {}, Lcom/dropbox/android/util/C;->b()V

    .line 419
    const-string v0, "/mobile_gandalf"

    .line 420
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, Lcom/dropbox/android/util/bd;->O:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Ldbxyzptlk/db231222/i/c;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/dropbox/android/util/bd;->q:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Ldbxyzptlk/db231222/i/c;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/dropbox/android/util/bd;->r:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Ldbxyzptlk/db231222/i/c;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dropbox/android/util/bd;->i:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Ldbxyzptlk/db231222/i/c;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dropbox/android/util/bd;->j:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "android"

    aput-object v2, v0, v1

    .line 428
    const-string v1, "/mobile_gandalf"

    invoke-virtual {p0, v1, v0}, Ldbxyzptlk/db231222/z/F;->a(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 430
    :try_start_0
    sget-object v1, Lcom/dropbox/android/util/analytics/r;->b:Ldbxyzptlk/db231222/x/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v0

    const-string v2, "data"

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/c;->b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/e;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 431
    :catch_0
    move-exception v0

    .line 432
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Ldbxyzptlk/db231222/y/m;Ljava/lang/String;)Ldbxyzptlk/db231222/z/H;
    .locals 5

    .prologue
    .line 271
    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/F;->b()Ldbxyzptlk/db231222/z/D;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/D;->l()Ldbxyzptlk/db231222/y/r;

    move-result-object v0

    .line 272
    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/y/r;->a(Ldbxyzptlk/db231222/y/m;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 276
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 280
    new-instance v2, Ldbxyzptlk/db231222/z/H;

    int-to-long v3, v1

    invoke-virtual {v0}, Ldbxyzptlk/db231222/y/r;->a()Ldbxyzptlk/db231222/y/k;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {v2, v3, v4, v0, v1}, Ldbxyzptlk/db231222/z/H;-><init>(JLdbxyzptlk/db231222/y/k;Ldbxyzptlk/db231222/z/G;)V

    return-object v2

    .line 277
    :catch_0
    move-exception v0

    .line 278
    new-instance v0, Ldbxyzptlk/db231222/w/a;

    const-string v1, "Bad user id retrieving access token"

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/w/a;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/z/H;
    .locals 3

    .prologue
    .line 288
    invoke-static {p1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/String;)V

    .line 290
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "email"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    const-string v2, "password"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    aput-object p2, v0, v1

    const/4 v1, 0x4

    const-string v2, "locale"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/F;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "anew"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "true"

    aput-object v2, v0, v1

    .line 298
    const-string v1, "/login"

    invoke-virtual {p0, v1, v0}, Ldbxyzptlk/db231222/z/F;->a(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 301
    :try_start_0
    sget-object v1, Ldbxyzptlk/db231222/z/H;->a:Ldbxyzptlk/db231222/x/c;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/c;->b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/H;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 302
    :catch_0
    move-exception v0

    .line 303
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/z/H;
    .locals 3

    .prologue
    .line 358
    invoke-static {p1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/String;)V

    .line 359
    invoke-static {p2}, Lcom/dropbox/android/util/C;->a(Ljava/lang/String;)V

    .line 361
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, Lcom/dropbox/android/util/bd;->s:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/dropbox/android/util/bd;->F:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    aput-object p2, v0, v1

    const/4 v1, 0x4

    const-string v2, "first_name"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    aput-object p3, v0, v1

    const/4 v1, 0x6

    const-string v2, "last_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    aput-object p4, v0, v1

    const/16 v1, 0x8

    const-string v2, "source"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/dropbox/android/util/bd;->p:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "locale"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/F;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "anew"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "true"

    aput-object v2, v0, v1

    .line 372
    const-string v1, "/account"

    invoke-virtual {p0, v1, v0}, Ldbxyzptlk/db231222/z/F;->a(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 374
    :try_start_0
    sget-object v1, Ldbxyzptlk/db231222/z/H;->a:Ldbxyzptlk/db231222/x/c;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/c;->b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/H;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 375
    :catch_0
    move-exception v0

    .line 376
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Ljava/lang/String;)Ldbxyzptlk/db231222/z/K;
    .locals 3

    .prologue
    .line 186
    invoke-static {p1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/String;)V

    .line 188
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "email"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    const-string v2, "locale"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/F;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 193
    const-string v1, "/check_sso_user"

    invoke-virtual {p0, v1, v0}, Ldbxyzptlk/db231222/z/F;->a(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 195
    :try_start_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v0

    .line 196
    const-string v1, "user_sso_state"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->g()Ljava/lang/String;

    move-result-object v0

    .line 197
    const-string v1, "none"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 198
    sget-object v0, Ldbxyzptlk/db231222/z/K;->a:Ldbxyzptlk/db231222/z/K;

    .line 206
    :goto_0
    return-object v0

    .line 200
    :cond_0
    const-string v1, "optional"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 201
    sget-object v0, Ldbxyzptlk/db231222/z/K;->b:Ldbxyzptlk/db231222/z/K;

    goto :goto_0

    .line 203
    :cond_1
    const-string v1, "required"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 204
    sget-object v0, Ldbxyzptlk/db231222/z/K;->c:Ldbxyzptlk/db231222/z/K;

    goto :goto_0

    .line 206
    :cond_2
    sget-object v0, Ldbxyzptlk/db231222/z/K;->a:Ldbxyzptlk/db231222/z/K;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 207
    :catch_0
    move-exception v0

    .line 208
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Ljava/lang/String;Ljava/io/OutputStream;)V
    .locals 0

    .prologue
    .line 505
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/z/F;->c(Ljava/lang/String;Ljava/io/OutputStream;)V

    .line 506
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/db231222/z/J;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/io/InputStream;JZ)V
    .locals 8

    .prologue
    .line 447
    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/F;->b()Ldbxyzptlk/db231222/z/D;

    move-result-object v4

    .line 448
    const/16 v3, 0x10

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    sget-object v6, Lcom/dropbox/android/util/bd;->j:Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    const/4 v5, 0x1

    aput-object p1, v3, v5

    const/4 v5, 0x2

    sget-object v6, Lcom/dropbox/android/util/bd;->q:Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    const/4 v5, 0x3

    aput-object p2, v3, v5

    const/4 v5, 0x4

    const-string v6, "log_level"

    aput-object v6, v3, v5

    const/4 v5, 0x5

    invoke-virtual {p3}, Ldbxyzptlk/db231222/z/J;->name()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    const/4 v5, 0x6

    sget-object v6, Lcom/dropbox/android/util/bd;->h:Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    const/4 v5, 0x7

    if-nez p4, :cond_0

    const-string p4, "0"

    :cond_0
    aput-object p4, v3, v5

    const/16 v5, 0x8

    sget-object v6, Lcom/dropbox/android/util/bd;->O:Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    const/16 v5, 0x9

    aput-object p5, v3, v5

    const/16 v5, 0xa

    sget-object v6, Lcom/dropbox/android/util/bd;->r:Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    const/16 v5, 0xb

    aput-object p6, v3, v5

    const/16 v5, 0xc

    sget-object v6, Lcom/dropbox/android/util/bd;->i:Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    const/16 v5, 0xd

    aput-object p7, v3, v5

    const/16 v5, 0xe

    const-string v6, "ts"

    aput-object v6, v3, v5

    const/16 v5, 0xf

    invoke-static/range {p8 .. p9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    .line 458
    invoke-virtual {v4}, Ldbxyzptlk/db231222/z/D;->m()Ljava/lang/String;

    move-result-object v5

    const-string v6, "r6"

    sget-object v7, Lcom/dropbox/android/util/bd;->D:Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7, v3}, Ldbxyzptlk/db231222/v/v;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 461
    new-instance v5, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v5, v3}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    .line 462
    invoke-virtual {v4, v5}, Ldbxyzptlk/db231222/z/D;->a(Lorg/apache/http/HttpRequest;)V

    .line 464
    new-instance v6, Lorg/apache/http/entity/InputStreamEntity;

    move-object/from16 v0, p10

    move-wide/from16 v1, p11

    invoke-direct {v6, v0, v1, v2}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    .line 465
    if-eqz p13, :cond_1

    const-string v3, "gzip"

    :goto_0
    invoke-virtual {v6, v3}, Lorg/apache/http/entity/InputStreamEntity;->setContentEncoding(Ljava/lang/String;)V

    .line 466
    const-string v3, "text/plain"

    invoke-virtual {v6, v3}, Lorg/apache/http/entity/InputStreamEntity;->setContentType(Ljava/lang/String;)V

    .line 467
    const/4 v3, 0x0

    invoke-virtual {v6, v3}, Lorg/apache/http/entity/InputStreamEntity;->setChunked(Z)V

    .line 468
    invoke-virtual {v5, v6}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 470
    invoke-static {v4, v5}, Ldbxyzptlk/db231222/v/v;->a(Ldbxyzptlk/db231222/y/n;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    .line 471
    return-void

    .line 465
    :cond_1
    const-string v3, "application/octet-stream"

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/z/H;
    .locals 3

    .prologue
    .line 311
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "checkpoint_token"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    const-string v2, "twofactor_code"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    aput-object p2, v0, v1

    const/4 v1, 0x4

    const-string v2, "locale"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/F;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 317
    const-string v1, "/twofactor_verify"

    invoke-virtual {p0, v1, v0}, Ldbxyzptlk/db231222/z/F;->a(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 319
    :try_start_0
    sget-object v1, Ldbxyzptlk/db231222/z/H;->a:Ldbxyzptlk/db231222/x/c;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/c;->b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/H;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 320
    :catch_0
    move-exception v0

    .line 321
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 214
    invoke-static {p1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/String;)V

    .line 216
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "saml_assertion"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 221
    :try_start_0
    const-string v1, "/save_saml_assertion"

    invoke-virtual {p0, v1, v0}, Ldbxyzptlk/db231222/z/F;->a(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 222
    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v0

    const-string v1, "saml_token"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->g()Ljava/lang/String;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 223
    :catch_0
    move-exception v0

    .line 224
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(Ljava/lang/String;Ljava/io/OutputStream;)V
    .locals 0

    .prologue
    .line 510
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/z/F;->c(Ljava/lang/String;Ljava/io/OutputStream;)V

    .line 511
    return-void
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 330
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "checkpoint_token"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    const-string v2, "locale"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/F;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 335
    const-string v1, "/twofactor_resend"

    invoke-virtual {p0, v1, v0}, Ldbxyzptlk/db231222/z/F;->a(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 337
    :try_start_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v0

    const-string v1, "twofactor_desc"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->g()Ljava/lang/String;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 338
    :catch_0
    move-exception v0

    .line 339
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 351
    invoke-static {p1}, Lcom/dropbox/android/util/C;->a(Ljava/lang/String;)V

    .line 352
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "email"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 353
    const-string v1, "/password_reset"

    invoke-virtual {p0, v1, v0}, Ldbxyzptlk/db231222/z/F;->a(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    .line 354
    return-void
.end method

.method public final e(Ljava/lang/String;)Ldbxyzptlk/db231222/z/H;
    .locals 3

    .prologue
    .line 385
    const-string v0, "/validate_htc_token"

    .line 386
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "htc_token"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 390
    const-string v1, "/validate_htc_token"

    invoke-virtual {p0, v1, v0}, Ldbxyzptlk/db231222/z/F;->a(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 392
    :try_start_0
    sget-object v1, Ldbxyzptlk/db231222/z/H;->a:Ldbxyzptlk/db231222/x/c;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/c;->b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/H;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 393
    :catch_0
    move-exception v0

    .line 394
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final f(Ljava/lang/String;)Ldbxyzptlk/db231222/z/H;
    .locals 3

    .prologue
    .line 403
    const-string v0, "/validate_web_session_token"

    .line 404
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "web_session_token"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 408
    const-string v1, "/validate_web_session_token"

    invoke-virtual {p0, v1, v0}, Ldbxyzptlk/db231222/z/F;->a(Ljava/lang/String;[Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 410
    :try_start_0
    sget-object v1, Ldbxyzptlk/db231222/z/H;->a:Ldbxyzptlk/db231222/x/c;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/x/c;->b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/H;
    :try_end_0
    .catch Ldbxyzptlk/db231222/x/b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 411
    :catch_0
    move-exception v0

    .line 412
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
