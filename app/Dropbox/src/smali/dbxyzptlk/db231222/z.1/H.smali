.class public final Ldbxyzptlk/db231222/z/H;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final a:Ldbxyzptlk/db231222/x/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/x/c",
            "<",
            "Ldbxyzptlk/db231222/z/H;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:J

.field private final c:Ldbxyzptlk/db231222/y/k;

.field private final d:Ljava/lang/String;

.field private final e:Ldbxyzptlk/db231222/n/s;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 148
    new-instance v0, Ldbxyzptlk/db231222/z/I;

    invoke-direct {v0}, Ldbxyzptlk/db231222/z/I;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/z/H;->a:Ldbxyzptlk/db231222/x/c;

    return-void
.end method

.method private constructor <init>(JLdbxyzptlk/db231222/y/k;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-wide p1, p0, Ldbxyzptlk/db231222/z/H;->b:J

    .line 91
    iput-object p3, p0, Ldbxyzptlk/db231222/z/H;->c:Ldbxyzptlk/db231222/y/k;

    .line 92
    iput-object v0, p0, Ldbxyzptlk/db231222/z/H;->d:Ljava/lang/String;

    .line 93
    iput-object v0, p0, Ldbxyzptlk/db231222/z/H;->e:Ldbxyzptlk/db231222/n/s;

    .line 94
    return-void
.end method

.method synthetic constructor <init>(JLdbxyzptlk/db231222/y/k;Ldbxyzptlk/db231222/z/G;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1, p2, p3}, Ldbxyzptlk/db231222/z/H;-><init>(JLdbxyzptlk/db231222/y/k;)V

    return-void
.end method

.method private constructor <init>(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    const-string v0, "uid"

    invoke-static {p1, v0}, Ldbxyzptlk/db231222/v/a;->b(Ljava/util/Map;Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/z/H;->b:J

    .line 99
    const/4 v0, 0x0

    .line 100
    const-string v1, "requires_twofactor"

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 101
    const-string v0, "requires_twofactor"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 104
    :cond_0
    if-eqz v0, :cond_1

    .line 105
    const-string v0, "checkpoint_token_ttl"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    .line 106
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v2, v0

    .line 108
    iput-object v4, p0, Ldbxyzptlk/db231222/z/H;->c:Ldbxyzptlk/db231222/y/k;

    .line 109
    new-instance v0, Ldbxyzptlk/db231222/n/s;

    const-string v1, "checkpoint_token"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v4, "twofactor_desc"

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, "twofactor_delivery_mode"

    invoke-interface {p1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ldbxyzptlk/db231222/n/t;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/n/t;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/n/s;-><init>(Ljava/lang/String;JLjava/lang/String;Ldbxyzptlk/db231222/n/t;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/z/H;->e:Ldbxyzptlk/db231222/n/s;

    .line 120
    :goto_0
    const-string v0, "email"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/db231222/z/H;->d:Ljava/lang/String;

    .line 121
    return-void

    .line 115
    :cond_1
    new-instance v2, Ldbxyzptlk/db231222/y/k;

    const-string v0, "token"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "secret"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v2, v0, v1}, Ldbxyzptlk/db231222/y/k;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Ldbxyzptlk/db231222/z/H;->c:Ldbxyzptlk/db231222/y/k;

    .line 118
    iput-object v4, p0, Ldbxyzptlk/db231222/z/H;->e:Ldbxyzptlk/db231222/n/s;

    goto :goto_0
.end method

.method synthetic constructor <init>(Ljava/util/Map;Ldbxyzptlk/db231222/z/G;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/z/H;-><init>(Ljava/util/Map;)V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Ldbxyzptlk/db231222/z/H;->e:Ldbxyzptlk/db231222/n/s;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ldbxyzptlk/db231222/n/s;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Ldbxyzptlk/db231222/z/H;->e:Ldbxyzptlk/db231222/n/s;

    return-object v0
.end method

.method public final c()Ldbxyzptlk/db231222/y/k;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Ldbxyzptlk/db231222/z/H;->c:Ldbxyzptlk/db231222/y/k;

    return-object v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 136
    iget-wide v0, p0, Ldbxyzptlk/db231222/z/H;->b:J

    return-wide v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Ldbxyzptlk/db231222/z/H;->d:Ljava/lang/String;

    return-object v0
.end method
