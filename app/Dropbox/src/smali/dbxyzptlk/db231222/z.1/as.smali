.class public final Ldbxyzptlk/db231222/z/as;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final a:Ldbxyzptlk/db231222/x/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/x/c",
            "<",
            "Ldbxyzptlk/db231222/z/as;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ldbxyzptlk/db231222/z/au;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1789
    new-instance v0, Ldbxyzptlk/db231222/z/at;

    invoke-direct {v0}, Ldbxyzptlk/db231222/z/at;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/z/as;->a:Ldbxyzptlk/db231222/x/c;

    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/z/au;)V
    .locals 0

    .prologue
    .line 1781
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1782
    iput-object p1, p0, Ldbxyzptlk/db231222/z/as;->b:Ldbxyzptlk/db231222/z/au;

    .line 1783
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1764
    const-string v0, "okay"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1765
    sget-object v0, Ldbxyzptlk/db231222/z/au;->a:Ldbxyzptlk/db231222/z/au;

    iput-object v0, p0, Ldbxyzptlk/db231222/z/as;->b:Ldbxyzptlk/db231222/z/au;

    .line 1779
    :goto_0
    return-void

    .line 1766
    :cond_0
    const-string v0, "okay_max_invited"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1767
    sget-object v0, Ldbxyzptlk/db231222/z/au;->b:Ldbxyzptlk/db231222/z/au;

    iput-object v0, p0, Ldbxyzptlk/db231222/z/as;->b:Ldbxyzptlk/db231222/z/au;

    goto :goto_0

    .line 1768
    :cond_1
    const-string v0, "err_too_many"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1769
    sget-object v0, Ldbxyzptlk/db231222/z/au;->c:Ldbxyzptlk/db231222/z/au;

    iput-object v0, p0, Ldbxyzptlk/db231222/z/as;->b:Ldbxyzptlk/db231222/z/au;

    goto :goto_0

    .line 1770
    :cond_2
    const-string v0, "err_enter_one"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1771
    sget-object v0, Ldbxyzptlk/db231222/z/au;->d:Ldbxyzptlk/db231222/z/au;

    iput-object v0, p0, Ldbxyzptlk/db231222/z/as;->b:Ldbxyzptlk/db231222/z/au;

    goto :goto_0

    .line 1772
    :cond_3
    const-string v0, "err_already_sent_max"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1773
    sget-object v0, Ldbxyzptlk/db231222/z/au;->e:Ldbxyzptlk/db231222/z/au;

    iput-object v0, p0, Ldbxyzptlk/db231222/z/as;->b:Ldbxyzptlk/db231222/z/au;

    goto :goto_0

    .line 1774
    :cond_4
    const-string v0, "err"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1775
    sget-object v0, Ldbxyzptlk/db231222/z/au;->f:Ldbxyzptlk/db231222/z/au;

    iput-object v0, p0, Ldbxyzptlk/db231222/z/as;->b:Ldbxyzptlk/db231222/z/au;

    goto :goto_0

    .line 1777
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method


# virtual methods
.method public final a()Ldbxyzptlk/db231222/z/au;
    .locals 1

    .prologue
    .line 1786
    iget-object v0, p0, Ldbxyzptlk/db231222/z/as;->b:Ldbxyzptlk/db231222/z/au;

    return-object v0
.end method
