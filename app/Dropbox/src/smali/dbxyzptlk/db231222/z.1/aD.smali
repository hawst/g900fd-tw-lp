.class public final enum Ldbxyzptlk/db231222/z/aD;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldbxyzptlk/db231222/z/aD;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ldbxyzptlk/db231222/z/aD;

.field public static final enum b:Ldbxyzptlk/db231222/z/aD;

.field public static final enum c:Ldbxyzptlk/db231222/z/aD;

.field private static final synthetic e:[Ldbxyzptlk/db231222/z/aD;


# instance fields
.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1955
    new-instance v0, Ldbxyzptlk/db231222/z/aD;

    const-string v1, "STARTED"

    const-string v2, "subscription_started"

    invoke-direct {v0, v1, v3, v2}, Ldbxyzptlk/db231222/z/aD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/z/aD;->a:Ldbxyzptlk/db231222/z/aD;

    .line 1961
    new-instance v0, Ldbxyzptlk/db231222/z/aD;

    const-string v1, "USED"

    const-string v2, "subscription_already_used"

    invoke-direct {v0, v1, v4, v2}, Ldbxyzptlk/db231222/z/aD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/z/aD;->b:Ldbxyzptlk/db231222/z/aD;

    .line 1966
    new-instance v0, Ldbxyzptlk/db231222/z/aD;

    const-string v1, "USED_BY_THIS_USER"

    const-string v2, "subscription_already_used_by_user"

    invoke-direct {v0, v1, v5, v2}, Ldbxyzptlk/db231222/z/aD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/z/aD;->c:Ldbxyzptlk/db231222/z/aD;

    .line 1953
    const/4 v0, 0x3

    new-array v0, v0, [Ldbxyzptlk/db231222/z/aD;

    sget-object v1, Ldbxyzptlk/db231222/z/aD;->a:Ldbxyzptlk/db231222/z/aD;

    aput-object v1, v0, v3

    sget-object v1, Ldbxyzptlk/db231222/z/aD;->b:Ldbxyzptlk/db231222/z/aD;

    aput-object v1, v0, v4

    sget-object v1, Ldbxyzptlk/db231222/z/aD;->c:Ldbxyzptlk/db231222/z/aD;

    aput-object v1, v0, v5

    sput-object v0, Ldbxyzptlk/db231222/z/aD;->e:[Ldbxyzptlk/db231222/z/aD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1970
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1971
    iput-object p3, p0, Ldbxyzptlk/db231222/z/aD;->d:Ljava/lang/String;

    .line 1972
    return-void
.end method

.method public static a(Ljava/lang/String;)Ldbxyzptlk/db231222/z/aD;
    .locals 5

    .prologue
    .line 1975
    invoke-static {}, Ldbxyzptlk/db231222/z/aD;->values()[Ldbxyzptlk/db231222/z/aD;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1976
    iget-object v4, v3, Ldbxyzptlk/db231222/z/aD;->d:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1977
    return-object v3

    .line 1975
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1980
    :cond_1
    new-instance v0, Ldbxyzptlk/db231222/x/b;

    const-string v1, ""

    const-string v2, "Unexpected value for ResponseType"

    invoke-direct {v0, v1, v2, p0}, Ldbxyzptlk/db231222/x/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Ldbxyzptlk/db231222/z/aD;
    .locals 1

    .prologue
    .line 1953
    const-class v0, Ldbxyzptlk/db231222/z/aD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/aD;

    return-object v0
.end method

.method public static values()[Ldbxyzptlk/db231222/z/aD;
    .locals 1

    .prologue
    .line 1953
    sget-object v0, Ldbxyzptlk/db231222/z/aD;->e:[Ldbxyzptlk/db231222/z/aD;

    invoke-virtual {v0}, [Ldbxyzptlk/db231222/z/aD;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/z/aD;

    return-object v0
.end method
