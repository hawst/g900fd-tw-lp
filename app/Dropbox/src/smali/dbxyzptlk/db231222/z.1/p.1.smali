.class public final Ldbxyzptlk/db231222/z/p;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final h:Ldbxyzptlk/db231222/x/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/x/c",
            "<",
            "Ldbxyzptlk/db231222/z/p;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:J

.field public final d:Ljava/util/Date;

.field public final e:Ljava/util/Date;

.field public final f:Ljava/lang/String;

.field public final g:Ldbxyzptlk/db231222/z/n;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 136
    new-instance v0, Ldbxyzptlk/db231222/z/q;

    invoke-direct {v0}, Ldbxyzptlk/db231222/z/q;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/z/p;->h:Ldbxyzptlk/db231222/x/c;

    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/x/k;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    invoke-virtual {p1}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v0

    .line 109
    const-string v1, "id"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/x/k;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Ldbxyzptlk/db231222/z/p;->a:Ljava/lang/String;

    .line 110
    const-string v1, "name"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/x/k;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Ldbxyzptlk/db231222/z/p;->b:Ljava/lang/String;

    .line 111
    const-string v1, "count"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/x/k;->e()J

    move-result-wide v1

    iput-wide v1, p0, Ldbxyzptlk/db231222/z/p;->c:J

    .line 113
    const-string v1, "creation_time"

    invoke-static {v0, v1, v3}, Ldbxyzptlk/db231222/z/h;->a(Ldbxyzptlk/db231222/x/g;Ljava/lang/String;Z)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Ldbxyzptlk/db231222/z/p;->e:Ljava/util/Date;

    .line 114
    const-string v1, "update_time"

    invoke-static {v0, v1, v3}, Ldbxyzptlk/db231222/z/h;->a(Ldbxyzptlk/db231222/x/g;Ljava/lang/String;Z)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Ldbxyzptlk/db231222/z/p;->d:Ljava/util/Date;

    .line 116
    const-string v1, "share_link"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->c(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v1

    .line 117
    if-eqz v1, :cond_1

    .line 118
    invoke-virtual {v1}, Ldbxyzptlk/db231222/x/k;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Ldbxyzptlk/db231222/z/p;->f:Ljava/lang/String;

    .line 123
    :goto_0
    const-string v1, "cover_file"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->c(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    .line 124
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 125
    :cond_0
    iput-object v4, p0, Ldbxyzptlk/db231222/z/p;->g:Ldbxyzptlk/db231222/z/n;

    .line 129
    :goto_1
    return-void

    .line 120
    :cond_1
    iput-object v4, p0, Ldbxyzptlk/db231222/z/p;->f:Ljava/lang/String;

    goto :goto_0

    .line 127
    :cond_2
    new-instance v1, Ldbxyzptlk/db231222/z/n;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/z/n;-><init>(Ldbxyzptlk/db231222/x/k;)V

    iput-object v1, p0, Ldbxyzptlk/db231222/z/p;->g:Ldbxyzptlk/db231222/z/n;

    goto :goto_1
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    invoke-static {p0}, Ldbxyzptlk/db231222/z/h;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
