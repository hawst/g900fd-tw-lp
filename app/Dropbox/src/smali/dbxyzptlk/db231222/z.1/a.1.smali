.class public Ldbxyzptlk/db231222/z/a;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field static final synthetic a:Z

.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ldbxyzptlk/db231222/z/c;",
            ">;"
        }
    .end annotation
.end field

.field private d:J

.field private final e:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Ldbxyzptlk/db231222/z/a;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Ldbxyzptlk/db231222/z/a;->a:Z

    .line 26
    const-class v0, Ldbxyzptlk/db231222/z/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/z/a;->b:Ljava/lang/String;

    return-void

    .line 24
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(JLjava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/ArrayList",
            "<",
            "Ldbxyzptlk/db231222/z/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ldbxyzptlk/db231222/z/a;->d:J

    .line 201
    iput-wide p1, p0, Ldbxyzptlk/db231222/z/a;->e:J

    .line 202
    iput-object p3, p0, Ldbxyzptlk/db231222/z/a;->c:Ljava/util/ArrayList;

    .line 203
    return-void
.end method

.method static synthetic a(Ldbxyzptlk/db231222/z/a;)J
    .locals 2

    .prologue
    .line 24
    iget-wide v0, p0, Ldbxyzptlk/db231222/z/a;->d:J

    return-wide v0
.end method

.method public static a(DLdbxyzptlk/db231222/v/s;J)Landroid/util/Pair;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(D",
            "Ldbxyzptlk/db231222/v/s;",
            "J)",
            "Landroid/util/Pair",
            "<",
            "Ldbxyzptlk/db231222/v/s;",
            "Ldbxyzptlk/db231222/v/s;",
            ">;"
        }
    .end annotation

    .prologue
    .line 465
    const/4 v0, 0x0

    .line 466
    const/4 v1, 0x0

    .line 467
    if-eqz p2, :cond_0

    .line 468
    new-instance v0, Ldbxyzptlk/db231222/z/e;

    const-wide/16 v3, 0x0

    const/4 v8, 0x0

    move-wide v1, p0

    move-wide v5, p3

    move-object v7, p2

    invoke-direct/range {v0 .. v8}, Ldbxyzptlk/db231222/z/e;-><init>(DDJLdbxyzptlk/db231222/v/s;Ldbxyzptlk/db231222/z/b;)V

    .line 469
    new-instance v1, Ldbxyzptlk/db231222/z/e;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v2, p0

    const/4 v9, 0x0

    move-wide v4, p0

    move-wide v6, p3

    move-object v8, p2

    invoke-direct/range {v1 .. v9}, Ldbxyzptlk/db231222/z/e;-><init>(DDJLdbxyzptlk/db231222/v/s;Ldbxyzptlk/db231222/z/b;)V

    .line 471
    :cond_0
    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2
.end method

.method public static a(Ljava/io/FileInputStream;JLdbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/z/g;
    .locals 5

    .prologue
    .line 128
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-static {v0, v1, p3, p1, p2}, Ldbxyzptlk/db231222/z/a;->a(DLdbxyzptlk/db231222/v/s;J)Landroid/util/Pair;

    move-result-object v1

    .line 129
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ldbxyzptlk/db231222/v/s;

    invoke-static {p0, p1, p2, v0}, Ldbxyzptlk/db231222/z/a;->c(Ljava/io/FileInputStream;JLdbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/z/a;

    move-result-object v2

    .line 130
    new-instance v3, Ldbxyzptlk/db231222/z/g;

    const/4 v4, 0x0

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ldbxyzptlk/db231222/v/s;

    invoke-direct {v3, v2, v4, v0}, Ldbxyzptlk/db231222/z/g;-><init>(Ldbxyzptlk/db231222/z/a;Ldbxyzptlk/db231222/z/f;Ldbxyzptlk/db231222/v/s;)V

    return-object v3
.end method

.method public static a([B)Ljava/lang/String;
    .locals 6

    .prologue
    .line 287
    new-instance v1, Ljava/lang/StringBuilder;

    array-length v0, p0

    mul-int/lit8 v0, v0, 0x2

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 288
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-byte v3, p0, v0

    .line 289
    const-string v4, "0123456789abcdef"

    and-int/lit16 v5, v3, 0xf0

    ushr-int/lit8 v5, v5, 0x4

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 290
    const-string v4, "0123456789abcdef"

    and-int/lit8 v3, v3, 0xf

    invoke-virtual {v4, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 288
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 292
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ldbxyzptlk/db231222/z/a;)J
    .locals 2

    .prologue
    .line 24
    iget-wide v0, p0, Ldbxyzptlk/db231222/z/a;->e:J

    return-wide v0
.end method

.method private b(Ljava/lang/String;)Ldbxyzptlk/db231222/z/c;
    .locals 3

    .prologue
    .line 262
    iget-object v0, p0, Ldbxyzptlk/db231222/z/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/c;

    .line 263
    iget-object v2, v0, Ldbxyzptlk/db231222/z/c;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 267
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/io/FileInputStream;JLdbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/z/g;
    .locals 4

    .prologue
    .line 155
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-static {v0, v1, p3, p1, p2}, Ldbxyzptlk/db231222/z/a;->a(DLdbxyzptlk/db231222/v/s;J)Landroid/util/Pair;

    move-result-object v2

    .line 156
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ldbxyzptlk/db231222/v/s;

    invoke-static {p0, p1, p2, v0}, Ldbxyzptlk/db231222/z/a;->d(Ljava/io/FileInputStream;JLdbxyzptlk/db231222/v/s;)Landroid/util/Pair;

    move-result-object v1

    .line 157
    new-instance v3, Ldbxyzptlk/db231222/z/g;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ldbxyzptlk/db231222/z/a;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ldbxyzptlk/db231222/z/f;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ldbxyzptlk/db231222/v/s;

    invoke-direct {v3, v0, v1, v2}, Ldbxyzptlk/db231222/z/g;-><init>(Ldbxyzptlk/db231222/z/a;Ldbxyzptlk/db231222/z/f;Ldbxyzptlk/db231222/v/s;)V

    return-object v3
.end method

.method private static c(Ljava/io/FileInputStream;JLdbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/z/a;
    .locals 3

    .prologue
    .line 161
    invoke-virtual {p0}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v0

    .line 162
    new-instance v2, Ldbxyzptlk/db231222/z/d;

    invoke-direct {v2, v0, v1}, Ldbxyzptlk/db231222/z/d;-><init>(J)V

    .line 164
    :try_start_0
    invoke-virtual {v2, p0, p1, p2, p3}, Ldbxyzptlk/db231222/z/d;->a(Ljava/io/FileInputStream;JLdbxyzptlk/db231222/v/s;)V

    .line 165
    new-instance v0, Ldbxyzptlk/db231222/z/a;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/z/d;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {v0, p1, p2, v1}, Ldbxyzptlk/db231222/z/a;-><init>(JLjava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 167
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V

    throw v0
.end method

.method private static d(Ljava/io/FileInputStream;JLdbxyzptlk/db231222/v/s;)Landroid/util/Pair;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/FileInputStream;",
            "J",
            "Ldbxyzptlk/db231222/v/s;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ldbxyzptlk/db231222/z/a;",
            "Ldbxyzptlk/db231222/z/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 176
    invoke-virtual {p0}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v0

    .line 177
    new-instance v2, Ldbxyzptlk/db231222/z/d;

    invoke-direct {v2, v0, v1}, Ldbxyzptlk/db231222/z/d;-><init>(J)V

    .line 181
    :try_start_0
    const-string v0, "MD5"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 182
    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/z/d;->a(Ljava/security/MessageDigest;)V

    .line 183
    const-string v1, "MD5"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 184
    invoke-virtual {v2, v1, p1, p2}, Ldbxyzptlk/db231222/z/d;->a(Ljava/security/MessageDigest;J)V

    .line 185
    invoke-virtual {v2, p0, p1, p2, p3}, Ldbxyzptlk/db231222/z/d;->a(Ljava/io/FileInputStream;JLdbxyzptlk/db231222/v/s;)V

    .line 186
    new-instance v3, Ldbxyzptlk/db231222/z/f;

    invoke-direct {v3, v0, v1}, Ldbxyzptlk/db231222/z/f;-><init>(Ljava/security/MessageDigest;Ljava/security/MessageDigest;)V

    .line 187
    new-instance v0, Landroid/util/Pair;

    new-instance v1, Ldbxyzptlk/db231222/z/a;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/z/d;->a()Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {v1, p1, p2, v4}, Ldbxyzptlk/db231222/z/a;-><init>(JLjava/util/ArrayList;)V

    invoke-direct {v0, v1, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V

    :goto_0
    return-object v0

    .line 188
    :catch_0
    move-exception v0

    .line 189
    :try_start_1
    sget-object v1, Ldbxyzptlk/db231222/z/a;->b:Ljava/lang/String;

    const-string v3, "fromStreamWithFull"

    invoke-static {v1, v3, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 190
    const/4 v0, 0x0

    .line 192
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/OutputStream;)V

    throw v0
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Ldbxyzptlk/db231222/z/a;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 206
    iget-wide v0, p0, Ldbxyzptlk/db231222/z/a;->e:J

    return-wide v0
.end method

.method public final a(Ldbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/v/s;
    .locals 1

    .prologue
    .line 242
    new-instance v0, Ldbxyzptlk/db231222/z/b;

    invoke-direct {v0, p0, p1}, Ldbxyzptlk/db231222/z/b;-><init>(Ldbxyzptlk/db231222/z/a;Ldbxyzptlk/db231222/v/s;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 271
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/z/a;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/z/c;

    move-result-object v0

    .line 272
    iget-wide v1, p0, Ldbxyzptlk/db231222/z/a;->d:J

    iget-wide v3, v0, Ldbxyzptlk/db231222/z/c;->c:J

    add-long/2addr v1, v3

    iput-wide v1, p0, Ldbxyzptlk/db231222/z/a;->d:J

    .line 273
    const/4 v1, 0x0

    iput-boolean v1, v0, Ldbxyzptlk/db231222/z/c;->d:Z

    .line 274
    return-void
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 222
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ldbxyzptlk/db231222/z/a;->d:J

    .line 223
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 224
    const/4 v0, 0x0

    .line 225
    iget-object v1, p0, Ldbxyzptlk/db231222/z/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/c;

    .line 226
    iget-object v4, v0, Ldbxyzptlk/db231222/z/c;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    iput-boolean v4, v0, Ldbxyzptlk/db231222/z/c;->d:Z

    .line 227
    iget-boolean v4, v0, Ldbxyzptlk/db231222/z/c;->d:Z

    if-eqz v4, :cond_0

    .line 228
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 232
    goto :goto_0

    .line 230
    :cond_0
    iget-wide v4, p0, Ldbxyzptlk/db231222/z/a;->d:J

    iget-wide v6, v0, Ldbxyzptlk/db231222/z/c;->c:J

    add-long/2addr v4, v6

    iput-wide v4, p0, Ldbxyzptlk/db231222/z/a;->d:J

    move v0, v1

    goto :goto_1

    .line 233
    :cond_1
    sget-boolean v0, Ldbxyzptlk/db231222/z/a;->a:Z

    if-nez v0, :cond_2

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v0

    if-eq v1, v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 234
    :cond_2
    return-void
.end method

.method public final b()Ldbxyzptlk/db231222/z/c;
    .locals 3

    .prologue
    .line 210
    iget-object v0, p0, Ldbxyzptlk/db231222/z/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/c;

    .line 211
    iget-boolean v2, v0, Ldbxyzptlk/db231222/z/c;->d:Z

    if-eqz v2, :cond_0

    .line 215
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 258
    invoke-virtual {p0}, Ldbxyzptlk/db231222/z/a;->b()Ldbxyzptlk/db231222/z/c;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 277
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Ldbxyzptlk/db231222/z/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 278
    iget-object v0, p0, Ldbxyzptlk/db231222/z/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/c;

    .line 279
    iget-object v0, v0, Ldbxyzptlk/db231222/z/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 281
    :cond_0
    return-object v1
.end method
