.class public final Ldbxyzptlk/db231222/z/n;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final c:Ldbxyzptlk/db231222/x/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/x/c",
            "<",
            "Ldbxyzptlk/db231222/z/n;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/dropbox/android/util/DropboxPath;

.field public final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 163
    new-instance v0, Ldbxyzptlk/db231222/z/o;

    invoke-direct {v0}, Ldbxyzptlk/db231222/z/o;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/z/n;->c:Ldbxyzptlk/db231222/x/c;

    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/x/k;)V
    .locals 4

    .prologue
    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    invoke-virtual {p1}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v0

    .line 154
    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    const-string v2, "path"

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/x/k;->g()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;Z)V

    iput-object v1, p0, Ldbxyzptlk/db231222/z/n;->a:Lcom/dropbox/android/util/DropboxPath;

    .line 155
    const-string v1, "rev"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/z/n;->b:Ljava/lang/String;

    .line 156
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    invoke-static {p0}, Ldbxyzptlk/db231222/z/h;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
