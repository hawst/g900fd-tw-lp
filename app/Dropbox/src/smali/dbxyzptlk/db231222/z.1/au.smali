.class public final enum Ldbxyzptlk/db231222/z/au;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldbxyzptlk/db231222/z/au;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ldbxyzptlk/db231222/z/au;

.field public static final enum b:Ldbxyzptlk/db231222/z/au;

.field public static final enum c:Ldbxyzptlk/db231222/z/au;

.field public static final enum d:Ldbxyzptlk/db231222/z/au;

.field public static final enum e:Ldbxyzptlk/db231222/z/au;

.field public static final enum f:Ldbxyzptlk/db231222/z/au;

.field private static final synthetic g:[Ldbxyzptlk/db231222/z/au;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1748
    new-instance v0, Ldbxyzptlk/db231222/z/au;

    const-string v1, "OKAY"

    invoke-direct {v0, v1, v3}, Ldbxyzptlk/db231222/z/au;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbxyzptlk/db231222/z/au;->a:Ldbxyzptlk/db231222/z/au;

    .line 1750
    new-instance v0, Ldbxyzptlk/db231222/z/au;

    const-string v1, "OKAY_MAX_REFERRED"

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/db231222/z/au;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbxyzptlk/db231222/z/au;->b:Ldbxyzptlk/db231222/z/au;

    .line 1752
    new-instance v0, Ldbxyzptlk/db231222/z/au;

    const-string v1, "ERR_TOO_MANY"

    invoke-direct {v0, v1, v5}, Ldbxyzptlk/db231222/z/au;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbxyzptlk/db231222/z/au;->c:Ldbxyzptlk/db231222/z/au;

    .line 1754
    new-instance v0, Ldbxyzptlk/db231222/z/au;

    const-string v1, "ERR_ENTER_ONE"

    invoke-direct {v0, v1, v6}, Ldbxyzptlk/db231222/z/au;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbxyzptlk/db231222/z/au;->d:Ldbxyzptlk/db231222/z/au;

    .line 1756
    new-instance v0, Ldbxyzptlk/db231222/z/au;

    const-string v1, "ERR_ALREADY_SENT_MAX"

    invoke-direct {v0, v1, v7}, Ldbxyzptlk/db231222/z/au;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbxyzptlk/db231222/z/au;->e:Ldbxyzptlk/db231222/z/au;

    .line 1758
    new-instance v0, Ldbxyzptlk/db231222/z/au;

    const-string v1, "ERR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/z/au;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbxyzptlk/db231222/z/au;->f:Ldbxyzptlk/db231222/z/au;

    .line 1746
    const/4 v0, 0x6

    new-array v0, v0, [Ldbxyzptlk/db231222/z/au;

    sget-object v1, Ldbxyzptlk/db231222/z/au;->a:Ldbxyzptlk/db231222/z/au;

    aput-object v1, v0, v3

    sget-object v1, Ldbxyzptlk/db231222/z/au;->b:Ldbxyzptlk/db231222/z/au;

    aput-object v1, v0, v4

    sget-object v1, Ldbxyzptlk/db231222/z/au;->c:Ldbxyzptlk/db231222/z/au;

    aput-object v1, v0, v5

    sget-object v1, Ldbxyzptlk/db231222/z/au;->d:Ldbxyzptlk/db231222/z/au;

    aput-object v1, v0, v6

    sget-object v1, Ldbxyzptlk/db231222/z/au;->e:Ldbxyzptlk/db231222/z/au;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Ldbxyzptlk/db231222/z/au;->f:Ldbxyzptlk/db231222/z/au;

    aput-object v2, v0, v1

    sput-object v0, Ldbxyzptlk/db231222/z/au;->g:[Ldbxyzptlk/db231222/z/au;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1746
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldbxyzptlk/db231222/z/au;
    .locals 1

    .prologue
    .line 1746
    const-class v0, Ldbxyzptlk/db231222/z/au;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/z/au;

    return-object v0
.end method

.method public static values()[Ldbxyzptlk/db231222/z/au;
    .locals 1

    .prologue
    .line 1746
    sget-object v0, Ldbxyzptlk/db231222/z/au;->g:[Ldbxyzptlk/db231222/z/au;

    invoke-virtual {v0}, [Ldbxyzptlk/db231222/z/au;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/z/au;

    return-object v0
.end method
