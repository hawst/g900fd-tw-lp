.class public final Ldbxyzptlk/db231222/Y/b;
.super Ldbxyzptlk/db231222/Y/a;
.source "panda.py"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final a:J

.field private final b:Z


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Ldbxyzptlk/db231222/Y/b;-><init>(JZ)V

    .line 67
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Ldbxyzptlk/db231222/Y/a;-><init>()V

    .line 78
    iput-boolean p3, p0, Ldbxyzptlk/db231222/Y/b;->b:Z

    .line 79
    iput-wide p1, p0, Ldbxyzptlk/db231222/Y/b;->a:J

    .line 80
    return-void
.end method


# virtual methods
.method public final accept(Ljava/io/File;)Z
    .locals 2

    .prologue
    .line 144
    iget-wide v0, p0, Ldbxyzptlk/db231222/Y/b;->a:J

    invoke-static {p1, v0, v1}, Ldbxyzptlk/db231222/X/b;->a(Ljava/io/File;J)Z

    move-result v0

    .line 145
    iget-boolean v1, p0, Ldbxyzptlk/db231222/Y/b;->b:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 155
    iget-boolean v0, p0, Ldbxyzptlk/db231222/Y/b;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "<="

    .line 156
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ldbxyzptlk/db231222/Y/a;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Ldbxyzptlk/db231222/Y/b;->a:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 155
    :cond_0
    const-string v0, ">"

    goto :goto_0
.end method
