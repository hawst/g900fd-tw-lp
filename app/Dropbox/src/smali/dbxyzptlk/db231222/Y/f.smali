.class public final Ldbxyzptlk/db231222/Y/f;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ldbxyzptlk/db231222/Y/g;

.field private static final b:Ldbxyzptlk/db231222/Y/g;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 729
    new-array v0, v4, [Ldbxyzptlk/db231222/Y/g;

    invoke-static {}, Ldbxyzptlk/db231222/Y/f;->a()Ldbxyzptlk/db231222/Y/g;

    move-result-object v1

    aput-object v1, v0, v2

    const-string v1, "CVS"

    invoke-static {v1}, Ldbxyzptlk/db231222/Y/f;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/Y/g;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v0}, Ldbxyzptlk/db231222/Y/f;->a([Ldbxyzptlk/db231222/Y/g;)Ldbxyzptlk/db231222/Y/g;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/Y/f;->a(Ldbxyzptlk/db231222/Y/g;)Ldbxyzptlk/db231222/Y/g;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/Y/f;->a:Ldbxyzptlk/db231222/Y/g;

    .line 733
    new-array v0, v4, [Ldbxyzptlk/db231222/Y/g;

    invoke-static {}, Ldbxyzptlk/db231222/Y/f;->a()Ldbxyzptlk/db231222/Y/g;

    move-result-object v1

    aput-object v1, v0, v2

    const-string v1, ".svn"

    invoke-static {v1}, Ldbxyzptlk/db231222/Y/f;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/Y/g;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v0}, Ldbxyzptlk/db231222/Y/f;->a([Ldbxyzptlk/db231222/Y/g;)Ldbxyzptlk/db231222/Y/g;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/Y/f;->a(Ldbxyzptlk/db231222/Y/g;)Ldbxyzptlk/db231222/Y/g;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/Y/f;->b:Ldbxyzptlk/db231222/Y/g;

    return-void
.end method

.method public static a()Ldbxyzptlk/db231222/Y/g;
    .locals 1

    .prologue
    .line 370
    sget-object v0, Ldbxyzptlk/db231222/Y/d;->a:Ldbxyzptlk/db231222/Y/g;

    return-object v0
.end method

.method public static a(Ldbxyzptlk/db231222/Y/g;)Ldbxyzptlk/db231222/Y/g;
    .locals 1

    .prologue
    .line 475
    new-instance v0, Ldbxyzptlk/db231222/Y/i;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/Y/i;-><init>(Ldbxyzptlk/db231222/Y/g;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ldbxyzptlk/db231222/Y/g;
    .locals 1

    .prologue
    .line 347
    new-instance v0, Ldbxyzptlk/db231222/Y/h;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/Y/h;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static varargs a([Ldbxyzptlk/db231222/Y/g;)Ldbxyzptlk/db231222/Y/g;
    .locals 2

    .prologue
    .line 426
    new-instance v0, Ldbxyzptlk/db231222/Y/c;

    invoke-static {p0}, Ldbxyzptlk/db231222/Y/f;->c([Ldbxyzptlk/db231222/Y/g;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/Y/c;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public static varargs b([Ldbxyzptlk/db231222/Y/g;)Ldbxyzptlk/db231222/Y/g;
    .locals 2

    .prologue
    .line 441
    new-instance v0, Ldbxyzptlk/db231222/Y/j;

    invoke-static {p0}, Ldbxyzptlk/db231222/Y/f;->c([Ldbxyzptlk/db231222/Y/g;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/Y/j;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public static varargs c([Ldbxyzptlk/db231222/Y/g;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ldbxyzptlk/db231222/Y/g;",
            ")",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/Y/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 454
    if-nez p0, :cond_0

    .line 455
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The filters must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 457
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    array-length v0, p0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 458
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 459
    aget-object v2, p0, v0

    if-nez v2, :cond_1

    .line 460
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The filter["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "] is null"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 462
    :cond_1
    aget-object v2, p0, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 458
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 464
    :cond_2
    return-object v1
.end method
