.class public final Ldbxyzptlk/db231222/d/e;
.super Landroid/text/style/ImageSpan;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/d/b;


# instance fields
.field private final a:Ldbxyzptlk/db231222/d/d;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;Lcom/android/ex/chips/RecipientEntry;)V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 37
    new-instance v0, Ldbxyzptlk/db231222/d/d;

    invoke-direct {v0, p2}, Ldbxyzptlk/db231222/d/d;-><init>(Lcom/android/ex/chips/RecipientEntry;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/d/e;->a:Ldbxyzptlk/db231222/d/d;

    .line 38
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Ldbxyzptlk/db231222/d/e;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 93
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Ldbxyzptlk/db231222/d/e;->a:Ldbxyzptlk/db231222/d/d;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/d/d;->a(Ljava/lang/String;)V

    .line 78
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Ldbxyzptlk/db231222/d/e;->a:Ldbxyzptlk/db231222/d/d;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/d/d;->a(Z)V

    .line 43
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Ldbxyzptlk/db231222/d/e;->a:Ldbxyzptlk/db231222/d/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/d/d;->a()Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Ldbxyzptlk/db231222/d/e;->a:Ldbxyzptlk/db231222/d/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/d/d;->b()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Ldbxyzptlk/db231222/d/e;->a:Ldbxyzptlk/db231222/d/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/d/d;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Ldbxyzptlk/db231222/d/e;->a:Ldbxyzptlk/db231222/d/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/d/d;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public final e()Lcom/android/ex/chips/RecipientEntry;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Ldbxyzptlk/db231222/d/e;->a:Ldbxyzptlk/db231222/d/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/d/d;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Ldbxyzptlk/db231222/d/e;->a:Ldbxyzptlk/db231222/d/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/d/d;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, Ldbxyzptlk/db231222/d/e;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Ldbxyzptlk/db231222/d/e;->a:Ldbxyzptlk/db231222/d/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/d/d;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
