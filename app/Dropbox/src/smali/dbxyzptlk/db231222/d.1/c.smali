.class public final Ldbxyzptlk/db231222/d/c;
.super Landroid/text/style/ReplacementSpan;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/d/b;


# instance fields
.field private final a:Ldbxyzptlk/db231222/d/d;


# direct methods
.method public constructor <init>(Lcom/android/ex/chips/RecipientEntry;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    .line 36
    new-instance v0, Ldbxyzptlk/db231222/d/d;

    invoke-direct {v0, p1}, Ldbxyzptlk/db231222/d/d;-><init>(Lcom/android/ex/chips/RecipientEntry;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/d/c;->a:Ldbxyzptlk/db231222/d/d;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 104
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Ldbxyzptlk/db231222/d/c;->a:Ldbxyzptlk/db231222/d/d;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/d/d;->a(Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Ldbxyzptlk/db231222/d/c;->a:Ldbxyzptlk/db231222/d/d;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/d/d;->a(Z)V

    .line 42
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Ldbxyzptlk/db231222/d/c;->a:Ldbxyzptlk/db231222/d/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/d/d;->a()Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Ldbxyzptlk/db231222/d/c;->a:Ldbxyzptlk/db231222/d/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/d/d;->b()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Ldbxyzptlk/db231222/d/c;->a:Ldbxyzptlk/db231222/d/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/d/d;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Ldbxyzptlk/db231222/d/c;->a:Ldbxyzptlk/db231222/d/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/d/d;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public final draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public final e()Lcom/android/ex/chips/RecipientEntry;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Ldbxyzptlk/db231222/d/c;->a:Ldbxyzptlk/db231222/d/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/d/d;->e()Lcom/android/ex/chips/RecipientEntry;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Ldbxyzptlk/db231222/d/c;->a:Ldbxyzptlk/db231222/d/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/d/d;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()Landroid/graphics/Rect;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 98
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method public final getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    return v0
.end method
