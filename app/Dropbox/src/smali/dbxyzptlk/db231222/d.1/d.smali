.class final Ldbxyzptlk/db231222/d/d;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/d/a;


# instance fields
.field private final a:Ljava/lang/CharSequence;

.field private final b:Ljava/lang/CharSequence;

.field private final c:J

.field private final d:J

.field private final e:Lcom/android/ex/chips/RecipientEntry;

.field private f:Z

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/ex/chips/RecipientEntry;)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/d/d;->f:Z

    .line 39
    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/d/d;->a:Ljava/lang/CharSequence;

    .line 40
    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/d/d;->b:Ljava/lang/CharSequence;

    .line 41
    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->g()J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/d/d;->c:J

    .line 42
    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEntry;->h()J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/d/d;->d:J

    .line 43
    iput-object p1, p0, Ldbxyzptlk/db231222/d/d;->e:Lcom/android/ex/chips/RecipientEntry;

    .line 44
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 83
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    iput-object p1, p0, Ldbxyzptlk/db231222/d/d;->g:Ljava/lang/String;

    .line 88
    :goto_0
    return-void

    .line 86
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/d/d;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 48
    iput-boolean p1, p0, Ldbxyzptlk/db231222/d/d;->f:Z

    .line 49
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Ldbxyzptlk/db231222/d/d;->f:Z

    return v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Ldbxyzptlk/db231222/d/d;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Ldbxyzptlk/db231222/d/d;->c:J

    return-wide v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 73
    iget-wide v0, p0, Ldbxyzptlk/db231222/d/d;->d:J

    return-wide v0
.end method

.method public final e()Lcom/android/ex/chips/RecipientEntry;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Ldbxyzptlk/db231222/d/d;->e:Lcom/android/ex/chips/RecipientEntry;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Ldbxyzptlk/db231222/d/d;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/d/d;->g:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/d/d;->e:Lcom/android/ex/chips/RecipientEntry;

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEntry;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Ldbxyzptlk/db231222/d/d;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " <"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/d/d;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
