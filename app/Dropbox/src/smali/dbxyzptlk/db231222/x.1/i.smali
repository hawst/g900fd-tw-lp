.class final Ldbxyzptlk/db231222/x/i;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/util/Map$Entry;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Map$Entry",
        "<",
        "Ljava/lang/String;",
        "Ldbxyzptlk/db231222/x/k;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ldbxyzptlk/db231222/x/k;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/util/Map$Entry;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    invoke-interface {p2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/db231222/x/i;->a:Ljava/lang/String;

    .line 89
    new-instance v0, Ldbxyzptlk/db231222/x/k;

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/db231222/x/i;->a:Ljava/lang/String;

    invoke-static {p1, v2}, Ldbxyzptlk/db231222/x/g;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/x/k;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/x/i;->b:Ldbxyzptlk/db231222/x/k;

    .line 90
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/util/Map$Entry;Ldbxyzptlk/db231222/x/h;)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/x/i;-><init>(Ljava/lang/String;Ljava/util/Map$Entry;)V

    return-void
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/x/k;)Ldbxyzptlk/db231222/x/k;
    .locals 1

    .prologue
    .line 97
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Ldbxyzptlk/db231222/x/i;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ldbxyzptlk/db231222/x/k;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Ldbxyzptlk/db231222/x/i;->b:Ldbxyzptlk/db231222/x/k;

    return-object v0
.end method

.method public final synthetic getKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0}, Ldbxyzptlk/db231222/x/i;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic getValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0}, Ldbxyzptlk/db231222/x/i;->b()Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 83
    check-cast p1, Ldbxyzptlk/db231222/x/k;

    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/x/i;->a(Ldbxyzptlk/db231222/x/k;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    return-object v0
.end method
