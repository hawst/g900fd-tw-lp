.class public final Ldbxyzptlk/db231222/x/d;
.super Ldbxyzptlk/db231222/x/a;
.source "panda.py"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/x/a",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;>;",
        "Ljava/lang/Iterable",
        "<",
        "Ldbxyzptlk/db231222/x/k;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/x/a;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method static synthetic a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    invoke-static {p0, p1}, Ldbxyzptlk/db231222/x/d;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Ldbxyzptlk/db231222/x/k;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Ldbxyzptlk/db231222/x/d;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;)Ldbxyzptlk/db231222/x/b;
    .locals 1

    .prologue
    .line 12
    invoke-super {p0, p1}, Ldbxyzptlk/db231222/x/a;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/x/b;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Ldbxyzptlk/db231222/x/k;
    .locals 3

    .prologue
    .line 27
    iget-object v0, p0, Ldbxyzptlk/db231222/x/d;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "expecting array to have an element at index "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", but it only has length "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Ldbxyzptlk/db231222/x/d;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/x/d;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/x/b;

    move-result-object v0

    throw v0

    .line 30
    :cond_0
    new-instance v1, Ldbxyzptlk/db231222/x/k;

    iget-object v0, p0, Ldbxyzptlk/db231222/x/d;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Ldbxyzptlk/db231222/x/d;->b:Ljava/lang/String;

    invoke-static {v2, p1}, Ldbxyzptlk/db231222/x/d;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ldbxyzptlk/db231222/x/k;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1
.end method

.method public final a(Ldbxyzptlk/db231222/x/c;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ldbxyzptlk/db231222/x/c",
            "<TT;>;)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 77
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Ldbxyzptlk/db231222/x/d;->a()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 78
    iget-object v0, p0, Ldbxyzptlk/db231222/x/d;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 79
    new-instance v3, Ldbxyzptlk/db231222/x/k;

    invoke-direct {v3, v2}, Ldbxyzptlk/db231222/x/k;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p1, v3}, Ldbxyzptlk/db231222/x/c;->b(Ldbxyzptlk/db231222/x/k;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 81
    :cond_0
    return-object v1
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ldbxyzptlk/db231222/x/k;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    new-instance v1, Ldbxyzptlk/db231222/x/f;

    iget-object v2, p0, Ldbxyzptlk/db231222/x/d;->b:Ljava/lang/String;

    iget-object v0, p0, Ldbxyzptlk/db231222/x/d;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Ldbxyzptlk/db231222/x/f;-><init>(Ljava/lang/String;Ljava/util/Iterator;Ldbxyzptlk/db231222/x/e;)V

    return-object v1
.end method
