.class final Ldbxyzptlk/db231222/ai/g;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field final a:Ldbxyzptlk/db231222/ai/e;

.field final b:Ljava/lang/String;

.field final c:I


# direct methods
.method constructor <init>(Ldbxyzptlk/db231222/ai/e;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 747
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 748
    iput-object p1, p0, Ldbxyzptlk/db231222/ai/g;->a:Ldbxyzptlk/db231222/ai/e;

    .line 749
    iput-object p2, p0, Ldbxyzptlk/db231222/ai/g;->b:Ljava/lang/String;

    .line 750
    iput p3, p0, Ldbxyzptlk/db231222/ai/g;->c:I

    .line 751
    return-void
.end method

.method static a(Ljava/io/DataInput;)Ldbxyzptlk/db231222/ai/g;
    .locals 5

    .prologue
    .line 740
    new-instance v0, Ldbxyzptlk/db231222/ai/g;

    invoke-static {p0}, Ldbxyzptlk/db231222/ai/e;->a(Ljava/io/DataInput;)Ldbxyzptlk/db231222/ai/e;

    move-result-object v1

    invoke-interface {p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Ldbxyzptlk/db231222/ai/c;->a(Ljava/io/DataInput;)J

    move-result-wide v3

    long-to-int v3, v3

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/ai/g;-><init>(Ldbxyzptlk/db231222/ai/e;Ljava/lang/String;I)V

    return-object v0
.end method


# virtual methods
.method public final a(JII)J
    .locals 2

    .prologue
    .line 761
    iget-object v0, p0, Ldbxyzptlk/db231222/ai/g;->a:Ldbxyzptlk/db231222/ai/e;

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ai/e;->a(JII)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 772
    iget-object v0, p0, Ldbxyzptlk/db231222/ai/g;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 776
    iget v0, p0, Ldbxyzptlk/db231222/ai/g;->c:I

    return v0
.end method

.method public final b(JII)J
    .locals 2

    .prologue
    .line 768
    iget-object v0, p0, Ldbxyzptlk/db231222/ai/g;->a:Ldbxyzptlk/db231222/ai/e;

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ai/e;->b(JII)J

    move-result-wide v0

    return-wide v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 780
    if-ne p0, p1, :cond_1

    .line 790
    :cond_0
    :goto_0
    return v0

    .line 783
    :cond_1
    instance-of v2, p1, Ldbxyzptlk/db231222/ai/g;

    if-eqz v2, :cond_3

    .line 784
    check-cast p1, Ldbxyzptlk/db231222/ai/g;

    .line 785
    iget v2, p0, Ldbxyzptlk/db231222/ai/g;->c:I

    iget v3, p1, Ldbxyzptlk/db231222/ai/g;->c:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Ldbxyzptlk/db231222/ai/g;->b:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/db231222/ai/g;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ldbxyzptlk/db231222/ai/g;->a:Ldbxyzptlk/db231222/ai/e;

    iget-object v3, p1, Ldbxyzptlk/db231222/ai/g;->a:Ldbxyzptlk/db231222/ai/e;

    invoke-virtual {v2, v3}, Ldbxyzptlk/db231222/ai/e;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 790
    goto :goto_0
.end method
