.class public final Ldbxyzptlk/db231222/ac/s;
.super Ldbxyzptlk/db231222/ad/i;
.source "panda.py"


# static fields
.field public static final a:Ldbxyzptlk/db231222/ac/s;

.field public static final b:Ldbxyzptlk/db231222/ac/s;

.field public static final c:Ldbxyzptlk/db231222/ac/s;

.field public static final d:Ldbxyzptlk/db231222/ac/s;

.field public static final e:Ldbxyzptlk/db231222/ac/s;

.field public static final f:Ldbxyzptlk/db231222/ac/s;

.field private static final g:Ldbxyzptlk/db231222/ah/B;

.field private static final serialVersionUID:J = 0x136f3c64899417fL


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    new-instance v0, Ldbxyzptlk/db231222/ac/s;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/ac/s;-><init>(I)V

    sput-object v0, Ldbxyzptlk/db231222/ac/s;->a:Ldbxyzptlk/db231222/ac/s;

    .line 47
    new-instance v0, Ldbxyzptlk/db231222/ac/s;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/ac/s;-><init>(I)V

    sput-object v0, Ldbxyzptlk/db231222/ac/s;->b:Ldbxyzptlk/db231222/ac/s;

    .line 49
    new-instance v0, Ldbxyzptlk/db231222/ac/s;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/ac/s;-><init>(I)V

    sput-object v0, Ldbxyzptlk/db231222/ac/s;->c:Ldbxyzptlk/db231222/ac/s;

    .line 51
    new-instance v0, Ldbxyzptlk/db231222/ac/s;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/ac/s;-><init>(I)V

    sput-object v0, Ldbxyzptlk/db231222/ac/s;->d:Ldbxyzptlk/db231222/ac/s;

    .line 53
    new-instance v0, Ldbxyzptlk/db231222/ac/s;

    const v1, 0x7fffffff

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/ac/s;-><init>(I)V

    sput-object v0, Ldbxyzptlk/db231222/ac/s;->e:Ldbxyzptlk/db231222/ac/s;

    .line 55
    new-instance v0, Ldbxyzptlk/db231222/ac/s;

    const/high16 v1, -0x80000000

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/ac/s;-><init>(I)V

    sput-object v0, Ldbxyzptlk/db231222/ac/s;->f:Ldbxyzptlk/db231222/ac/s;

    .line 58
    invoke-static {}, Ldbxyzptlk/db231222/ah/A;->a()Ldbxyzptlk/db231222/ah/B;

    move-result-object v0

    invoke-static {}, Ldbxyzptlk/db231222/ac/w;->c()Ldbxyzptlk/db231222/ac/w;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/ah/B;->a(Ldbxyzptlk/db231222/ac/w;)Ldbxyzptlk/db231222/ah/B;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/ac/s;->g:Ldbxyzptlk/db231222/ah/B;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 0

    .prologue
    .line 198
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/ad/i;-><init>(I)V

    .line 199
    return-void
.end method

.method public static a(I)Ldbxyzptlk/db231222/ac/s;
    .locals 1

    .prologue
    .line 72
    sparse-switch p0, :sswitch_data_0

    .line 86
    new-instance v0, Ldbxyzptlk/db231222/ac/s;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/ac/s;-><init>(I)V

    :goto_0
    return-object v0

    .line 74
    :sswitch_0
    sget-object v0, Ldbxyzptlk/db231222/ac/s;->a:Ldbxyzptlk/db231222/ac/s;

    goto :goto_0

    .line 76
    :sswitch_1
    sget-object v0, Ldbxyzptlk/db231222/ac/s;->b:Ldbxyzptlk/db231222/ac/s;

    goto :goto_0

    .line 78
    :sswitch_2
    sget-object v0, Ldbxyzptlk/db231222/ac/s;->c:Ldbxyzptlk/db231222/ac/s;

    goto :goto_0

    .line 80
    :sswitch_3
    sget-object v0, Ldbxyzptlk/db231222/ac/s;->d:Ldbxyzptlk/db231222/ac/s;

    goto :goto_0

    .line 82
    :sswitch_4
    sget-object v0, Ldbxyzptlk/db231222/ac/s;->e:Ldbxyzptlk/db231222/ac/s;

    goto :goto_0

    .line 84
    :sswitch_5
    sget-object v0, Ldbxyzptlk/db231222/ac/s;->f:Ldbxyzptlk/db231222/ac/s;

    goto :goto_0

    .line 72
    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_5
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x7fffffff -> :sswitch_4
    .end sparse-switch
.end method

.method public static a(Ldbxyzptlk/db231222/ac/B;Ldbxyzptlk/db231222/ac/B;)Ldbxyzptlk/db231222/ac/s;
    .locals 1

    .prologue
    .line 101
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->c()Ldbxyzptlk/db231222/ac/m;

    move-result-object v0

    invoke-static {p0, p1, v0}, Ldbxyzptlk/db231222/ad/i;->a(Ldbxyzptlk/db231222/ac/B;Ldbxyzptlk/db231222/ac/B;Ldbxyzptlk/db231222/ac/m;)I

    move-result v0

    .line 102
    invoke-static {v0}, Ldbxyzptlk/db231222/ac/s;->a(I)Ldbxyzptlk/db231222/ac/s;

    move-result-object v0

    return-object v0
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 207
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/s;->d()I

    move-result v0

    invoke-static {v0}, Ldbxyzptlk/db231222/ac/s;->a(I)Ldbxyzptlk/db231222/ac/s;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ldbxyzptlk/db231222/ac/m;
    .locals 1

    .prologue
    .line 217
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->c()Ldbxyzptlk/db231222/ac/m;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ldbxyzptlk/db231222/ac/w;
    .locals 1

    .prologue
    .line 226
    invoke-static {}, Ldbxyzptlk/db231222/ac/w;->c()Ldbxyzptlk/db231222/ac/w;

    move-result-object v0

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 323
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/s;->d()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2
    .annotation runtime Lorg/joda/convert/ToString;
    .end annotation

    .prologue
    .line 468
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/s;->d()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "M"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
