.class public final Ldbxyzptlk/db231222/ac/f;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ldbxyzptlk/db231222/ac/h;

.field private static volatile b:Ldbxyzptlk/db231222/ac/g;

.field private static volatile c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ldbxyzptlk/db231222/ac/i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 39
    new-instance v0, Ldbxyzptlk/db231222/ac/h;

    invoke-direct {v0}, Ldbxyzptlk/db231222/ac/h;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/ac/f;->a:Ldbxyzptlk/db231222/ac/h;

    .line 41
    sget-object v0, Ldbxyzptlk/db231222/ac/f;->a:Ldbxyzptlk/db231222/ac/h;

    sput-object v0, Ldbxyzptlk/db231222/ac/f;->b:Ldbxyzptlk/db231222/ac/g;

    .line 47
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 48
    const-string v1, "UT"

    sget-object v2, Ldbxyzptlk/db231222/ac/i;->a:Ldbxyzptlk/db231222/ac/i;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    const-string v1, "UTC"

    sget-object v2, Ldbxyzptlk/db231222/ac/i;->a:Ldbxyzptlk/db231222/ac/i;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    const-string v1, "GMT"

    sget-object v2, Ldbxyzptlk/db231222/ac/i;->a:Ldbxyzptlk/db231222/ac/i;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    const-string v1, "EST"

    const-string v2, "America/New_York"

    invoke-static {v0, v1, v2}, Ldbxyzptlk/db231222/ac/f;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const-string v1, "EDT"

    const-string v2, "America/New_York"

    invoke-static {v0, v1, v2}, Ldbxyzptlk/db231222/ac/f;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-string v1, "CST"

    const-string v2, "America/Chicago"

    invoke-static {v0, v1, v2}, Ldbxyzptlk/db231222/ac/f;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v1, "CDT"

    const-string v2, "America/Chicago"

    invoke-static {v0, v1, v2}, Ldbxyzptlk/db231222/ac/f;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string v1, "MST"

    const-string v2, "America/Denver"

    invoke-static {v0, v1, v2}, Ldbxyzptlk/db231222/ac/f;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v1, "MDT"

    const-string v2, "America/Denver"

    invoke-static {v0, v1, v2}, Ldbxyzptlk/db231222/ac/f;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-string v1, "PST"

    const-string v2, "America/Los_Angeles"

    invoke-static {v0, v1, v2}, Ldbxyzptlk/db231222/ac/f;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const-string v1, "PDT"

    const-string v2, "America/Los_Angeles"

    invoke-static {v0, v1, v2}, Ldbxyzptlk/db231222/ac/f;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/ac/f;->c:Ljava/util/Map;

    .line 60
    return-void
.end method

.method public static final a()J
    .locals 2

    .prologue
    .line 86
    sget-object v0, Ldbxyzptlk/db231222/ac/f;->b:Ldbxyzptlk/db231222/ac/g;

    invoke-interface {v0}, Ldbxyzptlk/db231222/ac/g;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static final a(Ldbxyzptlk/db231222/ac/B;)J
    .locals 2

    .prologue
    .line 176
    if-nez p0, :cond_0

    .line 177
    invoke-static {}, Ldbxyzptlk/db231222/ac/f;->a()J

    move-result-wide v0

    .line 179
    :goto_0
    return-wide v0

    :cond_0
    invoke-interface {p0}, Ldbxyzptlk/db231222/ac/B;->c()J

    move-result-wide v0

    goto :goto_0
.end method

.method public static final a(Ldbxyzptlk/db231222/ac/B;Ldbxyzptlk/db231222/ac/B;)Ldbxyzptlk/db231222/ac/a;
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x0

    .line 218
    if-eqz p0, :cond_2

    .line 219
    invoke-interface {p0}, Ldbxyzptlk/db231222/ac/B;->d()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    .line 223
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 224
    invoke-static {}, Ldbxyzptlk/db231222/ae/z;->O()Ldbxyzptlk/db231222/ae/z;

    move-result-object v0

    .line 226
    :cond_1
    return-object v0

    .line 220
    :cond_2
    if-eqz p1, :cond_0

    .line 221
    invoke-interface {p1}, Ldbxyzptlk/db231222/ac/B;->d()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    goto :goto_0
.end method

.method public static final a(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/a;
    .locals 0

    .prologue
    .line 282
    if-nez p0, :cond_0

    .line 283
    invoke-static {}, Ldbxyzptlk/db231222/ae/z;->O()Ldbxyzptlk/db231222/ae/z;

    move-result-object p0

    .line 285
    :cond_0
    return-object p0
.end method

.method public static final a(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ac/i;
    .locals 0

    .prologue
    .line 299
    if-nez p0, :cond_0

    .line 300
    invoke-static {}, Ldbxyzptlk/db231222/ac/i;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object p0

    .line 302
    :cond_0
    return-object p0
.end method

.method public static final a(Ldbxyzptlk/db231222/ac/w;)Ldbxyzptlk/db231222/ac/w;
    .locals 0

    .prologue
    .line 316
    if-nez p0, :cond_0

    .line 317
    invoke-static {}, Ldbxyzptlk/db231222/ac/w;->a()Ldbxyzptlk/db231222/ac/w;

    move-result-object p0

    .line 319
    :cond_0
    return-object p0
.end method

.method public static final a(Ljava/util/Locale;)Ljava/text/DateFormatSymbols;
    .locals 5

    .prologue
    .line 395
    :try_start_0
    const-class v0, Ljava/text/DateFormatSymbols;

    const-string v1, "getInstance"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/util/Locale;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 396
    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/DateFormatSymbols;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 398
    :goto_0
    return-object v0

    .line 397
    :catch_0
    move-exception v0

    .line 398
    new-instance v0, Ljava/text/DateFormatSymbols;

    invoke-direct {v0, p0}, Ljava/text/DateFormatSymbols;-><init>(Ljava/util/Locale;)V

    goto :goto_0
.end method

.method private static a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ldbxyzptlk/db231222/ac/i;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 63
    :try_start_0
    invoke-static {p2}, Ldbxyzptlk/db231222/ac/i;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/ac/i;

    move-result-object v0

    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :goto_0
    return-void

    .line 64
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static final a(Ldbxyzptlk/db231222/ac/D;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 364
    if-nez p0, :cond_0

    .line 365
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Partial must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 367
    :cond_0
    const/4 v0, 0x0

    move-object v2, v0

    move v0, v1

    .line 368
    :goto_0
    invoke-interface {p0}, Ldbxyzptlk/db231222/ac/D;->a()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 369
    invoke-interface {p0, v0}, Ldbxyzptlk/db231222/ac/D;->c(I)Ldbxyzptlk/db231222/ac/c;

    move-result-object v3

    .line 370
    if-lez v0, :cond_1

    .line 371
    invoke-virtual {v3}, Ldbxyzptlk/db231222/ac/c;->e()Ldbxyzptlk/db231222/ac/l;

    move-result-object v4

    invoke-virtual {v4}, Ldbxyzptlk/db231222/ac/l;->a()Ldbxyzptlk/db231222/ac/m;

    move-result-object v4

    if-eq v4, v2, :cond_1

    .line 377
    :goto_1
    return v1

    .line 375
    :cond_1
    invoke-virtual {v3}, Ldbxyzptlk/db231222/ac/c;->d()Ldbxyzptlk/db231222/ac/l;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/ac/l;->a()Ldbxyzptlk/db231222/ac/m;

    move-result-object v2

    .line 368
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 377
    :cond_2
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public static final b(Ldbxyzptlk/db231222/ac/B;)Ldbxyzptlk/db231222/ac/a;
    .locals 1

    .prologue
    .line 194
    if-nez p0, :cond_1

    .line 195
    invoke-static {}, Ldbxyzptlk/db231222/ae/z;->O()Ldbxyzptlk/db231222/ae/z;

    move-result-object v0

    .line 201
    :cond_0
    :goto_0
    return-object v0

    .line 197
    :cond_1
    invoke-interface {p0}, Ldbxyzptlk/db231222/ac/B;->d()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    .line 198
    if-nez v0, :cond_0

    .line 199
    invoke-static {}, Ldbxyzptlk/db231222/ae/z;->O()Ldbxyzptlk/db231222/ae/z;

    move-result-object v0

    goto :goto_0
.end method

.method public static final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ldbxyzptlk/db231222/ac/i;",
            ">;"
        }
    .end annotation

    .prologue
    .line 427
    sget-object v0, Ldbxyzptlk/db231222/ac/f;->c:Ljava/util/Map;

    return-object v0
.end method
