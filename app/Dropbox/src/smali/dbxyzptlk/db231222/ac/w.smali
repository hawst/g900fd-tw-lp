.class public final Ldbxyzptlk/db231222/ac/w;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static a:I = 0x0

.field static b:I = 0x0

.field static c:I = 0x0

.field static d:I = 0x0

.field static e:I = 0x0

.field static f:I = 0x0

.field static g:I = 0x0

.field static h:I = 0x0

.field private static final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ldbxyzptlk/db231222/ac/w;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static j:Ldbxyzptlk/db231222/ac/w; = null

.field private static k:Ldbxyzptlk/db231222/ac/w; = null

.field private static l:Ldbxyzptlk/db231222/ac/w; = null

.field private static final serialVersionUID:J = 0x1f900670aab2350eL


# instance fields
.field private final m:Ljava/lang/String;

.field private final n:[Ldbxyzptlk/db231222/ac/m;

.field private final o:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Ldbxyzptlk/db231222/ac/w;->i:Ljava/util/Map;

    .line 58
    const/4 v0, 0x0

    sput v0, Ldbxyzptlk/db231222/ac/w;->a:I

    .line 59
    const/4 v0, 0x1

    sput v0, Ldbxyzptlk/db231222/ac/w;->b:I

    .line 60
    const/4 v0, 0x2

    sput v0, Ldbxyzptlk/db231222/ac/w;->c:I

    .line 61
    const/4 v0, 0x3

    sput v0, Ldbxyzptlk/db231222/ac/w;->d:I

    .line 62
    const/4 v0, 0x4

    sput v0, Ldbxyzptlk/db231222/ac/w;->e:I

    .line 63
    const/4 v0, 0x5

    sput v0, Ldbxyzptlk/db231222/ac/w;->f:I

    .line 64
    const/4 v0, 0x6

    sput v0, Ldbxyzptlk/db231222/ac/w;->g:I

    .line 65
    const/4 v0, 0x7

    sput v0, Ldbxyzptlk/db231222/ac/w;->h:I

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;[Ldbxyzptlk/db231222/ac/m;[I)V
    .locals 0

    .prologue
    .line 595
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 596
    iput-object p1, p0, Ldbxyzptlk/db231222/ac/w;->m:Ljava/lang/String;

    .line 597
    iput-object p2, p0, Ldbxyzptlk/db231222/ac/w;->n:[Ldbxyzptlk/db231222/ac/m;

    .line 598
    iput-object p3, p0, Ldbxyzptlk/db231222/ac/w;->o:[I

    .line 599
    return-void
.end method

.method public static a()Ldbxyzptlk/db231222/ac/w;
    .locals 6

    .prologue
    const/16 v5, 0x8

    .line 102
    sget-object v0, Ldbxyzptlk/db231222/ac/w;->j:Ldbxyzptlk/db231222/ac/w;

    .line 103
    if-nez v0, :cond_0

    .line 104
    new-instance v0, Ldbxyzptlk/db231222/ac/w;

    const-string v1, "Standard"

    new-array v2, v5, [Ldbxyzptlk/db231222/ac/m;

    const/4 v3, 0x0

    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->j()Ldbxyzptlk/db231222/ac/m;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->i()Ldbxyzptlk/db231222/ac/m;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->g()Ldbxyzptlk/db231222/ac/m;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->f()Ldbxyzptlk/db231222/ac/m;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->d()Ldbxyzptlk/db231222/ac/m;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->c()Ldbxyzptlk/db231222/ac/m;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->b()Ldbxyzptlk/db231222/ac/m;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->a()Ldbxyzptlk/db231222/ac/m;

    move-result-object v4

    aput-object v4, v2, v3

    new-array v3, v5, [I

    fill-array-data v3, :array_0

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/ac/w;-><init>(Ljava/lang/String;[Ldbxyzptlk/db231222/ac/m;[I)V

    .line 114
    sput-object v0, Ldbxyzptlk/db231222/ac/w;->j:Ldbxyzptlk/db231222/ac/w;

    .line 116
    :cond_0
    return-object v0

    .line 104
    nop

    :array_0
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
    .end array-data
.end method

.method public static b()Ldbxyzptlk/db231222/ac/w;
    .locals 5

    .prologue
    .line 333
    sget-object v0, Ldbxyzptlk/db231222/ac/w;->k:Ldbxyzptlk/db231222/ac/w;

    .line 334
    if-nez v0, :cond_0

    .line 335
    new-instance v0, Ldbxyzptlk/db231222/ac/w;

    const-string v1, "Time"

    const/4 v2, 0x4

    new-array v2, v2, [Ldbxyzptlk/db231222/ac/m;

    const/4 v3, 0x0

    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->d()Ldbxyzptlk/db231222/ac/m;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->c()Ldbxyzptlk/db231222/ac/m;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->b()Ldbxyzptlk/db231222/ac/m;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->a()Ldbxyzptlk/db231222/ac/m;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/ac/w;-><init>(Ljava/lang/String;[Ldbxyzptlk/db231222/ac/m;[I)V

    .line 343
    sput-object v0, Ldbxyzptlk/db231222/ac/w;->k:Ldbxyzptlk/db231222/ac/w;

    .line 345
    :cond_0
    return-object v0

    .line 335
    :array_0
    .array-data 4
        -0x1
        -0x1
        -0x1
        -0x1
        0x0
        0x1
        0x2
        0x3
    .end array-data
.end method

.method public static c()Ldbxyzptlk/db231222/ac/w;
    .locals 5

    .prologue
    .line 444
    sget-object v0, Ldbxyzptlk/db231222/ac/w;->l:Ldbxyzptlk/db231222/ac/w;

    .line 445
    if-nez v0, :cond_0

    .line 446
    new-instance v0, Ldbxyzptlk/db231222/ac/w;

    const-string v1, "Minutes"

    const/4 v2, 0x1

    new-array v2, v2, [Ldbxyzptlk/db231222/ac/m;

    const/4 v3, 0x0

    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->c()Ldbxyzptlk/db231222/ac/m;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/ac/w;-><init>(Ljava/lang/String;[Ldbxyzptlk/db231222/ac/m;[I)V

    .line 451
    sput-object v0, Ldbxyzptlk/db231222/ac/w;->l:Ldbxyzptlk/db231222/ac/w;

    .line 453
    :cond_0
    return-object v0

    .line 446
    nop

    :array_0
    .array-data 4
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        0x0
        -0x1
        -0x1
    .end array-data
.end method


# virtual methods
.method final a(Ldbxyzptlk/db231222/ac/E;I)I
    .locals 2

    .prologue
    .line 674
    iget-object v0, p0, Ldbxyzptlk/db231222/ac/w;->o:[I

    aget v0, v0, p2

    .line 675
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1, v0}, Ldbxyzptlk/db231222/ac/E;->c(I)I

    move-result v0

    goto :goto_0
.end method

.method public final a(I)Ldbxyzptlk/db231222/ac/m;
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Ldbxyzptlk/db231222/ac/w;->n:[Ldbxyzptlk/db231222/ac/m;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/ac/m;)Z
    .locals 1

    .prologue
    .line 638
    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/ac/w;->b(Ldbxyzptlk/db231222/ac/m;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ldbxyzptlk/db231222/ac/m;)I
    .locals 3

    .prologue
    .line 648
    const/4 v0, 0x0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/w;->e()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_1

    .line 649
    iget-object v2, p0, Ldbxyzptlk/db231222/ac/w;->n:[Ldbxyzptlk/db231222/ac/m;

    aget-object v2, v2, v0

    if-ne v2, p1, :cond_0

    .line 653
    :goto_1
    return v0

    .line 648
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 653
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 608
    iget-object v0, p0, Ldbxyzptlk/db231222/ac/w;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 617
    iget-object v0, p0, Ldbxyzptlk/db231222/ac/w;->n:[Ldbxyzptlk/db231222/ac/m;

    array-length v0, v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 835
    if-ne p0, p1, :cond_0

    .line 836
    const/4 v0, 0x1

    .line 842
    :goto_0
    return v0

    .line 838
    :cond_0
    instance-of v0, p1, Ldbxyzptlk/db231222/ac/w;

    if-nez v0, :cond_1

    .line 839
    const/4 v0, 0x0

    goto :goto_0

    .line 841
    :cond_1
    check-cast p1, Ldbxyzptlk/db231222/ac/w;

    .line 842
    iget-object v0, p0, Ldbxyzptlk/db231222/ac/w;->n:[Ldbxyzptlk/db231222/ac/m;

    iget-object v1, p1, Ldbxyzptlk/db231222/ac/w;->n:[Ldbxyzptlk/db231222/ac/m;

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 851
    move v1, v0

    .line 852
    :goto_0
    iget-object v2, p0, Ldbxyzptlk/db231222/ac/w;->n:[Ldbxyzptlk/db231222/ac/m;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 853
    iget-object v2, p0, Ldbxyzptlk/db231222/ac/w;->n:[Ldbxyzptlk/db231222/ac/m;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    .line 852
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 855
    :cond_0
    return v1
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 662
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PeriodType["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/w;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
