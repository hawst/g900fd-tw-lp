.class public abstract Ldbxyzptlk/db231222/ac/m;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final a:Ldbxyzptlk/db231222/ac/m;

.field static final b:Ldbxyzptlk/db231222/ac/m;

.field static final c:Ldbxyzptlk/db231222/ac/m;

.field static final d:Ldbxyzptlk/db231222/ac/m;

.field static final e:Ldbxyzptlk/db231222/ac/m;

.field static final f:Ldbxyzptlk/db231222/ac/m;

.field static final g:Ldbxyzptlk/db231222/ac/m;

.field static final h:Ldbxyzptlk/db231222/ac/m;

.field static final i:Ldbxyzptlk/db231222/ac/m;

.field static final j:Ldbxyzptlk/db231222/ac/m;

.field static final k:Ldbxyzptlk/db231222/ac/m;

.field static final l:Ldbxyzptlk/db231222/ac/m;

.field private static final serialVersionUID:J = 0x7f8cac4ed77L


# instance fields
.field private final m:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 60
    new-instance v0, Ldbxyzptlk/db231222/ac/n;

    const-string v1, "eras"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/ac/n;-><init>(Ljava/lang/String;B)V

    sput-object v0, Ldbxyzptlk/db231222/ac/m;->a:Ldbxyzptlk/db231222/ac/m;

    .line 62
    new-instance v0, Ldbxyzptlk/db231222/ac/n;

    const-string v1, "centuries"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/ac/n;-><init>(Ljava/lang/String;B)V

    sput-object v0, Ldbxyzptlk/db231222/ac/m;->b:Ldbxyzptlk/db231222/ac/m;

    .line 64
    new-instance v0, Ldbxyzptlk/db231222/ac/n;

    const-string v1, "weekyears"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/ac/n;-><init>(Ljava/lang/String;B)V

    sput-object v0, Ldbxyzptlk/db231222/ac/m;->c:Ldbxyzptlk/db231222/ac/m;

    .line 66
    new-instance v0, Ldbxyzptlk/db231222/ac/n;

    const-string v1, "years"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/ac/n;-><init>(Ljava/lang/String;B)V

    sput-object v0, Ldbxyzptlk/db231222/ac/m;->d:Ldbxyzptlk/db231222/ac/m;

    .line 68
    new-instance v0, Ldbxyzptlk/db231222/ac/n;

    const-string v1, "months"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/ac/n;-><init>(Ljava/lang/String;B)V

    sput-object v0, Ldbxyzptlk/db231222/ac/m;->e:Ldbxyzptlk/db231222/ac/m;

    .line 70
    new-instance v0, Ldbxyzptlk/db231222/ac/n;

    const-string v1, "weeks"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/ac/n;-><init>(Ljava/lang/String;B)V

    sput-object v0, Ldbxyzptlk/db231222/ac/m;->f:Ldbxyzptlk/db231222/ac/m;

    .line 72
    new-instance v0, Ldbxyzptlk/db231222/ac/n;

    const-string v1, "days"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/ac/n;-><init>(Ljava/lang/String;B)V

    sput-object v0, Ldbxyzptlk/db231222/ac/m;->g:Ldbxyzptlk/db231222/ac/m;

    .line 74
    new-instance v0, Ldbxyzptlk/db231222/ac/n;

    const-string v1, "halfdays"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/ac/n;-><init>(Ljava/lang/String;B)V

    sput-object v0, Ldbxyzptlk/db231222/ac/m;->h:Ldbxyzptlk/db231222/ac/m;

    .line 76
    new-instance v0, Ldbxyzptlk/db231222/ac/n;

    const-string v1, "hours"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/ac/n;-><init>(Ljava/lang/String;B)V

    sput-object v0, Ldbxyzptlk/db231222/ac/m;->i:Ldbxyzptlk/db231222/ac/m;

    .line 78
    new-instance v0, Ldbxyzptlk/db231222/ac/n;

    const-string v1, "minutes"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/ac/n;-><init>(Ljava/lang/String;B)V

    sput-object v0, Ldbxyzptlk/db231222/ac/m;->j:Ldbxyzptlk/db231222/ac/m;

    .line 80
    new-instance v0, Ldbxyzptlk/db231222/ac/n;

    const-string v1, "seconds"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/ac/n;-><init>(Ljava/lang/String;B)V

    sput-object v0, Ldbxyzptlk/db231222/ac/m;->k:Ldbxyzptlk/db231222/ac/m;

    .line 82
    new-instance v0, Ldbxyzptlk/db231222/ac/n;

    const-string v1, "millis"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/ac/n;-><init>(Ljava/lang/String;B)V

    sput-object v0, Ldbxyzptlk/db231222/ac/m;->l:Ldbxyzptlk/db231222/ac/m;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object p1, p0, Ldbxyzptlk/db231222/ac/m;->m:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public static a()Ldbxyzptlk/db231222/ac/m;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Ldbxyzptlk/db231222/ac/m;->l:Ldbxyzptlk/db231222/ac/m;

    return-object v0
.end method

.method public static b()Ldbxyzptlk/db231222/ac/m;
    .locals 1

    .prologue
    .line 114
    sget-object v0, Ldbxyzptlk/db231222/ac/m;->k:Ldbxyzptlk/db231222/ac/m;

    return-object v0
.end method

.method public static c()Ldbxyzptlk/db231222/ac/m;
    .locals 1

    .prologue
    .line 123
    sget-object v0, Ldbxyzptlk/db231222/ac/m;->j:Ldbxyzptlk/db231222/ac/m;

    return-object v0
.end method

.method public static d()Ldbxyzptlk/db231222/ac/m;
    .locals 1

    .prologue
    .line 132
    sget-object v0, Ldbxyzptlk/db231222/ac/m;->i:Ldbxyzptlk/db231222/ac/m;

    return-object v0
.end method

.method public static e()Ldbxyzptlk/db231222/ac/m;
    .locals 1

    .prologue
    .line 141
    sget-object v0, Ldbxyzptlk/db231222/ac/m;->h:Ldbxyzptlk/db231222/ac/m;

    return-object v0
.end method

.method public static f()Ldbxyzptlk/db231222/ac/m;
    .locals 1

    .prologue
    .line 151
    sget-object v0, Ldbxyzptlk/db231222/ac/m;->g:Ldbxyzptlk/db231222/ac/m;

    return-object v0
.end method

.method public static g()Ldbxyzptlk/db231222/ac/m;
    .locals 1

    .prologue
    .line 160
    sget-object v0, Ldbxyzptlk/db231222/ac/m;->f:Ldbxyzptlk/db231222/ac/m;

    return-object v0
.end method

.method public static h()Ldbxyzptlk/db231222/ac/m;
    .locals 1

    .prologue
    .line 169
    sget-object v0, Ldbxyzptlk/db231222/ac/m;->c:Ldbxyzptlk/db231222/ac/m;

    return-object v0
.end method

.method public static i()Ldbxyzptlk/db231222/ac/m;
    .locals 1

    .prologue
    .line 178
    sget-object v0, Ldbxyzptlk/db231222/ac/m;->e:Ldbxyzptlk/db231222/ac/m;

    return-object v0
.end method

.method public static j()Ldbxyzptlk/db231222/ac/m;
    .locals 1

    .prologue
    .line 187
    sget-object v0, Ldbxyzptlk/db231222/ac/m;->d:Ldbxyzptlk/db231222/ac/m;

    return-object v0
.end method

.method public static k()Ldbxyzptlk/db231222/ac/m;
    .locals 1

    .prologue
    .line 196
    sget-object v0, Ldbxyzptlk/db231222/ac/m;->b:Ldbxyzptlk/db231222/ac/m;

    return-object v0
.end method

.method public static l()Ldbxyzptlk/db231222/ac/m;
    .locals 1

    .prologue
    .line 205
    sget-object v0, Ldbxyzptlk/db231222/ac/m;->a:Ldbxyzptlk/db231222/ac/m;

    return-object v0
.end method


# virtual methods
.method public abstract a(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/l;
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Ldbxyzptlk/db231222/ac/m;->m:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 243
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/m;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
