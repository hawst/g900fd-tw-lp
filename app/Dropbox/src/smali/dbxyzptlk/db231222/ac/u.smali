.class public final Ldbxyzptlk/db231222/ac/u;
.super Ldbxyzptlk/db231222/ag/a;
.source "panda.py"


# static fields
.field private static final serialVersionUID:J = -0x3e3028d97dec5739L


# instance fields
.field private a:Ldbxyzptlk/db231222/ac/t;

.field private b:Ldbxyzptlk/db231222/ac/c;


# direct methods
.method constructor <init>(Ldbxyzptlk/db231222/ac/t;Ldbxyzptlk/db231222/ac/c;)V
    .locals 0

    .prologue
    .line 1270
    invoke-direct {p0}, Ldbxyzptlk/db231222/ag/a;-><init>()V

    .line 1271
    iput-object p1, p0, Ldbxyzptlk/db231222/ac/u;->a:Ldbxyzptlk/db231222/ac/t;

    .line 1272
    iput-object p2, p0, Ldbxyzptlk/db231222/ac/u;->b:Ldbxyzptlk/db231222/ac/c;

    .line 1273
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 1287
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/ac/t;

    iput-object v0, p0, Ldbxyzptlk/db231222/ac/u;->a:Ldbxyzptlk/db231222/ac/t;

    .line 1288
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/ac/d;

    .line 1289
    iget-object v1, p0, Ldbxyzptlk/db231222/ac/u;->a:Ldbxyzptlk/db231222/ac/t;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/ac/t;->d()Ldbxyzptlk/db231222/ac/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/ac/d;->a(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/ac/u;->b:Ldbxyzptlk/db231222/ac/c;

    .line 1290
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    .prologue
    .line 1279
    iget-object v0, p0, Ldbxyzptlk/db231222/ac/u;->a:Ldbxyzptlk/db231222/ac/t;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 1280
    iget-object v0, p0, Ldbxyzptlk/db231222/ac/u;->b:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/c;->a()Ldbxyzptlk/db231222/ac/d;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 1281
    return-void
.end method


# virtual methods
.method public final a()Ldbxyzptlk/db231222/ac/c;
    .locals 1

    .prologue
    .line 1299
    iget-object v0, p0, Ldbxyzptlk/db231222/ac/u;->b:Ldbxyzptlk/db231222/ac/c;

    return-object v0
.end method

.method public final a(I)Ldbxyzptlk/db231222/ac/t;
    .locals 4

    .prologue
    .line 1376
    iget-object v0, p0, Ldbxyzptlk/db231222/ac/u;->a:Ldbxyzptlk/db231222/ac/t;

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/u;->a()Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/db231222/ac/u;->a:Ldbxyzptlk/db231222/ac/t;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/ac/t;->c()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, p1}, Ldbxyzptlk/db231222/ac/c;->b(JI)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/ac/t;->a(J)V

    .line 1377
    iget-object v0, p0, Ldbxyzptlk/db231222/ac/u;->a:Ldbxyzptlk/db231222/ac/t;

    return-object v0
.end method

.method protected final b()J
    .locals 2

    .prologue
    .line 1308
    iget-object v0, p0, Ldbxyzptlk/db231222/ac/u;->a:Ldbxyzptlk/db231222/ac/t;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/t;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method protected final c()Ldbxyzptlk/db231222/ac/a;
    .locals 1

    .prologue
    .line 1318
    iget-object v0, p0, Ldbxyzptlk/db231222/ac/u;->a:Ldbxyzptlk/db231222/ac/t;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/t;->d()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    return-object v0
.end method
