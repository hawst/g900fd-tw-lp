.class public final Ldbxyzptlk/db231222/ac/t;
.super Ldbxyzptlk/db231222/ad/e;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/ac/x;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field private static final serialVersionUID:J = 0x2796807cf37e0267L


# instance fields
.field private a:Ldbxyzptlk/db231222/ac/c;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0}, Ldbxyzptlk/db231222/ad/e;-><init>()V

    .line 173
    return-void
.end method

.method public constructor <init>(JLdbxyzptlk/db231222/ac/a;)V
    .locals 0

    .prologue
    .line 237
    invoke-direct {p0, p1, p2, p3}, Ldbxyzptlk/db231222/ad/e;-><init>(JLdbxyzptlk/db231222/ac/a;)V

    .line 238
    return-void
.end method

.method public constructor <init>(JLdbxyzptlk/db231222/ac/i;)V
    .locals 0

    .prologue
    .line 223
    invoke-direct {p0, p1, p2, p3}, Ldbxyzptlk/db231222/ad/e;-><init>(JLdbxyzptlk/db231222/ac/i;)V

    .line 224
    return-void
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/ac/d;)Ldbxyzptlk/db231222/ac/u;
    .locals 3

    .prologue
    .line 1022
    if-nez p1, :cond_0

    .line 1023
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The DateTimeFieldType must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1025
    :cond_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/t;->d()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/ac/d;->a(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    .line 1026
    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/c;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1027
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Field \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' is not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1029
    :cond_1
    new-instance v1, Ldbxyzptlk/db231222/ac/u;

    invoke-direct {v1, p0, v0}, Ldbxyzptlk/db231222/ac/u;-><init>(Ldbxyzptlk/db231222/ac/t;Ldbxyzptlk/db231222/ac/c;)V

    return-object v1
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 454
    iget v0, p0, Ldbxyzptlk/db231222/ac/t;->b:I

    packed-switch v0, :pswitch_data_0

    .line 474
    :goto_0
    :pswitch_0
    invoke-super {p0, p1, p2}, Ldbxyzptlk/db231222/ad/e;->a(J)V

    .line 475
    return-void

    .line 458
    :pswitch_1
    iget-object v0, p0, Ldbxyzptlk/db231222/ac/t;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->d(J)J

    move-result-wide p1

    goto :goto_0

    .line 461
    :pswitch_2
    iget-object v0, p0, Ldbxyzptlk/db231222/ac/t;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->e(J)J

    move-result-wide p1

    goto :goto_0

    .line 464
    :pswitch_3
    iget-object v0, p0, Ldbxyzptlk/db231222/ac/t;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->f(J)J

    move-result-wide p1

    goto :goto_0

    .line 467
    :pswitch_4
    iget-object v0, p0, Ldbxyzptlk/db231222/ac/t;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->g(J)J

    move-result-wide p1

    goto :goto_0

    .line 470
    :pswitch_5
    iget-object v0, p0, Ldbxyzptlk/db231222/ac/t;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->h(J)J

    move-result-wide p1

    goto :goto_0

    .line 454
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Ldbxyzptlk/db231222/ac/a;)V
    .locals 0

    .prologue
    .line 564
    invoke-super {p0, p1}, Ldbxyzptlk/db231222/ad/e;->a(Ldbxyzptlk/db231222/ac/a;)V

    .line 565
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/ac/i;)V
    .locals 4

    .prologue
    .line 602
    invoke-static {p1}, Ldbxyzptlk/db231222/ac/f;->a(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ac/i;

    move-result-object v0

    .line 603
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/t;->h()Ldbxyzptlk/db231222/ac/i;

    move-result-object v1

    invoke-static {v1}, Ldbxyzptlk/db231222/ac/f;->a(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ac/i;

    move-result-object v1

    .line 604
    if-ne v0, v1, :cond_0

    .line 611
    :goto_0
    return-void

    .line 608
    :cond_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/t;->c()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Ldbxyzptlk/db231222/ac/i;->a(Ldbxyzptlk/db231222/ac/i;J)J

    move-result-wide v1

    .line 609
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/t;->d()Ldbxyzptlk/db231222/ac/a;

    move-result-object v3

    invoke-virtual {v3, v0}, Ldbxyzptlk/db231222/ac/a;->a(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/ac/t;->a(Ldbxyzptlk/db231222/ac/a;)V

    .line 610
    invoke-virtual {p0, v1, v2}, Ldbxyzptlk/db231222/ac/t;->a(J)V

    goto :goto_0
.end method

.method public final clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1217
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 1218
    :catch_0
    move-exception v0

    .line 1219
    new-instance v0, Ljava/lang/InternalError;

    const-string v1, "Clone error"

    invoke-direct {v0, v1}, Ljava/lang/InternalError;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1
    .annotation runtime Lorg/joda/convert/ToString;
    .end annotation

    .prologue
    .line 1230
    invoke-static {}, Ldbxyzptlk/db231222/ah/y;->c()Ldbxyzptlk/db231222/ah/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Ldbxyzptlk/db231222/ah/c;->a(Ldbxyzptlk/db231222/ac/B;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
