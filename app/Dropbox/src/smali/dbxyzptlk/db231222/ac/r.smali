.class public final Ldbxyzptlk/db231222/ac/r;
.super Ldbxyzptlk/db231222/ad/f;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/ac/D;
.implements Ljava/io/Serializable;


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ldbxyzptlk/db231222/ac/m;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = -0x7fb2c1b144bL


# instance fields
.field private final b:J

.field private final c:Ldbxyzptlk/db231222/ac/a;

.field private volatile transient d:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 96
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/ac/r;->a:Ljava/util/Set;

    .line 98
    sget-object v0, Ldbxyzptlk/db231222/ac/r;->a:Ljava/util/Set;

    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->f()Ldbxyzptlk/db231222/ac/m;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 99
    sget-object v0, Ldbxyzptlk/db231222/ac/r;->a:Ljava/util/Set;

    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->g()Ldbxyzptlk/db231222/ac/m;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 100
    sget-object v0, Ldbxyzptlk/db231222/ac/r;->a:Ljava/util/Set;

    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->i()Ldbxyzptlk/db231222/ac/m;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 101
    sget-object v0, Ldbxyzptlk/db231222/ac/r;->a:Ljava/util/Set;

    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->h()Ldbxyzptlk/db231222/ac/m;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 102
    sget-object v0, Ldbxyzptlk/db231222/ac/r;->a:Ljava/util/Set;

    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->j()Ldbxyzptlk/db231222/ac/m;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 103
    sget-object v0, Ldbxyzptlk/db231222/ac/r;->a:Ljava/util/Set;

    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->k()Ldbxyzptlk/db231222/ac/m;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 105
    sget-object v0, Ldbxyzptlk/db231222/ac/r;->a:Ljava/util/Set;

    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->l()Ldbxyzptlk/db231222/ac/m;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 106
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 267
    invoke-static {}, Ldbxyzptlk/db231222/ac/f;->a()J

    move-result-wide v0

    invoke-static {}, Ldbxyzptlk/db231222/ae/z;->O()Ldbxyzptlk/db231222/ae/z;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Ldbxyzptlk/db231222/ac/r;-><init>(JLdbxyzptlk/db231222/ac/a;)V

    .line 268
    return-void
.end method

.method public constructor <init>(JLdbxyzptlk/db231222/ac/a;)V
    .locals 4

    .prologue
    .line 335
    invoke-direct {p0}, Ldbxyzptlk/db231222/ad/f;-><init>()V

    .line 336
    invoke-static {p3}, Ldbxyzptlk/db231222/ac/f;->a(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    .line 338
    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/a;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/db231222/ac/i;->a:Ldbxyzptlk/db231222/ac/i;

    invoke-virtual {v1, v2, p1, p2}, Ldbxyzptlk/db231222/ac/i;->a(Ldbxyzptlk/db231222/ac/i;J)J

    move-result-wide v1

    .line 339
    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/a;->b()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    .line 340
    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/a;->u()Ldbxyzptlk/db231222/ac/c;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Ldbxyzptlk/db231222/ac/c;->d(J)J

    move-result-wide v1

    iput-wide v1, p0, Ldbxyzptlk/db231222/ac/r;->b:J

    .line 341
    iput-object v0, p0, Ldbxyzptlk/db231222/ac/r;->c:Ldbxyzptlk/db231222/ac/a;

    .line 342
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 467
    iget-object v0, p0, Ldbxyzptlk/db231222/ac/r;->c:Ldbxyzptlk/db231222/ac/a;

    if-nez v0, :cond_1

    .line 468
    new-instance v0, Ldbxyzptlk/db231222/ac/r;

    iget-wide v1, p0, Ldbxyzptlk/db231222/ac/r;->b:J

    invoke-static {}, Ldbxyzptlk/db231222/ae/z;->N()Ldbxyzptlk/db231222/ae/z;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/ac/r;-><init>(JLdbxyzptlk/db231222/ac/a;)V

    move-object p0, v0

    .line 473
    :cond_0
    :goto_0
    return-object p0

    .line 470
    :cond_1
    sget-object v0, Ldbxyzptlk/db231222/ac/i;->a:Ldbxyzptlk/db231222/ac/i;

    iget-object v1, p0, Ldbxyzptlk/db231222/ac/r;->c:Ldbxyzptlk/db231222/ac/a;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/ac/a;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/ac/i;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 471
    new-instance v0, Ldbxyzptlk/db231222/ac/r;

    iget-wide v1, p0, Ldbxyzptlk/db231222/ac/r;->b:J

    iget-object v3, p0, Ldbxyzptlk/db231222/ac/r;->c:Ldbxyzptlk/db231222/ac/a;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/ac/a;->b()Ldbxyzptlk/db231222/ac/a;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/ac/r;-><init>(JLdbxyzptlk/db231222/ac/a;)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 486
    const/4 v0, 0x3

    return v0
.end method

.method public final a(I)I
    .locals 3

    .prologue
    .line 524
    packed-switch p1, :pswitch_data_0

    .line 532
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 526
    :pswitch_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/r;->c()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/a;->E()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/r;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/ac/c;->a(J)I

    move-result v0

    .line 530
    :goto_0
    return v0

    .line 528
    :pswitch_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/r;->c()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/a;->C()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/r;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/ac/c;->a(J)I

    move-result v0

    goto :goto_0

    .line 530
    :pswitch_2
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/r;->c()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/a;->u()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/r;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/ac/c;->a(J)I

    move-result v0

    goto :goto_0

    .line 524
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ldbxyzptlk/db231222/ac/D;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 676
    if-ne p0, p1, :cond_0

    .line 687
    :goto_0
    return v1

    .line 679
    :cond_0
    instance-of v0, p1, Ldbxyzptlk/db231222/ac/r;

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 680
    check-cast v0, Ldbxyzptlk/db231222/ac/r;

    .line 681
    iget-object v2, p0, Ldbxyzptlk/db231222/ac/r;->c:Ldbxyzptlk/db231222/ac/a;

    iget-object v3, v0, Ldbxyzptlk/db231222/ac/r;->c:Ldbxyzptlk/db231222/ac/a;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 682
    iget-wide v2, p0, Ldbxyzptlk/db231222/ac/r;->b:J

    iget-wide v4, v0, Ldbxyzptlk/db231222/ac/r;->b:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    const/4 v0, -0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    iget-wide v2, p0, Ldbxyzptlk/db231222/ac/r;->b:J

    iget-wide v4, v0, Ldbxyzptlk/db231222/ac/r;->b:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    goto :goto_1

    .line 687
    :cond_3
    invoke-super {p0, p1}, Ldbxyzptlk/db231222/ad/f;->a(Ldbxyzptlk/db231222/ac/D;)I

    move-result v1

    goto :goto_0
.end method

.method public final a(Ldbxyzptlk/db231222/ac/d;)I
    .locals 3

    .prologue
    .line 552
    if-nez p1, :cond_0

    .line 553
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The DateTimeFieldType must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 555
    :cond_0
    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/ac/r;->b(Ldbxyzptlk/db231222/ac/d;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 556
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Field \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' is not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 558
    :cond_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/r;->c()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/ac/d;->a(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/r;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/ac/c;->a(J)I

    move-result v0

    return v0
.end method

.method protected final a(ILdbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/c;
    .locals 3

    .prologue
    .line 499
    packed-switch p1, :pswitch_data_0

    .line 507
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 501
    :pswitch_0
    invoke-virtual {p2}, Ldbxyzptlk/db231222/ac/a;->E()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    .line 505
    :goto_0
    return-object v0

    .line 503
    :pswitch_1
    invoke-virtual {p2}, Ldbxyzptlk/db231222/ac/a;->C()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    goto :goto_0

    .line 505
    :pswitch_2
    invoke-virtual {p2}, Ldbxyzptlk/db231222/ac/a;->u()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    goto :goto_0

    .line 499
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected final b()J
    .locals 2

    .prologue
    .line 610
    iget-wide v0, p0, Ldbxyzptlk/db231222/ac/r;->b:J

    return-wide v0
.end method

.method public final b(Ldbxyzptlk/db231222/ac/d;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 570
    if-nez p1, :cond_1

    .line 579
    :cond_0
    :goto_0
    return v0

    .line 573
    :cond_1
    invoke-virtual {p1}, Ldbxyzptlk/db231222/ac/d;->y()Ldbxyzptlk/db231222/ac/m;

    move-result-object v1

    .line 574
    sget-object v2, Ldbxyzptlk/db231222/ac/r;->a:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/r;->c()Ldbxyzptlk/db231222/ac/a;

    move-result-object v2

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/ac/m;->a(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/ac/l;->d()J

    move-result-wide v1

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/r;->c()Ldbxyzptlk/db231222/ac/a;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/ac/a;->s()Ldbxyzptlk/db231222/ac/l;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/ac/l;->d()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-ltz v1, :cond_0

    .line 577
    :cond_2
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/r;->c()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/ac/d;->a(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/c;->c()Z

    move-result v0

    goto :goto_0
.end method

.method public final c()Ldbxyzptlk/db231222/ac/a;
    .locals 1

    .prologue
    .line 619
    iget-object v0, p0, Ldbxyzptlk/db231222/ac/r;->c:Ldbxyzptlk/db231222/ac/a;

    return-object v0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 82
    check-cast p1, Ldbxyzptlk/db231222/ac/D;

    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/ac/r;->a(Ldbxyzptlk/db231222/ac/D;)I

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 3

    .prologue
    .line 1462
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/r;->c()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/a;->E()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/r;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/ac/c;->a(J)I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 632
    if-ne p0, p1, :cond_0

    .line 641
    :goto_0
    return v1

    .line 635
    :cond_0
    instance-of v0, p1, Ldbxyzptlk/db231222/ac/r;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 636
    check-cast v0, Ldbxyzptlk/db231222/ac/r;

    .line 637
    iget-object v2, p0, Ldbxyzptlk/db231222/ac/r;->c:Ldbxyzptlk/db231222/ac/a;

    iget-object v3, v0, Ldbxyzptlk/db231222/ac/r;->c:Ldbxyzptlk/db231222/ac/a;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 638
    iget-wide v2, p0, Ldbxyzptlk/db231222/ac/r;->b:J

    iget-wide v4, v0, Ldbxyzptlk/db231222/ac/r;->b:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 641
    :cond_2
    invoke-super {p0, p1}, Ldbxyzptlk/db231222/ad/f;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 651
    iget v0, p0, Ldbxyzptlk/db231222/ac/r;->d:I

    .line 652
    if-nez v0, :cond_0

    .line 653
    invoke-super {p0}, Ldbxyzptlk/db231222/ad/f;->hashCode()I

    move-result v0

    iput v0, p0, Ldbxyzptlk/db231222/ac/r;->d:I

    .line 655
    :cond_0
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1
    .annotation runtime Lorg/joda/convert/ToString;
    .end annotation

    .prologue
    .line 1819
    invoke-static {}, Ldbxyzptlk/db231222/ah/y;->b()Ldbxyzptlk/db231222/ah/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Ldbxyzptlk/db231222/ah/c;->a(Ldbxyzptlk/db231222/ac/D;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
