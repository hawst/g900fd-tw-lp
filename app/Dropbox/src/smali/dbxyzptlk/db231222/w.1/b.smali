.class Ldbxyzptlk/db231222/w/b;
.super Ldbxyzptlk/db231222/w/a;
.source "panda.py"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public a:Ldbxyzptlk/db231222/w/c;

.field public b:I

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/http/HttpResponse;)V
    .locals 2

    .prologue
    .line 158
    invoke-direct {p0}, Ldbxyzptlk/db231222/w/a;-><init>()V

    .line 159
    invoke-virtual {p0}, Ldbxyzptlk/db231222/w/b;->fillInStackTrace()Ljava/lang/Throwable;

    .line 160
    if-eqz p1, :cond_0

    .line 161
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    .line 162
    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    iput v1, p0, Ldbxyzptlk/db231222/w/b;->b:I

    .line 163
    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/w/b;->c:Ljava/lang/String;

    .line 164
    const-string v0, "server"

    invoke-static {p1, v0}, Ldbxyzptlk/db231222/w/b;->a(Lorg/apache/http/HttpResponse;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/w/b;->d:Ljava/lang/String;

    .line 165
    const-string v0, "location"

    invoke-static {p1, v0}, Ldbxyzptlk/db231222/w/b;->a(Lorg/apache/http/HttpResponse;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/w/b;->e:Ljava/lang/String;

    .line 167
    :cond_0
    return-void
.end method

.method public constructor <init>(Lorg/apache/http/HttpResponse;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 177
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/w/b;-><init>(Lorg/apache/http/HttpResponse;)V

    .line 179
    if-eqz p2, :cond_0

    instance-of v0, p2, Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 180
    check-cast p2, Ljava/util/Map;

    iput-object p2, p0, Ldbxyzptlk/db231222/w/b;->f:Ljava/util/Map;

    .line 181
    new-instance v0, Ldbxyzptlk/db231222/w/c;

    iget-object v1, p0, Ldbxyzptlk/db231222/w/b;->f:Ljava/util/Map;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/w/c;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/w/b;->a:Ldbxyzptlk/db231222/w/c;

    .line 183
    :cond_0
    return-void
.end method

.method private static a(Lorg/apache/http/HttpResponse;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 249
    const/4 v0, 0x0

    .line 250
    invoke-interface {p0, p1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    .line 251
    if-eqz v1, :cond_0

    .line 252
    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 254
    :cond_0
    return-object v0
.end method

.method public static a(Lorg/apache/http/HttpResponse;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v4, -0x1

    .line 225
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    .line 226
    const/16 v3, 0x12e

    if-ne v2, v3, :cond_1

    .line 227
    const-string v2, "location"

    invoke-static {p0, v2}, Ldbxyzptlk/db231222/w/b;->a(Lorg/apache/http/HttpResponse;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 229
    if-eqz v2, :cond_2

    .line 230
    const-string v3, "://"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 231
    if-le v3, v4, :cond_2

    .line 232
    add-int/lit8 v3, v3, 0x3

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 233
    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 234
    if-le v3, v4, :cond_2

    .line 235
    invoke-virtual {v2, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 236
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "dropbox.com"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 245
    :cond_0
    :goto_0
    return v0

    .line 242
    :cond_1
    const/16 v3, 0x130

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    .line 245
    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Ldbxyzptlk/db231222/w/b;->a:Ldbxyzptlk/db231222/w/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/w/b;->a:Ldbxyzptlk/db231222/w/c;

    iget-object v0, v0, Ldbxyzptlk/db231222/w/c;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/w/b;->a:Ldbxyzptlk/db231222/w/c;

    iget-object v0, v0, Ldbxyzptlk/db231222/w/c;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 214
    iget-object v0, p0, Ldbxyzptlk/db231222/w/b;->a:Ldbxyzptlk/db231222/w/c;

    iget-object p1, v0, Ldbxyzptlk/db231222/w/c;->b:Ljava/lang/String;

    .line 216
    :cond_0
    return-object p1
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 191
    iget v0, p0, Ldbxyzptlk/db231222/w/b;->b:I

    const/16 v1, 0x190

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/w/b;->a:Ldbxyzptlk/db231222/w/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/w/b;->a:Ldbxyzptlk/db231222/w/c;

    iget-object v0, v0, Ldbxyzptlk/db231222/w/c;->a:Ljava/lang/String;

    const-string v1, "taken"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 196
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Ldbxyzptlk/db231222/w/b;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/w/b;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 197
    iget-object v1, p0, Ldbxyzptlk/db231222/w/b;->a:Ldbxyzptlk/db231222/w/c;

    if-eqz v1, :cond_0

    .line 198
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/w/b;->a:Ldbxyzptlk/db231222/w/c;

    iget-object v1, v1, Ldbxyzptlk/db231222/w/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 200
    :cond_0
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/w/b;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
