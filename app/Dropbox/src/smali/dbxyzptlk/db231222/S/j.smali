.class public final Ldbxyzptlk/db231222/S/j;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/S/G;


# instance fields
.field private final a:Ldbxyzptlk/db231222/S/h;

.field private final b:Ljava/io/InputStream;

.field private final c:Ljava/io/OutputStream;

.field private d:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/S/h;Ljava/io/OutputStream;Ljava/io/InputStream;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    .line 56
    iput-object p2, p0, Ldbxyzptlk/db231222/S/j;->c:Ljava/io/OutputStream;

    .line 57
    iput-object p2, p0, Ldbxyzptlk/db231222/S/j;->d:Ljava/io/OutputStream;

    .line 58
    iput-object p3, p0, Ldbxyzptlk/db231222/S/j;->b:Ljava/io/InputStream;

    .line 59
    return-void
.end method

.method static synthetic a(Ldbxyzptlk/db231222/S/j;)Ldbxyzptlk/db231222/S/h;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    return-object v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/S/h;Ljava/io/InputStream;)Z
    .locals 1

    .prologue
    .line 32
    invoke-static {p0, p1}, Ldbxyzptlk/db231222/S/j;->b(Ldbxyzptlk/db231222/S/h;Ljava/io/InputStream;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Ldbxyzptlk/db231222/S/j;)Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Ldbxyzptlk/db231222/S/j;->b:Ljava/io/InputStream;

    return-object v0
.end method

.method private static b(Ldbxyzptlk/db231222/S/h;Ljava/io/InputStream;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 186
    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    .line 187
    if-nez v1, :cond_1

    .line 200
    :cond_0
    :goto_0
    return v0

    .line 188
    :cond_1
    invoke-virtual {v1}, Ldbxyzptlk/db231222/Q/b;->c()Ljava/net/Socket;

    move-result-object v2

    .line 189
    if-eqz v2, :cond_0

    .line 191
    :try_start_0
    invoke-virtual {v2}, Ljava/net/Socket;->getSoTimeout()I

    move-result v3

    .line 192
    const/16 v1, 0x64

    invoke-virtual {v2, v1}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    :try_start_1
    invoke-static {p1}, Ldbxyzptlk/db231222/R/v;->b(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 195
    const/4 v1, 0x1

    .line 197
    :try_start_2
    invoke-virtual {v2, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-virtual {v2, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    throw v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 199
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/net/CacheRequest;)Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 205
    iget-object v0, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/h;->o()Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    new-instance v0, Ldbxyzptlk/db231222/S/n;

    iget-object v1, p0, Ldbxyzptlk/db231222/S/j;->b:Ljava/io/InputStream;

    iget-object v2, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    const/4 v3, 0x0

    invoke-direct {v0, v1, p1, v2, v3}, Ldbxyzptlk/db231222/S/n;-><init>(Ljava/io/InputStream;Ljava/net/CacheRequest;Ldbxyzptlk/db231222/S/h;I)V

    .line 221
    :goto_0
    return-object v0

    .line 209
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v0, v0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/B;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 210
    new-instance v0, Ldbxyzptlk/db231222/S/l;

    iget-object v1, p0, Ldbxyzptlk/db231222/S/j;->b:Ljava/io/InputStream;

    invoke-direct {v0, v1, p1, p0}, Ldbxyzptlk/db231222/S/l;-><init>(Ljava/io/InputStream;Ljava/net/CacheRequest;Ldbxyzptlk/db231222/S/j;)V

    goto :goto_0

    .line 213
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v0, v0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/B;->h()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 214
    new-instance v0, Ldbxyzptlk/db231222/S/n;

    iget-object v1, p0, Ldbxyzptlk/db231222/S/j;->b:Ljava/io/InputStream;

    iget-object v2, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v3, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v3, v3, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/S/B;->h()I

    move-result v3

    invoke-direct {v0, v1, p1, v2, v3}, Ldbxyzptlk/db231222/S/n;-><init>(Ljava/io/InputStream;Ljava/net/CacheRequest;Ldbxyzptlk/db231222/S/h;I)V

    goto :goto_0

    .line 221
    :cond_2
    new-instance v0, Ldbxyzptlk/db231222/S/H;

    iget-object v1, p0, Ldbxyzptlk/db231222/S/j;->b:Ljava/io/InputStream;

    iget-object v2, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    invoke-direct {v0, v1, p1, v2}, Ldbxyzptlk/db231222/S/H;-><init>(Ljava/io/InputStream;Ljava/net/CacheRequest;Ldbxyzptlk/db231222/S/h;)V

    goto :goto_0
.end method

.method public final a()Ljava/io/OutputStream;
    .locals 7

    .prologue
    const-wide/16 v5, -0x1

    const/4 v4, 0x0

    .line 62
    iget-object v0, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v0, v0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/z;->a()Z

    move-result v0

    .line 63
    if-nez v0, :cond_0

    iget-object v1, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v1, v1, Ldbxyzptlk/db231222/S/h;->a:Ldbxyzptlk/db231222/S/w;

    invoke-interface {v1}, Ldbxyzptlk/db231222/S/w;->d()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v1, v1, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/Q/b;->j()I

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    iget-object v0, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v0, v0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/z;->q()V

    .line 67
    const/4 v0, 0x1

    .line 71
    :cond_0
    if-eqz v0, :cond_2

    .line 72
    iget-object v0, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v0, v0, Ldbxyzptlk/db231222/S/h;->a:Ldbxyzptlk/db231222/S/w;

    invoke-interface {v0}, Ldbxyzptlk/db231222/S/w;->d()I

    move-result v0

    .line 73
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 74
    const/16 v0, 0x400

    .line 76
    :cond_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/S/j;->c()V

    .line 77
    new-instance v1, Ldbxyzptlk/db231222/S/m;

    iget-object v2, p0, Ldbxyzptlk/db231222/S/j;->d:Ljava/io/OutputStream;

    invoke-direct {v1, v2, v0, v4}, Ldbxyzptlk/db231222/S/m;-><init>(Ljava/io/OutputStream;ILdbxyzptlk/db231222/S/k;)V

    move-object v0, v1

    .line 103
    :goto_0
    return-object v0

    .line 81
    :cond_2
    iget-object v0, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v0, v0, Ldbxyzptlk/db231222/S/h;->a:Ldbxyzptlk/db231222/S/w;

    invoke-interface {v0}, Ldbxyzptlk/db231222/S/w;->c()J

    move-result-wide v1

    .line 82
    cmp-long v0, v1, v5

    if-eqz v0, :cond_3

    .line 83
    iget-object v0, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v0, v0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/S/z;->a(J)V

    .line 84
    invoke-virtual {p0}, Ldbxyzptlk/db231222/S/j;->c()V

    .line 85
    new-instance v0, Ldbxyzptlk/db231222/S/o;

    iget-object v3, p0, Ldbxyzptlk/db231222/S/j;->d:Ljava/io/OutputStream;

    invoke-direct {v0, v3, v1, v2, v4}, Ldbxyzptlk/db231222/S/o;-><init>(Ljava/io/OutputStream;JLdbxyzptlk/db231222/S/k;)V

    goto :goto_0

    .line 88
    :cond_3
    iget-object v0, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v0, v0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/z;->j()J

    move-result-wide v1

    .line 89
    const-wide/32 v3, 0x7fffffff

    cmp-long v0, v1, v3

    if-lez v0, :cond_4

    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Use setFixedLengthStreamingMode() or setChunkedStreamingMode() for requests larger than 2 GiB."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_4
    cmp-long v0, v1, v5

    if-eqz v0, :cond_5

    .line 96
    invoke-virtual {p0}, Ldbxyzptlk/db231222/S/j;->c()V

    .line 97
    new-instance v0, Ldbxyzptlk/db231222/S/D;

    long-to-int v1, v1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/S/D;-><init>(I)V

    goto :goto_0

    .line 103
    :cond_5
    new-instance v0, Ldbxyzptlk/db231222/S/D;

    invoke-direct {v0}, Ldbxyzptlk/db231222/S/D;-><init>()V

    goto :goto_0
.end method

.method public final a(Ldbxyzptlk/db231222/S/D;)V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Ldbxyzptlk/db231222/S/j;->d:Ljava/io/OutputStream;

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/S/D;->a(Ljava/io/OutputStream;)V

    .line 113
    return-void
.end method

.method public final a(ZLjava/io/OutputStream;Ljava/io/InputStream;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 146
    if-eqz p1, :cond_1

    .line 173
    :cond_0
    :goto_0
    return v0

    .line 151
    :cond_1
    if-eqz p2, :cond_2

    check-cast p2, Ldbxyzptlk/db231222/R/a;

    invoke-virtual {p2}, Ldbxyzptlk/db231222/R/a;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 156
    :cond_2
    iget-object v1, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v1, v1, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/S/z;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 161
    iget-object v1, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v1, v1, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v1, v1, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/S/B;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 165
    :cond_3
    instance-of v1, p3, Ldbxyzptlk/db231222/S/H;

    if-nez v1, :cond_0

    .line 169
    if-eqz p3, :cond_4

    .line 170
    iget-object v0, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    invoke-static {v0, p3}, Ldbxyzptlk/db231222/S/j;->b(Ldbxyzptlk/db231222/S/h;Ljava/io/InputStream;)Z

    move-result v0

    goto :goto_0

    .line 173
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Ldbxyzptlk/db231222/S/j;->d:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 108
    iget-object v0, p0, Ldbxyzptlk/db231222/S/j;->c:Ljava/io/OutputStream;

    iput-object v0, p0, Ldbxyzptlk/db231222/S/j;->d:Ljava/io/OutputStream;

    .line 109
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/h;->d()V

    .line 129
    iget-object v0, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v0, v0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/z;->c()Ldbxyzptlk/db231222/S/x;

    move-result-object v0

    .line 130
    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/x;->f()[B

    move-result-object v0

    .line 131
    iget-object v1, p0, Ldbxyzptlk/db231222/S/j;->d:Ljava/io/OutputStream;

    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 132
    return-void
.end method

.method public final d()Ldbxyzptlk/db231222/S/B;
    .locals 3

    .prologue
    .line 135
    iget-object v0, p0, Ldbxyzptlk/db231222/S/j;->b:Ljava/io/InputStream;

    invoke-static {v0}, Ldbxyzptlk/db231222/S/x;->a(Ljava/io/InputStream;)Ldbxyzptlk/db231222/S/x;

    move-result-object v0

    .line 136
    iget-object v1, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v1, v1, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/x;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/Q/b;->a(I)V

    .line 137
    iget-object v1, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/S/h;->a(Ldbxyzptlk/db231222/S/x;)V

    .line 139
    new-instance v1, Ldbxyzptlk/db231222/S/B;

    iget-object v2, p0, Ldbxyzptlk/db231222/S/j;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v2, v2, Ldbxyzptlk/db231222/S/h;->g:Ljava/net/URI;

    invoke-direct {v1, v2, v0}, Ldbxyzptlk/db231222/S/B;-><init>(Ljava/net/URI;Ldbxyzptlk/db231222/S/x;)V

    .line 140
    const-string v0, "http/1.1"

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/S/B;->a(Ljava/lang/String;)V

    .line 141
    return-object v1
.end method
