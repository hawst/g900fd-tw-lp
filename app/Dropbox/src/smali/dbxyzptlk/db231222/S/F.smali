.class public final Ldbxyzptlk/db231222/S/F;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/S/G;


# instance fields
.field private final a:Ldbxyzptlk/db231222/S/h;

.field private final b:Ldbxyzptlk/db231222/T/q;

.field private c:Ldbxyzptlk/db231222/T/x;


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/S/h;Ldbxyzptlk/db231222/T/q;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Ldbxyzptlk/db231222/S/F;->a:Ldbxyzptlk/db231222/S/h;

    .line 36
    iput-object p2, p0, Ldbxyzptlk/db231222/S/F;->b:Ldbxyzptlk/db231222/T/q;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Ljava/net/CacheRequest;)Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 81
    new-instance v0, Ldbxyzptlk/db231222/S/H;

    iget-object v1, p0, Ldbxyzptlk/db231222/S/F;->c:Ldbxyzptlk/db231222/T/x;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/T/x;->d()Ljava/io/InputStream;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/db231222/S/F;->a:Ldbxyzptlk/db231222/S/h;

    invoke-direct {v0, v1, p1, v2}, Ldbxyzptlk/db231222/S/H;-><init>(Ljava/io/InputStream;Ljava/net/CacheRequest;Ldbxyzptlk/db231222/S/h;)V

    return-object v0
.end method

.method public final a()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Ldbxyzptlk/db231222/S/F;->c()V

    .line 42
    iget-object v0, p0, Ldbxyzptlk/db231222/S/F;->c:Ldbxyzptlk/db231222/T/x;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/T/x;->e()Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/S/D;)V
    .locals 1

    .prologue
    .line 63
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(ZLjava/io/OutputStream;Ljava/io/InputStream;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 86
    if-eqz p1, :cond_0

    .line 87
    iget-object v1, p0, Ldbxyzptlk/db231222/S/F;->c:Ldbxyzptlk/db231222/T/x;

    if-eqz v1, :cond_1

    .line 88
    iget-object v1, p0, Ldbxyzptlk/db231222/S/F;->c:Ldbxyzptlk/db231222/T/x;

    sget-object v2, Ldbxyzptlk/db231222/T/a;->l:Ldbxyzptlk/db231222/T/a;

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/T/x;->b(Ldbxyzptlk/db231222/T/a;)V

    .line 97
    :cond_0
    :goto_0
    return v0

    .line 94
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Ldbxyzptlk/db231222/S/F;->c:Ldbxyzptlk/db231222/T/x;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/T/x;->e()Ljava/io/OutputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 68
    return-void
.end method

.method public final c()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 46
    iget-object v0, p0, Ldbxyzptlk/db231222/S/F;->c:Ldbxyzptlk/db231222/T/x;

    if-eqz v0, :cond_0

    .line 60
    :goto_0
    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/S/F;->a:Ldbxyzptlk/db231222/S/h;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/h;->d()V

    .line 50
    iget-object v0, p0, Ldbxyzptlk/db231222/S/F;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v0, v0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/z;->c()Ldbxyzptlk/db231222/S/x;

    move-result-object v0

    .line 51
    iget-object v1, p0, Ldbxyzptlk/db231222/S/F;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v1, v1, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/Q/b;->j()I

    move-result v1

    if-ne v1, v6, :cond_1

    const-string v3, "HTTP/1.1"

    .line 52
    :goto_1
    iget-object v1, p0, Ldbxyzptlk/db231222/S/F;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v1, v1, Ldbxyzptlk/db231222/S/h;->a:Ldbxyzptlk/db231222/S/w;

    invoke-interface {v1}, Ldbxyzptlk/db231222/S/w;->getURL()Ljava/net/URL;

    move-result-object v4

    .line 53
    iget-object v1, p0, Ldbxyzptlk/db231222/S/F;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v1, v1, Ldbxyzptlk/db231222/S/h;->c:Ljava/lang/String;

    invoke-static {v4}, Ldbxyzptlk/db231222/S/h;->a(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4}, Ldbxyzptlk/db231222/S/h;->b(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Ldbxyzptlk/db231222/S/F;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v5, v5, Ldbxyzptlk/db231222/S/h;->g:Ljava/net/URI;

    invoke-virtual {v5}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Ldbxyzptlk/db231222/S/x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    iget-object v1, p0, Ldbxyzptlk/db231222/S/F;->a:Ldbxyzptlk/db231222/S/h;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/S/h;->e()Z

    move-result v1

    .line 57
    iget-object v2, p0, Ldbxyzptlk/db231222/S/F;->b:Ldbxyzptlk/db231222/T/q;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/x;->g()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v0, v1, v6}, Ldbxyzptlk/db231222/T/q;->a(Ljava/util/List;ZZ)Ldbxyzptlk/db231222/T/x;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/S/F;->c:Ldbxyzptlk/db231222/T/x;

    .line 59
    iget-object v0, p0, Ldbxyzptlk/db231222/S/F;->c:Ldbxyzptlk/db231222/T/x;

    iget-object v1, p0, Ldbxyzptlk/db231222/S/F;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v1, v1, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/Q/r;->b()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/T/x;->a(J)V

    goto :goto_0

    .line 51
    :cond_1
    const-string v3, "HTTP/1.0"

    goto :goto_1
.end method

.method public final d()Ldbxyzptlk/db231222/S/B;
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Ldbxyzptlk/db231222/S/F;->c:Ldbxyzptlk/db231222/T/x;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/T/x;->c()Ljava/util/List;

    move-result-object v0

    .line 72
    invoke-static {v0}, Ldbxyzptlk/db231222/S/x;->a(Ljava/util/List;)Ldbxyzptlk/db231222/S/x;

    move-result-object v0

    .line 73
    iget-object v1, p0, Ldbxyzptlk/db231222/S/F;->a:Ldbxyzptlk/db231222/S/h;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/S/h;->a(Ldbxyzptlk/db231222/S/x;)V

    .line 75
    new-instance v1, Ldbxyzptlk/db231222/S/B;

    iget-object v2, p0, Ldbxyzptlk/db231222/S/F;->a:Ldbxyzptlk/db231222/S/h;

    iget-object v2, v2, Ldbxyzptlk/db231222/S/h;->g:Ljava/net/URI;

    invoke-direct {v1, v2, v0}, Ldbxyzptlk/db231222/S/B;-><init>(Ljava/net/URI;Ldbxyzptlk/db231222/S/x;)V

    .line 76
    const-string v0, "spdy/3"

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/S/B;->a(Ljava/lang/String;)V

    .line 77
    return-object v1
.end method
