.class public Ldbxyzptlk/db231222/S/h;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final j:Ljava/net/CacheResponse;


# instance fields
.field protected final a:Ldbxyzptlk/db231222/S/w;

.field protected final b:Ldbxyzptlk/db231222/Q/r;

.field protected final c:Ljava/lang/String;

.field protected d:Ldbxyzptlk/db231222/Q/b;

.field protected e:Ldbxyzptlk/db231222/S/E;

.field f:J

.field final g:Ljava/net/URI;

.field final h:Ldbxyzptlk/db231222/S/z;

.field i:Ldbxyzptlk/db231222/S/B;

.field private k:Ldbxyzptlk/db231222/Q/u;

.field private l:Ljava/io/OutputStream;

.field private m:Ldbxyzptlk/db231222/S/G;

.field private n:Ljava/io/InputStream;

.field private o:Ljava/io/InputStream;

.field private p:Ljava/net/CacheResponse;

.field private q:Ljava/net/CacheRequest;

.field private r:Z

.field private s:Ldbxyzptlk/db231222/S/B;

.field private t:Ljava/io/InputStream;

.field private u:Z

.field private v:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    new-instance v0, Ldbxyzptlk/db231222/S/i;

    invoke-direct {v0}, Ldbxyzptlk/db231222/S/i;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/S/h;->j:Ljava/net/CacheResponse;

    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/Q/r;Ldbxyzptlk/db231222/S/w;Ljava/lang/String;Ldbxyzptlk/db231222/S/x;Ldbxyzptlk/db231222/Q/b;Ldbxyzptlk/db231222/S/D;)V
    .locals 3

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Ldbxyzptlk/db231222/S/h;->f:J

    .line 152
    iput-object p1, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    .line 153
    iput-object p2, p0, Ldbxyzptlk/db231222/S/h;->a:Ldbxyzptlk/db231222/S/w;

    .line 154
    iput-object p3, p0, Ldbxyzptlk/db231222/S/h;->c:Ljava/lang/String;

    .line 155
    iput-object p5, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    .line 156
    iput-object p6, p0, Ldbxyzptlk/db231222/S/h;->l:Ljava/io/OutputStream;

    .line 159
    :try_start_0
    invoke-static {}, Ldbxyzptlk/db231222/R/m;->a()Ldbxyzptlk/db231222/R/m;

    move-result-object v0

    invoke-interface {p2}, Ldbxyzptlk/db231222/S/w;->getURL()Ljava/net/URL;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/R/m;->a(Ljava/net/URL;)Ljava/net/URI;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/S/h;->g:Ljava/net/URI;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    new-instance v0, Ldbxyzptlk/db231222/S/z;

    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->g:Ljava/net/URI;

    new-instance v2, Ldbxyzptlk/db231222/S/x;

    invoke-direct {v2, p4}, Ldbxyzptlk/db231222/S/x;-><init>(Ldbxyzptlk/db231222/S/x;)V

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/S/z;-><init>(Ljava/net/URI;Ldbxyzptlk/db231222/S/x;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    .line 165
    return-void

    .line 160
    :catch_0
    move-exception v0

    .line 161
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static a(Ljava/net/URL;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 564
    invoke-virtual {p0}, Ljava/net/URL;->getFile()Ljava/lang/String;

    move-result-object v0

    .line 565
    if-nez v0, :cond_1

    .line 566
    const-string v0, "/"

    .line 570
    :cond_0
    :goto_0
    return-object v0

    .line 567
    :cond_1
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 568
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ldbxyzptlk/db231222/S/B;Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->o:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 326
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 328
    :cond_0
    iput-object p1, p0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    .line 329
    if-eqz p2, :cond_1

    .line 330
    invoke-direct {p0, p2}, Ldbxyzptlk/db231222/S/h;->a(Ljava/io/InputStream;)V

    .line 332
    :cond_1
    return-void
.end method

.method private a(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 450
    iput-object p1, p0, Ldbxyzptlk/db231222/S/h;->n:Ljava/io/InputStream;

    .line 451
    iget-boolean v0, p0, Ldbxyzptlk/db231222/S/h;->r:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/B;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 459
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/B;->b()V

    .line 460
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/B;->c()V

    .line 461
    new-instance v0, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v0, p1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/S/h;->o:Ljava/io/InputStream;

    .line 465
    :goto_0
    return-void

    .line 463
    :cond_0
    iput-object p1, p0, Ldbxyzptlk/db231222/S/h;->o:Ljava/io/InputStream;

    goto :goto_0
.end method

.method public static b(Ljava/net/URL;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 594
    invoke-virtual {p0}, Ljava/net/URL;->getPort()I

    move-result v1

    .line 595
    invoke-virtual {p0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 596
    if-lez v1, :cond_0

    invoke-virtual {p0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ldbxyzptlk/db231222/R/v;->a(Ljava/lang/String;)I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 597
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 599
    :cond_0
    return-object v0
.end method

.method public static r()Ljava/lang/String;
    .locals 2

    .prologue
    .line 589
    const-string v0, "http.agent"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 590
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Java"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "java.version"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private u()V
    .locals 5

    .prologue
    .line 215
    sget-object v0, Ldbxyzptlk/db231222/Q/u;->c:Ldbxyzptlk/db231222/Q/u;

    iput-object v0, p0, Ldbxyzptlk/db231222/S/h;->k:Ldbxyzptlk/db231222/Q/u;

    .line 216
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->a:Ldbxyzptlk/db231222/S/w;

    invoke-interface {v0}, Ldbxyzptlk/db231222/S/w;->getUseCaches()Z

    move-result v0

    if-nez v0, :cond_1

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/r;->f()Ldbxyzptlk/db231222/Q/t;

    move-result-object v0

    .line 219
    if-eqz v0, :cond_0

    .line 221
    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->g:Ljava/net/URI;

    iget-object v2, p0, Ldbxyzptlk/db231222/S/h;->c:Ljava/lang/String;

    iget-object v3, p0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/S/z;->c()Ldbxyzptlk/db231222/S/x;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ldbxyzptlk/db231222/S/x;->a(Z)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Ldbxyzptlk/db231222/Q/t;->a(Ljava/net/URI;Ljava/lang/String;Ljava/util/Map;)Ljava/net/CacheResponse;

    move-result-object v0

    .line 223
    if-eqz v0, :cond_0

    .line 225
    invoke-virtual {v0}, Ljava/net/CacheResponse;->getHeaders()Ljava/util/Map;

    move-result-object v1

    .line 226
    invoke-virtual {v0}, Ljava/net/CacheResponse;->getBody()Ljava/io/InputStream;

    move-result-object v2

    iput-object v2, p0, Ldbxyzptlk/db231222/S/h;->t:Ljava/io/InputStream;

    .line 227
    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/S/h;->a(Ljava/net/CacheResponse;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    iget-object v2, p0, Ldbxyzptlk/db231222/S/h;->t:Ljava/io/InputStream;

    if-nez v2, :cond_3

    .line 230
    :cond_2
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->t:Ljava/io/InputStream;

    invoke-static {v0}, Ldbxyzptlk/db231222/R/v;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 234
    :cond_3
    const/4 v2, 0x1

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/S/x;->a(Ljava/util/Map;Z)Ldbxyzptlk/db231222/S/x;

    move-result-object v1

    .line 235
    new-instance v2, Ldbxyzptlk/db231222/S/B;

    iget-object v3, p0, Ldbxyzptlk/db231222/S/h;->g:Ljava/net/URI;

    invoke-direct {v2, v3, v1}, Ldbxyzptlk/db231222/S/B;-><init>(Ljava/net/URI;Ldbxyzptlk/db231222/S/x;)V

    iput-object v2, p0, Ldbxyzptlk/db231222/S/h;->s:Ldbxyzptlk/db231222/S/B;

    .line 236
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 237
    iget-object v3, p0, Ldbxyzptlk/db231222/S/h;->s:Ldbxyzptlk/db231222/S/B;

    iget-object v4, p0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    invoke-virtual {v3, v1, v2, v4}, Ldbxyzptlk/db231222/S/B;->a(JLdbxyzptlk/db231222/S/z;)Ldbxyzptlk/db231222/Q/u;

    move-result-object v1

    iput-object v1, p0, Ldbxyzptlk/db231222/S/h;->k:Ldbxyzptlk/db231222/Q/u;

    .line 238
    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->k:Ldbxyzptlk/db231222/Q/u;

    sget-object v2, Ldbxyzptlk/db231222/Q/u;->a:Ldbxyzptlk/db231222/Q/u;

    if-ne v1, v2, :cond_4

    .line 239
    iput-object v0, p0, Ldbxyzptlk/db231222/S/h;->p:Ljava/net/CacheResponse;

    .line 240
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->s:Ldbxyzptlk/db231222/S/B;

    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->t:Ljava/io/InputStream;

    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/S/h;->a(Ldbxyzptlk/db231222/S/B;Ljava/io/InputStream;)V

    goto :goto_0

    .line 241
    :cond_4
    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->k:Ldbxyzptlk/db231222/Q/u;

    sget-object v2, Ldbxyzptlk/db231222/Q/u;->b:Ldbxyzptlk/db231222/Q/u;

    if-ne v1, v2, :cond_5

    .line 242
    iput-object v0, p0, Ldbxyzptlk/db231222/S/h;->p:Ljava/net/CacheResponse;

    goto :goto_0

    .line 243
    :cond_5
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->k:Ldbxyzptlk/db231222/Q/u;

    sget-object v1, Ldbxyzptlk/db231222/Q/u;->c:Ldbxyzptlk/db231222/Q/u;

    if-ne v0, v1, :cond_6

    .line 244
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->t:Ljava/io/InputStream;

    invoke-static {v0}, Ldbxyzptlk/db231222/R/v;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 246
    :cond_6
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private v()V
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    if-nez v0, :cond_0

    .line 252
    invoke-virtual {p0}, Ldbxyzptlk/db231222/S/h;->c()V

    .line 255
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->m:Ldbxyzptlk/db231222/S/G;

    if-eqz v0, :cond_1

    .line 256
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 259
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    invoke-virtual {v0, p0}, Ldbxyzptlk/db231222/Q/b;->a(Ldbxyzptlk/db231222/S/h;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/S/G;

    iput-object v0, p0, Ldbxyzptlk/db231222/S/h;->m:Ldbxyzptlk/db231222/S/G;

    .line 261
    invoke-virtual {p0}, Ldbxyzptlk/db231222/S/h;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->l:Ljava/io/OutputStream;

    if-nez v0, :cond_2

    .line 264
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->m:Ldbxyzptlk/db231222/S/G;

    invoke-interface {v0}, Ldbxyzptlk/db231222/S/G;->a()Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/S/h;->l:Ljava/io/OutputStream;

    .line 266
    :cond_2
    return-void
.end method

.method private w()V
    .locals 4

    .prologue
    .line 394
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->a:Ldbxyzptlk/db231222/S/w;

    invoke-interface {v0}, Ldbxyzptlk/db231222/S/w;->getUseCaches()Z

    move-result v0

    if-nez v0, :cond_1

    .line 408
    :cond_0
    :goto_0
    return-void

    .line 395
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/r;->f()Ldbxyzptlk/db231222/Q/t;

    move-result-object v0

    .line 396
    if-eqz v0, :cond_0

    .line 398
    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->a:Ldbxyzptlk/db231222/S/w;

    invoke-interface {v1}, Ldbxyzptlk/db231222/S/w;->a()Ljava/net/HttpURLConnection;

    move-result-object v1

    .line 401
    iget-object v2, p0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    iget-object v3, p0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    invoke-virtual {v2, v3}, Ldbxyzptlk/db231222/S/B;->a(Ldbxyzptlk/db231222/S/z;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 402
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getRequestMethod()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/db231222/S/h;->g:Ljava/net/URI;

    invoke-interface {v0, v1, v2}, Ldbxyzptlk/db231222/Q/t;->a(Ljava/lang/String;Ljava/net/URI;)V

    goto :goto_0

    .line 407
    :cond_2
    iget-object v2, p0, Ldbxyzptlk/db231222/S/h;->g:Ljava/net/URI;

    invoke-interface {v0, v2, v1}, Ldbxyzptlk/db231222/Q/t;->a(Ljava/net/URI;Ljava/net/URLConnection;)Ljava/net/CacheRequest;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/S/h;->q:Ljava/net/CacheRequest;

    goto :goto_0
.end method

.method private x()V
    .locals 5

    .prologue
    .line 502
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/z;->c()Ldbxyzptlk/db231222/S/x;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/S/h;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/S/x;->a(Ljava/lang/String;)V

    .line 504
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/z;->k()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 505
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    invoke-static {}, Ldbxyzptlk/db231222/S/h;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/S/z;->a(Ljava/lang/String;)V

    .line 508
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/z;->l()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 509
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->a:Ldbxyzptlk/db231222/S/w;

    invoke-interface {v1}, Ldbxyzptlk/db231222/S/w;->getURL()Ljava/net/URL;

    move-result-object v1

    invoke-static {v1}, Ldbxyzptlk/db231222/S/h;->b(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/S/z;->b(Ljava/lang/String;)V

    .line 512
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/b;->j()I

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/z;->m()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    .line 514
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    const-string v1, "Keep-Alive"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/S/z;->c(Ljava/lang/String;)V

    .line 517
    :cond_3
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/z;->n()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    .line 518
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/db231222/S/h;->r:Z

    .line 519
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    const-string v1, "gzip"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/S/z;->d(Ljava/lang/String;)V

    .line 522
    :cond_4
    invoke-virtual {p0}, Ldbxyzptlk/db231222/S/h;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/z;->o()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    .line 523
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    const-string v1, "application/x-www-form-urlencoded"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/S/z;->e(Ljava/lang/String;)V

    .line 526
    :cond_5
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->a:Ldbxyzptlk/db231222/S/w;

    invoke-interface {v0}, Ldbxyzptlk/db231222/S/w;->getIfModifiedSince()J

    move-result-wide v0

    .line 527
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_6

    .line 528
    iget-object v2, p0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ldbxyzptlk/db231222/S/z;->a(Ljava/util/Date;)V

    .line 531
    :cond_6
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/r;->e()Ljava/net/CookieHandler;

    move-result-object v0

    .line 532
    if-eqz v0, :cond_7

    .line 533
    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    iget-object v2, p0, Ldbxyzptlk/db231222/S/h;->g:Ljava/net/URI;

    iget-object v3, p0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/S/z;->c()Ldbxyzptlk/db231222/S/x;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ldbxyzptlk/db231222/S/x;->a(Z)Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/net/CookieHandler;->get(Ljava/net/URI;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/S/z;->a(Ljava/util/Map;)V

    .line 536
    :cond_7
    return-void
.end method

.method private y()Ljava/lang/String;
    .locals 2

    .prologue
    .line 550
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->a:Ldbxyzptlk/db231222/S/w;

    invoke-interface {v0}, Ldbxyzptlk/db231222/S/w;->getURL()Ljava/net/URL;

    move-result-object v0

    .line 551
    invoke-virtual {p0}, Ldbxyzptlk/db231222/S/h;->q()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 552
    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    .line 554
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Ldbxyzptlk/db231222/S/h;->a(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/net/URI;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->g:Ljava/net/URI;

    return-object v0
.end method

.method protected a(Ldbxyzptlk/db231222/Q/b;)V
    .locals 0

    .prologue
    .line 307
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/S/x;)V
    .locals 3

    .prologue
    .line 667
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/r;->e()Ljava/net/CookieHandler;

    move-result-object v0

    .line 668
    if-eqz v0, :cond_0

    .line 669
    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->g:Ljava/net/URI;

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Ldbxyzptlk/db231222/S/x;->a(Z)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/CookieHandler;->put(Ljava/net/URI;Ljava/util/Map;)V

    .line 671
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 431
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->o:Ljava/io/InputStream;

    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->t:Ljava/io/InputStream;

    if-ne v0, v1, :cond_0

    .line 432
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->o:Ljava/io/InputStream;

    invoke-static {v0}, Ldbxyzptlk/db231222/R/v;->a(Ljava/io/Closeable;)V

    .line 435
    :cond_0
    iget-boolean v0, p0, Ldbxyzptlk/db231222/S/h;->v:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    if-eqz v0, :cond_2

    .line 436
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/db231222/S/h;->v:Z

    .line 438
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->m:Ldbxyzptlk/db231222/S/G;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->m:Ldbxyzptlk/db231222/S/G;

    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->l:Ljava/io/OutputStream;

    iget-object v2, p0, Ldbxyzptlk/db231222/S/h;->n:Ljava/io/InputStream;

    invoke-interface {v0, p1, v1, v2}, Ldbxyzptlk/db231222/S/G;->a(ZLjava/io/OutputStream;Ljava/io/InputStream;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 440
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    invoke-static {v0}, Ldbxyzptlk/db231222/R/v;->a(Ljava/io/Closeable;)V

    .line 441
    iput-object v3, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    .line 447
    :cond_2
    :goto_0
    return-void

    .line 442
    :cond_3
    iget-boolean v0, p0, Ldbxyzptlk/db231222/S/h;->u:Z

    if-eqz v0, :cond_2

    .line 443
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/r;->j()Ldbxyzptlk/db231222/Q/c;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/Q/c;->a(Ldbxyzptlk/db231222/Q/b;)V

    .line 444
    iput-object v3, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    goto :goto_0
.end method

.method protected a(Ljava/net/CacheResponse;)Z
    .locals 1

    .prologue
    .line 389
    const/4 v0, 0x1

    return v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 177
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->k:Ldbxyzptlk/db231222/Q/u;

    if-eqz v0, :cond_1

    .line 208
    :cond_0
    :goto_0
    return-void

    .line 181
    :cond_1
    invoke-direct {p0}, Ldbxyzptlk/db231222/S/h;->x()V

    .line 182
    invoke-direct {p0}, Ldbxyzptlk/db231222/S/h;->u()V

    .line 183
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/r;->f()Ldbxyzptlk/db231222/Q/t;

    move-result-object v0

    .line 184
    if-eqz v0, :cond_2

    .line 185
    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->k:Ldbxyzptlk/db231222/Q/u;

    invoke-interface {v0, v1}, Ldbxyzptlk/db231222/Q/t;->a(Ldbxyzptlk/db231222/Q/u;)V

    .line 192
    :cond_2
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/z;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->k:Ldbxyzptlk/db231222/Q/u;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/u;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 193
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->k:Ldbxyzptlk/db231222/Q/u;

    sget-object v1, Ldbxyzptlk/db231222/Q/u;->b:Ldbxyzptlk/db231222/Q/u;

    if-ne v0, v1, :cond_3

    .line 194
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->t:Ljava/io/InputStream;

    invoke-static {v0}, Ldbxyzptlk/db231222/R/v;->a(Ljava/io/Closeable;)V

    .line 196
    :cond_3
    sget-object v0, Ldbxyzptlk/db231222/Q/u;->a:Ldbxyzptlk/db231222/Q/u;

    iput-object v0, p0, Ldbxyzptlk/db231222/S/h;->k:Ldbxyzptlk/db231222/Q/u;

    .line 197
    sget-object v0, Ldbxyzptlk/db231222/S/h;->j:Ljava/net/CacheResponse;

    iput-object v0, p0, Ldbxyzptlk/db231222/S/h;->p:Ljava/net/CacheResponse;

    .line 198
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->p:Ljava/net/CacheResponse;

    invoke-virtual {v0}, Ljava/net/CacheResponse;->getHeaders()Ljava/util/Map;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/S/x;->a(Ljava/util/Map;Z)Ldbxyzptlk/db231222/S/x;

    move-result-object v0

    .line 199
    new-instance v1, Ldbxyzptlk/db231222/S/B;

    iget-object v2, p0, Ldbxyzptlk/db231222/S/h;->g:Ljava/net/URI;

    invoke-direct {v1, v2, v0}, Ldbxyzptlk/db231222/S/B;-><init>(Ljava/net/URI;Ldbxyzptlk/db231222/S/x;)V

    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->p:Ljava/net/CacheResponse;

    invoke-virtual {v0}, Ljava/net/CacheResponse;->getBody()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/S/h;->a(Ldbxyzptlk/db231222/S/B;Ljava/io/InputStream;)V

    .line 202
    :cond_4
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->k:Ldbxyzptlk/db231222/Q/u;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/u;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 203
    invoke-direct {p0}, Ldbxyzptlk/db231222/S/h;->v()V

    goto :goto_0

    .line 204
    :cond_5
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/r;->j()Ldbxyzptlk/db231222/Q/c;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/Q/c;->a(Ldbxyzptlk/db231222/Q/b;)V

    .line 206
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    goto :goto_0
.end method

.method protected final c()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 270
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    if-eqz v0, :cond_1

    .line 300
    :cond_0
    :goto_0
    return-void

    .line 273
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->e:Ldbxyzptlk/db231222/S/E;

    if-nez v0, :cond_3

    .line 274
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->g:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 275
    if-nez v1, :cond_2

    .line 276
    new-instance v0, Ljava/net/UnknownHostException;

    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->g:Ljava/net/URI;

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 280
    :cond_2
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->g:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v2, "https"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 281
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/r;->g()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v3

    .line 282
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/r;->h()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v4

    .line 284
    :goto_1
    new-instance v0, Ldbxyzptlk/db231222/Q/a;

    iget-object v2, p0, Ldbxyzptlk/db231222/S/h;->g:Ljava/net/URI;

    invoke-static {v2}, Ldbxyzptlk/db231222/R/v;->a(Ljava/net/URI;)I

    move-result v2

    iget-object v5, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v5}, Ldbxyzptlk/db231222/Q/r;->i()Ldbxyzptlk/db231222/Q/o;

    move-result-object v5

    iget-object v6, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v6}, Ldbxyzptlk/db231222/Q/r;->c()Ljava/net/Proxy;

    move-result-object v6

    iget-object v7, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v7}, Ldbxyzptlk/db231222/Q/r;->m()Ljava/util/List;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Ldbxyzptlk/db231222/Q/a;-><init>(Ljava/lang/String;ILjavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;Ldbxyzptlk/db231222/Q/o;Ljava/net/Proxy;Ljava/util/List;)V

    .line 286
    new-instance v1, Ldbxyzptlk/db231222/S/E;

    iget-object v3, p0, Ldbxyzptlk/db231222/S/h;->g:Ljava/net/URI;

    iget-object v2, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/Q/r;->d()Ljava/net/ProxySelector;

    move-result-object v4

    iget-object v2, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/Q/r;->j()Ldbxyzptlk/db231222/Q/c;

    move-result-object v5

    sget-object v6, Ldbxyzptlk/db231222/R/j;->a:Ldbxyzptlk/db231222/R/j;

    iget-object v2, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/Q/r;->l()Ldbxyzptlk/db231222/Q/w;

    move-result-object v7

    move-object v2, v0

    invoke-direct/range {v1 .. v7}, Ldbxyzptlk/db231222/S/E;-><init>(Ldbxyzptlk/db231222/Q/a;Ljava/net/URI;Ljava/net/ProxySelector;Ldbxyzptlk/db231222/Q/c;Ldbxyzptlk/db231222/R/j;Ldbxyzptlk/db231222/Q/w;)V

    iput-object v1, p0, Ldbxyzptlk/db231222/S/h;->e:Ldbxyzptlk/db231222/S/E;

    .line 289
    :cond_3
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->e:Ldbxyzptlk/db231222/S/E;

    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/S/E;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/Q/b;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    .line 290
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/b;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 291
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/Q/r;->a()I

    move-result v1

    iget-object v2, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/Q/r;->b()I

    move-result v2

    invoke-virtual {p0}, Ldbxyzptlk/db231222/S/h;->t()Ldbxyzptlk/db231222/Q/x;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Ldbxyzptlk/db231222/Q/b;->a(IILdbxyzptlk/db231222/Q/x;)V

    .line 292
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/r;->j()Ldbxyzptlk/db231222/Q/c;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/Q/c;->b(Ldbxyzptlk/db231222/Q/b;)V

    .line 293
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/r;->l()Ldbxyzptlk/db231222/Q/w;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/Q/b;->b()Ldbxyzptlk/db231222/Q/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/Q/w;->a(Ldbxyzptlk/db231222/Q/v;)V

    .line 295
    :cond_4
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/S/h;->a(Ldbxyzptlk/db231222/Q/b;)V

    .line 296
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/b;->b()Ldbxyzptlk/db231222/Q/v;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/v;->b()Ljava/net/Proxy;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/Q/r;->c()Ljava/net/Proxy;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 298
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/z;->c()Ldbxyzptlk/db231222/S/x;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/S/h;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/S/x;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    move-object v3, v4

    goto/16 :goto_1
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 314
    iget-wide v0, p0, Ldbxyzptlk/db231222/S/h;->f:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 315
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 317
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/S/h;->f:J

    .line 318
    return-void
.end method

.method final e()Z
    .locals 2

    .prologue
    .line 335
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->c:Ljava/lang/String;

    const-string v1, "POST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->c:Ljava/lang/String;

    const-string v1, "PUT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->k:Ldbxyzptlk/db231222/Q/u;

    if-nez v0, :cond_0

    .line 341
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 343
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->l:Ljava/io/OutputStream;

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Ldbxyzptlk/db231222/S/z;
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    return-object v0
.end method

.method public final i()Ldbxyzptlk/db231222/S/B;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    if-nez v0, :cond_0

    .line 356
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 358
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    return-object v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    if-nez v0, :cond_0

    .line 363
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 365
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/B;->f()Ldbxyzptlk/db231222/S/x;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/x;->c()I

    move-result v0

    return v0
.end method

.method public final k()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    if-nez v0, :cond_0

    .line 370
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 372
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->o:Ljava/io/InputStream;

    return-object v0
.end method

.method public final l()Ljava/net/CacheResponse;
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->p:Ljava/net/CacheResponse;

    return-object v0
.end method

.method public final m()Ldbxyzptlk/db231222/Q/b;
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    return-object v0
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 417
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/db231222/S/h;->u:Z

    .line 418
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldbxyzptlk/db231222/S/h;->v:Z

    if-eqz v0, :cond_0

    .line 419
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/r;->j()Ldbxyzptlk/db231222/Q/c;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/Q/c;->a(Ldbxyzptlk/db231222/Q/b;)V

    .line 420
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    .line 422
    :cond_0
    return-void
.end method

.method public final o()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 472
    iget-object v2, p0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/S/B;->f()Ldbxyzptlk/db231222/S/x;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/S/x;->c()I

    move-result v2

    .line 475
    iget-object v3, p0, Ldbxyzptlk/db231222/S/h;->c:Ljava/lang/String;

    const-string v4, "HEAD"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 492
    :cond_0
    :goto_0
    return v0

    .line 479
    :cond_1
    const/16 v3, 0x64

    if-lt v2, v3, :cond_2

    const/16 v3, 0xc8

    if-lt v2, v3, :cond_3

    :cond_2
    const/16 v3, 0xcc

    if-eq v2, v3, :cond_3

    const/16 v3, 0x130

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 482
    goto :goto_0

    .line 488
    :cond_3
    iget-object v2, p0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/S/B;->h()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/S/B;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    .line 489
    goto :goto_0
.end method

.method final p()Ljava/lang/String;
    .locals 3

    .prologue
    .line 544
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/b;->j()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "HTTP/1.1"

    .line 546
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Ldbxyzptlk/db231222/S/h;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Ldbxyzptlk/db231222/S/h;->y()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 544
    :cond_1
    const-string v0, "HTTP/1.0"

    goto :goto_0
.end method

.method protected q()Z
    .locals 2

    .prologue
    .line 583
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    if-nez v0, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->a:Ldbxyzptlk/db231222/S/w;

    invoke-interface {v0}, Ldbxyzptlk/db231222/S/w;->usingProxy()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->d:Ldbxyzptlk/db231222/Q/b;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/b;->b()Ldbxyzptlk/db231222/Q/v;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/v;->b()Ljava/net/Proxy;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()V
    .locals 5

    .prologue
    .line 607
    invoke-virtual {p0}, Ldbxyzptlk/db231222/S/h;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 608
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->k:Ldbxyzptlk/db231222/Q/u;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/S/B;->a(Ldbxyzptlk/db231222/Q/u;)V

    .line 660
    :cond_0
    :goto_0
    return-void

    .line 612
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->k:Ldbxyzptlk/db231222/Q/u;

    if-nez v0, :cond_2

    .line 613
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "readResponse() without sendRequest()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 616
    :cond_2
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->k:Ldbxyzptlk/db231222/Q/u;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/u;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 620
    iget-wide v0, p0, Ldbxyzptlk/db231222/S/h;->f:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    .line 621
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->l:Ljava/io/OutputStream;

    instance-of v0, v0, Ldbxyzptlk/db231222/S/D;

    if-eqz v0, :cond_3

    .line 622
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->l:Ljava/io/OutputStream;

    check-cast v0, Ldbxyzptlk/db231222/S/D;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/D;->c()I

    move-result v0

    .line 623
    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->h:Ldbxyzptlk/db231222/S/z;

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Ldbxyzptlk/db231222/S/z;->a(J)V

    .line 625
    :cond_3
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->m:Ldbxyzptlk/db231222/S/G;

    invoke-interface {v0}, Ldbxyzptlk/db231222/S/G;->c()V

    .line 628
    :cond_4
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->l:Ljava/io/OutputStream;

    if-eqz v0, :cond_5

    .line 629
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->l:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 630
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->l:Ljava/io/OutputStream;

    instance-of v0, v0, Ldbxyzptlk/db231222/S/D;

    if-eqz v0, :cond_5

    .line 631
    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->m:Ldbxyzptlk/db231222/S/G;

    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->l:Ljava/io/OutputStream;

    check-cast v0, Ldbxyzptlk/db231222/S/D;

    invoke-interface {v1, v0}, Ldbxyzptlk/db231222/S/G;->a(Ldbxyzptlk/db231222/S/D;)V

    .line 635
    :cond_5
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->m:Ldbxyzptlk/db231222/S/G;

    invoke-interface {v0}, Ldbxyzptlk/db231222/S/G;->b()V

    .line 637
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->m:Ldbxyzptlk/db231222/S/G;

    invoke-interface {v0}, Ldbxyzptlk/db231222/S/G;->d()Ldbxyzptlk/db231222/S/B;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    .line 638
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    iget-wide v1, p0, Ldbxyzptlk/db231222/S/h;->f:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v0, v1, v2, v3, v4}, Ldbxyzptlk/db231222/S/B;->a(JJ)V

    .line 639
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->k:Ldbxyzptlk/db231222/Q/u;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/S/B;->a(Ldbxyzptlk/db231222/Q/u;)V

    .line 641
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->k:Ldbxyzptlk/db231222/Q/u;

    sget-object v1, Ldbxyzptlk/db231222/Q/u;->b:Ldbxyzptlk/db231222/Q/u;

    if-ne v0, v1, :cond_7

    .line 642
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->s:Ldbxyzptlk/db231222/S/B;

    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/S/B;->a(Ldbxyzptlk/db231222/S/B;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 643
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/S/h;->a(Z)V

    .line 644
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->s:Ldbxyzptlk/db231222/S/B;

    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/S/B;->b(Ldbxyzptlk/db231222/S/B;)Ldbxyzptlk/db231222/S/B;

    move-result-object v0

    .line 645
    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->t:Ljava/io/InputStream;

    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/S/h;->a(Ldbxyzptlk/db231222/S/B;Ljava/io/InputStream;)V

    .line 646
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->b:Ldbxyzptlk/db231222/Q/r;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/Q/r;->f()Ldbxyzptlk/db231222/Q/t;

    move-result-object v0

    .line 647
    invoke-interface {v0}, Ldbxyzptlk/db231222/Q/t;->a()V

    .line 648
    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->p:Ljava/net/CacheResponse;

    iget-object v2, p0, Ldbxyzptlk/db231222/S/h;->a:Ldbxyzptlk/db231222/S/w;

    invoke-interface {v2}, Ldbxyzptlk/db231222/S/w;->a()Ljava/net/HttpURLConnection;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ldbxyzptlk/db231222/Q/t;->a(Ljava/net/CacheResponse;Ljava/net/HttpURLConnection;)V

    goto/16 :goto_0

    .line 651
    :cond_6
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->t:Ljava/io/InputStream;

    invoke-static {v0}, Ldbxyzptlk/db231222/R/v;->a(Ljava/io/Closeable;)V

    .line 655
    :cond_7
    invoke-virtual {p0}, Ldbxyzptlk/db231222/S/h;->o()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 656
    invoke-direct {p0}, Ldbxyzptlk/db231222/S/h;->w()V

    .line 659
    :cond_8
    iget-object v0, p0, Ldbxyzptlk/db231222/S/h;->m:Ldbxyzptlk/db231222/S/G;

    iget-object v1, p0, Ldbxyzptlk/db231222/S/h;->q:Ljava/net/CacheRequest;

    invoke-interface {v0, v1}, Ldbxyzptlk/db231222/S/G;->a(Ljava/net/CacheRequest;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/S/h;->a(Ljava/io/InputStream;)V

    goto/16 :goto_0
.end method

.method protected t()Ldbxyzptlk/db231222/Q/x;
    .locals 1

    .prologue
    .line 663
    const/4 v0, 0x0

    return-object v0
.end method
