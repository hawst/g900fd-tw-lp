.class final Ldbxyzptlk/db231222/S/l;
.super Ldbxyzptlk/db231222/S/a;
.source "panda.py"


# instance fields
.field private final d:Ldbxyzptlk/db231222/S/j;

.field private e:I

.field private f:Z


# direct methods
.method constructor <init>(Ljava/io/InputStream;Ljava/net/CacheRequest;Ldbxyzptlk/db231222/S/j;)V
    .locals 1

    .prologue
    .line 428
    invoke-static {p3}, Ldbxyzptlk/db231222/S/j;->a(Ldbxyzptlk/db231222/S/j;)Ldbxyzptlk/db231222/S/h;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Ldbxyzptlk/db231222/S/a;-><init>(Ljava/io/InputStream;Ldbxyzptlk/db231222/S/h;Ljava/net/CacheRequest;)V

    .line 423
    const/4 v0, -0x1

    iput v0, p0, Ldbxyzptlk/db231222/S/l;->e:I

    .line 424
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/db231222/S/l;->f:Z

    .line 429
    iput-object p3, p0, Ldbxyzptlk/db231222/S/l;->d:Ldbxyzptlk/db231222/S/j;

    .line 430
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 457
    iget v0, p0, Ldbxyzptlk/db231222/S/l;->e:I

    if-eq v0, v2, :cond_0

    .line 458
    iget-object v0, p0, Ldbxyzptlk/db231222/S/l;->a:Ljava/io/InputStream;

    invoke-static {v0}, Ldbxyzptlk/db231222/R/v;->c(Ljava/io/InputStream;)Ljava/lang/String;

    .line 460
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/S/l;->a:Ljava/io/InputStream;

    invoke-static {v0}, Ldbxyzptlk/db231222/R/v;->c(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    .line 461
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 462
    if-eq v1, v2, :cond_1

    .line 463
    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 466
    :cond_1
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Ldbxyzptlk/db231222/S/l;->e:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 470
    iget v0, p0, Ldbxyzptlk/db231222/S/l;->e:I

    if-nez v0, :cond_2

    .line 471
    iput-boolean v3, p0, Ldbxyzptlk/db231222/S/l;->f:Z

    .line 472
    iget-object v0, p0, Ldbxyzptlk/db231222/S/l;->b:Ldbxyzptlk/db231222/S/h;

    iget-object v0, v0, Ldbxyzptlk/db231222/S/h;->i:Ldbxyzptlk/db231222/S/B;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/B;->f()Ldbxyzptlk/db231222/S/x;

    move-result-object v0

    .line 473
    iget-object v1, p0, Ldbxyzptlk/db231222/S/l;->d:Ldbxyzptlk/db231222/S/j;

    invoke-static {v1}, Ldbxyzptlk/db231222/S/j;->b(Ldbxyzptlk/db231222/S/j;)Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1, v0}, Ldbxyzptlk/db231222/S/x;->a(Ljava/io/InputStream;Ldbxyzptlk/db231222/S/x;)V

    .line 474
    iget-object v1, p0, Ldbxyzptlk/db231222/S/l;->b:Ldbxyzptlk/db231222/S/h;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/S/h;->a(Ldbxyzptlk/db231222/S/x;)V

    .line 475
    invoke-virtual {p0}, Ldbxyzptlk/db231222/S/l;->b()V

    .line 477
    :cond_2
    return-void

    .line 467
    :catch_0
    move-exception v1

    .line 468
    new-instance v1, Ljava/net/ProtocolException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected a hex chunk size but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final available()I
    .locals 2

    .prologue
    .line 480
    invoke-virtual {p0}, Ldbxyzptlk/db231222/S/l;->a()V

    .line 481
    iget-boolean v0, p0, Ldbxyzptlk/db231222/S/l;->f:Z

    if-eqz v0, :cond_0

    iget v0, p0, Ldbxyzptlk/db231222/S/l;->e:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 482
    :cond_0
    const/4 v0, 0x0

    .line 484
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/S/l;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    iget v1, p0, Ldbxyzptlk/db231222/S/l;->e:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 488
    iget-boolean v0, p0, Ldbxyzptlk/db231222/S/l;->c:Z

    if-eqz v0, :cond_0

    .line 495
    :goto_0
    return-void

    .line 491
    :cond_0
    iget-boolean v0, p0, Ldbxyzptlk/db231222/S/l;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldbxyzptlk/db231222/S/l;->b:Ldbxyzptlk/db231222/S/h;

    invoke-static {v0, p0}, Ldbxyzptlk/db231222/S/j;->a(Ldbxyzptlk/db231222/S/h;Ljava/io/InputStream;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 492
    invoke-virtual {p0}, Ldbxyzptlk/db231222/S/l;->c()V

    .line 494
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/db231222/S/l;->c:Z

    goto :goto_0
.end method

.method public final read([BII)I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 433
    array-length v1, p1

    invoke-static {v1, p2, p3}, Ldbxyzptlk/db231222/R/v;->a(III)V

    .line 434
    invoke-virtual {p0}, Ldbxyzptlk/db231222/S/l;->a()V

    .line 436
    iget-boolean v1, p0, Ldbxyzptlk/db231222/S/l;->f:Z

    if-nez v1, :cond_1

    .line 452
    :cond_0
    :goto_0
    return v0

    .line 439
    :cond_1
    iget v1, p0, Ldbxyzptlk/db231222/S/l;->e:I

    if-eqz v1, :cond_2

    iget v1, p0, Ldbxyzptlk/db231222/S/l;->e:I

    if-ne v1, v0, :cond_3

    .line 440
    :cond_2
    invoke-direct {p0}, Ldbxyzptlk/db231222/S/l;->d()V

    .line 441
    iget-boolean v1, p0, Ldbxyzptlk/db231222/S/l;->f:Z

    if-eqz v1, :cond_0

    .line 445
    :cond_3
    iget-object v1, p0, Ldbxyzptlk/db231222/S/l;->a:Ljava/io/InputStream;

    iget v2, p0, Ldbxyzptlk/db231222/S/l;->e:I

    invoke-static {p3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {v1, p1, p2, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 446
    if-ne v1, v0, :cond_4

    .line 447
    invoke-virtual {p0}, Ldbxyzptlk/db231222/S/l;->c()V

    .line 448
    new-instance v0, Ljava/io/IOException;

    const-string v1, "unexpected end of stream"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 450
    :cond_4
    iget v0, p0, Ldbxyzptlk/db231222/S/l;->e:I

    sub-int/2addr v0, v1

    iput v0, p0, Ldbxyzptlk/db231222/S/l;->e:I

    .line 451
    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/S/l;->a([BII)V

    move v0, v1

    .line 452
    goto :goto_0
.end method
