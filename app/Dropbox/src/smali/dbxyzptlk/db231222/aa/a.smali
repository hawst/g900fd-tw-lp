.class public final Ldbxyzptlk/db231222/aa/a;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final a:[Ljava/lang/Object;

.field public static final b:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final c:[Ljava/lang/String;

.field public static final d:[J

.field public static final e:[Ljava/lang/Long;

.field public static final f:[I

.field public static final g:[Ljava/lang/Integer;

.field public static final h:[S

.field public static final i:[Ljava/lang/Short;

.field public static final j:[B

.field public static final k:[Ljava/lang/Byte;

.field public static final l:[D

.field public static final m:[Ljava/lang/Double;

.field public static final n:[F

.field public static final o:[Ljava/lang/Float;

.field public static final p:[Z

.field public static final q:[Ljava/lang/Boolean;

.field public static final r:[C

.field public static final s:[Ljava/lang/Character;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    new-array v0, v1, [Ljava/lang/Object;

    sput-object v0, Ldbxyzptlk/db231222/aa/a;->a:[Ljava/lang/Object;

    .line 53
    new-array v0, v1, [Ljava/lang/Class;

    sput-object v0, Ldbxyzptlk/db231222/aa/a;->b:[Ljava/lang/Class;

    .line 57
    new-array v0, v1, [Ljava/lang/String;

    sput-object v0, Ldbxyzptlk/db231222/aa/a;->c:[Ljava/lang/String;

    .line 61
    new-array v0, v1, [J

    sput-object v0, Ldbxyzptlk/db231222/aa/a;->d:[J

    .line 65
    new-array v0, v1, [Ljava/lang/Long;

    sput-object v0, Ldbxyzptlk/db231222/aa/a;->e:[Ljava/lang/Long;

    .line 69
    new-array v0, v1, [I

    sput-object v0, Ldbxyzptlk/db231222/aa/a;->f:[I

    .line 73
    new-array v0, v1, [Ljava/lang/Integer;

    sput-object v0, Ldbxyzptlk/db231222/aa/a;->g:[Ljava/lang/Integer;

    .line 77
    new-array v0, v1, [S

    sput-object v0, Ldbxyzptlk/db231222/aa/a;->h:[S

    .line 81
    new-array v0, v1, [Ljava/lang/Short;

    sput-object v0, Ldbxyzptlk/db231222/aa/a;->i:[Ljava/lang/Short;

    .line 85
    new-array v0, v1, [B

    sput-object v0, Ldbxyzptlk/db231222/aa/a;->j:[B

    .line 89
    new-array v0, v1, [Ljava/lang/Byte;

    sput-object v0, Ldbxyzptlk/db231222/aa/a;->k:[Ljava/lang/Byte;

    .line 93
    new-array v0, v1, [D

    sput-object v0, Ldbxyzptlk/db231222/aa/a;->l:[D

    .line 97
    new-array v0, v1, [Ljava/lang/Double;

    sput-object v0, Ldbxyzptlk/db231222/aa/a;->m:[Ljava/lang/Double;

    .line 101
    new-array v0, v1, [F

    sput-object v0, Ldbxyzptlk/db231222/aa/a;->n:[F

    .line 105
    new-array v0, v1, [Ljava/lang/Float;

    sput-object v0, Ldbxyzptlk/db231222/aa/a;->o:[Ljava/lang/Float;

    .line 109
    new-array v0, v1, [Z

    sput-object v0, Ldbxyzptlk/db231222/aa/a;->p:[Z

    .line 113
    new-array v0, v1, [Ljava/lang/Boolean;

    sput-object v0, Ldbxyzptlk/db231222/aa/a;->q:[Ljava/lang/Boolean;

    .line 117
    new-array v0, v1, [C

    sput-object v0, Ldbxyzptlk/db231222/aa/a;->r:[C

    .line 121
    new-array v0, v1, [Ljava/lang/Character;

    sput-object v0, Ldbxyzptlk/db231222/aa/a;->s:[Ljava/lang/Character;

    return-void
.end method

.method public static a([Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1618
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Ldbxyzptlk/db231222/aa/a;->a([Ljava/lang/Object;Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public static a([Ljava/lang/Object;Ljava/lang/Object;I)I
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 1636
    if-nez p0, :cond_1

    move v0, v1

    .line 1655
    :cond_0
    :goto_0
    return v0

    .line 1639
    :cond_1
    if-gez p2, :cond_4

    .line 1640
    const/4 v0, 0x0

    .line 1642
    :goto_1
    if-nez p1, :cond_2

    .line 1643
    :goto_2
    array-length v2, p0

    if-ge v0, v2, :cond_3

    .line 1644
    aget-object v2, p0, v0

    if-eqz v2, :cond_0

    .line 1643
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1648
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1649
    :goto_3
    array-length v2, p0

    if-ge v0, v2, :cond_3

    .line 1650
    aget-object v2, p0, v0

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1649
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    move v0, v1

    .line 1655
    goto :goto_0

    :cond_4
    move v0, p2

    goto :goto_1
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 3979
    if-eqz p0, :cond_0

    .line 3980
    invoke-static {p0}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v1

    .line 3981
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    add-int/lit8 v2, v1, 0x1

    invoke-static {v0, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    .line 3982
    invoke-static {p0, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3985
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    invoke-static {p1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public static a([Ljava/lang/Integer;)[I
    .locals 3

    .prologue
    .line 2827
    if-nez p0, :cond_0

    .line 2828
    const/4 v0, 0x0

    .line 2836
    :goto_0
    return-object v0

    .line 2829
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    .line 2830
    sget-object v0, Ldbxyzptlk/db231222/aa/a;->f:[I

    goto :goto_0

    .line 2832
    :cond_1
    array-length v0, p0

    new-array v1, v0, [I

    .line 2833
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 2834
    aget-object v2, p0, v0

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v1, v0

    .line 2833
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 2836
    goto :goto_0
.end method

.method public static a([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 320
    if-nez p0, :cond_0

    .line 321
    const/4 v0, 0x0

    .line 323
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    goto :goto_0
.end method

.method public static varargs a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;[TT;)[TT;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 3449
    if-nez p0, :cond_0

    .line 3450
    invoke-static {p1}, Ldbxyzptlk/db231222/aa/a;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 3474
    :goto_0
    return-object v0

    .line 3451
    :cond_0
    if-nez p1, :cond_1

    .line 3452
    invoke-static {p0}, Ldbxyzptlk/db231222/aa/a;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 3454
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v1

    .line 3456
    array-length v0, p0

    array-length v2, p1

    add-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 3457
    array-length v2, p0

    invoke-static {p0, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3459
    const/4 v2, 0x0

    :try_start_0
    array-length v3, p0

    array-length v4, p1

    invoke-static {p1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Ljava/lang/ArrayStoreException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3460
    :catch_0
    move-exception v0

    .line 3467
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v2

    .line 3468
    invoke-virtual {v1, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 3469
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot store "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " in an array of "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 3472
    :cond_2
    throw v0
.end method

.method public static b([Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1721
    invoke-static {p0, p1}, Ldbxyzptlk/db231222/aa/a;->a([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 3740
    if-eqz p0, :cond_0

    .line 3741
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 3748
    :goto_0
    invoke-static {p0, v0}, Ldbxyzptlk/db231222/aa/a;->a(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 3749
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aput-object p1, v0, v1

    .line 3750
    return-object v0

    .line 3742
    :cond_0
    if-eqz p1, :cond_1

    .line 3743
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 3745
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Arguments cannot both be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
