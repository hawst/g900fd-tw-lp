.class public final Ldbxyzptlk/db231222/aa/f;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 148
    const-string v0, "\\s+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/aa/f;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public static a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I
    .locals 14

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 6062
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 6063
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Strings must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 6083
    :cond_1
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 6084
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 6086
    if-nez v0, :cond_2

    .line 6135
    :goto_0
    return v1

    .line 6088
    :cond_2
    if-nez v1, :cond_3

    move v1, v0

    .line 6089
    goto :goto_0

    .line 6092
    :cond_3
    if-le v0, v1, :cond_8

    .line 6098
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 6101
    :goto_1
    add-int/lit8 v2, v1, 0x1

    new-array v8, v2, [I

    .line 6102
    add-int/lit8 v2, v1, 0x1

    new-array v2, v2, [I

    move v5, v3

    .line 6113
    :goto_2
    if-gt v5, v1, :cond_4

    .line 6114
    aput v5, v8, v5

    .line 6113
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_4
    move v6, v4

    move-object v7, v2

    .line 6117
    :goto_3
    if-gt v6, v0, :cond_7

    .line 6118
    add-int/lit8 v2, v6, -0x1

    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v9

    .line 6119
    aput v6, v7, v3

    move v5, v4

    .line 6121
    :goto_4
    if-gt v5, v1, :cond_6

    .line 6122
    add-int/lit8 v2, v5, -0x1

    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-ne v2, v9, :cond_5

    move v2, v3

    .line 6124
    :goto_5
    add-int/lit8 v10, v5, -0x1

    aget v10, v7, v10

    add-int/lit8 v10, v10, 0x1

    aget v11, v8, v5

    add-int/lit8 v11, v11, 0x1

    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    move-result v10

    add-int/lit8 v11, v5, -0x1

    aget v11, v8, v11

    add-int/2addr v2, v11

    invoke-static {v10, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    aput v2, v7, v5

    .line 6121
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_4

    :cond_5
    move v2, v4

    .line 6122
    goto :goto_5

    .line 6117
    :cond_6
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move-object v12, v8

    move-object v8, v7

    move-object v7, v12

    goto :goto_3

    .line 6135
    :cond_7
    aget v1, v8, v1

    goto :goto_0

    :cond_8
    move v12, v1

    move v1, v0

    move v0, v12

    move-object v13, p1

    move-object p1, p0

    move-object p0, v13

    goto :goto_1
.end method

.method public static a(CI)Ljava/lang/String;
    .locals 2

    .prologue
    .line 4554
    new-array v1, p1, [C

    .line 4555
    add-int/lit8 v0, p1, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 4556
    aput-char p0, v1, v0

    .line 4555
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 4558
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 4465
    if-nez p0, :cond_1

    .line 4466
    const/4 p0, 0x0

    .line 4497
    :cond_0
    :goto_0
    return-object p0

    .line 4468
    :cond_1
    if-gtz p1, :cond_2

    .line 4469
    const-string p0, ""

    goto :goto_0

    .line 4471
    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 4472
    if-eq p1, v3, :cond_0

    if-eqz v1, :cond_0

    .line 4475
    if-ne v1, v3, :cond_3

    const/16 v2, 0x2000

    if-gt p1, v2, :cond_3

    .line 4476
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0, p1}, Ldbxyzptlk/db231222/aa/f;->a(CI)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 4479
    :cond_3
    mul-int v2, v1, p1

    .line 4480
    packed-switch v1, :pswitch_data_0

    .line 4493
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 4494
    :goto_1
    if-ge v0, p1, :cond_5

    .line 4495
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4494
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 4482
    :pswitch_0
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0, p1}, Ldbxyzptlk/db231222/aa/f;->a(CI)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 4484
    :pswitch_1
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 4485
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 4486
    new-array v2, v2, [C

    .line 4487
    mul-int/lit8 v0, p1, 0x2

    add-int/lit8 v0, v0, -0x2

    :goto_2
    if-ltz v0, :cond_4

    .line 4488
    aput-char v1, v2, v0

    .line 4489
    add-int/lit8 v4, v0, 0x1

    aput-char v3, v2, v4

    .line 4487
    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 4491
    :cond_4
    new-instance p0, Ljava/lang/String;

    invoke-direct {p0, v2}, Ljava/lang/String;-><init>([C)V

    goto :goto_0

    .line 4497
    :cond_5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 4480
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 3667
    invoke-static {p0}, Ldbxyzptlk/db231222/aa/f;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Ldbxyzptlk/db231222/aa/f;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3673
    :cond_0
    :goto_0
    return-object p0

    .line 3670
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3671
    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 4522
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 4523
    :cond_0
    invoke-static {p0, p2}, Ldbxyzptlk/db231222/aa/f;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 4527
    :goto_0
    return-object v0

    .line 4526
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Ldbxyzptlk/db231222/aa/f;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 4527
    invoke-static {v0, p1}, Ldbxyzptlk/db231222/aa/f;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/CharSequence;)Z
    .locals 1

    .prologue
    .line 184
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
