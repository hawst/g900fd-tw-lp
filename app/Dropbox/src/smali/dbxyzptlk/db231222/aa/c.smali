.class public final enum Ldbxyzptlk/db231222/aa/c;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldbxyzptlk/db231222/aa/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ldbxyzptlk/db231222/aa/c;

.field public static final enum b:Ldbxyzptlk/db231222/aa/c;

.field public static final enum c:Ldbxyzptlk/db231222/aa/c;

.field public static final enum d:Ldbxyzptlk/db231222/aa/c;

.field public static final enum e:Ldbxyzptlk/db231222/aa/c;

.field public static final enum f:Ldbxyzptlk/db231222/aa/c;

.field public static final enum g:Ldbxyzptlk/db231222/aa/c;

.field public static final enum h:Ldbxyzptlk/db231222/aa/c;

.field public static final enum i:Ldbxyzptlk/db231222/aa/c;

.field private static final synthetic l:[Ldbxyzptlk/db231222/aa/c;


# instance fields
.field private j:F

.field private k:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/high16 v5, 0x3fc00000    # 1.5f

    .line 32
    new-instance v0, Ldbxyzptlk/db231222/aa/c;

    const-string v1, "JAVA_0_9"

    const-string v2, "0.9"

    invoke-direct {v0, v1, v6, v5, v2}, Ldbxyzptlk/db231222/aa/c;-><init>(Ljava/lang/String;IFLjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/aa/c;->a:Ldbxyzptlk/db231222/aa/c;

    .line 37
    new-instance v0, Ldbxyzptlk/db231222/aa/c;

    const-string v1, "JAVA_1_1"

    const v2, 0x3f8ccccd    # 1.1f

    const-string v3, "1.1"

    invoke-direct {v0, v1, v7, v2, v3}, Ldbxyzptlk/db231222/aa/c;-><init>(Ljava/lang/String;IFLjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/aa/c;->b:Ldbxyzptlk/db231222/aa/c;

    .line 42
    new-instance v0, Ldbxyzptlk/db231222/aa/c;

    const-string v1, "JAVA_1_2"

    const v2, 0x3f99999a    # 1.2f

    const-string v3, "1.2"

    invoke-direct {v0, v1, v8, v2, v3}, Ldbxyzptlk/db231222/aa/c;-><init>(Ljava/lang/String;IFLjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/aa/c;->c:Ldbxyzptlk/db231222/aa/c;

    .line 47
    new-instance v0, Ldbxyzptlk/db231222/aa/c;

    const-string v1, "JAVA_1_3"

    const v2, 0x3fa66666    # 1.3f

    const-string v3, "1.3"

    invoke-direct {v0, v1, v9, v2, v3}, Ldbxyzptlk/db231222/aa/c;-><init>(Ljava/lang/String;IFLjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/aa/c;->d:Ldbxyzptlk/db231222/aa/c;

    .line 52
    new-instance v0, Ldbxyzptlk/db231222/aa/c;

    const-string v1, "JAVA_1_4"

    const/4 v2, 0x4

    const v3, 0x3fb33333    # 1.4f

    const-string v4, "1.4"

    invoke-direct {v0, v1, v2, v3, v4}, Ldbxyzptlk/db231222/aa/c;-><init>(Ljava/lang/String;IFLjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/aa/c;->e:Ldbxyzptlk/db231222/aa/c;

    .line 57
    new-instance v0, Ldbxyzptlk/db231222/aa/c;

    const-string v1, "JAVA_1_5"

    const/4 v2, 0x5

    const-string v3, "1.5"

    invoke-direct {v0, v1, v2, v5, v3}, Ldbxyzptlk/db231222/aa/c;-><init>(Ljava/lang/String;IFLjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/aa/c;->f:Ldbxyzptlk/db231222/aa/c;

    .line 62
    new-instance v0, Ldbxyzptlk/db231222/aa/c;

    const-string v1, "JAVA_1_6"

    const/4 v2, 0x6

    const v3, 0x3fcccccd    # 1.6f

    const-string v4, "1.6"

    invoke-direct {v0, v1, v2, v3, v4}, Ldbxyzptlk/db231222/aa/c;-><init>(Ljava/lang/String;IFLjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/aa/c;->g:Ldbxyzptlk/db231222/aa/c;

    .line 67
    new-instance v0, Ldbxyzptlk/db231222/aa/c;

    const-string v1, "JAVA_1_7"

    const/4 v2, 0x7

    const v3, 0x3fd9999a    # 1.7f

    const-string v4, "1.7"

    invoke-direct {v0, v1, v2, v3, v4}, Ldbxyzptlk/db231222/aa/c;-><init>(Ljava/lang/String;IFLjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/aa/c;->h:Ldbxyzptlk/db231222/aa/c;

    .line 72
    new-instance v0, Ldbxyzptlk/db231222/aa/c;

    const-string v1, "JAVA_1_8"

    const/16 v2, 0x8

    const v3, 0x3fe66666    # 1.8f

    const-string v4, "1.8"

    invoke-direct {v0, v1, v2, v3, v4}, Ldbxyzptlk/db231222/aa/c;-><init>(Ljava/lang/String;IFLjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/aa/c;->i:Ldbxyzptlk/db231222/aa/c;

    .line 27
    const/16 v0, 0x9

    new-array v0, v0, [Ldbxyzptlk/db231222/aa/c;

    sget-object v1, Ldbxyzptlk/db231222/aa/c;->a:Ldbxyzptlk/db231222/aa/c;

    aput-object v1, v0, v6

    sget-object v1, Ldbxyzptlk/db231222/aa/c;->b:Ldbxyzptlk/db231222/aa/c;

    aput-object v1, v0, v7

    sget-object v1, Ldbxyzptlk/db231222/aa/c;->c:Ldbxyzptlk/db231222/aa/c;

    aput-object v1, v0, v8

    sget-object v1, Ldbxyzptlk/db231222/aa/c;->d:Ldbxyzptlk/db231222/aa/c;

    aput-object v1, v0, v9

    const/4 v1, 0x4

    sget-object v2, Ldbxyzptlk/db231222/aa/c;->e:Ldbxyzptlk/db231222/aa/c;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Ldbxyzptlk/db231222/aa/c;->f:Ldbxyzptlk/db231222/aa/c;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Ldbxyzptlk/db231222/aa/c;->g:Ldbxyzptlk/db231222/aa/c;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Ldbxyzptlk/db231222/aa/c;->h:Ldbxyzptlk/db231222/aa/c;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Ldbxyzptlk/db231222/aa/c;->i:Ldbxyzptlk/db231222/aa/c;

    aput-object v2, v0, v1

    sput-object v0, Ldbxyzptlk/db231222/aa/c;->l:[Ldbxyzptlk/db231222/aa/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IFLjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 90
    iput p3, p0, Ldbxyzptlk/db231222/aa/c;->j:F

    .line 91
    iput-object p4, p0, Ldbxyzptlk/db231222/aa/c;->k:Ljava/lang/String;

    .line 92
    return-void
.end method

.method static a(Ljava/lang/String;)Ldbxyzptlk/db231222/aa/c;
    .locals 1

    .prologue
    .line 132
    const-string v0, "0.9"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    sget-object v0, Ldbxyzptlk/db231222/aa/c;->a:Ldbxyzptlk/db231222/aa/c;

    .line 151
    :goto_0
    return-object v0

    .line 134
    :cond_0
    const-string v0, "1.1"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135
    sget-object v0, Ldbxyzptlk/db231222/aa/c;->b:Ldbxyzptlk/db231222/aa/c;

    goto :goto_0

    .line 136
    :cond_1
    const-string v0, "1.2"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 137
    sget-object v0, Ldbxyzptlk/db231222/aa/c;->c:Ldbxyzptlk/db231222/aa/c;

    goto :goto_0

    .line 138
    :cond_2
    const-string v0, "1.3"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 139
    sget-object v0, Ldbxyzptlk/db231222/aa/c;->d:Ldbxyzptlk/db231222/aa/c;

    goto :goto_0

    .line 140
    :cond_3
    const-string v0, "1.4"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 141
    sget-object v0, Ldbxyzptlk/db231222/aa/c;->e:Ldbxyzptlk/db231222/aa/c;

    goto :goto_0

    .line 142
    :cond_4
    const-string v0, "1.5"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 143
    sget-object v0, Ldbxyzptlk/db231222/aa/c;->f:Ldbxyzptlk/db231222/aa/c;

    goto :goto_0

    .line 144
    :cond_5
    const-string v0, "1.6"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 145
    sget-object v0, Ldbxyzptlk/db231222/aa/c;->g:Ldbxyzptlk/db231222/aa/c;

    goto :goto_0

    .line 146
    :cond_6
    const-string v0, "1.7"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 147
    sget-object v0, Ldbxyzptlk/db231222/aa/c;->h:Ldbxyzptlk/db231222/aa/c;

    goto :goto_0

    .line 148
    :cond_7
    const-string v0, "1.8"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 149
    sget-object v0, Ldbxyzptlk/db231222/aa/c;->i:Ldbxyzptlk/db231222/aa/c;

    goto :goto_0

    .line 151
    :cond_8
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Ldbxyzptlk/db231222/aa/c;
    .locals 1

    .prologue
    .line 27
    const-class v0, Ldbxyzptlk/db231222/aa/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/aa/c;

    return-object v0
.end method

.method public static final values()[Ldbxyzptlk/db231222/aa/c;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Ldbxyzptlk/db231222/aa/c;->l:[Ldbxyzptlk/db231222/aa/c;

    invoke-virtual {v0}, [Ldbxyzptlk/db231222/aa/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/aa/c;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Ldbxyzptlk/db231222/aa/c;->k:Ljava/lang/String;

    return-object v0
.end method
