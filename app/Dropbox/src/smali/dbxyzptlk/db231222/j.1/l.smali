.class public final Ldbxyzptlk/db231222/j/l;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field a:Lcom/dropbox/android/util/DropboxPath;

.field b:J

.field c:Lcom/dropbox/android/albums/AlbumItemEntry;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(J)V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/j/l;->a:Lcom/dropbox/android/util/DropboxPath;

    .line 50
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Ldbxyzptlk/db231222/j/l;->b:J

    .line 59
    iput-wide p1, p0, Ldbxyzptlk/db231222/j/l;->b:J

    .line 60
    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/albums/Album;)V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/j/l;->a:Lcom/dropbox/android/util/DropboxPath;

    .line 50
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Ldbxyzptlk/db231222/j/l;->b:J

    .line 67
    invoke-virtual {p1}, Lcom/dropbox/android/albums/Album;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/j/l;->d:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/albums/AlbumItemEntry;)V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/j/l;->a:Lcom/dropbox/android/util/DropboxPath;

    .line 50
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Ldbxyzptlk/db231222/j/l;->b:J

    .line 63
    iput-object p1, p0, Ldbxyzptlk/db231222/j/l;->c:Lcom/dropbox/android/albums/AlbumItemEntry;

    .line 64
    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/util/DropboxPath;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/j/l;->a:Lcom/dropbox/android/util/DropboxPath;

    .line 50
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Ldbxyzptlk/db231222/j/l;->b:J

    .line 55
    iput-object p1, p0, Ldbxyzptlk/db231222/j/l;->a:Lcom/dropbox/android/util/DropboxPath;

    .line 56
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 73
    instance-of v1, p1, Ldbxyzptlk/db231222/j/l;

    if-nez v1, :cond_1

    .line 86
    :cond_0
    :goto_0
    return v0

    .line 77
    :cond_1
    check-cast p1, Ldbxyzptlk/db231222/j/l;

    .line 79
    iget-object v1, p0, Ldbxyzptlk/db231222/j/l;->a:Lcom/dropbox/android/util/DropboxPath;

    if-eqz v1, :cond_2

    .line 80
    iget-object v0, p0, Ldbxyzptlk/db231222/j/l;->a:Lcom/dropbox/android/util/DropboxPath;

    iget-object v1, p1, Ldbxyzptlk/db231222/j/l;->a:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/DropboxPath;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 81
    :cond_2
    iget-wide v1, p0, Ldbxyzptlk/db231222/j/l;->b:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_3

    .line 82
    iget-wide v1, p0, Ldbxyzptlk/db231222/j/l;->b:J

    iget-wide v3, p1, Ldbxyzptlk/db231222/j/l;->b:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 83
    :cond_3
    iget-object v0, p0, Ldbxyzptlk/db231222/j/l;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 84
    iget-object v0, p0, Ldbxyzptlk/db231222/j/l;->d:Ljava/lang/String;

    iget-object v1, p1, Ldbxyzptlk/db231222/j/l;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 85
    :cond_4
    iget-object v0, p0, Ldbxyzptlk/db231222/j/l;->c:Lcom/dropbox/android/albums/AlbumItemEntry;

    if-eqz v0, :cond_5

    .line 86
    iget-object v0, p0, Ldbxyzptlk/db231222/j/l;->c:Lcom/dropbox/android/albums/AlbumItemEntry;

    iget-object v1, p1, Ldbxyzptlk/db231222/j/l;->c:Lcom/dropbox/android/albums/AlbumItemEntry;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/albums/AlbumItemEntry;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 88
    :cond_5
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected to be constructed without something to compare on"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 94
    iget-object v0, p0, Ldbxyzptlk/db231222/j/l;->a:Lcom/dropbox/android/util/DropboxPath;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Ldbxyzptlk/db231222/j/l;->a:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->hashCode()I

    move-result v0

    .line 101
    :goto_0
    return v0

    .line 96
    :cond_0
    iget-wide v0, p0, Ldbxyzptlk/db231222/j/l;->b:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 97
    iget-wide v0, p0, Ldbxyzptlk/db231222/j/l;->b:J

    long-to-int v0, v0

    goto :goto_0

    .line 98
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/j/l;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 99
    iget-object v0, p0, Ldbxyzptlk/db231222/j/l;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 100
    :cond_2
    iget-object v0, p0, Ldbxyzptlk/db231222/j/l;->c:Lcom/dropbox/android/albums/AlbumItemEntry;

    if-eqz v0, :cond_3

    .line 101
    iget-object v0, p0, Ldbxyzptlk/db231222/j/l;->c:Lcom/dropbox/android/albums/AlbumItemEntry;

    invoke-virtual {v0}, Lcom/dropbox/android/albums/AlbumItemEntry;->hashCode()I

    move-result v0

    goto :goto_0

    .line 103
    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected to be constructed without something to hash on"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 110
    iget-object v0, p0, Ldbxyzptlk/db231222/j/l;->a:Lcom/dropbox/android/util/DropboxPath;

    if-eqz v0, :cond_0

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StatusPath - path: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/j/l;->a:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 117
    :goto_0
    return-object v0

    .line 112
    :cond_0
    iget-wide v0, p0, Ldbxyzptlk/db231222/j/l;->b:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StatusPath - upload id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Ldbxyzptlk/db231222/j/l;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 114
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/j/l;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StatusPath - album: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/j/l;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 116
    :cond_2
    iget-object v0, p0, Ldbxyzptlk/db231222/j/l;->c:Lcom/dropbox/android/albums/AlbumItemEntry;

    if-eqz v0, :cond_3

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StatusPath - album entry: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/j/l;->c:Lcom/dropbox/android/albums/AlbumItemEntry;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 119
    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected to be constructed without something to be stringified"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
