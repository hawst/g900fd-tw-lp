.class public final Ldbxyzptlk/db231222/j/j;
.super Lcom/dropbox/android/util/bf;
.source "panda.py"


# instance fields
.field final synthetic a:Ldbxyzptlk/db231222/j/i;

.field private b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/database/ContentObserver;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ldbxyzptlk/db231222/j/l;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/j/i;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 246
    iput-object p1, p0, Ldbxyzptlk/db231222/j/j;->a:Ldbxyzptlk/db231222/j/i;

    .line 247
    invoke-direct {p0, p2}, Lcom/dropbox/android/util/bf;-><init>(Landroid/database/Cursor;)V

    .line 243
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/j/j;->b:Ljava/util/Set;

    .line 248
    invoke-direct {p0}, Ldbxyzptlk/db231222/j/j;->b()V

    .line 249
    return-void
.end method

.method static synthetic a(Ldbxyzptlk/db231222/j/j;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Ldbxyzptlk/db231222/j/j;->b:Ljava/util/Set;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 306
    invoke-virtual {p0}, Ldbxyzptlk/db231222/j/j;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 307
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can\'t track closed cursors"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 312
    :cond_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 314
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/j/j;->moveToPosition(I)Z

    .line 315
    :cond_1
    :goto_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/j/j;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 316
    invoke-static {p0}, Ldbxyzptlk/db231222/j/i;->a(Landroid/database/Cursor;)Ldbxyzptlk/db231222/j/l;

    move-result-object v0

    .line 317
    if-eqz v0, :cond_1

    .line 318
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 322
    :cond_2
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/j/l;

    .line 323
    iget-object v3, p0, Ldbxyzptlk/db231222/j/j;->a:Ldbxyzptlk/db231222/j/i;

    invoke-virtual {v3, v0, p0}, Ldbxyzptlk/db231222/j/i;->a(Ldbxyzptlk/db231222/j/l;Ldbxyzptlk/db231222/j/j;)V

    goto :goto_1

    .line 326
    :cond_3
    iput-object v1, p0, Ldbxyzptlk/db231222/j/j;->c:Ljava/util/Set;

    .line 328
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 3

    .prologue
    .line 276
    iget-object v0, p0, Ldbxyzptlk/db231222/j/j;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/j/l;

    .line 277
    iget-object v2, p0, Ldbxyzptlk/db231222/j/j;->a:Ldbxyzptlk/db231222/j/i;

    invoke-virtual {v2, v0, p0}, Ldbxyzptlk/db231222/j/i;->b(Ldbxyzptlk/db231222/j/l;Ldbxyzptlk/db231222/j/j;)V

    goto :goto_0

    .line 279
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/j/j;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 280
    return-void
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 284
    invoke-super {p0}, Lcom/dropbox/android/util/bf;->close()V

    .line 285
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/j/j;->b:Ljava/util/Set;

    .line 286
    invoke-virtual {p0}, Ldbxyzptlk/db231222/j/j;->a()V

    .line 287
    return-void
.end method

.method public final registerContentObserver(Landroid/database/ContentObserver;)V
    .locals 1

    .prologue
    .line 291
    invoke-super {p0, p1}, Lcom/dropbox/android/util/bf;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 292
    iget-object v0, p0, Ldbxyzptlk/db231222/j/j;->b:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Ldbxyzptlk/db231222/j/j;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 295
    :cond_0
    return-void
.end method

.method public final requery()Z
    .locals 1

    .prologue
    .line 267
    invoke-virtual {p0}, Ldbxyzptlk/db231222/j/j;->a()V

    .line 268
    invoke-super {p0}, Lcom/dropbox/android/util/bf;->requery()Z

    move-result v0

    .line 269
    if-eqz v0, :cond_0

    .line 270
    invoke-direct {p0}, Ldbxyzptlk/db231222/j/j;->b()V

    .line 272
    :cond_0
    return v0
.end method

.method public final unregisterContentObserver(Landroid/database/ContentObserver;)V
    .locals 1

    .prologue
    .line 299
    invoke-super {p0, p1}, Lcom/dropbox/android/util/bf;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 300
    iget-object v0, p0, Ldbxyzptlk/db231222/j/j;->b:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Ldbxyzptlk/db231222/j/j;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 303
    :cond_0
    return-void
.end method
