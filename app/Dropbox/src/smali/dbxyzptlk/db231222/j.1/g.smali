.class public abstract Ldbxyzptlk/db231222/j/g;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:Lcom/dropbox/android/taskqueue/w;

.field private final b:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ldbxyzptlk/db231222/j/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-object v0, Lcom/dropbox/android/taskqueue/w;->a:Lcom/dropbox/android/taskqueue/w;

    iput-object v0, p0, Ldbxyzptlk/db231222/j/g;->a:Lcom/dropbox/android/taskqueue/w;

    .line 39
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/j/g;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-void
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;)Ljava/lang/String;
.end method

.method public final a(Lcom/dropbox/android/taskqueue/w;)V
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Ldbxyzptlk/db231222/j/g;->a:Lcom/dropbox/android/taskqueue/w;

    if-eq p1, v0, :cond_0

    .line 26
    iput-object p1, p0, Ldbxyzptlk/db231222/j/g;->a:Lcom/dropbox/android/taskqueue/w;

    .line 27
    invoke-virtual {p0}, Ldbxyzptlk/db231222/j/g;->d()V

    .line 29
    :cond_0
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/j/h;)V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Ldbxyzptlk/db231222/j/g;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    return-void
.end method

.method public abstract a()Z
.end method

.method public final b(Ldbxyzptlk/db231222/j/h;)V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Ldbxyzptlk/db231222/j/g;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Tried to remove non-existent observer"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_0
    return-void
.end method

.method public abstract b()Z
.end method

.method public final c()Lcom/dropbox/android/taskqueue/w;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Ldbxyzptlk/db231222/j/g;->a:Lcom/dropbox/android/taskqueue/w;

    return-object v0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Ldbxyzptlk/db231222/j/g;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/j/h;

    .line 53
    invoke-interface {v0}, Ldbxyzptlk/db231222/j/h;->a()V

    goto :goto_0

    .line 55
    :cond_0
    return-void
.end method
