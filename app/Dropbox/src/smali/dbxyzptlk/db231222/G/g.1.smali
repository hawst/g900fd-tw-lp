.class final Ldbxyzptlk/db231222/G/g;
.super Ljava/util/AbstractSet;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractSet",
        "<",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ldbxyzptlk/db231222/G/e;


# direct methods
.method constructor <init>(Ldbxyzptlk/db231222/G/e;)V
    .locals 0

    .prologue
    .line 1901
    iput-object p1, p0, Ldbxyzptlk/db231222/G/g;->a:Ldbxyzptlk/db231222/G/e;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 1

    .prologue
    .line 1945
    iget-object v0, p0, Ldbxyzptlk/db231222/G/g;->a:Ldbxyzptlk/db231222/G/e;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/G/e;->clear()V

    .line 1946
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1910
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-nez v1, :cond_1

    .line 1920
    :cond_0
    :goto_0
    return v0

    .line 1913
    :cond_1
    check-cast p1, Ljava/util/Map$Entry;

    .line 1914
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 1915
    if-eqz v1, :cond_0

    .line 1918
    iget-object v2, p0, Ldbxyzptlk/db231222/G/g;->a:Ldbxyzptlk/db231222/G/e;

    invoke-virtual {v2, v1}, Ldbxyzptlk/db231222/G/e;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1920
    if-eqz v1, :cond_0

    iget-object v2, p0, Ldbxyzptlk/db231222/G/g;->a:Ldbxyzptlk/db231222/G/e;

    iget-object v2, v2, Ldbxyzptlk/db231222/G/e;->a:Ldbxyzptlk/db231222/G/r;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ldbxyzptlk/db231222/G/r;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 1940
    iget-object v0, p0, Ldbxyzptlk/db231222/G/g;->a:Ldbxyzptlk/db231222/G/e;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/G/e;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 1905
    new-instance v0, Ldbxyzptlk/db231222/G/f;

    iget-object v1, p0, Ldbxyzptlk/db231222/G/g;->a:Ldbxyzptlk/db231222/G/e;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/G/f;-><init>(Ldbxyzptlk/db231222/G/e;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1925
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-nez v1, :cond_1

    .line 1930
    :cond_0
    :goto_0
    return v0

    .line 1928
    :cond_1
    check-cast p1, Ljava/util/Map$Entry;

    .line 1929
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 1930
    if-eqz v1, :cond_0

    iget-object v2, p0, Ldbxyzptlk/db231222/G/g;->a:Ldbxyzptlk/db231222/G/e;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ldbxyzptlk/db231222/G/e;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1935
    iget-object v0, p0, Ldbxyzptlk/db231222/G/g;->a:Ldbxyzptlk/db231222/G/e;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/G/e;->size()I

    move-result v0

    return v0
.end method
