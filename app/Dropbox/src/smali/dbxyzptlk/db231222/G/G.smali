.class abstract enum Ldbxyzptlk/db231222/G/G;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldbxyzptlk/db231222/G/G;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ldbxyzptlk/db231222/G/G;

.field public static final enum b:Ldbxyzptlk/db231222/G/G;

.field public static final enum c:Ldbxyzptlk/db231222/G/G;

.field private static final synthetic d:[Ldbxyzptlk/db231222/G/G;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 332
    new-instance v0, Ldbxyzptlk/db231222/G/H;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/G/H;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbxyzptlk/db231222/G/G;->a:Ldbxyzptlk/db231222/G/G;

    .line 364
    new-instance v0, Ldbxyzptlk/db231222/G/I;

    const-string v1, "SOFT"

    invoke-direct {v0, v1, v3}, Ldbxyzptlk/db231222/G/I;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbxyzptlk/db231222/G/G;->b:Ldbxyzptlk/db231222/G/G;

    .line 396
    new-instance v0, Ldbxyzptlk/db231222/G/J;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/db231222/G/J;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbxyzptlk/db231222/G/G;->c:Ldbxyzptlk/db231222/G/G;

    .line 331
    const/4 v0, 0x3

    new-array v0, v0, [Ldbxyzptlk/db231222/G/G;

    sget-object v1, Ldbxyzptlk/db231222/G/G;->a:Ldbxyzptlk/db231222/G/G;

    aput-object v1, v0, v2

    sget-object v1, Ldbxyzptlk/db231222/G/G;->b:Ldbxyzptlk/db231222/G/G;

    aput-object v1, v0, v3

    sget-object v1, Ldbxyzptlk/db231222/G/G;->c:Ldbxyzptlk/db231222/G/G;

    aput-object v1, v0, v4

    sput-object v0, Ldbxyzptlk/db231222/G/G;->d:[Ldbxyzptlk/db231222/G/G;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 331
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILdbxyzptlk/db231222/G/u;)V
    .locals 0

    .prologue
    .line 331
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/G/G;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldbxyzptlk/db231222/G/G;
    .locals 1

    .prologue
    .line 331
    const-class v0, Ldbxyzptlk/db231222/G/G;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/G/G;

    return-object v0
.end method

.method public static values()[Ldbxyzptlk/db231222/G/G;
    .locals 1

    .prologue
    .line 331
    sget-object v0, Ldbxyzptlk/db231222/G/G;->d:[Ldbxyzptlk/db231222/G/G;

    invoke-virtual {v0}, [Ldbxyzptlk/db231222/G/G;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/G/G;

    return-object v0
.end method


# virtual methods
.method abstract a(Ljava/lang/Object;)I
.end method

.method abstract a(Ldbxyzptlk/db231222/G/z;Ljava/lang/Object;)Ldbxyzptlk/db231222/G/M;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ldbxyzptlk/db231222/G/z",
            "<TK;TV;>;TV;)",
            "Ldbxyzptlk/db231222/G/M",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method abstract a(Ldbxyzptlk/db231222/G/q;Ljava/lang/Object;ILdbxyzptlk/db231222/G/z;)Ldbxyzptlk/db231222/G/z;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ldbxyzptlk/db231222/G/q",
            "<TK;TV;",
            "Ldbxyzptlk/db231222/G/z",
            "<TK;TV;>;>;TK;I",
            "Ldbxyzptlk/db231222/G/z",
            "<TK;TV;>;)",
            "Ldbxyzptlk/db231222/G/z",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method abstract a(Ljava/lang/Object;Ljava/lang/Object;)Z
.end method
