.class public final Ldbxyzptlk/db231222/G/t;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final f:Ldbxyzptlk/db231222/G/M;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/G/M",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ldbxyzptlk/db231222/G/G;

.field private b:Ldbxyzptlk/db231222/G/G;

.field private c:J

.field private d:Z

.field private final e:Ldbxyzptlk/db231222/G/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 751
    new-instance v0, Ldbxyzptlk/db231222/G/u;

    invoke-direct {v0}, Ldbxyzptlk/db231222/G/u;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/G/t;->f:Ldbxyzptlk/db231222/G/M;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    sget-object v0, Ldbxyzptlk/db231222/G/G;->c:Ldbxyzptlk/db231222/G/G;

    iput-object v0, p0, Ldbxyzptlk/db231222/G/t;->a:Ldbxyzptlk/db231222/G/G;

    .line 91
    sget-object v0, Ldbxyzptlk/db231222/G/G;->c:Ldbxyzptlk/db231222/G/G;

    iput-object v0, p0, Ldbxyzptlk/db231222/G/t;->b:Ldbxyzptlk/db231222/G/G;

    .line 92
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ldbxyzptlk/db231222/G/t;->c:J

    .line 94
    new-instance v0, Ldbxyzptlk/db231222/G/c;

    invoke-direct {v0}, Ldbxyzptlk/db231222/G/c;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/G/t;->e:Ldbxyzptlk/db231222/G/c;

    .line 101
    return-void
.end method

.method static synthetic a(Ldbxyzptlk/db231222/G/t;)Ldbxyzptlk/db231222/G/G;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Ldbxyzptlk/db231222/G/t;->a:Ldbxyzptlk/db231222/G/G;

    return-object v0
.end method

.method private a(Ldbxyzptlk/db231222/G/G;)Ldbxyzptlk/db231222/G/t;
    .locals 3

    .prologue
    .line 232
    iget-object v0, p0, Ldbxyzptlk/db231222/G/t;->b:Ldbxyzptlk/db231222/G/G;

    sget-object v1, Ldbxyzptlk/db231222/G/G;->c:Ldbxyzptlk/db231222/G/G;

    if-eq v0, v1, :cond_0

    .line 233
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Value strength was already set to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/db231222/G/t;->b:Ldbxyzptlk/db231222/G/G;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 236
    :cond_0
    iput-object p1, p0, Ldbxyzptlk/db231222/G/t;->b:Ldbxyzptlk/db231222/G/G;

    .line 237
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/db231222/G/t;->d:Z

    .line 238
    return-object p0
.end method

.method static synthetic b(Ldbxyzptlk/db231222/G/t;)Ldbxyzptlk/db231222/G/G;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Ldbxyzptlk/db231222/G/t;->b:Ldbxyzptlk/db231222/G/G;

    return-object v0
.end method

.method static synthetic c(Ldbxyzptlk/db231222/G/t;)J
    .locals 2

    .prologue
    .line 89
    iget-wide v0, p0, Ldbxyzptlk/db231222/G/t;->c:J

    return-wide v0
.end method

.method static synthetic c()Ldbxyzptlk/db231222/G/M;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Ldbxyzptlk/db231222/G/t;->f:Ldbxyzptlk/db231222/G/M;

    return-object v0
.end method

.method static synthetic d()Ldbxyzptlk/db231222/G/M;
    .locals 1

    .prologue
    .line 89
    invoke-static {}, Ldbxyzptlk/db231222/G/t;->e()Ldbxyzptlk/db231222/G/M;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Ldbxyzptlk/db231222/G/t;)Ldbxyzptlk/db231222/G/c;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Ldbxyzptlk/db231222/G/t;->e:Ldbxyzptlk/db231222/G/c;

    return-object v0
.end method

.method private static e()Ldbxyzptlk/db231222/G/M;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Ldbxyzptlk/db231222/G/M",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 771
    sget-object v0, Ldbxyzptlk/db231222/G/t;->f:Ldbxyzptlk/db231222/G/M;

    return-object v0
.end method


# virtual methods
.method public final a()Ldbxyzptlk/db231222/G/t;
    .locals 1

    .prologue
    .line 206
    sget-object v0, Ldbxyzptlk/db231222/G/G;->a:Ldbxyzptlk/db231222/G/G;

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/G/t;->a(Ldbxyzptlk/db231222/G/G;)Ldbxyzptlk/db231222/G/t;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Ldbxyzptlk/db231222/G/t;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Ldbxyzptlk/db231222/G/t;->e:Ldbxyzptlk/db231222/G/c;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/G/c;->a(I)Ldbxyzptlk/db231222/G/c;

    .line 115
    return-object p0
.end method

.method public final b(I)Ldbxyzptlk/db231222/G/t;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Ldbxyzptlk/db231222/G/t;->e:Ldbxyzptlk/db231222/G/c;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/G/c;->b(I)Ldbxyzptlk/db231222/G/c;

    .line 139
    return-object p0
.end method

.method public final b()Ljava/util/concurrent/ConcurrentMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/util/concurrent/ConcurrentMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 280
    iget-boolean v0, p0, Ldbxyzptlk/db231222/G/t;->d:Z

    if-eqz v0, :cond_0

    new-instance v0, Ldbxyzptlk/db231222/G/C;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/G/C;-><init>(Ldbxyzptlk/db231222/G/t;)V

    iget-object v0, v0, Ldbxyzptlk/db231222/G/C;->c:Ljava/util/concurrent/ConcurrentMap;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v1, p0, Ldbxyzptlk/db231222/G/t;->e:Ldbxyzptlk/db231222/G/c;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/G/c;->a()I

    move-result v1

    const/high16 v2, 0x3f400000    # 0.75f

    iget-object v3, p0, Ldbxyzptlk/db231222/G/t;->e:Ldbxyzptlk/db231222/G/c;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/G/c;->b()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    goto :goto_0
.end method
