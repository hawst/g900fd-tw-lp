.class final Ldbxyzptlk/db231222/G/l;
.super Ljava/util/AbstractSet;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractSet",
        "<TK;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ldbxyzptlk/db231222/G/e;


# direct methods
.method constructor <init>(Ldbxyzptlk/db231222/G/e;)V
    .locals 0

    .prologue
    .line 1840
    iput-object p1, p0, Ldbxyzptlk/db231222/G/l;->a:Ldbxyzptlk/db231222/G/e;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 1

    .prologue
    .line 1869
    iget-object v0, p0, Ldbxyzptlk/db231222/G/l;->a:Ldbxyzptlk/db231222/G/e;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/G/e;->clear()V

    .line 1870
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1859
    iget-object v0, p0, Ldbxyzptlk/db231222/G/l;->a:Ldbxyzptlk/db231222/G/e;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/G/e;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 1854
    iget-object v0, p0, Ldbxyzptlk/db231222/G/l;->a:Ldbxyzptlk/db231222/G/e;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/G/e;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 1844
    new-instance v0, Ldbxyzptlk/db231222/G/k;

    iget-object v1, p0, Ldbxyzptlk/db231222/G/l;->a:Ldbxyzptlk/db231222/G/e;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/G/k;-><init>(Ldbxyzptlk/db231222/G/e;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1864
    iget-object v0, p0, Ldbxyzptlk/db231222/G/l;->a:Ldbxyzptlk/db231222/G/e;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/G/e;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1849
    iget-object v0, p0, Ldbxyzptlk/db231222/G/l;->a:Ldbxyzptlk/db231222/G/e;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/G/e;->size()I

    move-result v0

    return v0
.end method
