.class Ldbxyzptlk/db231222/G/C;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/G/d;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ldbxyzptlk/db231222/G/d",
        "<TK;TV;",
        "Ldbxyzptlk/db231222/G/z",
        "<TK;TV;>;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field final a:Ldbxyzptlk/db231222/G/G;

.field final b:Ldbxyzptlk/db231222/G/G;

.field final c:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final d:J

.field e:Ldbxyzptlk/db231222/G/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/G/q",
            "<TK;TV;",
            "Ldbxyzptlk/db231222/G/z",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ldbxyzptlk/db231222/G/t;)V
    .locals 2

    .prologue
    .line 468
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 469
    invoke-static {p1}, Ldbxyzptlk/db231222/G/t;->a(Ldbxyzptlk/db231222/G/t;)Ldbxyzptlk/db231222/G/G;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/G/C;->a:Ldbxyzptlk/db231222/G/G;

    .line 470
    invoke-static {p1}, Ldbxyzptlk/db231222/G/t;->b(Ldbxyzptlk/db231222/G/t;)Ldbxyzptlk/db231222/G/G;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/G/C;->b:Ldbxyzptlk/db231222/G/G;

    .line 471
    invoke-static {p1}, Ldbxyzptlk/db231222/G/t;->c(Ldbxyzptlk/db231222/G/t;)J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/G/C;->d:J

    .line 473
    invoke-static {p1}, Ldbxyzptlk/db231222/G/t;->d(Ldbxyzptlk/db231222/G/t;)Ldbxyzptlk/db231222/G/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Ldbxyzptlk/db231222/G/c;->a(Ldbxyzptlk/db231222/G/r;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/G/C;->c:Ljava/util/concurrent/ConcurrentMap;

    .line 474
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 3

    .prologue
    .line 722
    :try_start_0
    sget-object v0, Ldbxyzptlk/db231222/G/E;->a:Ljava/lang/reflect/Field;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 723
    sget-object v0, Ldbxyzptlk/db231222/G/E;->b:Ljava/lang/reflect/Field;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 724
    sget-object v0, Ldbxyzptlk/db231222/G/E;->c:Ljava/lang/reflect/Field;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 725
    sget-object v0, Ldbxyzptlk/db231222/G/E;->d:Ljava/lang/reflect/Field;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 726
    sget-object v0, Ldbxyzptlk/db231222/G/E;->e:Ljava/lang/reflect/Field;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 730
    return-void

    .line 727
    :catch_0
    move-exception v0

    .line 728
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 2

    .prologue
    .line 685
    iget-object v0, p0, Ldbxyzptlk/db231222/G/C;->a:Ldbxyzptlk/db231222/G/G;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 686
    iget-object v0, p0, Ldbxyzptlk/db231222/G/C;->b:Ldbxyzptlk/db231222/G/G;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 687
    iget-wide v0, p0, Ldbxyzptlk/db231222/G/C;->d:J

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    .line 693
    iget-object v0, p0, Ldbxyzptlk/db231222/G/C;->e:Ldbxyzptlk/db231222/G/q;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 694
    iget-object v0, p0, Ldbxyzptlk/db231222/G/C;->c:Ljava/util/concurrent/ConcurrentMap;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 695
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;ILdbxyzptlk/db231222/G/z;)Ldbxyzptlk/db231222/G/z;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "Ldbxyzptlk/db231222/G/z",
            "<TK;TV;>;)",
            "Ldbxyzptlk/db231222/G/z",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 535
    iget-object v0, p0, Ldbxyzptlk/db231222/G/C;->a:Ldbxyzptlk/db231222/G/G;

    iget-object v1, p0, Ldbxyzptlk/db231222/G/C;->e:Ldbxyzptlk/db231222/G/q;

    invoke-virtual {v0, v1, p1, p2, p3}, Ldbxyzptlk/db231222/G/G;->a(Ldbxyzptlk/db231222/G/q;Ljava/lang/Object;ILdbxyzptlk/db231222/G/z;)Ldbxyzptlk/db231222/G/z;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ldbxyzptlk/db231222/G/z;Ldbxyzptlk/db231222/G/z;)Ldbxyzptlk/db231222/G/z;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ldbxyzptlk/db231222/G/z",
            "<TK;TV;>;",
            "Ldbxyzptlk/db231222/G/z",
            "<TK;TV;>;)",
            "Ldbxyzptlk/db231222/G/z",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 540
    invoke-interface {p2}, Ldbxyzptlk/db231222/G/z;->c()Ldbxyzptlk/db231222/G/M;

    move-result-object v1

    .line 541
    invoke-static {}, Ldbxyzptlk/db231222/G/t;->c()Ldbxyzptlk/db231222/G/M;

    move-result-object v0

    if-ne v1, v0, :cond_0

    .line 542
    invoke-interface {p2}, Ldbxyzptlk/db231222/G/z;->e()I

    move-result v0

    invoke-virtual {p0, p1, v0, p3}, Ldbxyzptlk/db231222/G/C;->a(Ljava/lang/Object;ILdbxyzptlk/db231222/G/z;)Ldbxyzptlk/db231222/G/z;

    move-result-object v0

    .line 543
    new-instance v1, Ldbxyzptlk/db231222/G/F;

    invoke-direct {v1, p0, p2, v0}, Ldbxyzptlk/db231222/G/F;-><init>(Ldbxyzptlk/db231222/G/C;Ldbxyzptlk/db231222/G/z;Ldbxyzptlk/db231222/G/z;)V

    invoke-interface {v0, v1}, Ldbxyzptlk/db231222/G/z;->a(Ldbxyzptlk/db231222/G/M;)V

    .line 548
    :goto_0
    return-object v0

    .line 546
    :cond_0
    invoke-interface {p2}, Ldbxyzptlk/db231222/G/z;->e()I

    move-result v0

    invoke-virtual {p0, p1, v0, p3}, Ldbxyzptlk/db231222/G/C;->a(Ljava/lang/Object;ILdbxyzptlk/db231222/G/z;)Ldbxyzptlk/db231222/G/z;

    move-result-object v0

    .line 547
    invoke-interface {v1, v0}, Ldbxyzptlk/db231222/G/M;->a(Ldbxyzptlk/db231222/G/z;)Ldbxyzptlk/db231222/G/M;

    move-result-object v1

    invoke-interface {v0, v1}, Ldbxyzptlk/db231222/G/z;->a(Ldbxyzptlk/db231222/G/M;)V

    goto :goto_0
.end method

.method public final a(Ldbxyzptlk/db231222/G/z;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/G/z",
            "<TK;TV;>;)TK;"
        }
    .end annotation

    .prologue
    .line 527
    invoke-interface {p1}, Ldbxyzptlk/db231222/G/z;->f()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 460
    check-cast p1, Ldbxyzptlk/db231222/G/z;

    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/G/C;->c(Ldbxyzptlk/db231222/G/z;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 460
    check-cast p3, Ldbxyzptlk/db231222/G/z;

    invoke-virtual {p0, p1, p2, p3}, Ldbxyzptlk/db231222/G/C;->a(Ljava/lang/Object;ILdbxyzptlk/db231222/G/z;)Ldbxyzptlk/db231222/G/z;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 460
    check-cast p2, Ldbxyzptlk/db231222/G/z;

    check-cast p3, Ldbxyzptlk/db231222/G/z;

    invoke-virtual {p0, p1, p2, p3}, Ldbxyzptlk/db231222/G/C;->a(Ljava/lang/Object;Ldbxyzptlk/db231222/G/z;Ldbxyzptlk/db231222/G/z;)Ldbxyzptlk/db231222/G/z;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/G/q;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/G/q",
            "<TK;TV;",
            "Ldbxyzptlk/db231222/G/z",
            "<TK;TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 676
    iput-object p1, p0, Ldbxyzptlk/db231222/G/C;->e:Ldbxyzptlk/db231222/G/q;

    .line 677
    return-void
.end method

.method final a(Ldbxyzptlk/db231222/G/z;Ldbxyzptlk/db231222/G/M;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/G/z",
            "<TK;TV;>;",
            "Ldbxyzptlk/db231222/G/M",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 607
    invoke-interface {p1}, Ldbxyzptlk/db231222/G/z;->c()Ldbxyzptlk/db231222/G/M;

    move-result-object v0

    invoke-static {}, Ldbxyzptlk/db231222/G/t;->c()Ldbxyzptlk/db231222/G/M;

    move-result-object v1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    .line 608
    :goto_0
    invoke-interface {p1, p2}, Ldbxyzptlk/db231222/G/z;->a(Ldbxyzptlk/db231222/G/M;)V

    .line 609
    if-eqz v0, :cond_0

    .line 610
    monitor-enter p1

    .line 611
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->notifyAll()V

    .line 612
    monitor-exit p1

    .line 614
    :cond_0
    return-void

    .line 607
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 612
    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ldbxyzptlk/db231222/G/z;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/G/z",
            "<TK;TV;>;TV;)V"
        }
    .end annotation

    .prologue
    .line 485
    iget-object v0, p0, Ldbxyzptlk/db231222/G/C;->b:Ldbxyzptlk/db231222/G/G;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/G/G;->a(Ldbxyzptlk/db231222/G/z;Ljava/lang/Object;)Ldbxyzptlk/db231222/G/M;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ldbxyzptlk/db231222/G/C;->a(Ldbxyzptlk/db231222/G/z;Ldbxyzptlk/db231222/G/M;)V

    .line 486
    iget-wide v0, p0, Ldbxyzptlk/db231222/G/C;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 487
    invoke-interface {p1}, Ldbxyzptlk/db231222/G/z;->f()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Ldbxyzptlk/db231222/G/C;->d(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 489
    :cond_0
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 460
    check-cast p1, Ldbxyzptlk/db231222/G/z;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/G/C;->a(Ldbxyzptlk/db231222/G/z;Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Ldbxyzptlk/db231222/G/z;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/G/z",
            "<TK;TV;>;)I"
        }
    .end annotation

    .prologue
    .line 531
    invoke-interface {p1}, Ldbxyzptlk/db231222/G/z;->e()I

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 523
    iget-object v0, p0, Ldbxyzptlk/db231222/G/C;->a:Ldbxyzptlk/db231222/G/G;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/G/G;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/lang/Object;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 515
    iget-object v0, p0, Ldbxyzptlk/db231222/G/C;->a:Ldbxyzptlk/db231222/G/G;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/G/G;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c(Ldbxyzptlk/db231222/G/z;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/G/z",
            "<TK;TV;>;)TV;"
        }
    .end annotation

    .prologue
    .line 573
    invoke-interface {p1}, Ldbxyzptlk/db231222/G/z;->c()Ldbxyzptlk/db231222/G/M;

    move-result-object v0

    .line 574
    invoke-interface {v0}, Ldbxyzptlk/db231222/G/M;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 460
    check-cast p1, Ldbxyzptlk/db231222/G/z;

    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/G/C;->a(Ldbxyzptlk/db231222/G/z;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;",
            "Ljava/lang/Object;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 519
    iget-object v0, p0, Ldbxyzptlk/db231222/G/C;->b:Ldbxyzptlk/db231222/G/G;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/G/G;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d(Ldbxyzptlk/db231222/G/z;)Ldbxyzptlk/db231222/G/z;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/G/z",
            "<TK;TV;>;)",
            "Ldbxyzptlk/db231222/G/z",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 672
    invoke-interface {p1}, Ldbxyzptlk/db231222/G/z;->b()Ldbxyzptlk/db231222/G/z;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic d(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 460
    check-cast p1, Ldbxyzptlk/db231222/G/z;

    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/G/C;->d(Ldbxyzptlk/db231222/G/z;)Ldbxyzptlk/db231222/G/z;

    move-result-object v0

    return-object v0
.end method

.method final d(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 500
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 501
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 502
    sget-object v2, Ldbxyzptlk/db231222/G/s;->a:Ljava/util/Timer;

    new-instance v3, Ldbxyzptlk/db231222/G/D;

    invoke-direct {v3, p0, v0, v1}, Ldbxyzptlk/db231222/G/D;-><init>(Ldbxyzptlk/db231222/G/C;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V

    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v4, p0, Ldbxyzptlk/db231222/G/C;->d:J

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    invoke-virtual {v2, v3, v0, v1}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 512
    return-void
.end method

.method public final synthetic e(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 460
    check-cast p1, Ldbxyzptlk/db231222/G/z;

    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/G/C;->b(Ldbxyzptlk/db231222/G/z;)I

    move-result v0

    return v0
.end method
