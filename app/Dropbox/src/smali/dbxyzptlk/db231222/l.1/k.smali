.class public final Ldbxyzptlk/db231222/l/k;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Key:",
        "Ljava/lang/Object;",
        "SuccessResult:",
        "Ljava/lang/Object;",
        "ErrorResult:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TKey;>;"
        }
    .end annotation
.end field

.field private b:Ldbxyzptlk/db231222/l/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/l/l",
            "<TKey;TSuccessResult;TErrorResult;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TKey;",
            "Ldbxyzptlk/db231222/l/d",
            "<TKey;TSuccessResult;TErrorResult;>;>;"
        }
    .end annotation
.end field

.field private final d:Z


# direct methods
.method private constructor <init>(Z)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/l/k;->a:Ljava/util/Set;

    .line 53
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/l/k;->c:Ljava/util/Map;

    .line 58
    iput-boolean p1, p0, Ldbxyzptlk/db231222/l/k;->d:Z

    .line 59
    return-void
.end method

.method public static a()Ldbxyzptlk/db231222/l/k;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Key:",
            "Ljava/lang/Object;",
            "SuccessResult:",
            "Ljava/lang/Object;",
            "ErrorResult:",
            "Ljava/lang/Object;",
            ">()",
            "Ldbxyzptlk/db231222/l/k",
            "<TKey;TSuccessResult;TErrorResult;>;"
        }
    .end annotation

    .prologue
    .line 67
    new-instance v0, Ldbxyzptlk/db231222/l/k;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/l/k;-><init>(Z)V

    return-object v0
.end method

.method public static b()Ldbxyzptlk/db231222/l/k;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Key:",
            "Ljava/lang/Object;",
            "SuccessResult:",
            "Ljava/lang/Object;",
            "ErrorResult:",
            "Ljava/lang/Object;",
            ">()",
            "Ldbxyzptlk/db231222/l/k",
            "<TKey;TSuccessResult;TErrorResult;>;"
        }
    .end annotation

    .prologue
    .line 75
    new-instance v0, Ldbxyzptlk/db231222/l/k;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/l/k;-><init>(Z)V

    return-object v0
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/l/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/l/a",
            "<TKey;TSuccessResult;TErrorResult;>;)V"
        }
    .end annotation

    .prologue
    .line 116
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 117
    iget-object v0, p0, Ldbxyzptlk/db231222/l/k;->a:Ljava/util/Set;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/l/a;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 118
    invoke-virtual {p1, p0}, Ldbxyzptlk/db231222/l/a;->a(Ldbxyzptlk/db231222/l/k;)V

    .line 119
    iget-object v0, p0, Ldbxyzptlk/db231222/l/k;->b:Ldbxyzptlk/db231222/l/l;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Ldbxyzptlk/db231222/l/k;->b:Ldbxyzptlk/db231222/l/l;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/l/a;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ldbxyzptlk/db231222/l/l;->a(Ljava/lang/Object;)V

    .line 122
    :cond_0
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/l/l;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/l/l",
            "<TKey;TSuccessResult;TErrorResult;>;)V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 90
    iget-object v0, p0, Ldbxyzptlk/db231222/l/k;->b:Ldbxyzptlk/db231222/l/l;

    const-string v1, "listener already registered"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/C;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    iput-object p1, p0, Ldbxyzptlk/db231222/l/k;->b:Ldbxyzptlk/db231222/l/l;

    .line 92
    iget-object v0, p0, Ldbxyzptlk/db231222/l/k;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 93
    invoke-interface {p1, v1}, Ldbxyzptlk/db231222/l/l;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 95
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/l/k;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 96
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldbxyzptlk/db231222/l/d;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0, p1}, Ldbxyzptlk/db231222/l/d;->a(Ljava/lang/Object;Ldbxyzptlk/db231222/l/l;)V

    goto :goto_1

    .line 98
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/l/k;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 99
    return-void
.end method

.method final a(Ljava/lang/Object;Ldbxyzptlk/db231222/l/d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;",
            "Ldbxyzptlk/db231222/l/d",
            "<TKey;TSuccessResult;TErrorResult;>;)V"
        }
    .end annotation

    .prologue
    .line 142
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 143
    iget-object v0, p0, Ldbxyzptlk/db231222/l/k;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 150
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Class;)V

    .line 152
    iget-object v0, p0, Ldbxyzptlk/db231222/l/k;->b:Ldbxyzptlk/db231222/l/l;

    if-eqz v0, :cond_1

    .line 153
    iget-object v0, p0, Ldbxyzptlk/db231222/l/k;->b:Ldbxyzptlk/db231222/l/l;

    invoke-interface {p2, p1, v0}, Ldbxyzptlk/db231222/l/d;->a(Ljava/lang/Object;Ldbxyzptlk/db231222/l/l;)V

    .line 159
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    iget-boolean v0, p0, Ldbxyzptlk/db231222/l/k;->d:Z

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Ldbxyzptlk/db231222/l/k;->c:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;)Z"
        }
    .end annotation

    .prologue
    .line 129
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 130
    iget-object v0, p0, Ldbxyzptlk/db231222/l/k;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Ldbxyzptlk/db231222/l/l;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/l/l",
            "<TKey;TSuccessResult;TErrorResult;>;)V"
        }
    .end annotation

    .prologue
    .line 102
    invoke-static {}, Lcom/dropbox/android/util/C;->a()V

    .line 103
    iget-object v0, p0, Ldbxyzptlk/db231222/l/k;->b:Ldbxyzptlk/db231222/l/l;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "listener not registered"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/C;->a(ZLjava/lang/String;)V

    .line 104
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/l/k;->b:Ldbxyzptlk/db231222/l/l;

    .line 105
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Ldbxyzptlk/db231222/l/k;->d:Z

    return v0
.end method
