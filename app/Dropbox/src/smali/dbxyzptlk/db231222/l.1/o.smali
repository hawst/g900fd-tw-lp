.class public final Ldbxyzptlk/db231222/l/o;
.super Ldbxyzptlk/db231222/l/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/l/a",
        "<",
        "Lcom/dropbox/android/notifications/NotificationKey;",
        "Ljava/lang/Void;",
        "Ldbxyzptlk/db231222/m/a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:Lcom/dropbox/sync/android/aJ;

.field private final c:Ldbxyzptlk/db231222/z/M;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/notifications/NotificationKey;JLcom/dropbox/sync/android/aJ;Ldbxyzptlk/db231222/z/M;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/l/a;-><init>(Ljava/lang/Object;)V

    .line 32
    iput-wide p2, p0, Ldbxyzptlk/db231222/l/o;->a:J

    .line 33
    iput-object p4, p0, Ldbxyzptlk/db231222/l/o;->b:Lcom/dropbox/sync/android/aJ;

    .line 34
    iput-object p5, p0, Ldbxyzptlk/db231222/l/o;->c:Ldbxyzptlk/db231222/z/M;

    .line 35
    return-void
.end method


# virtual methods
.method protected final a()Ldbxyzptlk/db231222/l/d;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldbxyzptlk/db231222/l/d",
            "<",
            "Lcom/dropbox/android/notifications/NotificationKey;",
            "Ljava/lang/Void;",
            "Ldbxyzptlk/db231222/m/a;",
            ">;"
        }
    .end annotation

    .prologue
    const v6, 0x7f0d005d

    const/4 v5, 0x0

    .line 39
    const-string v0, "decline.start"

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->d(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "invite_id"

    iget-wide v2, p0, Ldbxyzptlk/db231222/l/o;->a:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 44
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/l/o;->c:Ldbxyzptlk/db231222/z/M;

    iget-wide v1, p0, Ldbxyzptlk/db231222/l/o;->a:J

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/z/M;->b(J)V
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_3

    .line 67
    const-string v0, "decline.success"

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->d(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "invite_id"

    iget-wide v2, p0, Ldbxyzptlk/db231222/l/o;->a:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 78
    :try_start_1
    iget-object v0, p0, Ldbxyzptlk/db231222/l/o;->b:Lcom/dropbox/sync/android/aJ;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/aJ;->f()V
    :try_end_1
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_1 .. :try_end_1} :catch_4

    .line 81
    :goto_0
    invoke-static {v5}, Ldbxyzptlk/db231222/l/e;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/e;

    move-result-object v0

    :goto_1
    return-object v0

    .line 45
    :catch_0
    move-exception v0

    .line 46
    const-string v0, "decline.error.io"

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->d(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 47
    const v0, 0x7f0d005b

    invoke-static {v0, v5}, Ldbxyzptlk/db231222/m/b;->b(ILjava/lang/String;)Ldbxyzptlk/db231222/m/a;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto :goto_1

    .line 48
    :catch_1
    move-exception v0

    .line 49
    const-string v1, "decline.error.server"

    invoke-static {v1}, Lcom/dropbox/android/util/analytics/a;->d(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "code"

    iget v3, v0, Ldbxyzptlk/db231222/w/i;->b:I

    int-to-long v3, v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 50
    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x19a

    if-ne v1, v2, :cond_0

    .line 52
    invoke-virtual {v0, v5}, Ldbxyzptlk/db231222/w/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Ldbxyzptlk/db231222/m/b;->a(ILjava/lang/String;)Ldbxyzptlk/db231222/m/a;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto :goto_1

    .line 53
    :cond_0
    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x1f4

    if-lt v1, v2, :cond_1

    .line 54
    const v1, 0x7f0d005c

    invoke-virtual {v0, v5}, Ldbxyzptlk/db231222/w/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/db231222/m/b;->b(ILjava/lang/String;)Ldbxyzptlk/db231222/m/a;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto :goto_1

    .line 56
    :cond_1
    invoke-virtual {v0, v5}, Ldbxyzptlk/db231222/w/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Ldbxyzptlk/db231222/m/b;->b(ILjava/lang/String;)Ldbxyzptlk/db231222/m/a;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto :goto_1

    .line 58
    :catch_2
    move-exception v0

    .line 59
    const-string v0, "decline.error.unlinked"

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->d(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 61
    new-instance v0, Ldbxyzptlk/db231222/m/c;

    invoke-direct {v0}, Ldbxyzptlk/db231222/m/c;-><init>()V

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto :goto_1

    .line 62
    :catch_3
    move-exception v0

    .line 63
    const-string v1, "decline.error.unknown"

    invoke-static {v1}, Lcom/dropbox/android/util/analytics/a;->d(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "err"

    invoke-virtual {v0}, Ldbxyzptlk/db231222/w/a;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 64
    invoke-static {v6, v5}, Ldbxyzptlk/db231222/m/b;->b(ILjava/lang/String;)Ldbxyzptlk/db231222/m/a;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto/16 :goto_1

    .line 79
    :catch_4
    move-exception v0

    goto/16 :goto_0
.end method
