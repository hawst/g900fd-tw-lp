.class public abstract Ldbxyzptlk/db231222/l/a;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Key:",
        "Ljava/lang/Object;",
        "SuccessResult:",
        "Ljava/lang/Object;",
        "ErrorResult:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TKey;"
        }
    .end annotation
.end field

.field private b:Ldbxyzptlk/db231222/l/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/l/k",
            "<TKey;TSuccessResult;TErrorResult;>;"
        }
    .end annotation
.end field

.field private final c:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ldbxyzptlk/db231222/l/d",
            "<TKey;TSuccessResult;TErrorResult;>;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;)V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ldbxyzptlk/db231222/l/b;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/l/b;-><init>(Ldbxyzptlk/db231222/l/a;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/l/a;->c:Landroid/os/AsyncTask;

    .line 44
    iput-object p1, p0, Ldbxyzptlk/db231222/l/a;->a:Ljava/lang/Object;

    .line 50
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/Class;)V

    .line 51
    return-void
.end method

.method static synthetic a(Ldbxyzptlk/db231222/l/a;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Ldbxyzptlk/db231222/l/a;->a:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Ldbxyzptlk/db231222/l/a;)Ldbxyzptlk/db231222/l/k;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Ldbxyzptlk/db231222/l/a;->b:Ldbxyzptlk/db231222/l/k;

    return-object v0
.end method


# virtual methods
.method protected abstract a()Ldbxyzptlk/db231222/l/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldbxyzptlk/db231222/l/d",
            "<TKey;TSuccessResult;TErrorResult;>;"
        }
    .end annotation
.end method

.method final a(Ldbxyzptlk/db231222/l/k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/l/k",
            "<TKey;TSuccessResult;TErrorResult;>;)V"
        }
    .end annotation

    .prologue
    .line 71
    iput-object p1, p0, Ldbxyzptlk/db231222/l/a;->b:Ldbxyzptlk/db231222/l/k;

    .line 72
    iget-object v0, p0, Ldbxyzptlk/db231222/l/a;->c:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 73
    return-void
.end method

.method final b()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TKey;"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Ldbxyzptlk/db231222/l/a;->a:Ljava/lang/Object;

    return-object v0
.end method
