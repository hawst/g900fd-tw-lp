.class public final Ldbxyzptlk/db231222/l/n;
.super Ldbxyzptlk/db231222/l/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/l/a",
        "<",
        "Lcom/dropbox/android/notifications/NotificationKey;",
        "Ljava/lang/Void;",
        "Ldbxyzptlk/db231222/m/a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:Lcom/dropbox/android/provider/MetadataManager;

.field private final c:Lcom/dropbox/sync/android/aJ;

.field private final d:Ldbxyzptlk/db231222/z/M;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/notifications/NotificationKey;JLcom/dropbox/android/provider/MetadataManager;Lcom/dropbox/sync/android/aJ;Ldbxyzptlk/db231222/z/M;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/l/a;-><init>(Ljava/lang/Object;)V

    .line 37
    iput-wide p2, p0, Ldbxyzptlk/db231222/l/n;->a:J

    .line 38
    iput-object p4, p0, Ldbxyzptlk/db231222/l/n;->b:Lcom/dropbox/android/provider/MetadataManager;

    .line 39
    iput-object p5, p0, Ldbxyzptlk/db231222/l/n;->c:Lcom/dropbox/sync/android/aJ;

    .line 40
    iput-object p6, p0, Ldbxyzptlk/db231222/l/n;->d:Ldbxyzptlk/db231222/z/M;

    .line 41
    return-void
.end method


# virtual methods
.method protected final a()Ldbxyzptlk/db231222/l/d;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldbxyzptlk/db231222/l/d",
            "<",
            "Lcom/dropbox/android/notifications/NotificationKey;",
            "Ljava/lang/Void;",
            "Ldbxyzptlk/db231222/m/a;",
            ">;"
        }
    .end annotation

    .prologue
    const v6, 0x7f0d005d

    const/4 v5, 0x0

    .line 45
    const-string v0, "accept.start"

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->d(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "invite_id"

    iget-wide v2, p0, Ldbxyzptlk/db231222/l/n;->a:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 51
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/l/n;->d:Ldbxyzptlk/db231222/z/M;

    iget-wide v1, p0, Ldbxyzptlk/db231222/l/n;->a:J

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/z/M;->a(J)Ldbxyzptlk/db231222/v/j;
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    .line 81
    const-string v1, "accept.success"

    invoke-static {v1}, Lcom/dropbox/android/util/analytics/a;->d(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "invite_id"

    iget-wide v3, p0, Ldbxyzptlk/db231222/l/n;->a:J

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 85
    iget-boolean v1, v0, Ldbxyzptlk/db231222/v/j;->d:Z

    invoke-static {v1}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 87
    iget-object v1, p0, Ldbxyzptlk/db231222/l/n;->b:Lcom/dropbox/android/provider/MetadataManager;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/provider/MetadataManager;->a(Ldbxyzptlk/db231222/v/j;)V

    .line 96
    :try_start_1
    iget-object v0, p0, Ldbxyzptlk/db231222/l/n;->c:Lcom/dropbox/sync/android/aJ;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/aJ;->f()V
    :try_end_1
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_1 .. :try_end_1} :catch_4

    .line 99
    :goto_0
    invoke-static {v5}, Ldbxyzptlk/db231222/l/e;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/e;

    move-result-object v0

    :goto_1
    return-object v0

    .line 52
    :catch_0
    move-exception v0

    .line 53
    const-string v0, "accept.error.io"

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->d(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 54
    const v0, 0x7f0d005b

    invoke-static {v0, v5}, Ldbxyzptlk/db231222/m/b;->b(ILjava/lang/String;)Ldbxyzptlk/db231222/m/a;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto :goto_1

    .line 55
    :catch_1
    move-exception v0

    .line 56
    const-string v1, "accept.error.server"

    invoke-static {v1}, Lcom/dropbox/android/util/analytics/a;->d(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "code"

    iget v3, v0, Ldbxyzptlk/db231222/w/i;->b:I

    int-to-long v3, v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 57
    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x199

    if-ne v1, v2, :cond_0

    .line 59
    invoke-virtual {v0, v5}, Ldbxyzptlk/db231222/w/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Ldbxyzptlk/db231222/m/b;->b(ILjava/lang/String;)Ldbxyzptlk/db231222/m/a;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto :goto_1

    .line 60
    :cond_0
    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x19a

    if-eq v1, v2, :cond_1

    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x1a1

    if-ne v1, v2, :cond_2

    .line 63
    :cond_1
    invoke-virtual {v0, v5}, Ldbxyzptlk/db231222/w/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Ldbxyzptlk/db231222/m/b;->a(ILjava/lang/String;)Ldbxyzptlk/db231222/m/a;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto :goto_1

    .line 64
    :cond_2
    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x19c

    if-ne v1, v2, :cond_3

    .line 66
    invoke-virtual {v0, v5}, Ldbxyzptlk/db231222/w/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Ldbxyzptlk/db231222/m/b;->a(ILjava/lang/String;)Ldbxyzptlk/db231222/m/a;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto :goto_1

    .line 67
    :cond_3
    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x1f4

    if-lt v1, v2, :cond_4

    .line 68
    const v1, 0x7f0d005c

    invoke-virtual {v0, v5}, Ldbxyzptlk/db231222/w/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/db231222/m/b;->b(ILjava/lang/String;)Ldbxyzptlk/db231222/m/a;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto :goto_1

    .line 70
    :cond_4
    invoke-virtual {v0, v5}, Ldbxyzptlk/db231222/w/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Ldbxyzptlk/db231222/m/b;->b(ILjava/lang/String;)Ldbxyzptlk/db231222/m/a;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto/16 :goto_1

    .line 72
    :catch_2
    move-exception v0

    .line 73
    const-string v0, "accept.error.unlinked"

    invoke-static {v0}, Lcom/dropbox/android/util/analytics/a;->d(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 75
    new-instance v0, Ldbxyzptlk/db231222/m/c;

    invoke-direct {v0}, Ldbxyzptlk/db231222/m/c;-><init>()V

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto/16 :goto_1

    .line 76
    :catch_3
    move-exception v0

    .line 77
    const-string v1, "accept.error.unknown"

    invoke-static {v1}, Lcom/dropbox/android/util/analytics/a;->d(Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "err"

    invoke-virtual {v0}, Ldbxyzptlk/db231222/w/a;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 78
    invoke-static {v6, v5}, Ldbxyzptlk/db231222/m/b;->b(ILjava/lang/String;)Ldbxyzptlk/db231222/m/a;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto/16 :goto_1

    .line 97
    :catch_4
    move-exception v0

    goto/16 :goto_0
.end method
