.class public final Ldbxyzptlk/db231222/l/p;
.super Ldbxyzptlk/db231222/l/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/l/a",
        "<",
        "Lcom/dropbox/android/notifications/NotificationKey;",
        "Lcom/dropbox/android/util/DropboxPath;",
        "Ldbxyzptlk/db231222/m/a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:Ldbxyzptlk/db231222/z/M;

.field private final c:Lcom/dropbox/android/provider/MetadataManager;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/notifications/NotificationKey;Ldbxyzptlk/db231222/z/M;Lcom/dropbox/android/provider/MetadataManager;J)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/l/a;-><init>(Ljava/lang/Object;)V

    .line 34
    iput-object p2, p0, Ldbxyzptlk/db231222/l/p;->b:Ldbxyzptlk/db231222/z/M;

    .line 35
    iput-object p3, p0, Ldbxyzptlk/db231222/l/p;->c:Lcom/dropbox/android/provider/MetadataManager;

    .line 36
    iput-wide p4, p0, Ldbxyzptlk/db231222/l/p;->a:J

    .line 37
    return-void
.end method


# virtual methods
.method protected final a()Ldbxyzptlk/db231222/l/d;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldbxyzptlk/db231222/l/d",
            "<",
            "Lcom/dropbox/android/notifications/NotificationKey;",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Ldbxyzptlk/db231222/m/a;",
            ">;"
        }
    .end annotation

    .prologue
    const v4, 0x7f0d005d

    const/4 v3, 0x0

    .line 41
    iget-object v0, p0, Ldbxyzptlk/db231222/l/p;->c:Lcom/dropbox/android/provider/MetadataManager;

    iget-wide v1, p0, Ldbxyzptlk/db231222/l/p;->a:J

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/provider/MetadataManager;->a(J)Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    .line 42
    if-eqz v0, :cond_0

    .line 43
    invoke-static {v0}, Ldbxyzptlk/db231222/l/e;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/e;

    move-result-object v0

    .line 63
    :goto_0
    return-object v0

    .line 47
    :cond_0
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/l/p;->b:Ldbxyzptlk/db231222/z/M;

    iget-wide v1, p0, Ldbxyzptlk/db231222/l/p;->a:J

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/z/M;->c(J)Ldbxyzptlk/db231222/v/j;

    move-result-object v0

    .line 48
    iget-object v1, p0, Ldbxyzptlk/db231222/l/p;->c:Lcom/dropbox/android/provider/MetadataManager;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/provider/MetadataManager;->a(Ldbxyzptlk/db231222/v/j;)V

    .line 49
    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v1, v0}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ldbxyzptlk/db231222/v/j;)V

    invoke-static {v1}, Ldbxyzptlk/db231222/l/e;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/e;
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldbxyzptlk/db231222/w/j; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    goto :goto_0

    .line 50
    :catch_0
    move-exception v0

    .line 51
    const v0, 0x7f0d005b

    invoke-static {v0, v3}, Ldbxyzptlk/db231222/m/b;->b(ILjava/lang/String;)Ldbxyzptlk/db231222/m/a;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto :goto_0

    .line 52
    :catch_1
    move-exception v0

    .line 53
    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x1f4

    if-lt v1, v2, :cond_1

    .line 54
    const v1, 0x7f0d005c

    invoke-virtual {v0, v3}, Ldbxyzptlk/db231222/w/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/db231222/m/b;->b(ILjava/lang/String;)Ldbxyzptlk/db231222/m/a;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto :goto_0

    .line 55
    :cond_1
    iget v1, v0, Ldbxyzptlk/db231222/w/i;->b:I

    const/16 v2, 0x193

    if-ne v1, v2, :cond_2

    .line 56
    const v1, 0x7f0d0305

    invoke-virtual {v0, v3}, Ldbxyzptlk/db231222/w/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/db231222/m/b;->b(ILjava/lang/String;)Ldbxyzptlk/db231222/m/a;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto :goto_0

    .line 58
    :cond_2
    invoke-virtual {v0, v3}, Ldbxyzptlk/db231222/w/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Ldbxyzptlk/db231222/m/b;->b(ILjava/lang/String;)Ldbxyzptlk/db231222/m/a;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto :goto_0

    .line 60
    :catch_2
    move-exception v0

    .line 61
    new-instance v0, Ldbxyzptlk/db231222/m/c;

    invoke-direct {v0}, Ldbxyzptlk/db231222/m/c;-><init>()V

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto :goto_0

    .line 62
    :catch_3
    move-exception v0

    .line 63
    invoke-static {v4, v3}, Ldbxyzptlk/db231222/m/b;->b(ILjava/lang/String;)Ldbxyzptlk/db231222/m/a;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto :goto_0
.end method
