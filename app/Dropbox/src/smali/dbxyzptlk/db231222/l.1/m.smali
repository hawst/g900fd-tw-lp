.class public final Ldbxyzptlk/db231222/l/m;
.super Ldbxyzptlk/db231222/l/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/l/a",
        "<",
        "Lcom/dropbox/android/notifications/NotificationKey;",
        "Ljava/lang/Void;",
        "Ldbxyzptlk/db231222/m/a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/dropbox/sync/android/aJ;

.field private final b:Ldbxyzptlk/db231222/z/M;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/notifications/NotificationKey;Lcom/dropbox/sync/android/aJ;Ldbxyzptlk/db231222/z/M;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/l/a;-><init>(Ljava/lang/Object;)V

    .line 29
    iput-object p2, p0, Ldbxyzptlk/db231222/l/m;->a:Lcom/dropbox/sync/android/aJ;

    .line 30
    iput-object p3, p0, Ldbxyzptlk/db231222/l/m;->b:Ldbxyzptlk/db231222/z/M;

    .line 31
    return-void
.end method


# virtual methods
.method protected final a()Ldbxyzptlk/db231222/l/d;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldbxyzptlk/db231222/l/d",
            "<",
            "Lcom/dropbox/android/notifications/NotificationKey;",
            "Ljava/lang/Void;",
            "Ldbxyzptlk/db231222/m/a;",
            ">;"
        }
    .end annotation

    .prologue
    const v6, 0x7f0d005d

    const/4 v5, 0x0

    .line 37
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/l/m;->b:Ldbxyzptlk/db231222/z/M;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/z/M;->f()V
    :try_end_0
    .catch Ldbxyzptlk/db231222/w/d; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldbxyzptlk/db231222/w/i; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldbxyzptlk/db231222/w/a; {:try_start_0 .. :try_end_0} :catch_2

    .line 49
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bP()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 58
    :try_start_1
    iget-object v0, p0, Ldbxyzptlk/db231222/l/m;->a:Lcom/dropbox/sync/android/aJ;

    invoke-virtual {v0}, Lcom/dropbox/sync/android/aJ;->f()V
    :try_end_1
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_1 .. :try_end_1} :catch_3

    .line 61
    :goto_0
    invoke-static {v5}, Ldbxyzptlk/db231222/l/e;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/e;

    move-result-object v0

    :goto_1
    return-object v0

    .line 38
    :catch_0
    move-exception v0

    .line 39
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bO()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "type"

    const-string v2, "io"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 40
    const v0, 0x7f0d005b

    invoke-static {v0, v5}, Ldbxyzptlk/db231222/m/b;->b(ILjava/lang/String;)Ldbxyzptlk/db231222/m/a;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto :goto_1

    .line 41
    :catch_1
    move-exception v0

    .line 42
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bO()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "type"

    const-string v3, "server"

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "code"

    iget v3, v0, Ldbxyzptlk/db231222/w/i;->b:I

    int-to-long v3, v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 43
    invoke-virtual {v0, v5}, Ldbxyzptlk/db231222/w/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Ldbxyzptlk/db231222/m/b;->b(ILjava/lang/String;)Ldbxyzptlk/db231222/m/a;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto :goto_1

    .line 44
    :catch_2
    move-exception v0

    .line 45
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->bO()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "type"

    const-string v3, "unknown"

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "err"

    invoke-virtual {v0}, Ldbxyzptlk/db231222/w/a;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 46
    invoke-static {v6, v5}, Ldbxyzptlk/db231222/m/b;->b(ILjava/lang/String;)Ldbxyzptlk/db231222/m/a;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/l/c;->a(Ljava/lang/Object;)Ldbxyzptlk/db231222/l/c;

    move-result-object v0

    goto :goto_1

    .line 59
    :catch_3
    move-exception v0

    goto :goto_0
.end method
