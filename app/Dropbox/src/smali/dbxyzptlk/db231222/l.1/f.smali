.class public final Ldbxyzptlk/db231222/l/f;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Key::",
        "Landroid/os/Parcelable;",
        "SuccessResult:",
        "Ljava/lang/Object;",
        "ErrorResult:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Landroid/support/v4/app/FragmentManager;

.field private final b:Ldbxyzptlk/db231222/l/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/l/k",
            "<TKey;TSuccessResult;TErrorResult;>;"
        }
    .end annotation
.end field

.field private final c:Ldbxyzptlk/db231222/l/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/l/h",
            "<TKey;>;"
        }
    .end annotation
.end field

.field private final d:Ldbxyzptlk/db231222/l/g;

.field private e:Ldbxyzptlk/db231222/l/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/l/l",
            "<TKey;TSuccessResult;TErrorResult;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/l/k;Ldbxyzptlk/db231222/l/h;Ldbxyzptlk/db231222/l/g;Landroid/support/v4/app/FragmentManager;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/l/k",
            "<TKey;TSuccessResult;TErrorResult;>;",
            "Ldbxyzptlk/db231222/l/h",
            "<TKey;>;",
            "Ldbxyzptlk/db231222/l/g;",
            "Landroid/support/v4/app/FragmentManager;",
            ")V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    invoke-virtual {p1}, Ldbxyzptlk/db231222/l/k;->c()Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 69
    iput-object p1, p0, Ldbxyzptlk/db231222/l/f;->b:Ldbxyzptlk/db231222/l/k;

    .line 70
    iput-object p2, p0, Ldbxyzptlk/db231222/l/f;->c:Ldbxyzptlk/db231222/l/h;

    .line 71
    iput-object p3, p0, Ldbxyzptlk/db231222/l/f;->d:Ldbxyzptlk/db231222/l/g;

    .line 72
    iput-object p4, p0, Ldbxyzptlk/db231222/l/f;->a:Landroid/support/v4/app/FragmentManager;

    .line 73
    return-void
.end method

.method private a(Landroid/os/Parcelable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;)V"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Ldbxyzptlk/db231222/l/f;->a:Landroid/support/v4/app/FragmentManager;

    iget-object v1, p0, Ldbxyzptlk/db231222/l/f;->c:Ldbxyzptlk/db231222/l/h;

    invoke-interface {v1, p1}, Ldbxyzptlk/db231222/l/h;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 77
    if-eqz v0, :cond_0

    .line 78
    iget-object v1, p0, Ldbxyzptlk/db231222/l/f;->a:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 79
    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 80
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 82
    :cond_0
    return-void
.end method

.method static synthetic a(Ldbxyzptlk/db231222/l/f;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/l/f;->b(Landroid/os/Parcelable;)V

    return-void
.end method

.method private b(Landroid/os/Parcelable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;)V"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Ldbxyzptlk/db231222/l/f;->a:Landroid/support/v4/app/FragmentManager;

    iget-object v1, p0, Ldbxyzptlk/db231222/l/f;->c:Ldbxyzptlk/db231222/l/h;

    invoke-interface {v1, p1}, Ldbxyzptlk/db231222/l/h;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 86
    if-nez v0, :cond_0

    .line 87
    iget-object v0, p0, Ldbxyzptlk/db231222/l/f;->d:Ldbxyzptlk/db231222/l/g;

    invoke-interface {v0}, Ldbxyzptlk/db231222/l/g;->a()Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/l/f;->a:Landroid/support/v4/app/FragmentManager;

    iget-object v2, p0, Ldbxyzptlk/db231222/l/f;->c:Ldbxyzptlk/db231222/l/h;

    invoke-interface {v2, p1}, Ldbxyzptlk/db231222/l/h;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Ldbxyzptlk/db231222/l/f;->a:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 97
    :cond_0
    return-void
.end method

.method static synthetic b(Ldbxyzptlk/db231222/l/f;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/l/f;->a(Landroid/os/Parcelable;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Ldbxyzptlk/db231222/l/f;->b:Ldbxyzptlk/db231222/l/k;

    iget-object v1, p0, Ldbxyzptlk/db231222/l/f;->e:Ldbxyzptlk/db231222/l/l;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/l/k;->b(Ldbxyzptlk/db231222/l/l;)V

    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/l/f;->e:Ldbxyzptlk/db231222/l/l;

    .line 118
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/l/i;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/l/i",
            "<TKey;TSuccessResult;TErrorResult;>;)V"
        }
    .end annotation

    .prologue
    .line 107
    new-instance v0, Ldbxyzptlk/db231222/l/j;

    invoke-direct {v0, p0, p1}, Ldbxyzptlk/db231222/l/j;-><init>(Ldbxyzptlk/db231222/l/f;Ldbxyzptlk/db231222/l/i;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/l/f;->e:Ldbxyzptlk/db231222/l/l;

    .line 108
    iget-object v0, p0, Ldbxyzptlk/db231222/l/f;->b:Ldbxyzptlk/db231222/l/k;

    iget-object v1, p0, Ldbxyzptlk/db231222/l/f;->e:Ldbxyzptlk/db231222/l/l;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/l/k;->a(Ldbxyzptlk/db231222/l/l;)V

    .line 109
    return-void
.end method
