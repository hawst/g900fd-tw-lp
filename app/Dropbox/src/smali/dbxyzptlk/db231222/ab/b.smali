.class public abstract Ldbxyzptlk/db231222/ab/b;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Ldbxyzptlk/db231222/ab/b;

.field public static final b:Ldbxyzptlk/db231222/ab/b;

.field public static final c:Ldbxyzptlk/db231222/ab/b;

.field public static final d:Ldbxyzptlk/db231222/ab/b;

.field public static final e:Ldbxyzptlk/db231222/ab/b;

.field private static final f:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = -0x23ea08d00c05296cL


# instance fields
.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Z

.field private o:Z

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Z

.field private t:Ljava/lang/String;

.field private u:Z

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    new-instance v0, Ldbxyzptlk/db231222/ab/c;

    invoke-direct {v0}, Ldbxyzptlk/db231222/ab/c;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/ab/b;->a:Ldbxyzptlk/db231222/ab/b;

    .line 95
    new-instance v0, Ldbxyzptlk/db231222/ab/d;

    invoke-direct {v0}, Ldbxyzptlk/db231222/ab/d;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/ab/b;->b:Ldbxyzptlk/db231222/ab/b;

    .line 106
    new-instance v0, Ldbxyzptlk/db231222/ab/e;

    invoke-direct {v0}, Ldbxyzptlk/db231222/ab/e;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/ab/b;->c:Ldbxyzptlk/db231222/ab/b;

    .line 118
    new-instance v0, Ldbxyzptlk/db231222/ab/f;

    invoke-direct {v0}, Ldbxyzptlk/db231222/ab/f;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/ab/b;->d:Ldbxyzptlk/db231222/ab/b;

    .line 128
    new-instance v0, Ldbxyzptlk/db231222/ab/g;

    invoke-direct {v0}, Ldbxyzptlk/db231222/ab/g;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/ab/b;->e:Ldbxyzptlk/db231222/ab/b;

    .line 136
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/ab/b;->f:Ljava/lang/ThreadLocal;

    return-void
.end method

.method protected constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213
    iput-boolean v1, p0, Ldbxyzptlk/db231222/ab/b;->g:Z

    .line 218
    iput-boolean v1, p0, Ldbxyzptlk/db231222/ab/b;->h:Z

    .line 223
    iput-boolean v2, p0, Ldbxyzptlk/db231222/ab/b;->i:Z

    .line 228
    iput-boolean v1, p0, Ldbxyzptlk/db231222/ab/b;->j:Z

    .line 233
    const-string v0, "["

    iput-object v0, p0, Ldbxyzptlk/db231222/ab/b;->k:Ljava/lang/String;

    .line 238
    const-string v0, "]"

    iput-object v0, p0, Ldbxyzptlk/db231222/ab/b;->l:Ljava/lang/String;

    .line 243
    const-string v0, "="

    iput-object v0, p0, Ldbxyzptlk/db231222/ab/b;->m:Ljava/lang/String;

    .line 248
    iput-boolean v2, p0, Ldbxyzptlk/db231222/ab/b;->n:Z

    .line 253
    iput-boolean v2, p0, Ldbxyzptlk/db231222/ab/b;->o:Z

    .line 258
    const-string v0, ","

    iput-object v0, p0, Ldbxyzptlk/db231222/ab/b;->p:Ljava/lang/String;

    .line 263
    const-string v0, "{"

    iput-object v0, p0, Ldbxyzptlk/db231222/ab/b;->q:Ljava/lang/String;

    .line 268
    const-string v0, ","

    iput-object v0, p0, Ldbxyzptlk/db231222/ab/b;->r:Ljava/lang/String;

    .line 273
    iput-boolean v1, p0, Ldbxyzptlk/db231222/ab/b;->s:Z

    .line 278
    const-string v0, "}"

    iput-object v0, p0, Ldbxyzptlk/db231222/ab/b;->t:Ljava/lang/String;

    .line 284
    iput-boolean v1, p0, Ldbxyzptlk/db231222/ab/b;->u:Z

    .line 289
    const-string v0, "<null>"

    iput-object v0, p0, Ldbxyzptlk/db231222/ab/b;->v:Ljava/lang/String;

    .line 294
    const-string v0, "<size="

    iput-object v0, p0, Ldbxyzptlk/db231222/ab/b;->w:Ljava/lang/String;

    .line 299
    const-string v0, ">"

    iput-object v0, p0, Ldbxyzptlk/db231222/ab/b;->x:Ljava/lang/String;

    .line 304
    const-string v0, "<"

    iput-object v0, p0, Ldbxyzptlk/db231222/ab/b;->y:Ljava/lang/String;

    .line 309
    const-string v0, ">"

    iput-object v0, p0, Ldbxyzptlk/db231222/ab/b;->z:Ljava/lang/String;

    .line 318
    return-void
.end method

.method static a()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    sget-object v0, Ldbxyzptlk/db231222/ab/b;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method static a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 163
    invoke-static {}, Ldbxyzptlk/db231222/ab/b;->a()Ljava/util/Map;

    move-result-object v0

    .line 164
    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static b(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 177
    if-eqz p0, :cond_1

    .line 178
    invoke-static {}, Ldbxyzptlk/db231222/ab/b;->a()Ljava/util/Map;

    move-result-object v0

    .line 179
    if-nez v0, :cond_0

    .line 180
    sget-object v0, Ldbxyzptlk/db231222/ab/b;->f:Ljava/lang/ThreadLocal;

    new-instance v1, Ljava/util/WeakHashMap;

    invoke-direct {v1}, Ljava/util/WeakHashMap;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 182
    :cond_0
    invoke-static {}, Ldbxyzptlk/db231222/ab/b;->a()Ljava/util/Map;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    :cond_1
    return-void
.end method

.method static c(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 199
    if-eqz p0, :cond_0

    .line 200
    invoke-static {}, Ldbxyzptlk/db231222/ab/b;->a()Ljava/util/Map;

    move-result-object v0

    .line 201
    if-eqz v0, :cond_0

    .line 202
    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    sget-object v0, Ldbxyzptlk/db231222/ab/b;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    .line 208
    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(Ljava/lang/Class;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1581
    invoke-static {p1}, Ldbxyzptlk/db231222/aa/b;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1809
    if-nez p1, :cond_0

    .line 1810
    const-string p1, ""

    .line 1812
    :cond_0
    iput-object p1, p0, Ldbxyzptlk/db231222/ab/b;->k:Ljava/lang/String;

    .line 1813
    return-void
.end method

.method protected final a(Ljava/lang/StringBuffer;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 400
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    .line 401
    iget-object v1, p0, Ldbxyzptlk/db231222/ab/b;->p:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    .line 402
    if-lez v3, :cond_0

    if-lez v4, :cond_0

    if-lt v3, v4, :cond_0

    .line 403
    const/4 v1, 0x1

    move v2, v0

    .line 404
    :goto_0
    if-ge v2, v4, :cond_2

    .line 405
    add-int/lit8 v5, v3, -0x1

    sub-int/2addr v5, v2

    invoke-virtual {p1, v5}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v5

    iget-object v6, p0, Ldbxyzptlk/db231222/ab/b;->p:Ljava/lang/String;

    add-int/lit8 v7, v4, -0x1

    sub-int/2addr v7, v2

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eq v5, v6, :cond_1

    .line 410
    :goto_1
    if-eqz v0, :cond_0

    .line 411
    sub-int v0, v3, v4

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 414
    :cond_0
    return-void

    .line 404
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/StringBuffer;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 368
    if-eqz p2, :cond_0

    .line 369
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/ab/b;->c(Ljava/lang/StringBuffer;Ljava/lang/Object;)V

    .line 370
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/ab/b;->d(Ljava/lang/StringBuffer;Ljava/lang/Object;)V

    .line 371
    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;)V

    .line 372
    iget-boolean v0, p0, Ldbxyzptlk/db231222/ab/b;->n:Z

    if-eqz v0, :cond_0

    .line 373
    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/ab/b;->d(Ljava/lang/StringBuffer;)V

    .line 376
    :cond_0
    return-void
.end method

.method protected final a(Ljava/lang/StringBuffer;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1494
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1495
    return-void
.end method

.method protected final a(Ljava/lang/StringBuffer;Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 737
    invoke-virtual {p1, p3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 738
    return-void
.end method

.method protected final a(Ljava/lang/StringBuffer;Ljava/lang/String;C)V
    .locals 0

    .prologue
    .line 765
    invoke-virtual {p1, p3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 766
    return-void
.end method

.method protected final a(Ljava/lang/StringBuffer;Ljava/lang/String;D)V
    .locals 0

    .prologue
    .line 793
    invoke-virtual {p1, p3, p4}, Ljava/lang/StringBuffer;->append(D)Ljava/lang/StringBuffer;

    .line 794
    return-void
.end method

.method protected final a(Ljava/lang/StringBuffer;Ljava/lang/String;F)V
    .locals 0

    .prologue
    .line 821
    invoke-virtual {p1, p3}, Ljava/lang/StringBuffer;->append(F)Ljava/lang/StringBuffer;

    .line 822
    return-void
.end method

.method protected final a(Ljava/lang/StringBuffer;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 681
    invoke-virtual {p1, p3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 682
    return-void
.end method

.method protected final a(Ljava/lang/StringBuffer;Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 653
    invoke-virtual {p1, p3, p4}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 654
    return-void
.end method

.method protected final a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 573
    invoke-static {p1, p3}, Ldbxyzptlk/db231222/aa/d;->a(Ljava/lang/StringBuffer;Ljava/lang/Object;)V

    .line 574
    return-void
.end method

.method public final a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 430
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    .line 432
    if-nez p3, :cond_0

    .line 433
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    .line 439
    :goto_0
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/ab/b;->c(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    .line 440
    return-void

    .line 436
    :cond_0
    invoke-virtual {p0, p4}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/Boolean;)Z

    move-result v0

    invoke-virtual {p0, p1, p2, p3, v0}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/Object;Z)V

    goto :goto_0
.end method

.method protected final a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 462
    invoke-static {p3}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    instance-of v1, p3, Ljava/lang/Number;

    if-nez v1, :cond_0

    instance-of v1, p3, Ljava/lang/Boolean;

    if-nez v1, :cond_0

    instance-of v1, p3, Ljava/lang/Character;

    if-nez v1, :cond_0

    .line 464
    invoke-virtual {p0, p1, p2, p3}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/Object;)V

    .line 558
    :goto_0
    return-void

    .line 468
    :cond_0
    invoke-static {p3}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/Object;)V

    .line 471
    :try_start_0
    instance-of v1, p3, Ljava/util/Collection;

    if-eqz v1, :cond_2

    .line 472
    if-eqz p4, :cond_1

    .line 473
    move-object v0, p3

    check-cast v0, Ljava/util/Collection;

    move-object v1, v0

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 556
    :goto_1
    invoke-static {p3}, Ldbxyzptlk/db231222/ab/b;->c(Ljava/lang/Object;)V

    goto :goto_0

    .line 475
    :cond_1
    :try_start_1
    move-object v0, p3

    check-cast v0, Ljava/util/Collection;

    move-object v1, v0

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 556
    :catchall_0
    move-exception v1

    invoke-static {p3}, Ldbxyzptlk/db231222/ab/b;->c(Ljava/lang/Object;)V

    throw v1

    .line 478
    :cond_2
    :try_start_2
    instance-of v1, p3, Ljava/util/Map;

    if-eqz v1, :cond_4

    .line 479
    if-eqz p4, :cond_3

    .line 480
    move-object v0, p3

    check-cast v0, Ljava/util/Map;

    move-object v1, v0

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_1

    .line 482
    :cond_3
    move-object v0, p3

    check-cast v0, Ljava/util/Map;

    move-object v1, v0

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;I)V

    goto :goto_1

    .line 485
    :cond_4
    instance-of v1, p3, [J

    if-eqz v1, :cond_6

    .line 486
    if-eqz p4, :cond_5

    .line 487
    move-object v0, p3

    check-cast v0, [J

    move-object v1, v0

    check-cast v1, [J

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;[J)V

    goto :goto_1

    .line 489
    :cond_5
    move-object v0, p3

    check-cast v0, [J

    move-object v1, v0

    check-cast v1, [J

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;[J)V

    goto :goto_1

    .line 492
    :cond_6
    instance-of v1, p3, [I

    if-eqz v1, :cond_8

    .line 493
    if-eqz p4, :cond_7

    .line 494
    move-object v0, p3

    check-cast v0, [I

    move-object v1, v0

    check-cast v1, [I

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;[I)V

    goto :goto_1

    .line 496
    :cond_7
    move-object v0, p3

    check-cast v0, [I

    move-object v1, v0

    check-cast v1, [I

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;[I)V

    goto :goto_1

    .line 499
    :cond_8
    instance-of v1, p3, [S

    if-eqz v1, :cond_a

    .line 500
    if-eqz p4, :cond_9

    .line 501
    move-object v0, p3

    check-cast v0, [S

    move-object v1, v0

    check-cast v1, [S

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;[S)V

    goto :goto_1

    .line 503
    :cond_9
    move-object v0, p3

    check-cast v0, [S

    move-object v1, v0

    check-cast v1, [S

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;[S)V

    goto :goto_1

    .line 506
    :cond_a
    instance-of v1, p3, [B

    if-eqz v1, :cond_c

    .line 507
    if-eqz p4, :cond_b

    .line 508
    move-object v0, p3

    check-cast v0, [B

    move-object v1, v0

    check-cast v1, [B

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;[B)V

    goto/16 :goto_1

    .line 510
    :cond_b
    move-object v0, p3

    check-cast v0, [B

    move-object v1, v0

    check-cast v1, [B

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;[B)V

    goto/16 :goto_1

    .line 513
    :cond_c
    instance-of v1, p3, [C

    if-eqz v1, :cond_e

    .line 514
    if-eqz p4, :cond_d

    .line 515
    move-object v0, p3

    check-cast v0, [C

    move-object v1, v0

    check-cast v1, [C

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;[C)V

    goto/16 :goto_1

    .line 517
    :cond_d
    move-object v0, p3

    check-cast v0, [C

    move-object v1, v0

    check-cast v1, [C

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;[C)V

    goto/16 :goto_1

    .line 520
    :cond_e
    instance-of v1, p3, [D

    if-eqz v1, :cond_10

    .line 521
    if-eqz p4, :cond_f

    .line 522
    move-object v0, p3

    check-cast v0, [D

    move-object v1, v0

    check-cast v1, [D

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;[D)V

    goto/16 :goto_1

    .line 524
    :cond_f
    move-object v0, p3

    check-cast v0, [D

    move-object v1, v0

    check-cast v1, [D

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;[D)V

    goto/16 :goto_1

    .line 527
    :cond_10
    instance-of v1, p3, [F

    if-eqz v1, :cond_12

    .line 528
    if-eqz p4, :cond_11

    .line 529
    move-object v0, p3

    check-cast v0, [F

    move-object v1, v0

    check-cast v1, [F

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;[F)V

    goto/16 :goto_1

    .line 531
    :cond_11
    move-object v0, p3

    check-cast v0, [F

    move-object v1, v0

    check-cast v1, [F

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;[F)V

    goto/16 :goto_1

    .line 534
    :cond_12
    instance-of v1, p3, [Z

    if-eqz v1, :cond_14

    .line 535
    if-eqz p4, :cond_13

    .line 536
    move-object v0, p3

    check-cast v0, [Z

    move-object v1, v0

    check-cast v1, [Z

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;[Z)V

    goto/16 :goto_1

    .line 538
    :cond_13
    move-object v0, p3

    check-cast v0, [Z

    move-object v1, v0

    check-cast v1, [Z

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;[Z)V

    goto/16 :goto_1

    .line 541
    :cond_14
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->isArray()Z

    move-result v1

    if-eqz v1, :cond_16

    .line 542
    if-eqz p4, :cond_15

    .line 543
    move-object v0, p3

    check-cast v0, [Ljava/lang/Object;

    move-object v1, v0

    check-cast v1, [Ljava/lang/Object;

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 545
    :cond_15
    move-object v0, p3

    check-cast v0, [Ljava/lang/Object;

    move-object v1, v0

    check-cast v1, [Ljava/lang/Object;

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 549
    :cond_16
    if-eqz p4, :cond_17

    .line 550
    invoke-virtual {p0, p1, p2, p3}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 552
    :cond_17
    invoke-virtual {p0, p1, p2, p3}, Ldbxyzptlk/db231222/ab/b;->c(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1
.end method

.method protected final a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuffer;",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 598
    invoke-virtual {p1, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 599
    return-void
.end method

.method protected final a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuffer;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 610
    invoke-virtual {p1, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 611
    return-void
.end method

.method protected final a(Ljava/lang/StringBuffer;Ljava/lang/String;S)V
    .locals 0

    .prologue
    .line 709
    invoke-virtual {p1, p3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 710
    return-void
.end method

.method public final a(Ljava/lang/StringBuffer;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 835
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    .line 836
    invoke-virtual {p0, p1, p2, p3}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;Z)V

    .line 837
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/ab/b;->c(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    .line 838
    return-void
.end method

.method protected final a(Ljava/lang/StringBuffer;Ljava/lang/String;[B)V
    .locals 2

    .prologue
    .line 1167
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1168
    const/4 v0, 0x0

    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_1

    .line 1169
    if-lez v0, :cond_0

    .line 1170
    iget-object v1, p0, Ldbxyzptlk/db231222/ab/b;->r:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1172
    :cond_0
    aget-byte v1, p3, v0

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;B)V

    .line 1168
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1174
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1175
    return-void
.end method

.method protected final a(Ljava/lang/StringBuffer;Ljava/lang/String;[C)V
    .locals 2

    .prologue
    .line 1228
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1229
    const/4 v0, 0x0

    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_1

    .line 1230
    if-lez v0, :cond_0

    .line 1231
    iget-object v1, p0, Ldbxyzptlk/db231222/ab/b;->r:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1233
    :cond_0
    aget-char v1, p3, v0

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;C)V

    .line 1229
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1235
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1236
    return-void
.end method

.method protected final a(Ljava/lang/StringBuffer;Ljava/lang/String;[D)V
    .locals 3

    .prologue
    .line 1289
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1290
    const/4 v0, 0x0

    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_1

    .line 1291
    if-lez v0, :cond_0

    .line 1292
    iget-object v1, p0, Ldbxyzptlk/db231222/ab/b;->r:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1294
    :cond_0
    aget-wide v1, p3, v0

    invoke-virtual {p0, p1, p2, v1, v2}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;D)V

    .line 1290
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1296
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1297
    return-void
.end method

.method protected final a(Ljava/lang/StringBuffer;Ljava/lang/String;[F)V
    .locals 2

    .prologue
    .line 1350
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1351
    const/4 v0, 0x0

    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_1

    .line 1352
    if-lez v0, :cond_0

    .line 1353
    iget-object v1, p0, Ldbxyzptlk/db231222/ab/b;->r:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1355
    :cond_0
    aget v1, p3, v0

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;F)V

    .line 1351
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1357
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1358
    return-void
.end method

.method protected final a(Ljava/lang/StringBuffer;Ljava/lang/String;[I)V
    .locals 2

    .prologue
    .line 1045
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1046
    const/4 v0, 0x0

    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_1

    .line 1047
    if-lez v0, :cond_0

    .line 1048
    iget-object v1, p0, Ldbxyzptlk/db231222/ab/b;->r:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1050
    :cond_0
    aget v1, p3, v0

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;I)V

    .line 1046
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1052
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1053
    return-void
.end method

.method protected final a(Ljava/lang/StringBuffer;Ljava/lang/String;[J)V
    .locals 3

    .prologue
    .line 984
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 985
    const/4 v0, 0x0

    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_1

    .line 986
    if-lez v0, :cond_0

    .line 987
    iget-object v1, p0, Ldbxyzptlk/db231222/ab/b;->r:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 989
    :cond_0
    aget-wide v1, p3, v0

    invoke-virtual {p0, p1, p2, v1, v2}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;J)V

    .line 985
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 991
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 992
    return-void
.end method

.method protected final a(Ljava/lang/StringBuffer;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 890
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 891
    const/4 v0, 0x0

    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_2

    .line 892
    aget-object v1, p3, v0

    .line 893
    if-lez v0, :cond_0

    .line 894
    iget-object v2, p0, Ldbxyzptlk/db231222/ab/b;->r:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 896
    :cond_0
    if-nez v1, :cond_1

    .line 897
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;)V

    .line 891
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 900
    :cond_1
    iget-boolean v2, p0, Ldbxyzptlk/db231222/ab/b;->s:Z

    invoke-virtual {p0, p1, p2, v1, v2}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/Object;Z)V

    goto :goto_1

    .line 903
    :cond_2
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 904
    return-void
.end method

.method protected final a(Ljava/lang/StringBuffer;Ljava/lang/String;[S)V
    .locals 2

    .prologue
    .line 1106
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1107
    const/4 v0, 0x0

    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_1

    .line 1108
    if-lez v0, :cond_0

    .line 1109
    iget-object v1, p0, Ldbxyzptlk/db231222/ab/b;->r:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1111
    :cond_0
    aget-short v1, p3, v0

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;S)V

    .line 1107
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1113
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1114
    return-void
.end method

.method protected final a(Ljava/lang/StringBuffer;Ljava/lang/String;[Z)V
    .locals 2

    .prologue
    .line 1411
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1412
    const/4 v0, 0x0

    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_1

    .line 1413
    if-lez v0, :cond_0

    .line 1414
    iget-object v1, p0, Ldbxyzptlk/db231222/ab/b;->r:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1416
    :cond_0
    aget-boolean v1, p3, v0

    invoke-virtual {p0, p1, p2, v1}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;Z)V

    .line 1412
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1418
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1419
    return-void
.end method

.method protected final a(Z)V
    .locals 0

    .prologue
    .line 1604
    iput-boolean p1, p0, Ldbxyzptlk/db231222/ab/b;->h:Z

    .line 1605
    return-void
.end method

.method protected final a(Ljava/lang/Boolean;)Z
    .locals 1

    .prologue
    .line 1565
    if-nez p1, :cond_0

    .line 1566
    iget-boolean v0, p0, Ldbxyzptlk/db231222/ab/b;->u:Z

    .line 1568
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method protected final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1835
    if-nez p1, :cond_0

    .line 1836
    const-string p1, ""

    .line 1838
    :cond_0
    iput-object p1, p0, Ldbxyzptlk/db231222/ab/b;->l:Ljava/lang/String;

    .line 1839
    return-void
.end method

.method protected final b(Ljava/lang/StringBuffer;)V
    .locals 1

    .prologue
    .line 1473
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1474
    return-void
.end method

.method public final b(Ljava/lang/StringBuffer;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 386
    iget-boolean v0, p0, Ldbxyzptlk/db231222/ab/b;->o:Z

    if-nez v0, :cond_0

    .line 387
    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;)V

    .line 389
    :cond_0
    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/ab/b;->c(Ljava/lang/StringBuffer;)V

    .line 390
    invoke-static {p2}, Ldbxyzptlk/db231222/ab/b;->c(Ljava/lang/Object;)V

    .line 391
    return-void
.end method

.method protected final b(Ljava/lang/StringBuffer;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1513
    iget-boolean v0, p0, Ldbxyzptlk/db231222/ab/b;->g:Z

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 1514
    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1515
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1517
    :cond_0
    return-void
.end method

.method protected final b(Ljava/lang/StringBuffer;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 1545
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->w:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1546
    invoke-virtual {p1, p3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1547
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->x:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1548
    return-void
.end method

.method protected final b(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 586
    invoke-virtual {p1, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 587
    return-void
.end method

.method protected final b(Ljava/lang/StringBuffer;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 849
    invoke-virtual {p1, p3}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    .line 850
    return-void
.end method

.method protected final b(Ljava/lang/StringBuffer;Ljava/lang/String;[B)V
    .locals 1

    .prologue
    .line 1187
    array-length v0, p3

    invoke-virtual {p0, p1, p2, v0}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;I)V

    .line 1188
    return-void
.end method

.method protected final b(Ljava/lang/StringBuffer;Ljava/lang/String;[C)V
    .locals 1

    .prologue
    .line 1248
    array-length v0, p3

    invoke-virtual {p0, p1, p2, v0}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;I)V

    .line 1249
    return-void
.end method

.method protected final b(Ljava/lang/StringBuffer;Ljava/lang/String;[D)V
    .locals 1

    .prologue
    .line 1309
    array-length v0, p3

    invoke-virtual {p0, p1, p2, v0}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;I)V

    .line 1310
    return-void
.end method

.method protected final b(Ljava/lang/StringBuffer;Ljava/lang/String;[F)V
    .locals 1

    .prologue
    .line 1370
    array-length v0, p3

    invoke-virtual {p0, p1, p2, v0}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;I)V

    .line 1371
    return-void
.end method

.method protected final b(Ljava/lang/StringBuffer;Ljava/lang/String;[I)V
    .locals 1

    .prologue
    .line 1065
    array-length v0, p3

    invoke-virtual {p0, p1, p2, v0}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;I)V

    .line 1066
    return-void
.end method

.method protected final b(Ljava/lang/StringBuffer;Ljava/lang/String;[J)V
    .locals 1

    .prologue
    .line 1004
    array-length v0, p3

    invoke-virtual {p0, p1, p2, v0}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;I)V

    .line 1005
    return-void
.end method

.method protected final b(Ljava/lang/StringBuffer;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 943
    array-length v0, p3

    invoke-virtual {p0, p1, p2, v0}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;I)V

    .line 944
    return-void
.end method

.method protected final b(Ljava/lang/StringBuffer;Ljava/lang/String;[S)V
    .locals 1

    .prologue
    .line 1126
    array-length v0, p3

    invoke-virtual {p0, p1, p2, v0}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;I)V

    .line 1127
    return-void
.end method

.method protected final b(Ljava/lang/StringBuffer;Ljava/lang/String;[Z)V
    .locals 1

    .prologue
    .line 1431
    array-length v0, p3

    invoke-virtual {p0, p1, p2, v0}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/String;I)V

    .line 1432
    return-void
.end method

.method protected final b(Z)V
    .locals 0

    .prologue
    .line 1626
    iput-boolean p1, p0, Ldbxyzptlk/db231222/ab/b;->i:Z

    .line 1627
    return-void
.end method

.method protected final b()Z
    .locals 1

    .prologue
    .line 1637
    iget-boolean v0, p0, Ldbxyzptlk/db231222/ab/b;->j:Z

    return v0
.end method

.method protected final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1949
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->v:Ljava/lang/String;

    return-object v0
.end method

.method protected final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1887
    if-nez p1, :cond_0

    .line 1888
    const-string p1, ""

    .line 1890
    :cond_0
    iput-object p1, p0, Ldbxyzptlk/db231222/ab/b;->p:Ljava/lang/String;

    .line 1891
    return-void
.end method

.method protected final c(Ljava/lang/StringBuffer;)V
    .locals 1

    .prologue
    .line 1482
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1483
    return-void
.end method

.method protected final c(Ljava/lang/StringBuffer;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1443
    iget-boolean v0, p0, Ldbxyzptlk/db231222/ab/b;->h:Z

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 1444
    invoke-static {p2}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/Object;)V

    .line 1445
    iget-boolean v0, p0, Ldbxyzptlk/db231222/ab/b;->i:Z

    if-eqz v0, :cond_1

    .line 1446
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1451
    :cond_0
    :goto_0
    return-void

    .line 1448
    :cond_1
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method protected final c(Ljava/lang/StringBuffer;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1526
    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/ab/b;->d(Ljava/lang/StringBuffer;)V

    .line 1527
    return-void
.end method

.method protected final c(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 623
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->y:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 624
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 625
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->z:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 626
    return-void
.end method

.method protected final c(Z)V
    .locals 0

    .prologue
    .line 1646
    iput-boolean p1, p0, Ldbxyzptlk/db231222/ab/b;->j:Z

    .line 1647
    return-void
.end method

.method protected final d(Ljava/lang/StringBuffer;)V
    .locals 1

    .prologue
    .line 1503
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/b;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1504
    return-void
.end method

.method protected final d(Ljava/lang/StringBuffer;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1460
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ab/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 1461
    invoke-static {p2}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/Object;)V

    .line 1462
    const/16 v0, 0x40

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1463
    invoke-static {p2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1465
    :cond_0
    return-void
.end method

.method protected final d(Z)V
    .locals 0

    .prologue
    .line 1666
    iput-boolean p1, p0, Ldbxyzptlk/db231222/ab/b;->g:Z

    .line 1667
    return-void
.end method

.method protected final e(Z)V
    .locals 0

    .prologue
    .line 1914
    iput-boolean p1, p0, Ldbxyzptlk/db231222/ab/b;->n:Z

    .line 1915
    return-void
.end method
