.class public final Ldbxyzptlk/db231222/ab/a;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static volatile a:Ldbxyzptlk/db231222/ab/b;


# instance fields
.field private final b:Ljava/lang/StringBuffer;

.field private final c:Ljava/lang/Object;

.field private final d:Ldbxyzptlk/db231222/ab/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    sget-object v0, Ldbxyzptlk/db231222/ab/b;->a:Ldbxyzptlk/db231222/ab/b;

    sput-object v0, Ldbxyzptlk/db231222/ab/a;->a:Ldbxyzptlk/db231222/ab/b;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 226
    invoke-direct {p0, p1, v0, v0}, Ldbxyzptlk/db231222/ab/a;-><init>(Ljava/lang/Object;Ldbxyzptlk/db231222/ab/b;Ljava/lang/StringBuffer;)V

    .line 227
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Ldbxyzptlk/db231222/ab/b;Ljava/lang/StringBuffer;)V
    .locals 1

    .prologue
    .line 252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 253
    if-nez p2, :cond_0

    .line 254
    invoke-static {}, Ldbxyzptlk/db231222/ab/a;->a()Ldbxyzptlk/db231222/ab/b;

    move-result-object p2

    .line 256
    :cond_0
    if-nez p3, :cond_1

    .line 257
    new-instance p3, Ljava/lang/StringBuffer;

    const/16 v0, 0x200

    invoke-direct {p3, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 259
    :cond_1
    iput-object p3, p0, Ldbxyzptlk/db231222/ab/a;->b:Ljava/lang/StringBuffer;

    .line 260
    iput-object p2, p0, Ldbxyzptlk/db231222/ab/a;->d:Ldbxyzptlk/db231222/ab/b;

    .line 261
    iput-object p1, p0, Ldbxyzptlk/db231222/ab/a;->c:Ljava/lang/Object;

    .line 263
    invoke-virtual {p2, p3, p1}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/Object;)V

    .line 264
    return-void
.end method

.method public static a()Ldbxyzptlk/db231222/ab/b;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Ldbxyzptlk/db231222/ab/a;->a:Ldbxyzptlk/db231222/ab/b;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;)Ldbxyzptlk/db231222/ab/a;
    .locals 3

    .prologue
    .line 848
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/a;->d:Ldbxyzptlk/db231222/ab/b;

    iget-object v1, p0, Ldbxyzptlk/db231222/ab/a;->b:Ljava/lang/StringBuffer;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, p2, v2}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Boolean;)V

    .line 849
    return-object p0
.end method

.method public final a(Ljava/lang/String;Z)Ldbxyzptlk/db231222/ab/a;
    .locals 2

    .prologue
    .line 527
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/a;->d:Ldbxyzptlk/db231222/ab/b;

    iget-object v1, p0, Ldbxyzptlk/db231222/ab/a;->b:Ljava/lang/StringBuffer;

    invoke-virtual {v0, v1, p1, p2}, Ldbxyzptlk/db231222/ab/b;->a(Ljava/lang/StringBuffer;Ljava/lang/String;Z)V

    .line 528
    return-object p0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1022
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/a;->c:Ljava/lang/Object;

    return-object v0
.end method

.method public final c()Ljava/lang/StringBuffer;
    .locals 1

    .prologue
    .line 1031
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/a;->b:Ljava/lang/StringBuffer;

    return-object v0
.end method

.method public final d()Ldbxyzptlk/db231222/ab/b;
    .locals 1

    .prologue
    .line 1043
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/a;->d:Ldbxyzptlk/db231222/ab/b;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1058
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ab/a;->b()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1059
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ab/a;->c()Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ab/a;->d()Ldbxyzptlk/db231222/ab/b;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/ab/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1063
    :goto_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ab/a;->c()Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1061
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/ab/a;->d:Ldbxyzptlk/db231222/ab/b;

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ab/a;->c()Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ab/a;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/ab/b;->b(Ljava/lang/StringBuffer;Ljava/lang/Object;)V

    goto :goto_0
.end method
