.class public Ldbxyzptlk/db231222/ag/f;
.super Ldbxyzptlk/db231222/ac/c;
.source "panda.py"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x41a4eb7f342b7c67L


# instance fields
.field private final a:Ldbxyzptlk/db231222/ac/c;

.field private final b:Ldbxyzptlk/db231222/ac/d;


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/ac/c;)V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ldbxyzptlk/db231222/ag/f;-><init>(Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/d;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/d;)V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Ldbxyzptlk/db231222/ac/c;-><init>()V

    .line 63
    if-nez p1, :cond_0

    .line 64
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The field must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_0
    iput-object p1, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    .line 67
    if-nez p2, :cond_1

    invoke-virtual {p1}, Ldbxyzptlk/db231222/ac/c;->a()Ldbxyzptlk/db231222/ac/d;

    move-result-object p2

    :cond_1
    iput-object p2, p0, Ldbxyzptlk/db231222/ag/f;->b:Ldbxyzptlk/db231222/ac/d;

    .line 68
    return-void
.end method


# virtual methods
.method public a(J)I
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->a(J)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/util/Locale;)I
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/ac/c;->a(Ljava/util/Locale;)I

    move-result v0

    return v0
.end method

.method public final a(JI)J
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2, p3}, Ldbxyzptlk/db231222/ac/c;->a(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(JJ)J
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ac/c;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(JLjava/lang/String;Ljava/util/Locale;)J
    .locals 2

    .prologue
    .line 176
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ac/c;->a(JLjava/lang/String;Ljava/util/Locale;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a()Ldbxyzptlk/db231222/ac/d;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->b:Ldbxyzptlk/db231222/ac/d;

    return-object v0
.end method

.method public final a(ILjava/util/Locale;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->a(ILjava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(JLjava/util/Locale;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2, p3}, Ldbxyzptlk/db231222/ac/c;->a(JLjava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/ac/D;Ljava/util/Locale;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->a(Ldbxyzptlk/db231222/ac/D;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(JJ)I
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ac/c;->b(JJ)I

    move-result v0

    return v0
.end method

.method public b(JI)J
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2, p3}, Ldbxyzptlk/db231222/ac/c;->b(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->b:Ldbxyzptlk/db231222/ac/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/d;->x()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(ILjava/util/Locale;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->b(ILjava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(JLjava/util/Locale;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2, p3}, Ldbxyzptlk/db231222/ac/c;->b(JLjava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ldbxyzptlk/db231222/ac/D;Ljava/util/Locale;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->b(Ldbxyzptlk/db231222/ac/D;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(J)Z
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->b(J)Z

    move-result v0

    return v0
.end method

.method public final c(J)I
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->c(J)I

    move-result v0

    return v0
.end method

.method public final c(JJ)J
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ac/c;->c(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/c;->c()Z

    move-result v0

    return v0
.end method

.method public final d(J)J
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->d(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final d()Ldbxyzptlk/db231222/ac/l;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/c;->d()Ldbxyzptlk/db231222/ac/l;

    move-result-object v0

    return-object v0
.end method

.method public final e(J)J
    .locals 2

    .prologue
    .line 256
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->e(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final e()Ldbxyzptlk/db231222/ac/l;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/c;->e()Ldbxyzptlk/db231222/ac/l;

    move-result-object v0

    return-object v0
.end method

.method public final f(J)J
    .locals 2

    .prologue
    .line 260
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->f(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final f()Ldbxyzptlk/db231222/ac/l;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/c;->f()Ldbxyzptlk/db231222/ac/l;

    move-result-object v0

    return-object v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/c;->g()I

    move-result v0

    return v0
.end method

.method public final g(J)J
    .locals 2

    .prologue
    .line 264
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->g(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/c;->h()I

    move-result v0

    return v0
.end method

.method public final h(J)J
    .locals 2

    .prologue
    .line 268
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->h(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final i(J)J
    .locals 2

    .prologue
    .line 272
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/f;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->i(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 276
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DateTimeField["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
