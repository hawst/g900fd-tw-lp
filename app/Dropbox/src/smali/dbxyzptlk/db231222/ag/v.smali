.class public final Ldbxyzptlk/db231222/ag/v;
.super Ldbxyzptlk/db231222/ag/d;
.source "panda.py"


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/d;)V
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/ag/d;-><init>(Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/d;)V

    .line 47
    invoke-virtual {p1}, Ldbxyzptlk/db231222/ac/c;->g()I

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Wrapped field\'s minumum value must be zero"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(J)I
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/v;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->a(J)I

    move-result v0

    .line 54
    if-nez v0, :cond_0

    .line 55
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/v;->h()I

    move-result v0

    .line 57
    :cond_0
    return v0
.end method

.method public final a(JI)J
    .locals 2

    .prologue
    .line 61
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/v;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Ldbxyzptlk/db231222/ac/c;->a(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(JJ)J
    .locals 2

    .prologue
    .line 65
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/v;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ac/c;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final b(JJ)I
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/v;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ac/c;->b(JJ)I

    move-result v0

    return v0
.end method

.method public final b(JI)J
    .locals 2

    .prologue
    .line 85
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/v;->h()I

    move-result v0

    .line 86
    const/4 v1, 0x1

    invoke-static {p0, p3, v1, v0}, Ldbxyzptlk/db231222/ag/h;->a(Ldbxyzptlk/db231222/ac/c;III)V

    .line 87
    if-ne p3, v0, :cond_0

    .line 88
    const/4 p3, 0x0

    .line 90
    :cond_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/v;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Ldbxyzptlk/db231222/ac/c;->b(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method public final b(J)Z
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/v;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->b(J)Z

    move-result v0

    return v0
.end method

.method public final c(J)I
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/v;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->c(J)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final c(JJ)J
    .locals 2

    .prologue
    .line 81
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/v;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ac/c;->c(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final d(J)J
    .locals 2

    .prologue
    .line 182
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/v;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->d(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final e(J)J
    .locals 2

    .prologue
    .line 186
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/v;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->e(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final f(J)J
    .locals 2

    .prologue
    .line 190
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/v;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->f(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final f()Ldbxyzptlk/db231222/ac/l;
    .locals 1

    .prologue
    .line 102
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/v;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/c;->f()Ldbxyzptlk/db231222/ac/l;

    move-result-object v0

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x1

    return v0
.end method

.method public final g(J)J
    .locals 2

    .prologue
    .line 194
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/v;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->g(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 148
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/v;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/c;->h()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final h(J)J
    .locals 2

    .prologue
    .line 198
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/v;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->h(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final i(J)J
    .locals 2

    .prologue
    .line 202
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/v;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->i(J)J

    move-result-wide v0

    return-wide v0
.end method
