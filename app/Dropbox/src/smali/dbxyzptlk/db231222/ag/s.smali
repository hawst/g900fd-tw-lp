.class public final Ldbxyzptlk/db231222/ag/s;
.super Ldbxyzptlk/db231222/ag/f;
.source "panda.py"


# static fields
.field private static final serialVersionUID:J = -0x558158f3a13L


# instance fields
.field private final a:Ldbxyzptlk/db231222/ac/a;

.field private final b:I

.field private transient c:I


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/c;)V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Ldbxyzptlk/db231222/ag/s;-><init>(Ldbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/c;I)V

    .line 54
    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/c;I)V
    .locals 2

    .prologue
    .line 64
    invoke-direct {p0, p2}, Ldbxyzptlk/db231222/ag/f;-><init>(Ldbxyzptlk/db231222/ac/c;)V

    .line 65
    iput-object p1, p0, Ldbxyzptlk/db231222/ag/s;->a:Ldbxyzptlk/db231222/ac/a;

    .line 66
    invoke-super {p0}, Ldbxyzptlk/db231222/ag/f;->g()I

    move-result v0

    .line 67
    if-ge v0, p3, :cond_0

    .line 68
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/ag/s;->c:I

    .line 74
    :goto_0
    iput p3, p0, Ldbxyzptlk/db231222/ag/s;->b:I

    .line 75
    return-void

    .line 69
    :cond_0
    add-int/lit8 v1, p3, 0x1

    if-ne v0, v1, :cond_1

    .line 70
    iput p3, p0, Ldbxyzptlk/db231222/ag/s;->c:I

    goto :goto_0

    .line 72
    :cond_1
    iput v0, p0, Ldbxyzptlk/db231222/ag/s;->c:I

    goto :goto_0
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 99
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/s;->a()Ldbxyzptlk/db231222/ac/d;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/ag/s;->a:Ldbxyzptlk/db231222/ac/a;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/ac/d;->a(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(J)I
    .locals 2

    .prologue
    .line 79
    invoke-super {p0, p1, p2}, Ldbxyzptlk/db231222/ag/f;->a(J)I

    move-result v0

    .line 80
    iget v1, p0, Ldbxyzptlk/db231222/ag/s;->b:I

    if-ge v0, v1, :cond_0

    .line 81
    add-int/lit8 v0, v0, 0x1

    .line 83
    :cond_0
    return v0
.end method

.method public final b(JI)J
    .locals 2

    .prologue
    .line 87
    iget v0, p0, Ldbxyzptlk/db231222/ag/s;->c:I

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/s;->h()I

    move-result v1

    invoke-static {p0, p3, v0, v1}, Ldbxyzptlk/db231222/ag/h;->a(Ldbxyzptlk/db231222/ac/c;III)V

    .line 88
    iget v0, p0, Ldbxyzptlk/db231222/ag/s;->b:I

    if-gt p3, v0, :cond_0

    .line 89
    add-int/lit8 p3, p3, -0x1

    .line 91
    :cond_0
    invoke-super {p0, p1, p2, p3}, Ldbxyzptlk/db231222/ag/f;->b(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Ldbxyzptlk/db231222/ag/s;->c:I

    return v0
.end method
