.class public final Ldbxyzptlk/db231222/ag/p;
.super Ldbxyzptlk/db231222/ag/d;
.source "panda.py"


# instance fields
.field final a:I

.field final b:Ldbxyzptlk/db231222/ac/l;


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/d;I)V
    .locals 3

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/ag/d;-><init>(Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/d;)V

    .line 55
    const/4 v0, 0x2

    if-ge p3, v0, :cond_0

    .line 56
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The divisor must be at least 2"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_0
    invoke-virtual {p1}, Ldbxyzptlk/db231222/ac/c;->d()Ldbxyzptlk/db231222/ac/l;

    move-result-object v0

    .line 60
    if-nez v0, :cond_1

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/ag/p;->b:Ldbxyzptlk/db231222/ac/l;

    .line 67
    :goto_0
    iput p3, p0, Ldbxyzptlk/db231222/ag/p;->a:I

    .line 68
    return-void

    .line 63
    :cond_1
    new-instance v1, Ldbxyzptlk/db231222/ag/q;

    invoke-virtual {p2}, Ldbxyzptlk/db231222/ac/d;->z()Ldbxyzptlk/db231222/ac/m;

    move-result-object v2

    invoke-direct {v1, v0, v2, p3}, Ldbxyzptlk/db231222/ag/q;-><init>(Ldbxyzptlk/db231222/ac/l;Ldbxyzptlk/db231222/ac/m;I)V

    iput-object v1, p0, Ldbxyzptlk/db231222/ag/p;->b:Ldbxyzptlk/db231222/ac/l;

    goto :goto_0
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/ag/g;)V
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p1}, Ldbxyzptlk/db231222/ag/g;->a()Ldbxyzptlk/db231222/ac/d;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Ldbxyzptlk/db231222/ag/p;-><init>(Ldbxyzptlk/db231222/ag/g;Ldbxyzptlk/db231222/ac/d;)V

    .line 78
    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/ag/g;Ldbxyzptlk/db231222/ac/d;)V
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p1}, Ldbxyzptlk/db231222/ag/g;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Ldbxyzptlk/db231222/ag/d;-><init>(Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/d;)V

    .line 89
    iget v0, p1, Ldbxyzptlk/db231222/ag/g;->a:I

    iput v0, p0, Ldbxyzptlk/db231222/ag/p;->a:I

    .line 90
    iget-object v0, p1, Ldbxyzptlk/db231222/ag/g;->b:Ldbxyzptlk/db231222/ac/l;

    iput-object v0, p0, Ldbxyzptlk/db231222/ag/p;->b:Ldbxyzptlk/db231222/ac/l;

    .line 91
    return-void
.end method

.method private a(I)I
    .locals 2

    .prologue
    .line 196
    if-ltz p1, :cond_0

    .line 197
    iget v0, p0, Ldbxyzptlk/db231222/ag/p;->a:I

    div-int v0, p1, v0

    .line 199
    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, p1, 0x1

    iget v1, p0, Ldbxyzptlk/db231222/ag/p;->a:I

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(J)I
    .locals 3

    .prologue
    .line 101
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/p;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->a(J)I

    move-result v0

    .line 102
    if-ltz v0, :cond_0

    .line 103
    iget v1, p0, Ldbxyzptlk/db231222/ag/p;->a:I

    rem-int/2addr v0, v1

    .line 105
    :goto_0
    return v0

    :cond_0
    iget v1, p0, Ldbxyzptlk/db231222/ag/p;->a:I

    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v0, v0, 0x1

    iget v2, p0, Ldbxyzptlk/db231222/ag/p;->a:I

    rem-int/2addr v0, v2

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final b(JI)J
    .locals 3

    .prologue
    .line 131
    const/4 v0, 0x0

    iget v1, p0, Ldbxyzptlk/db231222/ag/p;->a:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {p0, p3, v0, v1}, Ldbxyzptlk/db231222/ag/h;->a(Ldbxyzptlk/db231222/ac/c;III)V

    .line 132
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/p;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->a(J)I

    move-result v0

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/ag/p;->a(I)I

    move-result v0

    .line 133
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/p;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iget v2, p0, Ldbxyzptlk/db231222/ag/p;->a:I

    mul-int/2addr v0, v2

    add-int/2addr v0, p3

    invoke-virtual {v1, p1, p2, v0}, Ldbxyzptlk/db231222/ac/c;->b(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method public final d(J)J
    .locals 2

    .prologue
    .line 163
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/p;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->d(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final e(J)J
    .locals 2

    .prologue
    .line 167
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/p;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->e(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final e()Ldbxyzptlk/db231222/ac/l;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/p;->b:Ldbxyzptlk/db231222/ac/l;

    return-object v0
.end method

.method public final f(J)J
    .locals 2

    .prologue
    .line 171
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/p;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->f(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    return v0
.end method

.method public final g(J)J
    .locals 2

    .prologue
    .line 175
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/p;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->g(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Ldbxyzptlk/db231222/ag/p;->a:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final h(J)J
    .locals 2

    .prologue
    .line 179
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/p;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->h(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final i(J)J
    .locals 2

    .prologue
    .line 183
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/p;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->i(J)J

    move-result-wide v0

    return-wide v0
.end method
