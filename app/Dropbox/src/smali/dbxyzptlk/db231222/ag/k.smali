.class public final Ldbxyzptlk/db231222/ag/k;
.super Ldbxyzptlk/db231222/ac/l;
.source "panda.py"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Ldbxyzptlk/db231222/ac/l;

.field private static final serialVersionUID:J = 0x24de85b89b81f517L


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Ldbxyzptlk/db231222/ag/k;

    invoke-direct {v0}, Ldbxyzptlk/db231222/ag/k;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/ag/k;->a:Ldbxyzptlk/db231222/ac/l;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ldbxyzptlk/db231222/ac/l;-><init>()V

    .line 45
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 171
    sget-object v0, Ldbxyzptlk/db231222/ag/k;->a:Ldbxyzptlk/db231222/ac/l;

    return-object v0
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/ac/l;)I
    .locals 5

    .prologue
    .line 134
    invoke-virtual {p1}, Ldbxyzptlk/db231222/ac/l;->d()J

    move-result-wide v0

    .line 135
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/k;->d()J

    move-result-wide v2

    .line 137
    cmp-long v4, v2, v0

    if-nez v4, :cond_0

    .line 138
    const/4 v0, 0x0

    .line 143
    :goto_0
    return v0

    .line 140
    :cond_0
    cmp-long v0, v2, v0

    if-gez v0, :cond_1

    .line 141
    const/4 v0, -0x1

    goto :goto_0

    .line 143
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(JI)J
    .locals 2

    .prologue
    .line 117
    int-to-long v0, p3

    invoke-static {p1, p2, v0, v1}, Ldbxyzptlk/db231222/ag/h;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(JJ)J
    .locals 2

    .prologue
    .line 121
    invoke-static {p1, p2, p3, p4}, Ldbxyzptlk/db231222/ag/h;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a()Ldbxyzptlk/db231222/ac/m;
    .locals 1

    .prologue
    .line 49
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->a()Ldbxyzptlk/db231222/ac/m;

    move-result-object v0

    return-object v0
.end method

.method public final b(JJ)I
    .locals 2

    .prologue
    .line 125
    invoke-static {p1, p2, p3, p4}, Ldbxyzptlk/db231222/ag/h;->b(JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/ag/h;->a(J)I

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x1

    return v0
.end method

.method public final c(JJ)J
    .locals 2

    .prologue
    .line 129
    invoke-static {p1, p2, p3, p4}, Ldbxyzptlk/db231222/ag/h;->b(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 32
    check-cast p1, Ldbxyzptlk/db231222/ac/l;

    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/ag/k;->a(Ldbxyzptlk/db231222/ac/l;)I

    move-result v0

    return v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 80
    const-wide/16 v0, 0x1

    return-wide v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 148
    instance-of v1, p1, Ldbxyzptlk/db231222/ag/k;

    if-eqz v1, :cond_0

    .line 149
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/k;->d()J

    move-result-wide v1

    check-cast p1, Ldbxyzptlk/db231222/ag/k;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/ag/k;->d()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 151
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 155
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/k;->d()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    const-string v0, "DurationField[millis]"

    return-object v0
.end method
