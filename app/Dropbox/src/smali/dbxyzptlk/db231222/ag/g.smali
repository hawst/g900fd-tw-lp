.class public final Ldbxyzptlk/db231222/ag/g;
.super Ldbxyzptlk/db231222/ag/d;
.source "panda.py"


# instance fields
.field final a:I

.field final b:Ldbxyzptlk/db231222/ac/l;

.field private final c:I

.field private final d:I


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/d;I)V
    .locals 3

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/ag/d;-><init>(Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/d;)V

    .line 59
    const/4 v0, 0x2

    if-ge p3, v0, :cond_0

    .line 60
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The divisor must be at least 2"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_0
    invoke-virtual {p1}, Ldbxyzptlk/db231222/ac/c;->d()Ldbxyzptlk/db231222/ac/l;

    move-result-object v0

    .line 64
    if-nez v0, :cond_1

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/ag/g;->b:Ldbxyzptlk/db231222/ac/l;

    .line 71
    :goto_0
    iput p3, p0, Ldbxyzptlk/db231222/ag/g;->a:I

    .line 73
    invoke-virtual {p1}, Ldbxyzptlk/db231222/ac/c;->g()I

    move-result v0

    .line 74
    if-ltz v0, :cond_2

    div-int/2addr v0, p3

    .line 76
    :goto_1
    invoke-virtual {p1}, Ldbxyzptlk/db231222/ac/c;->h()I

    move-result v1

    .line 77
    if-ltz v1, :cond_3

    div-int/2addr v1, p3

    .line 79
    :goto_2
    iput v0, p0, Ldbxyzptlk/db231222/ag/g;->c:I

    .line 80
    iput v1, p0, Ldbxyzptlk/db231222/ag/g;->d:I

    .line 81
    return-void

    .line 67
    :cond_1
    new-instance v1, Ldbxyzptlk/db231222/ag/q;

    invoke-virtual {p2}, Ldbxyzptlk/db231222/ac/d;->y()Ldbxyzptlk/db231222/ac/m;

    move-result-object v2

    invoke-direct {v1, v0, v2, p3}, Ldbxyzptlk/db231222/ag/q;-><init>(Ldbxyzptlk/db231222/ac/l;Ldbxyzptlk/db231222/ac/m;I)V

    iput-object v1, p0, Ldbxyzptlk/db231222/ag/g;->b:Ldbxyzptlk/db231222/ac/l;

    goto :goto_0

    .line 74
    :cond_2
    add-int/lit8 v0, v0, 0x1

    div-int/2addr v0, p3

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 77
    :cond_3
    add-int/lit8 v1, v1, 0x1

    div-int/2addr v1, p3

    add-int/lit8 v1, v1, -0x1

    goto :goto_2
.end method

.method private a(I)I
    .locals 3

    .prologue
    .line 223
    if-ltz p1, :cond_0

    .line 224
    iget v0, p0, Ldbxyzptlk/db231222/ag/g;->a:I

    rem-int v0, p1, v0

    .line 226
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Ldbxyzptlk/db231222/ag/g;->a:I

    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v1, p1, 0x1

    iget v2, p0, Ldbxyzptlk/db231222/ag/g;->a:I

    rem-int/2addr v1, v2

    add-int/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(J)I
    .locals 2

    .prologue
    .line 113
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/g;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->a(J)I

    move-result v0

    .line 114
    if-ltz v0, :cond_0

    .line 115
    iget v1, p0, Ldbxyzptlk/db231222/ag/g;->a:I

    div-int/2addr v0, v1

    .line 117
    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Ldbxyzptlk/db231222/ag/g;->a:I

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public final a(JI)J
    .locals 2

    .prologue
    .line 130
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/g;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    iget v1, p0, Ldbxyzptlk/db231222/ag/g;->a:I

    mul-int/2addr v1, p3

    invoke-virtual {v0, p1, p2, v1}, Ldbxyzptlk/db231222/ac/c;->a(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(JJ)J
    .locals 3

    .prologue
    .line 142
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/g;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    iget v1, p0, Ldbxyzptlk/db231222/ag/g;->a:I

    int-to-long v1, v1

    mul-long/2addr v1, p3

    invoke-virtual {v0, p1, p2, v1, v2}, Ldbxyzptlk/db231222/ac/c;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final b(JJ)I
    .locals 2

    .prologue
    .line 158
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/g;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ac/c;->b(JJ)I

    move-result v0

    iget v1, p0, Ldbxyzptlk/db231222/ag/g;->a:I

    div-int/2addr v0, v1

    return v0
.end method

.method public final b(JI)J
    .locals 3

    .prologue
    .line 174
    iget v0, p0, Ldbxyzptlk/db231222/ag/g;->c:I

    iget v1, p0, Ldbxyzptlk/db231222/ag/g;->d:I

    invoke-static {p0, p3, v0, v1}, Ldbxyzptlk/db231222/ag/h;->a(Ldbxyzptlk/db231222/ac/c;III)V

    .line 175
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/g;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->a(J)I

    move-result v0

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/ag/g;->a(I)I

    move-result v0

    .line 176
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/g;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iget v2, p0, Ldbxyzptlk/db231222/ag/g;->a:I

    mul-int/2addr v2, p3

    add-int/2addr v0, v2

    invoke-virtual {v1, p1, p2, v0}, Ldbxyzptlk/db231222/ac/c;->b(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c(JJ)J
    .locals 4

    .prologue
    .line 162
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/g;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ac/c;->c(JJ)J

    move-result-wide v0

    iget v2, p0, Ldbxyzptlk/db231222/ag/g;->a:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public final d(J)J
    .locals 3

    .prologue
    .line 205
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/g;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    .line 206
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/ag/g;->a(J)I

    move-result v1

    iget v2, p0, Ldbxyzptlk/db231222/ag/g;->a:I

    mul-int/2addr v1, v2

    invoke-virtual {v0, p1, p2, v1}, Ldbxyzptlk/db231222/ac/c;->b(JI)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/ac/c;->d(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final d()Ldbxyzptlk/db231222/ac/l;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Ldbxyzptlk/db231222/ag/g;->b:Ldbxyzptlk/db231222/ac/l;

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 192
    iget v0, p0, Ldbxyzptlk/db231222/ag/g;->c:I

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Ldbxyzptlk/db231222/ag/g;->d:I

    return v0
.end method

.method public final i(J)J
    .locals 2

    .prologue
    .line 210
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/g;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->i(J)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ldbxyzptlk/db231222/ag/g;->a(J)I

    move-result v0

    invoke-virtual {p0, p1, p2, v0}, Ldbxyzptlk/db231222/ag/g;->b(JI)J

    move-result-wide v0

    return-wide v0
.end method
