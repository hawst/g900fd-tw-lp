.class public final Ldbxyzptlk/db231222/ag/r;
.super Ldbxyzptlk/db231222/ag/f;
.source "panda.py"


# static fields
.field private static final serialVersionUID:J = -0x7b158b898ffc2946L


# instance fields
.field private final a:Ldbxyzptlk/db231222/ac/a;

.field private final b:I

.field private transient c:I


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/c;)V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Ldbxyzptlk/db231222/ag/r;-><init>(Ldbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/c;I)V

    .line 55
    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/c;I)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0, p2}, Ldbxyzptlk/db231222/ag/f;-><init>(Ldbxyzptlk/db231222/ac/c;)V

    .line 66
    iput-object p1, p0, Ldbxyzptlk/db231222/ag/r;->a:Ldbxyzptlk/db231222/ac/a;

    .line 67
    invoke-super {p0}, Ldbxyzptlk/db231222/ag/f;->g()I

    move-result v0

    .line 68
    if-ge v0, p3, :cond_0

    .line 69
    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Ldbxyzptlk/db231222/ag/r;->c:I

    .line 75
    :goto_0
    iput p3, p0, Ldbxyzptlk/db231222/ag/r;->b:I

    .line 76
    return-void

    .line 70
    :cond_0
    if-ne v0, p3, :cond_1

    .line 71
    add-int/lit8 v0, p3, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/ag/r;->c:I

    goto :goto_0

    .line 73
    :cond_1
    iput v0, p0, Ldbxyzptlk/db231222/ag/r;->c:I

    goto :goto_0
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 104
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/r;->a()Ldbxyzptlk/db231222/ac/d;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/ag/r;->a:Ldbxyzptlk/db231222/ac/a;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/ac/d;->a(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(J)I
    .locals 2

    .prologue
    .line 80
    invoke-super {p0, p1, p2}, Ldbxyzptlk/db231222/ag/f;->a(J)I

    move-result v0

    .line 81
    iget v1, p0, Ldbxyzptlk/db231222/ag/r;->b:I

    if-gt v0, v1, :cond_0

    .line 82
    add-int/lit8 v0, v0, -0x1

    .line 84
    :cond_0
    return v0
.end method

.method public final b(JI)J
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 88
    iget v0, p0, Ldbxyzptlk/db231222/ag/r;->c:I

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ag/r;->h()I

    move-result v1

    invoke-static {p0, p3, v0, v1}, Ldbxyzptlk/db231222/ag/h;->a(Ldbxyzptlk/db231222/ac/c;III)V

    .line 89
    iget v0, p0, Ldbxyzptlk/db231222/ag/r;->b:I

    if-gt p3, v0, :cond_1

    .line 90
    iget v0, p0, Ldbxyzptlk/db231222/ag/r;->b:I

    if-ne p3, v0, :cond_0

    .line 91
    new-instance v0, Ldbxyzptlk/db231222/ac/o;

    invoke-static {}, Ldbxyzptlk/db231222/ac/d;->s()Ldbxyzptlk/db231222/ac/d;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3, v3}, Ldbxyzptlk/db231222/ac/o;-><init>(Ldbxyzptlk/db231222/ac/d;Ljava/lang/Number;Ljava/lang/Number;Ljava/lang/Number;)V

    throw v0

    .line 94
    :cond_0
    add-int/lit8 p3, p3, 0x1

    .line 96
    :cond_1
    invoke-super {p0, p1, p2, p3}, Ldbxyzptlk/db231222/ag/f;->b(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Ldbxyzptlk/db231222/ag/r;->c:I

    return v0
.end method
