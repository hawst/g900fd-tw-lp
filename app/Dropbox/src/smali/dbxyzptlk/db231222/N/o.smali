.class public final enum Ldbxyzptlk/db231222/N/o;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldbxyzptlk/db231222/N/o;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ldbxyzptlk/db231222/N/o;

.field public static final enum b:Ldbxyzptlk/db231222/N/o;

.field public static final enum c:Ldbxyzptlk/db231222/N/o;

.field public static final enum d:Ldbxyzptlk/db231222/N/o;

.field private static final e:[Ldbxyzptlk/db231222/N/o;

.field private static final synthetic g:[Ldbxyzptlk/db231222/N/o;


# instance fields
.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 28
    new-instance v0, Ldbxyzptlk/db231222/N/o;

    const-string v1, "L"

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/N/o;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldbxyzptlk/db231222/N/o;->a:Ldbxyzptlk/db231222/N/o;

    .line 30
    new-instance v0, Ldbxyzptlk/db231222/N/o;

    const-string v1, "M"

    invoke-direct {v0, v1, v3, v2}, Ldbxyzptlk/db231222/N/o;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldbxyzptlk/db231222/N/o;->b:Ldbxyzptlk/db231222/N/o;

    .line 32
    new-instance v0, Ldbxyzptlk/db231222/N/o;

    const-string v1, "Q"

    invoke-direct {v0, v1, v4, v5}, Ldbxyzptlk/db231222/N/o;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldbxyzptlk/db231222/N/o;->c:Ldbxyzptlk/db231222/N/o;

    .line 34
    new-instance v0, Ldbxyzptlk/db231222/N/o;

    const-string v1, "H"

    invoke-direct {v0, v1, v5, v4}, Ldbxyzptlk/db231222/N/o;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldbxyzptlk/db231222/N/o;->d:Ldbxyzptlk/db231222/N/o;

    .line 25
    new-array v0, v6, [Ldbxyzptlk/db231222/N/o;

    sget-object v1, Ldbxyzptlk/db231222/N/o;->a:Ldbxyzptlk/db231222/N/o;

    aput-object v1, v0, v2

    sget-object v1, Ldbxyzptlk/db231222/N/o;->b:Ldbxyzptlk/db231222/N/o;

    aput-object v1, v0, v3

    sget-object v1, Ldbxyzptlk/db231222/N/o;->c:Ldbxyzptlk/db231222/N/o;

    aput-object v1, v0, v4

    sget-object v1, Ldbxyzptlk/db231222/N/o;->d:Ldbxyzptlk/db231222/N/o;

    aput-object v1, v0, v5

    sput-object v0, Ldbxyzptlk/db231222/N/o;->g:[Ldbxyzptlk/db231222/N/o;

    .line 36
    new-array v0, v6, [Ldbxyzptlk/db231222/N/o;

    sget-object v1, Ldbxyzptlk/db231222/N/o;->b:Ldbxyzptlk/db231222/N/o;

    aput-object v1, v0, v2

    sget-object v1, Ldbxyzptlk/db231222/N/o;->a:Ldbxyzptlk/db231222/N/o;

    aput-object v1, v0, v3

    sget-object v1, Ldbxyzptlk/db231222/N/o;->d:Ldbxyzptlk/db231222/N/o;

    aput-object v1, v0, v4

    sget-object v1, Ldbxyzptlk/db231222/N/o;->c:Ldbxyzptlk/db231222/N/o;

    aput-object v1, v0, v5

    sput-object v0, Ldbxyzptlk/db231222/N/o;->e:[Ldbxyzptlk/db231222/N/o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput p3, p0, Ldbxyzptlk/db231222/N/o;->f:I

    .line 42
    return-void
.end method

.method public static a(I)Ldbxyzptlk/db231222/N/o;
    .locals 1

    .prologue
    .line 53
    if-ltz p0, :cond_0

    sget-object v0, Ldbxyzptlk/db231222/N/o;->e:[Ldbxyzptlk/db231222/N/o;

    array-length v0, v0

    if-lt p0, v0, :cond_1

    .line 54
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 56
    :cond_1
    sget-object v0, Ldbxyzptlk/db231222/N/o;->e:[Ldbxyzptlk/db231222/N/o;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Ldbxyzptlk/db231222/N/o;
    .locals 1

    .prologue
    .line 25
    const-class v0, Ldbxyzptlk/db231222/N/o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/N/o;

    return-object v0
.end method

.method public static values()[Ldbxyzptlk/db231222/N/o;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Ldbxyzptlk/db231222/N/o;->g:[Ldbxyzptlk/db231222/N/o;

    invoke-virtual {v0}, [Ldbxyzptlk/db231222/N/o;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/N/o;

    return-object v0
.end method
