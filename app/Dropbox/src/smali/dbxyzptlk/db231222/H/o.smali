.class public final Ldbxyzptlk/db231222/H/o;
.super Ljava/io/IOException;
.source "panda.py"


# static fields
.field private static final serialVersionUID:J = -0x166db9773d0dffacL


# instance fields
.field private a:Ldbxyzptlk/db231222/H/t;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/H/o;->a:Ldbxyzptlk/db231222/H/t;

    .line 47
    return-void
.end method

.method static b()Ldbxyzptlk/db231222/H/o;
    .locals 2

    .prologue
    .line 70
    new-instance v0, Ldbxyzptlk/db231222/H/o;

    const-string v1, "While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either than the input has been truncated or that an embedded message misreported its own length."

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/H/o;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static c()Ldbxyzptlk/db231222/H/o;
    .locals 2

    .prologue
    .line 78
    new-instance v0, Ldbxyzptlk/db231222/H/o;

    const-string v1, "CodedInputStream encountered an embedded string or message which claimed to have negative size."

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/H/o;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static d()Ldbxyzptlk/db231222/H/o;
    .locals 2

    .prologue
    .line 84
    new-instance v0, Ldbxyzptlk/db231222/H/o;

    const-string v1, "CodedInputStream encountered a malformed varint."

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/H/o;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static e()Ldbxyzptlk/db231222/H/o;
    .locals 2

    .prologue
    .line 89
    new-instance v0, Ldbxyzptlk/db231222/H/o;

    const-string v1, "Protocol message contained an invalid tag (zero)."

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/H/o;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static f()Ldbxyzptlk/db231222/H/o;
    .locals 2

    .prologue
    .line 94
    new-instance v0, Ldbxyzptlk/db231222/H/o;

    const-string v1, "Protocol message end-group tag did not match expected tag."

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/H/o;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static g()Ldbxyzptlk/db231222/H/o;
    .locals 2

    .prologue
    .line 99
    new-instance v0, Ldbxyzptlk/db231222/H/o;

    const-string v1, "Protocol message tag had invalid wire type."

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/H/o;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static h()Ldbxyzptlk/db231222/H/o;
    .locals 2

    .prologue
    .line 104
    new-instance v0, Ldbxyzptlk/db231222/H/o;

    const-string v1, "Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit."

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/H/o;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static i()Ldbxyzptlk/db231222/H/o;
    .locals 2

    .prologue
    .line 110
    new-instance v0, Ldbxyzptlk/db231222/H/o;

    const-string v1, "Protocol message was too large.  May be malicious.  Use CodedInputStream.setSizeLimit() to increase the size limit."

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/H/o;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/o;
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Ldbxyzptlk/db231222/H/o;->a:Ldbxyzptlk/db231222/H/t;

    .line 58
    return-object p0
.end method

.method public final a()Ldbxyzptlk/db231222/H/t;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Ldbxyzptlk/db231222/H/o;->a:Ldbxyzptlk/db231222/H/t;

    return-object v0
.end method
