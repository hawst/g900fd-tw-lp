.class public abstract Ldbxyzptlk/db231222/H/c;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/H/w;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageType::",
        "Ldbxyzptlk/db231222/H/t;",
        ">",
        "Ljava/lang/Object;",
        "Ldbxyzptlk/db231222/H/w",
        "<TMessageType;>;"
    }
.end annotation


# static fields
.field private static final a:Ldbxyzptlk/db231222/H/i;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    invoke-static {}, Ldbxyzptlk/db231222/H/i;->a()Ldbxyzptlk/db231222/H/i;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/H/c;->a:Ldbxyzptlk/db231222/H/i;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/C;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMessageType;)",
            "Ldbxyzptlk/db231222/H/C;"
        }
    .end annotation

    .prologue
    .line 56
    instance-of v0, p1, Ldbxyzptlk/db231222/H/a;

    if-eqz v0, :cond_0

    .line 57
    check-cast p1, Ldbxyzptlk/db231222/H/a;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/a;->K()Ldbxyzptlk/db231222/H/C;

    move-result-object v0

    .line 59
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ldbxyzptlk/db231222/H/C;

    invoke-direct {v0, p1}, Ldbxyzptlk/db231222/H/C;-><init>(Ldbxyzptlk/db231222/H/t;)V

    goto :goto_0
.end method

.method private b(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/t;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMessageType;)TMessageType;"
        }
    .end annotation

    .prologue
    .line 70
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ldbxyzptlk/db231222/H/t;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/H/c;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/C;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/H/C;->a()Ldbxyzptlk/db231222/H/o;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/H/o;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/o;

    move-result-object v0

    throw v0

    .line 75
    :cond_0
    return-object p1
.end method


# virtual methods
.method public final a([B)Ldbxyzptlk/db231222/H/t;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)TMessageType;"
        }
    .end annotation

    .prologue
    .line 193
    sget-object v0, Ldbxyzptlk/db231222/H/c;->a:Ldbxyzptlk/db231222/H/i;

    invoke-virtual {p0, p1, v0}, Ldbxyzptlk/db231222/H/c;->a([BLdbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    return-object v0
.end method

.method public final a([BIILdbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/H/t;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BII",
            "Ldbxyzptlk/db231222/H/i;",
            ")TMessageType;"
        }
    .end annotation

    .prologue
    .line 140
    :try_start_0
    invoke-static {p1, p2, p3}, Ldbxyzptlk/db231222/H/f;->a([BII)Ldbxyzptlk/db231222/H/f;

    move-result-object v1

    .line 141
    invoke-virtual {p0, v1, p4}, Ldbxyzptlk/db231222/H/c;->b(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/H/t;
    :try_end_0
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 143
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/H/f;->a(I)V
    :try_end_1
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 147
    return-object v0

    .line 144
    :catch_0
    move-exception v1

    .line 145
    :try_start_2
    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/H/o;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/o;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 148
    :catch_1
    move-exception v0

    .line 149
    throw v0

    .line 150
    :catch_2
    move-exception v0

    .line 151
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Reading from a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a([BLdbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/H/t;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Ldbxyzptlk/db231222/H/i;",
            ")TMessageType;"
        }
    .end annotation

    .prologue
    .line 188
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1, p2}, Ldbxyzptlk/db231222/H/c;->b([BIILdbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    return-object v0
.end method

.method public final b([BIILdbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/H/t;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BII",
            "Ldbxyzptlk/db231222/H/i;",
            ")TMessageType;"
        }
    .end annotation

    .prologue
    .line 176
    invoke-virtual {p0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/H/c;->a([BIILdbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/H/c;->b(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b([B)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/H/c;->a([B)Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    return-object v0
.end method
