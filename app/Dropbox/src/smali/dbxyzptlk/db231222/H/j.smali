.class public abstract Ldbxyzptlk/db231222/H/j;
.super Ldbxyzptlk/db231222/H/a;
.source "panda.py"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/a;-><init>()V

    .line 53
    return-void
.end method

.method protected constructor <init>(Ldbxyzptlk/db231222/H/k;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/a;-><init>()V

    .line 56
    return-void
.end method


# virtual methods
.method protected final L()V
    .locals 0

    .prologue
    .line 79
    return-void
.end method

.method protected final a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;I)Z
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p1, p3}, Ldbxyzptlk/db231222/H/f;->b(I)Z

    move-result v0

    return v0
.end method

.method public b()Ldbxyzptlk/db231222/H/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldbxyzptlk/db231222/H/w",
            "<+",
            "Ldbxyzptlk/db231222/H/t;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is supposed to be overridden by subclasses."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 795
    new-instance v0, Ldbxyzptlk/db231222/H/l;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/H/l;-><init>(Ldbxyzptlk/db231222/H/t;)V

    return-object v0
.end method
