.class public abstract Ldbxyzptlk/db231222/H/b;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/H/u;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<BuilderType:",
        "Ldbxyzptlk/db231222/H/b;",
        ">",
        "Ljava/lang/Object;",
        "Ldbxyzptlk/db231222/H/u;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 231
    return-void
.end method

.method protected static a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/C;
    .locals 1

    .prologue
    .line 306
    new-instance v0, Ldbxyzptlk/db231222/H/C;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/H/C;-><init>(Ldbxyzptlk/db231222/H/t;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Iterable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 336
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 337
    if-nez v1, :cond_0

    .line 338
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 341
    :cond_1
    return-void
.end method

.method protected static a(Ljava/lang/Iterable;Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<TT;>;",
            "Ljava/util/Collection",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 318
    instance-of v0, p0, Ldbxyzptlk/db231222/H/p;

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 321
    check-cast v0, Ldbxyzptlk/db231222/H/p;

    invoke-interface {v0}, Ldbxyzptlk/db231222/H/p;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/H/b;->a(Ljava/lang/Iterable;)V

    .line 325
    :goto_0
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_2

    .line 326
    check-cast p0, Ljava/util/Collection;

    .line 327
    invoke-interface {p1, p0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 333
    :cond_0
    return-void

    .line 323
    :cond_1
    invoke-static {p0}, Ldbxyzptlk/db231222/H/b;->a(Ljava/lang/Iterable;)V

    goto :goto_0

    .line 329
    :cond_2
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 330
    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/H/f;)Ldbxyzptlk/db231222/H/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/H/f;",
            ")TBuilderType;"
        }
    .end annotation

    .prologue
    .line 116
    invoke-static {}, Ldbxyzptlk/db231222/H/i;->a()Ldbxyzptlk/db231222/H/i;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ldbxyzptlk/db231222/H/b;->b(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/H/b;

    move-result-object v0

    return-object v0
.end method

.method public final a([B)Ldbxyzptlk/db231222/H/b;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)TBuilderType;"
        }
    .end annotation

    .prologue
    .line 161
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Ldbxyzptlk/db231222/H/b;->a([BII)Ldbxyzptlk/db231222/H/b;

    move-result-object v0

    return-object v0
.end method

.method public final a([BII)Ldbxyzptlk/db231222/H/b;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BII)TBuilderType;"
        }
    .end annotation

    .prologue
    .line 168
    :try_start_0
    invoke-static {p1, p2, p3}, Ldbxyzptlk/db231222/H/f;->a([BII)Ldbxyzptlk/db231222/H/f;

    move-result-object v0

    .line 170
    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/H/b;->a(Ldbxyzptlk/db231222/H/f;)Ldbxyzptlk/db231222/H/b;

    .line 171
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/H/f;->a(I)V
    :try_end_0
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 172
    return-object p0

    .line 173
    :catch_0
    move-exception v0

    .line 174
    throw v0

    .line 175
    :catch_1
    move-exception v0

    .line 176
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Reading from a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public abstract b(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/H/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/H/f;",
            "Ldbxyzptlk/db231222/H/i;",
            ")TBuilderType;"
        }
    .end annotation
.end method

.method public final synthetic b([B)Ldbxyzptlk/db231222/H/u;
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/H/b;->a([B)Ldbxyzptlk/db231222/H/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0}, Ldbxyzptlk/db231222/H/b;->i()Ldbxyzptlk/db231222/H/b;

    move-result-object v0

    return-object v0
.end method

.method public abstract i()Ldbxyzptlk/db231222/H/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TBuilderType;"
        }
    .end annotation
.end method
