.class public final Ldbxyzptlk/db231222/H/g;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:[B

.field private final b:I

.field private c:I

.field private final d:Ljava/io/OutputStream;


# direct methods
.method private constructor <init>([BII)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/H/g;->d:Ljava/io/OutputStream;

    .line 79
    iput-object p1, p0, Ldbxyzptlk/db231222/H/g;->a:[B

    .line 80
    iput p2, p0, Ldbxyzptlk/db231222/H/g;->c:I

    .line 81
    add-int v0, p2, p3

    iput v0, p0, Ldbxyzptlk/db231222/H/g;->b:I

    .line 82
    return-void
.end method

.method public static a([B)Ldbxyzptlk/db231222/H/g;
    .locals 2

    .prologue
    .line 116
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Ldbxyzptlk/db231222/H/g;->a([BII)Ldbxyzptlk/db231222/H/g;

    move-result-object v0

    return-object v0
.end method

.method public static a([BII)Ldbxyzptlk/db231222/H/g;
    .locals 1

    .prologue
    .line 129
    new-instance v0, Ldbxyzptlk/db231222/H/g;

    invoke-direct {v0, p0, p1, p2}, Ldbxyzptlk/db231222/H/g;-><init>([BII)V

    return-object v0
.end method

.method public static b(ILdbxyzptlk/db231222/H/d;)I
    .locals 2

    .prologue
    .line 539
    invoke-static {p0}, Ldbxyzptlk/db231222/H/g;->f(I)I

    move-result v0

    invoke-static {p1}, Ldbxyzptlk/db231222/H/g;->b(Ldbxyzptlk/db231222/H/d;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(ILdbxyzptlk/db231222/H/t;)I
    .locals 2

    .prologue
    .line 530
    invoke-static {p0}, Ldbxyzptlk/db231222/H/g;->f(I)I

    move-result v0

    invoke-static {p1}, Ldbxyzptlk/db231222/H/g;->b(Ldbxyzptlk/db231222/H/t;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(IZ)I
    .locals 2

    .prologue
    .line 489
    invoke-static {p0}, Ldbxyzptlk/db231222/H/g;->f(I)I

    move-result v0

    invoke-static {p1}, Ldbxyzptlk/db231222/H/g;->b(Z)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(Ldbxyzptlk/db231222/H/d;)I
    .locals 2

    .prologue
    .line 767
    invoke-virtual {p0}, Ldbxyzptlk/db231222/H/d;->b()I

    move-result v0

    invoke-static {v0}, Ldbxyzptlk/db231222/H/g;->h(I)I

    move-result v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/H/d;->b()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(Ldbxyzptlk/db231222/H/t;)I
    .locals 2

    .prologue
    .line 749
    invoke-interface {p0}, Ldbxyzptlk/db231222/H/t;->f()I

    move-result v0

    .line 750
    invoke-static {v0}, Ldbxyzptlk/db231222/H/g;->h(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(Z)I
    .locals 1

    .prologue
    .line 706
    const/4 v0, 0x1

    return v0
.end method

.method public static c(I)I
    .locals 1

    .prologue
    .line 677
    if-ltz p0, :cond_0

    .line 678
    invoke-static {p0}, Ldbxyzptlk/db231222/H/g;->h(I)I

    move-result v0

    .line 681
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xa

    goto :goto_0
.end method

.method public static c(II)I
    .locals 2

    .prologue
    .line 462
    invoke-static {p0}, Ldbxyzptlk/db231222/H/g;->f(I)I

    move-result v0

    invoke-static {p1}, Ldbxyzptlk/db231222/H/g;->c(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static c(IJ)I
    .locals 2

    .prologue
    .line 446
    invoke-static {p0}, Ldbxyzptlk/db231222/H/g;->f(I)I

    move-result v0

    invoke-static {p1, p2}, Ldbxyzptlk/db231222/H/g;->c(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static c(J)I
    .locals 1

    .prologue
    .line 661
    invoke-static {p0, p1}, Ldbxyzptlk/db231222/H/g;->f(J)I

    move-result v0

    return v0
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 826
    iget-object v0, p0, Ldbxyzptlk/db231222/H/g;->d:Ljava/io/OutputStream;

    if-nez v0, :cond_0

    .line 828
    new-instance v0, Ldbxyzptlk/db231222/H/h;

    invoke-direct {v0}, Ldbxyzptlk/db231222/H/h;-><init>()V

    throw v0

    .line 833
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/H/g;->d:Ljava/io/OutputStream;

    iget-object v1, p0, Ldbxyzptlk/db231222/H/g;->a:[B

    iget v2, p0, Ldbxyzptlk/db231222/H/g;->c:I

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 834
    iput v3, p0, Ldbxyzptlk/db231222/H/g;->c:I

    .line 835
    return-void
.end method

.method public static d(I)I
    .locals 1

    .prologue
    .line 784
    invoke-static {p0}, Ldbxyzptlk/db231222/H/g;->c(I)I

    move-result v0

    return v0
.end method

.method public static d(II)I
    .locals 2

    .prologue
    .line 565
    invoke-static {p0}, Ldbxyzptlk/db231222/H/g;->f(I)I

    move-result v0

    invoke-static {p1}, Ldbxyzptlk/db231222/H/g;->d(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static d(IJ)I
    .locals 2

    .prologue
    .line 454
    invoke-static {p0}, Ldbxyzptlk/db231222/H/g;->f(I)I

    move-result v0

    invoke-static {p1, p2}, Ldbxyzptlk/db231222/H/g;->d(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static d(J)I
    .locals 1

    .prologue
    .line 669
    invoke-static {p0, p1}, Ldbxyzptlk/db231222/H/g;->f(J)I

    move-result v0

    return v0
.end method

.method public static f(I)I
    .locals 1

    .prologue
    .line 999
    const/4 v0, 0x0

    invoke-static {p0, v0}, Ldbxyzptlk/db231222/H/E;->a(II)I

    move-result v0

    invoke-static {v0}, Ldbxyzptlk/db231222/H/g;->h(I)I

    move-result v0

    return v0
.end method

.method public static f(J)I
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1046
    const-wide/16 v0, -0x80

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1055
    :goto_0
    return v0

    .line 1047
    :cond_0
    const-wide/16 v0, -0x4000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    .line 1048
    :cond_1
    const-wide/32 v0, -0x200000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    .line 1049
    :cond_2
    const-wide/32 v0, -0x10000000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    .line 1050
    :cond_3
    const-wide v0, -0x800000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    const/4 v0, 0x5

    goto :goto_0

    .line 1051
    :cond_4
    const-wide v0, -0x40000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_5

    const/4 v0, 0x6

    goto :goto_0

    .line 1052
    :cond_5
    const-wide/high16 v0, -0x2000000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_6

    const/4 v0, 0x7

    goto :goto_0

    .line 1053
    :cond_6
    const-wide/high16 v0, -0x100000000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_7

    const/16 v0, 0x8

    goto :goto_0

    .line 1054
    :cond_7
    const-wide/high16 v0, -0x8000000000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_8

    const/16 v0, 0x9

    goto :goto_0

    .line 1055
    :cond_8
    const/16 v0, 0xa

    goto :goto_0
.end method

.method public static h(I)I
    .locals 1

    .prologue
    .line 1024
    and-int/lit8 v0, p0, -0x80

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1028
    :goto_0
    return v0

    .line 1025
    :cond_0
    and-int/lit16 v0, p0, -0x4000

    if-nez v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    .line 1026
    :cond_1
    const/high16 v0, -0x200000

    and-int/2addr v0, p0

    if-nez v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    .line 1027
    :cond_2
    const/high16 v0, -0x10000000

    and-int/2addr v0, p0

    if-nez v0, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    .line 1028
    :cond_3
    const/4 v0, 0x5

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 852
    iget-object v0, p0, Ldbxyzptlk/db231222/H/g;->d:Ljava/io/OutputStream;

    if-nez v0, :cond_0

    .line 853
    iget v0, p0, Ldbxyzptlk/db231222/H/g;->b:I

    iget v1, p0, Ldbxyzptlk/db231222/H/g;->c:I

    sub-int/2addr v0, v1

    return v0

    .line 855
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "spaceLeft() can only be called on CodedOutputStreams that are writing to a flat array."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(B)V
    .locals 3

    .prologue
    .line 891
    iget v0, p0, Ldbxyzptlk/db231222/H/g;->c:I

    iget v1, p0, Ldbxyzptlk/db231222/H/g;->b:I

    if-ne v0, v1, :cond_0

    .line 892
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/g;->c()V

    .line 895
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/H/g;->a:[B

    iget v1, p0, Ldbxyzptlk/db231222/H/g;->c:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Ldbxyzptlk/db231222/H/g;->c:I

    aput-byte p1, v0, v1

    .line 896
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 327
    if-ltz p1, :cond_0

    .line 328
    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/H/g;->g(I)V

    .line 333
    :goto_0
    return-void

    .line 331
    :cond_0
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, Ldbxyzptlk/db231222/H/g;->e(J)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ldbxyzptlk/db231222/H/g;->e(II)V

    .line 166
    invoke-virtual {p0, p2}, Ldbxyzptlk/db231222/H/g;->a(I)V

    .line 167
    return-void
.end method

.method public final a(IJ)V
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ldbxyzptlk/db231222/H/g;->e(II)V

    .line 152
    invoke-virtual {p0, p2, p3}, Ldbxyzptlk/db231222/H/g;->a(J)V

    .line 153
    return-void
.end method

.method public final a(ILdbxyzptlk/db231222/H/d;)V
    .locals 1

    .prologue
    .line 228
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Ldbxyzptlk/db231222/H/g;->e(II)V

    .line 229
    invoke-virtual {p0, p2}, Ldbxyzptlk/db231222/H/g;->a(Ldbxyzptlk/db231222/H/d;)V

    .line 230
    return-void
.end method

.method public final a(ILdbxyzptlk/db231222/H/t;)V
    .locals 1

    .prologue
    .line 221
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Ldbxyzptlk/db231222/H/g;->e(II)V

    .line 222
    invoke-virtual {p0, p2}, Ldbxyzptlk/db231222/H/g;->a(Ldbxyzptlk/db231222/H/t;)V

    .line 223
    return-void
.end method

.method public final a(IZ)V
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ldbxyzptlk/db231222/H/g;->e(II)V

    .line 187
    invoke-virtual {p0, p2}, Ldbxyzptlk/db231222/H/g;->a(Z)V

    .line 188
    return-void
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 317
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/H/g;->e(J)V

    .line 318
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/H/d;)V
    .locals 1

    .prologue
    .line 385
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/d;->b()I

    move-result v0

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/H/g;->g(I)V

    .line 386
    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/H/g;->c(Ldbxyzptlk/db231222/H/d;)V

    .line 387
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/H/d;II)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 947
    iget v0, p0, Ldbxyzptlk/db231222/H/g;->b:I

    iget v1, p0, Ldbxyzptlk/db231222/H/g;->c:I

    sub-int/2addr v0, v1

    if-lt v0, p3, :cond_1

    .line 949
    iget-object v0, p0, Ldbxyzptlk/db231222/H/g;->a:[B

    iget v1, p0, Ldbxyzptlk/db231222/H/g;->c:I

    invoke-virtual {p1, v0, p2, v1, p3}, Ldbxyzptlk/db231222/H/d;->a([BIII)V

    .line 950
    iget v0, p0, Ldbxyzptlk/db231222/H/g;->c:I

    add-int/2addr v0, p3

    iput v0, p0, Ldbxyzptlk/db231222/H/g;->c:I

    .line 989
    :cond_0
    :goto_0
    return-void

    .line 954
    :cond_1
    iget v0, p0, Ldbxyzptlk/db231222/H/g;->b:I

    iget v1, p0, Ldbxyzptlk/db231222/H/g;->c:I

    sub-int/2addr v0, v1

    .line 955
    iget-object v1, p0, Ldbxyzptlk/db231222/H/g;->a:[B

    iget v2, p0, Ldbxyzptlk/db231222/H/g;->c:I

    invoke-virtual {p1, v1, p2, v2, v0}, Ldbxyzptlk/db231222/H/d;->a([BIII)V

    .line 956
    add-int v1, p2, v0

    .line 957
    sub-int v0, p3, v0

    .line 958
    iget v2, p0, Ldbxyzptlk/db231222/H/g;->b:I

    iput v2, p0, Ldbxyzptlk/db231222/H/g;->c:I

    .line 959
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/g;->c()V

    .line 964
    iget v2, p0, Ldbxyzptlk/db231222/H/g;->b:I

    if-gt v0, v2, :cond_2

    .line 966
    iget-object v2, p0, Ldbxyzptlk/db231222/H/g;->a:[B

    invoke-virtual {p1, v2, v1, v7, v0}, Ldbxyzptlk/db231222/H/d;->a([BIII)V

    .line 967
    iput v0, p0, Ldbxyzptlk/db231222/H/g;->c:I

    goto :goto_0

    .line 973
    :cond_2
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/d;->g()Ljava/io/InputStream;

    move-result-object v2

    .line 974
    int-to-long v3, v1

    int-to-long v5, v1

    invoke-virtual {v2, v5, v6}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-eqz v1, :cond_4

    .line 975
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Skip failed? Should never happen."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 984
    :cond_3
    iget-object v1, p0, Ldbxyzptlk/db231222/H/g;->d:Ljava/io/OutputStream;

    iget-object v4, p0, Ldbxyzptlk/db231222/H/g;->a:[B

    invoke-virtual {v1, v4, v7, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 985
    sub-int/2addr v0, v3

    .line 978
    :cond_4
    if-lez v0, :cond_0

    .line 979
    iget v1, p0, Ldbxyzptlk/db231222/H/g;->b:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 980
    iget-object v3, p0, Ldbxyzptlk/db231222/H/g;->a:[B

    invoke-virtual {v2, v3, v7, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    .line 981
    if-eq v3, v1, :cond_3

    .line 982
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Read failed? Should never happen"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ldbxyzptlk/db231222/H/t;)V
    .locals 1

    .prologue
    .line 379
    invoke-interface {p1}, Ldbxyzptlk/db231222/H/t;->f()I

    move-result v0

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/H/g;->g(I)V

    .line 380
    invoke-interface {p1, p0}, Ldbxyzptlk/db231222/H/t;->a(Ldbxyzptlk/db231222/H/g;)V

    .line 381
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 347
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/H/g;->e(I)V

    .line 348
    return-void

    .line 347
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 869
    invoke-virtual {p0}, Ldbxyzptlk/db231222/H/g;->a()I

    move-result v0

    if-eqz v0, :cond_0

    .line 870
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Did not write as much data as expected."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 873
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 399
    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/H/g;->a(I)V

    .line 400
    return-void
.end method

.method public final b(II)V
    .locals 1

    .prologue
    .line 245
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ldbxyzptlk/db231222/H/g;->e(II)V

    .line 246
    invoke-virtual {p0, p2}, Ldbxyzptlk/db231222/H/g;->b(I)V

    .line 247
    return-void
.end method

.method public final b(IJ)V
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ldbxyzptlk/db231222/H/g;->e(II)V

    .line 159
    invoke-virtual {p0, p2, p3}, Ldbxyzptlk/db231222/H/g;->b(J)V

    .line 160
    return-void
.end method

.method public final b(J)V
    .locals 0

    .prologue
    .line 322
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/H/g;->e(J)V

    .line 323
    return-void
.end method

.method public final c(Ldbxyzptlk/db231222/H/d;)V
    .locals 2

    .prologue
    .line 905
    const/4 v0, 0x0

    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/d;->b()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Ldbxyzptlk/db231222/H/g;->a(Ldbxyzptlk/db231222/H/d;II)V

    .line 906
    return-void
.end method

.method public final e(I)V
    .locals 1

    .prologue
    .line 900
    int-to-byte v0, p1

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/H/g;->a(B)V

    .line 901
    return-void
.end method

.method public final e(II)V
    .locals 1

    .prologue
    .line 994
    invoke-static {p1, p2}, Ldbxyzptlk/db231222/H/E;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/H/g;->g(I)V

    .line 995
    return-void
.end method

.method public final e(J)V
    .locals 4

    .prologue
    .line 1034
    :goto_0
    const-wide/16 v0, -0x80

    and-long/2addr v0, p1

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1035
    long-to-int v0, p1

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/H/g;->e(I)V

    .line 1036
    return-void

    .line 1038
    :cond_0
    long-to-int v0, p1

    and-int/lit8 v0, v0, 0x7f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/H/g;->e(I)V

    .line 1039
    const/4 v0, 0x7

    ushr-long/2addr p1, v0

    goto :goto_0
.end method

.method public final g(I)V
    .locals 1

    .prologue
    .line 1008
    :goto_0
    and-int/lit8 v0, p1, -0x80

    if-nez v0, :cond_0

    .line 1009
    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/H/g;->e(I)V

    .line 1010
    return-void

    .line 1012
    :cond_0
    and-int/lit8 v0, p1, 0x7f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/H/g;->e(I)V

    .line 1013
    ushr-int/lit8 p1, p1, 0x7

    goto :goto_0
.end method
