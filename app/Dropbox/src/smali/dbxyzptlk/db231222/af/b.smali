.class final Ldbxyzptlk/db231222/af/b;
.super Ldbxyzptlk/db231222/af/a;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/af/i;
.implements Ldbxyzptlk/db231222/af/m;


# static fields
.field static final a:Ldbxyzptlk/db231222/af/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Ldbxyzptlk/db231222/af/b;

    invoke-direct {v0}, Ldbxyzptlk/db231222/af/b;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/af/b;->a:Ldbxyzptlk/db231222/af/b;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ldbxyzptlk/db231222/af/a;-><init>()V

    .line 50
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ldbxyzptlk/db231222/ac/a;)J
    .locals 2

    .prologue
    .line 121
    check-cast p1, Ljava/util/Calendar;

    .line 122
    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Ljava/lang/Object;Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ac/a;
    .locals 4

    .prologue
    .line 94
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".BuddhistCalendar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    invoke-static {p2}, Ldbxyzptlk/db231222/ae/o;->b(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ae/o;

    move-result-object v0

    .line 107
    :goto_0
    return-object v0

    .line 96
    :cond_0
    instance-of v0, p1, Ljava/util/GregorianCalendar;

    if-eqz v0, :cond_3

    .line 97
    check-cast p1, Ljava/util/GregorianCalendar;

    .line 98
    invoke-virtual {p1}, Ljava/util/GregorianCalendar;->getGregorianChange()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 99
    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 100
    invoke-static {p2}, Ldbxyzptlk/db231222/ae/y;->b(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ae/y;

    move-result-object v0

    goto :goto_0

    .line 101
    :cond_1
    const-wide v2, 0x7fffffffffffffffL

    cmp-long v2, v0, v2

    if-nez v2, :cond_2

    .line 102
    invoke-static {p2}, Ldbxyzptlk/db231222/ae/C;->b(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ae/C;

    move-result-object v0

    goto :goto_0

    .line 104
    :cond_2
    const/4 v2, 0x4

    invoke-static {p2, v0, v1, v2}, Ldbxyzptlk/db231222/ae/p;->a(Ldbxyzptlk/db231222/ac/i;JI)Ldbxyzptlk/db231222/ae/p;

    move-result-object v0

    goto :goto_0

    .line 107
    :cond_3
    invoke-static {p2}, Ldbxyzptlk/db231222/ae/z;->b(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ae/z;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 132
    const-class v0, Ljava/util/Calendar;

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ac/a;
    .locals 1

    .prologue
    .line 68
    if-eqz p2, :cond_0

    .line 79
    :goto_0
    return-object p2

    .line 71
    :cond_0
    check-cast p1, Ljava/util/Calendar;

    .line 74
    :try_start_0
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/ac/i;->a(Ljava/util/TimeZone;)Ldbxyzptlk/db231222/ac/i;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 79
    :goto_1
    invoke-virtual {p0, p1, v0}, Ldbxyzptlk/db231222/af/b;->a(Ljava/lang/Object;Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ac/a;

    move-result-object p2

    goto :goto_0

    .line 76
    :catch_0
    move-exception v0

    .line 77
    invoke-static {}, Ldbxyzptlk/db231222/ac/i;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v0

    goto :goto_1
.end method
