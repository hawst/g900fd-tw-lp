.class public final Ldbxyzptlk/db231222/Q/r;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/net/URLStreamHandlerFactory;


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ldbxyzptlk/db231222/Q/w;

.field private final c:Ldbxyzptlk/db231222/Q/e;

.field private d:Ljava/net/Proxy;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/net/ProxySelector;

.field private g:Ljava/net/CookieHandler;

.field private h:Ljava/net/ResponseCache;

.field private i:Ljavax/net/ssl/SSLSocketFactory;

.field private j:Ljavax/net/ssl/HostnameVerifier;

.field private k:Ldbxyzptlk/db231222/Q/o;

.field private l:Ldbxyzptlk/db231222/Q/c;

.field private m:Z

.field private n:I

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 42
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "spdy/3"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "http/1.1"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/R/v;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/Q/r;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/db231222/Q/r;->m:Z

    .line 61
    new-instance v0, Ldbxyzptlk/db231222/Q/w;

    invoke-direct {v0}, Ldbxyzptlk/db231222/Q/w;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/Q/r;->b:Ldbxyzptlk/db231222/Q/w;

    .line 62
    new-instance v0, Ldbxyzptlk/db231222/Q/e;

    invoke-direct {v0}, Ldbxyzptlk/db231222/Q/e;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/Q/r;->c:Ldbxyzptlk/db231222/Q/e;

    .line 63
    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/Q/r;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/db231222/Q/r;->m:Z

    .line 66
    iget-object v0, p1, Ldbxyzptlk/db231222/Q/r;->b:Ldbxyzptlk/db231222/Q/w;

    iput-object v0, p0, Ldbxyzptlk/db231222/Q/r;->b:Ldbxyzptlk/db231222/Q/w;

    .line 67
    iget-object v0, p1, Ldbxyzptlk/db231222/Q/r;->c:Ldbxyzptlk/db231222/Q/e;

    iput-object v0, p0, Ldbxyzptlk/db231222/Q/r;->c:Ldbxyzptlk/db231222/Q/e;

    .line 68
    return-void
.end method

.method private n()Ldbxyzptlk/db231222/Q/r;
    .locals 2

    .prologue
    .line 357
    new-instance v1, Ldbxyzptlk/db231222/Q/r;

    invoke-direct {v1, p0}, Ldbxyzptlk/db231222/Q/r;-><init>(Ldbxyzptlk/db231222/Q/r;)V

    .line 358
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->d:Ljava/net/Proxy;

    iput-object v0, v1, Ldbxyzptlk/db231222/Q/r;->d:Ljava/net/Proxy;

    .line 359
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->f:Ljava/net/ProxySelector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->f:Ljava/net/ProxySelector;

    :goto_0
    iput-object v0, v1, Ldbxyzptlk/db231222/Q/r;->f:Ljava/net/ProxySelector;

    .line 360
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->g:Ljava/net/CookieHandler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->g:Ljava/net/CookieHandler;

    :goto_1
    iput-object v0, v1, Ldbxyzptlk/db231222/Q/r;->g:Ljava/net/CookieHandler;

    .line 361
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->h:Ljava/net/ResponseCache;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->h:Ljava/net/ResponseCache;

    :goto_2
    iput-object v0, v1, Ldbxyzptlk/db231222/Q/r;->h:Ljava/net/ResponseCache;

    .line 362
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->i:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->i:Ljavax/net/ssl/SSLSocketFactory;

    :goto_3
    iput-object v0, v1, Ldbxyzptlk/db231222/Q/r;->i:Ljavax/net/ssl/SSLSocketFactory;

    .line 365
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->j:Ljavax/net/ssl/HostnameVerifier;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->j:Ljavax/net/ssl/HostnameVerifier;

    :goto_4
    iput-object v0, v1, Ldbxyzptlk/db231222/Q/r;->j:Ljavax/net/ssl/HostnameVerifier;

    .line 368
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->k:Ldbxyzptlk/db231222/Q/o;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->k:Ldbxyzptlk/db231222/Q/o;

    :goto_5
    iput-object v0, v1, Ldbxyzptlk/db231222/Q/r;->k:Ldbxyzptlk/db231222/Q/o;

    .line 371
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->l:Ldbxyzptlk/db231222/Q/c;

    if-eqz v0, :cond_6

    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->l:Ldbxyzptlk/db231222/Q/c;

    :goto_6
    iput-object v0, v1, Ldbxyzptlk/db231222/Q/r;->l:Ldbxyzptlk/db231222/Q/c;

    .line 372
    iget-boolean v0, p0, Ldbxyzptlk/db231222/Q/r;->m:Z

    iput-boolean v0, v1, Ldbxyzptlk/db231222/Q/r;->m:Z

    .line 373
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->e:Ljava/util/List;

    if-eqz v0, :cond_7

    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->e:Ljava/util/List;

    :goto_7
    iput-object v0, v1, Ldbxyzptlk/db231222/Q/r;->e:Ljava/util/List;

    .line 374
    iget v0, p0, Ldbxyzptlk/db231222/Q/r;->n:I

    iput v0, v1, Ldbxyzptlk/db231222/Q/r;->n:I

    .line 375
    iget v0, p0, Ldbxyzptlk/db231222/Q/r;->o:I

    iput v0, v1, Ldbxyzptlk/db231222/Q/r;->o:I

    .line 376
    return-object v1

    .line 359
    :cond_0
    invoke-static {}, Ljava/net/ProxySelector;->getDefault()Ljava/net/ProxySelector;

    move-result-object v0

    goto :goto_0

    .line 360
    :cond_1
    invoke-static {}, Ljava/net/CookieHandler;->getDefault()Ljava/net/CookieHandler;

    move-result-object v0

    goto :goto_1

    .line 361
    :cond_2
    invoke-static {}, Ljava/net/ResponseCache;->getDefault()Ljava/net/ResponseCache;

    move-result-object v0

    goto :goto_2

    .line 362
    :cond_3
    invoke-static {}, Ljavax/net/ssl/HttpsURLConnection;->getDefaultSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    goto :goto_3

    .line 365
    :cond_4
    sget-object v0, Ldbxyzptlk/db231222/U/b;->a:Ldbxyzptlk/db231222/U/b;

    goto :goto_4

    .line 368
    :cond_5
    sget-object v0, Ldbxyzptlk/db231222/S/d;->a:Ldbxyzptlk/db231222/Q/o;

    goto :goto_5

    .line 371
    :cond_6
    invoke-static {}, Ldbxyzptlk/db231222/Q/c;->a()Ldbxyzptlk/db231222/Q/c;

    move-result-object v0

    goto :goto_6

    .line 373
    :cond_7
    sget-object v0, Ldbxyzptlk/db231222/Q/r;->a:Ljava/util/List;

    goto :goto_7
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Ldbxyzptlk/db231222/Q/r;->n:I

    return v0
.end method

.method public final a(Ljava/net/ResponseCache;)Ldbxyzptlk/db231222/Q/r;
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Ldbxyzptlk/db231222/Q/r;->h:Ljava/net/ResponseCache;

    .line 175
    return-object p0
.end method

.method public final a(Ljava/util/List;)Ldbxyzptlk/db231222/Q/r;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ldbxyzptlk/db231222/Q/r;"
        }
    .end annotation

    .prologue
    .line 302
    invoke-static {p1}, Ldbxyzptlk/db231222/R/v;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 303
    const-string v1, "http/1.1"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 304
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "transports doesn\'t contain http/1.1: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 306
    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 307
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "transports must not contain null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 309
    :cond_1
    const-string v1, ""

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 310
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "transports contains an empty string"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 312
    :cond_2
    iput-object v0, p0, Ldbxyzptlk/db231222/Q/r;->e:Ljava/util/List;

    .line 313
    return-object p0
.end method

.method public final a(Ljavax/net/ssl/HostnameVerifier;)Ldbxyzptlk/db231222/Q/r;
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Ldbxyzptlk/db231222/Q/r;->j:Ljavax/net/ssl/HostnameVerifier;

    .line 216
    return-object p0
.end method

.method public final a(Ljavax/net/ssl/SSLSocketFactory;)Ldbxyzptlk/db231222/Q/r;
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Ldbxyzptlk/db231222/Q/r;->i:Ljavax/net/ssl/SSLSocketFactory;

    .line 200
    return-object p0
.end method

.method public final a(Ljava/net/URL;)Ljava/net/HttpURLConnection;
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->d:Ljava/net/Proxy;

    invoke-virtual {p0, p1, v0}, Ldbxyzptlk/db231222/Q/r;->a(Ljava/net/URL;Ljava/net/Proxy;)Ljava/net/HttpURLConnection;

    move-result-object v0

    return-object v0
.end method

.method final a(Ljava/net/URL;Ljava/net/Proxy;)Ljava/net/HttpURLConnection;
    .locals 4

    .prologue
    .line 343
    invoke-virtual {p1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    .line 344
    invoke-direct {p0}, Ldbxyzptlk/db231222/Q/r;->n()Ldbxyzptlk/db231222/Q/r;

    move-result-object v1

    .line 345
    iput-object p2, v1, Ldbxyzptlk/db231222/Q/r;->d:Ljava/net/Proxy;

    .line 347
    const-string v2, "http"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v0, Ldbxyzptlk/db231222/S/p;

    invoke-direct {v0, p1, v1}, Ldbxyzptlk/db231222/S/p;-><init>(Ljava/net/URL;Ldbxyzptlk/db231222/Q/r;)V

    .line 348
    :goto_0
    return-object v0

    :cond_0
    const-string v2, "https"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v0, Ldbxyzptlk/db231222/S/s;

    invoke-direct {v0, p1, v1}, Ldbxyzptlk/db231222/S/s;-><init>(Ljava/net/URL;Ldbxyzptlk/db231222/Q/r;)V

    goto :goto_0

    .line 349
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected protocol: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final a(JLjava/util/concurrent/TimeUnit;)V
    .locals 4

    .prologue
    .line 76
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 77
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "timeout < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_0
    if-nez p3, :cond_1

    .line 80
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unit == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_1
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 83
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 84
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Timeout too large."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_2
    long-to-int v0, v0

    iput v0, p0, Ldbxyzptlk/db231222/Q/r;->n:I

    .line 87
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Ldbxyzptlk/db231222/Q/r;->o:I

    return v0
.end method

.method public final b(JLjava/util/concurrent/TimeUnit;)V
    .locals 4

    .prologue
    .line 100
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 101
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "timeout < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_0
    if-nez p3, :cond_1

    .line 104
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unit == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :cond_1
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 107
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 108
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Timeout too large."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_2
    long-to-int v0, v0

    iput v0, p0, Ldbxyzptlk/db231222/Q/r;->o:I

    .line 111
    return-void
.end method

.method public final c()Ljava/net/Proxy;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->d:Ljava/net/Proxy;

    return-object v0
.end method

.method public final createURLStreamHandler(Ljava/lang/String;)Ljava/net/URLStreamHandler;
    .locals 1

    .prologue
    .line 390
    const-string v0, "http"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "https"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 392
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ldbxyzptlk/db231222/Q/s;

    invoke-direct {v0, p0, p1}, Ldbxyzptlk/db231222/Q/s;-><init>(Ldbxyzptlk/db231222/Q/r;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final d()Ljava/net/ProxySelector;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->f:Ljava/net/ProxySelector;

    return-object v0
.end method

.method public final e()Ljava/net/CookieHandler;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->g:Ljava/net/CookieHandler;

    return-object v0
.end method

.method public final f()Ldbxyzptlk/db231222/Q/t;
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->h:Ljava/net/ResponseCache;

    instance-of v0, v0, Ldbxyzptlk/db231222/Q/f;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->h:Ljava/net/ResponseCache;

    check-cast v0, Ldbxyzptlk/db231222/Q/f;

    iget-object v0, v0, Ldbxyzptlk/db231222/Q/f;->a:Ldbxyzptlk/db231222/Q/t;

    .line 188
    :goto_0
    return-object v0

    .line 185
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->h:Ljava/net/ResponseCache;

    if-eqz v0, :cond_1

    .line 186
    new-instance v0, Ldbxyzptlk/db231222/S/v;

    iget-object v1, p0, Ldbxyzptlk/db231222/Q/r;->h:Ljava/net/ResponseCache;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/S/v;-><init>(Ljava/net/ResponseCache;)V

    goto :goto_0

    .line 188
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljavax/net/ssl/SSLSocketFactory;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->i:Ljavax/net/ssl/SSLSocketFactory;

    return-object v0
.end method

.method public final h()Ljavax/net/ssl/HostnameVerifier;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->j:Ljavax/net/ssl/HostnameVerifier;

    return-object v0
.end method

.method public final i()Ldbxyzptlk/db231222/Q/o;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->k:Ldbxyzptlk/db231222/Q/o;

    return-object v0
.end method

.method public final j()Ldbxyzptlk/db231222/Q/c;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->l:Ldbxyzptlk/db231222/Q/c;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 267
    iget-boolean v0, p0, Ldbxyzptlk/db231222/Q/r;->m:Z

    return v0
.end method

.method public final l()Ldbxyzptlk/db231222/Q/w;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->b:Ldbxyzptlk/db231222/Q/w;

    return-object v0
.end method

.method public final m()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 317
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/r;->e:Ljava/util/List;

    return-object v0
.end method
