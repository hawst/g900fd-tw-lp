.class public final Ldbxyzptlk/db231222/Q/f;
.super Ljava/net/ResponseCache;
.source "panda.py"


# static fields
.field private static final b:[C


# instance fields
.field final a:Ldbxyzptlk/db231222/Q/t;

.field private final c:Ldbxyzptlk/db231222/R/c;

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 122
    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Ldbxyzptlk/db231222/Q/f;->b:[C

    return-void

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x61s
        0x62s
        0x63s
        0x64s
        0x65s
        0x66s
    .end array-data
.end method

.method public constructor <init>(Ljava/io/File;J)V
    .locals 2

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/net/ResponseCache;-><init>()V

    .line 146
    new-instance v0, Ldbxyzptlk/db231222/Q/g;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/Q/g;-><init>(Ldbxyzptlk/db231222/Q/f;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/Q/f;->a:Ldbxyzptlk/db231222/Q/t;

    .line 175
    const v0, 0x31191

    const/4 v1, 0x2

    invoke-static {p1, v0, v1, p2, p3}, Ldbxyzptlk/db231222/R/c;->a(Ljava/io/File;IIJ)Ldbxyzptlk/db231222/R/c;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/Q/f;->c:Ldbxyzptlk/db231222/R/c;

    .line 176
    return-void
.end method

.method private a(Ljava/net/URLConnection;)Ldbxyzptlk/db231222/S/h;
    .locals 1

    .prologue
    .line 323
    instance-of v0, p1, Ldbxyzptlk/db231222/S/p;

    if-eqz v0, :cond_0

    .line 324
    check-cast p1, Ldbxyzptlk/db231222/S/p;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/S/p;->b()Ldbxyzptlk/db231222/S/h;

    move-result-object v0

    .line 328
    :goto_0
    return-object v0

    .line 325
    :cond_0
    instance-of v0, p1, Ldbxyzptlk/db231222/S/s;

    if-eqz v0, :cond_1

    .line 326
    check-cast p1, Ldbxyzptlk/db231222/S/s;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/S/s;->a()Ldbxyzptlk/db231222/S/h;

    move-result-object v0

    goto :goto_0

    .line 328
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/R/i;)Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 121
    invoke-static {p0}, Ldbxyzptlk/db231222/Q/f;->b(Ldbxyzptlk/db231222/R/i;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/net/URI;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 180
    :try_start_0
    const-string v0, "MD5"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 181
    invoke-virtual {p1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v0

    .line 182
    invoke-static {v0}, Ldbxyzptlk/db231222/Q/f;->a([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 183
    :catch_0
    move-exception v0

    .line 184
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 185
    :catch_1
    move-exception v0

    .line 186
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method private static a([B)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 191
    sget-object v2, Ldbxyzptlk/db231222/Q/f;->b:[C

    .line 192
    array-length v1, p0

    mul-int/lit8 v1, v1, 0x2

    new-array v3, v1, [C

    .line 194
    array-length v4, p0

    move v1, v0

    :goto_0
    if-ge v0, v4, :cond_0

    aget-byte v5, p0, v0

    .line 195
    add-int/lit8 v6, v1, 0x1

    shr-int/lit8 v7, v5, 0x4

    and-int/lit8 v7, v7, 0xf

    aget-char v7, v2, v7

    aput-char v7, v3, v1

    .line 196
    add-int/lit8 v1, v6, 0x1

    and-int/lit8 v5, v5, 0xf

    aget-char v5, v2, v5

    aput-char v5, v3, v6

    .line 194
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 198
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/Q/f;)V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Ldbxyzptlk/db231222/Q/f;->b()V

    return-void
.end method

.method static synthetic a(Ldbxyzptlk/db231222/Q/f;Ldbxyzptlk/db231222/Q/u;)V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/Q/f;->a(Ldbxyzptlk/db231222/Q/u;)V

    return-void
.end method

.method static synthetic a(Ldbxyzptlk/db231222/Q/f;Ljava/net/CacheResponse;Ljava/net/HttpURLConnection;)V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/Q/f;->a(Ljava/net/CacheResponse;Ljava/net/HttpURLConnection;)V

    return-void
.end method

.method private declared-synchronized a(Ldbxyzptlk/db231222/Q/u;)V
    .locals 2

    .prologue
    .line 374
    monitor-enter p0

    :try_start_0
    iget v0, p0, Ldbxyzptlk/db231222/Q/f;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/Q/f;->h:I

    .line 376
    sget-object v0, Ldbxyzptlk/db231222/Q/i;->a:[I

    invoke-virtual {p1}, Ldbxyzptlk/db231222/Q/u;->ordinal()I

    move-result v1

    aget v0, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v0, :pswitch_data_0

    .line 385
    :goto_0
    monitor-exit p0

    return-void

    .line 378
    :pswitch_0
    :try_start_1
    iget v0, p0, Ldbxyzptlk/db231222/Q/f;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/Q/f;->g:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 374
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 382
    :pswitch_1
    :try_start_2
    iget v0, p0, Ldbxyzptlk/db231222/Q/f;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/Q/f;->f:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 376
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ldbxyzptlk/db231222/R/f;)V
    .locals 1

    .prologue
    .line 315
    if-eqz p1, :cond_0

    .line 316
    :try_start_0
    invoke-virtual {p1}, Ldbxyzptlk/db231222/R/f;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 320
    :cond_0
    :goto_0
    return-void

    .line 318
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Ljava/net/CacheResponse;Ljava/net/HttpURLConnection;)V
    .locals 3

    .prologue
    .line 291
    invoke-direct {p0, p2}, Ldbxyzptlk/db231222/Q/f;->a(Ljava/net/URLConnection;)Ldbxyzptlk/db231222/S/h;

    move-result-object v0

    .line 292
    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/h;->a()Ljava/net/URI;

    move-result-object v1

    .line 293
    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/h;->i()Ldbxyzptlk/db231222/S/B;

    move-result-object v2

    .line 294
    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/h;->h()Ldbxyzptlk/db231222/S/z;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/S/z;->c()Ldbxyzptlk/db231222/S/x;

    move-result-object v0

    invoke-virtual {v2}, Ldbxyzptlk/db231222/S/B;->g()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/S/x;->a(Ljava/util/Set;)Ldbxyzptlk/db231222/S/x;

    move-result-object v0

    .line 296
    new-instance v2, Ldbxyzptlk/db231222/Q/l;

    invoke-direct {v2, v1, v0, p2}, Ldbxyzptlk/db231222/Q/l;-><init>(Ljava/net/URI;Ldbxyzptlk/db231222/S/x;Ljava/net/HttpURLConnection;)V

    .line 297
    instance-of v0, p1, Ldbxyzptlk/db231222/Q/m;

    if-eqz v0, :cond_1

    check-cast p1, Ldbxyzptlk/db231222/Q/m;

    invoke-static {p1}, Ldbxyzptlk/db231222/Q/m;->a(Ldbxyzptlk/db231222/Q/m;)Ldbxyzptlk/db231222/R/i;

    move-result-object v0

    .line 300
    :goto_0
    const/4 v1, 0x0

    .line 302
    :try_start_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/R/i;->a()Ldbxyzptlk/db231222/R/f;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 303
    if-eqz v0, :cond_0

    .line 304
    :try_start_1
    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/Q/l;->a(Ldbxyzptlk/db231222/R/f;)V

    .line 305
    invoke-virtual {v0}, Ldbxyzptlk/db231222/R/f;->a()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 310
    :cond_0
    :goto_1
    return-void

    .line 297
    :cond_1
    check-cast p1, Ldbxyzptlk/db231222/Q/n;

    invoke-static {p1}, Ldbxyzptlk/db231222/Q/n;->a(Ldbxyzptlk/db231222/Q/n;)Ldbxyzptlk/db231222/R/i;

    move-result-object v0

    goto :goto_0

    .line 307
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 308
    :goto_2
    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/Q/f;->a(Ldbxyzptlk/db231222/R/f;)V

    goto :goto_1

    .line 307
    :catch_1
    move-exception v1

    goto :goto_2
.end method

.method static synthetic a(Ldbxyzptlk/db231222/Q/f;Ljava/lang/String;Ljava/net/URI;)Z
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/Q/f;->a(Ljava/lang/String;Ljava/net/URI;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;Ljava/net/URI;)Z
    .locals 2

    .prologue
    .line 277
    const-string v0, "POST"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "PUT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "DELETE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 280
    :cond_0
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/f;->c:Ldbxyzptlk/db231222/R/c;

    invoke-direct {p0, p2}, Ldbxyzptlk/db231222/Q/f;->a(Ljava/net/URI;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/R/c;->c(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 284
    :goto_0
    const/4 v0, 0x1

    .line 286
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 281
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic b(Ldbxyzptlk/db231222/Q/f;)I
    .locals 2

    .prologue
    .line 121
    iget v0, p0, Ldbxyzptlk/db231222/Q/f;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Ldbxyzptlk/db231222/Q/f;->d:I

    return v0
.end method

.method private static b(Ldbxyzptlk/db231222/R/i;)Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 667
    new-instance v0, Ldbxyzptlk/db231222/Q/h;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Ldbxyzptlk/db231222/R/i;->a(I)Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Ldbxyzptlk/db231222/Q/h;-><init>(Ljava/io/InputStream;Ldbxyzptlk/db231222/R/i;)V

    return-object v0
.end method

.method private declared-synchronized b()V
    .locals 1

    .prologue
    .line 388
    monitor-enter p0

    :try_start_0
    iget v0, p0, Ldbxyzptlk/db231222/Q/f;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/Q/f;->g:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 389
    monitor-exit p0

    return-void

    .line 388
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c(Ldbxyzptlk/db231222/Q/f;)I
    .locals 2

    .prologue
    .line 121
    iget v0, p0, Ldbxyzptlk/db231222/Q/f;->e:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Ldbxyzptlk/db231222/Q/f;->e:I

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Ldbxyzptlk/db231222/Q/f;->c:Ldbxyzptlk/db231222/R/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/R/c;->a()V

    .line 339
    return-void
.end method

.method public final get(Ljava/net/URI;Ljava/lang/String;Ljava/util/Map;)Ljava/net/CacheResponse;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URI;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)",
            "Ljava/net/CacheResponse;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 203
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/Q/f;->a(Ljava/net/URI;)Ljava/lang/String;

    move-result-object v1

    .line 207
    :try_start_0
    iget-object v2, p0, Ldbxyzptlk/db231222/Q/f;->c:Ldbxyzptlk/db231222/R/c;

    invoke-virtual {v2, v1}, Ldbxyzptlk/db231222/R/c;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/R/i;

    move-result-object v1

    .line 208
    if-nez v1, :cond_0

    .line 222
    :goto_0
    return-object v0

    .line 211
    :cond_0
    new-instance v2, Ldbxyzptlk/db231222/Q/l;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ldbxyzptlk/db231222/R/i;->a(I)Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v2, v3}, Ldbxyzptlk/db231222/Q/l;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 217
    invoke-virtual {v2, p1, p2, p3}, Ldbxyzptlk/db231222/Q/l;->a(Ljava/net/URI;Ljava/lang/String;Ljava/util/Map;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 218
    invoke-virtual {v1}, Ldbxyzptlk/db231222/R/i;->close()V

    goto :goto_0

    .line 222
    :cond_1
    invoke-static {v2}, Ldbxyzptlk/db231222/Q/l;->a(Ldbxyzptlk/db231222/Q/l;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ldbxyzptlk/db231222/Q/n;

    invoke-direct {v0, v2, v1}, Ldbxyzptlk/db231222/Q/n;-><init>(Ldbxyzptlk/db231222/Q/l;Ldbxyzptlk/db231222/R/i;)V

    goto :goto_0

    :cond_2
    new-instance v0, Ldbxyzptlk/db231222/Q/m;

    invoke-direct {v0, v2, v1}, Ldbxyzptlk/db231222/Q/m;-><init>(Ldbxyzptlk/db231222/Q/l;Ldbxyzptlk/db231222/R/i;)V

    goto :goto_0

    .line 212
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final put(Ljava/net/URI;Ljava/net/URLConnection;)Ljava/net/CacheRequest;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 227
    instance-of v1, p2, Ljava/net/HttpURLConnection;

    if-nez v1, :cond_1

    .line 268
    :cond_0
    :goto_0
    return-object v0

    .line 231
    :cond_1
    check-cast p2, Ljava/net/HttpURLConnection;

    .line 232
    invoke-virtual {p2}, Ljava/net/HttpURLConnection;->getRequestMethod()Ljava/lang/String;

    move-result-object v1

    .line 234
    invoke-direct {p0, v1, p1}, Ldbxyzptlk/db231222/Q/f;->a(Ljava/lang/String;Ljava/net/URI;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 237
    const-string v2, "GET"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 244
    invoke-direct {p0, p2}, Ldbxyzptlk/db231222/Q/f;->a(Ljava/net/URLConnection;)Ldbxyzptlk/db231222/S/h;

    move-result-object v1

    .line 245
    if-eqz v1, :cond_0

    .line 250
    invoke-virtual {v1}, Ldbxyzptlk/db231222/S/h;->i()Ldbxyzptlk/db231222/S/B;

    move-result-object v2

    .line 251
    invoke-virtual {v2}, Ldbxyzptlk/db231222/S/B;->i()Z

    move-result v3

    if-nez v3, :cond_0

    .line 255
    invoke-virtual {v1}, Ldbxyzptlk/db231222/S/h;->h()Ldbxyzptlk/db231222/S/z;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/S/z;->c()Ldbxyzptlk/db231222/S/x;

    move-result-object v1

    invoke-virtual {v2}, Ldbxyzptlk/db231222/S/B;->g()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/S/x;->a(Ljava/util/Set;)Ldbxyzptlk/db231222/S/x;

    move-result-object v1

    .line 257
    new-instance v3, Ldbxyzptlk/db231222/Q/l;

    invoke-direct {v3, p1, v1, p2}, Ldbxyzptlk/db231222/Q/l;-><init>(Ljava/net/URI;Ldbxyzptlk/db231222/S/x;Ljava/net/HttpURLConnection;)V

    .line 260
    :try_start_0
    iget-object v1, p0, Ldbxyzptlk/db231222/Q/f;->c:Ldbxyzptlk/db231222/R/c;

    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/Q/f;->a(Ljava/net/URI;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ldbxyzptlk/db231222/R/c;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/R/f;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 261
    if-eqz v2, :cond_0

    .line 264
    :try_start_1
    invoke-virtual {v3, v2}, Ldbxyzptlk/db231222/Q/l;->a(Ldbxyzptlk/db231222/R/f;)V

    .line 265
    new-instance v1, Ldbxyzptlk/db231222/Q/j;

    invoke-direct {v1, p0, v2}, Ldbxyzptlk/db231222/Q/j;-><init>(Ldbxyzptlk/db231222/Q/f;Ldbxyzptlk/db231222/R/f;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    goto :goto_0

    .line 266
    :catch_0
    move-exception v1

    move-object v1, v0

    .line 267
    :goto_1
    invoke-direct {p0, v1}, Ldbxyzptlk/db231222/Q/f;->a(Ldbxyzptlk/db231222/R/f;)V

    goto :goto_0

    .line 266
    :catch_1
    move-exception v1

    move-object v1, v2

    goto :goto_1
.end method
