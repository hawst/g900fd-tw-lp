.class final Ldbxyzptlk/db231222/V/w;
.super Ldbxyzptlk/db231222/V/d;
.source "panda.py"


# instance fields
.field o:I

.field private final p:Ldbxyzptlk/db231222/V/q;


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/V/y;Ldbxyzptlk/db231222/V/m;Ldbxyzptlk/db231222/V/e;Ldbxyzptlk/db231222/V/O;Ldbxyzptlk/db231222/V/a;Ldbxyzptlk/db231222/V/q;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct/range {p0 .. p5}, Ldbxyzptlk/db231222/V/d;-><init>(Ldbxyzptlk/db231222/V/y;Ldbxyzptlk/db231222/V/m;Ldbxyzptlk/db231222/V/e;Ldbxyzptlk/db231222/V/O;Ldbxyzptlk/db231222/V/a;)V

    .line 39
    iput-object p6, p0, Ldbxyzptlk/db231222/V/w;->p:Ldbxyzptlk/db231222/V/q;

    .line 40
    const/4 v0, 0x2

    iput v0, p0, Ldbxyzptlk/db231222/V/w;->o:I

    .line 41
    return-void
.end method

.method private a(Ljava/io/InputStream;Ldbxyzptlk/db231222/V/J;)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 77
    if-nez p1, :cond_0

    .line 94
    :goto_0
    return-object v1

    .line 81
    :cond_0
    invoke-virtual {p2}, Ldbxyzptlk/db231222/V/J;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 83
    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 85
    new-instance v2, Ldbxyzptlk/db231222/V/v;

    invoke-direct {v2, p1}, Ldbxyzptlk/db231222/V/v;-><init>(Ljava/io/InputStream;)V

    .line 88
    const/16 v3, 0x4000

    invoke-virtual {v2, v3}, Ldbxyzptlk/db231222/V/v;->a(I)J

    move-result-wide v3

    .line 89
    invoke-static {v2, v1, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 90
    iget v5, p2, Ldbxyzptlk/db231222/V/J;->d:I

    iget v6, p2, Ldbxyzptlk/db231222/V/J;->e:I

    invoke-static {v5, v6, v0}, Ldbxyzptlk/db231222/V/w;->a(IILandroid/graphics/BitmapFactory$Options;)V

    .line 92
    invoke-virtual {v2, v3, v4}, Ldbxyzptlk/db231222/V/v;->a(J)V

    move-object p1, v2

    .line 94
    :goto_1
    invoke-static {p1, v1, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method final a(Ldbxyzptlk/db231222/V/J;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 44
    iget v0, p0, Ldbxyzptlk/db231222/V/w;->o:I

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 46
    :goto_0
    iget-object v2, p0, Ldbxyzptlk/db231222/V/w;->p:Ldbxyzptlk/db231222/V/q;

    iget-object v3, p1, Ldbxyzptlk/db231222/V/J;->a:Landroid/net/Uri;

    invoke-interface {v2, v3, v0}, Ldbxyzptlk/db231222/V/q;->a(Landroid/net/Uri;Z)Ldbxyzptlk/db231222/V/r;

    move-result-object v2

    .line 47
    if-nez v2, :cond_2

    move-object v0, v1

    .line 63
    :cond_0
    :goto_1
    return-object v0

    .line 44
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 51
    :cond_2
    iget-boolean v0, v2, Ldbxyzptlk/db231222/V/r;->c:Z

    if-eqz v0, :cond_3

    sget-object v0, Ldbxyzptlk/db231222/V/E;->b:Ldbxyzptlk/db231222/V/E;

    :goto_2
    iput-object v0, p0, Ldbxyzptlk/db231222/V/w;->l:Ldbxyzptlk/db231222/V/E;

    .line 53
    invoke-virtual {v2}, Ldbxyzptlk/db231222/V/r;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 54
    if-nez v0, :cond_0

    .line 60
    :try_start_0
    invoke-virtual {v2}, Ldbxyzptlk/db231222/V/r;->a()Ljava/io/InputStream;

    move-result-object v1

    .line 61
    invoke-direct {p0, v1, p1}, Ldbxyzptlk/db231222/V/w;->a(Ljava/io/InputStream;Ldbxyzptlk/db231222/V/J;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 63
    invoke-static {v1}, Ldbxyzptlk/db231222/V/U;->a(Ljava/io/InputStream;)V

    goto :goto_1

    .line 51
    :cond_3
    sget-object v0, Ldbxyzptlk/db231222/V/E;->c:Ldbxyzptlk/db231222/V/E;

    goto :goto_2

    .line 63
    :catchall_0
    move-exception v0

    invoke-static {v1}, Ldbxyzptlk/db231222/V/U;->a(Ljava/io/InputStream;)V

    throw v0
.end method

.method final a(ZLandroid/net/NetworkInfo;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 68
    iget v2, p0, Ldbxyzptlk/db231222/V/w;->o:I

    if-lez v2, :cond_1

    move v2, v1

    .line 69
    :goto_0
    if-nez v2, :cond_2

    .line 73
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v0

    .line 68
    goto :goto_0

    .line 72
    :cond_2
    iget v2, p0, Ldbxyzptlk/db231222/V/w;->o:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Ldbxyzptlk/db231222/V/w;->o:I

    .line 73
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method
