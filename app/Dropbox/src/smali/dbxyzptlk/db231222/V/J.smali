.class public final Ldbxyzptlk/db231222/V/J;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:I

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/V/R;",
            ">;"
        }
    .end annotation
.end field

.field public final d:I

.field public final e:I

.field public final f:Z

.field public final g:Z

.field public final h:F

.field public final i:F

.field public final j:F

.field public final k:Z


# direct methods
.method private constructor <init>(Landroid/net/Uri;ILjava/util/List;IIZZFFFZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "I",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/V/R;",
            ">;IIZZFFFZ)V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Ldbxyzptlk/db231222/V/J;->a:Landroid/net/Uri;

    .line 69
    iput p2, p0, Ldbxyzptlk/db231222/V/J;->b:I

    .line 70
    if-nez p3, :cond_0

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/V/J;->c:Ljava/util/List;

    .line 75
    :goto_0
    iput p4, p0, Ldbxyzptlk/db231222/V/J;->d:I

    .line 76
    iput p5, p0, Ldbxyzptlk/db231222/V/J;->e:I

    .line 77
    iput-boolean p6, p0, Ldbxyzptlk/db231222/V/J;->f:Z

    .line 78
    iput-boolean p7, p0, Ldbxyzptlk/db231222/V/J;->g:Z

    .line 79
    iput p8, p0, Ldbxyzptlk/db231222/V/J;->h:F

    .line 80
    iput p9, p0, Ldbxyzptlk/db231222/V/J;->i:F

    .line 81
    iput p10, p0, Ldbxyzptlk/db231222/V/J;->j:F

    .line 82
    iput-boolean p11, p0, Ldbxyzptlk/db231222/V/J;->k:Z

    .line 83
    return-void

    .line 73
    :cond_0
    invoke-static {p3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/V/J;->c:Ljava/util/List;

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/net/Uri;ILjava/util/List;IIZZFFFZLdbxyzptlk/db231222/V/K;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct/range {p0 .. p11}, Ldbxyzptlk/db231222/V/J;-><init>(Landroid/net/Uri;ILjava/util/List;IIZZFFFZ)V

    return-void
.end method


# virtual methods
.method final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Ldbxyzptlk/db231222/V/J;->a:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Ldbxyzptlk/db231222/V/J;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 89
    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Ldbxyzptlk/db231222/V/J;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Ldbxyzptlk/db231222/V/J;->d:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final c()Z
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Ldbxyzptlk/db231222/V/J;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/V/J;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final d()Z
    .locals 2

    .prologue
    .line 101
    iget v0, p0, Ldbxyzptlk/db231222/V/J;->d:I

    if-nez v0, :cond_0

    iget v0, p0, Ldbxyzptlk/db231222/V/J;->h:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final e()Z
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Ldbxyzptlk/db231222/V/J;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
