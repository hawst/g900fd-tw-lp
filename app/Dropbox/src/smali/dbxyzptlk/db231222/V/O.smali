.class final Ldbxyzptlk/db231222/V/O;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field final a:Landroid/os/HandlerThread;

.field final b:Ldbxyzptlk/db231222/V/e;

.field final c:Landroid/os/Handler;

.field d:J

.field e:J

.field f:J

.field g:J

.field h:J

.field i:J

.field j:I

.field k:I


# direct methods
.method constructor <init>(Ldbxyzptlk/db231222/V/e;)V
    .locals 3

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Ldbxyzptlk/db231222/V/O;->b:Ldbxyzptlk/db231222/V/e;

    .line 50
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "Picasso-Stats"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Ldbxyzptlk/db231222/V/O;->a:Landroid/os/HandlerThread;

    .line 51
    iget-object v0, p0, Ldbxyzptlk/db231222/V/O;->a:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 52
    new-instance v0, Ldbxyzptlk/db231222/V/P;

    iget-object v1, p0, Ldbxyzptlk/db231222/V/O;->a:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ldbxyzptlk/db231222/V/P;-><init>(Ldbxyzptlk/db231222/V/O;Landroid/os/Looper;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/V/O;->c:Landroid/os/Handler;

    .line 53
    return-void
.end method

.method private static a(IJ)J
    .locals 2

    .prologue
    .line 110
    int-to-long v0, p0

    div-long v0, p1, v0

    return-wide v0
.end method

.method private a(Landroid/graphics/Bitmap;I)V
    .locals 4

    .prologue
    .line 105
    invoke-static {p1}, Ldbxyzptlk/db231222/V/U;->a(Landroid/graphics/Bitmap;)I

    move-result v0

    .line 106
    iget-object v1, p0, Ldbxyzptlk/db231222/V/O;->c:Landroid/os/Handler;

    iget-object v2, p0, Ldbxyzptlk/db231222/V/O;->c:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, p2, v0, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 107
    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Ldbxyzptlk/db231222/V/O;->c:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 65
    return-void
.end method

.method final a(J)V
    .locals 3

    .prologue
    .line 84
    iget v0, p0, Ldbxyzptlk/db231222/V/O;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/V/O;->j:I

    .line 85
    iget-wide v0, p0, Ldbxyzptlk/db231222/V/O;->f:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Ldbxyzptlk/db231222/V/O;->f:J

    .line 86
    iget v0, p0, Ldbxyzptlk/db231222/V/O;->j:I

    iget-wide v1, p0, Ldbxyzptlk/db231222/V/O;->f:J

    invoke-static {v0, v1, v2}, Ldbxyzptlk/db231222/V/O;->a(IJ)J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/V/O;->h:J

    .line 87
    return-void
.end method

.method final a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Ldbxyzptlk/db231222/V/O;->a(Landroid/graphics/Bitmap;I)V

    .line 57
    return-void
.end method

.method final b()V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Ldbxyzptlk/db231222/V/O;->c:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 69
    return-void
.end method

.method final b(J)V
    .locals 3

    .prologue
    .line 90
    iget v0, p0, Ldbxyzptlk/db231222/V/O;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/V/O;->k:I

    .line 91
    iget-wide v0, p0, Ldbxyzptlk/db231222/V/O;->g:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Ldbxyzptlk/db231222/V/O;->g:J

    .line 92
    iget v0, p0, Ldbxyzptlk/db231222/V/O;->j:I

    iget-wide v1, p0, Ldbxyzptlk/db231222/V/O;->g:J

    invoke-static {v0, v1, v2}, Ldbxyzptlk/db231222/V/O;->a(IJ)J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/V/O;->i:J

    .line 94
    return-void
.end method

.method final b(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Ldbxyzptlk/db231222/V/O;->a(Landroid/graphics/Bitmap;I)V

    .line 61
    return-void
.end method

.method final c()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Ldbxyzptlk/db231222/V/O;->a:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 73
    return-void
.end method

.method final d()V
    .locals 4

    .prologue
    .line 76
    iget-wide v0, p0, Ldbxyzptlk/db231222/V/O;->d:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Ldbxyzptlk/db231222/V/O;->d:J

    .line 77
    return-void
.end method

.method final e()V
    .locals 4

    .prologue
    .line 80
    iget-wide v0, p0, Ldbxyzptlk/db231222/V/O;->e:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Ldbxyzptlk/db231222/V/O;->e:J

    .line 81
    return-void
.end method
