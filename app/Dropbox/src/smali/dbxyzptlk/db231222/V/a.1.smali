.class abstract Ldbxyzptlk/db231222/V/a;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final a:Ldbxyzptlk/db231222/V/y;

.field final b:Ldbxyzptlk/db231222/V/J;

.field final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TT;>;"
        }
    .end annotation
.end field

.field final d:Z

.field final e:Z

.field final f:I

.field final g:Landroid/graphics/drawable/Drawable;

.field final h:Ljava/lang/String;

.field i:Z


# direct methods
.method constructor <init>(Ldbxyzptlk/db231222/V/y;Ljava/lang/Object;Ldbxyzptlk/db231222/V/J;ZZILandroid/graphics/drawable/Drawable;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/V/y;",
            "TT;",
            "Ldbxyzptlk/db231222/V/J;",
            "ZZI",
            "Landroid/graphics/drawable/Drawable;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Ldbxyzptlk/db231222/V/a;->a:Ldbxyzptlk/db231222/V/y;

    .line 47
    iput-object p3, p0, Ldbxyzptlk/db231222/V/a;->b:Ldbxyzptlk/db231222/V/J;

    .line 48
    new-instance v0, Ldbxyzptlk/db231222/V/b;

    iget-object v1, p1, Ldbxyzptlk/db231222/V/y;->i:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0, p0, p2, v1}, Ldbxyzptlk/db231222/V/b;-><init>(Ldbxyzptlk/db231222/V/a;Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/V/a;->c:Ljava/lang/ref/WeakReference;

    .line 49
    iput-boolean p4, p0, Ldbxyzptlk/db231222/V/a;->d:Z

    .line 50
    iput-boolean p5, p0, Ldbxyzptlk/db231222/V/a;->e:Z

    .line 51
    iput p6, p0, Ldbxyzptlk/db231222/V/a;->f:I

    .line 52
    iput-object p7, p0, Ldbxyzptlk/db231222/V/a;->g:Landroid/graphics/drawable/Drawable;

    .line 53
    iput-object p8, p0, Ldbxyzptlk/db231222/V/a;->h:Ljava/lang/String;

    .line 54
    return-void
.end method


# virtual methods
.method abstract a()V
.end method

.method abstract a(Landroid/graphics/Bitmap;Ldbxyzptlk/db231222/V/E;)V
.end method

.method b()V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/db231222/V/a;->i:Z

    .line 62
    return-void
.end method

.method final c()Ldbxyzptlk/db231222/V/J;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Ldbxyzptlk/db231222/V/a;->b:Ldbxyzptlk/db231222/V/J;

    return-object v0
.end method

.method final d()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Ldbxyzptlk/db231222/V/a;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Ldbxyzptlk/db231222/V/a;->h:Ljava/lang/String;

    return-object v0
.end method

.method final f()Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Ldbxyzptlk/db231222/V/a;->i:Z

    return v0
.end method

.method final g()Ldbxyzptlk/db231222/V/y;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Ldbxyzptlk/db231222/V/a;->a:Ldbxyzptlk/db231222/V/y;

    return-object v0
.end method
