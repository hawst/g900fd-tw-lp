.class abstract Ldbxyzptlk/db231222/V/d;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field protected static final a:I

.field private static final o:Ljava/lang/Object;


# instance fields
.field final b:Ldbxyzptlk/db231222/V/y;

.field final c:Ldbxyzptlk/db231222/V/m;

.field final d:Ldbxyzptlk/db231222/V/e;

.field final e:Ldbxyzptlk/db231222/V/O;

.field final f:Ljava/lang/String;

.field final g:Ldbxyzptlk/db231222/V/J;

.field final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/V/a;",
            ">;"
        }
    .end annotation
.end field

.field final i:Z

.field j:Landroid/graphics/Bitmap;

.field k:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field l:Ldbxyzptlk/db231222/V/E;

.field m:Ljava/lang/Exception;

.field n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/V/d;->o:Ljava/lang/Object;

    .line 44
    const-string v0, "file:///android_asset/"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Ldbxyzptlk/db231222/V/d;->a:I

    return-void
.end method

.method constructor <init>(Ldbxyzptlk/db231222/V/y;Ldbxyzptlk/db231222/V/m;Ldbxyzptlk/db231222/V/e;Ldbxyzptlk/db231222/V/O;Ldbxyzptlk/db231222/V/a;)V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Ldbxyzptlk/db231222/V/d;->b:Ldbxyzptlk/db231222/V/y;

    .line 64
    iput-object p2, p0, Ldbxyzptlk/db231222/V/d;->c:Ldbxyzptlk/db231222/V/m;

    .line 65
    iput-object p3, p0, Ldbxyzptlk/db231222/V/d;->d:Ldbxyzptlk/db231222/V/e;

    .line 66
    iput-object p4, p0, Ldbxyzptlk/db231222/V/d;->e:Ldbxyzptlk/db231222/V/O;

    .line 67
    invoke-virtual {p5}, Ldbxyzptlk/db231222/V/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/V/d;->f:Ljava/lang/String;

    .line 68
    invoke-virtual {p5}, Ldbxyzptlk/db231222/V/a;->c()Ldbxyzptlk/db231222/V/J;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/V/d;->g:Ldbxyzptlk/db231222/V/J;

    .line 69
    iget-boolean v0, p5, Ldbxyzptlk/db231222/V/a;->d:Z

    iput-boolean v0, p0, Ldbxyzptlk/db231222/V/d;->i:Z

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Ldbxyzptlk/db231222/V/d;->h:Ljava/util/List;

    .line 71
    invoke-virtual {p0, p5}, Ldbxyzptlk/db231222/V/d;->a(Ldbxyzptlk/db231222/V/a;)V

    .line 72
    return-void
.end method

.method static a(Ldbxyzptlk/db231222/V/J;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 254
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 255
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 262
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 264
    invoke-virtual {p0}, Ldbxyzptlk/db231222/V/J;->d()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 265
    iget v0, p0, Ldbxyzptlk/db231222/V/J;->d:I

    .line 266
    iget v1, p0, Ldbxyzptlk/db231222/V/J;->e:I

    .line 268
    iget v6, p0, Ldbxyzptlk/db231222/V/J;->h:F

    .line 269
    const/4 v7, 0x0

    cmpl-float v7, v6, v7

    if-eqz v7, :cond_0

    .line 270
    iget-boolean v7, p0, Ldbxyzptlk/db231222/V/J;->k:Z

    if-eqz v7, :cond_3

    .line 271
    iget v7, p0, Ldbxyzptlk/db231222/V/J;->i:F

    iget v8, p0, Ldbxyzptlk/db231222/V/J;->j:F

    invoke-virtual {v5, v6, v7, v8}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 277
    :cond_0
    :goto_0
    iget-boolean v6, p0, Ldbxyzptlk/db231222/V/J;->f:Z

    if-eqz v6, :cond_5

    .line 278
    int-to-float v0, v0

    int-to-float v6, v3

    div-float/2addr v0, v6

    .line 279
    int-to-float v1, v1

    int-to-float v6, v2

    div-float/2addr v1, v6

    .line 281
    cmpl-float v6, v0, v1

    if-lez v6, :cond_4

    .line 283
    int-to-float v6, v2

    div-float/2addr v1, v0

    mul-float/2addr v1, v6

    float-to-double v6, v1

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v1, v6

    .line 284
    sub-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x2

    move v9, v3

    move v3, v2

    move v2, v9

    .line 292
    :goto_1
    invoke-virtual {v5, v0, v0}, Landroid/graphics/Matrix;->preScale(FF)Z

    move v9, v1

    move v1, v4

    move v4, v9

    move v10, v3

    move v3, v2

    move v2, v10

    .line 308
    :goto_2
    if-eqz p2, :cond_1

    .line 309
    int-to-float v0, p2

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->preRotate(F)Z

    .line 312
    :cond_1
    const/4 v6, 0x1

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 314
    if-eq v0, p1, :cond_2

    .line 315
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    move-object p1, v0

    .line 319
    :cond_2
    return-object p1

    .line 273
    :cond_3
    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->setRotate(F)V

    goto :goto_0

    .line 288
    :cond_4
    int-to-float v6, v3

    div-float/2addr v0, v1

    mul-float/2addr v0, v6

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v0, v6

    .line 289
    sub-int/2addr v3, v0

    div-int/lit8 v3, v3, 0x2

    move v9, v1

    move v1, v2

    move v2, v0

    move v0, v9

    move v10, v4

    move v4, v3

    move v3, v10

    .line 290
    goto :goto_1

    .line 293
    :cond_5
    iget-boolean v6, p0, Ldbxyzptlk/db231222/V/J;->g:Z

    if-eqz v6, :cond_7

    .line 294
    int-to-float v0, v0

    int-to-float v6, v3

    div-float/2addr v0, v6

    .line 295
    int-to-float v1, v1

    int-to-float v6, v2

    div-float/2addr v1, v6

    .line 296
    cmpg-float v6, v0, v1

    if-gez v6, :cond_6

    .line 297
    :goto_3
    invoke-virtual {v5, v0, v0}, Landroid/graphics/Matrix;->preScale(FF)Z

    move v1, v4

    move v9, v4

    move v4, v2

    move v2, v9

    .line 298
    goto :goto_2

    :cond_6
    move v0, v1

    .line 296
    goto :goto_3

    .line 298
    :cond_7
    if-eqz v0, :cond_9

    if-eqz v1, :cond_9

    if-ne v0, v3, :cond_8

    if-eq v1, v2, :cond_9

    .line 302
    :cond_8
    int-to-float v0, v0

    int-to-float v6, v3

    div-float/2addr v0, v6

    .line 303
    int-to-float v1, v1

    int-to-float v6, v2

    div-float/2addr v1, v6

    .line 304
    invoke-virtual {v5, v0, v1}, Landroid/graphics/Matrix;->preScale(FF)Z

    :cond_9
    move v1, v4

    move v9, v4

    move v4, v2

    move v2, v9

    goto :goto_2
.end method

.method static a(Ljava/util/List;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/V/R;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ")",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 220
    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    .line 221
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/V/R;

    .line 222
    invoke-interface {v0, p1}, Ldbxyzptlk/db231222/V/R;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 224
    if-nez v2, :cond_1

    .line 225
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Transformation "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Ldbxyzptlk/db231222/V/R;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " returned null after "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " previous transformation(s).\n\nTransformation list:\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 231
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/V/R;

    .line 232
    invoke-interface {v0}, Ldbxyzptlk/db231222/V/R;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 234
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 237
    :cond_1
    if-ne v2, p1, :cond_2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 238
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Transformation "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Ldbxyzptlk/db231222/V/R;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " returned input Bitmap but recycled it."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 243
    :cond_2
    if-eq v2, p1, :cond_3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_3

    .line 244
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Transformation "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Ldbxyzptlk/db231222/V/R;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " mutated input Bitmap but failed to recycle the original."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 220
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object p1, v2

    goto/16 :goto_0

    .line 250
    :cond_4
    return-object p1
.end method

.method static a(Landroid/content/Context;Ldbxyzptlk/db231222/V/y;Ldbxyzptlk/db231222/V/m;Ldbxyzptlk/db231222/V/e;Ldbxyzptlk/db231222/V/O;Ldbxyzptlk/db231222/V/a;Ldbxyzptlk/db231222/V/q;)Ldbxyzptlk/db231222/V/d;
    .locals 7

    .prologue
    .line 181
    invoke-virtual {p5}, Ldbxyzptlk/db231222/V/a;->c()Ldbxyzptlk/db231222/V/J;

    move-result-object v0

    iget v0, v0, Ldbxyzptlk/db231222/V/J;->b:I

    if-eqz v0, :cond_0

    .line 182
    new-instance v0, Ldbxyzptlk/db231222/V/N;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/V/N;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/V/y;Ldbxyzptlk/db231222/V/m;Ldbxyzptlk/db231222/V/e;Ldbxyzptlk/db231222/V/O;Ldbxyzptlk/db231222/V/a;)V

    .line 201
    :goto_0
    return-object v0

    .line 184
    :cond_0
    invoke-virtual {p5}, Ldbxyzptlk/db231222/V/a;->c()Ldbxyzptlk/db231222/V/J;

    move-result-object v0

    iget-object v0, v0, Ldbxyzptlk/db231222/V/J;->a:Landroid/net/Uri;

    .line 185
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 186
    const-string v2, "content"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 187
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const-string v1, "photo"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 189
    new-instance v0, Ldbxyzptlk/db231222/V/h;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/V/h;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/V/y;Ldbxyzptlk/db231222/V/m;Ldbxyzptlk/db231222/V/e;Ldbxyzptlk/db231222/V/O;Ldbxyzptlk/db231222/V/a;)V

    goto :goto_0

    .line 191
    :cond_1
    new-instance v0, Ldbxyzptlk/db231222/V/j;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/V/j;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/V/y;Ldbxyzptlk/db231222/V/m;Ldbxyzptlk/db231222/V/e;Ldbxyzptlk/db231222/V/O;Ldbxyzptlk/db231222/V/a;)V

    goto :goto_0

    .line 193
    :cond_2
    const-string v2, "file"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 194
    const-string v1, "android_asset"

    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 195
    new-instance v0, Ldbxyzptlk/db231222/V/c;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/V/c;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/V/y;Ldbxyzptlk/db231222/V/m;Ldbxyzptlk/db231222/V/e;Ldbxyzptlk/db231222/V/O;Ldbxyzptlk/db231222/V/a;)V

    goto :goto_0

    .line 197
    :cond_3
    new-instance v0, Ldbxyzptlk/db231222/V/s;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/V/s;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/V/y;Ldbxyzptlk/db231222/V/m;Ldbxyzptlk/db231222/V/e;Ldbxyzptlk/db231222/V/O;Ldbxyzptlk/db231222/V/a;)V

    goto :goto_0

    .line 198
    :cond_4
    const-string v0, "android.resource"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 199
    new-instance v0, Ldbxyzptlk/db231222/V/N;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/V/N;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/V/y;Ldbxyzptlk/db231222/V/m;Ldbxyzptlk/db231222/V/e;Ldbxyzptlk/db231222/V/O;Ldbxyzptlk/db231222/V/a;)V

    goto/16 :goto_0

    .line 201
    :cond_5
    new-instance v0, Ldbxyzptlk/db231222/V/w;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/V/w;-><init>(Ldbxyzptlk/db231222/V/y;Ldbxyzptlk/db231222/V/m;Ldbxyzptlk/db231222/V/e;Ldbxyzptlk/db231222/V/O;Ldbxyzptlk/db231222/V/a;Ldbxyzptlk/db231222/V/q;)V

    goto/16 :goto_0
.end method

.method static a(IILandroid/graphics/BitmapFactory$Options;)V
    .locals 3

    .prologue
    .line 206
    iget v1, p2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 207
    iget v2, p2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 208
    const/4 v0, 0x1

    .line 209
    if-gt v1, p1, :cond_0

    if-le v2, p0, :cond_1

    .line 210
    :cond_0
    int-to-float v0, v1

    int-to-float v1, p1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 211
    int-to-float v1, v2

    int-to-float v2, p0

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 212
    if-ge v0, v1, :cond_2

    .line 215
    :cond_1
    :goto_0
    iput v0, p2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 216
    const/4 v0, 0x0

    iput-boolean v0, p2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 217
    return-void

    :cond_2
    move v0, v1

    .line 212
    goto :goto_0
.end method


# virtual methods
.method abstract a(Ldbxyzptlk/db231222/V/J;)Landroid/graphics/Bitmap;
.end method

.method a()Ldbxyzptlk/db231222/V/E;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Ldbxyzptlk/db231222/V/d;->l:Ldbxyzptlk/db231222/V/E;

    return-object v0
.end method

.method protected final a(I)V
    .locals 0

    .prologue
    .line 75
    iput p1, p0, Ldbxyzptlk/db231222/V/d;->n:I

    .line 76
    return-void
.end method

.method final a(Ldbxyzptlk/db231222/V/a;)V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Ldbxyzptlk/db231222/V/d;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    return-void
.end method

.method a(ZLandroid/net/NetworkInfo;)Z
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    return v0
.end method

.method final b()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 102
    iget-boolean v0, p0, Ldbxyzptlk/db231222/V/d;->i:Z

    if-nez v0, :cond_1

    .line 103
    iget-object v0, p0, Ldbxyzptlk/db231222/V/d;->d:Ldbxyzptlk/db231222/V/e;

    iget-object v1, p0, Ldbxyzptlk/db231222/V/d;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Ldbxyzptlk/db231222/V/e;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 104
    if-eqz v0, :cond_1

    .line 105
    iget-object v1, p0, Ldbxyzptlk/db231222/V/d;->e:Ldbxyzptlk/db231222/V/O;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/V/O;->a()V

    .line 106
    sget-object v1, Ldbxyzptlk/db231222/V/E;->a:Ldbxyzptlk/db231222/V/E;

    iput-object v1, p0, Ldbxyzptlk/db231222/V/d;->l:Ldbxyzptlk/db231222/V/E;

    .line 128
    :cond_0
    :goto_0
    return-object v0

    .line 111
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/V/d;->g:Ldbxyzptlk/db231222/V/J;

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/V/d;->a(Ldbxyzptlk/db231222/V/J;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 113
    if-eqz v0, :cond_0

    .line 114
    iget-object v1, p0, Ldbxyzptlk/db231222/V/d;->e:Ldbxyzptlk/db231222/V/O;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/V/O;->a(Landroid/graphics/Bitmap;)V

    .line 115
    iget-object v1, p0, Ldbxyzptlk/db231222/V/d;->g:Ldbxyzptlk/db231222/V/J;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/V/J;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 116
    sget-object v1, Ldbxyzptlk/db231222/V/d;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 117
    :try_start_0
    iget-object v2, p0, Ldbxyzptlk/db231222/V/d;->g:Ldbxyzptlk/db231222/V/J;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/V/J;->d()Z

    move-result v2

    if-nez v2, :cond_2

    iget v2, p0, Ldbxyzptlk/db231222/V/d;->n:I

    if-eqz v2, :cond_3

    .line 118
    :cond_2
    iget-object v2, p0, Ldbxyzptlk/db231222/V/d;->g:Ldbxyzptlk/db231222/V/J;

    iget v3, p0, Ldbxyzptlk/db231222/V/d;->n:I

    invoke-static {v2, v0, v3}, Ldbxyzptlk/db231222/V/d;->a(Ldbxyzptlk/db231222/V/J;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 120
    :cond_3
    iget-object v2, p0, Ldbxyzptlk/db231222/V/d;->g:Ldbxyzptlk/db231222/V/J;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/V/J;->e()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 121
    iget-object v2, p0, Ldbxyzptlk/db231222/V/d;->g:Ldbxyzptlk/db231222/V/J;

    iget-object v2, v2, Ldbxyzptlk/db231222/V/J;->c:Ljava/util/List;

    invoke-static {v2, v0}, Ldbxyzptlk/db231222/V/d;->a(Ljava/util/List;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 123
    :cond_4
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    iget-object v1, p0, Ldbxyzptlk/db231222/V/d;->e:Ldbxyzptlk/db231222/V/O;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/V/O;->b(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 123
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method final b(Ldbxyzptlk/db231222/V/a;)V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Ldbxyzptlk/db231222/V/d;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 137
    return-void
.end method

.method final c()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 140
    iget-object v1, p0, Ldbxyzptlk/db231222/V/d;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldbxyzptlk/db231222/V/d;->k:Ljava/util/concurrent/Future;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldbxyzptlk/db231222/V/d;->k:Ljava/util/concurrent/Future;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Future;->cancel(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method final d()Z
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Ldbxyzptlk/db231222/V/d;->k:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/V/d;->k:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final e()Z
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Ldbxyzptlk/db231222/V/d;->i:Z

    return v0
.end method

.method final f()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Ldbxyzptlk/db231222/V/d;->j:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Ldbxyzptlk/db231222/V/d;->f:Ljava/lang/String;

    return-object v0
.end method

.method final h()Ldbxyzptlk/db231222/V/J;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Ldbxyzptlk/db231222/V/d;->g:Ldbxyzptlk/db231222/V/J;

    return-object v0
.end method

.method final i()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/V/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 168
    iget-object v0, p0, Ldbxyzptlk/db231222/V/d;->h:Ljava/util/List;

    return-object v0
.end method

.method final j()Ljava/lang/Exception;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Ldbxyzptlk/db231222/V/d;->m:Ljava/lang/Exception;

    return-object v0
.end method

.method public run()V
    .locals 3

    .prologue
    .line 80
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Picasso-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/db231222/V/d;->g:Ldbxyzptlk/db231222/V/J;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/V/J;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 82
    invoke-virtual {p0}, Ldbxyzptlk/db231222/V/d;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/V/d;->j:Landroid/graphics/Bitmap;

    .line 84
    iget-object v0, p0, Ldbxyzptlk/db231222/V/d;->j:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 85
    iget-object v0, p0, Ldbxyzptlk/db231222/V/d;->c:Ldbxyzptlk/db231222/V/m;

    invoke-virtual {v0, p0}, Ldbxyzptlk/db231222/V/m;->c(Ldbxyzptlk/db231222/V/d;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    const-string v1, "Picasso-Idle"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 95
    :goto_1
    return-void

    .line 87
    :cond_0
    :try_start_1
    iget-object v0, p0, Ldbxyzptlk/db231222/V/d;->c:Ldbxyzptlk/db231222/V/m;

    invoke-virtual {v0, p0}, Ldbxyzptlk/db231222/V/m;->a(Ldbxyzptlk/db231222/V/d;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    :try_start_2
    iput-object v0, p0, Ldbxyzptlk/db231222/V/d;->m:Ljava/lang/Exception;

    .line 91
    iget-object v0, p0, Ldbxyzptlk/db231222/V/d;->c:Ldbxyzptlk/db231222/V/m;

    invoke-virtual {v0, p0}, Ldbxyzptlk/db231222/V/m;->b(Ldbxyzptlk/db231222/V/d;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 93
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    const-string v1, "Picasso-Idle"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    const-string v2, "Picasso-Idle"

    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    throw v0
.end method
