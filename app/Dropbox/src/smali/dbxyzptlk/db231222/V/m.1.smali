.class final Ldbxyzptlk/db231222/V/m;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field final a:Ldbxyzptlk/db231222/V/o;

.field final b:Landroid/content/Context;

.field final c:Ljava/util/concurrent/ExecutorService;

.field final d:Ldbxyzptlk/db231222/V/q;

.field final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ldbxyzptlk/db231222/V/d;",
            ">;"
        }
    .end annotation
.end field

.field final f:Landroid/os/Handler;

.field final g:Landroid/os/Handler;

.field final h:Ldbxyzptlk/db231222/V/e;

.field final i:Ldbxyzptlk/db231222/V/O;

.field final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/V/d;",
            ">;"
        }
    .end annotation
.end field

.field k:Landroid/net/NetworkInfo;

.field l:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;Landroid/os/Handler;Ldbxyzptlk/db231222/V/q;Ldbxyzptlk/db231222/V/e;Ldbxyzptlk/db231222/V/O;)V
    .locals 2

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Ldbxyzptlk/db231222/V/o;

    invoke-direct {v0}, Ldbxyzptlk/db231222/V/o;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/V/m;->a:Ldbxyzptlk/db231222/V/o;

    .line 78
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->a:Ldbxyzptlk/db231222/V/o;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/V/o;->start()V

    .line 79
    iput-object p1, p0, Ldbxyzptlk/db231222/V/m;->b:Landroid/content/Context;

    .line 80
    iput-object p2, p0, Ldbxyzptlk/db231222/V/m;->c:Ljava/util/concurrent/ExecutorService;

    .line 81
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/V/m;->e:Ljava/util/Map;

    .line 82
    new-instance v0, Ldbxyzptlk/db231222/V/n;

    iget-object v1, p0, Ldbxyzptlk/db231222/V/m;->a:Ldbxyzptlk/db231222/V/o;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/V/o;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ldbxyzptlk/db231222/V/n;-><init>(Ldbxyzptlk/db231222/V/m;Landroid/os/Looper;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/V/m;->f:Landroid/os/Handler;

    .line 83
    iput-object p4, p0, Ldbxyzptlk/db231222/V/m;->d:Ldbxyzptlk/db231222/V/q;

    .line 84
    iput-object p3, p0, Ldbxyzptlk/db231222/V/m;->g:Landroid/os/Handler;

    .line 85
    iput-object p5, p0, Ldbxyzptlk/db231222/V/m;->h:Ldbxyzptlk/db231222/V/e;

    .line 86
    iput-object p6, p0, Ldbxyzptlk/db231222/V/m;->i:Ldbxyzptlk/db231222/V/O;

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Ldbxyzptlk/db231222/V/m;->j:Ljava/util/List;

    .line 88
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->b:Landroid/content/Context;

    invoke-static {v0}, Ldbxyzptlk/db231222/V/U;->d(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/V/m;->l:Z

    .line 89
    new-instance v0, Ldbxyzptlk/db231222/V/p;

    iget-object v1, p0, Ldbxyzptlk/db231222/V/m;->b:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Ldbxyzptlk/db231222/V/p;-><init>(Ldbxyzptlk/db231222/V/m;Landroid/content/Context;)V

    .line 90
    invoke-virtual {v0}, Ldbxyzptlk/db231222/V/p;->a()V

    .line 91
    return-void
.end method

.method private g(Ldbxyzptlk/db231222/V/d;)V
    .locals 4

    .prologue
    const/4 v3, 0x7

    .line 200
    invoke-virtual {p1}, Ldbxyzptlk/db231222/V/d;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 203
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->f:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 205
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->f:Landroid/os/Handler;

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method


# virtual methods
.method final a()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 95
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->a:Ldbxyzptlk/db231222/V/o;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/V/o;->quit()Z

    .line 96
    return-void
.end method

.method final a(Landroid/net/NetworkInfo;)V
    .locals 3

    .prologue
    .line 119
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->f:Landroid/os/Handler;

    iget-object v1, p0, Ldbxyzptlk/db231222/V/m;->f:Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 120
    return-void
.end method

.method final a(Ldbxyzptlk/db231222/V/a;)V
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->f:Landroid/os/Handler;

    iget-object v1, p0, Ldbxyzptlk/db231222/V/m;->f:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 100
    return-void
.end method

.method final a(Ldbxyzptlk/db231222/V/d;)V
    .locals 3

    .prologue
    .line 107
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->f:Landroid/os/Handler;

    iget-object v1, p0, Ldbxyzptlk/db231222/V/m;->f:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 108
    return-void
.end method

.method final a(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 123
    iget-object v2, p0, Ldbxyzptlk/db231222/V/m;->f:Landroid/os/Handler;

    iget-object v3, p0, Ldbxyzptlk/db231222/V/m;->f:Landroid/os/Handler;

    const/16 v4, 0xa

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v3, v4, v0, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 125
    return-void

    :cond_0
    move v0, v1

    .line 123
    goto :goto_0
.end method

.method final b()V
    .locals 4

    .prologue
    .line 178
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Ldbxyzptlk/db231222/V/m;->j:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 179
    iget-object v1, p0, Ldbxyzptlk/db231222/V/m;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 180
    iget-object v1, p0, Ldbxyzptlk/db231222/V/m;->g:Landroid/os/Handler;

    iget-object v2, p0, Ldbxyzptlk/db231222/V/m;->g:Landroid/os/Handler;

    const/16 v3, 0x8

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 181
    return-void
.end method

.method final b(Landroid/net/NetworkInfo;)V
    .locals 1

    .prologue
    .line 193
    iput-object p1, p0, Ldbxyzptlk/db231222/V/m;->k:Landroid/net/NetworkInfo;

    .line 194
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->c:Ljava/util/concurrent/ExecutorService;

    instance-of v0, v0, Ldbxyzptlk/db231222/V/I;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->c:Ljava/util/concurrent/ExecutorService;

    check-cast v0, Ldbxyzptlk/db231222/V/I;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/V/I;->a(Landroid/net/NetworkInfo;)V

    .line 197
    :cond_0
    return-void
.end method

.method final b(Ldbxyzptlk/db231222/V/a;)V
    .locals 3

    .prologue
    .line 103
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->f:Landroid/os/Handler;

    iget-object v1, p0, Ldbxyzptlk/db231222/V/m;->f:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 104
    return-void
.end method

.method final b(Ldbxyzptlk/db231222/V/d;)V
    .locals 4

    .prologue
    .line 111
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->f:Landroid/os/Handler;

    iget-object v1, p0, Ldbxyzptlk/db231222/V/m;->f:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 112
    return-void
.end method

.method final b(Z)V
    .locals 0

    .prologue
    .line 189
    iput-boolean p1, p0, Ldbxyzptlk/db231222/V/m;->l:Z

    .line 190
    return-void
.end method

.method final c(Ldbxyzptlk/db231222/V/a;)V
    .locals 7

    .prologue
    .line 128
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->e:Ljava/util/Map;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/V/a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/V/d;

    .line 129
    if-eqz v0, :cond_1

    .line 130
    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/V/d;->a(Ldbxyzptlk/db231222/V/a;)V

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 134
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 138
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->b:Landroid/content/Context;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/V/a;->g()Ldbxyzptlk/db231222/V/y;

    move-result-object v1

    iget-object v3, p0, Ldbxyzptlk/db231222/V/m;->h:Ldbxyzptlk/db231222/V/e;

    iget-object v4, p0, Ldbxyzptlk/db231222/V/m;->i:Ldbxyzptlk/db231222/V/O;

    iget-object v6, p0, Ldbxyzptlk/db231222/V/m;->d:Ldbxyzptlk/db231222/V/q;

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v6}, Ldbxyzptlk/db231222/V/d;->a(Landroid/content/Context;Ldbxyzptlk/db231222/V/y;Ldbxyzptlk/db231222/V/m;Ldbxyzptlk/db231222/V/e;Ldbxyzptlk/db231222/V/O;Ldbxyzptlk/db231222/V/a;Ldbxyzptlk/db231222/V/q;)Ldbxyzptlk/db231222/V/d;

    move-result-object v0

    .line 139
    iget-object v1, p0, Ldbxyzptlk/db231222/V/m;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v1

    iput-object v1, v0, Ldbxyzptlk/db231222/V/d;->k:Ljava/util/concurrent/Future;

    .line 140
    iget-object v1, p0, Ldbxyzptlk/db231222/V/m;->e:Ljava/util/Map;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/V/a;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method final c(Ldbxyzptlk/db231222/V/d;)V
    .locals 3

    .prologue
    .line 115
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->f:Landroid/os/Handler;

    iget-object v1, p0, Ldbxyzptlk/db231222/V/m;->f:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 116
    return-void
.end method

.method final d(Ldbxyzptlk/db231222/V/a;)V
    .locals 2

    .prologue
    .line 144
    invoke-virtual {p1}, Ldbxyzptlk/db231222/V/a;->e()Ljava/lang/String;

    move-result-object v1

    .line 145
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->e:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/V/d;

    .line 146
    if-eqz v0, :cond_0

    .line 147
    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/V/d;->b(Ldbxyzptlk/db231222/V/a;)V

    .line 148
    invoke-virtual {v0}, Ldbxyzptlk/db231222/V/d;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->e:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    :cond_0
    return-void
.end method

.method final d(Ldbxyzptlk/db231222/V/d;)V
    .locals 2

    .prologue
    .line 155
    invoke-virtual {p1}, Ldbxyzptlk/db231222/V/d;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    :goto_0
    return-void

    .line 157
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/V/m;->f(Ldbxyzptlk/db231222/V/d;)V

    goto :goto_0

    .line 162
    :cond_1
    iget-boolean v0, p0, Ldbxyzptlk/db231222/V/m;->l:Z

    iget-object v1, p0, Ldbxyzptlk/db231222/V/m;->k:Landroid/net/NetworkInfo;

    invoke-virtual {p1, v0, v1}, Ldbxyzptlk/db231222/V/d;->a(ZLandroid/net/NetworkInfo;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 163
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p1, Ldbxyzptlk/db231222/V/d;->k:Ljava/util/concurrent/Future;

    goto :goto_0

    .line 165
    :cond_2
    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/V/m;->f(Ldbxyzptlk/db231222/V/d;)V

    goto :goto_0
.end method

.method final e(Ldbxyzptlk/db231222/V/d;)V
    .locals 3

    .prologue
    .line 170
    invoke-virtual {p1}, Ldbxyzptlk/db231222/V/d;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->h:Ldbxyzptlk/db231222/V/e;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/V/d;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ldbxyzptlk/db231222/V/d;->f()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ldbxyzptlk/db231222/V/e;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 173
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->e:Ljava/util/Map;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/V/d;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/V/m;->g(Ldbxyzptlk/db231222/V/d;)V

    .line 175
    return-void
.end method

.method final f(Ldbxyzptlk/db231222/V/d;)V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Ldbxyzptlk/db231222/V/m;->e:Ljava/util/Map;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/V/d;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/V/m;->g(Ldbxyzptlk/db231222/V/d;)V

    .line 186
    return-void
.end method
