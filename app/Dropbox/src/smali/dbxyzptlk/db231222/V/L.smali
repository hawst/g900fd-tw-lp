.class public final Ldbxyzptlk/db231222/V/L;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:Landroid/net/Uri;

.field private b:I

.field private c:I

.field private d:I

.field private e:Z

.field private f:Z

.field private g:F

.field private h:F

.field private i:F

.field private j:Z

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/V/R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/net/Uri;I)V
    .locals 0

    .prologue
    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 137
    iput-object p1, p0, Ldbxyzptlk/db231222/V/L;->a:Landroid/net/Uri;

    .line 138
    iput p2, p0, Ldbxyzptlk/db231222/V/L;->b:I

    .line 139
    return-void
.end method


# virtual methods
.method public final a(II)Ldbxyzptlk/db231222/V/L;
    .locals 2

    .prologue
    .line 195
    if-gtz p1, :cond_0

    .line 196
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Width must be positive number."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :cond_0
    if-gtz p2, :cond_1

    .line 199
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Height must be positive number."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 201
    :cond_1
    iput p1, p0, Ldbxyzptlk/db231222/V/L;->c:I

    .line 202
    iput p2, p0, Ldbxyzptlk/db231222/V/L;->d:I

    .line 203
    return-object p0
.end method

.method final a()Z
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Ldbxyzptlk/db231222/V/L;->a:Landroid/net/Uri;

    if-nez v0, :cond_0

    iget v0, p0, Ldbxyzptlk/db231222/V/L;->b:I

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b()Z
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Ldbxyzptlk/db231222/V/L;->c:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ldbxyzptlk/db231222/V/J;
    .locals 13

    .prologue
    .line 294
    iget-boolean v0, p0, Ldbxyzptlk/db231222/V/L;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldbxyzptlk/db231222/V/L;->e:Z

    if-eqz v0, :cond_0

    .line 295
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Center crop and center inside can not be used together."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 297
    :cond_0
    iget-boolean v0, p0, Ldbxyzptlk/db231222/V/L;->e:Z

    if-eqz v0, :cond_1

    iget v0, p0, Ldbxyzptlk/db231222/V/L;->c:I

    if-nez v0, :cond_1

    .line 298
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Center crop requires calling resize."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300
    :cond_1
    iget-boolean v0, p0, Ldbxyzptlk/db231222/V/L;->f:Z

    if-eqz v0, :cond_2

    iget v0, p0, Ldbxyzptlk/db231222/V/L;->c:I

    if-nez v0, :cond_2

    .line 301
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Center inside requires calling resize."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 303
    :cond_2
    new-instance v0, Ldbxyzptlk/db231222/V/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/V/L;->a:Landroid/net/Uri;

    iget v2, p0, Ldbxyzptlk/db231222/V/L;->b:I

    iget-object v3, p0, Ldbxyzptlk/db231222/V/L;->k:Ljava/util/List;

    iget v4, p0, Ldbxyzptlk/db231222/V/L;->c:I

    iget v5, p0, Ldbxyzptlk/db231222/V/L;->d:I

    iget-boolean v6, p0, Ldbxyzptlk/db231222/V/L;->e:Z

    iget-boolean v7, p0, Ldbxyzptlk/db231222/V/L;->f:Z

    iget v8, p0, Ldbxyzptlk/db231222/V/L;->g:F

    iget v9, p0, Ldbxyzptlk/db231222/V/L;->h:F

    iget v10, p0, Ldbxyzptlk/db231222/V/L;->i:F

    iget-boolean v11, p0, Ldbxyzptlk/db231222/V/L;->j:Z

    const/4 v12, 0x0

    invoke-direct/range {v0 .. v12}, Ldbxyzptlk/db231222/V/J;-><init>(Landroid/net/Uri;ILjava/util/List;IIZZFFFZLdbxyzptlk/db231222/V/K;)V

    return-object v0
.end method
