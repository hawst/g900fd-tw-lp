.class public final Ldbxyzptlk/db231222/V/M;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Ldbxyzptlk/db231222/V/y;

.field private final b:Ldbxyzptlk/db231222/V/L;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:I

.field private g:Landroid/graphics/drawable/Drawable;

.field private h:I

.field private i:Landroid/graphics/drawable/Drawable;


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object v2, p0, Ldbxyzptlk/db231222/V/M;->a:Ldbxyzptlk/db231222/V/y;

    .line 56
    new-instance v0, Ldbxyzptlk/db231222/V/L;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v1}, Ldbxyzptlk/db231222/V/L;-><init>(Landroid/net/Uri;I)V

    iput-object v0, p0, Ldbxyzptlk/db231222/V/M;->b:Ldbxyzptlk/db231222/V/L;

    .line 57
    return-void
.end method

.method constructor <init>(Ldbxyzptlk/db231222/V/y;Landroid/net/Uri;I)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iget-boolean v0, p1, Ldbxyzptlk/db231222/V/y;->k:Z

    if-eqz v0, :cond_0

    .line 47
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Picasso instance already shut down. Cannot submit new requests."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_0
    iput-object p1, p0, Ldbxyzptlk/db231222/V/M;->a:Ldbxyzptlk/db231222/V/y;

    .line 51
    new-instance v0, Ldbxyzptlk/db231222/V/L;

    invoke-direct {v0, p2, p3}, Ldbxyzptlk/db231222/V/L;-><init>(Landroid/net/Uri;I)V

    iput-object v0, p0, Ldbxyzptlk/db231222/V/M;->b:Ldbxyzptlk/db231222/V/L;

    .line 52
    return-void
.end method


# virtual methods
.method final a()Ldbxyzptlk/db231222/V/M;
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/V/M;->e:Z

    .line 129
    return-object p0
.end method

.method public final a(II)Ldbxyzptlk/db231222/V/M;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Ldbxyzptlk/db231222/V/M;->b:Ldbxyzptlk/db231222/V/L;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/V/L;->a(II)Ldbxyzptlk/db231222/V/L;

    .line 143
    return-object p0
.end method

.method public final a(Landroid/widget/ImageView;)V
    .locals 1

    .prologue
    .line 311
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ldbxyzptlk/db231222/V/M;->a(Landroid/widget/ImageView;Ldbxyzptlk/db231222/V/g;)V

    .line 312
    return-void
.end method

.method public final a(Landroid/widget/ImageView;Ldbxyzptlk/db231222/V/g;)V
    .locals 10

    .prologue
    .line 324
    if-nez p1, :cond_0

    .line 325
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Target must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 328
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/V/M;->b:Ldbxyzptlk/db231222/V/L;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/V/L;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 329
    iget-object v0, p0, Ldbxyzptlk/db231222/V/M;->a:Ldbxyzptlk/db231222/V/y;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/V/y;->a(Landroid/widget/ImageView;)V

    .line 330
    iget v0, p0, Ldbxyzptlk/db231222/V/M;->f:I

    iget-object v1, p0, Ldbxyzptlk/db231222/V/M;->g:Landroid/graphics/drawable/Drawable;

    invoke-static {p1, v0, v1}, Ldbxyzptlk/db231222/V/H;->a(Landroid/widget/ImageView;ILandroid/graphics/drawable/Drawable;)V

    .line 371
    :cond_1
    :goto_0
    return-void

    .line 334
    :cond_2
    iget-boolean v0, p0, Ldbxyzptlk/db231222/V/M;->e:Z

    if-eqz v0, :cond_5

    .line 335
    iget-object v0, p0, Ldbxyzptlk/db231222/V/M;->b:Ldbxyzptlk/db231222/V/L;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/V/L;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 336
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Fit cannot be used with resize."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 338
    :cond_3
    invoke-virtual {p1}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    .line 339
    invoke-virtual {p1}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v1

    .line 340
    if-nez v0, :cond_4

    if-nez v1, :cond_4

    .line 341
    iget v0, p0, Ldbxyzptlk/db231222/V/M;->f:I

    iget-object v1, p0, Ldbxyzptlk/db231222/V/M;->g:Landroid/graphics/drawable/Drawable;

    invoke-static {p1, v0, v1}, Ldbxyzptlk/db231222/V/H;->a(Landroid/widget/ImageView;ILandroid/graphics/drawable/Drawable;)V

    .line 342
    iget-object v0, p0, Ldbxyzptlk/db231222/V/M;->a:Ldbxyzptlk/db231222/V/y;

    new-instance v1, Ldbxyzptlk/db231222/V/l;

    invoke-direct {v1, p0, p1, p2}, Ldbxyzptlk/db231222/V/l;-><init>(Ldbxyzptlk/db231222/V/M;Landroid/widget/ImageView;Ldbxyzptlk/db231222/V/g;)V

    invoke-virtual {v0, p1, v1}, Ldbxyzptlk/db231222/V/y;->a(Landroid/widget/ImageView;Ldbxyzptlk/db231222/V/l;)V

    goto :goto_0

    .line 345
    :cond_4
    iget-object v2, p0, Ldbxyzptlk/db231222/V/M;->b:Ldbxyzptlk/db231222/V/L;

    invoke-virtual {v2, v0, v1}, Ldbxyzptlk/db231222/V/L;->a(II)Ldbxyzptlk/db231222/V/L;

    .line 348
    :cond_5
    iget-object v0, p0, Ldbxyzptlk/db231222/V/M;->a:Ldbxyzptlk/db231222/V/y;

    iget-object v1, p0, Ldbxyzptlk/db231222/V/M;->b:Ldbxyzptlk/db231222/V/L;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/V/L;->c()Ldbxyzptlk/db231222/V/J;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/V/y;->a(Ldbxyzptlk/db231222/V/J;)Ldbxyzptlk/db231222/V/J;

    move-result-object v3

    .line 349
    invoke-static {v3}, Ldbxyzptlk/db231222/V/U;->a(Ldbxyzptlk/db231222/V/J;)Ljava/lang/String;

    move-result-object v8

    .line 351
    iget-boolean v0, p0, Ldbxyzptlk/db231222/V/M;->c:Z

    if-nez v0, :cond_6

    .line 352
    iget-object v0, p0, Ldbxyzptlk/db231222/V/M;->a:Ldbxyzptlk/db231222/V/y;

    invoke-virtual {v0, v8}, Ldbxyzptlk/db231222/V/y;->b(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 353
    if-eqz v2, :cond_6

    .line 354
    iget-object v0, p0, Ldbxyzptlk/db231222/V/M;->a:Ldbxyzptlk/db231222/V/y;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/V/y;->a(Landroid/widget/ImageView;)V

    .line 355
    iget-object v0, p0, Ldbxyzptlk/db231222/V/M;->a:Ldbxyzptlk/db231222/V/y;

    iget-object v1, v0, Ldbxyzptlk/db231222/V/y;->c:Landroid/content/Context;

    sget-object v3, Ldbxyzptlk/db231222/V/E;->a:Ldbxyzptlk/db231222/V/E;

    iget-boolean v4, p0, Ldbxyzptlk/db231222/V/M;->d:Z

    iget-object v0, p0, Ldbxyzptlk/db231222/V/M;->a:Ldbxyzptlk/db231222/V/y;

    iget-boolean v5, v0, Ldbxyzptlk/db231222/V/y;->j:Z

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/db231222/V/H;->a(Landroid/widget/ImageView;Landroid/content/Context;Landroid/graphics/Bitmap;Ldbxyzptlk/db231222/V/E;ZZ)V

    .line 357
    if-eqz p2, :cond_1

    .line 358
    invoke-interface {p2}, Ldbxyzptlk/db231222/V/g;->a()V

    goto :goto_0

    .line 364
    :cond_6
    iget v0, p0, Ldbxyzptlk/db231222/V/M;->f:I

    iget-object v1, p0, Ldbxyzptlk/db231222/V/M;->g:Landroid/graphics/drawable/Drawable;

    invoke-static {p1, v0, v1}, Ldbxyzptlk/db231222/V/H;->a(Landroid/widget/ImageView;ILandroid/graphics/drawable/Drawable;)V

    .line 366
    new-instance v0, Ldbxyzptlk/db231222/V/t;

    iget-object v1, p0, Ldbxyzptlk/db231222/V/M;->a:Ldbxyzptlk/db231222/V/y;

    iget-boolean v4, p0, Ldbxyzptlk/db231222/V/M;->c:Z

    iget-boolean v5, p0, Ldbxyzptlk/db231222/V/M;->d:Z

    iget v6, p0, Ldbxyzptlk/db231222/V/M;->h:I

    iget-object v7, p0, Ldbxyzptlk/db231222/V/M;->i:Landroid/graphics/drawable/Drawable;

    move-object v2, p1

    move-object v9, p2

    invoke-direct/range {v0 .. v9}, Ldbxyzptlk/db231222/V/t;-><init>(Ldbxyzptlk/db231222/V/y;Landroid/widget/ImageView;Ldbxyzptlk/db231222/V/J;ZZILandroid/graphics/drawable/Drawable;Ljava/lang/String;Ldbxyzptlk/db231222/V/g;)V

    .line 370
    iget-object v1, p0, Ldbxyzptlk/db231222/V/M;->a:Ldbxyzptlk/db231222/V/y;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/V/y;->a(Ldbxyzptlk/db231222/V/a;)V

    goto/16 :goto_0
.end method
