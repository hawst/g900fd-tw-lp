.class final Ldbxyzptlk/db231222/V/p;
.super Landroid/content/BroadcastReceiver;
.source "panda.py"


# instance fields
.field final synthetic a:Ldbxyzptlk/db231222/V/m;

.field private final b:Landroid/net/ConnectivityManager;


# direct methods
.method constructor <init>(Ldbxyzptlk/db231222/V/m;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 271
    iput-object p1, p0, Ldbxyzptlk/db231222/V/p;->a:Ldbxyzptlk/db231222/V/m;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 272
    const-string v0, "connectivity"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Ldbxyzptlk/db231222/V/p;->b:Landroid/net/ConnectivityManager;

    .line 273
    return-void
.end method


# virtual methods
.method final a()V
    .locals 3

    .prologue
    .line 276
    iget-object v0, p0, Ldbxyzptlk/db231222/V/p;->a:Ldbxyzptlk/db231222/V/m;

    iget-object v0, v0, Ldbxyzptlk/db231222/V/m;->c:Ljava/util/concurrent/ExecutorService;

    instance-of v0, v0, Ldbxyzptlk/db231222/V/I;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldbxyzptlk/db231222/V/p;->a:Ldbxyzptlk/db231222/V/m;

    iget-object v0, v0, Ldbxyzptlk/db231222/V/m;->b:Landroid/content/Context;

    const-string v1, "android.permission.ACCESS_NETWORK_STATE"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/V/U;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 278
    :goto_0
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 279
    const-string v2, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 280
    if-eqz v0, :cond_0

    .line 281
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 283
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/V/p;->a:Ldbxyzptlk/db231222/V/m;

    iget-object v0, v0, Ldbxyzptlk/db231222/V/m;->b:Landroid/content/Context;

    invoke-virtual {v0, p0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 284
    return-void

    .line 276
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 287
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 288
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 290
    const-string v2, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 291
    iget-object v0, p0, Ldbxyzptlk/db231222/V/p;->a:Ldbxyzptlk/db231222/V/m;

    const-string v2, "state"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/V/m;->a(Z)V

    .line 295
    :cond_0
    :goto_0
    return-void

    .line 292
    :cond_1
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Ldbxyzptlk/db231222/V/p;->a:Ldbxyzptlk/db231222/V/m;

    iget-object v1, p0, Ldbxyzptlk/db231222/V/p;->b:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/V/m;->a(Landroid/net/NetworkInfo;)V

    goto :goto_0
.end method
