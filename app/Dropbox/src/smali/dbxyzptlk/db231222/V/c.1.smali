.class final Ldbxyzptlk/db231222/V/c;
.super Ldbxyzptlk/db231222/V/d;
.source "panda.py"


# instance fields
.field private o:Landroid/content/res/AssetManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/V/y;Ldbxyzptlk/db231222/V/m;Ldbxyzptlk/db231222/V/e;Ldbxyzptlk/db231222/V/O;Ldbxyzptlk/db231222/V/a;)V
    .locals 6

    .prologue
    .line 17
    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/V/d;-><init>(Ldbxyzptlk/db231222/V/y;Ldbxyzptlk/db231222/V/m;Ldbxyzptlk/db231222/V/e;Ldbxyzptlk/db231222/V/O;Ldbxyzptlk/db231222/V/a;)V

    .line 18
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/V/c;->o:Landroid/content/res/AssetManager;

    .line 19
    return-void
.end method


# virtual methods
.method final a(Ldbxyzptlk/db231222/V/J;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 22
    iget-object v0, p1, Ldbxyzptlk/db231222/V/J;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sget v1, Ldbxyzptlk/db231222/V/c;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 23
    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/V/c;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method final a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 31
    .line 32
    iget-object v0, p0, Ldbxyzptlk/db231222/V/c;->g:Ldbxyzptlk/db231222/V/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/V/J;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 34
    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 37
    :try_start_0
    iget-object v2, p0, Ldbxyzptlk/db231222/V/c;->o:Landroid/content/res/AssetManager;

    invoke-virtual {v2, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 38
    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    invoke-static {v1}, Ldbxyzptlk/db231222/V/U;->a(Ljava/io/InputStream;)V

    .line 42
    iget-object v1, p0, Ldbxyzptlk/db231222/V/c;->g:Ldbxyzptlk/db231222/V/J;

    iget v1, v1, Ldbxyzptlk/db231222/V/J;->d:I

    iget-object v2, p0, Ldbxyzptlk/db231222/V/c;->g:Ldbxyzptlk/db231222/V/J;

    iget v2, v2, Ldbxyzptlk/db231222/V/J;->e:I

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/V/c;->a(IILandroid/graphics/BitmapFactory$Options;)V

    .line 44
    :goto_0
    iget-object v1, p0, Ldbxyzptlk/db231222/V/c;->o:Landroid/content/res/AssetManager;

    invoke-virtual {v1, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 46
    const/4 v2, 0x0

    :try_start_1
    invoke-static {v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 48
    invoke-static {v1}, Ldbxyzptlk/db231222/V/U;->a(Ljava/io/InputStream;)V

    return-object v0

    .line 40
    :catchall_0
    move-exception v0

    invoke-static {v1}, Ldbxyzptlk/db231222/V/U;->a(Ljava/io/InputStream;)V

    throw v0

    .line 48
    :catchall_1
    move-exception v0

    invoke-static {v1}, Ldbxyzptlk/db231222/V/U;->a(Ljava/io/InputStream;)V

    throw v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method final a()Ldbxyzptlk/db231222/V/E;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Ldbxyzptlk/db231222/V/E;->b:Ldbxyzptlk/db231222/V/E;

    return-object v0
.end method
