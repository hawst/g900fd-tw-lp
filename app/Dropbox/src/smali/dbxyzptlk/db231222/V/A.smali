.class public final Ldbxyzptlk/db231222/V/A;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Ldbxyzptlk/db231222/V/q;

.field private c:Ljava/util/concurrent/ExecutorService;

.field private d:Ldbxyzptlk/db231222/V/e;

.field private e:Ldbxyzptlk/db231222/V/D;

.field private f:Ldbxyzptlk/db231222/V/F;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 407
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 408
    if-nez p1, :cond_0

    .line 409
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 411
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/V/A;->a:Landroid/content/Context;

    .line 412
    return-void
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/V/q;)Ldbxyzptlk/db231222/V/A;
    .locals 2

    .prologue
    .line 416
    if-nez p1, :cond_0

    .line 417
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Downloader must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 419
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/V/A;->b:Ldbxyzptlk/db231222/V/q;

    if-eqz v0, :cond_1

    .line 420
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Downloader already set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 422
    :cond_1
    iput-object p1, p0, Ldbxyzptlk/db231222/V/A;->b:Ldbxyzptlk/db231222/V/q;

    .line 423
    return-object p0
.end method

.method public final a()Ldbxyzptlk/db231222/V/y;
    .locals 15

    .prologue
    .line 487
    iget-object v1, p0, Ldbxyzptlk/db231222/V/A;->a:Landroid/content/Context;

    .line 489
    iget-object v0, p0, Ldbxyzptlk/db231222/V/A;->b:Ldbxyzptlk/db231222/V/q;

    if-nez v0, :cond_0

    .line 490
    invoke-static {v1}, Ldbxyzptlk/db231222/V/U;->a(Landroid/content/Context;)Ldbxyzptlk/db231222/V/q;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/V/A;->b:Ldbxyzptlk/db231222/V/q;

    .line 492
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/V/A;->d:Ldbxyzptlk/db231222/V/e;

    if-nez v0, :cond_1

    .line 493
    new-instance v0, Ldbxyzptlk/db231222/V/u;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/V/u;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/V/A;->d:Ldbxyzptlk/db231222/V/e;

    .line 495
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/V/A;->c:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_2

    .line 496
    new-instance v0, Ldbxyzptlk/db231222/V/I;

    invoke-direct {v0}, Ldbxyzptlk/db231222/V/I;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/V/A;->c:Ljava/util/concurrent/ExecutorService;

    .line 498
    :cond_2
    iget-object v0, p0, Ldbxyzptlk/db231222/V/A;->f:Ldbxyzptlk/db231222/V/F;

    if-nez v0, :cond_3

    .line 499
    sget-object v0, Ldbxyzptlk/db231222/V/F;->a:Ldbxyzptlk/db231222/V/F;

    iput-object v0, p0, Ldbxyzptlk/db231222/V/A;->f:Ldbxyzptlk/db231222/V/F;

    .line 502
    :cond_3
    new-instance v6, Ldbxyzptlk/db231222/V/O;

    iget-object v0, p0, Ldbxyzptlk/db231222/V/A;->d:Ldbxyzptlk/db231222/V/e;

    invoke-direct {v6, v0}, Ldbxyzptlk/db231222/V/O;-><init>(Ldbxyzptlk/db231222/V/e;)V

    .line 504
    new-instance v0, Ldbxyzptlk/db231222/V/m;

    iget-object v2, p0, Ldbxyzptlk/db231222/V/A;->c:Ljava/util/concurrent/ExecutorService;

    sget-object v3, Ldbxyzptlk/db231222/V/y;->a:Landroid/os/Handler;

    iget-object v4, p0, Ldbxyzptlk/db231222/V/A;->b:Ldbxyzptlk/db231222/V/q;

    iget-object v5, p0, Ldbxyzptlk/db231222/V/A;->d:Ldbxyzptlk/db231222/V/e;

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/V/m;-><init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;Landroid/os/Handler;Ldbxyzptlk/db231222/V/q;Ldbxyzptlk/db231222/V/e;Ldbxyzptlk/db231222/V/O;)V

    .line 506
    new-instance v7, Ldbxyzptlk/db231222/V/y;

    iget-object v10, p0, Ldbxyzptlk/db231222/V/A;->d:Ldbxyzptlk/db231222/V/e;

    iget-object v11, p0, Ldbxyzptlk/db231222/V/A;->e:Ldbxyzptlk/db231222/V/D;

    iget-object v12, p0, Ldbxyzptlk/db231222/V/A;->f:Ldbxyzptlk/db231222/V/F;

    iget-boolean v14, p0, Ldbxyzptlk/db231222/V/A;->g:Z

    move-object v8, v1

    move-object v9, v0

    move-object v13, v6

    invoke-direct/range {v7 .. v14}, Ldbxyzptlk/db231222/V/y;-><init>(Landroid/content/Context;Ldbxyzptlk/db231222/V/m;Ldbxyzptlk/db231222/V/e;Ldbxyzptlk/db231222/V/D;Ldbxyzptlk/db231222/V/F;Ldbxyzptlk/db231222/V/O;Z)V

    return-object v7
.end method
