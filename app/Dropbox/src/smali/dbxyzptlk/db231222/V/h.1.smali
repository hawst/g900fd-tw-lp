.class final Ldbxyzptlk/db231222/V/h;
.super Ldbxyzptlk/db231222/V/d;
.source "panda.py"


# instance fields
.field final o:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/V/y;Ldbxyzptlk/db231222/V/m;Ldbxyzptlk/db231222/V/e;Ldbxyzptlk/db231222/V/O;Ldbxyzptlk/db231222/V/a;)V
    .locals 6

    .prologue
    .line 37
    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/V/d;-><init>(Ldbxyzptlk/db231222/V/y;Ldbxyzptlk/db231222/V/m;Ldbxyzptlk/db231222/V/e;Ldbxyzptlk/db231222/V/O;Ldbxyzptlk/db231222/V/a;)V

    .line 38
    iput-object p1, p0, Ldbxyzptlk/db231222/V/h;->o:Landroid/content/Context;

    .line 39
    return-void
.end method

.method private a(Ljava/io/InputStream;Ldbxyzptlk/db231222/V/J;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 73
    if-nez p1, :cond_0

    .line 88
    :goto_0
    return-object v1

    .line 77
    :cond_0
    invoke-virtual {p2}, Ldbxyzptlk/db231222/V/J;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 79
    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 80
    invoke-direct {p0}, Ldbxyzptlk/db231222/V/h;->k()Ljava/io/InputStream;

    move-result-object v2

    .line 82
    const/4 v3, 0x0

    :try_start_0
    invoke-static {v2, v3, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    invoke-static {v2}, Ldbxyzptlk/db231222/V/U;->a(Ljava/io/InputStream;)V

    .line 86
    iget v2, p2, Ldbxyzptlk/db231222/V/J;->d:I

    iget v3, p2, Ldbxyzptlk/db231222/V/J;->e:I

    invoke-static {v2, v3, v0}, Ldbxyzptlk/db231222/V/h;->a(IILandroid/graphics/BitmapFactory$Options;)V

    .line 88
    :goto_1
    invoke-static {p1, v1, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0

    .line 84
    :catchall_0
    move-exception v0

    invoke-static {v2}, Ldbxyzptlk/db231222/V/U;->a(Ljava/io/InputStream;)V

    throw v0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method private k()Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 57
    iget-object v0, p0, Ldbxyzptlk/db231222/V/h;->o:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 58
    invoke-virtual {p0}, Ldbxyzptlk/db231222/V/h;->h()Ldbxyzptlk/db231222/V/J;

    move-result-object v0

    iget-object v0, v0, Ldbxyzptlk/db231222/V/J;->a:Landroid/net/Uri;

    .line 59
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 60
    invoke-static {v1, v0}, Landroid/provider/ContactsContract$Contacts;->lookupContact(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 61
    if-nez v0, :cond_0

    .line 62
    const/4 v0, 0x0

    .line 68
    :goto_0
    return-object v0

    .line 65
    :cond_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-ge v2, v3, :cond_1

    .line 66
    invoke-static {v1, v0}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0

    .line 68
    :cond_1
    invoke-static {v1, v0}, Ldbxyzptlk/db231222/V/i;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method final a(Ldbxyzptlk/db231222/V/J;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 43
    const/4 v1, 0x0

    .line 45
    :try_start_0
    invoke-direct {p0}, Ldbxyzptlk/db231222/V/h;->k()Ljava/io/InputStream;

    move-result-object v1

    .line 46
    invoke-direct {p0, v1, p1}, Ldbxyzptlk/db231222/V/h;->a(Ljava/io/InputStream;Ldbxyzptlk/db231222/V/J;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 48
    invoke-static {v1}, Ldbxyzptlk/db231222/V/U;->a(Ljava/io/InputStream;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, Ldbxyzptlk/db231222/V/U;->a(Ljava/io/InputStream;)V

    throw v0
.end method

.method final a()Ldbxyzptlk/db231222/V/E;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Ldbxyzptlk/db231222/V/E;->b:Ldbxyzptlk/db231222/V/E;

    return-object v0
.end method
