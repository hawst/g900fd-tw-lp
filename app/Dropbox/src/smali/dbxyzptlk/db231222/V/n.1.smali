.class final Ldbxyzptlk/db231222/V/n;
.super Landroid/os/Handler;
.source "panda.py"


# instance fields
.field final synthetic a:Ldbxyzptlk/db231222/V/m;


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/V/m;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Ldbxyzptlk/db231222/V/n;->a:Ldbxyzptlk/db231222/V/m;

    .line 211
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 212
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 215
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 255
    :pswitch_0
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown handler message received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 217
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ldbxyzptlk/db231222/V/a;

    .line 218
    iget-object v1, p0, Ldbxyzptlk/db231222/V/n;->a:Ldbxyzptlk/db231222/V/m;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/V/m;->c(Ldbxyzptlk/db231222/V/a;)V

    .line 257
    :goto_0
    return-void

    .line 222
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ldbxyzptlk/db231222/V/a;

    .line 223
    iget-object v1, p0, Ldbxyzptlk/db231222/V/n;->a:Ldbxyzptlk/db231222/V/m;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/V/m;->d(Ldbxyzptlk/db231222/V/a;)V

    goto :goto_0

    .line 227
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ldbxyzptlk/db231222/V/d;

    .line 228
    iget-object v1, p0, Ldbxyzptlk/db231222/V/n;->a:Ldbxyzptlk/db231222/V/m;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/V/m;->e(Ldbxyzptlk/db231222/V/d;)V

    goto :goto_0

    .line 232
    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ldbxyzptlk/db231222/V/d;

    .line 233
    iget-object v1, p0, Ldbxyzptlk/db231222/V/n;->a:Ldbxyzptlk/db231222/V/m;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/V/m;->d(Ldbxyzptlk/db231222/V/d;)V

    goto :goto_0

    .line 237
    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ldbxyzptlk/db231222/V/d;

    .line 238
    iget-object v1, p0, Ldbxyzptlk/db231222/V/n;->a:Ldbxyzptlk/db231222/V/m;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/V/m;->f(Ldbxyzptlk/db231222/V/d;)V

    goto :goto_0

    .line 242
    :pswitch_6
    iget-object v0, p0, Ldbxyzptlk/db231222/V/n;->a:Ldbxyzptlk/db231222/V/m;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/V/m;->b()V

    goto :goto_0

    .line 246
    :pswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/net/NetworkInfo;

    .line 247
    iget-object v1, p0, Ldbxyzptlk/db231222/V/n;->a:Ldbxyzptlk/db231222/V/m;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/V/m;->b(Landroid/net/NetworkInfo;)V

    goto :goto_0

    .line 251
    :pswitch_8
    iget-object v1, p0, Ldbxyzptlk/db231222/V/n;->a:Ldbxyzptlk/db231222/V/m;

    iget v2, p1, Landroid/os/Message;->arg1:I

    if-ne v2, v0, :cond_0

    :goto_1
    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/V/m;->b(Z)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 215
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
