.class public final enum Ldbxyzptlk/db231222/V/E;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldbxyzptlk/db231222/V/E;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ldbxyzptlk/db231222/V/E;

.field public static final enum b:Ldbxyzptlk/db231222/V/E;

.field public static final enum c:Ldbxyzptlk/db231222/V/E;

.field private static final synthetic e:[Ldbxyzptlk/db231222/V/E;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 512
    new-instance v0, Ldbxyzptlk/db231222/V/E;

    const-string v1, "MEMORY"

    const v2, -0xff0100

    invoke-direct {v0, v1, v3, v2}, Ldbxyzptlk/db231222/V/E;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldbxyzptlk/db231222/V/E;->a:Ldbxyzptlk/db231222/V/E;

    .line 513
    new-instance v0, Ldbxyzptlk/db231222/V/E;

    const-string v1, "DISK"

    const/16 v2, -0x100

    invoke-direct {v0, v1, v4, v2}, Ldbxyzptlk/db231222/V/E;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldbxyzptlk/db231222/V/E;->b:Ldbxyzptlk/db231222/V/E;

    .line 514
    new-instance v0, Ldbxyzptlk/db231222/V/E;

    const-string v1, "NETWORK"

    const/high16 v2, -0x10000

    invoke-direct {v0, v1, v5, v2}, Ldbxyzptlk/db231222/V/E;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldbxyzptlk/db231222/V/E;->c:Ldbxyzptlk/db231222/V/E;

    .line 511
    const/4 v0, 0x3

    new-array v0, v0, [Ldbxyzptlk/db231222/V/E;

    sget-object v1, Ldbxyzptlk/db231222/V/E;->a:Ldbxyzptlk/db231222/V/E;

    aput-object v1, v0, v3

    sget-object v1, Ldbxyzptlk/db231222/V/E;->b:Ldbxyzptlk/db231222/V/E;

    aput-object v1, v0, v4

    sget-object v1, Ldbxyzptlk/db231222/V/E;->c:Ldbxyzptlk/db231222/V/E;

    aput-object v1, v0, v5

    sput-object v0, Ldbxyzptlk/db231222/V/E;->e:[Ldbxyzptlk/db231222/V/E;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 518
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 519
    iput p3, p0, Ldbxyzptlk/db231222/V/E;->d:I

    .line 520
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldbxyzptlk/db231222/V/E;
    .locals 1

    .prologue
    .line 511
    const-class v0, Ldbxyzptlk/db231222/V/E;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/V/E;

    return-object v0
.end method

.method public static values()[Ldbxyzptlk/db231222/V/E;
    .locals 1

    .prologue
    .line 511
    sget-object v0, Ldbxyzptlk/db231222/V/E;->e:[Ldbxyzptlk/db231222/V/E;

    invoke-virtual {v0}, [Ldbxyzptlk/db231222/V/E;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/V/E;

    return-object v0
.end method
