.class final Ldbxyzptlk/db231222/V/H;
.super Landroid/graphics/drawable/Drawable;
.source "panda.py"


# static fields
.field private static final e:Landroid/graphics/Paint;


# instance fields
.field final a:Landroid/graphics/drawable/BitmapDrawable;

.field b:Landroid/graphics/drawable/Drawable;

.field c:J

.field d:Z

.field private final f:Z

.field private final g:F

.field private final h:Ldbxyzptlk/db231222/V/E;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/V/H;->e:Landroid/graphics/Paint;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;Ldbxyzptlk/db231222/V/E;ZZ)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 76
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 77
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 79
    iput-boolean p6, p0, Ldbxyzptlk/db231222/V/H;->f:Z

    .line 80
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    iput v2, p0, Ldbxyzptlk/db231222/V/H;->g:F

    .line 82
    iput-object p4, p0, Ldbxyzptlk/db231222/V/H;->h:Ldbxyzptlk/db231222/V/E;

    .line 84
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v2, v0, p3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Ldbxyzptlk/db231222/V/H;->a:Landroid/graphics/drawable/BitmapDrawable;

    .line 86
    sget-object v0, Ldbxyzptlk/db231222/V/E;->a:Ldbxyzptlk/db231222/V/E;

    if-eq p4, v0, :cond_1

    if-nez p5, :cond_1

    move v0, v1

    .line 87
    :goto_0
    if-eqz v0, :cond_0

    .line 88
    iput-object p2, p0, Ldbxyzptlk/db231222/V/H;->b:Landroid/graphics/drawable/Drawable;

    .line 89
    iput-boolean v1, p0, Ldbxyzptlk/db231222/V/H;->d:Z

    .line 90
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/V/H;->c:J

    .line 92
    :cond_0
    return-void

    .line 86
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/graphics/Point;I)Landroid/graphics/Path;
    .locals 5

    .prologue
    .line 194
    new-instance v0, Landroid/graphics/Point;

    iget v1, p0, Landroid/graphics/Point;->x:I

    add-int/2addr v1, p1

    iget v2, p0, Landroid/graphics/Point;->y:I

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 195
    new-instance v1, Landroid/graphics/Point;

    iget v2, p0, Landroid/graphics/Point;->x:I

    iget v3, p0, Landroid/graphics/Point;->y:I

    add-int/2addr v3, p1

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 197
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 198
    iget v3, p0, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    iget v4, p0, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 199
    iget v3, v0, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    invoke-virtual {v2, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 200
    iget v0, v1, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 202
    return-object v2
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 184
    sget-object v0, Ldbxyzptlk/db231222/V/H;->e:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 185
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    const/high16 v1, 0x41800000    # 16.0f

    iget v2, p0, Ldbxyzptlk/db231222/V/H;->g:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/V/H;->a(Landroid/graphics/Point;I)Landroid/graphics/Path;

    move-result-object v0

    .line 186
    sget-object v1, Ldbxyzptlk/db231222/V/H;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 188
    sget-object v0, Ldbxyzptlk/db231222/V/H;->e:Landroid/graphics/Paint;

    iget-object v1, p0, Ldbxyzptlk/db231222/V/H;->h:Ldbxyzptlk/db231222/V/E;

    iget v1, v1, Ldbxyzptlk/db231222/V/E;->d:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 189
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    const/high16 v1, 0x41700000    # 15.0f

    iget v2, p0, Ldbxyzptlk/db231222/V/H;->g:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/V/H;->a(Landroid/graphics/Point;I)Landroid/graphics/Path;

    move-result-object v0

    .line 190
    sget-object v1, Ldbxyzptlk/db231222/V/H;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 191
    return-void
.end method

.method private a(Landroid/graphics/drawable/Drawable;)V
    .locals 8

    .prologue
    .line 158
    invoke-virtual {p0}, Ldbxyzptlk/db231222/V/H;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 160
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 161
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    .line 162
    int-to-float v3, v1

    int-to-float v4, v2

    div-float/2addr v3, v4

    .line 164
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    .line 165
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    .line 166
    int-to-float v6, v4

    int-to-float v7, v5

    div-float/2addr v6, v7

    .line 168
    cmpg-float v3, v6, v3

    if-gez v3, :cond_0

    .line 169
    int-to-float v2, v2

    int-to-float v3, v5

    div-float/2addr v2, v3

    .line 170
    int-to-float v3, v4

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 171
    iget v3, v0, Landroid/graphics/Rect;->left:I

    sub-int v1, v2, v1

    div-int/lit8 v1, v1, 0x2

    sub-int v1, v3, v1

    .line 172
    add-int/2addr v2, v1

    .line 173
    iget v3, v0, Landroid/graphics/Rect;->top:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1, v1, v3, v2, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 181
    :goto_0
    return-void

    .line 175
    :cond_0
    int-to-float v1, v1

    int-to-float v3, v4

    div-float/2addr v1, v3

    .line 176
    int-to-float v3, v5

    mul-float/2addr v1, v3

    float-to-int v1, v1

    .line 177
    iget v3, v0, Landroid/graphics/Rect;->top:I

    sub-int v2, v1, v2

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v3, v2

    .line 178
    add-int/2addr v1, v2

    .line 179
    iget v3, v0, Landroid/graphics/Rect;->left:I

    iget v0, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p1, v3, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0
.end method

.method static a(Landroid/widget/ImageView;ILandroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 58
    if-eqz p1, :cond_0

    .line 59
    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 63
    :goto_0
    return-void

    .line 61
    :cond_0
    invoke-virtual {p0, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method static a(Landroid/widget/ImageView;Landroid/content/Context;Landroid/graphics/Bitmap;Ldbxyzptlk/db231222/V/E;ZZ)V
    .locals 7

    .prologue
    .line 47
    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 48
    new-instance v0, Ldbxyzptlk/db231222/V/H;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/V/H;-><init>(Landroid/content/Context;Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;Ldbxyzptlk/db231222/V/E;ZZ)V

    .line 50
    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 51
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 95
    iget-boolean v0, p0, Ldbxyzptlk/db231222/V/H;->d:Z

    if-nez v0, :cond_1

    .line 96
    iget-object v0, p0, Ldbxyzptlk/db231222/V/H;->a:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 116
    :goto_0
    iget-boolean v0, p0, Ldbxyzptlk/db231222/V/H;->f:Z

    if-eqz v0, :cond_0

    .line 117
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/V/H;->a(Landroid/graphics/Canvas;)V

    .line 119
    :cond_0
    return-void

    .line 98
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Ldbxyzptlk/db231222/V/H;->c:J

    sub-long/2addr v0, v2

    long-to-float v0, v0

    const/high16 v1, 0x43480000    # 200.0f

    div-float/2addr v0, v1

    .line 99
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_2

    .line 100
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/V/H;->d:Z

    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/V/H;->b:Landroid/graphics/drawable/Drawable;

    .line 102
    iget-object v0, p0, Ldbxyzptlk/db231222/V/H;->a:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 104
    :cond_2
    iget-object v1, p0, Ldbxyzptlk/db231222/V/H;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    .line 105
    iget-object v1, p0, Ldbxyzptlk/db231222/V/H;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 108
    :cond_3
    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 109
    iget-object v1, p0, Ldbxyzptlk/db231222/V/H;->a:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/BitmapDrawable;->setAlpha(I)V

    .line 110
    iget-object v0, p0, Ldbxyzptlk/db231222/V/H;->a:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 111
    iget-object v0, p0, Ldbxyzptlk/db231222/V/H;->a:Landroid/graphics/drawable/BitmapDrawable;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setAlpha(I)V

    .line 112
    invoke-virtual {p0}, Ldbxyzptlk/db231222/V/H;->invalidateSelf()V

    goto :goto_0
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Ldbxyzptlk/db231222/V/H;->a:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v0

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Ldbxyzptlk/db231222/V/H;->a:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v0

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Ldbxyzptlk/db231222/V/H;->a:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getOpacity()I

    move-result v0

    return v0
.end method

.method protected final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 148
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 150
    iget-object v0, p0, Ldbxyzptlk/db231222/V/H;->a:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 151
    iget-object v0, p0, Ldbxyzptlk/db231222/V/H;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Ldbxyzptlk/db231222/V/H;->b:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/V/H;->a(Landroid/graphics/drawable/Drawable;)V

    .line 155
    :cond_0
    return-void
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Ldbxyzptlk/db231222/V/H;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Ldbxyzptlk/db231222/V/H;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 133
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/V/H;->a:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->setAlpha(I)V

    .line 134
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Ldbxyzptlk/db231222/V/H;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Ldbxyzptlk/db231222/V/H;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 140
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/V/H;->a:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 141
    return-void
.end method
