.class public final Ldbxyzptlk/db231222/al/c;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:I

.field private b:[F

.field private c:[F

.field private d:[F

.field private e:[I

.field private f:F

.field private g:F

.field private h:F

.field private i:F

.field private j:F

.field private k:F

.field private l:F

.field private m:F

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:I

.field private t:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0xa

    .line 478
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 482
    new-array v0, v1, [F

    iput-object v0, p0, Ldbxyzptlk/db231222/al/c;->b:[F

    .line 483
    new-array v0, v1, [F

    iput-object v0, p0, Ldbxyzptlk/db231222/al/c;->c:[F

    .line 484
    new-array v0, v1, [F

    iput-object v0, p0, Ldbxyzptlk/db231222/al/c;->d:[F

    .line 485
    new-array v0, v1, [I

    iput-object v0, p0, Ldbxyzptlk/db231222/al/c;->e:[I

    return-void
.end method

.method private a(I)I
    .locals 7

    .prologue
    .line 599
    const/4 v2, 0x0

    const v1, 0x8000

    const/16 v0, 0xf

    move v4, v1

    move v1, p1

    move v6, v2

    move v2, v0

    move v0, v6

    .line 601
    :goto_0
    shl-int/lit8 v3, v0, 0x1

    add-int v5, v3, v4

    add-int/lit8 v3, v2, -0x1

    shl-int v2, v5, v2

    if-lt v1, v2, :cond_0

    .line 602
    add-int/2addr v0, v4

    .line 603
    sub-int/2addr v1, v2

    .line 605
    :cond_0
    shr-int/lit8 v2, v4, 0x1

    if-gtz v2, :cond_1

    .line 606
    return v0

    :cond_1
    move v4, v2

    move v2, v3

    goto :goto_0
.end method

.method private a(I[F[F[F[IIZJ)V
    .locals 6

    .prologue
    const/high16 v5, 0x3f000000    # 0.5f

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 510
    iput-wide p8, p0, Ldbxyzptlk/db231222/al/c;->t:J

    .line 511
    iput p6, p0, Ldbxyzptlk/db231222/al/c;->s:I

    .line 512
    iput p1, p0, Ldbxyzptlk/db231222/al/c;->a:I

    move v0, v2

    .line 513
    :goto_0
    if-ge v0, p1, :cond_0

    .line 514
    iget-object v3, p0, Ldbxyzptlk/db231222/al/c;->b:[F

    aget v4, p2, v0

    aput v4, v3, v0

    .line 515
    iget-object v3, p0, Ldbxyzptlk/db231222/al/c;->c:[F

    aget v4, p3, v0

    aput v4, v3, v0

    .line 516
    iget-object v3, p0, Ldbxyzptlk/db231222/al/c;->d:[F

    aget v4, p4, v0

    aput v4, v3, v0

    .line 517
    iget-object v3, p0, Ldbxyzptlk/db231222/al/c;->e:[I

    aget v4, p5, v0

    aput v4, v3, v0

    .line 513
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 519
    :cond_0
    iput-boolean p7, p0, Ldbxyzptlk/db231222/al/c;->n:Z

    .line 520
    const/4 v0, 0x2

    if-lt p1, v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Ldbxyzptlk/db231222/al/c;->o:Z

    .line 522
    iget-boolean v0, p0, Ldbxyzptlk/db231222/al/c;->o:Z

    if-eqz v0, :cond_2

    .line 523
    aget v0, p2, v2

    aget v3, p2, v1

    add-float/2addr v0, v3

    mul-float/2addr v0, v5

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->f:F

    .line 524
    aget v0, p3, v2

    aget v3, p3, v1

    add-float/2addr v0, v3

    mul-float/2addr v0, v5

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->g:F

    .line 525
    aget v0, p4, v2

    aget v3, p4, v1

    add-float/2addr v0, v3

    mul-float/2addr v0, v5

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->h:F

    .line 526
    aget v0, p2, v1

    aget v3, p2, v2

    sub-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->i:F

    .line 527
    aget v0, p3, v1

    aget v1, p3, v2

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->j:F

    .line 537
    :goto_2
    iput-boolean v2, p0, Ldbxyzptlk/db231222/al/c;->r:Z

    iput-boolean v2, p0, Ldbxyzptlk/db231222/al/c;->q:Z

    iput-boolean v2, p0, Ldbxyzptlk/db231222/al/c;->p:Z

    .line 538
    return-void

    :cond_1
    move v0, v2

    .line 520
    goto :goto_1

    .line 531
    :cond_2
    aget v0, p2, v2

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->f:F

    .line 532
    aget v0, p3, v2

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->g:F

    .line 533
    aget v0, p4, v2

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->h:F

    .line 534
    const/4 v0, 0x0

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->j:F

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->i:F

    goto :goto_2
.end method

.method static synthetic a(Ldbxyzptlk/db231222/al/c;I[F[F[F[IIZJ)V
    .locals 0

    .prologue
    .line 478
    invoke-direct/range {p0 .. p9}, Ldbxyzptlk/db231222/al/c;->a(I[F[F[F[IIZJ)V

    return-void
.end method

.method static synthetic b(Ldbxyzptlk/db231222/al/c;)J
    .locals 2

    .prologue
    .line 478
    iget-wide v0, p0, Ldbxyzptlk/db231222/al/c;->t:J

    return-wide v0
.end method

.method private j()F
    .locals 3

    .prologue
    .line 611
    iget-boolean v0, p0, Ldbxyzptlk/db231222/al/c;->p:Z

    if-nez v0, :cond_0

    .line 612
    iget v0, p0, Ldbxyzptlk/db231222/al/c;->i:F

    iget v1, p0, Ldbxyzptlk/db231222/al/c;->i:F

    mul-float/2addr v0, v1

    iget v1, p0, Ldbxyzptlk/db231222/al/c;->j:F

    iget v2, p0, Ldbxyzptlk/db231222/al/c;->j:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->l:F

    .line 613
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/db231222/al/c;->p:Z

    .line 615
    :cond_0
    iget v0, p0, Ldbxyzptlk/db231222/al/c;->l:F

    return v0
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/al/c;)V
    .locals 3

    .prologue
    .line 545
    iget v0, p1, Ldbxyzptlk/db231222/al/c;->a:I

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->a:I

    .line 546
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Ldbxyzptlk/db231222/al/c;->a:I

    if-ge v0, v1, :cond_0

    .line 547
    iget-object v1, p0, Ldbxyzptlk/db231222/al/c;->b:[F

    iget-object v2, p1, Ldbxyzptlk/db231222/al/c;->b:[F

    aget v2, v2, v0

    aput v2, v1, v0

    .line 548
    iget-object v1, p0, Ldbxyzptlk/db231222/al/c;->c:[F

    iget-object v2, p1, Ldbxyzptlk/db231222/al/c;->c:[F

    aget v2, v2, v0

    aput v2, v1, v0

    .line 549
    iget-object v1, p0, Ldbxyzptlk/db231222/al/c;->d:[F

    iget-object v2, p1, Ldbxyzptlk/db231222/al/c;->d:[F

    aget v2, v2, v0

    aput v2, v1, v0

    .line 550
    iget-object v1, p0, Ldbxyzptlk/db231222/al/c;->e:[I

    iget-object v2, p1, Ldbxyzptlk/db231222/al/c;->e:[I

    aget v2, v2, v0

    aput v2, v1, v0

    .line 546
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 552
    :cond_0
    iget v0, p1, Ldbxyzptlk/db231222/al/c;->f:F

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->f:F

    .line 553
    iget v0, p1, Ldbxyzptlk/db231222/al/c;->g:F

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->g:F

    .line 554
    iget v0, p1, Ldbxyzptlk/db231222/al/c;->h:F

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->h:F

    .line 555
    iget v0, p1, Ldbxyzptlk/db231222/al/c;->i:F

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->i:F

    .line 556
    iget v0, p1, Ldbxyzptlk/db231222/al/c;->j:F

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->j:F

    .line 557
    iget v0, p1, Ldbxyzptlk/db231222/al/c;->k:F

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->k:F

    .line 558
    iget v0, p1, Ldbxyzptlk/db231222/al/c;->l:F

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->l:F

    .line 559
    iget v0, p1, Ldbxyzptlk/db231222/al/c;->m:F

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->m:F

    .line 560
    iget-boolean v0, p1, Ldbxyzptlk/db231222/al/c;->n:Z

    iput-boolean v0, p0, Ldbxyzptlk/db231222/al/c;->n:Z

    .line 561
    iget v0, p1, Ldbxyzptlk/db231222/al/c;->s:I

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->s:I

    .line 562
    iget-boolean v0, p1, Ldbxyzptlk/db231222/al/c;->o:Z

    iput-boolean v0, p0, Ldbxyzptlk/db231222/al/c;->o:Z

    .line 563
    iget-boolean v0, p1, Ldbxyzptlk/db231222/al/c;->q:Z

    iput-boolean v0, p0, Ldbxyzptlk/db231222/al/c;->q:Z

    .line 564
    iget-boolean v0, p1, Ldbxyzptlk/db231222/al/c;->p:Z

    iput-boolean v0, p0, Ldbxyzptlk/db231222/al/c;->p:Z

    .line 565
    iget-boolean v0, p1, Ldbxyzptlk/db231222/al/c;->r:Z

    iput-boolean v0, p0, Ldbxyzptlk/db231222/al/c;->r:Z

    .line 566
    iget-wide v0, p1, Ldbxyzptlk/db231222/al/c;->t:J

    iput-wide v0, p0, Ldbxyzptlk/db231222/al/c;->t:J

    .line 567
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 579
    iget v0, p0, Ldbxyzptlk/db231222/al/c;->i:F

    iget v1, p0, Ldbxyzptlk/db231222/al/c;->i:F

    mul-float/2addr v0, v1

    iget v1, p0, Ldbxyzptlk/db231222/al/c;->j:F

    iget v2, p0, Ldbxyzptlk/db231222/al/c;->j:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    const/high16 v1, 0x45c80000    # 6400.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b()F
    .locals 1

    .prologue
    .line 589
    iget v0, p0, Ldbxyzptlk/db231222/al/c;->i:F

    return v0
.end method

.method final c()F
    .locals 1

    .prologue
    .line 594
    iget v0, p0, Ldbxyzptlk/db231222/al/c;->j:F

    return v0
.end method

.method public final d()F
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 620
    iget-boolean v1, p0, Ldbxyzptlk/db231222/al/c;->q:Z

    if-nez v1, :cond_1

    .line 621
    iget-boolean v1, p0, Ldbxyzptlk/db231222/al/c;->o:Z

    if-nez v1, :cond_2

    .line 622
    iput v0, p0, Ldbxyzptlk/db231222/al/c;->k:F

    .line 635
    :cond_0
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/db231222/al/c;->q:Z

    .line 637
    :cond_1
    iget v0, p0, Ldbxyzptlk/db231222/al/c;->k:F

    return v0

    .line 627
    :cond_2
    invoke-direct {p0}, Ldbxyzptlk/db231222/al/c;->j()F

    move-result v1

    .line 628
    cmpl-float v2, v1, v0

    if-nez v2, :cond_4

    :goto_1
    iput v0, p0, Ldbxyzptlk/db231222/al/c;->k:F

    .line 630
    iget v0, p0, Ldbxyzptlk/db231222/al/c;->k:F

    iget v1, p0, Ldbxyzptlk/db231222/al/c;->i:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_3

    .line 631
    iget v0, p0, Ldbxyzptlk/db231222/al/c;->i:F

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->k:F

    .line 632
    :cond_3
    iget v0, p0, Ldbxyzptlk/db231222/al/c;->k:F

    iget v1, p0, Ldbxyzptlk/db231222/al/c;->j:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 633
    iget v0, p0, Ldbxyzptlk/db231222/al/c;->j:F

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->k:F

    goto :goto_0

    .line 628
    :cond_4
    const/high16 v0, 0x43800000    # 256.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/al/c;->a(I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x41800000    # 16.0f

    div-float/2addr v0, v1

    goto :goto_1
.end method

.method public final e()F
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 645
    iget-boolean v0, p0, Ldbxyzptlk/db231222/al/c;->r:Z

    if-nez v0, :cond_0

    .line 646
    iget-boolean v0, p0, Ldbxyzptlk/db231222/al/c;->o:Z

    if-nez v0, :cond_1

    .line 647
    const/4 v0, 0x0

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->m:F

    .line 650
    :goto_0
    iput-boolean v4, p0, Ldbxyzptlk/db231222/al/c;->r:Z

    .line 652
    :cond_0
    iget v0, p0, Ldbxyzptlk/db231222/al/c;->m:F

    return v0

    .line 649
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/al/c;->c:[F

    aget v0, v0, v4

    iget-object v1, p0, Ldbxyzptlk/db231222/al/c;->c:[F

    aget v1, v1, v5

    sub-float/2addr v0, v1

    float-to-double v0, v0

    iget-object v2, p0, Ldbxyzptlk/db231222/al/c;->b:[F

    aget v2, v2, v4

    iget-object v3, p0, Ldbxyzptlk/db231222/al/c;->b:[F

    aget v3, v3, v5

    sub-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Ldbxyzptlk/db231222/al/c;->m:F

    goto :goto_0
.end method

.method public final f()F
    .locals 1

    .prologue
    .line 659
    iget v0, p0, Ldbxyzptlk/db231222/al/c;->f:F

    return v0
.end method

.method public final g()F
    .locals 1

    .prologue
    .line 669
    iget v0, p0, Ldbxyzptlk/db231222/al/c;->g:F

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 700
    iget-boolean v0, p0, Ldbxyzptlk/db231222/al/c;->n:Z

    return v0
.end method

.method public final i()J
    .locals 2

    .prologue
    .line 708
    iget-wide v0, p0, Ldbxyzptlk/db231222/al/c;->t:J

    return-wide v0
.end method
