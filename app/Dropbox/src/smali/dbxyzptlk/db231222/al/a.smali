.class public final Ldbxyzptlk/db231222/al/a;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static A:Ljava/lang/reflect/Method;

.field private static B:Ljava/lang/reflect/Method;

.field private static C:Ljava/lang/reflect/Method;

.field private static D:Ljava/lang/reflect/Method;

.field private static E:Ljava/lang/reflect/Method;

.field private static F:Ljava/lang/reflect/Method;

.field private static G:I

.field private static H:I

.field private static final I:[F

.field private static final J:[F

.field private static final K:[F

.field private static final L:[I

.field public static final b:Z

.field private static y:Ljava/lang/reflect/Method;

.field private static z:Ljava/lang/reflect/Method;


# instance fields
.field a:Ldbxyzptlk/db231222/al/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/al/b",
            "<TT;>;"
        }
    .end annotation
.end field

.field private c:Ldbxyzptlk/db231222/al/c;

.field private d:Ldbxyzptlk/db231222/al/c;

.field private e:F

.field private f:F

.field private g:F

.field private h:F

.field private i:F

.field private j:F

.field private k:Z

.field private l:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private m:Ldbxyzptlk/db231222/al/d;

.field private n:J

.field private o:J

.field private p:F

.field private q:F

.field private r:F

.field private s:F

.field private t:F

.field private u:F

.field private v:F

.field private w:F

.field private x:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v7, 0xa

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 215
    const/4 v2, 0x6

    sput v2, Ldbxyzptlk/db231222/al/a;->G:I

    .line 216
    const/16 v2, 0x8

    sput v2, Ldbxyzptlk/db231222/al/a;->H:I

    .line 222
    :try_start_0
    const-class v2, Landroid/view/MotionEvent;

    const-string v3, "getPointerCount"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Ldbxyzptlk/db231222/al/a;->y:Ljava/lang/reflect/Method;

    .line 223
    const-class v2, Landroid/view/MotionEvent;

    const-string v3, "findPointerIndex"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Ldbxyzptlk/db231222/al/a;->z:Ljava/lang/reflect/Method;

    .line 224
    const-class v2, Landroid/view/MotionEvent;

    const-string v3, "getPressure"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Ldbxyzptlk/db231222/al/a;->A:Ljava/lang/reflect/Method;

    .line 225
    const-class v2, Landroid/view/MotionEvent;

    const-string v3, "getHistoricalX"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Ldbxyzptlk/db231222/al/a;->B:Ljava/lang/reflect/Method;

    .line 226
    const-class v2, Landroid/view/MotionEvent;

    const-string v3, "getHistoricalY"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Ldbxyzptlk/db231222/al/a;->C:Ljava/lang/reflect/Method;

    .line 227
    const-class v2, Landroid/view/MotionEvent;

    const-string v3, "getHistoricalPressure"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Ldbxyzptlk/db231222/al/a;->D:Ljava/lang/reflect/Method;

    .line 228
    const-class v2, Landroid/view/MotionEvent;

    const-string v3, "getX"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Ldbxyzptlk/db231222/al/a;->E:Ljava/lang/reflect/Method;

    .line 229
    const-class v2, Landroid/view/MotionEvent;

    const-string v3, "getY"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Ldbxyzptlk/db231222/al/a;->F:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 234
    :goto_0
    sput-boolean v0, Ldbxyzptlk/db231222/al/a;->b:Z

    .line 235
    sget-boolean v0, Ldbxyzptlk/db231222/al/a;->b:Z

    if-eqz v0, :cond_0

    .line 239
    :try_start_1
    const-class v0, Landroid/view/MotionEvent;

    const-string v1, "ACTION_POINTER_UP"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    sput v0, Ldbxyzptlk/db231222/al/a;->G:I

    .line 240
    const-class v0, Landroid/view/MotionEvent;

    const-string v1, "ACTION_POINTER_INDEX_SHIFT"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    sput v0, Ldbxyzptlk/db231222/al/a;->H:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 248
    :cond_0
    :goto_1
    new-array v0, v7, [F

    sput-object v0, Ldbxyzptlk/db231222/al/a;->I:[F

    .line 249
    new-array v0, v7, [F

    sput-object v0, Ldbxyzptlk/db231222/al/a;->J:[F

    .line 250
    new-array v0, v7, [F

    sput-object v0, Ldbxyzptlk/db231222/al/a;->K:[F

    .line 251
    new-array v0, v7, [I

    sput-object v0, Ldbxyzptlk/db231222/al/a;->L:[I

    return-void

    .line 231
    :catch_0
    move-exception v0

    .line 232
    const-string v2, "MultiTouchController"

    const-string v3, "static initializer failed"

    invoke-static {v2, v3, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    goto :goto_0

    .line 241
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/al/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/al/b",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 172
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Ldbxyzptlk/db231222/al/a;-><init>(Ldbxyzptlk/db231222/al/b;Z)V

    .line 173
    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/al/b;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/al/b",
            "<TT;>;Z)V"
        }
    .end annotation

    .prologue
    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/al/a;->l:Ljava/lang/Object;

    .line 134
    new-instance v0, Ldbxyzptlk/db231222/al/d;

    invoke-direct {v0}, Ldbxyzptlk/db231222/al/d;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/al/a;->m:Ldbxyzptlk/db231222/al/d;

    .line 149
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Ldbxyzptlk/db231222/al/a;->v:F

    .line 152
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Ldbxyzptlk/db231222/al/a;->w:F

    .line 166
    const/4 v0, 0x0

    iput v0, p0, Ldbxyzptlk/db231222/al/a;->x:I

    .line 177
    new-instance v0, Ldbxyzptlk/db231222/al/c;

    invoke-direct {v0}, Ldbxyzptlk/db231222/al/c;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    .line 178
    new-instance v0, Ldbxyzptlk/db231222/al/c;

    invoke-direct {v0}, Ldbxyzptlk/db231222/al/c;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/al/a;->d:Ldbxyzptlk/db231222/al/c;

    .line 179
    iput-boolean p2, p0, Ldbxyzptlk/db231222/al/a;->k:Z

    .line 180
    iput-object p1, p0, Ldbxyzptlk/db231222/al/a;->a:Ldbxyzptlk/db231222/al/b;

    .line 181
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/high16 v3, 0x41f00000    # 30.0f

    const/4 v1, 0x0

    .line 117
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->f()F

    move-result v0

    iput v0, p0, Ldbxyzptlk/db231222/al/a;->e:F

    .line 118
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->g()F

    move-result v0

    iput v0, p0, Ldbxyzptlk/db231222/al/a;->f:F

    .line 119
    const v2, 0x41aa6666    # 21.3f

    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->m:Ldbxyzptlk/db231222/al/d;

    invoke-static {v0}, Ldbxyzptlk/db231222/al/d;->a(Ldbxyzptlk/db231222/al/d;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Ldbxyzptlk/db231222/al/a;->g:F

    .line 120
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->m:Ldbxyzptlk/db231222/al/d;

    invoke-static {v0}, Ldbxyzptlk/db231222/al/d;->b(Ldbxyzptlk/db231222/al/d;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Ldbxyzptlk/db231222/al/a;->h:F

    .line 121
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->m:Ldbxyzptlk/db231222/al/d;

    invoke-static {v0}, Ldbxyzptlk/db231222/al/d;->b(Ldbxyzptlk/db231222/al/d;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Ldbxyzptlk/db231222/al/a;->i:F

    .line 122
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->m:Ldbxyzptlk/db231222/al/d;

    invoke-static {v0}, Ldbxyzptlk/db231222/al/d;->c(Ldbxyzptlk/db231222/al/d;)Z

    move-result v0

    if-nez v0, :cond_3

    :goto_3
    iput v1, p0, Ldbxyzptlk/db231222/al/a;->j:F

    .line 123
    return-void

    .line 119
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->d()F

    move-result v0

    goto :goto_0

    .line 120
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->b()F

    move-result v0

    goto :goto_1

    .line 121
    :cond_2
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->c()F

    move-result v0

    goto :goto_2

    .line 122
    :cond_3
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->e()F

    move-result v1

    goto :goto_3
.end method

.method private a(I[F[F[F[IIZJ)V
    .locals 10

    .prologue
    .line 320
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->d:Ldbxyzptlk/db231222/al/c;

    .line 321
    iget-object v1, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    iput-object v1, p0, Ldbxyzptlk/db231222/al/a;->d:Ldbxyzptlk/db231222/al/c;

    .line 322
    iput-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    .line 324
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-wide/from16 v8, p8

    invoke-static/range {v0 .. v9}, Ldbxyzptlk/db231222/al/c;->a(Ldbxyzptlk/db231222/al/c;I[F[F[F[IIZJ)V

    .line 325
    invoke-direct {p0}, Ldbxyzptlk/db231222/al/a;->d()V

    .line 326
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 332
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->l:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 351
    :goto_0
    return-void

    .line 336
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->a:Ldbxyzptlk/db231222/al/b;

    iget-object v2, p0, Ldbxyzptlk/db231222/al/a;->l:Ljava/lang/Object;

    iget-object v3, p0, Ldbxyzptlk/db231222/al/a;->m:Ldbxyzptlk/db231222/al/d;

    invoke-interface {v0, v2, v3}, Ldbxyzptlk/db231222/al/b;->a(Ljava/lang/Object;Ldbxyzptlk/db231222/al/d;)V

    .line 343
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->m:Ldbxyzptlk/db231222/al/d;

    invoke-static {v0}, Ldbxyzptlk/db231222/al/d;->a(Ldbxyzptlk/db231222/al/d;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    div-float v0, v1, v0

    .line 344
    invoke-direct {p0}, Ldbxyzptlk/db231222/al/a;->a()V

    .line 345
    iget v1, p0, Ldbxyzptlk/db231222/al/a;->e:F

    iget-object v2, p0, Ldbxyzptlk/db231222/al/a;->m:Ldbxyzptlk/db231222/al/d;

    invoke-static {v2}, Ldbxyzptlk/db231222/al/d;->e(Ldbxyzptlk/db231222/al/d;)F

    move-result v2

    sub-float/2addr v1, v2

    mul-float/2addr v1, v0

    iput v1, p0, Ldbxyzptlk/db231222/al/a;->p:F

    .line 346
    iget v1, p0, Ldbxyzptlk/db231222/al/a;->f:F

    iget-object v2, p0, Ldbxyzptlk/db231222/al/a;->m:Ldbxyzptlk/db231222/al/d;

    invoke-static {v2}, Ldbxyzptlk/db231222/al/d;->f(Ldbxyzptlk/db231222/al/d;)F

    move-result v2

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    iput v0, p0, Ldbxyzptlk/db231222/al/a;->q:F

    .line 347
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->m:Ldbxyzptlk/db231222/al/d;

    invoke-static {v0}, Ldbxyzptlk/db231222/al/d;->d(Ldbxyzptlk/db231222/al/d;)F

    move-result v0

    iget v1, p0, Ldbxyzptlk/db231222/al/a;->g:F

    div-float/2addr v0, v1

    iput v0, p0, Ldbxyzptlk/db231222/al/a;->r:F

    .line 348
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->m:Ldbxyzptlk/db231222/al/d;

    invoke-static {v0}, Ldbxyzptlk/db231222/al/d;->g(Ldbxyzptlk/db231222/al/d;)F

    move-result v0

    iget v1, p0, Ldbxyzptlk/db231222/al/a;->h:F

    div-float/2addr v0, v1

    iput v0, p0, Ldbxyzptlk/db231222/al/a;->t:F

    .line 349
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->m:Ldbxyzptlk/db231222/al/d;

    invoke-static {v0}, Ldbxyzptlk/db231222/al/d;->h(Ldbxyzptlk/db231222/al/d;)F

    move-result v0

    iget v1, p0, Ldbxyzptlk/db231222/al/a;->i:F

    div-float/2addr v0, v1

    iput v0, p0, Ldbxyzptlk/db231222/al/a;->u:F

    .line 350
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->m:Ldbxyzptlk/db231222/al/d;

    invoke-static {v0}, Ldbxyzptlk/db231222/al/d;->i(Ldbxyzptlk/db231222/al/d;)F

    move-result v0

    iget v1, p0, Ldbxyzptlk/db231222/al/a;->j:F

    sub-float/2addr v0, v1

    iput v0, p0, Ldbxyzptlk/db231222/al/a;->s:F

    goto :goto_0

    .line 343
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->m:Ldbxyzptlk/db231222/al/d;

    invoke-static {v0}, Ldbxyzptlk/db231222/al/d;->d(Ldbxyzptlk/db231222/al/d;)F

    move-result v0

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->m:Ldbxyzptlk/db231222/al/d;

    invoke-static {v0}, Ldbxyzptlk/db231222/al/d;->d(Ldbxyzptlk/db231222/al/d;)F

    move-result v0

    goto :goto_1
.end method

.method private c()V
    .locals 7

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 356
    iget-object v1, p0, Ldbxyzptlk/db231222/al/a;->l:Ljava/lang/Object;

    if-nez v1, :cond_0

    .line 378
    :goto_0
    return-void

    .line 360
    :cond_0
    iget-object v1, p0, Ldbxyzptlk/db231222/al/a;->m:Ldbxyzptlk/db231222/al/d;

    invoke-static {v1}, Ldbxyzptlk/db231222/al/d;->a(Ldbxyzptlk/db231222/al/d;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 361
    :cond_1
    :goto_1
    invoke-direct {p0}, Ldbxyzptlk/db231222/al/a;->a()V

    .line 363
    iget v1, p0, Ldbxyzptlk/db231222/al/a;->e:F

    iget v2, p0, Ldbxyzptlk/db231222/al/a;->p:F

    mul-float/2addr v2, v0

    sub-float/2addr v1, v2

    .line 364
    iget v2, p0, Ldbxyzptlk/db231222/al/a;->f:F

    iget v3, p0, Ldbxyzptlk/db231222/al/a;->q:F

    mul-float/2addr v0, v3

    sub-float/2addr v2, v0

    .line 365
    iget v0, p0, Ldbxyzptlk/db231222/al/a;->r:F

    iget v3, p0, Ldbxyzptlk/db231222/al/a;->g:F

    mul-float/2addr v0, v3

    .line 368
    iget v3, p0, Ldbxyzptlk/db231222/al/a;->v:F

    iget v4, p0, Ldbxyzptlk/db231222/al/a;->w:F

    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 370
    iget v0, p0, Ldbxyzptlk/db231222/al/a;->t:F

    iget v4, p0, Ldbxyzptlk/db231222/al/a;->h:F

    mul-float/2addr v4, v0

    .line 371
    iget v0, p0, Ldbxyzptlk/db231222/al/a;->u:F

    iget v5, p0, Ldbxyzptlk/db231222/al/a;->i:F

    mul-float/2addr v5, v0

    .line 372
    iget v0, p0, Ldbxyzptlk/db231222/al/a;->s:F

    iget v6, p0, Ldbxyzptlk/db231222/al/a;->j:F

    add-float/2addr v6, v0

    .line 375
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->m:Ldbxyzptlk/db231222/al/d;

    invoke-virtual/range {v0 .. v6}, Ldbxyzptlk/db231222/al/d;->a(FFFFFF)V

    .line 377
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->a:Ldbxyzptlk/db231222/al/b;

    iget-object v1, p0, Ldbxyzptlk/db231222/al/a;->l:Ljava/lang/Object;

    iget-object v2, p0, Ldbxyzptlk/db231222/al/a;->m:Ldbxyzptlk/db231222/al/d;

    iget-object v3, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-interface {v0, v1, v2, v3}, Ldbxyzptlk/db231222/al/b;->a(Ljava/lang/Object;Ldbxyzptlk/db231222/al/d;Ldbxyzptlk/db231222/al/c;)Z

    goto :goto_0

    .line 360
    :cond_2
    iget-object v1, p0, Ldbxyzptlk/db231222/al/a;->m:Ldbxyzptlk/db231222/al/d;

    invoke-static {v1}, Ldbxyzptlk/db231222/al/d;->d(Ldbxyzptlk/db231222/al/d;)F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_1

    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->m:Ldbxyzptlk/db231222/al/d;

    invoke-static {v0}, Ldbxyzptlk/db231222/al/d;->d(Ldbxyzptlk/db231222/al/d;)F

    move-result v0

    goto :goto_1
.end method

.method private d()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/high16 v6, 0x42200000    # 40.0f

    const/high16 v5, 0x41f00000    # 30.0f

    const/high16 v4, 0x3f000000    # 0.5f

    const-wide/16 v2, 0x14

    .line 386
    iget v0, p0, Ldbxyzptlk/db231222/al/a;->x:I

    packed-switch v0, :pswitch_data_0

    .line 473
    :cond_0
    :goto_0
    return-void

    .line 389
    :pswitch_0
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->a:Ldbxyzptlk/db231222/al/b;

    iget-object v1, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-interface {v0, v1}, Ldbxyzptlk/db231222/al/b;->b(Ldbxyzptlk/db231222/al/c;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/al/a;->l:Ljava/lang/Object;

    .line 392
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->l:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 394
    const/4 v0, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/al/a;->x:I

    .line 395
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->a:Ldbxyzptlk/db231222/al/b;

    iget-object v1, p0, Ldbxyzptlk/db231222/al/a;->l:Ljava/lang/Object;

    iget-object v2, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-interface {v0, v1, v2}, Ldbxyzptlk/db231222/al/b;->a(Ljava/lang/Object;Ldbxyzptlk/db231222/al/c;)V

    .line 396
    invoke-direct {p0}, Ldbxyzptlk/db231222/al/a;->b()V

    .line 398
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->i()J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/al/a;->o:J

    iput-wide v0, p0, Ldbxyzptlk/db231222/al/a;->n:J

    goto :goto_0

    .line 405
    :pswitch_1
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 407
    iput v1, p0, Ldbxyzptlk/db231222/al/a;->x:I

    .line 408
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->a:Ldbxyzptlk/db231222/al/b;

    const/4 v1, 0x0

    iput-object v1, p0, Ldbxyzptlk/db231222/al/a;->l:Ljava/lang/Object;

    iget-object v2, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-interface {v0, v1, v2}, Ldbxyzptlk/db231222/al/b;->a(Ljava/lang/Object;Ldbxyzptlk/db231222/al/c;)V

    goto :goto_0

    .line 410
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 412
    const/4 v0, 0x2

    iput v0, p0, Ldbxyzptlk/db231222/al/a;->x:I

    .line 414
    invoke-direct {p0}, Ldbxyzptlk/db231222/al/a;->b()V

    .line 416
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->i()J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/al/a;->n:J

    .line 417
    iget-wide v0, p0, Ldbxyzptlk/db231222/al/a;->n:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Ldbxyzptlk/db231222/al/a;->o:J

    goto :goto_0

    .line 421
    :cond_2
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->i()J

    move-result-wide v0

    iget-wide v2, p0, Ldbxyzptlk/db231222/al/a;->o:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    .line 424
    invoke-direct {p0}, Ldbxyzptlk/db231222/al/a;->b()V

    goto :goto_0

    .line 427
    :cond_3
    invoke-direct {p0}, Ldbxyzptlk/db231222/al/a;->c()V

    goto :goto_0

    .line 434
    :pswitch_2
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->h()Z

    move-result v0

    if-nez v0, :cond_6

    .line 437
    :cond_4
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->h()Z

    move-result v0

    if-nez v0, :cond_5

    .line 439
    iput v1, p0, Ldbxyzptlk/db231222/al/a;->x:I

    .line 440
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->a:Ldbxyzptlk/db231222/al/b;

    const/4 v1, 0x0

    iput-object v1, p0, Ldbxyzptlk/db231222/al/a;->l:Ljava/lang/Object;

    iget-object v2, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-interface {v0, v1, v2}, Ldbxyzptlk/db231222/al/b;->a(Ljava/lang/Object;Ldbxyzptlk/db231222/al/c;)V

    goto/16 :goto_0

    .line 444
    :cond_5
    const/4 v0, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/al/a;->x:I

    .line 446
    invoke-direct {p0}, Ldbxyzptlk/db231222/al/a;->b()V

    .line 448
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->i()J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/al/a;->n:J

    .line 449
    iget-wide v0, p0, Ldbxyzptlk/db231222/al/a;->n:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Ldbxyzptlk/db231222/al/a;->o:J

    goto/16 :goto_0

    .line 454
    :cond_6
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->f()F

    move-result v0

    iget-object v1, p0, Ldbxyzptlk/db231222/al/a;->d:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/al/c;->f()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v5

    if-gtz v0, :cond_7

    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->g()F

    move-result v0

    iget-object v1, p0, Ldbxyzptlk/db231222/al/a;->d:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/al/c;->g()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v5

    if-gtz v0, :cond_7

    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->b()F

    move-result v0

    iget-object v1, p0, Ldbxyzptlk/db231222/al/a;->d:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/al/c;->b()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    mul-float/2addr v0, v4

    cmpl-float v0, v0, v6

    if-gtz v0, :cond_7

    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->c()F

    move-result v0

    iget-object v1, p0, Ldbxyzptlk/db231222/al/a;->d:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/al/c;->c()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    mul-float/2addr v0, v4

    cmpl-float v0, v0, v6

    if-lez v0, :cond_8

    .line 459
    :cond_7
    invoke-direct {p0}, Ldbxyzptlk/db231222/al/a;->b()V

    .line 460
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/al/c;->i()J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/al/a;->n:J

    .line 461
    iget-wide v0, p0, Ldbxyzptlk/db231222/al/a;->n:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Ldbxyzptlk/db231222/al/a;->o:J

    goto/16 :goto_0

    .line 463
    :cond_8
    iget-object v0, p0, Ldbxyzptlk/db231222/al/a;->c:Ldbxyzptlk/db231222/al/c;

    invoke-static {v0}, Ldbxyzptlk/db231222/al/c;->b(Ldbxyzptlk/db231222/al/c;)J

    move-result-wide v0

    iget-wide v2, p0, Ldbxyzptlk/db231222/al/a;->o:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_9

    .line 465
    invoke-direct {p0}, Ldbxyzptlk/db231222/al/a;->b()V

    goto/16 :goto_0

    .line 468
    :cond_9
    invoke-direct {p0}, Ldbxyzptlk/db231222/al/a;->c()V

    goto/16 :goto_0

    .line 386
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(FF)V
    .locals 0

    .prologue
    .line 184
    iput p1, p0, Ldbxyzptlk/db231222/al/a;->w:F

    .line 185
    iput p2, p0, Ldbxyzptlk/db231222/al/a;->v:F

    .line 186
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    .line 256
    :try_start_0
    sget-boolean v0, Ldbxyzptlk/db231222/al/a;->b:Z

    if-eqz v0, :cond_0

    sget-object v0, Ldbxyzptlk/db231222/al/a;->y:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 259
    :goto_0
    iget v0, p0, Ldbxyzptlk/db231222/al/a;->x:I

    if-nez v0, :cond_1

    iget-boolean v0, p0, Ldbxyzptlk/db231222/al/a;->k:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    if-ne v1, v0, :cond_1

    .line 261
    const/4 v0, 0x0

    .line 310
    :goto_1
    return v0

    .line 256
    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    .line 266
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v10

    .line 267
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v0

    div-int v12, v0, v1

    .line 268
    const/4 v0, 0x0

    move v11, v0

    :goto_2
    if-gt v11, v12, :cond_10

    .line 270
    if-ge v11, v12, :cond_4

    const/4 v0, 0x1

    move v8, v0

    .line 271
    :goto_3
    sget-boolean v0, Ldbxyzptlk/db231222/al/a;->b:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    if-ne v1, v0, :cond_8

    .line 277
    :cond_2
    sget-object v2, Ldbxyzptlk/db231222/al/a;->I:[F

    const/4 v3, 0x0

    if-eqz v8, :cond_5

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v0

    :goto_4
    aput v0, v2, v3

    .line 278
    sget-object v2, Ldbxyzptlk/db231222/al/a;->J:[F

    const/4 v3, 0x0

    if-eqz v8, :cond_6

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v0

    :goto_5
    aput v0, v2, v3

    .line 279
    sget-object v2, Ldbxyzptlk/db231222/al/a;->K:[F

    const/4 v3, 0x0

    if-eqz v8, :cond_7

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getHistoricalPressure(I)F

    move-result v0

    :goto_6
    aput v0, v2, v3

    .line 298
    :cond_3
    sget-object v2, Ldbxyzptlk/db231222/al/a;->I:[F

    sget-object v3, Ldbxyzptlk/db231222/al/a;->J:[F

    sget-object v4, Ldbxyzptlk/db231222/al/a;->K:[F

    sget-object v5, Ldbxyzptlk/db231222/al/a;->L:[I

    if-eqz v8, :cond_c

    const/4 v6, 0x2

    :goto_7
    if-eqz v8, :cond_d

    const/4 v7, 0x1

    :goto_8
    if-eqz v8, :cond_f

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v8

    :goto_9
    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Ldbxyzptlk/db231222/al/a;->a(I[F[F[F[IIZJ)V

    .line 268
    add-int/lit8 v0, v11, 0x1

    move v11, v0

    goto :goto_2

    .line 270
    :cond_4
    const/4 v0, 0x0

    move v8, v0

    goto :goto_3

    .line 277
    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    goto :goto_4

    .line 278
    :cond_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    goto :goto_5

    .line 279
    :cond_7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v0

    goto :goto_6

    .line 284
    :cond_8
    const/16 v0, 0xa

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 285
    const/4 v0, 0x0

    move v2, v0

    :goto_a
    if-ge v2, v3, :cond_3

    .line 286
    sget-object v0, Ldbxyzptlk/db231222/al/a;->z:Ljava/lang/reflect/Method;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, p1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 287
    sget-object v0, Ldbxyzptlk/db231222/al/a;->L:[I

    aput v4, v0, v2

    .line 291
    sget-object v5, Ldbxyzptlk/db231222/al/a;->I:[F

    if-eqz v8, :cond_9

    sget-object v0, Ldbxyzptlk/db231222/al/a;->B:Ljava/lang/reflect/Method;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v7

    const/4 v7, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v7

    invoke-virtual {v0, p1, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_b
    check-cast v0, Ljava/lang/Float;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v5, v2

    .line 292
    sget-object v5, Ldbxyzptlk/db231222/al/a;->J:[F

    if-eqz v8, :cond_a

    sget-object v0, Ldbxyzptlk/db231222/al/a;->C:Ljava/lang/reflect/Method;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v7

    const/4 v7, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v7

    invoke-virtual {v0, p1, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_c
    check-cast v0, Ljava/lang/Float;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v5, v2

    .line 293
    sget-object v5, Ldbxyzptlk/db231222/al/a;->K:[F

    if-eqz v8, :cond_b

    sget-object v0, Ldbxyzptlk/db231222/al/a;->D:Ljava/lang/reflect/Method;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v7

    const/4 v4, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-virtual {v0, p1, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_d
    check-cast v0, Ljava/lang/Float;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v5, v2

    .line 285
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_a

    .line 291
    :cond_9
    sget-object v0, Ldbxyzptlk/db231222/al/a;->E:Ljava/lang/reflect/Method;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v7

    invoke-virtual {v0, p1, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_b

    .line 292
    :cond_a
    sget-object v0, Ldbxyzptlk/db231222/al/a;->F:Ljava/lang/reflect/Method;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v7

    invoke-virtual {v0, p1, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_c

    .line 293
    :cond_b
    sget-object v0, Ldbxyzptlk/db231222/al/a;->A:Ljava/lang/reflect/Method;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v7

    invoke-virtual {v0, p1, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_d

    :cond_c
    move v6, v10

    .line 298
    goto/16 :goto_7

    :cond_d
    const/4 v0, 0x1

    if-eq v10, v0, :cond_e

    const/4 v0, 0x1

    sget v7, Ldbxyzptlk/db231222/al/a;->H:I

    shl-int/2addr v0, v7

    add-int/lit8 v0, v0, -0x1

    and-int/2addr v0, v10

    sget v7, Ldbxyzptlk/db231222/al/a;->G:I

    if-eq v0, v7, :cond_e

    const/4 v0, 0x3

    if-eq v10, v0, :cond_e

    const/4 v7, 0x1

    goto/16 :goto_8

    :cond_e
    const/4 v7, 0x0

    goto/16 :goto_8

    :cond_f
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v8

    goto/16 :goto_9

    .line 306
    :cond_10
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 307
    :catch_0
    move-exception v0

    .line 309
    const-string v1, "MultiTouchController"

    const-string v2, "onTouchEvent() failed"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 310
    const/4 v0, 0x0

    goto/16 :goto_1
.end method
