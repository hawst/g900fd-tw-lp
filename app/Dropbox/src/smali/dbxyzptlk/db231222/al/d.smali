.class public final Ldbxyzptlk/db231222/al/d;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:F

.field private b:F

.field private c:F

.field private d:F

.field private e:F

.field private f:F

.field private g:Z

.field private h:Z

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 717
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Ldbxyzptlk/db231222/al/d;)Z
    .locals 1

    .prologue
    .line 717
    iget-boolean v0, p0, Ldbxyzptlk/db231222/al/d;->g:Z

    return v0
.end method

.method static synthetic b(Ldbxyzptlk/db231222/al/d;)Z
    .locals 1

    .prologue
    .line 717
    iget-boolean v0, p0, Ldbxyzptlk/db231222/al/d;->h:Z

    return v0
.end method

.method static synthetic c(Ldbxyzptlk/db231222/al/d;)Z
    .locals 1

    .prologue
    .line 717
    iget-boolean v0, p0, Ldbxyzptlk/db231222/al/d;->i:Z

    return v0
.end method

.method static synthetic d(Ldbxyzptlk/db231222/al/d;)F
    .locals 1

    .prologue
    .line 717
    iget v0, p0, Ldbxyzptlk/db231222/al/d;->c:F

    return v0
.end method

.method static synthetic e(Ldbxyzptlk/db231222/al/d;)F
    .locals 1

    .prologue
    .line 717
    iget v0, p0, Ldbxyzptlk/db231222/al/d;->a:F

    return v0
.end method

.method static synthetic f(Ldbxyzptlk/db231222/al/d;)F
    .locals 1

    .prologue
    .line 717
    iget v0, p0, Ldbxyzptlk/db231222/al/d;->b:F

    return v0
.end method

.method static synthetic g(Ldbxyzptlk/db231222/al/d;)F
    .locals 1

    .prologue
    .line 717
    iget v0, p0, Ldbxyzptlk/db231222/al/d;->d:F

    return v0
.end method

.method static synthetic h(Ldbxyzptlk/db231222/al/d;)F
    .locals 1

    .prologue
    .line 717
    iget v0, p0, Ldbxyzptlk/db231222/al/d;->e:F

    return v0
.end method

.method static synthetic i(Ldbxyzptlk/db231222/al/d;)F
    .locals 1

    .prologue
    .line 717
    iget v0, p0, Ldbxyzptlk/db231222/al/d;->f:F

    return v0
.end method


# virtual methods
.method public final a()F
    .locals 1

    .prologue
    .line 752
    iget v0, p0, Ldbxyzptlk/db231222/al/d;->a:F

    return v0
.end method

.method protected final a(FFFFFF)V
    .locals 3

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 743
    iput p1, p0, Ldbxyzptlk/db231222/al/d;->a:F

    .line 744
    iput p2, p0, Ldbxyzptlk/db231222/al/d;->b:F

    .line 745
    cmpl-float v1, p3, v2

    if-nez v1, :cond_0

    move p3, v0

    :cond_0
    iput p3, p0, Ldbxyzptlk/db231222/al/d;->c:F

    .line 746
    cmpl-float v1, p4, v2

    if-nez v1, :cond_1

    move p4, v0

    :cond_1
    iput p4, p0, Ldbxyzptlk/db231222/al/d;->d:F

    .line 747
    cmpl-float v1, p5, v2

    if-nez v1, :cond_2

    :goto_0
    iput v0, p0, Ldbxyzptlk/db231222/al/d;->e:F

    .line 748
    iput p6, p0, Ldbxyzptlk/db231222/al/d;->f:F

    .line 749
    return-void

    :cond_2
    move v0, p5

    .line 747
    goto :goto_0
.end method

.method public final a(FFZFZFFZF)V
    .locals 3

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 730
    iput p1, p0, Ldbxyzptlk/db231222/al/d;->a:F

    .line 731
    iput p2, p0, Ldbxyzptlk/db231222/al/d;->b:F

    .line 732
    iput-boolean p3, p0, Ldbxyzptlk/db231222/al/d;->g:Z

    .line 733
    cmpl-float v1, p4, v2

    if-nez v1, :cond_0

    move p4, v0

    :cond_0
    iput p4, p0, Ldbxyzptlk/db231222/al/d;->c:F

    .line 734
    iput-boolean p5, p0, Ldbxyzptlk/db231222/al/d;->h:Z

    .line 735
    cmpl-float v1, p6, v2

    if-nez v1, :cond_1

    move p6, v0

    :cond_1
    iput p6, p0, Ldbxyzptlk/db231222/al/d;->d:F

    .line 736
    cmpl-float v1, p7, v2

    if-nez v1, :cond_2

    :goto_0
    iput v0, p0, Ldbxyzptlk/db231222/al/d;->e:F

    .line 737
    iput-boolean p8, p0, Ldbxyzptlk/db231222/al/d;->i:Z

    .line 738
    iput p9, p0, Ldbxyzptlk/db231222/al/d;->f:F

    .line 739
    return-void

    :cond_2
    move v0, p7

    .line 736
    goto :goto_0
.end method

.method public final b()F
    .locals 1

    .prologue
    .line 756
    iget v0, p0, Ldbxyzptlk/db231222/al/d;->b:F

    return v0
.end method

.method public final c()F
    .locals 1

    .prologue
    .line 760
    iget-boolean v0, p0, Ldbxyzptlk/db231222/al/d;->g:Z

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Ldbxyzptlk/db231222/al/d;->c:F

    goto :goto_0
.end method
