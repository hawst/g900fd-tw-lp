.class public Ldbxyzptlk/db231222/i/c;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Ldbxyzptlk/db231222/i/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/i/c;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/db231222/i/c;->c:Ljava/lang/String;

    .line 67
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/db231222/i/c;->d:Ljava/lang/String;

    .line 71
    invoke-static {p1}, Lcom/dropbox/android/util/U;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/i/c;->e:Ljava/lang/String;

    .line 72
    iget-object v0, p0, Ldbxyzptlk/db231222/i/c;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    const-string v0, "unknown"

    iput-object v0, p0, Ldbxyzptlk/db231222/i/c;->e:Ljava/lang/String;

    .line 77
    :cond_0
    const-string v1, "unknown"

    .line 78
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 79
    if-eqz v0, :cond_1

    .line 80
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v0

    .line 81
    if-eqz v0, :cond_1

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 85
    :goto_0
    iput-object v0, p0, Ldbxyzptlk/db231222/i/c;->f:Ljava/lang/String;

    .line 87
    const-string v2, "unknown"

    .line 88
    const-string v0, "unknown"

    .line 90
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 91
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 93
    iget-object v1, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :try_start_1
    iget-object v0, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 99
    :goto_1
    iput-object v1, p0, Ldbxyzptlk/db231222/i/c;->a:Ljava/lang/String;

    .line 100
    iput-object v0, p0, Ldbxyzptlk/db231222/i/c;->b:Ljava/lang/String;

    .line 101
    return-void

    .line 96
    :catch_0
    move-exception v1

    move-object v5, v1

    move-object v1, v2

    move-object v2, v5

    .line 97
    :goto_2
    sget-object v3, Ldbxyzptlk/db231222/i/c;->g:Ljava/lang/String;

    const-string v4, "can\'t get package info"

    invoke-static {v3, v4, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 96
    :catch_1
    move-exception v2

    goto :goto_2

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/n/k;)V
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p1}, Ldbxyzptlk/db231222/n/k;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/i/c;->e:Ljava/lang/String;

    .line 105
    return-void
.end method
