.class public Ldbxyzptlk/db231222/i/d;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Ldbxyzptlk/db231222/i/d;


# instance fields
.field private final c:Ldbxyzptlk/db231222/i/c;

.field private final d:Landroid/content/Context;

.field private final e:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ldbxyzptlk/db231222/r/e;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const-class v0, Ldbxyzptlk/db231222/i/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/i/d;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/i/d;->e:Ljava/util/concurrent/atomic/AtomicReference;

    .line 90
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/i/d;->d:Landroid/content/Context;

    .line 93
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/i/a;->a(Ljava/io/File;)V

    .line 94
    new-instance v0, Ldbxyzptlk/db231222/i/c;

    iget-object v1, p0, Ldbxyzptlk/db231222/i/d;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/i/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/i/d;->c:Ldbxyzptlk/db231222/i/c;

    .line 97
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/i/d;->f:Ljava/lang/String;

    .line 99
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    .line 101
    new-instance v1, Ldbxyzptlk/db231222/i/b;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/i/b;-><init>(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    invoke-static {v1}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 103
    return-void
.end method

.method public static a(Landroid/content/Context;)Ldbxyzptlk/db231222/i/d;
    .locals 1

    .prologue
    .line 303
    sget-object v0, Ldbxyzptlk/db231222/i/d;->b:Ldbxyzptlk/db231222/i/d;

    if-nez v0, :cond_0

    .line 304
    new-instance v0, Ldbxyzptlk/db231222/i/d;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/i/d;-><init>(Landroid/content/Context;)V

    sput-object v0, Ldbxyzptlk/db231222/i/d;->b:Ldbxyzptlk/db231222/i/d;

    .line 308
    sget-object v0, Ldbxyzptlk/db231222/i/d;->b:Ldbxyzptlk/db231222/i/d;

    return-object v0

    .line 306
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/i/d;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Ldbxyzptlk/db231222/i/d;->f()V

    return-void
.end method

.method private a(Ljava/io/File;)V
    .locals 14

    .prologue
    const/4 v3, 0x0

    .line 371
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 372
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 373
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 377
    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    aget-object v2, v2, v4

    .line 378
    sget-object v4, Ldbxyzptlk/db231222/i/d;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Stacktrace in file \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\' belongs to version "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    const-string v1, ".stacktrace.xx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    .line 384
    if-eqz v1, :cond_4

    .line 385
    new-instance v0, Ljava/io/InputStreamReader;

    invoke-static {p1}, Lcom/dropbox/android/util/v;->b(Ljava/io/File;)Ljava/io/InputStream;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 390
    :goto_0
    new-instance v8, Ljava/io/BufferedReader;

    const/16 v4, 0x2000

    invoke-direct {v8, v0, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V

    .line 393
    const-string v0, "0"

    .line 399
    invoke-virtual {v8}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_c

    .line 402
    :goto_1
    invoke-virtual {v8}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_b

    .line 405
    :goto_2
    invoke-virtual {v8}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_a

    .line 408
    :goto_3
    const-string v0, "0"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    invoke-direct {p0}, Ldbxyzptlk/db231222/i/d;->d()Ljava/lang/String;

    move-result-object v4

    .line 411
    :cond_0
    invoke-virtual {v8}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_9

    .line 414
    :goto_4
    invoke-virtual {v8}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 417
    :goto_5
    invoke-virtual {v8}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 418
    const-string v10, "Type: "

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 419
    const-string v3, "Type: "

    const-string v10, ""

    invoke-virtual {v9, v3, v10}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 421
    :try_start_1
    invoke-static {v3}, Ldbxyzptlk/db231222/z/J;->valueOf(Ljava/lang/String;)Ldbxyzptlk/db231222/z/J;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    .line 427
    :cond_1
    :goto_6
    :try_start_2
    invoke-virtual {v8}, Ljava/io/BufferedReader;->close()V

    .line 430
    if-nez v0, :cond_5

    .line 431
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    .line 437
    :goto_7
    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    .line 439
    if-eqz v1, :cond_6

    .line 440
    invoke-static {p1}, Lcom/dropbox/android/util/v;->b(Ljava/io/File;)Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v10

    .line 445
    :goto_8
    :try_start_3
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    .line 446
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/a;->h()Ldbxyzptlk/db231222/z/F;

    move-result-object v0

    const-string v1, "android"

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v11

    const/4 v13, 0x0

    invoke-virtual/range {v0 .. v13}, Ldbxyzptlk/db231222/z/F;->a(Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/db231222/z/J;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/io/InputStream;JZ)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 449
    :try_start_4
    invoke-static {v10}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    .line 453
    :cond_2
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_3

    .line 454
    sget-object v0, Ldbxyzptlk/db231222/i/d;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error deleting log file: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    :cond_3
    :goto_9
    return-void

    .line 387
    :cond_4
    new-instance v0, Ljava/io/FileReader;

    invoke-direct {v0, p1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0

    .line 459
    :catch_0
    move-exception v0

    .line 460
    sget-object v1, Ldbxyzptlk/db231222/i/d;->a:Ljava/lang/String;

    const-string v2, "Error uploading log: "

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_9

    .line 422
    :catch_1
    move-exception v3

    .line 423
    :try_start_5
    sget-object v3, Ldbxyzptlk/db231222/z/J;->a:Ldbxyzptlk/db231222/z/J;

    goto :goto_6

    .line 433
    :cond_5
    invoke-static {v0}, Ldbxyzptlk/db231222/v/v;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    goto :goto_7

    .line 442
    :cond_6
    new-instance v10, Ljava/io/FileInputStream;

    invoke-direct {v10, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    goto :goto_8

    .line 449
    :catchall_0
    move-exception v0

    invoke-static {v10}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    throw v0

    .line 457
    :cond_7
    invoke-virtual {p1}, Ljava/io/File;->delete()Z
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_9

    :cond_8
    move-object v0, v3

    goto/16 :goto_5

    :cond_9
    move-object v5, v3

    goto/16 :goto_4

    :cond_a
    move-object v4, v0

    goto/16 :goto_3

    :cond_b
    move-object v6, v3

    goto/16 :goto_2

    :cond_c
    move-object v7, v3

    goto/16 :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/Throwable;Ldbxyzptlk/db231222/z/J;)V
    .locals 6

    .prologue
    .line 192
    :try_start_0
    sget-object v0, Ldbxyzptlk/db231222/i/d;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Outputting exception to log: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Ldbxyzptlk/db231222/z/J;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 194
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->R()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "level"

    invoke-virtual {p3}, Ldbxyzptlk/db231222/z/J;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "class"

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "msg"

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "debug.msg"

    invoke-virtual {v0, v1, p1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 201
    iget-object v0, p0, Ldbxyzptlk/db231222/i/d;->d:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 202
    sget-object v0, Ldbxyzptlk/db231222/i/d;->a:Ljava/lang/String;

    const-string v1, "Got an error, but no Context."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    :goto_0
    return-void

    .line 208
    :cond_0
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 209
    if-eqz p2, :cond_1

    .line 210
    new-instance v0, Ljava/io/PrintWriter;

    invoke-direct {v0, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {p2, v0}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 213
    :cond_1
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    const v2, 0x1869f

    invoke-virtual {v0, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 215
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Ldbxyzptlk/db231222/i/d;->c:Ldbxyzptlk/db231222/i/c;

    iget-object v3, v3, Ldbxyzptlk/db231222/i/c;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 217
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Ldbxyzptlk/db231222/i/d;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ".stacktrace.xx"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 219
    sget-object v0, Ldbxyzptlk/db231222/i/d;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Writing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3}, Ldbxyzptlk/db231222/z/J;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " exception to: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 222
    invoke-static {v3}, Lcom/dropbox/android/util/v;->a(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v0

    .line 223
    new-instance v2, Ljava/io/BufferedWriter;

    new-instance v4, Ljava/io/OutputStreamWriter;

    invoke-direct {v4, v0}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    const/16 v0, 0x2000

    invoke-direct {v2, v4, v0}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V

    .line 225
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Ldbxyzptlk/db231222/i/d;->c:Ldbxyzptlk/db231222/i/c;

    iget-object v4, v4, Ldbxyzptlk/db231222/i/c;->d:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 226
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Ldbxyzptlk/db231222/i/d;->c:Ldbxyzptlk/db231222/i/c;

    iget-object v4, v4, Ldbxyzptlk/db231222/i/c;->c:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 228
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Ldbxyzptlk/db231222/i/d;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 230
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Ldbxyzptlk/db231222/i/d;->c:Ldbxyzptlk/db231222/i/c;

    iget-object v4, v4, Ldbxyzptlk/db231222/i/c;->e:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 231
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-static {v4}, Lcom/dropbox/android/util/ad;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 232
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Type: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ldbxyzptlk/db231222/z/J;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 235
    if-eqz p1, :cond_2

    .line 236
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Debug message: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 239
    :cond_2
    const-string v0, "----------------------\n"

    invoke-virtual {v2, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 242
    const/16 v0, 0x3e8

    invoke-static {v0}, Ldbxyzptlk/db231222/i/a;->a(I)Ljava/util/List;

    move-result-object v0

    .line 243
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 244
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 278
    :catch_0
    move-exception v0

    .line 280
    sget-object v1, Ldbxyzptlk/db231222/i/d;->a:Ljava/lang/String;

    const-string v2, "failed to output exception to file"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 247
    :cond_3
    :try_start_1
    const-string v0, "----------------------\n"

    invoke-virtual {v2, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 249
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 250
    invoke-virtual {v2, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 251
    const/16 v0, 0xa

    invoke-virtual {v2, v0}, Ljava/io/BufferedWriter;->write(I)V

    goto :goto_2

    .line 254
    :cond_4
    const-string v0, "----------------------\n"

    invoke-virtual {v2, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 256
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 258
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->flush()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 261
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V
    :try_end_2
    .catch Ljava/io/SyncFailedException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 265
    :goto_3
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V

    .line 266
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 268
    sget-object v0, Ldbxyzptlk/db231222/i/d;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 262
    :catch_1
    move-exception v0

    goto :goto_3
.end method

.method public static b()Ldbxyzptlk/db231222/i/d;
    .locals 1

    .prologue
    .line 312
    sget-object v0, Ldbxyzptlk/db231222/i/d;->b:Ldbxyzptlk/db231222/i/d;

    if-nez v0, :cond_0

    .line 313
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 315
    :cond_0
    sget-object v0, Ldbxyzptlk/db231222/i/d;->b:Ldbxyzptlk/db231222/i/d;

    return-object v0
.end method

.method private d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Ldbxyzptlk/db231222/i/d;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/r/e;

    .line 176
    if-eqz v0, :cond_0

    .line 177
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v0

    .line 178
    if-eqz v0, :cond_1

    .line 179
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/k;->f()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->e()Ljava/lang/String;

    move-result-object v0

    .line 184
    :goto_0
    return-object v0

    .line 182
    :cond_0
    sget-object v0, Ldbxyzptlk/db231222/i/d;->a:Ljava/lang/String;

    const-string v1, "Warning: no user info available."

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :cond_1
    const-string v0, "0"

    goto :goto_0
.end method

.method private e()[Ljava/io/File;
    .locals 3

    .prologue
    .line 323
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Ldbxyzptlk/db231222/i/d;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 326
    invoke-static {v0}, Ldbxyzptlk/db231222/k/a;->c(Ljava/io/File;)Z

    .line 329
    new-instance v1, Ldbxyzptlk/db231222/i/e;

    invoke-direct {v1, p0}, Ldbxyzptlk/db231222/i/e;-><init>(Ldbxyzptlk/db231222/i/d;)V

    .line 335
    invoke-virtual {v0, v1}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 360
    invoke-direct {p0}, Ldbxyzptlk/db231222/i/d;->e()[Ljava/io/File;

    move-result-object v1

    .line 361
    if-eqz v1, :cond_0

    array-length v0, v1

    if-lez v0, :cond_0

    .line 362
    sget-object v0, Ldbxyzptlk/db231222/i/d;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " stacktrace(s)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 364
    invoke-direct {p0, v3}, Ldbxyzptlk/db231222/i/d;->a(Ljava/io/File;)V

    .line 363
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 367
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ldbxyzptlk/db231222/i/c;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Ldbxyzptlk/db231222/i/d;->c:Ldbxyzptlk/db231222/i/c;

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/n/k;)V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Ldbxyzptlk/db231222/i/d;->c:Ldbxyzptlk/db231222/i/c;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/i/c;->a(Ldbxyzptlk/db231222/n/k;)V

    .line 156
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/r/e;)V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Ldbxyzptlk/db231222/i/d;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 167
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 129
    sget-object v0, Ldbxyzptlk/db231222/z/J;->a:Ldbxyzptlk/db231222/z/J;

    invoke-direct {p0, p1, p2, v0}, Ldbxyzptlk/db231222/i/d;->a(Ljava/lang/String;Ljava/lang/Throwable;Ldbxyzptlk/db231222/z/J;)V

    .line 130
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Ldbxyzptlk/db231222/i/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 126
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 145
    sget-object v0, Ldbxyzptlk/db231222/z/J;->b:Ldbxyzptlk/db231222/z/J;

    invoke-direct {p0, p1, p2, v0}, Ldbxyzptlk/db231222/i/d;->a(Ljava/lang/String;Ljava/lang/Throwable;Ldbxyzptlk/db231222/z/J;)V

    .line 146
    return-void
.end method

.method public final b(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 137
    const/4 v0, 0x0

    sget-object v1, Ldbxyzptlk/db231222/z/J;->b:Ldbxyzptlk/db231222/z/J;

    invoke-direct {p0, v0, p1, v1}, Ldbxyzptlk/db231222/i/d;->a(Ljava/lang/String;Ljava/lang/Throwable;Ldbxyzptlk/db231222/z/J;)V

    .line 138
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 343
    new-instance v0, Ldbxyzptlk/db231222/i/f;

    const-string v1, "stackTraces"

    invoke-direct {v0, p0, v1}, Ldbxyzptlk/db231222/i/f;-><init>(Ldbxyzptlk/db231222/i/d;Ljava/lang/String;)V

    .line 351
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 352
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 353
    return-void
.end method

.method public final c(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 141
    const/4 v0, 0x0

    sget-object v1, Ldbxyzptlk/db231222/z/J;->b:Ldbxyzptlk/db231222/z/J;

    invoke-direct {p0, v0, p1, v1}, Ldbxyzptlk/db231222/i/d;->a(Ljava/lang/String;Ljava/lang/Throwable;Ldbxyzptlk/db231222/z/J;)V

    .line 142
    return-void
.end method
