.class public final Ldbxyzptlk/db231222/k/a;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:[Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SdCardPath"
        }
    .end annotation
.end field

.field private static final c:[Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SdCardPath"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 38
    const-class v0, Ldbxyzptlk/db231222/k/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/k/a;->a:Ljava/lang/String;

    .line 217
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "/sdcard/external_sd"

    aput-object v1, v0, v3

    const-string v1, "/disk"

    aput-object v1, v0, v4

    const-string v1, "/sdcard"

    aput-object v1, v0, v5

    const-string v1, "/sdcard/sd"

    aput-object v1, v0, v6

    const-string v1, "/emmc"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "/media"

    aput-object v2, v0, v1

    sput-object v0, Ldbxyzptlk/db231222/k/a;->b:[Ljava/lang/String;

    .line 262
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "/mnt/extSdCard"

    aput-object v1, v0, v3

    const-string v1, "/mnt/external_sd"

    aput-object v1, v0, v4

    const-string v1, "/mnt/sdcard-ext"

    aput-object v1, v0, v5

    const-string v1, "/mnt/emmc"

    aput-object v1, v0, v6

    const-string v1, "/sdcard/external_sd"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "/sdcard/sd"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "/mnt/media"

    aput-object v2, v0, v1

    sput-object v0, Ldbxyzptlk/db231222/k/a;->c:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/io/File;)J
    .locals 7

    .prologue
    const-wide/16 v0, 0x0

    .line 86
    invoke-virtual {p0}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 87
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 95
    :cond_0
    return-wide v0

    .line 88
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 90
    invoke-static {p0}, Ldbxyzptlk/db231222/k/a;->b(Ljava/io/File;)[Ljava/io/File;

    move-result-object v5

    array-length v6, v5

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v6, :cond_0

    aget-object v3, v5, v2

    .line 91
    invoke-static {v3}, Ldbxyzptlk/db231222/k/a;->a(Ljava/io/File;)J

    move-result-wide v3

    add-long/2addr v3, v0

    .line 90
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-wide v0, v3

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Ljava/io/File;
    .locals 3

    .prologue
    .line 311
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "anl"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/io/File;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/Set",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    sget-object v0, Lcom/dropbox/android/util/DropboxPath;->a:Lcom/dropbox/android/util/DropboxPath;

    invoke-static {p0, p1, v0}, Ldbxyzptlk/db231222/k/a;->a(Ljava/io/File;Ljava/util/Set;Lcom/dropbox/android/util/DropboxPath;)V

    .line 45
    return-void
.end method

.method private static a(Ljava/io/File;Ljava/util/Set;Lcom/dropbox/android/util/DropboxPath;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/Set",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;",
            "Lcom/dropbox/android/util/DropboxPath;",
            ")V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-interface {p1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 49
    invoke-virtual {p2}, Lcom/dropbox/android/util/DropboxPath;->j()Ljava/lang/String;

    move-result-object v0

    .line 50
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 52
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 53
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    .line 54
    if-eqz v2, :cond_0

    .line 55
    invoke-static {v1}, Ldbxyzptlk/db231222/k/a;->b(Ljava/io/File;)[Ljava/io/File;

    move-result-object v3

    .line 56
    array-length v4, v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 57
    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    .line 58
    invoke-static {v6}, Ldbxyzptlk/db231222/k/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 59
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v5

    invoke-virtual {p2, v6, v5}, Lcom/dropbox/android/util/DropboxPath;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/DropboxPath;

    move-result-object v5

    invoke-static {p0, p1, v5}, Ldbxyzptlk/db231222/k/a;->a(Ljava/io/File;Ljava/util/Set;Lcom/dropbox/android/util/DropboxPath;)V

    .line 56
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 68
    :cond_0
    if-eqz v2, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 69
    :goto_1
    if-eqz v2, :cond_1

    if-eqz v0, :cond_2

    array-length v0, v0

    if-nez v0, :cond_2

    .line 70
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v0

    .line 71
    if-nez v0, :cond_2

    .line 72
    sget-object v0, Ldbxyzptlk/db231222/k/a;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to delete file "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v1}, Lcom/dropbox/android/util/ab;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    :cond_2
    return-void

    .line 68
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a()Z
    .locals 2

    .prologue
    .line 195
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 196
    const-string v1, "mounted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 198
    if-nez v0, :cond_0

    .line 202
    invoke-static {}, Ldbxyzptlk/db231222/k/a;->d()Ljava/io/File;

    move-result-object v1

    .line 203
    if-eqz v1, :cond_0

    invoke-static {v1}, Ldbxyzptlk/db231222/k/a;->e(Ljava/io/File;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 204
    const/4 v0, 0x1

    .line 208
    :cond_0
    return v0
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 212
    invoke-static {}, Ldbxyzptlk/db231222/k/a;->e()Ljava/io/File;

    move-result-object v0

    .line 213
    if-eqz v0, :cond_0

    invoke-static {v0}, Ldbxyzptlk/db231222/k/a;->e(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/io/File;)[Ljava/io/File;
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 108
    if-nez v0, :cond_0

    .line 109
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/io/File;

    .line 111
    :cond_0
    return-object v0
.end method

.method public static c()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SdCardPath"
        }
    .end annotation

    .prologue
    .line 240
    invoke-static {}, Ldbxyzptlk/db231222/k/a;->h()Ljava/util/ArrayList;

    move-result-object v0

    .line 241
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 242
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 244
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "/sdcard"

    goto :goto_0
.end method

.method public static declared-synchronized c(Ljava/io/File;)Z
    .locals 2

    .prologue
    .line 187
    const-class v1, Ldbxyzptlk/db231222/k/a;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 188
    invoke-virtual {p0}, Ljava/io/File;->mkdirs()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 190
    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 187
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static d()Ljava/io/File;
    .locals 2

    .prologue
    .line 254
    invoke-static {}, Lcom/dropbox/android/util/bn;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255
    new-instance v0, Ljava/io/File;

    const-string v1, "/mnt/media"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 257
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(Ljava/io/File;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 340
    .line 343
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 344
    const-string v2, "MD5"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v3

    .line 345
    new-instance v2, Ljava/security/DigestInputStream;

    invoke-direct {v2, v1, v3}, Ljava/security/DigestInputStream;-><init>(Ljava/io/InputStream;Ljava/security/MessageDigest;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346
    const/16 v1, 0x1000

    :try_start_1
    new-array v1, v1, [B

    .line 347
    :cond_0
    invoke-virtual {v2, v1}, Ljava/security/DigestInputStream;->read([B)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v4

    const/4 v5, -0x1

    if-gt v4, v5, :cond_0

    .line 356
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    .line 359
    new-instance v0, Ljava/math/BigInteger;

    invoke-virtual {v3}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/math/BigInteger;-><init>([B)V

    .line 360
    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 349
    :catch_0
    move-exception v1

    move-object v2, v0

    .line 350
    :goto_1
    :try_start_2
    sget-object v3, Ldbxyzptlk/db231222/k/a;->a:Ljava/lang/String;

    const-string v4, "md5"

    invoke-static {v3, v4, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 356
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto :goto_0

    .line 352
    :catch_1
    move-exception v1

    move-object v2, v0

    .line 353
    :goto_2
    :try_start_3
    sget-object v3, Ldbxyzptlk/db231222/k/a;->a:Ljava/lang/String;

    const-string v4, "md5"

    invoke-static {v3, v4, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 356
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_3
    invoke-static {v2}, Ldbxyzptlk/db231222/X/f;->a(Ljava/io/InputStream;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    .line 352
    :catch_2
    move-exception v1

    goto :goto_2

    .line 349
    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method public static e()Ljava/io/File;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 281
    invoke-static {}, Ldbxyzptlk/db231222/k/a;->d()Ljava/io/File;

    move-result-object v0

    .line 283
    if-nez v0, :cond_1

    move-object v0, v1

    .line 292
    :goto_0
    sget-object v4, Ldbxyzptlk/db231222/k/a;->c:[Ljava/lang/String;

    array-length v5, v4

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_0

    aget-object v6, v4, v3

    .line 293
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 294
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 295
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_2

    move-object v1, v2

    .line 301
    :cond_0
    return-object v1

    .line 287
    :cond_1
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 288
    :catch_0
    move-exception v2

    .line 289
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 292
    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1
.end method

.method private static e(Ljava/io/File;)Z
    .locals 2

    .prologue
    .line 326
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 327
    new-instance v0, Ljava/io/File;

    const-string v1, "funkyfun8675309.foo"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 329
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 330
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 331
    const/4 v0, 0x1

    .line 336
    :goto_0
    return v0

    .line 333
    :catch_0
    move-exception v0

    .line 336
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f()Ljava/io/File;
    .locals 3

    .prologue
    .line 306
    new-instance v0, Ljava/io/File;

    invoke-static {}, Ldbxyzptlk/db231222/k/a;->d()Ljava/io/File;

    move-result-object v1

    const-string v2, "dropbox"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Ldbxyzptlk/db231222/k/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method private static h()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 228
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 229
    sget-object v2, Ldbxyzptlk/db231222/k/a;->b:[Ljava/lang/String;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 230
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 231
    invoke-static {v5}, Ldbxyzptlk/db231222/k/a;->e(Ljava/io/File;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 232
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 229
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 235
    :cond_1
    return-object v1
.end method
