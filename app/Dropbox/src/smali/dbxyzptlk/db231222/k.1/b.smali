.class public final Ldbxyzptlk/db231222/k/b;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    iput-object p1, p0, Ldbxyzptlk/db231222/k/b;->a:Ljava/io/File;

    .line 129
    return-void
.end method


# virtual methods
.method public final a(J)I
    .locals 6

    .prologue
    .line 168
    const/4 v0, 0x0

    .line 169
    new-instance v1, Ldbxyzptlk/db231222/Y/b;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, p1

    invoke-direct {v1, v2, v3}, Ldbxyzptlk/db231222/Y/b;-><init>(J)V

    .line 170
    sget-object v2, Ldbxyzptlk/db231222/Y/k;->b:Ldbxyzptlk/db231222/Y/g;

    .line 171
    iget-object v3, p0, Ldbxyzptlk/db231222/k/b;->a:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 172
    iget-object v3, p0, Ldbxyzptlk/db231222/k/b;->a:Ljava/io/File;

    invoke-static {v3, v1, v2}, Ldbxyzptlk/db231222/X/b;->a(Ljava/io/File;Ldbxyzptlk/db231222/Y/g;Ldbxyzptlk/db231222/Y/g;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 173
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 174
    invoke-static {}, Ldbxyzptlk/db231222/k/a;->g()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Deleted temp file: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 177
    goto :goto_0

    :cond_0
    move v1, v0

    .line 179
    :cond_1
    return v1

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Ljava/io/File;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 138
    iget-object v3, p0, Ldbxyzptlk/db231222/k/b;->a:Ljava/io/File;

    .line 139
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v3}, Ldbxyzptlk/db231222/k/a;->c(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 161
    :cond_0
    :goto_0
    return-object v0

    .line 143
    :cond_1
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 144
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    move-result v2

    if-nez v2, :cond_0

    .line 149
    invoke-static {p1}, Lcom/dropbox/android/util/ab;->q(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v4

    .line 150
    const/4 v0, 0x1

    move v2, v0

    :goto_1
    const/16 v0, 0x64

    if-ge v2, v0, :cond_2

    .line 151
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "-"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v0, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 152
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 153
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-nez v5, :cond_0

    .line 150
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 157
    :catch_0
    move-exception v0

    .line 158
    invoke-static {}, Ldbxyzptlk/db231222/k/a;->g()Ljava/lang/String;

    move-result-object v2

    const-string v3, "newTempChooserFile"

    invoke-static {v2, v3, v0}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 159
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v2

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/i/d;->c(Ljava/lang/Throwable;)V

    :cond_2
    move-object v0, v1

    .line 161
    goto :goto_0
.end method
