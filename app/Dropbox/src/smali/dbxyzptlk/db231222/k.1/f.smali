.class public Ldbxyzptlk/db231222/k/f;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Ldbxyzptlk/db231222/k/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/k/f;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/k/h;Lcom/dropbox/android/util/DropboxPath;)V
    .locals 3

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-virtual {p1}, Ldbxyzptlk/db231222/k/h;->j()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/k/f;->c:Ljava/io/File;

    .line 30
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Ldbxyzptlk/db231222/k/f;->c:Ljava/io/File;

    invoke-virtual {p2}, Lcom/dropbox/android/util/DropboxPath;->j()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/k/f;->b:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/k/h;Ljava/io/File;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-virtual {p1}, Ldbxyzptlk/db231222/k/h;->j()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/k/f;->c:Ljava/io/File;

    .line 35
    iget-object v0, p0, Ldbxyzptlk/db231222/k/f;->c:Ljava/io/File;

    invoke-static {v0, p2}, Ldbxyzptlk/db231222/k/f;->b(Ljava/io/File;Ljava/io/File;)V

    .line 36
    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/k/f;->b:Ljava/lang/String;

    .line 37
    return-void
.end method

.method private a(Z)Lcom/dropbox/android/util/DropboxPath;
    .locals 3

    .prologue
    .line 201
    iget-object v0, p0, Ldbxyzptlk/db231222/k/f;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 203
    iget-object v1, p0, Ldbxyzptlk/db231222/k/f;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 204
    invoke-static {v0}, Ldbxyzptlk/db231222/k/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 206
    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v1, v0, p1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;Z)V

    return-object v1
.end method

.method static a(Ljava/io/File;Ljava/io/File;)Ldbxyzptlk/db231222/k/g;
    .locals 2

    .prologue
    .line 100
    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    const/4 v0, 0x0

    .line 103
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ldbxyzptlk/db231222/k/g;

    invoke-direct {v0, p0, p1}, Ldbxyzptlk/db231222/k/g;-><init>(Ljava/io/File;Ljava/io/File;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 210
    const/16 v0, 0x2236

    const/16 v1, 0x3a

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 211
    const/16 v1, 0x2217

    const/16 v2, 0x2a

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 212
    const/16 v1, 0x2758

    const/16 v2, 0x7c

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 213
    const/16 v1, 0x22d6

    const/16 v2, 0x3c

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 214
    const/16 v1, 0x22d7

    const/16 v2, 0x3e

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 215
    const/16 v1, 0x2033

    const/16 v2, 0x22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 216
    const/16 v1, 0x203d

    const/16 v2, 0x3f

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 217
    const/16 v1, 0x66a

    const/16 v2, 0x25

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 218
    return-object v0
.end method

.method private static b(Ljava/io/File;Ljava/io/File;)V
    .locals 2

    .prologue
    .line 107
    invoke-static {p1, p0}, Ldbxyzptlk/db231222/k/f;->a(Ljava/io/File;Ljava/io/File;)Ldbxyzptlk/db231222/k/g;

    move-result-object v0

    .line 108
    if-eqz v0, :cond_0

    .line 109
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->ao()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/dropbox/android/util/analytics/l;->a(Lcom/dropbox/android/util/analytics/m;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 111
    :cond_0
    return-void
.end method

.method private g()Z
    .locals 5

    .prologue
    .line 163
    invoke-virtual {p0}, Ldbxyzptlk/db231222/k/f;->a()Ljava/io/File;

    move-result-object v0

    .line 165
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    .line 167
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 168
    invoke-static {v1}, Ldbxyzptlk/db231222/k/a;->c(Ljava/io/File;)Z

    move-result v2

    .line 169
    if-nez v2, :cond_0

    .line 170
    sget-object v2, Ldbxyzptlk/db231222/k/f;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t create directory: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " for file "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const/4 v0, 0x0

    .line 174
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/io/File;
    .locals 2

    .prologue
    .line 133
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Ldbxyzptlk/db231222/k/f;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Ldbxyzptlk/db231222/k/f;->c:Ljava/io/File;

    invoke-virtual {p0}, Ldbxyzptlk/db231222/k/f;->a()Ljava/io/File;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ab;->a(Ljava/io/File;Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/io/File;
    .locals 2

    .prologue
    .line 153
    invoke-virtual {p0}, Ldbxyzptlk/db231222/k/f;->a()Ljava/io/File;

    move-result-object v0

    .line 155
    invoke-direct {p0}, Ldbxyzptlk/db231222/k/f;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 158
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 178
    iget-object v0, p0, Ldbxyzptlk/db231222/k/f;->b:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 179
    array-length v1, v0

    if-lez v1, :cond_0

    .line 180
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    .line 184
    :goto_0
    return-object v0

    .line 183
    :cond_0
    sget-object v0, Ldbxyzptlk/db231222/k/f;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Couldn\'t extract filename for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/db231222/k/f;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    iget-object v0, p0, Ldbxyzptlk/db231222/k/f;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public final e()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 189
    invoke-virtual {p0}, Ldbxyzptlk/db231222/k/f;->a()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 125
    instance-of v0, p1, Ldbxyzptlk/db231222/k/f;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Ldbxyzptlk/db231222/k/f;->b:Ljava/lang/String;

    check-cast p1, Ldbxyzptlk/db231222/k/f;

    iget-object v1, p1, Ldbxyzptlk/db231222/k/f;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 128
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/dropbox/android/util/DropboxPath;
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/k/f;->a(Z)Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Ldbxyzptlk/db231222/k/f;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Ldbxyzptlk/db231222/k/f;->b:Ljava/lang/String;

    return-object v0
.end method
