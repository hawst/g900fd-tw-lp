.class public final Ldbxyzptlk/db231222/k/h;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ldbxyzptlk/db231222/k/h;


# instance fields
.field private final b:Ljava/io/File;

.field private final c:Ljava/io/File;

.field private final d:Ljava/io/File;

.field private final e:Ljava/io/File;

.field private final f:Ljava/io/File;

.field private final g:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    new-instance v0, Ldbxyzptlk/db231222/k/h;

    invoke-static {}, Ldbxyzptlk/db231222/k/a;->d()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/k/h;-><init>(Ljava/io/File;)V

    sput-object v0, Ldbxyzptlk/db231222/k/h;->a:Ldbxyzptlk/db231222/k/h;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 4

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/Android/data/com.dropbox.android"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 46
    new-instance v1, Ljava/io/File;

    const-string v2, "cache"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p0, Ldbxyzptlk/db231222/k/h;->b:Ljava/io/File;

    .line 47
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Ldbxyzptlk/db231222/k/h;->b:Ljava/io/File;

    const-string v3, "thumbs"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p0, Ldbxyzptlk/db231222/k/h;->c:Ljava/io/File;

    .line 48
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Ldbxyzptlk/db231222/k/h;->b:Ljava/io/File;

    const-string v3, "tmp"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p0, Ldbxyzptlk/db231222/k/h;->d:Ljava/io/File;

    .line 49
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Ldbxyzptlk/db231222/k/h;->b:Ljava/io/File;

    const-string v3, "updates"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p0, Ldbxyzptlk/db231222/k/h;->f:Ljava/io/File;

    .line 54
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/io/File;

    const-string v3, "files"

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-string v0, "scratch"

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p0, Ldbxyzptlk/db231222/k/h;->e:Ljava/io/File;

    .line 55
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Ldbxyzptlk/db231222/k/h;->b:Ljava/io/File;

    const-string v2, "miscthumbs"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/k/h;->g:Ljava/io/File;

    .line 56
    return-void
.end method

.method public static a()Ldbxyzptlk/db231222/k/h;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 29
    sget-object v0, Ldbxyzptlk/db231222/k/h;->a:Ldbxyzptlk/db231222/k/h;

    return-object v0
.end method

.method private declared-synchronized a(Ljava/lang/String;JZ)Ljava/io/File;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 89
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Ldbxyzptlk/db231222/k/h;->d:Ljava/io/File;

    .line 90
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v3}, Ldbxyzptlk/db231222/k/a;->c(Ljava/io/File;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 124
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 94
    :cond_1
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    const/16 v1, 0xfa0

    if-gt v2, v1, :cond_0

    .line 95
    :try_start_1
    new-instance v1, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".tmp"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 96
    invoke-virtual {v1}, Ljava/io/File;->exists()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-nez v4, :cond_3

    .line 98
    if-eqz p4, :cond_2

    .line 99
    :try_start_2
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    move-result v4

    if-eqz v4, :cond_4

    move-object v0, v1

    .line 100
    goto :goto_0

    .line 105
    :cond_2
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v4

    if-eqz v4, :cond_4

    move-object v0, v1

    .line 106
    goto :goto_0

    .line 116
    :cond_3
    :try_start_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 117
    const-wide/16 v6, -0x1

    cmp-long v6, p2, v6

    if-lez v6, :cond_4

    cmp-long v4, v4, p2

    if-lez v4, :cond_4

    .line 119
    invoke-static {v1}, Ldbxyzptlk/db231222/X/b;->c(Ljava/io/File;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 94
    :cond_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 111
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 198
    invoke-virtual {p0}, Ldbxyzptlk/db231222/k/h;->j()Ljava/io/File;

    move-result-object v0

    .line 201
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 205
    :goto_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 202
    :catch_0
    move-exception v1

    .line 203
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/Set;)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/dropbox/android/util/DropboxPath;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 171
    invoke-virtual {p0}, Ldbxyzptlk/db231222/k/h;->j()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/k/a;->a(Ljava/io/File;)J

    move-result-wide v0

    .line 174
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-wide v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    .line 175
    invoke-virtual {v0, p0}, Lcom/dropbox/android/util/DropboxPath;->a(Ldbxyzptlk/db231222/k/h;)Ldbxyzptlk/db231222/k/f;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/k/f;->a()Ljava/io/File;

    move-result-object v0

    .line 176
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 177
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    sub-long v0, v1, v4

    :goto_1
    move-wide v1, v0

    .line 179
    goto :goto_0

    .line 180
    :cond_0
    return-wide v1

    :cond_1
    move-wide v0, v1

    goto :goto_1
.end method

.method public final declared-synchronized a(Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 129
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Ldbxyzptlk/db231222/k/h;->f:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 130
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Ldbxyzptlk/db231222/k/a;->c(Ljava/io/File;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 131
    const/4 v0, 0x0

    .line 133
    :cond_0
    monitor-exit p0

    return-object v0

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;)Z
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p1, p0}, Lcom/dropbox/android/util/DropboxPath;->a(Ldbxyzptlk/db231222/k/h;)Ldbxyzptlk/db231222/k/f;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/k/f;->a()Ljava/io/File;

    move-result-object v0

    .line 163
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/io/File;)Z
    .locals 1

    .prologue
    .line 191
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/k/h;->b(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 193
    :goto_0
    return v0

    .line 192
    :catch_0
    move-exception v0

    .line 193
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/k/h;->b(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 59
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Ldbxyzptlk/db231222/k/h;->b:Ljava/io/File;

    const-string v2, ".nomedia"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 60
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 62
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Ldbxyzptlk/db231222/k/a;->c(Ljava/io/File;)Z

    .line 63
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 64
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final c()Ldbxyzptlk/db231222/k/b;
    .locals 3

    .prologue
    .line 72
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Ldbxyzptlk/db231222/k/h;->d:Ljava/io/File;

    const-string v2, "ch"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 73
    new-instance v1, Ldbxyzptlk/db231222/k/b;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/k/b;-><init>(Ljava/io/File;)V

    return-object v1
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Ldbxyzptlk/db231222/k/h;->d:Ljava/io/File;

    invoke-static {v0}, Ldbxyzptlk/db231222/X/b;->c(Ljava/io/File;)Z

    move-result v0

    return v0
.end method

.method public final e()Ljava/io/File;
    .locals 4

    .prologue
    .line 81
    const-string v0, "file"

    const-wide/32 v1, 0x5265c00

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Ldbxyzptlk/db231222/k/h;->a(Ljava/lang/String;JZ)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/io/File;
    .locals 4

    .prologue
    .line 85
    const-string v0, "upload"

    const-wide v1, 0x9a7ec800L

    const/4 v3, 0x1

    invoke-direct {p0, v0, v1, v2, v3}, Ldbxyzptlk/db231222/k/h;->a(Ljava/lang/String;JZ)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Ldbxyzptlk/db231222/k/h;->f:Ljava/io/File;

    invoke-static {v0}, Ldbxyzptlk/db231222/X/b;->c(Ljava/io/File;)Z

    .line 138
    return-void
.end method

.method public final h()Ljava/io/File;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Ldbxyzptlk/db231222/k/h;->g:Ljava/io/File;

    return-object v0
.end method

.method public final i()Ljava/io/File;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Ldbxyzptlk/db231222/k/h;->c:Ljava/io/File;

    return-object v0
.end method

.method public final j()Ljava/io/File;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Ldbxyzptlk/db231222/k/h;->e:Ljava/io/File;

    return-object v0
.end method
