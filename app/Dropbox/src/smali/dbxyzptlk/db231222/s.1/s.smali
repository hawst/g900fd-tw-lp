.class public final Ldbxyzptlk/db231222/s/s;
.super Ldbxyzptlk/db231222/H/k;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/s/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/H/k",
        "<",
        "Ldbxyzptlk/db231222/s/q;",
        "Ldbxyzptlk/db231222/s/s;",
        ">;",
        "Ldbxyzptlk/db231222/s/t;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ldbxyzptlk/db231222/s/k;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 372
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/k;-><init>()V

    .line 479
    const-string v0, ""

    iput-object v0, p0, Ldbxyzptlk/db231222/s/s;->b:Ljava/lang/Object;

    .line 577
    const-string v0, ""

    iput-object v0, p0, Ldbxyzptlk/db231222/s/s;->c:Ljava/lang/Object;

    .line 675
    sget-object v0, Ldbxyzptlk/db231222/s/k;->a:Ldbxyzptlk/db231222/s/k;

    iput-object v0, p0, Ldbxyzptlk/db231222/s/s;->d:Ldbxyzptlk/db231222/s/k;

    .line 373
    invoke-direct {p0}, Ldbxyzptlk/db231222/s/s;->k()V

    .line 374
    return-void
.end method

.method static synthetic g()Ldbxyzptlk/db231222/s/s;
    .locals 1

    .prologue
    .line 367
    invoke-static {}, Ldbxyzptlk/db231222/s/s;->l()Ldbxyzptlk/db231222/s/s;

    move-result-object v0

    return-object v0
.end method

.method private k()V
    .locals 0

    .prologue
    .line 377
    return-void
.end method

.method private static l()Ldbxyzptlk/db231222/s/s;
    .locals 1

    .prologue
    .line 379
    new-instance v0, Ldbxyzptlk/db231222/s/s;

    invoke-direct {v0}, Ldbxyzptlk/db231222/s/s;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Ldbxyzptlk/db231222/s/s;
    .locals 2

    .prologue
    .line 394
    invoke-static {}, Ldbxyzptlk/db231222/s/s;->l()Ldbxyzptlk/db231222/s/s;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/s;->c()Ldbxyzptlk/db231222/s/q;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/s/s;->a(Ldbxyzptlk/db231222/s/q;)Ldbxyzptlk/db231222/s/s;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/s/s;
    .locals 4

    .prologue
    .line 463
    const/4 v2, 0x0

    .line 465
    :try_start_0
    sget-object v0, Ldbxyzptlk/db231222/s/q;->a:Ldbxyzptlk/db231222/H/w;

    invoke-interface {v0, p1, p2}, Ldbxyzptlk/db231222/H/w;->b(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/s/q;
    :try_end_0
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 470
    if-eqz v0, :cond_0

    .line 471
    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/s/s;->a(Ldbxyzptlk/db231222/s/q;)Ldbxyzptlk/db231222/s/s;

    .line 474
    :cond_0
    return-object p0

    .line 466
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 467
    :try_start_1
    invoke-virtual {v1}, Ldbxyzptlk/db231222/H/o;->a()Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/s/q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 468
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 470
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 471
    invoke-virtual {p0, v1}, Ldbxyzptlk/db231222/s/s;->a(Ldbxyzptlk/db231222/s/q;)Ldbxyzptlk/db231222/s/s;

    :cond_1
    throw v0

    .line 470
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public final a(Ldbxyzptlk/db231222/s/k;)Ldbxyzptlk/db231222/s/s;
    .locals 1

    .prologue
    .line 704
    if-nez p1, :cond_0

    .line 705
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 707
    :cond_0
    iget v0, p0, Ldbxyzptlk/db231222/s/s;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ldbxyzptlk/db231222/s/s;->a:I

    .line 708
    iput-object p1, p0, Ldbxyzptlk/db231222/s/s;->d:Ldbxyzptlk/db231222/s/k;

    .line 710
    return-object p0
.end method

.method public final a(Ldbxyzptlk/db231222/s/q;)Ldbxyzptlk/db231222/s/s;
    .locals 1

    .prologue
    .line 430
    invoke-static {}, Ldbxyzptlk/db231222/s/q;->a()Ldbxyzptlk/db231222/s/q;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 444
    :cond_0
    :goto_0
    return-object p0

    .line 431
    :cond_1
    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/q;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 432
    iget v0, p0, Ldbxyzptlk/db231222/s/s;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/s/s;->a:I

    .line 433
    invoke-static {p1}, Ldbxyzptlk/db231222/s/q;->b(Ldbxyzptlk/db231222/s/q;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/s/s;->b:Ljava/lang/Object;

    .line 436
    :cond_2
    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/q;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 437
    iget v0, p0, Ldbxyzptlk/db231222/s/s;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ldbxyzptlk/db231222/s/s;->a:I

    .line 438
    invoke-static {p1}, Ldbxyzptlk/db231222/s/q;->c(Ldbxyzptlk/db231222/s/q;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/s/s;->c:Ljava/lang/Object;

    .line 441
    :cond_3
    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/q;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 442
    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/q;->l()Ldbxyzptlk/db231222/s/k;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/s/s;->a(Ldbxyzptlk/db231222/s/k;)Ldbxyzptlk/db231222/s/s;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Ldbxyzptlk/db231222/s/s;
    .locals 1

    .prologue
    .line 537
    if-nez p1, :cond_0

    .line 538
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 540
    :cond_0
    iget v0, p0, Ldbxyzptlk/db231222/s/s;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/s/s;->a:I

    .line 541
    iput-object p1, p0, Ldbxyzptlk/db231222/s/s;->b:Ljava/lang/Object;

    .line 543
    return-object p0
.end method

.method public final synthetic b(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/H/b;
    .locals 1

    .prologue
    .line 367
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/s/s;->a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/s/s;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ldbxyzptlk/db231222/s/q;
    .locals 2

    .prologue
    .line 402
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/s;->c()Ldbxyzptlk/db231222/s/q;

    move-result-object v0

    .line 403
    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/q;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 404
    invoke-static {v0}, Ldbxyzptlk/db231222/s/s;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/C;

    move-result-object v0

    throw v0

    .line 406
    :cond_0
    return-object v0
.end method

.method public final b(Ljava/lang/String;)Ldbxyzptlk/db231222/s/s;
    .locals 1

    .prologue
    .line 635
    if-nez p1, :cond_0

    .line 636
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 638
    :cond_0
    iget v0, p0, Ldbxyzptlk/db231222/s/s;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ldbxyzptlk/db231222/s/s;->a:I

    .line 639
    iput-object p1, p0, Ldbxyzptlk/db231222/s/s;->c:Ljava/lang/Object;

    .line 641
    return-object p0
.end method

.method public final c()Ldbxyzptlk/db231222/s/q;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 410
    new-instance v2, Ldbxyzptlk/db231222/s/q;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Ldbxyzptlk/db231222/s/q;-><init>(Ldbxyzptlk/db231222/H/k;Ldbxyzptlk/db231222/s/r;)V

    .line 411
    iget v3, p0, Ldbxyzptlk/db231222/s/s;->a:I

    .line 412
    const/4 v1, 0x0

    .line 413
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 416
    :goto_0
    iget-object v1, p0, Ldbxyzptlk/db231222/s/s;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Ldbxyzptlk/db231222/s/q;->a(Ldbxyzptlk/db231222/s/q;Ljava/lang/Object;)Ljava/lang/Object;

    .line 417
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 418
    or-int/lit8 v0, v0, 0x2

    .line 420
    :cond_0
    iget-object v1, p0, Ldbxyzptlk/db231222/s/s;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Ldbxyzptlk/db231222/s/q;->b(Ldbxyzptlk/db231222/s/q;Ljava/lang/Object;)Ljava/lang/Object;

    .line 421
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 422
    or-int/lit8 v0, v0, 0x4

    .line 424
    :cond_1
    iget-object v1, p0, Ldbxyzptlk/db231222/s/s;->d:Ldbxyzptlk/db231222/s/k;

    invoke-static {v2, v1}, Ldbxyzptlk/db231222/s/q;->a(Ldbxyzptlk/db231222/s/q;Ldbxyzptlk/db231222/s/k;)Ldbxyzptlk/db231222/s/k;

    .line 425
    invoke-static {v2, v0}, Ldbxyzptlk/db231222/s/q;->a(Ldbxyzptlk/db231222/s/q;I)I

    .line 426
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 367
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/s;->a()Ldbxyzptlk/db231222/s/s;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 488
    iget v1, p0, Ldbxyzptlk/db231222/s/s;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 448
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/s;->d()Z

    move-result v1

    if-nez v1, :cond_1

    .line 456
    :cond_0
    :goto_0
    return v0

    .line 452
    :cond_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/s;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 456
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 586
    iget v0, p0, Ldbxyzptlk/db231222/s/s;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic h()Ldbxyzptlk/db231222/H/k;
    .locals 1

    .prologue
    .line 367
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/s;->a()Ldbxyzptlk/db231222/s/s;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()Ldbxyzptlk/db231222/H/b;
    .locals 1

    .prologue
    .line 367
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/s;->a()Ldbxyzptlk/db231222/s/s;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()Ldbxyzptlk/db231222/H/t;
    .locals 1

    .prologue
    .line 367
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/s;->c()Ldbxyzptlk/db231222/s/q;

    move-result-object v0

    return-object v0
.end method
