.class public final Ldbxyzptlk/db231222/s/o;
.super Ldbxyzptlk/db231222/H/k;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/s/p;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/H/k",
        "<",
        "Ldbxyzptlk/db231222/s/m;",
        "Ldbxyzptlk/db231222/s/o;",
        ">;",
        "Ldbxyzptlk/db231222/s/p;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ldbxyzptlk/db231222/s/q;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 242
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/k;-><init>()V

    .line 325
    invoke-static {}, Ldbxyzptlk/db231222/s/q;->a()Ldbxyzptlk/db231222/s/q;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/s/o;->b:Ldbxyzptlk/db231222/s/q;

    .line 243
    invoke-direct {p0}, Ldbxyzptlk/db231222/s/o;->k()V

    .line 244
    return-void
.end method

.method static synthetic g()Ldbxyzptlk/db231222/s/o;
    .locals 1

    .prologue
    .line 237
    invoke-static {}, Ldbxyzptlk/db231222/s/o;->l()Ldbxyzptlk/db231222/s/o;

    move-result-object v0

    return-object v0
.end method

.method private k()V
    .locals 0

    .prologue
    .line 247
    return-void
.end method

.method private static l()Ldbxyzptlk/db231222/s/o;
    .locals 1

    .prologue
    .line 249
    new-instance v0, Ldbxyzptlk/db231222/s/o;

    invoke-direct {v0}, Ldbxyzptlk/db231222/s/o;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Ldbxyzptlk/db231222/s/o;
    .locals 2

    .prologue
    .line 260
    invoke-static {}, Ldbxyzptlk/db231222/s/o;->l()Ldbxyzptlk/db231222/s/o;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/o;->c()Ldbxyzptlk/db231222/s/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/s/o;->a(Ldbxyzptlk/db231222/s/m;)Ldbxyzptlk/db231222/s/o;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/s/o;
    .locals 4

    .prologue
    .line 309
    const/4 v2, 0x0

    .line 311
    :try_start_0
    sget-object v0, Ldbxyzptlk/db231222/s/m;->a:Ldbxyzptlk/db231222/H/w;

    invoke-interface {v0, p1, p2}, Ldbxyzptlk/db231222/H/w;->b(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/s/m;
    :try_end_0
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 316
    if-eqz v0, :cond_0

    .line 317
    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/s/o;->a(Ldbxyzptlk/db231222/s/m;)Ldbxyzptlk/db231222/s/o;

    .line 320
    :cond_0
    return-object p0

    .line 312
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 313
    :try_start_1
    invoke-virtual {v1}, Ldbxyzptlk/db231222/H/o;->a()Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/s/m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 314
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 316
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 317
    invoke-virtual {p0, v1}, Ldbxyzptlk/db231222/s/o;->a(Ldbxyzptlk/db231222/s/m;)Ldbxyzptlk/db231222/s/o;

    :cond_1
    throw v0

    .line 316
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public final a(Ldbxyzptlk/db231222/s/m;)Ldbxyzptlk/db231222/s/o;
    .locals 1

    .prologue
    .line 288
    invoke-static {}, Ldbxyzptlk/db231222/s/m;->a()Ldbxyzptlk/db231222/s/m;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 292
    :cond_0
    :goto_0
    return-object p0

    .line 289
    :cond_1
    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/m;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/m;->d()Ldbxyzptlk/db231222/s/q;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/s/o;->a(Ldbxyzptlk/db231222/s/q;)Ldbxyzptlk/db231222/s/o;

    goto :goto_0
.end method

.method public final a(Ldbxyzptlk/db231222/s/q;)Ldbxyzptlk/db231222/s/o;
    .locals 2

    .prologue
    .line 384
    iget v0, p0, Ldbxyzptlk/db231222/s/o;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/s/o;->b:Ldbxyzptlk/db231222/s/q;

    invoke-static {}, Ldbxyzptlk/db231222/s/q;->a()Ldbxyzptlk/db231222/s/q;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 386
    iget-object v0, p0, Ldbxyzptlk/db231222/s/o;->b:Ldbxyzptlk/db231222/s/q;

    invoke-static {v0}, Ldbxyzptlk/db231222/s/q;->a(Ldbxyzptlk/db231222/s/q;)Ldbxyzptlk/db231222/s/s;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/s/s;->a(Ldbxyzptlk/db231222/s/q;)Ldbxyzptlk/db231222/s/s;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/s;->c()Ldbxyzptlk/db231222/s/q;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/s/o;->b:Ldbxyzptlk/db231222/s/q;

    .line 392
    :goto_0
    iget v0, p0, Ldbxyzptlk/db231222/s/o;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/s/o;->a:I

    .line 393
    return-object p0

    .line 389
    :cond_0
    iput-object p1, p0, Ldbxyzptlk/db231222/s/o;->b:Ldbxyzptlk/db231222/s/q;

    goto :goto_0
.end method

.method public final synthetic b(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/H/b;
    .locals 1

    .prologue
    .line 237
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/s/o;->a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/s/o;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ldbxyzptlk/db231222/s/m;
    .locals 2

    .prologue
    .line 268
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/o;->c()Ldbxyzptlk/db231222/s/m;

    move-result-object v0

    .line 269
    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/m;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 270
    invoke-static {v0}, Ldbxyzptlk/db231222/s/o;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/C;

    move-result-object v0

    throw v0

    .line 272
    :cond_0
    return-object v0
.end method

.method public final c()Ldbxyzptlk/db231222/s/m;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 276
    new-instance v2, Ldbxyzptlk/db231222/s/m;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Ldbxyzptlk/db231222/s/m;-><init>(Ldbxyzptlk/db231222/H/k;Ldbxyzptlk/db231222/s/n;)V

    .line 277
    iget v3, p0, Ldbxyzptlk/db231222/s/o;->a:I

    .line 278
    const/4 v1, 0x0

    .line 279
    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    .line 282
    :goto_0
    iget-object v1, p0, Ldbxyzptlk/db231222/s/o;->b:Ldbxyzptlk/db231222/s/q;

    invoke-static {v2, v1}, Ldbxyzptlk/db231222/s/m;->a(Ldbxyzptlk/db231222/s/m;Ldbxyzptlk/db231222/s/q;)Ldbxyzptlk/db231222/s/q;

    .line 283
    invoke-static {v2, v0}, Ldbxyzptlk/db231222/s/m;->a(Ldbxyzptlk/db231222/s/m;I)I

    .line 284
    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 237
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/o;->a()Ldbxyzptlk/db231222/s/o;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 334
    iget v1, p0, Ldbxyzptlk/db231222/s/o;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 296
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/o;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/o;->f()Ldbxyzptlk/db231222/s/q;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/q;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 299
    const/4 v0, 0x0

    .line 302
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final f()Ldbxyzptlk/db231222/s/q;
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Ldbxyzptlk/db231222/s/o;->b:Ldbxyzptlk/db231222/s/q;

    return-object v0
.end method

.method public final synthetic h()Ldbxyzptlk/db231222/H/k;
    .locals 1

    .prologue
    .line 237
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/o;->a()Ldbxyzptlk/db231222/s/o;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()Ldbxyzptlk/db231222/H/b;
    .locals 1

    .prologue
    .line 237
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/o;->a()Ldbxyzptlk/db231222/s/o;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()Ldbxyzptlk/db231222/H/t;
    .locals 1

    .prologue
    .line 237
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/o;->c()Ldbxyzptlk/db231222/s/m;

    move-result-object v0

    return-object v0
.end method
