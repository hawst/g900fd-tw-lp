.class public final Ldbxyzptlk/db231222/s/f;
.super Ldbxyzptlk/db231222/H/k;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/s/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/H/k",
        "<",
        "Ldbxyzptlk/db231222/s/d;",
        "Ldbxyzptlk/db231222/s/f;",
        ">;",
        "Ldbxyzptlk/db231222/s/g;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:J

.field private d:J


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 630
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/k;-><init>()V

    .line 631
    invoke-direct {p0}, Ldbxyzptlk/db231222/s/f;->l()V

    .line 632
    return-void
.end method

.method static synthetic k()Ldbxyzptlk/db231222/s/f;
    .locals 1

    .prologue
    .line 625
    invoke-static {}, Ldbxyzptlk/db231222/s/f;->m()Ldbxyzptlk/db231222/s/f;

    move-result-object v0

    return-object v0
.end method

.method private l()V
    .locals 0

    .prologue
    .line 635
    return-void
.end method

.method private static m()Ldbxyzptlk/db231222/s/f;
    .locals 1

    .prologue
    .line 637
    new-instance v0, Ldbxyzptlk/db231222/s/f;

    invoke-direct {v0}, Ldbxyzptlk/db231222/s/f;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Ldbxyzptlk/db231222/s/f;
    .locals 2

    .prologue
    .line 652
    invoke-static {}, Ldbxyzptlk/db231222/s/f;->m()Ldbxyzptlk/db231222/s/f;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/f;->c()Ldbxyzptlk/db231222/s/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/s/f;->a(Ldbxyzptlk/db231222/s/d;)Ldbxyzptlk/db231222/s/f;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)Ldbxyzptlk/db231222/s/f;
    .locals 1

    .prologue
    .line 766
    iget v0, p0, Ldbxyzptlk/db231222/s/f;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/s/f;->a:I

    .line 767
    iput-wide p1, p0, Ldbxyzptlk/db231222/s/f;->b:J

    .line 769
    return-object p0
.end method

.method public final a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/s/f;
    .locals 4

    .prologue
    .line 721
    const/4 v2, 0x0

    .line 723
    :try_start_0
    sget-object v0, Ldbxyzptlk/db231222/s/d;->a:Ldbxyzptlk/db231222/H/w;

    invoke-interface {v0, p1, p2}, Ldbxyzptlk/db231222/H/w;->b(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/s/d;
    :try_end_0
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 728
    if-eqz v0, :cond_0

    .line 729
    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/s/f;->a(Ldbxyzptlk/db231222/s/d;)Ldbxyzptlk/db231222/s/f;

    .line 732
    :cond_0
    return-object p0

    .line 724
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 725
    :try_start_1
    invoke-virtual {v1}, Ldbxyzptlk/db231222/H/o;->a()Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/s/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 726
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 728
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 729
    invoke-virtual {p0, v1}, Ldbxyzptlk/db231222/s/f;->a(Ldbxyzptlk/db231222/s/d;)Ldbxyzptlk/db231222/s/f;

    :cond_1
    throw v0

    .line 728
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public final a(Ldbxyzptlk/db231222/s/d;)Ldbxyzptlk/db231222/s/f;
    .locals 2

    .prologue
    .line 688
    invoke-static {}, Ldbxyzptlk/db231222/s/d;->a()Ldbxyzptlk/db231222/s/d;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 698
    :cond_0
    :goto_0
    return-object p0

    .line 689
    :cond_1
    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/d;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 690
    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/d;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ldbxyzptlk/db231222/s/f;->a(J)Ldbxyzptlk/db231222/s/f;

    .line 692
    :cond_2
    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/d;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 693
    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/d;->h()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ldbxyzptlk/db231222/s/f;->b(J)Ldbxyzptlk/db231222/s/f;

    .line 695
    :cond_3
    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/d;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 696
    invoke-virtual {p1}, Ldbxyzptlk/db231222/s/d;->j()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ldbxyzptlk/db231222/s/f;->c(J)Ldbxyzptlk/db231222/s/f;

    goto :goto_0
.end method

.method public final synthetic b(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/H/b;
    .locals 1

    .prologue
    .line 625
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/s/f;->a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/s/f;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ldbxyzptlk/db231222/s/d;
    .locals 2

    .prologue
    .line 660
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/f;->c()Ldbxyzptlk/db231222/s/d;

    move-result-object v0

    .line 661
    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/d;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 662
    invoke-static {v0}, Ldbxyzptlk/db231222/s/f;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/C;

    move-result-object v0

    throw v0

    .line 664
    :cond_0
    return-object v0
.end method

.method public final b(J)Ldbxyzptlk/db231222/s/f;
    .locals 1

    .prologue
    .line 815
    iget v0, p0, Ldbxyzptlk/db231222/s/f;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ldbxyzptlk/db231222/s/f;->a:I

    .line 816
    iput-wide p1, p0, Ldbxyzptlk/db231222/s/f;->c:J

    .line 818
    return-object p0
.end method

.method public final c()Ldbxyzptlk/db231222/s/d;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 668
    new-instance v2, Ldbxyzptlk/db231222/s/d;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Ldbxyzptlk/db231222/s/d;-><init>(Ldbxyzptlk/db231222/H/k;Ldbxyzptlk/db231222/s/b;)V

    .line 669
    iget v3, p0, Ldbxyzptlk/db231222/s/f;->a:I

    .line 670
    const/4 v1, 0x0

    .line 671
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 674
    :goto_0
    iget-wide v4, p0, Ldbxyzptlk/db231222/s/f;->b:J

    invoke-static {v2, v4, v5}, Ldbxyzptlk/db231222/s/d;->a(Ldbxyzptlk/db231222/s/d;J)J

    .line 675
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 676
    or-int/lit8 v0, v0, 0x2

    .line 678
    :cond_0
    iget-wide v4, p0, Ldbxyzptlk/db231222/s/f;->c:J

    invoke-static {v2, v4, v5}, Ldbxyzptlk/db231222/s/d;->b(Ldbxyzptlk/db231222/s/d;J)J

    .line 679
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 680
    or-int/lit8 v0, v0, 0x4

    .line 682
    :cond_1
    iget-wide v3, p0, Ldbxyzptlk/db231222/s/f;->d:J

    invoke-static {v2, v3, v4}, Ldbxyzptlk/db231222/s/d;->c(Ldbxyzptlk/db231222/s/d;J)J

    .line 683
    invoke-static {v2, v0}, Ldbxyzptlk/db231222/s/d;->a(Ldbxyzptlk/db231222/s/d;I)I

    .line 684
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final c(J)Ldbxyzptlk/db231222/s/f;
    .locals 1

    .prologue
    .line 864
    iget v0, p0, Ldbxyzptlk/db231222/s/f;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ldbxyzptlk/db231222/s/f;->a:I

    .line 865
    iput-wide p1, p0, Ldbxyzptlk/db231222/s/f;->d:J

    .line 867
    return-object p0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 625
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/f;->a()Ldbxyzptlk/db231222/s/f;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 746
    iget v1, p0, Ldbxyzptlk/db231222/s/f;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 702
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/f;->d()Z

    move-result v1

    if-nez v1, :cond_1

    .line 714
    :cond_0
    :goto_0
    return v0

    .line 706
    :cond_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/f;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 710
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/f;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 714
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 795
    iget v0, p0, Ldbxyzptlk/db231222/s/f;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 844
    iget v0, p0, Ldbxyzptlk/db231222/s/f;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic h()Ldbxyzptlk/db231222/H/k;
    .locals 1

    .prologue
    .line 625
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/f;->a()Ldbxyzptlk/db231222/s/f;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()Ldbxyzptlk/db231222/H/b;
    .locals 1

    .prologue
    .line 625
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/f;->a()Ldbxyzptlk/db231222/s/f;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()Ldbxyzptlk/db231222/H/t;
    .locals 1

    .prologue
    .line 625
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/f;->c()Ldbxyzptlk/db231222/s/d;

    move-result-object v0

    return-object v0
.end method
