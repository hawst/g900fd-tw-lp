.class public final Ldbxyzptlk/db231222/s/q;
.super Ldbxyzptlk/db231222/H/j;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/s/t;


# static fields
.field public static a:Ldbxyzptlk/db231222/H/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/H/w",
            "<",
            "Ldbxyzptlk/db231222/s/q;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ldbxyzptlk/db231222/s/q;

.field private static final serialVersionUID:J


# instance fields
.field private c:I

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Ldbxyzptlk/db231222/s/k;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 84
    new-instance v0, Ldbxyzptlk/db231222/s/r;

    invoke-direct {v0}, Ldbxyzptlk/db231222/s/r;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/s/q;->a:Ldbxyzptlk/db231222/H/w;

    .line 730
    new-instance v0, Ldbxyzptlk/db231222/s/q;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/s/q;-><init>(Z)V

    sput-object v0, Ldbxyzptlk/db231222/s/q;->b:Ldbxyzptlk/db231222/s/q;

    .line 731
    sget-object v0, Ldbxyzptlk/db231222/s/q;->b:Ldbxyzptlk/db231222/s/q;

    invoke-direct {v0}, Ldbxyzptlk/db231222/s/q;->o()V

    .line 732
    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 36
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/j;-><init>()V

    .line 239
    iput-byte v0, p0, Ldbxyzptlk/db231222/s/q;->g:B

    .line 270
    iput v0, p0, Ldbxyzptlk/db231222/s/q;->h:I

    .line 37
    invoke-direct {p0}, Ldbxyzptlk/db231222/s/q;->o()V

    .line 40
    const/4 v0, 0x0

    .line 41
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 42
    :try_start_0
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->a()I

    move-result v2

    .line 43
    sparse-switch v2, :sswitch_data_0

    .line 48
    invoke-virtual {p0, p1, p2, v2}, Ldbxyzptlk/db231222/s/q;->a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 50
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 46
    goto :goto_0

    .line 55
    :sswitch_1
    iget v2, p0, Ldbxyzptlk/db231222/s/q;->c:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Ldbxyzptlk/db231222/s/q;->c:I

    .line 56
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->g()Ldbxyzptlk/db231222/H/d;

    move-result-object v2

    iput-object v2, p0, Ldbxyzptlk/db231222/s/q;->d:Ljava/lang/Object;
    :try_end_0
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    :try_start_1
    invoke-virtual {v0, p0}, Ldbxyzptlk/db231222/H/o;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/o;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/q;->L()V

    throw v0

    .line 60
    :sswitch_2
    :try_start_2
    iget v2, p0, Ldbxyzptlk/db231222/s/q;->c:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Ldbxyzptlk/db231222/s/q;->c:I

    .line 61
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->g()Ldbxyzptlk/db231222/H/d;

    move-result-object v2

    iput-object v2, p0, Ldbxyzptlk/db231222/s/q;->e:Ljava/lang/Object;
    :try_end_2
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 77
    :catch_1
    move-exception v0

    .line 78
    :try_start_3
    new-instance v1, Ldbxyzptlk/db231222/H/o;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/H/o;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ldbxyzptlk/db231222/H/o;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/o;

    move-result-object v0

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 65
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->h()I

    move-result v2

    .line 66
    invoke-static {v2}, Ldbxyzptlk/db231222/s/k;->a(I)Ldbxyzptlk/db231222/s/k;

    move-result-object v2

    .line 67
    if-eqz v2, :cond_0

    .line 68
    iget v3, p0, Ldbxyzptlk/db231222/s/q;->c:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Ldbxyzptlk/db231222/s/q;->c:I

    .line 69
    iput-object v2, p0, Ldbxyzptlk/db231222/s/q;->f:Ldbxyzptlk/db231222/s/k;
    :try_end_4
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 81
    :cond_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/q;->L()V

    .line 83
    return-void

    .line 43
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method synthetic constructor <init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;Ldbxyzptlk/db231222/s/r;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/s/q;-><init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)V

    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/H/k;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 19
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/H/j;-><init>(Ldbxyzptlk/db231222/H/k;)V

    .line 239
    iput-byte v0, p0, Ldbxyzptlk/db231222/s/q;->g:B

    .line 270
    iput v0, p0, Ldbxyzptlk/db231222/s/q;->h:I

    .line 21
    return-void
.end method

.method synthetic constructor <init>(Ldbxyzptlk/db231222/H/k;Ldbxyzptlk/db231222/s/r;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/s/q;-><init>(Ldbxyzptlk/db231222/H/k;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 22
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/j;-><init>()V

    .line 239
    iput-byte v0, p0, Ldbxyzptlk/db231222/s/q;->g:B

    .line 270
    iput v0, p0, Ldbxyzptlk/db231222/s/q;->h:I

    .line 22
    return-void
.end method

.method static synthetic a(Ldbxyzptlk/db231222/s/q;I)I
    .locals 0

    .prologue
    .line 14
    iput p1, p0, Ldbxyzptlk/db231222/s/q;->c:I

    return p1
.end method

.method static synthetic a(Ldbxyzptlk/db231222/s/q;Ldbxyzptlk/db231222/s/k;)Ldbxyzptlk/db231222/s/k;
    .locals 0

    .prologue
    .line 14
    iput-object p1, p0, Ldbxyzptlk/db231222/s/q;->f:Ldbxyzptlk/db231222/s/k;

    return-object p1
.end method

.method public static a()Ldbxyzptlk/db231222/s/q;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Ldbxyzptlk/db231222/s/q;->b:Ldbxyzptlk/db231222/s/q;

    return-object v0
.end method

.method public static a(Ldbxyzptlk/db231222/s/q;)Ldbxyzptlk/db231222/s/s;
    .locals 1

    .prologue
    .line 355
    invoke-static {}, Ldbxyzptlk/db231222/s/q;->m()Ldbxyzptlk/db231222/s/s;

    move-result-object v0

    invoke-virtual {v0, p0}, Ldbxyzptlk/db231222/s/s;->a(Ldbxyzptlk/db231222/s/q;)Ldbxyzptlk/db231222/s/s;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/s/q;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 14
    iput-object p1, p0, Ldbxyzptlk/db231222/s/q;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Ldbxyzptlk/db231222/s/q;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Ldbxyzptlk/db231222/s/q;->d:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Ldbxyzptlk/db231222/s/q;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 14
    iput-object p1, p0, Ldbxyzptlk/db231222/s/q;->e:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic c(Ldbxyzptlk/db231222/s/q;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Ldbxyzptlk/db231222/s/q;->e:Ljava/lang/Object;

    return-object v0
.end method

.method public static m()Ldbxyzptlk/db231222/s/s;
    .locals 1

    .prologue
    .line 352
    invoke-static {}, Ldbxyzptlk/db231222/s/s;->g()Ldbxyzptlk/db231222/s/s;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 1

    .prologue
    .line 235
    const-string v0, ""

    iput-object v0, p0, Ldbxyzptlk/db231222/s/q;->d:Ljava/lang/Object;

    .line 236
    const-string v0, ""

    iput-object v0, p0, Ldbxyzptlk/db231222/s/q;->e:Ljava/lang/Object;

    .line 237
    sget-object v0, Ldbxyzptlk/db231222/s/k;->a:Ldbxyzptlk/db231222/s/k;

    iput-object v0, p0, Ldbxyzptlk/db231222/s/q;->f:Ldbxyzptlk/db231222/s/k;

    .line 238
    return-void
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/H/g;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 258
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/q;->f()I

    .line 259
    iget v0, p0, Ldbxyzptlk/db231222/s/q;->c:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 260
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/q;->g()Ldbxyzptlk/db231222/H/d;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Ldbxyzptlk/db231222/H/g;->a(ILdbxyzptlk/db231222/H/d;)V

    .line 262
    :cond_0
    iget v0, p0, Ldbxyzptlk/db231222/s/q;->c:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 263
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/q;->j()Ldbxyzptlk/db231222/H/d;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Ldbxyzptlk/db231222/H/g;->a(ILdbxyzptlk/db231222/H/d;)V

    .line 265
    :cond_1
    iget v0, p0, Ldbxyzptlk/db231222/s/q;->c:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 266
    const/4 v0, 0x3

    iget-object v1, p0, Ldbxyzptlk/db231222/s/q;->f:Ldbxyzptlk/db231222/s/k;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/k;->a()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ldbxyzptlk/db231222/H/g;->b(II)V

    .line 268
    :cond_2
    return-void
.end method

.method public final b()Ldbxyzptlk/db231222/H/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldbxyzptlk/db231222/H/w",
            "<",
            "Ldbxyzptlk/db231222/s/q;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    sget-object v0, Ldbxyzptlk/db231222/s/q;->a:Ldbxyzptlk/db231222/H/w;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 111
    iget v1, p0, Ldbxyzptlk/db231222/s/q;->c:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Ldbxyzptlk/db231222/s/q;->d:Ljava/lang/Object;

    .line 122
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 123
    check-cast v0, Ljava/lang/String;

    .line 131
    :goto_0
    return-object v0

    .line 125
    :cond_0
    check-cast v0, Ldbxyzptlk/db231222/H/d;

    .line 127
    invoke-virtual {v0}, Ldbxyzptlk/db231222/H/d;->e()Ljava/lang/String;

    move-result-object v1

    .line 128
    invoke-virtual {v0}, Ldbxyzptlk/db231222/H/d;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 129
    iput-object v1, p0, Ldbxyzptlk/db231222/s/q;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 131
    goto :goto_0
.end method

.method public final e()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 241
    iget-byte v2, p0, Ldbxyzptlk/db231222/s/q;->g:B

    .line 242
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    if-ne v2, v0, :cond_0

    .line 253
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 242
    goto :goto_0

    .line 244
    :cond_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/q;->c()Z

    move-result v2

    if-nez v2, :cond_2

    .line 245
    iput-byte v1, p0, Ldbxyzptlk/db231222/s/q;->g:B

    move v0, v1

    .line 246
    goto :goto_0

    .line 248
    :cond_2
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/q;->h()Z

    move-result v2

    if-nez v2, :cond_3

    .line 249
    iput-byte v1, p0, Ldbxyzptlk/db231222/s/q;->g:B

    move v0, v1

    .line 250
    goto :goto_0

    .line 252
    :cond_3
    iput-byte v0, p0, Ldbxyzptlk/db231222/s/q;->g:B

    goto :goto_0
.end method

.method public final f()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 272
    iget v0, p0, Ldbxyzptlk/db231222/s/q;->h:I

    .line 273
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 289
    :goto_0
    return v0

    .line 275
    :cond_0
    const/4 v0, 0x0

    .line 276
    iget v1, p0, Ldbxyzptlk/db231222/s/q;->c:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 277
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/q;->g()Ldbxyzptlk/db231222/H/d;

    move-result-object v1

    invoke-static {v2, v1}, Ldbxyzptlk/db231222/H/g;->b(ILdbxyzptlk/db231222/H/d;)I

    move-result v1

    add-int/2addr v0, v1

    .line 280
    :cond_1
    iget v1, p0, Ldbxyzptlk/db231222/s/q;->c:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 281
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/q;->j()Ldbxyzptlk/db231222/H/d;

    move-result-object v1

    invoke-static {v3, v1}, Ldbxyzptlk/db231222/H/g;->b(ILdbxyzptlk/db231222/H/d;)I

    move-result v1

    add-int/2addr v0, v1

    .line 284
    :cond_2
    iget v1, p0, Ldbxyzptlk/db231222/s/q;->c:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 285
    const/4 v1, 0x3

    iget-object v2, p0, Ldbxyzptlk/db231222/s/q;->f:Ldbxyzptlk/db231222/s/k;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/s/k;->a()I

    move-result v2

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/H/g;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 288
    :cond_3
    iput v0, p0, Ldbxyzptlk/db231222/s/q;->h:I

    goto :goto_0
.end method

.method public final g()Ldbxyzptlk/db231222/H/d;
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Ldbxyzptlk/db231222/s/q;->d:Ljava/lang/Object;

    .line 144
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 145
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ldbxyzptlk/db231222/H/d;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/H/d;

    move-result-object v0

    .line 148
    iput-object v0, p0, Ldbxyzptlk/db231222/s/q;->d:Ljava/lang/Object;

    .line 151
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ldbxyzptlk/db231222/H/d;

    goto :goto_0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 166
    iget v0, p0, Ldbxyzptlk/db231222/s/q;->c:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 176
    iget-object v0, p0, Ldbxyzptlk/db231222/s/q;->e:Ljava/lang/Object;

    .line 177
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 178
    check-cast v0, Ljava/lang/String;

    .line 186
    :goto_0
    return-object v0

    .line 180
    :cond_0
    check-cast v0, Ldbxyzptlk/db231222/H/d;

    .line 182
    invoke-virtual {v0}, Ldbxyzptlk/db231222/H/d;->e()Ljava/lang/String;

    move-result-object v1

    .line 183
    invoke-virtual {v0}, Ldbxyzptlk/db231222/H/d;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 184
    iput-object v1, p0, Ldbxyzptlk/db231222/s/q;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 186
    goto :goto_0
.end method

.method public final j()Ldbxyzptlk/db231222/H/d;
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Ldbxyzptlk/db231222/s/q;->e:Ljava/lang/Object;

    .line 199
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 200
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ldbxyzptlk/db231222/H/d;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/H/d;

    move-result-object v0

    .line 203
    iput-object v0, p0, Ldbxyzptlk/db231222/s/q;->e:Ljava/lang/Object;

    .line 206
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ldbxyzptlk/db231222/H/d;

    goto :goto_0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 221
    iget v0, p0, Ldbxyzptlk/db231222/s/q;->c:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Ldbxyzptlk/db231222/s/k;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Ldbxyzptlk/db231222/s/q;->f:Ldbxyzptlk/db231222/s/k;

    return-object v0
.end method

.method public final n()Ldbxyzptlk/db231222/s/s;
    .locals 1

    .prologue
    .line 357
    invoke-static {p0}, Ldbxyzptlk/db231222/s/q;->a(Ldbxyzptlk/db231222/s/q;)Ldbxyzptlk/db231222/s/s;

    move-result-object v0

    return-object v0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 296
    invoke-super {p0}, Ldbxyzptlk/db231222/H/j;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
