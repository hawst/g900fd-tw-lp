.class public final Ldbxyzptlk/db231222/s/m;
.super Ldbxyzptlk/db231222/H/j;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/s/p;


# static fields
.field public static a:Ldbxyzptlk/db231222/H/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/H/w",
            "<",
            "Ldbxyzptlk/db231222/s/m;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ldbxyzptlk/db231222/s/m;

.field private static final serialVersionUID:J


# instance fields
.field private c:I

.field private d:Ldbxyzptlk/db231222/s/q;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 80
    new-instance v0, Ldbxyzptlk/db231222/s/n;

    invoke-direct {v0}, Ldbxyzptlk/db231222/s/n;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/s/m;->a:Ldbxyzptlk/db231222/H/w;

    .line 413
    new-instance v0, Ldbxyzptlk/db231222/s/m;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/s/m;-><init>(Z)V

    sput-object v0, Ldbxyzptlk/db231222/s/m;->b:Ldbxyzptlk/db231222/s/m;

    .line 414
    sget-object v0, Ldbxyzptlk/db231222/s/m;->b:Ldbxyzptlk/db231222/s/m;

    invoke-direct {v0}, Ldbxyzptlk/db231222/s/m;->h()V

    .line 415
    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)V
    .locals 4

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x1

    .line 38
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/j;-><init>()V

    .line 123
    iput-byte v0, p0, Ldbxyzptlk/db231222/s/m;->e:B

    .line 146
    iput v0, p0, Ldbxyzptlk/db231222/s/m;->f:I

    .line 39
    invoke-direct {p0}, Ldbxyzptlk/db231222/s/m;->h()V

    .line 42
    const/4 v2, 0x0

    .line 43
    :goto_0
    if-nez v2, :cond_1

    .line 44
    :try_start_0
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->a()I

    move-result v0

    .line 45
    sparse-switch v0, :sswitch_data_0

    .line 50
    invoke-virtual {p0, p1, p2, v0}, Ldbxyzptlk/db231222/s/m;->a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;I)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    move v2, v0

    .line 70
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 48
    goto :goto_1

    .line 57
    :sswitch_1
    const/4 v0, 0x0

    .line 58
    iget v3, p0, Ldbxyzptlk/db231222/s/m;->c:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_2

    .line 59
    iget-object v0, p0, Ldbxyzptlk/db231222/s/m;->d:Ldbxyzptlk/db231222/s/q;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/q;->n()Ldbxyzptlk/db231222/s/s;

    move-result-object v0

    move-object v3, v0

    .line 61
    :goto_2
    sget-object v0, Ldbxyzptlk/db231222/s/q;->a:Ldbxyzptlk/db231222/H/w;

    invoke-virtual {p1, v0, p2}, Ldbxyzptlk/db231222/H/f;->a(Ldbxyzptlk/db231222/H/w;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/s/q;

    iput-object v0, p0, Ldbxyzptlk/db231222/s/m;->d:Ldbxyzptlk/db231222/s/q;

    .line 62
    if-eqz v3, :cond_0

    .line 63
    iget-object v0, p0, Ldbxyzptlk/db231222/s/m;->d:Ldbxyzptlk/db231222/s/q;

    invoke-virtual {v3, v0}, Ldbxyzptlk/db231222/s/s;->a(Ldbxyzptlk/db231222/s/q;)Ldbxyzptlk/db231222/s/s;

    .line 64
    invoke-virtual {v3}, Ldbxyzptlk/db231222/s/s;->c()Ldbxyzptlk/db231222/s/q;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/s/m;->d:Ldbxyzptlk/db231222/s/q;

    .line 66
    :cond_0
    iget v0, p0, Ldbxyzptlk/db231222/s/m;->c:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/s/m;->c:I
    :try_end_0
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v2

    .line 67
    goto :goto_1

    .line 77
    :cond_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/m;->L()V

    .line 79
    return-void

    .line 71
    :catch_0
    move-exception v0

    .line 72
    :try_start_1
    invoke-virtual {v0, p0}, Ldbxyzptlk/db231222/H/o;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/o;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/m;->L()V

    throw v0

    .line 73
    :catch_1
    move-exception v0

    .line 74
    :try_start_2
    new-instance v1, Ldbxyzptlk/db231222/H/o;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/H/o;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ldbxyzptlk/db231222/H/o;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/o;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    move-object v3, v0

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_1

    .line 45
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method synthetic constructor <init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;Ldbxyzptlk/db231222/s/n;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/s/m;-><init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)V

    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/H/k;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 21
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/H/j;-><init>(Ldbxyzptlk/db231222/H/k;)V

    .line 123
    iput-byte v0, p0, Ldbxyzptlk/db231222/s/m;->e:B

    .line 146
    iput v0, p0, Ldbxyzptlk/db231222/s/m;->f:I

    .line 23
    return-void
.end method

.method synthetic constructor <init>(Ldbxyzptlk/db231222/H/k;Ldbxyzptlk/db231222/s/n;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/s/m;-><init>(Ldbxyzptlk/db231222/H/k;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 24
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/j;-><init>()V

    .line 123
    iput-byte v0, p0, Ldbxyzptlk/db231222/s/m;->e:B

    .line 146
    iput v0, p0, Ldbxyzptlk/db231222/s/m;->f:I

    .line 24
    return-void
.end method

.method static synthetic a(Ldbxyzptlk/db231222/s/m;I)I
    .locals 0

    .prologue
    .line 16
    iput p1, p0, Ldbxyzptlk/db231222/s/m;->c:I

    return p1
.end method

.method public static a()Ldbxyzptlk/db231222/s/m;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Ldbxyzptlk/db231222/s/m;->b:Ldbxyzptlk/db231222/s/m;

    return-object v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/s/m;Ldbxyzptlk/db231222/s/q;)Ldbxyzptlk/db231222/s/q;
    .locals 0

    .prologue
    .line 16
    iput-object p1, p0, Ldbxyzptlk/db231222/s/m;->d:Ldbxyzptlk/db231222/s/q;

    return-object p1
.end method

.method public static g()Ldbxyzptlk/db231222/s/o;
    .locals 1

    .prologue
    .line 220
    invoke-static {}, Ldbxyzptlk/db231222/s/o;->g()Ldbxyzptlk/db231222/s/o;

    move-result-object v0

    return-object v0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 121
    invoke-static {}, Ldbxyzptlk/db231222/s/q;->a()Ldbxyzptlk/db231222/s/q;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/s/m;->d:Ldbxyzptlk/db231222/s/q;

    .line 122
    return-void
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/H/g;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 140
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/m;->f()I

    .line 141
    iget v0, p0, Ldbxyzptlk/db231222/s/m;->c:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 142
    iget-object v0, p0, Ldbxyzptlk/db231222/s/m;->d:Ldbxyzptlk/db231222/s/q;

    invoke-virtual {p1, v1, v0}, Ldbxyzptlk/db231222/H/g;->a(ILdbxyzptlk/db231222/H/t;)V

    .line 144
    :cond_0
    return-void
.end method

.method public final b()Ldbxyzptlk/db231222/H/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldbxyzptlk/db231222/H/w",
            "<",
            "Ldbxyzptlk/db231222/s/m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    sget-object v0, Ldbxyzptlk/db231222/s/m;->a:Ldbxyzptlk/db231222/H/w;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 107
    iget v1, p0, Ldbxyzptlk/db231222/s/m;->c:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ldbxyzptlk/db231222/s/q;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Ldbxyzptlk/db231222/s/m;->d:Ldbxyzptlk/db231222/s/q;

    return-object v0
.end method

.method public final e()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 125
    iget-byte v2, p0, Ldbxyzptlk/db231222/s/m;->e:B

    .line 126
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    if-ne v2, v0, :cond_0

    .line 135
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 126
    goto :goto_0

    .line 128
    :cond_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/m;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 129
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/m;->d()Ldbxyzptlk/db231222/s/q;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/s/q;->e()Z

    move-result v2

    if-nez v2, :cond_2

    .line 130
    iput-byte v1, p0, Ldbxyzptlk/db231222/s/m;->e:B

    move v0, v1

    .line 131
    goto :goto_0

    .line 134
    :cond_2
    iput-byte v0, p0, Ldbxyzptlk/db231222/s/m;->e:B

    goto :goto_0
.end method

.method public final f()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 148
    iget v0, p0, Ldbxyzptlk/db231222/s/m;->f:I

    .line 149
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 157
    :goto_0
    return v0

    .line 151
    :cond_0
    const/4 v0, 0x0

    .line 152
    iget v1, p0, Ldbxyzptlk/db231222/s/m;->c:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 153
    iget-object v1, p0, Ldbxyzptlk/db231222/s/m;->d:Ldbxyzptlk/db231222/s/q;

    invoke-static {v2, v1}, Ldbxyzptlk/db231222/H/g;->b(ILdbxyzptlk/db231222/H/t;)I

    move-result v1

    add-int/2addr v0, v1

    .line 156
    :cond_1
    iput v0, p0, Ldbxyzptlk/db231222/s/m;->f:I

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 164
    invoke-super {p0}, Ldbxyzptlk/db231222/H/j;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
