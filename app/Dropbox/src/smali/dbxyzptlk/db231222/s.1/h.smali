.class public final enum Ldbxyzptlk/db231222/s/h;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldbxyzptlk/db231222/s/h;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ldbxyzptlk/db231222/s/h;

.field public static final enum b:Ldbxyzptlk/db231222/s/h;

.field public static final enum c:Ldbxyzptlk/db231222/s/h;

.field public static final enum d:Ldbxyzptlk/db231222/s/h;

.field private static e:Ldbxyzptlk/db231222/H/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/H/n",
            "<",
            "Ldbxyzptlk/db231222/s/h;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic g:[Ldbxyzptlk/db231222/s/h;


# instance fields
.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 181
    new-instance v0, Ldbxyzptlk/db231222/s/h;

    const-string v1, "UNSPECIFIED"

    invoke-direct {v0, v1, v2, v2, v2}, Ldbxyzptlk/db231222/s/h;-><init>(Ljava/lang/String;III)V

    sput-object v0, Ldbxyzptlk/db231222/s/h;->a:Ldbxyzptlk/db231222/s/h;

    .line 189
    new-instance v0, Ldbxyzptlk/db231222/s/h;

    const-string v1, "ALLOW"

    invoke-direct {v0, v1, v3, v3, v3}, Ldbxyzptlk/db231222/s/h;-><init>(Ljava/lang/String;III)V

    sput-object v0, Ldbxyzptlk/db231222/s/h;->b:Ldbxyzptlk/db231222/s/h;

    .line 197
    new-instance v0, Ldbxyzptlk/db231222/s/h;

    const-string v1, "WARN"

    invoke-direct {v0, v1, v4, v4, v4}, Ldbxyzptlk/db231222/s/h;-><init>(Ljava/lang/String;III)V

    sput-object v0, Ldbxyzptlk/db231222/s/h;->c:Ldbxyzptlk/db231222/s/h;

    .line 205
    new-instance v0, Ldbxyzptlk/db231222/s/h;

    const-string v1, "FORBID"

    invoke-direct {v0, v1, v5, v5, v5}, Ldbxyzptlk/db231222/s/h;-><init>(Ljava/lang/String;III)V

    sput-object v0, Ldbxyzptlk/db231222/s/h;->d:Ldbxyzptlk/db231222/s/h;

    .line 176
    const/4 v0, 0x4

    new-array v0, v0, [Ldbxyzptlk/db231222/s/h;

    sget-object v1, Ldbxyzptlk/db231222/s/h;->a:Ldbxyzptlk/db231222/s/h;

    aput-object v1, v0, v2

    sget-object v1, Ldbxyzptlk/db231222/s/h;->b:Ldbxyzptlk/db231222/s/h;

    aput-object v1, v0, v3

    sget-object v1, Ldbxyzptlk/db231222/s/h;->c:Ldbxyzptlk/db231222/s/h;

    aput-object v1, v0, v4

    sget-object v1, Ldbxyzptlk/db231222/s/h;->d:Ldbxyzptlk/db231222/s/h;

    aput-object v1, v0, v5

    sput-object v0, Ldbxyzptlk/db231222/s/h;->g:[Ldbxyzptlk/db231222/s/h;

    .line 255
    new-instance v0, Ldbxyzptlk/db231222/s/i;

    invoke-direct {v0}, Ldbxyzptlk/db231222/s/i;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/s/h;->e:Ldbxyzptlk/db231222/H/n;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 264
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 265
    iput p4, p0, Ldbxyzptlk/db231222/s/h;->f:I

    .line 266
    return-void
.end method

.method public static a(I)Ldbxyzptlk/db231222/s/h;
    .locals 1

    .prologue
    .line 241
    packed-switch p0, :pswitch_data_0

    .line 246
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 242
    :pswitch_0
    sget-object v0, Ldbxyzptlk/db231222/s/h;->a:Ldbxyzptlk/db231222/s/h;

    goto :goto_0

    .line 243
    :pswitch_1
    sget-object v0, Ldbxyzptlk/db231222/s/h;->b:Ldbxyzptlk/db231222/s/h;

    goto :goto_0

    .line 244
    :pswitch_2
    sget-object v0, Ldbxyzptlk/db231222/s/h;->c:Ldbxyzptlk/db231222/s/h;

    goto :goto_0

    .line 245
    :pswitch_3
    sget-object v0, Ldbxyzptlk/db231222/s/h;->d:Ldbxyzptlk/db231222/s/h;

    goto :goto_0

    .line 241
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Ldbxyzptlk/db231222/s/h;
    .locals 1

    .prologue
    .line 176
    const-class v0, Ldbxyzptlk/db231222/s/h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/s/h;

    return-object v0
.end method

.method public static values()[Ldbxyzptlk/db231222/s/h;
    .locals 1

    .prologue
    .line 176
    sget-object v0, Ldbxyzptlk/db231222/s/h;->g:[Ldbxyzptlk/db231222/s/h;

    invoke-virtual {v0}, [Ldbxyzptlk/db231222/s/h;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/s/h;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 238
    iget v0, p0, Ldbxyzptlk/db231222/s/h;->f:I

    return v0
.end method
