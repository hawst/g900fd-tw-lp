.class public final enum Ldbxyzptlk/db231222/s/k;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldbxyzptlk/db231222/s/k;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ldbxyzptlk/db231222/s/k;

.field public static final enum b:Ldbxyzptlk/db231222/s/k;

.field public static final enum c:Ldbxyzptlk/db231222/s/k;

.field private static d:Ldbxyzptlk/db231222/H/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/H/n",
            "<",
            "Ldbxyzptlk/db231222/s/k;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic f:[Ldbxyzptlk/db231222/s/k;


# instance fields
.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 22
    new-instance v0, Ldbxyzptlk/db231222/s/k;

    const-string v1, "UNSPECIFIED"

    invoke-direct {v0, v1, v2, v2, v2}, Ldbxyzptlk/db231222/s/k;-><init>(Ljava/lang/String;III)V

    sput-object v0, Ldbxyzptlk/db231222/s/k;->a:Ldbxyzptlk/db231222/s/k;

    .line 30
    new-instance v0, Ldbxyzptlk/db231222/s/k;

    const-string v1, "PERSONAL"

    invoke-direct {v0, v1, v3, v3, v3}, Ldbxyzptlk/db231222/s/k;-><init>(Ljava/lang/String;III)V

    sput-object v0, Ldbxyzptlk/db231222/s/k;->b:Ldbxyzptlk/db231222/s/k;

    .line 38
    new-instance v0, Ldbxyzptlk/db231222/s/k;

    const-string v1, "BUSINESS"

    invoke-direct {v0, v1, v4, v4, v4}, Ldbxyzptlk/db231222/s/k;-><init>(Ljava/lang/String;III)V

    sput-object v0, Ldbxyzptlk/db231222/s/k;->c:Ldbxyzptlk/db231222/s/k;

    .line 13
    const/4 v0, 0x3

    new-array v0, v0, [Ldbxyzptlk/db231222/s/k;

    sget-object v1, Ldbxyzptlk/db231222/s/k;->a:Ldbxyzptlk/db231222/s/k;

    aput-object v1, v0, v2

    sget-object v1, Ldbxyzptlk/db231222/s/k;->b:Ldbxyzptlk/db231222/s/k;

    aput-object v1, v0, v3

    sget-object v1, Ldbxyzptlk/db231222/s/k;->c:Ldbxyzptlk/db231222/s/k;

    aput-object v1, v0, v4

    sput-object v0, Ldbxyzptlk/db231222/s/k;->f:[Ldbxyzptlk/db231222/s/k;

    .line 83
    new-instance v0, Ldbxyzptlk/db231222/s/l;

    invoke-direct {v0}, Ldbxyzptlk/db231222/s/l;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/s/k;->d:Ldbxyzptlk/db231222/H/n;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 93
    iput p4, p0, Ldbxyzptlk/db231222/s/k;->e:I

    .line 94
    return-void
.end method

.method public static a(I)Ldbxyzptlk/db231222/s/k;
    .locals 1

    .prologue
    .line 70
    packed-switch p0, :pswitch_data_0

    .line 74
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 71
    :pswitch_0
    sget-object v0, Ldbxyzptlk/db231222/s/k;->a:Ldbxyzptlk/db231222/s/k;

    goto :goto_0

    .line 72
    :pswitch_1
    sget-object v0, Ldbxyzptlk/db231222/s/k;->b:Ldbxyzptlk/db231222/s/k;

    goto :goto_0

    .line 73
    :pswitch_2
    sget-object v0, Ldbxyzptlk/db231222/s/k;->c:Ldbxyzptlk/db231222/s/k;

    goto :goto_0

    .line 70
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Ldbxyzptlk/db231222/s/k;
    .locals 1

    .prologue
    .line 13
    const-class v0, Ldbxyzptlk/db231222/s/k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/s/k;

    return-object v0
.end method

.method public static values()[Ldbxyzptlk/db231222/s/k;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Ldbxyzptlk/db231222/s/k;->f:[Ldbxyzptlk/db231222/s/k;

    invoke-virtual {v0}, [Ldbxyzptlk/db231222/s/k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/s/k;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Ldbxyzptlk/db231222/s/k;->e:I

    return v0
.end method
