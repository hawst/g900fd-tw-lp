.class public final Ldbxyzptlk/db231222/s/d;
.super Ldbxyzptlk/db231222/H/j;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/s/g;


# static fields
.field public static a:Ldbxyzptlk/db231222/H/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/H/w",
            "<",
            "Ldbxyzptlk/db231222/s/d;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ldbxyzptlk/db231222/s/d;

.field private static final serialVersionUID:J


# instance fields
.field private c:I

.field private d:J

.field private e:J

.field private f:J

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 401
    new-instance v0, Ldbxyzptlk/db231222/s/e;

    invoke-direct {v0}, Ldbxyzptlk/db231222/s/e;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/s/d;->a:Ldbxyzptlk/db231222/H/w;

    .line 887
    new-instance v0, Ldbxyzptlk/db231222/s/d;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/s/d;-><init>(Z)V

    sput-object v0, Ldbxyzptlk/db231222/s/d;->b:Ldbxyzptlk/db231222/s/d;

    .line 888
    sget-object v0, Ldbxyzptlk/db231222/s/d;->b:Ldbxyzptlk/db231222/s/d;

    invoke-direct {v0}, Ldbxyzptlk/db231222/s/d;->m()V

    .line 889
    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 357
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/j;-><init>()V

    .line 494
    iput-byte v0, p0, Ldbxyzptlk/db231222/s/d;->g:B

    .line 529
    iput v0, p0, Ldbxyzptlk/db231222/s/d;->h:I

    .line 358
    invoke-direct {p0}, Ldbxyzptlk/db231222/s/d;->m()V

    .line 361
    const/4 v0, 0x0

    .line 362
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 363
    :try_start_0
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->a()I

    move-result v2

    .line 364
    sparse-switch v2, :sswitch_data_0

    .line 369
    invoke-virtual {p0, p1, p2, v2}, Ldbxyzptlk/db231222/s/d;->a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 371
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 367
    goto :goto_0

    .line 376
    :sswitch_1
    iget v2, p0, Ldbxyzptlk/db231222/s/d;->c:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Ldbxyzptlk/db231222/s/d;->c:I

    .line 377
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->c()J

    move-result-wide v2

    iput-wide v2, p0, Ldbxyzptlk/db231222/s/d;->d:J
    :try_end_0
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 392
    :catch_0
    move-exception v0

    .line 393
    :try_start_1
    invoke-virtual {v0, p0}, Ldbxyzptlk/db231222/H/o;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/o;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 398
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/d;->L()V

    throw v0

    .line 381
    :sswitch_2
    :try_start_2
    iget v2, p0, Ldbxyzptlk/db231222/s/d;->c:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Ldbxyzptlk/db231222/s/d;->c:I

    .line 382
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->c()J

    move-result-wide v2

    iput-wide v2, p0, Ldbxyzptlk/db231222/s/d;->e:J
    :try_end_2
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 394
    :catch_1
    move-exception v0

    .line 395
    :try_start_3
    new-instance v1, Ldbxyzptlk/db231222/H/o;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/H/o;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ldbxyzptlk/db231222/H/o;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/o;

    move-result-object v0

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 386
    :sswitch_3
    :try_start_4
    iget v2, p0, Ldbxyzptlk/db231222/s/d;->c:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Ldbxyzptlk/db231222/s/d;->c:I

    .line 387
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->c()J

    move-result-wide v2

    iput-wide v2, p0, Ldbxyzptlk/db231222/s/d;->f:J
    :try_end_4
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 398
    :cond_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/d;->L()V

    .line 400
    return-void

    .line 364
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method synthetic constructor <init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;Ldbxyzptlk/db231222/s/b;)V
    .locals 0

    .prologue
    .line 335
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/s/d;-><init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)V

    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/H/k;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 340
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/H/j;-><init>(Ldbxyzptlk/db231222/H/k;)V

    .line 494
    iput-byte v0, p0, Ldbxyzptlk/db231222/s/d;->g:B

    .line 529
    iput v0, p0, Ldbxyzptlk/db231222/s/d;->h:I

    .line 342
    return-void
.end method

.method synthetic constructor <init>(Ldbxyzptlk/db231222/H/k;Ldbxyzptlk/db231222/s/b;)V
    .locals 0

    .prologue
    .line 335
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/s/d;-><init>(Ldbxyzptlk/db231222/H/k;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 343
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/j;-><init>()V

    .line 494
    iput-byte v0, p0, Ldbxyzptlk/db231222/s/d;->g:B

    .line 529
    iput v0, p0, Ldbxyzptlk/db231222/s/d;->h:I

    .line 343
    return-void
.end method

.method static synthetic a(Ldbxyzptlk/db231222/s/d;I)I
    .locals 0

    .prologue
    .line 335
    iput p1, p0, Ldbxyzptlk/db231222/s/d;->c:I

    return p1
.end method

.method static synthetic a(Ldbxyzptlk/db231222/s/d;J)J
    .locals 0

    .prologue
    .line 335
    iput-wide p1, p0, Ldbxyzptlk/db231222/s/d;->d:J

    return-wide p1
.end method

.method public static a()Ldbxyzptlk/db231222/s/d;
    .locals 1

    .prologue
    .line 347
    sget-object v0, Ldbxyzptlk/db231222/s/d;->b:Ldbxyzptlk/db231222/s/d;

    return-object v0
.end method

.method public static a(Ldbxyzptlk/db231222/s/d;)Ldbxyzptlk/db231222/s/f;
    .locals 1

    .prologue
    .line 614
    invoke-static {}, Ldbxyzptlk/db231222/s/d;->k()Ldbxyzptlk/db231222/s/f;

    move-result-object v0

    invoke-virtual {v0, p0}, Ldbxyzptlk/db231222/s/f;->a(Ldbxyzptlk/db231222/s/d;)Ldbxyzptlk/db231222/s/f;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ldbxyzptlk/db231222/s/d;J)J
    .locals 0

    .prologue
    .line 335
    iput-wide p1, p0, Ldbxyzptlk/db231222/s/d;->e:J

    return-wide p1
.end method

.method static synthetic c(Ldbxyzptlk/db231222/s/d;J)J
    .locals 0

    .prologue
    .line 335
    iput-wide p1, p0, Ldbxyzptlk/db231222/s/d;->f:J

    return-wide p1
.end method

.method public static k()Ldbxyzptlk/db231222/s/f;
    .locals 1

    .prologue
    .line 611
    invoke-static {}, Ldbxyzptlk/db231222/s/f;->k()Ldbxyzptlk/db231222/s/f;

    move-result-object v0

    return-object v0
.end method

.method private m()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 490
    iput-wide v0, p0, Ldbxyzptlk/db231222/s/d;->d:J

    .line 491
    iput-wide v0, p0, Ldbxyzptlk/db231222/s/d;->e:J

    .line 492
    iput-wide v0, p0, Ldbxyzptlk/db231222/s/d;->f:J

    .line 493
    return-void
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/H/g;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 517
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/d;->f()I

    .line 518
    iget v0, p0, Ldbxyzptlk/db231222/s/d;->c:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 519
    iget-wide v0, p0, Ldbxyzptlk/db231222/s/d;->d:J

    invoke-virtual {p1, v2, v0, v1}, Ldbxyzptlk/db231222/H/g;->a(IJ)V

    .line 521
    :cond_0
    iget v0, p0, Ldbxyzptlk/db231222/s/d;->c:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 522
    iget-wide v0, p0, Ldbxyzptlk/db231222/s/d;->e:J

    invoke-virtual {p1, v3, v0, v1}, Ldbxyzptlk/db231222/H/g;->a(IJ)V

    .line 524
    :cond_1
    iget v0, p0, Ldbxyzptlk/db231222/s/d;->c:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 525
    const/4 v0, 0x3

    iget-wide v1, p0, Ldbxyzptlk/db231222/s/d;->f:J

    invoke-virtual {p1, v0, v1, v2}, Ldbxyzptlk/db231222/H/g;->a(IJ)V

    .line 527
    :cond_2
    return-void
.end method

.method public final b()Ldbxyzptlk/db231222/H/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldbxyzptlk/db231222/H/w",
            "<",
            "Ldbxyzptlk/db231222/s/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 413
    sget-object v0, Ldbxyzptlk/db231222/s/d;->a:Ldbxyzptlk/db231222/H/w;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 428
    iget v1, p0, Ldbxyzptlk/db231222/s/d;->c:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 438
    iget-wide v0, p0, Ldbxyzptlk/db231222/s/d;->d:J

    return-wide v0
.end method

.method public final e()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 496
    iget-byte v2, p0, Ldbxyzptlk/db231222/s/d;->g:B

    .line 497
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    if-ne v2, v0, :cond_0

    :goto_0
    move v1, v0

    .line 512
    :goto_1
    return v1

    :cond_0
    move v0, v1

    .line 497
    goto :goto_0

    .line 499
    :cond_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/d;->c()Z

    move-result v2

    if-nez v2, :cond_2

    .line 500
    iput-byte v1, p0, Ldbxyzptlk/db231222/s/d;->g:B

    goto :goto_1

    .line 503
    :cond_2
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/d;->g()Z

    move-result v2

    if-nez v2, :cond_3

    .line 504
    iput-byte v1, p0, Ldbxyzptlk/db231222/s/d;->g:B

    goto :goto_1

    .line 507
    :cond_3
    invoke-virtual {p0}, Ldbxyzptlk/db231222/s/d;->i()Z

    move-result v2

    if-nez v2, :cond_4

    .line 508
    iput-byte v1, p0, Ldbxyzptlk/db231222/s/d;->g:B

    goto :goto_1

    .line 511
    :cond_4
    iput-byte v0, p0, Ldbxyzptlk/db231222/s/d;->g:B

    move v1, v0

    .line 512
    goto :goto_1
.end method

.method public final f()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 531
    iget v0, p0, Ldbxyzptlk/db231222/s/d;->h:I

    .line 532
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 548
    :goto_0
    return v0

    .line 534
    :cond_0
    const/4 v0, 0x0

    .line 535
    iget v1, p0, Ldbxyzptlk/db231222/s/d;->c:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 536
    iget-wide v1, p0, Ldbxyzptlk/db231222/s/d;->d:J

    invoke-static {v3, v1, v2}, Ldbxyzptlk/db231222/H/g;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 539
    :cond_1
    iget v1, p0, Ldbxyzptlk/db231222/s/d;->c:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_2

    .line 540
    iget-wide v1, p0, Ldbxyzptlk/db231222/s/d;->e:J

    invoke-static {v4, v1, v2}, Ldbxyzptlk/db231222/H/g;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 543
    :cond_2
    iget v1, p0, Ldbxyzptlk/db231222/s/d;->c:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 544
    const/4 v1, 0x3

    iget-wide v2, p0, Ldbxyzptlk/db231222/s/d;->f:J

    invoke-static {v1, v2, v3}, Ldbxyzptlk/db231222/H/g;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 547
    :cond_3
    iput v0, p0, Ldbxyzptlk/db231222/s/d;->h:I

    goto :goto_0
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 452
    iget v0, p0, Ldbxyzptlk/db231222/s/d;->c:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()J
    .locals 2

    .prologue
    .line 462
    iget-wide v0, p0, Ldbxyzptlk/db231222/s/d;->e:J

    return-wide v0
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 476
    iget v0, p0, Ldbxyzptlk/db231222/s/d;->c:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 486
    iget-wide v0, p0, Ldbxyzptlk/db231222/s/d;->f:J

    return-wide v0
.end method

.method public final l()Ldbxyzptlk/db231222/s/f;
    .locals 1

    .prologue
    .line 616
    invoke-static {p0}, Ldbxyzptlk/db231222/s/d;->a(Ldbxyzptlk/db231222/s/d;)Ldbxyzptlk/db231222/s/f;

    move-result-object v0

    return-object v0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 555
    invoke-super {p0}, Ldbxyzptlk/db231222/H/j;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
