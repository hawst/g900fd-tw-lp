.class public final Ldbxyzptlk/db231222/R/i;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field final synthetic a:Ldbxyzptlk/db231222/R/c;

.field private final b:Ljava/lang/String;

.field private final c:J

.field private final d:[Ljava/io/InputStream;

.field private final e:[J


# direct methods
.method private constructor <init>(Ldbxyzptlk/db231222/R/c;Ljava/lang/String;J[Ljava/io/InputStream;[J)V
    .locals 0

    .prologue
    .line 671
    iput-object p1, p0, Ldbxyzptlk/db231222/R/i;->a:Ldbxyzptlk/db231222/R/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672
    iput-object p2, p0, Ldbxyzptlk/db231222/R/i;->b:Ljava/lang/String;

    .line 673
    iput-wide p3, p0, Ldbxyzptlk/db231222/R/i;->c:J

    .line 674
    iput-object p5, p0, Ldbxyzptlk/db231222/R/i;->d:[Ljava/io/InputStream;

    .line 675
    iput-object p6, p0, Ldbxyzptlk/db231222/R/i;->e:[J

    .line 676
    return-void
.end method

.method synthetic constructor <init>(Ldbxyzptlk/db231222/R/c;Ljava/lang/String;J[Ljava/io/InputStream;[JLdbxyzptlk/db231222/R/d;)V
    .locals 0

    .prologue
    .line 665
    invoke-direct/range {p0 .. p6}, Ldbxyzptlk/db231222/R/i;-><init>(Ldbxyzptlk/db231222/R/c;Ljava/lang/String;J[Ljava/io/InputStream;[J)V

    return-void
.end method


# virtual methods
.method public final a()Ldbxyzptlk/db231222/R/f;
    .locals 4

    .prologue
    .line 684
    iget-object v0, p0, Ldbxyzptlk/db231222/R/i;->a:Ldbxyzptlk/db231222/R/c;

    iget-object v1, p0, Ldbxyzptlk/db231222/R/i;->b:Ljava/lang/String;

    iget-wide v2, p0, Ldbxyzptlk/db231222/R/i;->c:J

    invoke-static {v0, v1, v2, v3}, Ldbxyzptlk/db231222/R/c;->a(Ldbxyzptlk/db231222/R/c;Ljava/lang/String;J)Ldbxyzptlk/db231222/R/f;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 689
    iget-object v0, p0, Ldbxyzptlk/db231222/R/i;->d:[Ljava/io/InputStream;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final close()V
    .locals 4

    .prologue
    .line 703
    iget-object v1, p0, Ldbxyzptlk/db231222/R/i;->d:[Ljava/io/InputStream;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 704
    invoke-static {v3}, Ldbxyzptlk/db231222/R/v;->a(Ljava/io/Closeable;)V

    .line 703
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 706
    :cond_0
    return-void
.end method
