.class public final enum Ldbxyzptlk/db231222/v/n;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldbxyzptlk/db231222/v/n;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ldbxyzptlk/db231222/v/n;

.field public static final enum b:Ldbxyzptlk/db231222/v/n;

.field public static final enum c:Ldbxyzptlk/db231222/v/n;

.field public static final enum d:Ldbxyzptlk/db231222/v/n;

.field public static final enum e:Ldbxyzptlk/db231222/v/n;

.field public static final enum f:Ldbxyzptlk/db231222/v/n;

.field public static final enum g:Ldbxyzptlk/db231222/v/n;

.field public static final enum h:Ldbxyzptlk/db231222/v/n;

.field public static final enum i:Ldbxyzptlk/db231222/v/n;

.field public static final enum j:Ldbxyzptlk/db231222/v/n;

.field private static final synthetic l:[Ldbxyzptlk/db231222/v/n;


# instance fields
.field private k:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1311
    new-instance v0, Ldbxyzptlk/db231222/v/n;

    const-string v1, "ICON_32x32"

    const-string v2, "small"

    invoke-direct {v0, v1, v4, v2}, Ldbxyzptlk/db231222/v/n;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/v/n;->a:Ldbxyzptlk/db231222/v/n;

    .line 1313
    new-instance v0, Ldbxyzptlk/db231222/v/n;

    const-string v1, "ICON_64x64"

    const-string v2, "medium"

    invoke-direct {v0, v1, v5, v2}, Ldbxyzptlk/db231222/v/n;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/v/n;->b:Ldbxyzptlk/db231222/v/n;

    .line 1315
    new-instance v0, Ldbxyzptlk/db231222/v/n;

    const-string v1, "ICON_128x128"

    const-string v2, "large"

    invoke-direct {v0, v1, v6, v2}, Ldbxyzptlk/db231222/v/n;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/v/n;->c:Ldbxyzptlk/db231222/v/n;

    .line 1317
    new-instance v0, Ldbxyzptlk/db231222/v/n;

    const-string v1, "ICON_256x256"

    const-string v2, "256x256"

    invoke-direct {v0, v1, v7, v2}, Ldbxyzptlk/db231222/v/n;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/v/n;->d:Ldbxyzptlk/db231222/v/n;

    .line 1322
    new-instance v0, Ldbxyzptlk/db231222/v/n;

    const-string v1, "BESTFIT_FITONE_256"

    const-string v2, "256x256_fit_one_bestfit"

    invoke-direct {v0, v1, v8, v2}, Ldbxyzptlk/db231222/v/n;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/v/n;->e:Ldbxyzptlk/db231222/v/n;

    .line 1327
    new-instance v0, Ldbxyzptlk/db231222/v/n;

    const-string v1, "BESTFIT_320x240"

    const/4 v2, 0x5

    const-string v3, "320x240_bestfit"

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/v/n;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/v/n;->f:Ldbxyzptlk/db231222/v/n;

    .line 1329
    new-instance v0, Ldbxyzptlk/db231222/v/n;

    const-string v1, "BESTFIT_480x320"

    const/4 v2, 0x6

    const-string v3, "480x320_bestfit"

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/v/n;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/v/n;->g:Ldbxyzptlk/db231222/v/n;

    .line 1331
    new-instance v0, Ldbxyzptlk/db231222/v/n;

    const-string v1, "BESTFIT_640x480"

    const/4 v2, 0x7

    const-string v3, "640x480_bestfit"

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/v/n;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/v/n;->h:Ldbxyzptlk/db231222/v/n;

    .line 1333
    new-instance v0, Ldbxyzptlk/db231222/v/n;

    const-string v1, "BESTFIT_960x640"

    const/16 v2, 0x8

    const-string v3, "960x640_bestfit"

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/v/n;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/v/n;->i:Ldbxyzptlk/db231222/v/n;

    .line 1335
    new-instance v0, Ldbxyzptlk/db231222/v/n;

    const-string v1, "BESTFIT_1024x768"

    const/16 v2, 0x9

    const-string v3, "1024x768_bestfit"

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/v/n;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/v/n;->j:Ldbxyzptlk/db231222/v/n;

    .line 1306
    const/16 v0, 0xa

    new-array v0, v0, [Ldbxyzptlk/db231222/v/n;

    sget-object v1, Ldbxyzptlk/db231222/v/n;->a:Ldbxyzptlk/db231222/v/n;

    aput-object v1, v0, v4

    sget-object v1, Ldbxyzptlk/db231222/v/n;->b:Ldbxyzptlk/db231222/v/n;

    aput-object v1, v0, v5

    sget-object v1, Ldbxyzptlk/db231222/v/n;->c:Ldbxyzptlk/db231222/v/n;

    aput-object v1, v0, v6

    sget-object v1, Ldbxyzptlk/db231222/v/n;->d:Ldbxyzptlk/db231222/v/n;

    aput-object v1, v0, v7

    sget-object v1, Ldbxyzptlk/db231222/v/n;->e:Ldbxyzptlk/db231222/v/n;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Ldbxyzptlk/db231222/v/n;->f:Ldbxyzptlk/db231222/v/n;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Ldbxyzptlk/db231222/v/n;->g:Ldbxyzptlk/db231222/v/n;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Ldbxyzptlk/db231222/v/n;->h:Ldbxyzptlk/db231222/v/n;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Ldbxyzptlk/db231222/v/n;->i:Ldbxyzptlk/db231222/v/n;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Ldbxyzptlk/db231222/v/n;->j:Ldbxyzptlk/db231222/v/n;

    aput-object v2, v0, v1

    sput-object v0, Ldbxyzptlk/db231222/v/n;->l:[Ldbxyzptlk/db231222/v/n;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1339
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1340
    iput-object p3, p0, Ldbxyzptlk/db231222/v/n;->k:Ljava/lang/String;

    .line 1341
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldbxyzptlk/db231222/v/n;
    .locals 1

    .prologue
    .line 1306
    const-class v0, Ldbxyzptlk/db231222/v/n;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/v/n;

    return-object v0
.end method

.method public static values()[Ldbxyzptlk/db231222/v/n;
    .locals 1

    .prologue
    .line 1306
    sget-object v0, Ldbxyzptlk/db231222/v/n;->l:[Ldbxyzptlk/db231222/v/n;

    invoke-virtual {v0}, [Ldbxyzptlk/db231222/v/n;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/v/n;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1344
    iget-object v0, p0, Ldbxyzptlk/db231222/v/n;->k:Ljava/lang/String;

    return-object v0
.end method
