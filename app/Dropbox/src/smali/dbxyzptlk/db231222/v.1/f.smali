.class public final Ldbxyzptlk/db231222/v/f;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MD:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Z

.field public final b:Ljava/lang/String;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/v/d",
            "<TMD;>;>;"
        }
    .end annotation
.end field

.field public final d:Z


# direct methods
.method public constructor <init>(ZLjava/util/List;Ljava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/v/d",
            "<TMD;>;>;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2484
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2485
    iput-boolean p1, p0, Ldbxyzptlk/db231222/v/f;->a:Z

    .line 2486
    iput-object p2, p0, Ldbxyzptlk/db231222/v/f;->c:Ljava/util/List;

    .line 2487
    iput-object p3, p0, Ldbxyzptlk/db231222/v/f;->b:Ljava/lang/String;

    .line 2488
    iput-boolean p4, p0, Ldbxyzptlk/db231222/v/f;->d:Z

    .line 2489
    return-void
.end method

.method public static a(Ldbxyzptlk/db231222/x/k;Ldbxyzptlk/db231222/x/c;)Ldbxyzptlk/db231222/v/f;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<MD:",
            "Ljava/lang/Object;",
            ">(",
            "Ldbxyzptlk/db231222/x/k;",
            "Ldbxyzptlk/db231222/x/c",
            "<TMD;>;)",
            "Ldbxyzptlk/db231222/v/f",
            "<TMD;>;"
        }
    .end annotation

    .prologue
    .line 2492
    invoke-virtual {p0}, Ldbxyzptlk/db231222/x/k;->b()Ldbxyzptlk/db231222/x/g;

    move-result-object v0

    .line 2493
    const-string v1, "reset"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/x/k;->i()Z

    move-result v1

    .line 2494
    const-string v2, "cursor"

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/x/k;->g()Ljava/lang/String;

    move-result-object v2

    .line 2495
    const-string v3, "has_more"

    invoke-virtual {v0, v3}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/x/k;->i()Z

    move-result v3

    .line 2496
    const-string v4, "entries"

    invoke-virtual {v0, v4}, Ldbxyzptlk/db231222/x/g;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/x/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/x/k;->c()Ldbxyzptlk/db231222/x/d;

    move-result-object v0

    new-instance v4, Ldbxyzptlk/db231222/v/e;

    invoke-direct {v4, p1}, Ldbxyzptlk/db231222/v/e;-><init>(Ldbxyzptlk/db231222/x/c;)V

    invoke-virtual {v0, v4}, Ldbxyzptlk/db231222/x/d;->a(Ldbxyzptlk/db231222/x/c;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2498
    new-instance v4, Ldbxyzptlk/db231222/v/f;

    invoke-direct {v4, v1, v0, v2, v3}, Ldbxyzptlk/db231222/v/f;-><init>(ZLjava/util/List;Ljava/lang/String;Z)V

    return-object v4
.end method
