.class public final Ldbxyzptlk/db231222/v/g;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:Ljava/lang/String;

.field private b:J

.field private c:Ljava/lang/String;

.field private d:Ldbxyzptlk/db231222/v/j;


# direct methods
.method private constructor <init>(Lorg/apache/http/HttpResponse;)V
    .locals 5

    .prologue
    const-wide/16 v3, -0x1

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 532
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 525
    iput-object v0, p0, Ldbxyzptlk/db231222/v/g;->a:Ljava/lang/String;

    .line 526
    iput-wide v3, p0, Ldbxyzptlk/db231222/v/g;->b:J

    .line 527
    iput-object v0, p0, Ldbxyzptlk/db231222/v/g;->c:Ljava/lang/String;

    .line 528
    iput-object v0, p0, Ldbxyzptlk/db231222/v/g;->d:Ldbxyzptlk/db231222/v/j;

    .line 533
    invoke-static {p1}, Ldbxyzptlk/db231222/v/g;->a(Lorg/apache/http/HttpResponse;)Ldbxyzptlk/db231222/v/j;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/v/g;->d:Ldbxyzptlk/db231222/v/j;

    .line 534
    iget-object v0, p0, Ldbxyzptlk/db231222/v/g;->d:Ldbxyzptlk/db231222/v/j;

    if-nez v0, :cond_0

    .line 535
    new-instance v0, Ldbxyzptlk/db231222/w/f;

    const-string v1, "Error parsing metadata."

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/w/f;-><init>(Ljava/lang/String;)V

    throw v0

    .line 538
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/v/g;->d:Ldbxyzptlk/db231222/v/j;

    invoke-static {p1, v0}, Ldbxyzptlk/db231222/v/g;->a(Lorg/apache/http/HttpResponse;Ldbxyzptlk/db231222/v/j;)J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/v/g;->b:J

    .line 539
    iget-wide v0, p0, Ldbxyzptlk/db231222/v/g;->b:J

    cmp-long v0, v0, v3

    if-nez v0, :cond_1

    .line 540
    new-instance v0, Ldbxyzptlk/db231222/w/f;

    const-string v1, "Error determining file size."

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/w/f;-><init>(Ljava/lang/String;)V

    throw v0

    .line 544
    :cond_1
    const-string v0, "Content-Type"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 545
    if-eqz v0, :cond_3

    .line 546
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 547
    if-eqz v0, :cond_3

    .line 548
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 549
    array-length v1, v0

    if-lez v1, :cond_2

    .line 550
    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Ldbxyzptlk/db231222/v/g;->a:Ljava/lang/String;

    .line 552
    :cond_2
    array-length v1, v0

    if-le v1, v2, :cond_3

    .line 553
    aget-object v0, v0, v2

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 554
    array-length v1, v0

    if-le v1, v2, :cond_3

    .line 555
    aget-object v0, v0, v2

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/v/g;->c:Ljava/lang/String;

    .line 560
    :cond_3
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/http/HttpResponse;Ldbxyzptlk/db231222/v/b;)V
    .locals 0

    .prologue
    .line 523
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/v/g;-><init>(Lorg/apache/http/HttpResponse;)V

    return-void
.end method

.method private static a(Lorg/apache/http/HttpResponse;Ldbxyzptlk/db231222/v/j;)J
    .locals 4

    .prologue
    .line 606
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v0

    .line 607
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    .line 616
    :goto_0
    return-wide v0

    .line 612
    :cond_0
    if-eqz p1, :cond_1

    .line 613
    iget-wide v0, p1, Ldbxyzptlk/db231222/v/j;->a:J

    goto :goto_0

    .line 616
    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private static a(Lorg/apache/http/HttpResponse;)Ldbxyzptlk/db231222/v/j;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 571
    if-nez p0, :cond_0

    move-object v0, v1

    .line 589
    :goto_0
    return-object v0

    .line 575
    :cond_0
    const-string v0, "X-Dropbox-Metadata"

    invoke-interface {p0, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 577
    if-nez v0, :cond_1

    move-object v0, v1

    .line 578
    goto :goto_0

    .line 582
    :cond_1
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 583
    invoke-static {v0}, Ldbxyzptlk/db231222/aj/d;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 584
    if-nez v0, :cond_2

    move-object v0, v1

    .line 585
    goto :goto_0

    .line 588
    :cond_2
    check-cast v0, Ljava/util/Map;

    .line 589
    new-instance v1, Ldbxyzptlk/db231222/v/j;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/v/j;-><init>(Ljava/util/Map;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 624
    iget-object v0, p0, Ldbxyzptlk/db231222/v/g;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 639
    iget-wide v0, p0, Ldbxyzptlk/db231222/v/g;->b:J

    return-wide v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 647
    iget-object v0, p0, Ldbxyzptlk/db231222/v/g;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ldbxyzptlk/db231222/v/j;
    .locals 1

    .prologue
    .line 654
    iget-object v0, p0, Ldbxyzptlk/db231222/v/g;->d:Ldbxyzptlk/db231222/v/j;

    return-object v0
.end method
