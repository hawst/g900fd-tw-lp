.class public final Ldbxyzptlk/db231222/v/j;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final r:Ldbxyzptlk/db231222/x/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/x/c",
            "<",
            "Ldbxyzptlk/db231222/v/j;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Z

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Z

.field public m:Z

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/v/j;",
            ">;"
        }
    .end annotation
.end field

.field public o:Z

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 388
    new-instance v0, Ldbxyzptlk/db231222/v/k;

    invoke-direct {v0}, Ldbxyzptlk/db231222/v/k;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/v/j;->r:Ldbxyzptlk/db231222/x/c;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 365
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331
    const-string v0, "bytes"

    invoke-static {p1, v0}, Ldbxyzptlk/db231222/v/a;->b(Ljava/util/Map;Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/v/j;->a:J

    .line 332
    const-string v0, "hash"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/db231222/v/j;->b:Ljava/lang/String;

    .line 333
    const-string v0, "icon"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/db231222/v/j;->c:Ljava/lang/String;

    .line 334
    const-string v0, "is_dir"

    invoke-static {p1, v0}, Ldbxyzptlk/db231222/v/a;->a(Ljava/util/Map;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/v/j;->d:Z

    .line 335
    const-string v0, "modified"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/db231222/v/j;->e:Ljava/lang/String;

    .line 336
    const-string v0, "client_mtime"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/db231222/v/j;->f:Ljava/lang/String;

    .line 337
    const-string v0, "path"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/db231222/v/j;->g:Ljava/lang/String;

    .line 338
    const-string v0, "root"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/db231222/v/j;->h:Ljava/lang/String;

    .line 339
    const-string v0, "size"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/db231222/v/j;->i:Ljava/lang/String;

    .line 340
    const-string v0, "mime_type"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/db231222/v/j;->j:Ljava/lang/String;

    .line 341
    const-string v0, "rev"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/db231222/v/j;->k:Ljava/lang/String;

    .line 342
    const-string v0, "thumb_exists"

    invoke-static {p1, v0}, Ldbxyzptlk/db231222/v/a;->a(Ljava/util/Map;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/v/j;->l:Z

    .line 343
    const-string v0, "is_deleted"

    invoke-static {p1, v0}, Ldbxyzptlk/db231222/v/a;->a(Ljava/util/Map;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/v/j;->m:Z

    .line 344
    const-string v0, "shareable"

    invoke-static {p1, v0}, Ldbxyzptlk/db231222/v/a;->a(Ljava/util/Map;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/v/j;->o:Z

    .line 345
    const-string v0, "shared_folder_id"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/db231222/v/j;->p:Ljava/lang/String;

    .line 346
    const-string v0, "parent_shared_folder_id"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/db231222/v/j;->q:Ljava/lang/String;

    .line 348
    const-string v0, "contents"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 349
    if-eqz v0, :cond_1

    instance-of v1, v0, Ldbxyzptlk/db231222/aj/a;

    if-eqz v1, :cond_1

    .line 350
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldbxyzptlk/db231222/v/j;->n:Ljava/util/List;

    .line 352
    check-cast v0, Ldbxyzptlk/db231222/aj/a;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/aj/a;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 353
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 354
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 355
    instance-of v2, v0, Ljava/util/Map;

    if-eqz v2, :cond_0

    .line 356
    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->n:Ljava/util/List;

    new-instance v3, Ldbxyzptlk/db231222/v/j;

    check-cast v0, Ljava/util/Map;

    invoke-direct {v3, v0}, Ldbxyzptlk/db231222/v/j;-><init>(Ljava/util/Map;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 360
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/v/j;->n:Ljava/util/List;

    .line 362
    :cond_2
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 372
    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->g:Ljava/lang/String;

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 373
    iget-object v1, p0, Ldbxyzptlk/db231222/v/j;->g:Ljava/lang/String;

    add-int/lit8 v0, v0, 0x1

    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->g:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 380
    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->g:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    const-string v0, ""

    .line 384
    :goto_0
    return-object v0

    .line 383
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->g:Ljava/lang/String;

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 384
    iget-object v1, p0, Ldbxyzptlk/db231222/v/j;->g:Ljava/lang/String;

    const/4 v2, 0x0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 423
    if-ne p0, p1, :cond_1

    .line 489
    :cond_0
    :goto_0
    return v0

    .line 425
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 426
    goto :goto_0

    .line 427
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 428
    goto :goto_0

    .line 429
    :cond_3
    check-cast p1, Ldbxyzptlk/db231222/v/j;

    .line 430
    iget-wide v2, p0, Ldbxyzptlk/db231222/v/j;->a:J

    iget-wide v4, p1, Ldbxyzptlk/db231222/v/j;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    .line 431
    goto :goto_0

    .line 432
    :cond_4
    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->f:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 433
    iget-object v2, p1, Ldbxyzptlk/db231222/v/j;->f:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 434
    goto :goto_0

    .line 435
    :cond_5
    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->f:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/db231222/v/j;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 436
    goto :goto_0

    .line 437
    :cond_6
    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 438
    iget-object v2, p1, Ldbxyzptlk/db231222/v/j;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 439
    goto :goto_0

    .line 440
    :cond_7
    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->b:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/db231222/v/j;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 441
    goto :goto_0

    .line 442
    :cond_8
    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->c:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 443
    iget-object v2, p1, Ldbxyzptlk/db231222/v/j;->c:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    .line 444
    goto :goto_0

    .line 445
    :cond_9
    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->c:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/db231222/v/j;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 446
    goto :goto_0

    .line 447
    :cond_a
    iget-boolean v2, p0, Ldbxyzptlk/db231222/v/j;->m:Z

    iget-boolean v3, p1, Ldbxyzptlk/db231222/v/j;->m:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 448
    goto :goto_0

    .line 449
    :cond_b
    iget-boolean v2, p0, Ldbxyzptlk/db231222/v/j;->d:Z

    iget-boolean v3, p1, Ldbxyzptlk/db231222/v/j;->d:Z

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 450
    goto :goto_0

    .line 451
    :cond_c
    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->j:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 452
    iget-object v2, p1, Ldbxyzptlk/db231222/v/j;->j:Ljava/lang/String;

    if-eqz v2, :cond_e

    move v0, v1

    .line 453
    goto :goto_0

    .line 454
    :cond_d
    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->j:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/db231222/v/j;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 455
    goto/16 :goto_0

    .line 456
    :cond_e
    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->e:Ljava/lang/String;

    if-nez v2, :cond_f

    .line 457
    iget-object v2, p1, Ldbxyzptlk/db231222/v/j;->e:Ljava/lang/String;

    if-eqz v2, :cond_10

    move v0, v1

    .line 458
    goto/16 :goto_0

    .line 459
    :cond_f
    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->e:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/db231222/v/j;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    move v0, v1

    .line 460
    goto/16 :goto_0

    .line 461
    :cond_10
    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->g:Ljava/lang/String;

    if-nez v2, :cond_11

    .line 462
    iget-object v2, p1, Ldbxyzptlk/db231222/v/j;->g:Ljava/lang/String;

    if-eqz v2, :cond_12

    move v0, v1

    .line 463
    goto/16 :goto_0

    .line 464
    :cond_11
    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->g:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/db231222/v/j;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    move v0, v1

    .line 465
    goto/16 :goto_0

    .line 466
    :cond_12
    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->k:Ljava/lang/String;

    if-nez v2, :cond_13

    .line 467
    iget-object v2, p1, Ldbxyzptlk/db231222/v/j;->k:Ljava/lang/String;

    if-eqz v2, :cond_14

    move v0, v1

    .line 468
    goto/16 :goto_0

    .line 469
    :cond_13
    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->k:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/db231222/v/j;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    move v0, v1

    .line 470
    goto/16 :goto_0

    .line 471
    :cond_14
    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->h:Ljava/lang/String;

    if-nez v2, :cond_15

    .line 472
    iget-object v2, p1, Ldbxyzptlk/db231222/v/j;->h:Ljava/lang/String;

    if-eqz v2, :cond_16

    move v0, v1

    .line 473
    goto/16 :goto_0

    .line 474
    :cond_15
    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->h:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/db231222/v/j;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    move v0, v1

    .line 475
    goto/16 :goto_0

    .line 476
    :cond_16
    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->i:Ljava/lang/String;

    if-nez v2, :cond_17

    .line 477
    iget-object v2, p1, Ldbxyzptlk/db231222/v/j;->i:Ljava/lang/String;

    if-eqz v2, :cond_1b

    move v0, v1

    .line 478
    goto/16 :goto_0

    .line 479
    :cond_17
    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->i:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/db231222/v/j;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    move v0, v1

    .line 480
    goto/16 :goto_0

    .line 481
    :cond_18
    iget-boolean v2, p0, Ldbxyzptlk/db231222/v/j;->o:Z

    iget-boolean v3, p1, Ldbxyzptlk/db231222/v/j;->o:Z

    if-eq v2, v3, :cond_19

    move v0, v1

    .line 482
    goto/16 :goto_0

    .line 483
    :cond_19
    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->p:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/db231222/v/j;->p:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    move v0, v1

    .line 484
    goto/16 :goto_0

    .line 485
    :cond_1a
    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->q:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/db231222/v/j;->q:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    move v0, v1

    .line 486
    goto/16 :goto_0

    .line 487
    :cond_1b
    iget-boolean v2, p0, Ldbxyzptlk/db231222/v/j;->l:Z

    iget-boolean v3, p1, Ldbxyzptlk/db231222/v/j;->l:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 488
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 8

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    .line 397
    .line 399
    iget-wide v4, p0, Ldbxyzptlk/db231222/v/j;->a:J

    iget-wide v6, p0, Ldbxyzptlk/db231222/v/j;->a:J

    const/16 v0, 0x20

    ushr-long/2addr v6, v0

    xor-long/2addr v4, v6

    long-to-int v0, v4

    add-int/lit8 v0, v0, 0x1f

    .line 400
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    .line 402
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    .line 403
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    .line 404
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Ldbxyzptlk/db231222/v/j;->m:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_3
    add-int/2addr v0, v4

    .line 405
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Ldbxyzptlk/db231222/v/j;->d:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_4
    add-int/2addr v0, v4

    .line 406
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->j:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v4

    .line 408
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->e:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v4

    .line 410
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->g:Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v4

    .line 411
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->k:Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v4

    .line 412
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->h:Ljava/lang/String;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    add-int/2addr v0, v4

    .line 413
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->i:Ljava/lang/String;

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    add-int/2addr v0, v4

    .line 414
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Ldbxyzptlk/db231222/v/j;->l:Z

    if-eqz v0, :cond_b

    move v0, v2

    :goto_b
    add-int/2addr v0, v4

    .line 415
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Ldbxyzptlk/db231222/v/j;->o:Z

    if-eqz v4, :cond_c

    :goto_c
    add-int/2addr v0, v2

    .line 416
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->p:Ljava/lang/String;

    if-nez v0, :cond_d

    move v0, v1

    :goto_d
    add-int/2addr v0, v2

    .line 417
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ldbxyzptlk/db231222/v/j;->q:Ljava/lang/String;

    if-nez v2, :cond_e

    :goto_e
    add-int/2addr v0, v1

    .line 418
    return v0

    .line 400
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 402
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 403
    :cond_2
    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    move v0, v3

    .line 404
    goto :goto_3

    :cond_4
    move v0, v3

    .line 405
    goto :goto_4

    .line 406
    :cond_5
    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5

    .line 408
    :cond_6
    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_6

    .line 410
    :cond_7
    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_7

    .line 411
    :cond_8
    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_8

    .line 412
    :cond_9
    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_9

    .line 413
    :cond_a
    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_a

    :cond_b
    move v0, v3

    .line 414
    goto :goto_b

    :cond_c
    move v2, v3

    .line 415
    goto :goto_c

    .line 416
    :cond_d
    iget-object v0, p0, Ldbxyzptlk/db231222/v/j;->p:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_d

    .line 417
    :cond_e
    iget-object v1, p0, Ldbxyzptlk/db231222/v/j;->q:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_e
.end method
