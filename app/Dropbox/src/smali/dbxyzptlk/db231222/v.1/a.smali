.class public final Ldbxyzptlk/db231222/v/a;
.super Ljava/lang/Object;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<SESS_T::",
        "Ldbxyzptlk/db231222/y/n;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field protected final b:Ldbxyzptlk/db231222/y/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TSESS_T;"
        }
    .end annotation
.end field

.field protected final c:Ldbxyzptlk/db231222/v/r;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    invoke-static {}, Ldbxyzptlk/db231222/v/x;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/v/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/y/n;Ldbxyzptlk/db231222/v/r;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TSESS_T;",
            "Ldbxyzptlk/db231222/v/r;",
            ")V"
        }
    .end annotation

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    if-nez p1, :cond_0

    .line 106
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Session must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_0
    iput-object p1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    .line 109
    iput-object p2, p0, Ldbxyzptlk/db231222/v/a;->c:Ldbxyzptlk/db231222/v/r;

    .line 110
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/db231222/v/q;)Ldbxyzptlk/db231222/v/h;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1482
    invoke-virtual {p0}, Ldbxyzptlk/db231222/v/a;->b()V

    .line 1484
    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1485
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1488
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/files/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v1}, Ldbxyzptlk/db231222/y/n;->g()Ldbxyzptlk/db231222/y/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1489
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "rev"

    aput-object v3, v1, v2

    aput-object p2, v1, v4

    const/4 v2, 0x2

    const-string v3, "locale"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v3}, Ldbxyzptlk/db231222/y/n;->h()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1493
    iget-object v2, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v2}, Ldbxyzptlk/db231222/y/n;->c()Ljava/lang/String;

    move-result-object v2

    const-string v3, "r6"

    invoke-static {v2, v3, v0, v1}, Ldbxyzptlk/db231222/v/v;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1495
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 1496
    iget-object v0, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v0, v1}, Ldbxyzptlk/db231222/y/n;->a(Lorg/apache/http/HttpRequest;)V

    .line 1498
    invoke-static {p3, v1}, Ldbxyzptlk/db231222/v/p;->a(Ldbxyzptlk/db231222/v/q;Lorg/apache/http/client/methods/HttpUriRequest;)V

    .line 1499
    invoke-static {p3}, Ldbxyzptlk/db231222/v/p;->a(Ldbxyzptlk/db231222/v/q;)V

    .line 1500
    iget-object v0, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/v/v;->a(Ldbxyzptlk/db231222/y/n;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 1501
    invoke-static {p3, v0, v4}, Ldbxyzptlk/db231222/v/p;->a(Ldbxyzptlk/db231222/v/q;Lorg/apache/http/HttpResponse;Z)V

    .line 1503
    new-instance v2, Ldbxyzptlk/db231222/v/h;

    invoke-direct {v2, v1, v0}, Ldbxyzptlk/db231222/v/h;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    return-object v2
.end method

.method private a(Ljava/lang/String;Ljava/io/InputStream;JZLjava/lang/String;Ldbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/v/o;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2334
    if-eqz p1, :cond_0

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2335
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "path is null or empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2338
    :cond_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/v/a;->b()V

    .line 2340
    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2341
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 2344
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/files_put/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v1}, Ldbxyzptlk/db231222/y/n;->g()Ldbxyzptlk/db231222/y/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2346
    if-nez p6, :cond_3

    .line 2347
    const-string p6, ""

    .line 2350
    :cond_3
    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "overwrite"

    aput-object v2, v1, v4

    const/4 v2, 0x1

    invoke-static {p5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "parent_rev"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object p6, v1, v2

    const/4 v2, 0x4

    const-string v3, "locale"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v3}, Ldbxyzptlk/db231222/y/n;->h()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 2356
    iget-object v2, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v2}, Ldbxyzptlk/db231222/y/n;->c()Ljava/lang/String;

    move-result-object v2

    const-string v3, "r6"

    invoke-static {v2, v3, v0, v1}, Ldbxyzptlk/db231222/v/v;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2359
    new-instance v2, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v2, v0}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    .line 2360
    iget-object v0, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v0, v2}, Ldbxyzptlk/db231222/y/n;->a(Lorg/apache/http/HttpRequest;)V

    .line 2362
    new-instance v1, Lorg/apache/http/entity/InputStreamEntity;

    invoke-direct {v1, p2, p3, p4}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    .line 2363
    const-string v0, "application/octet-stream"

    invoke-virtual {v1, v0}, Lorg/apache/http/entity/InputStreamEntity;->setContentEncoding(Ljava/lang/String;)V

    .line 2364
    invoke-virtual {v1, v4}, Lorg/apache/http/entity/InputStreamEntity;->setChunked(Z)V

    .line 2368
    if-eqz p7, :cond_4

    .line 2369
    new-instance v0, Ldbxyzptlk/db231222/v/t;

    invoke-direct {v0, v1, p7}, Ldbxyzptlk/db231222/v/t;-><init>(Lorg/apache/http/HttpEntity;Ldbxyzptlk/db231222/v/s;)V

    .line 2372
    :goto_0
    invoke-virtual {v2, v0}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 2374
    new-instance v0, Ldbxyzptlk/db231222/v/c;

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-direct {v0, v2, v1}, Ldbxyzptlk/db231222/v/c;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Ldbxyzptlk/db231222/y/n;)V

    return-object v0

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/util/Map;Ljava/lang/String;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 2285
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2286
    if-eqz v0, :cond_0

    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 2287
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2289
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/util/Map;Ljava/lang/String;)J
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")J"
        }
    .end annotation

    .prologue
    .line 2701
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2702
    const-wide/16 v1, 0x0

    .line 2703
    if-eqz v0, :cond_1

    .line 2704
    instance-of v3, v0, Ljava/lang/Number;

    if-eqz v3, :cond_0

    .line 2705
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    .line 2712
    :goto_0
    return-wide v0

    .line 2706
    :cond_0
    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 2709
    check-cast v0, Ljava/lang/String;

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    goto :goto_0

    :cond_1
    move-wide v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/io/OutputStream;Ldbxyzptlk/db231222/v/n;Ldbxyzptlk/db231222/v/m;Ldbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/v/g;
    .locals 6

    .prologue
    .line 1731
    iget-object v0, p0, Ldbxyzptlk/db231222/v/a;->c:Ldbxyzptlk/db231222/v/r;

    invoke-interface {v0}, Ldbxyzptlk/db231222/v/r;->a()Ldbxyzptlk/db231222/v/q;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    .line 1732
    invoke-virtual/range {v0 .. v5}, Ldbxyzptlk/db231222/v/a;->a(Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;Ldbxyzptlk/db231222/v/m;Ldbxyzptlk/db231222/v/q;)Ldbxyzptlk/db231222/v/h;

    move-result-object v0

    .line 1733
    invoke-static {v0, p3, p6, v5}, Ldbxyzptlk/db231222/v/h;->a(Ldbxyzptlk/db231222/v/h;Ljava/io/OutputStream;Ldbxyzptlk/db231222/v/s;Ldbxyzptlk/db231222/v/q;)V

    .line 1734
    invoke-static {v5}, Ldbxyzptlk/db231222/v/p;->b(Ldbxyzptlk/db231222/v/q;)V

    .line 1735
    invoke-virtual {v0}, Ldbxyzptlk/db231222/v/h;->a()Ldbxyzptlk/db231222/v/g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/io/OutputStream;Ldbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/v/g;
    .locals 2

    .prologue
    .line 1443
    iget-object v0, p0, Ldbxyzptlk/db231222/v/a;->c:Ldbxyzptlk/db231222/v/r;

    invoke-interface {v0}, Ldbxyzptlk/db231222/v/r;->a()Ldbxyzptlk/db231222/v/q;

    move-result-object v0

    .line 1444
    invoke-direct {p0, p1, p2, v0}, Ldbxyzptlk/db231222/v/a;->a(Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/db231222/v/q;)Ldbxyzptlk/db231222/v/h;

    move-result-object v1

    .line 1445
    invoke-static {v1, p3, p4, v0}, Ldbxyzptlk/db231222/v/h;->a(Ldbxyzptlk/db231222/v/h;Ljava/io/OutputStream;Ldbxyzptlk/db231222/v/s;Ldbxyzptlk/db231222/v/q;)V

    .line 1446
    invoke-static {v0}, Ldbxyzptlk/db231222/v/p;->b(Ldbxyzptlk/db231222/v/q;)V

    .line 1447
    invoke-virtual {v1}, Ldbxyzptlk/db231222/v/h;->a()Ldbxyzptlk/db231222/v/g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/db231222/v/n;Ldbxyzptlk/db231222/v/m;Ldbxyzptlk/db231222/v/q;)Ldbxyzptlk/db231222/v/h;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1785
    invoke-virtual {p0}, Ldbxyzptlk/db231222/v/a;->b()V

    .line 1787
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/thumbnails/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v1}, Ldbxyzptlk/db231222/y/n;->g()Ldbxyzptlk/db231222/y/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1788
    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "size"

    aput-object v3, v1, v2

    invoke-virtual {p3}, Ldbxyzptlk/db231222/v/n;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string v3, "format"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p4}, Ldbxyzptlk/db231222/v/m;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "rev"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    aput-object p2, v1, v2

    const/4 v2, 0x6

    const-string v3, "locale"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v3}, Ldbxyzptlk/db231222/y/n;->h()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1794
    iget-object v2, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v2}, Ldbxyzptlk/db231222/y/n;->c()Ljava/lang/String;

    move-result-object v2

    const-string v3, "r6"

    invoke-static {v2, v3, v0, v1}, Ldbxyzptlk/db231222/v/v;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1796
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 1797
    iget-object v0, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v0, v1}, Ldbxyzptlk/db231222/y/n;->a(Lorg/apache/http/HttpRequest;)V

    .line 1799
    invoke-static {p5, v1}, Ldbxyzptlk/db231222/v/p;->a(Ldbxyzptlk/db231222/v/q;Lorg/apache/http/client/methods/HttpUriRequest;)V

    .line 1800
    invoke-static {p5}, Ldbxyzptlk/db231222/v/p;->a(Ldbxyzptlk/db231222/v/q;)V

    .line 1801
    iget-object v0, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/v/v;->a(Ldbxyzptlk/db231222/y/n;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 1802
    invoke-static {p5, v0, v4}, Ldbxyzptlk/db231222/v/p;->a(Ldbxyzptlk/db231222/v/q;Lorg/apache/http/HttpResponse;Z)V

    .line 1804
    new-instance v2, Ldbxyzptlk/db231222/v/h;

    invoke-direct {v2, v1, v0}, Ldbxyzptlk/db231222/v/h;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    return-object v2
.end method

.method public final a(Ljava/lang/String;Z)Ldbxyzptlk/db231222/v/i;
    .locals 7

    .prologue
    .line 2217
    invoke-virtual {p0}, Ldbxyzptlk/db231222/v/a;->b()V

    .line 2218
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/media/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v1}, Ldbxyzptlk/db231222/y/n;->g()Ldbxyzptlk/db231222/y/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2221
    sget-object v0, Ldbxyzptlk/db231222/v/w;->a:Ldbxyzptlk/db231222/v/w;

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v1}, Ldbxyzptlk/db231222/y/n;->b()Ljava/lang/String;

    move-result-object v1

    const-string v3, "r6"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "locale"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v6}, Ldbxyzptlk/db231222/y/n;->h()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    iget-object v5, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/db231222/v/v;->a(Ldbxyzptlk/db231222/v/w;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/db231222/y/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 2227
    new-instance v1, Ldbxyzptlk/db231222/v/i;

    const/4 v2, 0x0

    invoke-direct {v1, v0, p2, v2}, Ldbxyzptlk/db231222/v/i;-><init>(Ljava/util/Map;ZLdbxyzptlk/db231222/v/b;)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;)Ldbxyzptlk/db231222/v/j;
    .locals 6

    .prologue
    .line 2106
    invoke-virtual {p0}, Ldbxyzptlk/db231222/v/a;->b()V

    .line 2108
    const/4 v0, 0x6

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "root"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v1}, Ldbxyzptlk/db231222/y/n;->g()Ldbxyzptlk/db231222/y/o;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/y/o;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "path"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    aput-object p1, v4, v0

    const/4 v0, 0x4

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v1}, Ldbxyzptlk/db231222/y/n;->h()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 2113
    sget-object v0, Ldbxyzptlk/db231222/v/w;->b:Ldbxyzptlk/db231222/v/w;

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v1}, Ldbxyzptlk/db231222/y/n;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/fileops/create_folder"

    const-string v3, "r6"

    iget-object v5, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/db231222/v/v;->a(Ldbxyzptlk/db231222/v/w;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/db231222/y/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 2117
    new-instance v1, Ldbxyzptlk/db231222/v/j;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/v/j;-><init>(Ljava/util/Map;)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;)Ldbxyzptlk/db231222/v/j;
    .locals 6

    .prologue
    .line 1871
    invoke-virtual {p0}, Ldbxyzptlk/db231222/v/a;->b()V

    .line 1873
    if-gtz p2, :cond_0

    .line 1874
    const/16 p2, 0x61a8

    .line 1877
    :cond_0
    const/16 v0, 0xa

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "file_limit"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "hash"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    aput-object p3, v4, v0

    const/4 v0, 0x4

    const-string v1, "list"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    invoke-static {p4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x6

    const-string v1, "rev"

    aput-object v1, v4, v0

    const/4 v0, 0x7

    aput-object p5, v4, v0

    const/16 v0, 0x8

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/16 v0, 0x9

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v1}, Ldbxyzptlk/db231222/y/n;->h()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 1885
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/metadata/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v1}, Ldbxyzptlk/db231222/y/n;->g()Ldbxyzptlk/db231222/y/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1888
    sget-object v0, Ldbxyzptlk/db231222/v/w;->a:Ldbxyzptlk/db231222/v/w;

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v1}, Ldbxyzptlk/db231222/y/n;->b()Ljava/lang/String;

    move-result-object v1

    const-string v3, "r6"

    iget-object v5, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/db231222/v/v;->a(Ldbxyzptlk/db231222/v/w;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/db231222/y/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 1892
    new-instance v1, Ldbxyzptlk/db231222/v/j;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/v/j;-><init>(Ljava/util/Map;)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/db231222/v/j;
    .locals 6

    .prologue
    .line 2029
    invoke-virtual {p0}, Ldbxyzptlk/db231222/v/a;->b()V

    .line 2031
    const/16 v0, 0x8

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "root"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v1}, Ldbxyzptlk/db231222/y/n;->g()Ldbxyzptlk/db231222/y/o;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/y/o;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "from_path"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    aput-object p1, v4, v0

    const/4 v0, 0x4

    const-string v1, "to_path"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    aput-object p2, v4, v0

    const/4 v0, 0x6

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x7

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v1}, Ldbxyzptlk/db231222/y/n;->h()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 2037
    sget-object v0, Ldbxyzptlk/db231222/v/w;->b:Ldbxyzptlk/db231222/v/w;

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v1}, Ldbxyzptlk/db231222/y/n;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/fileops/move"

    const-string v3, "r6"

    iget-object v5, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/db231222/v/v;->a(Ldbxyzptlk/db231222/v/w;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/db231222/y/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 2042
    new-instance v1, Ldbxyzptlk/db231222/v/j;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/v/j;-><init>(Ljava/util/Map;)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;Ljava/io/InputStream;JLdbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/v/o;
    .locals 8

    .prologue
    .line 1693
    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Ldbxyzptlk/db231222/v/a;->a(Ljava/lang/String;Ljava/io/InputStream;JZLjava/lang/String;Ldbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/v/o;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/io/InputStream;JLjava/lang/String;Ldbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/v/o;
    .locals 8

    .prologue
    .line 1620
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Ldbxyzptlk/db231222/v/a;->a(Ljava/lang/String;Ljava/io/InputStream;JZLjava/lang/String;Ldbxyzptlk/db231222/v/s;)Ldbxyzptlk/db231222/v/o;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ldbxyzptlk/db231222/y/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TSESS_T;"
        }
    .end annotation

    .prologue
    .line 1359
    iget-object v0, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;IZ)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IZ)",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/v/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1972
    invoke-virtual {p0}, Ldbxyzptlk/db231222/v/a;->b()V

    .line 1974
    if-gtz p3, :cond_0

    .line 1975
    const/16 p3, 0x2710

    .line 1978
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/search/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v1}, Ldbxyzptlk/db231222/y/n;->g()Ldbxyzptlk/db231222/y/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1980
    const/16 v0, 0x8

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "query"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    aput-object p2, v4, v0

    const/4 v0, 0x2

    const-string v1, "file_limit"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string v1, "include_deleted"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    invoke-static {p4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x6

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x7

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v1}, Ldbxyzptlk/db231222/y/n;->h()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 1987
    sget-object v0, Ldbxyzptlk/db231222/v/w;->a:Ldbxyzptlk/db231222/v/w;

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v1}, Ldbxyzptlk/db231222/y/n;->b()Ljava/lang/String;

    move-result-object v1

    const-string v3, "r6"

    iget-object v5, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/db231222/v/v;->a(Ldbxyzptlk/db231222/v/w;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/db231222/y/n;)Ljava/lang/Object;

    move-result-object v0

    .line 1990
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1991
    instance-of v2, v0, Ldbxyzptlk/db231222/aj/a;

    if-eqz v2, :cond_2

    .line 1992
    check-cast v0, Ldbxyzptlk/db231222/aj/a;

    .line 1993
    invoke-virtual {v0}, Ldbxyzptlk/db231222/aj/a;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 1994
    instance-of v3, v0, Ljava/util/Map;

    if-eqz v3, :cond_1

    .line 1996
    new-instance v3, Ldbxyzptlk/db231222/v/j;

    check-cast v0, Ljava/util/Map;

    invoke-direct {v3, v0}, Ldbxyzptlk/db231222/v/j;-><init>(Ljava/util/Map;)V

    .line 1997
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2002
    :cond_2
    return-object v1
.end method

.method protected final b()V
    .locals 2

    .prologue
    .line 2686
    iget-object v0, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v0}, Ldbxyzptlk/db231222/y/n;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2687
    new-instance v0, Ldbxyzptlk/db231222/w/j;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/w/j;-><init>(Lorg/apache/http/HttpResponse;)V

    throw v0

    .line 2689
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2141
    invoke-virtual {p0}, Ldbxyzptlk/db231222/v/a;->b()V

    .line 2143
    const/4 v0, 0x6

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "root"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v1}, Ldbxyzptlk/db231222/y/n;->g()Ldbxyzptlk/db231222/y/o;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/y/o;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "path"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    aput-object p1, v4, v0

    const/4 v0, 0x4

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v1}, Ldbxyzptlk/db231222/y/n;->h()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 2147
    sget-object v0, Ldbxyzptlk/db231222/v/w;->b:Ldbxyzptlk/db231222/v/w;

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v1}, Ldbxyzptlk/db231222/y/n;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/fileops/delete"

    const-string v3, "r6"

    iget-object v5, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/db231222/v/v;->a(Ldbxyzptlk/db231222/v/w;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/db231222/y/n;)Ljava/lang/Object;

    .line 2149
    return-void
.end method

.method public final c(Ljava/lang/String;)Ldbxyzptlk/db231222/v/i;
    .locals 7

    .prologue
    .line 2251
    invoke-virtual {p0}, Ldbxyzptlk/db231222/v/a;->b()V

    .line 2253
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/shares/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v1}, Ldbxyzptlk/db231222/y/n;->g()Ldbxyzptlk/db231222/y/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2256
    sget-object v0, Ldbxyzptlk/db231222/v/w;->a:Ldbxyzptlk/db231222/v/w;

    iget-object v1, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v1}, Ldbxyzptlk/db231222/y/n;->b()Ljava/lang/String;

    move-result-object v1

    const-string v3, "r6"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "locale"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-interface {v6}, Ldbxyzptlk/db231222/y/n;->h()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "short_url"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "false"

    aput-object v6, v4, v5

    iget-object v5, p0, Ldbxyzptlk/db231222/v/a;->b:Ldbxyzptlk/db231222/y/n;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/db231222/v/v;->a(Ldbxyzptlk/db231222/v/w;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/db231222/y/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 2265
    const-string v1, "url"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2266
    const-string v2, "expires"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ldbxyzptlk/db231222/v/v;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    .line 2268
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 2269
    :cond_0
    new-instance v0, Ldbxyzptlk/db231222/w/f;

    const-string v1, "Could not parse share response."

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/w/f;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2272
    :cond_1
    new-instance v1, Ldbxyzptlk/db231222/v/i;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Ldbxyzptlk/db231222/v/i;-><init>(Ljava/util/Map;Ldbxyzptlk/db231222/v/b;)V

    return-object v1
.end method
