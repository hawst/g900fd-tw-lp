.class public final Ldbxyzptlk/db231222/v/h;
.super Ljava/io/FilterInputStream;
.source "panda.py"


# instance fields
.field private final a:Lorg/apache/http/client/methods/HttpUriRequest;

.field private final b:Ldbxyzptlk/db231222/v/g;


# direct methods
.method public constructor <init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 672
    invoke-direct {p0, v1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 674
    invoke-interface {p2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 675
    if-nez v0, :cond_0

    .line 676
    new-instance v0, Ldbxyzptlk/db231222/w/a;

    const-string v1, "Didn\'t get entity from HttpResponse"

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/w/a;-><init>(Ljava/lang/String;)V

    throw v0

    .line 682
    :cond_0
    :try_start_0
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/v/h;->in:Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 687
    iput-object p1, p0, Ldbxyzptlk/db231222/v/h;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 688
    new-instance v0, Ldbxyzptlk/db231222/v/g;

    invoke-direct {v0, p2, v1}, Ldbxyzptlk/db231222/v/g;-><init>(Lorg/apache/http/HttpResponse;Ldbxyzptlk/db231222/v/b;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/v/h;->b:Ldbxyzptlk/db231222/v/g;

    .line 689
    return-void

    .line 683
    :catch_0
    move-exception v0

    .line 684
    new-instance v1, Ldbxyzptlk/db231222/w/d;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/w/d;-><init>(Ljava/io/IOException;)V

    throw v1
.end method

.method static synthetic a(Ldbxyzptlk/db231222/v/h;Ljava/io/OutputStream;Ldbxyzptlk/db231222/v/s;Ldbxyzptlk/db231222/v/q;)V
    .locals 0

    .prologue
    .line 663
    invoke-direct {p0, p1, p2, p3}, Ldbxyzptlk/db231222/v/h;->a(Ljava/io/OutputStream;Ldbxyzptlk/db231222/v/s;Ldbxyzptlk/db231222/v/q;)V

    return-void
.end method

.method private a(Ljava/io/OutputStream;Ldbxyzptlk/db231222/v/s;Ldbxyzptlk/db231222/v/q;)V
    .locals 17

    .prologue
    .line 745
    const/4 v4, 0x0

    .line 746
    const-wide/16 v2, 0x0

    .line 747
    const-wide/16 v5, 0x0

    .line 748
    move-object/from16 v0, p0

    iget-object v1, v0, Ldbxyzptlk/db231222/v/h;->b:Ldbxyzptlk/db231222/v/g;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/v/g;->b()J

    move-result-wide v8

    .line 751
    :try_start_0
    new-instance v7, Ljava/io/BufferedOutputStream;

    move-object/from16 v0, p1

    invoke-direct {v7, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 753
    const/16 v1, 0x1000

    :try_start_1
    new-array v10, v1, [B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-wide v15, v5

    move-wide v5, v2

    move-wide v3, v15

    .line 756
    :cond_0
    :goto_0
    :try_start_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Ldbxyzptlk/db231222/v/h;->read([B)I

    move-result v1

    .line 757
    if-gez v1, :cond_7

    .line 758
    const-wide/16 v1, 0x0

    cmp-long v1, v8, v1

    if-ltz v1, :cond_3

    cmp-long v1, v5, v8

    if-gez v1, :cond_3

    .line 760
    new-instance v1, Ldbxyzptlk/db231222/w/g;

    invoke-direct {v1, v5, v6}, Ldbxyzptlk/db231222/w/g;-><init>(J)V

    throw v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 791
    :catch_0
    move-exception v1

    move-wide v2, v5

    move-object v4, v7

    .line 792
    :goto_1
    :try_start_3
    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 793
    if-eqz v1, :cond_8

    const-string v5, "No space"

    invoke-virtual {v1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 796
    new-instance v1, Ldbxyzptlk/db231222/w/e;

    invoke-direct {v1}, Ldbxyzptlk/db231222/w/e;-><init>()V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 807
    :catchall_0
    move-exception v1

    move-object v7, v4

    :goto_2
    if-eqz v7, :cond_1

    .line 809
    :try_start_4
    invoke-virtual {v7}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 812
    :cond_1
    :goto_3
    if-eqz p1, :cond_2

    .line 814
    :try_start_5
    invoke-virtual/range {p1 .. p1}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    .line 820
    :cond_2
    :goto_4
    :try_start_6
    invoke-virtual/range {p0 .. p0}, Ldbxyzptlk/db231222/v/h;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    .line 821
    :goto_5
    throw v1

    .line 763
    :cond_3
    :try_start_7
    move-object/from16 v0, p3

    invoke-static {v0, v8, v9}, Ldbxyzptlk/db231222/v/p;->b(Ldbxyzptlk/db231222/v/q;J)V

    .line 781
    invoke-virtual {v7}, Ljava/io/BufferedOutputStream;->flush()V

    .line 782
    invoke-virtual/range {p1 .. p1}, Ljava/io/OutputStream;->flush()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 785
    :try_start_8
    move-object/from16 v0, p1

    instance-of v1, v0, Ljava/io/FileOutputStream;

    if-eqz v1, :cond_4

    .line 786
    move-object/from16 v0, p1

    check-cast v0, Ljava/io/FileOutputStream;

    move-object v1, v0

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/FileDescriptor;->sync()V
    :try_end_8
    .catch Ljava/io/SyncFailedException; {:try_start_8 .. :try_end_8} :catch_9
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 807
    :cond_4
    :goto_6
    if-eqz v7, :cond_5

    .line 809
    :try_start_9
    invoke-virtual {v7}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1

    .line 812
    :cond_5
    :goto_7
    if-eqz p1, :cond_6

    .line 814
    :try_start_a
    invoke-virtual/range {p1 .. p1}, Ljava/io/OutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    .line 820
    :cond_6
    :goto_8
    :try_start_b
    invoke-virtual/range {p0 .. p0}, Ldbxyzptlk/db231222/v/h;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3

    .line 823
    :goto_9
    return-void

    .line 767
    :cond_7
    int-to-long v11, v1

    :try_start_c
    move-object/from16 v0, p3

    invoke-static {v0, v11, v12}, Ldbxyzptlk/db231222/v/p;->a(Ldbxyzptlk/db231222/v/q;J)V

    .line 768
    const/4 v2, 0x0

    invoke-virtual {v7, v10, v2, v1}, Ljava/io/BufferedOutputStream;->write([BII)V

    .line 770
    int-to-long v1, v1

    add-long/2addr v5, v1

    .line 772
    if-eqz p2, :cond_0

    .line 773
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 774
    sub-long v11, v1, v3

    invoke-virtual/range {p2 .. p2}, Ldbxyzptlk/db231222/v/s;->a()J

    move-result-wide v13

    cmp-long v11, v11, v13

    if-lez v11, :cond_9

    .line 776
    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v6, v8, v9}, Ldbxyzptlk/db231222/v/s;->a(JJ)V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :goto_a
    move-wide v3, v1

    .line 778
    goto/16 :goto_0

    .line 804
    :cond_8
    :try_start_d
    new-instance v1, Ldbxyzptlk/db231222/w/g;

    invoke-direct {v1, v2, v3}, Ldbxyzptlk/db231222/w/g;-><init>(J)V

    throw v1
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 810
    :catch_1
    move-exception v1

    goto :goto_7

    .line 815
    :catch_2
    move-exception v1

    goto :goto_8

    .line 821
    :catch_3
    move-exception v1

    goto :goto_9

    .line 810
    :catch_4
    move-exception v2

    goto :goto_3

    .line 815
    :catch_5
    move-exception v2

    goto :goto_4

    .line 821
    :catch_6
    move-exception v2

    goto :goto_5

    .line 807
    :catchall_1
    move-exception v1

    move-object v7, v4

    goto :goto_2

    :catchall_2
    move-exception v1

    goto :goto_2

    .line 791
    :catch_7
    move-exception v1

    goto/16 :goto_1

    :catch_8
    move-exception v1

    move-object v4, v7

    goto/16 :goto_1

    .line 788
    :catch_9
    move-exception v1

    goto :goto_6

    :cond_9
    move-wide v1, v3

    goto :goto_a
.end method


# virtual methods
.method public final a()Ldbxyzptlk/db231222/v/g;
    .locals 1

    .prologue
    .line 709
    iget-object v0, p0, Ldbxyzptlk/db231222/v/h;->b:Ldbxyzptlk/db231222/v/g;

    return-object v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 702
    iget-object v0, p0, Ldbxyzptlk/db231222/v/h;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 703
    return-void
.end method
