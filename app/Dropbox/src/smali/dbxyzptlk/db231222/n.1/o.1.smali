.class final Ldbxyzptlk/db231222/n/o;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/n/x;


# instance fields
.field private final a:Ldbxyzptlk/db231222/n/k;

.field private final b:[Ljava/lang/String;

.field private final c:Ldbxyzptlk/db231222/n/p;


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/n/k;Ldbxyzptlk/db231222/n/p;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 289
    iput-object p1, p0, Ldbxyzptlk/db231222/n/o;->a:Ldbxyzptlk/db231222/n/k;

    .line 290
    iput-object p3, p0, Ldbxyzptlk/db231222/n/o;->b:[Ljava/lang/String;

    .line 291
    iput-object p2, p0, Ldbxyzptlk/db231222/n/o;->c:Ldbxyzptlk/db231222/n/p;

    .line 292
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 296
    iget-object v0, p0, Ldbxyzptlk/db231222/n/o;->c:Ldbxyzptlk/db231222/n/p;

    sget-object v1, Ldbxyzptlk/db231222/n/p;->a:Ldbxyzptlk/db231222/n/p;

    if-ne v0, v1, :cond_1

    .line 297
    iget-object v0, p0, Ldbxyzptlk/db231222/n/o;->a:Ldbxyzptlk/db231222/n/k;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/k;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 300
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/n/o;->c:Ldbxyzptlk/db231222/n/p;

    sget-object v1, Ldbxyzptlk/db231222/n/p;->b:Ldbxyzptlk/db231222/n/p;

    if-ne v0, v1, :cond_2

    .line 301
    iget-object v0, p0, Ldbxyzptlk/db231222/n/o;->a:Ldbxyzptlk/db231222/n/k;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/k;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 306
    :cond_2
    iget-object v0, p0, Ldbxyzptlk/db231222/n/o;->a:Ldbxyzptlk/db231222/n/k;

    invoke-static {v0}, Ldbxyzptlk/db231222/n/k;->a(Ldbxyzptlk/db231222/n/k;)Ldbxyzptlk/db231222/n/a;

    move-result-object v0

    .line 307
    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/a;->d()Ljava/util/Map;

    move-result-object v2

    .line 309
    new-instance v3, Landroid/content/ContentValues;

    const/4 v0, 0x2

    invoke-direct {v3, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 310
    iget-object v4, p0, Ldbxyzptlk/db231222/n/o;->b:[Ljava/lang/String;

    array-length v5, v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_4

    aget-object v6, v4, v1

    .line 312
    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 313
    if-nez v0, :cond_3

    .line 310
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 318
    :cond_3
    const-string v7, "pref_name"

    invoke-virtual {v3, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    const-string v6, "pref_value"

    invoke-virtual {v3, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    const-string v0, "DropboxAccountPrefs"

    const/4 v6, 0x0

    invoke-virtual {p1, v0, v6, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 321
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    goto :goto_2

    .line 324
    :cond_4
    iget-object v0, p0, Ldbxyzptlk/db231222/n/o;->c:Ldbxyzptlk/db231222/n/p;

    sget-object v1, Ldbxyzptlk/db231222/n/p;->a:Ldbxyzptlk/db231222/n/p;

    if-ne v0, v1, :cond_5

    .line 325
    iget-object v0, p0, Ldbxyzptlk/db231222/n/o;->a:Ldbxyzptlk/db231222/n/k;

    invoke-virtual {v0, v8}, Ldbxyzptlk/db231222/n/k;->b(Z)V

    goto :goto_0

    .line 326
    :cond_5
    iget-object v0, p0, Ldbxyzptlk/db231222/n/o;->c:Ldbxyzptlk/db231222/n/p;

    sget-object v1, Ldbxyzptlk/db231222/n/p;->b:Ldbxyzptlk/db231222/n/p;

    if-ne v0, v1, :cond_0

    .line 327
    iget-object v0, p0, Ldbxyzptlk/db231222/n/o;->a:Ldbxyzptlk/db231222/n/k;

    invoke-virtual {v0, v8}, Ldbxyzptlk/db231222/n/k;->c(Z)V

    goto :goto_0
.end method
