.class final Ldbxyzptlk/db231222/n/E;
.super Ldbxyzptlk/db231222/n/B;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Enum",
        "<TT;>;>",
        "Ldbxyzptlk/db231222/n/B;"
    }
.end annotation


# instance fields
.field private final b:Ljava/lang/Enum;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/n/I;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 221
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/n/B;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;)V

    .line 222
    iput-object p4, p0, Ldbxyzptlk/db231222/n/E;->b:Ljava/lang/Enum;

    .line 223
    iput-object p3, p0, Ldbxyzptlk/db231222/n/E;->c:Ljava/lang/Class;

    .line 224
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Enum;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 235
    invoke-virtual {p0}, Ldbxyzptlk/db231222/n/E;->c()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/n/E;->a:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 236
    return-void
.end method

.method public final d()Ljava/lang/Enum;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 227
    invoke-virtual {p0}, Ldbxyzptlk/db231222/n/E;->c()Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/n/E;->a:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 228
    if-nez v0, :cond_0

    .line 229
    iget-object v0, p0, Ldbxyzptlk/db231222/n/E;->b:Ljava/lang/Enum;

    .line 231
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Ldbxyzptlk/db231222/n/E;->c:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    goto :goto_0
.end method
