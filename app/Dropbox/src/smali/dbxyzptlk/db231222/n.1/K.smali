.class public final Ldbxyzptlk/db231222/n/K;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static e:Ldbxyzptlk/db231222/n/K;

.field private static final n:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ldbxyzptlk/db231222/n/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:Ldbxyzptlk/db231222/n/x;

.field private final f:Ldbxyzptlk/db231222/n/I;

.field private final g:Ldbxyzptlk/db231222/n/E;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/n/E",
            "<",
            "Ldbxyzptlk/db231222/n/r;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ldbxyzptlk/db231222/n/D;

.field private final i:Ldbxyzptlk/db231222/n/F;

.field private final j:Ldbxyzptlk/db231222/n/G;

.field private final k:Ldbxyzptlk/db231222/n/G;

.field private final l:Ldbxyzptlk/db231222/n/H;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/n/H",
            "<",
            "Ldbxyzptlk/db231222/o/a;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Ldbxyzptlk/db231222/n/J;

.field private final o:Ldbxyzptlk/db231222/n/y;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 123
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "SORT_ORDER"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "EARLY_RELEASES_ENABLED"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "EARLY_RELEASES_DISABLED_AT_VERSION"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "LAST_REPORT_HOST_TIME"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "LAST_UPDATE_PROMPT_TIME"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "SHARE_COUNT_BY_ACTIVITY"

    aput-object v2, v0, v1

    sput-object v0, Ldbxyzptlk/db231222/n/K;->n:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Ldbxyzptlk/db231222/n/k;)V
    .locals 9

    .prologue
    const-wide/16 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/K;->a:Ljava/util/concurrent/atomic/AtomicReference;

    .line 98
    new-instance v0, Ldbxyzptlk/db231222/n/M;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/n/M;-><init>(Ldbxyzptlk/db231222/n/K;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/K;->f:Ldbxyzptlk/db231222/n/I;

    .line 108
    new-instance v0, Ldbxyzptlk/db231222/n/E;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/K;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "SORT_ORDER"

    const-class v3, Ldbxyzptlk/db231222/n/r;

    sget-object v4, Ldbxyzptlk/db231222/n/r;->a:Ldbxyzptlk/db231222/n/r;

    invoke-direct {v0, v1, v2, v3, v4}, Ldbxyzptlk/db231222/n/E;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/K;->g:Ldbxyzptlk/db231222/n/E;

    .line 112
    new-instance v0, Ldbxyzptlk/db231222/n/D;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/K;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "EARLY_RELEASES_ENABLED"

    invoke-direct {v0, v1, v2, v5}, Ldbxyzptlk/db231222/n/D;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Z)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/K;->h:Ldbxyzptlk/db231222/n/D;

    .line 113
    new-instance v0, Ldbxyzptlk/db231222/n/F;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/K;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "EARLY_RELEASES_DISABLED_AT_VERSION"

    invoke-direct {v0, v1, v2, v5}, Ldbxyzptlk/db231222/n/F;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;I)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/K;->i:Ldbxyzptlk/db231222/n/F;

    .line 114
    new-instance v0, Ldbxyzptlk/db231222/n/G;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/K;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "LAST_REPORT_HOST_TIME"

    invoke-direct {v0, v1, v2, v7, v8}, Ldbxyzptlk/db231222/n/G;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;J)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/K;->j:Ldbxyzptlk/db231222/n/G;

    .line 115
    new-instance v0, Ldbxyzptlk/db231222/n/G;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/K;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "LAST_UPDATE_PROMPT_TIME"

    invoke-direct {v0, v1, v2, v7, v8}, Ldbxyzptlk/db231222/n/G;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;J)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/K;->k:Ldbxyzptlk/db231222/n/G;

    .line 117
    new-instance v0, Ldbxyzptlk/db231222/n/H;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/K;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "SHARE_COUNT_BY_ACTIVITY"

    invoke-static {}, Ldbxyzptlk/db231222/o/a;->a()Ldbxyzptlk/db231222/o/a;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/n/H;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ldbxyzptlk/db231222/H/t;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/K;->l:Ldbxyzptlk/db231222/n/H;

    .line 120
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/K;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "SENT_REFERRAL_EMAILS"

    invoke-direct {v0, v1, v2, v6}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/K;->m:Ldbxyzptlk/db231222/n/J;

    .line 181
    new-instance v0, Ldbxyzptlk/db231222/n/y;

    invoke-direct {v0}, Ldbxyzptlk/db231222/n/y;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/K;->o:Ldbxyzptlk/db231222/n/y;

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/n/K;->b:Landroid/content/Context;

    .line 53
    if-eqz p3, :cond_0

    .line 54
    sget-object v0, Ldbxyzptlk/db231222/n/K;->n:[Ljava/lang/String;

    invoke-virtual {p3, v0}, Ldbxyzptlk/db231222/n/k;->b([Ljava/lang/String;)Ldbxyzptlk/db231222/n/x;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/n/K;->d:Ldbxyzptlk/db231222/n/x;

    .line 58
    :goto_0
    iput-object p2, p0, Ldbxyzptlk/db231222/n/K;->c:Ljava/lang/String;

    .line 59
    return-void

    .line 56
    :cond_0
    iput-object v6, p0, Ldbxyzptlk/db231222/n/K;->d:Ldbxyzptlk/db231222/n/x;

    goto :goto_0
.end method

.method public static declared-synchronized a()Ldbxyzptlk/db231222/n/K;
    .locals 2

    .prologue
    .line 79
    const-class v1, Ldbxyzptlk/db231222/n/K;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ldbxyzptlk/db231222/n/K;->e:Ldbxyzptlk/db231222/n/K;

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 82
    :cond_0
    :try_start_1
    sget-object v0, Ldbxyzptlk/db231222/n/K;->e:Ldbxyzptlk/db231222/n/K;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;Ldbxyzptlk/db231222/n/k;)Ldbxyzptlk/db231222/n/K;
    .locals 3

    .prologue
    .line 62
    const-class v1, Ldbxyzptlk/db231222/n/K;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ldbxyzptlk/db231222/n/K;->e:Ldbxyzptlk/db231222/n/K;

    if-nez v0, :cond_0

    .line 63
    new-instance v0, Ldbxyzptlk/db231222/n/K;

    const-string v2, "prefs-shared.db"

    invoke-direct {v0, p0, v2, p1}, Ldbxyzptlk/db231222/n/K;-><init>(Landroid/content/Context;Ljava/lang/String;Ldbxyzptlk/db231222/n/k;)V

    sput-object v0, Ldbxyzptlk/db231222/n/K;->e:Ldbxyzptlk/db231222/n/K;

    .line 64
    sget-object v0, Ldbxyzptlk/db231222/n/K;->e:Ldbxyzptlk/db231222/n/K;

    invoke-direct {v0}, Ldbxyzptlk/db231222/n/K;->i()V

    .line 65
    sget-object v0, Ldbxyzptlk/db231222/n/K;->e:Ldbxyzptlk/db231222/n/K;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 67
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/n/K;)Ldbxyzptlk/db231222/n/a;
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ldbxyzptlk/db231222/n/K;->j()Ldbxyzptlk/db231222/n/a;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 1

    .prologue
    .line 90
    new-instance v0, Ldbxyzptlk/db231222/n/L;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/n/L;-><init>(Ldbxyzptlk/db231222/n/K;)V

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/L;->start()V

    .line 96
    return-void
.end method

.method private j()Ldbxyzptlk/db231222/n/a;
    .locals 6

    .prologue
    .line 130
    iget-object v0, p0, Ldbxyzptlk/db231222/n/K;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/n/a;

    .line 131
    if-nez v0, :cond_1

    .line 132
    iget-object v1, p0, Ldbxyzptlk/db231222/n/K;->a:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v1

    .line 133
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/n/K;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/n/a;

    .line 134
    if-nez v0, :cond_0

    .line 135
    new-instance v2, Ldbxyzptlk/db231222/n/v;

    iget-object v0, p0, Ldbxyzptlk/db231222/n/K;->b:Landroid/content/Context;

    iget-object v3, p0, Ldbxyzptlk/db231222/n/K;->c:Ljava/lang/String;

    sget-object v4, Ldbxyzptlk/db231222/n/w;->a:Ldbxyzptlk/db231222/n/w;

    iget-object v5, p0, Ldbxyzptlk/db231222/n/K;->d:Ldbxyzptlk/db231222/n/x;

    invoke-direct {v2, v0, v3, v4, v5}, Ldbxyzptlk/db231222/n/v;-><init>(Landroid/content/Context;Ljava/lang/String;Ldbxyzptlk/db231222/n/w;Ldbxyzptlk/db231222/n/x;)V

    .line 136
    new-instance v0, Ldbxyzptlk/db231222/n/a;

    invoke-direct {v0, v2}, Ldbxyzptlk/db231222/n/a;-><init>(Ldbxyzptlk/db231222/n/v;)V

    .line 137
    iget-object v2, p0, Ldbxyzptlk/db231222/n/K;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 139
    :cond_0
    monitor-exit v1

    .line 142
    :cond_1
    return-object v0

    .line 139
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Ldbxyzptlk/db231222/n/K;->i:Ldbxyzptlk/db231222/n/F;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/F;->a(I)V

    .line 179
    return-void
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Ldbxyzptlk/db231222/n/K;->j:Ldbxyzptlk/db231222/n/G;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/n/G;->a(J)V

    .line 197
    iget-object v0, p0, Ldbxyzptlk/db231222/n/K;->o:Ldbxyzptlk/db231222/n/y;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/y;->a()V

    .line 198
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/n/r;)V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Ldbxyzptlk/db231222/n/K;->g:Ldbxyzptlk/db231222/n/E;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/E;->a(Ljava/lang/Enum;)V

    .line 163
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/n/z;)V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Ldbxyzptlk/db231222/n/K;->o:Ldbxyzptlk/db231222/n/y;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/y;->a(Ldbxyzptlk/db231222/n/z;)Z

    .line 185
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/o/a;)V
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Ldbxyzptlk/db231222/n/K;->l:Ldbxyzptlk/db231222/n/H;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/H;->a(Ldbxyzptlk/db231222/H/t;)V

    .line 215
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 238
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 239
    const-string v1, "emails"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    iget-object v1, p0, Ldbxyzptlk/db231222/n/K;->m:Ldbxyzptlk/db231222/n/J;

    invoke-static {v0}, Ldbxyzptlk/db231222/aj/c;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/n/J;->a(Ljava/lang/String;)V

    .line 241
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Ldbxyzptlk/db231222/n/K;->h:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/D;->a(Z)V

    .line 171
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 146
    invoke-direct {p0}, Ldbxyzptlk/db231222/n/K;->j()Ldbxyzptlk/db231222/n/a;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/a;->c()Ldbxyzptlk/db231222/n/h;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/h;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 147
    return-void
.end method

.method public final b(J)V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Ldbxyzptlk/db231222/n/K;->k:Ldbxyzptlk/db231222/n/G;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/n/G;->a(J)V

    .line 206
    return-void
.end method

.method public final b(Ldbxyzptlk/db231222/n/z;)V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Ldbxyzptlk/db231222/n/K;->o:Ldbxyzptlk/db231222/n/y;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/y;->b(Ldbxyzptlk/db231222/n/z;)Z

    .line 189
    return-void
.end method

.method public final c()Ldbxyzptlk/db231222/n/r;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Ldbxyzptlk/db231222/n/K;->g:Ldbxyzptlk/db231222/n/E;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/E;->d()Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/n/r;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Ldbxyzptlk/db231222/n/K;->h:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/D;->d()Z

    move-result v0

    return v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Ldbxyzptlk/db231222/n/K;->j:Ldbxyzptlk/db231222/n/G;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/G;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Ldbxyzptlk/db231222/n/K;->k:Ldbxyzptlk/db231222/n/G;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/G;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public final g()Ldbxyzptlk/db231222/o/a;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Ldbxyzptlk/db231222/n/K;->l:Ldbxyzptlk/db231222/n/H;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/H;->d()Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/o/a;

    return-object v0
.end method

.method public final h()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218
    iget-object v0, p0, Ldbxyzptlk/db231222/n/K;->m:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v0

    .line 219
    if-nez v0, :cond_0

    .line 220
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 233
    :goto_0
    return-object v0

    .line 224
    :cond_0
    :try_start_0
    new-instance v1, Ldbxyzptlk/db231222/ak/b;

    invoke-direct {v1}, Ldbxyzptlk/db231222/ak/b;-><init>()V

    .line 226
    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/ak/b;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 227
    const-string v1, "emails"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 228
    new-instance v1, Ljava/util/ArrayList;

    const-string v2, "emails"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v0, v1

    goto :goto_0

    .line 230
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ldbxyzptlk/db231222/ak/c; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 232
    :catch_0
    move-exception v0

    .line 233
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method
