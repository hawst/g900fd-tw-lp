.class public Ldbxyzptlk/db231222/n/k;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static d:Ldbxyzptlk/db231222/n/k;


# instance fields
.field private final A:Ldbxyzptlk/db231222/n/D;

.field private final b:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ldbxyzptlk/db231222/n/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/Context;

.field private final e:Ljava/lang/String;

.field private final f:Ldbxyzptlk/db231222/n/I;

.field private final g:Ldbxyzptlk/db231222/n/D;

.field private final h:Ldbxyzptlk/db231222/n/J;

.field private final i:Ldbxyzptlk/db231222/n/F;

.field private final j:Ldbxyzptlk/db231222/n/J;

.field private final k:Ldbxyzptlk/db231222/n/J;

.field private final l:Ldbxyzptlk/db231222/n/G;

.field private final m:Ldbxyzptlk/db231222/n/J;

.field private final n:Ldbxyzptlk/db231222/n/J;

.field private final o:Ldbxyzptlk/db231222/n/J;

.field private final p:Ldbxyzptlk/db231222/n/G;

.field private final q:Ldbxyzptlk/db231222/n/J;

.field private final r:Ldbxyzptlk/db231222/n/J;

.field private final s:Ldbxyzptlk/db231222/n/D;

.field private final t:Ldbxyzptlk/db231222/n/J;

.field private final u:Ldbxyzptlk/db231222/n/J;

.field private final v:Ldbxyzptlk/db231222/n/J;

.field private final w:Ldbxyzptlk/db231222/n/J;

.field private final x:Ldbxyzptlk/db231222/n/D;

.field private y:Ldbxyzptlk/db231222/n/H;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/n/H",
            "<",
            "Ldbxyzptlk/db231222/t/e;",
            ">;"
        }
    .end annotation
.end field

.field private final z:Ldbxyzptlk/db231222/n/D;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Ldbxyzptlk/db231222/n/k;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/n/k;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->b:Ljava/util/concurrent/atomic/AtomicReference;

    .line 211
    new-instance v0, Ldbxyzptlk/db231222/n/m;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/n/m;-><init>(Ldbxyzptlk/db231222/n/k;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    .line 342
    new-instance v0, Ldbxyzptlk/db231222/n/D;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "SEEN_INTRO_TOUR"

    invoke-direct {v0, v1, v2, v5}, Ldbxyzptlk/db231222/n/D;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Z)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->g:Ldbxyzptlk/db231222/n/D;

    .line 348
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "UPDATE_NAG_VERSION"

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->h:Ldbxyzptlk/db231222/n/J;

    .line 349
    new-instance v0, Ldbxyzptlk/db231222/n/F;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "UPDATE_NAG_TIMES"

    invoke-direct {v0, v1, v2, v5}, Ldbxyzptlk/db231222/n/F;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;I)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->i:Ldbxyzptlk/db231222/n/F;

    .line 351
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "DEVICE_UDID"

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->j:Ldbxyzptlk/db231222/n/J;

    .line 352
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "INSTALL_TYPE"

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->k:Ldbxyzptlk/db231222/n/J;

    .line 354
    new-instance v0, Ldbxyzptlk/db231222/n/G;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "ANAL_NEXT_ROTATION"

    invoke-direct {v0, v1, v2, v6, v7}, Ldbxyzptlk/db231222/n/G;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;J)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->l:Ldbxyzptlk/db231222/n/G;

    .line 356
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "ANAL_LAST_USER_INFO"

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->m:Ldbxyzptlk/db231222/n/J;

    .line 358
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "TWOFACTOR_CHECKPOINT_TOKEN"

    invoke-direct {v0, v1, v2, v4}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->n:Ldbxyzptlk/db231222/n/J;

    .line 359
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "TWOFACTOR_DESCRIPTION"

    invoke-direct {v0, v1, v2, v4}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->o:Ldbxyzptlk/db231222/n/J;

    .line 360
    new-instance v0, Ldbxyzptlk/db231222/n/G;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "TWOFACTOR_CHECKPOINT_EXPIRE_TIME_MS"

    invoke-direct {v0, v1, v2, v6, v7}, Ldbxyzptlk/db231222/n/G;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;J)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->p:Ldbxyzptlk/db231222/n/G;

    .line 361
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "TWOFACTOR_DELIVERY_MODE"

    invoke-direct {v0, v1, v2, v4}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->q:Ldbxyzptlk/db231222/n/J;

    .line 362
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "TWOFACTOR_TEMP_USERNAME"

    invoke-direct {v0, v1, v2, v4}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->r:Ldbxyzptlk/db231222/n/J;

    .line 364
    new-instance v0, Ldbxyzptlk/db231222/n/D;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "DID_REPORT_HOST_SPECIAL"

    invoke-direct {v0, v1, v2, v5}, Ldbxyzptlk/db231222/n/D;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Z)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->s:Ldbxyzptlk/db231222/n/D;

    .line 366
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "SSO_REQUEST_TOKEN_KEY"

    invoke-direct {v0, v1, v2, v4}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->t:Ldbxyzptlk/db231222/n/J;

    .line 367
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "SSO_REQUEST_TOKEN_SECRET"

    invoke-direct {v0, v1, v2, v4}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->u:Ldbxyzptlk/db231222/n/J;

    .line 368
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "SSO_USERNAME"

    invoke-direct {v0, v1, v2, v4}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->v:Ldbxyzptlk/db231222/n/J;

    .line 369
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "SSO_AUTHORIZATION_URL"

    invoke-direct {v0, v1, v2, v4}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->w:Ldbxyzptlk/db231222/n/J;

    .line 371
    new-instance v0, Ldbxyzptlk/db231222/n/D;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "DID_LOG_MEDIA_COUNTS"

    invoke-direct {v0, v1, v2, v5}, Ldbxyzptlk/db231222/n/D;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Z)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->x:Ldbxyzptlk/db231222/n/D;

    .line 376
    new-instance v0, Ldbxyzptlk/db231222/n/H;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "GANDALF_FEATURES"

    sget-object v3, Ldbxyzptlk/db231222/t/e;->a:Ldbxyzptlk/db231222/H/w;

    invoke-direct {v0, v1, v2, v3, v4}, Ldbxyzptlk/db231222/n/H;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ldbxyzptlk/db231222/H/w;Ldbxyzptlk/db231222/H/t;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->y:Ldbxyzptlk/db231222/n/H;

    .line 380
    new-instance v0, Ldbxyzptlk/db231222/n/D;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "MIGRATED_OUT_IDENTITY"

    invoke-direct {v0, v1, v2, v5}, Ldbxyzptlk/db231222/n/D;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Z)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->z:Ldbxyzptlk/db231222/n/D;

    .line 381
    new-instance v0, Ldbxyzptlk/db231222/n/D;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    const-string v2, "MIGRATED_OUT_USER"

    invoke-direct {v0, v1, v2, v5}, Ldbxyzptlk/db231222/n/D;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Z)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->A:Ldbxyzptlk/db231222/n/D;

    .line 177
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/n/k;->c:Landroid/content/Context;

    .line 178
    iput-object p2, p0, Ldbxyzptlk/db231222/n/k;->e:Ljava/lang/String;

    .line 179
    return-void
.end method

.method static synthetic a(Ldbxyzptlk/db231222/n/k;)Ldbxyzptlk/db231222/n/a;
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ldbxyzptlk/db231222/n/k;->p()Ldbxyzptlk/db231222/n/a;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized a()Ldbxyzptlk/db231222/n/k;
    .locals 2

    .prologue
    .line 170
    const-class v1, Ldbxyzptlk/db231222/n/k;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ldbxyzptlk/db231222/n/k;->d:Ldbxyzptlk/db231222/n/k;

    if-nez v0, :cond_0

    .line 171
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 173
    :cond_0
    :try_start_1
    sget-object v0, Ldbxyzptlk/db231222/n/k;->d:Ldbxyzptlk/db231222/n/k;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Ldbxyzptlk/db231222/n/k;
    .locals 3

    .prologue
    .line 160
    const-class v1, Ldbxyzptlk/db231222/n/k;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ldbxyzptlk/db231222/n/k;->d:Ldbxyzptlk/db231222/n/k;

    if-nez v0, :cond_0

    .line 161
    new-instance v0, Ldbxyzptlk/db231222/n/k;

    const-string v2, "prefs.db"

    invoke-direct {v0, p0, v2}, Ldbxyzptlk/db231222/n/k;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/n/k;->d:Ldbxyzptlk/db231222/n/k;

    .line 162
    sget-object v0, Ldbxyzptlk/db231222/n/k;->d:Ldbxyzptlk/db231222/n/k;

    invoke-direct {v0}, Ldbxyzptlk/db231222/n/k;->q()V

    .line 163
    sget-object v0, Ldbxyzptlk/db231222/n/k;->d:Ldbxyzptlk/db231222/n/k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 165
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 160
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static i()V
    .locals 6

    .prologue
    .line 506
    invoke-static {}, Ldbxyzptlk/db231222/n/k;->a()Ldbxyzptlk/db231222/n/k;

    move-result-object v0

    .line 507
    invoke-static {}, Ldbxyzptlk/db231222/i/d;->b()Ldbxyzptlk/db231222/i/d;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/i/d;->a()Ldbxyzptlk/db231222/i/c;

    move-result-object v1

    iget-object v1, v1, Ldbxyzptlk/db231222/i/c;->a:Ljava/lang/String;

    .line 509
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->c()Lcom/dropbox/android/util/analytics/l;

    move-result-object v2

    .line 510
    const-string v3, "SEEN_INTRO_TOUR"

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/k;->f()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 511
    const-string v3, "UPDATE_NAG_TIMES"

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/k;->a(Ljava/lang/String;)I

    move-result v1

    int-to-long v4, v1

    invoke-virtual {v2, v3, v4, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    .line 512
    const-string v1, "DID_REPORT_HOST_SPECIAL"

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/k;->l()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 514
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v0

    .line 515
    if-eqz v0, :cond_3

    .line 516
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/k;->g()Ldbxyzptlk/db231222/n/K;

    move-result-object v1

    .line 517
    const-string v3, "EARLY_RELEASES_ENABLED"

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/K;->d()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 519
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/k;->e()Ldbxyzptlk/db231222/r/d;

    move-result-object v1

    .line 520
    if-eqz v1, :cond_0

    .line 521
    invoke-virtual {v1}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v1

    .line 522
    const-string v3, "CAMERA_UPLOAD_ENABLED"

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/P;->o()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 523
    const-string v3, "CAMERA_UPLOAD_INITIAL_SCAN"

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/P;->q()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 524
    const-string v3, "CAMERA_UPLOAD_HAS_UPLOADED_ONCE"

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/P;->p()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 525
    const-string v3, "CAMERA_UPLOAD_HASH_UPDATE"

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/P;->r()Ldbxyzptlk/db231222/n/q;

    move-result-object v4

    invoke-virtual {v4}, Ldbxyzptlk/db231222/n/q;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 526
    const-string v3, "CAMERA_UPLOAD_FIRST_MEDIA_SCAN"

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/P;->x()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 527
    const-string v3, "CAMERA_UPLOAD_USE_3G"

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/P;->y()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 528
    const-string v3, "CAMERA_UPLOAD_3G_LIMIT"

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/P;->z()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 529
    const-string v3, "CAMERA_UPLOAD_VIDEOS_ENABLED"

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/P;->A()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 530
    const-string v3, "CAMERA_UPLOAD_IGNORE_EXISTING"

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/P;->C()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 531
    const-string v3, "CAMERA_UPLOAD_SEEN_INTRO"

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/P;->D()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 534
    :cond_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/k;->f()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v0

    .line 535
    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->g()Ldbxyzptlk/db231222/s/a;

    move-result-object v1

    .line 536
    if-eqz v1, :cond_2

    .line 537
    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->n()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 538
    const-string v3, "COUNTRY"

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 540
    :cond_1
    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->s()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 541
    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/a;->t()Ldbxyzptlk/db231222/s/d;

    move-result-object v1

    .line 542
    const-string v3, "QUOTA_QUOTA"

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/d;->d()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    .line 543
    const-string v3, "QUOTA_NORMAL"

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/d;->h()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    .line 544
    const-string v3, "QUOTA_SHARED"

    invoke-virtual {v1}, Ldbxyzptlk/db231222/s/d;->j()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    .line 548
    :cond_2
    const-string v1, "NOTIFICATIONS_TO_MUTE"

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 549
    const-string v1, "NOTIFICATIONS_MUTED"

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/P;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    .line 552
    :cond_3
    invoke-virtual {v2}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 553
    return-void
.end method

.method static synthetic o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Ldbxyzptlk/db231222/n/k;->a:Ljava/lang/String;

    return-object v0
.end method

.method private p()Ldbxyzptlk/db231222/n/a;
    .locals 6

    .prologue
    .line 183
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/n/a;

    .line 184
    if-nez v0, :cond_1

    .line 185
    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->b:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v1

    .line 186
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/n/a;

    .line 187
    if-nez v0, :cond_0

    .line 188
    new-instance v0, Ldbxyzptlk/db231222/n/a;

    new-instance v2, Ldbxyzptlk/db231222/n/v;

    iget-object v3, p0, Ldbxyzptlk/db231222/n/k;->c:Landroid/content/Context;

    iget-object v4, p0, Ldbxyzptlk/db231222/n/k;->e:Ljava/lang/String;

    sget-object v5, Ldbxyzptlk/db231222/n/w;->b:Ldbxyzptlk/db231222/n/w;

    invoke-direct {v2, v3, v4, v5}, Ldbxyzptlk/db231222/n/v;-><init>(Landroid/content/Context;Ljava/lang/String;Ldbxyzptlk/db231222/n/w;)V

    invoke-direct {v0, v2}, Ldbxyzptlk/db231222/n/a;-><init>(Ldbxyzptlk/db231222/n/v;)V

    .line 189
    iget-object v2, p0, Ldbxyzptlk/db231222/n/k;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 191
    :cond_0
    monitor-exit v1

    .line 193
    :cond_1
    return-object v0

    .line 191
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private q()V
    .locals 1

    .prologue
    .line 201
    new-instance v0, Ldbxyzptlk/db231222/n/l;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/n/l;-><init>(Ldbxyzptlk/db231222/n/k;)V

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/l;->start()V

    .line 207
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->h:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v0

    .line 483
    if-eqz p1, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 484
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->i:Ldbxyzptlk/db231222/n/F;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/F;->d()I

    move-result v0

    .line 486
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final a([Ljava/lang/String;)Ldbxyzptlk/db231222/n/x;
    .locals 2

    .prologue
    .line 235
    new-instance v0, Ldbxyzptlk/db231222/n/o;

    sget-object v1, Ldbxyzptlk/db231222/n/p;->a:Ldbxyzptlk/db231222/n/p;

    invoke-direct {v0, p0, v1, p1}, Ldbxyzptlk/db231222/n/o;-><init>(Ldbxyzptlk/db231222/n/k;Ldbxyzptlk/db231222/n/p;[Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 567
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->l:Ldbxyzptlk/db231222/n/G;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/n/G;->a(J)V

    .line 568
    return-void
.end method

.method public final a(Lcom/dropbox/android/filemanager/h;)V
    .locals 3

    .prologue
    .line 634
    new-instance v0, Ldbxyzptlk/db231222/n/C;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/n/C;-><init>(Ldbxyzptlk/db231222/n/I;)V

    .line 635
    if-eqz p1, :cond_0

    .line 636
    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->t:Ldbxyzptlk/db231222/n/J;

    iget-object v2, p1, Lcom/dropbox/android/filemanager/h;->b:Ldbxyzptlk/db231222/y/m;

    iget-object v2, v2, Ldbxyzptlk/db231222/y/m;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/db231222/n/J;->a(Ldbxyzptlk/db231222/n/C;Ljava/lang/String;)V

    .line 637
    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->u:Ldbxyzptlk/db231222/n/J;

    iget-object v2, p1, Lcom/dropbox/android/filemanager/h;->b:Ldbxyzptlk/db231222/y/m;

    iget-object v2, v2, Ldbxyzptlk/db231222/y/m;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/db231222/n/J;->a(Ldbxyzptlk/db231222/n/C;Ljava/lang/String;)V

    .line 638
    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->v:Ldbxyzptlk/db231222/n/J;

    iget-object v2, p1, Lcom/dropbox/android/filemanager/h;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/db231222/n/J;->a(Ldbxyzptlk/db231222/n/C;Ljava/lang/String;)V

    .line 639
    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->w:Ldbxyzptlk/db231222/n/J;

    iget-object v2, p1, Lcom/dropbox/android/filemanager/h;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/db231222/n/J;->a(Ldbxyzptlk/db231222/n/C;Ljava/lang/String;)V

    .line 646
    :goto_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/C;->a()Z

    .line 647
    return-void

    .line 641
    :cond_0
    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->t:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/n/J;->b(Ldbxyzptlk/db231222/n/C;)V

    .line 642
    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->u:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/n/J;->b(Ldbxyzptlk/db231222/n/C;)V

    .line 643
    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->v:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/n/J;->b(Ldbxyzptlk/db231222/n/C;)V

    .line 644
    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->w:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/n/J;->b(Ldbxyzptlk/db231222/n/C;)V

    goto :goto_0
.end method

.method public final a(Ldbxyzptlk/db231222/n/u;)V
    .locals 4

    .prologue
    .line 616
    new-instance v0, Ldbxyzptlk/db231222/n/C;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/n/C;-><init>(Ldbxyzptlk/db231222/n/I;)V

    .line 617
    if-eqz p1, :cond_0

    .line 618
    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->n:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/n/u;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/db231222/n/J;->a(Ldbxyzptlk/db231222/n/C;Ljava/lang/String;)V

    .line 619
    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->p:Ldbxyzptlk/db231222/n/G;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/n/u;->c()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Ldbxyzptlk/db231222/n/G;->a(Ldbxyzptlk/db231222/n/C;J)V

    .line 620
    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->o:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/n/u;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/db231222/n/J;->a(Ldbxyzptlk/db231222/n/C;Ljava/lang/String;)V

    .line 621
    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->q:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/n/u;->e()Ldbxyzptlk/db231222/n/t;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/n/t;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/db231222/n/J;->a(Ldbxyzptlk/db231222/n/C;Ljava/lang/String;)V

    .line 622
    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->r:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/n/u;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/db231222/n/J;->a(Ldbxyzptlk/db231222/n/C;Ljava/lang/String;)V

    .line 630
    :goto_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/C;->a()Z

    .line 631
    return-void

    .line 624
    :cond_0
    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->n:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/n/J;->b(Ldbxyzptlk/db231222/n/C;)V

    .line 625
    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->p:Ldbxyzptlk/db231222/n/G;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/n/G;->b(Ldbxyzptlk/db231222/n/C;)V

    .line 626
    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->o:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/n/J;->b(Ldbxyzptlk/db231222/n/C;)V

    .line 627
    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->q:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/n/J;->b(Ldbxyzptlk/db231222/n/C;)V

    .line 628
    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->r:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/n/J;->b(Ldbxyzptlk/db231222/n/C;)V

    goto :goto_0
.end method

.method public final a(Ldbxyzptlk/db231222/t/e;)V
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->y:Ldbxyzptlk/db231222/n/H;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/H;->a(Ldbxyzptlk/db231222/H/t;)V

    .line 426
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->x:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/D;->a(Z)V

    .line 396
    return-void
.end method

.method final b([Ljava/lang/String;)Ldbxyzptlk/db231222/n/x;
    .locals 2

    .prologue
    .line 253
    new-instance v0, Ldbxyzptlk/db231222/n/o;

    sget-object v1, Ldbxyzptlk/db231222/n/p;->b:Ldbxyzptlk/db231222/n/p;

    invoke-direct {v0, p0, v1, p1}, Ldbxyzptlk/db231222/n/o;-><init>(Ldbxyzptlk/db231222/n/k;Ldbxyzptlk/db231222/n/p;[Ljava/lang/String;)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 490
    new-instance v1, Ldbxyzptlk/db231222/n/C;

    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->f:Ldbxyzptlk/db231222/n/I;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/n/C;-><init>(Ldbxyzptlk/db231222/n/I;)V

    .line 492
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->h:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v2

    .line 493
    const/4 v0, 0x0

    .line 494
    if-eqz p1, :cond_0

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 495
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->i:Ldbxyzptlk/db231222/n/F;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/F;->d()I

    move-result v0

    .line 500
    :goto_0
    add-int/lit8 v0, v0, 0x1

    .line 501
    iget-object v2, p0, Ldbxyzptlk/db231222/n/k;->i:Ldbxyzptlk/db231222/n/F;

    invoke-virtual {v2, v1, v0}, Ldbxyzptlk/db231222/n/F;->a(Ldbxyzptlk/db231222/n/C;I)V

    .line 502
    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/C;->a()Z

    .line 503
    return-void

    .line 497
    :cond_0
    iget-object v2, p0, Ldbxyzptlk/db231222/n/k;->h:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v2, v1, p1}, Ldbxyzptlk/db231222/n/J;->a(Ldbxyzptlk/db231222/n/C;Ljava/lang/String;)V

    goto :goto_0
.end method

.method final b(Z)V
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->A:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/D;->a(Z)V

    .line 404
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->x:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/D;->d()Z

    move-result v0

    return v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 585
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->m:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/J;->a(Ljava/lang/String;)V

    .line 586
    return-void
.end method

.method final c(Z)V
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->z:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/D;->a(Z)V

    .line 413
    return-void
.end method

.method final c()Z
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->A:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/D;->d()Z

    move-result v0

    return v0
.end method

.method public final d(Z)V
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->g:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/D;->a(Z)V

    .line 444
    return-void
.end method

.method final d()Z
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->z:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/D;->d()Z

    move-result v0

    return v0
.end method

.method public final e()Ldbxyzptlk/db231222/t/e;
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->y:Ldbxyzptlk/db231222/n/H;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/H;->d()Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/e;

    return-object v0
.end method

.method public final e(Z)V
    .locals 1

    .prologue
    .line 593
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->s:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/D;->a(Z)V

    .line 594
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 435
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->g:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/D;->d()Z

    move-result v0

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 454
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->j:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v0

    .line 456
    invoke-static {v0}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 464
    :goto_0
    return-object v0

    .line 462
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/dropbox/android/util/U;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 463
    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->j:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/n/J;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 468
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->k:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v0

    .line 470
    invoke-static {v0}, Lcom/dropbox/android/util/bh;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 478
    :goto_0
    return-object v0

    .line 476
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/dropbox/android/util/U;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 477
    iget-object v1, p0, Ldbxyzptlk/db231222/n/k;->k:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/n/J;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 560
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->l:Ldbxyzptlk/db231222/n/G;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/G;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 576
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->m:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 589
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->s:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/D;->d()Z

    move-result v0

    return v0
.end method

.method public final m()Ldbxyzptlk/db231222/n/u;
    .locals 7

    .prologue
    .line 598
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->n:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v1

    .line 599
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->p:Ldbxyzptlk/db231222/n/G;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/G;->d()J

    move-result-wide v2

    .line 600
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->o:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v4

    .line 601
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->q:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/n/t;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/n/t;

    move-result-object v5

    .line 602
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->r:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v6

    .line 603
    if-eqz v1, :cond_0

    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    if-eqz v6, :cond_0

    if-eqz v5, :cond_0

    .line 604
    new-instance v0, Ldbxyzptlk/db231222/n/u;

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/n/u;-><init>(Ljava/lang/String;JLjava/lang/String;Ldbxyzptlk/db231222/n/t;Ljava/lang/String;)V

    .line 611
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Lcom/dropbox/android/filemanager/h;
    .locals 5

    .prologue
    .line 651
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->t:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v1

    .line 652
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->u:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v2

    .line 653
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->v:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v3

    .line 654
    iget-object v0, p0, Ldbxyzptlk/db231222/n/k;->w:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v4

    .line 655
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    .line 656
    new-instance v0, Lcom/dropbox/android/filemanager/h;

    invoke-direct {v0, v3, v4, v1, v2}, Lcom/dropbox/android/filemanager/h;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 658
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
