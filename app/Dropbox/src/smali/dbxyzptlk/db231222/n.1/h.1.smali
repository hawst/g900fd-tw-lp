.class public final Ldbxyzptlk/db231222/n/h;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/content/SharedPreferences$Editor;


# instance fields
.field final synthetic a:Ldbxyzptlk/db231222/n/a;

.field private final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/n/a;)V
    .locals 1

    .prologue
    .line 372
    iput-object p1, p0, Ldbxyzptlk/db231222/n/h;->a:Ldbxyzptlk/db231222/n/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 374
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/h;->b:Ljava/util/HashMap;

    .line 375
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/n/h;->c:Z

    return-void
.end method


# virtual methods
.method public final apply()V
    .locals 0

    .prologue
    .line 379
    invoke-virtual {p0}, Ldbxyzptlk/db231222/n/h;->commit()Z

    .line 380
    return-void
.end method

.method public final clear()Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 384
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/db231222/n/h;->c:Z

    .line 385
    return-object p0
.end method

.method public final commit()Z
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 393
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 396
    iget-object v0, p0, Ldbxyzptlk/db231222/n/h;->a:Ldbxyzptlk/db231222/n/a;

    invoke-static {v0}, Ldbxyzptlk/db231222/n/a;->a(Ldbxyzptlk/db231222/n/a;)Ljava/util/HashMap;

    move-result-object v3

    monitor-enter v3

    .line 397
    :try_start_0
    iget-boolean v0, p0, Ldbxyzptlk/db231222/n/h;->c:Z

    if-eqz v0, :cond_0

    .line 398
    iget-object v0, p0, Ldbxyzptlk/db231222/n/h;->a:Ldbxyzptlk/db231222/n/a;

    invoke-static {v0}, Ldbxyzptlk/db231222/n/a;->a(Ldbxyzptlk/db231222/n/a;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 400
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/n/h;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 401
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 402
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 403
    invoke-static {}, Ldbxyzptlk/db231222/n/a;->e()Ljava/lang/Object;

    move-result-object v5

    if-ne v0, v5, :cond_2

    .line 404
    iget-object v0, p0, Ldbxyzptlk/db231222/n/h;->a:Ldbxyzptlk/db231222/n/a;

    invoke-static {v0}, Ldbxyzptlk/db231222/n/a;->a(Ldbxyzptlk/db231222/n/a;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 415
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 406
    :cond_2
    :try_start_1
    iget-object v5, p0, Ldbxyzptlk/db231222/n/h;->a:Ldbxyzptlk/db231222/n/a;

    invoke-static {v5}, Ldbxyzptlk/db231222/n/a;->a(Ldbxyzptlk/db231222/n/a;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 409
    if-eqz v5, :cond_3

    invoke-virtual {v5, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 410
    :cond_3
    iget-object v0, p0, Ldbxyzptlk/db231222/n/h;->a:Ldbxyzptlk/db231222/n/a;

    invoke-static {v0}, Ldbxyzptlk/db231222/n/a;->a(Ldbxyzptlk/db231222/n/a;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v5, p0, Ldbxyzptlk/db231222/n/h;->b:Ljava/util/HashMap;

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 415
    :cond_4
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 417
    iget-object v0, p0, Ldbxyzptlk/db231222/n/h;->a:Ldbxyzptlk/db231222/n/a;

    invoke-static {v0}, Ldbxyzptlk/db231222/n/a;->b(Ldbxyzptlk/db231222/n/a;)Ldbxyzptlk/db231222/n/v;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/v;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 418
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V

    .line 420
    :try_start_2
    iget-object v0, p0, Ldbxyzptlk/db231222/n/h;->a:Ldbxyzptlk/db231222/n/a;

    invoke-static {v0}, Ldbxyzptlk/db231222/n/a;->b(Ldbxyzptlk/db231222/n/a;)Ldbxyzptlk/db231222/n/v;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/v;->b()Ljava/lang/String;

    move-result-object v4

    .line 421
    iget-boolean v0, p0, Ldbxyzptlk/db231222/n/h;->c:Z

    if-eqz v0, :cond_5

    .line 422
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {v3, v4, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 425
    :cond_5
    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    .line 426
    new-instance v6, Landroid/content/ContentValues;

    const/4 v0, 0x2

    invoke-direct {v6, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 427
    iget-object v0, p0, Ldbxyzptlk/db231222/n/h;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_6
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 428
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 429
    const/4 v8, 0x0

    aput-object v1, v5, v8

    .line 430
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 431
    invoke-static {}, Ldbxyzptlk/db231222/n/a;->e()Ljava/lang/Object;

    move-result-object v8

    if-ne v0, v8, :cond_8

    .line 432
    const-string v0, "pref_name=?"

    invoke-virtual {v3, v4, v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 433
    if-nez v0, :cond_7

    .line 435
    invoke-static {}, Ldbxyzptlk/db231222/n/a;->f()Ljava/lang/String;

    move-result-object v0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "NOTE: Tried to delete pref that doesn\'t exist: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, " in "

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v8, p0, Ldbxyzptlk/db231222/n/h;->a:Ldbxyzptlk/db231222/n/a;

    invoke-static {v8}, Ldbxyzptlk/db231222/n/a;->b(Ldbxyzptlk/db231222/n/a;)Ldbxyzptlk/db231222/n/v;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    .line 455
    :catchall_1
    move-exception v0

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 438
    :cond_7
    :try_start_3
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 441
    :cond_8
    const-string v8, "pref_name"

    invoke-virtual {v6, v8, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    const-string v1, "pref_value"

    invoke-static {v0}, Ldbxyzptlk/db231222/n/a;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    const-string v0, "pref_name=?"

    invoke-virtual {v3, v4, v6, v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 444
    if-nez v0, :cond_6

    .line 446
    const-string v0, "pref_name"

    invoke-virtual {v3, v4, v0, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    const-wide/16 v8, -0x1

    cmp-long v0, v0, v8

    if-nez v0, :cond_6

    .line 447
    invoke-static {}, Ldbxyzptlk/db231222/n/a;->f()Ljava/lang/String;

    move-result-object v0

    const-string v1, "insert failed"

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 453
    :cond_9
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 455
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 458
    iget-object v0, p0, Ldbxyzptlk/db231222/n/h;->a:Ldbxyzptlk/db231222/n/a;

    invoke-static {v0, v2}, Ldbxyzptlk/db231222/n/a;->a(Ldbxyzptlk/db231222/n/a;Ljava/util/Collection;)V

    .line 459
    return v10
.end method

.method public final putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;
    .locals 2

    .prologue
    .line 464
    iget-object v0, p0, Ldbxyzptlk/db231222/n/h;->b:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 465
    return-object p0
.end method

.method public final putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;
    .locals 2

    .prologue
    .line 470
    iget-object v0, p0, Ldbxyzptlk/db231222/n/h;->b:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 471
    return-object p0
.end method

.method public final putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;
    .locals 2

    .prologue
    .line 476
    iget-object v0, p0, Ldbxyzptlk/db231222/n/h;->b:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 477
    return-object p0
.end method

.method public final putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;
    .locals 2

    .prologue
    .line 482
    iget-object v0, p0, Ldbxyzptlk/db231222/n/h;->b:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 483
    return-object p0
.end method

.method public final putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    .locals 2

    .prologue
    .line 488
    if-nez p2, :cond_0

    .line 498
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null values are currently unsupported."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 500
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/n/h;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501
    return-object p0
.end method

.method public final putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/content/SharedPreferences$Editor;"
        }
    .end annotation

    .prologue
    .line 506
    if-nez p2, :cond_0

    .line 508
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null values are currently unsupported."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 510
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/n/h;->b:Ljava/util/HashMap;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 511
    return-object p0
.end method

.method public final remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    .locals 2

    .prologue
    .line 516
    iget-object v0, p0, Ldbxyzptlk/db231222/n/h;->b:Ljava/util/HashMap;

    invoke-static {}, Ldbxyzptlk/db231222/n/a;->e()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 517
    return-object p0
.end method
