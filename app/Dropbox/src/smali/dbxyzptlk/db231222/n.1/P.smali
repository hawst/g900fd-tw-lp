.class public final Ldbxyzptlk/db231222/n/P;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final f:[Ljava/lang/String;


# instance fields
.field private final A:Ldbxyzptlk/db231222/n/D;

.field private final B:Ldbxyzptlk/db231222/n/G;

.field private final C:Ldbxyzptlk/db231222/n/D;

.field private final D:Ldbxyzptlk/db231222/n/D;

.field private final E:Ldbxyzptlk/db231222/n/D;

.field private final F:Ldbxyzptlk/db231222/n/D;

.field private final G:Ldbxyzptlk/db231222/n/D;

.field private final H:Ldbxyzptlk/db231222/n/D;

.field private final I:Ldbxyzptlk/db231222/n/D;

.field private final J:Ldbxyzptlk/db231222/n/G;

.field private final K:Ldbxyzptlk/db231222/n/E;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/n/E",
            "<",
            "Ldbxyzptlk/db231222/n/q;",
            ">;"
        }
    .end annotation
.end field

.field private final L:Ldbxyzptlk/db231222/n/E;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/n/E",
            "<",
            "Ldbxyzptlk/db231222/n/n;",
            ">;"
        }
    .end annotation
.end field

.field private final M:Ldbxyzptlk/db231222/n/J;

.field private final N:Ldbxyzptlk/db231222/n/J;

.field private final O:Ldbxyzptlk/db231222/n/J;

.field private final P:Ldbxyzptlk/db231222/n/y;

.field private final Q:Ldbxyzptlk/db231222/n/y;

.field private final a:Ljava/lang/String;

.field private final b:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ldbxyzptlk/db231222/n/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/Context;

.field private final d:Ldbxyzptlk/db231222/n/x;

.field private final e:Ldbxyzptlk/db231222/n/I;

.field private final g:Ldbxyzptlk/db231222/n/H;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/n/H",
            "<",
            "Ldbxyzptlk/db231222/s/a;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ldbxyzptlk/db231222/n/H;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/n/H",
            "<",
            "Ldbxyzptlk/db231222/s/m;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ldbxyzptlk/db231222/n/H;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/n/H",
            "<",
            "Ldbxyzptlk/db231222/t/i;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ldbxyzptlk/db231222/n/J;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final k:Ldbxyzptlk/db231222/n/J;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final l:Ldbxyzptlk/db231222/n/J;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final m:Ldbxyzptlk/db231222/n/J;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final n:Ldbxyzptlk/db231222/n/G;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final o:Ldbxyzptlk/db231222/n/G;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final p:Ldbxyzptlk/db231222/n/G;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final q:Ldbxyzptlk/db231222/n/G;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final r:Ldbxyzptlk/db231222/n/J;

.field private final s:Ldbxyzptlk/db231222/n/J;

.field private final t:Ldbxyzptlk/db231222/n/J;

.field private final u:Ldbxyzptlk/db231222/n/J;

.field private final v:Ldbxyzptlk/db231222/n/J;

.field private final w:Ldbxyzptlk/db231222/n/J;

.field private final x:Ldbxyzptlk/db231222/n/D;

.field private final y:Ldbxyzptlk/db231222/n/D;

.field private final z:Ldbxyzptlk/db231222/n/D;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 100
    const/16 v0, 0x1f

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "ACCOUNT_INFO"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "COUNTRY"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "DISPLAY_NAME"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "REFERRAL_LINK"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "EMAIL"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "QUOTA"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "QUOTA_NORMAL"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "QUOTA_SHARED"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "UID"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "LAST_EXPORT"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "LAST_EXPORT_URI_V2"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "LAST_GET_CONTENT_URI"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "LAST_MOVE_URI"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "NOTIFICATIONS_TO_MUTE"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "NOTIFICATIONS_MUTED"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "CAMERA_UPLOAD_ENABLED"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "CAMERA_UPLOAD_INITIAL_SCAN"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "CAMERA_UPLOAD_HAS_UPLOADED_ONCE"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "CAMERA_UPLOAD_HAD_BACKLOG"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "CAMERA_UPLOAD_NUM_UPLOADS"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "CAMERA_UPLOAD_FIRST_MEDIA_SCAN"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "CAMERA_UPLOAD_USE_3G"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "CAMERA_UPLOAD_3G_LIMIT"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "CAMERA_UPLOAD_IGNORE_EXISTING"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "CAMERA_UPLOAD_SEEN_INTRO"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "CAMERA_UPLOAD_TURNED_ON_TIME"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "CAMERA_UPLOAD_HASH_UPDATE"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "CAMERA_UPLOAD_NOTIFICATION"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "ALBUMS_DELTA_CURSOR"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "LIGHT_ALBUMS_DELTA_CURSOR"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "ALL_PHOTOS_CURSOR"

    aput-object v2, v0, v1

    sput-object v0, Ldbxyzptlk/db231222/n/P;->f:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ldbxyzptlk/db231222/n/k;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const-wide/16 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->b:Ljava/util/concurrent/atomic/AtomicReference;

    .line 91
    new-instance v0, Ldbxyzptlk/db231222/n/Q;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/n/Q;-><init>(Ldbxyzptlk/db231222/n/P;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    .line 120
    new-instance v0, Ldbxyzptlk/db231222/n/H;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "ACCOUNT_INFO"

    sget-object v3, Ldbxyzptlk/db231222/s/a;->a:Ldbxyzptlk/db231222/H/w;

    invoke-direct {v0, v1, v2, v3, v5}, Ldbxyzptlk/db231222/n/H;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ldbxyzptlk/db231222/H/w;Ldbxyzptlk/db231222/H/t;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->g:Ldbxyzptlk/db231222/n/H;

    .line 121
    new-instance v0, Ldbxyzptlk/db231222/n/H;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "PAIRING_INFO"

    sget-object v3, Ldbxyzptlk/db231222/s/m;->a:Ldbxyzptlk/db231222/H/w;

    invoke-direct {v0, v1, v2, v3, v5}, Ldbxyzptlk/db231222/n/H;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ldbxyzptlk/db231222/H/w;Ldbxyzptlk/db231222/H/t;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->h:Ldbxyzptlk/db231222/n/H;

    .line 124
    new-instance v0, Ldbxyzptlk/db231222/n/H;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "USER_FLAGS"

    sget-object v3, Ldbxyzptlk/db231222/t/i;->a:Ldbxyzptlk/db231222/H/w;

    invoke-static {}, Ldbxyzptlk/db231222/t/i;->a()Ldbxyzptlk/db231222/t/i;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Ldbxyzptlk/db231222/n/H;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ldbxyzptlk/db231222/H/w;Ldbxyzptlk/db231222/H/t;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->i:Ldbxyzptlk/db231222/n/H;

    .line 130
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "COUNTRY"

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->j:Ldbxyzptlk/db231222/n/J;

    .line 131
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "DISPLAY_NAME"

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->k:Ldbxyzptlk/db231222/n/J;

    .line 132
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "REFERRAL_LINK"

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->l:Ldbxyzptlk/db231222/n/J;

    .line 133
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "EMAIL"

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->m:Ldbxyzptlk/db231222/n/J;

    .line 135
    new-instance v0, Ldbxyzptlk/db231222/n/G;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "QUOTA_QUOTA"

    invoke-direct {v0, v1, v2, v7, v8}, Ldbxyzptlk/db231222/n/G;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;J)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->n:Ldbxyzptlk/db231222/n/G;

    .line 136
    new-instance v0, Ldbxyzptlk/db231222/n/G;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "QUOTA_NORMAL"

    invoke-direct {v0, v1, v2, v7, v8}, Ldbxyzptlk/db231222/n/G;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;J)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->o:Ldbxyzptlk/db231222/n/G;

    .line 137
    new-instance v0, Ldbxyzptlk/db231222/n/G;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "QUOTA_SHARED"

    invoke-direct {v0, v1, v2, v7, v8}, Ldbxyzptlk/db231222/n/G;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;J)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->p:Ldbxyzptlk/db231222/n/G;

    .line 138
    new-instance v0, Ldbxyzptlk/db231222/n/G;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "UID"

    invoke-direct {v0, v1, v2, v7, v8}, Ldbxyzptlk/db231222/n/G;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;J)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->q:Ldbxyzptlk/db231222/n/G;

    .line 140
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "LAST_URI"

    sget-object v3, Lcom/dropbox/android/util/DropboxPath;->a:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v3}, Lcom/dropbox/android/util/DropboxPath;->c()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->r:Ldbxyzptlk/db231222/n/J;

    .line 141
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "LAST_EXPORT_URI_V2"

    invoke-direct {v0, v1, v2, v5}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->s:Ldbxyzptlk/db231222/n/J;

    .line 142
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "LAST_GET_CONTENT_URI"

    sget-object v3, Lcom/dropbox/android/util/DropboxPath;->a:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v3}, Lcom/dropbox/android/util/DropboxPath;->c()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->t:Ldbxyzptlk/db231222/n/J;

    .line 143
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "LAST_MOVE_URI"

    sget-object v3, Lcom/dropbox/android/util/DropboxPath;->a:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v3}, Lcom/dropbox/android/util/DropboxPath;->c()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->u:Ldbxyzptlk/db231222/n/J;

    .line 145
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "NOTIFICATIONS_TO_MUTE"

    invoke-direct {v0, v1, v2, v5}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->v:Ldbxyzptlk/db231222/n/J;

    .line 146
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "NOTIFICATIONS_MUTED"

    invoke-direct {v0, v1, v2, v5}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->w:Ldbxyzptlk/db231222/n/J;

    .line 148
    new-instance v0, Ldbxyzptlk/db231222/n/D;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "CAMERA_UPLOAD_ENABLED"

    invoke-direct {v0, v1, v2, v6}, Ldbxyzptlk/db231222/n/D;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Z)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->x:Ldbxyzptlk/db231222/n/D;

    .line 149
    new-instance v0, Ldbxyzptlk/db231222/n/D;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "CAMERA_UPLOAD_INITIAL_SCAN"

    invoke-direct {v0, v1, v2, v9}, Ldbxyzptlk/db231222/n/D;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Z)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->y:Ldbxyzptlk/db231222/n/D;

    .line 150
    new-instance v0, Ldbxyzptlk/db231222/n/D;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "CAMERA_UPLOAD_HAS_UPLOADED_ONCE"

    invoke-direct {v0, v1, v2, v6}, Ldbxyzptlk/db231222/n/D;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Z)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->z:Ldbxyzptlk/db231222/n/D;

    .line 152
    new-instance v0, Ldbxyzptlk/db231222/n/D;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "CAMERA_UPLOAD_HAD_BACKLOG"

    invoke-direct {v0, v1, v2, v6}, Ldbxyzptlk/db231222/n/D;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Z)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->A:Ldbxyzptlk/db231222/n/D;

    .line 154
    new-instance v0, Ldbxyzptlk/db231222/n/G;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "CAMERA_UPLOAD_NUM_UPLOADS"

    invoke-direct {v0, v1, v2, v7, v8}, Ldbxyzptlk/db231222/n/G;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;J)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->B:Ldbxyzptlk/db231222/n/G;

    .line 155
    new-instance v0, Ldbxyzptlk/db231222/n/D;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "CAMERA_UPLOAD_FIRST_MEDIA_SCAN"

    invoke-direct {v0, v1, v2, v6}, Ldbxyzptlk/db231222/n/D;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Z)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->C:Ldbxyzptlk/db231222/n/D;

    .line 156
    new-instance v0, Ldbxyzptlk/db231222/n/D;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "CAMERA_UPLOAD_USE_3G"

    invoke-direct {v0, v1, v2, v6}, Ldbxyzptlk/db231222/n/D;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Z)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->D:Ldbxyzptlk/db231222/n/D;

    .line 157
    new-instance v0, Ldbxyzptlk/db231222/n/D;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "CAMERA_UPLOAD_3G_LIMIT"

    invoke-direct {v0, v1, v2, v6}, Ldbxyzptlk/db231222/n/D;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Z)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->E:Ldbxyzptlk/db231222/n/D;

    .line 158
    new-instance v0, Ldbxyzptlk/db231222/n/D;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "CAMERA_UPLOAD_IGNORE_EXISTING"

    invoke-direct {v0, v1, v2, v6}, Ldbxyzptlk/db231222/n/D;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Z)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->F:Ldbxyzptlk/db231222/n/D;

    .line 160
    new-instance v0, Ldbxyzptlk/db231222/n/D;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "CAMERA_UPLOAD_VIDEOS_ENABLED"

    invoke-direct {v0, v1, v2, v9}, Ldbxyzptlk/db231222/n/D;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Z)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->G:Ldbxyzptlk/db231222/n/D;

    .line 166
    new-instance v0, Ldbxyzptlk/db231222/n/D;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "CAMERA_UPLOAD_VIDEOS_ENABLED_SUGGESTED_DEFAULT"

    invoke-direct {v0, v1, v2, v9}, Ldbxyzptlk/db231222/n/D;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Z)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->H:Ldbxyzptlk/db231222/n/D;

    .line 168
    new-instance v0, Ldbxyzptlk/db231222/n/D;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "CAMERA_UPLOAD_SEEN_INTRO"

    invoke-direct {v0, v1, v2, v6}, Ldbxyzptlk/db231222/n/D;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Z)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->I:Ldbxyzptlk/db231222/n/D;

    .line 170
    new-instance v0, Ldbxyzptlk/db231222/n/G;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "CAMERA_UPLOAD_TURNED_ON_TIME"

    const-wide/16 v3, -0x1

    invoke-direct {v0, v1, v2, v3, v4}, Ldbxyzptlk/db231222/n/G;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;J)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->J:Ldbxyzptlk/db231222/n/G;

    .line 172
    new-instance v0, Ldbxyzptlk/db231222/n/E;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "CAMERA_UPLOAD_HASH_UPDATE"

    const-class v3, Ldbxyzptlk/db231222/n/q;

    sget-object v4, Ldbxyzptlk/db231222/n/q;->b:Ldbxyzptlk/db231222/n/q;

    invoke-direct {v0, v1, v2, v3, v4}, Ldbxyzptlk/db231222/n/E;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->K:Ldbxyzptlk/db231222/n/E;

    .line 175
    new-instance v0, Ldbxyzptlk/db231222/n/E;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "CAMERA_UPLOAD_NOTIFICATION"

    const-class v3, Ldbxyzptlk/db231222/n/n;

    sget-object v4, Ldbxyzptlk/db231222/n/n;->a:Ldbxyzptlk/db231222/n/n;

    invoke-direct {v0, v1, v2, v3, v4}, Ldbxyzptlk/db231222/n/E;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->L:Ldbxyzptlk/db231222/n/E;

    .line 178
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "ALBUMS_DELTA_CURSOR"

    invoke-direct {v0, v1, v2, v5}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->M:Ldbxyzptlk/db231222/n/J;

    .line 179
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "LIGHT_ALBUMS_DELTA_CURSOR"

    invoke-direct {v0, v1, v2, v5}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->N:Ldbxyzptlk/db231222/n/J;

    .line 181
    new-instance v0, Ldbxyzptlk/db231222/n/J;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    const-string v2, "ALL_PHOTOS_CURSOR"

    invoke-direct {v0, v1, v2, v5}, Ldbxyzptlk/db231222/n/J;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->O:Ldbxyzptlk/db231222/n/J;

    .line 404
    new-instance v0, Ldbxyzptlk/db231222/n/y;

    invoke-direct {v0}, Ldbxyzptlk/db231222/n/y;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->P:Ldbxyzptlk/db231222/n/y;

    .line 405
    new-instance v0, Ldbxyzptlk/db231222/n/y;

    invoke-direct {v0}, Ldbxyzptlk/db231222/n/y;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->Q:Ldbxyzptlk/db231222/n/y;

    .line 46
    iput-object p1, p0, Ldbxyzptlk/db231222/n/P;->c:Landroid/content/Context;

    .line 47
    if-eqz p3, :cond_0

    .line 48
    sget-object v0, Ldbxyzptlk/db231222/n/P;->f:[Ljava/lang/String;

    invoke-virtual {p3, v0}, Ldbxyzptlk/db231222/n/k;->a([Ljava/lang/String;)Ldbxyzptlk/db231222/n/x;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/n/P;->d:Ldbxyzptlk/db231222/n/x;

    .line 52
    :goto_0
    iput-object p2, p0, Ldbxyzptlk/db231222/n/P;->a:Ljava/lang/String;

    .line 53
    return-void

    .line 50
    :cond_0
    iput-object v5, p0, Ldbxyzptlk/db231222/n/P;->d:Ldbxyzptlk/db231222/n/x;

    goto :goto_0
.end method

.method private L()Ldbxyzptlk/db231222/n/a;
    .locals 6

    .prologue
    .line 77
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/n/a;

    .line 78
    if-nez v0, :cond_1

    .line 79
    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->b:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v1

    .line 80
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/n/a;

    .line 81
    if-nez v0, :cond_0

    .line 82
    new-instance v2, Ldbxyzptlk/db231222/n/v;

    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->c:Landroid/content/Context;

    iget-object v3, p0, Ldbxyzptlk/db231222/n/P;->a:Ljava/lang/String;

    sget-object v4, Ldbxyzptlk/db231222/n/w;->a:Ldbxyzptlk/db231222/n/w;

    iget-object v5, p0, Ldbxyzptlk/db231222/n/P;->d:Ldbxyzptlk/db231222/n/x;

    invoke-direct {v2, v0, v3, v4, v5}, Ldbxyzptlk/db231222/n/v;-><init>(Landroid/content/Context;Ljava/lang/String;Ldbxyzptlk/db231222/n/w;Ldbxyzptlk/db231222/n/x;)V

    .line 83
    new-instance v0, Ldbxyzptlk/db231222/n/a;

    invoke-direct {v0, v2}, Ldbxyzptlk/db231222/n/a;-><init>(Ldbxyzptlk/db231222/n/v;)V

    .line 84
    iget-object v2, p0, Ldbxyzptlk/db231222/n/P;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 86
    :cond_0
    monitor-exit v1

    .line 88
    :cond_1
    return-object v0

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/n/P;)Ldbxyzptlk/db231222/n/a;
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ldbxyzptlk/db231222/n/P;->L()Ldbxyzptlk/db231222/n/a;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 64
    invoke-static {p0}, Lcom/dropbox/android/util/C;->a(Ljava/lang/String;)V

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-prefs.db"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final A()Z
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->G:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/D;->d()Z

    move-result v0

    return v0
.end method

.method public final B()Z
    .locals 1

    .prologue
    .line 568
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->G:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/D;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 570
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->G:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/D;->d()Z

    move-result v0

    .line 574
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->H:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/D;->d()Z

    move-result v0

    goto :goto_0
.end method

.method public final C()Z
    .locals 1

    .prologue
    .line 587
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->F:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/D;->d()Z

    move-result v0

    return v0
.end method

.method public final D()Z
    .locals 1

    .prologue
    .line 595
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->I:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/D;->d()Z

    move-result v0

    return v0
.end method

.method public final E()Ljava/lang/String;
    .locals 1

    .prologue
    .line 608
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->O:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final F()J
    .locals 4

    .prologue
    .line 616
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->J:Ldbxyzptlk/db231222/n/G;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/G;->d()J

    move-result-wide v0

    .line 617
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 619
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 620
    invoke-virtual {p0, v0, v1}, Ldbxyzptlk/db231222/n/P;->a(J)V

    .line 622
    :cond_0
    return-wide v0
.end method

.method public final G()Ljava/lang/String;
    .locals 1

    .prologue
    .line 668
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->M:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final H()V
    .locals 1

    .prologue
    .line 672
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->M:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->b()V

    .line 673
    return-void
.end method

.method public final I()Ljava/lang/String;
    .locals 1

    .prologue
    .line 681
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->N:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final J()V
    .locals 1

    .prologue
    .line 685
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->N:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->b()V

    .line 686
    return-void
.end method

.method public final K()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 699
    invoke-static {}, Ldbxyzptlk/db231222/r/e;->a()Ldbxyzptlk/db231222/r/e;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/e;->b()Ldbxyzptlk/db231222/r/k;

    move-result-object v2

    .line 700
    if-nez v2, :cond_1

    .line 719
    :cond_0
    :goto_0
    return v0

    .line 705
    :cond_1
    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/k;->e()Ldbxyzptlk/db231222/r/d;

    move-result-object v3

    .line 706
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/n/P;->o()Z

    move-result v3

    if-nez v3, :cond_0

    .line 713
    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/k;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    .line 714
    goto :goto_0

    .line 718
    :cond_2
    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/k;->d()Ldbxyzptlk/db231222/r/d;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/r/d;->n()Ldbxyzptlk/db231222/n/P;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/n/P;->g()Ldbxyzptlk/db231222/s/a;

    move-result-object v2

    .line 719
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ldbxyzptlk/db231222/s/a;->v()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 188
    invoke-direct {p0}, Ldbxyzptlk/db231222/n/P;->L()Ldbxyzptlk/db231222/n/a;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/a;->a()V

    .line 189
    return-void
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 626
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->J:Ldbxyzptlk/db231222/n/G;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/n/G;->a(J)V

    .line 627
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 370
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->s:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/J;->a(Ljava/lang/String;)V

    .line 371
    return-void
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;)V
    .locals 2

    .prologue
    .line 347
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->r:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->c()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/J;->a(Ljava/lang/String;)V

    .line 348
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/n/n;)V
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->L:Ldbxyzptlk/db231222/n/E;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/E;->a(Ljava/lang/Enum;)V

    .line 486
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/n/q;)V
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->K:Ldbxyzptlk/db231222/n/E;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/E;->a(Ljava/lang/Enum;)V

    .line 478
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/n/z;)V
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->P:Ldbxyzptlk/db231222/n/y;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/y;->a(Ldbxyzptlk/db231222/n/z;)Z

    .line 426
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/s/a;)V
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->g:Ldbxyzptlk/db231222/n/H;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/H;->a(Ldbxyzptlk/db231222/H/t;)V

    .line 323
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/s/m;)V
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->h:Ldbxyzptlk/db231222/n/H;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/H;->a(Ldbxyzptlk/db231222/H/t;)V

    .line 339
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 205
    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->i:Ldbxyzptlk/db231222/n/H;

    monitor-enter v1

    .line 206
    :try_start_0
    iget-object v2, p0, Ldbxyzptlk/db231222/n/P;->i:Ldbxyzptlk/db231222/n/H;

    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->i:Ldbxyzptlk/db231222/n/H;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/H;->d()Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/i;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/i;->n()Ldbxyzptlk/db231222/t/k;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/t/k;->a(Z)Ldbxyzptlk/db231222/t/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/k;->b()Ldbxyzptlk/db231222/t/i;

    move-result-object v0

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/n/H;->a(Ldbxyzptlk/db231222/H/t;)V

    .line 207
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->Q:Ldbxyzptlk/db231222/n/y;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/y;->a()V

    .line 209
    return-void

    .line 207
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(ZZZLcom/dropbox/android/service/i;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 633
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->d()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v3, "value"

    const-string v4, "ok"

    invoke-virtual {v0, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 634
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->o()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v3, "cameraupload.usedataplan"

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 635
    invoke-static {}, Lcom/dropbox/android/util/S;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 636
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->o()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v3, "cameraupload.uploadexisting"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 643
    :goto_0
    invoke-virtual {p0, v1}, Ldbxyzptlk/db231222/n/P;->i(Z)V

    .line 644
    invoke-virtual {p0, v1}, Ldbxyzptlk/db231222/n/P;->g(Z)V

    .line 645
    if-nez p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/n/P;->n(Z)V

    .line 646
    invoke-virtual {p0, p2}, Ldbxyzptlk/db231222/n/P;->j(Z)V

    .line 647
    invoke-virtual {p0, v1}, Ldbxyzptlk/db231222/n/P;->k(Z)V

    .line 648
    invoke-virtual {p0, p3}, Ldbxyzptlk/db231222/n/P;->l(Z)V

    .line 649
    sget-object v0, Ldbxyzptlk/db231222/n/q;->b:Ldbxyzptlk/db231222/n/q;

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/n/P;->a(Ldbxyzptlk/db231222/n/q;)V

    .line 650
    if-eqz p1, :cond_2

    sget-object v0, Ldbxyzptlk/db231222/n/n;->b:Ldbxyzptlk/db231222/n/n;

    :goto_2
    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/n/P;->a(Ldbxyzptlk/db231222/n/n;)V

    .line 652
    invoke-virtual {p0}, Ldbxyzptlk/db231222/n/P;->v()V

    .line 653
    invoke-virtual {p0, v2}, Ldbxyzptlk/db231222/n/P;->h(Z)V

    .line 654
    invoke-virtual {p4, v1}, Lcom/dropbox/android/service/i;->a(Z)Z

    .line 657
    invoke-virtual {p4, p3}, Lcom/dropbox/android/service/i;->d(Z)V

    .line 659
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->aH()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 660
    return-void

    .line 639
    :cond_0
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->o()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v3, "cameraupload.uploadexisting"

    const-string v4, "auto_false"

    invoke-virtual {v0, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 645
    goto :goto_1

    .line 650
    :cond_2
    sget-object v0, Ldbxyzptlk/db231222/n/n;->c:Ldbxyzptlk/db231222/n/n;

    goto :goto_2
.end method

.method public final b(Lcom/dropbox/android/util/DropboxPath;)V
    .locals 2

    .prologue
    .line 356
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->u:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->c()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/J;->a(Ljava/lang/String;)V

    .line 357
    return-void
.end method

.method public final b(Ldbxyzptlk/db231222/n/z;)V
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->Q:Ldbxyzptlk/db231222/n/y;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/y;->a(Ldbxyzptlk/db231222/n/z;)Z

    .line 433
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->v:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/J;->a(Ljava/lang/String;)V

    .line 389
    return-void
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    .line 224
    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->i:Ldbxyzptlk/db231222/n/H;

    monitor-enter v1

    .line 225
    :try_start_0
    iget-object v2, p0, Ldbxyzptlk/db231222/n/P;->i:Ldbxyzptlk/db231222/n/H;

    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->i:Ldbxyzptlk/db231222/n/H;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/H;->d()Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/i;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/i;->n()Ldbxyzptlk/db231222/t/k;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/t/k;->b(Z)Ldbxyzptlk/db231222/t/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/k;->b()Ldbxyzptlk/db231222/t/i;

    move-result-object v0

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/n/H;->a(Ldbxyzptlk/db231222/H/t;)V

    .line 226
    monitor-exit v1

    .line 227
    return-void

    .line 226
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 195
    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->i:Ldbxyzptlk/db231222/n/H;

    monitor-enter v1

    .line 196
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->i:Ldbxyzptlk/db231222/n/H;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/H;->d()Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/i;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/i;->d()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 197
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c(Lcom/dropbox/android/util/DropboxPath;)V
    .locals 2

    .prologue
    .line 379
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->t:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->c()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/J;->a(Ljava/lang/String;)V

    .line 380
    return-void
.end method

.method public final c(Ldbxyzptlk/db231222/n/z;)V
    .locals 1

    .prologue
    .line 439
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->P:Ldbxyzptlk/db231222/n/y;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/y;->b(Ldbxyzptlk/db231222/n/z;)Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 440
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->w:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/J;->a(Ljava/lang/String;)V

    .line 398
    return-void
.end method

.method public final c(Z)V
    .locals 3

    .prologue
    .line 242
    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->i:Ldbxyzptlk/db231222/n/H;

    monitor-enter v1

    .line 243
    :try_start_0
    iget-object v2, p0, Ldbxyzptlk/db231222/n/P;->i:Ldbxyzptlk/db231222/n/H;

    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->i:Ldbxyzptlk/db231222/n/H;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/H;->d()Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/i;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/i;->n()Ldbxyzptlk/db231222/t/k;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/t/k;->c(Z)Ldbxyzptlk/db231222/t/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/k;->b()Ldbxyzptlk/db231222/t/i;

    move-result-object v0

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/n/H;->a(Ldbxyzptlk/db231222/H/t;)V

    .line 244
    monitor-exit v1

    .line 245
    return-void

    .line 244
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 215
    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->i:Ldbxyzptlk/db231222/n/H;

    monitor-enter v1

    .line 216
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->i:Ldbxyzptlk/db231222/n/H;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/H;->d()Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/i;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/i;->h()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 217
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d(Ldbxyzptlk/db231222/n/z;)V
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->Q:Ldbxyzptlk/db231222/n/y;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/y;->b(Ldbxyzptlk/db231222/n/z;)Z

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/util/C;->a(Z)V

    .line 447
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 603
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->O:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/J;->a(Ljava/lang/String;)V

    .line 604
    return-void
.end method

.method public final d(Z)V
    .locals 3

    .prologue
    .line 260
    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->i:Ldbxyzptlk/db231222/n/H;

    monitor-enter v1

    .line 261
    :try_start_0
    iget-object v2, p0, Ldbxyzptlk/db231222/n/P;->i:Ldbxyzptlk/db231222/n/H;

    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->i:Ldbxyzptlk/db231222/n/H;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/H;->d()Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/i;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/i;->n()Ldbxyzptlk/db231222/t/k;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/t/k;->d(Z)Ldbxyzptlk/db231222/t/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/k;->b()Ldbxyzptlk/db231222/t/i;

    move-result-object v0

    invoke-virtual {v2, v0}, Ldbxyzptlk/db231222/n/H;->a(Ldbxyzptlk/db231222/H/t;)V

    .line 262
    monitor-exit v1

    .line 263
    return-void

    .line 262
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 233
    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->i:Ldbxyzptlk/db231222/n/H;

    monitor-enter v1

    .line 234
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->i:Ldbxyzptlk/db231222/n/H;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/H;->d()Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/i;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/i;->j()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 235
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 663
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->M:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/J;->a(Ljava/lang/String;)V

    .line 664
    return-void
.end method

.method public final declared-synchronized e(Z)V
    .locals 3

    .prologue
    .line 408
    monitor-enter p0

    :try_start_0
    new-instance v0, Ldbxyzptlk/db231222/n/C;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/n/C;-><init>(Ldbxyzptlk/db231222/n/I;)V

    .line 409
    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->x:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v1, v0, p1}, Ldbxyzptlk/db231222/n/D;->a(Ldbxyzptlk/db231222/n/C;Z)V

    .line 411
    if-nez p1, :cond_0

    .line 412
    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->y:Ldbxyzptlk/db231222/n/D;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/db231222/n/D;->a(Ldbxyzptlk/db231222/n/C;Z)V

    .line 413
    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->z:Ldbxyzptlk/db231222/n/D;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/db231222/n/D;->a(Ldbxyzptlk/db231222/n/C;Z)V

    .line 417
    :goto_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/C;->a()Z

    .line 418
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->P:Ldbxyzptlk/db231222/n/y;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/y;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 419
    monitor-exit p0

    return-void

    .line 415
    :cond_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Ldbxyzptlk/db231222/n/P;->a(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 408
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 251
    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->i:Ldbxyzptlk/db231222/n/H;

    monitor-enter v1

    .line 252
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->i:Ldbxyzptlk/db231222/n/H;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/H;->d()Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/i;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/i;->l()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 253
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 676
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->N:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/J;->a(Ljava/lang/String;)V

    .line 677
    return-void
.end method

.method public final f(Z)V
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->z:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/D;->a(Z)V

    .line 455
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->g:Ldbxyzptlk/db231222/n/H;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/H;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->q:Ldbxyzptlk/db231222/n/G;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/G;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ldbxyzptlk/db231222/s/a;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 278
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->g:Ldbxyzptlk/db231222/n/H;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/H;->d()Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/s/a;

    .line 279
    if-eqz v0, :cond_0

    .line 318
    :goto_0
    return-object v0

    .line 284
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->q:Ldbxyzptlk/db231222/n/G;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/G;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 286
    invoke-static {}, Ldbxyzptlk/db231222/s/q;->m()Ldbxyzptlk/db231222/s/s;

    move-result-object v0

    .line 287
    iget-object v2, p0, Ldbxyzptlk/db231222/n/P;->q:Ldbxyzptlk/db231222/n/G;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/n/G;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/s/s;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/s/s;

    .line 288
    iget-object v2, p0, Ldbxyzptlk/db231222/n/P;->m:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v2}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/s/s;->b(Ljava/lang/String;)Ldbxyzptlk/db231222/s/s;

    .line 290
    invoke-static {}, Ldbxyzptlk/db231222/s/d;->k()Ldbxyzptlk/db231222/s/f;

    move-result-object v2

    .line 291
    iget-object v3, p0, Ldbxyzptlk/db231222/n/P;->n:Ldbxyzptlk/db231222/n/G;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/n/G;->d()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ldbxyzptlk/db231222/s/f;->a(J)Ldbxyzptlk/db231222/s/f;

    .line 292
    iget-object v3, p0, Ldbxyzptlk/db231222/n/P;->o:Ldbxyzptlk/db231222/n/G;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/n/G;->d()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ldbxyzptlk/db231222/s/f;->b(J)Ldbxyzptlk/db231222/s/f;

    .line 293
    iget-object v3, p0, Ldbxyzptlk/db231222/n/P;->p:Ldbxyzptlk/db231222/n/G;

    invoke-virtual {v3}, Ldbxyzptlk/db231222/n/G;->d()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ldbxyzptlk/db231222/s/f;->c(J)Ldbxyzptlk/db231222/s/f;

    .line 295
    invoke-static {}, Ldbxyzptlk/db231222/s/a;->I()Ldbxyzptlk/db231222/s/c;

    move-result-object v3

    .line 296
    invoke-virtual {v0}, Ldbxyzptlk/db231222/s/s;->b()Ldbxyzptlk/db231222/s/q;

    move-result-object v0

    invoke-virtual {v3, v0}, Ldbxyzptlk/db231222/s/c;->a(Ldbxyzptlk/db231222/s/q;)Ldbxyzptlk/db231222/s/c;

    .line 297
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->k:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ldbxyzptlk/db231222/s/c;->c(Ljava/lang/String;)Ldbxyzptlk/db231222/s/c;

    .line 298
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->j:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ldbxyzptlk/db231222/s/c;->d(Ljava/lang/String;)Ldbxyzptlk/db231222/s/c;

    .line 299
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->l:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ldbxyzptlk/db231222/s/c;->e(Ljava/lang/String;)Ldbxyzptlk/db231222/s/c;

    .line 300
    invoke-virtual {v2}, Ldbxyzptlk/db231222/s/f;->b()Ldbxyzptlk/db231222/s/d;

    move-result-object v0

    invoke-virtual {v3, v0}, Ldbxyzptlk/db231222/s/c;->a(Ldbxyzptlk/db231222/s/d;)Ldbxyzptlk/db231222/s/c;

    .line 303
    invoke-virtual {v3}, Ldbxyzptlk/db231222/s/c;->b()Ldbxyzptlk/db231222/s/a;

    move-result-object v0

    .line 304
    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/n/P;->a(Ldbxyzptlk/db231222/s/a;)V

    .line 307
    new-instance v2, Ldbxyzptlk/db231222/n/C;

    iget-object v3, p0, Ldbxyzptlk/db231222/n/P;->e:Ldbxyzptlk/db231222/n/I;

    invoke-direct {v2, v3}, Ldbxyzptlk/db231222/n/C;-><init>(Ldbxyzptlk/db231222/n/I;)V

    .line 308
    const/16 v3, 0x8

    new-array v3, v3, [Ldbxyzptlk/db231222/n/B;

    iget-object v4, p0, Ldbxyzptlk/db231222/n/P;->q:Ldbxyzptlk/db231222/n/G;

    aput-object v4, v3, v1

    const/4 v4, 0x1

    iget-object v5, p0, Ldbxyzptlk/db231222/n/P;->m:Ldbxyzptlk/db231222/n/J;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Ldbxyzptlk/db231222/n/P;->k:Ldbxyzptlk/db231222/n/J;

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-object v5, p0, Ldbxyzptlk/db231222/n/P;->l:Ldbxyzptlk/db231222/n/J;

    aput-object v5, v3, v4

    const/4 v4, 0x4

    iget-object v5, p0, Ldbxyzptlk/db231222/n/P;->j:Ldbxyzptlk/db231222/n/J;

    aput-object v5, v3, v4

    const/4 v4, 0x5

    iget-object v5, p0, Ldbxyzptlk/db231222/n/P;->n:Ldbxyzptlk/db231222/n/G;

    aput-object v5, v3, v4

    const/4 v4, 0x6

    iget-object v5, p0, Ldbxyzptlk/db231222/n/P;->o:Ldbxyzptlk/db231222/n/G;

    aput-object v5, v3, v4

    const/4 v4, 0x7

    iget-object v5, p0, Ldbxyzptlk/db231222/n/P;->p:Ldbxyzptlk/db231222/n/G;

    aput-object v5, v3, v4

    .line 309
    array-length v4, v3

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 310
    invoke-virtual {v5, v2}, Ldbxyzptlk/db231222/n/B;->b(Ldbxyzptlk/db231222/n/C;)V

    .line 309
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 313
    :cond_1
    invoke-virtual {v2}, Ldbxyzptlk/db231222/n/C;->a()Z

    goto/16 :goto_0

    .line 318
    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final g(Z)V
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->y:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/D;->a(Z)V

    .line 469
    return-void
.end method

.method public final h()Ldbxyzptlk/db231222/s/m;
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->h:Ldbxyzptlk/db231222/n/H;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/H;->d()Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/s/m;

    return-object v0
.end method

.method public final h(Z)V
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->A:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/D;->a(Z)V

    .line 506
    return-void
.end method

.method public final i()Lcom/dropbox/android/util/DropboxPath;
    .locals 2

    .prologue
    .line 343
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->r:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    return-object v0
.end method

.method public final i(Z)V
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->C:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/D;->a(Z)V

    .line 520
    return-void
.end method

.method public final j()Lcom/dropbox/android/util/DropboxPath;
    .locals 2

    .prologue
    .line 352
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->u:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    return-object v0
.end method

.method public final j(Z)V
    .locals 1

    .prologue
    .line 527
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->D:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/D;->a(Z)V

    .line 528
    return-void
.end method

.method public final k()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->s:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v0

    .line 362
    if-eqz v0, :cond_0

    .line 363
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 365
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k(Z)V
    .locals 1

    .prologue
    .line 539
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->E:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/D;->a(Z)V

    .line 540
    return-void
.end method

.method public final l()Lcom/dropbox/android/util/DropboxPath;
    .locals 2

    .prologue
    .line 375
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->t:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    return-object v0
.end method

.method public final l(Z)V
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->G:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/D;->a(Z)V

    .line 556
    return-void
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->v:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final m(Z)V
    .locals 1

    .prologue
    .line 583
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->H:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/D;->a(Z)V

    .line 584
    return-void
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->w:Ldbxyzptlk/db231222/n/J;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/J;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final n(Z)V
    .locals 1

    .prologue
    .line 591
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->F:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/D;->a(Z)V

    .line 592
    return-void
.end method

.method public final o(Z)V
    .locals 1

    .prologue
    .line 599
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->I:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/n/D;->a(Z)V

    .line 600
    return-void
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->x:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/D;->d()Z

    move-result v0

    return v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->z:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/D;->d()Z

    move-result v0

    return v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->y:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/D;->d()Z

    move-result v0

    return v0
.end method

.method public final r()Ldbxyzptlk/db231222/n/q;
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->K:Ldbxyzptlk/db231222/n/E;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/E;->d()Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/n/q;

    return-object v0
.end method

.method public final s()Ldbxyzptlk/db231222/n/n;
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->L:Ldbxyzptlk/db231222/n/E;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/E;->d()Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/n/n;

    return-object v0
.end method

.method public final t()J
    .locals 2

    .prologue
    .line 489
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->B:Ldbxyzptlk/db231222/n/G;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/G;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public final u()V
    .locals 5

    .prologue
    .line 493
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->B:Ldbxyzptlk/db231222/n/G;

    iget-object v1, p0, Ldbxyzptlk/db231222/n/P;->B:Ldbxyzptlk/db231222/n/G;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/n/G;->d()J

    move-result-wide v1

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/n/G;->a(J)V

    .line 494
    return-void
.end method

.method public final v()V
    .locals 3

    .prologue
    .line 497
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->B:Ldbxyzptlk/db231222/n/G;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/n/G;->a(J)V

    .line 498
    return-void
.end method

.method public final w()Z
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->A:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/D;->d()Z

    move-result v0

    return v0
.end method

.method public final x()Z
    .locals 1

    .prologue
    .line 515
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->C:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/D;->d()Z

    move-result v0

    return v0
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 523
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->D:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/D;->d()Z

    move-result v0

    return v0
.end method

.method public final z()Z
    .locals 1

    .prologue
    .line 531
    iget-object v0, p0, Ldbxyzptlk/db231222/n/P;->E:Ldbxyzptlk/db231222/n/D;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/D;->d()Z

    move-result v0

    return v0
.end method
