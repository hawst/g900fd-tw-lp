.class final Ldbxyzptlk/db231222/n/H;
.super Ldbxyzptlk/db231222/n/B;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ldbxyzptlk/db231222/H/t;",
        ">",
        "Ldbxyzptlk/db231222/n/B;"
    }
.end annotation


# instance fields
.field private final b:Ldbxyzptlk/db231222/H/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/H/w",
            "<+TT;>;"
        }
    .end annotation
.end field

.field private final c:Ldbxyzptlk/db231222/H/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ldbxyzptlk/db231222/H/t;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/n/I;",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 255
    invoke-static {p3}, Lcom/dropbox/android/util/aZ;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/w;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0, p3}, Ldbxyzptlk/db231222/n/H;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ldbxyzptlk/db231222/H/w;Ldbxyzptlk/db231222/H/t;)V

    .line 256
    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;Ldbxyzptlk/db231222/H/w;Ldbxyzptlk/db231222/H/t;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/n/I;",
            "Ljava/lang/String;",
            "Ldbxyzptlk/db231222/H/w",
            "<+TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 249
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/n/B;-><init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;)V

    .line 250
    iput-object p3, p0, Ldbxyzptlk/db231222/n/H;->b:Ldbxyzptlk/db231222/H/w;

    .line 251
    iput-object p4, p0, Ldbxyzptlk/db231222/n/H;->c:Ldbxyzptlk/db231222/H/t;

    .line 252
    return-void
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/H/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 264
    invoke-virtual {p0}, Ldbxyzptlk/db231222/n/H;->c()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/n/H;->a:Ljava/lang/String;

    invoke-static {p1}, Lcom/dropbox/android/util/aZ;->b(Ldbxyzptlk/db231222/H/t;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 265
    return-void
.end method

.method public final d()Ldbxyzptlk/db231222/H/t;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 259
    invoke-virtual {p0}, Ldbxyzptlk/db231222/n/H;->c()Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/n/H;->a:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 260
    if-eqz v0, :cond_0

    iget-object v1, p0, Ldbxyzptlk/db231222/n/H;->b:Ldbxyzptlk/db231222/H/w;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/aZ;->a(Ljava/lang/String;Ldbxyzptlk/db231222/H/w;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/H/t;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/n/H;->c:Ldbxyzptlk/db231222/H/t;

    goto :goto_0
.end method
