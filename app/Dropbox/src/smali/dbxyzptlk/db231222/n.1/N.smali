.class public final Ldbxyzptlk/db231222/n/N;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ldbxyzptlk/db231222/r/d;

.field private final b:Z

.field private final c:Z

.field private final d:Ldbxyzptlk/db231222/n/O;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldbxyzptlk/db231222/r/d;ZLdbxyzptlk/db231222/n/O;Z)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 35
    iput-object p2, p0, Ldbxyzptlk/db231222/n/N;->a:Ldbxyzptlk/db231222/r/d;

    .line 36
    iput-boolean p3, p0, Ldbxyzptlk/db231222/n/N;->b:Z

    .line 37
    iput-boolean p5, p0, Ldbxyzptlk/db231222/n/N;->c:Z

    .line 38
    iput-object p4, p0, Ldbxyzptlk/db231222/n/N;->d:Ldbxyzptlk/db231222/n/O;

    .line 39
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/n/N;->a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 43
    iget-boolean v0, p0, Ldbxyzptlk/db231222/n/N;->b:Z

    if-eqz v0, :cond_1

    .line 44
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/n/N;->a:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/filemanager/a;->a(Ldbxyzptlk/db231222/r/d;Z)V

    .line 49
    :goto_0
    iget-boolean v0, p0, Ldbxyzptlk/db231222/n/N;->c:Z

    if-eqz v0, :cond_0

    .line 50
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->y()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 52
    :cond_0
    invoke-static {p1}, Lcom/dropbox/android/util/Activities;->b(Landroid/content/Context;)V

    .line 53
    const/4 v0, 0x0

    return-object v0

    .line 46
    :cond_1
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/n/N;->a:Ldbxyzptlk/db231222/r/d;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/filemanager/a;->b(Ldbxyzptlk/db231222/r/d;Z)V

    goto :goto_0
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 58
    iget-boolean v0, p0, Ldbxyzptlk/db231222/n/N;->b:Z

    if-eqz v0, :cond_0

    instance-of v0, p2, Ldbxyzptlk/db231222/w/a;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Ldbxyzptlk/db231222/n/N;->d:Ldbxyzptlk/db231222/n/O;

    check-cast p2, Ldbxyzptlk/db231222/w/a;

    invoke-interface {v0, p2}, Ldbxyzptlk/db231222/n/O;->a(Ldbxyzptlk/db231222/w/a;)V

    .line 63
    return-void

    .line 61
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Error unlinking."

    invoke-direct {v0, v1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 14
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/n/N;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method
