.class public final Ldbxyzptlk/db231222/n/v;
.super Lcom/dropbox/android/provider/a;
.source "panda.py"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:Ldbxyzptlk/db231222/n/w;

.field private final e:Ldbxyzptlk/db231222/n/x;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 33
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "pref_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "pref_value"

    aput-object v2, v0, v1

    sput-object v0, Ldbxyzptlk/db231222/n/v;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ldbxyzptlk/db231222/n/w;)V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Ldbxyzptlk/db231222/n/v;-><init>(Landroid/content/Context;Ljava/lang/String;Ldbxyzptlk/db231222/n/w;Ldbxyzptlk/db231222/n/x;)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ldbxyzptlk/db231222/n/w;Ldbxyzptlk/db231222/n/x;)V
    .locals 3

    .prologue
    .line 70
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, p2, v1, v2}, Lcom/dropbox/android/provider/a;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 72
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/n/v;->b:Landroid/content/Context;

    .line 73
    iput-object p2, p0, Ldbxyzptlk/db231222/n/v;->c:Ljava/lang/String;

    .line 74
    iput-object p3, p0, Ldbxyzptlk/db231222/n/v;->d:Ldbxyzptlk/db231222/n/w;

    .line 76
    iput-object p4, p0, Ldbxyzptlk/db231222/n/v;->e:Ldbxyzptlk/db231222/n/x;

    .line 77
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CREATE TABLE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "pref_name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " TEXT NOT NULL UNIQUE, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "pref_value"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " TEXT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ");"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 91
    return-void
.end method


# virtual methods
.method final a(Ldbxyzptlk/db231222/n/w;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/n/w;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 129
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 130
    invoke-virtual {p0}, Ldbxyzptlk/db231222/n/v;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {p1}, Ldbxyzptlk/db231222/n/w;->a(Ldbxyzptlk/db231222/n/w;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/db231222/n/v;->a:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    move-object v8, v3

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 132
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v9, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 136
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 139
    return-object v9
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Ldbxyzptlk/db231222/n/v;->d:Ldbxyzptlk/db231222/n/w;

    invoke-static {v0}, Ldbxyzptlk/db231222/n/w;->a(Ldbxyzptlk/db231222/n/w;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    iget-object v0, p0, Ldbxyzptlk/db231222/n/v;->d:Ldbxyzptlk/db231222/n/w;

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/n/v;->a(Ldbxyzptlk/db231222/n/w;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 95
    sget-object v0, Ldbxyzptlk/db231222/n/w;->b:Ldbxyzptlk/db231222/n/w;

    invoke-static {v0}, Ldbxyzptlk/db231222/n/w;->a(Ldbxyzptlk/db231222/n/w;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Ldbxyzptlk/db231222/n/v;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 96
    sget-object v0, Ldbxyzptlk/db231222/n/w;->a:Ldbxyzptlk/db231222/n/w;

    invoke-static {v0}, Ldbxyzptlk/db231222/n/w;->a(Ldbxyzptlk/db231222/n/w;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Ldbxyzptlk/db231222/n/v;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Ldbxyzptlk/db231222/n/v;->e:Ldbxyzptlk/db231222/n/x;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Ldbxyzptlk/db231222/n/v;->e:Ldbxyzptlk/db231222/n/x;

    invoke-interface {v0, p1}, Ldbxyzptlk/db231222/n/x;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 114
    :cond_0
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    .prologue
    .line 118
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected upgrade."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Ldbxyzptlk/db231222/n/v;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/n/v;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
