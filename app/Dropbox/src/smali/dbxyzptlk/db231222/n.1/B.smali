.class abstract Ldbxyzptlk/db231222/n/B;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected final a:Ljava/lang/String;

.field private final c:Ldbxyzptlk/db231222/n/I;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 68
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "CAMERA_UPLOADS_ALBUM_REV"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "LAST_RESYNC_TS"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "PHOTO_SELECT_POPUP_DISMISSED_NEW"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "BROMOS_DISMISSED"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "BROMOS_METADATA"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "SHARING_FLOW_STAGE"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "CAMERA_UPLOAD_BROMO_MAX_IDS"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "CAMERA_UPLOAD_BROMO_COUNT"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "CAMERA_UPLOAD_SEEN_BROMO"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "SEEN_TOUR"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Ldbxyzptlk/db231222/n/B;->b:Ljava/util/Set;

    return-void
.end method

.method protected constructor <init>(Ldbxyzptlk/db231222/n/I;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    sget-object v0, Ldbxyzptlk/db231222/n/B;->b:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is a reserved key and should not be used"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 91
    :cond_0
    iput-object p1, p0, Ldbxyzptlk/db231222/n/B;->c:Ldbxyzptlk/db231222/n/I;

    .line 92
    iput-object p2, p0, Ldbxyzptlk/db231222/n/B;->a:Ljava/lang/String;

    .line 93
    return-void
.end method


# virtual methods
.method protected final a(Ldbxyzptlk/db231222/n/C;)Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Ldbxyzptlk/db231222/n/B;->c:Ldbxyzptlk/db231222/n/I;

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/n/C;->a(Ldbxyzptlk/db231222/n/I;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    return-object v0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 99
    invoke-virtual {p0}, Ldbxyzptlk/db231222/n/B;->c()Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/n/B;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 103
    invoke-virtual {p0}, Ldbxyzptlk/db231222/n/B;->c()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/n/B;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 104
    return-void
.end method

.method public final b(Ldbxyzptlk/db231222/n/C;)V
    .locals 2

    .prologue
    .line 119
    invoke-virtual {p0, p1}, Ldbxyzptlk/db231222/n/B;->a(Ldbxyzptlk/db231222/n/C;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/n/B;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 120
    return-void
.end method

.method protected final c()Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Ldbxyzptlk/db231222/n/B;->c:Ldbxyzptlk/db231222/n/I;

    invoke-interface {v0}, Ldbxyzptlk/db231222/n/I;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method
