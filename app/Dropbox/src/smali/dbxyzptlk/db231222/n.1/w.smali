.class public final enum Ldbxyzptlk/db231222/n/w;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldbxyzptlk/db231222/n/w;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ldbxyzptlk/db231222/n/w;

.field public static final enum b:Ldbxyzptlk/db231222/n/w;

.field private static final synthetic d:[Ldbxyzptlk/db231222/n/w;


# instance fields
.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 154
    new-instance v0, Ldbxyzptlk/db231222/n/w;

    const-string v1, "ACCOUNT"

    const-string v2, "DropboxAccountPrefs"

    invoke-direct {v0, v1, v3, v2}, Ldbxyzptlk/db231222/n/w;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/n/w;->a:Ldbxyzptlk/db231222/n/w;

    .line 157
    new-instance v0, Ldbxyzptlk/db231222/n/w;

    const-string v1, "PERSISTENT"

    const-string v2, "DropboxPersistentPrefs"

    invoke-direct {v0, v1, v4, v2}, Ldbxyzptlk/db231222/n/w;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/n/w;->b:Ldbxyzptlk/db231222/n/w;

    .line 148
    const/4 v0, 0x2

    new-array v0, v0, [Ldbxyzptlk/db231222/n/w;

    sget-object v1, Ldbxyzptlk/db231222/n/w;->a:Ldbxyzptlk/db231222/n/w;

    aput-object v1, v0, v3

    sget-object v1, Ldbxyzptlk/db231222/n/w;->b:Ldbxyzptlk/db231222/n/w;

    aput-object v1, v0, v4

    sput-object v0, Ldbxyzptlk/db231222/n/w;->d:[Ldbxyzptlk/db231222/n/w;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 161
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 162
    iput-object p3, p0, Ldbxyzptlk/db231222/n/w;->c:Ljava/lang/String;

    .line 163
    return-void
.end method

.method static synthetic a(Ldbxyzptlk/db231222/n/w;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Ldbxyzptlk/db231222/n/w;->c:Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Ldbxyzptlk/db231222/n/w;
    .locals 1

    .prologue
    .line 148
    const-class v0, Ldbxyzptlk/db231222/n/w;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/n/w;

    return-object v0
.end method

.method public static values()[Ldbxyzptlk/db231222/n/w;
    .locals 1

    .prologue
    .line 148
    sget-object v0, Ldbxyzptlk/db231222/n/w;->d:[Ldbxyzptlk/db231222/n/w;

    invoke-virtual {v0}, [Ldbxyzptlk/db231222/n/w;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/n/w;

    return-object v0
.end method
