.class public Ldbxyzptlk/db231222/n/a;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/content/SharedPreferences;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final e:Ldbxyzptlk/db231222/n/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/n/j",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Ldbxyzptlk/db231222/n/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/n/j",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static final g:Ldbxyzptlk/db231222/n/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/n/j",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final h:Ldbxyzptlk/db231222/n/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/n/j",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final i:Ldbxyzptlk/db231222/n/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/n/j",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final j:Ldbxyzptlk/db231222/n/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/n/j",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final k:Ljava/lang/Object;


# instance fields
.field private final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ldbxyzptlk/db231222/n/v;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Ldbxyzptlk/db231222/n/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/n/a;->a:Ljava/lang/String;

    .line 127
    new-instance v0, Ldbxyzptlk/db231222/n/b;

    invoke-direct {v0}, Ldbxyzptlk/db231222/n/b;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/n/a;->e:Ldbxyzptlk/db231222/n/j;

    .line 135
    new-instance v0, Ldbxyzptlk/db231222/n/c;

    invoke-direct {v0}, Ldbxyzptlk/db231222/n/c;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/n/a;->f:Ldbxyzptlk/db231222/n/j;

    .line 143
    new-instance v0, Ldbxyzptlk/db231222/n/d;

    invoke-direct {v0}, Ldbxyzptlk/db231222/n/d;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/n/a;->g:Ldbxyzptlk/db231222/n/j;

    .line 151
    new-instance v0, Ldbxyzptlk/db231222/n/e;

    invoke-direct {v0}, Ldbxyzptlk/db231222/n/e;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/n/a;->h:Ldbxyzptlk/db231222/n/j;

    .line 159
    new-instance v0, Ldbxyzptlk/db231222/n/f;

    invoke-direct {v0}, Ldbxyzptlk/db231222/n/f;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/n/a;->i:Ldbxyzptlk/db231222/n/j;

    .line 167
    new-instance v0, Ldbxyzptlk/db231222/n/g;

    invoke-direct {v0}, Ldbxyzptlk/db231222/n/g;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/n/a;->j:Ldbxyzptlk/db231222/n/j;

    .line 360
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/n/a;->k:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/db231222/n/v;)V
    .locals 5

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/a;->b:Ljava/util/HashMap;

    .line 43
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/n/a;->c:Ljava/util/HashSet;

    .line 47
    iput-object p1, p0, Ldbxyzptlk/db231222/n/a;->d:Ldbxyzptlk/db231222/n/v;

    .line 51
    invoke-virtual {p0}, Ldbxyzptlk/db231222/n/a;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 52
    iget-object v2, p0, Ldbxyzptlk/db231222/n/a;->b:Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    new-instance v4, Ldbxyzptlk/db231222/n/i;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v4, v0}, Ldbxyzptlk/db231222/n/i;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 54
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ldbxyzptlk/db231222/n/j;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ldbxyzptlk/db231222/n/j",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 181
    iget-object v2, p0, Ldbxyzptlk/db231222/n/a;->b:Ljava/util/HashMap;

    monitor-enter v2

    .line 182
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/n/a;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 183
    if-eqz v0, :cond_0

    instance-of v3, v0, Ldbxyzptlk/db231222/n/i;

    if-nez v3, :cond_0

    .line 189
    monitor-exit v2

    .line 200
    :goto_0
    return-object v0

    .line 192
    :cond_0
    instance-of v3, v0, Ldbxyzptlk/db231222/n/i;

    if-eqz v3, :cond_2

    .line 193
    check-cast v0, Ldbxyzptlk/db231222/n/i;

    iget-object v0, v0, Ldbxyzptlk/db231222/n/i;->a:Ljava/lang/String;

    .line 195
    :goto_1
    if-nez v0, :cond_1

    .line 196
    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    .line 198
    :cond_1
    invoke-interface {p2, v0}, Ldbxyzptlk/db231222/n/j;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 199
    iget-object v1, p0, Ldbxyzptlk/db231222/n/a;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    monitor-exit v2

    goto :goto_0

    .line 201
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method static synthetic a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    invoke-static {p0}, Ldbxyzptlk/db231222/n/a;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/Set;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 269
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 270
    const/4 v0, 0x1

    .line 271
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 273
    const-string v4, "\\^"

    const-string v5, "^^"

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 275
    const-string v4, "~"

    const-string v5, "^t"

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 276
    if-nez v1, :cond_0

    .line 277
    const/16 v0, 0x7e

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v0, v1

    .line 281
    :goto_1
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v0

    .line 282
    goto :goto_0

    .line 279
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 283
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/n/a;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Ldbxyzptlk/db231222/n/a;->b:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 38
    invoke-static {p0}, Ldbxyzptlk/db231222/n/a;->b(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/n/a;Ljava/util/Collection;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/n/a;->a(Ljava/util/Collection;)V

    return-void
.end method

.method private a(Ljava/util/Collection;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 61
    iget-object v1, p0, Ldbxyzptlk/db231222/n/a;->c:Ljava/util/HashSet;

    monitor-enter v1

    .line 62
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Ldbxyzptlk/db231222/n/a;->c:Ljava/util/HashSet;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 63
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 65
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 66
    invoke-interface {v1, p0, v0}, Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    goto :goto_0

    .line 63
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 69
    :cond_1
    return-void
.end method

.method static synthetic b(Ldbxyzptlk/db231222/n/a;)Ldbxyzptlk/db231222/n/v;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Ldbxyzptlk/db231222/n/a;->d:Ldbxyzptlk/db231222/n/v;

    return-object v0
.end method

.method private static b(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 364
    instance-of v0, p0, Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 366
    check-cast p0, Ljava/util/Set;

    .line 367
    invoke-static {p0}, Ldbxyzptlk/db231222/n/a;->a(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v0

    .line 369
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 252
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 255
    const-string v0, "~"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 256
    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 258
    const-string v5, "\\^t"

    const-string v6, "~"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 260
    const-string v5, "\\^\\^"

    const-string v6, "^"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 261
    invoke-virtual {v1, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 256
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 264
    :cond_0
    return-object v1
.end method

.method static synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Ldbxyzptlk/db231222/n/a;->k:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Ldbxyzptlk/db231222/n/a;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Ldbxyzptlk/db231222/n/a;->d:Ldbxyzptlk/db231222/n/v;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/v;->a()V

    .line 83
    return-void
.end method

.method public final a(Landroid/content/SharedPreferences;)V
    .locals 6

    .prologue
    .line 313
    invoke-interface {p1}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 315
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 316
    invoke-virtual {p0}, Ldbxyzptlk/db231222/n/a;->c()Ldbxyzptlk/db231222/n/h;

    move-result-object v2

    .line 318
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 319
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 320
    instance-of v4, v1, Ljava/lang/Boolean;

    if-eqz v4, :cond_0

    .line 321
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v2, v0, v1}, Ldbxyzptlk/db231222/n/h;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 322
    :cond_0
    instance-of v4, v1, Ljava/lang/Float;

    if-eqz v4, :cond_1

    .line 323
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v2, v0, v1}, Ldbxyzptlk/db231222/n/h;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 324
    :cond_1
    instance-of v4, v1, Ljava/lang/Integer;

    if-eqz v4, :cond_2

    .line 325
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v0, v1}, Ldbxyzptlk/db231222/n/h;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 326
    :cond_2
    instance-of v4, v1, Ljava/lang/Long;

    if-eqz v4, :cond_3

    .line 327
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, Ldbxyzptlk/db231222/n/h;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 328
    :cond_3
    instance-of v4, v1, Ljava/lang/String;

    if-eqz v4, :cond_4

    .line 329
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Ldbxyzptlk/db231222/n/h;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 330
    :cond_4
    instance-of v4, v1, Ljava/util/Set;

    if-eqz v4, :cond_5

    .line 332
    check-cast v1, Ljava/util/Set;

    .line 333
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Ldbxyzptlk/db231222/n/h;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 335
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown setting type during migration: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 340
    :cond_6
    invoke-virtual {v2}, Ldbxyzptlk/db231222/n/h;->commit()Z

    .line 343
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 345
    :cond_7
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Ldbxyzptlk/db231222/n/a;->d:Ldbxyzptlk/db231222/n/v;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/v;->close()V

    .line 90
    return-void
.end method

.method public final c()Ldbxyzptlk/db231222/n/h;
    .locals 1

    .prologue
    .line 95
    new-instance v0, Ldbxyzptlk/db231222/n/h;

    invoke-direct {v0, p0}, Ldbxyzptlk/db231222/n/h;-><init>(Ldbxyzptlk/db231222/n/a;)V

    return-object v0
.end method

.method public contains(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 73
    iget-object v1, p0, Ldbxyzptlk/db231222/n/a;->b:Ljava/util/HashMap;

    monitor-enter v1

    .line 74
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/n/a;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final d()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Ldbxyzptlk/db231222/n/a;->d:Ldbxyzptlk/db231222/n/v;

    sget-object v1, Ldbxyzptlk/db231222/n/w;->a:Ldbxyzptlk/db231222/n/w;

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/n/v;->a(Ldbxyzptlk/db231222/n/w;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public synthetic edit()Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Ldbxyzptlk/db231222/n/a;->c()Ldbxyzptlk/db231222/n/h;

    move-result-object v0

    return-object v0
.end method

.method public getAll()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Ldbxyzptlk/db231222/n/a;->d:Ldbxyzptlk/db231222/n/v;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/n/v;->c()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getBoolean(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 206
    sget-object v0, Ldbxyzptlk/db231222/n/a;->e:Ldbxyzptlk/db231222/n/j;

    invoke-direct {p0, p1, v0}, Ldbxyzptlk/db231222/n/a;->a(Ljava/lang/String;Ldbxyzptlk/db231222/n/j;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 207
    if-nez v0, :cond_0

    .line 210
    :goto_0
    return p2

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    goto :goto_0
.end method

.method public getFloat(Ljava/lang/String;F)F
    .locals 1

    .prologue
    .line 215
    sget-object v0, Ldbxyzptlk/db231222/n/a;->f:Ldbxyzptlk/db231222/n/j;

    invoke-direct {p0, p1, v0}, Ldbxyzptlk/db231222/n/a;->a(Ljava/lang/String;Ldbxyzptlk/db231222/n/j;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 216
    if-nez v0, :cond_0

    .line 219
    :goto_0
    return p2

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result p2

    goto :goto_0
.end method

.method public getInt(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 224
    sget-object v0, Ldbxyzptlk/db231222/n/a;->g:Ldbxyzptlk/db231222/n/j;

    invoke-direct {p0, p1, v0}, Ldbxyzptlk/db231222/n/a;->a(Ljava/lang/String;Ldbxyzptlk/db231222/n/j;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 225
    if-nez v0, :cond_0

    .line 228
    :goto_0
    return p2

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p2

    goto :goto_0
.end method

.method public getLong(Ljava/lang/String;J)J
    .locals 1

    .prologue
    .line 233
    sget-object v0, Ldbxyzptlk/db231222/n/a;->h:Ldbxyzptlk/db231222/n/j;

    invoke-direct {p0, p1, v0}, Ldbxyzptlk/db231222/n/a;->a(Ljava/lang/String;Ldbxyzptlk/db231222/n/j;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 234
    if-nez v0, :cond_0

    .line 237
    :goto_0
    return-wide p2

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    goto :goto_0
.end method

.method public getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 243
    sget-object v0, Ldbxyzptlk/db231222/n/a;->i:Ldbxyzptlk/db231222/n/j;

    invoke-direct {p0, p1, v0}, Ldbxyzptlk/db231222/n/a;->a(Ljava/lang/String;Ldbxyzptlk/db231222/n/j;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 244
    if-nez v0, :cond_0

    .line 247
    :goto_0
    return-object p2

    :cond_0
    move-object p2, v0

    goto :goto_0
.end method

.method public getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 289
    sget-object v0, Ldbxyzptlk/db231222/n/a;->j:Ldbxyzptlk/db231222/n/j;

    invoke-direct {p0, p1, v0}, Ldbxyzptlk/db231222/n/a;->a(Ljava/lang/String;Ldbxyzptlk/db231222/n/j;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 290
    if-nez v0, :cond_0

    .line 293
    :goto_0
    return-object p2

    :cond_0
    move-object p2, v0

    goto :goto_0
.end method

.method public registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .locals 2

    .prologue
    .line 299
    iget-object v1, p0, Ldbxyzptlk/db231222/n/a;->c:Ljava/util/HashSet;

    monitor-enter v1

    .line 300
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/n/a;->c:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 301
    monitor-exit v1

    .line 302
    return-void

    .line 301
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .locals 2

    .prologue
    .line 307
    iget-object v1, p0, Ldbxyzptlk/db231222/n/a;->c:Ljava/util/HashSet;

    monitor-enter v1

    .line 308
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/n/a;->c:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 309
    monitor-exit v1

    .line 310
    return-void

    .line 309
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
