.class public final enum Ldbxyzptlk/db231222/n/t;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldbxyzptlk/db231222/n/t;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ldbxyzptlk/db231222/n/t;

.field public static final enum b:Ldbxyzptlk/db231222/n/t;

.field private static final synthetic c:[Ldbxyzptlk/db231222/n/t;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 79
    new-instance v0, Ldbxyzptlk/db231222/n/t;

    const-string v1, "SMS"

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/n/t;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbxyzptlk/db231222/n/t;->a:Ldbxyzptlk/db231222/n/t;

    .line 80
    new-instance v0, Ldbxyzptlk/db231222/n/t;

    const-string v1, "OFFLINE"

    invoke-direct {v0, v1, v3}, Ldbxyzptlk/db231222/n/t;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbxyzptlk/db231222/n/t;->b:Ldbxyzptlk/db231222/n/t;

    .line 78
    const/4 v0, 0x2

    new-array v0, v0, [Ldbxyzptlk/db231222/n/t;

    sget-object v1, Ldbxyzptlk/db231222/n/t;->a:Ldbxyzptlk/db231222/n/t;

    aput-object v1, v0, v2

    sget-object v1, Ldbxyzptlk/db231222/n/t;->b:Ldbxyzptlk/db231222/n/t;

    aput-object v1, v0, v3

    sput-object v0, Ldbxyzptlk/db231222/n/t;->c:[Ldbxyzptlk/db231222/n/t;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Ldbxyzptlk/db231222/n/t;
    .locals 3

    .prologue
    .line 83
    if-nez p0, :cond_0

    .line 84
    sget-object v0, Ldbxyzptlk/db231222/n/t;->a:Ldbxyzptlk/db231222/n/t;

    .line 91
    :goto_0
    return-object v0

    .line 88
    :cond_0
    :try_start_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/n/t;->valueOf(Ljava/lang/String;)Ldbxyzptlk/db231222/n/t;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    invoke-static {}, Ldbxyzptlk/db231222/n/k;->o()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown twofactor delivery mode ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), defaulting to SMS"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    sget-object v0, Ldbxyzptlk/db231222/n/t;->a:Ldbxyzptlk/db231222/n/t;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Ldbxyzptlk/db231222/n/t;
    .locals 1

    .prologue
    .line 78
    const-class v0, Ldbxyzptlk/db231222/n/t;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/n/t;

    return-object v0
.end method

.method public static values()[Ldbxyzptlk/db231222/n/t;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Ldbxyzptlk/db231222/n/t;->c:[Ldbxyzptlk/db231222/n/t;

    invoke-virtual {v0}, [Ldbxyzptlk/db231222/n/t;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/n/t;

    return-object v0
.end method
