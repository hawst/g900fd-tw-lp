.class public final Ldbxyzptlk/db231222/t/a;
.super Ldbxyzptlk/db231222/H/j;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/t/d;


# static fields
.field public static a:Ldbxyzptlk/db231222/H/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/H/w",
            "<",
            "Ldbxyzptlk/db231222/t/a;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ldbxyzptlk/db231222/t/a;

.field private static final serialVersionUID:J


# instance fields
.field private c:I

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:I

.field private g:I

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 80
    new-instance v0, Ldbxyzptlk/db231222/t/b;

    invoke-direct {v0}, Ldbxyzptlk/db231222/t/b;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/t/a;->a:Ldbxyzptlk/db231222/H/w;

    .line 800
    new-instance v0, Ldbxyzptlk/db231222/t/a;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/t/a;-><init>(Z)V

    sput-object v0, Ldbxyzptlk/db231222/t/a;->b:Ldbxyzptlk/db231222/t/a;

    .line 801
    sget-object v0, Ldbxyzptlk/db231222/t/a;->b:Ldbxyzptlk/db231222/t/a;

    invoke-direct {v0}, Ldbxyzptlk/db231222/t/a;->p()V

    .line 802
    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 31
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/j;-><init>()V

    .line 260
    iput-byte v0, p0, Ldbxyzptlk/db231222/t/a;->h:B

    .line 290
    iput v0, p0, Ldbxyzptlk/db231222/t/a;->i:I

    .line 32
    invoke-direct {p0}, Ldbxyzptlk/db231222/t/a;->p()V

    .line 35
    const/4 v0, 0x0

    .line 36
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 37
    :try_start_0
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->a()I

    move-result v2

    .line 38
    sparse-switch v2, :sswitch_data_0

    .line 43
    invoke-virtual {p0, p1, p2, v2}, Ldbxyzptlk/db231222/t/a;->a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 45
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 41
    goto :goto_0

    .line 50
    :sswitch_1
    iget v2, p0, Ldbxyzptlk/db231222/t/a;->c:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Ldbxyzptlk/db231222/t/a;->c:I

    .line 51
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->g()Ldbxyzptlk/db231222/H/d;

    move-result-object v2

    iput-object v2, p0, Ldbxyzptlk/db231222/t/a;->d:Ljava/lang/Object;
    :try_end_0
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 71
    :catch_0
    move-exception v0

    .line 72
    :try_start_1
    invoke-virtual {v0, p0}, Ldbxyzptlk/db231222/H/o;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/o;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/a;->L()V

    throw v0

    .line 55
    :sswitch_2
    :try_start_2
    iget v2, p0, Ldbxyzptlk/db231222/t/a;->c:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Ldbxyzptlk/db231222/t/a;->c:I

    .line 56
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->g()Ldbxyzptlk/db231222/H/d;

    move-result-object v2

    iput-object v2, p0, Ldbxyzptlk/db231222/t/a;->e:Ljava/lang/Object;
    :try_end_2
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 73
    :catch_1
    move-exception v0

    .line 74
    :try_start_3
    new-instance v1, Ldbxyzptlk/db231222/H/o;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/H/o;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ldbxyzptlk/db231222/H/o;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/o;

    move-result-object v0

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 60
    :sswitch_3
    :try_start_4
    iget v2, p0, Ldbxyzptlk/db231222/t/a;->c:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Ldbxyzptlk/db231222/t/a;->c:I

    .line 61
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->e()I

    move-result v2

    iput v2, p0, Ldbxyzptlk/db231222/t/a;->f:I

    goto :goto_0

    .line 65
    :sswitch_4
    iget v2, p0, Ldbxyzptlk/db231222/t/a;->c:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Ldbxyzptlk/db231222/t/a;->c:I

    .line 66
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->e()I

    move-result v2

    iput v2, p0, Ldbxyzptlk/db231222/t/a;->g:I
    :try_end_4
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 77
    :cond_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/a;->L()V

    .line 79
    return-void

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method synthetic constructor <init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;Ldbxyzptlk/db231222/t/b;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/t/a;-><init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)V

    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/H/k;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 14
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/H/j;-><init>(Ldbxyzptlk/db231222/H/k;)V

    .line 260
    iput-byte v0, p0, Ldbxyzptlk/db231222/t/a;->h:B

    .line 290
    iput v0, p0, Ldbxyzptlk/db231222/t/a;->i:I

    .line 16
    return-void
.end method

.method synthetic constructor <init>(Ldbxyzptlk/db231222/H/k;Ldbxyzptlk/db231222/t/b;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/t/a;-><init>(Ldbxyzptlk/db231222/H/k;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/j;-><init>()V

    .line 260
    iput-byte v0, p0, Ldbxyzptlk/db231222/t/a;->h:B

    .line 290
    iput v0, p0, Ldbxyzptlk/db231222/t/a;->i:I

    .line 17
    return-void
.end method

.method static synthetic a(Ldbxyzptlk/db231222/t/a;I)I
    .locals 0

    .prologue
    .line 9
    iput p1, p0, Ldbxyzptlk/db231222/t/a;->f:I

    return p1
.end method

.method public static a()Ldbxyzptlk/db231222/t/a;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Ldbxyzptlk/db231222/t/a;->b:Ldbxyzptlk/db231222/t/a;

    return-object v0
.end method

.method public static a(Ldbxyzptlk/db231222/t/a;)Ldbxyzptlk/db231222/t/c;
    .locals 1

    .prologue
    .line 379
    invoke-static {}, Ldbxyzptlk/db231222/t/a;->o()Ldbxyzptlk/db231222/t/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Ldbxyzptlk/db231222/t/c;->a(Ldbxyzptlk/db231222/t/a;)Ldbxyzptlk/db231222/t/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/t/a;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 9
    iput-object p1, p0, Ldbxyzptlk/db231222/t/a;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Ldbxyzptlk/db231222/t/a;I)I
    .locals 0

    .prologue
    .line 9
    iput p1, p0, Ldbxyzptlk/db231222/t/a;->g:I

    return p1
.end method

.method static synthetic b(Ldbxyzptlk/db231222/t/a;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Ldbxyzptlk/db231222/t/a;->d:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Ldbxyzptlk/db231222/t/a;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 9
    iput-object p1, p0, Ldbxyzptlk/db231222/t/a;->e:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic c(Ldbxyzptlk/db231222/t/a;I)I
    .locals 0

    .prologue
    .line 9
    iput p1, p0, Ldbxyzptlk/db231222/t/a;->c:I

    return p1
.end method

.method static synthetic c(Ldbxyzptlk/db231222/t/a;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Ldbxyzptlk/db231222/t/a;->e:Ljava/lang/Object;

    return-object v0
.end method

.method public static o()Ldbxyzptlk/db231222/t/c;
    .locals 1

    .prologue
    .line 376
    invoke-static {}, Ldbxyzptlk/db231222/t/c;->f()Ldbxyzptlk/db231222/t/c;

    move-result-object v0

    return-object v0
.end method

.method private p()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 255
    const-string v0, ""

    iput-object v0, p0, Ldbxyzptlk/db231222/t/a;->d:Ljava/lang/Object;

    .line 256
    const-string v0, ""

    iput-object v0, p0, Ldbxyzptlk/db231222/t/a;->e:Ljava/lang/Object;

    .line 257
    iput v1, p0, Ldbxyzptlk/db231222/t/a;->f:I

    .line 258
    iput v1, p0, Ldbxyzptlk/db231222/t/a;->g:I

    .line 259
    return-void
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/H/g;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 275
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/a;->f()I

    .line 276
    iget v0, p0, Ldbxyzptlk/db231222/t/a;->c:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 277
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/a;->g()Ldbxyzptlk/db231222/H/d;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Ldbxyzptlk/db231222/H/g;->a(ILdbxyzptlk/db231222/H/d;)V

    .line 279
    :cond_0
    iget v0, p0, Ldbxyzptlk/db231222/t/a;->c:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 280
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/a;->j()Ldbxyzptlk/db231222/H/d;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Ldbxyzptlk/db231222/H/g;->a(ILdbxyzptlk/db231222/H/d;)V

    .line 282
    :cond_1
    iget v0, p0, Ldbxyzptlk/db231222/t/a;->c:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 283
    const/4 v0, 0x3

    iget v1, p0, Ldbxyzptlk/db231222/t/a;->f:I

    invoke-virtual {p1, v0, v1}, Ldbxyzptlk/db231222/H/g;->a(II)V

    .line 285
    :cond_2
    iget v0, p0, Ldbxyzptlk/db231222/t/a;->c:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 286
    iget v0, p0, Ldbxyzptlk/db231222/t/a;->g:I

    invoke-virtual {p1, v3, v0}, Ldbxyzptlk/db231222/H/g;->a(II)V

    .line 288
    :cond_3
    return-void
.end method

.method public final b()Ldbxyzptlk/db231222/H/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldbxyzptlk/db231222/H/w",
            "<",
            "Ldbxyzptlk/db231222/t/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    sget-object v0, Ldbxyzptlk/db231222/t/a;->a:Ldbxyzptlk/db231222/H/w;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 107
    iget v1, p0, Ldbxyzptlk/db231222/t/a;->c:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Ldbxyzptlk/db231222/t/a;->d:Ljava/lang/Object;

    .line 118
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 119
    check-cast v0, Ljava/lang/String;

    .line 127
    :goto_0
    return-object v0

    .line 121
    :cond_0
    check-cast v0, Ldbxyzptlk/db231222/H/d;

    .line 123
    invoke-virtual {v0}, Ldbxyzptlk/db231222/H/d;->e()Ljava/lang/String;

    move-result-object v1

    .line 124
    invoke-virtual {v0}, Ldbxyzptlk/db231222/H/d;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 125
    iput-object v1, p0, Ldbxyzptlk/db231222/t/a;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 127
    goto :goto_0
.end method

.method public final e()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 262
    iget-byte v2, p0, Ldbxyzptlk/db231222/t/a;->h:B

    .line 263
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    if-ne v2, v0, :cond_0

    .line 270
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 263
    goto :goto_0

    .line 265
    :cond_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/a;->c()Z

    move-result v2

    if-nez v2, :cond_2

    .line 266
    iput-byte v1, p0, Ldbxyzptlk/db231222/t/a;->h:B

    move v0, v1

    .line 267
    goto :goto_0

    .line 269
    :cond_2
    iput-byte v0, p0, Ldbxyzptlk/db231222/t/a;->h:B

    goto :goto_0
.end method

.method public final f()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 292
    iget v0, p0, Ldbxyzptlk/db231222/t/a;->i:I

    .line 293
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 313
    :goto_0
    return v0

    .line 295
    :cond_0
    const/4 v0, 0x0

    .line 296
    iget v1, p0, Ldbxyzptlk/db231222/t/a;->c:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 297
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/a;->g()Ldbxyzptlk/db231222/H/d;

    move-result-object v1

    invoke-static {v2, v1}, Ldbxyzptlk/db231222/H/g;->b(ILdbxyzptlk/db231222/H/d;)I

    move-result v1

    add-int/2addr v0, v1

    .line 300
    :cond_1
    iget v1, p0, Ldbxyzptlk/db231222/t/a;->c:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 301
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/a;->j()Ldbxyzptlk/db231222/H/d;

    move-result-object v1

    invoke-static {v3, v1}, Ldbxyzptlk/db231222/H/g;->b(ILdbxyzptlk/db231222/H/d;)I

    move-result v1

    add-int/2addr v0, v1

    .line 304
    :cond_2
    iget v1, p0, Ldbxyzptlk/db231222/t/a;->c:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 305
    const/4 v1, 0x3

    iget v2, p0, Ldbxyzptlk/db231222/t/a;->f:I

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/H/g;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 308
    :cond_3
    iget v1, p0, Ldbxyzptlk/db231222/t/a;->c:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 309
    iget v1, p0, Ldbxyzptlk/db231222/t/a;->g:I

    invoke-static {v4, v1}, Ldbxyzptlk/db231222/H/g;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 312
    :cond_4
    iput v0, p0, Ldbxyzptlk/db231222/t/a;->i:I

    goto :goto_0
.end method

.method public final g()Ldbxyzptlk/db231222/H/d;
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Ldbxyzptlk/db231222/t/a;->d:Ljava/lang/Object;

    .line 140
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 141
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ldbxyzptlk/db231222/H/d;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/H/d;

    move-result-object v0

    .line 144
    iput-object v0, p0, Ldbxyzptlk/db231222/t/a;->d:Ljava/lang/Object;

    .line 147
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ldbxyzptlk/db231222/H/d;

    goto :goto_0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 162
    iget v0, p0, Ldbxyzptlk/db231222/t/a;->c:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Ldbxyzptlk/db231222/t/a;->e:Ljava/lang/Object;

    .line 173
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 174
    check-cast v0, Ljava/lang/String;

    .line 182
    :goto_0
    return-object v0

    .line 176
    :cond_0
    check-cast v0, Ldbxyzptlk/db231222/H/d;

    .line 178
    invoke-virtual {v0}, Ldbxyzptlk/db231222/H/d;->e()Ljava/lang/String;

    move-result-object v1

    .line 179
    invoke-virtual {v0}, Ldbxyzptlk/db231222/H/d;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 180
    iput-object v1, p0, Ldbxyzptlk/db231222/t/a;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 182
    goto :goto_0
.end method

.method public final j()Ldbxyzptlk/db231222/H/d;
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Ldbxyzptlk/db231222/t/a;->e:Ljava/lang/Object;

    .line 195
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 196
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ldbxyzptlk/db231222/H/d;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/H/d;

    move-result-object v0

    .line 199
    iput-object v0, p0, Ldbxyzptlk/db231222/t/a;->e:Ljava/lang/Object;

    .line 202
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ldbxyzptlk/db231222/H/d;

    goto :goto_0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 217
    iget v0, p0, Ldbxyzptlk/db231222/t/a;->c:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 227
    iget v0, p0, Ldbxyzptlk/db231222/t/a;->f:I

    return v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 241
    iget v0, p0, Ldbxyzptlk/db231222/t/a;->c:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 251
    iget v0, p0, Ldbxyzptlk/db231222/t/a;->g:I

    return v0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 320
    invoke-super {p0}, Ldbxyzptlk/db231222/H/j;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
