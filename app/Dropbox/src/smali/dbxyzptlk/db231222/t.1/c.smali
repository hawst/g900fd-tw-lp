.class public final Ldbxyzptlk/db231222/t/c;
.super Ldbxyzptlk/db231222/H/k;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/t/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/H/k",
        "<",
        "Ldbxyzptlk/db231222/t/a;",
        "Ldbxyzptlk/db231222/t/c;",
        ">;",
        "Ldbxyzptlk/db231222/t/d;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:I

.field private e:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 391
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/k;-><init>()V

    .line 503
    const-string v0, ""

    iput-object v0, p0, Ldbxyzptlk/db231222/t/c;->b:Ljava/lang/Object;

    .line 601
    const-string v0, ""

    iput-object v0, p0, Ldbxyzptlk/db231222/t/c;->c:Ljava/lang/Object;

    .line 392
    invoke-direct {p0}, Ldbxyzptlk/db231222/t/c;->g()V

    .line 393
    return-void
.end method

.method static synthetic f()Ldbxyzptlk/db231222/t/c;
    .locals 1

    .prologue
    .line 386
    invoke-static {}, Ldbxyzptlk/db231222/t/c;->k()Ldbxyzptlk/db231222/t/c;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 396
    return-void
.end method

.method private static k()Ldbxyzptlk/db231222/t/c;
    .locals 1

    .prologue
    .line 398
    new-instance v0, Ldbxyzptlk/db231222/t/c;

    invoke-direct {v0}, Ldbxyzptlk/db231222/t/c;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Ldbxyzptlk/db231222/t/c;
    .locals 2

    .prologue
    .line 415
    invoke-static {}, Ldbxyzptlk/db231222/t/c;->k()Ldbxyzptlk/db231222/t/c;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/c;->c()Ldbxyzptlk/db231222/t/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/t/c;->a(Ldbxyzptlk/db231222/t/a;)Ldbxyzptlk/db231222/t/c;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Ldbxyzptlk/db231222/t/c;
    .locals 1

    .prologue
    .line 728
    iget v0, p0, Ldbxyzptlk/db231222/t/c;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ldbxyzptlk/db231222/t/c;->a:I

    .line 729
    iput p1, p0, Ldbxyzptlk/db231222/t/c;->d:I

    .line 731
    return-object p0
.end method

.method public final a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/t/c;
    .locals 4

    .prologue
    .line 487
    const/4 v2, 0x0

    .line 489
    :try_start_0
    sget-object v0, Ldbxyzptlk/db231222/t/a;->a:Ldbxyzptlk/db231222/H/w;

    invoke-interface {v0, p1, p2}, Ldbxyzptlk/db231222/H/w;->b(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/a;
    :try_end_0
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 494
    if-eqz v0, :cond_0

    .line 495
    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/t/c;->a(Ldbxyzptlk/db231222/t/a;)Ldbxyzptlk/db231222/t/c;

    .line 498
    :cond_0
    return-object p0

    .line 490
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 491
    :try_start_1
    invoke-virtual {v1}, Ldbxyzptlk/db231222/H/o;->a()Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 492
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 494
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 495
    invoke-virtual {p0, v1}, Ldbxyzptlk/db231222/t/c;->a(Ldbxyzptlk/db231222/t/a;)Ldbxyzptlk/db231222/t/c;

    :cond_1
    throw v0

    .line 494
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public final a(Ldbxyzptlk/db231222/t/a;)Ldbxyzptlk/db231222/t/c;
    .locals 1

    .prologue
    .line 455
    invoke-static {}, Ldbxyzptlk/db231222/t/a;->a()Ldbxyzptlk/db231222/t/a;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 472
    :cond_0
    :goto_0
    return-object p0

    .line 456
    :cond_1
    invoke-virtual {p1}, Ldbxyzptlk/db231222/t/a;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 457
    iget v0, p0, Ldbxyzptlk/db231222/t/c;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/t/c;->a:I

    .line 458
    invoke-static {p1}, Ldbxyzptlk/db231222/t/a;->b(Ldbxyzptlk/db231222/t/a;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/t/c;->b:Ljava/lang/Object;

    .line 461
    :cond_2
    invoke-virtual {p1}, Ldbxyzptlk/db231222/t/a;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 462
    iget v0, p0, Ldbxyzptlk/db231222/t/c;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ldbxyzptlk/db231222/t/c;->a:I

    .line 463
    invoke-static {p1}, Ldbxyzptlk/db231222/t/a;->c(Ldbxyzptlk/db231222/t/a;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/t/c;->c:Ljava/lang/Object;

    .line 466
    :cond_3
    invoke-virtual {p1}, Ldbxyzptlk/db231222/t/a;->k()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 467
    invoke-virtual {p1}, Ldbxyzptlk/db231222/t/a;->l()I

    move-result v0

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/t/c;->a(I)Ldbxyzptlk/db231222/t/c;

    .line 469
    :cond_4
    invoke-virtual {p1}, Ldbxyzptlk/db231222/t/a;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 470
    invoke-virtual {p1}, Ldbxyzptlk/db231222/t/a;->n()I

    move-result v0

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/t/c;->b(I)Ldbxyzptlk/db231222/t/c;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Ldbxyzptlk/db231222/t/c;
    .locals 1

    .prologue
    .line 561
    if-nez p1, :cond_0

    .line 562
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 564
    :cond_0
    iget v0, p0, Ldbxyzptlk/db231222/t/c;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/t/c;->a:I

    .line 565
    iput-object p1, p0, Ldbxyzptlk/db231222/t/c;->b:Ljava/lang/Object;

    .line 567
    return-object p0
.end method

.method public final synthetic b(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/H/b;
    .locals 1

    .prologue
    .line 386
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/t/c;->a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/t/c;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ldbxyzptlk/db231222/t/a;
    .locals 2

    .prologue
    .line 423
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/c;->c()Ldbxyzptlk/db231222/t/a;

    move-result-object v0

    .line 424
    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/a;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 425
    invoke-static {v0}, Ldbxyzptlk/db231222/t/c;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/C;

    move-result-object v0

    throw v0

    .line 427
    :cond_0
    return-object v0
.end method

.method public final b(I)Ldbxyzptlk/db231222/t/c;
    .locals 1

    .prologue
    .line 777
    iget v0, p0, Ldbxyzptlk/db231222/t/c;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ldbxyzptlk/db231222/t/c;->a:I

    .line 778
    iput p1, p0, Ldbxyzptlk/db231222/t/c;->e:I

    .line 780
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Ldbxyzptlk/db231222/t/c;
    .locals 1

    .prologue
    .line 659
    if-nez p1, :cond_0

    .line 660
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 662
    :cond_0
    iget v0, p0, Ldbxyzptlk/db231222/t/c;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ldbxyzptlk/db231222/t/c;->a:I

    .line 663
    iput-object p1, p0, Ldbxyzptlk/db231222/t/c;->c:Ljava/lang/Object;

    .line 665
    return-object p0
.end method

.method public final c()Ldbxyzptlk/db231222/t/a;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 431
    new-instance v2, Ldbxyzptlk/db231222/t/a;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Ldbxyzptlk/db231222/t/a;-><init>(Ldbxyzptlk/db231222/H/k;Ldbxyzptlk/db231222/t/b;)V

    .line 432
    iget v3, p0, Ldbxyzptlk/db231222/t/c;->a:I

    .line 433
    const/4 v1, 0x0

    .line 434
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 437
    :goto_0
    iget-object v1, p0, Ldbxyzptlk/db231222/t/c;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Ldbxyzptlk/db231222/t/a;->a(Ldbxyzptlk/db231222/t/a;Ljava/lang/Object;)Ljava/lang/Object;

    .line 438
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 439
    or-int/lit8 v0, v0, 0x2

    .line 441
    :cond_0
    iget-object v1, p0, Ldbxyzptlk/db231222/t/c;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Ldbxyzptlk/db231222/t/a;->b(Ldbxyzptlk/db231222/t/a;Ljava/lang/Object;)Ljava/lang/Object;

    .line 442
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 443
    or-int/lit8 v0, v0, 0x4

    .line 445
    :cond_1
    iget v1, p0, Ldbxyzptlk/db231222/t/c;->d:I

    invoke-static {v2, v1}, Ldbxyzptlk/db231222/t/a;->a(Ldbxyzptlk/db231222/t/a;I)I

    .line 446
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 447
    or-int/lit8 v0, v0, 0x8

    .line 449
    :cond_2
    iget v1, p0, Ldbxyzptlk/db231222/t/c;->e:I

    invoke-static {v2, v1}, Ldbxyzptlk/db231222/t/a;->b(Ldbxyzptlk/db231222/t/a;I)I

    .line 450
    invoke-static {v2, v0}, Ldbxyzptlk/db231222/t/a;->c(Ldbxyzptlk/db231222/t/a;I)I

    .line 451
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 386
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/c;->a()Ldbxyzptlk/db231222/t/c;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 512
    iget v1, p0, Ldbxyzptlk/db231222/t/c;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 476
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/c;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 478
    const/4 v0, 0x0

    .line 480
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final synthetic h()Ldbxyzptlk/db231222/H/k;
    .locals 1

    .prologue
    .line 386
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/c;->a()Ldbxyzptlk/db231222/t/c;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()Ldbxyzptlk/db231222/H/b;
    .locals 1

    .prologue
    .line 386
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/c;->a()Ldbxyzptlk/db231222/t/c;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()Ldbxyzptlk/db231222/H/t;
    .locals 1

    .prologue
    .line 386
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/c;->c()Ldbxyzptlk/db231222/t/a;

    move-result-object v0

    return-object v0
.end method
