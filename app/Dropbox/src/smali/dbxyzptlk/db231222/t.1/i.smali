.class public final Ldbxyzptlk/db231222/t/i;
.super Ldbxyzptlk/db231222/H/j;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/t/l;


# static fields
.field public static a:Ldbxyzptlk/db231222/H/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/H/w",
            "<",
            "Ldbxyzptlk/db231222/t/i;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ldbxyzptlk/db231222/t/i;

.field private static final serialVersionUID:J


# instance fields
.field private c:I

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 80
    new-instance v0, Ldbxyzptlk/db231222/t/j;

    invoke-direct {v0}, Ldbxyzptlk/db231222/t/j;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/t/i;->a:Ldbxyzptlk/db231222/H/w;

    .line 646
    new-instance v0, Ldbxyzptlk/db231222/t/i;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/t/i;-><init>(Z)V

    sput-object v0, Ldbxyzptlk/db231222/t/i;->b:Ldbxyzptlk/db231222/t/i;

    .line 647
    sget-object v0, Ldbxyzptlk/db231222/t/i;->b:Ldbxyzptlk/db231222/t/i;

    invoke-direct {v0}, Ldbxyzptlk/db231222/t/i;->o()V

    .line 648
    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 31
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/j;-><init>()V

    .line 204
    iput-byte v0, p0, Ldbxyzptlk/db231222/t/i;->h:B

    .line 230
    iput v0, p0, Ldbxyzptlk/db231222/t/i;->i:I

    .line 32
    invoke-direct {p0}, Ldbxyzptlk/db231222/t/i;->o()V

    .line 35
    const/4 v0, 0x0

    .line 36
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 37
    :try_start_0
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->a()I

    move-result v2

    .line 38
    sparse-switch v2, :sswitch_data_0

    .line 43
    invoke-virtual {p0, p1, p2, v2}, Ldbxyzptlk/db231222/t/i;->a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 45
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 41
    goto :goto_0

    .line 50
    :sswitch_1
    iget v2, p0, Ldbxyzptlk/db231222/t/i;->c:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Ldbxyzptlk/db231222/t/i;->c:I

    .line 51
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->f()Z

    move-result v2

    iput-boolean v2, p0, Ldbxyzptlk/db231222/t/i;->d:Z
    :try_end_0
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 71
    :catch_0
    move-exception v0

    .line 72
    :try_start_1
    invoke-virtual {v0, p0}, Ldbxyzptlk/db231222/H/o;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/o;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/i;->L()V

    throw v0

    .line 55
    :sswitch_2
    :try_start_2
    iget v2, p0, Ldbxyzptlk/db231222/t/i;->c:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Ldbxyzptlk/db231222/t/i;->c:I

    .line 56
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->f()Z

    move-result v2

    iput-boolean v2, p0, Ldbxyzptlk/db231222/t/i;->e:Z
    :try_end_2
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 73
    :catch_1
    move-exception v0

    .line 74
    :try_start_3
    new-instance v1, Ldbxyzptlk/db231222/H/o;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/H/o;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ldbxyzptlk/db231222/H/o;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/o;

    move-result-object v0

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 60
    :sswitch_3
    :try_start_4
    iget v2, p0, Ldbxyzptlk/db231222/t/i;->c:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Ldbxyzptlk/db231222/t/i;->c:I

    .line 61
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->f()Z

    move-result v2

    iput-boolean v2, p0, Ldbxyzptlk/db231222/t/i;->f:Z

    goto :goto_0

    .line 65
    :sswitch_4
    iget v2, p0, Ldbxyzptlk/db231222/t/i;->c:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Ldbxyzptlk/db231222/t/i;->c:I

    .line 66
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->f()Z

    move-result v2

    iput-boolean v2, p0, Ldbxyzptlk/db231222/t/i;->g:Z
    :try_end_4
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 77
    :cond_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/i;->L()V

    .line 79
    return-void

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method synthetic constructor <init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;Ldbxyzptlk/db231222/t/j;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/t/i;-><init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)V

    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/H/k;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 14
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/H/j;-><init>(Ldbxyzptlk/db231222/H/k;)V

    .line 204
    iput-byte v0, p0, Ldbxyzptlk/db231222/t/i;->h:B

    .line 230
    iput v0, p0, Ldbxyzptlk/db231222/t/i;->i:I

    .line 16
    return-void
.end method

.method synthetic constructor <init>(Ldbxyzptlk/db231222/H/k;Ldbxyzptlk/db231222/t/j;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/t/i;-><init>(Ldbxyzptlk/db231222/H/k;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/j;-><init>()V

    .line 204
    iput-byte v0, p0, Ldbxyzptlk/db231222/t/i;->h:B

    .line 230
    iput v0, p0, Ldbxyzptlk/db231222/t/i;->i:I

    .line 17
    return-void
.end method

.method static synthetic a(Ldbxyzptlk/db231222/t/i;I)I
    .locals 0

    .prologue
    .line 9
    iput p1, p0, Ldbxyzptlk/db231222/t/i;->c:I

    return p1
.end method

.method public static a()Ldbxyzptlk/db231222/t/i;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Ldbxyzptlk/db231222/t/i;->b:Ldbxyzptlk/db231222/t/i;

    return-object v0
.end method

.method public static a(Ldbxyzptlk/db231222/t/i;)Ldbxyzptlk/db231222/t/k;
    .locals 1

    .prologue
    .line 319
    invoke-static {}, Ldbxyzptlk/db231222/t/i;->m()Ldbxyzptlk/db231222/t/k;

    move-result-object v0

    invoke-virtual {v0, p0}, Ldbxyzptlk/db231222/t/k;->a(Ldbxyzptlk/db231222/t/i;)Ldbxyzptlk/db231222/t/k;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/t/i;Z)Z
    .locals 0

    .prologue
    .line 9
    iput-boolean p1, p0, Ldbxyzptlk/db231222/t/i;->d:Z

    return p1
.end method

.method static synthetic b(Ldbxyzptlk/db231222/t/i;Z)Z
    .locals 0

    .prologue
    .line 9
    iput-boolean p1, p0, Ldbxyzptlk/db231222/t/i;->e:Z

    return p1
.end method

.method static synthetic c(Ldbxyzptlk/db231222/t/i;Z)Z
    .locals 0

    .prologue
    .line 9
    iput-boolean p1, p0, Ldbxyzptlk/db231222/t/i;->f:Z

    return p1
.end method

.method static synthetic d(Ldbxyzptlk/db231222/t/i;Z)Z
    .locals 0

    .prologue
    .line 9
    iput-boolean p1, p0, Ldbxyzptlk/db231222/t/i;->g:Z

    return p1
.end method

.method public static m()Ldbxyzptlk/db231222/t/k;
    .locals 1

    .prologue
    .line 316
    invoke-static {}, Ldbxyzptlk/db231222/t/k;->d()Ldbxyzptlk/db231222/t/k;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 199
    iput-boolean v0, p0, Ldbxyzptlk/db231222/t/i;->d:Z

    .line 200
    iput-boolean v0, p0, Ldbxyzptlk/db231222/t/i;->e:Z

    .line 201
    iput-boolean v0, p0, Ldbxyzptlk/db231222/t/i;->f:Z

    .line 202
    iput-boolean v0, p0, Ldbxyzptlk/db231222/t/i;->g:Z

    .line 203
    return-void
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/H/g;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 215
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/i;->f()I

    .line 216
    iget v0, p0, Ldbxyzptlk/db231222/t/i;->c:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 217
    iget-boolean v0, p0, Ldbxyzptlk/db231222/t/i;->d:Z

    invoke-virtual {p1, v1, v0}, Ldbxyzptlk/db231222/H/g;->a(IZ)V

    .line 219
    :cond_0
    iget v0, p0, Ldbxyzptlk/db231222/t/i;->c:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 220
    iget-boolean v0, p0, Ldbxyzptlk/db231222/t/i;->e:Z

    invoke-virtual {p1, v2, v0}, Ldbxyzptlk/db231222/H/g;->a(IZ)V

    .line 222
    :cond_1
    iget v0, p0, Ldbxyzptlk/db231222/t/i;->c:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 223
    const/4 v0, 0x3

    iget-boolean v1, p0, Ldbxyzptlk/db231222/t/i;->f:Z

    invoke-virtual {p1, v0, v1}, Ldbxyzptlk/db231222/H/g;->a(IZ)V

    .line 225
    :cond_2
    iget v0, p0, Ldbxyzptlk/db231222/t/i;->c:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 226
    iget-boolean v0, p0, Ldbxyzptlk/db231222/t/i;->g:Z

    invoke-virtual {p1, v3, v0}, Ldbxyzptlk/db231222/H/g;->a(IZ)V

    .line 228
    :cond_3
    return-void
.end method

.method public final b()Ldbxyzptlk/db231222/H/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldbxyzptlk/db231222/H/w",
            "<",
            "Ldbxyzptlk/db231222/t/i;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    sget-object v0, Ldbxyzptlk/db231222/t/i;->a:Ldbxyzptlk/db231222/H/w;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 108
    iget v1, p0, Ldbxyzptlk/db231222/t/i;->c:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Ldbxyzptlk/db231222/t/i;->d:Z

    return v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 206
    iget-byte v1, p0, Ldbxyzptlk/db231222/t/i;->h:B

    .line 207
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 210
    :goto_0
    return v0

    .line 207
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 209
    :cond_1
    iput-byte v0, p0, Ldbxyzptlk/db231222/t/i;->h:B

    goto :goto_0
.end method

.method public final f()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 232
    iget v0, p0, Ldbxyzptlk/db231222/t/i;->i:I

    .line 233
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 253
    :goto_0
    return v0

    .line 235
    :cond_0
    const/4 v0, 0x0

    .line 236
    iget v1, p0, Ldbxyzptlk/db231222/t/i;->c:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 237
    iget-boolean v1, p0, Ldbxyzptlk/db231222/t/i;->d:Z

    invoke-static {v2, v1}, Ldbxyzptlk/db231222/H/g;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 240
    :cond_1
    iget v1, p0, Ldbxyzptlk/db231222/t/i;->c:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 241
    iget-boolean v1, p0, Ldbxyzptlk/db231222/t/i;->e:Z

    invoke-static {v3, v1}, Ldbxyzptlk/db231222/H/g;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 244
    :cond_2
    iget v1, p0, Ldbxyzptlk/db231222/t/i;->c:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 245
    const/4 v1, 0x3

    iget-boolean v2, p0, Ldbxyzptlk/db231222/t/i;->f:Z

    invoke-static {v1, v2}, Ldbxyzptlk/db231222/H/g;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 248
    :cond_3
    iget v1, p0, Ldbxyzptlk/db231222/t/i;->c:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 249
    iget-boolean v1, p0, Ldbxyzptlk/db231222/t/i;->g:Z

    invoke-static {v4, v1}, Ldbxyzptlk/db231222/H/g;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 252
    :cond_4
    iput v0, p0, Ldbxyzptlk/db231222/t/i;->i:I

    goto :goto_0
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 135
    iget v0, p0, Ldbxyzptlk/db231222/t/i;->c:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Ldbxyzptlk/db231222/t/i;->e:Z

    return v0
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 161
    iget v0, p0, Ldbxyzptlk/db231222/t/i;->c:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 171
    iget-boolean v0, p0, Ldbxyzptlk/db231222/t/i;->f:Z

    return v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 185
    iget v0, p0, Ldbxyzptlk/db231222/t/i;->c:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 195
    iget-boolean v0, p0, Ldbxyzptlk/db231222/t/i;->g:Z

    return v0
.end method

.method public final n()Ldbxyzptlk/db231222/t/k;
    .locals 1

    .prologue
    .line 321
    invoke-static {p0}, Ldbxyzptlk/db231222/t/i;->a(Ldbxyzptlk/db231222/t/i;)Ldbxyzptlk/db231222/t/k;

    move-result-object v0

    return-object v0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 260
    invoke-super {p0}, Ldbxyzptlk/db231222/H/j;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
