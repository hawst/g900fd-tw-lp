.class public final Ldbxyzptlk/db231222/t/g;
.super Ldbxyzptlk/db231222/H/k;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/t/h;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/H/k",
        "<",
        "Ldbxyzptlk/db231222/t/e;",
        "Ldbxyzptlk/db231222/t/g;",
        ">;",
        "Ldbxyzptlk/db231222/t/h;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/t/a;",
            ">;"
        }
    .end annotation
.end field

.field private c:J


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 295
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/k;-><init>()V

    .line 395
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/t/g;->b:Ljava/util/List;

    .line 296
    invoke-direct {p0}, Ldbxyzptlk/db231222/t/g;->k()V

    .line 297
    return-void
.end method

.method static synthetic g()Ldbxyzptlk/db231222/t/g;
    .locals 1

    .prologue
    .line 290
    invoke-static {}, Ldbxyzptlk/db231222/t/g;->l()Ldbxyzptlk/db231222/t/g;

    move-result-object v0

    return-object v0
.end method

.method private k()V
    .locals 0

    .prologue
    .line 300
    return-void
.end method

.method private static l()Ldbxyzptlk/db231222/t/g;
    .locals 1

    .prologue
    .line 302
    new-instance v0, Ldbxyzptlk/db231222/t/g;

    invoke-direct {v0}, Ldbxyzptlk/db231222/t/g;-><init>()V

    return-object v0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 398
    iget v0, p0, Ldbxyzptlk/db231222/t/g;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 399
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Ldbxyzptlk/db231222/t/g;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/t/g;->b:Ljava/util/List;

    .line 400
    iget v0, p0, Ldbxyzptlk/db231222/t/g;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/t/g;->a:I

    .line 402
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)Ldbxyzptlk/db231222/t/a;
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Ldbxyzptlk/db231222/t/g;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/a;

    return-object v0
.end method

.method public final a()Ldbxyzptlk/db231222/t/g;
    .locals 2

    .prologue
    .line 315
    invoke-static {}, Ldbxyzptlk/db231222/t/g;->l()Ldbxyzptlk/db231222/t/g;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/g;->c()Ldbxyzptlk/db231222/t/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/t/g;->a(Ldbxyzptlk/db231222/t/e;)Ldbxyzptlk/db231222/t/g;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)Ldbxyzptlk/db231222/t/g;
    .locals 1

    .prologue
    .line 597
    iget v0, p0, Ldbxyzptlk/db231222/t/g;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ldbxyzptlk/db231222/t/g;->a:I

    .line 598
    iput-wide p1, p0, Ldbxyzptlk/db231222/t/g;->c:J

    .line 600
    return-object p0
.end method

.method public final a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/t/g;
    .locals 4

    .prologue
    .line 379
    const/4 v2, 0x0

    .line 381
    :try_start_0
    sget-object v0, Ldbxyzptlk/db231222/t/e;->a:Ldbxyzptlk/db231222/H/w;

    invoke-interface {v0, p1, p2}, Ldbxyzptlk/db231222/H/w;->b(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/e;
    :try_end_0
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 386
    if-eqz v0, :cond_0

    .line 387
    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/t/g;->a(Ldbxyzptlk/db231222/t/e;)Ldbxyzptlk/db231222/t/g;

    .line 390
    :cond_0
    return-object p0

    .line 382
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 383
    :try_start_1
    invoke-virtual {v1}, Ldbxyzptlk/db231222/H/o;->a()Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 384
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 386
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 387
    invoke-virtual {p0, v1}, Ldbxyzptlk/db231222/t/g;->a(Ldbxyzptlk/db231222/t/e;)Ldbxyzptlk/db231222/t/g;

    :cond_1
    throw v0

    .line 386
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public final a(Ldbxyzptlk/db231222/t/a;)Ldbxyzptlk/db231222/t/g;
    .locals 1

    .prologue
    .line 473
    if-nez p1, :cond_0

    .line 474
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 476
    :cond_0
    invoke-direct {p0}, Ldbxyzptlk/db231222/t/g;->m()V

    .line 477
    iget-object v0, p0, Ldbxyzptlk/db231222/t/g;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 479
    return-object p0
.end method

.method public final a(Ldbxyzptlk/db231222/t/e;)Ldbxyzptlk/db231222/t/g;
    .locals 2

    .prologue
    .line 348
    invoke-static {}, Ldbxyzptlk/db231222/t/e;->a()Ldbxyzptlk/db231222/t/e;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 362
    :cond_0
    :goto_0
    return-object p0

    .line 349
    :cond_1
    invoke-static {p1}, Ldbxyzptlk/db231222/t/e;->b(Ldbxyzptlk/db231222/t/e;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 350
    iget-object v0, p0, Ldbxyzptlk/db231222/t/g;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 351
    invoke-static {p1}, Ldbxyzptlk/db231222/t/e;->b(Ldbxyzptlk/db231222/t/e;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/t/g;->b:Ljava/util/List;

    .line 352
    iget v0, p0, Ldbxyzptlk/db231222/t/g;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Ldbxyzptlk/db231222/t/g;->a:I

    .line 359
    :cond_2
    :goto_1
    invoke-virtual {p1}, Ldbxyzptlk/db231222/t/e;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360
    invoke-virtual {p1}, Ldbxyzptlk/db231222/t/e;->h()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ldbxyzptlk/db231222/t/g;->a(J)Ldbxyzptlk/db231222/t/g;

    goto :goto_0

    .line 354
    :cond_3
    invoke-direct {p0}, Ldbxyzptlk/db231222/t/g;->m()V

    .line 355
    iget-object v0, p0, Ldbxyzptlk/db231222/t/g;->b:Ljava/util/List;

    invoke-static {p1}, Ldbxyzptlk/db231222/t/e;->b(Ldbxyzptlk/db231222/t/e;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public final a(Ljava/lang/Iterable;)Ldbxyzptlk/db231222/t/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ldbxyzptlk/db231222/t/a;",
            ">;)",
            "Ldbxyzptlk/db231222/t/g;"
        }
    .end annotation

    .prologue
    .line 535
    invoke-direct {p0}, Ldbxyzptlk/db231222/t/g;->m()V

    .line 536
    iget-object v0, p0, Ldbxyzptlk/db231222/t/g;->b:Ljava/util/List;

    invoke-static {p1, v0}, Ldbxyzptlk/db231222/H/k;->a(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 538
    return-object p0
.end method

.method public final synthetic b(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/H/b;
    .locals 1

    .prologue
    .line 290
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/t/g;->a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/t/g;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ldbxyzptlk/db231222/t/e;
    .locals 2

    .prologue
    .line 323
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/g;->c()Ldbxyzptlk/db231222/t/e;

    move-result-object v0

    .line 324
    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/e;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 325
    invoke-static {v0}, Ldbxyzptlk/db231222/t/g;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/C;

    move-result-object v0

    throw v0

    .line 327
    :cond_0
    return-object v0
.end method

.method public final c()Ldbxyzptlk/db231222/t/e;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 331
    new-instance v2, Ldbxyzptlk/db231222/t/e;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Ldbxyzptlk/db231222/t/e;-><init>(Ldbxyzptlk/db231222/H/k;Ldbxyzptlk/db231222/t/f;)V

    .line 332
    iget v3, p0, Ldbxyzptlk/db231222/t/g;->a:I

    .line 333
    const/4 v1, 0x0

    .line 334
    iget v4, p0, Ldbxyzptlk/db231222/t/g;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    .line 335
    iget-object v4, p0, Ldbxyzptlk/db231222/t/g;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Ldbxyzptlk/db231222/t/g;->b:Ljava/util/List;

    .line 336
    iget v4, p0, Ldbxyzptlk/db231222/t/g;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Ldbxyzptlk/db231222/t/g;->a:I

    .line 338
    :cond_0
    iget-object v4, p0, Ldbxyzptlk/db231222/t/g;->b:Ljava/util/List;

    invoke-static {v2, v4}, Ldbxyzptlk/db231222/t/e;->a(Ldbxyzptlk/db231222/t/e;Ljava/util/List;)Ljava/util/List;

    .line 339
    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 342
    :goto_0
    iget-wide v3, p0, Ldbxyzptlk/db231222/t/g;->c:J

    invoke-static {v2, v3, v4}, Ldbxyzptlk/db231222/t/e;->a(Ldbxyzptlk/db231222/t/e;J)J

    .line 343
    invoke-static {v2, v0}, Ldbxyzptlk/db231222/t/e;->a(Ldbxyzptlk/db231222/t/e;I)I

    .line 344
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 290
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/g;->a()Ldbxyzptlk/db231222/t/g;

    move-result-object v0

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Ldbxyzptlk/db231222/t/g;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 366
    move v0, v1

    :goto_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/g;->d()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 367
    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/t/g;->a(I)Ldbxyzptlk/db231222/t/a;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/t/a;->e()Z

    move-result v2

    if-nez v2, :cond_0

    .line 372
    :goto_1
    return v1

    .line 366
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 372
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public final f()Ldbxyzptlk/db231222/t/g;
    .locals 1

    .prologue
    .line 548
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/t/g;->b:Ljava/util/List;

    .line 549
    iget v0, p0, Ldbxyzptlk/db231222/t/g;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Ldbxyzptlk/db231222/t/g;->a:I

    .line 551
    return-object p0
.end method

.method public final synthetic h()Ldbxyzptlk/db231222/H/k;
    .locals 1

    .prologue
    .line 290
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/g;->a()Ldbxyzptlk/db231222/t/g;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()Ldbxyzptlk/db231222/H/b;
    .locals 1

    .prologue
    .line 290
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/g;->a()Ldbxyzptlk/db231222/t/g;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()Ldbxyzptlk/db231222/H/t;
    .locals 1

    .prologue
    .line 290
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/g;->c()Ldbxyzptlk/db231222/t/e;

    move-result-object v0

    return-object v0
.end method
