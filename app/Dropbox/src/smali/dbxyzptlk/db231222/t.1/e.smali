.class public final Ldbxyzptlk/db231222/t/e;
.super Ldbxyzptlk/db231222/H/j;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/t/h;


# static fields
.field public static a:Ldbxyzptlk/db231222/H/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/H/w",
            "<",
            "Ldbxyzptlk/db231222/t/e;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ldbxyzptlk/db231222/t/e;

.field private static final serialVersionUID:J


# instance fields
.field private c:I

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/t/a;",
            ">;"
        }
    .end annotation
.end field

.field private e:J

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 76
    new-instance v0, Ldbxyzptlk/db231222/t/f;

    invoke-direct {v0}, Ldbxyzptlk/db231222/t/f;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/t/e;->a:Ldbxyzptlk/db231222/H/w;

    .line 620
    new-instance v0, Ldbxyzptlk/db231222/t/e;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/t/e;-><init>(Z)V

    sput-object v0, Ldbxyzptlk/db231222/t/e;->b:Ldbxyzptlk/db231222/t/e;

    .line 621
    sget-object v0, Ldbxyzptlk/db231222/t/e;->b:Ldbxyzptlk/db231222/t/e;

    invoke-direct {v0}, Ldbxyzptlk/db231222/t/e;->k()V

    .line 622
    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x1

    .line 31
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/j;-><init>()V

    .line 176
    iput-byte v2, p0, Ldbxyzptlk/db231222/t/e;->f:B

    .line 202
    iput v2, p0, Ldbxyzptlk/db231222/t/e;->g:I

    .line 32
    invoke-direct {p0}, Ldbxyzptlk/db231222/t/e;->k()V

    move v2, v0

    .line 36
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 37
    :try_start_0
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->a()I

    move-result v3

    .line 38
    sparse-switch v3, :sswitch_data_0

    .line 43
    invoke-virtual {p0, p1, p2, v3}, Ldbxyzptlk/db231222/t/e;->a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;I)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 45
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 41
    goto :goto_0

    .line 50
    :sswitch_1
    and-int/lit8 v3, v2, 0x1

    if-eq v3, v1, :cond_1

    .line 51
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Ldbxyzptlk/db231222/t/e;->d:Ljava/util/List;

    .line 52
    or-int/lit8 v2, v2, 0x1

    .line 54
    :cond_1
    iget-object v3, p0, Ldbxyzptlk/db231222/t/e;->d:Ljava/util/List;

    sget-object v4, Ldbxyzptlk/db231222/t/a;->a:Ldbxyzptlk/db231222/H/w;

    invoke-virtual {p1, v4, p2}, Ldbxyzptlk/db231222/H/f;->a(Ldbxyzptlk/db231222/H/w;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/H/t;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 64
    :catch_0
    move-exception v0

    .line 65
    :try_start_1
    invoke-virtual {v0, p0}, Ldbxyzptlk/db231222/H/o;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/o;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 70
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_2

    .line 71
    iget-object v1, p0, Ldbxyzptlk/db231222/t/e;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Ldbxyzptlk/db231222/t/e;->d:Ljava/util/List;

    .line 73
    :cond_2
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/e;->L()V

    throw v0

    .line 58
    :sswitch_2
    :try_start_2
    iget v3, p0, Ldbxyzptlk/db231222/t/e;->c:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Ldbxyzptlk/db231222/t/e;->c:I

    .line 59
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->d()J

    move-result-wide v3

    iput-wide v3, p0, Ldbxyzptlk/db231222/t/e;->e:J
    :try_end_2
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 66
    :catch_1
    move-exception v0

    .line 67
    :try_start_3
    new-instance v3, Ldbxyzptlk/db231222/H/o;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ldbxyzptlk/db231222/H/o;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ldbxyzptlk/db231222/H/o;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/o;

    move-result-object v0

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 70
    :cond_3
    and-int/lit8 v0, v2, 0x1

    if-ne v0, v1, :cond_4

    .line 71
    iget-object v0, p0, Ldbxyzptlk/db231222/t/e;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/t/e;->d:Ljava/util/List;

    .line 73
    :cond_4
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/e;->L()V

    .line 75
    return-void

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method synthetic constructor <init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;Ldbxyzptlk/db231222/t/f;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/t/e;-><init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)V

    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/H/k;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 14
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/H/j;-><init>(Ldbxyzptlk/db231222/H/k;)V

    .line 176
    iput-byte v0, p0, Ldbxyzptlk/db231222/t/e;->f:B

    .line 202
    iput v0, p0, Ldbxyzptlk/db231222/t/e;->g:I

    .line 16
    return-void
.end method

.method synthetic constructor <init>(Ldbxyzptlk/db231222/H/k;Ldbxyzptlk/db231222/t/f;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/t/e;-><init>(Ldbxyzptlk/db231222/H/k;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/j;-><init>()V

    .line 176
    iput-byte v0, p0, Ldbxyzptlk/db231222/t/e;->f:B

    .line 202
    iput v0, p0, Ldbxyzptlk/db231222/t/e;->g:I

    .line 17
    return-void
.end method

.method static synthetic a(Ldbxyzptlk/db231222/t/e;I)I
    .locals 0

    .prologue
    .line 9
    iput p1, p0, Ldbxyzptlk/db231222/t/e;->c:I

    return p1
.end method

.method static synthetic a(Ldbxyzptlk/db231222/t/e;J)J
    .locals 0

    .prologue
    .line 9
    iput-wide p1, p0, Ldbxyzptlk/db231222/t/e;->e:J

    return-wide p1
.end method

.method public static a()Ldbxyzptlk/db231222/t/e;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Ldbxyzptlk/db231222/t/e;->b:Ldbxyzptlk/db231222/t/e;

    return-object v0
.end method

.method public static a(Ldbxyzptlk/db231222/t/e;)Ldbxyzptlk/db231222/t/g;
    .locals 1

    .prologue
    .line 283
    invoke-static {}, Ldbxyzptlk/db231222/t/e;->i()Ldbxyzptlk/db231222/t/g;

    move-result-object v0

    invoke-virtual {v0, p0}, Ldbxyzptlk/db231222/t/g;->a(Ldbxyzptlk/db231222/t/e;)Ldbxyzptlk/db231222/t/g;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/t/e;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 9
    iput-object p1, p0, Ldbxyzptlk/db231222/t/e;->d:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Ldbxyzptlk/db231222/t/e;)Ljava/util/List;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Ldbxyzptlk/db231222/t/e;->d:Ljava/util/List;

    return-object v0
.end method

.method public static i()Ldbxyzptlk/db231222/t/g;
    .locals 1

    .prologue
    .line 280
    invoke-static {}, Ldbxyzptlk/db231222/t/g;->g()Ldbxyzptlk/db231222/t/g;

    move-result-object v0

    return-object v0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 173
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/t/e;->d:Ljava/util/List;

    .line 174
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ldbxyzptlk/db231222/t/e;->e:J

    .line 175
    return-void
.end method


# virtual methods
.method public final a(I)Ldbxyzptlk/db231222/t/a;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Ldbxyzptlk/db231222/t/e;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/a;

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/H/g;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 193
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/e;->f()I

    .line 194
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Ldbxyzptlk/db231222/t/e;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 195
    iget-object v0, p0, Ldbxyzptlk/db231222/t/e;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/H/t;

    invoke-virtual {p1, v2, v0}, Ldbxyzptlk/db231222/H/g;->a(ILdbxyzptlk/db231222/H/t;)V

    .line 194
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 197
    :cond_0
    iget v0, p0, Ldbxyzptlk/db231222/t/e;->c:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 198
    const/4 v0, 0x2

    iget-wide v1, p0, Ldbxyzptlk/db231222/t/e;->e:J

    invoke-virtual {p1, v0, v1, v2}, Ldbxyzptlk/db231222/H/g;->b(IJ)V

    .line 200
    :cond_1
    return-void
.end method

.method public final b()Ldbxyzptlk/db231222/H/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldbxyzptlk/db231222/H/w",
            "<",
            "Ldbxyzptlk/db231222/t/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    sget-object v0, Ldbxyzptlk/db231222/t/e;->a:Ldbxyzptlk/db231222/H/w;

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/t/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Ldbxyzptlk/db231222/t/e;->d:Ljava/util/List;

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Ldbxyzptlk/db231222/t/e;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 178
    iget-byte v2, p0, Ldbxyzptlk/db231222/t/e;->f:B

    .line 179
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    if-ne v2, v0, :cond_0

    .line 188
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 179
    goto :goto_0

    :cond_1
    move v2, v1

    .line 181
    :goto_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/e;->d()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 182
    invoke-virtual {p0, v2}, Ldbxyzptlk/db231222/t/e;->a(I)Ldbxyzptlk/db231222/t/a;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/t/a;->e()Z

    move-result v3

    if-nez v3, :cond_2

    .line 183
    iput-byte v1, p0, Ldbxyzptlk/db231222/t/e;->f:B

    move v0, v1

    .line 184
    goto :goto_0

    .line 181
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 187
    :cond_3
    iput-byte v0, p0, Ldbxyzptlk/db231222/t/e;->f:B

    goto :goto_0
.end method

.method public final f()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 204
    iget v2, p0, Ldbxyzptlk/db231222/t/e;->g:I

    .line 205
    const/4 v1, -0x1

    if-eq v2, v1, :cond_0

    .line 217
    :goto_0
    return v2

    :cond_0
    move v1, v0

    move v2, v0

    .line 208
    :goto_1
    iget-object v0, p0, Ldbxyzptlk/db231222/t/e;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 209
    iget-object v0, p0, Ldbxyzptlk/db231222/t/e;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/H/t;

    invoke-static {v3, v0}, Ldbxyzptlk/db231222/H/g;->b(ILdbxyzptlk/db231222/H/t;)I

    move-result v0

    add-int/2addr v2, v0

    .line 208
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 212
    :cond_1
    iget v0, p0, Ldbxyzptlk/db231222/t/e;->c:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 213
    const/4 v0, 0x2

    iget-wide v3, p0, Ldbxyzptlk/db231222/t/e;->e:J

    invoke-static {v0, v3, v4}, Ldbxyzptlk/db231222/H/g;->d(IJ)I

    move-result v0

    add-int/2addr v2, v0

    .line 216
    :cond_2
    iput v2, p0, Ldbxyzptlk/db231222/t/e;->g:I

    goto :goto_0
.end method

.method public final g()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 159
    iget v1, p0, Ldbxyzptlk/db231222/t/e;->c:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()J
    .locals 2

    .prologue
    .line 169
    iget-wide v0, p0, Ldbxyzptlk/db231222/t/e;->e:J

    return-wide v0
.end method

.method public final j()Ldbxyzptlk/db231222/t/g;
    .locals 1

    .prologue
    .line 285
    invoke-static {p0}, Ldbxyzptlk/db231222/t/e;->a(Ldbxyzptlk/db231222/t/e;)Ldbxyzptlk/db231222/t/g;

    move-result-object v0

    return-object v0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 224
    invoke-super {p0}, Ldbxyzptlk/db231222/H/j;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
