.class public final Ldbxyzptlk/db231222/t/k;
.super Ldbxyzptlk/db231222/H/k;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/t/l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/H/k",
        "<",
        "Ldbxyzptlk/db231222/t/i;",
        "Ldbxyzptlk/db231222/t/k;",
        ">;",
        "Ldbxyzptlk/db231222/t/l;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 331
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/k;-><init>()V

    .line 332
    invoke-direct {p0}, Ldbxyzptlk/db231222/t/k;->f()V

    .line 333
    return-void
.end method

.method static synthetic d()Ldbxyzptlk/db231222/t/k;
    .locals 1

    .prologue
    .line 326
    invoke-static {}, Ldbxyzptlk/db231222/t/k;->g()Ldbxyzptlk/db231222/t/k;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 0

    .prologue
    .line 336
    return-void
.end method

.method private static g()Ldbxyzptlk/db231222/t/k;
    .locals 1

    .prologue
    .line 338
    new-instance v0, Ldbxyzptlk/db231222/t/k;

    invoke-direct {v0}, Ldbxyzptlk/db231222/t/k;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Ldbxyzptlk/db231222/t/k;
    .locals 2

    .prologue
    .line 355
    invoke-static {}, Ldbxyzptlk/db231222/t/k;->g()Ldbxyzptlk/db231222/t/k;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/k;->c()Ldbxyzptlk/db231222/t/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/t/k;->a(Ldbxyzptlk/db231222/t/i;)Ldbxyzptlk/db231222/t/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/t/k;
    .locals 4

    .prologue
    .line 419
    const/4 v2, 0x0

    .line 421
    :try_start_0
    sget-object v0, Ldbxyzptlk/db231222/t/i;->a:Ldbxyzptlk/db231222/H/w;

    invoke-interface {v0, p1, p2}, Ldbxyzptlk/db231222/H/w;->b(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/i;
    :try_end_0
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 426
    if-eqz v0, :cond_0

    .line 427
    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/t/k;->a(Ldbxyzptlk/db231222/t/i;)Ldbxyzptlk/db231222/t/k;

    .line 430
    :cond_0
    return-object p0

    .line 422
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 423
    :try_start_1
    invoke-virtual {v1}, Ldbxyzptlk/db231222/H/o;->a()Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/t/i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 424
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 426
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 427
    invoke-virtual {p0, v1}, Ldbxyzptlk/db231222/t/k;->a(Ldbxyzptlk/db231222/t/i;)Ldbxyzptlk/db231222/t/k;

    :cond_1
    throw v0

    .line 426
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public final a(Ldbxyzptlk/db231222/t/i;)Ldbxyzptlk/db231222/t/k;
    .locals 1

    .prologue
    .line 395
    invoke-static {}, Ldbxyzptlk/db231222/t/i;->a()Ldbxyzptlk/db231222/t/i;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 408
    :cond_0
    :goto_0
    return-object p0

    .line 396
    :cond_1
    invoke-virtual {p1}, Ldbxyzptlk/db231222/t/i;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 397
    invoke-virtual {p1}, Ldbxyzptlk/db231222/t/i;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/t/k;->a(Z)Ldbxyzptlk/db231222/t/k;

    .line 399
    :cond_2
    invoke-virtual {p1}, Ldbxyzptlk/db231222/t/i;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 400
    invoke-virtual {p1}, Ldbxyzptlk/db231222/t/i;->h()Z

    move-result v0

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/t/k;->b(Z)Ldbxyzptlk/db231222/t/k;

    .line 402
    :cond_3
    invoke-virtual {p1}, Ldbxyzptlk/db231222/t/i;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 403
    invoke-virtual {p1}, Ldbxyzptlk/db231222/t/i;->j()Z

    move-result v0

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/t/k;->c(Z)Ldbxyzptlk/db231222/t/k;

    .line 405
    :cond_4
    invoke-virtual {p1}, Ldbxyzptlk/db231222/t/i;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 406
    invoke-virtual {p1}, Ldbxyzptlk/db231222/t/i;->l()Z

    move-result v0

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/t/k;->d(Z)Ldbxyzptlk/db231222/t/k;

    goto :goto_0
.end method

.method public final a(Z)Ldbxyzptlk/db231222/t/k;
    .locals 1

    .prologue
    .line 467
    iget v0, p0, Ldbxyzptlk/db231222/t/k;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/t/k;->a:I

    .line 468
    iput-boolean p1, p0, Ldbxyzptlk/db231222/t/k;->b:Z

    .line 470
    return-object p0
.end method

.method public final synthetic b(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/H/b;
    .locals 1

    .prologue
    .line 326
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/t/k;->a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/t/k;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ldbxyzptlk/db231222/t/i;
    .locals 2

    .prologue
    .line 363
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/k;->c()Ldbxyzptlk/db231222/t/i;

    move-result-object v0

    .line 364
    invoke-virtual {v0}, Ldbxyzptlk/db231222/t/i;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 365
    invoke-static {v0}, Ldbxyzptlk/db231222/t/k;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/C;

    move-result-object v0

    throw v0

    .line 367
    :cond_0
    return-object v0
.end method

.method public final b(Z)Ldbxyzptlk/db231222/t/k;
    .locals 1

    .prologue
    .line 523
    iget v0, p0, Ldbxyzptlk/db231222/t/k;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ldbxyzptlk/db231222/t/k;->a:I

    .line 524
    iput-boolean p1, p0, Ldbxyzptlk/db231222/t/k;->c:Z

    .line 526
    return-object p0
.end method

.method public final c()Ldbxyzptlk/db231222/t/i;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 371
    new-instance v2, Ldbxyzptlk/db231222/t/i;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Ldbxyzptlk/db231222/t/i;-><init>(Ldbxyzptlk/db231222/H/k;Ldbxyzptlk/db231222/t/j;)V

    .line 372
    iget v3, p0, Ldbxyzptlk/db231222/t/k;->a:I

    .line 373
    const/4 v1, 0x0

    .line 374
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 377
    :goto_0
    iget-boolean v1, p0, Ldbxyzptlk/db231222/t/k;->b:Z

    invoke-static {v2, v1}, Ldbxyzptlk/db231222/t/i;->a(Ldbxyzptlk/db231222/t/i;Z)Z

    .line 378
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 379
    or-int/lit8 v0, v0, 0x2

    .line 381
    :cond_0
    iget-boolean v1, p0, Ldbxyzptlk/db231222/t/k;->c:Z

    invoke-static {v2, v1}, Ldbxyzptlk/db231222/t/i;->b(Ldbxyzptlk/db231222/t/i;Z)Z

    .line 382
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 383
    or-int/lit8 v0, v0, 0x4

    .line 385
    :cond_1
    iget-boolean v1, p0, Ldbxyzptlk/db231222/t/k;->d:Z

    invoke-static {v2, v1}, Ldbxyzptlk/db231222/t/i;->c(Ldbxyzptlk/db231222/t/i;Z)Z

    .line 386
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 387
    or-int/lit8 v0, v0, 0x8

    .line 389
    :cond_2
    iget-boolean v1, p0, Ldbxyzptlk/db231222/t/k;->e:Z

    invoke-static {v2, v1}, Ldbxyzptlk/db231222/t/i;->d(Ldbxyzptlk/db231222/t/i;Z)Z

    .line 390
    invoke-static {v2, v0}, Ldbxyzptlk/db231222/t/i;->a(Ldbxyzptlk/db231222/t/i;I)I

    .line 391
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final c(Z)Ldbxyzptlk/db231222/t/k;
    .locals 1

    .prologue
    .line 574
    iget v0, p0, Ldbxyzptlk/db231222/t/k;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ldbxyzptlk/db231222/t/k;->a:I

    .line 575
    iput-boolean p1, p0, Ldbxyzptlk/db231222/t/k;->d:Z

    .line 577
    return-object p0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 326
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/k;->a()Ldbxyzptlk/db231222/t/k;

    move-result-object v0

    return-object v0
.end method

.method public final d(Z)Ldbxyzptlk/db231222/t/k;
    .locals 1

    .prologue
    .line 623
    iget v0, p0, Ldbxyzptlk/db231222/t/k;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ldbxyzptlk/db231222/t/k;->a:I

    .line 624
    iput-boolean p1, p0, Ldbxyzptlk/db231222/t/k;->e:Z

    .line 626
    return-object p0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 412
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic h()Ldbxyzptlk/db231222/H/k;
    .locals 1

    .prologue
    .line 326
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/k;->a()Ldbxyzptlk/db231222/t/k;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()Ldbxyzptlk/db231222/H/b;
    .locals 1

    .prologue
    .line 326
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/k;->a()Ldbxyzptlk/db231222/t/k;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()Ldbxyzptlk/db231222/H/t;
    .locals 1

    .prologue
    .line 326
    invoke-virtual {p0}, Ldbxyzptlk/db231222/t/k;->c()Ldbxyzptlk/db231222/t/i;

    move-result-object v0

    return-object v0
.end method
