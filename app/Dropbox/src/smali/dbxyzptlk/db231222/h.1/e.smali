.class final Ldbxyzptlk/db231222/h/e;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/h/f;
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ldbxyzptlk/db231222/h/a;

.field private b:Ljava/net/Socket;

.field private c:Z

.field private d:Z

.field private final e:[Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ldbxyzptlk/db231222/h/a;Ljava/net/Socket;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 539
    iput-object p1, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 537
    new-array v0, v1, [Ljava/lang/Object;

    iput-object v0, p0, Ldbxyzptlk/db231222/h/e;->e:[Ljava/lang/Object;

    .line 540
    iput-object p2, p0, Ldbxyzptlk/db231222/h/e;->b:Ljava/net/Socket;

    .line 541
    iput-boolean v1, p0, Ldbxyzptlk/db231222/h/e;->c:Z

    .line 542
    iput-boolean v1, p0, Ldbxyzptlk/db231222/h/e;->d:Z

    .line 543
    return-void
.end method

.method private a(I)Landroid/view/View;
    .locals 3

    .prologue
    .line 660
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 662
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v0}, Ldbxyzptlk/db231222/h/a;->a(Ldbxyzptlk/db231222/h/a;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 664
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v0}, Ldbxyzptlk/db231222/h/a;->b(Ldbxyzptlk/db231222/h/a;)Landroid/view/View;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 666
    iget-object v1, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v1}, Ldbxyzptlk/db231222/h/a;->a(Ldbxyzptlk/db231222/h/a;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 683
    :goto_0
    return-object v0

    .line 666
    :catchall_0
    move-exception v0

    iget-object v1, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v1}, Ldbxyzptlk/db231222/h/a;->a(Ldbxyzptlk/db231222/h/a;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0

    .line 672
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v0}, Ldbxyzptlk/db231222/h/a;->a(Ldbxyzptlk/db231222/h/a;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 674
    :try_start_1
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v0}, Ldbxyzptlk/db231222/h/a;->c(Ldbxyzptlk/db231222/h/a;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 675
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    if-ne v2, p1, :cond_1

    .line 676
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 680
    iget-object v1, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v1}, Ldbxyzptlk/db231222/h/a;->a(Ldbxyzptlk/db231222/h/a;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v0}, Ldbxyzptlk/db231222/h/a;->a(Ldbxyzptlk/db231222/h/a;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 683
    const/4 v0, 0x0

    goto :goto_0

    .line 680
    :catchall_1
    move-exception v0

    iget-object v1, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v1}, Ldbxyzptlk/db231222/h/a;->a(Ldbxyzptlk/db231222/h/a;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0
.end method

.method private a(Ljava/net/Socket;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 687
    const/4 v1, 0x1

    .line 688
    const/4 v0, 0x0

    .line 691
    :try_start_0
    iget-object v3, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v3}, Ldbxyzptlk/db231222/h/a;->a(Ldbxyzptlk/db231222/h/a;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 693
    invoke-virtual {p1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    .line 694
    new-instance v3, Ljava/io/BufferedWriter;

    new-instance v5, Ljava/io/OutputStreamWriter;

    invoke-direct {v5, v4}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    const/16 v4, 0x2000

    invoke-direct {v3, v5, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 696
    :try_start_1
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v0}, Ldbxyzptlk/db231222/h/a;->c(Ldbxyzptlk/db231222/h/a;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 697
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 698
    const/16 v5, 0x20

    invoke-virtual {v3, v5}, Ljava/io/BufferedWriter;->write(I)V

    .line 699
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v3, v0}, Ljava/io/BufferedWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 700
    const/16 v0, 0xa

    invoke-virtual {v3, v0}, Ljava/io/BufferedWriter;->write(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 705
    :catch_0
    move-exception v0

    move-object v0, v3

    .line 708
    :goto_1
    iget-object v1, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v1}, Ldbxyzptlk/db231222/h/a;->a(Ldbxyzptlk/db231222/h/a;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 710
    if-eqz v0, :cond_2

    .line 712
    :try_start_2
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move v0, v2

    .line 719
    :goto_2
    return v0

    .line 703
    :cond_0
    :try_start_3
    const-string v0, "DONE.\n"

    invoke-virtual {v3, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 704
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->flush()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 708
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v0}, Ldbxyzptlk/db231222/h/a;->a(Ldbxyzptlk/db231222/h/a;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 710
    if-eqz v3, :cond_3

    .line 712
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    move v0, v1

    .line 715
    goto :goto_2

    .line 713
    :catch_1
    move-exception v0

    move v0, v2

    .line 715
    goto :goto_2

    .line 713
    :catch_2
    move-exception v0

    move v0, v2

    .line 715
    goto :goto_2

    .line 708
    :catchall_0
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_3
    iget-object v1, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v1}, Ldbxyzptlk/db231222/h/a;->a(Ldbxyzptlk/db231222/h/a;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 710
    if-eqz v3, :cond_1

    .line 712
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 715
    :cond_1
    :goto_4
    throw v0

    .line 713
    :catch_3
    move-exception v1

    goto :goto_4

    .line 708
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 705
    :catch_4
    move-exception v1

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method private a(Ljava/net/Socket;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 605
    .line 606
    const/4 v2, 0x0

    .line 610
    const/16 v3, 0x20

    :try_start_0
    invoke-virtual {p3, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 611
    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 612
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v3

    .line 614
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {p3, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 615
    const/16 v5, 0x10

    invoke-static {v4, v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v4

    long-to-int v4, v4

    .line 618
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v3, v5, :cond_2

    .line 619
    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p3, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p3

    .line 624
    :goto_0
    invoke-direct {p0, v4}, Ldbxyzptlk/db231222/h/e;->a(I)Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 625
    if-nez v3, :cond_3

    .line 647
    if-eqz v2, :cond_1

    .line 649
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 656
    :cond_1
    :goto_1
    return v1

    .line 621
    :cond_2
    :try_start_2
    const-string p3, ""

    goto :goto_0

    .line 630
    :cond_3
    const-class v4, Landroid/view/ViewDebug;

    const-string v5, "dispatchCommand"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/view/View;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-class v8, Ljava/io/OutputStream;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 632
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 633
    const/4 v5, 0x0

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    const/4 v3, 0x1

    aput-object p2, v6, v3

    const/4 v3, 0x2

    aput-object p3, v6, v3

    const/4 v3, 0x3

    new-instance v7, Ldbxyzptlk/db231222/h/d;

    invoke-virtual {p1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v8

    invoke-direct {v7, v8}, Ldbxyzptlk/db231222/h/d;-><init>(Ljava/io/OutputStream;)V

    aput-object v7, v6, v3

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 636
    invoke-virtual {p1}, Ljava/net/Socket;->isOutputShutdown()Z

    move-result v3

    if-nez v3, :cond_7

    .line 637
    new-instance v3, Ljava/io/BufferedWriter;

    new-instance v4, Ljava/io/OutputStreamWriter;

    invoke-virtual {p1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 638
    :try_start_3
    const-string v2, "DONE\n"

    invoke-virtual {v3, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 639
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->flush()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 647
    :goto_2
    if-eqz v3, :cond_4

    .line 649
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    :cond_4
    :goto_3
    move v1, v0

    .line 656
    goto :goto_1

    .line 650
    :catch_0
    move-exception v0

    move v0, v1

    .line 652
    goto :goto_3

    .line 642
    :catch_1
    move-exception v0

    .line 643
    :goto_4
    :try_start_5
    const-string v3, "LocalViewServer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not send command "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " with parameters "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 647
    if-eqz v2, :cond_6

    .line 649
    :try_start_6
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    move v0, v1

    .line 652
    goto :goto_3

    .line 650
    :catch_2
    move-exception v0

    move v0, v1

    .line 652
    goto :goto_3

    .line 647
    :catchall_0
    move-exception v0

    :goto_5
    if-eqz v2, :cond_5

    .line 649
    :try_start_7
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 652
    :cond_5
    :goto_6
    throw v0

    .line 650
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :catch_4
    move-exception v1

    goto :goto_6

    .line 647
    :catchall_1
    move-exception v0

    move-object v2, v3

    goto :goto_5

    .line 642
    :catch_5
    move-exception v0

    move-object v2, v3

    goto :goto_4

    :cond_6
    move v0, v1

    goto :goto_3

    :cond_7
    move-object v3, v2

    goto :goto_2
.end method

.method private b(Ljava/net/Socket;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 723
    const/4 v1, 0x1

    .line 726
    const/4 v0, 0x0

    .line 728
    :try_start_0
    invoke-virtual {p1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    .line 729
    new-instance v3, Ljava/io/BufferedWriter;

    new-instance v5, Ljava/io/OutputStreamWriter;

    invoke-direct {v5, v4}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    const/16 v4, 0x2000

    invoke-direct {v3, v5, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 733
    :try_start_1
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v0}, Ldbxyzptlk/db231222/h/a;->d(Ldbxyzptlk/db231222/h/a;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 735
    :try_start_2
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v0}, Ldbxyzptlk/db231222/h/a;->b(Ldbxyzptlk/db231222/h/a;)Landroid/view/View;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    .line 737
    :try_start_3
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v0}, Ldbxyzptlk/db231222/h/a;->d(Ldbxyzptlk/db231222/h/a;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 740
    if-eqz v4, :cond_0

    .line 741
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v0}, Ldbxyzptlk/db231222/h/a;->a(Ldbxyzptlk/db231222/h/a;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 743
    :try_start_4
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v0}, Ldbxyzptlk/db231222/h/a;->c(Ldbxyzptlk/db231222/h/a;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v5, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v5}, Ldbxyzptlk/db231222/h/a;->b(Ldbxyzptlk/db231222/h/a;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 745
    :try_start_5
    iget-object v5, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v5}, Ldbxyzptlk/db231222/h/a;->a(Ldbxyzptlk/db231222/h/a;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 748
    invoke-static {v4}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 749
    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Ljava/io/BufferedWriter;->write(I)V

    .line 750
    invoke-virtual {v3, v0}, Ljava/io/BufferedWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 752
    :cond_0
    const/16 v0, 0xa

    invoke-virtual {v3, v0}, Ljava/io/BufferedWriter;->write(I)V

    .line 753
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->flush()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 757
    if-eqz v3, :cond_3

    .line 759
    :try_start_6
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    move v0, v1

    .line 766
    :goto_0
    return v0

    .line 737
    :catchall_0
    move-exception v0

    :try_start_7
    iget-object v1, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v1}, Ldbxyzptlk/db231222/h/a;->d(Ldbxyzptlk/db231222/h/a;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 754
    :catch_0
    move-exception v0

    move-object v0, v3

    .line 757
    :goto_1
    if-eqz v0, :cond_2

    .line 759
    :try_start_8
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    move v0, v2

    .line 762
    goto :goto_0

    .line 745
    :catchall_1
    move-exception v0

    :try_start_9
    iget-object v1, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v1}, Ldbxyzptlk/db231222/h/a;->a(Ldbxyzptlk/db231222/h/a;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 757
    :catchall_2
    move-exception v0

    :goto_2
    if-eqz v3, :cond_1

    .line 759
    :try_start_a
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3

    .line 762
    :cond_1
    :goto_3
    throw v0

    .line 760
    :catch_1
    move-exception v0

    move v0, v2

    .line 762
    goto :goto_0

    .line 760
    :catch_2
    move-exception v0

    move v0, v2

    .line 762
    goto :goto_0

    .line 760
    :catch_3
    move-exception v1

    goto :goto_3

    .line 757
    :catchall_3
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    goto :goto_2

    .line 754
    :catch_4
    move-exception v1

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private c()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 786
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v0, p0}, Ldbxyzptlk/db231222/h/a;->a(Ldbxyzptlk/db231222/h/a;Ldbxyzptlk/db231222/h/f;)V

    .line 787
    const/4 v2, 0x0

    .line 789
    :try_start_0
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v0, Ljava/io/OutputStreamWriter;

    iget-object v5, p0, Ldbxyzptlk/db231222/h/e;->b:Ljava/net/Socket;

    invoke-virtual {v5}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, v0}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 790
    :cond_0
    :goto_0
    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-nez v0, :cond_5

    .line 793
    iget-object v5, p0, Ldbxyzptlk/db231222/h/e;->e:[Ljava/lang/Object;

    monitor-enter v5
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 794
    :goto_1
    :try_start_2
    iget-boolean v0, p0, Ldbxyzptlk/db231222/h/e;->c:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Ldbxyzptlk/db231222/h/e;->d:Z

    if-nez v0, :cond_2

    .line 795
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->e:[Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    goto :goto_1

    .line 805
    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 815
    :catch_0
    move-exception v0

    .line 816
    :goto_2
    :try_start_4
    const-string v2, "LocalViewServer"

    const-string v4, "Connection error: "

    invoke-static {v2, v4, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 818
    if-eqz v1, :cond_1

    .line 820
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 825
    :cond_1
    :goto_3
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v0, p0}, Ldbxyzptlk/db231222/h/a;->b(Ldbxyzptlk/db231222/h/a;Ldbxyzptlk/db231222/h/f;)V

    .line 827
    :goto_4
    return v3

    .line 797
    :cond_2
    :try_start_6
    iget-boolean v0, p0, Ldbxyzptlk/db231222/h/e;->c:Z

    if-eqz v0, :cond_8

    .line 798
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/h/e;->c:Z

    move v2, v3

    .line 801
    :goto_5
    iget-boolean v0, p0, Ldbxyzptlk/db231222/h/e;->d:Z

    if-eqz v0, :cond_7

    .line 802
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/h/e;->d:Z

    move v0, v3

    .line 805
    :goto_6
    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 806
    if-eqz v2, :cond_3

    .line 807
    :try_start_7
    const-string v2, "LIST UPDATE\n"

    invoke-virtual {v1, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 808
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->flush()V

    .line 810
    :cond_3
    if-eqz v0, :cond_0

    .line 811
    const-string v0, "FOCUS UPDATE\n"

    invoke-virtual {v1, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 812
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->flush()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_0

    .line 818
    :catchall_1
    move-exception v0

    :goto_7
    if-eqz v1, :cond_4

    .line 820
    :try_start_8
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 825
    :cond_4
    :goto_8
    iget-object v1, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v1, p0}, Ldbxyzptlk/db231222/h/a;->b(Ldbxyzptlk/db231222/h/a;Ldbxyzptlk/db231222/h/f;)V

    throw v0

    .line 818
    :cond_5
    if-eqz v1, :cond_6

    .line 820
    :try_start_9
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1

    .line 825
    :cond_6
    :goto_9
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->a:Ldbxyzptlk/db231222/h/a;

    invoke-static {v0, p0}, Ldbxyzptlk/db231222/h/a;->b(Ldbxyzptlk/db231222/h/a;Ldbxyzptlk/db231222/h/f;)V

    goto :goto_4

    .line 821
    :catch_1
    move-exception v0

    goto :goto_9

    :catch_2
    move-exception v0

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_8

    .line 818
    :catchall_2
    move-exception v0

    move-object v1, v2

    goto :goto_7

    .line 815
    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_2

    :cond_7
    move v0, v4

    goto :goto_6

    :cond_8
    move v2, v4

    goto :goto_5
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 771
    iget-object v1, p0, Ldbxyzptlk/db231222/h/e;->e:[Ljava/lang/Object;

    monitor-enter v1

    .line 772
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Ldbxyzptlk/db231222/h/e;->c:Z

    .line 773
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->e:[Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 774
    monitor-exit v1

    .line 775
    return-void

    .line 774
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 779
    iget-object v1, p0, Ldbxyzptlk/db231222/h/e;->e:[Ljava/lang/Object;

    monitor-enter v1

    .line 780
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Ldbxyzptlk/db231222/h/e;->d:Z

    .line 781
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->e:[Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 782
    monitor-exit v1

    .line 783
    return-void

    .line 782
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final run()V
    .locals 5

    .prologue
    .line 547
    const/4 v2, 0x0

    .line 549
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/InputStreamReader;

    iget-object v3, p0, Ldbxyzptlk/db231222/h/e;->b:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    const/16 v3, 0x400

    invoke-direct {v1, v0, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 551
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 556
    const/16 v0, 0x20

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 557
    const/4 v3, -0x1

    if-ne v0, v3, :cond_3

    .line 559
    const-string v0, ""

    .line 566
    :goto_0
    const-string v3, "PROTOCOL"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 567
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->b:Ljava/net/Socket;

    const-string v3, "4"

    invoke-static {v0, v3}, Ldbxyzptlk/db231222/h/a;->a(Ljava/net/Socket;Ljava/lang/String;)Z

    move-result v0

    .line 580
    :goto_1
    if-nez v0, :cond_0

    .line 581
    const-string v0, "LocalViewServer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "An error occurred with the command: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 586
    :cond_0
    if-eqz v1, :cond_1

    .line 588
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 594
    :cond_1
    :goto_2
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->b:Ljava/net/Socket;

    if-eqz v0, :cond_2

    .line 596
    :try_start_3
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->b:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 602
    :cond_2
    :goto_3
    return-void

    .line 561
    :cond_3
    const/4 v3, 0x0

    :try_start_4
    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 562
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v3

    goto :goto_0

    .line 568
    :cond_4
    const-string v3, "SERVER"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 569
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->b:Ljava/net/Socket;

    const-string v3, "4"

    invoke-static {v0, v3}, Ldbxyzptlk/db231222/h/a;->a(Ljava/net/Socket;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 570
    :cond_5
    const-string v3, "LIST"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 571
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->b:Ljava/net/Socket;

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/h/e;->a(Ljava/net/Socket;)Z

    move-result v0

    goto :goto_1

    .line 572
    :cond_6
    const-string v3, "GET_FOCUS"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 573
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->b:Ljava/net/Socket;

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/h/e;->b(Ljava/net/Socket;)Z

    move-result v0

    goto :goto_1

    .line 574
    :cond_7
    const-string v3, "AUTOLIST"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 575
    invoke-direct {p0}, Ldbxyzptlk/db231222/h/e;->c()Z

    move-result v0

    goto :goto_1

    .line 577
    :cond_8
    iget-object v3, p0, Ldbxyzptlk/db231222/h/e;->b:Ljava/net/Socket;

    invoke-direct {p0, v3, v2, v0}, Ldbxyzptlk/db231222/h/e;->a(Ljava/net/Socket;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v0

    goto :goto_1

    .line 590
    :catch_0
    move-exception v0

    .line 591
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 597
    :catch_1
    move-exception v0

    .line 598
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 583
    :catch_2
    move-exception v0

    move-object v1, v2

    .line 584
    :goto_4
    :try_start_5
    const-string v2, "LocalViewServer"

    const-string v3, "Connection error: "

    invoke-static {v2, v3, v0}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 586
    if-eqz v1, :cond_9

    .line 588
    :try_start_6
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 594
    :cond_9
    :goto_5
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->b:Ljava/net/Socket;

    if-eqz v0, :cond_2

    .line 596
    :try_start_7
    iget-object v0, p0, Ldbxyzptlk/db231222/h/e;->b:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_3

    .line 597
    :catch_3
    move-exception v0

    .line 598
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 590
    :catch_4
    move-exception v0

    .line 591
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 586
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_6
    if-eqz v1, :cond_a

    .line 588
    :try_start_8
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 594
    :cond_a
    :goto_7
    iget-object v1, p0, Ldbxyzptlk/db231222/h/e;->b:Ljava/net/Socket;

    if-eqz v1, :cond_b

    .line 596
    :try_start_9
    iget-object v1, p0, Ldbxyzptlk/db231222/h/e;->b:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 599
    :cond_b
    :goto_8
    throw v0

    .line 590
    :catch_5
    move-exception v1

    .line 591
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 597
    :catch_6
    move-exception v1

    .line 598
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 586
    :catchall_1
    move-exception v0

    goto :goto_6

    .line 583
    :catch_7
    move-exception v0

    goto :goto_4
.end method
