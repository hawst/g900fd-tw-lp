.class public final Ldbxyzptlk/db231222/f/g;
.super Ldbxyzptlk/db231222/f/i;
.source "panda.py"


# instance fields
.field private b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ldbxyzptlk/db231222/f/i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ldbxyzptlk/db231222/f/i;-><init>()V

    .line 53
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/f/g;->b:Ljava/util/HashMap;

    .line 54
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ldbxyzptlk/db231222/f/i;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Ldbxyzptlk/db231222/f/g;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/f/i;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ldbxyzptlk/db231222/f/i;)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Ldbxyzptlk/db231222/f/g;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 147
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p1, Ldbxyzptlk/db231222/f/g;

    iget-object v0, p1, Ldbxyzptlk/db231222/f/g;->b:Ljava/util/HashMap;

    iget-object v1, p0, Ldbxyzptlk/db231222/f/g;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 160
    .line 161
    iget-object v0, p0, Ldbxyzptlk/db231222/f/g;->b:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/f/g;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->hashCode()I

    move-result v0

    :goto_0
    add-int/lit16 v0, v0, 0x245

    .line 162
    return v0

    .line 161
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
