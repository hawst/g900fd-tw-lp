.class public final Ldbxyzptlk/db231222/f/j;
.super Ldbxyzptlk/db231222/f/i;
.source "panda.py"


# instance fields
.field private b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ldbxyzptlk/db231222/f/i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ldbxyzptlk/db231222/f/i;-><init>()V

    .line 47
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/f/j;->b:Ljava/util/Set;

    .line 48
    return-void
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/f/i;)V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Ldbxyzptlk/db231222/f/j;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 65
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 163
    if-nez p1, :cond_1

    .line 173
    :cond_0
    :goto_0
    return v0

    .line 166
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 169
    check-cast p1, Ldbxyzptlk/db231222/f/j;

    .line 170
    iget-object v1, p0, Ldbxyzptlk/db231222/f/j;->b:Ljava/util/Set;

    iget-object v2, p1, Ldbxyzptlk/db231222/f/j;->b:Ljava/util/Set;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Ldbxyzptlk/db231222/f/j;->b:Ljava/util/Set;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldbxyzptlk/db231222/f/j;->b:Ljava/util/Set;

    iget-object v2, p1, Ldbxyzptlk/db231222/f/j;->b:Ljava/util/Set;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 173
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 156
    .line 157
    iget-object v0, p0, Ldbxyzptlk/db231222/f/j;->b:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/f/j;->b:Ljava/util/Set;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    add-int/lit16 v0, v0, 0xcb

    .line 158
    return v0

    .line 157
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
