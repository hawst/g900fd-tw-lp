.class public final Ldbxyzptlk/db231222/f/f;
.super Ldbxyzptlk/db231222/f/i;
.source "panda.py"


# static fields
.field private static final c:Ljava/text/SimpleDateFormat;

.field private static final d:Ljava/text/SimpleDateFormat;


# instance fields
.field private b:Ljava/util/Date;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/f/f;->c:Ljava/text/SimpleDateFormat;

    .line 46
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd HH:mm:ss Z"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/f/f;->d:Ljava/text/SimpleDateFormat;

    .line 48
    sget-object v0, Ldbxyzptlk/db231222/f/f;->c:Ljava/text/SimpleDateFormat;

    const-string v1, "GMT"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 49
    sget-object v0, Ldbxyzptlk/db231222/f/f;->d:Ljava/text/SimpleDateFormat;

    const-string v1, "GMT"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Ldbxyzptlk/db231222/f/i;-><init>()V

    .line 94
    invoke-static {p1}, Ldbxyzptlk/db231222/f/f;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/f/f;->b:Ljava/util/Date;

    .line 95
    return-void
.end method

.method public constructor <init>([B)V
    .locals 7

    .prologue
    .line 82
    invoke-direct {p0}, Ldbxyzptlk/db231222/f/i;-><init>()V

    .line 84
    new-instance v0, Ljava/util/Date;

    const-wide v1, 0xe3c7a73400L

    const-wide v3, 0x408f400000000000L    # 1000.0

    invoke-static {p1}, Ldbxyzptlk/db231222/f/c;->d([B)D

    move-result-wide v5

    mul-double/2addr v3, v5

    double-to-long v3, v3

    add-long/2addr v1, v3

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Ldbxyzptlk/db231222/f/f;->b:Ljava/util/Date;

    .line 85
    return-void
.end method

.method private static declared-synchronized a(Ljava/lang/String;)Ljava/util/Date;
    .locals 2

    .prologue
    .line 62
    const-class v1, Ldbxyzptlk/db231222/f/f;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ldbxyzptlk/db231222/f/f;->c:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 64
    :goto_0
    monitor-exit v1

    return-object v0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    :try_start_1
    sget-object v0, Ldbxyzptlk/db231222/f/f;->d:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/util/Date;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Ldbxyzptlk/db231222/f/f;->b:Ljava/util/Date;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 117
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/f/f;->b:Ljava/util/Date;

    check-cast p1, Ldbxyzptlk/db231222/f/f;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/f/f;->a()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Ldbxyzptlk/db231222/f/f;->b:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Ldbxyzptlk/db231222/f/f;->b:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
