.class public final Ldbxyzptlk/db231222/f/n;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static a:Ljavax/xml/parsers/DocumentBuilderFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    sput-object v0, Ldbxyzptlk/db231222/f/n;->a:Ljavax/xml/parsers/DocumentBuilderFactory;

    return-void
.end method

.method public static a(Ljava/io/File;)Ldbxyzptlk/db231222/f/i;
    .locals 1

    .prologue
    .line 97
    invoke-static {}, Ldbxyzptlk/db231222/f/n;->b()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    .line 99
    invoke-virtual {v0, p0}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/File;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 101
    invoke-static {v0}, Ldbxyzptlk/db231222/f/n;->a(Lorg/w3c/dom/Document;)Ldbxyzptlk/db231222/f/i;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lorg/w3c/dom/Document;)Ldbxyzptlk/db231222/f/i;
    .locals 2

    .prologue
    .line 137
    invoke-interface {p0}, Lorg/w3c/dom/Document;->getDoctype()Lorg/w3c/dom/DocumentType;

    move-result-object v0

    .line 138
    if-nez v0, :cond_0

    .line 139
    invoke-interface {p0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Element;->getNodeName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "plist"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 140
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "The given XML document is not a property list."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 143
    :cond_0
    invoke-interface {v0}, Lorg/w3c/dom/DocumentType;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "plist"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 144
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "The given XML document is not a property list."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :cond_1
    invoke-interface {p0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/f/n;->a(Lorg/w3c/dom/NodeList;)Ljava/util/List;

    move-result-object v0

    .line 149
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 150
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Node;

    invoke-static {v0}, Ldbxyzptlk/db231222/f/n;->a(Lorg/w3c/dom/Node;)Ldbxyzptlk/db231222/f/i;

    move-result-object v0

    return-object v0

    .line 152
    :cond_2
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "No root node found!"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Lorg/w3c/dom/Node;)Ldbxyzptlk/db231222/f/i;
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 162
    invoke-interface {p0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v0

    .line 163
    const-string v1, "dict"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 164
    new-instance v7, Ldbxyzptlk/db231222/f/g;

    invoke-direct {v7}, Ldbxyzptlk/db231222/f/g;-><init>()V

    .line 165
    invoke-interface {p0}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/f/n;->a(Lorg/w3c/dom/NodeList;)Ljava/util/List;

    move-result-object v8

    move v2, v3

    .line 166
    :goto_0
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 167
    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Node;

    .line 168
    add-int/lit8 v1, v2, 0x1

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/w3c/dom/Node;

    .line 170
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v4

    invoke-interface {v4, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    invoke-interface {v4}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v4

    move-object v6, v4

    move v4, v5

    .line 174
    :goto_1
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v9

    invoke-interface {v9}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v9

    if-ge v4, v9, :cond_0

    .line 175
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v9

    invoke-interface {v9, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v9

    invoke-interface {v9}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 174
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 177
    :cond_0
    invoke-static {v1}, Ldbxyzptlk/db231222/f/n;->a(Lorg/w3c/dom/Node;)Ldbxyzptlk/db231222/f/i;

    move-result-object v0

    invoke-virtual {v7, v6, v0}, Ldbxyzptlk/db231222/f/g;->a(Ljava/lang/String;Ldbxyzptlk/db231222/f/i;)V

    .line 166
    add-int/lit8 v0, v2, 0x2

    move v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v7

    .line 212
    :goto_2
    return-object v0

    .line 180
    :cond_2
    const-string v1, "array"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 181
    invoke-interface {p0}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/f/n;->a(Lorg/w3c/dom/NodeList;)Ljava/util/List;

    move-result-object v2

    .line 182
    new-instance v1, Ldbxyzptlk/db231222/f/d;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/f/d;-><init>(I)V

    .line 183
    :goto_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 184
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Node;

    invoke-static {v0}, Ldbxyzptlk/db231222/f/n;->a(Lorg/w3c/dom/Node;)Ldbxyzptlk/db231222/f/i;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Ldbxyzptlk/db231222/f/d;->a(ILdbxyzptlk/db231222/f/i;)V

    .line 183
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_3
    move-object v0, v1

    .line 186
    goto :goto_2

    .line 187
    :cond_4
    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 188
    new-instance v0, Ldbxyzptlk/db231222/f/h;

    invoke-direct {v0, v5}, Ldbxyzptlk/db231222/f/h;-><init>(Z)V

    goto :goto_2

    .line 189
    :cond_5
    const-string v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 190
    new-instance v0, Ldbxyzptlk/db231222/f/h;

    invoke-direct {v0, v3}, Ldbxyzptlk/db231222/f/h;-><init>(Z)V

    goto :goto_2

    .line 191
    :cond_6
    const-string v1, "integer"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 192
    new-instance v0, Ldbxyzptlk/db231222/f/h;

    invoke-interface {p0}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    invoke-interface {v1, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v1

    invoke-interface {v1}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/f/h;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 193
    :cond_7
    const-string v1, "real"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 194
    new-instance v0, Ldbxyzptlk/db231222/f/h;

    invoke-interface {p0}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    invoke-interface {v1, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v1

    invoke-interface {v1}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/f/h;-><init>(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 195
    :cond_8
    const-string v1, "string"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 196
    invoke-interface {p0}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 197
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v0

    if-nez v0, :cond_9

    .line 198
    new-instance v0, Ldbxyzptlk/db231222/f/k;

    const-string v1, ""

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/f/k;-><init>(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 200
    :cond_9
    invoke-interface {v1, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v0

    .line 203
    :goto_4
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    if-ge v5, v2, :cond_a

    .line 204
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v1, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 203
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 205
    :cond_a
    new-instance v1, Ldbxyzptlk/db231222/f/k;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/f/k;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    goto/16 :goto_2

    .line 207
    :cond_b
    const-string v1, "data"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 208
    new-instance v0, Ldbxyzptlk/db231222/f/e;

    invoke-interface {p0}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    invoke-interface {v1, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v1

    invoke-interface {v1}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/f/e;-><init>(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 209
    :cond_c
    const-string v1, "date"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 210
    new-instance v0, Ldbxyzptlk/db231222/f/f;

    invoke-interface {p0}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    invoke-interface {v1, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v1

    invoke-interface {v1}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/f/f;-><init>(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 212
    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_2
.end method

.method private static a(Lorg/w3c/dom/NodeList;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/w3c/dom/NodeList;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/w3c/dom/Node;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 222
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 223
    invoke-interface {p0, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 224
    invoke-interface {p0, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 227
    :cond_1
    return-object v1
.end method

.method private static declared-synchronized a()V
    .locals 3

    .prologue
    .line 61
    const-class v1, Ldbxyzptlk/db231222/f/n;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/f/n;->a:Ljavax/xml/parsers/DocumentBuilderFactory;

    .line 62
    sget-object v0, Ldbxyzptlk/db231222/f/n;->a:Ljavax/xml/parsers/DocumentBuilderFactory;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljavax/xml/parsers/DocumentBuilderFactory;->setIgnoringComments(Z)V

    .line 63
    sget-object v0, Ldbxyzptlk/db231222/f/n;->a:Ljavax/xml/parsers/DocumentBuilderFactory;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljavax/xml/parsers/DocumentBuilderFactory;->setCoalescing(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    monitor-exit v1

    return-void

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized b()Ljavax/xml/parsers/DocumentBuilder;
    .locals 3

    .prologue
    .line 72
    const-class v1, Ldbxyzptlk/db231222/f/n;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ldbxyzptlk/db231222/f/n;->a:Ljavax/xml/parsers/DocumentBuilderFactory;

    if-nez v0, :cond_0

    .line 73
    invoke-static {}, Ldbxyzptlk/db231222/f/n;->a()V

    .line 74
    :cond_0
    sget-object v0, Ldbxyzptlk/db231222/f/n;->a:Ljavax/xml/parsers/DocumentBuilderFactory;

    invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    .line 75
    new-instance v2, Ldbxyzptlk/db231222/f/o;

    invoke-direct {v2}, Ldbxyzptlk/db231222/f/o;-><init>()V

    invoke-virtual {v0, v2}, Ljavax/xml/parsers/DocumentBuilder;->setEntityResolver(Lorg/xml/sax/EntityResolver;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    monitor-exit v1

    return-object v0

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
