.class public final Ldbxyzptlk/db231222/f/h;
.super Ldbxyzptlk/db231222/f/i;
.source "panda.py"


# instance fields
.field private b:I

.field private c:J

.field private d:D

.field private e:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 94
    invoke-direct {p0}, Ldbxyzptlk/db231222/f/i;-><init>()V

    .line 96
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 97
    iput-wide v0, p0, Ldbxyzptlk/db231222/f/h;->c:J

    long-to-double v0, v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/f/h;->d:D

    .line 98
    const/4 v0, 0x0

    iput v0, p0, Ldbxyzptlk/db231222/f/h;->b:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    :goto_0
    return-void

    .line 99
    :catch_0
    move-exception v0

    .line 101
    :try_start_1
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 102
    iput-wide v0, p0, Ldbxyzptlk/db231222/f/h;->d:D

    double-to-long v0, v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/f/h;->c:J

    .line 103
    const/4 v0, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/f/h;->b:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 104
    :catch_1
    move-exception v0

    .line 106
    :try_start_2
    invoke-static {p1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldbxyzptlk/db231222/f/h;->e:Z

    .line 107
    const/4 v0, 0x2

    iput v0, p0, Ldbxyzptlk/db231222/f/h;->b:I

    .line 108
    iget-boolean v0, p0, Ldbxyzptlk/db231222/f/h;->e:Z

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x1

    :goto_1
    iput-wide v0, p0, Ldbxyzptlk/db231222/f/h;->c:J

    long-to-double v0, v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/f/h;->d:D
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 109
    :catch_2
    move-exception v0

    .line 110
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Given text neither represents a double, int nor boolean value."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_1
.end method

.method public constructor <init>(Z)V
    .locals 2

    .prologue
    .line 138
    invoke-direct {p0}, Ldbxyzptlk/db231222/f/i;-><init>()V

    .line 139
    iput-boolean p1, p0, Ldbxyzptlk/db231222/f/h;->e:Z

    .line 140
    if-eqz p1, :cond_0

    const-wide/16 v0, 0x1

    :goto_0
    iput-wide v0, p0, Ldbxyzptlk/db231222/f/h;->c:J

    long-to-double v0, v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/f/h;->d:D

    .line 141
    const/4 v0, 0x2

    iput v0, p0, Ldbxyzptlk/db231222/f/h;->b:I

    .line 142
    return-void

    .line 140
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>([BI)V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0}, Ldbxyzptlk/db231222/f/i;-><init>()V

    .line 69
    packed-switch p2, :pswitch_data_0

    .line 80
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Type argument is not valid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :pswitch_0
    invoke-static {p1}, Ldbxyzptlk/db231222/f/c;->c([B)J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/f/h;->c:J

    long-to-double v0, v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/f/h;->d:D

    .line 83
    :goto_0
    iput p2, p0, Ldbxyzptlk/db231222/f/h;->b:I

    .line 84
    return-void

    .line 75
    :pswitch_1
    invoke-static {p1}, Ldbxyzptlk/db231222/f/c;->d([B)D

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/f/h;->d:D

    .line 76
    iget-wide v0, p0, Ldbxyzptlk/db231222/f/h;->d:D

    double-to-long v0, v0

    iput-wide v0, p0, Ldbxyzptlk/db231222/f/h;->c:J

    goto :goto_0

    .line 69
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    .line 160
    iget v0, p0, Ldbxyzptlk/db231222/f/h;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 161
    iget-boolean v0, p0, Ldbxyzptlk/db231222/f/h;->e:Z

    .line 163
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Ldbxyzptlk/db231222/f/h;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 171
    iget-wide v0, p0, Ldbxyzptlk/db231222/f/h;->c:J

    return-wide v0
.end method

.method public final c()D
    .locals 2

    .prologue
    .line 190
    iget-wide v0, p0, Ldbxyzptlk/db231222/f/h;->d:D

    return-wide v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 209
    instance-of v1, p1, Ldbxyzptlk/db231222/f/h;

    if-nez v1, :cond_1

    .line 211
    :cond_0
    :goto_0
    return v0

    .line 210
    :cond_1
    check-cast p1, Ldbxyzptlk/db231222/f/h;

    .line 211
    iget v1, p0, Ldbxyzptlk/db231222/f/h;->b:I

    iget v2, p1, Ldbxyzptlk/db231222/f/h;->b:I

    if-ne v1, v2, :cond_0

    iget-wide v1, p0, Ldbxyzptlk/db231222/f/h;->c:J

    iget-wide v3, p1, Ldbxyzptlk/db231222/f/h;->c:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-wide v1, p0, Ldbxyzptlk/db231222/f/h;->d:D

    iget-wide v3, p1, Ldbxyzptlk/db231222/f/h;->d:D

    cmpl-double v1, v1, v3

    if-nez v1, :cond_0

    iget-boolean v1, p0, Ldbxyzptlk/db231222/f/h;->e:Z

    iget-boolean v2, p1, Ldbxyzptlk/db231222/f/h;->e:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    const/16 v5, 0x20

    .line 216
    iget v0, p0, Ldbxyzptlk/db231222/f/h;->b:I

    .line 217
    mul-int/lit8 v0, v0, 0x25

    iget-wide v1, p0, Ldbxyzptlk/db231222/f/h;->c:J

    iget-wide v3, p0, Ldbxyzptlk/db231222/f/h;->c:J

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    .line 218
    mul-int/lit8 v0, v0, 0x25

    iget-wide v1, p0, Ldbxyzptlk/db231222/f/h;->d:D

    invoke-static {v1, v2}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v1

    iget-wide v3, p0, Ldbxyzptlk/db231222/f/h;->d:D

    invoke-static {v3, v4}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v3

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    .line 219
    mul-int/lit8 v1, v0, 0x25

    invoke-virtual {p0}, Ldbxyzptlk/db231222/f/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    .line 220
    return v0

    .line 219
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 226
    iget v0, p0, Ldbxyzptlk/db231222/f/h;->b:I

    packed-switch v0, :pswitch_data_0

    .line 237
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 228
    :pswitch_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/f/h;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 231
    :pswitch_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/f/h;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 234
    :pswitch_2
    invoke-virtual {p0}, Ldbxyzptlk/db231222/f/h;->a()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 226
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
