.class public final Ldbxyzptlk/db231222/f/e;
.super Ldbxyzptlk/db231222/f/i;
.source "panda.py"


# instance fields
.field private b:[B


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Ldbxyzptlk/db231222/f/i;-><init>()V

    .line 56
    const-string v0, "\\s+"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 57
    invoke-static {v0}, Ldbxyzptlk/db231222/f/b;->a(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/f/e;->b:[B

    .line 58
    return-void
.end method

.method public constructor <init>([B)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ldbxyzptlk/db231222/f/i;-><init>()V

    .line 45
    iput-object p1, p0, Ldbxyzptlk/db231222/f/e;->b:[B

    .line 46
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 118
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p1, Ldbxyzptlk/db231222/f/e;

    iget-object v0, p1, Ldbxyzptlk/db231222/f/e;->b:[B

    iget-object v1, p0, Ldbxyzptlk/db231222/f/e;->b:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 123
    .line 124
    iget-object v0, p0, Ldbxyzptlk/db231222/f/e;->b:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    add-int/lit16 v0, v0, 0x14f

    .line 125
    return v0
.end method
