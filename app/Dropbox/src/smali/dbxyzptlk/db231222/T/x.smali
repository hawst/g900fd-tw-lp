.class public final Ldbxyzptlk/db231222/T/x;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:I

.field private final c:Ldbxyzptlk/db231222/T/q;

.field private final d:I

.field private final e:I

.field private f:J

.field private g:I

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ldbxyzptlk/db231222/T/z;

.field private final k:Ldbxyzptlk/db231222/T/A;

.field private l:Ldbxyzptlk/db231222/T/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Ldbxyzptlk/db231222/T/x;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Ldbxyzptlk/db231222/T/x;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(ILdbxyzptlk/db231222/T/q;ZZIILjava/util/List;Ldbxyzptlk/db231222/T/k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ldbxyzptlk/db231222/T/q;",
            "ZZII",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ldbxyzptlk/db231222/T/k;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ldbxyzptlk/db231222/T/x;->f:J

    .line 57
    new-instance v0, Ldbxyzptlk/db231222/T/z;

    invoke-direct {v0, p0, v2}, Ldbxyzptlk/db231222/T/z;-><init>(Ldbxyzptlk/db231222/T/x;Ldbxyzptlk/db231222/T/y;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/T/x;->j:Ldbxyzptlk/db231222/T/z;

    .line 58
    new-instance v0, Ldbxyzptlk/db231222/T/A;

    invoke-direct {v0, p0, v2}, Ldbxyzptlk/db231222/T/A;-><init>(Ldbxyzptlk/db231222/T/x;Ldbxyzptlk/db231222/T/y;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/T/x;->k:Ldbxyzptlk/db231222/T/A;

    .line 65
    iput-object v2, p0, Ldbxyzptlk/db231222/T/x;->l:Ldbxyzptlk/db231222/T/a;

    .line 69
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "connection == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_0
    if-nez p7, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "requestHeaders == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_1
    iput p1, p0, Ldbxyzptlk/db231222/T/x;->b:I

    .line 72
    iput-object p2, p0, Ldbxyzptlk/db231222/T/x;->c:Ldbxyzptlk/db231222/T/q;

    .line 73
    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->j:Ldbxyzptlk/db231222/T/z;

    invoke-static {v0, p4}, Ldbxyzptlk/db231222/T/z;->a(Ldbxyzptlk/db231222/T/z;Z)Z

    .line 74
    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->k:Ldbxyzptlk/db231222/T/A;

    invoke-static {v0, p3}, Ldbxyzptlk/db231222/T/A;->a(Ldbxyzptlk/db231222/T/A;Z)Z

    .line 75
    iput p5, p0, Ldbxyzptlk/db231222/T/x;->d:I

    .line 76
    iput p6, p0, Ldbxyzptlk/db231222/T/x;->e:I

    .line 77
    iput-object p7, p0, Ldbxyzptlk/db231222/T/x;->h:Ljava/util/List;

    .line 79
    invoke-direct {p0, p8}, Ldbxyzptlk/db231222/T/x;->b(Ldbxyzptlk/db231222/T/k;)V

    .line 80
    return-void
.end method

.method static synthetic a(Ldbxyzptlk/db231222/T/x;)I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Ldbxyzptlk/db231222/T/x;->b:I

    return v0
.end method

.method static synthetic b(Ldbxyzptlk/db231222/T/x;)Ldbxyzptlk/db231222/T/q;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->c:Ldbxyzptlk/db231222/T/q;

    return-object v0
.end method

.method private b(Ldbxyzptlk/db231222/T/k;)V
    .locals 2

    .prologue
    const/high16 v0, 0x10000

    .line 309
    sget-boolean v1, Ldbxyzptlk/db231222/T/x;->a:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Ldbxyzptlk/db231222/T/x;->c:Ldbxyzptlk/db231222/T/q;

    invoke-static {v1}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 310
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1, v0}, Ldbxyzptlk/db231222/T/k;->d(I)I

    move-result v0

    :cond_1
    iput v0, p0, Ldbxyzptlk/db231222/T/x;->g:I

    .line 313
    return-void
.end method

.method static synthetic c(Ldbxyzptlk/db231222/T/x;)J
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Ldbxyzptlk/db231222/T/x;->f:J

    return-wide v0
.end method

.method static synthetic d(Ldbxyzptlk/db231222/T/x;)Ldbxyzptlk/db231222/T/a;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->l:Ldbxyzptlk/db231222/T/a;

    return-object v0
.end method

.method private d(Ldbxyzptlk/db231222/T/a;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 228
    sget-boolean v1, Ldbxyzptlk/db231222/T/x;->a:Z

    if-nez v1, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 229
    :cond_0
    monitor-enter p0

    .line 230
    :try_start_0
    iget-object v1, p0, Ldbxyzptlk/db231222/T/x;->l:Ldbxyzptlk/db231222/T/a;

    if-eqz v1, :cond_1

    .line 231
    monitor-exit p0

    .line 240
    :goto_0
    return v0

    .line 233
    :cond_1
    iget-object v1, p0, Ldbxyzptlk/db231222/T/x;->j:Ldbxyzptlk/db231222/T/z;

    invoke-static {v1}, Ldbxyzptlk/db231222/T/z;->a(Ldbxyzptlk/db231222/T/z;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Ldbxyzptlk/db231222/T/x;->k:Ldbxyzptlk/db231222/T/A;

    invoke-static {v1}, Ldbxyzptlk/db231222/T/A;->a(Ldbxyzptlk/db231222/T/A;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 234
    monitor-exit p0

    goto :goto_0

    .line 238
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 236
    :cond_2
    :try_start_1
    iput-object p1, p0, Ldbxyzptlk/db231222/T/x;->l:Ldbxyzptlk/db231222/T/a;

    .line 237
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 238
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 239
    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->c:Ldbxyzptlk/db231222/T/q;

    iget v1, p0, Ldbxyzptlk/db231222/T/x;->b:I

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/T/q;->a(I)Ldbxyzptlk/db231222/T/x;

    .line 240
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic e(Ldbxyzptlk/db231222/T/x;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ldbxyzptlk/db231222/T/x;->g()V

    return-void
.end method

.method static synthetic f(Ldbxyzptlk/db231222/T/x;)Ldbxyzptlk/db231222/T/A;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->k:Ldbxyzptlk/db231222/T/A;

    return-object v0
.end method

.method static synthetic g(Ldbxyzptlk/db231222/T/x;)I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Ldbxyzptlk/db231222/T/x;->g:I

    return v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 547
    sget-boolean v0, Ldbxyzptlk/db231222/T/x;->a:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 550
    :cond_0
    monitor-enter p0

    .line 551
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->j:Ldbxyzptlk/db231222/T/z;

    invoke-static {v0}, Ldbxyzptlk/db231222/T/z;->a(Ldbxyzptlk/db231222/T/z;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->j:Ldbxyzptlk/db231222/T/z;

    invoke-static {v0}, Ldbxyzptlk/db231222/T/z;->b(Ldbxyzptlk/db231222/T/z;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->k:Ldbxyzptlk/db231222/T/A;

    invoke-static {v0}, Ldbxyzptlk/db231222/T/A;->a(Ldbxyzptlk/db231222/T/A;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->k:Ldbxyzptlk/db231222/T/A;

    invoke-static {v0}, Ldbxyzptlk/db231222/T/A;->b(Ldbxyzptlk/db231222/T/A;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    .line 552
    :goto_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/T/x;->a()Z

    move-result v1

    .line 553
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 554
    if-eqz v0, :cond_4

    .line 559
    sget-object v0, Ldbxyzptlk/db231222/T/a;->l:Ldbxyzptlk/db231222/T/a;

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/T/x;->a(Ldbxyzptlk/db231222/T/a;)V

    .line 563
    :cond_2
    :goto_1
    return-void

    .line 551
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 553
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 560
    :cond_4
    if-nez v1, :cond_2

    .line 561
    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->c:Ldbxyzptlk/db231222/T/q;

    iget v1, p0, Ldbxyzptlk/db231222/T/x;->b:I

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/T/q;->a(I)Ldbxyzptlk/db231222/T/x;

    goto :goto_1
.end method


# virtual methods
.method final declared-synchronized a(I)V
    .locals 1

    .prologue
    .line 322
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->k:Ldbxyzptlk/db231222/T/A;

    invoke-static {v0, p1}, Ldbxyzptlk/db231222/T/A;->a(Ldbxyzptlk/db231222/T/A;I)I

    .line 323
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 324
    monitor-exit p0

    return-void

    .line 322
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 177
    iput-wide p1, p0, Ldbxyzptlk/db231222/T/x;->f:J

    .line 178
    return-void
.end method

.method public final a(Ldbxyzptlk/db231222/T/a;)V
    .locals 2

    .prologue
    .line 209
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/T/x;->d(Ldbxyzptlk/db231222/T/a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 213
    :goto_0
    return-void

    .line 212
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->c:Ldbxyzptlk/db231222/T/q;

    iget v1, p0, Ldbxyzptlk/db231222/T/x;->b:I

    invoke-virtual {v0, v1, p1}, Ldbxyzptlk/db231222/T/q;->b(ILdbxyzptlk/db231222/T/a;)V

    goto :goto_0
.end method

.method final a(Ldbxyzptlk/db231222/T/k;)V
    .locals 1

    .prologue
    .line 316
    sget-boolean v0, Ldbxyzptlk/db231222/T/x;->a:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 317
    :cond_0
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/T/x;->b(Ldbxyzptlk/db231222/T/k;)V

    .line 318
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 319
    return-void
.end method

.method final a(Ljava/io/InputStream;I)V
    .locals 1

    .prologue
    .line 282
    sget-boolean v0, Ldbxyzptlk/db231222/T/x;->a:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 283
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->j:Ldbxyzptlk/db231222/T/z;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/T/z;->a(Ljava/io/InputStream;I)V

    .line 284
    return-void
.end method

.method final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 244
    sget-boolean v1, Ldbxyzptlk/db231222/T/x;->a:Z

    if-nez v1, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 245
    :cond_0
    const/4 v1, 0x0

    .line 247
    monitor-enter p0

    .line 248
    :try_start_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/T/x;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ldbxyzptlk/db231222/T/x;->i:Ljava/util/List;

    if-nez v2, :cond_2

    .line 249
    iput-object p1, p0, Ldbxyzptlk/db231222/T/x;->i:Ljava/util/List;

    .line 250
    invoke-virtual {p0}, Ldbxyzptlk/db231222/T/x;->a()Z

    move-result v0

    .line 251
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 255
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 256
    if-eqz v1, :cond_3

    .line 257
    sget-object v0, Ldbxyzptlk/db231222/T/a;->e:Ldbxyzptlk/db231222/T/a;

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/T/x;->b(Ldbxyzptlk/db231222/T/a;)V

    .line 261
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v1, v0

    .line 253
    goto :goto_0

    .line 255
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 258
    :cond_3
    if-nez v0, :cond_1

    .line 259
    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->c:Ldbxyzptlk/db231222/T/q;

    iget v1, p0, Ldbxyzptlk/db231222/T/x;->b:I

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/T/q;->a(I)Ldbxyzptlk/db231222/T/x;

    goto :goto_1
.end method

.method public final declared-synchronized a()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 93
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Ldbxyzptlk/db231222/T/x;->l:Ldbxyzptlk/db231222/T/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 99
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 96
    :cond_1
    :try_start_1
    iget-object v1, p0, Ldbxyzptlk/db231222/T/x;->j:Ldbxyzptlk/db231222/T/z;

    invoke-static {v1}, Ldbxyzptlk/db231222/T/z;->a(Ldbxyzptlk/db231222/T/z;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Ldbxyzptlk/db231222/T/x;->j:Ldbxyzptlk/db231222/T/z;

    invoke-static {v1}, Ldbxyzptlk/db231222/T/z;->b(Ldbxyzptlk/db231222/T/z;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_2
    iget-object v1, p0, Ldbxyzptlk/db231222/T/x;->k:Ldbxyzptlk/db231222/T/A;

    invoke-static {v1}, Ldbxyzptlk/db231222/T/A;->a(Ldbxyzptlk/db231222/T/A;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Ldbxyzptlk/db231222/T/x;->k:Ldbxyzptlk/db231222/T/A;

    invoke-static {v1}, Ldbxyzptlk/db231222/T/A;->b(Ldbxyzptlk/db231222/T/A;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    iget-object v1, p0, Ldbxyzptlk/db231222/T/x;->i:Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_0

    .line 99
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Ldbxyzptlk/db231222/T/a;)V
    .locals 2

    .prologue
    .line 220
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/T/x;->d(Ldbxyzptlk/db231222/T/a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 224
    :goto_0
    return-void

    .line 223
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->c:Ldbxyzptlk/db231222/T/q;

    iget v1, p0, Ldbxyzptlk/db231222/T/x;->b:I

    invoke-virtual {v0, v1, p1}, Ldbxyzptlk/db231222/T/q;->a(ILdbxyzptlk/db231222/T/a;)V

    goto :goto_0
.end method

.method final b(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 264
    sget-boolean v0, Ldbxyzptlk/db231222/T/x;->a:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 265
    :cond_0
    const/4 v0, 0x0

    .line 266
    monitor-enter p0

    .line 267
    :try_start_0
    iget-object v1, p0, Ldbxyzptlk/db231222/T/x;->i:Ljava/util/List;

    if-eqz v1, :cond_2

    .line 268
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 269
    iget-object v2, p0, Ldbxyzptlk/db231222/T/x;->i:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 270
    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 271
    iput-object v1, p0, Ldbxyzptlk/db231222/T/x;->i:Ljava/util/List;

    .line 275
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 276
    if-eqz v0, :cond_1

    .line 277
    sget-object v0, Ldbxyzptlk/db231222/T/a;->b:Ldbxyzptlk/db231222/T/a;

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/T/x;->b(Ldbxyzptlk/db231222/T/a;)V

    .line 279
    :cond_1
    return-void

    .line 273
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 275
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 104
    iget v0, p0, Ldbxyzptlk/db231222/T/x;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-ne v0, v1, :cond_0

    move v0, v1

    .line 105
    :goto_0
    iget-object v3, p0, Ldbxyzptlk/db231222/T/x;->c:Ldbxyzptlk/db231222/T/q;

    iget-boolean v3, v3, Ldbxyzptlk/db231222/T/q;->b:Z

    if-ne v3, v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 104
    goto :goto_0

    :cond_1
    move v1, v2

    .line 105
    goto :goto_1
.end method

.method public final declared-synchronized c()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->i:Ljava/util/List;

    if-nez v0, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->l:Ldbxyzptlk/db231222/T/a;

    if-nez v0, :cond_0

    .line 123
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 129
    :catch_0
    move-exception v0

    .line 130
    :try_start_1
    new-instance v1, Ljava/io/InterruptedIOException;

    invoke-direct {v1}, Ljava/io/InterruptedIOException;-><init>()V

    .line 131
    invoke-virtual {v1, v0}, Ljava/io/InterruptedIOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 132
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 125
    :cond_0
    :try_start_2
    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->i:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->i:Ljava/util/List;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    .line 128
    :cond_1
    :try_start_3
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stream was reset: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/db231222/T/x;->l:Ldbxyzptlk/db231222/T/a;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method final declared-synchronized c(Ldbxyzptlk/db231222/T/a;)V
    .locals 1

    .prologue
    .line 300
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->l:Ldbxyzptlk/db231222/T/a;

    if-nez v0, :cond_0

    .line 301
    iput-object p1, p0, Ldbxyzptlk/db231222/T/x;->l:Ldbxyzptlk/db231222/T/a;

    .line 302
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 304
    :cond_0
    monitor-exit p0

    return-void

    .line 300
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->j:Ldbxyzptlk/db231222/T/z;

    return-object v0
.end method

.method public final e()Ljava/io/OutputStream;
    .locals 2

    .prologue
    .line 196
    monitor-enter p0

    .line 197
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->i:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/T/x;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 198
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "reply before requesting the output stream"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 200
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 201
    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->k:Ldbxyzptlk/db231222/T/A;

    return-object v0
.end method

.method final f()V
    .locals 2

    .prologue
    .line 287
    sget-boolean v0, Ldbxyzptlk/db231222/T/x;->a:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 289
    :cond_0
    monitor-enter p0

    .line 290
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->j:Ldbxyzptlk/db231222/T/z;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/T/z;->a(Ldbxyzptlk/db231222/T/z;Z)Z

    .line 291
    invoke-virtual {p0}, Ldbxyzptlk/db231222/T/x;->a()Z

    move-result v0

    .line 292
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 293
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294
    if-nez v0, :cond_1

    .line 295
    iget-object v0, p0, Ldbxyzptlk/db231222/T/x;->c:Ldbxyzptlk/db231222/T/q;

    iget v1, p0, Ldbxyzptlk/db231222/T/x;->b:I

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/T/q;->a(I)Ldbxyzptlk/db231222/T/x;

    .line 297
    :cond_1
    return-void

    .line 293
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
