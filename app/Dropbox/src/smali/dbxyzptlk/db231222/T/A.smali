.class final Ldbxyzptlk/db231222/T/A;
.super Ljava/io/OutputStream;
.source "panda.py"


# static fields
.field static final synthetic a:Z


# instance fields
.field final synthetic b:Ldbxyzptlk/db231222/T/x;

.field private final c:[B

.field private d:I

.field private e:Z

.field private f:Z

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 569
    const-class v0, Ldbxyzptlk/db231222/T/x;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Ldbxyzptlk/db231222/T/A;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/T/x;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 569
    iput-object p1, p0, Ldbxyzptlk/db231222/T/A;->b:Ldbxyzptlk/db231222/T/x;

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 570
    const/16 v0, 0x2000

    new-array v0, v0, [B

    iput-object v0, p0, Ldbxyzptlk/db231222/T/A;->c:[B

    .line 571
    iput v1, p0, Ldbxyzptlk/db231222/T/A;->d:I

    .line 587
    iput v1, p0, Ldbxyzptlk/db231222/T/A;->g:I

    return-void
.end method

.method synthetic constructor <init>(Ldbxyzptlk/db231222/T/x;Ldbxyzptlk/db231222/T/y;)V
    .locals 0

    .prologue
    .line 569
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/T/A;-><init>(Ldbxyzptlk/db231222/T/x;)V

    return-void
.end method

.method static synthetic a(Ldbxyzptlk/db231222/T/A;I)I
    .locals 1

    .prologue
    .line 569
    iget v0, p0, Ldbxyzptlk/db231222/T/A;->g:I

    sub-int/2addr v0, p1

    iput v0, p0, Ldbxyzptlk/db231222/T/A;->g:I

    return v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 672
    iget-object v1, p0, Ldbxyzptlk/db231222/T/A;->b:Ldbxyzptlk/db231222/T/x;

    monitor-enter v1

    .line 673
    :try_start_0
    iget-boolean v0, p0, Ldbxyzptlk/db231222/T/A;->e:Z

    if-eqz v0, :cond_0

    .line 674
    new-instance v0, Ljava/io/IOException;

    const-string v2, "stream closed"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 680
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 675
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Ldbxyzptlk/db231222/T/A;->f:Z

    if-eqz v0, :cond_1

    .line 676
    new-instance v0, Ljava/io/IOException;

    const-string v2, "stream finished"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 677
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/T/A;->b:Ldbxyzptlk/db231222/T/x;

    invoke-static {v0}, Ldbxyzptlk/db231222/T/x;->d(Ldbxyzptlk/db231222/T/x;)Ldbxyzptlk/db231222/T/a;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 678
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stream was reset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Ldbxyzptlk/db231222/T/A;->b:Ldbxyzptlk/db231222/T/x;

    invoke-static {v3}, Ldbxyzptlk/db231222/T/x;->d(Ldbxyzptlk/db231222/T/x;)Ldbxyzptlk/db231222/T/a;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 680
    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 681
    return-void
.end method

.method private a(IZ)V
    .locals 3

    .prologue
    .line 654
    :cond_0
    :try_start_0
    iget v0, p0, Ldbxyzptlk/db231222/T/A;->g:I

    add-int/2addr v0, p1

    iget-object v1, p0, Ldbxyzptlk/db231222/T/A;->b:Ldbxyzptlk/db231222/T/x;

    invoke-static {v1}, Ldbxyzptlk/db231222/T/x;->g(Ldbxyzptlk/db231222/T/x;)I

    move-result v1

    if-lt v0, v1, :cond_3

    .line 655
    iget-object v0, p0, Ldbxyzptlk/db231222/T/A;->b:Ldbxyzptlk/db231222/T/x;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    .line 658
    if-nez p2, :cond_1

    iget-boolean v0, p0, Ldbxyzptlk/db231222/T/A;->e:Z

    if-eqz v0, :cond_1

    .line 659
    new-instance v0, Ljava/io/IOException;

    const-string v1, "stream closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 666
    :catch_0
    move-exception v0

    .line 667
    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0

    .line 660
    :cond_1
    :try_start_1
    iget-boolean v0, p0, Ldbxyzptlk/db231222/T/A;->f:Z

    if-eqz v0, :cond_2

    .line 661
    new-instance v0, Ljava/io/IOException;

    const-string v1, "stream finished"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 662
    :cond_2
    iget-object v0, p0, Ldbxyzptlk/db231222/T/A;->b:Ldbxyzptlk/db231222/T/x;

    invoke-static {v0}, Ldbxyzptlk/db231222/T/x;->d(Ldbxyzptlk/db231222/T/x;)Ldbxyzptlk/db231222/T/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 663
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stream was reset: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/db231222/T/A;->b:Ldbxyzptlk/db231222/T/x;

    invoke-static {v2}, Ldbxyzptlk/db231222/T/x;->d(Ldbxyzptlk/db231222/T/x;)Ldbxyzptlk/db231222/T/a;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 669
    :cond_3
    return-void
.end method

.method private a(Z)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 635
    sget-boolean v0, Ldbxyzptlk/db231222/T/A;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/T/A;->b:Ldbxyzptlk/db231222/T/x;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 637
    :cond_0
    iget v0, p0, Ldbxyzptlk/db231222/T/A;->d:I

    .line 638
    iget-object v1, p0, Ldbxyzptlk/db231222/T/A;->b:Ldbxyzptlk/db231222/T/x;

    monitor-enter v1

    .line 639
    :try_start_0
    invoke-direct {p0, v0, p1}, Ldbxyzptlk/db231222/T/A;->a(IZ)V

    .line 640
    iget v2, p0, Ldbxyzptlk/db231222/T/A;->g:I

    add-int/2addr v0, v2

    iput v0, p0, Ldbxyzptlk/db231222/T/A;->g:I

    .line 641
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 642
    iget-object v0, p0, Ldbxyzptlk/db231222/T/A;->b:Ldbxyzptlk/db231222/T/x;

    invoke-static {v0}, Ldbxyzptlk/db231222/T/x;->b(Ldbxyzptlk/db231222/T/x;)Ldbxyzptlk/db231222/T/q;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/T/A;->b:Ldbxyzptlk/db231222/T/x;

    invoke-static {v1}, Ldbxyzptlk/db231222/T/x;->a(Ldbxyzptlk/db231222/T/x;)I

    move-result v1

    iget-object v3, p0, Ldbxyzptlk/db231222/T/A;->c:[B

    iget v5, p0, Ldbxyzptlk/db231222/T/A;->d:I

    move v2, p1

    invoke-virtual/range {v0 .. v5}, Ldbxyzptlk/db231222/T/q;->a(IZ[BII)V

    .line 643
    iput v4, p0, Ldbxyzptlk/db231222/T/A;->d:I

    .line 644
    return-void

    .line 641
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/T/A;)Z
    .locals 1

    .prologue
    .line 569
    iget-boolean v0, p0, Ldbxyzptlk/db231222/T/A;->f:Z

    return v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/T/A;Z)Z
    .locals 0

    .prologue
    .line 569
    iput-boolean p1, p0, Ldbxyzptlk/db231222/T/A;->f:Z

    return p1
.end method

.method static synthetic b(Ldbxyzptlk/db231222/T/A;)Z
    .locals 1

    .prologue
    .line 569
    iget-boolean v0, p0, Ldbxyzptlk/db231222/T/A;->e:Z

    return v0
.end method


# virtual methods
.method public final close()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 620
    sget-boolean v0, Ldbxyzptlk/db231222/T/A;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/T/A;->b:Ldbxyzptlk/db231222/T/x;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 621
    :cond_0
    iget-object v1, p0, Ldbxyzptlk/db231222/T/A;->b:Ldbxyzptlk/db231222/T/x;

    monitor-enter v1

    .line 622
    :try_start_0
    iget-boolean v0, p0, Ldbxyzptlk/db231222/T/A;->e:Z

    if-eqz v0, :cond_1

    .line 623
    monitor-exit v1

    .line 632
    :goto_0
    return-void

    .line 625
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/db231222/T/A;->e:Z

    .line 626
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 627
    iget-object v0, p0, Ldbxyzptlk/db231222/T/A;->b:Ldbxyzptlk/db231222/T/x;

    invoke-static {v0}, Ldbxyzptlk/db231222/T/x;->f(Ldbxyzptlk/db231222/T/x;)Ldbxyzptlk/db231222/T/A;

    move-result-object v0

    iget-boolean v0, v0, Ldbxyzptlk/db231222/T/A;->f:Z

    if-nez v0, :cond_2

    .line 628
    invoke-direct {p0, v2}, Ldbxyzptlk/db231222/T/A;->a(Z)V

    .line 630
    :cond_2
    iget-object v0, p0, Ldbxyzptlk/db231222/T/A;->b:Ldbxyzptlk/db231222/T/x;

    invoke-static {v0}, Ldbxyzptlk/db231222/T/x;->b(Ldbxyzptlk/db231222/T/x;)Ldbxyzptlk/db231222/T/q;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/T/q;->c()V

    .line 631
    iget-object v0, p0, Ldbxyzptlk/db231222/T/A;->b:Ldbxyzptlk/db231222/T/x;

    invoke-static {v0}, Ldbxyzptlk/db231222/T/x;->e(Ldbxyzptlk/db231222/T/x;)V

    goto :goto_0

    .line 626
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final flush()V
    .locals 1

    .prologue
    .line 611
    sget-boolean v0, Ldbxyzptlk/db231222/T/A;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/T/A;->b:Ldbxyzptlk/db231222/T/x;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 612
    :cond_0
    invoke-direct {p0}, Ldbxyzptlk/db231222/T/A;->a()V

    .line 613
    iget v0, p0, Ldbxyzptlk/db231222/T/A;->d:I

    if-lez v0, :cond_1

    .line 614
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/T/A;->a(Z)V

    .line 615
    iget-object v0, p0, Ldbxyzptlk/db231222/T/A;->b:Ldbxyzptlk/db231222/T/x;

    invoke-static {v0}, Ldbxyzptlk/db231222/T/x;->b(Ldbxyzptlk/db231222/T/x;)Ldbxyzptlk/db231222/T/q;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/T/q;->c()V

    .line 617
    :cond_1
    return-void
.end method

.method public final write(I)V
    .locals 0

    .prologue
    .line 590
    invoke-static {p0, p1}, Ldbxyzptlk/db231222/R/v;->a(Ljava/io/OutputStream;I)V

    .line 591
    return-void
.end method

.method public final write([BII)V
    .locals 3

    .prologue
    .line 594
    sget-boolean v0, Ldbxyzptlk/db231222/T/A;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ldbxyzptlk/db231222/T/A;->b:Ldbxyzptlk/db231222/T/x;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 595
    :cond_0
    array-length v0, p1

    invoke-static {v0, p2, p3}, Ldbxyzptlk/db231222/R/v;->a(III)V

    .line 596
    invoke-direct {p0}, Ldbxyzptlk/db231222/T/A;->a()V

    .line 598
    :goto_0
    if-lez p3, :cond_2

    .line 599
    iget v0, p0, Ldbxyzptlk/db231222/T/A;->d:I

    iget-object v1, p0, Ldbxyzptlk/db231222/T/A;->c:[B

    array-length v1, v1

    if-ne v0, v1, :cond_1

    .line 600
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/T/A;->a(Z)V

    .line 602
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/T/A;->c:[B

    array-length v0, v0

    iget v1, p0, Ldbxyzptlk/db231222/T/A;->d:I

    sub-int/2addr v0, v1

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 603
    iget-object v1, p0, Ldbxyzptlk/db231222/T/A;->c:[B

    iget v2, p0, Ldbxyzptlk/db231222/T/A;->d:I

    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 604
    iget v1, p0, Ldbxyzptlk/db231222/T/A;->d:I

    add-int/2addr v1, v0

    iput v1, p0, Ldbxyzptlk/db231222/T/A;->d:I

    .line 605
    add-int/2addr p2, v0

    .line 606
    sub-int/2addr p3, v0

    .line 607
    goto :goto_0

    .line 608
    :cond_2
    return-void
.end method
