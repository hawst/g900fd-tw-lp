.class public final Ldbxyzptlk/db231222/A/c;
.super Ldbxyzptlk/db231222/A/a;
.source "panda.py"


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:Z

.field private final h:Z


# direct methods
.method public constructor <init>(Lcom/dropbox/sync/android/DbxNotificationHeader;ILjava/lang/String;Ljava/lang/String;IIIZZ)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 38
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/A/a;-><init>(Lcom/dropbox/sync/android/DbxNotificationHeader;)V

    .line 40
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 41
    :cond_0
    if-nez p4, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 43
    :cond_1
    const/4 v0, 0x0

    .line 44
    if-lez p7, :cond_2

    move v0, v1

    .line 47
    :cond_2
    if-lez p6, :cond_3

    .line 48
    add-int/lit8 v0, v0, 0x1

    .line 50
    :cond_3
    if-lez p5, :cond_4

    .line 51
    add-int/lit8 v0, v0, 0x1

    .line 55
    :cond_4
    if-eqz p9, :cond_5

    if-gtz v0, :cond_6

    :cond_5
    if-nez p9, :cond_7

    if-eq v0, v1, :cond_7

    .line 56
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 59
    :cond_7
    iput p2, p0, Ldbxyzptlk/db231222/A/c;->a:I

    .line 60
    iput-object p3, p0, Ldbxyzptlk/db231222/A/c;->b:Ljava/lang/String;

    .line 61
    iput-object p4, p0, Ldbxyzptlk/db231222/A/c;->c:Ljava/lang/String;

    .line 62
    iput p5, p0, Ldbxyzptlk/db231222/A/c;->d:I

    .line 63
    iput p6, p0, Ldbxyzptlk/db231222/A/c;->e:I

    .line 64
    iput p7, p0, Ldbxyzptlk/db231222/A/c;->f:I

    .line 65
    iput-boolean p8, p0, Ldbxyzptlk/db231222/A/c;->g:Z

    .line 66
    iput-boolean p9, p0, Ldbxyzptlk/db231222/A/c;->h:Z

    .line 67
    return-void
.end method

.method static b(Lcom/dropbox/sync/android/DbxNotificationHeader;Ljava/lang/String;)Ldbxyzptlk/db231222/A/c;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 70
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 71
    const-string v0, "deal_space"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 74
    const-string v0, "discount_code"

    invoke-virtual {v1, v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_0

    .line 78
    :goto_0
    const-string v0, "deal_phone_make"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 79
    const-string v0, "months_left"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 80
    const-string v0, "weeks_left"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 81
    const-string v0, "days_left"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 82
    const-string v0, "dismissable"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    .line 83
    const-string v0, "expired"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    .line 85
    new-instance v0, Ldbxyzptlk/db231222/A/c;

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, Ldbxyzptlk/db231222/A/c;-><init>(Lcom/dropbox/sync/android/DbxNotificationHeader;ILjava/lang/String;Ljava/lang/String;IIIZZ)V

    return-object v0

    :cond_0
    move-object v3, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/dropbox/sync/android/DbxNotificationHeader;)Ldbxyzptlk/db231222/A/a;
    .locals 10

    .prologue
    .line 91
    new-instance v0, Ldbxyzptlk/db231222/A/c;

    iget v2, p0, Ldbxyzptlk/db231222/A/c;->a:I

    iget-object v3, p0, Ldbxyzptlk/db231222/A/c;->b:Ljava/lang/String;

    iget-object v4, p0, Ldbxyzptlk/db231222/A/c;->c:Ljava/lang/String;

    iget v5, p0, Ldbxyzptlk/db231222/A/c;->d:I

    iget v6, p0, Ldbxyzptlk/db231222/A/c;->e:I

    iget v7, p0, Ldbxyzptlk/db231222/A/c;->f:I

    iget-boolean v8, p0, Ldbxyzptlk/db231222/A/c;->g:Z

    iget-boolean v9, p0, Ldbxyzptlk/db231222/A/c;->h:Z

    move-object v1, p1

    invoke-direct/range {v0 .. v9}, Ldbxyzptlk/db231222/A/c;-><init>(Lcom/dropbox/sync/android/DbxNotificationHeader;ILjava/lang/String;Ljava/lang/String;IIIZZ)V

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/A/b;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Arg:",
            "Ljava/lang/Object;",
            "Ret:",
            "Ljava/lang/Object;",
            "V::",
            "Ldbxyzptlk/db231222/A/b",
            "<TArg;TRet;>;>(TV;TArg;)TRet;"
        }
    .end annotation

    .prologue
    .line 97
    invoke-interface {p1, p0, p2}, Ldbxyzptlk/db231222/A/b;->a(Ldbxyzptlk/db231222/A/c;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Ldbxyzptlk/db231222/A/c;->a:I

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Ldbxyzptlk/db231222/A/c;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Ldbxyzptlk/db231222/A/c;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Ldbxyzptlk/db231222/A/c;->d:I

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Ldbxyzptlk/db231222/A/c;->e:I

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Ldbxyzptlk/db231222/A/c;->f:I

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Ldbxyzptlk/db231222/A/c;->g:Z

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 156
    iget-boolean v0, p0, Ldbxyzptlk/db231222/A/c;->h:Z

    return v0
.end method
