.class public final Ldbxyzptlk/db231222/A/e;
.super Ldbxyzptlk/db231222/A/a;
.source "panda.py"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/Boolean;

.field private final g:Ljava/lang/String;

.field private final h:I

.field private final i:Ljava/lang/Integer;

.field private final j:I

.field private final k:I


# direct methods
.method public constructor <init>(Lcom/dropbox/sync/android/DbxNotificationHeader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;ILjava/lang/Integer;II)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/A/a;-><init>(Lcom/dropbox/sync/android/DbxNotificationHeader;)V

    .line 61
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 62
    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 63
    :cond_1
    if-nez p4, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 64
    :cond_2
    if-nez p5, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 65
    :cond_3
    if-nez p8, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 67
    :cond_4
    iput-object p2, p0, Ldbxyzptlk/db231222/A/e;->a:Ljava/lang/String;

    .line 68
    iput-object p3, p0, Ldbxyzptlk/db231222/A/e;->b:Ljava/lang/String;

    .line 69
    iput-object p4, p0, Ldbxyzptlk/db231222/A/e;->c:Ljava/lang/String;

    .line 70
    iput-object p5, p0, Ldbxyzptlk/db231222/A/e;->d:Ljava/lang/String;

    .line 71
    iput-object p6, p0, Ldbxyzptlk/db231222/A/e;->e:Ljava/lang/String;

    .line 72
    iput-object p7, p0, Ldbxyzptlk/db231222/A/e;->f:Ljava/lang/Boolean;

    .line 73
    iput-object p8, p0, Ldbxyzptlk/db231222/A/e;->g:Ljava/lang/String;

    .line 74
    iput p9, p0, Ldbxyzptlk/db231222/A/e;->h:I

    .line 75
    iput-object p10, p0, Ldbxyzptlk/db231222/A/e;->i:Ljava/lang/Integer;

    .line 76
    iput p11, p0, Ldbxyzptlk/db231222/A/e;->j:I

    .line 77
    iput p12, p0, Ldbxyzptlk/db231222/A/e;->k:I

    .line 78
    return-void
.end method

.method static b(Lcom/dropbox/sync/android/DbxNotificationHeader;Ljava/lang/String;)Ldbxyzptlk/db231222/A/e;
    .locals 13

    .prologue
    const/4 v10, 0x0

    const/4 v0, 0x0

    .line 81
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 84
    const-string v2, "shmodel_url"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 85
    const-string v3, "tkey"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 86
    const-string v4, "origin_user_i18n_name"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 87
    const-string v5, "origin_user_public_name"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 88
    const-string v6, "token_type"

    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 89
    const-string v6, "token_name"

    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 92
    const-string v6, "thumbnail_url"

    invoke-virtual {v1, v6, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 93
    const-string v7, "num_photos"

    invoke-virtual {v1, v7, v10}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v11

    .line 94
    const-string v7, "num_videos"

    invoke-virtual {v1, v7, v10}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v12

    .line 97
    const-string v7, "is_thumbnail_placeholder"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 98
    const-string v7, "is_thumbnail_placeholder"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 104
    :goto_0
    const-string v10, "collection_type"

    invoke-virtual {v1, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 105
    const-string v0, "collection_type"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    .line 110
    :goto_1
    new-instance v0, Ldbxyzptlk/db231222/A/e;

    move-object v1, p0

    invoke-direct/range {v0 .. v12}, Ldbxyzptlk/db231222/A/e;-><init>(Lcom/dropbox/sync/android/DbxNotificationHeader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;ILjava/lang/Integer;II)V

    return-object v0

    :cond_0
    move-object v7, v0

    .line 100
    goto :goto_0

    :cond_1
    move-object v10, v0

    .line 107
    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/dropbox/sync/android/DbxNotificationHeader;)Ldbxyzptlk/db231222/A/a;
    .locals 13

    .prologue
    .line 117
    new-instance v0, Ldbxyzptlk/db231222/A/e;

    iget-object v2, p0, Ldbxyzptlk/db231222/A/e;->a:Ljava/lang/String;

    iget-object v3, p0, Ldbxyzptlk/db231222/A/e;->b:Ljava/lang/String;

    iget-object v4, p0, Ldbxyzptlk/db231222/A/e;->c:Ljava/lang/String;

    iget-object v5, p0, Ldbxyzptlk/db231222/A/e;->d:Ljava/lang/String;

    iget-object v6, p0, Ldbxyzptlk/db231222/A/e;->e:Ljava/lang/String;

    iget-object v7, p0, Ldbxyzptlk/db231222/A/e;->f:Ljava/lang/Boolean;

    iget-object v8, p0, Ldbxyzptlk/db231222/A/e;->g:Ljava/lang/String;

    iget v9, p0, Ldbxyzptlk/db231222/A/e;->h:I

    iget-object v10, p0, Ldbxyzptlk/db231222/A/e;->i:Ljava/lang/Integer;

    iget v11, p0, Ldbxyzptlk/db231222/A/e;->j:I

    iget v12, p0, Ldbxyzptlk/db231222/A/e;->k:I

    move-object v1, p1

    invoke-direct/range {v0 .. v12}, Ldbxyzptlk/db231222/A/e;-><init>(Lcom/dropbox/sync/android/DbxNotificationHeader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;ILjava/lang/Integer;II)V

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/A/b;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Arg:",
            "Ljava/lang/Object;",
            "Ret:",
            "Ljava/lang/Object;",
            "V::",
            "Ldbxyzptlk/db231222/A/b",
            "<TArg;TRet;>;>(TV;TArg;)TRet;"
        }
    .end annotation

    .prologue
    .line 125
    invoke-interface {p1, p0, p2}, Ldbxyzptlk/db231222/A/b;->a(Ldbxyzptlk/db231222/A/e;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Ldbxyzptlk/db231222/A/e;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Ldbxyzptlk/db231222/A/e;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Ldbxyzptlk/db231222/A/e;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Ldbxyzptlk/db231222/A/e;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Ldbxyzptlk/db231222/A/e;->f:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Ldbxyzptlk/db231222/A/e;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Ldbxyzptlk/db231222/A/e;->h:I

    return v0
.end method

.method public final i()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Ldbxyzptlk/db231222/A/e;->i:Ljava/lang/Integer;

    return-object v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Ldbxyzptlk/db231222/A/e;->j:I

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Ldbxyzptlk/db231222/A/e;->k:I

    return v0
.end method
