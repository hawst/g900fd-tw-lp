.class public final Ldbxyzptlk/db231222/A/d;
.super Ldbxyzptlk/db231222/A/a;
.source "panda.py"


# instance fields
.field private final a:J

.field private final b:J

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Lcom/dropbox/sync/android/DbxNotificationHeader;JJILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/A/a;-><init>(Lcom/dropbox/sync/android/DbxNotificationHeader;)V

    .line 49
    if-nez p7, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 50
    :cond_0
    if-nez p8, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 51
    :cond_1
    if-nez p9, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 53
    :cond_2
    iput-wide p2, p0, Ldbxyzptlk/db231222/A/d;->a:J

    .line 54
    iput-wide p4, p0, Ldbxyzptlk/db231222/A/d;->b:J

    .line 55
    iput p6, p0, Ldbxyzptlk/db231222/A/d;->c:I

    .line 56
    iput-object p7, p0, Ldbxyzptlk/db231222/A/d;->d:Ljava/lang/String;

    .line 57
    iput-object p8, p0, Ldbxyzptlk/db231222/A/d;->e:Ljava/lang/String;

    .line 58
    iput-object p9, p0, Ldbxyzptlk/db231222/A/d;->f:Ljava/lang/String;

    .line 59
    iput-object p10, p0, Ldbxyzptlk/db231222/A/d;->g:Ljava/lang/Integer;

    .line 60
    return-void
.end method

.method static b(Lcom/dropbox/sync/android/DbxNotificationHeader;Ljava/lang/String;)Ldbxyzptlk/db231222/A/d;
    .locals 11

    .prologue
    .line 63
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 66
    const-string v1, "ns_id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 67
    const-string v1, "invite_id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 68
    const-string v1, "invite_status"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 69
    const-string v1, "folder_name"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 70
    const-string v1, "origin_user_display_name"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 71
    const-string v1, "origin_user_public_name"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 75
    const-string v1, "restrictions"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 76
    const-string v1, "restrictions"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    .line 81
    :goto_0
    new-instance v0, Ldbxyzptlk/db231222/A/d;

    move-object v1, p0

    invoke-direct/range {v0 .. v10}, Ldbxyzptlk/db231222/A/d;-><init>(Lcom/dropbox/sync/android/DbxNotificationHeader;JJILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v0

    .line 78
    :cond_0
    const/4 v10, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/dropbox/sync/android/DbxNotificationHeader;)Ldbxyzptlk/db231222/A/a;
    .locals 11

    .prologue
    .line 88
    new-instance v0, Ldbxyzptlk/db231222/A/d;

    iget-wide v2, p0, Ldbxyzptlk/db231222/A/d;->a:J

    iget-wide v4, p0, Ldbxyzptlk/db231222/A/d;->b:J

    iget v6, p0, Ldbxyzptlk/db231222/A/d;->c:I

    iget-object v7, p0, Ldbxyzptlk/db231222/A/d;->d:Ljava/lang/String;

    iget-object v8, p0, Ldbxyzptlk/db231222/A/d;->e:Ljava/lang/String;

    iget-object v9, p0, Ldbxyzptlk/db231222/A/d;->f:Ljava/lang/String;

    iget-object v10, p0, Ldbxyzptlk/db231222/A/d;->g:Ljava/lang/Integer;

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, Ldbxyzptlk/db231222/A/d;-><init>(Lcom/dropbox/sync/android/DbxNotificationHeader;JJILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/A/b;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Arg:",
            "Ljava/lang/Object;",
            "Ret:",
            "Ljava/lang/Object;",
            "V::",
            "Ldbxyzptlk/db231222/A/b",
            "<TArg;TRet;>;>(TV;TArg;)TRet;"
        }
    .end annotation

    .prologue
    .line 95
    invoke-interface {p1, p0, p2}, Ldbxyzptlk/db231222/A/b;->a(Ldbxyzptlk/db231222/A/d;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 99
    iget-wide v0, p0, Ldbxyzptlk/db231222/A/d;->a:J

    return-wide v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 103
    iget-wide v0, p0, Ldbxyzptlk/db231222/A/d;->b:J

    return-wide v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Ldbxyzptlk/db231222/A/d;->c:I

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Ldbxyzptlk/db231222/A/d;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Ldbxyzptlk/db231222/A/d;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Ldbxyzptlk/db231222/A/d;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Ldbxyzptlk/db231222/A/d;->g:Ljava/lang/Integer;

    return-object v0
.end method
