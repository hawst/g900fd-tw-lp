.class public final enum Ldbxyzptlk/db231222/I/l;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldbxyzptlk/db231222/I/l;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ldbxyzptlk/db231222/I/l;

.field public static final enum b:Ldbxyzptlk/db231222/I/l;

.field public static final enum c:Ldbxyzptlk/db231222/I/l;

.field public static final enum d:Ldbxyzptlk/db231222/I/l;

.field public static final enum e:Ldbxyzptlk/db231222/I/l;

.field public static final enum f:Ldbxyzptlk/db231222/I/l;

.field public static final enum g:Ldbxyzptlk/db231222/I/l;

.field public static final enum h:Ldbxyzptlk/db231222/I/l;

.field public static final enum i:Ldbxyzptlk/db231222/I/l;

.field private static final synthetic j:[Ldbxyzptlk/db231222/I/l;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 30
    new-instance v0, Ldbxyzptlk/db231222/I/l;

    const-string v1, "OTHER"

    invoke-direct {v0, v1, v3}, Ldbxyzptlk/db231222/I/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbxyzptlk/db231222/I/l;->a:Ldbxyzptlk/db231222/I/l;

    .line 39
    new-instance v0, Ldbxyzptlk/db231222/I/l;

    const-string v1, "ORIENTATION"

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/db231222/I/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbxyzptlk/db231222/I/l;->b:Ldbxyzptlk/db231222/I/l;

    .line 50
    new-instance v0, Ldbxyzptlk/db231222/I/l;

    const-string v1, "BYTE_SEGMENTS"

    invoke-direct {v0, v1, v5}, Ldbxyzptlk/db231222/I/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbxyzptlk/db231222/I/l;->c:Ldbxyzptlk/db231222/I/l;

    .line 56
    new-instance v0, Ldbxyzptlk/db231222/I/l;

    const-string v1, "ERROR_CORRECTION_LEVEL"

    invoke-direct {v0, v1, v6}, Ldbxyzptlk/db231222/I/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbxyzptlk/db231222/I/l;->d:Ldbxyzptlk/db231222/I/l;

    .line 61
    new-instance v0, Ldbxyzptlk/db231222/I/l;

    const-string v1, "ISSUE_NUMBER"

    invoke-direct {v0, v1, v7}, Ldbxyzptlk/db231222/I/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbxyzptlk/db231222/I/l;->e:Ldbxyzptlk/db231222/I/l;

    .line 67
    new-instance v0, Ldbxyzptlk/db231222/I/l;

    const-string v1, "SUGGESTED_PRICE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/I/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbxyzptlk/db231222/I/l;->f:Ldbxyzptlk/db231222/I/l;

    .line 73
    new-instance v0, Ldbxyzptlk/db231222/I/l;

    const-string v1, "POSSIBLE_COUNTRY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/I/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbxyzptlk/db231222/I/l;->g:Ldbxyzptlk/db231222/I/l;

    .line 78
    new-instance v0, Ldbxyzptlk/db231222/I/l;

    const-string v1, "UPC_EAN_EXTENSION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/I/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbxyzptlk/db231222/I/l;->h:Ldbxyzptlk/db231222/I/l;

    .line 83
    new-instance v0, Ldbxyzptlk/db231222/I/l;

    const-string v1, "PDF417_EXTRA_METADATA"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/I/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbxyzptlk/db231222/I/l;->i:Ldbxyzptlk/db231222/I/l;

    .line 25
    const/16 v0, 0x9

    new-array v0, v0, [Ldbxyzptlk/db231222/I/l;

    sget-object v1, Ldbxyzptlk/db231222/I/l;->a:Ldbxyzptlk/db231222/I/l;

    aput-object v1, v0, v3

    sget-object v1, Ldbxyzptlk/db231222/I/l;->b:Ldbxyzptlk/db231222/I/l;

    aput-object v1, v0, v4

    sget-object v1, Ldbxyzptlk/db231222/I/l;->c:Ldbxyzptlk/db231222/I/l;

    aput-object v1, v0, v5

    sget-object v1, Ldbxyzptlk/db231222/I/l;->d:Ldbxyzptlk/db231222/I/l;

    aput-object v1, v0, v6

    sget-object v1, Ldbxyzptlk/db231222/I/l;->e:Ldbxyzptlk/db231222/I/l;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Ldbxyzptlk/db231222/I/l;->f:Ldbxyzptlk/db231222/I/l;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Ldbxyzptlk/db231222/I/l;->g:Ldbxyzptlk/db231222/I/l;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Ldbxyzptlk/db231222/I/l;->h:Ldbxyzptlk/db231222/I/l;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Ldbxyzptlk/db231222/I/l;->i:Ldbxyzptlk/db231222/I/l;

    aput-object v2, v0, v1

    sput-object v0, Ldbxyzptlk/db231222/I/l;->j:[Ldbxyzptlk/db231222/I/l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldbxyzptlk/db231222/I/l;
    .locals 1

    .prologue
    .line 25
    const-class v0, Ldbxyzptlk/db231222/I/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/I/l;

    return-object v0
.end method

.method public static values()[Ldbxyzptlk/db231222/I/l;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Ldbxyzptlk/db231222/I/l;->j:[Ldbxyzptlk/db231222/I/l;

    invoke-virtual {v0}, [Ldbxyzptlk/db231222/I/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/I/l;

    return-object v0
.end method
