.class public final Ldbxyzptlk/db231222/I/k;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:[B

.field private c:[Ldbxyzptlk/db231222/I/m;

.field private final d:Ldbxyzptlk/db231222/I/a;

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ldbxyzptlk/db231222/I/l;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final f:J


# direct methods
.method public constructor <init>(Ljava/lang/String;[B[Ldbxyzptlk/db231222/I/m;Ldbxyzptlk/db231222/I/a;)V
    .locals 7

    .prologue
    .line 40
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/I/k;-><init>(Ljava/lang/String;[B[Ldbxyzptlk/db231222/I/m;Ldbxyzptlk/db231222/I/a;J)V

    .line 41
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[B[Ldbxyzptlk/db231222/I/m;Ldbxyzptlk/db231222/I/a;J)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Ldbxyzptlk/db231222/I/k;->a:Ljava/lang/String;

    .line 49
    iput-object p2, p0, Ldbxyzptlk/db231222/I/k;->b:[B

    .line 50
    iput-object p3, p0, Ldbxyzptlk/db231222/I/k;->c:[Ldbxyzptlk/db231222/I/m;

    .line 51
    iput-object p4, p0, Ldbxyzptlk/db231222/I/k;->d:Ldbxyzptlk/db231222/I/a;

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/I/k;->e:Ljava/util/Map;

    .line 53
    iput-wide p5, p0, Ldbxyzptlk/db231222/I/k;->f:J

    .line 54
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Ldbxyzptlk/db231222/I/k;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/I/l;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Ldbxyzptlk/db231222/I/k;->e:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 97
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Ldbxyzptlk/db231222/I/l;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/I/k;->e:Ljava/util/Map;

    .line 99
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/I/k;->e:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Ldbxyzptlk/db231222/I/k;->a:Ljava/lang/String;

    return-object v0
.end method
