.class public final enum Ldbxyzptlk/db231222/I/e;
.super Ljava/lang/Enum;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldbxyzptlk/db231222/I/e;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ldbxyzptlk/db231222/I/e;

.field public static final enum b:Ldbxyzptlk/db231222/I/e;

.field public static final enum c:Ldbxyzptlk/db231222/I/e;

.field public static final enum d:Ldbxyzptlk/db231222/I/e;

.field public static final enum e:Ldbxyzptlk/db231222/I/e;

.field public static final enum f:Ldbxyzptlk/db231222/I/e;

.field public static final enum g:Ldbxyzptlk/db231222/I/e;

.field public static final enum h:Ldbxyzptlk/db231222/I/e;

.field public static final enum i:Ldbxyzptlk/db231222/I/e;

.field private static final synthetic k:[Ldbxyzptlk/db231222/I/e;


# instance fields
.field private final j:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 35
    new-instance v0, Ldbxyzptlk/db231222/I/e;

    const-string v1, "OTHER"

    const-class v2, Ljava/lang/Object;

    invoke-direct {v0, v1, v4, v2}, Ldbxyzptlk/db231222/I/e;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Ldbxyzptlk/db231222/I/e;->a:Ldbxyzptlk/db231222/I/e;

    .line 41
    new-instance v0, Ldbxyzptlk/db231222/I/e;

    const-string v1, "PURE_BARCODE"

    const-class v2, Ljava/lang/Void;

    invoke-direct {v0, v1, v5, v2}, Ldbxyzptlk/db231222/I/e;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Ldbxyzptlk/db231222/I/e;->b:Ldbxyzptlk/db231222/I/e;

    .line 47
    new-instance v0, Ldbxyzptlk/db231222/I/e;

    const-string v1, "POSSIBLE_FORMATS"

    const-class v2, Ljava/util/List;

    invoke-direct {v0, v1, v6, v2}, Ldbxyzptlk/db231222/I/e;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Ldbxyzptlk/db231222/I/e;->c:Ldbxyzptlk/db231222/I/e;

    .line 53
    new-instance v0, Ldbxyzptlk/db231222/I/e;

    const-string v1, "TRY_HARDER"

    const-class v2, Ljava/lang/Void;

    invoke-direct {v0, v1, v7, v2}, Ldbxyzptlk/db231222/I/e;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Ldbxyzptlk/db231222/I/e;->d:Ldbxyzptlk/db231222/I/e;

    .line 58
    new-instance v0, Ldbxyzptlk/db231222/I/e;

    const-string v1, "CHARACTER_SET"

    const-class v2, Ljava/lang/String;

    invoke-direct {v0, v1, v8, v2}, Ldbxyzptlk/db231222/I/e;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Ldbxyzptlk/db231222/I/e;->e:Ldbxyzptlk/db231222/I/e;

    .line 63
    new-instance v0, Ldbxyzptlk/db231222/I/e;

    const-string v1, "ALLOWED_LENGTHS"

    const/4 v2, 0x5

    const-class v3, [I

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/I/e;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Ldbxyzptlk/db231222/I/e;->f:Ldbxyzptlk/db231222/I/e;

    .line 69
    new-instance v0, Ldbxyzptlk/db231222/I/e;

    const-string v1, "ASSUME_CODE_39_CHECK_DIGIT"

    const/4 v2, 0x6

    const-class v3, Ljava/lang/Void;

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/I/e;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Ldbxyzptlk/db231222/I/e;->g:Ldbxyzptlk/db231222/I/e;

    .line 76
    new-instance v0, Ldbxyzptlk/db231222/I/e;

    const-string v1, "ASSUME_GS1"

    const/4 v2, 0x7

    const-class v3, Ljava/lang/Void;

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/I/e;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Ldbxyzptlk/db231222/I/e;->h:Ldbxyzptlk/db231222/I/e;

    .line 82
    new-instance v0, Ldbxyzptlk/db231222/I/e;

    const-string v1, "NEED_RESULT_POINT_CALLBACK"

    const/16 v2, 0x8

    const-class v3, Ldbxyzptlk/db231222/I/n;

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/I/e;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Ldbxyzptlk/db231222/I/e;->i:Ldbxyzptlk/db231222/I/e;

    .line 30
    const/16 v0, 0x9

    new-array v0, v0, [Ldbxyzptlk/db231222/I/e;

    sget-object v1, Ldbxyzptlk/db231222/I/e;->a:Ldbxyzptlk/db231222/I/e;

    aput-object v1, v0, v4

    sget-object v1, Ldbxyzptlk/db231222/I/e;->b:Ldbxyzptlk/db231222/I/e;

    aput-object v1, v0, v5

    sget-object v1, Ldbxyzptlk/db231222/I/e;->c:Ldbxyzptlk/db231222/I/e;

    aput-object v1, v0, v6

    sget-object v1, Ldbxyzptlk/db231222/I/e;->d:Ldbxyzptlk/db231222/I/e;

    aput-object v1, v0, v7

    sget-object v1, Ldbxyzptlk/db231222/I/e;->e:Ldbxyzptlk/db231222/I/e;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Ldbxyzptlk/db231222/I/e;->f:Ldbxyzptlk/db231222/I/e;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Ldbxyzptlk/db231222/I/e;->g:Ldbxyzptlk/db231222/I/e;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Ldbxyzptlk/db231222/I/e;->h:Ldbxyzptlk/db231222/I/e;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Ldbxyzptlk/db231222/I/e;->i:Ldbxyzptlk/db231222/I/e;

    aput-object v2, v0, v1

    sput-object v0, Ldbxyzptlk/db231222/I/e;->k:[Ldbxyzptlk/db231222/I/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 98
    iput-object p3, p0, Ldbxyzptlk/db231222/I/e;->j:Ljava/lang/Class;

    .line 99
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldbxyzptlk/db231222/I/e;
    .locals 1

    .prologue
    .line 30
    const-class v0, Ldbxyzptlk/db231222/I/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/I/e;

    return-object v0
.end method

.method public static values()[Ldbxyzptlk/db231222/I/e;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Ldbxyzptlk/db231222/I/e;->k:[Ldbxyzptlk/db231222/I/e;

    invoke-virtual {v0}, [Ldbxyzptlk/db231222/I/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/db231222/I/e;

    return-object v0
.end method
