.class public abstract Ldbxyzptlk/db231222/C/a;
.super Landroid/os/AsyncTask;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ParameterT:",
        "Ljava/lang/Object;",
        "ProgressT:",
        "Ljava/lang/Object;",
        "ReturnT:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/os/AsyncTask",
        "<TParameterT;TProgressT;TReturnT;>;"
    }
.end annotation


# instance fields
.field private a:Lcom/github/droidfu/DroidFuApplication;

.field private b:Ljava/lang/Exception;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Ldbxyzptlk/db231222/C/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/C/b",
            "<TParameterT;TProgressT;TReturnT;>;"
        }
    .end annotation
.end field

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 43
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 34
    iput-boolean v3, p0, Ldbxyzptlk/db231222/C/a;->e:Z

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Ldbxyzptlk/db231222/C/a;->h:I

    .line 45
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lcom/github/droidfu/DroidFuApplication;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context bound to this task must be a DroidFu context (DroidFuApplication)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/github/droidfu/DroidFuApplication;

    iput-object v0, p0, Ldbxyzptlk/db231222/C/a;->a:Lcom/github/droidfu/DroidFuApplication;

    .line 50
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/C/a;->f:Ljava/lang/String;

    .line 51
    instance-of v0, p1, Lcom/github/droidfu/activities/a;

    iput-boolean v0, p0, Ldbxyzptlk/db231222/C/a;->c:Z

    .line 53
    iget-object v0, p0, Ldbxyzptlk/db231222/C/a;->a:Lcom/github/droidfu/DroidFuApplication;

    iget-object v1, p0, Ldbxyzptlk/db231222/C/a;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/github/droidfu/DroidFuApplication;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 55
    iget-boolean v0, p0, Ldbxyzptlk/db231222/C/a;->c:Z

    if-eqz v0, :cond_1

    .line 56
    check-cast p1, Lcom/github/droidfu/activities/a;

    invoke-interface {p1}, Lcom/github/droidfu/activities/a;->d_()I

    move-result v0

    .line 57
    const/4 v1, 0x2

    and-int/lit8 v2, v0, 0x2

    if-ne v1, v2, :cond_2

    .line 58
    iput-boolean v3, p0, Ldbxyzptlk/db231222/C/a;->d:Z

    .line 63
    :cond_1
    :goto_0
    return-void

    .line 59
    :cond_2
    const/4 v1, 0x5

    and-int/lit8 v0, v0, 0x5

    if-ne v1, v0, :cond_1

    .line 60
    iput-boolean v3, p0, Ldbxyzptlk/db231222/C/a;->e:Z

    goto :goto_0
.end method


# virtual methods
.method protected varargs a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "[TParameterT;)TReturnT;"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Ldbxyzptlk/db231222/C/a;->g:Ldbxyzptlk/db231222/C/b;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Ldbxyzptlk/db231222/C/a;->g:Ldbxyzptlk/db231222/C/b;

    invoke-interface {v0, p0}, Ldbxyzptlk/db231222/C/b;->a(Ldbxyzptlk/db231222/C/a;)Ljava/lang/Object;

    move-result-object v0

    .line 124
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 168
    iput p1, p0, Ldbxyzptlk/db231222/C/a;->h:I

    .line 169
    return-void
.end method

.method protected a(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 106
    return-void
.end method

.method protected abstract a(Landroid/content/Context;Ljava/lang/Exception;)V
.end method

.method protected abstract a(Landroid/content/Context;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TReturnT;)V"
        }
    .end annotation
.end method

.method protected final d()Landroid/content/Context;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 67
    :try_start_0
    iget-object v1, p0, Ldbxyzptlk/db231222/C/a;->a:Lcom/github/droidfu/DroidFuApplication;

    iget-object v2, p0, Ldbxyzptlk/db231222/C/a;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/github/droidfu/DroidFuApplication;->a(Ljava/lang/String;)Landroid/content/Context;

    move-result-object v2

    .line 68
    if-eqz v2, :cond_0

    iget-object v1, p0, Ldbxyzptlk/db231222/C/a;->f:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    instance-of v1, v2, Landroid/app/Activity;

    if-eqz v1, :cond_1

    move-object v0, v2

    check-cast v0, Landroid/app/Activity;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move-object v2, v3

    .line 77
    :cond_1
    :goto_0
    return-object v2

    .line 75
    :catch_0
    move-exception v1

    .line 76
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    move-object v2, v3

    .line 77
    goto :goto_0
.end method

.method protected final varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParameterT;)TReturnT;"
        }
    .end annotation

    .prologue
    .line 110
    const/4 v0, 0x0

    .line 111
    invoke-virtual {p0}, Ldbxyzptlk/db231222/C/a;->d()Landroid/content/Context;

    move-result-object v1

    .line 113
    :try_start_0
    invoke-virtual {p0, v1, p1}, Ldbxyzptlk/db231222/C/a;->a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 117
    :goto_0
    return-object v0

    .line 114
    :catch_0
    move-exception v1

    .line 115
    iput-object v1, p0, Ldbxyzptlk/db231222/C/a;->b:Ljava/lang/Exception;

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Ldbxyzptlk/db231222/C/a;->b:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 172
    const/4 v0, -0x1

    iput v0, p0, Ldbxyzptlk/db231222/C/a;->h:I

    .line 173
    return-void
.end method

.method protected final onPostExecute(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TReturnT;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 131
    invoke-virtual {p0}, Ldbxyzptlk/db231222/C/a;->d()Landroid/content/Context;

    move-result-object v1

    .line 132
    if-nez v1, :cond_0

    .line 133
    const-class v0, Ldbxyzptlk/db231222/C/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "skipping post-exec handler for task "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (context is null)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    :goto_0
    return-void

    .line 138
    :cond_0
    iget-boolean v0, p0, Ldbxyzptlk/db231222/C/a;->c:Z

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 139
    check-cast v0, Landroid/app/Activity;

    .line 140
    iget v2, p0, Ldbxyzptlk/db231222/C/a;->h:I

    const/4 v3, -0x1

    if-le v2, v3, :cond_1

    .line 141
    iget v2, p0, Ldbxyzptlk/db231222/C/a;->h:I

    invoke-virtual {v0, v2}, Landroid/app/Activity;->removeDialog(I)V

    .line 143
    :cond_1
    iget-boolean v2, p0, Ldbxyzptlk/db231222/C/a;->d:Z

    if-eqz v2, :cond_3

    .line 144
    invoke-virtual {v0, v4}, Landroid/app/Activity;->setProgressBarVisibility(Z)V

    .line 150
    :cond_2
    :goto_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/C/a;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 151
    iget-object v0, p0, Ldbxyzptlk/db231222/C/a;->b:Ljava/lang/Exception;

    invoke-virtual {p0, v1, v0}, Ldbxyzptlk/db231222/C/a;->a(Landroid/content/Context;Ljava/lang/Exception;)V

    goto :goto_0

    .line 145
    :cond_3
    iget-boolean v2, p0, Ldbxyzptlk/db231222/C/a;->e:Z

    if-eqz v2, :cond_2

    .line 146
    invoke-virtual {v0, v4}, Landroid/app/Activity;->setProgressBarIndeterminateVisibility(Z)V

    goto :goto_1

    .line 153
    :cond_4
    invoke-virtual {p0, v1, p1}, Ldbxyzptlk/db231222/C/a;->a(Landroid/content/Context;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected final onPreExecute()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 83
    invoke-virtual {p0}, Ldbxyzptlk/db231222/C/a;->d()Landroid/content/Context;

    move-result-object v1

    .line 84
    if-nez v1, :cond_0

    .line 85
    const-class v0, Ldbxyzptlk/db231222/C/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "skipping pre-exec handler for task "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (context is null)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    invoke-virtual {p0, v4}, Ldbxyzptlk/db231222/C/a;->cancel(Z)Z

    .line 103
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-boolean v0, p0, Ldbxyzptlk/db231222/C/a;->c:Z

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 92
    check-cast v0, Landroid/app/Activity;

    .line 93
    iget v2, p0, Ldbxyzptlk/db231222/C/a;->h:I

    const/4 v3, -0x1

    if-le v2, v3, :cond_1

    .line 94
    iget v2, p0, Ldbxyzptlk/db231222/C/a;->h:I

    invoke-virtual {v0, v2}, Landroid/app/Activity;->showDialog(I)V

    .line 96
    :cond_1
    iget-boolean v2, p0, Ldbxyzptlk/db231222/C/a;->d:Z

    if-eqz v2, :cond_3

    .line 97
    invoke-virtual {v0, v4}, Landroid/app/Activity;->setProgressBarVisibility(Z)V

    .line 102
    :cond_2
    :goto_1
    invoke-virtual {p0, v1}, Ldbxyzptlk/db231222/C/a;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 98
    :cond_3
    iget-boolean v2, p0, Ldbxyzptlk/db231222/C/a;->e:Z

    if-eqz v2, :cond_2

    .line 99
    invoke-virtual {v0, v4}, Landroid/app/Activity;->setProgressBarIndeterminateVisibility(Z)V

    goto :goto_1
.end method
