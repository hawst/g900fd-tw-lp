.class public final Ldbxyzptlk/db231222/o/a;
.super Ldbxyzptlk/db231222/H/j;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/o/h;


# static fields
.field public static a:Ldbxyzptlk/db231222/H/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/H/w",
            "<",
            "Ldbxyzptlk/db231222/o/a;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ldbxyzptlk/db231222/o/a;

.field private static final serialVersionUID:J


# instance fields
.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/o/c;",
            ">;"
        }
    .end annotation
.end field

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 75
    new-instance v0, Ldbxyzptlk/db231222/o/b;

    invoke-direct {v0}, Ldbxyzptlk/db231222/o/b;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/o/a;->a:Ldbxyzptlk/db231222/H/w;

    .line 1267
    new-instance v0, Ldbxyzptlk/db231222/o/a;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/o/a;-><init>(Z)V

    sput-object v0, Ldbxyzptlk/db231222/o/a;->b:Ldbxyzptlk/db231222/o/a;

    .line 1268
    sget-object v0, Ldbxyzptlk/db231222/o/a;->b:Ldbxyzptlk/db231222/o/a;

    invoke-direct {v0}, Ldbxyzptlk/db231222/o/a;->h()V

    .line 1269
    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x1

    .line 35
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/j;-><init>()V

    .line 934
    iput-byte v2, p0, Ldbxyzptlk/db231222/o/a;->d:B

    .line 957
    iput v2, p0, Ldbxyzptlk/db231222/o/a;->e:I

    .line 36
    invoke-direct {p0}, Ldbxyzptlk/db231222/o/a;->h()V

    move v2, v0

    .line 40
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 41
    :try_start_0
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->a()I

    move-result v3

    .line 42
    sparse-switch v3, :sswitch_data_0

    .line 47
    invoke-virtual {p0, p1, p2, v3}, Ldbxyzptlk/db231222/o/a;->a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;I)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 49
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 45
    goto :goto_0

    .line 54
    :sswitch_1
    and-int/lit8 v3, v2, 0x1

    if-eq v3, v1, :cond_1

    .line 55
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Ldbxyzptlk/db231222/o/a;->c:Ljava/util/List;

    .line 56
    or-int/lit8 v2, v2, 0x1

    .line 58
    :cond_1
    iget-object v3, p0, Ldbxyzptlk/db231222/o/a;->c:Ljava/util/List;

    sget-object v4, Ldbxyzptlk/db231222/o/c;->a:Ldbxyzptlk/db231222/H/w;

    invoke-virtual {p1, v4, p2}, Ldbxyzptlk/db231222/H/f;->a(Ldbxyzptlk/db231222/H/w;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/H/t;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    :try_start_1
    invoke-virtual {v0, p0}, Ldbxyzptlk/db231222/H/o;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/o;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 69
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_2

    .line 70
    iget-object v1, p0, Ldbxyzptlk/db231222/o/a;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Ldbxyzptlk/db231222/o/a;->c:Ljava/util/List;

    .line 72
    :cond_2
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/a;->L()V

    throw v0

    .line 69
    :cond_3
    and-int/lit8 v0, v2, 0x1

    if-ne v0, v1, :cond_4

    .line 70
    iget-object v0, p0, Ldbxyzptlk/db231222/o/a;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/o/a;->c:Ljava/util/List;

    .line 72
    :cond_4
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/a;->L()V

    .line 74
    return-void

    .line 65
    :catch_1
    move-exception v0

    .line 66
    :try_start_2
    new-instance v3, Ldbxyzptlk/db231222/H/o;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ldbxyzptlk/db231222/H/o;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ldbxyzptlk/db231222/H/o;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/o;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method synthetic constructor <init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;Ldbxyzptlk/db231222/o/b;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/o/a;-><init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)V

    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/H/k;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 18
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/H/j;-><init>(Ldbxyzptlk/db231222/H/k;)V

    .line 934
    iput-byte v0, p0, Ldbxyzptlk/db231222/o/a;->d:B

    .line 957
    iput v0, p0, Ldbxyzptlk/db231222/o/a;->e:I

    .line 20
    return-void
.end method

.method synthetic constructor <init>(Ldbxyzptlk/db231222/H/k;Ldbxyzptlk/db231222/o/b;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/o/a;-><init>(Ldbxyzptlk/db231222/H/k;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 21
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/j;-><init>()V

    .line 934
    iput-byte v0, p0, Ldbxyzptlk/db231222/o/a;->d:B

    .line 957
    iput v0, p0, Ldbxyzptlk/db231222/o/a;->e:I

    .line 21
    return-void
.end method

.method public static a()Ldbxyzptlk/db231222/o/a;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Ldbxyzptlk/db231222/o/a;->b:Ldbxyzptlk/db231222/o/a;

    return-object v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/o/a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Ldbxyzptlk/db231222/o/a;->c:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/o/a;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 13
    iput-object p1, p0, Ldbxyzptlk/db231222/o/a;->c:Ljava/util/List;

    return-object p1
.end method

.method public static g()Ldbxyzptlk/db231222/o/g;
    .locals 1

    .prologue
    .line 1031
    invoke-static {}, Ldbxyzptlk/db231222/o/g;->f()Ldbxyzptlk/db231222/o/g;

    move-result-object v0

    return-object v0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 932
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/o/a;->c:Ljava/util/List;

    .line 933
    return-void
.end method


# virtual methods
.method public final a(I)Ldbxyzptlk/db231222/o/c;
    .locals 1

    .prologue
    .line 921
    iget-object v0, p0, Ldbxyzptlk/db231222/o/a;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/o/c;

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/H/g;)V
    .locals 3

    .prologue
    .line 951
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/a;->f()I

    .line 952
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Ldbxyzptlk/db231222/o/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 953
    const/4 v2, 0x1

    iget-object v0, p0, Ldbxyzptlk/db231222/o/a;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/H/t;

    invoke-virtual {p1, v2, v0}, Ldbxyzptlk/db231222/H/g;->a(ILdbxyzptlk/db231222/H/t;)V

    .line 952
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 955
    :cond_0
    return-void
.end method

.method public final b()Ldbxyzptlk/db231222/H/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldbxyzptlk/db231222/H/w",
            "<",
            "Ldbxyzptlk/db231222/o/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    sget-object v0, Ldbxyzptlk/db231222/o/a;->a:Ldbxyzptlk/db231222/H/w;

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/o/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 902
    iget-object v0, p0, Ldbxyzptlk/db231222/o/a;->c:Ljava/util/List;

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 915
    iget-object v0, p0, Ldbxyzptlk/db231222/o/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 936
    iget-byte v2, p0, Ldbxyzptlk/db231222/o/a;->d:B

    .line 937
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    if-ne v2, v0, :cond_0

    .line 946
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 937
    goto :goto_0

    :cond_1
    move v2, v1

    .line 939
    :goto_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/a;->d()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 940
    invoke-virtual {p0, v2}, Ldbxyzptlk/db231222/o/a;->a(I)Ldbxyzptlk/db231222/o/c;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/db231222/o/c;->e()Z

    move-result v3

    if-nez v3, :cond_2

    .line 941
    iput-byte v1, p0, Ldbxyzptlk/db231222/o/a;->d:B

    move v0, v1

    .line 942
    goto :goto_0

    .line 939
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 945
    :cond_3
    iput-byte v0, p0, Ldbxyzptlk/db231222/o/a;->d:B

    goto :goto_0
.end method

.method public final f()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 959
    iget v2, p0, Ldbxyzptlk/db231222/o/a;->e:I

    .line 960
    const/4 v1, -0x1

    if-eq v2, v1, :cond_0

    .line 968
    :goto_0
    return v2

    :cond_0
    move v1, v0

    move v2, v0

    .line 963
    :goto_1
    iget-object v0, p0, Ldbxyzptlk/db231222/o/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 964
    const/4 v3, 0x1

    iget-object v0, p0, Ldbxyzptlk/db231222/o/a;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/H/t;

    invoke-static {v3, v0}, Ldbxyzptlk/db231222/H/g;->b(ILdbxyzptlk/db231222/H/t;)I

    move-result v0

    add-int/2addr v2, v0

    .line 963
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 967
    :cond_1
    iput v2, p0, Ldbxyzptlk/db231222/o/a;->e:I

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 975
    invoke-super {p0}, Ldbxyzptlk/db231222/H/j;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
