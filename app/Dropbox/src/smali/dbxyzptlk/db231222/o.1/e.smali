.class public final Ldbxyzptlk/db231222/o/e;
.super Ldbxyzptlk/db231222/H/k;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/o/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/H/k",
        "<",
        "Ldbxyzptlk/db231222/o/c;",
        "Ldbxyzptlk/db231222/o/e;",
        ">;",
        "Ldbxyzptlk/db231222/o/f;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:J


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 529
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/k;-><init>()V

    .line 640
    const-string v0, ""

    iput-object v0, p0, Ldbxyzptlk/db231222/o/e;->b:Ljava/lang/Object;

    .line 738
    const-string v0, ""

    iput-object v0, p0, Ldbxyzptlk/db231222/o/e;->c:Ljava/lang/Object;

    .line 530
    invoke-direct {p0}, Ldbxyzptlk/db231222/o/e;->l()V

    .line 531
    return-void
.end method

.method static synthetic k()Ldbxyzptlk/db231222/o/e;
    .locals 1

    .prologue
    .line 524
    invoke-static {}, Ldbxyzptlk/db231222/o/e;->m()Ldbxyzptlk/db231222/o/e;

    move-result-object v0

    return-object v0
.end method

.method private l()V
    .locals 0

    .prologue
    .line 534
    return-void
.end method

.method private static m()Ldbxyzptlk/db231222/o/e;
    .locals 1

    .prologue
    .line 536
    new-instance v0, Ldbxyzptlk/db231222/o/e;

    invoke-direct {v0}, Ldbxyzptlk/db231222/o/e;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Ldbxyzptlk/db231222/o/e;
    .locals 2

    .prologue
    .line 551
    invoke-static {}, Ldbxyzptlk/db231222/o/e;->m()Ldbxyzptlk/db231222/o/e;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/e;->c()Ldbxyzptlk/db231222/o/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/o/e;->a(Ldbxyzptlk/db231222/o/c;)Ldbxyzptlk/db231222/o/e;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)Ldbxyzptlk/db231222/o/e;
    .locals 1

    .prologue
    .line 865
    iget v0, p0, Ldbxyzptlk/db231222/o/e;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ldbxyzptlk/db231222/o/e;->a:I

    .line 866
    iput-wide p1, p0, Ldbxyzptlk/db231222/o/e;->d:J

    .line 868
    return-object p0
.end method

.method public final a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/o/e;
    .locals 4

    .prologue
    .line 624
    const/4 v2, 0x0

    .line 626
    :try_start_0
    sget-object v0, Ldbxyzptlk/db231222/o/c;->a:Ldbxyzptlk/db231222/H/w;

    invoke-interface {v0, p1, p2}, Ldbxyzptlk/db231222/H/w;->b(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/o/c;
    :try_end_0
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 631
    if-eqz v0, :cond_0

    .line 632
    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/o/e;->a(Ldbxyzptlk/db231222/o/c;)Ldbxyzptlk/db231222/o/e;

    .line 635
    :cond_0
    return-object p0

    .line 627
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 628
    :try_start_1
    invoke-virtual {v1}, Ldbxyzptlk/db231222/H/o;->a()Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/o/c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 629
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 631
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 632
    invoke-virtual {p0, v1}, Ldbxyzptlk/db231222/o/e;->a(Ldbxyzptlk/db231222/o/c;)Ldbxyzptlk/db231222/o/e;

    :cond_1
    throw v0

    .line 631
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public final a(Ldbxyzptlk/db231222/o/c;)Ldbxyzptlk/db231222/o/e;
    .locals 2

    .prologue
    .line 587
    invoke-static {}, Ldbxyzptlk/db231222/o/c;->a()Ldbxyzptlk/db231222/o/c;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 601
    :cond_0
    :goto_0
    return-object p0

    .line 588
    :cond_1
    invoke-virtual {p1}, Ldbxyzptlk/db231222/o/c;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 589
    iget v0, p0, Ldbxyzptlk/db231222/o/e;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/o/e;->a:I

    .line 590
    invoke-static {p1}, Ldbxyzptlk/db231222/o/c;->a(Ldbxyzptlk/db231222/o/c;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/o/e;->b:Ljava/lang/Object;

    .line 593
    :cond_2
    invoke-virtual {p1}, Ldbxyzptlk/db231222/o/c;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 594
    iget v0, p0, Ldbxyzptlk/db231222/o/e;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ldbxyzptlk/db231222/o/e;->a:I

    .line 595
    invoke-static {p1}, Ldbxyzptlk/db231222/o/c;->b(Ldbxyzptlk/db231222/o/c;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/o/e;->c:Ljava/lang/Object;

    .line 598
    :cond_3
    invoke-virtual {p1}, Ldbxyzptlk/db231222/o/c;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599
    invoke-virtual {p1}, Ldbxyzptlk/db231222/o/c;->l()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ldbxyzptlk/db231222/o/e;->a(J)Ldbxyzptlk/db231222/o/e;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Ldbxyzptlk/db231222/o/e;
    .locals 1

    .prologue
    .line 698
    if-nez p1, :cond_0

    .line 699
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 701
    :cond_0
    iget v0, p0, Ldbxyzptlk/db231222/o/e;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/o/e;->a:I

    .line 702
    iput-object p1, p0, Ldbxyzptlk/db231222/o/e;->b:Ljava/lang/Object;

    .line 704
    return-object p0
.end method

.method public final synthetic b(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/H/b;
    .locals 1

    .prologue
    .line 524
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/o/e;->a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/o/e;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ldbxyzptlk/db231222/o/c;
    .locals 2

    .prologue
    .line 559
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/e;->c()Ldbxyzptlk/db231222/o/c;

    move-result-object v0

    .line 560
    invoke-virtual {v0}, Ldbxyzptlk/db231222/o/c;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 561
    invoke-static {v0}, Ldbxyzptlk/db231222/o/e;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/C;

    move-result-object v0

    throw v0

    .line 563
    :cond_0
    return-object v0
.end method

.method public final b(Ljava/lang/String;)Ldbxyzptlk/db231222/o/e;
    .locals 1

    .prologue
    .line 796
    if-nez p1, :cond_0

    .line 797
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 799
    :cond_0
    iget v0, p0, Ldbxyzptlk/db231222/o/e;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ldbxyzptlk/db231222/o/e;->a:I

    .line 800
    iput-object p1, p0, Ldbxyzptlk/db231222/o/e;->c:Ljava/lang/Object;

    .line 802
    return-object p0
.end method

.method public final c()Ldbxyzptlk/db231222/o/c;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 567
    new-instance v2, Ldbxyzptlk/db231222/o/c;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Ldbxyzptlk/db231222/o/c;-><init>(Ldbxyzptlk/db231222/H/k;Ldbxyzptlk/db231222/o/b;)V

    .line 568
    iget v3, p0, Ldbxyzptlk/db231222/o/e;->a:I

    .line 569
    const/4 v1, 0x0

    .line 570
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 573
    :goto_0
    iget-object v1, p0, Ldbxyzptlk/db231222/o/e;->b:Ljava/lang/Object;

    invoke-static {v2, v1}, Ldbxyzptlk/db231222/o/c;->a(Ldbxyzptlk/db231222/o/c;Ljava/lang/Object;)Ljava/lang/Object;

    .line 574
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 575
    or-int/lit8 v0, v0, 0x2

    .line 577
    :cond_0
    iget-object v1, p0, Ldbxyzptlk/db231222/o/e;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Ldbxyzptlk/db231222/o/c;->b(Ldbxyzptlk/db231222/o/c;Ljava/lang/Object;)Ljava/lang/Object;

    .line 578
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 579
    or-int/lit8 v0, v0, 0x4

    .line 581
    :cond_1
    iget-wide v3, p0, Ldbxyzptlk/db231222/o/e;->d:J

    invoke-static {v2, v3, v4}, Ldbxyzptlk/db231222/o/c;->a(Ldbxyzptlk/db231222/o/c;J)J

    .line 582
    invoke-static {v2, v0}, Ldbxyzptlk/db231222/o/c;->a(Ldbxyzptlk/db231222/o/c;I)I

    .line 583
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 524
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/e;->a()Ldbxyzptlk/db231222/o/e;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 649
    iget v1, p0, Ldbxyzptlk/db231222/o/e;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 605
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/e;->d()Z

    move-result v1

    if-nez v1, :cond_1

    .line 617
    :cond_0
    :goto_0
    return v0

    .line 609
    :cond_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/e;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 613
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/e;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 617
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 747
    iget v0, p0, Ldbxyzptlk/db231222/o/e;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 845
    iget v0, p0, Ldbxyzptlk/db231222/o/e;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic h()Ldbxyzptlk/db231222/H/k;
    .locals 1

    .prologue
    .line 524
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/e;->a()Ldbxyzptlk/db231222/o/e;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()Ldbxyzptlk/db231222/H/b;
    .locals 1

    .prologue
    .line 524
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/e;->a()Ldbxyzptlk/db231222/o/e;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()Ldbxyzptlk/db231222/H/t;
    .locals 1

    .prologue
    .line 524
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/e;->c()Ldbxyzptlk/db231222/o/c;

    move-result-object v0

    return-object v0
.end method
