.class public final Ldbxyzptlk/db231222/o/c;
.super Ldbxyzptlk/db231222/H/j;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/o/f;


# static fields
.field public static a:Ldbxyzptlk/db231222/H/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldbxyzptlk/db231222/H/w",
            "<",
            "Ldbxyzptlk/db231222/o/c;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ldbxyzptlk/db231222/o/c;

.field private static final serialVersionUID:J


# instance fields
.field private c:I

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:J

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 238
    new-instance v0, Ldbxyzptlk/db231222/o/d;

    invoke-direct {v0}, Ldbxyzptlk/db231222/o/d;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/o/c;->a:Ldbxyzptlk/db231222/H/w;

    .line 888
    new-instance v0, Ldbxyzptlk/db231222/o/c;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/o/c;-><init>(Z)V

    sput-object v0, Ldbxyzptlk/db231222/o/c;->b:Ldbxyzptlk/db231222/o/c;

    .line 889
    sget-object v0, Ldbxyzptlk/db231222/o/c;->b:Ldbxyzptlk/db231222/o/c;

    invoke-direct {v0}, Ldbxyzptlk/db231222/o/c;->n()V

    .line 890
    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 194
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/j;-><init>()V

    .line 393
    iput-byte v0, p0, Ldbxyzptlk/db231222/o/c;->g:B

    .line 428
    iput v0, p0, Ldbxyzptlk/db231222/o/c;->h:I

    .line 195
    invoke-direct {p0}, Ldbxyzptlk/db231222/o/c;->n()V

    .line 198
    const/4 v0, 0x0

    .line 199
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 200
    :try_start_0
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->a()I

    move-result v2

    .line 201
    sparse-switch v2, :sswitch_data_0

    .line 206
    invoke-virtual {p0, p1, p2, v2}, Ldbxyzptlk/db231222/o/c;->a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 208
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 204
    goto :goto_0

    .line 213
    :sswitch_1
    iget v2, p0, Ldbxyzptlk/db231222/o/c;->c:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Ldbxyzptlk/db231222/o/c;->c:I

    .line 214
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->g()Ldbxyzptlk/db231222/H/d;

    move-result-object v2

    iput-object v2, p0, Ldbxyzptlk/db231222/o/c;->d:Ljava/lang/Object;
    :try_end_0
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 229
    :catch_0
    move-exception v0

    .line 230
    :try_start_1
    invoke-virtual {v0, p0}, Ldbxyzptlk/db231222/H/o;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/o;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 235
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/c;->L()V

    throw v0

    .line 218
    :sswitch_2
    :try_start_2
    iget v2, p0, Ldbxyzptlk/db231222/o/c;->c:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Ldbxyzptlk/db231222/o/c;->c:I

    .line 219
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->g()Ldbxyzptlk/db231222/H/d;

    move-result-object v2

    iput-object v2, p0, Ldbxyzptlk/db231222/o/c;->e:Ljava/lang/Object;
    :try_end_2
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 231
    :catch_1
    move-exception v0

    .line 232
    :try_start_3
    new-instance v1, Ldbxyzptlk/db231222/H/o;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/H/o;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ldbxyzptlk/db231222/H/o;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/o;

    move-result-object v0

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 223
    :sswitch_3
    :try_start_4
    iget v2, p0, Ldbxyzptlk/db231222/o/c;->c:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Ldbxyzptlk/db231222/o/c;->c:I

    .line 224
    invoke-virtual {p1}, Ldbxyzptlk/db231222/H/f;->c()J

    move-result-wide v2

    iput-wide v2, p0, Ldbxyzptlk/db231222/o/c;->f:J
    :try_end_4
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 235
    :cond_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/c;->L()V

    .line 237
    return-void

    .line 201
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method synthetic constructor <init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;Ldbxyzptlk/db231222/o/b;)V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/o/c;-><init>(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)V

    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/H/k;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 177
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/H/j;-><init>(Ldbxyzptlk/db231222/H/k;)V

    .line 393
    iput-byte v0, p0, Ldbxyzptlk/db231222/o/c;->g:B

    .line 428
    iput v0, p0, Ldbxyzptlk/db231222/o/c;->h:I

    .line 179
    return-void
.end method

.method synthetic constructor <init>(Ldbxyzptlk/db231222/H/k;Ldbxyzptlk/db231222/o/b;)V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/o/c;-><init>(Ldbxyzptlk/db231222/H/k;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 180
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/j;-><init>()V

    .line 393
    iput-byte v0, p0, Ldbxyzptlk/db231222/o/c;->g:B

    .line 428
    iput v0, p0, Ldbxyzptlk/db231222/o/c;->h:I

    .line 180
    return-void
.end method

.method static synthetic a(Ldbxyzptlk/db231222/o/c;I)I
    .locals 0

    .prologue
    .line 172
    iput p1, p0, Ldbxyzptlk/db231222/o/c;->c:I

    return p1
.end method

.method static synthetic a(Ldbxyzptlk/db231222/o/c;J)J
    .locals 0

    .prologue
    .line 172
    iput-wide p1, p0, Ldbxyzptlk/db231222/o/c;->f:J

    return-wide p1
.end method

.method public static a()Ldbxyzptlk/db231222/o/c;
    .locals 1

    .prologue
    .line 184
    sget-object v0, Ldbxyzptlk/db231222/o/c;->b:Ldbxyzptlk/db231222/o/c;

    return-object v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/o/c;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Ldbxyzptlk/db231222/o/c;->d:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/o/c;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Ldbxyzptlk/db231222/o/c;->d:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Ldbxyzptlk/db231222/o/c;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Ldbxyzptlk/db231222/o/c;->e:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Ldbxyzptlk/db231222/o/c;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Ldbxyzptlk/db231222/o/c;->e:Ljava/lang/Object;

    return-object p1
.end method

.method public static m()Ldbxyzptlk/db231222/o/e;
    .locals 1

    .prologue
    .line 510
    invoke-static {}, Ldbxyzptlk/db231222/o/e;->k()Ldbxyzptlk/db231222/o/e;

    move-result-object v0

    return-object v0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 389
    const-string v0, ""

    iput-object v0, p0, Ldbxyzptlk/db231222/o/c;->d:Ljava/lang/Object;

    .line 390
    const-string v0, ""

    iput-object v0, p0, Ldbxyzptlk/db231222/o/c;->e:Ljava/lang/Object;

    .line 391
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ldbxyzptlk/db231222/o/c;->f:J

    .line 392
    return-void
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/H/g;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 416
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/c;->f()I

    .line 417
    iget v0, p0, Ldbxyzptlk/db231222/o/c;->c:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 418
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/c;->g()Ldbxyzptlk/db231222/H/d;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Ldbxyzptlk/db231222/H/g;->a(ILdbxyzptlk/db231222/H/d;)V

    .line 420
    :cond_0
    iget v0, p0, Ldbxyzptlk/db231222/o/c;->c:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 421
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/c;->j()Ldbxyzptlk/db231222/H/d;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Ldbxyzptlk/db231222/H/g;->a(ILdbxyzptlk/db231222/H/d;)V

    .line 423
    :cond_1
    iget v0, p0, Ldbxyzptlk/db231222/o/c;->c:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 424
    const/4 v0, 0x3

    iget-wide v1, p0, Ldbxyzptlk/db231222/o/c;->f:J

    invoke-virtual {p1, v0, v1, v2}, Ldbxyzptlk/db231222/H/g;->a(IJ)V

    .line 426
    :cond_2
    return-void
.end method

.method public final b()Ldbxyzptlk/db231222/H/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ldbxyzptlk/db231222/H/w",
            "<",
            "Ldbxyzptlk/db231222/o/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250
    sget-object v0, Ldbxyzptlk/db231222/o/c;->a:Ldbxyzptlk/db231222/H/w;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 265
    iget v1, p0, Ldbxyzptlk/db231222/o/c;->c:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 275
    iget-object v0, p0, Ldbxyzptlk/db231222/o/c;->d:Ljava/lang/Object;

    .line 276
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 277
    check-cast v0, Ljava/lang/String;

    .line 285
    :goto_0
    return-object v0

    .line 279
    :cond_0
    check-cast v0, Ldbxyzptlk/db231222/H/d;

    .line 281
    invoke-virtual {v0}, Ldbxyzptlk/db231222/H/d;->e()Ljava/lang/String;

    move-result-object v1

    .line 282
    invoke-virtual {v0}, Ldbxyzptlk/db231222/H/d;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 283
    iput-object v1, p0, Ldbxyzptlk/db231222/o/c;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 285
    goto :goto_0
.end method

.method public final e()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 395
    iget-byte v2, p0, Ldbxyzptlk/db231222/o/c;->g:B

    .line 396
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    if-ne v2, v0, :cond_0

    :goto_0
    move v1, v0

    .line 411
    :goto_1
    return v1

    :cond_0
    move v0, v1

    .line 396
    goto :goto_0

    .line 398
    :cond_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/c;->c()Z

    move-result v2

    if-nez v2, :cond_2

    .line 399
    iput-byte v1, p0, Ldbxyzptlk/db231222/o/c;->g:B

    goto :goto_1

    .line 402
    :cond_2
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/c;->h()Z

    move-result v2

    if-nez v2, :cond_3

    .line 403
    iput-byte v1, p0, Ldbxyzptlk/db231222/o/c;->g:B

    goto :goto_1

    .line 406
    :cond_3
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/c;->k()Z

    move-result v2

    if-nez v2, :cond_4

    .line 407
    iput-byte v1, p0, Ldbxyzptlk/db231222/o/c;->g:B

    goto :goto_1

    .line 410
    :cond_4
    iput-byte v0, p0, Ldbxyzptlk/db231222/o/c;->g:B

    move v1, v0

    .line 411
    goto :goto_1
.end method

.method public final f()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 430
    iget v0, p0, Ldbxyzptlk/db231222/o/c;->h:I

    .line 431
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 447
    :goto_0
    return v0

    .line 433
    :cond_0
    const/4 v0, 0x0

    .line 434
    iget v1, p0, Ldbxyzptlk/db231222/o/c;->c:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 435
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/c;->g()Ldbxyzptlk/db231222/H/d;

    move-result-object v1

    invoke-static {v2, v1}, Ldbxyzptlk/db231222/H/g;->b(ILdbxyzptlk/db231222/H/d;)I

    move-result v1

    add-int/2addr v0, v1

    .line 438
    :cond_1
    iget v1, p0, Ldbxyzptlk/db231222/o/c;->c:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 439
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/c;->j()Ldbxyzptlk/db231222/H/d;

    move-result-object v1

    invoke-static {v3, v1}, Ldbxyzptlk/db231222/H/g;->b(ILdbxyzptlk/db231222/H/d;)I

    move-result v1

    add-int/2addr v0, v1

    .line 442
    :cond_2
    iget v1, p0, Ldbxyzptlk/db231222/o/c;->c:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 443
    const/4 v1, 0x3

    iget-wide v2, p0, Ldbxyzptlk/db231222/o/c;->f:J

    invoke-static {v1, v2, v3}, Ldbxyzptlk/db231222/H/g;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 446
    :cond_3
    iput v0, p0, Ldbxyzptlk/db231222/o/c;->h:I

    goto :goto_0
.end method

.method public final g()Ldbxyzptlk/db231222/H/d;
    .locals 2

    .prologue
    .line 297
    iget-object v0, p0, Ldbxyzptlk/db231222/o/c;->d:Ljava/lang/Object;

    .line 298
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 299
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ldbxyzptlk/db231222/H/d;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/H/d;

    move-result-object v0

    .line 302
    iput-object v0, p0, Ldbxyzptlk/db231222/o/c;->d:Ljava/lang/Object;

    .line 305
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ldbxyzptlk/db231222/H/d;

    goto :goto_0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 320
    iget v0, p0, Ldbxyzptlk/db231222/o/c;->c:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 330
    iget-object v0, p0, Ldbxyzptlk/db231222/o/c;->e:Ljava/lang/Object;

    .line 331
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 332
    check-cast v0, Ljava/lang/String;

    .line 340
    :goto_0
    return-object v0

    .line 334
    :cond_0
    check-cast v0, Ldbxyzptlk/db231222/H/d;

    .line 336
    invoke-virtual {v0}, Ldbxyzptlk/db231222/H/d;->e()Ljava/lang/String;

    move-result-object v1

    .line 337
    invoke-virtual {v0}, Ldbxyzptlk/db231222/H/d;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 338
    iput-object v1, p0, Ldbxyzptlk/db231222/o/c;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 340
    goto :goto_0
.end method

.method public final j()Ldbxyzptlk/db231222/H/d;
    .locals 2

    .prologue
    .line 352
    iget-object v0, p0, Ldbxyzptlk/db231222/o/c;->e:Ljava/lang/Object;

    .line 353
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 354
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ldbxyzptlk/db231222/H/d;->a(Ljava/lang/String;)Ldbxyzptlk/db231222/H/d;

    move-result-object v0

    .line 357
    iput-object v0, p0, Ldbxyzptlk/db231222/o/c;->e:Ljava/lang/Object;

    .line 360
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ldbxyzptlk/db231222/H/d;

    goto :goto_0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 375
    iget v0, p0, Ldbxyzptlk/db231222/o/c;->c:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()J
    .locals 2

    .prologue
    .line 385
    iget-wide v0, p0, Ldbxyzptlk/db231222/o/c;->f:J

    return-wide v0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 454
    invoke-super {p0}, Ldbxyzptlk/db231222/H/j;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
