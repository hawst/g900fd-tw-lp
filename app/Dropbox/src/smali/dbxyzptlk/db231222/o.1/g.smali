.class public final Ldbxyzptlk/db231222/o/g;
.super Ldbxyzptlk/db231222/H/k;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/db231222/o/h;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/H/k",
        "<",
        "Ldbxyzptlk/db231222/o/a;",
        "Ldbxyzptlk/db231222/o/g;",
        ">;",
        "Ldbxyzptlk/db231222/o/h;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldbxyzptlk/db231222/o/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1050
    invoke-direct {p0}, Ldbxyzptlk/db231222/H/k;-><init>()V

    .line 1139
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/o/g;->b:Ljava/util/List;

    .line 1051
    invoke-direct {p0}, Ldbxyzptlk/db231222/o/g;->g()V

    .line 1052
    return-void
.end method

.method static synthetic f()Ldbxyzptlk/db231222/o/g;
    .locals 1

    .prologue
    .line 1045
    invoke-static {}, Ldbxyzptlk/db231222/o/g;->k()Ldbxyzptlk/db231222/o/g;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 0

    .prologue
    .line 1055
    return-void
.end method

.method private static k()Ldbxyzptlk/db231222/o/g;
    .locals 1

    .prologue
    .line 1057
    new-instance v0, Ldbxyzptlk/db231222/o/g;

    invoke-direct {v0}, Ldbxyzptlk/db231222/o/g;-><init>()V

    return-object v0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 1142
    iget v0, p0, Ldbxyzptlk/db231222/o/g;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 1143
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Ldbxyzptlk/db231222/o/g;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Ldbxyzptlk/db231222/o/g;->b:Ljava/util/List;

    .line 1144
    iget v0, p0, Ldbxyzptlk/db231222/o/g;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldbxyzptlk/db231222/o/g;->a:I

    .line 1146
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)Ldbxyzptlk/db231222/o/c;
    .locals 1

    .prologue
    .line 1164
    iget-object v0, p0, Ldbxyzptlk/db231222/o/g;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/o/c;

    return-object v0
.end method

.method public final a()Ldbxyzptlk/db231222/o/g;
    .locals 2

    .prologue
    .line 1068
    invoke-static {}, Ldbxyzptlk/db231222/o/g;->k()Ldbxyzptlk/db231222/o/g;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/g;->c()Ldbxyzptlk/db231222/o/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/o/g;->a(Ldbxyzptlk/db231222/o/a;)Ldbxyzptlk/db231222/o/g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/o/g;
    .locals 4

    .prologue
    .line 1123
    const/4 v2, 0x0

    .line 1125
    :try_start_0
    sget-object v0, Ldbxyzptlk/db231222/o/a;->a:Ldbxyzptlk/db231222/H/w;

    invoke-interface {v0, p1, p2}, Ldbxyzptlk/db231222/H/w;->b(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/o/a;
    :try_end_0
    .catch Ldbxyzptlk/db231222/H/o; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1130
    if-eqz v0, :cond_0

    .line 1131
    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/o/g;->a(Ldbxyzptlk/db231222/o/a;)Ldbxyzptlk/db231222/o/g;

    .line 1134
    :cond_0
    return-object p0

    .line 1126
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1127
    :try_start_1
    invoke-virtual {v1}, Ldbxyzptlk/db231222/H/o;->a()Ldbxyzptlk/db231222/H/t;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/o/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1128
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1130
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 1131
    invoke-virtual {p0, v1}, Ldbxyzptlk/db231222/o/g;->a(Ldbxyzptlk/db231222/o/a;)Ldbxyzptlk/db231222/o/g;

    :cond_1
    throw v0

    .line 1130
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public final a(Ldbxyzptlk/db231222/o/a;)Ldbxyzptlk/db231222/o/g;
    .locals 2

    .prologue
    .line 1095
    invoke-static {}, Ldbxyzptlk/db231222/o/a;->a()Ldbxyzptlk/db231222/o/a;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 1106
    :cond_0
    :goto_0
    return-object p0

    .line 1096
    :cond_1
    invoke-static {p1}, Ldbxyzptlk/db231222/o/a;->a(Ldbxyzptlk/db231222/o/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1097
    iget-object v0, p0, Ldbxyzptlk/db231222/o/g;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1098
    invoke-static {p1}, Ldbxyzptlk/db231222/o/a;->a(Ldbxyzptlk/db231222/o/a;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/o/g;->b:Ljava/util/List;

    .line 1099
    iget v0, p0, Ldbxyzptlk/db231222/o/g;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Ldbxyzptlk/db231222/o/g;->a:I

    goto :goto_0

    .line 1101
    :cond_2
    invoke-direct {p0}, Ldbxyzptlk/db231222/o/g;->l()V

    .line 1102
    iget-object v0, p0, Ldbxyzptlk/db231222/o/g;->b:Ljava/util/List;

    invoke-static {p1}, Ldbxyzptlk/db231222/o/a;->a(Ldbxyzptlk/db231222/o/a;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public final a(Ldbxyzptlk/db231222/o/c;)Ldbxyzptlk/db231222/o/g;
    .locals 1

    .prologue
    .line 1193
    if-nez p1, :cond_0

    .line 1194
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1196
    :cond_0
    invoke-direct {p0}, Ldbxyzptlk/db231222/o/g;->l()V

    .line 1197
    iget-object v0, p0, Ldbxyzptlk/db231222/o/g;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1199
    return-object p0
.end method

.method public final synthetic b(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/H/b;
    .locals 1

    .prologue
    .line 1045
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/o/g;->a(Ldbxyzptlk/db231222/H/f;Ldbxyzptlk/db231222/H/i;)Ldbxyzptlk/db231222/o/g;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ldbxyzptlk/db231222/o/a;
    .locals 2

    .prologue
    .line 1076
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/g;->c()Ldbxyzptlk/db231222/o/a;

    move-result-object v0

    .line 1077
    invoke-virtual {v0}, Ldbxyzptlk/db231222/o/a;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1078
    invoke-static {v0}, Ldbxyzptlk/db231222/o/g;->a(Ldbxyzptlk/db231222/H/t;)Ldbxyzptlk/db231222/H/C;

    move-result-object v0

    throw v0

    .line 1080
    :cond_0
    return-object v0
.end method

.method public final c()Ldbxyzptlk/db231222/o/a;
    .locals 3

    .prologue
    .line 1084
    new-instance v0, Ldbxyzptlk/db231222/o/a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Ldbxyzptlk/db231222/o/a;-><init>(Ldbxyzptlk/db231222/H/k;Ldbxyzptlk/db231222/o/b;)V

    .line 1085
    iget v1, p0, Ldbxyzptlk/db231222/o/g;->a:I

    .line 1086
    iget v1, p0, Ldbxyzptlk/db231222/o/g;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1087
    iget-object v1, p0, Ldbxyzptlk/db231222/o/g;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Ldbxyzptlk/db231222/o/g;->b:Ljava/util/List;

    .line 1088
    iget v1, p0, Ldbxyzptlk/db231222/o/g;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Ldbxyzptlk/db231222/o/g;->a:I

    .line 1090
    :cond_0
    iget-object v1, p0, Ldbxyzptlk/db231222/o/g;->b:Ljava/util/List;

    invoke-static {v0, v1}, Ldbxyzptlk/db231222/o/a;->a(Ldbxyzptlk/db231222/o/a;Ljava/util/List;)Ljava/util/List;

    .line 1091
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1045
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/g;->a()Ldbxyzptlk/db231222/o/g;

    move-result-object v0

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1158
    iget-object v0, p0, Ldbxyzptlk/db231222/o/g;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1110
    move v0, v1

    :goto_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/g;->d()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1111
    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/o/g;->a(I)Ldbxyzptlk/db231222/o/c;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/db231222/o/c;->e()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1116
    :goto_1
    return v1

    .line 1110
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1116
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public final synthetic h()Ldbxyzptlk/db231222/H/k;
    .locals 1

    .prologue
    .line 1045
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/g;->a()Ldbxyzptlk/db231222/o/g;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()Ldbxyzptlk/db231222/H/b;
    .locals 1

    .prologue
    .line 1045
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/g;->a()Ldbxyzptlk/db231222/o/g;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()Ldbxyzptlk/db231222/H/t;
    .locals 1

    .prologue
    .line 1045
    invoke-virtual {p0}, Ldbxyzptlk/db231222/o/g;->c()Ldbxyzptlk/db231222/o/a;

    move-result-object v0

    return-object v0
.end method
