.class final Ldbxyzptlk/db231222/ae/F;
.super Ldbxyzptlk/db231222/ag/e;
.source "panda.py"


# static fields
.field private static final serialVersionUID:J = 0x6fb4d99c50a123ccL


# instance fields
.field final synthetic a:Ldbxyzptlk/db231222/ae/D;


# direct methods
.method constructor <init>(Ldbxyzptlk/db231222/ae/D;Ldbxyzptlk/db231222/ac/l;)V
    .locals 1

    .prologue
    .line 391
    iput-object p1, p0, Ldbxyzptlk/db231222/ae/F;->a:Ldbxyzptlk/db231222/ae/D;

    .line 392
    invoke-virtual {p2}, Ldbxyzptlk/db231222/ac/l;->a()Ldbxyzptlk/db231222/ac/m;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Ldbxyzptlk/db231222/ag/e;-><init>(Ldbxyzptlk/db231222/ac/l;Ldbxyzptlk/db231222/ac/m;)V

    .line 393
    return-void
.end method


# virtual methods
.method public final a(JI)J
    .locals 4

    .prologue
    .line 416
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/F;->a:Ldbxyzptlk/db231222/ae/D;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Ldbxyzptlk/db231222/ae/D;->a(JLjava/lang/String;)V

    .line 417
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/F;->f()Ldbxyzptlk/db231222/ac/l;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Ldbxyzptlk/db231222/ac/l;->a(JI)J

    move-result-wide v0

    .line 418
    iget-object v2, p0, Ldbxyzptlk/db231222/ae/F;->a:Ldbxyzptlk/db231222/ae/D;

    const-string v3, "resulting"

    invoke-virtual {v2, v0, v1, v3}, Ldbxyzptlk/db231222/ae/D;->a(JLjava/lang/String;)V

    .line 419
    return-wide v0
.end method

.method public final a(JJ)J
    .locals 4

    .prologue
    .line 423
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/F;->a:Ldbxyzptlk/db231222/ae/D;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Ldbxyzptlk/db231222/ae/D;->a(JLjava/lang/String;)V

    .line 424
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/F;->f()Ldbxyzptlk/db231222/ac/l;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ac/l;->a(JJ)J

    move-result-wide v0

    .line 425
    iget-object v2, p0, Ldbxyzptlk/db231222/ae/F;->a:Ldbxyzptlk/db231222/ae/D;

    const-string v3, "resulting"

    invoke-virtual {v2, v0, v1, v3}, Ldbxyzptlk/db231222/ae/D;->a(JLjava/lang/String;)V

    .line 426
    return-wide v0
.end method

.method public final b(JJ)I
    .locals 2

    .prologue
    .line 430
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/F;->a:Ldbxyzptlk/db231222/ae/D;

    const-string v1, "minuend"

    invoke-virtual {v0, p1, p2, v1}, Ldbxyzptlk/db231222/ae/D;->a(JLjava/lang/String;)V

    .line 431
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/F;->a:Ldbxyzptlk/db231222/ae/D;

    const-string v1, "subtrahend"

    invoke-virtual {v0, p3, p4, v1}, Ldbxyzptlk/db231222/ae/D;->a(JLjava/lang/String;)V

    .line 432
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/F;->f()Ldbxyzptlk/db231222/ac/l;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ac/l;->b(JJ)I

    move-result v0

    return v0
.end method

.method public final c(JJ)J
    .locals 2

    .prologue
    .line 436
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/F;->a:Ldbxyzptlk/db231222/ae/D;

    const-string v1, "minuend"

    invoke-virtual {v0, p1, p2, v1}, Ldbxyzptlk/db231222/ae/D;->a(JLjava/lang/String;)V

    .line 437
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/F;->a:Ldbxyzptlk/db231222/ae/D;

    const-string v1, "subtrahend"

    invoke-virtual {v0, p3, p4, v1}, Ldbxyzptlk/db231222/ae/D;->a(JLjava/lang/String;)V

    .line 438
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/F;->f()Ldbxyzptlk/db231222/ac/l;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ac/l;->c(JJ)J

    move-result-wide v0

    return-wide v0
.end method
