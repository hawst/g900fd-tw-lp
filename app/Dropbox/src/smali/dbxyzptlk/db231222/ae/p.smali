.class public final Ldbxyzptlk/db231222/ae/p;
.super Ldbxyzptlk/db231222/ae/a;
.source "panda.py"


# static fields
.field static final a:Ldbxyzptlk/db231222/ac/q;

.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ldbxyzptlk/db231222/ac/i;",
            "Ljava/util/ArrayList",
            "<",
            "Ldbxyzptlk/db231222/ae/p;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = -0x2353b2d19aa5d9d7L


# instance fields
.field private c:Ldbxyzptlk/db231222/ae/C;

.field private d:Ldbxyzptlk/db231222/ae/y;

.field private e:Ldbxyzptlk/db231222/ac/q;

.field private f:J

.field private g:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 106
    new-instance v0, Ldbxyzptlk/db231222/ac/q;

    const-wide v1, -0xb1d069b5400L

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/ac/q;-><init>(J)V

    sput-object v0, Ldbxyzptlk/db231222/ae/p;->a:Ldbxyzptlk/db231222/ac/q;

    .line 109
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/ae/p;->b:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ae/C;Ldbxyzptlk/db231222/ae/y;Ldbxyzptlk/db231222/ac/q;)V
    .locals 2

    .prologue
    .line 284
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 v1, 0x1

    aput-object p3, v0, v1

    const/4 v1, 0x2

    aput-object p4, v0, v1

    invoke-direct {p0, p1, v0}, Ldbxyzptlk/db231222/ae/a;-><init>(Ldbxyzptlk/db231222/ac/a;Ljava/lang/Object;)V

    .line 285
    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/ae/C;Ldbxyzptlk/db231222/ae/y;Ldbxyzptlk/db231222/ac/q;)V
    .locals 3

    .prologue
    .line 274
    const/4 v0, 0x0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v2, 0x2

    aput-object p3, v1, v2

    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/ae/a;-><init>(Ldbxyzptlk/db231222/ac/a;Ljava/lang/Object;)V

    .line 275
    return-void
.end method

.method private static a(JLdbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/a;)J
    .locals 4

    .prologue
    .line 84
    invoke-virtual {p2}, Ldbxyzptlk/db231222/ac/a;->E()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ldbxyzptlk/db231222/ac/c;->a(J)I

    move-result v0

    invoke-virtual {p2}, Ldbxyzptlk/db231222/ac/a;->C()Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ldbxyzptlk/db231222/ac/c;->a(J)I

    move-result v1

    invoke-virtual {p2}, Ldbxyzptlk/db231222/ac/a;->u()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Ldbxyzptlk/db231222/ac/c;->a(J)I

    move-result v2

    invoke-virtual {p2}, Ldbxyzptlk/db231222/ac/a;->e()Ldbxyzptlk/db231222/ac/c;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ldbxyzptlk/db231222/ac/c;->a(J)I

    move-result v3

    invoke-virtual {p3, v0, v1, v2, v3}, Ldbxyzptlk/db231222/ac/a;->a(IIII)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/ae/p;)J
    .locals 2

    .prologue
    .line 75
    iget-wide v0, p0, Ldbxyzptlk/db231222/ae/p;->g:J

    return-wide v0
.end method

.method public static a(Ldbxyzptlk/db231222/ac/i;JI)Ldbxyzptlk/db231222/ae/p;
    .locals 2

    .prologue
    .line 250
    sget-object v0, Ldbxyzptlk/db231222/ae/p;->a:Ldbxyzptlk/db231222/ac/q;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/q;->c()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 251
    const/4 v0, 0x0

    .line 255
    :goto_0
    invoke-static {p0, v0, p3}, Ldbxyzptlk/db231222/ae/p;->a(Ldbxyzptlk/db231222/ac/i;Ldbxyzptlk/db231222/ac/B;I)Ldbxyzptlk/db231222/ae/p;

    move-result-object v0

    return-object v0

    .line 253
    :cond_0
    new-instance v0, Ldbxyzptlk/db231222/ac/q;

    invoke-direct {v0, p1, p2}, Ldbxyzptlk/db231222/ac/q;-><init>(J)V

    goto :goto_0
.end method

.method public static a(Ldbxyzptlk/db231222/ac/i;Ldbxyzptlk/db231222/ac/B;)Ldbxyzptlk/db231222/ae/p;
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x4

    invoke-static {p0, p1, v0}, Ldbxyzptlk/db231222/ae/p;->a(Ldbxyzptlk/db231222/ac/i;Ldbxyzptlk/db231222/ac/B;I)Ldbxyzptlk/db231222/ae/p;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized a(Ldbxyzptlk/db231222/ac/i;Ldbxyzptlk/db231222/ac/B;I)Ldbxyzptlk/db231222/ae/p;
    .locals 8

    .prologue
    .line 190
    const-class v4, Ldbxyzptlk/db231222/ae/p;

    monitor-enter v4

    :try_start_0
    invoke-static {p0}, Ldbxyzptlk/db231222/ac/f;->a(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ac/i;

    move-result-object v5

    .line 192
    if-nez p1, :cond_1

    .line 193
    sget-object v0, Ldbxyzptlk/db231222/ae/p;->a:Ldbxyzptlk/db231222/ac/q;

    move-object v3, v0

    .line 203
    :goto_0
    sget-object v6, Ldbxyzptlk/db231222/ae/p;->b:Ljava/util/Map;

    monitor-enter v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    :try_start_1
    sget-object v0, Ldbxyzptlk/db231222/ae/p;->b:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 205
    if-nez v0, :cond_2

    .line 206
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 207
    sget-object v1, Ldbxyzptlk/db231222/ae/p;->b:Ljava/util/Map;

    invoke-interface {v1, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    :cond_0
    sget-object v1, Ldbxyzptlk/db231222/ac/i;->a:Ldbxyzptlk/db231222/ac/i;

    if-ne v5, v1, :cond_3

    .line 219
    new-instance v1, Ldbxyzptlk/db231222/ae/p;

    invoke-static {v5, p2}, Ldbxyzptlk/db231222/ae/C;->a(Ldbxyzptlk/db231222/ac/i;I)Ldbxyzptlk/db231222/ae/C;

    move-result-object v2

    invoke-static {v5, p2}, Ldbxyzptlk/db231222/ae/y;->a(Ldbxyzptlk/db231222/ac/i;I)Ldbxyzptlk/db231222/ae/y;

    move-result-object v5

    invoke-direct {v1, v2, v5, v3}, Ldbxyzptlk/db231222/ae/p;-><init>(Ldbxyzptlk/db231222/ae/C;Ldbxyzptlk/db231222/ae/y;Ldbxyzptlk/db231222/ac/q;)V

    .line 231
    :goto_1
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 232
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 233
    :goto_2
    monitor-exit v4

    return-object v1

    .line 195
    :cond_1
    :try_start_2
    invoke-interface {p1}, Ldbxyzptlk/db231222/ac/B;->b()Ldbxyzptlk/db231222/ac/q;

    move-result-object v0

    .line 196
    new-instance v1, Ldbxyzptlk/db231222/ac/r;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/q;->c()J

    move-result-wide v2

    invoke-static {v5}, Ldbxyzptlk/db231222/ae/y;->b(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ae/y;

    move-result-object v6

    invoke-direct {v1, v2, v3, v6}, Ldbxyzptlk/db231222/ac/r;-><init>(JLdbxyzptlk/db231222/ac/a;)V

    .line 197
    invoke-virtual {v1}, Ldbxyzptlk/db231222/ac/r;->d()I

    move-result v1

    if-gtz v1, :cond_5

    .line 198
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cutover too early. Must be on or after 0001-01-01."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 190
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    .line 209
    :cond_2
    :try_start_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_3
    add-int/lit8 v2, v1, -0x1

    if-ltz v2, :cond_0

    .line 210
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldbxyzptlk/db231222/ae/p;

    .line 211
    invoke-virtual {v1}, Ldbxyzptlk/db231222/ae/p;->O()I

    move-result v7

    if-ne p2, v7, :cond_4

    invoke-virtual {v1}, Ldbxyzptlk/db231222/ae/p;->N()Ldbxyzptlk/db231222/ac/q;

    move-result-object v7

    invoke-virtual {v3, v7}, Ldbxyzptlk/db231222/ac/q;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 214
    monitor-exit v6

    goto :goto_2

    .line 232
    :catchall_1
    move-exception v0

    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 224
    :cond_3
    :try_start_5
    sget-object v1, Ldbxyzptlk/db231222/ac/i;->a:Ldbxyzptlk/db231222/ac/i;

    invoke-static {v1, v3, p2}, Ldbxyzptlk/db231222/ae/p;->a(Ldbxyzptlk/db231222/ac/i;Ldbxyzptlk/db231222/ac/B;I)Ldbxyzptlk/db231222/ae/p;

    move-result-object v2

    .line 225
    new-instance v1, Ldbxyzptlk/db231222/ae/p;

    invoke-static {v2, v5}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ae/H;

    move-result-object v3

    iget-object v5, v2, Ldbxyzptlk/db231222/ae/p;->c:Ldbxyzptlk/db231222/ae/C;

    iget-object v7, v2, Ldbxyzptlk/db231222/ae/p;->d:Ldbxyzptlk/db231222/ae/y;

    iget-object v2, v2, Ldbxyzptlk/db231222/ae/p;->e:Ldbxyzptlk/db231222/ac/q;

    invoke-direct {v1, v3, v5, v7, v2}, Ldbxyzptlk/db231222/ae/p;-><init>(Ldbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ae/C;Ldbxyzptlk/db231222/ae/y;Ldbxyzptlk/db231222/ac/q;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_3

    :cond_5
    move-object v3, v0

    goto/16 :goto_0
.end method

.method private static b(JLdbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/a;)J
    .locals 4

    .prologue
    .line 96
    invoke-virtual {p3}, Ldbxyzptlk/db231222/ac/a;->z()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {p2}, Ldbxyzptlk/db231222/ac/a;->z()Ldbxyzptlk/db231222/ac/c;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ldbxyzptlk/db231222/ac/c;->a(J)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Ldbxyzptlk/db231222/ac/c;->b(JI)J

    move-result-wide v0

    .line 97
    invoke-virtual {p3}, Ldbxyzptlk/db231222/ac/a;->x()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    invoke-virtual {p2}, Ldbxyzptlk/db231222/ac/a;->x()Ldbxyzptlk/db231222/ac/c;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ldbxyzptlk/db231222/ac/c;->a(J)I

    move-result v3

    invoke-virtual {v2, v0, v1, v3}, Ldbxyzptlk/db231222/ac/c;->b(JI)J

    move-result-wide v0

    .line 98
    invoke-virtual {p3}, Ldbxyzptlk/db231222/ac/a;->t()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    invoke-virtual {p2}, Ldbxyzptlk/db231222/ac/a;->t()Ldbxyzptlk/db231222/ac/c;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ldbxyzptlk/db231222/ac/c;->a(J)I

    move-result v3

    invoke-virtual {v2, v0, v1, v3}, Ldbxyzptlk/db231222/ac/c;->b(JI)J

    move-result-wide v0

    .line 99
    invoke-virtual {p3}, Ldbxyzptlk/db231222/ac/a;->e()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    invoke-virtual {p2}, Ldbxyzptlk/db231222/ac/a;->e()Ldbxyzptlk/db231222/ac/c;

    move-result-object v3

    invoke-virtual {v3, p0, p1}, Ldbxyzptlk/db231222/ac/c;->a(J)I

    move-result v3

    invoke-virtual {v2, v0, v1, v3}, Ldbxyzptlk/db231222/ac/c;->b(JI)J

    move-result-wide v0

    .line 100
    return-wide v0
.end method

.method static synthetic b(Ldbxyzptlk/db231222/ae/p;)Ldbxyzptlk/db231222/ae/y;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/p;->d:Ldbxyzptlk/db231222/ae/y;

    return-object v0
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 291
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/p;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/ae/p;->e:Ldbxyzptlk/db231222/ac/q;

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/p;->O()I

    move-result v2

    invoke-static {v0, v1, v2}, Ldbxyzptlk/db231222/ae/p;->a(Ldbxyzptlk/db231222/ac/i;Ldbxyzptlk/db231222/ac/B;I)Ldbxyzptlk/db231222/ae/p;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final N()Ldbxyzptlk/db231222/ac/q;
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/p;->e:Ldbxyzptlk/db231222/ac/q;

    return-object v0
.end method

.method public final O()I
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/p;->d:Ldbxyzptlk/db231222/ae/y;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ae/y;->N()I

    move-result v0

    return v0
.end method

.method public final a(IIII)J
    .locals 4

    .prologue
    .line 334
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/p;->L()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 335
    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ac/a;->a(IIII)J

    move-result-wide v0

    .line 350
    :cond_0
    return-wide v0

    .line 339
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/p;->d:Ldbxyzptlk/db231222/ae/y;

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ae/y;->a(IIII)J

    move-result-wide v0

    .line 341
    iget-wide v2, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 343
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/p;->c:Ldbxyzptlk/db231222/ae/C;

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ae/C;->a(IIII)J

    move-result-wide v0

    .line 345
    iget-wide v2, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    .line 347
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Specified date does not exist"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(IIIIIII)J
    .locals 9

    .prologue
    .line 359
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/p;->L()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    if-eqz v0, :cond_1

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    .line 360
    invoke-virtual/range {v0 .. v7}, Ldbxyzptlk/db231222/ac/a;->a(IIIIIII)J

    move-result-wide v0

    .line 392
    :cond_0
    return-wide v0

    .line 368
    :cond_1
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/p;->d:Ldbxyzptlk/db231222/ae/y;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-virtual/range {v0 .. v7}, Ldbxyzptlk/db231222/ae/y;->a(IIIIIII)J
    :try_end_0
    .catch Ldbxyzptlk/db231222/ac/o; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 382
    :cond_2
    iget-wide v2, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 384
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/p;->c:Ldbxyzptlk/db231222/ae/C;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-virtual/range {v0 .. v7}, Ldbxyzptlk/db231222/ae/C;->a(IIIIIII)J

    move-result-wide v0

    .line 387
    iget-wide v2, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    .line 389
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Specified date does not exist"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 371
    :catch_0
    move-exception v0

    move-object v8, v0

    .line 372
    const/4 v0, 0x2

    if-ne p2, v0, :cond_3

    const/16 v0, 0x1d

    if-eq p3, v0, :cond_4

    .line 373
    :cond_3
    throw v8

    .line 375
    :cond_4
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/p;->d:Ldbxyzptlk/db231222/ae/y;

    const/16 v3, 0x1c

    move v1, p1

    move v2, p2

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-virtual/range {v0 .. v7}, Ldbxyzptlk/db231222/ae/y;->a(IIIIIII)J

    move-result-wide v0

    .line 378
    iget-wide v2, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    cmp-long v2, v0, v2

    if-ltz v2, :cond_2

    .line 379
    throw v8
.end method

.method final a(J)J
    .locals 2

    .prologue
    .line 588
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/p;->c:Ldbxyzptlk/db231222/ae/C;

    iget-object v1, p0, Ldbxyzptlk/db231222/ae/p;->d:Ldbxyzptlk/db231222/ae/y;

    invoke-static {p1, p2, v0, v1}, Ldbxyzptlk/db231222/ae/p;->a(JLdbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/a;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ac/a;
    .locals 2

    .prologue
    .line 320
    if-nez p1, :cond_0

    .line 321
    invoke-static {}, Ldbxyzptlk/db231222/ac/i;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object p1

    .line 323
    :cond_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/p;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 326
    :goto_0
    return-object p0

    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/p;->e:Ldbxyzptlk/db231222/ac/q;

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/p;->O()I

    move-result v1

    invoke-static {p1, v0, v1}, Ldbxyzptlk/db231222/ae/p;->a(Ldbxyzptlk/db231222/ac/i;Ldbxyzptlk/db231222/ac/B;I)Ldbxyzptlk/db231222/ae/p;

    move-result-object p0

    goto :goto_0
.end method

.method public final a()Ldbxyzptlk/db231222/ac/i;
    .locals 1

    .prologue
    .line 296
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/p;->L()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 297
    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/a;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v0

    .line 299
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ldbxyzptlk/db231222/ac/i;->a:Ldbxyzptlk/db231222/ac/i;

    goto :goto_0
.end method

.method protected final a(Ldbxyzptlk/db231222/ae/b;)V
    .locals 9

    .prologue
    .line 478
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/p;->M()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 480
    const/4 v1, 0x0

    aget-object v1, v0, v1

    move-object v8, v1

    check-cast v8, Ldbxyzptlk/db231222/ae/C;

    .line 481
    const/4 v1, 0x1

    aget-object v1, v0, v1

    move-object v6, v1

    check-cast v6, Ldbxyzptlk/db231222/ae/y;

    .line 482
    const/4 v1, 0x2

    aget-object v0, v0, v1

    check-cast v0, Ldbxyzptlk/db231222/ac/q;

    .line 483
    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/q;->c()J

    move-result-wide v1

    iput-wide v1, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    .line 485
    iput-object v8, p0, Ldbxyzptlk/db231222/ae/p;->c:Ldbxyzptlk/db231222/ae/C;

    .line 486
    iput-object v6, p0, Ldbxyzptlk/db231222/ae/p;->d:Ldbxyzptlk/db231222/ae/y;

    .line 487
    iput-object v0, p0, Ldbxyzptlk/db231222/ae/p;->e:Ldbxyzptlk/db231222/ac/q;

    .line 489
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/p;->L()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 585
    :goto_0
    return-void

    .line 493
    :cond_0
    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->N()I

    move-result v0

    invoke-virtual {v6}, Ldbxyzptlk/db231222/ae/y;->N()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 494
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 498
    :cond_1
    iget-wide v0, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    iget-wide v2, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    invoke-virtual {p0, v2, v3}, Ldbxyzptlk/db231222/ae/p;->a(J)J

    move-result-wide v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, Ldbxyzptlk/db231222/ae/p;->g:J

    .line 504
    invoke-virtual {p1, v6}, Ldbxyzptlk/db231222/ae/b;->a(Ldbxyzptlk/db231222/ac/a;)V

    .line 510
    invoke-virtual {v6}, Ldbxyzptlk/db231222/ae/y;->e()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    iget-wide v1, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/ac/c;->a(J)I

    move-result v0

    if-nez v0, :cond_2

    .line 514
    new-instance v0, Ldbxyzptlk/db231222/ae/q;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->d()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    iget-object v3, p1, Ldbxyzptlk/db231222/ae/b;->m:Ldbxyzptlk/db231222/ac/c;

    iget-wide v4, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/ae/q;-><init>(Ldbxyzptlk/db231222/ae/p;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/c;J)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->m:Ldbxyzptlk/db231222/ac/c;

    .line 515
    new-instance v0, Ldbxyzptlk/db231222/ae/q;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->e()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    iget-object v3, p1, Ldbxyzptlk/db231222/ae/b;->n:Ldbxyzptlk/db231222/ac/c;

    iget-wide v4, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/ae/q;-><init>(Ldbxyzptlk/db231222/ae/p;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/c;J)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->n:Ldbxyzptlk/db231222/ac/c;

    .line 516
    new-instance v0, Ldbxyzptlk/db231222/ae/q;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->g()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    iget-object v3, p1, Ldbxyzptlk/db231222/ae/b;->o:Ldbxyzptlk/db231222/ac/c;

    iget-wide v4, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/ae/q;-><init>(Ldbxyzptlk/db231222/ae/p;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/c;J)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->o:Ldbxyzptlk/db231222/ac/c;

    .line 517
    new-instance v0, Ldbxyzptlk/db231222/ae/q;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->h()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    iget-object v3, p1, Ldbxyzptlk/db231222/ae/b;->p:Ldbxyzptlk/db231222/ac/c;

    iget-wide v4, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/ae/q;-><init>(Ldbxyzptlk/db231222/ae/p;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/c;J)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->p:Ldbxyzptlk/db231222/ac/c;

    .line 518
    new-instance v0, Ldbxyzptlk/db231222/ae/q;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->j()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    iget-object v3, p1, Ldbxyzptlk/db231222/ae/b;->q:Ldbxyzptlk/db231222/ac/c;

    iget-wide v4, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/ae/q;-><init>(Ldbxyzptlk/db231222/ae/p;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/c;J)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->q:Ldbxyzptlk/db231222/ac/c;

    .line 519
    new-instance v0, Ldbxyzptlk/db231222/ae/q;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->k()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    iget-object v3, p1, Ldbxyzptlk/db231222/ae/b;->r:Ldbxyzptlk/db231222/ac/c;

    iget-wide v4, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/ae/q;-><init>(Ldbxyzptlk/db231222/ae/p;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/c;J)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->r:Ldbxyzptlk/db231222/ac/c;

    .line 520
    new-instance v0, Ldbxyzptlk/db231222/ae/q;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->m()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    iget-object v3, p1, Ldbxyzptlk/db231222/ae/b;->s:Ldbxyzptlk/db231222/ac/c;

    iget-wide v4, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/ae/q;-><init>(Ldbxyzptlk/db231222/ae/p;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/c;J)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->s:Ldbxyzptlk/db231222/ac/c;

    .line 521
    new-instance v0, Ldbxyzptlk/db231222/ae/q;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->p()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    iget-object v3, p1, Ldbxyzptlk/db231222/ae/b;->u:Ldbxyzptlk/db231222/ac/c;

    iget-wide v4, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/ae/q;-><init>(Ldbxyzptlk/db231222/ae/p;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/c;J)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->u:Ldbxyzptlk/db231222/ac/c;

    .line 522
    new-instance v0, Ldbxyzptlk/db231222/ae/q;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->n()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    iget-object v3, p1, Ldbxyzptlk/db231222/ae/b;->t:Ldbxyzptlk/db231222/ac/c;

    iget-wide v4, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/ae/q;-><init>(Ldbxyzptlk/db231222/ae/p;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/c;J)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->t:Ldbxyzptlk/db231222/ac/c;

    .line 523
    new-instance v0, Ldbxyzptlk/db231222/ae/q;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->q()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    iget-object v3, p1, Ldbxyzptlk/db231222/ae/b;->v:Ldbxyzptlk/db231222/ac/c;

    iget-wide v4, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/ae/q;-><init>(Ldbxyzptlk/db231222/ae/p;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/c;J)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->v:Ldbxyzptlk/db231222/ac/c;

    .line 525
    new-instance v0, Ldbxyzptlk/db231222/ae/q;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->r()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    iget-object v3, p1, Ldbxyzptlk/db231222/ae/b;->w:Ldbxyzptlk/db231222/ac/c;

    iget-wide v4, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/ae/q;-><init>(Ldbxyzptlk/db231222/ae/p;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/c;J)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->w:Ldbxyzptlk/db231222/ac/c;

    .line 530
    :cond_2
    new-instance v0, Ldbxyzptlk/db231222/ae/q;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->K()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    iget-object v3, p1, Ldbxyzptlk/db231222/ae/b;->I:Ldbxyzptlk/db231222/ac/c;

    iget-wide v4, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/ae/q;-><init>(Ldbxyzptlk/db231222/ae/p;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/c;J)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->I:Ldbxyzptlk/db231222/ac/c;

    .line 539
    invoke-virtual {v6}, Ldbxyzptlk/db231222/ae/y;->E()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    iget-wide v1, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/ac/c;->e(J)J

    move-result-wide v4

    .line 540
    new-instance v0, Ldbxyzptlk/db231222/ae/q;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->v()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    iget-object v3, p1, Ldbxyzptlk/db231222/ae/b;->z:Ldbxyzptlk/db231222/ac/c;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/ae/q;-><init>(Ldbxyzptlk/db231222/ae/p;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/c;J)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->z:Ldbxyzptlk/db231222/ac/c;

    .line 545
    invoke-virtual {v6}, Ldbxyzptlk/db231222/ae/y;->z()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    iget-wide v1, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/ac/c;->e(J)J

    move-result-wide v4

    .line 546
    new-instance v0, Ldbxyzptlk/db231222/ae/q;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->x()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    iget-object v3, p1, Ldbxyzptlk/db231222/ae/b;->A:Ldbxyzptlk/db231222/ac/c;

    const/4 v6, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/ae/q;-><init>(Ldbxyzptlk/db231222/ae/p;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/c;JZ)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->A:Ldbxyzptlk/db231222/ac/c;

    .line 554
    new-instance v0, Ldbxyzptlk/db231222/ae/r;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->E()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    iget-object v3, p1, Ldbxyzptlk/db231222/ae/b;->E:Ldbxyzptlk/db231222/ac/c;

    iget-wide v4, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/ae/r;-><init>(Ldbxyzptlk/db231222/ae/p;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/c;J)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->E:Ldbxyzptlk/db231222/ac/c;

    .line 556
    iget-object v0, p1, Ldbxyzptlk/db231222/ae/b;->E:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/c;->d()Ldbxyzptlk/db231222/ac/l;

    move-result-object v0

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->j:Ldbxyzptlk/db231222/ac/l;

    .line 557
    new-instance v0, Ldbxyzptlk/db231222/ae/r;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->F()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    iget-object v3, p1, Ldbxyzptlk/db231222/ae/b;->F:Ldbxyzptlk/db231222/ac/c;

    iget-object v4, p1, Ldbxyzptlk/db231222/ae/b;->j:Ldbxyzptlk/db231222/ac/l;

    iget-wide v5, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/ae/r;-><init>(Ldbxyzptlk/db231222/ae/p;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/l;J)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->F:Ldbxyzptlk/db231222/ac/c;

    .line 559
    new-instance v0, Ldbxyzptlk/db231222/ae/r;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->G()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    iget-object v3, p1, Ldbxyzptlk/db231222/ae/b;->G:Ldbxyzptlk/db231222/ac/c;

    iget-object v4, p1, Ldbxyzptlk/db231222/ae/b;->j:Ldbxyzptlk/db231222/ac/l;

    iget-wide v5, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/ae/r;-><init>(Ldbxyzptlk/db231222/ae/p;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/l;J)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->G:Ldbxyzptlk/db231222/ac/c;

    .line 562
    new-instance v0, Ldbxyzptlk/db231222/ae/r;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->I()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    iget-object v3, p1, Ldbxyzptlk/db231222/ae/b;->H:Ldbxyzptlk/db231222/ac/c;

    iget-wide v4, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/ae/r;-><init>(Ldbxyzptlk/db231222/ae/p;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/c;J)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->H:Ldbxyzptlk/db231222/ac/c;

    .line 564
    iget-object v0, p1, Ldbxyzptlk/db231222/ae/b;->H:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/c;->d()Ldbxyzptlk/db231222/ac/l;

    move-result-object v0

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->k:Ldbxyzptlk/db231222/ac/l;

    .line 566
    new-instance v0, Ldbxyzptlk/db231222/ae/r;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->C()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    iget-object v3, p1, Ldbxyzptlk/db231222/ae/b;->D:Ldbxyzptlk/db231222/ac/c;

    iget-wide v4, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/ae/r;-><init>(Ldbxyzptlk/db231222/ae/p;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/c;J)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->D:Ldbxyzptlk/db231222/ac/c;

    .line 568
    iget-object v0, p1, Ldbxyzptlk/db231222/ae/b;->D:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/c;->d()Ldbxyzptlk/db231222/ac/l;

    move-result-object v0

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->i:Ldbxyzptlk/db231222/ac/l;

    .line 570
    new-instance v0, Ldbxyzptlk/db231222/ae/r;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->z()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    iget-object v3, p1, Ldbxyzptlk/db231222/ae/b;->B:Ldbxyzptlk/db231222/ac/c;

    const/4 v4, 0x0

    iget-wide v5, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    const/4 v7, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Ldbxyzptlk/db231222/ae/r;-><init>(Ldbxyzptlk/db231222/ae/p;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/l;JZ)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->B:Ldbxyzptlk/db231222/ac/c;

    .line 572
    new-instance v0, Ldbxyzptlk/db231222/ae/r;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->A()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    iget-object v3, p1, Ldbxyzptlk/db231222/ae/b;->C:Ldbxyzptlk/db231222/ac/c;

    iget-object v4, p1, Ldbxyzptlk/db231222/ae/b;->h:Ldbxyzptlk/db231222/ac/l;

    iget-wide v5, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/ae/r;-><init>(Ldbxyzptlk/db231222/ae/p;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/l;J)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->C:Ldbxyzptlk/db231222/ac/c;

    .line 574
    iget-object v0, p1, Ldbxyzptlk/db231222/ae/b;->B:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/c;->d()Ldbxyzptlk/db231222/ac/l;

    move-result-object v0

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->h:Ldbxyzptlk/db231222/ac/l;

    .line 580
    new-instance v0, Ldbxyzptlk/db231222/ae/q;

    invoke-virtual {v8}, Ldbxyzptlk/db231222/ae/C;->u()Ldbxyzptlk/db231222/ac/c;

    move-result-object v2

    iget-object v3, p1, Ldbxyzptlk/db231222/ae/b;->y:Ldbxyzptlk/db231222/ac/c;

    iget-wide v4, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/ae/q;-><init>(Ldbxyzptlk/db231222/ae/p;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/c;J)V

    .line 582
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->i:Ldbxyzptlk/db231222/ac/l;

    iput-object v1, v0, Ldbxyzptlk/db231222/ae/q;->f:Ldbxyzptlk/db231222/ac/l;

    .line 583
    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->y:Ldbxyzptlk/db231222/ac/c;

    goto/16 :goto_0
.end method

.method final b(J)J
    .locals 2

    .prologue
    .line 592
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/p;->d:Ldbxyzptlk/db231222/ae/y;

    iget-object v1, p0, Ldbxyzptlk/db231222/ae/p;->c:Ldbxyzptlk/db231222/ae/C;

    invoke-static {p1, p2, v0, v1}, Ldbxyzptlk/db231222/ae/p;->a(JLdbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/a;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final b()Ldbxyzptlk/db231222/ac/a;
    .locals 1

    .prologue
    .line 310
    sget-object v0, Ldbxyzptlk/db231222/ac/i;->a:Ldbxyzptlk/db231222/ac/i;

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/ae/p;->a(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    return-object v0
.end method

.method final c(J)J
    .locals 2

    .prologue
    .line 596
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/p;->c:Ldbxyzptlk/db231222/ae/C;

    iget-object v1, p0, Ldbxyzptlk/db231222/ae/p;->d:Ldbxyzptlk/db231222/ae/y;

    invoke-static {p1, p2, v0, v1}, Ldbxyzptlk/db231222/ae/p;->b(JLdbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/a;)J

    move-result-wide v0

    return-wide v0
.end method

.method final d(J)J
    .locals 2

    .prologue
    .line 600
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/p;->d:Ldbxyzptlk/db231222/ae/y;

    iget-object v1, p0, Ldbxyzptlk/db231222/ae/p;->c:Ldbxyzptlk/db231222/ae/C;

    invoke-static {p1, p2, v0, v1}, Ldbxyzptlk/db231222/ae/p;->b(JLdbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/a;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 421
    if-ne p0, p1, :cond_1

    .line 430
    :cond_0
    :goto_0
    return v0

    .line 424
    :cond_1
    instance-of v2, p1, Ldbxyzptlk/db231222/ae/p;

    if-eqz v2, :cond_3

    .line 425
    check-cast p1, Ldbxyzptlk/db231222/ae/p;

    .line 426
    iget-wide v2, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    iget-wide v4, p1, Ldbxyzptlk/db231222/ae/p;->f:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/p;->O()I

    move-result v2

    invoke-virtual {p1}, Ldbxyzptlk/db231222/ae/p;->O()I

    move-result v3

    if-ne v2, v3, :cond_2

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/p;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v2

    invoke-virtual {p1}, Ldbxyzptlk/db231222/ae/p;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v3

    invoke-virtual {v2, v3}, Ldbxyzptlk/db231222/ac/i;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 430
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 440
    const-string v0, "GJ"

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0xb

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/p;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/ac/i;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/p;->O()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Ldbxyzptlk/db231222/ae/p;->e:Ldbxyzptlk/db231222/ac/q;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/ac/q;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 452
    new-instance v1, Ljava/lang/StringBuffer;

    const/16 v0, 0x3c

    invoke-direct {v1, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 453
    const-string v0, "GJChronology"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 454
    const/16 v0, 0x5b

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 455
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/p;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/i;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 457
    iget-wide v2, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    sget-object v0, Ldbxyzptlk/db231222/ae/p;->a:Ldbxyzptlk/db231222/ac/q;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/q;->c()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 458
    const-string v0, ",cutover="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 460
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/p;->b()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/a;->v()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    iget-wide v2, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    invoke-virtual {v0, v2, v3}, Ldbxyzptlk/db231222/ac/c;->i(J)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    .line 461
    invoke-static {}, Ldbxyzptlk/db231222/ah/y;->b()Ldbxyzptlk/db231222/ah/c;

    move-result-object v0

    .line 465
    :goto_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/p;->b()Ldbxyzptlk/db231222/ac/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Ldbxyzptlk/db231222/ah/c;->a(Ldbxyzptlk/db231222/ac/a;)Ldbxyzptlk/db231222/ah/c;

    move-result-object v0

    iget-wide v2, p0, Ldbxyzptlk/db231222/ae/p;->f:J

    invoke-virtual {v0, v1, v2, v3}, Ldbxyzptlk/db231222/ah/c;->a(Ljava/lang/StringBuffer;J)V

    .line 468
    :cond_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/p;->O()I

    move-result v0

    const/4 v2, 0x4

    if-eq v0, v2, :cond_1

    .line 469
    const-string v0, ",mdfw="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 470
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/p;->O()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 472
    :cond_1
    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 474
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 463
    :cond_2
    invoke-static {}, Ldbxyzptlk/db231222/ah/y;->c()Ldbxyzptlk/db231222/ah/c;

    move-result-object v0

    goto :goto_0
.end method
