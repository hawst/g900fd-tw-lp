.class public final Ldbxyzptlk/db231222/ae/H;
.super Ldbxyzptlk/db231222/ae/a;
.source "panda.py"


# static fields
.field private static final serialVersionUID:J = -0xefa4c340f86ef80L


# direct methods
.method private constructor <init>(Ldbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/i;)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/ae/a;-><init>(Ldbxyzptlk/db231222/ac/a;Ljava/lang/Object;)V

    .line 82
    return-void
.end method

.method private a(J)J
    .locals 5

    .prologue
    .line 138
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/H;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v0

    .line 139
    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/i;->c(J)I

    move-result v1

    .line 140
    int-to-long v2, v1

    sub-long v2, p1, v2

    .line 141
    invoke-virtual {v0, v2, v3}, Ldbxyzptlk/db231222/ac/i;->b(J)I

    move-result v4

    if-eq v1, v4, :cond_0

    .line 142
    new-instance v1, Ldbxyzptlk/db231222/ac/p;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/i;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Ldbxyzptlk/db231222/ac/p;-><init>(JLjava/lang/String;)V

    throw v1

    .line 144
    :cond_0
    return-wide v2
.end method

.method private a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/ac/c;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)",
            "Ldbxyzptlk/db231222/ac/c;"
        }
    .end annotation

    .prologue
    .line 209
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ldbxyzptlk/db231222/ac/c;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, p1

    .line 221
    :goto_0
    return-object v0

    .line 212
    :cond_1
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 213
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/ac/c;

    goto :goto_0

    .line 215
    :cond_2
    new-instance v0, Ldbxyzptlk/db231222/ae/I;

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/H;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v2

    invoke-virtual {p1}, Ldbxyzptlk/db231222/ac/c;->d()Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v3

    invoke-virtual {p1}, Ldbxyzptlk/db231222/ac/c;->e()Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v4

    invoke-virtual {p1}, Ldbxyzptlk/db231222/ac/c;->f()Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v5

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/ae/I;-><init>(Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/i;Ldbxyzptlk/db231222/ac/l;Ldbxyzptlk/db231222/ac/l;Ldbxyzptlk/db231222/ac/l;)V

    .line 220
    invoke-virtual {p2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/ac/l;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)",
            "Ldbxyzptlk/db231222/ac/l;"
        }
    .end annotation

    .prologue
    .line 197
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ldbxyzptlk/db231222/ac/l;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, p1

    .line 205
    :goto_0
    return-object v0

    .line 200
    :cond_1
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 201
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/ac/l;

    goto :goto_0

    .line 203
    :cond_2
    new-instance v0, Ldbxyzptlk/db231222/ae/J;

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/H;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Ldbxyzptlk/db231222/ae/J;-><init>(Ldbxyzptlk/db231222/ac/l;Ldbxyzptlk/db231222/ac/i;)V

    .line 204
    invoke-virtual {p2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static a(Ldbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ae/H;
    .locals 2

    .prologue
    .line 55
    if-nez p0, :cond_0

    .line 56
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must supply a chronology"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/a;->b()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    .line 59
    if-nez v0, :cond_1

    .line 60
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "UTC chronology must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_1
    if-nez p1, :cond_2

    .line 63
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "DateTimeZone must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_2
    new-instance v1, Ldbxyzptlk/db231222/ae/H;

    invoke-direct {v1, v0, p1}, Ldbxyzptlk/db231222/ae/H;-><init>(Ldbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/i;)V

    return-object v1
.end method

.method static a(Ldbxyzptlk/db231222/ac/l;)Z
    .locals 4

    .prologue
    .line 71
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ac/l;->d()J

    move-result-wide v0

    const-wide/32 v2, 0x2932e00

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(IIII)J
    .locals 2

    .prologue
    .line 109
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/H;->L()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ac/a;->a(IIII)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/ae/H;->a(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(IIIIIII)J
    .locals 8

    .prologue
    .line 118
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/H;->L()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Ldbxyzptlk/db231222/ac/a;->a(IIIIIII)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/ae/H;->a(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ac/a;
    .locals 2

    .prologue
    .line 93
    if-nez p1, :cond_0

    .line 94
    invoke-static {}, Ldbxyzptlk/db231222/ac/i;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object p1

    .line 96
    :cond_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/H;->M()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 102
    :goto_0
    return-object p0

    .line 99
    :cond_1
    sget-object v0, Ldbxyzptlk/db231222/ac/i;->a:Ldbxyzptlk/db231222/ac/i;

    if-ne p1, v0, :cond_2

    .line 100
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/H;->L()Ldbxyzptlk/db231222/ac/a;

    move-result-object p0

    goto :goto_0

    .line 102
    :cond_2
    new-instance v0, Ldbxyzptlk/db231222/ae/H;

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/H;->L()Ldbxyzptlk/db231222/ac/a;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ldbxyzptlk/db231222/ae/H;-><init>(Ldbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/i;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Ldbxyzptlk/db231222/ac/i;
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/H;->M()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/ac/i;

    return-object v0
.end method

.method protected final a(Ldbxyzptlk/db231222/ae/b;)V
    .locals 2

    .prologue
    .line 150
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 154
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->l:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->l:Ldbxyzptlk/db231222/ac/l;

    .line 155
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->k:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->k:Ldbxyzptlk/db231222/ac/l;

    .line 156
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->j:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->j:Ldbxyzptlk/db231222/ac/l;

    .line 157
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->i:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->i:Ldbxyzptlk/db231222/ac/l;

    .line 158
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->h:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->h:Ldbxyzptlk/db231222/ac/l;

    .line 159
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->g:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->g:Ldbxyzptlk/db231222/ac/l;

    .line 160
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->f:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->f:Ldbxyzptlk/db231222/ac/l;

    .line 162
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->e:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->e:Ldbxyzptlk/db231222/ac/l;

    .line 163
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->d:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->d:Ldbxyzptlk/db231222/ac/l;

    .line 164
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->c:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->c:Ldbxyzptlk/db231222/ac/l;

    .line 165
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->b:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->b:Ldbxyzptlk/db231222/ac/l;

    .line 166
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->a:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->a:Ldbxyzptlk/db231222/ac/l;

    .line 170
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->E:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->E:Ldbxyzptlk/db231222/ac/c;

    .line 171
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->F:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->F:Ldbxyzptlk/db231222/ac/c;

    .line 172
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->G:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->G:Ldbxyzptlk/db231222/ac/c;

    .line 173
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->H:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->H:Ldbxyzptlk/db231222/ac/c;

    .line 174
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->I:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->I:Ldbxyzptlk/db231222/ac/c;

    .line 175
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->x:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->x:Ldbxyzptlk/db231222/ac/c;

    .line 176
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->y:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->y:Ldbxyzptlk/db231222/ac/c;

    .line 177
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->z:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->z:Ldbxyzptlk/db231222/ac/c;

    .line 178
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->D:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->D:Ldbxyzptlk/db231222/ac/c;

    .line 179
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->A:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->A:Ldbxyzptlk/db231222/ac/c;

    .line 180
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->B:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->B:Ldbxyzptlk/db231222/ac/c;

    .line 181
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->C:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->C:Ldbxyzptlk/db231222/ac/c;

    .line 183
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->m:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->m:Ldbxyzptlk/db231222/ac/c;

    .line 184
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->n:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->n:Ldbxyzptlk/db231222/ac/c;

    .line 185
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->o:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->o:Ldbxyzptlk/db231222/ac/c;

    .line 186
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->p:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->p:Ldbxyzptlk/db231222/ac/c;

    .line 187
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->q:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->q:Ldbxyzptlk/db231222/ac/c;

    .line 188
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->r:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->r:Ldbxyzptlk/db231222/ac/c;

    .line 189
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->s:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->s:Ldbxyzptlk/db231222/ac/c;

    .line 190
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->u:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->u:Ldbxyzptlk/db231222/ac/c;

    .line 191
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->t:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->t:Ldbxyzptlk/db231222/ac/c;

    .line 192
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->v:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->v:Ldbxyzptlk/db231222/ac/c;

    .line 193
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->w:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->w:Ldbxyzptlk/db231222/ac/c;

    .line 194
    return-void
.end method

.method public final b()Ldbxyzptlk/db231222/ac/a;
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/H;->L()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 234
    if-ne p0, p1, :cond_1

    .line 241
    :cond_0
    :goto_0
    return v0

    .line 237
    :cond_1
    instance-of v2, p1, Ldbxyzptlk/db231222/ae/H;

    if-nez v2, :cond_2

    move v0, v1

    .line 238
    goto :goto_0

    .line 240
    :cond_2
    check-cast p1, Ldbxyzptlk/db231222/ae/H;

    .line 241
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/H;->L()Ldbxyzptlk/db231222/ac/a;

    move-result-object v2

    invoke-virtual {p1}, Ldbxyzptlk/db231222/ae/H;->L()Ldbxyzptlk/db231222/ac/a;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/H;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v2

    invoke-virtual {p1}, Ldbxyzptlk/db231222/ae/H;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v3

    invoke-virtual {v2, v3}, Ldbxyzptlk/db231222/ac/i;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 253
    const v0, 0x4fba5

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/H;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/ac/i;->hashCode()I

    move-result v1

    mul-int/lit8 v1, v1, 0xb

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/H;->L()Ldbxyzptlk/db231222/ac/a;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    mul-int/lit8 v1, v1, 0x7

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 262
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ZonedChronology["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/H;->L()Ldbxyzptlk/db231222/ac/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/H;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/ac/i;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
