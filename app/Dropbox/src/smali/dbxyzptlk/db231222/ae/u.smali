.class final Ldbxyzptlk/db231222/ae/u;
.super Ldbxyzptlk/db231222/ag/b;
.source "panda.py"


# instance fields
.field private final a:Ldbxyzptlk/db231222/ae/d;


# direct methods
.method constructor <init>(Ldbxyzptlk/db231222/ae/d;)V
    .locals 1

    .prologue
    .line 47
    invoke-static {}, Ldbxyzptlk/db231222/ac/d;->w()Ldbxyzptlk/db231222/ac/d;

    move-result-object v0

    invoke-direct {p0, v0}, Ldbxyzptlk/db231222/ag/b;-><init>(Ldbxyzptlk/db231222/ac/d;)V

    .line 48
    iput-object p1, p0, Ldbxyzptlk/db231222/ae/u;->a:Ldbxyzptlk/db231222/ae/d;

    .line 49
    return-void
.end method


# virtual methods
.method public final a(J)I
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/u;->a:Ldbxyzptlk/db231222/ae/d;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ae/d;->a(J)I

    move-result v0

    if-gtz v0, :cond_0

    .line 62
    const/4 v0, 0x0

    .line 64
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Ljava/util/Locale;)I
    .locals 1

    .prologue
    .line 144
    invoke-static {p1}, Ldbxyzptlk/db231222/ae/v;->a(Ljava/util/Locale;)Ldbxyzptlk/db231222/ae/v;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ae/v;->a()I

    move-result v0

    return v0
.end method

.method public final a(JLjava/lang/String;Ljava/util/Locale;)J
    .locals 2

    .prologue
    .line 93
    invoke-static {p4}, Ldbxyzptlk/db231222/ae/v;->a(Ljava/util/Locale;)Ldbxyzptlk/db231222/ae/v;

    move-result-object v0

    invoke-virtual {v0, p3}, Ldbxyzptlk/db231222/ae/v;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, p1, p2, v0}, Ldbxyzptlk/db231222/ae/u;->b(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(ILjava/util/Locale;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    invoke-static {p2}, Ldbxyzptlk/db231222/ae/v;->a(Ljava/util/Locale;)Ldbxyzptlk/db231222/ae/v;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/ae/v;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(JI)J
    .locals 2

    .prologue
    .line 81
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p3, v0, v1}, Ldbxyzptlk/db231222/ag/h;->a(Ldbxyzptlk/db231222/ac/c;III)V

    .line 83
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/ae/u;->a(J)I

    move-result v0

    .line 84
    if-eq v0, p3, :cond_0

    .line 85
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/u;->a:Ldbxyzptlk/db231222/ae/d;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ae/d;->a(J)I

    move-result v0

    .line 86
    iget-object v1, p0, Ldbxyzptlk/db231222/ae/u;->a:Ldbxyzptlk/db231222/ae/d;

    neg-int v0, v0

    invoke-virtual {v1, p1, p2, v0}, Ldbxyzptlk/db231222/ae/d;->f(JI)J

    move-result-wide p1

    .line 88
    :cond_0
    return-wide p1
.end method

.method public final d(J)J
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 97
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/ae/u;->a(J)I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 98
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/u;->a:Ldbxyzptlk/db231222/ae/d;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2, v3}, Ldbxyzptlk/db231222/ae/d;->f(JI)J

    move-result-wide v0

    .line 100
    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0
.end method

.method public final d()Ldbxyzptlk/db231222/ac/l;
    .locals 1

    .prologue
    .line 128
    invoke-static {}, Ldbxyzptlk/db231222/ac/m;->l()Ldbxyzptlk/db231222/ac/m;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/ag/u;->a(Ldbxyzptlk/db231222/ac/m;)Ldbxyzptlk/db231222/ag/u;

    move-result-object v0

    return-object v0
.end method

.method public final e(J)J
    .locals 4

    .prologue
    .line 105
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/ae/u;->a(J)I

    move-result v0

    if-nez v0, :cond_0

    .line 106
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/u;->a:Ldbxyzptlk/db231222/ae/d;

    const-wide/16 v1, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Ldbxyzptlk/db231222/ae/d;->f(JI)J

    move-result-wide v0

    .line 108
    :goto_0
    return-wide v0

    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0
.end method

.method public final e()Ldbxyzptlk/db231222/ac/l;
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f(J)J
    .locals 2

    .prologue
    .line 114
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/ae/u;->d(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    return v0
.end method

.method public final g(J)J
    .locals 2

    .prologue
    .line 119
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/ae/u;->d(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x1

    return v0
.end method

.method public final h(J)J
    .locals 2

    .prologue
    .line 124
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/ae/u;->d(J)J

    move-result-wide v0

    return-wide v0
.end method
