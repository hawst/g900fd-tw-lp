.class public final Ldbxyzptlk/db231222/ae/o;
.super Ldbxyzptlk/db231222/ae/a;
.source "panda.py"


# static fields
.field private static final a:Ldbxyzptlk/db231222/ac/c;

.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ldbxyzptlk/db231222/ac/i;",
            "Ldbxyzptlk/db231222/ae/o;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ldbxyzptlk/db231222/ae/o;

.field private static final serialVersionUID:J = -0x30383de30522ba0eL


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 65
    new-instance v0, Ldbxyzptlk/db231222/ae/k;

    const-string v1, "BE"

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/ae/k;-><init>(Ljava/lang/String;)V

    sput-object v0, Ldbxyzptlk/db231222/ae/o;->a:Ldbxyzptlk/db231222/ac/c;

    .line 71
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/ae/o;->b:Ljava/util/Map;

    .line 74
    sget-object v0, Ldbxyzptlk/db231222/ac/i;->a:Ldbxyzptlk/db231222/ac/i;

    invoke-static {v0}, Ldbxyzptlk/db231222/ae/o;->b(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ae/o;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/db231222/ae/o;->c:Ldbxyzptlk/db231222/ae/o;

    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/ac/a;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 131
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/db231222/ae/a;-><init>(Ldbxyzptlk/db231222/ac/a;Ljava/lang/Object;)V

    .line 132
    return-void
.end method

.method public static N()Ldbxyzptlk/db231222/ae/o;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Ldbxyzptlk/db231222/ae/o;->c:Ldbxyzptlk/db231222/ae/o;

    return-object v0
.end method

.method public static declared-synchronized b(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ae/o;
    .locals 11

    .prologue
    .line 104
    const-class v9, Ldbxyzptlk/db231222/ae/o;

    monitor-enter v9

    if-nez p0, :cond_0

    .line 105
    :try_start_0
    invoke-static {}, Ldbxyzptlk/db231222/ac/i;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object p0

    .line 108
    :cond_0
    sget-object v10, Ldbxyzptlk/db231222/ae/o;->b:Ljava/util/Map;

    monitor-enter v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 109
    :try_start_1
    sget-object v0, Ldbxyzptlk/db231222/ae/o;->b:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/ae/o;

    .line 110
    if-nez v0, :cond_1

    .line 112
    new-instance v8, Ldbxyzptlk/db231222/ae/o;

    const/4 v0, 0x0

    invoke-static {p0, v0}, Ldbxyzptlk/db231222/ae/p;->a(Ldbxyzptlk/db231222/ac/i;Ldbxyzptlk/db231222/ac/B;)Ldbxyzptlk/db231222/ae/p;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {v8, v0, v1}, Ldbxyzptlk/db231222/ae/o;-><init>(Ldbxyzptlk/db231222/ac/a;Ljava/lang/Object;)V

    .line 114
    new-instance v0, Ldbxyzptlk/db231222/ac/b;

    const/4 v1, 0x1

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v8}, Ldbxyzptlk/db231222/ac/b;-><init>(IIIIIIILdbxyzptlk/db231222/ac/a;)V

    .line 115
    new-instance v1, Ldbxyzptlk/db231222/ae/o;

    const/4 v2, 0x0

    invoke-static {v8, v0, v2}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/z;Ldbxyzptlk/db231222/ac/z;)Ldbxyzptlk/db231222/ae/D;

    move-result-object v0

    const-string v2, ""

    invoke-direct {v1, v0, v2}, Ldbxyzptlk/db231222/ae/o;-><init>(Ldbxyzptlk/db231222/ac/a;Ljava/lang/Object;)V

    .line 116
    sget-object v0, Ldbxyzptlk/db231222/ae/o;->b:Ljava/util/Map;

    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 118
    :cond_1
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 119
    monitor-exit v9

    return-object v0

    .line 118
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 104
    :catchall_1
    move-exception v0

    monitor-exit v9

    throw v0
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/o;->L()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    .line 139
    if-nez v0, :cond_0

    invoke-static {}, Ldbxyzptlk/db231222/ae/o;->N()Ldbxyzptlk/db231222/ae/o;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/a;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/ae/o;->b(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ae/o;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ac/a;
    .locals 1

    .prologue
    .line 160
    if-nez p1, :cond_0

    .line 161
    invoke-static {}, Ldbxyzptlk/db231222/ac/i;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object p1

    .line 163
    :cond_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/o;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 166
    :goto_0
    return-object p0

    :cond_1
    invoke-static {p1}, Ldbxyzptlk/db231222/ae/o;->b(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ae/o;

    move-result-object p0

    goto :goto_0
.end method

.method protected final a(Ldbxyzptlk/db231222/ae/b;)V
    .locals 6

    .prologue
    const/16 v5, 0x21f

    const/16 v4, 0x64

    const/4 v3, 0x1

    .line 215
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/o;->M()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 217
    iget-object v0, p1, Ldbxyzptlk/db231222/ae/b;->E:Ldbxyzptlk/db231222/ac/c;

    .line 218
    new-instance v1, Ldbxyzptlk/db231222/ag/l;

    new-instance v2, Ldbxyzptlk/db231222/ag/s;

    invoke-direct {v2, p0, v0}, Ldbxyzptlk/db231222/ag/s;-><init>(Ldbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/c;)V

    invoke-direct {v1, v2, v5}, Ldbxyzptlk/db231222/ag/l;-><init>(Ldbxyzptlk/db231222/ac/c;I)V

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->E:Ldbxyzptlk/db231222/ac/c;

    .line 222
    iget-object v0, p1, Ldbxyzptlk/db231222/ae/b;->F:Ldbxyzptlk/db231222/ac/c;

    .line 223
    new-instance v0, Ldbxyzptlk/db231222/ag/f;

    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->E:Ldbxyzptlk/db231222/ac/c;

    invoke-static {}, Ldbxyzptlk/db231222/ac/d;->t()Ldbxyzptlk/db231222/ac/d;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/ag/f;-><init>(Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/d;)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->F:Ldbxyzptlk/db231222/ac/c;

    .line 227
    iget-object v0, p1, Ldbxyzptlk/db231222/ae/b;->B:Ldbxyzptlk/db231222/ac/c;

    .line 228
    new-instance v1, Ldbxyzptlk/db231222/ag/l;

    new-instance v2, Ldbxyzptlk/db231222/ag/s;

    invoke-direct {v2, p0, v0}, Ldbxyzptlk/db231222/ag/s;-><init>(Ldbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/c;)V

    invoke-direct {v1, v2, v5}, Ldbxyzptlk/db231222/ag/l;-><init>(Ldbxyzptlk/db231222/ac/c;I)V

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->B:Ldbxyzptlk/db231222/ac/c;

    .line 231
    new-instance v0, Ldbxyzptlk/db231222/ag/l;

    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->F:Ldbxyzptlk/db231222/ac/c;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/db231222/ag/l;-><init>(Ldbxyzptlk/db231222/ac/c;I)V

    .line 232
    new-instance v1, Ldbxyzptlk/db231222/ag/g;

    invoke-static {}, Ldbxyzptlk/db231222/ac/d;->v()Ldbxyzptlk/db231222/ac/d;

    move-result-object v2

    invoke-direct {v1, v0, v2, v4}, Ldbxyzptlk/db231222/ag/g;-><init>(Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/d;I)V

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->H:Ldbxyzptlk/db231222/ac/c;

    .line 235
    new-instance v1, Ldbxyzptlk/db231222/ag/p;

    iget-object v0, p1, Ldbxyzptlk/db231222/ae/b;->H:Ldbxyzptlk/db231222/ac/c;

    check-cast v0, Ldbxyzptlk/db231222/ag/g;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/ag/p;-><init>(Ldbxyzptlk/db231222/ag/g;)V

    .line 237
    new-instance v0, Ldbxyzptlk/db231222/ag/l;

    invoke-static {}, Ldbxyzptlk/db231222/ac/d;->u()Ldbxyzptlk/db231222/ac/d;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/ag/l;-><init>(Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/d;I)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->G:Ldbxyzptlk/db231222/ac/c;

    .line 240
    new-instance v0, Ldbxyzptlk/db231222/ag/p;

    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->B:Ldbxyzptlk/db231222/ac/c;

    invoke-static {}, Ldbxyzptlk/db231222/ac/d;->q()Ldbxyzptlk/db231222/ac/d;

    move-result-object v2

    invoke-direct {v0, v1, v2, v4}, Ldbxyzptlk/db231222/ag/p;-><init>(Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/d;I)V

    .line 242
    new-instance v1, Ldbxyzptlk/db231222/ag/l;

    invoke-static {}, Ldbxyzptlk/db231222/ac/d;->q()Ldbxyzptlk/db231222/ac/d;

    move-result-object v2

    invoke-direct {v1, v0, v2, v3}, Ldbxyzptlk/db231222/ag/l;-><init>(Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/d;I)V

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->C:Ldbxyzptlk/db231222/ac/c;

    .line 245
    sget-object v0, Ldbxyzptlk/db231222/ae/o;->a:Ldbxyzptlk/db231222/ac/c;

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->I:Ldbxyzptlk/db231222/ac/c;

    .line 247
    :cond_0
    return-void
.end method

.method public final b()Ldbxyzptlk/db231222/ac/a;
    .locals 1

    .prologue
    .line 150
    sget-object v0, Ldbxyzptlk/db231222/ae/o;->c:Ldbxyzptlk/db231222/ae/o;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 178
    if-ne p0, p1, :cond_0

    .line 179
    const/4 v0, 0x1

    .line 185
    :goto_0
    return v0

    .line 181
    :cond_0
    instance-of v0, p1, Ldbxyzptlk/db231222/ae/o;

    if-eqz v0, :cond_1

    .line 182
    check-cast p1, Ldbxyzptlk/db231222/ae/o;

    .line 183
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/o;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v0

    invoke-virtual {p1}, Ldbxyzptlk/db231222/ae/o;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/ac/i;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 185
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 195
    const-string v0, "Buddhist"

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0xb

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/o;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/ac/i;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 206
    const-string v0, "BuddhistChronology"

    .line 207
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/o;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v1

    .line 208
    if-eqz v1, :cond_0

    .line 209
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x5b

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ldbxyzptlk/db231222/ac/i;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 211
    :cond_0
    return-object v0
.end method
