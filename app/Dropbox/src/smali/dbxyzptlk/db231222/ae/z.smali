.class public final Ldbxyzptlk/db231222/ae/z;
.super Ldbxyzptlk/db231222/ae/a;
.source "panda.py"


# static fields
.field private static final a:Ldbxyzptlk/db231222/ae/z;

.field private static final b:[Ldbxyzptlk/db231222/ae/z;

.field private static final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ldbxyzptlk/db231222/ac/i;",
            "Ldbxyzptlk/db231222/ae/z;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = -0x5637ee998ec8afd9L


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/ae/z;->c:Ljava/util/Map;

    .line 65
    const/16 v0, 0x40

    new-array v0, v0, [Ldbxyzptlk/db231222/ae/z;

    sput-object v0, Ldbxyzptlk/db231222/ae/z;->b:[Ldbxyzptlk/db231222/ae/z;

    .line 66
    new-instance v0, Ldbxyzptlk/db231222/ae/z;

    invoke-static {}, Ldbxyzptlk/db231222/ae/y;->Z()Ldbxyzptlk/db231222/ae/y;

    move-result-object v1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/ae/z;-><init>(Ldbxyzptlk/db231222/ac/a;)V

    sput-object v0, Ldbxyzptlk/db231222/ae/z;->a:Ldbxyzptlk/db231222/ae/z;

    .line 67
    sget-object v0, Ldbxyzptlk/db231222/ae/z;->c:Ljava/util/Map;

    sget-object v1, Ldbxyzptlk/db231222/ac/i;->a:Ldbxyzptlk/db231222/ac/i;

    sget-object v2, Ldbxyzptlk/db231222/ae/z;->a:Ldbxyzptlk/db231222/ae/z;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/db231222/ac/a;)V
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ldbxyzptlk/db231222/ae/a;-><init>(Ldbxyzptlk/db231222/ac/a;Ljava/lang/Object;)V

    .line 123
    return-void
.end method

.method public static N()Ldbxyzptlk/db231222/ae/z;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Ldbxyzptlk/db231222/ae/z;->a:Ldbxyzptlk/db231222/ae/z;

    return-object v0
.end method

.method public static O()Ldbxyzptlk/db231222/ae/z;
    .locals 1

    .prologue
    .line 86
    invoke-static {}, Ldbxyzptlk/db231222/ac/i;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/db231222/ae/z;->b(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ae/z;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ae/z;
    .locals 4

    .prologue
    .line 96
    if-nez p0, :cond_0

    .line 97
    invoke-static {}, Ldbxyzptlk/db231222/ac/i;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object p0

    .line 99
    :cond_0
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    and-int/lit8 v1, v0, 0x3f

    .line 100
    sget-object v0, Ldbxyzptlk/db231222/ae/z;->b:[Ldbxyzptlk/db231222/ae/z;

    aget-object v0, v0, v1

    .line 101
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ae/z;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v2

    if-ne v2, p0, :cond_1

    .line 112
    :goto_0
    return-object v0

    .line 104
    :cond_1
    sget-object v2, Ldbxyzptlk/db231222/ae/z;->c:Ljava/util/Map;

    monitor-enter v2

    .line 105
    :try_start_0
    sget-object v0, Ldbxyzptlk/db231222/ae/z;->c:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/ae/z;

    .line 106
    if-nez v0, :cond_2

    .line 107
    new-instance v0, Ldbxyzptlk/db231222/ae/z;

    sget-object v3, Ldbxyzptlk/db231222/ae/z;->a:Ldbxyzptlk/db231222/ae/z;

    invoke-static {v3, p0}, Ldbxyzptlk/db231222/ae/H;->a(Ldbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ae/H;

    move-result-object v3

    invoke-direct {v0, v3}, Ldbxyzptlk/db231222/ae/z;-><init>(Ldbxyzptlk/db231222/ac/a;)V

    .line 108
    sget-object v3, Ldbxyzptlk/db231222/ae/z;->c:Ljava/util/Map;

    invoke-interface {v3, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    :cond_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    sget-object v2, Ldbxyzptlk/db231222/ae/z;->b:[Ldbxyzptlk/db231222/ae/z;

    aput-object v0, v2, v1

    goto :goto_0

    .line 110
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 217
    new-instance v0, Ldbxyzptlk/db231222/ae/A;

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/z;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v1

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/ae/A;-><init>(Ldbxyzptlk/db231222/ac/i;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ac/a;
    .locals 1

    .prologue
    .line 143
    if-nez p1, :cond_0

    .line 144
    invoke-static {}, Ldbxyzptlk/db231222/ac/i;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object p1

    .line 146
    :cond_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/z;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 149
    :goto_0
    return-object p0

    :cond_1
    invoke-static {p1}, Ldbxyzptlk/db231222/ae/z;->b(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ae/z;

    move-result-object p0

    goto :goto_0
.end method

.method protected final a(Ldbxyzptlk/db231222/ae/b;)V
    .locals 4

    .prologue
    .line 169
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/z;->L()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/a;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v0

    sget-object v1, Ldbxyzptlk/db231222/ac/i;->a:Ldbxyzptlk/db231222/ac/i;

    if-ne v0, v1, :cond_0

    .line 171
    new-instance v0, Ldbxyzptlk/db231222/ag/g;

    sget-object v1, Ldbxyzptlk/db231222/ae/B;->a:Ldbxyzptlk/db231222/ac/c;

    invoke-static {}, Ldbxyzptlk/db231222/ac/d;->v()Ldbxyzptlk/db231222/ac/d;

    move-result-object v2

    const/16 v3, 0x64

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/ag/g;-><init>(Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/d;I)V

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->H:Ldbxyzptlk/db231222/ac/c;

    .line 173
    new-instance v1, Ldbxyzptlk/db231222/ag/p;

    iget-object v0, p1, Ldbxyzptlk/db231222/ae/b;->H:Ldbxyzptlk/db231222/ac/c;

    check-cast v0, Ldbxyzptlk/db231222/ag/g;

    invoke-static {}, Ldbxyzptlk/db231222/ac/d;->u()Ldbxyzptlk/db231222/ac/d;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ldbxyzptlk/db231222/ag/p;-><init>(Ldbxyzptlk/db231222/ag/g;Ldbxyzptlk/db231222/ac/d;)V

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->G:Ldbxyzptlk/db231222/ac/c;

    .line 175
    new-instance v1, Ldbxyzptlk/db231222/ag/p;

    iget-object v0, p1, Ldbxyzptlk/db231222/ae/b;->H:Ldbxyzptlk/db231222/ac/c;

    check-cast v0, Ldbxyzptlk/db231222/ag/g;

    invoke-static {}, Ldbxyzptlk/db231222/ac/d;->q()Ldbxyzptlk/db231222/ac/d;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ldbxyzptlk/db231222/ag/p;-><init>(Ldbxyzptlk/db231222/ag/g;Ldbxyzptlk/db231222/ac/d;)V

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->C:Ldbxyzptlk/db231222/ac/c;

    .line 178
    iget-object v0, p1, Ldbxyzptlk/db231222/ae/b;->H:Ldbxyzptlk/db231222/ac/c;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/c;->d()Ldbxyzptlk/db231222/ac/l;

    move-result-object v0

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->k:Ldbxyzptlk/db231222/ac/l;

    .line 180
    :cond_0
    return-void
.end method

.method public final b()Ldbxyzptlk/db231222/ac/a;
    .locals 1

    .prologue
    .line 133
    sget-object v0, Ldbxyzptlk/db231222/ae/z;->a:Ldbxyzptlk/db231222/ae/z;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 191
    if-ne p0, p1, :cond_0

    .line 192
    const/4 v0, 0x1

    .line 198
    :goto_0
    return v0

    .line 194
    :cond_0
    instance-of v0, p1, Ldbxyzptlk/db231222/ae/z;

    if-eqz v0, :cond_1

    .line 195
    check-cast p1, Ldbxyzptlk/db231222/ae/z;

    .line 196
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/z;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v0

    invoke-virtual {p1}, Ldbxyzptlk/db231222/ae/z;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/ac/i;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 198
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 208
    const-string v0, "ISO"

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0xb

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/z;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/ac/i;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 160
    const-string v0, "ISOChronology"

    .line 161
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/z;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v1

    .line 162
    if-eqz v1, :cond_0

    .line 163
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x5b

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ldbxyzptlk/db231222/ac/i;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 165
    :cond_0
    return-object v0
.end method
