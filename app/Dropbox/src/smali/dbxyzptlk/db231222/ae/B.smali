.class final Ldbxyzptlk/db231222/ae/B;
.super Ldbxyzptlk/db231222/ag/d;
.source "panda.py"


# static fields
.field static final a:Ldbxyzptlk/db231222/ac/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Ldbxyzptlk/db231222/ae/B;

    invoke-direct {v0}, Ldbxyzptlk/db231222/ae/B;-><init>()V

    sput-object v0, Ldbxyzptlk/db231222/ae/B;->a:Ldbxyzptlk/db231222/ac/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 47
    invoke-static {}, Ldbxyzptlk/db231222/ae/y;->Z()Ldbxyzptlk/db231222/ae/y;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ae/y;->E()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-static {}, Ldbxyzptlk/db231222/ac/d;->t()Ldbxyzptlk/db231222/ac/d;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Ldbxyzptlk/db231222/ag/d;-><init>(Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/d;)V

    .line 48
    return-void
.end method


# virtual methods
.method public final a(J)I
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/B;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->a(J)I

    move-result v0

    .line 52
    if-gez v0, :cond_0

    neg-int v0, v0

    :cond_0
    return v0
.end method

.method public final a(JI)J
    .locals 2

    .prologue
    .line 56
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/B;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Ldbxyzptlk/db231222/ac/c;->a(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(JJ)J
    .locals 2

    .prologue
    .line 60
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/B;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ac/c;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final b(JJ)I
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/B;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ac/c;->b(JJ)I

    move-result v0

    return v0
.end method

.method public final b(JI)J
    .locals 2

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/B;->h()I

    move-result v1

    invoke-static {p0, p3, v0, v1}, Ldbxyzptlk/db231222/ag/h;->a(Ldbxyzptlk/db231222/ac/c;III)V

    .line 81
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/B;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->a(J)I

    move-result v0

    if-gez v0, :cond_0

    .line 82
    neg-int p3, p3

    .line 84
    :cond_0
    invoke-super {p0, p1, p2, p3}, Ldbxyzptlk/db231222/ag/d;->b(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c(JJ)J
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/B;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ac/c;->c(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final d(J)J
    .locals 2

    .prologue
    .line 96
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/B;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->d(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final e(J)J
    .locals 2

    .prologue
    .line 100
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/B;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->e(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/B;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/c;->h()I

    move-result v0

    return v0
.end method

.method public final i(J)J
    .locals 2

    .prologue
    .line 104
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/B;->i()Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ac/c;->i(J)J

    move-result-wide v0

    return-wide v0
.end method
