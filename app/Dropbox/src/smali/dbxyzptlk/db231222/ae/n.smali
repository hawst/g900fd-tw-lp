.class final Ldbxyzptlk/db231222/ae/n;
.super Ldbxyzptlk/db231222/ag/i;
.source "panda.py"


# instance fields
.field protected final a:Ldbxyzptlk/db231222/ae/d;


# direct methods
.method constructor <init>(Ldbxyzptlk/db231222/ae/d;)V
    .locals 3

    .prologue
    .line 46
    invoke-static {}, Ldbxyzptlk/db231222/ac/d;->s()Ldbxyzptlk/db231222/ac/d;

    move-result-object v0

    invoke-virtual {p1}, Ldbxyzptlk/db231222/ae/d;->T()J

    move-result-wide v1

    invoke-direct {p0, v0, v1, v2}, Ldbxyzptlk/db231222/ag/i;-><init>(Ldbxyzptlk/db231222/ac/d;J)V

    .line 47
    iput-object p1, p0, Ldbxyzptlk/db231222/ae/n;->a:Ldbxyzptlk/db231222/ae/d;

    .line 48
    return-void
.end method


# virtual methods
.method public final a(J)I
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/n;->a:Ldbxyzptlk/db231222/ae/d;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/db231222/ae/d;->a(J)I

    move-result v0

    return v0
.end method

.method public final a(JI)J
    .locals 1

    .prologue
    .line 59
    if-nez p3, :cond_0

    .line 64
    :goto_0
    return-wide p1

    .line 62
    :cond_0
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/ae/n;->a(J)I

    move-result v0

    .line 63
    invoke-static {v0, p3}, Ldbxyzptlk/db231222/ag/h;->a(II)I

    move-result v0

    .line 64
    invoke-virtual {p0, p1, p2, v0}, Ldbxyzptlk/db231222/ae/n;->b(JI)J

    move-result-wide p1

    goto :goto_0
.end method

.method public final a(JJ)J
    .locals 2

    .prologue
    .line 68
    invoke-static {p3, p4}, Ldbxyzptlk/db231222/ag/h;->a(J)I

    move-result v0

    invoke-virtual {p0, p1, p2, v0}, Ldbxyzptlk/db231222/ae/n;->a(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method public final b(JI)J
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/n;->a:Ldbxyzptlk/db231222/ae/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ae/d;->Q()I

    move-result v0

    iget-object v1, p0, Ldbxyzptlk/db231222/ae/n;->a:Ldbxyzptlk/db231222/ae/d;

    invoke-virtual {v1}, Ldbxyzptlk/db231222/ae/d;->R()I

    move-result v1

    invoke-static {p0, p3, v0, v1}, Ldbxyzptlk/db231222/ag/h;->a(Ldbxyzptlk/db231222/ac/c;III)V

    .line 85
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/n;->a:Ldbxyzptlk/db231222/ae/d;

    invoke-virtual {v0, p1, p2, p3}, Ldbxyzptlk/db231222/ae/d;->f(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method public final b(J)Z
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/n;->a:Ldbxyzptlk/db231222/ae/d;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/ae/n;->a(J)I

    move-result v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/ae/d;->e(I)Z

    move-result v0

    return v0
.end method

.method public final c(JJ)J
    .locals 2

    .prologue
    .line 89
    cmp-long v0, p1, p3

    if-gez v0, :cond_0

    .line 90
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/n;->a:Ldbxyzptlk/db231222/ae/d;

    invoke-virtual {v0, p3, p4, p1, p2}, Ldbxyzptlk/db231222/ae/d;->a(JJ)J

    move-result-wide v0

    neg-long v0, v0

    .line 92
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/n;->a:Ldbxyzptlk/db231222/ae/d;

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ae/d;->a(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final d(J)J
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/n;->a:Ldbxyzptlk/db231222/ae/d;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/ae/n;->a(J)I

    move-result v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/ae/d;->d(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final e(J)J
    .locals 3

    .prologue
    .line 128
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/ae/n;->a(J)I

    move-result v0

    .line 129
    iget-object v1, p0, Ldbxyzptlk/db231222/ae/n;->a:Ldbxyzptlk/db231222/ae/d;

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/ae/d;->d(I)J

    move-result-wide v1

    .line 130
    cmp-long v1, p1, v1

    if-eqz v1, :cond_0

    .line 132
    iget-object v1, p0, Ldbxyzptlk/db231222/ae/n;->a:Ldbxyzptlk/db231222/ae/d;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ldbxyzptlk/db231222/ae/d;->d(I)J

    move-result-wide p1

    .line 134
    :cond_0
    return-wide p1
.end method

.method public final e()Ldbxyzptlk/db231222/ac/l;
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Ldbxyzptlk/db231222/ac/l;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/n;->a:Ldbxyzptlk/db231222/ae/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ae/d;->s()Ldbxyzptlk/db231222/ac/l;

    move-result-object v0

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/n;->a:Ldbxyzptlk/db231222/ae/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ae/d;->Q()I

    move-result v0

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/n;->a:Ldbxyzptlk/db231222/ae/d;

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ae/d;->R()I

    move-result v0

    return v0
.end method

.method public final i(J)J
    .locals 2

    .prologue
    .line 138
    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/ae/n;->d(J)J

    move-result-wide v0

    sub-long v0, p1, v0

    return-wide v0
.end method
