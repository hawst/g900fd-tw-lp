.class public final Ldbxyzptlk/db231222/ae/D;
.super Ldbxyzptlk/db231222/ae/a;
.source "panda.py"


# static fields
.field private static final serialVersionUID:J = 0x6a7464792af0f66aL


# instance fields
.field final a:Ldbxyzptlk/db231222/ac/b;

.field final b:Ldbxyzptlk/db231222/ac/b;

.field private transient c:Ldbxyzptlk/db231222/ae/D;


# direct methods
.method private constructor <init>(Ldbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/b;Ldbxyzptlk/db231222/ac/b;)V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ldbxyzptlk/db231222/ae/a;-><init>(Ldbxyzptlk/db231222/ac/a;Ljava/lang/Object;)V

    .line 101
    iput-object p2, p0, Ldbxyzptlk/db231222/ae/D;->a:Ldbxyzptlk/db231222/ac/b;

    .line 102
    iput-object p3, p0, Ldbxyzptlk/db231222/ae/D;->b:Ldbxyzptlk/db231222/ac/b;

    .line 103
    return-void
.end method

.method private a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/ac/c;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)",
            "Ldbxyzptlk/db231222/ac/c;"
        }
    .end annotation

    .prologue
    .line 268
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ldbxyzptlk/db231222/ac/c;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, p1

    .line 280
    :goto_0
    return-object v0

    .line 271
    :cond_1
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 272
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/ac/c;

    goto :goto_0

    .line 274
    :cond_2
    new-instance v0, Ldbxyzptlk/db231222/ae/E;

    invoke-virtual {p1}, Ldbxyzptlk/db231222/ac/c;->d()Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v3

    invoke-virtual {p1}, Ldbxyzptlk/db231222/ac/c;->e()Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v4

    invoke-virtual {p1}, Ldbxyzptlk/db231222/ac/c;->f()Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v5

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/ae/E;-><init>(Ldbxyzptlk/db231222/ae/D;Ldbxyzptlk/db231222/ac/c;Ldbxyzptlk/db231222/ac/l;Ldbxyzptlk/db231222/ac/l;Ldbxyzptlk/db231222/ac/l;)V

    .line 279
    invoke-virtual {p2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldbxyzptlk/db231222/ac/l;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)",
            "Ldbxyzptlk/db231222/ac/l;"
        }
    .end annotation

    .prologue
    .line 256
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ldbxyzptlk/db231222/ac/l;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, p1

    .line 264
    :goto_0
    return-object v0

    .line 259
    :cond_1
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 260
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/ac/l;

    goto :goto_0

    .line 262
    :cond_2
    new-instance v0, Ldbxyzptlk/db231222/ae/F;

    invoke-direct {v0, p0, p1}, Ldbxyzptlk/db231222/ae/F;-><init>(Ldbxyzptlk/db231222/ae/D;Ldbxyzptlk/db231222/ac/l;)V

    .line 263
    invoke-virtual {p2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static a(Ldbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/z;Ldbxyzptlk/db231222/ac/z;)Ldbxyzptlk/db231222/ae/D;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 67
    if-nez p0, :cond_0

    .line 68
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must supply a chronology"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_0
    if-nez p1, :cond_1

    move-object v0, v1

    .line 72
    :goto_0
    if-nez p2, :cond_2

    .line 74
    :goto_1
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    .line 75
    invoke-interface {v0, v1}, Ldbxyzptlk/db231222/ac/z;->a(Ldbxyzptlk/db231222/ac/B;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 76
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The lower limit must be come before than the upper limit"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_1
    invoke-interface {p1}, Ldbxyzptlk/db231222/ac/z;->a()Ldbxyzptlk/db231222/ac/b;

    move-result-object v0

    goto :goto_0

    .line 72
    :cond_2
    invoke-interface {p2}, Ldbxyzptlk/db231222/ac/z;->a()Ldbxyzptlk/db231222/ac/b;

    move-result-object v1

    goto :goto_1

    .line 81
    :cond_3
    new-instance v2, Ldbxyzptlk/db231222/ae/D;

    check-cast v0, Ldbxyzptlk/db231222/ac/b;

    check-cast v1, Ldbxyzptlk/db231222/ac/b;

    invoke-direct {v2, p0, v0, v1}, Ldbxyzptlk/db231222/ae/D;-><init>(Ldbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/b;Ldbxyzptlk/db231222/ac/b;)V

    return-object v2
.end method


# virtual methods
.method public final N()Ldbxyzptlk/db231222/ac/b;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/D;->a:Ldbxyzptlk/db231222/ac/b;

    return-object v0
.end method

.method public final O()Ldbxyzptlk/db231222/ac/b;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/D;->b:Ldbxyzptlk/db231222/ac/b;

    return-object v0
.end method

.method public final a(IIII)J
    .locals 3

    .prologue
    .line 177
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/D;->L()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Ldbxyzptlk/db231222/ac/a;->a(IIII)J

    move-result-wide v0

    .line 178
    const-string v2, "resulting"

    invoke-virtual {p0, v0, v1, v2}, Ldbxyzptlk/db231222/ae/D;->a(JLjava/lang/String;)V

    .line 179
    return-wide v0
.end method

.method public final a(IIIIIII)J
    .locals 8

    .prologue
    .line 187
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/D;->L()Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Ldbxyzptlk/db231222/ac/a;->a(IIIIIII)J

    move-result-wide v0

    .line 190
    const-string v2, "resulting"

    invoke-virtual {p0, v0, v1, v2}, Ldbxyzptlk/db231222/ae/D;->a(JLjava/lang/String;)V

    .line 191
    return-wide v0
.end method

.method public final a(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ac/a;
    .locals 3

    .prologue
    .line 138
    if-nez p1, :cond_0

    .line 139
    invoke-static {}, Ldbxyzptlk/db231222/ac/i;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object p1

    .line 141
    :cond_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/D;->a()Ldbxyzptlk/db231222/ac/i;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 170
    :goto_0
    return-object p0

    .line 145
    :cond_1
    sget-object v0, Ldbxyzptlk/db231222/ac/i;->a:Ldbxyzptlk/db231222/ac/i;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Ldbxyzptlk/db231222/ae/D;->c:Ldbxyzptlk/db231222/ae/D;

    if-eqz v0, :cond_2

    .line 146
    iget-object p0, p0, Ldbxyzptlk/db231222/ae/D;->c:Ldbxyzptlk/db231222/ae/D;

    goto :goto_0

    .line 149
    :cond_2
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/D;->a:Ldbxyzptlk/db231222/ac/b;

    .line 150
    if-eqz v0, :cond_3

    .line 151
    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/b;->e()Ldbxyzptlk/db231222/ac/t;

    move-result-object v0

    .line 152
    invoke-virtual {v0, p1}, Ldbxyzptlk/db231222/ac/t;->a(Ldbxyzptlk/db231222/ac/i;)V

    .line 153
    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/t;->a()Ldbxyzptlk/db231222/ac/b;

    move-result-object v0

    .line 156
    :cond_3
    iget-object v1, p0, Ldbxyzptlk/db231222/ae/D;->b:Ldbxyzptlk/db231222/ac/b;

    .line 157
    if-eqz v1, :cond_4

    .line 158
    invoke-virtual {v1}, Ldbxyzptlk/db231222/ac/b;->e()Ldbxyzptlk/db231222/ac/t;

    move-result-object v1

    .line 159
    invoke-virtual {v1, p1}, Ldbxyzptlk/db231222/ac/t;->a(Ldbxyzptlk/db231222/ac/i;)V

    .line 160
    invoke-virtual {v1}, Ldbxyzptlk/db231222/ac/t;->a()Ldbxyzptlk/db231222/ac/b;

    move-result-object v1

    .line 163
    :cond_4
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/D;->L()Ldbxyzptlk/db231222/ac/a;

    move-result-object v2

    invoke-virtual {v2, p1}, Ldbxyzptlk/db231222/ac/a;->a(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ac/a;

    move-result-object v2

    invoke-static {v2, v0, v1}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/a;Ldbxyzptlk/db231222/ac/z;Ldbxyzptlk/db231222/ac/z;)Ldbxyzptlk/db231222/ae/D;

    move-result-object v0

    .line 166
    sget-object v1, Ldbxyzptlk/db231222/ac/i;->a:Ldbxyzptlk/db231222/ac/i;

    if-ne p1, v1, :cond_5

    .line 167
    iput-object v0, p0, Ldbxyzptlk/db231222/ae/D;->c:Ldbxyzptlk/db231222/ae/D;

    :cond_5
    move-object p0, v0

    .line 170
    goto :goto_0
.end method

.method final a(JLjava/lang/String;)V
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/D;->a:Ldbxyzptlk/db231222/ac/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/b;->c()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 286
    new-instance v0, Ldbxyzptlk/db231222/ae/G;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p3, v1}, Ldbxyzptlk/db231222/ae/G;-><init>(Ldbxyzptlk/db231222/ae/D;Ljava/lang/String;Z)V

    throw v0

    .line 288
    :cond_0
    iget-object v0, p0, Ldbxyzptlk/db231222/ae/D;->b:Ldbxyzptlk/db231222/ac/b;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/b;->c()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_1

    .line 289
    new-instance v0, Ldbxyzptlk/db231222/ae/G;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p3, v1}, Ldbxyzptlk/db231222/ae/G;-><init>(Ldbxyzptlk/db231222/ae/D;Ljava/lang/String;Z)V

    throw v0

    .line 291
    :cond_1
    return-void
.end method

.method protected final a(Ldbxyzptlk/db231222/ae/b;)V
    .locals 2

    .prologue
    .line 209
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 213
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->l:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->l:Ldbxyzptlk/db231222/ac/l;

    .line 214
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->k:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->k:Ldbxyzptlk/db231222/ac/l;

    .line 215
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->j:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->j:Ldbxyzptlk/db231222/ac/l;

    .line 216
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->i:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->i:Ldbxyzptlk/db231222/ac/l;

    .line 217
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->h:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->h:Ldbxyzptlk/db231222/ac/l;

    .line 218
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->g:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->g:Ldbxyzptlk/db231222/ac/l;

    .line 219
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->f:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->f:Ldbxyzptlk/db231222/ac/l;

    .line 221
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->e:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->e:Ldbxyzptlk/db231222/ac/l;

    .line 222
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->d:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->d:Ldbxyzptlk/db231222/ac/l;

    .line 223
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->c:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->c:Ldbxyzptlk/db231222/ac/l;

    .line 224
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->b:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->b:Ldbxyzptlk/db231222/ac/l;

    .line 225
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->a:Ldbxyzptlk/db231222/ac/l;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/l;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/l;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->a:Ldbxyzptlk/db231222/ac/l;

    .line 229
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->E:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->E:Ldbxyzptlk/db231222/ac/c;

    .line 230
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->F:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->F:Ldbxyzptlk/db231222/ac/c;

    .line 231
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->G:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->G:Ldbxyzptlk/db231222/ac/c;

    .line 232
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->H:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->H:Ldbxyzptlk/db231222/ac/c;

    .line 233
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->I:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->I:Ldbxyzptlk/db231222/ac/c;

    .line 234
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->x:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->x:Ldbxyzptlk/db231222/ac/c;

    .line 235
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->y:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->y:Ldbxyzptlk/db231222/ac/c;

    .line 236
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->z:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->z:Ldbxyzptlk/db231222/ac/c;

    .line 237
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->D:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->D:Ldbxyzptlk/db231222/ac/c;

    .line 238
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->A:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->A:Ldbxyzptlk/db231222/ac/c;

    .line 239
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->B:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->B:Ldbxyzptlk/db231222/ac/c;

    .line 240
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->C:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->C:Ldbxyzptlk/db231222/ac/c;

    .line 242
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->m:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->m:Ldbxyzptlk/db231222/ac/c;

    .line 243
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->n:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->n:Ldbxyzptlk/db231222/ac/c;

    .line 244
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->o:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->o:Ldbxyzptlk/db231222/ac/c;

    .line 245
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->p:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->p:Ldbxyzptlk/db231222/ac/c;

    .line 246
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->q:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->q:Ldbxyzptlk/db231222/ac/c;

    .line 247
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->r:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->r:Ldbxyzptlk/db231222/ac/c;

    .line 248
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->s:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->s:Ldbxyzptlk/db231222/ac/c;

    .line 249
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->u:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->u:Ldbxyzptlk/db231222/ac/c;

    .line 250
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->t:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->t:Ldbxyzptlk/db231222/ac/c;

    .line 251
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->v:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v1

    iput-object v1, p1, Ldbxyzptlk/db231222/ae/b;->v:Ldbxyzptlk/db231222/ac/c;

    .line 252
    iget-object v1, p1, Ldbxyzptlk/db231222/ae/b;->w:Ldbxyzptlk/db231222/ac/c;

    invoke-direct {p0, v1, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/c;Ljava/util/HashMap;)Ldbxyzptlk/db231222/ac/c;

    move-result-object v0

    iput-object v0, p1, Ldbxyzptlk/db231222/ae/b;->w:Ldbxyzptlk/db231222/ac/c;

    .line 253
    return-void
.end method

.method public final b()Ldbxyzptlk/db231222/ac/a;
    .locals 1

    .prologue
    .line 129
    sget-object v0, Ldbxyzptlk/db231222/ac/i;->a:Ldbxyzptlk/db231222/ac/i;

    invoke-virtual {p0, v0}, Ldbxyzptlk/db231222/ae/D;->a(Ldbxyzptlk/db231222/ac/i;)Ldbxyzptlk/db231222/ac/a;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 303
    if-ne p0, p1, :cond_1

    .line 310
    :cond_0
    :goto_0
    return v0

    .line 306
    :cond_1
    instance-of v2, p1, Ldbxyzptlk/db231222/ae/D;

    if-nez v2, :cond_2

    move v0, v1

    .line 307
    goto :goto_0

    .line 309
    :cond_2
    check-cast p1, Ldbxyzptlk/db231222/ae/D;

    .line 310
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/D;->L()Ldbxyzptlk/db231222/ac/a;

    move-result-object v2

    invoke-virtual {p1}, Ldbxyzptlk/db231222/ae/D;->L()Ldbxyzptlk/db231222/ac/a;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/D;->N()Ldbxyzptlk/db231222/ac/b;

    move-result-object v2

    invoke-virtual {p1}, Ldbxyzptlk/db231222/ae/D;->N()Ldbxyzptlk/db231222/ac/b;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/ag/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/D;->O()Ldbxyzptlk/db231222/ac/b;

    move-result-object v2

    invoke-virtual {p1}, Ldbxyzptlk/db231222/ae/D;->O()Ldbxyzptlk/db231222/ac/b;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/db231222/ag/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 323
    const v2, 0x12ea67c5

    .line 324
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/D;->N()Ldbxyzptlk/db231222/ac/b;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/D;->N()Ldbxyzptlk/db231222/ac/b;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/b;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    .line 325
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/D;->O()Ldbxyzptlk/db231222/ac/b;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/D;->O()Ldbxyzptlk/db231222/ac/b;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/ac/b;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 326
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/D;->L()Ldbxyzptlk/db231222/ac/a;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    mul-int/lit8 v1, v1, 0x7

    add-int/2addr v0, v1

    .line 327
    return v0

    :cond_1
    move v0, v1

    .line 324
    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 336
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LimitChronology["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/D;->L()Ldbxyzptlk/db231222/ac/a;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/db231222/ac/a;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/D;->N()Ldbxyzptlk/db231222/ac/b;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "NoLimit"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/D;->O()Ldbxyzptlk/db231222/ac/b;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "NoLimit"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/D;->N()Ldbxyzptlk/db231222/ac/b;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/b;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Ldbxyzptlk/db231222/ae/D;->O()Ldbxyzptlk/db231222/ac/b;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/db231222/ac/b;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
