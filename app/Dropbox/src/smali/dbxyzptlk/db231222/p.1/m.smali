.class public final Ldbxyzptlk/db231222/p/m;
.super Ldbxyzptlk/db231222/p/j;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/p/j",
        "<",
        "Lcom/dropbox/android/sharedfolder/SharedFolderInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;Lcom/dropbox/sync/android/aV;Ljava/lang/String;Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;)V
    .locals 6

    .prologue
    .line 22
    const/4 v3, 0x0

    const v0, 0x7f0d010b

    invoke-virtual {p1, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v0, 0x7f0d010c

    invoke-virtual {p1, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderManageActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/p/j;-><init>(Lcom/dropbox/android/activity/base/BaseUserActivity;Lcom/dropbox/sync/android/aV;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    iput-object p3, p0, Ldbxyzptlk/db231222/p/m;->a:Ljava/lang/String;

    .line 25
    iput-object p4, p0, Ldbxyzptlk/db231222/p/m;->b:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    .line 26
    return-void
.end method


# virtual methods
.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Lcom/dropbox/android/sharedfolder/SharedFolderInfo;
    .locals 4

    .prologue
    .line 34
    invoke-virtual {p0}, Ldbxyzptlk/db231222/p/m;->b()Lcom/dropbox/sync/android/aV;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/p/m;->a:Ljava/lang/String;

    iget-object v2, p0, Ldbxyzptlk/db231222/p/m;->b:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    invoke-virtual {v2}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->a()Z

    move-result v2

    iget-object v3, p0, Ldbxyzptlk/db231222/p/m;->b:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    invoke-virtual {v3}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->b()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/sync/android/aV;->a(Ljava/lang/String;ZZ)Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    move-result-object v0

    .line 36
    new-instance v1, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    invoke-direct {v1, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;-><init>(Lcom/dropbox/sync/android/DbxSharedFolderInfo;)V

    return-object v1
.end method

.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/p/m;->a(Landroid/content/Context;[Ljava/lang/Void;)Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    move-result-object v0

    return-object v0
.end method
