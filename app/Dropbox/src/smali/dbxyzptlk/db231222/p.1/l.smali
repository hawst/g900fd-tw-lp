.class public final Ldbxyzptlk/db231222/p/l;
.super Ldbxyzptlk/db231222/p/e;
.source "panda.py"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/base/BaseUserActivity;Lcom/dropbox/android/util/DropboxPath;Lcom/dropbox/sync/android/aV;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    .line 21
    const v0, 0x7f0d0128

    invoke-virtual {p1, v0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f0d0129

    invoke-virtual {p1, v0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v0, 0x7f0d012a

    invoke-virtual {p1, v0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/p/e;-><init>(Lcom/dropbox/android/activity/base/BaseUserActivity;Lcom/dropbox/sync/android/aV;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/util/DropboxPath;)V

    .line 25
    iput-object p4, p0, Ldbxyzptlk/db231222/p/l;->a:Ljava/lang/String;

    .line 26
    iput-boolean p5, p0, Ldbxyzptlk/db231222/p/l;->b:Z

    .line 27
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/p/l;->a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3

    .prologue
    .line 35
    invoke-virtual {p0}, Ldbxyzptlk/db231222/p/l;->b()Lcom/dropbox/sync/android/aV;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/p/l;->a:Ljava/lang/String;

    iget-boolean v2, p0, Ldbxyzptlk/db231222/p/l;->b:Z

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/aV;->b(Ljava/lang/String;Z)V

    .line 36
    invoke-super {p0, p1, p2}, Ldbxyzptlk/db231222/p/e;->a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 14
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/p/l;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 3

    .prologue
    .line 41
    invoke-super {p0, p1, p2}, Ldbxyzptlk/db231222/p/e;->a(Landroid/content/Context;Ljava/lang/Void;)V

    .line 43
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cg()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "shared_folder_id"

    iget-object v2, p0, Ldbxyzptlk/db231222/p/l;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "keep_files"

    iget-boolean v2, p0, Ldbxyzptlk/db231222/p/l;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 46
    return-void
.end method
