.class public final Ldbxyzptlk/db231222/p/c;
.super Ldbxyzptlk/db231222/p/e;
.source "panda.py"


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;

.field private final c:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

.field private final d:I


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/base/BaseUserActivity;Lcom/dropbox/sync/android/aV;Lcom/dropbox/android/util/DropboxPath;Ljava/util/ArrayList;Ljava/lang/String;Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/activity/base/BaseUserActivity;",
            "Lcom/dropbox/sync/android/aV;",
            "Lcom/dropbox/android/util/DropboxPath;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 31
    const v0, 0x7f0d013f

    invoke-virtual {p1, v0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f0d0140

    invoke-virtual {p1, v0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v0, 0x7f0d0141

    invoke-virtual {p1, v0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/db231222/p/e;-><init>(Lcom/dropbox/android/activity/base/BaseUserActivity;Lcom/dropbox/sync/android/aV;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/util/DropboxPath;)V

    .line 35
    iput-object p4, p0, Ldbxyzptlk/db231222/p/c;->a:Ljava/util/ArrayList;

    .line 36
    iput-object p5, p0, Ldbxyzptlk/db231222/p/c;->b:Ljava/lang/String;

    .line 37
    iput-object p6, p0, Ldbxyzptlk/db231222/p/c;->c:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    .line 38
    iput p7, p0, Ldbxyzptlk/db231222/p/c;->d:I

    .line 39
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/p/c;->a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/Void;
    .locals 7

    .prologue
    .line 47
    invoke-virtual {p0}, Ldbxyzptlk/db231222/p/c;->b()Lcom/dropbox/sync/android/aV;

    move-result-object v0

    new-instance v1, Lcom/dropbox/sync/android/aT;

    invoke-virtual {p0}, Ldbxyzptlk/db231222/p/c;->a()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/dropbox/sync/android/aT;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ldbxyzptlk/db231222/p/c;->a:Ljava/util/ArrayList;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, Ldbxyzptlk/db231222/p/c;->b:Ljava/lang/String;

    iget-object v5, p0, Ldbxyzptlk/db231222/p/c;->c:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    invoke-virtual {v5}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->a()Z

    move-result v5

    iget-object v6, p0, Ldbxyzptlk/db231222/p/c;->c:Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;

    invoke-virtual {v6}, Lcom/dropbox/android/sharedfolder/SharedFolderPrefsState;->b()Z

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/dropbox/sync/android/aV;->a(Lcom/dropbox/sync/android/aT;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;ZZ)Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    move-result-object v0

    .line 50
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->ca()Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "is_new"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "num_recipients"

    iget-object v3, p0, Ldbxyzptlk/db231222/p/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "shared_folder_id"

    invoke-virtual {v0}, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    const-string v2, "is_only_owner_can_invite_perm"

    invoke-virtual {v0}, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->d()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v1

    .line 55
    iget v2, p0, Ldbxyzptlk/db231222/p/c;->d:I

    if-ltz v2, :cond_0

    .line 56
    const-string v2, "dfb_num_team_recipients"

    iget v3, p0, Ldbxyzptlk/db231222/p/c;->d:I

    int-to-long v3, v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    .line 57
    const-string v2, "dfb_is_team_only_perm"

    invoke-virtual {v0}, Lcom/dropbox/sync/android/DbxSharedFolderInfo;->e()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 59
    :cond_0
    invoke-virtual {v1}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 61
    invoke-super {p0, p1, p2}, Ldbxyzptlk/db231222/p/e;->a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
