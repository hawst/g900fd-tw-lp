.class public abstract Ldbxyzptlk/db231222/p/e;
.super Ldbxyzptlk/db231222/p/j;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/p/j",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/dropbox/android/util/DropboxPath;

.field private final b:Lcom/dropbox/android/provider/MetadataManager;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/base/BaseUserActivity;Lcom/dropbox/sync/android/aV;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/util/DropboxPath;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct/range {p0 .. p5}, Ldbxyzptlk/db231222/p/j;-><init>(Lcom/dropbox/android/activity/base/BaseUserActivity;Lcom/dropbox/sync/android/aV;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    iput-object p6, p0, Ldbxyzptlk/db231222/p/e;->a:Lcom/dropbox/android/util/DropboxPath;

    .line 31
    invoke-virtual {p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->i()Ldbxyzptlk/db231222/r/d;

    move-result-object v0

    .line 32
    if-eqz v0, :cond_0

    .line 33
    invoke-virtual {v0}, Ldbxyzptlk/db231222/r/d;->y()Lcom/dropbox/android/provider/MetadataManager;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/db231222/p/e;->b:Lcom/dropbox/android/provider/MetadataManager;

    .line 37
    :goto_0
    return-void

    .line 35
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/db231222/p/e;->b:Lcom/dropbox/android/provider/MetadataManager;

    goto :goto_0
.end method


# virtual methods
.method protected final a()Lcom/dropbox/android/util/DropboxPath;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Ldbxyzptlk/db231222/p/e;->a:Lcom/dropbox/android/util/DropboxPath;

    return-object v0
.end method

.method public bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/p/e;->a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Ldbxyzptlk/db231222/p/e;->b:Lcom/dropbox/android/provider/MetadataManager;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Ldbxyzptlk/db231222/p/e;->b:Lcom/dropbox/android/provider/MetadataManager;

    iget-object v1, p0, Ldbxyzptlk/db231222/p/e;->a:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/provider/MetadataManager;->d(Lcom/dropbox/android/util/DropboxPath;)V

    .line 51
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 20
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/p/e;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 1

    .prologue
    .line 59
    invoke-super {p0, p1, p2}, Ldbxyzptlk/db231222/p/j;->a(Landroid/content/Context;Ljava/lang/Object;)V

    .line 61
    check-cast p1, Lcom/dropbox/android/activity/base/BaseUserActivity;

    .line 62
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->setResult(I)V

    .line 63
    invoke-virtual {p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->finish()V

    .line 64
    return-void
.end method
