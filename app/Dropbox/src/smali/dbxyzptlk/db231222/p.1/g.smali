.class public final Ldbxyzptlk/db231222/p/g;
.super Ldbxyzptlk/db231222/p/j;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/p/j",
        "<",
        "Lcom/dropbox/android/sharedfolder/SharedFolderInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:I


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/base/BaseUserActivity;Lcom/dropbox/sync/android/aV;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/android/activity/base/BaseUserActivity;",
            "Lcom/dropbox/sync/android/aV;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 32
    const v0, 0x7f0d013f

    invoke-virtual {p1, v0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const v0, 0x7f0d0141

    invoke-virtual {p1, v0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/p/j;-><init>(Lcom/dropbox/android/activity/base/BaseUserActivity;Lcom/dropbox/sync/android/aV;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    iput-object p3, p0, Ldbxyzptlk/db231222/p/g;->a:Ljava/lang/String;

    .line 35
    iput-object p4, p0, Ldbxyzptlk/db231222/p/g;->c:Ljava/util/ArrayList;

    .line 36
    iput-object p5, p0, Ldbxyzptlk/db231222/p/g;->b:Ljava/lang/String;

    .line 37
    iput p6, p0, Ldbxyzptlk/db231222/p/g;->d:I

    .line 38
    return-void
.end method


# virtual methods
.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Lcom/dropbox/android/sharedfolder/SharedFolderInfo;
    .locals 5

    .prologue
    .line 46
    invoke-virtual {p0}, Ldbxyzptlk/db231222/p/g;->b()Lcom/dropbox/sync/android/aV;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/p/g;->a:Ljava/lang/String;

    iget-object v2, p0, Ldbxyzptlk/db231222/p/g;->c:Ljava/util/ArrayList;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, Ldbxyzptlk/db231222/p/g;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/dropbox/sync/android/aV;->a(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    move-result-object v0

    .line 48
    new-instance v1, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    invoke-direct {v1, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;-><init>(Lcom/dropbox/sync/android/DbxSharedFolderInfo;)V

    return-object v1
.end method

.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/p/g;->a(Landroid/content/Context;[Ljava/lang/Void;)Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Lcom/dropbox/android/sharedfolder/SharedFolderInfo;)V
    .locals 4

    .prologue
    .line 56
    invoke-super {p0, p1, p2}, Ldbxyzptlk/db231222/p/j;->a(Landroid/content/Context;Ljava/lang/Object;)V

    .line 57
    check-cast p1, Landroid/app/Activity;

    .line 58
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER_METADATA"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 59
    const/4 v1, -0x1

    invoke-virtual {p1, v1, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 60
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 62
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->ca()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "is_new"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "num_recipients"

    iget-object v2, p0, Ldbxyzptlk/db231222/p/g;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "shared_folder_id"

    iget-object v2, p2, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "is_only_owner_can_invite_perm"

    invoke-virtual {p2}, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    .line 67
    iget v1, p0, Ldbxyzptlk/db231222/p/g;->d:I

    if-ltz v1, :cond_0

    .line 68
    const-string v1, "dfb_num_team_recipients"

    iget v2, p0, Ldbxyzptlk/db231222/p/g;->d:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/analytics/l;

    .line 69
    const-string v1, "dfb_is_team_only_perm"

    invoke-virtual {p2}, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;->b()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    .line 71
    :cond_0
    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 72
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p2, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/p/g;->a(Landroid/content/Context;Lcom/dropbox/android/sharedfolder/SharedFolderInfo;)V

    return-void
.end method
