.class public final Ldbxyzptlk/db231222/p/f;
.super Ldbxyzptlk/db231222/p/j;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/p/j",
        "<",
        "Lcom/dropbox/android/sharedfolder/SharedFolderInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/base/BaseUserActivity;Lcom/dropbox/sync/android/aV;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 21
    const/4 v3, 0x0

    const v0, 0x7f0d011c

    invoke-virtual {p1, v0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v0, 0x7f0d011d

    invoke-virtual {p1, v0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/p/j;-><init>(Lcom/dropbox/android/activity/base/BaseUserActivity;Lcom/dropbox/sync/android/aV;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    iput-object p3, p0, Ldbxyzptlk/db231222/p/f;->a:Ljava/lang/String;

    .line 24
    iput-object p4, p0, Ldbxyzptlk/db231222/p/f;->b:Ljava/lang/String;

    .line 25
    return-void
.end method


# virtual methods
.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Lcom/dropbox/android/sharedfolder/SharedFolderInfo;
    .locals 3

    .prologue
    .line 33
    invoke-virtual {p0}, Ldbxyzptlk/db231222/p/f;->b()Lcom/dropbox/sync/android/aV;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/p/f;->a:Ljava/lang/String;

    iget-object v2, p0, Ldbxyzptlk/db231222/p/f;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/sync/android/aV;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    move-result-object v0

    .line 34
    new-instance v1, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    invoke-direct {v1, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;-><init>(Lcom/dropbox/sync/android/DbxSharedFolderInfo;)V

    return-object v1
.end method

.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/p/f;->a(Landroid/content/Context;[Ljava/lang/Void;)Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Lcom/dropbox/android/sharedfolder/SharedFolderInfo;)V
    .locals 3

    .prologue
    .line 39
    invoke-super {p0, p1, p2}, Ldbxyzptlk/db231222/p/j;->a(Landroid/content/Context;Ljava/lang/Object;)V

    .line 41
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cj()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "shared_folder_id"

    iget-object v2, p0, Ldbxyzptlk/db231222/p/f;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "invite_id"

    iget-object v2, p0, Ldbxyzptlk/db231222/p/f;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 44
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 15
    check-cast p2, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/p/f;->a(Landroid/content/Context;Lcom/dropbox/android/sharedfolder/SharedFolderInfo;)V

    return-void
.end method
