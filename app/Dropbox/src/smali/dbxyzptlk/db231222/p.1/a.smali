.class public final Ldbxyzptlk/db231222/p/a;
.super Ldbxyzptlk/db231222/p/j;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldbxyzptlk/db231222/p/j",
        "<",
        "Lcom/dropbox/android/sharedfolder/SharedFolderInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Z


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/base/BaseUserActivity;Lcom/dropbox/sync/android/aV;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 25
    const v0, 0x7f0d0136

    invoke-virtual {p1, v0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const v0, 0x7f0d0137

    invoke-virtual {p1, v0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/db231222/p/j;-><init>(Lcom/dropbox/android/activity/base/BaseUserActivity;Lcom/dropbox/sync/android/aV;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    iput-object p3, p0, Ldbxyzptlk/db231222/p/a;->a:Ljava/lang/String;

    .line 28
    iput-object p4, p0, Ldbxyzptlk/db231222/p/a;->b:Ljava/lang/String;

    .line 29
    iput-boolean p5, p0, Ldbxyzptlk/db231222/p/a;->c:Z

    .line 30
    return-void
.end method


# virtual methods
.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Lcom/dropbox/android/sharedfolder/SharedFolderInfo;
    .locals 4

    .prologue
    .line 38
    invoke-virtual {p0}, Ldbxyzptlk/db231222/p/a;->b()Lcom/dropbox/sync/android/aV;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/db231222/p/a;->a:Ljava/lang/String;

    iget-object v2, p0, Ldbxyzptlk/db231222/p/a;->b:Ljava/lang/String;

    iget-boolean v3, p0, Ldbxyzptlk/db231222/p/a;->c:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/sync/android/aV;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    move-result-object v0

    .line 39
    new-instance v1, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    invoke-direct {v1, v0}, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;-><init>(Lcom/dropbox/sync/android/DbxSharedFolderInfo;)V

    return-object v1
.end method

.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/p/a;->a(Landroid/content/Context;[Ljava/lang/Void;)Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Lcom/dropbox/android/sharedfolder/SharedFolderInfo;)V
    .locals 3

    .prologue
    .line 47
    invoke-super {p0, p1, p2}, Ldbxyzptlk/db231222/p/j;->a(Landroid/content/Context;Ljava/lang/Object;)V

    .line 48
    check-cast p1, Landroid/app/Activity;

    .line 49
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.dropbox.android.activity.util.SharedFolderCommon.extra.SHARED_FOLDER_METADATA"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 50
    const/4 v1, -0x1

    invoke-virtual {p1, v1, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 51
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 53
    invoke-static {}, Lcom/dropbox/android/util/analytics/a;->cd()Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "shared_folder_id"

    iget-object v2, p0, Ldbxyzptlk/db231222/p/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "kicked_user_id"

    iget-object v2, p0, Ldbxyzptlk/db231222/p/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    const-string v1, "keep_files"

    iget-boolean v2, p0, Ldbxyzptlk/db231222/p/a;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/analytics/l;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/dropbox/android/util/analytics/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/analytics/l;->e()V

    .line 57
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p2, Lcom/dropbox/android/sharedfolder/SharedFolderInfo;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/db231222/p/a;->a(Landroid/content/Context;Lcom/dropbox/android/sharedfolder/SharedFolderInfo;)V

    return-void
.end method
