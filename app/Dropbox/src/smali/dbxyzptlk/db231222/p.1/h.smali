.class public final Ldbxyzptlk/db231222/p/h;
.super Landroid/support/v4/content/c;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/content/c",
        "<",
        "Ldbxyzptlk/db231222/p/i;",
        ">;"
    }
.end annotation


# instance fields
.field private final f:Lcom/dropbox/sync/android/aV;

.field private final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/sync/android/aV;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/support/v4/content/c;-><init>(Landroid/content/Context;)V

    .line 32
    iput-object p2, p0, Ldbxyzptlk/db231222/p/h;->f:Lcom/dropbox/sync/android/aV;

    .line 33
    iput-object p3, p0, Ldbxyzptlk/db231222/p/h;->g:Ljava/lang/String;

    .line 34
    return-void
.end method


# virtual methods
.method public final synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Ldbxyzptlk/db231222/p/h;->j()Ldbxyzptlk/db231222/p/i;

    move-result-object v0

    return-object v0
.end method

.method public final j()Ldbxyzptlk/db231222/p/i;
    .locals 7

    .prologue
    const v6, 0x7f0d00c8

    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 39
    :try_start_0
    iget-object v0, p0, Ldbxyzptlk/db231222/p/h;->f:Lcom/dropbox/sync/android/aV;

    iget-object v1, p0, Ldbxyzptlk/db231222/p/h;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/dropbox/sync/android/aV;->a(Ljava/lang/String;)Lcom/dropbox/sync/android/DbxSharedFolderInfo;

    move-result-object v1

    .line 40
    new-instance v0, Ldbxyzptlk/db231222/p/i;

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/db231222/p/i;-><init>(Lcom/dropbox/sync/android/DbxSharedFolderInfo;Ljava/lang/String;I)V
    :try_end_0
    .catch Lcom/dropbox/sync/android/aw; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/dropbox/sync/android/ax; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/dropbox/sync/android/DbxException; {:try_start_0 .. :try_end_0} :catch_2

    .line 47
    :goto_0
    return-object v0

    .line 41
    :catch_0
    move-exception v0

    .line 42
    new-instance v0, Ldbxyzptlk/db231222/p/i;

    invoke-direct {v0, v4, v4, v6}, Ldbxyzptlk/db231222/p/i;-><init>(Lcom/dropbox/sync/android/DbxSharedFolderInfo;Ljava/lang/String;I)V

    goto :goto_0

    .line 43
    :catch_1
    move-exception v0

    .line 44
    new-instance v0, Ldbxyzptlk/db231222/p/i;

    invoke-direct {v0, v4, v4, v6}, Ldbxyzptlk/db231222/p/i;-><init>(Lcom/dropbox/sync/android/DbxSharedFolderInfo;Ljava/lang/String;I)V

    goto :goto_0

    .line 45
    :catch_2
    move-exception v0

    move-object v1, v0

    .line 46
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Failed to fetch shared folder metadata."

    invoke-static {v0, v2, v1}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 47
    new-instance v0, Ldbxyzptlk/db231222/p/i;

    invoke-static {v1}, Lcom/dropbox/sync/android/bx;->a(Lcom/dropbox/sync/android/DbxException;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v4, v1, v5}, Ldbxyzptlk/db231222/p/i;-><init>(Lcom/dropbox/sync/android/DbxSharedFolderInfo;Ljava/lang/String;I)V

    goto :goto_0
.end method
