.class public abstract Ldbxyzptlk/db231222/p/j;
.super Ldbxyzptlk/db231222/C/a;
.source "panda.py"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ReturnType:",
        "Ljava/lang/Object;",
        ">",
        "Ldbxyzptlk/db231222/C/a",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "TReturnType;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/dropbox/sync/android/aV;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/base/BaseUserActivity;Lcom/dropbox/sync/android/aV;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Ldbxyzptlk/db231222/C/a;-><init>(Landroid/content/Context;)V

    .line 29
    iput-object p2, p0, Ldbxyzptlk/db231222/p/j;->d:Lcom/dropbox/sync/android/aV;

    .line 30
    iput-object p3, p0, Ldbxyzptlk/db231222/p/j;->a:Ljava/lang/String;

    .line 31
    iput-object p4, p0, Ldbxyzptlk/db231222/p/j;->b:Ljava/lang/String;

    .line 32
    iput-object p5, p0, Ldbxyzptlk/db231222/p/j;->c:Ljava/lang/String;

    .line 34
    invoke-virtual {p0}, Ldbxyzptlk/db231222/p/j;->f()V

    .line 35
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Ldbxyzptlk/db231222/p/j;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Ldbxyzptlk/db231222/p/j;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(Ljava/lang/String;)Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;

    move-result-object v0

    .line 44
    check-cast p1, Lcom/dropbox/android/activity/base/BaseUserActivity;

    invoke-virtual {p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 46
    :cond_0
    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "A shared folder async task failed."

    invoke-static {v0, v2, p2}, Ldbxyzptlk/db231222/i/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, p1

    .line 68
    check-cast v0, Lcom/dropbox/android/activity/base/BaseUserActivity;

    .line 69
    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/BaseUserActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->b(Landroid/support/v4/app/FragmentManager;)V

    .line 72
    instance-of v0, p2, Lcom/dropbox/sync/android/aw;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/dropbox/sync/android/ax;

    if-eqz v0, :cond_3

    .line 73
    :cond_0
    const v0, 0x7f0d00c8

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 78
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2

    .line 79
    :cond_1
    iget-object v0, p0, Ldbxyzptlk/db231222/p/j;->c:Ljava/lang/String;

    .line 82
    :cond_2
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0d023f

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0d0014

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 87
    return-void

    .line 74
    :cond_3
    instance-of v0, p2, Lcom/dropbox/sync/android/DbxException;

    if-eqz v0, :cond_4

    .line 75
    check-cast p2, Lcom/dropbox/sync/android/DbxException;

    invoke-static {p2}, Lcom/dropbox/sync/android/bx;->a(Lcom/dropbox/sync/android/DbxException;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method protected a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TReturnType;)V"
        }
    .end annotation

    .prologue
    .line 53
    check-cast p1, Lcom/dropbox/android/activity/base/BaseUserActivity;

    .line 54
    invoke-virtual {p1}, Lcom/dropbox/android/activity/base/BaseUserActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->b(Landroid/support/v4/app/FragmentManager;)V

    .line 56
    iget-object v0, p0, Ldbxyzptlk/db231222/p/j;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Ldbxyzptlk/db231222/p/j;->b:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/dropbox/android/util/bk;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 59
    :cond_0
    return-void
.end method

.method protected final b()Lcom/dropbox/sync/android/aV;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Ldbxyzptlk/db231222/p/j;->d:Lcom/dropbox/sync/android/aV;

    return-object v0
.end method
