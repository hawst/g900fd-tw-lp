.class public final Ldbxyzptlk/db231222/P/a;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:Ljava/lang/Object;

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ldbxyzptlk/db231222/P/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/P/a;->a:Ljava/lang/Object;

    .line 86
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/db231222/P/a;->b:Ljava/util/Map;

    .line 26
    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)Ldbxyzptlk/db231222/P/b;
    .locals 5

    .prologue
    .line 105
    const-string v0, "EnterpriseAuthentication"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getServiceConnection for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    iget-object v0, p0, Ldbxyzptlk/db231222/P/a;->b:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/db231222/P/b;

    .line 108
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ldbxyzptlk/db231222/P/b;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 139
    :cond_0
    :goto_0
    return-object v0

    .line 113
    :cond_1
    if-nez v0, :cond_2

    .line 115
    new-instance v0, Ldbxyzptlk/db231222/P/b;

    invoke-direct {v0, p0, p2}, Ldbxyzptlk/db231222/P/b;-><init>(Ldbxyzptlk/db231222/P/a;Ljava/lang/String;)V

    .line 116
    iget-object v1, p0, Ldbxyzptlk/db231222/P/a;->b:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.safe.auth"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 121
    invoke-virtual {v1, p2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 123
    const-string v2, "EnterpriseAuthentication"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Bind to service:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v0, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 125
    iget-object v1, p0, Ldbxyzptlk/db231222/P/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 126
    :goto_1
    :try_start_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/P/b;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 125
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    :cond_3
    invoke-virtual {v0}, Ldbxyzptlk/db231222/P/b;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 137
    iget-object v1, p0, Ldbxyzptlk/db231222/P/a;->b:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 128
    :cond_4
    :try_start_1
    iget-object v2, p0, Ldbxyzptlk/db231222/P/a;->a:Ljava/lang/Object;

    const-wide/16 v3, 0x2710

    invoke-virtual {v2, v3, v4}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 129
    :catch_0
    move-exception v2

    goto :goto_1

    .line 125
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method static synthetic a(Ldbxyzptlk/db231222/P/a;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ldbxyzptlk/db231222/P/a;->a:Ljava/lang/Object;

    return-object v0
.end method

.method private a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    const-string v0, "com.samsung.safe.auth.type.activedirectory"

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ldbxyzptlk/db231222/P/g;
    .locals 4

    .prologue
    .line 151
    invoke-direct {p0}, Ldbxyzptlk/db231222/P/a;->a()Ljava/lang/String;

    move-result-object v1

    .line 152
    invoke-direct {p0, p1, v1}, Ldbxyzptlk/db231222/P/a;->a(Landroid/content/Context;Ljava/lang/String;)Ldbxyzptlk/db231222/P/b;

    move-result-object v0

    .line 153
    invoke-virtual {v0}, Ldbxyzptlk/db231222/P/b;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 154
    new-instance v0, Ldbxyzptlk/db231222/P/d;

    const-string v1, "Failed to find security provider."

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/P/d;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157
    :cond_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/P/b;->b()Lcom/centrify/auth/aidl/b;

    move-result-object v0

    .line 160
    :try_start_0
    invoke-interface {v0}, Lcom/centrify/auth/aidl/b;->a()Lcom/centrify/auth/aidl/AuthUserInformation;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 166
    const/4 v0, 0x0

    .line 167
    invoke-virtual {v2}, Lcom/centrify/auth/aidl/AuthUserInformation;->c()Z

    move-result v3

    if-nez v3, :cond_1

    .line 169
    invoke-virtual {v2}, Lcom/centrify/auth/aidl/AuthUserInformation;->d()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 182
    :goto_0
    new-instance v1, Ldbxyzptlk/db231222/P/c;

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/P/c;-><init>(Ljava/lang/String;)V

    throw v1

    .line 161
    :catch_0
    move-exception v0

    .line 162
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 163
    new-instance v1, Ldbxyzptlk/db231222/P/c;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/P/c;-><init>(Ljava/lang/String;)V

    throw v1

    .line 171
    :pswitch_0
    new-instance v0, Ldbxyzptlk/db231222/P/e;

    invoke-direct {v0}, Ldbxyzptlk/db231222/P/e;-><init>()V

    throw v0

    .line 173
    :pswitch_1
    const-string v0, "You are not allowed to run, please contact your IT administrator."

    goto :goto_0

    .line 177
    :pswitch_2
    const-string v0, "You are not authenticated."

    goto :goto_0

    .line 186
    :cond_1
    new-instance v0, Ldbxyzptlk/db231222/P/g;

    invoke-direct {v0}, Ldbxyzptlk/db231222/P/g;-><init>()V

    .line 187
    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/P/g;->a(Ljava/lang/String;)V

    .line 188
    invoke-virtual {v2}, Lcom/centrify/auth/aidl/AuthUserInformation;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/db231222/P/g;->b(Ljava/lang/String;)V

    .line 189
    invoke-virtual {v2}, Lcom/centrify/auth/aidl/AuthUserInformation;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/db231222/P/g;->a(J)V

    .line 190
    return-object v0

    .line 169
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 252
    invoke-direct {p0}, Ldbxyzptlk/db231222/P/a;->a()Ljava/lang/String;

    move-result-object v0

    .line 253
    invoke-direct {p0, p3, v0}, Ldbxyzptlk/db231222/P/a;->a(Landroid/content/Context;Ljava/lang/String;)Ldbxyzptlk/db231222/P/b;

    move-result-object v0

    .line 254
    invoke-virtual {v0}, Ldbxyzptlk/db231222/P/b;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 255
    new-instance v0, Ldbxyzptlk/db231222/P/d;

    const-string v1, "Failed to find security provider."

    invoke-direct {v0, v1}, Ldbxyzptlk/db231222/P/d;-><init>(Ljava/lang/String;)V

    throw v0

    .line 258
    :cond_0
    invoke-virtual {v0}, Ldbxyzptlk/db231222/P/b;->b()Lcom/centrify/auth/aidl/b;

    move-result-object v0

    .line 261
    :try_start_0
    invoke-interface {v0, p1, p2}, Lcom/centrify/auth/aidl/b;->a(Ljava/lang/String;Z)Lcom/centrify/auth/aidl/SecurityTokenResponse;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 267
    invoke-virtual {v0}, Lcom/centrify/auth/aidl/SecurityTokenResponse;->d()I

    move-result v1

    .line 269
    packed-switch v1, :pswitch_data_0

    .line 280
    :pswitch_0
    new-instance v2, Ldbxyzptlk/db231222/P/c;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to get token, responseCode = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ",message:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/centrify/auth/aidl/SecurityTokenResponse;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ldbxyzptlk/db231222/P/c;-><init>(Ljava/lang/String;)V

    throw v2

    .line 262
    :catch_0
    move-exception v0

    .line 263
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 264
    new-instance v1, Ldbxyzptlk/db231222/P/c;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ldbxyzptlk/db231222/P/c;-><init>(Ljava/lang/String;)V

    throw v1

    .line 271
    :pswitch_1
    invoke-virtual {v0}, Lcom/centrify/auth/aidl/SecurityTokenResponse;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 274
    :pswitch_2
    new-instance v0, Ldbxyzptlk/db231222/P/e;

    invoke-direct {v0}, Ldbxyzptlk/db231222/P/e;-><init>()V

    throw v0

    .line 277
    :pswitch_3
    new-instance v0, Ldbxyzptlk/db231222/P/f;

    invoke-direct {v0}, Ldbxyzptlk/db231222/P/f;-><init>()V

    throw v0

    .line 269
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
