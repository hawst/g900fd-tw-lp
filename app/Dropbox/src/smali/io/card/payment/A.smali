.class public final Lio/card/payment/A;
.super Ljava/lang/Object;

# interfaces
.implements Lio/card/payment/k;


# static fields
.field private static a:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lio/card/payment/A;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lio/card/payment/A;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->a:Lio/card/payment/am;

    const-string v2, "\uce74\ub4dc\ub97c \uc2a4\uce94\ud560 \uc218 \uc788\ub294 \uc560\ud50c\ub9ac\ucf00\uc774\uc158\uc774 \uc544\ub2d9\ub2c8\ub2e4."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/A;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->b:Lio/card/payment/am;

    const-string v2, "\ucde8\uc18c"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/A;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->c:Lio/card/payment/am;

    const-string v2, "\uc644\ub8cc"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/A;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->d:Lio/card/payment/am;

    const-string v2, "CVV"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/A;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->e:Lio/card/payment/am;

    const-string v2, "\uc6b0\ud3b8\ubc88\ud638"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/A;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->f:Lio/card/payment/am;

    const-string v2, "\uc720\ud6a8\uae30\uac04"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/A;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->g:Lio/card/payment/am;

    const-string v2, "\ubc88\ud638"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/A;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->h:Lio/card/payment/am;

    const-string v2, "\uce74\ub4dc"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/A;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->i:Lio/card/payment/am;

    const-string v2, "MM / YY"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/A;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->j:Lio/card/payment/am;

    const-string v2, "\ud655\uc778"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/A;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->k:Lio/card/payment/am;

    const-string v2, "\uce74\ub4dc\ub97c \uc5ec\uae30\uc5d0 \uac16\ub2e4 \ub300\uc138\uc694.\n\uc790\ub3d9\uc73c\ub85c \uc2a4\uce94\ub429\ub2c8\ub2e4."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/A;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->l:Lio/card/payment/am;

    const-string v2, "\ud0a4\ubcf4\ub4dc\u2026"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/A;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->m:Lio/card/payment/am;

    const-string v2, "\uce74\ub4dc \ubc88\ud638"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/A;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->n:Lio/card/payment/am;

    const-string v2, "\uce74\ub4dc \uc138\ubd80\uc815\ubcf4"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/A;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->o:Lio/card/payment/am;

    const-string v2, "\uc774\ub7f0!"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/A;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->p:Lio/card/payment/am;

    const-string v2, "\uae30\uae30\uac00 \uce74\uba54\ub77c\ub97c \uc774\uc6a9\ud55c \uce74\ub4dc \uc22b\uc790 \ud310\ub3c5\uc744 \uc9c0\uc6d0\ud558\uc9c0 \uc54a\uc2b5\ub2c8\ub2e4."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/A;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->q:Lio/card/payment/am;

    const-string v2, "\uae30\uae30\uc5d0\uc11c \uce74\uba54\ub77c\ub97c \uc0ac\uc6a9\ud560 \uc218 \uc5c6\uc2b5\ub2c8\ub2e4."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/A;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->r:Lio/card/payment/am;

    const-string v2, "\uae30\uae30\uc5d0\uc11c \uce74\uba54\ub77c\ub97c \uc5ec\ub294 \ub3d9\uc548 \uc608\uc0c1\uce58 \ubabb\ud55c \uc624\ub958\uac00 \ubc1c\uc0dd\ud588\uc2b5\ub2c8\ub2e4."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "ko"

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Enum;)Ljava/lang/String;
    .locals 1

    check-cast p1, Lio/card/payment/am;

    sget-object v0, Lio/card/payment/A;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
