.class public final enum Lio/card/payment/am;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lio/card/payment/am;

.field public static final enum b:Lio/card/payment/am;

.field public static final enum c:Lio/card/payment/am;

.field public static final enum d:Lio/card/payment/am;

.field public static final enum e:Lio/card/payment/am;

.field public static final enum f:Lio/card/payment/am;

.field public static final enum g:Lio/card/payment/am;

.field public static final enum h:Lio/card/payment/am;

.field public static final enum i:Lio/card/payment/am;

.field public static final enum j:Lio/card/payment/am;

.field public static final enum k:Lio/card/payment/am;

.field public static final enum l:Lio/card/payment/am;

.field public static final enum m:Lio/card/payment/am;

.field public static final enum n:Lio/card/payment/am;

.field public static final enum o:Lio/card/payment/am;

.field public static final enum p:Lio/card/payment/am;

.field public static final enum q:Lio/card/payment/am;

.field public static final enum r:Lio/card/payment/am;

.field private static final synthetic s:[Lio/card/payment/am;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lio/card/payment/am;

    const-string v1, "APP_NOT_AUTHORIZED_MESSAGE"

    invoke-direct {v0, v1, v3}, Lio/card/payment/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/card/payment/am;->a:Lio/card/payment/am;

    new-instance v0, Lio/card/payment/am;

    const-string v1, "CANCEL"

    invoke-direct {v0, v1, v4}, Lio/card/payment/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/card/payment/am;->b:Lio/card/payment/am;

    new-instance v0, Lio/card/payment/am;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v5}, Lio/card/payment/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/card/payment/am;->c:Lio/card/payment/am;

    new-instance v0, Lio/card/payment/am;

    const-string v1, "ENTRY_CVV"

    invoke-direct {v0, v1, v6}, Lio/card/payment/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/card/payment/am;->d:Lio/card/payment/am;

    new-instance v0, Lio/card/payment/am;

    const-string v1, "ENTRY_POSTAL_CODE"

    invoke-direct {v0, v1, v7}, Lio/card/payment/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/card/payment/am;->e:Lio/card/payment/am;

    new-instance v0, Lio/card/payment/am;

    const-string v1, "ENTRY_EXPIRES"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lio/card/payment/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/card/payment/am;->f:Lio/card/payment/am;

    new-instance v0, Lio/card/payment/am;

    const-string v1, "ENTRY_NUMBER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lio/card/payment/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/card/payment/am;->g:Lio/card/payment/am;

    new-instance v0, Lio/card/payment/am;

    const-string v1, "ENTRY_TITLE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lio/card/payment/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/card/payment/am;->h:Lio/card/payment/am;

    new-instance v0, Lio/card/payment/am;

    const-string v1, "EXPIRES_PLACEHOLDER"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lio/card/payment/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/card/payment/am;->i:Lio/card/payment/am;

    new-instance v0, Lio/card/payment/am;

    const-string v1, "OK"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lio/card/payment/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/card/payment/am;->j:Lio/card/payment/am;

    new-instance v0, Lio/card/payment/am;

    const-string v1, "SCAN_GUIDE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lio/card/payment/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/card/payment/am;->k:Lio/card/payment/am;

    new-instance v0, Lio/card/payment/am;

    const-string v1, "KEYBOARD"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lio/card/payment/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/card/payment/am;->l:Lio/card/payment/am;

    new-instance v0, Lio/card/payment/am;

    const-string v1, "ENTRY_CARD_NUMBER"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lio/card/payment/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/card/payment/am;->m:Lio/card/payment/am;

    new-instance v0, Lio/card/payment/am;

    const-string v1, "MANUAL_ENTRY_TITLE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lio/card/payment/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/card/payment/am;->n:Lio/card/payment/am;

    new-instance v0, Lio/card/payment/am;

    const-string v1, "WHOOPS"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lio/card/payment/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/card/payment/am;->o:Lio/card/payment/am;

    new-instance v0, Lio/card/payment/am;

    const-string v1, "ERROR_NO_DEVICE_SUPPORT"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lio/card/payment/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/card/payment/am;->p:Lio/card/payment/am;

    new-instance v0, Lio/card/payment/am;

    const-string v1, "ERROR_CAMERA_CONNECT_FAIL"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lio/card/payment/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/card/payment/am;->q:Lio/card/payment/am;

    new-instance v0, Lio/card/payment/am;

    const-string v1, "ERROR_CAMERA_UNEXPECTED_FAIL"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lio/card/payment/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/card/payment/am;->r:Lio/card/payment/am;

    const/16 v0, 0x12

    new-array v0, v0, [Lio/card/payment/am;

    sget-object v1, Lio/card/payment/am;->a:Lio/card/payment/am;

    aput-object v1, v0, v3

    sget-object v1, Lio/card/payment/am;->b:Lio/card/payment/am;

    aput-object v1, v0, v4

    sget-object v1, Lio/card/payment/am;->c:Lio/card/payment/am;

    aput-object v1, v0, v5

    sget-object v1, Lio/card/payment/am;->d:Lio/card/payment/am;

    aput-object v1, v0, v6

    sget-object v1, Lio/card/payment/am;->e:Lio/card/payment/am;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lio/card/payment/am;->f:Lio/card/payment/am;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lio/card/payment/am;->g:Lio/card/payment/am;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lio/card/payment/am;->h:Lio/card/payment/am;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lio/card/payment/am;->i:Lio/card/payment/am;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lio/card/payment/am;->j:Lio/card/payment/am;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lio/card/payment/am;->k:Lio/card/payment/am;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lio/card/payment/am;->l:Lio/card/payment/am;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lio/card/payment/am;->m:Lio/card/payment/am;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lio/card/payment/am;->n:Lio/card/payment/am;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lio/card/payment/am;->o:Lio/card/payment/am;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lio/card/payment/am;->p:Lio/card/payment/am;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lio/card/payment/am;->q:Lio/card/payment/am;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lio/card/payment/am;->r:Lio/card/payment/am;

    aput-object v2, v0, v1

    sput-object v0, Lio/card/payment/am;->s:[Lio/card/payment/am;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lio/card/payment/am;
    .locals 1

    const-class v0, Lio/card/payment/am;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lio/card/payment/am;

    return-object v0
.end method

.method public static values()[Lio/card/payment/am;
    .locals 1

    sget-object v0, Lio/card/payment/am;->s:[Lio/card/payment/am;

    invoke-virtual {v0}, [Lio/card/payment/am;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/card/payment/am;

    return-object v0
.end method
