.class final Lio/card/payment/ad;
.super Ljava/lang/Object;


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private b:Landroid/graphics/Bitmap;

.field private c:Z

.field private final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lio/card/payment/ad;->a:Landroid/graphics/Paint;

    iget-object v0, p0, Lio/card/payment/ad;->a:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lio/card/payment/ad;->a:Landroid/graphics/Paint;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lio/card/payment/ad;->b:Landroid/graphics/Bitmap;

    iput-object p1, p0, Lio/card/payment/ad;->d:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;FF)V
    .locals 7

    const/high16 v2, 0x40000000    # 2.0f

    const/4 v6, 0x0

    iget-object v0, p0, Lio/card/payment/ad;->b:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    invoke-virtual {p0, v6}, Lio/card/payment/ad;->a(Z)V

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Lio/card/payment/ad;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lio/card/payment/ad;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    div-float v1, p3, p2

    cmpg-float v1, v1, v0

    if-gez v1, :cond_1

    div-float p2, p3, v0

    :goto_0
    div-float v0, p2, v2

    div-float v1, p3, v2

    iget-object v2, p0, Lio/card/payment/ad;->b:Landroid/graphics/Bitmap;

    new-instance v3, Landroid/graphics/Rect;

    iget-object v4, p0, Lio/card/payment/ad;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iget-object v5, p0, Lio/card/payment/ad;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-direct {v3, v6, v6, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v4, Landroid/graphics/RectF;

    neg-float v5, v0

    neg-float v6, v1

    invoke-direct {v4, v5, v6, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v0, p0, Lio/card/payment/ad;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void

    :cond_1
    mul-float p3, p2, v0

    goto :goto_0
.end method

.method final a(Z)V
    .locals 3

    const/16 v2, 0xf0

    iget-object v0, p0, Lio/card/payment/ad;->b:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lio/card/payment/ad;->c:Z

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lio/card/payment/ad;->c:Z

    if-eqz p1, :cond_1

    const-string v0, "iVBORw0KGgoAAAANSUhEUgAAAG4AAABGCAYAAAAtpKGgAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAACAdJREFUeNrsXX1MVWUYv94Au+VHaehmZcut5kd+pWlWOhxYUFP+KHSJNO5aKGjqGLhBuhwW6vDakHLTpXclosutkhlQiFBOSWeZn9UcpYYfJB+hJB9XoN9zfS87vr3n696Dcsf7bM+ee973eZ73Oc/vnPe8H+eAzSYpKKmP70dbW1sOxDKrHIeBZHq9ed0PMcMid5+BU5HaG96j5ubmdZ2dnbc6LSQJ2W1CKsotTCthVNJ1x+HAAxFi6a0MkrDdBg4iwkKXHbjRnrSzgxCZ4qAhu8Ph6AJOUpCNTSRwwXrbyRR0O/XrDqe6z7bHQDL3/hPSFwPR16xddXX1HohpeiMfOazveaPRAxqwRMiuMggWR+QzTg5OJEngJEngJHCSJHCSJHCSJHDBT71yO6esrKw/xACuuDYyMrI1qE6kty154bQWgY9zPNkfX6WlpcTDuiFGrZ3ziN66gfo4eAJXNsCsk+Li4oVRUVHz8fNB/K6CTIqJiWm0KMZwjbq+XuDmgHoZcB5BWYcZB4WFhZOio6PX+BKM35NY1TwrAgQk70A8olJ9rFfebuhqVou6H5M+Mu7lo0WOKgPAX04HgpDi4+N300iUK94mpwM9nAoKCs7b7fa5O3bseI0VXU1ISNjZo4Fzu90kBvuOnU5nnUE72sLvZ9bOQAzX4cujo1MHHUuTl5+fX15TU1OunBoYjH2w4tCvuAwDt3XrVhL0AI9KTEwcDjmka4jm8VyEqATvTUpKqufsBkHEgsfBboRN8f6Fjt1UiClcGIfBZ8HL4Gsi5EBWvgInf1JhS/ElQecphU4j2jsHSXdFkxXAoZ136+vrfReMPS4u7grO4ysV3VEsDyNY/nz0N8vD1zRahL11V9bmzZvvx4CJXlM/ozEprAV/A91YZtMHnExlrE7PbhQ3assE3+A4C/w5vXnN+YhSxPo6jss02jsErgh0VMli5OMrEuSOOJ2169GI6yfKMeXaEtByc3MdcOg28X57FjupGHC7CbsStPWAIilrBDrn1FYSWKwv4fdffr6X7w9wPB3kckc6H+gAxpObch4QaC6XixreZKLRvampqSPJFvJZHJ9X1DWAC8CfMtkgsH9FZ66lRrMQK/UKJwL4oMIK4Mq5+iV+xrIp0InqDBXHleC8NBBkISsrxKGy77bhOILdJZUYPs8BDwTfB3aAZ6G8kfO7zQBwN8G7we+z9reDR4JjNWJdr9BtuBvA5eTkDBKcH9ElRe7ywL+IgoH9M4EMTt4SlP2YmZmZAnlqw4YNt0JDQ/dkZ2dT2U4cX1Qq4riitraW1vIuh4eHX4Kk/nsYG+ldAJ8Gv6AwmakTDy1LrURb+fRQh38b2qeV/puI4UOBfgV0UyGPK3SLoZsHObQ7h+vA5WXB+ucFxLMEsoTljvI/BfG4IJ/n7BPT09PT/L3jrgguhum83qpVq4T2WVlZtpSUlPHgt2H3MXgX+DvwMcZV/PdfOnfcCbW2RHcSdEer6GZ19x3HzpWndBU/0wW6ZwLpKv83OCAwjBCSFgb9teDT/nwQqQLcJhOxVpl8BFgN3CG+EhfwYLULXDDybrcSuJ+N2GVkZJDtRg18rjNuMQnceyZiPaShO/UuAPcDX4n5m5ava5YtWAu6Hw/63gEG7CYJTqqZRpR46K4AO4lxvMckcKs12uSnHo2I9SEV3flGgIN9CHg8DbrAZoErEtRHqTwPhwp0GwIBrljgMA9D/a5BDX4TR4NDFXYrBHYraXrB+XdZCNyvosGZMlYW79MoP6IHHPSeQFku+DBNc8BpvC8d4NaJ5nnwMZxrpz8b7fJUHAhw8Spd3calS5fGgWfj93JwNTgbxz67tQKbrlET6YHHouyChcCJuuZ2ZazgBBx/a2Q6wOaaPM0zChwm0RNw3CHQ+ZLiYPHE4XiLSjzxgSx19dVY5moF13Nli5KTk+mEUgX6ZaibD36Vrl7RMyAQ4BDrBPbcNBqrKnAYWD2horPf5ARca8WpnsUlXP6i3Ae6kBrBJo1GiK7wONiMgWxT0akLYFS5WqeHSNNoV0ltWsAVFRWNU7GrMAMc8jCELQCYoSrYTbZZQW63+03qnw00Sl1mpmKtsUlD90/wUQ3gRN3tWr2tHtbuZY12m1TWQSM5UEQfFi7UAe6IIKbRKN+nkwsflUJ/tmXbOk6nc1dISAhtvyxesGDBWMhHbXd+Hnsd/Ed+fj5tmexle1U0U/4H+rNoK0Oh28p0t6OO2l+s0uzv4HJBmVacNsRJ7f4G33NZu8o4qV3aNHOh/kXOvI7ba1sOnTXMB8V8GGVbOBs+vhOCmM4ipjdY7mYIYiLfl+D7FOR66Ndauh+XkJBwHiLdbrfTstVE250fpV+lJTDoKPW969TQ/wRSudpCe2GVVI862g34XmWH+QuIfVzxTQNxktgJ33QRTePiPIj6Fu/mmd0+lzNt5PychE4s89HEnx+LkffhUYmpheXCJYiJfB/3xSVJkiRJkiwj75f90SA1hRKQTNPdJ0BC79sMUqk+6vvreaoLmvKv4N0bol0ViDFquMoXYnsuXdOoa5XABSlJ4CRwkiRwkiRwEjhJEjhJEjgJnCQJnCQJXK8mzRdidXfAh4L8abUGJHPvzd/DEP78gyjNt7x0dwf8JbmrwG6bzs6jEM9Z7Ham7Cq7n/6VzzhJErhg74F9wLXLXAQNdbS0tDR4gfN4PB/ZTP71OEn3BjTwAYfDcdILXFhYGH3imivz0uOJ/kcqvRVt+0+AAQBy6IkzldTBagAAAABJRU5ErkJggg=="

    iget-object v1, p0, Lio/card/payment/ad;->d:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lio/card/payment/m;->a(Ljava/lang/String;Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lio/card/payment/ad;->b:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_1
    const-string v0, "iVBORw0KGgoAAAANSUhEUgAAAJ4AAAAyCAYAAACtW2LuAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABmBJREFUeNrsXdFx2zgQpTT+P10F5lVgXgVhKrCugsgVRK7AdgVyKlBSgZQKRFUgugIxFYhXAQ+8Wfp4GAC7CyxsjYU3w/FkbBLA42Kx7wGcZFlCQkJCQkJCQsJHw4Tzx13XzdWPlbpyZjsV/KzV9Utd28lk0lwi4YrDHDicedzequtFXT131TlxqMbVj2ehrlvtV30fv6m+1t4PVtepk8NOXeUFBt7hI3IIfbH2M+TBZRcH60vKdh+RQ8K4Nvo9U8bzY82sherY6kJir4jI4fqMx/USEnifInZ8eSHLbkwOF1CDn+O4qnPMeAO+pIwXjIdzHJcSFpXvGl5Q6gxQNnpduCLWKacLqPEw9MKj0GqnErgl4QzHdfC2U9TNS7AAbGhUVP/hEibqB6ps1DMmBoleQLa9BhtnZplhLdg1DdQURssGXuywJF2PrKG9up7VPS2D8ME+MFkje/WsRyYHv9vah34fCN36rGcYaLuA8RaOFWzgsAU+KswGIYyr5/TeN6I3oaqKYiPoL1XIbsgZsv+oZ22HtYSNZ6fd8+iTGbRnUDJfqa1UJwEOC1dSQu5fmO6j1nhs1WKZTRxIKN3+JRxcxGnIiXXYkvB3P7R/33ALcAN+Mce/zvyMahOHcylhQQo8yBi5AGksz1CAsAEzeAGvZUGIiAI+sCK+VsvLd6Y4o0ze36gTfFSmSGFtWQ1cbbS23RVKxsM63xLqgBmB+Cqigi5GWQ97wdcCmfjeUJ/NBCbvnPEupDmc6e0TkpI1Lq5ieDSWlI/hp6PNOvtvn3cofodB3xBeSDZ68dieYY5kYqyt7wb7AJu8DbbvCgKPs/LobTYw9peRiBgmxA0E6ozJDTaufUjgeS8RkOnWxMDYah1uoU6qMKUJGWVHXJ7rgPFiE6jVsx1x8taEoKNk2m/aM7cwoW0HCirtXe2Yy7NEUgrynnLNu5uDAqMqqkeBunDFUHtYv3IP9WYdB6jljqOmgccl4d7wjXi68l4yHIKO4hBYl5YuPg4BRA0Ga4nZG0zCSoN9ggXr0WG9xMbJNFmY77kk2GZlqHFMXWqlC1TTEvOZIEx0E7nIeKq3NtQeJTLuShMUWHt378RhC6YxViPqJjJb9Y5rV8Leeh0SeDE3tfv6485Uv40OFX4RsgQqQ6FNUrZQPy6wsTj2I2Ny2I/jL5urMNpZmUfg0FtYUAKviETWvSJrawm4vo74KujjmUjAAi9n2Cc2QRGTwx5PmWWLDwLuIeOfFOdwGCSYnEpRuA7Z2LZPRu0dItVAOVM0nYYXGCqMhMdxBAGQO+rJXSQO5wzBhB74uIpU31WjWVJjlghkug1xho4PAgx+1MqRWWweWeNob0bcoeif8YzUVaE18DDWLeG7Baod0sC1H72vr8iSrFsvuXi2g4evCUpKpHAmtrW0yXOfAwyEzHDkZgFfC2akKkOU6SOxLVu2PFAVKthlcewxAvFLoaDLCUFXBFg+y4AXFeSbSZzqYfB48p0kBMtnzeQOTUhTWzBkb3cwgLIFVQeUBJX4cuC2Tzh92wsFXYmIscok5gL6GSwspp5KrPX+TtJQTyG//xv5/a1nP5uAPj8RfLM8kzkYQEERGOC3zH66ArWmHKadenpPUkFHaevGsURjxXTtMEN9x+AUFIws0gh+kD3z4RiW2BXiU/7vaBPhbCOJ16v3XCKImWcOAbYfKdFPRNW9J5DE9dmeiMfj33LyopMABMJPaDeHCb3wMI5FYuPKM3VXgqS8ELOHj4KuCEHPCbzKcMAz1vLHHecDoT8+ZvaesgJJGMfowQDJqRjx6/qO0DZX2RbEMVEOBpTCPB4j0Vgy2iF/KTgVVIlegPqB+xXSsE1VBc48zux8ZtSFFHEmyqMHh/+WDb1rwOxnHiU2IAOdHKZqlL1HMFsp5/dez/47zuCR+sk49nXinC1DOOxcW4eBHM6JHG4Gfhxm8MnUT4fRzIqNSXZGAAtiDoX5TKvF+lpjqxf2Bs+xZn4bSykd7hi13etya8l8rP55cDic7NE5bEccNgiHzq//Dd+QSNprl4GYB1UTElzL+5uKgIQUdJTj7OvEVIJ04K0kBUVCAlV1dm9x+iYhYRx46Fm8xFKCdNCVSVAkvEfgYVtLm8RSgnTQLQiCIk9Mxcf0wsbr2jHoT+j+ean/8UtCQkJCQkJCQkJCKP4RYAAu5uOH6xFvdgAAAABJRU5ErkJggg=="

    iget-object v1, p0, Lio/card/payment/ad;->d:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lio/card/payment/m;->a(Ljava/lang/String;Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lio/card/payment/ad;->b:Landroid/graphics/Bitmap;

    goto :goto_0
.end method
