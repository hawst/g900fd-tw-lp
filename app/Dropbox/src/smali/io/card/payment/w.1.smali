.class public final Lio/card/payment/w;
.super Ljava/lang/Object;

# interfaces
.implements Lio/card/payment/k;


# static fields
.field private static a:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lio/card/payment/w;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lio/card/payment/w;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->a:Lio/card/payment/am;

    const-string v2, "Cette application n\u2019est pas autoris\u00e9e \u00e0 scanner la carte."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/w;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->b:Lio/card/payment/am;

    const-string v2, "Annuler"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/w;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->c:Lio/card/payment/am;

    const-string v2, "OK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/w;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->d:Lio/card/payment/am;

    const-string v2, "Crypto."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/w;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->e:Lio/card/payment/am;

    const-string v2, "Code postal"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/w;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->f:Lio/card/payment/am;

    const-string v2, "Date d\u2019expiration"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/w;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->g:Lio/card/payment/am;

    const-string v2, "Num\u00e9ro"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/w;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->h:Lio/card/payment/am;

    const-string v2, "Carte"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/w;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->i:Lio/card/payment/am;

    const-string v2, "MM/AA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/w;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->j:Lio/card/payment/am;

    const-string v2, "OK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/w;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->k:Lio/card/payment/am;

    const-string v2, "Maintenez la carte \u00e0 cet endroit.\nElle va \u00eatre automatiquement scann\u00e9e."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/w;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->l:Lio/card/payment/am;

    const-string v2, "Clavier\u2026"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/w;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->m:Lio/card/payment/am;

    const-string v2, "N\u00ba de carte"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/w;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->n:Lio/card/payment/am;

    const-string v2, "Carte"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/w;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->o:Lio/card/payment/am;

    const-string v2, "D\u00e9sol\u00e9"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/w;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->p:Lio/card/payment/am;

    const-string v2, "Cet appareil ne peut pas utiliser l\u2019appareil photo pour lire les num\u00e9ros de carte."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/w;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->q:Lio/card/payment/am;

    const-string v2, "L\u2019appareil photo n\u2019est pas disponible."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/w;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->r:Lio/card/payment/am;

    const-string v2, "Une erreur s\u2019est produite en ouvrant l\u2019appareil photo."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "fr"

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Enum;)Ljava/lang/String;
    .locals 1

    check-cast p1, Lio/card/payment/am;

    sget-object v0, Lio/card/payment/w;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
