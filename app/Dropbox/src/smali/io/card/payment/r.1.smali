.class public final Lio/card/payment/r;
.super Ljava/lang/Object;

# interfaces
.implements Lio/card/payment/k;


# static fields
.field private static a:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lio/card/payment/r;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lio/card/payment/r;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->a:Lio/card/payment/am;

    const-string v2, "This application is not authorised for card scanning."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/r;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->b:Lio/card/payment/am;

    const-string v2, "Cancel"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/r;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->c:Lio/card/payment/am;

    const-string v2, "Done"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/r;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->d:Lio/card/payment/am;

    const-string v2, "CVV"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/r;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->e:Lio/card/payment/am;

    const-string v2, "Postcode"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/r;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->f:Lio/card/payment/am;

    const-string v2, "Expires"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/r;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->g:Lio/card/payment/am;

    const-string v2, "Number"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/r;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->h:Lio/card/payment/am;

    const-string v2, "Card"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/r;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->i:Lio/card/payment/am;

    const-string v2, "MM/YY"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/r;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->j:Lio/card/payment/am;

    const-string v2, "OK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/r;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->k:Lio/card/payment/am;

    const-string v2, "Hold card here.\nIt will scan automatically."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/r;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->l:Lio/card/payment/am;

    const-string v2, "Keyboard\u2026"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/r;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->m:Lio/card/payment/am;

    const-string v2, "Card Number"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/r;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->n:Lio/card/payment/am;

    const-string v2, "Card Details"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/r;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->o:Lio/card/payment/am;

    const-string v2, "Whoops!"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/r;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->p:Lio/card/payment/am;

    const-string v2, "This device cannot use the camera to read card numbers."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/r;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->q:Lio/card/payment/am;

    const-string v2, "Device camera is unavailable."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/r;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->r:Lio/card/payment/am;

    const-string v2, "The device had an unexpected error opening the camera."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "en_GB"

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Enum;)Ljava/lang/String;
    .locals 1

    check-cast p1, Lio/card/payment/am;

    sget-object v0, Lio/card/payment/r;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
