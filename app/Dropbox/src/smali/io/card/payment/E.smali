.class public final Lio/card/payment/E;
.super Ljava/lang/Object;

# interfaces
.implements Lio/card/payment/k;


# static fields
.field private static a:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lio/card/payment/E;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lio/card/payment/E;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->a:Lio/card/payment/am;

    const-string v2, "Ta aplikacja nie jest autoryzowana do skanowania karty."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/E;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->b:Lio/card/payment/am;

    const-string v2, "Anuluj"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/E;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->c:Lio/card/payment/am;

    const-string v2, "Gotowe"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/E;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->d:Lio/card/payment/am;

    const-string v2, "Kod CVV2/CVC2"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/E;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->e:Lio/card/payment/am;

    const-string v2, "Kod pocztowy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/E;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->f:Lio/card/payment/am;

    const-string v2, "Wygasa"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/E;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->g:Lio/card/payment/am;

    const-string v2, "Numer"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/E;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->h:Lio/card/payment/am;

    const-string v2, "Karta"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/E;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->i:Lio/card/payment/am;

    const-string v2, "MM/RR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/E;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->j:Lio/card/payment/am;

    const-string v2, "OK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/E;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->k:Lio/card/payment/am;

    const-string v2, "Przytrzymaj kart\u0119 tutaj.\nZostanie ona zeskanowana automatycznie."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/E;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->l:Lio/card/payment/am;

    const-string v2, "Klawiatura\u2026"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/E;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->m:Lio/card/payment/am;

    const-string v2, "Numer karty"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/E;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->n:Lio/card/payment/am;

    const-string v2, "Dane karty"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/E;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->o:Lio/card/payment/am;

    const-string v2, "Ups!"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/E;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->p:Lio/card/payment/am;

    const-string v2, "Na tym urz\u0105dzeniu nie mo\u017cna odczyta\u0107 numeru karty za pomoc\u0105 aparatu."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/E;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->q:Lio/card/payment/am;

    const-string v2, "Aparat na tym urz\u0105dzeniu jest niedostepny."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/E;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->r:Lio/card/payment/am;

    const-string v2, "Przy otwieraniu aparatu na tym urz\u0105dzeniu wyst\u0105pi\u0142 nieoczekiwany b\u0142\u0105d."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "pl"

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Enum;)Ljava/lang/String;
    .locals 1

    check-cast p1, Lio/card/payment/am;

    sget-object v0, Lio/card/payment/E;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
