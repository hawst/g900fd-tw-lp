.class public final Lio/card/payment/D;
.super Ljava/lang/Object;

# interfaces
.implements Lio/card/payment/k;


# static fields
.field private static a:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lio/card/payment/D;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lio/card/payment/D;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->a:Lio/card/payment/am;

    const-string v2, "Deze toepassing is niet goedgekeurd voor het scannen van creditcards."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/D;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->b:Lio/card/payment/am;

    const-string v2, "Annuleren"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/D;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->c:Lio/card/payment/am;

    const-string v2, "Gereed"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/D;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->d:Lio/card/payment/am;

    const-string v2, "CVV"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/D;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->e:Lio/card/payment/am;

    const-string v2, "Postcode"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/D;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->f:Lio/card/payment/am;

    const-string v2, "Vervaldatum"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/D;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->g:Lio/card/payment/am;

    const-string v2, "Nummer"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/D;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->h:Lio/card/payment/am;

    const-string v2, "Creditcard"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/D;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->i:Lio/card/payment/am;

    const-string v2, "MM/JJ"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/D;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->j:Lio/card/payment/am;

    const-string v2, "OK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/D;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->k:Lio/card/payment/am;

    const-string v2, "Houd kaart hier.\nScannen gaat automatisch."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/D;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->l:Lio/card/payment/am;

    const-string v2, "Toetsenbord\u2026"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/D;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->m:Lio/card/payment/am;

    const-string v2, "Creditcardnummer"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/D;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->n:Lio/card/payment/am;

    const-string v2, "Kaartgegevens"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/D;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->o:Lio/card/payment/am;

    const-string v2, "Oeps!"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/D;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->p:Lio/card/payment/am;

    const-string v2, "Met de camera van dit apparaat kunnen geen kaartnummers worden gelezen."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/D;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->q:Lio/card/payment/am;

    const-string v2, "Camera apparaat niet beschikbaar."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lio/card/payment/D;->a:Ljava/util/Map;

    sget-object v1, Lio/card/payment/am;->r:Lio/card/payment/am;

    const-string v2, "Er is een onverwachte fout opgetreden bij het starten van de camera."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "nl"

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Enum;)Ljava/lang/String;
    .locals 1

    check-cast p1, Lio/card/payment/am;

    sget-object v0, Lio/card/payment/D;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
