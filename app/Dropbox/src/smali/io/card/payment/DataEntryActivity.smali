.class public final Lio/card/payment/DataEntryActivity;
.super Landroid/app/Activity;

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private a:I

.field private b:I

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/EditText;

.field private e:Lio/card/payment/ak;

.field private f:Landroid/widget/EditText;

.field private g:Lio/card/payment/ak;

.field private h:Landroid/widget/EditText;

.field private i:Lio/card/payment/ak;

.field private j:Landroid/widget/EditText;

.field private k:Lio/card/payment/ak;

.field private l:Landroid/widget/ImageView;

.field private m:Landroid/widget/Button;

.field private n:Landroid/widget/Button;

.field private o:Lio/card/payment/CreditCard;

.field private p:Z

.field private q:Ljava/lang/String;

.field private final r:Ljava/lang/String;

.field private s:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lio/card/payment/DataEntryActivity;->a:I

    const/16 v0, 0x64

    iput v0, p0, Lio/card/payment/DataEntryActivity;->b:I

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lio/card/payment/DataEntryActivity;->r:Ljava/lang/String;

    return-void
.end method

.method private a()V
    .locals 6

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->o:Lio/card/payment/CreditCard;

    if-nez v0, :cond_0

    new-instance v0, Lio/card/payment/CreditCard;

    invoke-direct {v0}, Lio/card/payment/CreditCard;-><init>()V

    iput-object v0, p0, Lio/card/payment/DataEntryActivity;->o:Lio/card/payment/CreditCard;

    :cond_0
    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->f:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lio/card/payment/DataEntryActivity;->o:Lio/card/payment/CreditCard;

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->g:Lio/card/payment/ak;

    check-cast v0, Lio/card/payment/aa;

    iget v0, v0, Lio/card/payment/aa;->a:I

    iput v0, v1, Lio/card/payment/CreditCard;->expiryMonth:I

    iget-object v1, p0, Lio/card/payment/DataEntryActivity;->o:Lio/card/payment/CreditCard;

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->g:Lio/card/payment/ak;

    check-cast v0, Lio/card/payment/aa;

    iget v0, v0, Lio/card/payment/aa;->b:I

    iput v0, v1, Lio/card/payment/CreditCard;->expiryYear:I

    :cond_1
    new-instance v0, Lio/card/payment/CreditCard;

    iget-object v1, p0, Lio/card/payment/DataEntryActivity;->e:Lio/card/payment/ak;

    invoke-interface {v1}, Lio/card/payment/ak;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lio/card/payment/DataEntryActivity;->o:Lio/card/payment/CreditCard;

    iget v2, v2, Lio/card/payment/CreditCard;->expiryMonth:I

    iget-object v3, p0, Lio/card/payment/DataEntryActivity;->o:Lio/card/payment/CreditCard;

    iget v3, v3, Lio/card/payment/CreditCard;->expiryYear:I

    iget-object v4, p0, Lio/card/payment/DataEntryActivity;->i:Lio/card/payment/ak;

    invoke-interface {v4}, Lio/card/payment/ak;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lio/card/payment/DataEntryActivity;->k:Lio/card/payment/ak;

    invoke-interface {v5}, Lio/card/payment/ak;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lio/card/payment/CreditCard;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "io.card.payment.scanResult"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    sget v0, Lio/card/payment/CardIOActivity;->RESULT_CARD_INFO:I

    invoke-virtual {p0, v0, v1}, Lio/card/payment/DataEntryActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lio/card/payment/DataEntryActivity;->finish()V

    return-void
.end method

.method static synthetic a(Lio/card/payment/DataEntryActivity;)V
    .locals 0

    invoke-direct {p0}, Lio/card/payment/DataEntryActivity;->a()V

    return-void
.end method

.method private b()Landroid/widget/EditText;
    .locals 3

    const/16 v0, 0x64

    :goto_0
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v0}, Lio/card/payment/DataEntryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private c()V
    .locals 2

    iget-object v1, p0, Lio/card/payment/DataEntryActivity;->m:Landroid/widget/Button;

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->e:Lio/card/payment/ak;

    invoke-interface {v0}, Lio/card/payment/ak;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->g:Lio/card/payment/ak;

    invoke-interface {v0}, Lio/card/payment/ak;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->i:Lio/card/payment/ak;

    invoke-interface {v0}, Lio/card/payment/ak;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->k:Lio/card/payment/ak;

    invoke-interface {v0}, Lio/card/payment/ak;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    iget-boolean v0, p0, Lio/card/payment/DataEntryActivity;->p:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->e:Lio/card/payment/ak;

    invoke-interface {v0}, Lio/card/payment/ak;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->g:Lio/card/payment/ak;

    invoke-interface {v0}, Lio/card/payment/ak;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->i:Lio/card/payment/ak;

    invoke-interface {v0}, Lio/card/payment/ak;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->k:Lio/card/payment/ak;

    invoke-interface {v0}, Lio/card/payment/ak;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lio/card/payment/DataEntryActivity;->a()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    const v1, -0xbbbbbc

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->d:Landroid/widget/EditText;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->e:Lio/card/payment/ak;

    invoke-interface {v0}, Lio/card/payment/ak;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->e:Lio/card/payment/ak;

    invoke-interface {v0}, Lio/card/payment/ak;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->d:Landroid/widget/EditText;

    sget v1, Lio/card/payment/l;->d:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextColor(I)V

    :goto_0
    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->h:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->e:Lio/card/payment/ak;

    invoke-interface {v0}, Lio/card/payment/ak;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lio/card/payment/CardType;->fromCardNumber(Ljava/lang/String;)Lio/card/payment/CardType;

    move-result-object v1

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->i:Lio/card/payment/ak;

    check-cast v0, Lio/card/payment/ab;

    invoke-virtual {v1}, Lio/card/payment/CardType;->cvvLength()I

    move-result v1

    iput v1, v0, Lio/card/payment/ab;->a:I

    iget-object v2, p0, Lio/card/payment/DataEntryActivity;->h:Landroid/widget/EditText;

    const/4 v0, 0x4

    if-ne v1, v0, :cond_3

    const-string v0, "1234"

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_2
    invoke-direct {p0}, Lio/card/payment/DataEntryActivity;->c()V

    return-void

    :cond_1
    invoke-direct {p0}, Lio/card/payment/DataEntryActivity;->b()Landroid/widget/EditText;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextColor(I)V

    goto :goto_0

    :cond_3
    const-string v0, "123"

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->f:Landroid/widget/EditText;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-ne p1, v0, :cond_7

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->g:Lio/card/payment/ak;

    invoke-interface {v0}, Lio/card/payment/ak;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->g:Lio/card/payment/ak;

    invoke-interface {v0}, Lio/card/payment/ak;->a()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->f:Landroid/widget/EditText;

    sget v1, Lio/card/payment/l;->d:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextColor(I)V

    goto :goto_2

    :cond_5
    invoke-direct {p0}, Lio/card/payment/DataEntryActivity;->b()Landroid/widget/EditText;

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->f:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextColor(I)V

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->h:Landroid/widget/EditText;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-ne p1, v0, :cond_a

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->i:Lio/card/payment/ak;

    invoke-interface {v0}, Lio/card/payment/ak;->c()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->i:Lio/card/payment/ak;

    invoke-interface {v0}, Lio/card/payment/ak;->a()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->h:Landroid/widget/EditText;

    sget v1, Lio/card/payment/l;->d:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextColor(I)V

    goto :goto_2

    :cond_8
    invoke-direct {p0}, Lio/card/payment/DataEntryActivity;->b()Landroid/widget/EditText;

    goto :goto_2

    :cond_9
    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->h:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextColor(I)V

    goto :goto_2

    :cond_a
    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->j:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->k:Lio/card/payment/ak;

    invoke-interface {v0}, Lio/card/payment/ak;->c()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->k:Lio/card/payment/ak;

    invoke-interface {v0}, Lio/card/payment/ak;->a()Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->j:Landroid/widget/EditText;

    sget v1, Lio/card/payment/l;->d:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextColor(I)V

    goto/16 :goto_2

    :cond_b
    invoke-direct {p0}, Lio/card/payment/DataEntryActivity;->b()Landroid/widget/EditText;

    goto/16 :goto_2

    :cond_c
    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextColor(I)V

    goto/16 :goto_2
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 22

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/card/payment/DataEntryActivity;->r:Ljava/lang/String;

    invoke-static {}, Lio/card/payment/f;->d()Z

    move-result v4

    if-eqz v4, :cond_0

    const v4, 0x103006e

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/app/Activity;->setTheme(I)V

    :goto_0
    invoke-static {}, Lio/card/payment/f;->d()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "12dip"

    :goto_1
    move-object/from16 v0, p0

    iput-object v4, v0, Lio/card/payment/DataEntryActivity;->q:Ljava/lang/String;

    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual/range {p0 .. p0}, Lio/card/payment/DataEntryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    if-nez v5, :cond_2

    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Didn\'t find any extras!"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    const v4, 0x103000c

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/app/Activity;->setTheme(I)V

    goto :goto_0

    :cond_1
    const-string v4, "2dip"

    goto :goto_1

    :cond_2
    invoke-virtual/range {p0 .. p0}, Lio/card/payment/DataEntryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-static {v4}, Lio/card/payment/al;->a(Landroid/content/Intent;)V

    const-string v4, "io.card.payment.intentSenderIsPayPal"

    invoke-virtual {v5, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lio/card/payment/DataEntryActivity;->s:Z

    const-string v4, "4dip"

    move-object/from16 v0, p0

    invoke-static {v4, v0}, Lio/card/payment/m;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v7

    new-instance v8, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    sget v4, Lio/card/payment/l;->c:I

    invoke-virtual {v8, v4}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    new-instance v4, Landroid/widget/ScrollView;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget v6, v0, Lio/card/payment/DataEntryActivity;->a:I

    add-int/lit8 v9, v6, 0x1

    move-object/from16 v0, p0

    iput v9, v0, Lio/card/payment/DataEntryActivity;->a:I

    invoke-virtual {v4, v6}, Landroid/widget/ScrollView;->setId(I)V

    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x1

    const/4 v10, -0x2

    invoke-direct {v9, v6, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v6, 0xa

    invoke-virtual {v9, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v8, v4, v9}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v10, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v6, 0x1

    invoke-virtual {v10, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const/4 v6, -0x1

    const/4 v11, -0x1

    invoke-virtual {v4, v10, v6, v11}, Landroid/widget/ScrollView;->addView(Landroid/view/View;II)V

    new-instance v11, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x1

    invoke-virtual {v11, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v12, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v6, -0x1

    invoke-direct {v12, v4, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const-string v4, "io.card.payment.scanResult"

    invoke-virtual {v5, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lio/card/payment/CreditCard;

    move-object/from16 v0, p0

    iput-object v4, v0, Lio/card/payment/DataEntryActivity;->o:Lio/card/payment/CreditCard;

    const-string v4, "debug_autoAcceptResult"

    invoke-virtual {v5, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lio/card/payment/DataEntryActivity;->p:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/card/payment/DataEntryActivity;->o:Lio/card/payment/CreditCard;

    if-eqz v4, :cond_a

    new-instance v4, Lio/card/payment/T;

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->o:Lio/card/payment/CreditCard;

    iget-object v6, v6, Lio/card/payment/CreditCard;->cardNumber:Ljava/lang/String;

    invoke-direct {v4, v6}, Lio/card/payment/T;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lio/card/payment/DataEntryActivity;->e:Lio/card/payment/ak;

    new-instance v4, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lio/card/payment/DataEntryActivity;->l:Landroid/widget/ImageView;

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v6, -0x1

    const/4 v13, -0x2

    invoke-direct {v4, v6, v13}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->l:Landroid/widget/ImageView;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v6, v13, v14, v15, v7}, Landroid/widget/ImageView;->setPadding(IIII)V

    const/high16 v6, 0x3f800000    # 1.0f

    iput v6, v4, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->l:Landroid/widget/ImageView;

    sget-object v13, Lio/card/payment/CardIOActivity;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v13}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->l:Landroid/widget/ImageView;

    invoke-virtual {v11, v6, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/card/payment/DataEntryActivity;->l:Landroid/widget/ImageView;

    const/4 v6, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const-string v15, "8dip"

    invoke-static {v4, v6, v13, v14, v15}, Lio/card/payment/m;->b(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    new-instance v13, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v14, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v6, -0x2

    invoke-direct {v14, v4, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/4 v4, 0x0

    const-string v6, "4dip"

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-static {v13, v4, v6, v15, v0}, Lio/card/payment/m;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-virtual {v13, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const-string v4, "io.card.payment.requireExpiry"

    invoke-virtual {v5, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v15

    const-string v4, "io.card.payment.requireCVV"

    invoke-virtual {v5, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v16

    const-string v4, "io.card.payment.requireZip"

    invoke-virtual {v5, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "io.card.payment.requirePostalCode"

    invoke-virtual {v5, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    :cond_3
    const/4 v4, 0x1

    move v6, v4

    :goto_3
    if-eqz v15, :cond_e

    new-instance v5, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v17, 0x0

    const/16 v18, -0x1

    const/high16 v19, 0x3f800000    # 1.0f

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v4, v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    sget v18, Lio/card/payment/l;->e:I

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setTextColor(I)V

    sget-object v18, Lio/card/payment/am;->f:Lio/card/payment/am;

    invoke-static/range {v18 .. v18}, Lio/card/payment/al;->a(Lio/card/payment/am;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->q:Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    invoke-static/range {v17 .. v21}, Lio/card/payment/m;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v18, -0x2

    const/16 v19, -0x2

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v5, v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;II)V

    new-instance v17, Landroid/widget/EditText;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lio/card/payment/DataEntryActivity;->f:Landroid/widget/EditText;

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->f:Landroid/widget/EditText;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lio/card/payment/DataEntryActivity;->b:I

    move/from16 v18, v0

    add-int/lit8 v19, v18, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lio/card/payment/DataEntryActivity;->b:I

    invoke-virtual/range {v17 .. v18}, Landroid/widget/EditText;->setId(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->f:Landroid/widget/EditText;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Landroid/widget/EditText;->setMaxLines(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->f:Landroid/widget/EditText;

    move-object/from16 v17, v0

    const/16 v18, 0x6

    invoke-virtual/range {v17 .. v18}, Landroid/widget/EditText;->setImeOptions(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->f:Landroid/widget/EditText;

    move-object/from16 v17, v0

    invoke-virtual/range {p0 .. p0}, Lio/card/payment/DataEntryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v18

    const v19, 0x1010040

    invoke-virtual/range {v17 .. v19}, Landroid/widget/EditText;->setTextAppearance(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->f:Landroid/widget/EditText;

    move-object/from16 v17, v0

    const/16 v18, 0x3

    invoke-virtual/range {v17 .. v18}, Landroid/widget/EditText;->setInputType(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->f:Landroid/widget/EditText;

    move-object/from16 v17, v0

    sget-object v18, Lio/card/payment/am;->i:Lio/card/payment/am;

    invoke-static/range {v18 .. v18}, Lio/card/payment/al;->a(Lio/card/payment/am;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->o:Lio/card/payment/CreditCard;

    move-object/from16 v17, v0

    if-eqz v17, :cond_c

    new-instance v17, Lio/card/payment/aa;

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->o:Lio/card/payment/CreditCard;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lio/card/payment/CreditCard;->expiryMonth:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->o:Lio/card/payment/CreditCard;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lio/card/payment/CreditCard;->expiryYear:I

    move/from16 v19, v0

    invoke-direct/range {v17 .. v19}, Lio/card/payment/aa;-><init>(II)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lio/card/payment/DataEntryActivity;->g:Lio/card/payment/ak;

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->g:Lio/card/payment/ak;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lio/card/payment/ak;->c()Z

    move-result v17

    if-eqz v17, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->f:Landroid/widget/EditText;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->g:Lio/card/payment/ak;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lio/card/payment/ak;->b()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->g:Lio/card/payment/ak;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lio/card/payment/ak;->a()Z

    move-result v17

    if-nez v17, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->f:Landroid/widget/EditText;

    move-object/from16 v17, v0

    sget v18, Lio/card/payment/l;->d:I

    invoke-virtual/range {v17 .. v18}, Landroid/widget/EditText;->setTextColor(I)V

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->f:Landroid/widget/EditText;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->g:Lio/card/payment/ak;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->f:Landroid/widget/EditText;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->f:Landroid/widget/EditText;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Landroid/text/InputFilter;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    new-instance v20, Landroid/text/method/DateKeyListener;

    invoke-direct/range {v20 .. v20}, Landroid/text/method/DateKeyListener;-><init>()V

    aput-object v20, v18, v19

    const/16 v19, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->g:Lio/card/payment/ak;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    invoke-virtual/range {v17 .. v18}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->f:Landroid/widget/EditText;

    move-object/from16 v17, v0

    const/16 v18, -0x1

    const/16 v19, -0x2

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v5, v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;II)V

    invoke-virtual {v13, v5, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v17, 0x0

    const/16 v18, 0x0

    if-nez v16, :cond_5

    if-eqz v6, :cond_d

    :cond_5
    const-string v4, "4dip"

    :goto_5
    const/16 v19, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v5, v0, v1, v4, v2}, Lio/card/payment/m;->b(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_6
    if-eqz v16, :cond_11

    new-instance v17, Landroid/widget/LinearLayout;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, 0x0

    const/16 v18, -0x1

    const/high16 v19, 0x3f800000    # 1.0f

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v5, v4, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    const/4 v4, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v4, Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    sget v18, Lio/card/payment/l;->e:I

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->q:Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-static {v4, v0, v1, v2, v3}, Lio/card/payment/m;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v18, Lio/card/payment/am;->d:Lio/card/payment/am;

    invoke-static/range {v18 .. v18}, Lio/card/payment/al;->a(Lio/card/payment/am;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v18, -0x2

    const/16 v19, -0x2

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v4, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;II)V

    new-instance v4, Landroid/widget/EditText;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lio/card/payment/DataEntryActivity;->h:Landroid/widget/EditText;

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/card/payment/DataEntryActivity;->h:Landroid/widget/EditText;

    move-object/from16 v0, p0

    iget v0, v0, Lio/card/payment/DataEntryActivity;->b:I

    move/from16 v18, v0

    add-int/lit8 v19, v18, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lio/card/payment/DataEntryActivity;->b:I

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setId(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/card/payment/DataEntryActivity;->h:Landroid/widget/EditText;

    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setMaxLines(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/card/payment/DataEntryActivity;->h:Landroid/widget/EditText;

    const/16 v18, 0x6

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setImeOptions(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/card/payment/DataEntryActivity;->h:Landroid/widget/EditText;

    invoke-virtual/range {p0 .. p0}, Lio/card/payment/DataEntryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v18

    const v19, 0x1010040

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Landroid/widget/EditText;->setTextAppearance(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/card/payment/DataEntryActivity;->h:Landroid/widget/EditText;

    const/16 v18, 0x3

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setInputType(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/card/payment/DataEntryActivity;->h:Landroid/widget/EditText;

    const-string v18, "123"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    const/4 v4, 0x4

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->o:Lio/card/payment/CreditCard;

    move-object/from16 v18, v0

    if-eqz v18, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/card/payment/DataEntryActivity;->e:Lio/card/payment/ak;

    invoke-interface {v4}, Lio/card/payment/ak;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lio/card/payment/CardType;->fromCardNumber(Ljava/lang/String;)Lio/card/payment/CardType;

    move-result-object v4

    invoke-virtual {v4}, Lio/card/payment/CardType;->cvvLength()I

    move-result v4

    :cond_6
    new-instance v18, Lio/card/payment/ab;

    move-object/from16 v0, v18

    invoke-direct {v0, v4}, Lio/card/payment/ab;-><init>(I)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lio/card/payment/DataEntryActivity;->i:Lio/card/payment/ak;

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/card/payment/DataEntryActivity;->h:Landroid/widget/EditText;

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Landroid/text/InputFilter;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    new-instance v20, Landroid/text/method/DigitsKeyListener;

    invoke-direct/range {v20 .. v20}, Landroid/text/method/DigitsKeyListener;-><init>()V

    aput-object v20, v18, v19

    const/16 v19, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->i:Lio/card/payment/ak;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/card/payment/DataEntryActivity;->h:Landroid/widget/EditText;

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->i:Lio/card/payment/ak;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/card/payment/DataEntryActivity;->h:Landroid/widget/EditText;

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/card/payment/DataEntryActivity;->h:Landroid/widget/EditText;

    const/16 v18, -0x1

    const/16 v19, -0x2

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v4, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;II)V

    move-object/from16 v0, v17

    invoke-virtual {v13, v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    if-eqz v15, :cond_f

    const-string v4, "4dip"

    move-object v5, v4

    :goto_7
    const/16 v18, 0x0

    if-eqz v6, :cond_10

    const-string v4, "4dip"

    :goto_8
    const/16 v19, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v5, v1, v4, v2}, Lio/card/payment/m;->b(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_9
    if-eqz v6, :cond_13

    new-instance v5, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v6, 0x0

    const/16 v17, -0x1

    const/high16 v18, 0x3f800000    # 1.0f

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v4, v6, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v6, Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    sget v17, Lio/card/payment/l;->e:I

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setTextColor(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->q:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-static {v6, v0, v1, v2, v3}, Lio/card/payment/m;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v17, Lio/card/payment/am;->e:Lio/card/payment/am;

    invoke-static/range {v17 .. v17}, Lio/card/payment/al;->a(Lio/card/payment/am;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v17, -0x2

    const/16 v18, -0x2

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v5, v6, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;II)V

    new-instance v6, Landroid/widget/EditText;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lio/card/payment/DataEntryActivity;->j:Landroid/widget/EditText;

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->j:Landroid/widget/EditText;

    move-object/from16 v0, p0

    iget v0, v0, Lio/card/payment/DataEntryActivity;->b:I

    move/from16 v17, v0

    add-int/lit8 v18, v17, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lio/card/payment/DataEntryActivity;->b:I

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/widget/EditText;->setId(I)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->j:Landroid/widget/EditText;

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/widget/EditText;->setMaxLines(I)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->j:Landroid/widget/EditText;

    const/16 v17, 0x6

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/widget/EditText;->setImeOptions(I)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->j:Landroid/widget/EditText;

    invoke-virtual/range {p0 .. p0}, Lio/card/payment/DataEntryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v17

    const v18, 0x1010040

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v6, v0, v1}, Landroid/widget/EditText;->setTextAppearance(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->j:Landroid/widget/EditText;

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/widget/EditText;->setInputType(I)V

    new-instance v6, Lio/card/payment/ae;

    invoke-direct {v6}, Lio/card/payment/ae;-><init>()V

    move-object/from16 v0, p0

    iput-object v6, v0, Lio/card/payment/DataEntryActivity;->k:Lio/card/payment/ak;

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->j:Landroid/widget/EditText;

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/card/payment/DataEntryActivity;->k:Lio/card/payment/ak;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->j:Landroid/widget/EditText;

    move-object/from16 v0, p0

    invoke-virtual {v6, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->j:Landroid/widget/EditText;

    const/16 v17, -0x1

    const/16 v18, -0x2

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v5, v6, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;II)V

    invoke-virtual {v13, v5, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    if-nez v15, :cond_7

    if-eqz v16, :cond_12

    :cond_7
    const-string v4, "4dip"

    :goto_a
    const/4 v6, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-static {v5, v4, v6, v15, v0}, Lio/card/payment/m;->b(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_b
    invoke-virtual {v11, v13, v14}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v10, v11, v12}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const-string v4, "16dip"

    const-string v5, "20dip"

    const-string v6, "16dip"

    const-string v10, "20dip"

    invoke-static {v11, v4, v5, v6, v10}, Lio/card/payment/m;->b(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget v5, v0, Lio/card/payment/DataEntryActivity;->a:I

    add-int/lit8 v6, v5, 0x1

    move-object/from16 v0, p0

    iput v6, v0, Lio/card/payment/DataEntryActivity;->a:I

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setId(I)V

    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x1

    const/4 v10, -0x2

    invoke-direct {v5, v6, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v6, 0xc

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/4 v6, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v4, v6, v7, v10, v11}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    const/4 v6, 0x2

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getId()I

    move-result v7

    invoke-virtual {v9, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    new-instance v6, Landroid/widget/Button;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lio/card/payment/DataEntryActivity;->m:Landroid/widget/Button;

    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v7, -0x1

    const/4 v9, -0x2

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-direct {v6, v7, v9, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lio/card/payment/DataEntryActivity;->m:Landroid/widget/Button;

    sget-object v9, Lio/card/payment/am;->c:Lio/card/payment/am;

    invoke-static {v9}, Lio/card/payment/al;->a(Lio/card/payment/am;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lio/card/payment/DataEntryActivity;->m:Landroid/widget/Button;

    new-instance v9, Lio/card/payment/X;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lio/card/payment/X;-><init>(Lio/card/payment/DataEntryActivity;)V

    invoke-virtual {v7, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lio/card/payment/DataEntryActivity;->m:Landroid/widget/Button;

    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Landroid/widget/Button;->setEnabled(Z)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lio/card/payment/DataEntryActivity;->m:Landroid/widget/Button;

    invoke-virtual {v4, v7, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->m:Landroid/widget/Button;

    const/4 v7, 0x1

    move-object/from16 v0, p0

    invoke-static {v6, v7, v0}, Lio/card/payment/m;->a(Landroid/view/View;ZLandroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->m:Landroid/widget/Button;

    const-string v7, "5dip"

    const/4 v9, 0x0

    const-string v10, "5dip"

    const/4 v11, 0x0

    invoke-static {v6, v7, v9, v10, v11}, Lio/card/payment/m;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->m:Landroid/widget/Button;

    const-string v7, "8dip"

    const-string v9, "8dip"

    const-string v10, "4dip"

    const-string v11, "8dip"

    invoke-static {v6, v7, v9, v10, v11}, Lio/card/payment/m;->b(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->m:Landroid/widget/Button;

    const/high16 v7, 0x41800000    # 16.0f

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setTextSize(F)V

    new-instance v6, Landroid/widget/Button;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lio/card/payment/DataEntryActivity;->n:Landroid/widget/Button;

    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v7, -0x1

    const/4 v9, -0x2

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-direct {v6, v7, v9, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lio/card/payment/DataEntryActivity;->n:Landroid/widget/Button;

    sget-object v9, Lio/card/payment/am;->b:Lio/card/payment/am;

    invoke-static {v9}, Lio/card/payment/al;->a(Lio/card/payment/am;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lio/card/payment/DataEntryActivity;->n:Landroid/widget/Button;

    new-instance v9, Lio/card/payment/Y;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lio/card/payment/Y;-><init>(Lio/card/payment/DataEntryActivity;)V

    invoke-virtual {v7, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lio/card/payment/DataEntryActivity;->n:Landroid/widget/Button;

    invoke-virtual {v4, v7, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->n:Landroid/widget/Button;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-static {v6, v7, v0}, Lio/card/payment/m;->a(Landroid/view/View;ZLandroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->n:Landroid/widget/Button;

    const-string v7, "5dip"

    const/4 v9, 0x0

    const-string v10, "5dip"

    const/4 v11, 0x0

    invoke-static {v6, v7, v9, v10, v11}, Lio/card/payment/m;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->n:Landroid/widget/Button;

    const-string v7, "4dip"

    const-string v9, "8dip"

    const-string v10, "8dip"

    const-string v11, "8dip"

    invoke-static {v6, v7, v9, v10, v11}, Lio/card/payment/m;->b(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->n:Landroid/widget/Button;

    const/high16 v7, 0x41800000    # 16.0f

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setTextSize(F)V

    invoke-virtual {v8, v4, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-static {}, Lio/card/payment/f;->c()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/app/Activity;->requestWindowFeature(I)Z

    :cond_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lio/card/payment/DataEntryActivity;->setContentView(Landroid/view/View;)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lio/card/payment/DataEntryActivity;->s:Z

    if-eqz v5, :cond_9

    const-string v4, "iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAACXBIWXMAADE2AAAxNgGa50IgAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAABKFJREFUeNrkm29kXWccxz+5uUK4ZDqdUC4hdMolZDKlk+mU0Rmp0gmbzuhsVqnNJmzG2LT2om+6l5vESqnWRolVq7VrITaqI1qL1WLXarGrIRa9xK7fXtzf2R4n59xzzvOckz5pvhznnOs+z3m+z/P7/5zTIyLsJJTYYShbtKkA7wMTQM3x+feBFeN+A1gEmkADuAcsAK28CPdkFOk+4Htg/xYuygZQ1+O8ToQ9RCTLcUIeLf4RkRkR2Ztx3P8dWXV44hGrYC9wHPgF+KRoke4FHgADHtmgq8ArwFoRVrrmGVmAF4FLRbmlA556mkPqNXYMYYBPgeG8CY97TLgPeDdPozUM/Go5mFvqP1vqQ9eBfTrIGjAIHFaj6IIm8JQxARsufvi4pd88l7L/QRGZFpG/HP104J9Pa5/WfthGf5eAUyn/uwKcAZ4Blh1WeUTPDeBHlR4rHbYh/C3QztimAbztQDgguABUgRkbwruBvRYPrzsEE/ct21aMiQv89BtZCdusbltn2Ra2Yt0OnQE+NA1iUYQXs4R7MRmSDdaMMDjAkJndpSE8voXiHGCPQ35tirYZjaUiXDEsXxbMO5Ct6KrY4G7MhNXSEt5vGRC4ED6qgUNWrBq6X42x3omEbfT3XqhskwX9wLRl2yuGsRqJsd6FEHbR33OWLhBgNo3dKSUk/Da1Kxt31Ad8HfaZGfCzMdGDwFiM9e5KeFRFrGiXMgHcAV51kIwPjOvJCLuzGlyUC0gHp4A58yEh7FKreUhJVh1d2HnguiEpUzFxQSJh24R/VFPJ5ZhYdw/54S5w0rh/K2YCF9Lkww90NXxFA3hBvUIQUd1mc92tBTyp51gd3uc52VvAswbZwOhFFRnnMHYuStuwfnUVeD7k62e6jPlsGrfkI+E28BnwkpaJTN89GdNmbpObjCm5/CZ+4QcRqYXG2CsiX3Zp87eIDIW5RZGtekT0hogcjBjjbhG5ltD2WNRiRhGe9GA1p+KKcCIyISJ/JPTxcVzBsOyB/jbU6i4AF4neDu0FXqZTez6QoOengC/i/pA34WZowOH7YAO8pUHDUsgAhTEEnKCzYziY8OwWcESteDxCS75L92Cz4qGqAo5HVUX2cxG5k+H5t0VkNM0zyjkl/K8Bl437ATq7FSNd+qtpwBCEmzWLxH8F+Aj4Km2Dcg7ivGSQ7Qe+0fJokVhTPT2ToBKFEL5pXL9TMNnrwAU1blYvupRD5ZUxx/rV0RzJrahhW9Tk/qZj6XcT4THL4lndmLDRhKpEO8Jih++XlGizCBEpO4rzslELjjN4s8B7XQoCWwpXwvMJ7WeB133KQEqOBbskwmd9S7lKhk8ccNDfqAlb5f+dAO8I24hzUw0MGmBUIla//TgRThLneTxEyaEkm0S47iPhHhGpAr9btH3aEOk/Q9nMOvCEryK9RvbdglmD7HBE6ual/pqET6aMTdc1YH/T+O3wdtHfQKRd2lfoFL/Dr/0957vRssE48F0E2ZaWbLxE0jcP08DpjH3+RI7fKGz1Ch+06PMyHiOJcNb3pepZyi0+Gq1+OhXDkRQll3k671mwnQk/dthxX6b9OwATMfvNqxMG0gAAAABJRU5ErkJggg=="

    const/16 v5, 0xf0

    move-object/from16 v0, p0

    invoke-static {v4, v0, v5}, Lio/card/payment/m;->a(Ljava/lang/String;Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v5

    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual/range {p0 .. p0}, Lio/card/payment/DataEntryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-direct {v4, v6, v5}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    :cond_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lio/card/payment/DataEntryActivity;->c:Landroid/widget/TextView;

    sget-object v6, Lio/card/payment/am;->n:Lio/card/payment/am;

    invoke-static {v6}, Lio/card/payment/al;->a(Lio/card/payment/am;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "card.io - "

    move-object/from16 v0, p0

    invoke-static {v0, v5, v6, v7, v4}, Lio/card/payment/f;->a(Landroid/app/Activity;Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_a
    new-instance v4, Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lio/card/payment/DataEntryActivity;->c:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/card/payment/DataEntryActivity;->c:Landroid/widget/TextView;

    const/high16 v6, 0x41c00000    # 24.0f

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/card/payment/DataEntryActivity;->c:Landroid/widget/TextView;

    sget v6, Lio/card/payment/l;->a:I

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setTextColor(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/card/payment/DataEntryActivity;->c:Landroid/widget/TextView;

    invoke-virtual {v11, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/card/payment/DataEntryActivity;->c:Landroid/widget/TextView;

    const/4 v6, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const-string v15, "8dip"

    invoke-static {v4, v6, v13, v14, v15}, Lio/card/payment/m;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/card/payment/DataEntryActivity;->c:Landroid/widget/TextView;

    const/4 v6, -0x2

    const/4 v13, -0x2

    invoke-static {v4, v6, v13}, Lio/card/payment/m;->a(Landroid/view/View;II)V

    new-instance v4, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const/4 v6, 0x0

    const-string v13, "4dip"

    const/4 v14, 0x0

    const-string v15, "4dip"

    invoke-static {v4, v6, v13, v14, v15}, Lio/card/payment/m;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lio/card/payment/DataEntryActivity;->q:Ljava/lang/String;

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-static {v6, v13, v14, v15, v0}, Lio/card/payment/m;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v13, Lio/card/payment/am;->m:Lio/card/payment/am;

    invoke-static {v13}, Lio/card/payment/al;->a(Lio/card/payment/am;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v13, Lio/card/payment/l;->e:I

    invoke-virtual {v6, v13}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v13, -0x2

    const/4 v14, -0x2

    invoke-virtual {v4, v6, v13, v14}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;II)V

    new-instance v6, Landroid/widget/EditText;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lio/card/payment/DataEntryActivity;->d:Landroid/widget/EditText;

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->d:Landroid/widget/EditText;

    move-object/from16 v0, p0

    iget v13, v0, Lio/card/payment/DataEntryActivity;->b:I

    add-int/lit8 v14, v13, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lio/card/payment/DataEntryActivity;->b:I

    invoke-virtual {v6, v13}, Landroid/widget/EditText;->setId(I)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->d:Landroid/widget/EditText;

    const/4 v13, 0x1

    invoke-virtual {v6, v13}, Landroid/widget/EditText;->setMaxLines(I)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->d:Landroid/widget/EditText;

    const/4 v13, 0x6

    invoke-virtual {v6, v13}, Landroid/widget/EditText;->setImeOptions(I)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->d:Landroid/widget/EditText;

    invoke-virtual/range {p0 .. p0}, Lio/card/payment/DataEntryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    const v14, 0x1010040

    invoke-virtual {v6, v13, v14}, Landroid/widget/EditText;->setTextAppearance(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->d:Landroid/widget/EditText;

    const/4 v13, 0x3

    invoke-virtual {v6, v13}, Landroid/widget/EditText;->setInputType(I)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->d:Landroid/widget/EditText;

    const-string v13, "1234 5678 1234 5678"

    invoke-virtual {v6, v13}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    new-instance v6, Lio/card/payment/T;

    invoke-direct {v6}, Lio/card/payment/T;-><init>()V

    move-object/from16 v0, p0

    iput-object v6, v0, Lio/card/payment/DataEntryActivity;->e:Lio/card/payment/ak;

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->d:Landroid/widget/EditText;

    move-object/from16 v0, p0

    iget-object v13, v0, Lio/card/payment/DataEntryActivity;->e:Lio/card/payment/ak;

    invoke-virtual {v6, v13}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->d:Landroid/widget/EditText;

    move-object/from16 v0, p0

    invoke-virtual {v6, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->d:Landroid/widget/EditText;

    const/4 v13, 0x2

    new-array v13, v13, [Landroid/text/InputFilter;

    const/4 v14, 0x0

    new-instance v15, Landroid/text/method/DigitsKeyListener;

    invoke-direct {v15}, Landroid/text/method/DigitsKeyListener;-><init>()V

    aput-object v15, v13, v14

    const/4 v14, 0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lio/card/payment/DataEntryActivity;->e:Lio/card/payment/ak;

    aput-object v15, v13, v14

    invoke-virtual {v6, v13}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/card/payment/DataEntryActivity;->d:Landroid/widget/EditText;

    const/4 v13, -0x1

    const/4 v14, -0x2

    invoke-virtual {v4, v6, v13, v14}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;II)V

    const/4 v6, -0x1

    const/4 v13, -0x1

    invoke-virtual {v11, v4, v6, v13}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;II)V

    goto/16 :goto_2

    :cond_b
    const/4 v4, 0x0

    move v6, v4

    goto/16 :goto_3

    :cond_c
    new-instance v17, Lio/card/payment/aa;

    invoke-direct/range {v17 .. v17}, Lio/card/payment/aa;-><init>()V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lio/card/payment/DataEntryActivity;->g:Lio/card/payment/ak;

    goto/16 :goto_4

    :cond_d
    const/4 v4, 0x0

    goto/16 :goto_5

    :cond_e
    new-instance v4, Lio/card/payment/N;

    invoke-direct {v4}, Lio/card/payment/N;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lio/card/payment/DataEntryActivity;->g:Lio/card/payment/ak;

    goto/16 :goto_6

    :cond_f
    const/4 v4, 0x0

    move-object v5, v4

    goto/16 :goto_7

    :cond_10
    const/4 v4, 0x0

    goto/16 :goto_8

    :cond_11
    new-instance v4, Lio/card/payment/N;

    invoke-direct {v4}, Lio/card/payment/N;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lio/card/payment/DataEntryActivity;->i:Lio/card/payment/ak;

    goto/16 :goto_9

    :cond_12
    const/4 v4, 0x0

    goto/16 :goto_a

    :cond_13
    new-instance v4, Lio/card/payment/N;

    invoke-direct {v4}, Lio/card/payment/N;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lio/card/payment/DataEntryActivity;->k:Lio/card/payment/ak;

    goto/16 :goto_b
.end method

.method protected final onResume()V
    .locals 3

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->r:Ljava/lang/String;

    invoke-virtual {p0}, Lio/card/payment/DataEntryActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x400

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFlags(II)V

    invoke-direct {p0}, Lio/card/payment/DataEntryActivity;->c()V

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->d:Landroid/widget/EditText;

    if-nez v0, :cond_2

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->f:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->g:Lio/card/payment/ak;

    invoke-interface {v0}, Lio/card/payment/ak;->a()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    :goto_0
    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->d:Landroid/widget/EditText;

    if-nez v0, :cond_0

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->f:Landroid/widget/EditText;

    if-nez v0, :cond_0

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->h:Landroid/widget/EditText;

    if-nez v0, :cond_0

    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->j:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lio/card/payment/DataEntryActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    :cond_1
    iget-object v0, p0, Lio/card/payment/DataEntryActivity;->r:Ljava/lang/String;

    return-void

    :cond_2
    invoke-direct {p0}, Lio/card/payment/DataEntryActivity;->b()Landroid/widget/EditText;

    goto :goto_0
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
