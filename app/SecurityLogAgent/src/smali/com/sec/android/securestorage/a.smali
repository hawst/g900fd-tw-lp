.class public Lcom/sec/android/securestorage/a;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/sec/android/securestorage/SecureStorageJNI;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/sec/android/securestorage/SecureStorageJNI;->a()Lcom/sec/android/securestorage/SecureStorageJNI;

    move-result-object v0

    sput-object v0, Lcom/sec/android/securestorage/a;->a:Lcom/sec/android/securestorage/SecureStorageJNI;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(ZLjava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    new-instance v0, Lcom/sec/android/securestorage/b;

    invoke-direct {v0, p2}, Lcom/sec/android/securestorage/b;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public static a()Z
    .locals 1

    sget-object v0, Lcom/sec/android/securestorage/a;->a:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v0}, Lcom/sec/android/securestorage/SecureStorageJNI;->isSupported()Z

    move-result v0

    return v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Lcom/sec/android/securestorage/b;

    const-string v1, "Error input data name"

    invoke-direct {v0, v1}, Lcom/sec/android/securestorage/b;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;[B)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/a;->b(Ljava/lang/String;)V

    if-nez p2, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Error: data block is null"

    invoke-direct {p0, v0, v3}, Lcom/sec/android/securestorage/a;->a(ZLjava/lang/String;)V

    sget-object v0, Lcom/sec/android/securestorage/a;->a:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/securestorage/SecureStorageJNI;->put(Ljava/lang/String;[B)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v2, "Error saving data"

    invoke-direct {p0, v1, v2}, Lcom/sec/android/securestorage/a;->a(ZLjava/lang/String;)V

    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)[B
    .locals 3

    invoke-direct {p0, p1}, Lcom/sec/android/securestorage/a;->b(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/securestorage/a;->a:Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-virtual {v0, p1}, Lcom/sec/android/securestorage/SecureStorageJNI;->get(Ljava/lang/String;)[B

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Error: input data are incorrect"

    invoke-direct {p0, v0, v2}, Lcom/sec/android/securestorage/a;->a(ZLjava/lang/String;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
