.class public final Lcom/sec/android/securestorage/SecureStorageJNI;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/sec/android/securestorage/SecureStorageJNI;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/sec/android/securestorage/SecureStorageJNI;

    invoke-direct {v0}, Lcom/sec/android/securestorage/SecureStorageJNI;-><init>()V

    sput-object v0, Lcom/sec/android/securestorage/SecureStorageJNI;->a:Lcom/sec/android/securestorage/SecureStorageJNI;

    const-string v0, "secure_storage_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/sec/android/securestorage/SecureStorageJNI;
    .locals 1

    sget-object v0, Lcom/sec/android/securestorage/SecureStorageJNI;->a:Lcom/sec/android/securestorage/SecureStorageJNI;

    return-object v0
.end method


# virtual methods
.method public native get(Ljava/lang/String;)[B
.end method

.method public native isSupported()Z
.end method

.method public native put(Ljava/lang/String;[B)Z
.end method
