.class public Lcom/samsung/android/securitylogagent/services/a/g;
.super Ljava/lang/Object;


# direct methods
.method public static a(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/securitylogagent/services/a/f;
    .locals 2

    const/4 v0, 0x0

    const-string v1, "samsung.intent.action.knox.TIMA_NOTIFICATION"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lcom/samsung/android/securitylogagent/services/a/o;

    invoke-direct {v0, p0}, Lcom/samsung/android/securitylogagent/services/a/o;-><init>(Landroid/content/Context;)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "samsung.intent.action.knox.DENIAL_NOTIFICATION"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Lcom/samsung/android/securitylogagent/services/a/b;

    invoke-direct {v0, p0}, Lcom/samsung/android/securitylogagent/services/a/b;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_2
    const-string v1, "samsung.intent.action.knox.AMS_NOTIFICATION"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/samsung/android/securitylogagent/services/a/a;

    invoke-direct {v0, p0}, Lcom/samsung/android/securitylogagent/services/a/a;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method
