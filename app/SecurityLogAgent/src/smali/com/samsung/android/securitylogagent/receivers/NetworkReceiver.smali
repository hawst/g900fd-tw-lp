.class public Lcom/samsung/android/securitylogagent/receivers/NetworkReceiver;
.super Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    const-string v0, "SecurityLogAgent"

    const-string v1, "NetworkReceiver : Network Change Receiver Called "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/samsung/android/securitylogagent/a/a;

    invoke-direct {v1, p1}, Lcom/samsung/android/securitylogagent/a/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/samsung/android/securitylogagent/a/a;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SecurityLogAgent"

    const-string v2, "NetworkReceiver : Change state "

    invoke-static {v0, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    new-instance v2, Lcom/samsung/android/securitylogagent/b/a;

    invoke-direct {v2, v1, p1}, Lcom/samsung/android/securitylogagent/b/a;-><init>(Lcom/samsung/android/securitylogagent/a/a;Landroid/content/Context;)V

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/samsung/android/securitylogagent/b/c;->d:Lcom/samsung/android/securitylogagent/b/c;

    invoke-virtual {v2, v0}, Lcom/samsung/android/securitylogagent/b/a;->a(Lcom/samsung/android/securitylogagent/b/c;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/samsung/android/securitylogagent/b/c;->c:Lcom/samsung/android/securitylogagent/b/c;

    invoke-virtual {v2, v0}, Lcom/samsung/android/securitylogagent/b/a;->a(Lcom/samsung/android/securitylogagent/b/c;)V

    goto :goto_0
.end method
