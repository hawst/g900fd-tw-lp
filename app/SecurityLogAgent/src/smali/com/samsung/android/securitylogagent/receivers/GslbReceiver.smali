.class public Lcom/samsung/android/securitylogagent/receivers/GslbReceiver;
.super Landroid/support/v4/a/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/a/c;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    const-string v0, "SecurityLogAgent"

    const-string v1, "GslbReceiver : Gslb Receiver Called "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/samsung/android/securitylogagent/a/a;

    invoke-direct {v0, p1}, Lcom/samsung/android/securitylogagent/a/a;-><init>(Landroid/content/Context;)V

    const-string v1, "com.samsung.android.securitylogagent.services.GslbService"

    invoke-static {p1, v1}, Lcom/samsung/android/securitylogagent/c/a;->c(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->q()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SecurityLogAgent"

    const-string v1, "GslbReceiver : Gslb Service is not running "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/securitylogagent/services/GslbService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p1, v0}, Lcom/samsung/android/securitylogagent/receivers/GslbReceiver;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    return-void
.end method
