.class Lcom/samsung/android/securitylogagent/notifications/f;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/securitylogagent/notifications/f;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    const/16 v4, 0x1e61

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/f;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->d(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)I

    move-result v0

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/f;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->a(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)Lcom/samsung/android/securitylogagent/a/a;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/securitylogagent/a/a;->a(I)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/f;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->a(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;I)I

    const-string v0, "SecurityLogAgent"

    const-string v1, "EulaActivity:Displaying EULA"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/f;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->e(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/f;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->d(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/f;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->a(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)Lcom/samsung/android/securitylogagent/a/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/samsung/android/securitylogagent/a/a;->a(I)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/f;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->b(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)Landroid/app/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/NotificationManager;->cancel(I)V

    const-string v0, "SecurityLogAgent"

    const-string v1, "EulaActivity:EULA Accepted"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/f;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->f(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/f;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->finish()V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/f;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-virtual {v0, v2, v2}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->overridePendingTransition(II)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/f;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->d(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/f;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->a(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)Lcom/samsung/android/securitylogagent/a/a;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/securitylogagent/a/a;->a(I)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/f;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->b(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)Landroid/app/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/NotificationManager;->cancel(I)V

    const-string v0, "SecurityLogAgent"

    const-string v1, "EulaActivity:EULA Rejected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/f;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->c(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/f;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->g(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/f;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->finish()V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/f;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-virtual {v0, v2, v2}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->overridePendingTransition(II)V

    goto :goto_0
.end method
