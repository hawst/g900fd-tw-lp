.class Lcom/samsung/android/securitylogagent/notifications/e;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# instance fields
.field final synthetic a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/securitylogagent/notifications/e;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "SecurityLogAgent"

    const-string v1, "EulaActivity:EulaActivity cancelled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/e;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->a(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)Lcom/samsung/android/securitylogagent/a/a;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/securitylogagent/a/a;->a(I)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/e;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->b(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)Landroid/app/NotificationManager;

    move-result-object v0

    const/16 v1, 0x1e61

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/e;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->c(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/e;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->finish()V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/e;->a:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-virtual {v0, v2, v2}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->overridePendingTransition(II)V

    return-void
.end method
