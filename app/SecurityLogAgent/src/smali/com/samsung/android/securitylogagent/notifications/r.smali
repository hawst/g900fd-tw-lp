.class Lcom/samsung/android/securitylogagent/notifications/r;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/samsung/android/securitylogagent/notifications/q;


# direct methods
.method constructor <init>(Lcom/samsung/android/securitylogagent/notifications/q;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/securitylogagent/notifications/r;->a:Lcom/samsung/android/securitylogagent/notifications/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MASTER_CLEAR"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "CustomWipe"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "args"

    const-string v2, "--tima_kernel_recovery\n"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "SecurityLogAgent"

    const-string v2, "TimaView : MASTER_CLEAR!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/r;->a:Lcom/samsung/android/securitylogagent/notifications/q;

    invoke-static {v1}, Lcom/samsung/android/securitylogagent/notifications/q;->a(Lcom/samsung/android/securitylogagent/notifications/q;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method
