.class public Lcom/samsung/android/securitylogagent/receivers/DateTimeChangedReceiver;
.super Landroid/content/BroadcastReceiver;


# static fields
.field private static a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "DateTimeReceiver"

    sput-object v0, Lcom/samsung/android/securitylogagent/receivers/DateTimeChangedReceiver;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    const-string v0, "SecurityLogAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/samsung/android/securitylogagent/receivers/DateTimeChangedReceiver;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : Date/Time Change Received"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/samsung/android/securitylogagent/a/a;

    invoke-direct {v0, p1}, Lcom/samsung/android/securitylogagent/a/a;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/samsung/android/securitylogagent/b/a;

    invoke-direct {v1, v0, p1}, Lcom/samsung/android/securitylogagent/b/a;-><init>(Lcom/samsung/android/securitylogagent/a/a;Landroid/content/Context;)V

    invoke-static {p1}, Lcom/samsung/android/securitylogagent/c/a;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->r()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->j()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x5265c00

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->i()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    invoke-static {p1}, Lcom/samsung/android/securitylogagent/notifications/p;->a(Landroid/content/Context;)V

    :cond_0
    invoke-virtual {v1}, Lcom/samsung/android/securitylogagent/b/a;->a()Lcom/samsung/android/securitylogagent/b/d;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/securitylogagent/b/d;->f:Lcom/samsung/android/securitylogagent/b/d;

    if-ne v1, v2, :cond_1

    new-instance v1, Lcom/samsung/android/securitylogagent/receivers/a;

    invoke-direct {v1, v0, p1}, Lcom/samsung/android/securitylogagent/receivers/a;-><init>(Lcom/samsung/android/securitylogagent/a/a;Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/samsung/android/securitylogagent/receivers/a;->b()V

    invoke-virtual {v1}, Lcom/samsung/android/securitylogagent/receivers/a;->a()Z

    :cond_1
    return-void
.end method
