.class public Lcom/samsung/android/securitylogagent/notifications/EulaActivity;
.super Landroid/app/Activity;


# instance fields
.field private a:Landroid/app/AlertDialog;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private f:I

.field private g:Lcom/samsung/android/securitylogagent/a/a;

.field private h:Z

.field private i:Lcom/samsung/android/securitylogagent/b/a;

.field private j:Landroid/app/NotificationManager;

.field private final k:Landroid/content/DialogInterface$OnCancelListener;

.field private final l:Landroid/content/DialogInterface$OnClickListener;

.field private final m:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->b:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->c:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->d:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->e:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->j:Landroid/app/NotificationManager;

    new-instance v0, Lcom/samsung/android/securitylogagent/notifications/e;

    invoke-direct {v0, p0}, Lcom/samsung/android/securitylogagent/notifications/e;-><init>(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)V

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->k:Landroid/content/DialogInterface$OnCancelListener;

    new-instance v0, Lcom/samsung/android/securitylogagent/notifications/f;

    invoke-direct {v0, p0}, Lcom/samsung/android/securitylogagent/notifications/f;-><init>(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)V

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->l:Landroid/content/DialogInterface$OnClickListener;

    new-instance v0, Lcom/samsung/android/securitylogagent/notifications/g;

    invoke-direct {v0, p0}, Lcom/samsung/android/securitylogagent/notifications/g;-><init>(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)V

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->m:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method static synthetic a(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;I)I
    .locals 0

    iput p1, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->f:I

    return p1
.end method

.method static synthetic a(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)Lcom/samsung/android/securitylogagent/a/a;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->g:Lcom/samsung/android/securitylogagent/a/a;

    return-object v0
.end method

.method private a()V
    .locals 6

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->g:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/denialLogData/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/denialLogData_dec/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/timaLogData/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/timaLogData_dec/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/c/a;->c(Ljava/io/File;)V

    invoke-static {v1}, Lcom/samsung/android/securitylogagent/c/a;->c(Ljava/io/File;)V

    invoke-static {v2}, Lcom/samsung/android/securitylogagent/c/a;->c(Ljava/io/File;)V

    invoke-static {v3}, Lcom/samsung/android/securitylogagent/c/a;->c(Ljava/io/File;)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->i:Lcom/samsung/android/securitylogagent/b/a;

    sget-object v1, Lcom/samsung/android/securitylogagent/b/c;->a:Lcom/samsung/android/securitylogagent/b/c;

    invoke-virtual {v0, v1}, Lcom/samsung/android/securitylogagent/b/a;->a(Lcom/samsung/android/securitylogagent/b/c;)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->g:Lcom/samsung/android/securitylogagent/a/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/securitylogagent/a/a;->b(Z)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->h:Z

    return p1
.end method

.method static synthetic b(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)Landroid/app/NotificationManager;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->j:Landroid/app/NotificationManager;

    return-object v0
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->i:Lcom/samsung/android/securitylogagent/b/a;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/b/a;->a()Lcom/samsung/android/securitylogagent/b/d;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/securitylogagent/b/d;->f:Lcom/samsung/android/securitylogagent/b/d;

    if-ne v0, v1, :cond_0

    const-string v0, "SecurityLogAgent"

    const-string v1, "EulaActivity:Cancelling Existing Alarms"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/samsung/android/securitylogagent/receivers/a;

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->g:Lcom/samsung/android/securitylogagent/a/a;

    invoke-direct {v0, v1, p0}, Lcom/samsung/android/securitylogagent/receivers/a;-><init>(Lcom/samsung/android/securitylogagent/a/a;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/receivers/a;->b()V

    :cond_0
    return-void
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->i:Lcom/samsung/android/securitylogagent/b/a;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/b/a;->a()Lcom/samsung/android/securitylogagent/b/d;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/securitylogagent/b/d;->b:Lcom/samsung/android/securitylogagent/b/d;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->i:Lcom/samsung/android/securitylogagent/b/a;

    sget-object v1, Lcom/samsung/android/securitylogagent/b/c;->c:Lcom/samsung/android/securitylogagent/b/c;

    invoke-virtual {v0, v1}, Lcom/samsung/android/securitylogagent/b/a;->a(Lcom/samsung/android/securitylogagent/b/c;)V

    const-string v0, "SecurityLogAgent"

    const-string v1, "EulaActivity:State is changed to EULA_ACCEPTED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->b()V

    return-void
.end method

.method static synthetic d(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->f:I

    return v0
.end method

.method private d()V
    .locals 5

    const v4, 0x7f090004

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->g:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->g:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->g:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->m()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/samsung/android/securitylogagent/c/a;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    const v0, 0x7f030004

    invoke-virtual {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->setContentView(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    invoke-virtual {p0, v4}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-static {}, Lcom/samsung/android/securitylogagent/c/a;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "USA"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const v1, 0x7f07001e

    invoke-virtual {p0, v1}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    const v0, 0x7f090007

    invoke-virtual {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    new-instance v1, Lcom/samsung/android/securitylogagent/notifications/h;

    invoke-direct {v1, p0}, Lcom/samsung/android/securitylogagent/notifications/h;-><init>(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->g:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v1}, Lcom/samsung/android/securitylogagent/a/a;->o()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->g:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v1}, Lcom/samsung/android/securitylogagent/a/a;->m()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->g:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v1}, Lcom/samsung/android/securitylogagent/a/a;->l()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    :cond_2
    const v1, 0x7f090006

    invoke-virtual {p0, v1}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    new-instance v2, Lcom/samsung/android/securitylogagent/notifications/i;

    invoke-direct {v2, p0}, Lcom/samsung/android/securitylogagent/notifications/i;-><init>(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f090002

    invoke-virtual {p0, v1}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f070007

    invoke-virtual {p0, v2}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-virtual {p0, v4}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-boolean v3, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->h:Z

    new-instance v2, Lcom/samsung/android/securitylogagent/notifications/j;

    invoke-direct {v2, p0, v0}, Lcom/samsung/android/securitylogagent/notifications/j;-><init>(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;Landroid/widget/Button;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_3
    const v0, 0x7f030003

    invoke-virtual {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->setContentView(I)V

    goto/16 :goto_0

    :cond_4
    const v1, 0x7f07001d

    invoke-virtual {p0, v1}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method private e()V
    .locals 2

    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->setContentView(I)V

    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->f:I

    invoke-direct {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->g()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->a:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    const-string v0, "SecurityLogAgent"

    const-string v1, "EulaActivity:Showing Agreement confirmation dialog"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method static synthetic e(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->d()V

    return-void
.end method

.method private f()V
    .locals 2

    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->setContentView(I)V

    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->f:I

    invoke-direct {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->h()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->a:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    const-string v0, "SecurityLogAgent"

    const-string v1, "EulaActivity:Showing Rejection confirmation dialog"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method static synthetic f(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->c()V

    return-void
.end method

.method private g()Landroid/app/AlertDialog;
    .locals 6

    const v5, 0x7f070027

    const/4 v4, 0x1

    invoke-static {}, Lcom/samsung/android/securitylogagent/c/a;->d()Ljava/lang/String;

    invoke-static {}, Lcom/samsung/android/securitylogagent/c/a;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->hide()V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f070039

    invoke-virtual {p0, v2}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f070038

    iget-object v3, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->l:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->g:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v2}, Lcom/samsung/android/securitylogagent/a/a;->l()I

    move-result v2

    if-ne v2, v4, :cond_1

    const-string v2, "VZW"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v5}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->k:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    :cond_0
    const v0, 0x7f070020

    invoke-virtual {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :cond_1
    const/4 v3, 0x3

    if-eq v2, v3, :cond_2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_3

    :cond_2
    const v0, 0x7f070023

    invoke-virtual {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :cond_3
    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    const-string v2, "ATT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f070025

    invoke-virtual {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :cond_4
    const v0, 0x7f070022

    invoke-virtual {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :cond_5
    const/4 v0, 0x4

    if-ne v2, v0, :cond_6

    invoke-virtual {p0, v5}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :cond_6
    const v0, 0x7f07001f

    invoke-virtual {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method static synthetic g(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->a()V

    return-void
.end method

.method private h()Landroid/app/AlertDialog;
    .locals 6

    const v5, 0x7f070035

    const/4 v4, 0x1

    invoke-static {}, Lcom/samsung/android/securitylogagent/c/a;->d()Ljava/lang/String;

    invoke-static {}, Lcom/samsung/android/securitylogagent/c/a;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->hide()V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f070039

    invoke-virtual {p0, v2}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f070038

    iget-object v3, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->l:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->g:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v2}, Lcom/samsung/android/securitylogagent/a/a;->l()I

    move-result v2

    if-ne v2, v4, :cond_1

    const-string v2, "VZW"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v5}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->k:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    :cond_0
    const v0, 0x7f07002e

    invoke-virtual {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :cond_1
    const/4 v0, 0x3

    if-eq v2, v0, :cond_2

    const/4 v0, 0x5

    if-ne v2, v0, :cond_3

    :cond_2
    const v0, 0x7f070031

    invoke-virtual {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :cond_3
    const/4 v0, 0x2

    if-ne v2, v0, :cond_4

    const v0, 0x7f070030

    invoke-virtual {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :cond_4
    const/4 v0, 0x4

    if-ne v2, v0, :cond_5

    invoke-virtual {p0, v5}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :cond_5
    const v0, 0x7f07002d

    invoke-virtual {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method static synthetic h(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->e()V

    return-void
.end method

.method private i()Landroid/app/AlertDialog;
    .locals 6

    const v5, 0x7f070029

    const v4, 0x7f070028

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f070039

    invoke-virtual {p0, v1}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/samsung/android/securitylogagent/c/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/samsung/android/securitylogagent/c/a;->e()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->g:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v3}, Lcom/samsung/android/securitylogagent/a/a;->o()Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "VZW"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const v2, 0x7f07002c

    invoke-virtual {p0, v2}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    :goto_0
    const-string v2, "USA"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    const v1, 0x7f070037

    iget-object v2, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->l:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :goto_1
    const v1, 0x7f070006

    iget-object v2, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->m:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->k:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v3, "ATT"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0, v5}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v4}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :cond_2
    const-string v3, "USA"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "ATT"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0, v5}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :cond_3
    const-string v3, "VZW"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const v2, 0x7f07002b

    invoke-virtual {p0, v2}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :cond_4
    const v2, 0x7f07002a

    invoke-virtual {p0, v2}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :cond_5
    invoke-virtual {p0, v4}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :cond_6
    const v1, 0x7f070036

    iget-object v2, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->l:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_1
.end method

.method static synthetic i(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->f()V

    return-void
.end method

.method static synthetic j(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->h:Z

    return v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->g:Lcom/samsung/android/securitylogagent/a/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/securitylogagent/a/a;->b(I)V

    invoke-virtual {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->finish()V

    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v4, v4}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->overridePendingTransition(II)V

    invoke-virtual {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f050000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setElevation(F)V

    invoke-virtual {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "intentFrom"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "SettingsChangeReceiver"

    invoke-virtual {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "intentFrom"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "SecurityLogAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EulaActivity: intentFrom is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "intentFrom"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030001

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->setContentView(I)V

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->j:Landroid/app/NotificationManager;

    iput v5, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->f:I

    new-instance v0, Lcom/samsung/android/securitylogagent/a/a;

    invoke-direct {v0, p0}, Lcom/samsung/android/securitylogagent/a/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->g:Lcom/samsung/android/securitylogagent/a/a;

    new-instance v0, Lcom/samsung/android/securitylogagent/b/a;

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->g:Lcom/samsung/android/securitylogagent/a/a;

    invoke-direct {v0, v1, p0}, Lcom/samsung/android/securitylogagent/b/a;-><init>(Lcom/samsung/android/securitylogagent/a/a;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->i:Lcom/samsung/android/securitylogagent/b/a;

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->g:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->g:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->g:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->m()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/samsung/android/securitylogagent/c/a;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const v0, 0x103012b

    invoke-virtual {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->setTheme(I)V

    :goto_0
    invoke-direct {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->i()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->a:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void

    :cond_3
    const v0, 0x1030128

    invoke-virtual {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->setTheme(I)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    const-string v0, "SecurityLogAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EulaActivity: Selected item is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->finish()V

    const/4 v0, 0x1

    return v0
.end method
