.class public Lcom/samsung/android/securitylogagent/notifications/SecurityLogAgentActivity;
.super Landroid/app/Activity;


# instance fields
.field private a:Lcom/samsung/android/securitylogagent/notifications/m;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v0, 0x0

    const/4 v2, -0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v0, v0}, Lcom/samsung/android/securitylogagent/notifications/SecurityLogAgentActivity;->overridePendingTransition(II)V

    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/SecurityLogAgentActivity;->setContentView(I)V

    new-instance v0, Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {p0}, Lcom/samsung/android/securitylogagent/notifications/SecurityLogAgentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/securitylogagent/a/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->o()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->m()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/samsung/android/securitylogagent/c/a;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    const v0, 0x103012b

    invoke-virtual {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/SecurityLogAgentActivity;->setTheme(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/securitylogagent/notifications/SecurityLogAgentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v1, "DenialTimaflag"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v2, :cond_2

    invoke-static {v1}, Lcom/samsung/android/securitylogagent/notifications/o;->a(I)Lcom/samsung/android/securitylogagent/notifications/m;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/SecurityLogAgentActivity;->a:Lcom/samsung/android/securitylogagent/notifications/m;

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/SecurityLogAgentActivity;->a:Lcom/samsung/android/securitylogagent/notifications/m;

    invoke-interface {v1, p0, v0}, Lcom/samsung/android/securitylogagent/notifications/m;->a(Landroid/content/Context;Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/SecurityLogAgentActivity;->a:Lcom/samsung/android/securitylogagent/notifications/m;

    invoke-interface {v0}, Lcom/samsung/android/securitylogagent/notifications/m;->a()V

    new-instance v0, Lcom/samsung/android/securitylogagent/notifications/l;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/securitylogagent/notifications/l;-><init>(Lcom/samsung/android/securitylogagent/notifications/SecurityLogAgentActivity;Lcom/samsung/android/securitylogagent/notifications/k;)V

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/SecurityLogAgentActivity;->a:Lcom/samsung/android/securitylogagent/notifications/m;

    invoke-interface {v1, v0}, Lcom/samsung/android/securitylogagent/notifications/m;->a(Lcom/samsung/android/securitylogagent/notifications/n;)V

    :cond_2
    return-void

    :cond_3
    const v0, 0x1030128

    invoke-virtual {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/SecurityLogAgentActivity;->setTheme(I)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method
