.class public final Lcom/samsung/android/securitylogagent/b/a;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/util/List;

.field private b:Lcom/samsung/android/securitylogagent/b/d;

.field private final c:Lcom/samsung/android/securitylogagent/a/a;

.field private final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/samsung/android/securitylogagent/a/a;Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->a:Ljava/util/List;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->b:Lcom/samsung/android/securitylogagent/b/d;

    iput-object p1, p0, Lcom/samsung/android/securitylogagent/b/a;->c:Lcom/samsung/android/securitylogagent/a/a;

    iput-object p2, p0, Lcom/samsung/android/securitylogagent/b/a;->d:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/samsung/android/securitylogagent/a/a;->k()Lcom/samsung/android/securitylogagent/b/d;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->b:Lcom/samsung/android/securitylogagent/b/d;

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->b:Lcom/samsung/android/securitylogagent/b/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->c:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->r()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "SecurityLogAgent"

    const-string v1, "StateMachine : Initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/samsung/android/securitylogagent/b/c;->a:Lcom/samsung/android/securitylogagent/b/c;

    invoke-virtual {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->a(Lcom/samsung/android/securitylogagent/b/c;)V

    :cond_1
    const-string v0, "SecurityLogAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "StateMachine : Current State = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/securitylogagent/b/a;->b:Lcom/samsung/android/securitylogagent/b/d;

    invoke-virtual {v2}, Lcom/samsung/android/securitylogagent/b/d;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private a(Lcom/samsung/android/securitylogagent/b/d;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/securitylogagent/b/a;->b:Lcom/samsung/android/securitylogagent/b/d;

    return-void
.end method

.method private b(Lcom/samsung/android/securitylogagent/b/d;)V
    .locals 2

    sget-object v0, Lcom/samsung/android/securitylogagent/b/d;->e:Lcom/samsung/android/securitylogagent/b/d;

    if-ne p1, v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.android.securitylogagent.intent.gslb"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/b/a;->d:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method private c(Lcom/samsung/android/securitylogagent/b/d;)V
    .locals 3

    sget-object v0, Lcom/samsung/android/securitylogagent/b/d;->f:Lcom/samsung/android/securitylogagent/b/d;

    if-ne p1, v0, :cond_0

    new-instance v0, Lcom/samsung/android/securitylogagent/receivers/a;

    new-instance v1, Lcom/samsung/android/securitylogagent/a/a;

    iget-object v2, p0, Lcom/samsung/android/securitylogagent/b/a;->d:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/securitylogagent/a/a;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/samsung/android/securitylogagent/b/a;->d:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/securitylogagent/receivers/a;-><init>(Lcom/samsung/android/securitylogagent/a/a;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/receivers/a;->a()Z

    :cond_0
    return-void
.end method

.method private d(Lcom/samsung/android/securitylogagent/b/d;)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/securitylogagent/b/e;

    invoke-interface {v0, p1}, Lcom/samsung/android/securitylogagent/b/e;->a(Lcom/samsung/android/securitylogagent/b/d;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/samsung/android/securitylogagent/b/d;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->b:Lcom/samsung/android/securitylogagent/b/d;

    return-object v0
.end method

.method public a(Lcom/samsung/android/securitylogagent/b/c;)V
    .locals 8

    const-wide/32 v6, 0x5265c00

    const/4 v4, -0x1

    const/4 v3, 0x1

    sget-object v0, Lcom/samsung/android/securitylogagent/b/b;->a:[I

    invoke-virtual {p1}, Lcom/samsung/android/securitylogagent/b/c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    const-string v0, "SecurityLogAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "StateMachine : Changed Current State = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/securitylogagent/b/a;->b:Lcom/samsung/android/securitylogagent/b/d;

    invoke-virtual {v2}, Lcom/samsung/android/securitylogagent/b/d;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->c:Lcom/samsung/android/securitylogagent/a/a;

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/b/a;->b:Lcom/samsung/android/securitylogagent/b/d;

    invoke-virtual {v0, v1}, Lcom/samsung/android/securitylogagent/a/a;->a(Lcom/samsung/android/securitylogagent/b/d;)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->b:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->d(Lcom/samsung/android/securitylogagent/b/d;)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->b:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->c(Lcom/samsung/android/securitylogagent/b/d;)V

    return-void

    :pswitch_0
    sget-object v0, Lcom/samsung/android/securitylogagent/b/d;->a:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->a(Lcom/samsung/android/securitylogagent/b/d;)V

    goto :goto_0

    :pswitch_1
    const-string v0, "SecurityLogAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "StateMachine : send security report status =  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/securitylogagent/b/a;->c:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v2}, Lcom/samsung/android/securitylogagent/a/a;->g()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->c:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->g()I

    move-result v0

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/c/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->c:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/samsung/android/securitylogagent/b/d;->f:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->a(Lcom/samsung/android/securitylogagent/b/d;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/samsung/android/securitylogagent/b/d;->e:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->a(Lcom/samsung/android/securitylogagent/b/d;)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->b:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->b(Lcom/samsung/android/securitylogagent/b/d;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/samsung/android/securitylogagent/b/d;->c:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->a(Lcom/samsung/android/securitylogagent/b/d;)V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->c:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->i()I

    move-result v0

    if-ne v0, v4, :cond_4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/samsung/android/securitylogagent/b/a;->c:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v2}, Lcom/samsung/android/securitylogagent/a/a;->j()J

    move-result-wide v2

    sub-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-lez v0, :cond_0

    const-string v0, "SecurityLogAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "StateMachine getActivationTime : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/securitylogagent/b/a;->c:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v2}, Lcom/samsung/android/securitylogagent/a/a;->j()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SecurityLogAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "StateMachine System. : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/securitylogagent/b/a;->c:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v2}, Lcom/samsung/android/securitylogagent/a/a;->j()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/p;->a(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_4
    sget-object v0, Lcom/samsung/android/securitylogagent/b/d;->b:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->a(Lcom/samsung/android/securitylogagent/b/d;)V

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->c:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->g()I

    move-result v0

    if-ne v0, v3, :cond_7

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/c/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->c:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->q()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/samsung/android/securitylogagent/b/d;->f:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->a(Lcom/samsung/android/securitylogagent/b/d;)V

    goto/16 :goto_0

    :cond_5
    sget-object v0, Lcom/samsung/android/securitylogagent/b/d;->e:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->a(Lcom/samsung/android/securitylogagent/b/d;)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->b:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->b(Lcom/samsung/android/securitylogagent/b/d;)V

    goto/16 :goto_0

    :cond_6
    sget-object v0, Lcom/samsung/android/securitylogagent/b/d;->c:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->a(Lcom/samsung/android/securitylogagent/b/d;)V

    goto/16 :goto_0

    :cond_7
    sget-object v0, Lcom/samsung/android/securitylogagent/b/d;->b:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->a(Lcom/samsung/android/securitylogagent/b/d;)V

    goto/16 :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/c/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->c:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->g()I

    move-result v0

    if-ne v0, v3, :cond_9

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->c:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->q()Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, Lcom/samsung/android/securitylogagent/b/d;->f:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->a(Lcom/samsung/android/securitylogagent/b/d;)V

    goto/16 :goto_0

    :cond_8
    sget-object v0, Lcom/samsung/android/securitylogagent/b/d;->e:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->a(Lcom/samsung/android/securitylogagent/b/d;)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->b:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->b(Lcom/samsung/android/securitylogagent/b/d;)V

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->c:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->i()I

    move-result v0

    if-ne v0, v4, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/samsung/android/securitylogagent/b/a;->c:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v2}, Lcom/samsung/android/securitylogagent/a/a;->j()J

    move-result-wide v2

    sub-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/p;->a(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->c:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->g()I

    move-result v0

    if-ne v0, v3, :cond_b

    sget-object v0, Lcom/samsung/android/securitylogagent/b/d;->c:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->a(Lcom/samsung/android/securitylogagent/b/d;)V

    goto/16 :goto_0

    :cond_b
    sget-object v0, Lcom/samsung/android/securitylogagent/b/d;->b:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->a(Lcom/samsung/android/securitylogagent/b/d;)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->c:Lcom/samsung/android/securitylogagent/a/a;

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->q()Z

    move-result v0

    if-eqz v0, :cond_c

    sget-object v0, Lcom/samsung/android/securitylogagent/b/d;->f:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->a(Lcom/samsung/android/securitylogagent/b/d;)V

    goto/16 :goto_0

    :cond_c
    sget-object v0, Lcom/samsung/android/securitylogagent/b/d;->e:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->a(Lcom/samsung/android/securitylogagent/b/d;)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/b/a;->b:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->b(Lcom/samsung/android/securitylogagent/b/d;)V

    goto/16 :goto_0

    :pswitch_5
    sget-object v0, Lcom/samsung/android/securitylogagent/b/d;->f:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->a(Lcom/samsung/android/securitylogagent/b/d;)V

    goto/16 :goto_0

    :pswitch_6
    sget-object v0, Lcom/samsung/android/securitylogagent/b/d;->a:Lcom/samsung/android/securitylogagent/b/d;

    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/b/a;->a(Lcom/samsung/android/securitylogagent/b/d;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
