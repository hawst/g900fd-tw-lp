.class Lcom/samsung/android/securitylogagent/notifications/j;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/Button;

.field final synthetic b:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;Landroid/widget/Button;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/securitylogagent/notifications/j;->b:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    iput-object p2, p0, Lcom/samsung/android/securitylogagent/notifications/j;->a:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/android/securitylogagent/notifications/j;->b:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/j;->b:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->j(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v3, v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->a(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;Z)Z

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/j;->b:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->a(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)Lcom/samsung/android/securitylogagent/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/j;->b:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->a(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)Lcom/samsung/android/securitylogagent/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/j;->b:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->a(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)Lcom/samsung/android/securitylogagent/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->m()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/j;->b:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->j(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/j;->a:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/j;->a:Landroid/widget/Button;

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/j;->b:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-virtual {v1}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    :goto_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/j;->a:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/j;->a:Landroid/widget/Button;

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/j;->b:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-virtual {v1}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/j;->b:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->a(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)Lcom/samsung/android/securitylogagent/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->l()I

    move-result v0

    const/4 v3, 0x2

    if-eq v0, v3, :cond_5

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/j;->b:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->a(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)Lcom/samsung/android/securitylogagent/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->o()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/j;->b:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->a(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)Lcom/samsung/android/securitylogagent/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->n()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/j;->b:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->j(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/j;->a:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/j;->a:Landroid/widget/Button;

    const-string v1, "#f5f5f5"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/j;->a:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/j;->a:Landroid/widget/Button;

    const-string v1, "#6b6b6b"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/j;->b:Lcom/samsung/android/securitylogagent/notifications/EulaActivity;

    invoke-static {v0}, Lcom/samsung/android/securitylogagent/notifications/EulaActivity;->j(Lcom/samsung/android/securitylogagent/notifications/EulaActivity;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/j;->a:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/j;->a:Landroid/widget/Button;

    const-string v1, "#FFFFFF"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    goto/16 :goto_1

    :cond_8
    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/j;->a:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/j;->a:Landroid/widget/Button;

    const-string v1, "#888888"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    goto/16 :goto_1
.end method
