.class public Lcom/samsung/android/securitylogagent/notifications/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/securitylogagent/notifications/m;


# instance fields
.field a:Landroid/content/Context;

.field b:Landroid/app/AlertDialog;

.field c:Lcom/samsung/android/securitylogagent/notifications/n;

.field d:Ljava/lang/String;

.field e:Landroid/content/DialogInterface$OnClickListener;

.field f:Landroid/content/DialogInterface$OnCancelListener;

.field g:Landroid/content/DialogInterface$OnDismissListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/samsung/android/securitylogagent/notifications/b;

    invoke-direct {v0, p0}, Lcom/samsung/android/securitylogagent/notifications/b;-><init>(Lcom/samsung/android/securitylogagent/notifications/a;)V

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/a;->e:Landroid/content/DialogInterface$OnClickListener;

    new-instance v0, Lcom/samsung/android/securitylogagent/notifications/c;

    invoke-direct {v0, p0}, Lcom/samsung/android/securitylogagent/notifications/c;-><init>(Lcom/samsung/android/securitylogagent/notifications/a;)V

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/a;->f:Landroid/content/DialogInterface$OnCancelListener;

    new-instance v0, Lcom/samsung/android/securitylogagent/notifications/d;

    invoke-direct {v0, p0}, Lcom/samsung/android/securitylogagent/notifications/d;-><init>(Lcom/samsung/android/securitylogagent/notifications/a;)V

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/a;->g:Landroid/content/DialogInterface$OnDismissListener;

    return-void
.end method

.method private a(Lcom/samsung/android/securitylogagent/a/a;)Landroid/app/AlertDialog;
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/a;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/a;->a:Landroid/content/Context;

    const v2, 0x7f070011

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f070038

    iget-object v2, p0, Lcom/samsung/android/securitylogagent/notifications/a;->e:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/a;->f:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/a;->g:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    invoke-direct {p0, p1}, Lcom/samsung/android/securitylogagent/notifications/a;->b(Lcom/samsung/android/securitylogagent/a/a;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method private a(Z)Ljava/lang/String;
    .locals 5

    const-string v0, ""

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/a;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/a;->a:Landroid/content/Context;

    const v1, 0x7f07000a

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/android/securitylogagent/notifications/a;->d:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v1, "SecurityLogAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DenialView: Message shown is -"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/a;->a:Landroid/content/Context;

    const v1, 0x7f070012

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Lcom/samsung/android/securitylogagent/a/a;)Landroid/view/View;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/a;->a:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    invoke-virtual {p1}, Lcom/samsung/android/securitylogagent/a/a;->o()Z

    move-result v2

    invoke-virtual {p1}, Lcom/samsung/android/securitylogagent/a/a;->m()Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/securitylogagent/a/a;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/samsung/android/securitylogagent/c/a;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const v1, 0x7f030007

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    :goto_0
    const v0, 0x7f09000c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lcom/samsung/android/securitylogagent/notifications/a;->a(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v1

    :cond_2
    const v1, 0x7f030006

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/a;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    const-string v0, "SecurityLogAgent"

    const-string v1, "DenialView:Showing Denial Dialog"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    iput-object p1, p0, Lcom/samsung/android/securitylogagent/notifications/a;->a:Landroid/content/Context;

    new-instance v0, Lcom/samsung/android/securitylogagent/a/a;

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/a;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/securitylogagent/a/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->o()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/securitylogagent/a/a;->m()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/samsung/android/securitylogagent/c/a;->g()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/a;->a:Landroid/content/Context;

    const v2, 0x103012b

    invoke-virtual {v1, v2}, Landroid/content/Context;->setTheme(I)V

    :goto_0
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v2, "APP_NAME"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    const-string v2, "APP_NAME"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/a;->d:Ljava/lang/String;

    :cond_2
    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/a;->a(Lcom/samsung/android/securitylogagent/a/a;)Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/a;->b:Landroid/app/AlertDialog;

    return-void

    :cond_3
    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/a;->a:Landroid/content/Context;

    const v2, 0x1030128

    invoke-virtual {v1, v2}, Landroid/content/Context;->setTheme(I)V

    goto :goto_0
.end method

.method public a(Lcom/samsung/android/securitylogagent/notifications/n;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/securitylogagent/notifications/a;->c:Lcom/samsung/android/securitylogagent/notifications/n;

    return-void
.end method
