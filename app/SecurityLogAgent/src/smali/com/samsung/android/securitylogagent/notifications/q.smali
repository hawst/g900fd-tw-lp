.class public Lcom/samsung/android/securitylogagent/notifications/q;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/securitylogagent/notifications/m;


# instance fields
.field private a:I

.field private b:Landroid/app/AlertDialog;

.field private c:Landroid/content/Context;

.field private d:Lcom/samsung/android/securitylogagent/notifications/n;

.field private final e:Landroid/content/DialogInterface$OnClickListener;

.field private final f:Landroid/content/DialogInterface$OnClickListener;

.field private final g:Landroid/content/DialogInterface$OnCancelListener;

.field private final h:Landroid/content/DialogInterface$OnDismissListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/securitylogagent/notifications/q;->a:I

    new-instance v0, Lcom/samsung/android/securitylogagent/notifications/r;

    invoke-direct {v0, p0}, Lcom/samsung/android/securitylogagent/notifications/r;-><init>(Lcom/samsung/android/securitylogagent/notifications/q;)V

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/q;->e:Landroid/content/DialogInterface$OnClickListener;

    new-instance v0, Lcom/samsung/android/securitylogagent/notifications/s;

    invoke-direct {v0, p0}, Lcom/samsung/android/securitylogagent/notifications/s;-><init>(Lcom/samsung/android/securitylogagent/notifications/q;)V

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/q;->f:Landroid/content/DialogInterface$OnClickListener;

    new-instance v0, Lcom/samsung/android/securitylogagent/notifications/t;

    invoke-direct {v0, p0}, Lcom/samsung/android/securitylogagent/notifications/t;-><init>(Lcom/samsung/android/securitylogagent/notifications/q;)V

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/q;->g:Landroid/content/DialogInterface$OnCancelListener;

    new-instance v0, Lcom/samsung/android/securitylogagent/notifications/u;

    invoke-direct {v0, p0}, Lcom/samsung/android/securitylogagent/notifications/u;-><init>(Lcom/samsung/android/securitylogagent/notifications/q;)V

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/q;->h:Landroid/content/DialogInterface$OnDismissListener;

    return-void
.end method

.method static synthetic a(Lcom/samsung/android/securitylogagent/notifications/q;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/q;->c:Landroid/content/Context;

    return-object v0
.end method

.method private a(I)Ljava/lang/String;
    .locals 9

    const v8, 0x7f070049

    const v7, 0x7f070046

    new-instance v1, Lcom/samsung/android/securitylogagent/a/a;

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/q;->c:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/samsung/android/securitylogagent/a/a;-><init>(Landroid/content/Context;)V

    const-string v0, ""

    if-nez p1, :cond_0

    const-string v0, "SecurityLogAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TimaView : alertMsgId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    const-string v2, "ro.csc.country_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "SecurityLogAgent"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TimaView : CSC_COUTRY_CODE : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "USA"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v1}, Lcom/samsung/android/securitylogagent/a/a;->o()Z

    move-result v1

    if-eqz v1, :cond_4

    if-ne p1, v7, :cond_2

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/q;->c:Landroid/content/Context;

    const v1, 0x7f070048

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_1
    const-string v1, "SecurityLogAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TimaView : AlertMsg = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    if-ne p1, v8, :cond_1

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/q;->c:Landroid/content/Context;

    const-string v0, "att"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f07004b

    :goto_2
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    const v0, 0x7f07004d

    goto :goto_2

    :cond_4
    if-ne p1, v7, :cond_5

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/q;->c:Landroid/content/Context;

    const v1, 0x7f070047

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    if-ne p1, v8, :cond_1

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/q;->c:Landroid/content/Context;

    const-string v0, "att"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f07004a

    :goto_3
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_6
    const v0, 0x7f07004c

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/q;->c:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private b()Landroid/app/AlertDialog;
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/q;->c:Landroid/content/Context;

    const v2, 0x7f07003b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/securitylogagent/notifications/q;->a:I

    packed-switch v2, :pswitch_data_0

    const-string v2, "SecurityLogAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "TimaView : Can\'t be Here : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/securitylogagent/notifications/q;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    invoke-direct {p0, v0}, Lcom/samsung/android/securitylogagent/notifications/q;->a(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/samsung/android/securitylogagent/notifications/q;->c:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/q;->c:Landroid/content/Context;

    const v1, 0x7f070003

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/q;->f:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/q;->c:Landroid/content/Context;

    const v1, 0x7f07004f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/securitylogagent/notifications/q;->e:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/q;->g:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/q;->h:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    const-string v0, "SecurityLogAgent"

    const-string v1, "TimaViewcreating tima Pop-up..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    :pswitch_0
    const v0, 0x7f070049

    goto :goto_0

    :pswitch_1
    const v0, 0x7f070046

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lcom/samsung/android/securitylogagent/notifications/q;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/securitylogagent/notifications/q;->c()V

    return-void
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/q;->d:Lcom/samsung/android/securitylogagent/notifications/n;

    invoke-interface {v0}, Lcom/samsung/android/securitylogagent/notifications/n;->a()V

    const-string v0, "SecurityLogAgent"

    const-string v1, "TimaView : pop-up finish"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/q;->b:Landroid/app/AlertDialog;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/q;->b:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/q;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "SecurityLogAgent"

    const-string v1, "TimaView : mainTimaDialog is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    iput-object p1, p0, Lcom/samsung/android/securitylogagent/notifications/q;->c:Landroid/content/Context;

    const-string v0, "SecurityLogAgent"

    const-string v1, "TimaView : onCreate "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "TIMA_STATUS"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/securitylogagent/notifications/q;->a:I

    invoke-direct {p0}, Lcom/samsung/android/securitylogagent/notifications/q;->b()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/securitylogagent/notifications/q;->b:Landroid/app/AlertDialog;

    return-void
.end method

.method public a(Lcom/samsung/android/securitylogagent/notifications/n;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/securitylogagent/notifications/q;->d:Lcom/samsung/android/securitylogagent/notifications/n;

    return-void
.end method
