.class public final enum Lcom/samsung/android/securitylogagent/b/d;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/samsung/android/securitylogagent/b/d;

.field public static final enum b:Lcom/samsung/android/securitylogagent/b/d;

.field public static final enum c:Lcom/samsung/android/securitylogagent/b/d;

.field public static final enum d:Lcom/samsung/android/securitylogagent/b/d;

.field public static final enum e:Lcom/samsung/android/securitylogagent/b/d;

.field public static final enum f:Lcom/samsung/android/securitylogagent/b/d;

.field private static final synthetic h:[Lcom/samsung/android/securitylogagent/b/d;


# instance fields
.field private final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/samsung/android/securitylogagent/b/d;

    const-string v1, "INIT"

    invoke-direct {v0, v1, v4, v4}, Lcom/samsung/android/securitylogagent/b/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/securitylogagent/b/d;->a:Lcom/samsung/android/securitylogagent/b/d;

    new-instance v0, Lcom/samsung/android/securitylogagent/b/d;

    const-string v1, "FILE_EXISTS"

    invoke-direct {v0, v1, v5, v5}, Lcom/samsung/android/securitylogagent/b/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/securitylogagent/b/d;->b:Lcom/samsung/android/securitylogagent/b/d;

    new-instance v0, Lcom/samsung/android/securitylogagent/b/d;

    const-string v1, "EULA_ACCEPTED"

    invoke-direct {v0, v1, v6, v6}, Lcom/samsung/android/securitylogagent/b/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/securitylogagent/b/d;->c:Lcom/samsung/android/securitylogagent/b/d;

    new-instance v0, Lcom/samsung/android/securitylogagent/b/d;

    const-string v1, "NETWORK"

    invoke-direct {v0, v1, v7, v7}, Lcom/samsung/android/securitylogagent/b/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/securitylogagent/b/d;->d:Lcom/samsung/android/securitylogagent/b/d;

    new-instance v0, Lcom/samsung/android/securitylogagent/b/d;

    const-string v1, "GSLB"

    invoke-direct {v0, v1, v8, v8}, Lcom/samsung/android/securitylogagent/b/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/securitylogagent/b/d;->e:Lcom/samsung/android/securitylogagent/b/d;

    new-instance v0, Lcom/samsung/android/securitylogagent/b/d;

    const-string v1, "UPLOAD"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/securitylogagent/b/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/securitylogagent/b/d;->f:Lcom/samsung/android/securitylogagent/b/d;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/android/securitylogagent/b/d;

    sget-object v1, Lcom/samsung/android/securitylogagent/b/d;->a:Lcom/samsung/android/securitylogagent/b/d;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/securitylogagent/b/d;->b:Lcom/samsung/android/securitylogagent/b/d;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/securitylogagent/b/d;->c:Lcom/samsung/android/securitylogagent/b/d;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/securitylogagent/b/d;->d:Lcom/samsung/android/securitylogagent/b/d;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/android/securitylogagent/b/d;->e:Lcom/samsung/android/securitylogagent/b/d;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/securitylogagent/b/d;->f:Lcom/samsung/android/securitylogagent/b/d;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/securitylogagent/b/d;->h:[Lcom/samsung/android/securitylogagent/b/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/samsung/android/securitylogagent/b/d;->g:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/securitylogagent/b/d;
    .locals 1

    const-class v0, Lcom/samsung/android/securitylogagent/b/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/securitylogagent/b/d;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/securitylogagent/b/d;
    .locals 1

    sget-object v0, Lcom/samsung/android/securitylogagent/b/d;->h:[Lcom/samsung/android/securitylogagent/b/d;

    invoke-virtual {v0}, [Lcom/samsung/android/securitylogagent/b/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/securitylogagent/b/d;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/securitylogagent/b/d;->g:I

    return v0
.end method
