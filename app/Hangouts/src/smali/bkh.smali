.class public final Lbkh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbme;


# instance fields
.field private final a:Ljava/lang/Object;

.field private b:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Laux;",
            ">;"
        }
    .end annotation
.end field

.field private c:Laux;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbkh;->a:Ljava/lang/Object;

    .line 18
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lbkh;->b:Ljava/util/concurrent/ConcurrentHashMap;

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lbkh;->c:Laux;

    return-void
.end method


# virtual methods
.method public a()Laux;
    .locals 1

    .prologue
    .line 24
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbkh;->a(Lyj;)Laux;

    move-result-object v0

    return-object v0
.end method

.method public a(Lyj;)Laux;
    .locals 3

    .prologue
    .line 29
    if-nez p1, :cond_2

    .line 30
    iget-object v1, p0, Lbkh;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 31
    :try_start_0
    iget-object v0, p0, Lbkh;->c:Laux;

    if-nez v0, :cond_0

    .line 32
    new-instance v0, Laux;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Laux;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lbkh;->c:Laux;

    .line 34
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 35
    const-string v0, "Babel"

    const-string v1, "Account is null, return default Clearcut logger."

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lbkh;->c:Laux;

    .line 51
    :cond_1
    :goto_0
    return-object v0

    .line 34
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 39
    :cond_2
    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    .line 41
    iget-object v0, p0, Lbkh;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laux;

    .line 42
    if-nez v0, :cond_1

    .line 43
    new-instance v0, Laux;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Laux;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 47
    iget-object v2, p0, Lbkh;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    iget-object v0, p0, Lbkh;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laux;

    goto :goto_0
.end method
