.class public final Lbka;
.super Lbqr;
.source "PG"


# instance fields
.field final a:Ljava/lang/String;

.field final b:I


# direct methods
.method public constructor <init>(Lbjq;)V
    .locals 1

    .prologue
    .line 129
    invoke-direct {p0}, Lbqr;-><init>()V

    .line 130
    iget-object v0, p1, Lbjq;->b:Ljava/lang/String;

    iput-object v0, p0, Lbka;->a:Ljava/lang/String;

    .line 131
    iget v0, p1, Lbjq;->c:I

    iput v0, p0, Lbka;->b:I

    .line 132
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 137
    invoke-direct {p0}, Lbqr;-><init>()V

    .line 138
    iput-object p1, p0, Lbka;->a:Ljava/lang/String;

    .line 139
    const/4 v0, 0x1

    iput v0, p0, Lbka;->b:I

    .line 140
    return-void
.end method


# virtual methods
.method public a(Lyt;)V
    .locals 3

    .prologue
    .line 145
    iget-object v0, p0, Lbka;->a:Ljava/lang/String;

    iget v1, p0, Lbka;->b:I

    invoke-virtual {p1, v0, v1}, Lyt;->c(Ljava/lang/String;I)V

    .line 146
    iget-object v0, p0, Lbka;->a:Ljava/lang/String;

    invoke-static {v0}, Lyt;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    iget v0, p0, Lbka;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 148
    iget-object v0, p0, Lbka;->a:Ljava/lang/String;

    sget-wide v1, Lyp;->e:J

    invoke-virtual {p1, v0, v1, v2}, Lyt;->n(Ljava/lang/String;J)V

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    iget-object v0, p0, Lbka;->a:Ljava/lang/String;

    sget-wide v1, Lyp;->e:J

    invoke-virtual {p1, v0, v1, v2}, Lyt;->m(Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public b(Lyt;)V
    .locals 3

    .prologue
    .line 162
    invoke-virtual {p1}, Lyt;->a()V

    .line 164
    :try_start_0
    iget-object v0, p0, Lbka;->a:Ljava/lang/String;

    iget v1, p0, Lbka;->b:I

    invoke-virtual {p1, v0, v1}, Lyt;->c(Ljava/lang/String;I)V

    .line 165
    iget-object v0, p0, Lbka;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lyt;->ai(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget v2, p0, Lbka;->b:I

    invoke-virtual {p1, v0, v2}, Lyt;->c(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 168
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lyt;->c()V

    throw v0

    .line 166
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 168
    invoke-virtual {p1}, Lyt;->c()V

    .line 169
    return-void
.end method
