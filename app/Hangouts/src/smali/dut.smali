.class public final Ldut;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldut;


# instance fields
.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Boolean;

.field public d:[Ldrb;

.field public e:Lduc;

.field public f:Ldsz;

.field public g:Ldrc;

.field public h:Ldra;

.field public i:Ldpp;

.field public j:Ldwy;

.field public k:Ldto;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13860
    const/4 v0, 0x0

    new-array v0, v0, [Ldut;

    sput-object v0, Ldut;->a:[Ldut;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 13861
    invoke-direct {p0}, Lepn;-><init>()V

    .line 13868
    sget-object v0, Ldrb;->a:[Ldrb;

    iput-object v0, p0, Ldut;->d:[Ldrb;

    .line 13871
    iput-object v1, p0, Ldut;->e:Lduc;

    .line 13874
    iput-object v1, p0, Ldut;->f:Ldsz;

    .line 13877
    iput-object v1, p0, Ldut;->g:Ldrc;

    .line 13880
    iput-object v1, p0, Ldut;->h:Ldra;

    .line 13883
    iput-object v1, p0, Ldut;->i:Ldpp;

    .line 13886
    iput-object v1, p0, Ldut;->j:Ldwy;

    .line 13889
    iput-object v1, p0, Ldut;->k:Ldto;

    .line 13861
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 13935
    iget-object v0, p0, Ldut;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 13936
    const/4 v0, 0x1

    iget-object v2, p0, Ldut;->b:Ljava/lang/Boolean;

    .line 13937
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 13939
    :goto_0
    iget-object v2, p0, Ldut;->c:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    .line 13940
    const/4 v2, 0x2

    iget-object v3, p0, Ldut;->c:Ljava/lang/Boolean;

    .line 13941
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 13943
    :cond_0
    iget-object v2, p0, Ldut;->d:[Ldrb;

    if-eqz v2, :cond_2

    .line 13944
    iget-object v2, p0, Ldut;->d:[Ldrb;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 13945
    if-eqz v4, :cond_1

    .line 13946
    const/4 v5, 0x3

    .line 13947
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 13944
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 13951
    :cond_2
    iget-object v1, p0, Ldut;->e:Lduc;

    if-eqz v1, :cond_3

    .line 13952
    const/4 v1, 0x4

    iget-object v2, p0, Ldut;->e:Lduc;

    .line 13953
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13955
    :cond_3
    iget-object v1, p0, Ldut;->f:Ldsz;

    if-eqz v1, :cond_4

    .line 13956
    const/4 v1, 0x5

    iget-object v2, p0, Ldut;->f:Ldsz;

    .line 13957
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13959
    :cond_4
    iget-object v1, p0, Ldut;->g:Ldrc;

    if-eqz v1, :cond_5

    .line 13960
    const/4 v1, 0x6

    iget-object v2, p0, Ldut;->g:Ldrc;

    .line 13961
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13963
    :cond_5
    iget-object v1, p0, Ldut;->h:Ldra;

    if-eqz v1, :cond_6

    .line 13964
    const/4 v1, 0x7

    iget-object v2, p0, Ldut;->h:Ldra;

    .line 13965
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13967
    :cond_6
    iget-object v1, p0, Ldut;->i:Ldpp;

    if-eqz v1, :cond_7

    .line 13968
    const/16 v1, 0x8

    iget-object v2, p0, Ldut;->i:Ldpp;

    .line 13969
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13971
    :cond_7
    iget-object v1, p0, Ldut;->j:Ldwy;

    if-eqz v1, :cond_8

    .line 13972
    const/16 v1, 0x9

    iget-object v2, p0, Ldut;->j:Ldwy;

    .line 13973
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13975
    :cond_8
    iget-object v1, p0, Ldut;->k:Ldto;

    if-eqz v1, :cond_9

    .line 13976
    const/16 v1, 0xa

    iget-object v2, p0, Ldut;->k:Ldto;

    .line 13977
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13979
    :cond_9
    iget-object v1, p0, Ldut;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13980
    iput v0, p0, Ldut;->cachedSize:I

    .line 13981
    return v0

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 13857
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldut;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldut;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldut;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldut;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldut;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldut;->d:[Ldrb;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldrb;

    iget-object v3, p0, Ldut;->d:[Ldrb;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldut;->d:[Ldrb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldut;->d:[Ldrb;

    :goto_2
    iget-object v2, p0, Ldut;->d:[Ldrb;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldut;->d:[Ldrb;

    new-instance v3, Ldrb;

    invoke-direct {v3}, Ldrb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldut;->d:[Ldrb;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldut;->d:[Ldrb;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldut;->d:[Ldrb;

    new-instance v3, Ldrb;

    invoke-direct {v3}, Ldrb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldut;->d:[Ldrb;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldut;->e:Lduc;

    if-nez v0, :cond_5

    new-instance v0, Lduc;

    invoke-direct {v0}, Lduc;-><init>()V

    iput-object v0, p0, Ldut;->e:Lduc;

    :cond_5
    iget-object v0, p0, Ldut;->e:Lduc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Ldut;->f:Ldsz;

    if-nez v0, :cond_6

    new-instance v0, Ldsz;

    invoke-direct {v0}, Ldsz;-><init>()V

    iput-object v0, p0, Ldut;->f:Ldsz;

    :cond_6
    iget-object v0, p0, Ldut;->f:Ldsz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Ldut;->g:Ldrc;

    if-nez v0, :cond_7

    new-instance v0, Ldrc;

    invoke-direct {v0}, Ldrc;-><init>()V

    iput-object v0, p0, Ldut;->g:Ldrc;

    :cond_7
    iget-object v0, p0, Ldut;->g:Ldrc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Ldut;->h:Ldra;

    if-nez v0, :cond_8

    new-instance v0, Ldra;

    invoke-direct {v0}, Ldra;-><init>()V

    iput-object v0, p0, Ldut;->h:Ldra;

    :cond_8
    iget-object v0, p0, Ldut;->h:Ldra;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Ldut;->i:Ldpp;

    if-nez v0, :cond_9

    new-instance v0, Ldpp;

    invoke-direct {v0}, Ldpp;-><init>()V

    iput-object v0, p0, Ldut;->i:Ldpp;

    :cond_9
    iget-object v0, p0, Ldut;->i:Ldpp;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Ldut;->j:Ldwy;

    if-nez v0, :cond_a

    new-instance v0, Ldwy;

    invoke-direct {v0}, Ldwy;-><init>()V

    iput-object v0, p0, Ldut;->j:Ldwy;

    :cond_a
    iget-object v0, p0, Ldut;->j:Ldwy;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Ldut;->k:Ldto;

    if-nez v0, :cond_b

    new-instance v0, Ldto;

    invoke-direct {v0}, Ldto;-><init>()V

    iput-object v0, p0, Ldut;->k:Ldto;

    :cond_b
    iget-object v0, p0, Ldut;->k:Ldto;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 13894
    iget-object v0, p0, Ldut;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 13895
    const/4 v0, 0x1

    iget-object v1, p0, Ldut;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 13897
    :cond_0
    iget-object v0, p0, Ldut;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 13898
    const/4 v0, 0x2

    iget-object v1, p0, Ldut;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 13900
    :cond_1
    iget-object v0, p0, Ldut;->d:[Ldrb;

    if-eqz v0, :cond_3

    .line 13901
    iget-object v1, p0, Ldut;->d:[Ldrb;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 13902
    if-eqz v3, :cond_2

    .line 13903
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 13901
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 13907
    :cond_3
    iget-object v0, p0, Ldut;->e:Lduc;

    if-eqz v0, :cond_4

    .line 13908
    const/4 v0, 0x4

    iget-object v1, p0, Ldut;->e:Lduc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 13910
    :cond_4
    iget-object v0, p0, Ldut;->f:Ldsz;

    if-eqz v0, :cond_5

    .line 13911
    const/4 v0, 0x5

    iget-object v1, p0, Ldut;->f:Ldsz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 13913
    :cond_5
    iget-object v0, p0, Ldut;->g:Ldrc;

    if-eqz v0, :cond_6

    .line 13914
    const/4 v0, 0x6

    iget-object v1, p0, Ldut;->g:Ldrc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 13916
    :cond_6
    iget-object v0, p0, Ldut;->h:Ldra;

    if-eqz v0, :cond_7

    .line 13917
    const/4 v0, 0x7

    iget-object v1, p0, Ldut;->h:Ldra;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 13919
    :cond_7
    iget-object v0, p0, Ldut;->i:Ldpp;

    if-eqz v0, :cond_8

    .line 13920
    const/16 v0, 0x8

    iget-object v1, p0, Ldut;->i:Ldpp;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 13922
    :cond_8
    iget-object v0, p0, Ldut;->j:Ldwy;

    if-eqz v0, :cond_9

    .line 13923
    const/16 v0, 0x9

    iget-object v1, p0, Ldut;->j:Ldwy;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 13925
    :cond_9
    iget-object v0, p0, Ldut;->k:Ldto;

    if-eqz v0, :cond_a

    .line 13926
    const/16 v0, 0xa

    iget-object v1, p0, Ldut;->k:Ldto;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 13928
    :cond_a
    iget-object v0, p0, Ldut;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 13930
    return-void
.end method
