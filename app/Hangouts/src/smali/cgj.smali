.class public abstract Lcgj;
.super Lcgh;

# interfaces
.implements Lcha;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R::",
        "Lcgq;",
        "A::",
        "Lcge;",
        ">",
        "Lcgh",
        "<TR;>;",
        "Lcha",
        "<TA;>;"
    }
.end annotation


# instance fields
.field private final d:Lcgg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcgg",
            "<TA;>;"
        }
    .end annotation
.end field

.field private e:Lcgy;


# direct methods
.method public constructor <init>(Lcgg;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcgg",
            "<TA;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcgh;-><init>()V

    invoke-static {p1}, Lg;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgg;

    iput-object v0, p0, Lcgj;->d:Lcgg;

    return-void
.end method

.method private a(Landroid/os/RemoteException;)V
    .locals 4

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x8

    invoke-virtual {p1}, Landroid/os/RemoteException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-virtual {p0, v0}, Lcgj;->b(Lcom/google/android/gms/common/api/Status;)V

    return-void
.end method


# virtual methods
.method public final a(Lcge;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)V"
        }
    .end annotation

    iget-object v0, p0, Lcgj;->b:Lcgi;

    if-nez v0, :cond_0

    new-instance v0, Lcgi;

    invoke-interface {p1}, Lcge;->d()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcgi;-><init>(Landroid/os/Looper;)V

    invoke-virtual {p0, v0}, Lcgj;->a(Lcgi;)V

    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, Lcgj;->b(Lcge;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcgj;->a(Landroid/os/RemoteException;)V

    throw v0

    :catch_1
    move-exception v0

    invoke-direct {p0, v0}, Lcgj;->a(Landroid/os/RemoteException;)V

    goto :goto_0
.end method

.method public a(Lcgy;)V
    .locals 0

    iput-object p1, p0, Lcgj;->e:Lcgy;

    return-void
.end method

.method public abstract b(Lcge;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)V"
        }
    .end annotation
.end method

.method public final b(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Failed result must not be success"

    invoke-static {v0, v1}, Lg;->b(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lcgj;->a(Lcom/google/android/gms/common/api/Status;)Lcgq;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcgj;->a(Lcgq;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected c()V
    .locals 1

    invoke-super {p0}, Lcgh;->c()V

    iget-object v0, p0, Lcgj;->e:Lcgy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcgj;->e:Lcgy;

    invoke-interface {v0, p0}, Lcgy;->a(Lcha;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcgj;->e:Lcgy;

    :cond_0
    return-void
.end method

.method public final d()Lcgg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcgg",
            "<TA;>;"
        }
    .end annotation

    iget-object v0, p0, Lcgj;->d:Lcgg;

    return-object v0
.end method
