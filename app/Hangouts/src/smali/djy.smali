.class public final Ldjy;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldjy;


# instance fields
.field public b:Ldnc;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Integer;

.field public e:Ldjz;

.field public f:Ldjv;

.field public g:Ldka;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    new-array v0, v0, [Ldjy;

    sput-object v0, Ldjy;->a:[Ldjy;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Lepn;-><init>()V

    .line 48
    iput-object v0, p0, Ldjy;->b:Ldnc;

    .line 53
    iput-object v0, p0, Ldjy;->d:Ljava/lang/Integer;

    .line 56
    iput-object v0, p0, Ldjy;->e:Ldjz;

    .line 59
    iput-object v0, p0, Ldjy;->f:Ldjv;

    .line 62
    iput-object v0, p0, Ldjy;->g:Ldka;

    .line 45
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 91
    const/4 v0, 0x0

    .line 92
    iget-object v1, p0, Ldjy;->b:Ldnc;

    if-eqz v1, :cond_0

    .line 93
    const/4 v0, 0x1

    iget-object v1, p0, Ldjy;->b:Ldnc;

    .line 94
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 96
    :cond_0
    iget-object v1, p0, Ldjy;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 97
    const/4 v1, 0x2

    iget-object v2, p0, Ldjy;->c:Ljava/lang/String;

    .line 98
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 100
    :cond_1
    iget-object v1, p0, Ldjy;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 101
    const/4 v1, 0x3

    iget-object v2, p0, Ldjy;->d:Ljava/lang/Integer;

    .line 102
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 104
    :cond_2
    iget-object v1, p0, Ldjy;->e:Ldjz;

    if-eqz v1, :cond_3

    .line 105
    const/4 v1, 0x4

    iget-object v2, p0, Ldjy;->e:Ldjz;

    .line 106
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 108
    :cond_3
    iget-object v1, p0, Ldjy;->f:Ldjv;

    if-eqz v1, :cond_4

    .line 109
    const/4 v1, 0x5

    iget-object v2, p0, Ldjy;->f:Ldjv;

    .line 110
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 112
    :cond_4
    iget-object v1, p0, Ldjy;->g:Ldka;

    if-eqz v1, :cond_5

    .line 113
    const/4 v1, 0x6

    iget-object v2, p0, Ldjy;->g:Ldka;

    .line 114
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 116
    :cond_5
    iget-object v1, p0, Ldjy;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 117
    iput v0, p0, Ldjy;->cachedSize:I

    .line 118
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 41
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldjy;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldjy;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldjy;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldjy;->b:Ldnc;

    if-nez v0, :cond_2

    new-instance v0, Ldnc;

    invoke-direct {v0}, Ldnc;-><init>()V

    iput-object v0, p0, Ldjy;->b:Ldnc;

    :cond_2
    iget-object v0, p0, Ldjy;->b:Ldnc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldjy;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-eq v0, v1, :cond_3

    const/4 v1, 0x7

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldjy;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldjy;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldjy;->e:Ldjz;

    if-nez v0, :cond_5

    new-instance v0, Ldjz;

    invoke-direct {v0}, Ldjz;-><init>()V

    iput-object v0, p0, Ldjy;->e:Ldjz;

    :cond_5
    iget-object v0, p0, Ldjy;->e:Ldjz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldjy;->f:Ldjv;

    if-nez v0, :cond_6

    new-instance v0, Ldjv;

    invoke-direct {v0}, Ldjv;-><init>()V

    iput-object v0, p0, Ldjy;->f:Ldjv;

    :cond_6
    iget-object v0, p0, Ldjy;->f:Ldjv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ldjy;->g:Ldka;

    if-nez v0, :cond_7

    new-instance v0, Ldka;

    invoke-direct {v0}, Ldka;-><init>()V

    iput-object v0, p0, Ldjy;->g:Ldka;

    :cond_7
    iget-object v0, p0, Ldjy;->g:Ldka;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Ldjy;->b:Ldnc;

    if-eqz v0, :cond_0

    .line 68
    const/4 v0, 0x1

    iget-object v1, p0, Ldjy;->b:Ldnc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 70
    :cond_0
    iget-object v0, p0, Ldjy;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 71
    const/4 v0, 0x2

    iget-object v1, p0, Ldjy;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 73
    :cond_1
    iget-object v0, p0, Ldjy;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 74
    const/4 v0, 0x3

    iget-object v1, p0, Ldjy;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 76
    :cond_2
    iget-object v0, p0, Ldjy;->e:Ldjz;

    if-eqz v0, :cond_3

    .line 77
    const/4 v0, 0x4

    iget-object v1, p0, Ldjy;->e:Ldjz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 79
    :cond_3
    iget-object v0, p0, Ldjy;->f:Ldjv;

    if-eqz v0, :cond_4

    .line 80
    const/4 v0, 0x5

    iget-object v1, p0, Ldjy;->f:Ldjv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 82
    :cond_4
    iget-object v0, p0, Ldjy;->g:Ldka;

    if-eqz v0, :cond_5

    .line 83
    const/4 v0, 0x6

    iget-object v1, p0, Ldjy;->g:Ldka;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 85
    :cond_5
    iget-object v0, p0, Ldjy;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 87
    return-void
.end method
