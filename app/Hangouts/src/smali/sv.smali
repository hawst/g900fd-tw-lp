.class public final Lsv;
.super Lorg/apache/http/entity/ByteArrayEntity;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:[B

.field private final c:J


# direct methods
.method public constructor <init>(Landroid/content/Context;[B)V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0, p2}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .line 43
    iput-object p1, p0, Lsv;->a:Landroid/content/Context;

    .line 44
    iput-object p2, p0, Lsv;->b:[B

    .line 45
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lsv;->c:J

    .line 46
    return-void
.end method

.method private a(I)V
    .locals 4

    .prologue
    .line 82
    iget-wide v0, p0, Lsv;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 83
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.mms.PROGRESS_STATUS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 84
    const-string v1, "progress"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 85
    const-string v1, "token"

    iget-wide v2, p0, Lsv;->c:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 86
    iget-object v1, p0, Lsv;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 88
    :cond_0
    return-void
.end method


# virtual methods
.method public writeTo(Ljava/io/OutputStream;)V
    .locals 5

    .prologue
    const/16 v1, 0x1000

    .line 50
    if-nez p1, :cond_0

    .line 51
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Output stream may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_0
    const/4 v0, -0x1

    :try_start_0
    invoke-direct {p0, v0}, Lsv;->a(I)V

    .line 58
    const/4 v0, 0x0

    iget-object v2, p0, Lsv;->b:[B

    array-length v3, v2

    move v2, v0

    .line 59
    :goto_0
    if-ge v2, v3, :cond_2

    .line 60
    sub-int v0, v3, v2

    .line 61
    if-le v0, v1, :cond_1

    move v0, v1

    .line 64
    :cond_1
    iget-object v4, p0, Lsv;->b:[B

    invoke-virtual {p1, v4, v2, v0}, Ljava/io/OutputStream;->write([BII)V

    .line 65
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    .line 67
    add-int/2addr v0, v2

    .line 69
    mul-int/lit8 v2, v0, 0x64

    div-int/2addr v2, v3

    invoke-direct {p0, v2}, Lsv;->a(I)V

    move v2, v0

    .line 70
    goto :goto_0

    .line 72
    :cond_2
    const/16 v0, 0x64

    invoke-direct {p0, v0}, Lsv;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    return-void

    .line 76
    :catchall_0
    move-exception v0

    const/4 v1, -0x2

    invoke-direct {p0, v1}, Lsv;->a(I)V

    throw v0
.end method
