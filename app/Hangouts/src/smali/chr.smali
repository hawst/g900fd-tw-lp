.class public abstract Lchr;
.super Ljava/lang/Object;

# interfaces
.implements Lcge;
.implements Lcib;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Landroid/os/IInterface;",
        ">",
        "Ljava/lang/Object;",
        "Lcge;",
        "Lcib;"
    }
.end annotation


# static fields
.field public static final e:[Ljava/lang/String;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/os/Handler;

.field public final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lchr",
            "<TT;>.cht<*>;>;"
        }
    .end annotation
.end field

.field d:Z

.field private final f:Landroid/os/Looper;

.field private g:Landroid/os/IInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private h:Lchw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lchr",
            "<TT;>.chw;"
        }
    .end annotation
.end field

.field private volatile i:I

.field private final j:[Ljava/lang/String;

.field private final k:Lchz;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "service_esmobile"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "service_googleme"

    aput-object v2, v0, v1

    sput-object v0, Lchr;->e:[Ljava/lang/String;

    return-void
.end method

.method public varargs constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcgn;Lcfw;[Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lchr;->c:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput v0, p0, Lchr;->i:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lchr;->d:Z

    invoke-static {p1}, Lg;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lchr;->a:Landroid/content/Context;

    const-string v0, "Looper must not be null"

    invoke-static {p2, v0}, Lg;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Looper;

    iput-object v0, p0, Lchr;->f:Landroid/os/Looper;

    new-instance v0, Lchz;

    invoke-direct {v0, p2, p0}, Lchz;-><init>(Landroid/os/Looper;Lcib;)V

    iput-object v0, p0, Lchr;->k:Lchz;

    new-instance v0, Lchs;

    invoke-direct {v0, p0, p2}, Lchs;-><init>(Lchr;Landroid/os/Looper;)V

    iput-object v0, p0, Lchr;->b:Landroid/os/Handler;

    iput-object p5, p0, Lchr;->j:[Ljava/lang/String;

    invoke-static {p3}, Lg;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgn;

    invoke-virtual {p0, v0}, Lchr;->a(Lcgn;)V

    invoke-static {p4}, Lg;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfw;

    invoke-virtual {p0, v0}, Lchr;->a(Lcfw;)V

    return-void
.end method

.method public varargs constructor <init>(Landroid/content/Context;Lcfv;Lcfw;[Ljava/lang/String;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lchu;

    invoke-direct {v3, p2}, Lchu;-><init>(Lcfv;)V

    new-instance v4, Lchx;

    invoke-direct {v4, p3}, Lchx;-><init>(Lcfw;)V

    move-object v0, p0

    move-object v1, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lchr;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcgn;Lcfw;[Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lchr;Landroid/os/IInterface;)Landroid/os/IInterface;
    .locals 0

    iput-object p1, p0, Lchr;->g:Landroid/os/IInterface;

    return-object p1
.end method

.method static synthetic a(Lchr;)Lchz;
    .locals 1

    iget-object v0, p0, Lchr;->k:Lchz;

    return-object v0
.end method

.method private a(I)V
    .locals 1

    iget v0, p0, Lchr;->i:I

    iput p1, p0, Lchr;->i:I

    return-void
.end method

.method static synthetic a(Lchr;I)V
    .locals 0

    invoke-direct {p0, p1}, Lchr;->a(I)V

    return-void
.end method

.method static synthetic b(Lchr;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lchr;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Lchr;)Landroid/os/IInterface;
    .locals 1

    iget-object v0, p0, Lchr;->g:Landroid/os/IInterface;

    return-object v0
.end method

.method static synthetic d(Lchr;)Lchw;
    .locals 1

    iget-object v0, p0, Lchr;->h:Lchw;

    return-object v0
.end method

.method static synthetic e(Lchr;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lchr;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic f(Lchr;)Lchw;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lchr;->h:Lchw;

    return-object v0
.end method


# virtual methods
.method public D_()Z
    .locals 1

    iget-boolean v0, p0, Lchr;->d:Z

    return v0
.end method

.method public abstract a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/IBinder;",
            ")TT;"
        }
    .end annotation
.end method

.method public a()V
    .locals 4

    const/4 v3, 0x3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lchr;->d:Z

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lchr;->a(I)V

    iget-object v0, p0, Lchr;->a:Landroid/content/Context;

    invoke-static {v0}, Lcfz;->a(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lchr;->a(I)V

    iget-object v1, p0, Lchr;->b:Landroid/os/Handler;

    iget-object v2, p0, Lchr;->b:Landroid/os/Handler;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lchr;->h:Lchw;

    if-eqz v0, :cond_2

    const-string v0, "GmsClient"

    const-string v1, "Calling connect() while still connected, missing disconnect()."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lchr;->g:Landroid/os/IInterface;

    iget-object v0, p0, Lchr;->a:Landroid/content/Context;

    invoke-static {v0}, Lcic;->a(Landroid/content/Context;)Lcic;

    move-result-object v0

    invoke-virtual {p0}, Lchr;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lchr;->h:Lchw;

    invoke-virtual {v0, v1, v2}, Lcic;->b(Ljava/lang/String;Lchw;)V

    :cond_2
    new-instance v0, Lchw;

    invoke-direct {v0, p0}, Lchw;-><init>(Lchr;)V

    iput-object v0, p0, Lchr;->h:Lchw;

    iget-object v0, p0, Lchr;->a:Landroid/content/Context;

    invoke-static {v0}, Lcic;->a(Landroid/content/Context;)Lcic;

    move-result-object v0

    invoke-virtual {p0}, Lchr;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lchr;->h:Lchw;

    invoke-virtual {v0, v1, v2}, Lcic;->a(Ljava/lang/String;Lchw;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GmsClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unable to connect to service: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lchr;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lchr;->b:Landroid/os/Handler;

    iget-object v1, p0, Lchr;->b:Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 4

    iget-object v0, p0, Lchr;->b:Landroid/os/Handler;

    iget-object v1, p0, Lchr;->b:Landroid/os/Handler;

    const/4 v2, 0x1

    new-instance v3, Lchy;

    invoke-direct {v3, p0, p1, p2, p3}, Lchy;-><init>(Lchr;ILandroid/os/IBinder;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public a(Lcfw;)V
    .locals 1

    iget-object v0, p0, Lchr;->k:Lchz;

    invoke-virtual {v0, p1}, Lchz;->a(Lcfw;)V

    return-void
.end method

.method public a(Lcgn;)V
    .locals 1

    iget-object v0, p0, Lchr;->k:Lchz;

    invoke-virtual {v0, p1}, Lchz;->a(Lcgn;)V

    return-void
.end method

.method public abstract a(Lcim;Lchv;)V
.end method

.method public b()V
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lchr;->d:Z

    iget-object v2, p0, Lchr;->c:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lchr;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lchr;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcht;

    invoke-virtual {v0}, Lcht;->f()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lchr;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lchr;->a(I)V

    iput-object v4, p0, Lchr;->g:Landroid/os/IInterface;

    iget-object v0, p0, Lchr;->h:Lchw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lchr;->a:Landroid/content/Context;

    invoke-static {v0}, Lcic;->a(Landroid/content/Context;)Lcic;

    move-result-object v0

    invoke-virtual {p0}, Lchr;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lchr;->h:Lchw;

    invoke-virtual {v0, v1, v2}, Lcic;->b(Ljava/lang/String;Lchw;)V

    iput-object v4, p0, Lchr;->h:Lchw;

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public c()Z
    .locals 2

    iget v0, p0, Lchr;->i:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Landroid/os/Looper;
    .locals 1

    iget-object v0, p0, Lchr;->f:Landroid/os/Looper;

    return-object v0
.end method

.method public abstract e()Ljava/lang/String;
.end method

.method public abstract f()Ljava/lang/String;
.end method

.method public g()Z
    .locals 2

    iget v0, p0, Lchr;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()V
    .locals 2

    invoke-virtual {p0}, Lchr;->c()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public final i()Landroid/os/IInterface;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    invoke-virtual {p0}, Lchr;->h()V

    iget-object v0, p0, Lchr;->g:Landroid/os/IInterface;

    return-object v0
.end method
