.class public final Lewa;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lewa;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Letw;

.field public e:Lexp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 388
    const/4 v0, 0x0

    new-array v0, v0, [Lewa;

    sput-object v0, Lewa;->a:[Lewa;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 389
    invoke-direct {p0}, Lepn;-><init>()V

    .line 398
    iput-object v0, p0, Lewa;->b:Ljava/lang/Integer;

    .line 403
    iput-object v0, p0, Lewa;->d:Letw;

    .line 406
    iput-object v0, p0, Lewa;->e:Lexp;

    .line 389
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 429
    const/4 v0, 0x0

    .line 430
    iget-object v1, p0, Lewa;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 431
    const/4 v0, 0x1

    iget-object v1, p0, Lewa;->b:Ljava/lang/Integer;

    .line 432
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 434
    :cond_0
    iget-object v1, p0, Lewa;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 435
    const/4 v1, 0x4

    iget-object v2, p0, Lewa;->c:Ljava/lang/String;

    .line 436
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 438
    :cond_1
    iget-object v1, p0, Lewa;->e:Lexp;

    if-eqz v1, :cond_2

    .line 439
    const/4 v1, 0x5

    iget-object v2, p0, Lewa;->e:Lexp;

    .line 440
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 442
    :cond_2
    iget-object v1, p0, Lewa;->d:Letw;

    if-eqz v1, :cond_3

    .line 443
    const/4 v1, 0x6

    iget-object v2, p0, Lewa;->d:Letw;

    .line 444
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 446
    :cond_3
    iget-object v1, p0, Lewa;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 447
    iput v0, p0, Lewa;->cachedSize:I

    .line 448
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 385
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lewa;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lewa;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lewa;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lewa;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lewa;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewa;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lewa;->e:Lexp;

    if-nez v0, :cond_4

    new-instance v0, Lexp;

    invoke-direct {v0}, Lexp;-><init>()V

    iput-object v0, p0, Lewa;->e:Lexp;

    :cond_4
    iget-object v0, p0, Lewa;->e:Lexp;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lewa;->d:Letw;

    if-nez v0, :cond_5

    new-instance v0, Letw;

    invoke-direct {v0}, Letw;-><init>()V

    iput-object v0, p0, Lewa;->d:Letw;

    :cond_5
    iget-object v0, p0, Lewa;->d:Letw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x22 -> :sswitch_2
        0x2a -> :sswitch_3
        0x32 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 411
    iget-object v0, p0, Lewa;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 412
    const/4 v0, 0x1

    iget-object v1, p0, Lewa;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 414
    :cond_0
    iget-object v0, p0, Lewa;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 415
    const/4 v0, 0x4

    iget-object v1, p0, Lewa;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 417
    :cond_1
    iget-object v0, p0, Lewa;->e:Lexp;

    if-eqz v0, :cond_2

    .line 418
    const/4 v0, 0x5

    iget-object v1, p0, Lewa;->e:Lexp;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 420
    :cond_2
    iget-object v0, p0, Lewa;->d:Letw;

    if-eqz v0, :cond_3

    .line 421
    const/4 v0, 0x6

    iget-object v1, p0, Lewa;->d:Letw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 423
    :cond_3
    iget-object v0, p0, Lewa;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 425
    return-void
.end method
