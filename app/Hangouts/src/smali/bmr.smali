.class public abstract Lbmr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lyc;",
            ">;"
        }
    .end annotation
.end field

.field public final m:Lyj;

.field public final n:Lbxz;

.field public final o:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lbu;

.field public q:Lbk;

.field public r:Z

.field public s:I

.field public t:I

.field public u:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lyj;Lbxz;)V
    .locals 1

    .prologue
    .line 251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 245
    const/4 v0, 0x0

    iput-object v0, p0, Lbmr;->u:Ljava/util/ArrayList;

    .line 247
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lbmr;->a:Ljava/util/List;

    .line 252
    iput-object p1, p0, Lbmr;->m:Lyj;

    .line 253
    iput-object p2, p0, Lbmr;->n:Lbxz;

    .line 254
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbmr;->o:Ljava/util/HashSet;

    .line 255
    return-void
.end method

.method protected static a(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 360
    const/16 v0, 0x9

    .line 361
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 362
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 363
    const/16 v0, 0xa

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 365
    :cond_0
    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 327
    iget-object v0, p0, Lbmr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyc;

    .line 328
    invoke-virtual {v0}, Lyc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351
    :cond_1
    return-void

    .line 333
    :cond_2
    new-instance v0, Lyt;

    iget-object v1, p0, Lbmr;->m:Lyj;

    invoke-direct {v0, v1}, Lyt;-><init>(Lyj;)V

    .line 335
    invoke-virtual {v0, p1}, Lyt;->ah(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 336
    iget-object v1, p0, Lbmr;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 338
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyv;

    .line 339
    iget-object v2, p0, Lbmr;->m:Lyj;

    iget-object v3, v0, Lyv;->s:Ljava/lang/String;

    .line 340
    invoke-static {v2, v3}, Lyc;->c(Lyj;Ljava/lang/String;)Lyc;

    move-result-object v2

    .line 343
    iget v0, v0, Lyv;->c:I

    invoke-static {v0}, Lf;->d(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 346
    iget-object v0, p0, Lbmr;->a:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v0, v3, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 348
    :cond_3
    iget-object v0, p0, Lbmr;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public abstract a()Landroid/content/Intent;
.end method

.method protected abstract a(Lbk;)Lbu;
.end method

.method protected a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 284
    invoke-direct {p0, p1}, Lbmr;->c(Ljava/lang/String;)V

    .line 286
    const/4 v0, 0x0

    iput-object v0, p0, Lbmr;->u:Ljava/util/ArrayList;

    .line 287
    iget-object v0, p0, Lbmr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 290
    iget-object v0, p0, Lbmr;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyc;

    .line 291
    invoke-virtual {v0}, Lyc;->b()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbdh;

    .line 292
    iget-object v3, v1, Lbdh;->h:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 293
    iget-object v3, p0, Lbmr;->u:Ljava/util/ArrayList;

    if-nez v3, :cond_1

    .line 294
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v0}, Lyc;->d()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, p0, Lbmr;->u:Ljava/util/ArrayList;

    .line 296
    :cond_1
    iget-object v3, p0, Lbmr;->u:Ljava/util/ArrayList;

    iget-object v1, v1, Lbdh;->h:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 300
    :cond_2
    return-void
.end method

.method public abstract b()I
.end method

.method protected b(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 303
    invoke-direct {p0, p1}, Lbmr;->c(Ljava/lang/String;)V

    .line 305
    iget-object v0, p0, Lbmr;->o:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 306
    iget-object v0, p0, Lbmr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyc;

    .line 307
    invoke-virtual {v0}, Lyc;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 309
    iget-object v3, v0, Lbdh;->c:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 311
    iget-object v3, p0, Lbmr;->o:Ljava/util/HashSet;

    const-string v4, "tel"

    iget-object v0, v0, Lbdh;->c:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 312
    :cond_2
    invoke-virtual {v0}, Lbdh;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 315
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lbmr;->m:Lyj;

    .line 316
    invoke-virtual {v4}, Lyj;->b()Ljava/lang/String;

    move-result-object v4

    .line 317
    invoke-virtual {v0}, Lbdh;->a()Ljava/lang/String;

    move-result-object v0

    .line 314
    invoke-static {v3, v4, v0}, Lcvv;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcvv;->a(Landroid/content/Context;J)Landroid/net/Uri;

    move-result-object v0

    .line 318
    if-eqz v0, :cond_1

    .line 319
    iget-object v3, p0, Lbmr;->o:Ljava/util/HashSet;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 324
    :cond_3
    return-void
.end method

.method public abstract c()Z
.end method

.method protected abstract d()Landroid/content/Intent;
.end method

.method public abstract e()I
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 268
    const/4 v0, 0x0

    return-object v0
.end method

.method protected g()I
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x0

    return v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 373
    const/4 v0, 0x3

    return v0
.end method

.method public l()I
    .locals 1

    .prologue
    .line 398
    const/4 v0, 0x1

    return v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 413
    const/4 v0, 0x0

    return-object v0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 377
    iget v0, p0, Lbmr;->t:I

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 381
    iget v0, p0, Lbmr;->t:I

    return v0
.end method

.method public q()I
    .locals 1

    .prologue
    .line 385
    iget v0, p0, Lbmr;->t:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
