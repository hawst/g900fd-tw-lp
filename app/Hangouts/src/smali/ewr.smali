.class public final Lewr;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lewr;


# instance fields
.field public b:[Levy;

.field public c:Levt;

.field public d:Levt;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Integer;

.field public h:Lexc;

.field public i:Lewb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lewr;

    sput-object v0, Lewr;->a:[Lewr;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 23
    sget-object v0, Levy;->a:[Levy;

    iput-object v0, p0, Lewr;->b:[Levy;

    .line 26
    iput-object v1, p0, Lewr;->c:Levt;

    .line 29
    iput-object v1, p0, Lewr;->d:Levt;

    .line 36
    iput-object v1, p0, Lewr;->g:Ljava/lang/Integer;

    .line 39
    iput-object v1, p0, Lewr;->h:Lexc;

    .line 42
    iput-object v1, p0, Lewr;->i:Lewb;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 82
    iget-object v1, p0, Lewr;->b:[Levy;

    if-eqz v1, :cond_1

    .line 83
    iget-object v2, p0, Lewr;->b:[Levy;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 84
    if-eqz v4, :cond_0

    .line 85
    const/4 v5, 0x1

    .line 86
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 83
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 90
    :cond_1
    iget-object v1, p0, Lewr;->c:Levt;

    if-eqz v1, :cond_2

    .line 91
    const/4 v1, 0x3

    iget-object v2, p0, Lewr;->c:Levt;

    .line 92
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 94
    :cond_2
    iget-object v1, p0, Lewr;->d:Levt;

    if-eqz v1, :cond_3

    .line 95
    const/4 v1, 0x4

    iget-object v2, p0, Lewr;->d:Levt;

    .line 96
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    :cond_3
    iget-object v1, p0, Lewr;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 99
    const/4 v1, 0x6

    iget-object v2, p0, Lewr;->e:Ljava/lang/Integer;

    .line 100
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    :cond_4
    iget-object v1, p0, Lewr;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 103
    const/4 v1, 0x7

    iget-object v2, p0, Lewr;->f:Ljava/lang/Integer;

    .line 104
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 106
    :cond_5
    iget-object v1, p0, Lewr;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 107
    const/16 v1, 0x8

    iget-object v2, p0, Lewr;->g:Ljava/lang/Integer;

    .line 108
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 110
    :cond_6
    iget-object v1, p0, Lewr;->h:Lexc;

    if-eqz v1, :cond_7

    .line 111
    const/16 v1, 0x9

    iget-object v2, p0, Lewr;->h:Lexc;

    .line 112
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 114
    :cond_7
    iget-object v1, p0, Lewr;->i:Lewb;

    if-eqz v1, :cond_8

    .line 115
    const/16 v1, 0xa

    iget-object v2, p0, Lewr;->i:Lewb;

    .line 116
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 118
    :cond_8
    iget-object v1, p0, Lewr;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 119
    iput v0, p0, Lewr;->cachedSize:I

    .line 120
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lewr;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lewr;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lewr;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lewr;->b:[Levy;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Levy;

    iget-object v3, p0, Lewr;->b:[Levy;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lewr;->b:[Levy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lewr;->b:[Levy;

    :goto_2
    iget-object v2, p0, Lewr;->b:[Levy;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lewr;->b:[Levy;

    new-instance v3, Levy;

    invoke-direct {v3}, Levy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lewr;->b:[Levy;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lewr;->b:[Levy;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lewr;->b:[Levy;

    new-instance v3, Levy;

    invoke-direct {v3}, Levy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lewr;->b:[Levy;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lewr;->c:Levt;

    if-nez v0, :cond_5

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Lewr;->c:Levt;

    :cond_5
    iget-object v0, p0, Lewr;->c:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lewr;->d:Levt;

    if-nez v0, :cond_6

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Lewr;->d:Levt;

    :cond_6
    iget-object v0, p0, Lewr;->d:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lewr;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lewr;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_7

    const/4 v2, 0x1

    if-eq v0, v2, :cond_7

    const/4 v2, 0x2

    if-eq v0, v2, :cond_7

    const/4 v2, 0x3

    if-eq v0, v2, :cond_7

    const/4 v2, 0x4

    if-eq v0, v2, :cond_7

    const/4 v2, 0x5

    if-eq v0, v2, :cond_7

    const/4 v2, 0x6

    if-ne v0, v2, :cond_8

    :cond_7
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lewr;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_8
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lewr;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lewr;->h:Lexc;

    if-nez v0, :cond_9

    new-instance v0, Lexc;

    invoke-direct {v0}, Lexc;-><init>()V

    iput-object v0, p0, Lewr;->h:Lexc;

    :cond_9
    iget-object v0, p0, Lewr;->h:Lexc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lewr;->i:Lewb;

    if-nez v0, :cond_a

    new-instance v0, Lewb;

    invoke-direct {v0}, Lewb;-><init>()V

    iput-object v0, p0, Lewr;->i:Lewb;

    :cond_a
    iget-object v0, p0, Lewr;->i:Lewb;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x30 -> :sswitch_4
        0x38 -> :sswitch_5
        0x40 -> :sswitch_6
        0x4a -> :sswitch_7
        0x52 -> :sswitch_8
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 47
    iget-object v0, p0, Lewr;->b:[Levy;

    if-eqz v0, :cond_1

    .line 48
    iget-object v1, p0, Lewr;->b:[Levy;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 49
    if-eqz v3, :cond_0

    .line 50
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 48
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54
    :cond_1
    iget-object v0, p0, Lewr;->c:Levt;

    if-eqz v0, :cond_2

    .line 55
    const/4 v0, 0x3

    iget-object v1, p0, Lewr;->c:Levt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 57
    :cond_2
    iget-object v0, p0, Lewr;->d:Levt;

    if-eqz v0, :cond_3

    .line 58
    const/4 v0, 0x4

    iget-object v1, p0, Lewr;->d:Levt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 60
    :cond_3
    iget-object v0, p0, Lewr;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 61
    const/4 v0, 0x6

    iget-object v1, p0, Lewr;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 63
    :cond_4
    iget-object v0, p0, Lewr;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 64
    const/4 v0, 0x7

    iget-object v1, p0, Lewr;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 66
    :cond_5
    iget-object v0, p0, Lewr;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 67
    const/16 v0, 0x8

    iget-object v1, p0, Lewr;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 69
    :cond_6
    iget-object v0, p0, Lewr;->h:Lexc;

    if-eqz v0, :cond_7

    .line 70
    const/16 v0, 0x9

    iget-object v1, p0, Lewr;->h:Lexc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 72
    :cond_7
    iget-object v0, p0, Lewr;->i:Lewb;

    if-eqz v0, :cond_8

    .line 73
    const/16 v0, 0xa

    iget-object v1, p0, Lewr;->i:Lewb;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 75
    :cond_8
    iget-object v0, p0, Lewr;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 77
    return-void
.end method
