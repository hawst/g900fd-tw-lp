.class public final Lbwf;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final B:Ljava/util/concurrent/ScheduledExecutorService;

.field private static final C:[Ljava/lang/String;

.field private static D:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Z

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field private static final d:Ljava/lang/String;

.field private static volatile e:I

.field private static volatile f:I

.field private static volatile g:I

.field private static volatile h:J

.field private static volatile i:J

.field private static volatile j:I

.field private static volatile k:J

.field private static volatile l:I

.field private static final m:Landroid/net/Uri;

.field private static final n:Landroid/net/Uri;

.field private static o:[Lbyt;

.field private static final p:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lbwp;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final A:Z

.field private final q:Landroid/content/Context;

.field private final r:Lyj;

.field private final s:Lyt;

.field private final t:Lew;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lew",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final u:Lew;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lew",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final v:Les;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Les",
            "<",
            "Ljava/lang/String;",
            "Lbwi;",
            ">;"
        }
    .end annotation
.end field

.field private final w:J

.field private x:Z

.field private y:Z

.field private volatile z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 67
    sget-object v0, Lbys;->n:Lcyp;

    sput-boolean v6, Lbwf;->a:Z

    .line 72
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "(%s IN (%d, %d))"

    new-array v2, v8, [Ljava/lang/Object;

    const-string v3, "type"

    aput-object v3, v2, v6

    .line 76
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    .line 77
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    .line 72
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbwf;->b:Ljava/lang/String;

    .line 78
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "((%s IN (%d, %d)) AND (%s IN (%d, %d)))"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "msg_box"

    aput-object v3, v2, v6

    .line 82
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    .line 83
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    const-string v3, "m_type"

    aput-object v3, v2, v8

    const/16 v3, 0x80

    .line 85
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const/16 v4, 0x84

    .line 86
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 78
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbwf;->c:Ljava/lang/String;

    .line 92
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "((%s=%d) AND (%s=%d OR %s=%d) AND (%s=%d))"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "transport_type"

    aput-object v3, v2, v6

    .line 96
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const-string v3, "sms_type"

    aput-object v3, v2, v7

    .line 98
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    const-string v3, "sms_type"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    .line 100
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "status"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 102
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 92
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbwf;->d:Ljava/lang/String;

    .line 621
    invoke-static {}, Lbwf;->i()V

    .line 624
    new-instance v0, Lbwg;

    invoke-direct {v0}, Lbwg;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/Runnable;)V

    .line 632
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 633
    sget-object v0, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    :goto_0
    sput-object v0, Lbwf;->m:Landroid/net/Uri;

    .line 635
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 636
    sget-object v0, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    :goto_1
    sput-object v0, Lbwf;->n:Landroid/net/Uri;

    .line 669
    new-array v0, v7, [Lbyt;

    new-instance v1, Lbyt;

    invoke-direct {v1}, Lbyt;-><init>()V

    aput-object v1, v0, v6

    new-instance v1, Lbyt;

    invoke-direct {v1}, Lbyt;-><init>()V

    aput-object v1, v0, v5

    sput-object v0, Lbwf;->o:[Lbyt;

    .line 705
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lbwf;->p:Ljava/util/Map;

    .line 782
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    sput-object v0, Lbwf;->B:Ljava/util/concurrent/ScheduledExecutorService;

    .line 880
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "count()"

    aput-object v1, v0, v6

    sput-object v0, Lbwf;->C:[Ljava/lang/String;

    .line 1736
    const/4 v0, 0x0

    sput-object v0, Lbwf;->D:Ljava/util/List;

    return-void

    .line 633
    :cond_0
    const-string v0, "content://mms"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 636
    :cond_1
    const-string v0, "content://sms"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_1
.end method

.method private constructor <init>(Lyt;JZ)V
    .locals 1

    .prologue
    .line 785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 786
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbwf;->q:Landroid/content/Context;

    .line 787
    iput-object p1, p0, Lbwf;->s:Lyt;

    .line 788
    invoke-virtual {p1}, Lyt;->f()Lyj;

    move-result-object v0

    iput-object v0, p0, Lbwf;->r:Lyj;

    .line 789
    iput-wide p2, p0, Lbwf;->w:J

    .line 790
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbwf;->z:Z

    .line 791
    iput-boolean p4, p0, Lbwf;->A:Z

    .line 792
    new-instance v0, Lew;

    invoke-direct {v0}, Lew;-><init>()V

    iput-object v0, p0, Lbwf;->t:Lew;

    .line 793
    new-instance v0, Lew;

    invoke-direct {v0}, Lew;-><init>()V

    iput-object v0, p0, Lbwf;->u:Lew;

    .line 794
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    iput-object v0, p0, Lbwf;->v:Les;

    .line 795
    return-void
.end method

.method synthetic constructor <init>(Lyt;JZB)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3, p4}, Lbwf;-><init>(Lyt;JZ)V

    return-void
.end method

.method private static a(Landroid/database/Cursor;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 884
    if-eqz p0, :cond_1

    :try_start_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 885
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 888
    if-eqz p0, :cond_0

    .line 889
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    .line 892
    :cond_0
    :goto_0
    return v0

    .line 888
    :cond_1
    if-eqz p0, :cond_0

    .line 889
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 888
    :catchall_0
    move-exception v0

    if-eqz p0, :cond_2

    .line 889
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1834
    array-length v4, p3

    move v1, v2

    move v3, v2

    .line 1835
    :goto_0
    if-ge v1, v4, :cond_0

    .line 1836
    add-int/lit16 v0, v1, 0x80

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1837
    sub-int v5, v0, v1

    .line 1838
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "%s IN %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    aput-object p2, v8, v2

    const/4 v9, 0x1

    .line 1842
    invoke-static {v5}, Lbvx;->b(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v9

    .line 1838
    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1843
    invoke-static {p3, v1, v0}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 1844
    iget-object v6, p0, Lbwf;->s:Lyt;

    invoke-virtual {v6}, Lyt;->e()Lzr;

    move-result-object v6

    invoke-virtual {v6, p1, v5, v0}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1848
    add-int/2addr v3, v0

    .line 1835
    add-int/lit16 v0, v1, 0x80

    move v1, v0

    goto :goto_0

    .line 1850
    :cond_0
    return v3
.end method

.method static synthetic a(Landroid/net/Uri;)J
    .locals 2

    .prologue
    .line 65
    invoke-static {p0}, Lbwf;->b(Landroid/net/Uri;)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(Lbvj;)J
    .locals 16

    .prologue
    .line 1485
    move-object/from16 v0, p1

    iget-object v1, v0, Lbvj;->w:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_0

    .line 1486
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SmsSyncManager A#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbwf;->r:Lyj;

    invoke-virtual {v3}, Lyj;->j()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": MMS message has no part"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1489
    :cond_0
    move-object/from16 v0, p1

    iget-wide v1, v0, Lbvj;->r:J

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lbwf;->a(J)Ljava/util/List;

    move-result-object v7

    .line 1490
    if-eqz v7, :cond_1

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_2

    .line 1492
    :cond_1
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SmsSyncManager A#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbwf;->r:Lyj;

    invoke-virtual {v3}, Lyj;->j()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": MMS message has no recipient"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1494
    const-wide/16 v8, -0x1

    .line 1636
    :goto_0
    return-wide v8

    .line 1496
    :cond_2
    const/4 v1, 0x1

    move-object/from16 v0, p1

    iget-wide v2, v0, Lbvj;->m:J

    invoke-static {v1, v2, v3}, Lbwf;->c(IJ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1499
    const-wide/16 v8, -0x1

    goto :goto_0

    .line 1501
    :cond_3
    move-object/from16 v0, p1

    iget v1, v0, Lbvj;->n:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_5

    const/4 v1, 0x1

    move v6, v1

    .line 1502
    :goto_1
    const/4 v1, 0x0

    .line 1503
    if-nez v6, :cond_15

    .line 1505
    move-object/from16 v0, p1

    iget-wide v1, v0, Lbvj;->r:J

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lbwf;->a(J)Ljava/util/List;

    move-result-object v1

    move-object/from16 v0, p1

    iget-wide v2, v0, Lbvj;->m:J

    invoke-static {v1, v2, v3}, Lbvx;->a(Ljava/util/List;J)Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    .line 1507
    :goto_2
    const/4 v3, 0x0

    .line 1508
    if-eqz v6, :cond_7

    .line 1509
    move-object/from16 v0, p0

    iget-object v1, v0, Lbwf;->r:Lyj;

    if-eqz v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lbwf;->r:Lyj;

    invoke-virtual {v1}, Lyj;->c()Lbdk;

    move-result-object v1

    :goto_3
    move-object v3, v1

    .line 1513
    :cond_4
    :goto_4
    if-nez v3, :cond_8

    .line 1515
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SmsSyncManager A#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbwf;->r:Lyj;

    invoke-virtual {v3}, Lyj;->j()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": found MMS has no From: id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget-wide v3, v0, Lbvj;->m:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " type="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Lbvj;->n:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " thread_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget-wide v3, v0, Lbvj;->r:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1519
    const-wide/16 v8, -0x1

    goto/16 :goto_0

    .line 1501
    :cond_5
    const/4 v1, 0x0

    move v6, v1

    goto :goto_1

    .line 1509
    :cond_6
    const/4 v1, 0x0

    goto :goto_3

    .line 1510
    :cond_7
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1511
    invoke-static {v5}, Lbdk;->d(Ljava/lang/String;)Lbdk;

    move-result-object v3

    goto :goto_4

    .line 1522
    :cond_8
    invoke-static {v3}, Lyp;->a(Lbdk;)Z

    move-result v1

    .line 1527
    move-object/from16 v0, p1

    iget-wide v8, v0, Lbvj;->r:J

    if-eqz v1, :cond_9

    move-object v1, v3

    :goto_5
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9, v1}, Lbwf;->a(JLbdk;)Ljava/lang/String;

    move-result-object v2

    .line 1529
    if-nez v2, :cond_a

    .line 1531
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SmsSyncManager A#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbwf;->r:Lyj;

    invoke-virtual {v3}, Lyj;->j()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": failed to create conversation for mms thread "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget-wide v3, v0, Lbvj;->r:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1533
    const-wide/16 v8, -0x1

    goto/16 :goto_0

    .line 1527
    :cond_9
    const/4 v1, 0x0

    goto :goto_5

    .line 1536
    :cond_a
    invoke-static {}, Lyt;->h()Ljava/lang/String;

    move-result-object v10

    .line 1539
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 1540
    const-string v1, "message_id"

    invoke-virtual {v8, v1, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1541
    const-string v1, "conversation_id"

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1542
    const-string v1, "transport_type"

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v8, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1543
    const-string v4, "type"

    if-eqz v6, :cond_d

    const/4 v1, 0x1

    :goto_6
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1545
    const-string v1, "author_chat_id"

    iget-object v4, v3, Lbdk;->b:Ljava/lang/String;

    invoke-virtual {v8, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1546
    const-string v1, "author_gaia_id"

    iget-object v4, v3, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v8, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1547
    invoke-virtual/range {p1 .. p1}, Lbvj;->e()Ljava/lang/String;

    move-result-object v4

    .line 1548
    if-eqz v4, :cond_b

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    sget v9, Lbwf;->l:I

    if-le v1, v9, :cond_b

    .line 1552
    const/4 v1, 0x0

    sget v9, Lbwf;->l:I

    invoke-virtual {v4, v1, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 1554
    :cond_b
    const-string v1, "text"

    invoke-virtual {v8, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1555
    const-string v1, "local_url"

    invoke-virtual/range {p1 .. p1}, Lbvj;->f()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v1, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1556
    const-string v1, "attachment_content_type"

    invoke-virtual/range {p1 .. p1}, Lbvj;->g()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v1, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1558
    const-string v1, "width_pixels"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v1, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1559
    const-string v1, "height_pixels"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v1, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1560
    const-string v1, "status"

    const/4 v9, 0x4

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v1, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1561
    const-string v1, "timestamp"

    move-object/from16 v0, p1

    iget-wide v11, v0, Lbvj;->q:J

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v1, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1562
    const-string v1, "notification_level"

    const/4 v9, -0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v1, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1563
    const-string v1, "expiration_timestamp"

    const-wide v11, 0x7fffffffffffffffL

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v1, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1564
    const-string v1, "external_ids"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lbwf;->m:Landroid/net/Uri;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    iget-wide v13, v0, Lbvj;->m:J

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 1565
    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v9, v11

    .line 1564
    invoke-static {v9}, Lf;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v1, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1566
    if-eqz v6, :cond_e

    .line 1567
    const-string v1, "sms_raw_recipients"

    const-string v5, ","

    .line 1568
    invoke-static {v5, v7}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    .line 1567
    invoke-virtual {v8, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1577
    :cond_c
    :goto_7
    const-string v1, "sms_priority"

    move-object/from16 v0, p1

    iget v5, v0, Lbvj;->s:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v8, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1578
    const-string v1, "mms_subject"

    move-object/from16 v0, p1

    iget-object v5, v0, Lbvj;->o:Ljava/lang/String;

    invoke-virtual {v8, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1579
    const-string v1, "sms_message_status"

    const/4 v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v8, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1580
    const-string v1, "sms_type"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v8, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1581
    const-string v1, "sms_message_size"

    invoke-virtual/range {p1 .. p1}, Lbvj;->h()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v8, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1582
    const-string v1, "persisted"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v8, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1583
    move-object/from16 v0, p0

    iget-object v1, v0, Lbwf;->s:Lyt;

    invoke-virtual {v1}, Lyt;->e()Lzr;

    move-result-object v1

    const-string v5, "messages"

    invoke-virtual {v1, v5, v8}, Lzr;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v8

    .line 1585
    const-wide/16 v5, -0x1

    cmp-long v1, v8, v5

    if-nez v1, :cond_f

    .line 1586
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SmsSyncManager A#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbwf;->r:Lyj;

    invoke-virtual {v3}, Lyj;->j()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": failed to insert message into table"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1588
    const-wide/16 v8, -0x1

    goto/16 :goto_0

    .line 1543
    :cond_d
    const/4 v1, 0x2

    goto/16 :goto_6

    .line 1570
    :cond_e
    const-string v1, "sms_raw_sender"

    invoke-virtual {v8, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1572
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    const/4 v6, 0x1

    if-le v1, v6, :cond_c

    .line 1573
    const-string v1, "sms_raw_recipients"

    .line 1574
    invoke-static {v7, v5}, Lbvx;->a(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1573
    invoke-virtual {v8, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1594
    :cond_f
    invoke-virtual/range {p1 .. p1}, Lbvj;->k()I

    move-result v1

    const/4 v5, 0x1

    if-le v1, v5, :cond_11

    .line 1595
    invoke-virtual/range {p1 .. p1}, Lbvj;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_10
    :goto_8
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbvk;

    .line 1596
    invoke-virtual {v1}, Lbvk;->b()Z

    move-result v6

    if-eqz v6, :cond_10

    .line 1597
    new-instance v6, Lzh;

    invoke-direct {v6}, Lzh;-><init>()V

    .line 1602
    iput-object v2, v6, Lzh;->a:Ljava/lang/String;

    .line 1603
    iput-object v10, v6, Lzh;->b:Ljava/lang/String;

    .line 1604
    invoke-virtual {v1}, Lbvk;->d()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lzh;->c:Ljava/lang/String;

    .line 1605
    iget-object v1, v1, Lbvk;->i:Ljava/lang/String;

    iput-object v1, v6, Lzh;->d:Ljava/lang/String;

    .line 1606
    const/4 v1, 0x0

    iput v1, v6, Lzh;->e:I

    .line 1607
    const/4 v1, 0x0

    iput v1, v6, Lzh;->f:I

    .line 1609
    move-object/from16 v0, p0

    iget-object v1, v0, Lbwf;->s:Lyt;

    invoke-virtual {v1, v6}, Lyt;->a(Lzh;)J

    goto :goto_8

    .line 1614
    :cond_11
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lbwf;->y:Z

    move-object/from16 v0, p1

    iget-boolean v5, v0, Lbvj;->v:Z

    or-int/2addr v1, v5

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lbwf;->y:Z

    .line 1616
    const/4 v10, 0x2

    .line 1617
    invoke-virtual/range {p1 .. p1}, Lbvj;->j()I

    move-result v1

    const/4 v5, 0x1

    if-gt v1, v5, :cond_13

    invoke-virtual/range {p1 .. p1}, Lbvj;->k()I

    move-result v1

    const/4 v5, 0x1

    if-ne v1, v5, :cond_13

    .line 1618
    invoke-virtual/range {p1 .. p1}, Lbvj;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lf;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 1619
    const/4 v10, 0x3

    .line 1628
    :cond_12
    :goto_9
    invoke-virtual/range {p1 .. p1}, Lbvj;->f()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    iget-wide v6, v0, Lbvj;->q:J

    .line 1632
    invoke-virtual/range {p1 .. p1}, Lbvj;->g()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    move-object/from16 v0, p1

    iget-boolean v13, v0, Lbvj;->u:Z

    move-object/from16 v0, p1

    iget-wide v14, v0, Lbvj;->r:J

    move-object/from16 v1, p0

    .line 1624
    invoke-direct/range {v1 .. v15}, Lbwf;->a(Ljava/lang/String;Lbdk;Ljava/lang/String;Ljava/lang/String;JJILjava/lang/String;IZJ)V

    goto/16 :goto_0

    .line 1620
    :cond_13
    move-object/from16 v0, p1

    iget-object v1, v0, Lbvj;->w:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v5, 0x1

    if-gt v1, v5, :cond_14

    invoke-virtual/range {p1 .. p1}, Lbvj;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lf;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 1621
    :cond_14
    const/16 v10, 0x9

    goto :goto_9

    :cond_15
    move-object v5, v1

    goto/16 :goto_2
.end method

.method static synthetic a(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 65
    invoke-static {p0}, Lf;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1332
    sget-object v0, Lbwf;->b:Ljava/lang/String;

    return-object v0
.end method

.method private a(JLbdk;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 1694
    iget-object v0, p0, Lbwf;->t:Lew;

    invoke-virtual {v0, p1, p2}, Lew;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1695
    if-eqz v0, :cond_1

    move-object v3, v0

    .line 1733
    :cond_0
    :goto_0
    return-object v3

    .line 1698
    :cond_1
    invoke-direct {p0, p1, p2}, Lbwf;->a(J)Ljava/util/List;

    move-result-object v1

    .line 1699
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_3

    .line 1700
    :cond_2
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SmsSyncManager A#"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbwf;->r:Lyj;

    invoke-virtual {v2}, Lyj;->j()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": could not find recipients for threadId "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1704
    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1705
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1708
    invoke-static {v0, v3, v3}, Lbdh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbdh;

    move-result-object v0

    .line 1709
    if-eqz p3, :cond_4

    iget-object v6, v0, Lbdh;->b:Lbdk;

    invoke-virtual {p3, v6}, Lbdk;->a(Lbdk;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1710
    sget-object v6, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v6, v0, Lbdh;->i:Ljava/lang/Boolean;

    .line 1712
    :cond_4
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1714
    :cond_5
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v5, :cond_6

    const/4 v6, 0x2

    .line 1717
    :goto_2
    iget-object v0, p0, Lbwf;->s:Lyt;

    if-eqz p3, :cond_7

    move v1, v5

    :goto_3
    const/4 v7, 0x3

    move-object v4, v3

    move v8, v5

    move-object v9, v3

    .line 1718
    invoke-static/range {v0 .. v9}, Lyp;->b(Lyt;ZLjava/util/List;Ljava/util/List;Ljava/util/List;ZIIZLbnl;)Ljava/lang/String;

    move-result-object v0

    .line 1729
    if-eqz v0, :cond_0

    .line 1730
    iget-object v1, p0, Lbwf;->t:Lew;

    invoke-virtual {v1, p1, p2, v0}, Lew;->a(JLjava/lang/Object;)V

    move-object v3, v0

    .line 1731
    goto :goto_0

    :cond_6
    move v6, v5

    .line 1714
    goto :goto_2

    .line 1717
    :cond_7
    const/4 v1, 0x0

    goto :goto_3
.end method

.method static synthetic a(Ljava/lang/String;Ljava/lang/String;JJ)Ljava/lang/String;
    .locals 5

    .prologue
    const-wide/16 v3, 0x0

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    cmp-long v1, p2, v3

    if-lez v1, :cond_0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    :cond_0
    cmp-long v1, p4, v3

    if-lez v1, :cond_1

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "<="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(J)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1743
    iget-object v0, p0, Lbwf;->u:Lew;

    invoke-virtual {v0, p1, p2}, Lew;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1744
    if-nez v0, :cond_0

    .line 1745
    invoke-static {p1, p2}, Lbvx;->a(J)Ljava/util/List;

    move-result-object v0

    .line 1746
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1747
    iget-object v1, p0, Lbwf;->u:Lew;

    invoke-virtual {v1, p1, p2, v0}, Lew;->a(JLjava/lang/Object;)V

    .line 1750
    :cond_0
    if-nez v0, :cond_2

    .line 1753
    sget-object v0, Lbwf;->D:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1754
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1755
    sput-object v0, Lbwf;->D:Ljava/util/List;

    invoke-static {}, Lbvx;->f()Lbdh;

    move-result-object v1

    iget-object v1, v1, Lbdh;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1757
    :cond_1
    sget-object v0, Lbwf;->D:Ljava/util/List;

    .line 1759
    :cond_2
    return-object v0
.end method

.method public static a(IJ)V
    .locals 4

    .prologue
    .line 675
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcwz;->a(III)V

    .line 676
    sget-object v1, Lbwf;->o:[Lbyt;

    monitor-enter v1

    .line 677
    const-wide/16 v2, 0x0

    cmp-long v0, p1, v2

    if-ltz v0, :cond_0

    .line 678
    :try_start_0
    sget-object v0, Lbwf;->o:[Lbyt;

    aget-object v0, v0, p0

    invoke-virtual {v0, p1, p2}, Lbyt;->b(J)V

    .line 680
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lbwf;)V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbwf;->z:Z

    return-void
.end method

.method private a(Lew;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lew",
            "<",
            "Lbvj;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1643
    invoke-virtual {p1}, Lew;->a()I

    move-result v8

    move v7, v6

    .line 1644
    :goto_0
    if-ge v7, v8, :cond_4

    .line 1645
    add-int/lit16 v0, v7, 0x80

    invoke-static {v0, v8}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1646
    sub-int v2, v0, v7

    .line 1647
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s != \'%s\' AND %s IN %s"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "ct"

    aput-object v4, v3, v6

    const/4 v4, 0x1

    const-string v5, "application/smil"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "mid"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    .line 1653
    invoke-static {v2}, Lbvx;->b(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 1647
    invoke-static {v0, v1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1654
    new-array v4, v2, [Ljava/lang/String;

    move v1, v6

    .line 1655
    :goto_1
    if-ge v1, v2, :cond_0

    .line 1656
    add-int v0, v7, v1

    invoke-virtual {p1, v0}, Lew;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvj;

    iget-wide v9, v0, Lbvj;->m:J

    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 1655
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1658
    :cond_0
    iget-object v0, p0, Lbwf;->q:Landroid/content/Context;

    iget-object v0, p0, Lbwf;->q:Landroid/content/Context;

    .line 1660
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lbvx;->b:Landroid/net/Uri;

    sget-object v2, Lbvk;->a:[Ljava/lang/String;

    const/4 v5, 0x0

    .line 1658
    invoke-static/range {v0 .. v5}, Lf;->a(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1666
    if-eqz v1, :cond_3

    .line 1668
    :cond_1
    :goto_2
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1672
    const/4 v0, 0x0

    .line 1673
    invoke-static {v1, v0}, Lbvk;->a(Landroid/database/Cursor;Z)Lbvk;

    move-result-object v2

    .line 1674
    iget-wide v3, v2, Lbvk;->h:J

    invoke-virtual {p1, v3, v4}, Lew;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvj;

    .line 1675
    if-eqz v0, :cond_1

    .line 1676
    invoke-virtual {v0, v2}, Lbvj;->a(Lbvk;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 1680
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1644
    :cond_3
    add-int/lit16 v0, v7, 0x80

    move v7, v0

    goto :goto_0

    .line 1684
    :cond_4
    return-void
.end method

.method private a(Ljava/lang/String;Lbdk;Ljava/lang/String;Ljava/lang/String;JJILjava/lang/String;IZJ)V
    .locals 15

    .prologue
    .line 1768
    iget-object v2, p0, Lbwf;->v:Les;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Les;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbwi;

    .line 1769
    if-nez v2, :cond_0

    .line 1770
    new-instance v2, Lbwi;

    invoke-direct {v2}, Lbwi;-><init>()V

    .line 1771
    iget-object v3, p0, Lbwf;->v:Les;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v2}, Les;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1774
    :cond_0
    iget-wide v3, v2, Lbwi;->c:J

    cmp-long v3, p5, v3

    if-ltz v3, :cond_1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    move/from16 v10, p9

    move-object/from16 v11, p10

    move/from16 v12, p11

    move-wide/from16 v13, p13

    .line 1775
    invoke-virtual/range {v2 .. v14}, Lbwi;->a(Lbdk;Ljava/lang/String;Ljava/lang/String;JJILjava/lang/String;IJ)V

    .line 1780
    :cond_1
    if-eqz p12, :cond_2

    iget-wide v3, v2, Lbwi;->i:J

    cmp-long v3, p5, v3

    if-ltz v3, :cond_2

    .line 1781
    move-wide/from16 v0, p5

    invoke-virtual {v2, v0, v1}, Lbwi;->a(J)V

    .line 1783
    :cond_2
    return-void
.end method

.method static synthetic a(Ljava/lang/String;Lbwp;)V
    .locals 2

    .prologue
    .line 65
    sget-object v1, Lbwf;->p:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lbwf;->p:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Ljava/util/List;Lew;Lbvh;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbvl;",
            ">;",
            "Lew",
            "<",
            "Lbvj;",
            ">;",
            "Lbvh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1374
    invoke-virtual {p2}, Lbvh;->a()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1375
    invoke-virtual {p2}, Lbvh;->b()J

    move-result-wide v0

    check-cast p2, Lbvj;

    invoke-virtual {p1, v0, v1, p2}, Lew;->b(JLjava/lang/Object;)V

    .line 1379
    :goto_0
    return-void

    .line 1377
    :cond_0
    check-cast p2, Lbvl;

    invoke-interface {p0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static a(Lyj;)V
    .locals 3

    .prologue
    .line 804
    invoke-static {p0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 805
    invoke-virtual {p0}, Lyj;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 808
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lyj;->e(Z)V

    .line 828
    :goto_0
    return-void

    .line 811
    :cond_0
    sget-boolean v0, Lbwf;->a:Z

    .line 812
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SmsSyncManager A#"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lyj;->j()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": scheduled cleanse"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 814
    sget-object v0, Lbwf;->B:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lbwh;

    invoke-direct {v1, p0}, Lbwh;-><init>(Lyj;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static a(Lyj;ZJ)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 854
    invoke-static {p0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 855
    invoke-virtual {p0}, Lyj;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 861
    invoke-virtual {p0, v1}, Lyj;->d(Z)V

    .line 878
    :cond_0
    :goto_0
    return-void

    .line 866
    :cond_1
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lbwf;->p:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwp;

    invoke-static {v0}, Lbwp;->a(Lbwp;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_0

    .line 867
    sget-boolean v0, Lbwf;->a:Z

    .line 868
    const-string v1, "Babel"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "SmsSyncManager A#"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lyj;->j()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": scheduled"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p1, :cond_5

    const-string v0, " full"

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " sync in "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " milliseconds"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 872
    new-instance v1, Lbwp;

    invoke-direct {v1, p0, p1}, Lbwp;-><init>(Lyj;Z)V

    .line 873
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lbwf;->p:Ljava/util/Map;

    monitor-enter v3

    :try_start_0
    sget-object v0, Lbwf;->p:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v4, Lbwf;->p:Ljava/util/Map;

    invoke-interface {v4, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 875
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbwf;->b(Ljava/lang/String;)V

    .line 876
    sget-object v0, Lbwf;->B:Ljava/util/concurrent/ScheduledExecutorService;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, p2, p3, v2}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto/16 :goto_0

    .line 866
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 868
    :cond_5
    const-string v0, ""

    goto :goto_2

    .line 873
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method private a(IJLbwo;)Z
    .locals 31

    .prologue
    .line 1040
    invoke-static/range {p4 .. p4}, Lcwz;->b(Ljava/lang/Object;)V

    .line 1042
    invoke-virtual/range {p4 .. p4}, Lbwo;->a()V

    .line 1043
    sget-boolean v3, Lbwf;->a:Z

    if-nez v3, :cond_0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lbwf;->A:Z

    .line 1044
    :cond_0
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SmsSyncManager A#"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lbwf;->r:Lyj;

    invoke-virtual {v5}, Lyj;->j()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": batch started with size = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", last ts = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p2

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1048
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v24

    .line 1050
    const/16 v21, 0x1

    .line 1052
    const/4 v12, 0x0

    .line 1053
    const/4 v11, 0x0

    .line 1055
    const/16 v19, 0x0

    .line 1057
    const/16 v20, 0x0

    .line 1059
    const/16 v18, 0x0

    .line 1061
    const/4 v10, 0x0

    .line 1064
    const/4 v9, 0x0

    .line 1066
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 1068
    new-instance v26, Lew;

    invoke-direct/range {v26 .. v26}, Lew;-><init>()V

    .line 1070
    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    .line 1072
    new-instance v14, Ljava/util/HashSet;

    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    .line 1074
    new-instance v15, Ljava/util/HashSet;

    invoke-direct {v15}, Ljava/util/HashSet;-><init>()V

    .line 1079
    :try_start_0
    new-instance v3, Lbwk;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbwf;->s:Lyt;

    move-object/from16 v0, p0

    iget-wide v5, v0, Lbwf;->w:J

    move-wide/from16 v7, p2

    invoke-direct/range {v3 .. v8}, Lbwk;-><init>(Lyt;JJ)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1082
    :try_start_1
    new-instance v7, Lbwn;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lbwf;->w:J

    move-wide/from16 v0, p2

    invoke-direct {v7, v4, v5, v0, v1}, Lbwn;-><init>(JJ)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 1085
    :try_start_2
    invoke-interface {v7}, Lbwj;->a()Lbvh;

    move-result-object v10

    .line 1086
    invoke-interface {v3}, Lbwj;->a()Lbvh;

    move-result-object v5

    move/from16 v22, v11

    move/from16 v23, v12

    .line 1089
    :goto_0
    add-int v4, v23, v22

    sget v6, Lbwf;->j:I

    if-ge v4, v6, :cond_1

    add-int v4, v19, v18

    move/from16 v0, p1

    if-ge v4, v0, :cond_1

    .line 1091
    if-nez v10, :cond_4

    if-nez v5, :cond_4

    .line 1093
    const/16 v21, 0x0

    .line 1196
    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lbwf;->a(Lew;)V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 1208
    invoke-interface {v3}, Lbwj;->b()V

    .line 1211
    invoke-interface {v7}, Lbwj;->b()V

    .line 1216
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v28

    .line 1217
    move-object/from16 v0, p0

    iget-object v3, v0, Lbwf;->s:Lyt;

    invoke-virtual {v3}, Lyt;->a()V

    .line 1220
    :try_start_3
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v30

    :goto_1
    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_23

    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbvl;

    .line 1221
    iget-object v4, v3, Lbvl;->n:Ljava/lang/String;

    if-nez v4, :cond_2

    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SmsSyncManager A#"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lbwf;->r:Lyj;

    invoke-virtual {v6}, Lyj;->j()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": empty sms body."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, ""

    iput-object v4, v3, Lbvl;->n:Ljava/lang/String;

    :cond_2
    iget-object v4, v3, Lbvl;->m:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SmsSyncManager A#"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lbwf;->r:Lyj;

    invoke-virtual {v6}, Lyj;->j()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": empty sms address."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lbvx;->f()Lbdh;

    move-result-object v4

    iget-object v4, v4, Lbdh;->c:Ljava/lang/String;

    iput-object v4, v3, Lbvl;->m:Ljava/lang/String;

    :cond_3
    const/4 v4, 0x0

    iget-wide v5, v3, Lbvl;->o:J

    invoke-static {v4, v5, v6}, Lbwf;->c(IJ)Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v4

    if-eqz v4, :cond_18

    const-wide/16 v10, -0x1

    :goto_2
    const-wide/16 v3, 0x0

    cmp-long v3, v10, v3

    if-lez v3, :cond_27

    .line 1222
    add-int/lit8 v3, v20, 0x1

    :goto_3
    move/from16 v20, v3

    .line 1224
    goto :goto_1

    .line 1095
    :cond_4
    if-nez v10, :cond_5

    if-nez v5, :cond_6

    :cond_5
    if-eqz v5, :cond_7

    if-eqz v10, :cond_7

    .line 1097
    :try_start_4
    invoke-virtual {v5}, Lbvh;->c()J

    move-result-wide v8

    .line 1098
    invoke-virtual {v10}, Lbvh;->c()J

    move-result-wide v11

    cmp-long v4, v8, v11

    if-lez v4, :cond_7

    .line 1101
    :cond_6
    move-object v0, v5

    check-cast v0, Lbwl;

    move-object v4, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1102
    invoke-virtual {v5}, Lbvh;->c()J

    move-result-wide v4

    move-object/from16 v0, p4

    invoke-virtual {v0, v4, v5}, Lbwo;->a(J)V

    .line 1104
    invoke-interface {v3}, Lbwj;->a()Lbvh;

    move-result-object v5

    .line 1105
    add-int/lit8 v9, v23, 0x1

    .line 1106
    add-int/lit8 v18, v18, 0x1

    move/from16 v23, v9

    goto/16 :goto_0

    .line 1107
    :cond_7
    if-nez v5, :cond_8

    if-nez v10, :cond_9

    :cond_8
    if-eqz v5, :cond_a

    if-eqz v10, :cond_a

    .line 1109
    invoke-virtual {v5}, Lbvh;->c()J

    move-result-wide v8

    .line 1110
    invoke-virtual {v10}, Lbvh;->c()J

    move-result-wide v11

    cmp-long v4, v8, v11

    if-gez v4, :cond_a

    .line 1113
    :cond_9
    move-object/from16 v0, v26

    invoke-static {v13, v0, v10}, Lbwf;->a(Ljava/util/List;Lew;Lbvh;)V

    .line 1114
    invoke-virtual {v10}, Lbvh;->c()J

    move-result-wide v8

    move-object/from16 v0, p4

    invoke-virtual {v0, v8, v9}, Lbwo;->a(J)V

    .line 1116
    invoke-interface {v7}, Lbwj;->a()Lbvh;

    move-result-object v10

    .line 1117
    add-int/lit8 v11, v22, 0x1

    .line 1118
    add-int/lit8 v19, v19, 0x1

    move/from16 v22, v11

    goto/16 :goto_0

    .line 1121
    :cond_a
    invoke-virtual {v5}, Lbvh;->c()J

    move-result-wide v16

    .line 1122
    move-object/from16 v0, p4

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Lbwo;->a(J)V

    .line 1124
    invoke-interface {v7}, Lbwj;->a()Lbvh;

    move-result-object v8

    .line 1125
    invoke-interface {v3}, Lbwj;->a()Lbvh;

    move-result-object v6

    .line 1133
    if-eqz v8, :cond_b

    .line 1134
    invoke-virtual {v8}, Lbvh;->c()J

    move-result-wide v11

    cmp-long v4, v11, v16

    if-eqz v4, :cond_d

    :cond_b
    if-eqz v6, :cond_c

    .line 1136
    invoke-virtual {v6}, Lbvh;->c()J

    move-result-wide v11

    cmp-long v4, v11, v16

    if-eqz v4, :cond_d

    .line 1139
    :cond_c
    invoke-virtual {v10, v5}, Lbvh;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2a

    .line 1142
    check-cast v5, Lbwl;

    move-object/from16 v0, v27

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1143
    add-int/lit8 v4, v18, 0x1

    .line 1145
    move-object/from16 v0, v26

    invoke-static {v13, v0, v10}, Lbwf;->a(Ljava/util/List;Lew;Lbvh;)V

    .line 1146
    add-int/lit8 v5, v19, 0x1

    .line 1151
    :goto_4
    add-int/lit8 v9, v23, 0x1

    .line 1152
    add-int/lit8 v11, v22, 0x1

    move-object v10, v8

    move/from16 v18, v4

    move/from16 v19, v5

    move/from16 v22, v11

    move/from16 v23, v9

    move-object v5, v6

    goto/16 :goto_0

    .line 1157
    :cond_d
    invoke-interface {v15}, Ljava/util/Set;->clear()V

    .line 1158
    invoke-interface {v15, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1159
    add-int/lit8 v4, v22, 0x1

    move-object v10, v8

    move v11, v4

    .line 1161
    :goto_5
    if-eqz v10, :cond_e

    .line 1162
    invoke-virtual {v10}, Lbvh;->c()J

    move-result-wide v8

    cmp-long v4, v8, v16

    if-nez v4, :cond_e

    .line 1163
    invoke-interface {v15, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1164
    add-int/lit8 v8, v11, 0x1

    .line 1165
    invoke-interface {v7}, Lbwj;->a()Lbvh;

    move-result-object v4

    move-object v10, v4

    move v11, v8

    goto :goto_5

    .line 1168
    :cond_e
    invoke-interface {v14}, Ljava/util/Set;->clear()V

    .line 1169
    invoke-interface {v14, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1170
    add-int/lit8 v4, v23, 0x1

    move-object v8, v6

    move v9, v4

    .line 1172
    :goto_6
    if-eqz v8, :cond_f

    .line 1173
    invoke-virtual {v8}, Lbvh;->c()J

    move-result-wide v4

    cmp-long v4, v4, v16

    if-nez v4, :cond_f

    .line 1174
    invoke-interface {v14, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1175
    add-int/lit8 v5, v9, 0x1

    .line 1176
    invoke-interface {v3}, Lbwj;->a()Lbvh;

    move-result-object v4

    move-object v8, v4

    move v9, v5

    goto :goto_6

    .line 1179
    :cond_f
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move/from16 v5, v18

    :goto_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lbvh;

    .line 1180
    invoke-interface {v15, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_29

    .line 1181
    check-cast v4, Lbwl;

    move-object/from16 v0, v27

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1182
    add-int/lit8 v4, v5, 0x1

    :goto_8
    move v5, v4

    .line 1184
    goto :goto_7

    .line 1186
    :cond_10
    invoke-interface {v15}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    move/from16 v6, v19

    :goto_9
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_11

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lbvh;

    .line 1187
    invoke-interface {v14, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_28

    .line 1188
    move-object/from16 v0, v26

    invoke-static {v13, v0, v4}, Lbwf;->a(Ljava/util/List;Lew;Lbvh;)V
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 1189
    add-int/lit8 v4, v6, 0x1

    :goto_a
    move v6, v4

    .line 1191
    goto :goto_9

    :cond_11
    move/from16 v18, v5

    move/from16 v19, v6

    move/from16 v22, v11

    move/from16 v23, v9

    move-object v5, v8

    .line 1193
    goto/16 :goto_0

    .line 1197
    :catch_0
    move-exception v3

    move-object v4, v9

    move-object v5, v10

    .line 1198
    :goto_b
    :try_start_5
    const-string v6, "Babel"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "SmsSyncManager: Something\'s seriously wrong -- "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v3}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 1200
    if-eqz v5, :cond_12

    .line 1209
    invoke-interface {v5}, Lbwj;->b()V

    .line 1211
    :cond_12
    if-eqz v4, :cond_13

    .line 1212
    invoke-interface {v4}, Lbwj;->b()V

    :cond_13
    const/4 v3, 0x0

    .line 1265
    :goto_c
    return v3

    .line 1201
    :catch_1
    move-exception v3

    move-object v7, v9

    .line 1205
    :goto_d
    :try_start_6
    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SmsSyncManager: unexpected failure in scan "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v3}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_5

    .line 1206
    if-eqz v10, :cond_14

    .line 1209
    invoke-interface {v10}, Lbwj;->b()V

    .line 1211
    :cond_14
    if-eqz v7, :cond_15

    .line 1212
    invoke-interface {v7}, Lbwj;->b()V

    :cond_15
    const/4 v3, 0x0

    goto :goto_c

    .line 1208
    :catchall_0
    move-exception v3

    move-object v7, v9

    :goto_e
    if-eqz v10, :cond_16

    .line 1209
    invoke-interface {v10}, Lbwj;->b()V

    .line 1211
    :cond_16
    if-eqz v7, :cond_17

    .line 1212
    invoke-interface {v7}, Lbwj;->b()V

    :cond_17
    throw v3

    .line 1221
    :cond_18
    :try_start_7
    iget v4, v3, Lbvl;->r:I

    const/4 v5, 0x1

    if-eq v4, v5, :cond_1a

    const/4 v4, 0x1

    move v7, v4

    :goto_f
    const/4 v5, 0x0

    if-eqz v7, :cond_1c

    move-object/from16 v0, p0

    iget-object v4, v0, Lbwf;->r:Lyj;

    if-eqz v4, :cond_1b

    move-object/from16 v0, p0

    iget-object v4, v0, Lbwf;->r:Lyj;

    invoke-virtual {v4}, Lyj;->c()Lbdk;

    move-result-object v4

    :goto_10
    move-object v5, v4

    :cond_19
    :goto_11
    if-nez v5, :cond_1d

    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SmsSyncManager A#"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lbwf;->r:Lyj;

    invoke-virtual {v6}, Lyj;->j()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": found SMS has no address: id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v3, Lbvl;->o:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " type="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lbvl;->r:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " thread_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v3, Lbvl;->s:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v10, -0x1

    goto/16 :goto_2

    :cond_1a
    const/4 v4, 0x0

    move v7, v4

    goto :goto_f

    :cond_1b
    const/4 v4, 0x0

    goto :goto_10

    :cond_1c
    iget-object v4, v3, Lbvl;->m:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_19

    iget-object v4, v3, Lbvl;->m:Ljava/lang/String;

    invoke-static {v4}, Lbdk;->d(Ljava/lang/String;)Lbdk;

    move-result-object v5

    goto :goto_11

    :cond_1d
    invoke-static {v5}, Lyp;->a(Lbdk;)Z

    move-result v4

    iget-wide v8, v3, Lbvl;->s:J

    if-eqz v4, :cond_1e

    move-object v4, v5

    :goto_12
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9, v4}, Lbwf;->a(JLbdk;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1f

    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SmsSyncManager A#"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lbwf;->r:Lyj;

    invoke-virtual {v6}, Lyj;->j()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": failed to create conversation for sms thread "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v3, Lbvl;->s:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v10, -0x1

    goto/16 :goto_2

    :cond_1e
    const/4 v4, 0x0

    goto :goto_12

    :cond_1f
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "message_id"

    invoke-static {}, Lyt;->h()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "conversation_id"

    invoke-virtual {v8, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "transport_type"

    const/4 v9, 0x3

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "type"

    if-eqz v7, :cond_20

    const/4 v6, 0x1

    :goto_13
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v8, v9, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "author_chat_id"

    iget-object v9, v5, Lbdk;->b:Ljava/lang/String;

    invoke-virtual {v8, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "author_gaia_id"

    iget-object v9, v5, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v8, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, v3, Lbvl;->n:Ljava/lang/String;

    const-string v9, "text"

    invoke-virtual {v8, v9, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "status"

    const/4 v10, 0x4

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "timestamp"

    iget-wide v10, v3, Lbvl;->p:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v9, "notification_level"

    const/4 v10, -0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "expiration_timestamp"

    const-wide v10, 0x7fffffffffffffffL

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v9, "external_ids"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lbwf;->n:Landroid/net/Uri;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-wide v13, v3, Lbvl;->o:J

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v10}, Lf;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "sms_timestamp_sent"

    iget-wide v10, v3, Lbvl;->q:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    if-eqz v7, :cond_21

    const-string v7, "sms_raw_recipients"

    iget-object v9, v3, Lbvl;->m:Ljava/lang/String;

    invoke-virtual {v8, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_14
    const-string v7, "sms_message_status"

    iget v9, v3, Lbvl;->t:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v7, "sms_type"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v7, "persisted"

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v8, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lbwf;->s:Lyt;

    invoke-virtual {v7}, Lyt;->e()Lzr;

    move-result-object v7

    const-string v9, "messages"

    invoke-virtual {v7, v9, v8}, Lzr;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v10

    const-wide/16 v7, -0x1

    cmp-long v7, v10, v7

    if-nez v7, :cond_22

    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SmsSyncManager A#"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lbwf;->r:Lyj;

    invoke-virtual {v5}, Lyj;->j()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": failed to insert sms into table"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v10, -0x1

    goto/16 :goto_2

    :cond_20
    const/4 v6, 0x2

    goto/16 :goto_13

    :cond_21
    const-string v7, "sms_raw_sender"

    iget-object v9, v3, Lbvl;->m:Ljava/lang/String;

    invoke-virtual {v8, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_14

    .line 1241
    :catch_2
    move-exception v3

    .line 1245
    :try_start_8
    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SmsSyncManager: unexpected failure in db txn "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v3}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 1246
    move-object/from16 v0, p0

    iget-object v3, v0, Lbwf;->s:Lyt;

    invoke-virtual {v3}, Lyt;->c()V

    const/4 v3, 0x0

    goto/16 :goto_c

    .line 1221
    :cond_22
    :try_start_9
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lbwf;->x:Z

    iget-boolean v8, v3, Lbvl;->v:Z

    or-int/2addr v7, v8

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lbwf;->x:Z

    const/4 v7, 0x0

    iget-wide v8, v3, Lbvl;->p:J

    const/4 v12, 0x2

    const/4 v13, 0x0

    const/4 v14, 0x0

    iget-boolean v15, v3, Lbvl;->u:Z

    iget-wide v0, v3, Lbvl;->s:J

    move-wide/from16 v16, v0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v17}, Lbwf;->a(Ljava/lang/String;Lbdk;Ljava/lang/String;Ljava/lang/String;JJILjava/lang/String;IZJ)V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto/16 :goto_2

    .line 1246
    :catchall_1
    move-exception v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lbwf;->s:Lyt;

    invoke-virtual {v4}, Lyt;->c()V

    throw v3

    .line 1226
    :cond_23
    const/4 v3, 0x0

    move v5, v3

    move/from16 v4, v20

    :goto_15
    :try_start_a
    invoke-virtual/range {v26 .. v26}, Lew;->a()I

    move-result v3

    if-ge v5, v3, :cond_24

    .line 1227
    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Lew;->a(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbvj;

    .line 1228
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lbwf;->a(Lbvj;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-lez v3, :cond_26

    .line 1229
    add-int/lit8 v3, v4, 0x1

    .line 1226
    :goto_16
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v3

    goto :goto_15

    .line 1233
    :cond_24
    const-string v3, "messages"

    const-string v5, "_id"

    .line 1236
    invoke-static/range {v27 .. v27}, Lbwf;->a(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v6

    .line 1233
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v6}, Lbwf;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 1239
    invoke-direct/range {p0 .. p0}, Lbwf;->k()V

    .line 1240
    move-object/from16 v0, p0

    iget-object v5, v0, Lbwf;->s:Lyt;

    invoke-virtual {v5}, Lyt;->b()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 1248
    move-object/from16 v0, p0

    iget-object v5, v0, Lbwf;->s:Lyt;

    invoke-virtual {v5}, Lyt;->c()V

    .line 1251
    move-object/from16 v0, p0

    iget-object v5, v0, Lbwf;->s:Lyt;

    invoke-static {v5}, Lyp;->d(Lyt;)V

    .line 1252
    move/from16 v0, v23

    move-object/from16 v1, p4

    iput v0, v1, Lbwo;->c:I

    .line 1253
    move/from16 v0, v22

    move-object/from16 v1, p4

    iput v0, v1, Lbwo;->d:I

    .line 1254
    move/from16 v0, v19

    move-object/from16 v1, p4

    iput v0, v1, Lbwo;->e:I

    .line 1255
    move-object/from16 v0, p4

    iput v4, v0, Lbwo;->f:I

    .line 1256
    move/from16 v0, v18

    move-object/from16 v1, p4

    iput v0, v1, Lbwo;->g:I

    .line 1257
    move-object/from16 v0, p4

    iput v3, v0, Lbwo;->h:I

    .line 1258
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    .line 1259
    sub-long v5, v3, v24

    move-object/from16 v0, p4

    iput-wide v5, v0, Lbwo;->a:J

    .line 1260
    sub-long v3, v3, v28

    move-object/from16 v0, p4

    iput-wide v3, v0, Lbwo;->b:J

    .line 1261
    sget-boolean v3, Lbwf;->a:Z

    if-nez v3, :cond_25

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lbwf;->A:Z

    .line 1262
    :cond_25
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SmsSyncManager A#"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lbwf;->r:Lyj;

    invoke-virtual {v5}, Lyj;->j()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": batch done. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1263
    invoke-virtual/range {p4 .. p4}, Lbwo;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1262
    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    move/from16 v3, v21

    .line 1265
    goto/16 :goto_c

    .line 1208
    :catchall_2
    move-exception v4

    move-object v7, v9

    move-object v10, v3

    move-object v3, v4

    goto/16 :goto_e

    :catchall_3
    move-exception v4

    move-object v10, v3

    move-object v3, v4

    goto/16 :goto_e

    :catchall_4
    move-exception v3

    move-object v7, v4

    move-object v10, v5

    goto/16 :goto_e

    :catchall_5
    move-exception v3

    goto/16 :goto_e

    .line 1201
    :catch_3
    move-exception v4

    move-object v7, v9

    move-object v10, v3

    move-object v3, v4

    goto/16 :goto_d

    :catch_4
    move-exception v4

    move-object v10, v3

    move-object v3, v4

    goto/16 :goto_d

    .line 1197
    :catch_5
    move-exception v4

    move-object v5, v3

    move-object v3, v4

    move-object v4, v9

    goto/16 :goto_b

    :catch_6
    move-exception v4

    move-object v5, v3

    move-object v3, v4

    move-object v4, v7

    goto/16 :goto_b

    :cond_26
    move v3, v4

    goto/16 :goto_16

    :cond_27
    move/from16 v3, v20

    goto/16 :goto_3

    :cond_28
    move v4, v6

    goto/16 :goto_a

    :cond_29
    move v4, v5

    goto/16 :goto_8

    :cond_2a
    move/from16 v4, v18

    move/from16 v5, v19

    goto/16 :goto_4
.end method

.method static synthetic a(Lyt;)Z
    .locals 1

    .prologue
    .line 65
    invoke-static {p0}, Lbwf;->b(Lyt;)Z

    move-result v0

    return v0
.end method

.method private static a(Ljava/util/List;)[Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbwl;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1269
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    .line 1270
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v2

    if-ge v1, v0, :cond_0

    .line 1271
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwl;

    invoke-virtual {v0}, Lbwl;->d()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    .line 1270
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1273
    :cond_0
    return-object v2
.end method

.method static synthetic b(Lbwf;)I
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Lbwf;->j()I

    move-result v0

    return v0
.end method

.method private static b(Landroid/net/Uri;)J
    .locals 2

    .prologue
    .line 1317
    if-eqz p0, :cond_0

    .line 1318
    :try_start_0
    invoke-static {p0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v0

    .line 1323
    :goto_0
    return-wide v0

    .line 1322
    :catch_0
    move-exception v0

    .line 1323
    :cond_0
    :goto_1
    const-wide/16 v0, -0x1

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1347
    sget-object v0, Lbwf;->c:Ljava/lang/String;

    return-object v0
.end method

.method public static b(IJ)V
    .locals 4

    .prologue
    .line 684
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcwz;->a(III)V

    .line 685
    sget-object v1, Lbwf;->o:[Lbyt;

    monitor-enter v1

    .line 686
    const-wide/16 v2, 0x0

    cmp-long v0, p1, v2

    if-ltz v0, :cond_0

    .line 687
    :try_start_0
    sget-object v0, Lbwf;->o:[Lbyt;

    aget-object v0, v0, p0

    invoke-virtual {v0, p1, p2}, Lbyt;->c(J)V

    .line 689
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 729
    sget-object v1, Lbwf;->p:Ljava/util/Map;

    monitor-enter v1

    .line 730
    :try_start_0
    sget-object v0, Lbwf;->p:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 731
    invoke-static {p0, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 732
    sget-object v3, Lbwf;->p:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 733
    if-eqz v0, :cond_0

    .line 734
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwp;

    .line 735
    invoke-virtual {v0}, Lbwp;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 740
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public static b(Lyj;)V
    .locals 3

    .prologue
    .line 834
    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    invoke-static {p0, v0, v1, v2}, Lbwf;->a(Lyj;ZJ)V

    .line 835
    return-void
.end method

.method private static b(Lyt;)Z
    .locals 10

    .prologue
    const/4 v6, 0x1

    .line 907
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v7

    .line 909
    :try_start_0
    invoke-virtual {p0}, Lyt;->e()Lzr;

    move-result-object v0

    const-string v1, "messages"

    sget-object v2, Lbwf;->C:[Ljava/lang/String;

    sget-object v3, Lbwf;->d:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {v0}, Lbwf;->a(Landroid/database/Cursor;)I

    move-result v8

    .line 919
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lbwf;->n:Landroid/net/Uri;

    sget-object v2, Lbwf;->C:[Ljava/lang/String;

    .line 922
    sget-object v3, Lbwf;->b:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 917
    invoke-static/range {v0 .. v5}, Lf;->a(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {v0}, Lbwf;->a(Landroid/database/Cursor;)I

    move-result v9

    .line 927
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lbwf;->m:Landroid/net/Uri;

    sget-object v2, Lbwf;->C:[Ljava/lang/String;

    .line 930
    sget-object v3, Lbwf;->c:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 925
    invoke-static/range {v0 .. v5}, Lf;->a(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {v0}, Lbwf;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 933
    add-int v2, v9, v0

    .line 934
    if-ne v8, v2, :cond_0

    move v0, v6

    .line 935
    :goto_0
    sget-boolean v1, Lbwf;->a:Z

    .line 936
    const-string v3, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "SmsSyncManager A#"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v4

    invoke-virtual {v4}, Lyj;->j()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v0, :cond_1

    const-string v1, ": "

    :goto_1
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "local = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", remote = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 947
    :goto_2
    return v0

    .line 934
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 936
    :cond_1
    const-string v1, ": NOT IN SYNC, "
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 941
    :catch_0
    move-exception v0

    .line 942
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SmsSyncManager A#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v3

    invoke-virtual {v3}, Lyj;->j()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": failed to query counts "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v6

    .line 947
    goto :goto_2
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lbwf;->d:Ljava/lang/String;

    return-object v0
.end method

.method public static c(Lyj;)V
    .locals 3

    .prologue
    .line 843
    const/4 v0, 0x1

    const-wide/16 v1, 0x0

    invoke-static {p0, v0, v1, v2}, Lbwf;->a(Lyj;ZJ)V

    .line 844
    return-void
.end method

.method private static c(IJ)Z
    .locals 2

    .prologue
    .line 693
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcwz;->a(III)V

    .line 694
    sget-object v1, Lbwf;->o:[Lbyt;

    monitor-enter v1

    .line 695
    :try_start_0
    sget-object v0, Lbwf;->o:[Lbyt;

    aget-object v0, v0, p0

    invoke-virtual {v0, p1, p2}, Lbyt;->a(J)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 696
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic d()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lbwf;->n:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic e()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lbwf;->m:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic f()Z
    .locals 1

    .prologue
    .line 65
    sget-boolean v0, Lbwf;->a:Z

    return v0
.end method

.method static synthetic g()J
    .locals 2

    .prologue
    .line 65
    sget-wide v0, Lbwf;->k:J

    return-wide v0
.end method

.method static synthetic h()V
    .locals 0

    .prologue
    .line 65
    invoke-static {}, Lbwf;->i()V

    return-void
.end method

.method private static i()V
    .locals 4

    .prologue
    const/16 v3, 0x7d0

    const/16 v1, 0x64

    .line 592
    const-string v0, "babel_sms_sync_first_batch_size"

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lbwf;->e:I

    .line 595
    const-string v0, "babel_sms_sync_subsequent_batch_size_min"

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lbwf;->f:I

    .line 598
    const-string v0, "babel_sms_sync_subsequent_batch_size_max"

    const/16 v1, 0x3e8

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lbwf;->g:I

    .line 601
    const-string v0, "babel_sms_sync_subsequent_batch_time_limit"

    const-wide/16 v1, 0x7d0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Lbwf;->h:J

    .line 604
    const-string v0, "babel_sms_incremental_sync_batch_interval_in_millis"

    const-wide/16 v1, 0x3e8

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Lbwf;->i:J

    .line 607
    const-string v0, "babel_sms_sync_batch_max_messages_to_scan"

    invoke-static {v0, v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lbwf;->j:I

    .line 610
    const-string v0, "babel_sms_full_sync_backoff_time_millis"

    const-wide/32 v1, 0x36ee80

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Lbwf;->k:J

    .line 613
    const-string v0, "babel_mms_text_limit"

    invoke-static {v0, v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lbwf;->l:I

    .line 616
    return-void
.end method

.method private j()I
    .locals 18

    .prologue
    .line 957
    sget-boolean v1, Lbwf;->a:Z

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lbwf;->A:Z

    .line 958
    :cond_0
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SmsSyncManager A#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbwf;->r:Lyj;

    invoke-virtual {v3}, Lyj;->j()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": sync from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v3, v0, Lbwf;->w:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 961
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    .line 962
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v11

    .line 963
    new-instance v13, Lbwo;

    invoke-direct {v13}, Lbwo;-><init>()V

    .line 964
    sget v8, Lbwf;->e:I

    .line 966
    const/4 v7, 0x0

    .line 967
    const/4 v6, 0x0

    .line 968
    const/4 v5, 0x0

    .line 969
    const/4 v4, 0x0

    .line 970
    const-wide/16 v2, -0x1

    .line 971
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lbwf;->x:Z

    .line 972
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lbwf;->y:Z

    .line 973
    const/4 v1, 0x1

    .line 974
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lbwf;->z:Z

    if-nez v14, :cond_5

    .line 975
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v2, v3, v13}, Lbwf;->a(IJLbwo;)Z

    move-result v1

    .line 976
    iget v2, v13, Lbwo;->c:I

    add-int/2addr v7, v2

    .line 977
    iget v2, v13, Lbwo;->d:I

    add-int/2addr v6, v2

    .line 978
    iget v2, v13, Lbwo;->f:I

    add-int/2addr v5, v2

    .line 979
    iget v2, v13, Lbwo;->h:I

    add-int/2addr v4, v2

    .line 980
    if-eqz v1, :cond_5

    .line 981
    iget-wide v2, v13, Lbwo;->b:J

    const-wide/16 v14, 0x0

    cmp-long v2, v2, v14

    if-gtz v2, :cond_3

    sget v8, Lbwf;->e:I

    .line 985
    :cond_2
    :goto_1
    iget-wide v2, v13, Lbwo;->i:J

    const-wide/16 v14, 0x3e8

    div-long/2addr v2, v14

    .line 988
    sget-wide v14, Lbwf;->i:J

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-lez v14, :cond_1

    .line 990
    :try_start_0
    sget-wide v14, Lbwf;->i:J

    invoke-static {v14, v15}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 993
    :catch_0
    move-exception v14

    goto :goto_0

    .line 981
    :cond_3
    iget v2, v13, Lbwo;->h:I

    iget v3, v13, Lbwo;->f:I

    add-int/2addr v2, v3

    int-to-double v2, v2

    iget-wide v14, v13, Lbwo;->b:J

    long-to-double v14, v14

    div-double/2addr v2, v14

    sget-wide v14, Lbwf;->h:J

    long-to-double v14, v14

    mul-double/2addr v2, v14

    double-to-int v8, v2

    sget v2, Lbwf;->f:I

    if-ge v8, v2, :cond_4

    sget v8, Lbwf;->f:I

    goto :goto_1

    :cond_4
    sget v2, Lbwf;->g:I

    if-le v8, v2, :cond_2

    sget v8, Lbwf;->g:I

    goto :goto_1

    .line 996
    :cond_5
    if-nez v1, :cond_9

    .line 998
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lbwf;->x:Z

    if-eqz v1, :cond_6

    .line 999
    sget-object v1, Lbwf;->n:Landroid/net/Uri;

    invoke-static {v1}, Lbvx;->d(Landroid/net/Uri;)V

    .line 1001
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lbwf;->y:Z

    if-eqz v1, :cond_7

    .line 1002
    sget-object v1, Lbwf;->m:Landroid/net/Uri;

    invoke-static {v1}, Lbvx;->d(Landroid/net/Uri;)V

    .line 1004
    :cond_7
    sget-boolean v1, Lbwf;->a:Z

    if-nez v1, :cond_8

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lbwf;->A:Z

    .line 1005
    :cond_8
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SmsSyncManager A#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbwf;->r:Lyj;

    invoke-virtual {v3}, Lyj;->j()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": sync done in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1006
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v13

    sub-long v11, v13, v11

    invoke-virtual {v2, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " millis, scanned "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " local msgs, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " remote msgs, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " added, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " deleted."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1005
    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1013
    move-object/from16 v0, p0

    iget-object v1, v0, Lbwf;->r:Lyj;

    invoke-static {v1}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v1

    const-string v2, "sms_last_sync_time_millis"

    invoke-virtual {v1, v2, v9, v10}, Lbsx;->b(Ljava/lang/String;J)V

    .line 1016
    add-int v1, v5, v4

    .line 1021
    :goto_2
    return v1

    .line 1018
    :cond_9
    sget-boolean v1, Lbwf;->a:Z

    if-nez v1, :cond_a

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lbwf;->A:Z

    .line 1019
    :cond_a
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SmsSyncManager A#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbwf;->r:Lyj;

    invoke-virtual {v3}, Lyj;->j()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": sync aborted"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1021
    const/4 v1, -0x1

    goto :goto_2
.end method

.method private k()V
    .locals 18

    .prologue
    .line 1790
    move-object/from16 v0, p0

    iget-object v1, v0, Lbwf;->v:Les;

    invoke-virtual {v1}, Les;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_0
    :goto_0
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1791
    move-object/from16 v0, p0

    iget-object v1, v0, Lbwf;->v:Les;

    invoke-virtual {v1, v2}, Les;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lbwi;

    .line 1792
    move-object/from16 v0, v16

    iget-boolean v1, v0, Lbwi;->k:Z

    if-nez v1, :cond_0

    .line 1796
    move-object/from16 v0, p0

    iget-object v1, v0, Lbwf;->s:Lyt;

    move-object/from16 v0, v16

    iget-wide v3, v0, Lbwi;->c:J

    const-wide/16 v5, -0x1

    move-object/from16 v0, v16

    iget v7, v0, Lbwi;->f:I

    move-object/from16 v0, v16

    iget-object v8, v0, Lbwi;->g:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v9, v0, Lbwi;->a:Lbdk;

    move-object/from16 v0, v16

    iget-object v10, v0, Lbwi;->b:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v11, v0, Lbwi;->e:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-wide v12, v0, Lbwi;->d:J

    move-object/from16 v0, v16

    iget v14, v0, Lbwi;->h:I

    const/4 v15, 0x0

    invoke-virtual/range {v1 .. v15}, Lyt;->a(Ljava/lang/String;JJILjava/lang/String;Lbdk;Ljava/lang/String;Ljava/lang/String;JII)Z

    .line 1807
    move-object/from16 v0, p0

    iget-object v1, v0, Lbwf;->s:Lyt;

    move-object/from16 v0, v16

    iget-wide v3, v0, Lbwi;->c:J

    invoke-virtual {v1, v2, v3, v4}, Lyt;->k(Ljava/lang/String;J)V

    .line 1809
    move-object/from16 v0, p0

    iget-object v1, v0, Lbwf;->s:Lyt;

    move-object/from16 v0, v16

    iget-wide v3, v0, Lbwi;->i:J

    invoke-virtual {v1, v2, v3, v4}, Lyt;->c(Ljava/lang/String;J)V

    .line 1811
    move-object/from16 v0, p0

    iget-object v1, v0, Lbwf;->s:Lyt;

    move-object/from16 v0, v16

    iget-wide v3, v0, Lbwi;->j:J

    invoke-virtual {v1, v2, v3, v4}, Lyt;->p(Ljava/lang/String;J)V

    .line 1812
    const/4 v1, 0x1

    move-object/from16 v0, v16

    iput-boolean v1, v0, Lbwi;->k:Z

    goto :goto_0

    .line 1815
    :cond_1
    return-void
.end method
