.class public final Leqf;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leqf;

.field public static final b:Lepo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepo",
            "<",
            "Leqf;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Lepo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepo",
            "<",
            "Leqf;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Leqc;

.field public j:Ljava/lang/String;

.field public k:[Lepu;

.field public l:Lepu;

.field public m:[Lepu;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/Double;

.field public p:Ljava/lang/Double;

.field public q:Ljava/lang/String;

.field public r:Lepu;

.field public s:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const v1, 0x2308eed

    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Leqf;

    sput-object v0, Leqf;->a:[Leqf;

    .line 13
    new-instance v0, Leqg;

    invoke-direct {v0}, Leqg;-><init>()V

    .line 14
    invoke-static {v1, v0}, Lepo;->a(ILepp;)Lepo;

    move-result-object v0

    sput-object v0, Leqf;->b:Lepo;

    .line 17
    new-instance v0, Leqh;

    invoke-direct {v0}, Leqh;-><init>()V

    .line 18
    invoke-static {v1, v0}, Lepo;->a(ILepp;)Lepo;

    move-result-object v0

    sput-object v0, Leqf;->c:Lepo;

    .line 17
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 31
    iput-object v1, p0, Leqf;->i:Leqc;

    .line 36
    sget-object v0, Lepu;->a:[Lepu;

    iput-object v0, p0, Leqf;->k:[Lepu;

    .line 39
    iput-object v1, p0, Leqf;->l:Lepu;

    .line 42
    sget-object v0, Lepu;->a:[Lepu;

    iput-object v0, p0, Leqf;->m:[Lepu;

    .line 53
    iput-object v1, p0, Leqf;->r:Lepu;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 123
    iget-object v0, p0, Leqf;->d:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 124
    const/4 v0, 0x1

    iget-object v2, p0, Leqf;->d:Ljava/lang/String;

    .line 125
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 127
    :goto_0
    iget-object v2, p0, Leqf;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 128
    const/4 v2, 0x2

    iget-object v3, p0, Leqf;->e:Ljava/lang/String;

    .line 129
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 131
    :cond_0
    iget-object v2, p0, Leqf;->f:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 132
    const/4 v2, 0x3

    iget-object v3, p0, Leqf;->f:Ljava/lang/String;

    .line 133
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 135
    :cond_1
    iget-object v2, p0, Leqf;->g:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 136
    const/4 v2, 0x4

    iget-object v3, p0, Leqf;->g:Ljava/lang/String;

    .line 137
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 139
    :cond_2
    iget-object v2, p0, Leqf;->h:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 140
    const/4 v2, 0x5

    iget-object v3, p0, Leqf;->h:Ljava/lang/String;

    .line 141
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 143
    :cond_3
    iget-object v2, p0, Leqf;->i:Leqc;

    if-eqz v2, :cond_4

    .line 144
    const/4 v2, 0x6

    iget-object v3, p0, Leqf;->i:Leqc;

    .line 145
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 147
    :cond_4
    iget-object v2, p0, Leqf;->j:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 148
    const/4 v2, 0x7

    iget-object v3, p0, Leqf;->j:Ljava/lang/String;

    .line 149
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 151
    :cond_5
    iget-object v2, p0, Leqf;->k:[Lepu;

    if-eqz v2, :cond_7

    .line 152
    iget-object v3, p0, Leqf;->k:[Lepu;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 153
    if-eqz v5, :cond_6

    .line 154
    const/16 v6, 0x8

    .line 155
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 152
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 159
    :cond_7
    iget-object v2, p0, Leqf;->l:Lepu;

    if-eqz v2, :cond_8

    .line 160
    const/16 v2, 0x9

    iget-object v3, p0, Leqf;->l:Lepu;

    .line 161
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 163
    :cond_8
    iget-object v2, p0, Leqf;->m:[Lepu;

    if-eqz v2, :cond_a

    .line 164
    iget-object v2, p0, Leqf;->m:[Lepu;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_a

    aget-object v4, v2, v1

    .line 165
    if-eqz v4, :cond_9

    .line 166
    const/16 v5, 0xb

    .line 167
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 164
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 171
    :cond_a
    iget-object v1, p0, Leqf;->n:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 172
    const/16 v1, 0xc

    iget-object v2, p0, Leqf;->n:Ljava/lang/String;

    .line 173
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 175
    :cond_b
    iget-object v1, p0, Leqf;->o:Ljava/lang/Double;

    if-eqz v1, :cond_c

    .line 176
    const/16 v1, 0x24

    iget-object v2, p0, Leqf;->o:Ljava/lang/Double;

    .line 177
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 179
    :cond_c
    iget-object v1, p0, Leqf;->p:Ljava/lang/Double;

    if-eqz v1, :cond_d

    .line 180
    const/16 v1, 0x25

    iget-object v2, p0, Leqf;->p:Ljava/lang/Double;

    .line 181
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 183
    :cond_d
    iget-object v1, p0, Leqf;->q:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 184
    const/16 v1, 0x4b

    iget-object v2, p0, Leqf;->q:Ljava/lang/String;

    .line 185
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 187
    :cond_e
    iget-object v1, p0, Leqf;->r:Lepu;

    if-eqz v1, :cond_f

    .line 188
    const/16 v1, 0xb9

    iget-object v2, p0, Leqf;->r:Lepu;

    .line 189
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 191
    :cond_f
    iget-object v1, p0, Leqf;->s:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 192
    const/16 v1, 0xfe

    iget-object v2, p0, Leqf;->s:Ljava/lang/String;

    .line 193
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 195
    :cond_10
    iget-object v1, p0, Leqf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 196
    iput v0, p0, Leqf;->cachedSize:I

    .line 197
    return v0

    :cond_11
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Leqf;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Leqf;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Leqf;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leqf;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leqf;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leqf;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leqf;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leqf;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Leqf;->i:Leqc;

    if-nez v0, :cond_2

    new-instance v0, Leqc;

    invoke-direct {v0}, Leqc;-><init>()V

    iput-object v0, p0, Leqf;->i:Leqc;

    :cond_2
    iget-object v0, p0, Leqf;->i:Leqc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leqf;->j:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leqf;->k:[Lepu;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lepu;

    iget-object v3, p0, Leqf;->k:[Lepu;

    if-eqz v3, :cond_3

    iget-object v3, p0, Leqf;->k:[Lepu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Leqf;->k:[Lepu;

    :goto_2
    iget-object v2, p0, Leqf;->k:[Lepu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Leqf;->k:[Lepu;

    new-instance v3, Lepu;

    invoke-direct {v3}, Lepu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leqf;->k:[Lepu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Leqf;->k:[Lepu;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Leqf;->k:[Lepu;

    new-instance v3, Lepu;

    invoke-direct {v3}, Lepu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leqf;->k:[Lepu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Leqf;->l:Lepu;

    if-nez v0, :cond_6

    new-instance v0, Lepu;

    invoke-direct {v0}, Lepu;-><init>()V

    iput-object v0, p0, Leqf;->l:Lepu;

    :cond_6
    iget-object v0, p0, Leqf;->l:Lepu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_a
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leqf;->m:[Lepu;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lepu;

    iget-object v3, p0, Leqf;->m:[Lepu;

    if-eqz v3, :cond_7

    iget-object v3, p0, Leqf;->m:[Lepu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Leqf;->m:[Lepu;

    :goto_4
    iget-object v2, p0, Leqf;->m:[Lepu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Leqf;->m:[Lepu;

    new-instance v3, Lepu;

    invoke-direct {v3}, Lepu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leqf;->m:[Lepu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    iget-object v0, p0, Leqf;->m:[Lepu;

    array-length v0, v0

    goto :goto_3

    :cond_9
    iget-object v2, p0, Leqf;->m:[Lepu;

    new-instance v3, Lepu;

    invoke-direct {v3}, Lepu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leqf;->m:[Lepu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leqf;->n:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->b()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Leqf;->o:Ljava/lang/Double;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->b()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Leqf;->p:Ljava/lang/Double;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leqf;->q:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Leqf;->r:Lepu;

    if-nez v0, :cond_a

    new-instance v0, Lepu;

    invoke-direct {v0}, Lepu;-><init>()V

    iput-object v0, p0, Leqf;->r:Lepu;

    :cond_a
    iget-object v0, p0, Leqf;->r:Lepu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leqf;->s:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x121 -> :sswitch_c
        0x129 -> :sswitch_d
        0x25a -> :sswitch_e
        0x5ca -> :sswitch_f
        0x7f2 -> :sswitch_10
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 60
    iget-object v1, p0, Leqf;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 61
    const/4 v1, 0x1

    iget-object v2, p0, Leqf;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 63
    :cond_0
    iget-object v1, p0, Leqf;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 64
    const/4 v1, 0x2

    iget-object v2, p0, Leqf;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 66
    :cond_1
    iget-object v1, p0, Leqf;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 67
    const/4 v1, 0x3

    iget-object v2, p0, Leqf;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 69
    :cond_2
    iget-object v1, p0, Leqf;->g:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 70
    const/4 v1, 0x4

    iget-object v2, p0, Leqf;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 72
    :cond_3
    iget-object v1, p0, Leqf;->h:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 73
    const/4 v1, 0x5

    iget-object v2, p0, Leqf;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 75
    :cond_4
    iget-object v1, p0, Leqf;->i:Leqc;

    if-eqz v1, :cond_5

    .line 76
    const/4 v1, 0x6

    iget-object v2, p0, Leqf;->i:Leqc;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 78
    :cond_5
    iget-object v1, p0, Leqf;->j:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 79
    const/4 v1, 0x7

    iget-object v2, p0, Leqf;->j:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 81
    :cond_6
    iget-object v1, p0, Leqf;->k:[Lepu;

    if-eqz v1, :cond_8

    .line 82
    iget-object v2, p0, Leqf;->k:[Lepu;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 83
    if-eqz v4, :cond_7

    .line 84
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 82
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 88
    :cond_8
    iget-object v1, p0, Leqf;->l:Lepu;

    if-eqz v1, :cond_9

    .line 89
    const/16 v1, 0x9

    iget-object v2, p0, Leqf;->l:Lepu;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 91
    :cond_9
    iget-object v1, p0, Leqf;->m:[Lepu;

    if-eqz v1, :cond_b

    .line 92
    iget-object v1, p0, Leqf;->m:[Lepu;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_b

    aget-object v3, v1, v0

    .line 93
    if-eqz v3, :cond_a

    .line 94
    const/16 v4, 0xb

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 92
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 98
    :cond_b
    iget-object v0, p0, Leqf;->n:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 99
    const/16 v0, 0xc

    iget-object v1, p0, Leqf;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 101
    :cond_c
    iget-object v0, p0, Leqf;->o:Ljava/lang/Double;

    if-eqz v0, :cond_d

    .line 102
    const/16 v0, 0x24

    iget-object v1, p0, Leqf;->o:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(ID)V

    .line 104
    :cond_d
    iget-object v0, p0, Leqf;->p:Ljava/lang/Double;

    if-eqz v0, :cond_e

    .line 105
    const/16 v0, 0x25

    iget-object v1, p0, Leqf;->p:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(ID)V

    .line 107
    :cond_e
    iget-object v0, p0, Leqf;->q:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 108
    const/16 v0, 0x4b

    iget-object v1, p0, Leqf;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 110
    :cond_f
    iget-object v0, p0, Leqf;->r:Lepu;

    if-eqz v0, :cond_10

    .line 111
    const/16 v0, 0xb9

    iget-object v1, p0, Leqf;->r:Lepu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 113
    :cond_10
    iget-object v0, p0, Leqf;->s:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 114
    const/16 v0, 0xfe

    iget-object v1, p0, Leqf;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 116
    :cond_11
    iget-object v0, p0, Leqf;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 118
    return-void
.end method
