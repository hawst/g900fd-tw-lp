.class public final Ldkh;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldkh;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:[Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 633
    const/4 v0, 0x0

    new-array v0, v0, [Ldkh;

    sput-object v0, Ldkh;->a:[Ldkh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 634
    invoke-direct {p0}, Lepn;-><init>()V

    .line 637
    const/4 v0, 0x0

    iput-object v0, p0, Ldkh;->b:Ljava/lang/Integer;

    .line 642
    sget-object v0, Lept;->m:[Ljava/lang/Integer;

    iput-object v0, p0, Ldkh;->d:[Ljava/lang/Integer;

    .line 634
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 665
    iget-object v0, p0, Ldkh;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 666
    const/4 v0, 0x1

    iget-object v2, p0, Ldkh;->b:Ljava/lang/Integer;

    .line 667
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 669
    :goto_0
    iget-object v2, p0, Ldkh;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 670
    const/4 v2, 0x2

    iget-object v3, p0, Ldkh;->c:Ljava/lang/Integer;

    .line 671
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 673
    :cond_0
    iget-object v2, p0, Ldkh;->d:[Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ldkh;->d:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 675
    iget-object v3, p0, Ldkh;->d:[Ljava/lang/Integer;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 677
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 675
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 679
    :cond_1
    add-int/2addr v0, v2

    .line 680
    iget-object v1, p0, Ldkh;->d:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 682
    :cond_2
    iget-object v1, p0, Ldkh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 683
    iput v0, p0, Ldkh;->cachedSize:I

    .line 684
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 630
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldkh;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldkh;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldkh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v3, :cond_2

    if-eqz v0, :cond_2

    const/16 v1, 0x118

    if-eq v0, v1, :cond_2

    const/16 v1, 0x168

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1b8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x208

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldkh;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldkh;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldkh;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldkh;->d:[Ljava/lang/Integer;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    iget-object v2, p0, Ldkh;->d:[Ljava/lang/Integer;

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldkh;->d:[Ljava/lang/Integer;

    :goto_1
    iget-object v1, p0, Ldkh;->d:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Ldkh;->d:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iget-object v1, p0, Ldkh;->d:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 647
    iget-object v0, p0, Ldkh;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 648
    const/4 v0, 0x1

    iget-object v1, p0, Ldkh;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 650
    :cond_0
    iget-object v0, p0, Ldkh;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 651
    const/4 v0, 0x2

    iget-object v1, p0, Ldkh;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 653
    :cond_1
    iget-object v0, p0, Ldkh;->d:[Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 654
    iget-object v1, p0, Ldkh;->d:[Ljava/lang/Integer;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 655
    const/4 v4, 0x3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 654
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 658
    :cond_2
    iget-object v0, p0, Ldkh;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 660
    return-void
.end method
