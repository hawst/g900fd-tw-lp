.class public abstract enum Legd;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Legd;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Legd;

.field public static final enum b:Legd;

.field public static final enum c:Legd;

.field private static final synthetic d:[Legd;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 292
    new-instance v0, Lege;

    const-string v1, "STRONG"

    invoke-direct {v0, v1}, Lege;-><init>(Ljava/lang/String;)V

    sput-object v0, Legd;->a:Legd;

    .line 306
    new-instance v0, Legf;

    const-string v1, "SOFT"

    invoke-direct {v0, v1}, Legf;-><init>(Ljava/lang/String;)V

    sput-object v0, Legd;->b:Legd;

    .line 320
    new-instance v0, Legg;

    const-string v1, "WEAK"

    invoke-direct {v0, v1}, Legg;-><init>(Ljava/lang/String;)V

    sput-object v0, Legd;->c:Legd;

    .line 286
    const/4 v0, 0x3

    new-array v0, v0, [Legd;

    const/4 v1, 0x0

    sget-object v2, Legd;->a:Legd;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Legd;->b:Legd;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Legd;->c:Legd;

    aput-object v2, v0, v1

    sput-object v0, Legd;->d:[Legd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 286
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 286
    invoke-direct {p0, p1, p2}, Legd;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Legd;
    .locals 1

    .prologue
    .line 286
    const-class v0, Legd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Legd;

    return-object v0
.end method

.method public static values()[Legd;
    .locals 1

    .prologue
    .line 286
    sget-object v0, Legd;->d:[Legd;

    invoke-virtual {v0}, [Legd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Legd;

    return-object v0
.end method


# virtual methods
.method public abstract a()Leba;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Leba",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method abstract a(Lega;Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;Ljava/lang/Object;)Legn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lega",
            "<TK;TV;>;",
            "Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry",
            "<TK;TV;>;TV;)",
            "Legn",
            "<TK;TV;>;"
        }
    .end annotation
.end method
