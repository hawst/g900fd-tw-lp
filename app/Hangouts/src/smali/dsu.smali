.class public final Ldsu;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldsu;


# instance fields
.field public b:Ldro;

.field public c:Ldqf;

.field public d:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19283
    const/4 v0, 0x0

    new-array v0, v0, [Ldsu;

    sput-object v0, Ldsu;->a:[Ldsu;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 19284
    invoke-direct {p0}, Lepn;-><init>()V

    .line 19287
    iput-object v0, p0, Ldsu;->b:Ldro;

    .line 19290
    iput-object v0, p0, Ldsu;->c:Ldqf;

    .line 19293
    iput-object v0, p0, Ldsu;->d:Ljava/lang/Integer;

    .line 19284
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 19313
    const/4 v0, 0x0

    .line 19314
    iget-object v1, p0, Ldsu;->b:Ldro;

    if-eqz v1, :cond_0

    .line 19315
    const/4 v0, 0x1

    iget-object v1, p0, Ldsu;->b:Ldro;

    .line 19316
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 19318
    :cond_0
    iget-object v1, p0, Ldsu;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 19319
    const/4 v1, 0x2

    iget-object v2, p0, Ldsu;->d:Ljava/lang/Integer;

    .line 19320
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 19322
    :cond_1
    iget-object v1, p0, Ldsu;->c:Ldqf;

    if-eqz v1, :cond_2

    .line 19323
    const/4 v1, 0x3

    iget-object v2, p0, Ldsu;->c:Ldqf;

    .line 19324
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19326
    :cond_2
    iget-object v1, p0, Ldsu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19327
    iput v0, p0, Ldsu;->cachedSize:I

    .line 19328
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 19280
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldsu;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldsu;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldsu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldsu;->b:Ldro;

    if-nez v0, :cond_2

    new-instance v0, Ldro;

    invoke-direct {v0}, Ldro;-><init>()V

    iput-object v0, p0, Ldsu;->b:Ldro;

    :cond_2
    iget-object v0, p0, Ldsu;->b:Ldro;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldsu;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldsu;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldsu;->c:Ldqf;

    if-nez v0, :cond_5

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldsu;->c:Ldqf;

    :cond_5
    iget-object v0, p0, Ldsu;->c:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 19298
    iget-object v0, p0, Ldsu;->b:Ldro;

    if-eqz v0, :cond_0

    .line 19299
    const/4 v0, 0x1

    iget-object v1, p0, Ldsu;->b:Ldro;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 19301
    :cond_0
    iget-object v0, p0, Ldsu;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 19302
    const/4 v0, 0x2

    iget-object v1, p0, Ldsu;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 19304
    :cond_1
    iget-object v0, p0, Ldsu;->c:Ldqf;

    if-eqz v0, :cond_2

    .line 19305
    const/4 v0, 0x3

    iget-object v1, p0, Ldsu;->c:Ldqf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 19307
    :cond_2
    iget-object v0, p0, Ldsu;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 19309
    return-void
.end method
