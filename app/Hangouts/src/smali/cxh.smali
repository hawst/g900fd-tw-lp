.class public final Lcxh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lcxa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcxa",
            "<",
            "Lcxi;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcxh;->b:Ljava/lang/Object;

    .line 66
    new-instance v0, Lcxa;

    const/16 v1, 0x1f4

    invoke-direct {v0, v1}, Lcxa;-><init>(I)V

    iput-object v0, p0, Lcxh;->a:Lcxa;

    .line 67
    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 70
    iget-object v7, p0, Lcxh;->b:Ljava/lang/Object;

    monitor-enter v7

    .line 71
    :try_start_0
    iget-object v0, p0, Lcxh;->a:Lcxa;

    invoke-virtual {v0}, Lcxa;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxi;

    .line 72
    if-nez v0, :cond_0

    .line 73
    new-instance v0, Lcxi;

    invoke-direct {v0}, Lcxi;-><init>()V

    .line 75
    :cond_0
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    move v2, p1

    move-object v5, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, Lcxi;->a(IIJLjava/lang/String;Ljava/lang/String;)V

    .line 76
    iget-object v1, p0, Lcxh;->a:Lcxa;

    invoke-virtual {v1, v0}, Lcxa;->a(Ljava/lang/Object;)V

    .line 77
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0
.end method

.method public a(Ljava/io/PrintWriter;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 86
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v1, "MM-dd HH:mm:ss.SSS"

    invoke-direct {v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 87
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    .line 89
    iget-object v4, p0, Lcxh;->b:Ljava/lang/Object;

    monitor-enter v4

    move v1, v0

    .line 90
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcxh;->a:Lcxa;

    invoke-virtual {v0}, Lcxa;->a()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 91
    iget-object v0, p0, Lcxh;->a:Lcxa;

    invoke-virtual {v0, v1}, Lcxa;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxi;

    .line 92
    const-string v5, "%s %5d %5d %s %s: %s"

    const/4 v6, 0x6

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-wide v8, v0, Lcxi;->c:J

    .line 93
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    .line 94
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    iget v8, v0, Lcxi;->a:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    iget-object v8, v0, Lcxi;->b:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x4

    iget-object v8, v0, Lcxi;->d:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x5

    iget-object v0, v0, Lcxi;->e:Ljava/lang/String;

    aput-object v0, v6, v7

    .line 92
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 90
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 96
    :cond_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0
.end method
