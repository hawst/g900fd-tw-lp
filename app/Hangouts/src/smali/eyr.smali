.class public final Leyr;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leyr;


# instance fields
.field public b:[Ljava/lang/Float;

.field public c:[Ljava/lang/Float;

.field public d:[Ljava/lang/Float;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Leyr;

    sput-object v0, Leyr;->a:[Leyr;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 13
    sget-object v0, Lept;->o:[Ljava/lang/Float;

    iput-object v0, p0, Leyr;->b:[Ljava/lang/Float;

    .line 16
    sget-object v0, Lept;->o:[Ljava/lang/Float;

    iput-object v0, p0, Leyr;->c:[Ljava/lang/Float;

    .line 19
    sget-object v0, Lept;->o:[Ljava/lang/Float;

    iput-object v0, p0, Leyr;->d:[Ljava/lang/Float;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 55
    const/4 v0, 0x0

    .line 56
    iget-object v1, p0, Leyr;->b:[Ljava/lang/Float;

    if-eqz v1, :cond_0

    iget-object v1, p0, Leyr;->b:[Ljava/lang/Float;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 57
    iget-object v0, p0, Leyr;->b:[Ljava/lang/Float;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x4

    .line 58
    iget-object v1, p0, Leyr;->b:[Ljava/lang/Float;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 61
    :cond_0
    iget-object v1, p0, Leyr;->c:[Ljava/lang/Float;

    if-eqz v1, :cond_1

    iget-object v1, p0, Leyr;->c:[Ljava/lang/Float;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 62
    iget-object v1, p0, Leyr;->c:[Ljava/lang/Float;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    .line 63
    add-int/2addr v0, v1

    .line 64
    iget-object v1, p0, Leyr;->c:[Ljava/lang/Float;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 66
    :cond_1
    iget-object v1, p0, Leyr;->d:[Ljava/lang/Float;

    if-eqz v1, :cond_2

    iget-object v1, p0, Leyr;->d:[Ljava/lang/Float;

    array-length v1, v1

    if-lez v1, :cond_2

    .line 67
    iget-object v1, p0, Leyr;->d:[Ljava/lang/Float;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    .line 68
    add-int/2addr v0, v1

    .line 69
    iget-object v1, p0, Leyr;->d:[Ljava/lang/Float;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 71
    :cond_2
    iget-object v1, p0, Leyr;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 72
    const/4 v1, 0x4

    iget-object v2, p0, Leyr;->e:Ljava/lang/Integer;

    .line 73
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    :cond_3
    iget-object v1, p0, Leyr;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 76
    const/4 v1, 0x5

    iget-object v2, p0, Leyr;->f:Ljava/lang/Integer;

    .line 77
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    :cond_4
    iget-object v1, p0, Leyr;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    iput v0, p0, Leyr;->cachedSize:I

    .line 81
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leyr;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leyr;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leyr;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xd

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Leyr;->b:[Ljava/lang/Float;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Float;

    iget-object v2, p0, Leyr;->b:[Ljava/lang/Float;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Leyr;->b:[Ljava/lang/Float;

    :goto_1
    iget-object v1, p0, Leyr;->b:[Ljava/lang/Float;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Leyr;->b:[Ljava/lang/Float;

    invoke-virtual {p1}, Lepk;->c()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Leyr;->b:[Ljava/lang/Float;

    invoke-virtual {p1}, Lepk;->c()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x15

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Leyr;->c:[Ljava/lang/Float;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Float;

    iget-object v2, p0, Leyr;->c:[Ljava/lang/Float;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Leyr;->c:[Ljava/lang/Float;

    :goto_2
    iget-object v1, p0, Leyr;->c:[Ljava/lang/Float;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Leyr;->c:[Ljava/lang/Float;

    invoke-virtual {p1}, Lepk;->c()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v1, p0, Leyr;->c:[Ljava/lang/Float;

    invoke-virtual {p1}, Lepk;->c()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    :sswitch_3
    const/16 v0, 0x1d

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Leyr;->d:[Ljava/lang/Float;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Float;

    iget-object v2, p0, Leyr;->d:[Ljava/lang/Float;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Leyr;->d:[Ljava/lang/Float;

    :goto_3
    iget-object v1, p0, Leyr;->d:[Ljava/lang/Float;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Leyr;->d:[Ljava/lang/Float;

    invoke-virtual {p1}, Lepk;->c()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    iget-object v1, p0, Leyr;->d:[Ljava/lang/Float;

    invoke-virtual {p1}, Lepk;->c()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyr;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyr;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 28
    iget-object v1, p0, Leyr;->b:[Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 29
    iget-object v2, p0, Leyr;->b:[Ljava/lang/Float;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 30
    const/4 v5, 0x1

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {p1, v5, v4}, Lepl;->a(IF)V

    .line 29
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 33
    :cond_0
    iget-object v1, p0, Leyr;->c:[Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 34
    iget-object v2, p0, Leyr;->c:[Ljava/lang/Float;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 35
    const/4 v5, 0x2

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {p1, v5, v4}, Lepl;->a(IF)V

    .line 34
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 38
    :cond_1
    iget-object v1, p0, Leyr;->d:[Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 39
    iget-object v1, p0, Leyr;->d:[Ljava/lang/Float;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 40
    const/4 v4, 0x3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {p1, v4, v3}, Lepl;->a(IF)V

    .line 39
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 43
    :cond_2
    iget-object v0, p0, Leyr;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 44
    const/4 v0, 0x4

    iget-object v1, p0, Leyr;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 46
    :cond_3
    iget-object v0, p0, Leyr;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 47
    const/4 v0, 0x5

    iget-object v1, p0, Leyr;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 49
    :cond_4
    iget-object v0, p0, Leyr;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 51
    return-void
.end method
