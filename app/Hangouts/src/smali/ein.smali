.class public final Lein;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lein;


# instance fields
.field public b:Leip;

.field public c:Leio;

.field public d:Leio;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lein;

    sput-object v0, Lein;->a:[Lein;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 192
    iput-object v0, p0, Lein;->b:Leip;

    .line 195
    iput-object v0, p0, Lein;->c:Leio;

    .line 198
    iput-object v0, p0, Lein;->d:Leio;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 218
    const/4 v0, 0x0

    .line 219
    iget-object v1, p0, Lein;->b:Leip;

    if-eqz v1, :cond_0

    .line 220
    const/4 v0, 0x1

    iget-object v1, p0, Lein;->b:Leip;

    .line 221
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 223
    :cond_0
    iget-object v1, p0, Lein;->c:Leio;

    if-eqz v1, :cond_1

    .line 224
    const/4 v1, 0x2

    iget-object v2, p0, Lein;->c:Leio;

    .line 225
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 227
    :cond_1
    iget-object v1, p0, Lein;->d:Leio;

    if-eqz v1, :cond_2

    .line 228
    const/4 v1, 0x3

    iget-object v2, p0, Lein;->d:Leio;

    .line 229
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 231
    :cond_2
    iget-object v1, p0, Lein;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 232
    iput v0, p0, Lein;->cachedSize:I

    .line 233
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lein;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lein;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lein;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lein;->b:Leip;

    if-nez v0, :cond_2

    new-instance v0, Leip;

    invoke-direct {v0}, Leip;-><init>()V

    iput-object v0, p0, Lein;->b:Leip;

    :cond_2
    iget-object v0, p0, Lein;->b:Leip;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lein;->c:Leio;

    if-nez v0, :cond_3

    new-instance v0, Leio;

    invoke-direct {v0}, Leio;-><init>()V

    iput-object v0, p0, Lein;->c:Leio;

    :cond_3
    iget-object v0, p0, Lein;->c:Leio;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lein;->d:Leio;

    if-nez v0, :cond_4

    new-instance v0, Leio;

    invoke-direct {v0}, Leio;-><init>()V

    iput-object v0, p0, Lein;->d:Leio;

    :cond_4
    iget-object v0, p0, Lein;->d:Leio;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lein;->b:Leip;

    if-eqz v0, :cond_0

    .line 204
    const/4 v0, 0x1

    iget-object v1, p0, Lein;->b:Leip;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 206
    :cond_0
    iget-object v0, p0, Lein;->c:Leio;

    if-eqz v0, :cond_1

    .line 207
    const/4 v0, 0x2

    iget-object v1, p0, Lein;->c:Leio;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 209
    :cond_1
    iget-object v0, p0, Lein;->d:Leio;

    if-eqz v0, :cond_2

    .line 210
    const/4 v0, 0x3

    iget-object v1, p0, Lein;->d:Leio;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 212
    :cond_2
    iget-object v0, p0, Lein;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 214
    return-void
.end method
