.class public final Ldcv;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldcv;


# instance fields
.field public b:Ljava/lang/Boolean;

.field public c:Leym;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Ldcv;

    sput-object v0, Ldcv;->a:[Ldcv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Ldcv;->c:Leym;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 42
    const/4 v0, 0x0

    .line 43
    iget-object v1, p0, Ldcv;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 44
    const/4 v0, 0x2

    iget-object v1, p0, Ldcv;->b:Ljava/lang/Boolean;

    .line 45
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 47
    :cond_0
    iget-object v1, p0, Ldcv;->c:Leym;

    if-eqz v1, :cond_1

    .line 48
    const/4 v1, 0x3

    iget-object v2, p0, Ldcv;->c:Leym;

    .line 49
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51
    :cond_1
    iget-object v1, p0, Ldcv;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 52
    const/4 v1, 0x4

    iget-object v2, p0, Ldcv;->d:Ljava/lang/String;

    .line 53
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 55
    :cond_2
    iget-object v1, p0, Ldcv;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 56
    const/4 v1, 0x5

    iget-object v2, p0, Ldcv;->e:Ljava/lang/String;

    .line 57
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59
    :cond_3
    iget-object v1, p0, Ldcv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60
    iput v0, p0, Ldcv;->cachedSize:I

    .line 61
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldcv;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldcv;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldcv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldcv;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldcv;->c:Leym;

    if-nez v0, :cond_2

    new-instance v0, Leym;

    invoke-direct {v0}, Leym;-><init>()V

    iput-object v0, p0, Ldcv;->c:Leym;

    :cond_2
    iget-object v0, p0, Ldcv;->c:Leym;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldcv;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldcv;->e:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Ldcv;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 25
    const/4 v0, 0x2

    iget-object v1, p0, Ldcv;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 27
    :cond_0
    iget-object v0, p0, Ldcv;->c:Leym;

    if-eqz v0, :cond_1

    .line 28
    const/4 v0, 0x3

    iget-object v1, p0, Ldcv;->c:Leym;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 30
    :cond_1
    iget-object v0, p0, Ldcv;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 31
    const/4 v0, 0x4

    iget-object v1, p0, Ldcv;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 33
    :cond_2
    iget-object v0, p0, Ldcv;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 34
    const/4 v0, 0x5

    iget-object v1, p0, Ldcv;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 36
    :cond_3
    iget-object v0, p0, Ldcv;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 38
    return-void
.end method
