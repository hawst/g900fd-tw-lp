.class public final Lajc;
.super Lbai;
.source "PG"

# interfaces
.implements Lbzl;


# instance fields
.field final synthetic j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

.field private final k:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 723
    iput-object p1, p0, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    .line 724
    invoke-direct {p0, p2}, Lbai;-><init>(Landroid/content/Context;)V

    .line 725
    iput-object p2, p0, Lajc;->k:Landroid/app/Activity;

    .line 727
    invoke-static {}, Lbzh;->b()Lbzh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbzh;->a(Lbzl;)V

    .line 728
    return-void
.end method

.method static synthetic a(Lajc;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 719
    iget-object v0, p0, Lajc;->k:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 792
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lf;->eO:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 794
    return-object v0
.end method

.method public a(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 11

    .prologue
    .line 800
    iget-object v0, p0, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)Lbdh;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 801
    iget-object v0, p0, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)Lbdh;

    move-result-object v0

    move-object v2, v0

    .line 806
    :goto_0
    iget-object v6, v2, Lbdh;->b:Lbdk;

    .line 807
    iget-object v5, v2, Lbdh;->e:Ljava/lang/String;

    .line 808
    iget-object v7, v2, Lbdh;->c:Ljava/lang/String;

    .line 809
    const-string v1, ""

    .line 810
    const/4 v0, 0x0

    .line 811
    sget v3, Lg;->ao:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 812
    sget v3, Lg;->bz:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 813
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 814
    if-eqz v7, :cond_4

    .line 815
    invoke-static {v7}, Lbzd;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 817
    invoke-static {}, Lbvx;->f()Lbdh;

    move-result-object v0

    iget-object v0, v0, Lbdh;->b:Lbdk;

    invoke-virtual {v6, v0}, Lbdk;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 823
    invoke-static {v3, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    if-nez v0, :cond_3

    .line 824
    iget-object v0, p0, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->no:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 825
    const/4 v0, 0x1

    .line 830
    :goto_1
    const/4 v4, 0x0

    .line 831
    const/16 v10, 0x8

    invoke-virtual {v8, v10}, Landroid/view/View;->setVisibility(I)V

    .line 832
    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    .line 833
    if-eqz v0, :cond_0

    .line 834
    new-instance v0, Lajd;

    invoke-direct {v0, p0, v3}, Lajd;-><init>(Lajc;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    move v3, v4

    .line 867
    :goto_2
    sget v0, Lg;->eH:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 868
    invoke-static {}, Lec;->a()Lec;

    move-result-object v9

    .line 869
    invoke-static {v5}, Lsi;->d(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 870
    sget-object v4, Lel;->a:Lek;

    invoke-virtual {v9, v5, v4}, Lec;->a(Ljava/lang/String;Lek;)Ljava/lang/String;

    move-result-object v4

    .line 872
    :goto_3
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 874
    sget v0, Lg;->ap:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 875
    invoke-static {v1}, Lsi;->d(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 876
    sget-object v4, Lel;->a:Lek;

    invoke-virtual {v9, v1, v4}, Lec;->a(Ljava/lang/String;Lek;)Ljava/lang/String;

    .line 878
    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 880
    sget v1, Lg;->E:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/hangouts/views/AvatarView;

    .line 881
    iget-object v2, v2, Lbdh;->h:Ljava/lang/String;

    iget-object v4, p0, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    invoke-static {v4}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->c(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)Lyj;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Ljava/lang/String;Lyj;)V

    .line 883
    sget v1, Lg;->fw:I

    .line 884
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/hangouts/views/PresenceView;

    .line 885
    if-eqz v7, :cond_8

    const/4 v2, 0x1

    :goto_4
    iget-object v4, p0, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    invoke-static {v4}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->c(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)Lyj;

    move-result-object v4

    invoke-virtual {v1, v6, v2, v4}, Lcom/google/android/apps/hangouts/views/PresenceView;->a(Lbdk;ZLyj;)V

    .line 887
    if-eqz v3, :cond_a

    .line 888
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 889
    invoke-static {}, Lf;->y()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 890
    const/4 v1, 0x0

    const/4 v2, 0x0

    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->aN:I

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 897
    :goto_5
    new-instance v0, Laje;

    invoke-direct {v0, p0, v6}, Laje;-><init>(Lajc;Lbdk;)V

    invoke-virtual {v8, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 934
    :goto_6
    return-void

    .line 803
    :cond_2
    iget-object v0, p0, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    .line 804
    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->c(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)Lyj;

    move-result-object v0

    .line 803
    invoke-static {v0, p2}, Lyc;->a(Lyj;Landroid/database/Cursor;)Lbdh;

    move-result-object v0

    move-object v2, v0

    goto/16 :goto_0

    .line 828
    :cond_3
    const/4 v0, 0x0

    move-object v1, v3

    goto/16 :goto_1

    .line 845
    :cond_4
    if-eqz v6, :cond_c

    .line 846
    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    .line 847
    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Landroid/view/View;->setVisibility(I)V

    .line 848
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 849
    iget-object v0, p0, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->mCircleIdsMapping:Ljava/util/Map;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->mMyCircles:Ljava/util/Map;

    if-eqz v0, :cond_7

    .line 850
    iget-object v0, p0, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->mCircleIdsMapping:Ljava/util/Map;

    iget-object v3, v6, Lbdk;->a:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 851
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_7

    .line 852
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 853
    iget-object v4, p0, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    iget-object v4, v4, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->mMyCircles:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 854
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 855
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_6

    .line 856
    const-string v4, ", "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 858
    :cond_6
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 863
    :cond_7
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 864
    const/4 v0, 0x1

    move v3, v0

    goto/16 :goto_2

    .line 885
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 893
    :cond_9
    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->aN:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto/16 :goto_5

    .line 932
    :cond_a
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_6

    :cond_b
    move-object v4, v5

    goto/16 :goto_3

    :cond_c
    move v3, v0

    goto/16 :goto_2
.end method

.method public a(Ljava/lang/String;Lbdn;)V
    .locals 0

    .prologue
    .line 943
    return-void
.end method

.method public b(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 4

    .prologue
    .line 737
    invoke-super {p0, p1}, Lbai;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 738
    if-eqz p1, :cond_3

    .line 739
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 740
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 742
    :cond_0
    iget-object v2, p0, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    .line 744
    invoke-static {v2}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->c(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)Lyj;

    move-result-object v2

    .line 743
    invoke-static {v2, p1}, Lyc;->a(Lyj;Landroid/database/Cursor;)Lbdh;

    move-result-object v2

    .line 746
    iget-object v3, v2, Lbdh;->b:Lbdk;

    if-eqz v3, :cond_1

    .line 747
    iget-object v2, v2, Lbdh;->b:Lbdk;

    iget-object v2, v2, Lbdk;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 749
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 752
    :cond_2
    invoke-static {}, Lbzh;->b()Lbzh;

    move-result-object v2

    iget-object v3, p0, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->c(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)Lyj;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lbzh;->a(Ljava/util/List;Lyj;)V

    .line 754
    :cond_3
    return-object v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 758
    invoke-static {}, Lbzh;->b()Lbzh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbzh;->b(Lbzl;)V

    .line 759
    return-void
.end method

.method public f_()V
    .locals 0

    .prologue
    .line 938
    invoke-virtual {p0}, Lajc;->notifyDataSetChanged()V

    .line 939
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 764
    invoke-super {p0}, Lbai;->getCount()I

    move-result v0

    .line 765
    iget-object v1, p0, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)Lbdh;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 766
    if-gtz v0, :cond_1

    .line 768
    :cond_0
    :goto_0
    return v0

    .line 766
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 774
    if-eqz p2, :cond_0

    .line 775
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lg;->aU:I

    if-eq v0, v1, :cond_2

    .line 778
    :cond_0
    iget-object v0, p0, Lajc;->k:Landroid/app/Activity;

    iget-object v1, p0, Lajc;->c:Landroid/database/Cursor;

    invoke-virtual {p0, v0, v1, p3}, Lajc;->a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 779
    iget-object v1, p0, Lajc;->c:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 780
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "couldn\'t move cursor to position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 783
    :cond_1
    iget-object v1, p0, Lajc;->k:Landroid/app/Activity;

    iget-object v1, p0, Lajc;->c:Landroid/database/Cursor;

    invoke-virtual {p0, v0, v1}, Lajc;->a(Landroid/view/View;Landroid/database/Cursor;)V

    .line 787
    :goto_0
    return-object v0

    .line 785
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lbai;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 732
    const/4 v0, 0x0

    return v0
.end method
