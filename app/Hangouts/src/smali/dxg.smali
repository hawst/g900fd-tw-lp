.class public final Ldxg;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldxg;


# instance fields
.field public b:Ldvn;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Long;

.field public e:[Ldql;

.field public f:Ljava/lang/Boolean;

.field public g:Ldtc;

.field public h:[Ldro;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10241
    const/4 v0, 0x0

    new-array v0, v0, [Ldxg;

    sput-object v0, Ldxg;->a:[Ldxg;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10242
    invoke-direct {p0}, Lepn;-><init>()V

    .line 10245
    iput-object v1, p0, Ldxg;->b:Ldvn;

    .line 10252
    sget-object v0, Ldql;->a:[Ldql;

    iput-object v0, p0, Ldxg;->e:[Ldql;

    .line 10257
    iput-object v1, p0, Ldxg;->g:Ldtc;

    .line 10260
    sget-object v0, Ldro;->a:[Ldro;

    iput-object v0, p0, Ldxg;->h:[Ldro;

    .line 10242
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 10301
    iget-object v0, p0, Ldxg;->b:Ldvn;

    if-eqz v0, :cond_8

    .line 10302
    const/4 v0, 0x1

    iget-object v2, p0, Ldxg;->b:Ldvn;

    .line 10303
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 10305
    :goto_0
    iget-object v2, p0, Ldxg;->d:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 10306
    const/4 v2, 0x2

    iget-object v3, p0, Ldxg;->d:Ljava/lang/Long;

    .line 10307
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 10309
    :cond_0
    iget-object v2, p0, Ldxg;->e:[Ldql;

    if-eqz v2, :cond_2

    .line 10310
    iget-object v3, p0, Ldxg;->e:[Ldql;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 10311
    if-eqz v5, :cond_1

    .line 10312
    const/4 v6, 0x3

    .line 10313
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 10310
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 10317
    :cond_2
    iget-object v2, p0, Ldxg;->f:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    .line 10318
    const/4 v2, 0x4

    iget-object v3, p0, Ldxg;->f:Ljava/lang/Boolean;

    .line 10319
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 10321
    :cond_3
    iget-object v2, p0, Ldxg;->g:Ldtc;

    if-eqz v2, :cond_4

    .line 10322
    const/4 v2, 0x5

    iget-object v3, p0, Ldxg;->g:Ldtc;

    .line 10323
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 10325
    :cond_4
    iget-object v2, p0, Ldxg;->c:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    .line 10326
    const/4 v2, 0x6

    iget-object v3, p0, Ldxg;->c:Ljava/lang/Boolean;

    .line 10327
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 10329
    :cond_5
    iget-object v2, p0, Ldxg;->h:[Ldro;

    if-eqz v2, :cond_7

    .line 10330
    iget-object v2, p0, Ldxg;->h:[Ldro;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 10331
    if-eqz v4, :cond_6

    .line 10332
    const/4 v5, 0x7

    .line 10333
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 10330
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 10337
    :cond_7
    iget-object v1, p0, Ldxg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10338
    iput v0, p0, Ldxg;->cachedSize:I

    .line 10339
    return v0

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 10238
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldxg;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldxg;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldxg;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldxg;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldxg;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldxg;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldxg;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldxg;->e:[Ldql;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldql;

    iget-object v3, p0, Ldxg;->e:[Ldql;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldxg;->e:[Ldql;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldxg;->e:[Ldql;

    :goto_2
    iget-object v2, p0, Ldxg;->e:[Ldql;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldxg;->e:[Ldql;

    new-instance v3, Ldql;

    invoke-direct {v3}, Ldql;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldxg;->e:[Ldql;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldxg;->e:[Ldql;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldxg;->e:[Ldql;

    new-instance v3, Ldql;

    invoke-direct {v3}, Ldql;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldxg;->e:[Ldql;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldxg;->f:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Ldxg;->g:Ldtc;

    if-nez v0, :cond_6

    new-instance v0, Ldtc;

    invoke-direct {v0}, Ldtc;-><init>()V

    iput-object v0, p0, Ldxg;->g:Ldtc;

    :cond_6
    iget-object v0, p0, Ldxg;->g:Ldtc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldxg;->c:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldxg;->h:[Ldro;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldro;

    iget-object v3, p0, Ldxg;->h:[Ldro;

    if-eqz v3, :cond_7

    iget-object v3, p0, Ldxg;->h:[Ldro;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Ldxg;->h:[Ldro;

    :goto_4
    iget-object v2, p0, Ldxg;->h:[Ldro;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Ldxg;->h:[Ldro;

    new-instance v3, Ldro;

    invoke-direct {v3}, Ldro;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldxg;->h:[Ldro;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    iget-object v0, p0, Ldxg;->h:[Ldro;

    array-length v0, v0

    goto :goto_3

    :cond_9
    iget-object v2, p0, Ldxg;->h:[Ldro;

    new-instance v3, Ldro;

    invoke-direct {v3}, Ldro;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldxg;->h:[Ldro;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 10265
    iget-object v1, p0, Ldxg;->b:Ldvn;

    if-eqz v1, :cond_0

    .line 10266
    const/4 v1, 0x1

    iget-object v2, p0, Ldxg;->b:Ldvn;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 10268
    :cond_0
    iget-object v1, p0, Ldxg;->d:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 10269
    const/4 v1, 0x2

    iget-object v2, p0, Ldxg;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->a(IJ)V

    .line 10271
    :cond_1
    iget-object v1, p0, Ldxg;->e:[Ldql;

    if-eqz v1, :cond_3

    .line 10272
    iget-object v2, p0, Ldxg;->e:[Ldql;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 10273
    if-eqz v4, :cond_2

    .line 10274
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 10272
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 10278
    :cond_3
    iget-object v1, p0, Ldxg;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 10279
    const/4 v1, 0x4

    iget-object v2, p0, Ldxg;->f:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 10281
    :cond_4
    iget-object v1, p0, Ldxg;->g:Ldtc;

    if-eqz v1, :cond_5

    .line 10282
    const/4 v1, 0x5

    iget-object v2, p0, Ldxg;->g:Ldtc;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 10284
    :cond_5
    iget-object v1, p0, Ldxg;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 10285
    const/4 v1, 0x6

    iget-object v2, p0, Ldxg;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 10287
    :cond_6
    iget-object v1, p0, Ldxg;->h:[Ldro;

    if-eqz v1, :cond_8

    .line 10288
    iget-object v1, p0, Ldxg;->h:[Ldro;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    .line 10289
    if-eqz v3, :cond_7

    .line 10290
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 10288
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 10294
    :cond_8
    iget-object v0, p0, Ldxg;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 10296
    return-void
.end method
