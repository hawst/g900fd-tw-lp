.class public final Lbiz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/Long;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;ILjava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 617
    iput p1, p0, Lbiz;->a:I

    .line 618
    iput-object p2, p0, Lbiz;->b:Ljava/lang/String;

    .line 619
    iput p3, p0, Lbiz;->c:I

    .line 620
    iput-object p4, p0, Lbiz;->d:Ljava/lang/String;

    .line 621
    iput-object v0, p0, Lbiz;->e:Ljava/lang/Long;

    .line 622
    iput-object v0, p0, Lbiz;->f:Ljava/lang/String;

    .line 623
    iput-object v0, p0, Lbiz;->g:Ljava/lang/String;

    .line 624
    iput-object v0, p0, Lbiz;->h:Ljava/lang/String;

    .line 625
    return-void
.end method

.method public constructor <init>(Lesg;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 572
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 573
    iget-object v0, p1, Lesg;->b:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    iput v0, p0, Lbiz;->a:I

    .line 575
    iget-object v0, p1, Lesg;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lesg;->c:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lbiz;->b:Ljava/lang/String;

    .line 577
    iget-object v0, p1, Lesg;->d:Lesd;

    if-eqz v0, :cond_8

    .line 578
    iget-object v0, p1, Lesg;->d:Lesd;

    iget-object v0, v0, Lesd;->b:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 579
    const/4 v0, 0x1

    .line 581
    :goto_1
    iget-object v2, p1, Lesg;->d:Lesd;

    iget-object v2, v2, Lesd;->c:Ljava/lang/Boolean;

    invoke-static {v2, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 582
    or-int/lit8 v0, v0, 0x2

    .line 584
    :cond_0
    iget-object v2, p1, Lesg;->d:Lesd;

    iget-object v2, v2, Lesd;->d:Ljava/lang/Boolean;

    invoke-static {v2, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 585
    or-int/lit8 v0, v0, 0x4

    .line 587
    :cond_1
    iget-object v2, p1, Lesg;->d:Lesd;

    iget-object v2, v2, Lesd;->e:Ljava/lang/Boolean;

    invoke-static {v2, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 588
    or-int/lit8 v0, v0, 0x8

    .line 591
    :cond_2
    :goto_2
    iput v0, p0, Lbiz;->c:I

    .line 592
    iget-object v0, p1, Lesg;->e:Lesf;

    if-eqz v0, :cond_4

    .line 593
    iget-object v0, p1, Lesg;->e:Lesf;

    iget-object v0, v0, Lesf;->b:Ljava/lang/String;

    iput-object v0, p0, Lbiz;->d:Ljava/lang/String;

    .line 597
    :goto_3
    iget-object v0, p1, Lesg;->f:Lesi;

    if-eqz v0, :cond_5

    .line 598
    iget-object v0, p1, Lesg;->f:Lesi;

    iget-object v0, v0, Lesi;->b:Ljava/lang/Long;

    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lbiz;->e:Ljava/lang/Long;

    .line 600
    iget-object v0, p1, Lesg;->f:Lesi;

    iget-object v0, v0, Lesi;->c:Ljava/lang/String;

    iput-object v0, p0, Lbiz;->f:Ljava/lang/String;

    .line 601
    iget-object v0, p1, Lesg;->f:Lesi;

    iget-object v0, v0, Lesi;->d:Ljava/lang/String;

    iput-object v0, p0, Lbiz;->g:Ljava/lang/String;

    .line 607
    :goto_4
    iget-object v0, p1, Lesg;->g:Lese;

    if-eqz v0, :cond_6

    .line 608
    iget-object v0, p1, Lesg;->g:Lese;

    iget-object v0, v0, Lese;->b:Ljava/lang/String;

    iput-object v0, p0, Lbiz;->h:Ljava/lang/String;

    .line 612
    :goto_5
    return-void

    .line 575
    :cond_3
    const-string v0, ""

    goto :goto_0

    .line 595
    :cond_4
    iput-object v3, p0, Lbiz;->d:Ljava/lang/String;

    goto :goto_3

    .line 603
    :cond_5
    iput-object v3, p0, Lbiz;->e:Ljava/lang/Long;

    .line 604
    iput-object v3, p0, Lbiz;->f:Ljava/lang/String;

    .line 605
    iput-object v3, p0, Lbiz;->g:Ljava/lang/String;

    goto :goto_4

    .line 610
    :cond_6
    iput-object v3, p0, Lbiz;->h:Ljava/lang/String;

    goto :goto_5

    :cond_7
    move v0, v1

    goto :goto_1

    :cond_8
    move v0, v1

    goto :goto_2
.end method
