.class public final Lbcl;
.super Lbea;
.source "PG"


# static fields
.field private static final a:Z

.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lbys;->h:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbcl;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lbea;-><init>()V

    .line 50
    iput-object p1, p0, Lbcl;->b:Ljava/lang/String;

    .line 51
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lbpp;Lbpk;)Lbfz;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 125
    sget-boolean v0, Lbcl;->a:Z

    if-eqz v0, :cond_0

    .line 126
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[SEND] "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lbpp;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    :cond_0
    invoke-virtual {p0}, Lbcl;->B_()Lcom/google/api/client/http/GenericUrl;

    move-result-object v0

    invoke-virtual {p0}, Lbcl;->s()V

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lbcl;->b:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p2, Lbpp;->c:Ljava/lang/String;

    new-instance v4, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v4}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    new-instance v5, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v0}, Lcom/google/api/client/http/GenericUrl;->toURI()Ljava/net/URI;

    move-result-object v0

    invoke-direct {v5, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/net/URI;)V

    :try_start_0
    new-instance v0, Leyi;

    invoke-direct {v0}, Leyi;-><init>()V

    const-string v6, "prod"

    new-instance v7, Leyl;

    const-string v8, "Google_Hangouts_Android"

    invoke-direct {v7, v8}, Leyl;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6, v7}, Leyi;->a(Ljava/lang/String;Leyj;)V

    const-string v6, "ver"

    new-instance v7, Leyl;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lbcl;->g:Lbci;

    iget-object v9, v9, Lbci;->a:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "-calls"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Leyl;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6, v7}, Leyi;->a(Ljava/lang/String;Leyj;)V

    const-string v6, "email"

    new-instance v7, Leyl;

    invoke-direct {v7, v3}, Leyl;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6, v7}, Leyi;->a(Ljava/lang/String;Leyj;)V

    const-string v3, "type"

    new-instance v6, Leyl;

    const-string v7, "log"

    invoke-direct {v6, v7}, Leyl;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3, v6}, Leyi;->a(Ljava/lang/String;Leyj;)V

    const-string v3, "log"

    new-instance v6, Leyk;

    invoke-direct {v6, v2}, Leyk;-><init>(Ljava/io/File;)V

    invoke-virtual {v0, v3, v6}, Leyi;->a(Ljava/lang/String;Leyj;)V

    invoke-virtual {v5, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    invoke-virtual {v4, v5}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    const/16 v3, 0xc8

    if-eq v0, v3, :cond_2

    :cond_1
    const-string v0, "Babel"

    const-string v1, "Crash log upload unsuccessful."

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lbph;

    const/16 v1, 0x6c

    invoke-direct {v0, v1}, Lbph;-><init>(I)V

    throw v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Crash log upload failed due to ClientProtocolException "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lbph;

    const/16 v2, 0x72

    invoke-direct {v1, v2, v0}, Lbph;-><init>(ILjava/lang/Exception;)V

    throw v1

    :catch_2
    move-exception v0

    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Crash log upload failed due to IOException "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lbph;

    const/16 v2, 0x66

    invoke-direct {v1, v2, v0}, Lbph;-><init>(ILjava/lang/Exception;)V

    throw v1

    :cond_2
    const-string v0, "Babel"

    const-string v3, "Crash log successfully uploaded."

    invoke-static {v0, v3}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 129
    :goto_1
    return-object v1

    .line 128
    :cond_3
    const-string v0, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Crash no such log file "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;I)Lepr;
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://clients2.google.com"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbcl;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lyj;Lbph;)V
    .locals 2

    .prologue
    .line 145
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lbcl;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 146
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 148
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 150
    :cond_0
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    const-string v0, "/cr/report"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    const-string v0, "background_queue"

    return-object v0
.end method

.method public d()J
    .locals 3

    .prologue
    .line 140
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x18

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method
