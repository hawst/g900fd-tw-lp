.class public final Lakx;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)V
    .locals 1

    .prologue
    .line 411
    iput-object p1, p0, Lakx;->b:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 412
    iget-object v0, p0, Lakx;->b:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->d(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lakx;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 411
    new-instance v0, Lyt;

    iget-object v1, p0, Lakx;->b:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->f(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Lyj;

    move-result-object v1

    invoke-direct {v0, v1}, Lyt;-><init>(Lyj;)V

    iget-object v1, p0, Lakx;->b:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->d(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lyt;->D(Ljava/lang/String;)V

    iget-object v1, p0, Lakx;->b:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->d(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lyt;->h(Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 411
    check-cast p1, Ljava/lang/Void;

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object v0, p0, Lakx;->b:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->e(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lakx;->b:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->d(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lakx;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lahc;

    iget-object v1, p0, Lakx;->b:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->d(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lakx;->b:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->f(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Lyj;

    move-result-object v2

    iget-object v3, p0, Lakx;->b:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->g(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lahc;-><init>(Ljava/lang/String;Lyj;I)V

    iget-object v1, p0, Lakx;->b:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->e(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lahc;)V

    :cond_0
    return-void
.end method
