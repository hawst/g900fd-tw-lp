.class public Lbip;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method private constructor <init>(Ldvc;)V
    .locals 3

    .prologue
    .line 2226
    iget-object v0, p1, Ldvc;->b:Ldvn;

    const-wide/16 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 2227
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2228
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UploadAnalyticsResponse debugUrl: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Ldvc;->b:Ldvn;

    iget-object v2, v2, Ldvn;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2231
    :cond_0
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 2235
    new-instance v0, Ldvc;

    invoke-direct {v0}, Ldvc;-><init>()V

    .line 2236
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldvc;

    .line 2237
    iget-object v1, v0, Ldvc;->b:Ldvn;

    invoke-static {v1}, Lbip;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2238
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldvc;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 2240
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbip;

    invoke-direct {v1, v0}, Lbip;-><init>(Ldvc;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 2247
    invoke-super {p0, p1, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 2248
    const-string v0, "Babel"

    invoke-static {v0, v3}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2249
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "processEventResponse response status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbip;->c:Lbht;

    iget v2, v2, Lbht;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " error description"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbip;->c:Lbht;

    iget-object v2, v2, Lbht;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2254
    :cond_0
    iget-object v0, p0, Lbip;->b:Lbea;

    check-cast v0, Lbfw;

    .line 2255
    iget-object v1, v0, Lbfw;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2256
    iget-object v0, v0, Lbfw;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lyt;->y(Ljava/lang/String;)V

    .line 2259
    :cond_1
    invoke-virtual {p1}, Lyt;->f()Lyj;

    move-result-object v0

    .line 2260
    invoke-static {v0}, Lbqw;->a(Lyj;)Lbqw;

    move-result-object v0

    .line 2261
    invoke-virtual {v0, v3}, Lbqw;->a(I)V

    .line 2262
    return-void
.end method
