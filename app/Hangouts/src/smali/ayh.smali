.class public final Layh;
.super Layf;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 22
    sget v0, Lf;->em:I

    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    sget v3, Lg;->W:I

    aput v3, v1, v2

    const/4 v2, 0x1

    sget v3, Lg;->U:I

    aput v3, v1, v2

    invoke-direct {p0, v0, v1}, Layf;-><init>(I[I)V

    .line 24
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x3

    return v0
.end method

.method protected a(I)V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Layh;->b:Lyj;

    if-nez v0, :cond_0

    .line 66
    const-string v0, "Babel"

    const-string v1, "[BusinessFeaturesPromo.setBusinessPromo] Account changed?"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :goto_0
    invoke-super {p0, p1}, Layf;->a(I)V

    .line 73
    return-void

    .line 68
    :cond_0
    iget-object v1, p0, Layh;->b:Lyj;

    sget v0, Lg;->U:I

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v1, v0}, Lym;->d(Lyj;Z)V

    .line 70
    iget-object v0, p0, Layh;->b:Lyj;

    invoke-static {v0}, Lym;->h(Lyj;)V

    goto :goto_0

    .line 68
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Lyj;Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 34
    const-string v0, "babel_enable_call_me_maybe"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 47
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lyj;->O()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lyj;->S()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    invoke-virtual {p1}, Lyj;->U()Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    invoke-super {p0, p1, p2}, Layf;->a(Lyj;Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public b()Landroid/view/View;
    .locals 8

    .prologue
    .line 51
    invoke-super {p0}, Layf;->b()Landroid/view/View;

    move-result-object v1

    .line 53
    iget-object v0, p0, Layh;->b:Lyj;

    invoke-virtual {v0}, Lyj;->e()Ljava/lang/String;

    move-result-object v2

    .line 54
    sget v0, Lg;->V:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 55
    const-string v3, "https://support.google.com/business/?hl=%locale%"

    const-string v4, "hangouts_business"

    invoke-static {v3, v4}, Lf;->b(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 56
    iget-object v4, p0, Layh;->c:Landroid/app/Activity;

    sget v5, Lh;->R:I

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    const/4 v2, 0x1

    .line 57
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    .line 56
    invoke-virtual {v4, v5, v6}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 58
    iget-object v3, p0, Layh;->c:Landroid/app/Activity;

    invoke-static {v0, v3, v2}, Lf;->a(Landroid/widget/TextView;Landroid/app/Activity;Ljava/lang/String;)V

    .line 60
    return-object v1
.end method
