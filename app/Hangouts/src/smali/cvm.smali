.class public final Lcvm;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lcvm;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private e:Z

.field private f:J

.field private g:Ljava/lang/String;

.field private h:I

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcvm;

    invoke-direct {v0}, Lcvm;-><init>()V

    sput-object v0, Lcvm;->a:Lcvm;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x1fffff

    iput v0, p0, Lcvm;->d:I

    const/4 v0, 0x7

    iput v0, p0, Lcvm;->h:I

    const/4 v0, 0x0

    iput v0, p0, Lcvm;->i:I

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcvm;
    .locals 0

    iput-object p1, p0, Lcvm;->g:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/util/Collection;)Lcvm;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcvm;"
        }
    .end annotation

    iput-object p1, p0, Lcvm;->c:Ljava/util/Collection;

    return-object p0
.end method

.method public a(Z)Lcvm;
    .locals 0

    iput-boolean p1, p0, Lcvm;->e:Z

    return-object p0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcvm;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcvm;->c:Ljava/util/Collection;

    return-object v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcvm;->d:I

    return v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 3

    new-instance v0, Lcvm;

    invoke-direct {v0}, Lcvm;-><init>()V

    iget-object v1, p0, Lcvm;->b:Ljava/lang/String;

    iput-object v1, v0, Lcvm;->b:Ljava/lang/String;

    iget-object v1, p0, Lcvm;->c:Ljava/util/Collection;

    invoke-virtual {v0, v1}, Lcvm;->a(Ljava/util/Collection;)Lcvm;

    move-result-object v0

    iget v1, p0, Lcvm;->d:I

    iput v1, v0, Lcvm;->d:I

    iget-boolean v1, p0, Lcvm;->e:Z

    invoke-virtual {v0, v1}, Lcvm;->a(Z)Lcvm;

    move-result-object v0

    iget-wide v1, p0, Lcvm;->f:J

    iput-wide v1, v0, Lcvm;->f:J

    iget-object v1, p0, Lcvm;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcvm;->a(Ljava/lang/String;)Lcvm;

    move-result-object v0

    iget v1, p0, Lcvm;->h:I

    iput v1, v0, Lcvm;->h:I

    iget v1, p0, Lcvm;->i:I

    iput v1, v0, Lcvm;->i:I

    return-object v0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lcvm;->e:Z

    return v0
.end method

.method public e()J
    .locals 2

    iget-wide v0, p0, Lcvm;->f:J

    return-wide v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcvm;->g:Ljava/lang/String;

    return-object v0
.end method

.method public g()I
    .locals 1

    iget v0, p0, Lcvm;->h:I

    return v0
.end method

.method public h()I
    .locals 1

    iget v0, p0, Lcvm;->i:I

    return v0
.end method
