.class public final Ldxm;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldxm;


# instance fields
.field public b:Ldvm;

.field public c:Ldqf;

.field public d:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21213
    const/4 v0, 0x0

    new-array v0, v0, [Ldxm;

    sput-object v0, Ldxm;->a:[Ldxm;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21214
    invoke-direct {p0}, Lepn;-><init>()V

    .line 21217
    iput-object v0, p0, Ldxm;->b:Ldvm;

    .line 21220
    iput-object v0, p0, Ldxm;->c:Ldqf;

    .line 21214
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 21242
    const/4 v0, 0x0

    .line 21243
    iget-object v1, p0, Ldxm;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 21244
    const/4 v0, 0x1

    iget-object v1, p0, Ldxm;->b:Ldvm;

    .line 21245
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 21247
    :cond_0
    iget-object v1, p0, Ldxm;->c:Ldqf;

    if-eqz v1, :cond_1

    .line 21248
    const/4 v1, 0x2

    iget-object v2, p0, Ldxm;->c:Ldqf;

    .line 21249
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21251
    :cond_1
    iget-object v1, p0, Ldxm;->d:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 21252
    const/4 v1, 0x3

    iget-object v2, p0, Ldxm;->d:Ljava/lang/Long;

    .line 21253
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 21255
    :cond_2
    iget-object v1, p0, Ldxm;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21256
    iput v0, p0, Ldxm;->cachedSize:I

    .line 21257
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 21210
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldxm;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldxm;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldxm;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldxm;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldxm;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldxm;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldxm;->c:Ldqf;

    if-nez v0, :cond_3

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldxm;->c:Ldqf;

    :cond_3
    iget-object v0, p0, Ldxm;->c:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldxm;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 21227
    iget-object v0, p0, Ldxm;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 21228
    const/4 v0, 0x1

    iget-object v1, p0, Ldxm;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 21230
    :cond_0
    iget-object v0, p0, Ldxm;->c:Ldqf;

    if-eqz v0, :cond_1

    .line 21231
    const/4 v0, 0x2

    iget-object v1, p0, Ldxm;->c:Ldqf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 21233
    :cond_1
    iget-object v0, p0, Ldxm;->d:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 21234
    const/4 v0, 0x3

    iget-object v1, p0, Ldxm;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 21236
    :cond_2
    iget-object v0, p0, Ldxm;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 21238
    return-void
.end method
