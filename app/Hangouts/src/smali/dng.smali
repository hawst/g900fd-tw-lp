.class public final Ldng;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldng;


# instance fields
.field public b:Ldnc;

.field public c:[Ldnc;

.field public d:Ldnd;

.field public e:[Ldnr;

.field public f:Ldni;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2787
    const/4 v0, 0x0

    new-array v0, v0, [Ldng;

    sput-object v0, Ldng;->a:[Ldng;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2788
    invoke-direct {p0}, Lepn;-><init>()V

    .line 2791
    iput-object v1, p0, Ldng;->b:Ldnc;

    .line 2794
    sget-object v0, Ldnc;->a:[Ldnc;

    iput-object v0, p0, Ldng;->c:[Ldnc;

    .line 2797
    iput-object v1, p0, Ldng;->d:Ldnd;

    .line 2800
    sget-object v0, Ldnr;->a:[Ldnr;

    iput-object v0, p0, Ldng;->e:[Ldnr;

    .line 2803
    iput-object v1, p0, Ldng;->f:Ldni;

    .line 2788
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2838
    iget-object v0, p0, Ldng;->b:Ldnc;

    if-eqz v0, :cond_6

    .line 2839
    const/4 v0, 0x1

    iget-object v2, p0, Ldng;->b:Ldnc;

    .line 2840
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2842
    :goto_0
    iget-object v2, p0, Ldng;->c:[Ldnc;

    if-eqz v2, :cond_1

    .line 2843
    iget-object v3, p0, Ldng;->c:[Ldnc;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 2844
    if-eqz v5, :cond_0

    .line 2845
    const/4 v6, 0x2

    .line 2846
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 2843
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2850
    :cond_1
    iget-object v2, p0, Ldng;->d:Ldnd;

    if-eqz v2, :cond_2

    .line 2851
    const/4 v2, 0x3

    iget-object v3, p0, Ldng;->d:Ldnd;

    .line 2852
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2854
    :cond_2
    iget-object v2, p0, Ldng;->e:[Ldnr;

    if-eqz v2, :cond_4

    .line 2855
    iget-object v2, p0, Ldng;->e:[Ldnr;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 2856
    if-eqz v4, :cond_3

    .line 2857
    const/4 v5, 0x4

    .line 2858
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2855
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2862
    :cond_4
    iget-object v1, p0, Ldng;->f:Ldni;

    if-eqz v1, :cond_5

    .line 2863
    const/4 v1, 0x5

    iget-object v2, p0, Ldng;->f:Ldni;

    .line 2864
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2866
    :cond_5
    iget-object v1, p0, Ldng;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2867
    iput v0, p0, Ldng;->cachedSize:I

    .line 2868
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2784
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldng;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldng;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldng;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldng;->b:Ldnc;

    if-nez v0, :cond_2

    new-instance v0, Ldnc;

    invoke-direct {v0}, Ldnc;-><init>()V

    iput-object v0, p0, Ldng;->b:Ldnc;

    :cond_2
    iget-object v0, p0, Ldng;->b:Ldnc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldng;->c:[Ldnc;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldnc;

    iget-object v3, p0, Ldng;->c:[Ldnc;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldng;->c:[Ldnc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldng;->c:[Ldnc;

    :goto_2
    iget-object v2, p0, Ldng;->c:[Ldnc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldng;->c:[Ldnc;

    new-instance v3, Ldnc;

    invoke-direct {v3}, Ldnc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldng;->c:[Ldnc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldng;->c:[Ldnc;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldng;->c:[Ldnc;

    new-instance v3, Ldnc;

    invoke-direct {v3}, Ldnc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldng;->c:[Ldnc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldng;->d:Ldnd;

    if-nez v0, :cond_6

    new-instance v0, Ldnd;

    invoke-direct {v0}, Ldnd;-><init>()V

    iput-object v0, p0, Ldng;->d:Ldnd;

    :cond_6
    iget-object v0, p0, Ldng;->d:Ldnd;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldng;->e:[Ldnr;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldnr;

    iget-object v3, p0, Ldng;->e:[Ldnr;

    if-eqz v3, :cond_7

    iget-object v3, p0, Ldng;->e:[Ldnr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Ldng;->e:[Ldnr;

    :goto_4
    iget-object v2, p0, Ldng;->e:[Ldnr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Ldng;->e:[Ldnr;

    new-instance v3, Ldnr;

    invoke-direct {v3}, Ldnr;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldng;->e:[Ldnr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    iget-object v0, p0, Ldng;->e:[Ldnr;

    array-length v0, v0

    goto :goto_3

    :cond_9
    iget-object v2, p0, Ldng;->e:[Ldnr;

    new-instance v3, Ldnr;

    invoke-direct {v3}, Ldnr;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldng;->e:[Ldnr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Ldng;->f:Ldni;

    if-nez v0, :cond_a

    new-instance v0, Ldni;

    invoke-direct {v0}, Ldni;-><init>()V

    iput-object v0, p0, Ldng;->f:Ldni;

    :cond_a
    iget-object v0, p0, Ldng;->f:Ldni;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2808
    iget-object v1, p0, Ldng;->b:Ldnc;

    if-eqz v1, :cond_0

    .line 2809
    const/4 v1, 0x1

    iget-object v2, p0, Ldng;->b:Ldnc;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 2811
    :cond_0
    iget-object v1, p0, Ldng;->c:[Ldnc;

    if-eqz v1, :cond_2

    .line 2812
    iget-object v2, p0, Ldng;->c:[Ldnc;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 2813
    if-eqz v4, :cond_1

    .line 2814
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 2812
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2818
    :cond_2
    iget-object v1, p0, Ldng;->d:Ldnd;

    if-eqz v1, :cond_3

    .line 2819
    const/4 v1, 0x3

    iget-object v2, p0, Ldng;->d:Ldnd;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 2821
    :cond_3
    iget-object v1, p0, Ldng;->e:[Ldnr;

    if-eqz v1, :cond_5

    .line 2822
    iget-object v1, p0, Ldng;->e:[Ldnr;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 2823
    if-eqz v3, :cond_4

    .line 2824
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 2822
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2828
    :cond_5
    iget-object v0, p0, Ldng;->f:Ldni;

    if-eqz v0, :cond_6

    .line 2829
    const/4 v0, 0x5

    iget-object v1, p0, Ldng;->f:Ldni;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 2831
    :cond_6
    iget-object v0, p0, Ldng;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 2833
    return-void
.end method
