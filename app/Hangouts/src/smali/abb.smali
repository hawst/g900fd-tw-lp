.class final Labb;
.super Landroid/widget/ArrayAdapter;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Laaz;

.field private b:Z

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(Laaz;Landroid/content/Context;Ljava/util/List;ZII)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;ZII)V"
        }
    .end annotation

    .prologue
    .line 198
    iput-object p1, p0, Labb;->a:Laaz;

    .line 199
    sget v0, Lf;->dW:I

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 200
    iput-boolean p4, p0, Labb;->b:Z

    .line 201
    iput p5, p0, Labb;->c:I

    .line 202
    iput p6, p0, Labb;->d:I

    .line 203
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 207
    invoke-static {}, Laay;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    const-string v0, "Babel"

    const-string v1, "AccountListAdapter.getView()"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    :cond_0
    if-eqz p2, :cond_1

    instance-of v0, p2, Lcom/google/android/apps/hangouts/views/AccountListItemView;

    if-nez v0, :cond_2

    .line 212
    :cond_1
    invoke-virtual {p0}, Labb;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 213
    sget v1, Lf;->dW:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/AccountListItemView;

    move-object p2, v0

    .line 219
    :goto_0
    invoke-virtual {p0, p1}, Labb;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 220
    const-string v1, "*"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 221
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Labb;->c:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/google/android/apps/hangouts/views/AccountListItemView;->a(Ljava/lang/String;)V

    .line 229
    :goto_1
    new-instance v1, Labc;

    invoke-direct {v1, p0, v0}, Labc;-><init>(Labb;Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Lcom/google/android/apps/hangouts/views/AccountListItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 237
    return-object p2

    .line 216
    :cond_2
    check-cast p2, Lcom/google/android/apps/hangouts/views/AccountListItemView;

    goto :goto_0

    .line 222
    :cond_3
    const-string v1, "--None--"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 223
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Labb;->d:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/google/android/apps/hangouts/views/AccountListItemView;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 225
    :cond_4
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v1

    .line 226
    iget-boolean v2, p0, Labb;->b:Z

    invoke-virtual {p2, v1, v2}, Lcom/google/android/apps/hangouts/views/AccountListItemView;->a(Lyj;Z)V

    goto :goto_1
.end method
