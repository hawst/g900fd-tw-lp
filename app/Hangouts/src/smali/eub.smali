.class public final Leub;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leub;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Levt;

.field public d:[Leuc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Leub;

    sput-object v0, Leub;->a:[Leub;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 215
    const/4 v0, 0x0

    iput-object v0, p0, Leub;->c:Levt;

    .line 218
    sget-object v0, Leuc;->a:[Leuc;

    iput-object v0, p0, Leub;->d:[Leuc;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 243
    iget-object v0, p0, Leub;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 244
    const/4 v0, 0x1

    iget-object v2, p0, Leub;->b:Ljava/lang/String;

    .line 245
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 247
    :goto_0
    iget-object v2, p0, Leub;->c:Levt;

    if-eqz v2, :cond_0

    .line 248
    const/4 v2, 0x2

    iget-object v3, p0, Leub;->c:Levt;

    .line 249
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 251
    :cond_0
    iget-object v2, p0, Leub;->d:[Leuc;

    if-eqz v2, :cond_2

    .line 252
    iget-object v2, p0, Leub;->d:[Leuc;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 253
    if-eqz v4, :cond_1

    .line 254
    const/4 v5, 0x3

    .line 255
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 252
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 259
    :cond_2
    iget-object v1, p0, Leub;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 260
    iput v0, p0, Leub;->cachedSize:I

    .line 261
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Leub;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Leub;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Leub;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leub;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Leub;->c:Levt;

    if-nez v0, :cond_2

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Leub;->c:Levt;

    :cond_2
    iget-object v0, p0, Leub;->c:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leub;->d:[Leuc;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leuc;

    iget-object v3, p0, Leub;->d:[Leuc;

    if-eqz v3, :cond_3

    iget-object v3, p0, Leub;->d:[Leuc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Leub;->d:[Leuc;

    :goto_2
    iget-object v2, p0, Leub;->d:[Leuc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Leub;->d:[Leuc;

    new-instance v3, Leuc;

    invoke-direct {v3}, Leuc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leub;->d:[Leuc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Leub;->d:[Leuc;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Leub;->d:[Leuc;

    new-instance v3, Leuc;

    invoke-direct {v3}, Leuc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leub;->d:[Leuc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 223
    iget-object v0, p0, Leub;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 224
    const/4 v0, 0x1

    iget-object v1, p0, Leub;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 226
    :cond_0
    iget-object v0, p0, Leub;->c:Levt;

    if-eqz v0, :cond_1

    .line 227
    const/4 v0, 0x2

    iget-object v1, p0, Leub;->c:Levt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 229
    :cond_1
    iget-object v0, p0, Leub;->d:[Leuc;

    if-eqz v0, :cond_3

    .line 230
    iget-object v1, p0, Leub;->d:[Leuc;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 231
    if-eqz v3, :cond_2

    .line 232
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 230
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 236
    :cond_3
    iget-object v0, p0, Leub;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 238
    return-void
.end method
