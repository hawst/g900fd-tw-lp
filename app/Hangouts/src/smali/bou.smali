.class public abstract Lbou;
.super Landroid/app/Service;
.source "PG"


# static fields
.field private static final a:Z


# instance fields
.field private final b:Landroid/os/Handler;

.field private c:I

.field private d:Z

.field private e:I

.field private final f:Ljava/lang/Object;

.field private final g:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lbys;->k:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbou;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 21
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lbou;->b:Landroid/os/Handler;

    .line 24
    iput v1, p0, Lbou;->c:I

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbou;->d:Z

    .line 28
    iput v1, p0, Lbou;->e:I

    .line 30
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbou;->f:Ljava/lang/Object;

    .line 52
    new-instance v0, Lbov;

    invoke-direct {v0, p0}, Lbov;-><init>(Lbou;)V

    iput-object v0, p0, Lbou;->g:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lbou;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lbou;->f:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Lbou;)I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lbou;->c:I

    return v0
.end method

.method static synthetic c(Lbou;)I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lbou;->e:I

    return v0
.end method

.method static synthetic f()Z
    .locals 1

    .prologue
    .line 17
    sget-boolean v0, Lbou;->a:Z

    return v0
.end method


# virtual methods
.method public abstract a()I
.end method

.method protected a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 173
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RefCountedService("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbou;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", startId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbou;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " stopped="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lbou;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 103
    iget-object v1, p0, Lbou;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 104
    :try_start_0
    iput p1, p0, Lbou;->e:I

    .line 105
    sget-boolean v0, Lbou;->a:Z

    if-eqz v0, :cond_0

    .line 106
    const-string v0, "Babel"

    const-string v2, "Retain w/ startId"

    invoke-virtual {p0, v2}, Lbou;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :cond_0
    invoke-virtual {p0}, Lbou;->e()V

    .line 109
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public b()Z
    .locals 3

    .prologue
    .line 85
    iget-object v1, p0, Lbou;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 86
    :try_start_0
    iget-boolean v0, p0, Lbou;->d:Z

    if-eqz v0, :cond_1

    .line 87
    sget-boolean v0, Lbou;->a:Z

    if-eqz v0, :cond_0

    .line 88
    const-string v0, "Babel"

    const-string v2, "Retain failed."

    invoke-virtual {p0, v2}, Lbou;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    .line 98
    :goto_0
    return v0

    .line 92
    :cond_1
    iget v0, p0, Lbou;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbou;->c:I

    .line 93
    sget-boolean v0, Lbou;->a:Z

    if-eqz v0, :cond_2

    .line 94
    const-string v0, "Babel"

    const-string v2, "Retain"

    invoke-virtual {p0, v2}, Lbou;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_2
    iget-object v0, p0, Lbou;->b:Landroid/os/Handler;

    iget-object v2, p0, Lbou;->g:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 97
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    const/4 v0, 0x1

    goto :goto_0

    .line 97
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public c()V
    .locals 5

    .prologue
    .line 130
    iget-object v1, p0, Lbou;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 131
    :try_start_0
    iget v0, p0, Lbou;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbou;->c:I

    .line 132
    sget-boolean v0, Lbou;->a:Z

    if-eqz v0, :cond_0

    .line 133
    const-string v0, "Babel"

    const-string v2, "Release"

    invoke-virtual {p0, v2}, Lbou;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :cond_0
    iget v0, p0, Lbou;->c:I

    if-gez v0, :cond_1

    .line 136
    const-string v0, "Babel"

    const-string v2, "Negative service count"

    invoke-virtual {p0, v2}, Lbou;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :cond_1
    iget v0, p0, Lbou;->c:I

    if-nez v0, :cond_3

    .line 140
    sget-boolean v0, Lbou;->a:Z

    if-eqz v0, :cond_2

    .line 141
    const-string v0, "Babel"

    const-string v2, "Idle"

    invoke-virtual {p0, v2}, Lbou;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :cond_2
    iget-object v0, p0, Lbou;->b:Landroid/os/Handler;

    iget-object v2, p0, Lbou;->g:Ljava/lang/Runnable;

    invoke-virtual {p0}, Lbou;->a()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 146
    :cond_3
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbou;->d:Z

    .line 45
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 118
    invoke-static {}, Lcwz;->a()V

    .line 119
    invoke-virtual {p0}, Lbou;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    const-string v0, "TAG"

    const-string v1, "Retain Failed"

    invoke-virtual {p0, v1}, Lbou;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 123
    :cond_0
    return-void
.end method

.method public onCreate()V
    .locals 5

    .prologue
    .line 151
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 152
    iget-object v1, p0, Lbou;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 153
    const/4 v0, 0x0

    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v2, p0, Lbou;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v2}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 154
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbou;->d:Z

    .line 157
    iget-object v0, p0, Lbou;->b:Landroid/os/Handler;

    iget-object v2, p0, Lbou;->g:Ljava/lang/Runnable;

    invoke-virtual {p0}, Lbou;->a()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 158
    sget-boolean v0, Lbou;->a:Z

    if-eqz v0, :cond_0

    .line 159
    const-string v0, "Babel"

    const-string v2, "onCreate"

    invoke-virtual {p0, v2}, Lbou;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 166
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 167
    sget-boolean v0, Lbou;->a:Z

    if-eqz v0, :cond_0

    .line 168
    const-string v0, "Babel"

    const-string v1, "onDestroy"

    invoke-virtual {p0, v1}, Lbou;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :cond_0
    return-void
.end method
