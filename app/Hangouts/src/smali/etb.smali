.class public final Letb;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Letb;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Boolean;

.field public d:Letc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 554
    const/4 v0, 0x0

    new-array v0, v0, [Letb;

    sput-object v0, Letb;->a:[Letb;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 555
    invoke-direct {p0}, Lepn;-><init>()V

    .line 733
    iput-object v0, p0, Letb;->b:Ljava/lang/Integer;

    .line 738
    iput-object v0, p0, Letb;->d:Letc;

    .line 555
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 758
    const/4 v0, 0x0

    .line 759
    iget-object v1, p0, Letb;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 760
    const/4 v0, 0x1

    iget-object v1, p0, Letb;->b:Ljava/lang/Integer;

    .line 761
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 763
    :cond_0
    iget-object v1, p0, Letb;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 764
    const/4 v1, 0x2

    iget-object v2, p0, Letb;->c:Ljava/lang/Boolean;

    .line 765
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 767
    :cond_1
    iget-object v1, p0, Letb;->d:Letc;

    if-eqz v1, :cond_2

    .line 768
    const/4 v1, 0x3

    iget-object v2, p0, Letb;->d:Letc;

    .line 769
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 771
    :cond_2
    iget-object v1, p0, Letb;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 772
    iput v0, p0, Letb;->cachedSize:I

    .line 773
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 551
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Letb;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Letb;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Letb;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Letb;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Letb;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Letb;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Letb;->d:Letc;

    if-nez v0, :cond_4

    new-instance v0, Letc;

    invoke-direct {v0}, Letc;-><init>()V

    iput-object v0, p0, Letb;->d:Letc;

    :cond_4
    iget-object v0, p0, Letb;->d:Letc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 743
    iget-object v0, p0, Letb;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 744
    const/4 v0, 0x1

    iget-object v1, p0, Letb;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 746
    :cond_0
    iget-object v0, p0, Letb;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 747
    const/4 v0, 0x2

    iget-object v1, p0, Letb;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 749
    :cond_1
    iget-object v0, p0, Letb;->d:Letc;

    if-eqz v0, :cond_2

    .line 750
    const/4 v0, 0x3

    iget-object v1, p0, Letb;->d:Letc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 752
    :cond_2
    iget-object v0, p0, Letb;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 754
    return-void
.end method
