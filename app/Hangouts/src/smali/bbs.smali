.class public final Lbbs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lair;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/phone/ShareIntentActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/hangouts/phone/ShareIntentActivity;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lbbs;->a:Lcom/google/android/apps/hangouts/phone/ShareIntentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/hangouts/phone/ShareIntentActivity;B)V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lbbs;-><init>(Lcom/google/android/apps/hangouts/phone/ShareIntentActivity;)V

    return-void
.end method


# virtual methods
.method public a(Lahc;)V
    .locals 4

    .prologue
    .line 150
    iget-object v0, p0, Lbbs;->a:Lcom/google/android/apps/hangouts/phone/ShareIntentActivity;

    .line 151
    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/ShareIntentActivity;->a(Lcom/google/android/apps/hangouts/phone/ShareIntentActivity;)Lyj;

    move-result-object v0

    iget-object v1, p1, Lahc;->a:Ljava/lang/String;

    iget v2, p1, Lahc;->d:I

    .line 150
    invoke-static {v0, v1, v2}, Lbbl;->a(Lyj;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 152
    const-string v1, "conversation_parameters"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 153
    iget-object v1, p0, Lbbs;->a:Lcom/google/android/apps/hangouts/phone/ShareIntentActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/phone/ShareIntentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 154
    const-string v2, "conversation_id"

    iget-object v3, p1, Lahc;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 155
    const-string v2, "ShareIntentActivity.openConversation"

    invoke-static {v2, v1}, Lbxm;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 156
    const-string v2, "share_intent"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 157
    const/high16 v1, 0x800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 158
    iget-object v1, p0, Lbbs;->a:Lcom/google/android/apps/hangouts/phone/ShareIntentActivity;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/hangouts/phone/ShareIntentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 159
    iget-object v0, p0, Lbbs;->a:Lcom/google/android/apps/hangouts/phone/ShareIntentActivity;

    sget v1, Lf;->aV:I

    sget v2, Lf;->aW:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/hangouts/phone/ShareIntentActivity;->overridePendingTransition(II)V

    .line 161
    return-void
.end method

.method public a(Lbdk;Ljava/lang/String;IJ)V
    .locals 1

    .prologue
    .line 172
    const-string v0, "Should not get called"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    .line 173
    return-void
.end method

.method public a(Ljava/lang/String;ZII)V
    .locals 1

    .prologue
    .line 166
    const-string v0, "Should not get called"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    .line 167
    return-void
.end method

.method public b_(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x0

    return v0
.end method

.method public o_()V
    .locals 1

    .prologue
    .line 177
    const-string v0, "Should not get called"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    .line 178
    return-void
.end method
