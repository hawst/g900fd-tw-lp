.class public abstract Latz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;


# instance fields
.field final synthetic a:Latu;


# direct methods
.method protected constructor <init>(Latu;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Latz;->a:Latu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getIdealCaptureSize()Lcom/google/android/libraries/hangouts/video/Size;
    .locals 1

    .prologue
    .line 173
    invoke-static {}, Lcwz;->g()V

    .line 174
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getOutgoingNoEffectsVideoSpec()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getSize()Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v0

    return-object v0
.end method

.method public onCameraOpenError(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 156
    invoke-static {}, Lcwz;->c()V

    .line 157
    iget-object v0, p0, Latz;->a:Latu;

    iget v0, v0, Latu;->p:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 158
    iget-object v0, p0, Latz;->a:Latu;

    iget-object v0, v0, Latu;->e:Latd;

    new-instance v1, Laub;

    invoke-direct {v1, p0}, Laub;-><init>(Latz;)V

    invoke-virtual {v0, v1}, Latd;->a(Ljava/lang/Runnable;)V

    .line 168
    return-void
.end method

.method public onCameraOpened(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 127
    invoke-static {}, Lcwz;->c()V

    .line 129
    iget-object v0, p0, Latz;->a:Latu;

    invoke-static {v0, v2}, Latu;->c(Latu;Z)Z

    .line 130
    iget-object v0, p0, Latz;->a:Latu;

    iput-boolean p1, v0, Latu;->v:Z

    .line 131
    iget-object v0, p0, Latz;->a:Latu;

    iget-object v0, v0, Latu;->g:Lasx;

    invoke-virtual {v0}, Lasx;->g()V

    .line 136
    iget-object v0, p0, Latz;->a:Latu;

    iget v0, v0, Latu;->p:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 137
    iget-object v0, p0, Latz;->a:Latu;

    const/4 v1, 0x1

    iput-boolean v1, v0, Latu;->J:Z

    .line 138
    iget-object v0, p0, Latz;->a:Latu;

    iget-object v0, v0, Latu;->h:Laui;

    iget-object v1, p0, Latz;->a:Latu;

    iget-boolean v1, v1, Latu;->v:Z

    invoke-virtual {v0, v1}, Laui;->a(Z)V

    .line 141
    iget-object v0, p0, Latz;->a:Latu;

    iput v2, v0, Latu;->q:I

    .line 144
    iget-object v0, p0, Latz;->a:Latu;

    iget-object v0, v0, Latu;->e:Latd;

    new-instance v1, Laua;

    invoke-direct {v1, p0}, Laua;-><init>(Latz;)V

    invoke-virtual {v0, v1}, Latd;->a(Ljava/lang/Runnable;)V

    .line 151
    :cond_0
    return-void
.end method

.method public onOutputTextureNameChanged(I)V
    .locals 1

    .prologue
    .line 115
    invoke-static {}, Lcwz;->c()V

    .line 116
    iget-object v0, p0, Latz;->a:Latu;

    iput p1, v0, Latu;->I:I

    .line 117
    return-void
.end method

.method public queueEventForGLThread(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 108
    invoke-static {}, Lcwz;->d()V

    .line 109
    iget-object v0, p0, Latz;->a:Latu;

    iget-object v0, v0, Latu;->e:Latd;

    invoke-virtual {v0, p1}, Latd;->b(Ljava/lang/Runnable;)V

    .line 110
    return-void
.end method
