.class public final Lbzq;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Z

.field private static final b:Lbzr;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 26
    sget-object v0, Lbys;->r:Lcyp;

    sput-boolean v2, Lbzq;->a:Z

    .line 40
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    sget-boolean v0, Lbzq;->a:Z

    if-eqz v0, :cond_0

    .line 41
    new-instance v0, Lbzs;

    invoke-direct {v0, v2}, Lbzs;-><init>(B)V

    sput-object v0, Lbzq;->b:Lbzr;

    .line 45
    :goto_0
    return-void

    .line 43
    :cond_0
    new-instance v0, Lbzt;

    invoke-direct {v0, v2}, Lbzt;-><init>(B)V

    sput-object v0, Lbzq;->b:Lbzr;

    goto :goto_0
.end method

.method public static a()V
    .locals 2

    .prologue
    .line 72
    sget-object v0, Lbzq;->b:Lbzr;

    invoke-virtual {v0}, Lbzr;->a()V

    .line 73
    const-string v0, "Babel_Trace"

    const-string v1, "endSection()"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 59
    sget-boolean v0, Lbzq;->a:Z

    if-eqz v0, :cond_0

    .line 60
    const-string v0, "Babel_Trace"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "beginSection() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_0
    sget-object v0, Lbzq;->b:Lbzr;

    invoke-virtual {v0, p0}, Lbzr;->a(Ljava/lang/String;)V

    .line 63
    return-void
.end method
