.class public Lbem;
.super Lbep;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:[Ljava/lang/String;


# direct methods
.method public constructor <init>([Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 4627
    invoke-direct {p0}, Lbep;-><init>()V

    .line 4628
    iput-object p1, p0, Lbem;->a:[Ljava/lang/String;

    .line 4629
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 6

    .prologue
    .line 4633
    const-string v0, "Babel_RequestWriter"

    const-string v1, "GetFifeUrlRequest build protobuf"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 4635
    new-instance v1, Ldxr;

    invoke-direct {v1}, Ldxr;-><init>()V

    .line 4636
    invoke-virtual {p0, p1, p2}, Lbem;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v0

    iput-object v0, v1, Ldxr;->b:Ldvm;

    .line 4638
    iget-object v0, p0, Lbem;->a:[Ljava/lang/String;

    array-length v2, v0

    .line 4639
    new-array v3, v2, [Ldxq;

    .line 4641
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 4642
    iget-object v4, p0, Lbem;->a:[Ljava/lang/String;

    aget-object v4, v4, v0

    .line 4643
    new-instance v5, Ldxq;

    invoke-direct {v5}, Ldxq;-><init>()V

    .line 4644
    iput-object v4, v5, Ldxq;->c:Ljava/lang/String;

    .line 4645
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v5, Ldxq;->b:Ljava/lang/Integer;

    .line 4646
    aput-object v5, v3, v0

    .line 4641
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4648
    :cond_0
    iput-object v3, v1, Ldxr;->c:[Ldxq;

    .line 4649
    return-object v1
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4654
    const-string v0, "urls/urlredirectwrapper"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4659
    const-string v0, "ui_queue"

    return-object v0
.end method
