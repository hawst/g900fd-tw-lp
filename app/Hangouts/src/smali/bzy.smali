.class public abstract Lbzy;
.super Lbzx;
.source "PG"


# static fields
.field private static final a:Z


# instance fields
.field private final b:[Ljava/lang/String;

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:[Ljava/lang/String;

.field private final e:Lwg;

.field private f:[J

.field private g:[Z

.field private h:Z

.field private i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lbys;->r:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbzy;->a:Z

    return-void
.end method

.method protected constructor <init>([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lbzx;-><init>()V

    .line 43
    iput-object p1, p0, Lbzy;->b:[Ljava/lang/String;

    .line 44
    iput-object p2, p0, Lbzy;->d:[Ljava/lang/String;

    .line 45
    invoke-virtual {p0}, Lbzy;->b()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbzy;->c:Ljava/util/ArrayList;

    .line 46
    invoke-virtual {p0}, Lbzy;->d()Lwg;

    move-result-object v0

    iput-object v0, p0, Lbzy;->e:Lwg;

    .line 47
    iget-object v0, p0, Lbzy;->d:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [J

    iput-object v0, p0, Lbzy;->f:[J

    .line 48
    iget-object v0, p0, Lbzy;->d:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lbzy;->g:[Z

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbzy;->h:Z

    .line 50
    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method protected a(Ljava/lang/String;)Lwf;
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lwf;

    invoke-direct {v0, p1}, Lwf;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public declared-synchronized a(IJ)V
    .locals 3

    .prologue
    .line 119
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lbzy;->d:[Ljava/lang/String;

    array-length v1, v1

    invoke-static {p1, v0, v1}, Lcwz;->a(III)V

    .line 120
    sget-boolean v0, Lbzy;->a:Z

    if-eqz v0, :cond_0

    .line 121
    const-string v0, "MarkReporterDefault"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Marking ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbzy;->d:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_1

    .line 126
    iget-object v0, p0, Lbzy;->g:[Z

    aget-boolean v0, v0, p1

    if-nez v0, :cond_2

    .line 128
    iget-object v0, p0, Lbzy;->f:[J

    aput-wide p2, v0, p1

    .line 130
    iget-object v0, p0, Lbzy;->g:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 132
    :cond_2
    :try_start_1
    const-string v0, "MarkReporterDefault"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Mark ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbzy;->d:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] is set more than once"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Lwf;)V
    .locals 3

    .prologue
    .line 143
    invoke-virtual {p0}, Lbzy;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 145
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 146
    const-string v1, "e"

    const-string v2, ","

    invoke-static {v2, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lwf;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :cond_0
    return-void
.end method

.method public varargs declared-synchronized a([I)V
    .locals 5

    .prologue
    .line 109
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lbzy;->h:Z

    if-nez v0, :cond_0

    .line 110
    iget-object v0, p0, Lbzy;->e:Lwg;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    .line 111
    array-length v3, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, p1, v0

    .line 112
    invoke-virtual {p0, v4, v1, v2}, Lbzy;->a(IJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 115
    :cond_0
    monitor-exit p0

    return-void

    .line 109
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract b()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end method

.method protected b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lbzy;->i:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public declared-synchronized c()V
    .locals 9

    .prologue
    .line 80
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lbzy;->h:Z

    if-nez v0, :cond_2

    .line 81
    const-string v0, "MarkReporterDefault"

    const-string v1, "CSI Report is about to be sent"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string v0, ","

    iget-object v1, p0, Lbzy;->b:[Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbzy;->a(Ljava/lang/String;)Lwf;

    move-result-object v2

    .line 83
    iget-object v0, p0, Lbzy;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 85
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    const/4 v4, 0x0

    invoke-static {v1, v4}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v1

    .line 86
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    const/4 v4, 0x0

    invoke-static {v0, v4}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    .line 88
    iget-object v4, p0, Lbzy;->g:[Z

    aget-boolean v4, v4, v1

    if-eqz v4, :cond_0

    iget-object v4, p0, Lbzy;->g:[Z

    aget-boolean v4, v4, v0

    if-eqz v4, :cond_0

    .line 89
    iget-object v4, p0, Lbzy;->f:[J

    aget-wide v4, v4, v0

    iget-object v6, p0, Lbzy;->f:[J

    aget-wide v6, v6, v1

    sub-long/2addr v4, v6

    .line 91
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, p0, Lbzy;->d:[Ljava/lang/String;

    aget-object v8, v8, v0

    aput-object v8, v6, v7

    invoke-virtual {v2, v4, v5, v6}, Lwf;->a(J[Ljava/lang/String;)Z

    .line 92
    sget-boolean v6, Lbzy;->a:Z

    if-eqz v6, :cond_0

    .line 93
    const-string v6, "MarkReporterDefault"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "["

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lbzy;->d:[Ljava/lang/String;

    aget-object v1, v8, v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "-"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v7, p0, Lbzy;->d:[Ljava/lang/String;

    aget-object v0, v7, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 98
    :cond_1
    :try_start_1
    invoke-virtual {p0, v2}, Lbzy;->a(Lwf;)V

    .line 100
    invoke-virtual {p0}, Lbzy;->e()Lwa;

    move-result-object v0

    invoke-virtual {v0, v2}, Lwa;->a(Lwf;)Z

    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbzy;->h:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 103
    :cond_2
    monitor-exit p0

    return-void
.end method

.method protected d()Lwg;
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lwg;

    invoke-direct {v0}, Lwg;-><init>()V

    return-object v0
.end method

.method protected e()Lwa;
    .locals 2

    .prologue
    .line 68
    new-instance v0, Lvz;

    invoke-direct {v0}, Lvz;-><init>()V

    .line 69
    const-string v1, "amc_video"

    invoke-virtual {v0, v1}, Lvz;->a(Ljava/lang/String;)Lvz;

    .line 70
    iget-object v1, p0, Lbzy;->i:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 71
    iget-object v1, p0, Lbzy;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lvz;->b(Ljava/lang/String;)Lvz;

    .line 73
    :cond_0
    invoke-static {v0}, Lf;->a(Lvz;)V

    .line 75
    invoke-static {}, Lf;->k()Lwa;

    move-result-object v0

    return-object v0
.end method
