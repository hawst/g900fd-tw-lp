.class public interface abstract Lcpp;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a()Lcom/google/android/gms/maps/model/CameraPosition;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/PolylineOptions;)Lctg;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/CircleOptions;)Lctm;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/GroundOverlayOptions;)Lctp;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lctv;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/PolygonOptions;)Lcty;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/TileOverlayOptions;)Lcub;
.end method

.method public abstract a(I)V
.end method

.method public abstract a(IIII)V
.end method

.method public abstract a(Lcjc;)V
.end method

.method public abstract a(Lcjc;ILcqq;)V
.end method

.method public abstract a(Lcjc;Lcqq;)V
.end method

.method public abstract a(Lcps;)V
.end method

.method public abstract a(Lcqw;)V
.end method

.method public abstract a(Lcqz;)V
.end method

.method public abstract a(Lcrc;)V
.end method

.method public abstract a(Lcrf;)V
.end method

.method public abstract a(Lcrl;)V
.end method

.method public abstract a(Lcro;)V
.end method

.method public abstract a(Lcrr;)V
.end method

.method public abstract a(Lcru;)V
.end method

.method public abstract a(Lcrx;)V
.end method

.method public abstract a(Lcsa;)V
.end method

.method public abstract a(Lcsd;)V
.end method

.method public abstract a(Lcsp;Lcjc;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract b()F
.end method

.method public abstract b(Lcjc;)V
.end method

.method public abstract b(Z)Z
.end method

.method public abstract c()F
.end method

.method public abstract c(Z)V
.end method

.method public abstract d()V
.end method

.method public abstract d(Z)V
.end method

.method public abstract e()V
.end method

.method public abstract f()I
.end method

.method public abstract g()Z
.end method

.method public abstract h()Z
.end method

.method public abstract i()Z
.end method

.method public abstract j()Landroid/location/Location;
.end method

.method public abstract k()Lcqn;
.end method

.method public abstract l()Lcqb;
.end method

.method public abstract m()Z
.end method

.method public abstract n()Lcts;
.end method
