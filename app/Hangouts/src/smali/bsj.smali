.class public final Lbsj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcvn;
.implements Lcvr;


# static fields
.field private static volatile a:Z

.field private static volatile b:Z

.field private static volatile c:Ljava/lang/String;

.field private static volatile d:Ljava/lang/String;

.field private static volatile e:I


# instance fields
.field private final f:Lcvi;

.field private final g:Lyj;

.field private final h:Lbsl;

.field private final i:Ljava/lang/String;

.field private j:Lcvx;

.field private k:Lcwf;

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    invoke-static {}, Lbsj;->d()V

    .line 73
    new-instance v0, Lbsk;

    invoke-direct {v0}, Lbsk;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/Runnable;)V

    .line 79
    return-void
.end method

.method public constructor <init>(Lcvi;Lbsl;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 174
    invoke-direct {p0, p1, v0, p2, v0}, Lbsj;-><init>(Lcvi;Lyj;Lbsl;Ljava/lang/String;)V

    .line 175
    return-void
.end method

.method public constructor <init>(Lcvi;Lyj;Lbsl;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 187
    iput-object p1, p0, Lbsj;->f:Lcvi;

    .line 188
    iput-object p2, p0, Lbsj;->g:Lyj;

    .line 189
    iput-object p3, p0, Lbsj;->h:Lbsl;

    .line 190
    iput-object p4, p0, Lbsj;->i:Ljava/lang/String;

    .line 191
    return-void
.end method

.method static synthetic c()V
    .locals 0

    .prologue
    .line 31
    invoke-static {}, Lbsj;->d()V

    return-void
.end method

.method private static d()V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x0

    const/4 v2, 0x1

    .line 54
    const-string v0, "babel_local_contact_roster_mode"

    const-string v1, "default"

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbsj;->c:Ljava/lang/String;

    .line 57
    const-string v0, "babel_local_contact_search_mode"

    const-string v1, "default"

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbsj;->d:Ljava/lang/String;

    .line 60
    const-string v0, "babel_disable_local_contact_threshold"

    const/16 v1, 0x7d0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lbsj;->e:I

    .line 64
    sget-object v0, Lbsj;->c:Ljava/lang/String;

    const-string v1, "always_disable"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "Babel"

    const-string v1, "Force disable local contact roster"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    sput-boolean v2, Lbsj;->a:Z

    move v6, v2

    :goto_0
    sget-object v0, Lbsj;->d:Ljava/lang/String;

    const-string v1, "always_disable"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "Babel"

    const-string v1, "Force disable local contact search"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    sput-boolean v2, Lbsj;->b:Z

    move v8, v2

    :goto_1
    if-eqz v6, :cond_0

    if-nez v8, :cond_3

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_7

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "_id"

    aput-object v4, v2, v7

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_7

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    sget v2, Lbsj;->e:I

    if-le v0, v2, :cond_6

    if-nez v6, :cond_1

    const-string v0, "Babel"

    const-string v2, "Disable local contact roster. Too many contacts."

    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    sput-boolean v0, Lbsj;->a:Z

    :cond_1
    if-nez v8, :cond_2

    const-string v0, "Babel"

    const-string v2, "Disable local contact search. Too many contacts."

    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    sput-boolean v0, Lbsj;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 65
    :cond_3
    :goto_2
    return-void

    .line 64
    :cond_4
    sget-object v0, Lbsj;->c:Ljava/lang/String;

    const-string v1, "always_enable"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "Babel"

    const-string v1, "Force enable local contact roster"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    sput-boolean v7, Lbsj;->a:Z

    move v6, v2

    goto :goto_0

    :cond_5
    sget-object v0, Lbsj;->d:Ljava/lang/String;

    const-string v1, "always_enable"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "Babel"

    const-string v1, "Force enable local contact search"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    sput-boolean v7, Lbsj;->b:Z

    move v8, v2

    goto :goto_1

    :cond_6
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_7
    if-nez v6, :cond_8

    const-string v0, "Babel"

    const-string v1, "Enable local contact roster by default."

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    sput-boolean v7, Lbsj;->a:Z

    :cond_8
    if-nez v8, :cond_3

    const-string v0, "Babel"

    const-string v1, "Enable local contact search by default."

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    sput-boolean v7, Lbsj;->b:Z

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_9
    move v8, v7

    goto/16 :goto_1

    :cond_a
    move v6, v7

    goto/16 :goto_0
.end method

.method private e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lbsj;->g:Lyj;

    if-nez v0, :cond_0

    .line 226
    const-string v0, "fake_account"

    .line 229
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbsj;->g:Lyj;

    invoke-virtual {v0}, Lyj;->ak()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lbsj;->g:Lyj;

    if-nez v0, :cond_0

    .line 234
    const/4 v0, 0x0

    .line 236
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbsj;->g:Lyj;

    invoke-virtual {v0}, Lyj;->al()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private g()V
    .locals 4

    .prologue
    .line 242
    iget-object v0, p0, Lbsj;->f:Lcvi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbsj;->f:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 243
    :cond_0
    const-string v0, "Babel"

    const-string v1, "People client not connected. Skip loading aggregated people"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    :goto_0
    return-void

    .line 247
    :cond_1
    iget-object v0, p0, Lbsj;->f:Lcvi;

    invoke-direct {p0}, Lbsj;->e()Ljava/lang/String;

    move-result-object v1

    .line 248
    invoke-direct {p0}, Lbsj;->f()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lbsj;->i:Ljava/lang/String;

    .line 247
    invoke-virtual {v0, p0, v1, v2, v3}, Lcvi;->a(Lcvn;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private h()V
    .locals 4

    .prologue
    .line 259
    iget-object v0, p0, Lbsj;->f:Lcvi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbsj;->f:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 260
    :cond_0
    const-string v0, "Babel"

    const-string v1, "People client not connected. Skip loading people"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    :goto_0
    return-void

    .line 264
    :cond_1
    new-instance v0, Lcvm;

    invoke-direct {v0}, Lcvm;-><init>()V

    .line 265
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcvm;->a(Z)Lcvm;

    .line 267
    iget-object v1, p0, Lbsj;->i:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 268
    iget-object v1, p0, Lbsj;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcvm;->a(Ljava/lang/String;)Lcvm;

    .line 271
    :cond_2
    iget-object v1, p0, Lbsj;->f:Lcvi;

    invoke-direct {p0}, Lbsj;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lbsj;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, p0, v2, v3, v0}, Lcvi;->a(Lcvr;Ljava/lang/String;Ljava/lang/String;Lcvm;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 197
    iget-boolean v0, p0, Lbsj;->l:Z

    if-nez v0, :cond_0

    .line 198
    iget-object v0, p0, Lbsj;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-boolean v0, Lbsj;->a:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lbsj;->h()V

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 198
    :cond_1
    invoke-direct {p0}, Lbsj;->g()V

    goto :goto_0

    :cond_2
    sget-boolean v0, Lbsj;->b:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lbsj;->h()V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lbsj;->g()V

    goto :goto_0
.end method

.method public a(Lcft;Lcvx;)V
    .locals 3

    .prologue
    .line 280
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Aggregated people loaded: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " aggregatedPeople="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    :cond_0
    iget-boolean v0, p0, Lbsj;->l:Z

    if-eqz v0, :cond_2

    .line 285
    invoke-virtual {p2}, Lcvx;->d()V

    .line 305
    :cond_1
    :goto_0
    return-void

    .line 293
    :cond_2
    iget-object v0, p0, Lbsj;->j:Lcvx;

    invoke-static {v0, p2}, Lcwz;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 294
    iput-object p2, p0, Lbsj;->j:Lcvx;

    .line 296
    invoke-virtual {p1}, Lcft;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297
    iget-object v0, p0, Lbsj;->h:Lbsl;

    if-eqz v0, :cond_3

    .line 298
    new-instance v0, Lbtb;

    invoke-direct {v0, p2}, Lbtb;-><init>(Lcvx;)V

    .line 300
    iget-object v1, p0, Lbsj;->h:Lbsl;

    const/4 v2, 0x0

    invoke-interface {v1, p0, v0, v2}, Lbsl;->a(Lbsj;Lcvx;Lcwf;)V

    goto :goto_0

    .line 302
    :cond_3
    invoke-virtual {p2}, Lcvx;->d()V

    goto :goto_0
.end method

.method public a(Lcft;Lcwf;)V
    .locals 3

    .prologue
    .line 309
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Non aggregated people loaded: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " people="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    :cond_0
    iget-boolean v0, p0, Lbsj;->l:Z

    if-eqz v0, :cond_2

    .line 313
    invoke-virtual {p2}, Lcwf;->d()V

    .line 331
    :cond_1
    :goto_0
    return-void

    .line 321
    :cond_2
    iget-object v0, p0, Lbsj;->k:Lcwf;

    invoke-static {v0, p2}, Lcwz;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 322
    iput-object p2, p0, Lbsj;->k:Lcwf;

    .line 324
    invoke-virtual {p1}, Lcft;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 325
    iget-object v0, p0, Lbsj;->h:Lbsl;

    if-eqz v0, :cond_3

    .line 326
    iget-object v0, p0, Lbsj;->h:Lbsl;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1, p2}, Lbsl;->a(Lbsj;Lcvx;Lcwf;)V

    goto :goto_0

    .line 328
    :cond_3
    invoke-virtual {p2}, Lcwf;->d()V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbsj;->l:Z

    .line 204
    return-void
.end method
