.class final Lcpg;
.super Lciu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lciu",
        "<",
        "Lcpf;",
        ">;"
    }
.end annotation


# instance fields
.field protected d:Lcjg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcjg",
            "<",
            "Lcpf;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/view/ViewGroup;

.field private final f:Landroid/content/Context;

.field private final g:Lcom/google/android/gms/maps/GoogleMapOptions;


# direct methods
.method constructor <init>(Landroid/view/ViewGroup;Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V
    .locals 0

    invoke-direct {p0}, Lciu;-><init>()V

    iput-object p1, p0, Lcpg;->e:Landroid/view/ViewGroup;

    iput-object p2, p0, Lcpg;->f:Landroid/content/Context;

    iput-object p3, p0, Lcpg;->g:Lcom/google/android/gms/maps/GoogleMapOptions;

    return-void
.end method


# virtual methods
.method protected a(Lcjg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcjg",
            "<",
            "Lcpf;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcpg;->d:Lcjg;

    invoke-virtual {p0}, Lcpg;->g()V

    return-void
.end method

.method public g()V
    .locals 4

    iget-object v0, p0, Lcpg;->d:Lcjg;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcpg;->a()Lcit;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcpg;->f:Landroid/content/Context;

    invoke-static {v0}, Lcst;->a(Landroid/content/Context;)Lcqt;

    move-result-object v0

    iget-object v1, p0, Lcpg;->f:Landroid/content/Context;

    invoke-static {v1}, Lcjf;->a(Ljava/lang/Object;)Lcjc;

    move-result-object v1

    iget-object v2, p0, Lcpg;->g:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-interface {v0, v1, v2}, Lcqt;->a(Lcjc;Lcom/google/android/gms/maps/GoogleMapOptions;)Lcpy;

    move-result-object v0

    iget-object v1, p0, Lcpg;->d:Lcjg;

    new-instance v2, Lcpf;

    iget-object v3, p0, Lcpg;->e:Landroid/view/ViewGroup;

    invoke-direct {v2, v3, v0}, Lcpf;-><init>(Landroid/view/ViewGroup;Lcpy;)V

    invoke-interface {v1, v2}, Lcjg;->a(Lcit;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcfx; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lv;

    invoke-direct {v1, v0}, Lv;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :catch_1
    move-exception v0

    goto :goto_0
.end method
