.class public final Lbit;
.super Lbiq;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbiu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ldpn;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1231
    invoke-direct {p0}, Lbiq;-><init>()V

    .line 1232
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbit;->b:Ljava/util/List;

    .line 1233
    iget-object v2, p1, Ldpn;->b:[Ldpo;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 1234
    iget-object v5, v4, Ldpo;->b:Ldui;

    iget-object v5, v5, Ldui;->c:Ljava/lang/String;

    .line 1235
    iget-object v4, v4, Ldpo;->c:Ljava/lang/Integer;

    invoke-static {v4, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v4

    .line 1236
    iget-object v6, p0, Lbit;->b:Ljava/util/List;

    new-instance v7, Lbiu;

    invoke-direct {v7, v5, v4}, Lbiu;-><init>(Ljava/lang/String;I)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1233
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1238
    :cond_0
    return-void
.end method
