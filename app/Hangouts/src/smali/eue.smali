.class public final Leue;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leue;


# instance fields
.field public b:Ljava/lang/String;

.field public c:[B

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Lfab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Leue;

    sput-object v0, Leue;->a:[Leue;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Leue;->g:Lfab;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 52
    const/4 v0, 0x0

    .line 53
    iget-object v1, p0, Leue;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 54
    const/4 v0, 0x1

    iget-object v1, p0, Leue;->b:Ljava/lang/String;

    .line 55
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 57
    :cond_0
    iget-object v1, p0, Leue;->c:[B

    if-eqz v1, :cond_1

    .line 58
    const/4 v1, 0x2

    iget-object v2, p0, Leue;->c:[B

    .line 59
    invoke-static {v1, v2}, Lepl;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 61
    :cond_1
    iget-object v1, p0, Leue;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 62
    const/4 v1, 0x3

    iget-object v2, p0, Leue;->d:Ljava/lang/String;

    .line 63
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 65
    :cond_2
    iget-object v1, p0, Leue;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 66
    const/4 v1, 0x4

    iget-object v2, p0, Leue;->e:Ljava/lang/String;

    .line 67
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_3
    iget-object v1, p0, Leue;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 70
    const/4 v1, 0x5

    iget-object v2, p0, Leue;->f:Ljava/lang/String;

    .line 71
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    :cond_4
    iget-object v1, p0, Leue;->g:Lfab;

    if-eqz v1, :cond_5

    .line 74
    const/4 v1, 0x7

    iget-object v2, p0, Leue;->g:Lfab;

    .line 75
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 77
    :cond_5
    iget-object v1, p0, Leue;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    iput v0, p0, Leue;->cachedSize:I

    .line 79
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leue;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leue;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leue;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leue;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Leue;->c:[B

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leue;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leue;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leue;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Leue;->g:Lfab;

    if-nez v0, :cond_2

    new-instance v0, Lfab;

    invoke-direct {v0}, Lfab;-><init>()V

    iput-object v0, p0, Leue;->g:Lfab;

    :cond_2
    iget-object v0, p0, Leue;->g:Lfab;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x3a -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Leue;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 29
    const/4 v0, 0x1

    iget-object v1, p0, Leue;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 31
    :cond_0
    iget-object v0, p0, Leue;->c:[B

    if-eqz v0, :cond_1

    .line 32
    const/4 v0, 0x2

    iget-object v1, p0, Leue;->c:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 34
    :cond_1
    iget-object v0, p0, Leue;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 35
    const/4 v0, 0x3

    iget-object v1, p0, Leue;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 37
    :cond_2
    iget-object v0, p0, Leue;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 38
    const/4 v0, 0x4

    iget-object v1, p0, Leue;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 40
    :cond_3
    iget-object v0, p0, Leue;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 41
    const/4 v0, 0x5

    iget-object v1, p0, Leue;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 43
    :cond_4
    iget-object v0, p0, Leue;->g:Lfab;

    if-eqz v0, :cond_5

    .line 44
    const/4 v0, 0x7

    iget-object v1, p0, Leue;->g:Lfab;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 46
    :cond_5
    iget-object v0, p0, Leue;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 48
    return-void
.end method
