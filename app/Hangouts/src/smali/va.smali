.class public abstract Lva;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lva",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field a:Ljava/lang/Integer;

.field b:Z

.field public c:Lvi;

.field private final d:Lvn;

.field private final e:I

.field private final f:Ljava/lang/String;

.field private final g:I

.field private final h:Lvf;

.field private i:Lvd;

.field private j:Z

.field private k:Z

.field private l:J

.field private m:Lup;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lvf;)V
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lva;-><init>(Ljava/lang/String;Lvf;B)V

    .line 115
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lvf;B)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    sget-boolean v0, Lvn;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Lvn;

    invoke-direct {v0}, Lvn;-><init>()V

    :goto_0
    iput-object v0, p0, Lva;->d:Lvn;

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lva;->b:Z

    .line 81
    iput-boolean v2, p0, Lva;->j:Z

    .line 84
    iput-boolean v2, p0, Lva;->k:Z

    .line 87
    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lva;->l:J

    .line 100
    iput-object v1, p0, Lva;->m:Lup;

    .line 124
    const/4 v0, -0x1

    iput v0, p0, Lva;->e:I

    .line 125
    iput-object p1, p0, Lva;->f:Ljava/lang/String;

    .line 126
    iput-object p2, p0, Lva;->h:Lvf;

    .line 127
    new-instance v0, Lvi;

    invoke-direct {v0}, Lvi;-><init>()V

    invoke-virtual {p0, v0}, Lva;->a(Lvi;)V

    .line 129
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    iput v0, p0, Lva;->g:I

    .line 130
    return-void

    :cond_0
    move-object v0, v1

    .line 57
    goto :goto_0

    .line 129
    :cond_1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1
.end method

.method static synthetic b(Lva;)Lvn;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lva;->d:Lvn;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lva;->e:I

    return v0
.end method

.method public a(Lva;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lva",
            "<TT;>;)I"
        }
    .end annotation

    .prologue
    .line 527
    invoke-virtual {p0}, Lva;->j()Lvc;

    move-result-object v0

    .line 528
    invoke-virtual {p1}, Lva;->j()Lvc;

    move-result-object v1

    .line 532
    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lva;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p1, Lva;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v1}, Lvc;->ordinal()I

    move-result v1

    invoke-virtual {v0}, Lvc;->ordinal()I

    move-result v0

    sub-int v0, v1, v0

    goto :goto_0
.end method

.method public abstract a(Lux;)Lve;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lux;",
            ")",
            "Lve",
            "<TT;>;"
        }
    .end annotation
.end method

.method protected a(Lvl;)Lvl;
    .locals 0

    .prologue
    .line 497
    return-object p1
.end method

.method public abstract a(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 173
    sget-boolean v0, Lvn;->a:Z

    if-eqz v0, :cond_1

    .line 174
    iget-object v0, p0, Lva;->d:Lvn;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lvn;->a(Ljava/lang/String;J)V

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    iget-wide v0, p0, Lva;->l:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 176
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lva;->l:J

    goto :goto_0
.end method

.method public a(Lup;)V
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lva;->m:Lup;

    .line 260
    return-void
.end method

.method public a(Lvd;)V
    .locals 0

    .prologue
    .line 220
    iput-object p1, p0, Lva;->i:Lvd;

    .line 221
    return-void
.end method

.method public a(Lvi;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lva;->c:Lvi;

    .line 167
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lva;->g:I

    return v0
.end method

.method b(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 186
    iget-object v0, p0, Lva;->i:Lvd;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lva;->i:Lvd;

    invoke-virtual {v0, p0}, Lvd;->b(Lva;)V

    .line 189
    :cond_0
    sget-boolean v0, Lvn;->a:Z

    if-eqz v0, :cond_3

    .line 190
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    .line 191
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v2, v3, :cond_2

    .line 194
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 195
    new-instance v3, Lvb;

    invoke-direct {v3, p0, p1, v0, v1}, Lvb;-><init>(Lva;Ljava/lang/String;J)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 213
    :cond_1
    :goto_0
    return-void

    .line 205
    :cond_2
    iget-object v2, p0, Lva;->d:Lvn;

    invoke-virtual {v2, p1, v0, v1}, Lvn;->a(Ljava/lang/String;J)V

    .line 206
    iget-object v0, p0, Lva;->d:Lvn;

    invoke-virtual {p0}, Lva;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lvn;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 208
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lva;->l:J

    sub-long/2addr v0, v2

    .line 209
    const-wide/16 v2, 0xbb8

    cmp-long v2, v0, v2

    if-ltz v2, :cond_1

    .line 210
    const-string v2, "%d ms: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-virtual {p0}, Lva;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Lvm;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public b(Lvl;)V
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lva;->h:Lvf;

    if-eqz v0, :cond_0

    .line 517
    iget-object v0, p0, Lva;->h:Lvf;

    invoke-interface {v0, p1}, Lvf;->a(Lvl;)V

    .line 519
    :cond_0
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lva;->f:Ljava/lang/String;

    return-object v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 38
    check-cast p1, Lva;

    invoke-virtual {p0, p1}, Lva;->a(Lva;)I

    move-result v0

    return v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    invoke-virtual {p0}, Lva;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Lup;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lva;->m:Lup;

    return-object v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 280
    iget-boolean v0, p0, Lva;->j:Z

    return v0
.end method

.method public g()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 290
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method protected h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 377
    const-string v0, "UTF-8"

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 381
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "application/x-www-form-urlencoded; charset="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lva;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()Lvc;
    .locals 1

    .prologue
    .line 444
    sget-object v0, Lvc;->b:Lvc;

    return-object v0
.end method

.method public k()Lvi;
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lva;->c:Lvi;

    return-object v0
.end method

.method public l()V
    .locals 1

    .prologue
    .line 468
    const/4 v0, 0x1

    iput-boolean v0, p0, Lva;->k:Z

    .line 469
    return-void
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 475
    iget-boolean v0, p0, Lva;->k:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 539
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lva;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 540
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v0, p0, Lva;->j:Z

    if-eqz v0, :cond_0

    const-string v0, "[X] "

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lva;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lva;->j()Lvc;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lva;->a:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "[ ] "

    goto :goto_0
.end method
