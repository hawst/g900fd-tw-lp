.class public final Lacz;
.super Lacy;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field a:Landroid/view/View;

.field final synthetic b:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 225
    iput-object p1, p0, Lacz;->b:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    .line 226
    sget v0, Lf;->fc:I

    sget v1, Lg;->bB:I

    invoke-direct {p0, p1, p2, v0, v1}, Lacy;-><init>(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;Landroid/view/ViewGroup;II)V

    .line 228
    iget-object v0, p0, Lacz;->d:Landroid/view/View;

    sget v1, Lg;->ai:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lacz;->a:Landroid/view/View;

    .line 229
    iget-object v0, p0, Lacz;->a:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 230
    return-void
.end method


# virtual methods
.method a()Z
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 244
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v5

    .line 247
    :try_start_0
    iget-object v0, p0, Lacz;->b:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->e(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)Lyj;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->b(Lyj;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    move-wide v1, v0

    .line 251
    :goto_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v6

    .line 252
    cmp-long v0, v6, v1

    if-gtz v0, :cond_0

    .line 253
    iget-object v0, p0, Lacz;->d:Landroid/view/View;

    sget v6, Lg;->eN:I

    .line 254
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 255
    invoke-virtual {v5, v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(J)Ljava/lang/String;

    move-result-object v6

    .line 256
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    iget-object v0, p0, Lacz;->b:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->g(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)Landroid/os/Handler;

    move-result-object v0

    iget-object v7, p0, Lacz;->b:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-static {v7}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->f(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)Ljava/lang/Runnable;

    move-result-object v7

    .line 258
    invoke-virtual {v5, v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->b(J)J

    move-result-wide v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v1, v8

    .line 257
    invoke-virtual {v0, v7, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 259
    invoke-virtual {v5}, Lcom/google/android/apps/hangouts/phone/EsApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->hV:I

    new-array v2, v3, [Ljava/lang/Object;

    aput-object v6, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 261
    iget-object v0, p0, Lacz;->d:Landroid/view/View;

    sget v2, Lg;->eO:I

    .line 262
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 263
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    move v0, v3

    .line 266
    :goto_1
    return v0

    .line 249
    :catch_0
    move-exception v0

    const-wide/16 v0, 0x0

    move-wide v1, v0

    goto :goto_0

    :cond_0
    move v0, v4

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lacz;->a:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 235
    iget-object v0, p0, Lacz;->b:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->e(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)Lyj;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->c(Lyj;)V

    .line 236
    invoke-virtual {p0}, Lacz;->b()V

    .line 238
    :cond_0
    return-void
.end method
