.class public final Laqu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/hangouts/hangout/multiwaveview/GlowPadView$OnTriggerListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/hangout/IncomingRingActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/hangouts/hangout/IncomingRingActivity;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Laqu;->a:Lcom/google/android/apps/hangouts/hangout/IncomingRingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/hangouts/hangout/IncomingRingActivity;B)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0, p1}, Laqu;-><init>(Lcom/google/android/apps/hangouts/hangout/IncomingRingActivity;)V

    return-void
.end method


# virtual methods
.method public onFinishFinalAnimation()V
    .locals 0

    .prologue
    .line 109
    return-void
.end method

.method public onGrabbed(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 82
    return-void
.end method

.method public onGrabbedStateChange(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 105
    return-void
.end method

.method public onReleased(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 86
    return-void
.end method

.method public onTrigger(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 90
    packed-switch p2, :pswitch_data_0

    .line 98
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unexpected trigger for GlowPadView widget value: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf;->j(Ljava/lang/String;)V

    .line 101
    :goto_0
    return-void

    .line 92
    :pswitch_1
    iget-object v0, p0, Laqu;->a:Lcom/google/android/apps/hangouts/hangout/IncomingRingActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/IncomingRingActivity;->c(Lcom/google/android/apps/hangouts/hangout/IncomingRingActivity;)Lcom/google/android/apps/hangouts/hangout/IncomingRing;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->l()V

    goto :goto_0

    .line 95
    :pswitch_2
    iget-object v0, p0, Laqu;->a:Lcom/google/android/apps/hangouts/hangout/IncomingRingActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/IncomingRingActivity;->c(Lcom/google/android/apps/hangouts/hangout/IncomingRingActivity;)Lcom/google/android/apps/hangouts/hangout/IncomingRing;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->m()V

    goto :goto_0

    .line 90
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
