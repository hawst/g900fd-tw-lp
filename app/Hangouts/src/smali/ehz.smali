.class public final Lehz;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lehz;

.field public static final b:Lepo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepo",
            "<",
            "Lehz;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public c:[Leib;

.field public d:[Leic;

.field public e:[Leic;

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/Boolean;

.field public i:[Leib;

.field public j:Lfab;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lehz;

    sput-object v0, Lehz;->a:[Lehz;

    .line 13
    const v0, 0xba4a86

    new-instance v1, Leia;

    invoke-direct {v1}, Leia;-><init>()V

    .line 14
    invoke-static {v0, v1}, Lepo;->a(ILepp;)Lepo;

    move-result-object v0

    sput-object v0, Lehz;->b:Lepo;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 17
    sget-object v0, Leib;->a:[Leib;

    iput-object v0, p0, Lehz;->c:[Leib;

    .line 20
    sget-object v0, Leic;->a:[Leic;

    iput-object v0, p0, Lehz;->d:[Leic;

    .line 23
    sget-object v0, Leic;->a:[Leic;

    iput-object v0, p0, Lehz;->e:[Leic;

    .line 32
    sget-object v0, Leib;->a:[Leib;

    iput-object v0, p0, Lehz;->i:[Leib;

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lehz;->j:Lfab;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 87
    iget-object v0, p0, Lehz;->c:[Leib;

    if-eqz v0, :cond_1

    .line 88
    iget-object v3, p0, Lehz;->c:[Leib;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 89
    if-eqz v5, :cond_0

    .line 90
    const/4 v6, 0x2

    .line 91
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 88
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 95
    :cond_2
    iget-object v2, p0, Lehz;->f:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    .line 96
    const/4 v2, 0x3

    iget-object v3, p0, Lehz;->f:Ljava/lang/Boolean;

    .line 97
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 99
    :cond_3
    iget-object v2, p0, Lehz;->g:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    .line 100
    const/4 v2, 0x4

    iget-object v3, p0, Lehz;->g:Ljava/lang/Boolean;

    .line 101
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 103
    :cond_4
    iget-object v2, p0, Lehz;->i:[Leib;

    if-eqz v2, :cond_6

    .line 104
    iget-object v3, p0, Lehz;->i:[Leib;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    .line 105
    if-eqz v5, :cond_5

    .line 106
    const/4 v6, 0x5

    .line 107
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 104
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 111
    :cond_6
    iget-object v2, p0, Lehz;->d:[Leic;

    if-eqz v2, :cond_8

    .line 112
    iget-object v3, p0, Lehz;->d:[Leic;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_8

    aget-object v5, v3, v2

    .line 113
    if-eqz v5, :cond_7

    .line 114
    const/4 v6, 0x6

    .line 115
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 112
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 119
    :cond_8
    iget-object v2, p0, Lehz;->h:Ljava/lang/Boolean;

    if-eqz v2, :cond_9

    .line 120
    const/4 v2, 0x7

    iget-object v3, p0, Lehz;->h:Ljava/lang/Boolean;

    .line 121
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 123
    :cond_9
    iget-object v2, p0, Lehz;->j:Lfab;

    if-eqz v2, :cond_a

    .line 124
    const/16 v2, 0x8

    iget-object v3, p0, Lehz;->j:Lfab;

    .line 125
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 127
    :cond_a
    iget-object v2, p0, Lehz;->e:[Leic;

    if-eqz v2, :cond_c

    .line 128
    iget-object v2, p0, Lehz;->e:[Leic;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_c

    aget-object v4, v2, v1

    .line 129
    if-eqz v4, :cond_b

    .line 130
    const/16 v5, 0x9

    .line 131
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 128
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 135
    :cond_c
    iget-object v1, p0, Lehz;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 136
    iput v0, p0, Lehz;->cachedSize:I

    .line 137
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lehz;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lehz;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lehz;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lehz;->c:[Leib;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leib;

    iget-object v3, p0, Lehz;->c:[Leib;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lehz;->c:[Leib;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lehz;->c:[Leib;

    :goto_2
    iget-object v2, p0, Lehz;->c:[Leib;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lehz;->c:[Leib;

    new-instance v3, Leib;

    invoke-direct {v3}, Leib;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lehz;->c:[Leib;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lehz;->c:[Leib;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lehz;->c:[Leib;

    new-instance v3, Leib;

    invoke-direct {v3}, Leib;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lehz;->c:[Leib;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lehz;->f:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lehz;->g:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lehz;->i:[Leib;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Leib;

    iget-object v3, p0, Lehz;->i:[Leib;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lehz;->i:[Leib;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Lehz;->i:[Leib;

    :goto_4
    iget-object v2, p0, Lehz;->i:[Leib;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Lehz;->i:[Leib;

    new-instance v3, Leib;

    invoke-direct {v3}, Leib;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lehz;->i:[Leib;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lehz;->i:[Leib;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lehz;->i:[Leib;

    new-instance v3, Leib;

    invoke-direct {v3}, Leib;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lehz;->i:[Leib;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lehz;->d:[Leic;

    if-nez v0, :cond_9

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Leic;

    iget-object v3, p0, Lehz;->d:[Leic;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lehz;->d:[Leic;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    iput-object v2, p0, Lehz;->d:[Leic;

    :goto_6
    iget-object v2, p0, Lehz;->d:[Leic;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Lehz;->d:[Leic;

    new-instance v3, Leic;

    invoke-direct {v3}, Leic;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lehz;->d:[Leic;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    iget-object v0, p0, Lehz;->d:[Leic;

    array-length v0, v0

    goto :goto_5

    :cond_a
    iget-object v2, p0, Lehz;->d:[Leic;

    new-instance v3, Leic;

    invoke-direct {v3}, Leic;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lehz;->d:[Leic;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lehz;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lehz;->j:Lfab;

    if-nez v0, :cond_b

    new-instance v0, Lfab;

    invoke-direct {v0}, Lfab;-><init>()V

    iput-object v0, p0, Lehz;->j:Lfab;

    :cond_b
    iget-object v0, p0, Lehz;->j:Lfab;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lehz;->e:[Leic;

    if-nez v0, :cond_d

    move v0, v1

    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Leic;

    iget-object v3, p0, Lehz;->e:[Leic;

    if-eqz v3, :cond_c

    iget-object v3, p0, Lehz;->e:[Leic;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_c
    iput-object v2, p0, Lehz;->e:[Leic;

    :goto_8
    iget-object v2, p0, Lehz;->e:[Leic;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_e

    iget-object v2, p0, Lehz;->e:[Leic;

    new-instance v3, Leic;

    invoke-direct {v3}, Leic;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lehz;->e:[Leic;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_d
    iget-object v0, p0, Lehz;->e:[Leic;

    array-length v0, v0

    goto :goto_7

    :cond_e
    iget-object v2, p0, Lehz;->e:[Leic;

    new-instance v3, Leic;

    invoke-direct {v3}, Leic;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lehz;->e:[Leic;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x38 -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 40
    iget-object v1, p0, Lehz;->c:[Leib;

    if-eqz v1, :cond_1

    .line 41
    iget-object v2, p0, Lehz;->c:[Leib;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 42
    if-eqz v4, :cond_0

    .line 43
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 41
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 47
    :cond_1
    iget-object v1, p0, Lehz;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 48
    const/4 v1, 0x3

    iget-object v2, p0, Lehz;->f:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 50
    :cond_2
    iget-object v1, p0, Lehz;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 51
    const/4 v1, 0x4

    iget-object v2, p0, Lehz;->g:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 53
    :cond_3
    iget-object v1, p0, Lehz;->i:[Leib;

    if-eqz v1, :cond_5

    .line 54
    iget-object v2, p0, Lehz;->i:[Leib;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 55
    if-eqz v4, :cond_4

    .line 56
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 54
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 60
    :cond_5
    iget-object v1, p0, Lehz;->d:[Leic;

    if-eqz v1, :cond_7

    .line 61
    iget-object v2, p0, Lehz;->d:[Leic;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 62
    if-eqz v4, :cond_6

    .line 63
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 61
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 67
    :cond_7
    iget-object v1, p0, Lehz;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 68
    const/4 v1, 0x7

    iget-object v2, p0, Lehz;->h:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 70
    :cond_8
    iget-object v1, p0, Lehz;->j:Lfab;

    if-eqz v1, :cond_9

    .line 71
    const/16 v1, 0x8

    iget-object v2, p0, Lehz;->j:Lfab;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 73
    :cond_9
    iget-object v1, p0, Lehz;->e:[Leic;

    if-eqz v1, :cond_b

    .line 74
    iget-object v1, p0, Lehz;->e:[Leic;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_b

    aget-object v3, v1, v0

    .line 75
    if-eqz v3, :cond_a

    .line 76
    const/16 v4, 0x9

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 74
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 80
    :cond_b
    iget-object v0, p0, Lehz;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 82
    return-void
.end method
