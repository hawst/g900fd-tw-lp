.class public final Lasx;
.super Latc;
.source "PG"


# instance fields
.field protected final a:Latd;

.field private final d:Lapk;

.field private final e:Lasz;

.field private final f:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

.field private final g:Ljava/lang/Object;

.field private final h:Laui;

.field private final i:Lauh;

.field private final j:Landroid/graphics/Rect;

.field private final k:Landroid/graphics/Rect;

.field private l:Z

.field private m:Late;

.field private n:Z

.field private o:Z

.field private p:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

.field private q:Landroid/os/Handler;

.field private final r:Landroid/widget/TextView;

.field private final s:Landroid/widget/TextView;

.field private final t:Landroid/widget/ImageView;

.field private final u:Landroid/view/View;

.field private final v:Landroid/view/View;

.field private w:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Latd;Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 118
    invoke-direct {p0}, Latc;-><init>()V

    .line 88
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    iput-object v0, p0, Lasx;->d:Lapk;

    .line 89
    new-instance v0, Lasz;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lasz;-><init>(Lasx;B)V

    iput-object v0, p0, Lasx;->e:Lasz;

    .line 97
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lasx;->j:Landroid/graphics/Rect;

    .line 98
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lasx;->k:Landroid/graphics/Rect;

    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lasx;->l:Z

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lasx;->p:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 106
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lasx;->q:Landroid/os/Handler;

    .line 119
    iput-object p1, p0, Lasx;->a:Latd;

    .line 120
    iput-object p3, p0, Lasx;->g:Ljava/lang/Object;

    .line 121
    iput-object p2, p0, Lasx;->f:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    .line 123
    iget-object v0, p0, Lasx;->a:Latd;

    invoke-virtual {v0}, Latd;->b()Landroid/view/ViewGroup;

    move-result-object v0

    sget v1, Lg;->cm:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lasx;->t:Landroid/widget/ImageView;

    .line 125
    iget-object v0, p0, Lasx;->a:Latd;

    invoke-virtual {v0}, Latd;->b()Landroid/view/ViewGroup;

    move-result-object v0

    sget v1, Lg;->hx:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lasx;->r:Landroid/widget/TextView;

    .line 127
    iget-object v0, p0, Lasx;->a:Latd;

    invoke-virtual {v0}, Latd;->b()Landroid/view/ViewGroup;

    move-result-object v0

    sget v1, Lg;->aX:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lasx;->s:Landroid/widget/TextView;

    .line 130
    iget-object v0, p0, Lasx;->a:Latd;

    .line 131
    invoke-virtual {v0}, Latd;->b()Landroid/view/ViewGroup;

    move-result-object v0

    sget v1, Lg;->cg:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lasx;->u:Landroid/view/View;

    .line 132
    iget-object v0, p0, Lasx;->a:Latd;

    .line 133
    invoke-virtual {v0}, Latd;->b()Landroid/view/ViewGroup;

    move-result-object v0

    sget v1, Lg;->ch:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lasx;->v:Landroid/view/View;

    .line 135
    new-instance v0, Laui;

    const-string v1, "Main video"

    invoke-direct {v0, v1}, Laui;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lasx;->h:Laui;

    .line 136
    new-instance v0, Lauh;

    const-string v1, "Letterbox renderer"

    invoke-direct {v0, v1}, Lauh;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lasx;->i:Lauh;

    .line 137
    return-void
.end method

.method static synthetic a(Lasx;)Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lasx;->f:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    return-object v0
.end method

.method static synthetic a(Lasx;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lasx;->p:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    return-object p1
.end method

.method static synthetic b(Lasx;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lasx;->m()V

    return-void
.end method

.method static synthetic c(Lasx;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lasx;->q:Landroid/os/Handler;

    return-object v0
.end method

.method private l()V
    .locals 12

    .prologue
    const/high16 v10, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 266
    invoke-static {}, Lcwz;->c()V

    .line 267
    iget-object v4, p0, Lasx;->g:Ljava/lang/Object;

    monitor-enter v4

    .line 268
    :try_start_0
    iget v0, p0, Lasx;->b:I

    if-lez v0, :cond_0

    iget v0, p0, Lasx;->c:I

    if-gtz v0, :cond_1

    .line 269
    :cond_0
    iget-object v0, p0, Lasx;->j:Landroid/graphics/Rect;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 270
    monitor-exit v4

    .line 330
    :goto_0
    return-void

    .line 275
    :cond_1
    iget-object v0, p0, Lasx;->h:Laui;

    iget v3, p0, Lasx;->b:I

    iget v5, p0, Lasx;->c:I

    invoke-virtual {v0, v3, v5}, Laui;->a(II)V

    .line 276
    iget-object v0, p0, Lasx;->i:Lauh;

    iget v3, p0, Lasx;->b:I

    iget v5, p0, Lasx;->c:I

    invoke-virtual {v0, v3, v5}, Lauh;->a(II)V

    .line 278
    iget-object v0, p0, Lasx;->m:Late;

    if-eqz v0, :cond_2

    iget v0, p0, Lasx;->b:I

    if-lez v0, :cond_2

    iget v0, p0, Lasx;->c:I

    if-gtz v0, :cond_3

    .line 279
    :cond_2
    iget-object v0, p0, Lasx;->j:Landroid/graphics/Rect;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 280
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 330
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    .line 288
    :cond_3
    :try_start_1
    iget v0, p0, Lasx;->b:I

    int-to-float v0, v0

    iget v3, p0, Lasx;->c:I

    int-to-float v3, v3

    div-float/2addr v0, v3

    .line 289
    iget-object v3, p0, Lasx;->m:Late;

    invoke-virtual {v3}, Late;->s()F

    move-result v3

    .line 290
    iget-object v5, p0, Lasx;->m:Late;

    invoke-virtual {v5}, Late;->t()F

    move-result v5

    .line 291
    iget-object v6, p0, Lasx;->m:Late;

    invoke-virtual {v6}, Late;->u()F

    move-result v6

    .line 292
    cmpg-float v7, v3, v1

    if-lez v7, :cond_4

    invoke-static {v3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 293
    :cond_4
    iget-object v0, p0, Lasx;->j:Landroid/graphics/Rect;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 294
    monitor-exit v4

    goto :goto_0

    .line 302
    :cond_5
    cmpl-float v7, v0, v3

    if-lez v7, :cond_8

    .line 303
    div-float v7, v8, v3

    div-float v0, v8, v0

    sub-float v0, v7, v0

    mul-float/2addr v0, v3

    .line 306
    cmpl-float v7, v0, v10

    if-gtz v7, :cond_6

    iget-boolean v7, p0, Lasx;->l:Z

    if-nez v7, :cond_7

    .line 308
    :cond_6
    iget v0, p0, Lasx;->b:I

    iget v7, p0, Lasx;->c:I

    int-to-float v7, v7

    mul-float/2addr v3, v7

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    move v3, v0

    move v0, v1

    .line 326
    :goto_1
    iget-object v7, p0, Lasx;->h:Laui;

    add-float/2addr v1, v5

    add-float/2addr v0, v6

    invoke-virtual {v7, v1, v0}, Laui;->a(FF)V

    .line 328
    iget-object v0, p0, Lasx;->j:Landroid/graphics/Rect;

    iget v1, p0, Lasx;->b:I

    sub-int/2addr v1, v3

    iget v5, p0, Lasx;->c:I

    sub-int/2addr v5, v2

    invoke-virtual {v0, v3, v2, v1, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 330
    monitor-exit v4

    goto/16 :goto_0

    .line 311
    :cond_7
    div-float/2addr v0, v9

    .line 312
    mul-float v3, v6, v9

    sub-float v3, v8, v3

    mul-float/2addr v0, v3

    move v3, v2

    .line 314
    goto :goto_1

    .line 315
    :cond_8
    sub-float v0, v3, v0

    div-float/2addr v0, v3

    .line 317
    cmpl-float v7, v0, v10

    if-gtz v7, :cond_9

    iget-boolean v7, p0, Lasx;->l:Z

    if-nez v7, :cond_a

    .line 319
    :cond_9
    iget v0, p0, Lasx;->c:I

    iget v7, p0, Lasx;->b:I

    int-to-float v7, v7

    div-float v3, v7, v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v3, v2

    move v2, v0

    move v0, v1

    goto :goto_1

    .line 322
    :cond_a
    div-float/2addr v0, v9

    .line 323
    mul-float v3, v5, v9

    sub-float v3, v8, v3

    mul-float/2addr v0, v3

    move v3, v2

    move v11, v0

    move v0, v1

    move v1, v11

    goto :goto_1
.end method

.method private m()V
    .locals 10

    .prologue
    const-wide/16 v0, 0x0

    const/4 v4, 0x0

    const/4 v9, 0x0

    const/16 v8, 0x8

    .line 449
    iget-object v3, p0, Lasx;->g:Ljava/lang/Object;

    monitor-enter v3

    .line 450
    :try_start_0
    iget-object v2, p0, Lasx;->m:Late;

    if-eqz v2, :cond_b

    .line 451
    iget-object v2, p0, Lasx;->m:Late;

    invoke-virtual {v2}, Late;->l()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v2

    move-object v5, v2

    .line 453
    :goto_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 454
    iget-object v2, p0, Lasx;->d:Lapk;

    invoke-virtual {v2}, Lapk;->f()Z

    move-result v2

    if-nez v2, :cond_0

    instance-of v2, v5, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    if-eqz v2, :cond_9

    .line 456
    :cond_0
    iget-object v2, p0, Lasx;->w:Ljava/lang/Runnable;

    if-nez v2, :cond_1

    .line 457
    new-instance v2, Lasy;

    invoke-direct {v2, p0}, Lasy;-><init>(Lasx;)V

    iput-object v2, p0, Lasx;->w:Ljava/lang/Runnable;

    .line 464
    iget-object v2, p0, Lasx;->q:Landroid/os/Handler;

    iget-object v3, p0, Lasx;->w:Ljava/lang/Runnable;

    const-wide/16 v6, 0x1f4

    invoke-virtual {v2, v3, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 467
    :cond_1
    iget-object v2, p0, Lasx;->m:Late;

    instance-of v2, v2, Latp;

    if-eqz v2, :cond_4

    if-eqz v5, :cond_4

    invoke-virtual {v5}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getConnectionTime()J

    move-result-wide v2

    :goto_1
    cmp-long v6, v2, v0

    if-lez v6, :cond_5

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, v2

    iget-object v2, p0, Lasx;->r:Landroid/widget/TextView;

    const-wide/16 v6, 0x3e8

    div-long v6, v0, v6

    invoke-static {v6, v7}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lasx;->r:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    move-wide v2, v0

    :goto_2
    instance-of v0, v5, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    if-eqz v0, :cond_a

    invoke-virtual {v5}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getAttachedData()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lash;

    if-eqz v0, :cond_a

    invoke-virtual {v5}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getAttachedData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lash;

    iget-object v1, p0, Lasx;->d:Lapk;

    invoke-virtual {v1}, Lapk;->c()Lapx;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lapx;->S()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laqd;

    invoke-virtual {v0}, Lash;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lbzd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v1}, Laqd;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    move-object v0, v1

    :goto_3
    invoke-direct {p0}, Lasx;->n()V

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Laqd;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lasx;->s:Landroid/widget/TextView;

    iget-object v4, p0, Lasx;->a:Latd;

    invoke-virtual {v4}, Latd;->d()Ly;

    move-result-object v4

    invoke-virtual {v4}, Ly;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lh;->cc:I

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Laqd;->b()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lasx;->s:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    instance-of v1, v0, Laqf;

    if-eqz v1, :cond_3

    check-cast v0, Laqf;

    invoke-virtual {v0}, Laqf;->d()I

    move-result v1

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-gez v1, :cond_7

    invoke-virtual {v0}, Laqf;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "Babel"

    const-string v1, "FMF active"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lasx;->u:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lasx;->v:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 476
    :cond_3
    :goto_4
    return-void

    .line 453
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_4
    move-wide v2, v0

    .line 467
    goto/16 :goto_1

    :cond_5
    iget-object v2, p0, Lasx;->r:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    move-wide v2, v0

    goto/16 :goto_2

    :cond_6
    const-string v0, "Babel"

    const-string v1, "FMF ineligible"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lasx;->u:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lasx;->v:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    :cond_7
    const-string v0, "Babel"

    const-string v1, "FMF promo display time over"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lasx;->u:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lasx;->v:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    :cond_8
    iget-object v0, p0, Lasx;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    .line 470
    :cond_9
    iget-object v0, p0, Lasx;->q:Landroid/os/Handler;

    iget-object v1, p0, Lasx;->w:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 471
    iput-object v4, p0, Lasx;->w:Ljava/lang/Runnable;

    .line 472
    iget-object v0, p0, Lasx;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 473
    iget-object v0, p0, Lasx;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 474
    invoke-direct {p0}, Lasx;->n()V

    goto :goto_4

    :cond_a
    move-object v0, v4

    goto/16 :goto_3

    :cond_b
    move-object v5, v4

    goto/16 :goto_0
.end method

.method private n()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 551
    iget-object v0, p0, Lasx;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 552
    iget-object v0, p0, Lasx;->v:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 553
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 142
    invoke-static {}, Lcwz;->a()V

    .line 143
    iget-object v0, p0, Lasx;->d:Lapk;

    iget-object v1, p0, Lasx;->e:Lasz;

    invoke-virtual {v0, v1}, Lapk;->a(Lapo;)V

    .line 146
    return-void
.end method

.method a(Late;)V
    .locals 2

    .prologue
    .line 411
    iget-object v1, p0, Lasx;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 412
    :try_start_0
    iget-object v0, p0, Lasx;->m:Late;

    if-ne p1, v0, :cond_0

    .line 413
    const/4 v0, 0x1

    iput-boolean v0, p0, Lasx;->o:Z

    .line 414
    invoke-direct {p0}, Lasx;->l()V

    .line 416
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public a(Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 229
    if-eqz p1, :cond_0

    .line 230
    iget-object v0, p0, Lasx;->t:Landroid/widget/ImageView;

    iget-object v1, p0, Lasx;->m:Late;

    invoke-virtual {v1}, Late;->g()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 231
    iget-object v0, p0, Lasx;->t:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 232
    iget-object v0, p0, Lasx;->a:Latd;

    invoke-virtual {v0}, Latd;->d()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->av:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 239
    :goto_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_1

    .line 240
    iget-object v1, p0, Lasx;->t:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 244
    :goto_1
    return-void

    .line 235
    :cond_0
    iget-object v1, p0, Lasx;->t:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 236
    iget-object v1, p0, Lasx;->t:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 242
    :cond_1
    iget-object v1, p0, Lasx;->t:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public b()V
    .locals 2

    .prologue
    .line 151
    invoke-static {}, Lcwz;->a()V

    .line 152
    iget-object v0, p0, Lasx;->d:Lapk;

    iget-object v1, p0, Lasx;->e:Lasz;

    invoke-virtual {v0, v1}, Lapk;->b(Lapo;)V

    .line 153
    const/4 v0, 0x0

    iput-object v0, p0, Lasx;->m:Late;

    .line 154
    const/4 v0, 0x0

    iput-boolean v0, p0, Lasx;->o:Z

    .line 155
    return-void
.end method

.method b(Late;)V
    .locals 2

    .prologue
    .line 437
    iget-object v1, p0, Lasx;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 438
    :try_start_0
    iget-object v0, p0, Lasx;->m:Late;

    if-ne p1, v0, :cond_0

    .line 439
    invoke-virtual {p0}, Lasx;->e()V

    .line 441
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method c()V
    .locals 1

    .prologue
    .line 159
    iget-boolean v0, p0, Lasx;->l:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lasx;->l:Z

    .line 160
    return-void

    .line 159
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 5

    .prologue
    .line 164
    invoke-static {}, Lcwz;->a()V

    .line 165
    iget-object v1, p0, Lasx;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 166
    :try_start_0
    iget-object v0, p0, Lasx;->d:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v2

    .line 167
    iget-object v3, p0, Lasx;->m:Late;

    .line 168
    const/4 v0, 0x0

    iput-object v0, p0, Lasx;->m:Late;

    .line 169
    if-eqz v2, :cond_2

    .line 170
    invoke-virtual {v2}, Lapx;->F()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v0

    .line 171
    if-nez v0, :cond_0

    iget-object v4, p0, Lasx;->p:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lasx;->p:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v4}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v4

    if-nez v4, :cond_0

    .line 172
    iget-object v0, p0, Lasx;->p:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 174
    :cond_0
    if-nez v0, :cond_1

    .line 175
    invoke-virtual {v2}, Lapx;->E()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v0

    .line 177
    :cond_1
    if-eqz v0, :cond_2

    .line 178
    iget-object v2, p0, Lasx;->f:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    .line 179
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getEndpointMucJid()Ljava/lang/String;

    move-result-object v0

    .line 178
    invoke-virtual {v2, v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a(Ljava/lang/String;)Late;

    move-result-object v0

    iput-object v0, p0, Lasx;->m:Late;

    .line 182
    :cond_2
    iget-object v0, p0, Lasx;->m:Late;

    if-nez v0, :cond_3

    .line 183
    iget-object v0, p0, Lasx;->f:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->d()Late;

    move-result-object v0

    iput-object v0, p0, Lasx;->m:Late;

    .line 186
    :cond_3
    iget-object v0, p0, Lasx;->d:Lapk;

    invoke-virtual {v0}, Lapk;->f()Z

    move-result v0

    .line 188
    if-eqz v0, :cond_4

    iget-object v0, p0, Lasx;->m:Late;

    instance-of v0, v0, Latu;

    if-eqz v0, :cond_4

    .line 189
    const/4 v0, 0x0

    iput-object v0, p0, Lasx;->m:Late;

    .line 193
    :cond_4
    iget-object v0, p0, Lasx;->m:Late;

    if-eq v0, v3, :cond_7

    .line 194
    if-eqz v3, :cond_5

    .line 195
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Late;->b(Z)V

    .line 197
    :cond_5
    iget-object v0, p0, Lasx;->m:Late;

    if-eqz v0, :cond_6

    .line 198
    iget-object v0, p0, Lasx;->m:Late;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Late;->b(Z)V

    .line 200
    :cond_6
    const/4 v0, 0x1

    iput-boolean v0, p0, Lasx;->o:Z

    .line 201
    iget-object v0, p0, Lasx;->f:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->f()V

    .line 202
    invoke-direct {p0}, Lasx;->m()V

    .line 203
    invoke-virtual {p0}, Lasx;->e()V

    .line 208
    :cond_7
    iget-object v0, p0, Lasx;->f:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->c()V

    .line 209
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lasx;->m:Late;

    if-eqz v0, :cond_1

    .line 219
    iget-object v0, p0, Lasx;->m:Late;

    invoke-virtual {v0}, Late;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lasx;->m:Late;

    instance-of v0, v0, Latp;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lasx;->m:Late;

    .line 221
    invoke-virtual {v0}, Late;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 222
    :goto_0
    invoke-virtual {p0, v0}, Lasx;->a(Z)V

    .line 224
    :cond_1
    return-void

    .line 221
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method f()V
    .locals 0

    .prologue
    .line 335
    invoke-direct {p0}, Lasx;->l()V

    .line 336
    return-void
.end method

.method g()V
    .locals 2

    .prologue
    .line 424
    iget-object v1, p0, Lasx;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 425
    :try_start_0
    iget-object v0, p0, Lasx;->m:Late;

    instance-of v0, v0, Latu;

    if-eqz v0, :cond_0

    .line 426
    const/4 v0, 0x1

    iput-boolean v0, p0, Lasx;->o:Z

    .line 428
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 346
    iget-boolean v0, p0, Lasx;->n:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lasx;->h:Laui;

    invoke-virtual {v0}, Laui;->a()V

    iget-object v0, p0, Lasx;->i:Lauh;

    invoke-virtual {v0}, Lauh;->a()V

    iput-boolean v1, p0, Lasx;->n:Z

    iput-boolean v1, p0, Lasx;->o:Z

    :cond_0
    iget-boolean v0, p0, Lasx;->o:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lasx;->m:Late;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lasx;->m:Late;

    invoke-virtual {v0}, Late;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lasx;->m:Late;

    invoke-virtual {v0}, Late;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lasx;->h:Laui;

    iget-object v1, p0, Lasx;->m:Late;

    invoke-virtual {v1}, Late;->m()I

    move-result v1

    iget-object v2, p0, Lasx;->m:Late;

    invoke-virtual {v2}, Late;->o()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Laui;->a(IZ)V

    iget-object v0, p0, Lasx;->h:Laui;

    iget-object v1, p0, Lasx;->m:Late;

    invoke-virtual {v1}, Late;->v()Z

    move-result v1

    invoke-virtual {v0, v1}, Laui;->a(Z)V

    iput-boolean v4, p0, Lasx;->o:Z

    :cond_1
    iget-object v0, p0, Lasx;->m:Late;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lasx;->m:Late;

    invoke-virtual {v0}, Late;->c()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lasx;->j:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lasx;->m:Late;

    invoke-virtual {v0}, Late;->n()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lasx;->j:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    if-lez v0, :cond_2

    iget-object v0, p0, Lasx;->k:Landroid/graphics/Rect;

    iget-object v1, p0, Lasx;->j:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lasx;->c:I

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lasx;->i:Lauh;

    iget-object v1, p0, Lasx;->k:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lauh;->a(Landroid/graphics/Rect;)V

    :cond_2
    iget-object v0, p0, Lasx;->j:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget v1, p0, Lasx;->b:I

    if-ge v0, v1, :cond_3

    iget-object v0, p0, Lasx;->k:Landroid/graphics/Rect;

    iget-object v1, p0, Lasx;->j:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lasx;->b:I

    iget v3, p0, Lasx;->c:I

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lasx;->i:Lauh;

    iget-object v1, p0, Lasx;->k:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lauh;->a(Landroid/graphics/Rect;)V

    :cond_3
    iget-object v0, p0, Lasx;->j:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-lez v0, :cond_4

    iget-object v0, p0, Lasx;->k:Landroid/graphics/Rect;

    iget-object v1, p0, Lasx;->j:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lasx;->j:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lasx;->j:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lasx;->i:Lauh;

    iget-object v1, p0, Lasx;->k:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lauh;->a(Landroid/graphics/Rect;)V

    :cond_4
    iget-object v0, p0, Lasx;->j:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget v1, p0, Lasx;->c:I

    if-ge v0, v1, :cond_5

    iget-object v0, p0, Lasx;->k:Landroid/graphics/Rect;

    iget-object v1, p0, Lasx;->j:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lasx;->j:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lasx;->j:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget v4, p0, Lasx;->c:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lasx;->i:Lauh;

    iget-object v1, p0, Lasx;->k:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lauh;->a(Landroid/graphics/Rect;)V

    :cond_5
    iget-object v0, p0, Lasx;->h:Laui;

    iget-object v1, p0, Lasx;->j:Landroid/graphics/Rect;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2}, Laui;->a(Landroid/graphics/Rect;F)V

    .line 347
    :cond_6
    :goto_0
    return-void

    .line 346
    :cond_7
    iget v0, p0, Lasx;->c:I

    if-eqz v0, :cond_6

    iget v0, p0, Lasx;->b:I

    if-eqz v0, :cond_6

    iget-object v0, p0, Lasx;->k:Landroid/graphics/Rect;

    iget v1, p0, Lasx;->b:I

    iget v2, p0, Lasx;->c:I

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lasx;->i:Lauh;

    iget-object v1, p0, Lasx;->k:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lauh;->a(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 0

    .prologue
    .line 260
    invoke-super {p0, p1, p2, p3}, Latc;->onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 261
    invoke-direct {p0}, Lasx;->l()V

    .line 262
    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 1

    .prologue
    .line 254
    const/4 v0, 0x0

    iput-boolean v0, p0, Lasx;->n:Z

    .line 255
    return-void
.end method
