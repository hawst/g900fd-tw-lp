.class public final Ldmd;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldmd;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7734
    const/4 v0, 0x0

    new-array v0, v0, [Ldmd;

    sput-object v0, Ldmd;->a:[Ldmd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7735
    invoke-direct {p0}, Lepn;-><init>()V

    .line 7746
    iput-object v0, p0, Ldmd;->f:Ljava/lang/Integer;

    .line 7749
    iput-object v0, p0, Ldmd;->g:Ljava/lang/Integer;

    .line 7735
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 7778
    const/4 v0, 0x0

    .line 7779
    iget-object v1, p0, Ldmd;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 7780
    const/4 v0, 0x1

    iget-object v1, p0, Ldmd;->b:Ljava/lang/String;

    .line 7781
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7783
    :cond_0
    iget-object v1, p0, Ldmd;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 7784
    const/4 v1, 0x2

    iget-object v2, p0, Ldmd;->c:Ljava/lang/String;

    .line 7785
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7787
    :cond_1
    iget-object v1, p0, Ldmd;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 7788
    const/4 v1, 0x3

    iget-object v2, p0, Ldmd;->d:Ljava/lang/String;

    .line 7789
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7791
    :cond_2
    iget-object v1, p0, Ldmd;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 7792
    const/4 v1, 0x4

    iget-object v2, p0, Ldmd;->e:Ljava/lang/String;

    .line 7793
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7795
    :cond_3
    iget-object v1, p0, Ldmd;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 7796
    const/4 v1, 0x5

    iget-object v2, p0, Ldmd;->f:Ljava/lang/Integer;

    .line 7797
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7799
    :cond_4
    iget-object v1, p0, Ldmd;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 7800
    const/4 v1, 0x6

    iget-object v2, p0, Ldmd;->g:Ljava/lang/Integer;

    .line 7801
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7803
    :cond_5
    iget-object v1, p0, Ldmd;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7804
    iput v0, p0, Ldmd;->cachedSize:I

    .line 7805
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 7731
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldmd;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldmd;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldmd;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldmd;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldmd;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldmd;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldmd;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmd;->f:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmd;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-ne v0, v4, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmd;->g:Ljava/lang/Integer;

    goto :goto_0

    :cond_5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmd;->g:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 7754
    iget-object v0, p0, Ldmd;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 7755
    const/4 v0, 0x1

    iget-object v1, p0, Ldmd;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 7757
    :cond_0
    iget-object v0, p0, Ldmd;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 7758
    const/4 v0, 0x2

    iget-object v1, p0, Ldmd;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 7760
    :cond_1
    iget-object v0, p0, Ldmd;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 7761
    const/4 v0, 0x3

    iget-object v1, p0, Ldmd;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 7763
    :cond_2
    iget-object v0, p0, Ldmd;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 7764
    const/4 v0, 0x4

    iget-object v1, p0, Ldmd;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 7766
    :cond_3
    iget-object v0, p0, Ldmd;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 7767
    const/4 v0, 0x5

    iget-object v1, p0, Ldmd;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 7769
    :cond_4
    iget-object v0, p0, Ldmd;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 7770
    const/4 v0, 0x6

    iget-object v1, p0, Ldmd;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 7772
    :cond_5
    iget-object v0, p0, Ldmd;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 7774
    return-void
.end method
