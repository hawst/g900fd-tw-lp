.class public final Lexg;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lexg;


# instance fields
.field public b:[Lexf;

.field public c:[Lexf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    new-array v0, v0, [Lexg;

    sput-object v0, Lexg;->a:[Lexg;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0}, Lepn;-><init>()V

    .line 111
    sget-object v0, Lexf;->a:[Lexf;

    iput-object v0, p0, Lexg;->b:[Lexf;

    .line 114
    sget-object v0, Lexf;->a:[Lexf;

    iput-object v0, p0, Lexg;->c:[Lexf;

    .line 108
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 140
    iget-object v0, p0, Lexg;->b:[Lexf;

    if-eqz v0, :cond_1

    .line 141
    iget-object v3, p0, Lexg;->b:[Lexf;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 142
    if-eqz v5, :cond_0

    .line 143
    const/4 v6, 0x1

    .line 144
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 141
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 148
    :cond_2
    iget-object v2, p0, Lexg;->c:[Lexf;

    if-eqz v2, :cond_4

    .line 149
    iget-object v2, p0, Lexg;->c:[Lexf;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 150
    if-eqz v4, :cond_3

    .line 151
    const/4 v5, 0x2

    .line 152
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 149
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 156
    :cond_4
    iget-object v1, p0, Lexg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 157
    iput v0, p0, Lexg;->cachedSize:I

    .line 158
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 104
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lexg;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lexg;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lexg;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lexg;->b:[Lexf;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lexf;

    iget-object v3, p0, Lexg;->b:[Lexf;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lexg;->b:[Lexf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lexg;->b:[Lexf;

    :goto_2
    iget-object v2, p0, Lexg;->b:[Lexf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lexg;->b:[Lexf;

    new-instance v3, Lexf;

    invoke-direct {v3}, Lexf;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lexg;->b:[Lexf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lexg;->b:[Lexf;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lexg;->b:[Lexf;

    new-instance v3, Lexf;

    invoke-direct {v3}, Lexf;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lexg;->b:[Lexf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lexg;->c:[Lexf;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lexf;

    iget-object v3, p0, Lexg;->c:[Lexf;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lexg;->c:[Lexf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Lexg;->c:[Lexf;

    :goto_4
    iget-object v2, p0, Lexg;->c:[Lexf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Lexg;->c:[Lexf;

    new-instance v3, Lexf;

    invoke-direct {v3}, Lexf;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lexg;->c:[Lexf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lexg;->c:[Lexf;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lexg;->c:[Lexf;

    new-instance v3, Lexf;

    invoke-direct {v3}, Lexf;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lexg;->c:[Lexf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 119
    iget-object v1, p0, Lexg;->b:[Lexf;

    if-eqz v1, :cond_1

    .line 120
    iget-object v2, p0, Lexg;->b:[Lexf;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 121
    if-eqz v4, :cond_0

    .line 122
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 120
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 126
    :cond_1
    iget-object v1, p0, Lexg;->c:[Lexf;

    if-eqz v1, :cond_3

    .line 127
    iget-object v1, p0, Lexg;->c:[Lexf;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 128
    if-eqz v3, :cond_2

    .line 129
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 127
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 133
    :cond_3
    iget-object v0, p0, Lexg;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 135
    return-void
.end method
