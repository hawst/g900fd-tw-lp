.class final Lafd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lafc;


# direct methods
.method constructor <init>(Lafc;)V
    .locals 0

    .prologue
    .line 3383
    iput-object p1, p0, Lafd;->a:Lafc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 3386
    iget-object v0, p0, Lafd;->a:Lafc;

    iget-object v0, v0, Lafc;->c:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->isHidden()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lafd;->a:Lafc;

    iget-object v0, v0, Lafc;->b:Lyj;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lafd;->a:Lafc;

    iget-object v0, v0, Lafc;->b:Lyj;

    iget-object v1, p0, Lafd;->a:Lafc;

    iget-object v1, v1, Lafc;->c:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 3388
    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lyj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lafd;->a:Lafc;

    iget-object v0, v0, Lafc;->c:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 3389
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->F()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lafd;->a:Lafc;

    iget-object v0, v0, Lafc;->c:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 3390
    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Les;

    move-result-object v0

    iget-object v1, p0, Lafd;->a:Lafc;

    iget-object v1, v1, Lafc;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Les;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 3391
    :goto_0
    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aa()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3392
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "should send updateConversationWatermark? "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lafd;->a:Lafc;

    iget-object v3, v3, Lafc;->c:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 3395
    invoke-virtual {v3}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->isHidden()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lafd;->a:Lafc;

    iget-object v3, v3, Lafc;->c:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 3396
    invoke-virtual {v3}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->F()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lafd;->a:Lafc;

    iget-object v3, v3, Lafc;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lafd;->a:Lafc;

    iget-object v3, v3, Lafc;->c:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 3398
    invoke-static {v3}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->k(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3392
    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3401
    :cond_0
    if-eqz v0, :cond_1

    .line 3402
    iget-object v0, p0, Lafd;->a:Lafc;

    iget-object v0, v0, Lafc;->c:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->I(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    .line 3403
    iget-object v0, p0, Lafd;->a:Lafc;

    iget-object v0, v0, Lafc;->c:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->u(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    .line 3405
    :cond_1
    iget-object v0, p0, Lafd;->a:Lafc;

    iget-object v0, v0, Lafc;->c:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->J(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Z

    .line 3406
    return-void

    .line 3390
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
