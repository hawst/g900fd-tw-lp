.class public final Ldwp;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldwp;


# instance fields
.field public b:Ldvm;

.field public c:Ldqf;

.field public d:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8768
    const/4 v0, 0x0

    new-array v0, v0, [Ldwp;

    sput-object v0, Ldwp;->a:[Ldwp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8769
    invoke-direct {p0}, Lepn;-><init>()V

    .line 8772
    iput-object v0, p0, Ldwp;->b:Ldvm;

    .line 8775
    iput-object v0, p0, Ldwp;->c:Ldqf;

    .line 8778
    iput-object v0, p0, Ldwp;->d:Ljava/lang/Integer;

    .line 8769
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 8798
    const/4 v0, 0x0

    .line 8799
    iget-object v1, p0, Ldwp;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 8800
    const/4 v0, 0x1

    iget-object v1, p0, Ldwp;->b:Ldvm;

    .line 8801
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8803
    :cond_0
    iget-object v1, p0, Ldwp;->c:Ldqf;

    if-eqz v1, :cond_1

    .line 8804
    const/4 v1, 0x2

    iget-object v2, p0, Ldwp;->c:Ldqf;

    .line 8805
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8807
    :cond_1
    iget-object v1, p0, Ldwp;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 8808
    const/4 v1, 0x3

    iget-object v2, p0, Ldwp;->d:Ljava/lang/Integer;

    .line 8809
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8811
    :cond_2
    iget-object v1, p0, Ldwp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8812
    iput v0, p0, Ldwp;->cachedSize:I

    .line 8813
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 8765
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldwp;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldwp;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldwp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldwp;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldwp;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldwp;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldwp;->c:Ldqf;

    if-nez v0, :cond_3

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldwp;->c:Ldqf;

    :cond_3
    iget-object v0, p0, Ldwp;->c:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwp;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwp;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 8783
    iget-object v0, p0, Ldwp;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 8784
    const/4 v0, 0x1

    iget-object v1, p0, Ldwp;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 8786
    :cond_0
    iget-object v0, p0, Ldwp;->c:Ldqf;

    if-eqz v0, :cond_1

    .line 8787
    const/4 v0, 0x2

    iget-object v1, p0, Ldwp;->c:Ldqf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 8789
    :cond_1
    iget-object v0, p0, Ldwp;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 8790
    const/4 v0, 0x3

    iget-object v1, p0, Ldwp;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 8792
    :cond_2
    iget-object v0, p0, Ldwp;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 8794
    return-void
.end method
