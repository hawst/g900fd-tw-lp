.class public abstract Lcct;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Laac;
.implements Lcde;


# static fields
.field public static final c:Z

.field private static i:I


# instance fields
.field private a:Lzx;

.field private b:Lbzn;

.field private d:Lccm;

.field private final e:Landroid/widget/ImageView;

.field private final f:Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;

.field private final g:Landroid/widget/ImageView;

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lbys;->s:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcct;->c:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 59
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    sget v0, Lcct;->i:I

    if-nez v0, :cond_0

    .line 62
    invoke-virtual {p0}, Lcct;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->cP:I

    .line 63
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcct;->i:I

    .line 66
    :cond_0
    invoke-virtual {p0, v3}, Lcct;->setOrientation(I)V

    .line 67
    sget v0, Lf;->fR:I

    invoke-static {p1, v0, p0}, Lcct;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 68
    sget v0, Lg;->dy:I

    invoke-virtual {p0, v0}, Lcct;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcct;->e:Landroid/widget/ImageView;

    .line 69
    sget v0, Lg;->dD:I

    invoke-virtual {p0, v0}, Lcct;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;

    iput-object v0, p0, Lcct;->f:Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;

    .line 70
    sget v0, Lg;->dC:I

    invoke-virtual {p0, v0}, Lcct;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcct;->g:Landroid/widget/ImageView;

    .line 71
    invoke-virtual {p0}, Lcct;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lcct;->g:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->bO:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 73
    iget-object v0, p0, Lcct;->g:Landroid/widget/ImageView;

    .line 74
    invoke-virtual {p0}, Lcct;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lh;->iG:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 73
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 76
    :cond_1
    invoke-virtual {p0, v3}, Lcct;->setLongClickable(Z)V

    .line 77
    return-void
.end method


# virtual methods
.method protected a(I)V
    .locals 1

    .prologue
    .line 289
    invoke-virtual {p0}, Lcct;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 290
    invoke-virtual {p0, v0}, Lcct;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 291
    return-void
.end method

.method public a(Lbzn;Lbyd;ZLzx;Z)V
    .locals 8

    .prologue
    const/4 v7, -0x2

    const/4 v6, 0x0

    const/4 v1, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 150
    sget-boolean v0, Lcct;->c:Z

    if-eqz v0, :cond_0

    .line 151
    const-string v3, "Babel_medialoader"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "MediaThumbnailView: setImageBitmap "

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_2

    move-object v0, v1

    .line 152
    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "gifImage="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez p2, :cond_3

    move-object v0, v1

    .line 153
    :goto_1
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " success="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " loadedFromCache="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 151
    invoke-static {v3, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    :cond_0
    iget-object v0, p0, Lcct;->a:Lzx;

    if-eq v0, p4, :cond_4

    .line 159
    if-eqz p1, :cond_1

    .line 160
    invoke-virtual {p1}, Lbzn;->b()V

    .line 203
    :cond_1
    :goto_2
    return-void

    .line 152
    :cond_2
    invoke-virtual {p1}, Lbzn;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 153
    :cond_3
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 166
    :cond_4
    iput-object v1, p0, Lcct;->a:Lzx;

    .line 168
    if-nez p5, :cond_6

    const/4 v0, 0x1

    :goto_3
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v1, v3, :cond_8

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcct;->f:Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->setAlpha(F)V

    iget-object v0, p0, Lcct;->f:Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v3, 0xfa

    invoke-virtual {v0, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    :goto_4
    iget-object v0, p0, Lcct;->e:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcct;->f:Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->setVisibility(I)V

    .line 169
    invoke-virtual {p0}, Lcct;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 170
    iget-object v0, p0, Lcct;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 172
    :cond_5
    if-eqz p3, :cond_b

    .line 173
    if-eqz p2, :cond_a

    .line 174
    new-instance v0, Lccm;

    invoke-direct {v0, p2}, Lccm;-><init>(Lbyd;)V

    iput-object v0, p0, Lcct;->d:Lccm;

    .line 175
    iget-object v0, p0, Lcct;->f:Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;

    iget-object v1, p0, Lcct;->d:Lccm;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 176
    iget-object v0, p0, Lcct;->d:Lccm;

    invoke-virtual {v0}, Lccm;->a()V

    .line 182
    :goto_5
    iget-object v0, p0, Lcct;->f:Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->requestLayout()V

    goto :goto_2

    :cond_6
    move v0, v2

    .line 168
    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcct;->f:Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->setAlpha(F)V

    goto :goto_4

    :cond_8
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v6, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    if-eqz v0, :cond_9

    const-wide/16 v0, 0xfa

    :goto_6
    invoke-virtual {v3, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcct;->f:Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_4

    :cond_9
    const-wide/16 v0, 0x0

    goto :goto_6

    .line 178
    :cond_a
    iget-object v0, p0, Lcct;->b:Lbzn;

    invoke-static {v0}, Lcwz;->a(Ljava/lang/Object;)V

    .line 179
    iput-object p1, p0, Lcct;->b:Lbzn;

    .line 180
    iget-object v0, p0, Lcct;->f:Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;

    iget-object v1, p0, Lcct;->b:Lbzn;

    invoke-virtual {v1}, Lbzn;->e()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_5

    .line 184
    :cond_b
    invoke-virtual {p0}, Lcct;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 186
    invoke-virtual {p0}, Lcct;->a()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 187
    iget-object v1, p0, Lcct;->f:Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;

    sget v2, Lf;->cG:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->setBackgroundColor(I)V

    goto/16 :goto_2

    .line 190
    :cond_c
    iget-object v1, p0, Lcct;->f:Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;

    sget v3, Lf;->cH:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->setBackgroundColor(I)V

    .line 191
    iget-object v1, p0, Lcct;->g:Landroid/widget/ImageView;

    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->bc:I

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 192
    iget-object v1, p0, Lcct;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 194
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcct;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 195
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v7, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 197
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 198
    sget v3, Lh;->gJ:I

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 199
    sget v3, Lf;->bW:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 200
    invoke-virtual {p0, v1, v2}, Lcct;->addView(Landroid/view/View;I)V

    goto/16 :goto_2
.end method

.method protected a(Lyj;ZLjava/lang/String;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 101
    iput-boolean p2, p0, Lcct;->h:Z

    .line 103
    new-instance v0, Lbyq;

    invoke-direct {v0, p3, p1}, Lbyq;-><init>(Ljava/lang/String;Lyj;)V

    sget v1, Lcct;->i:I

    .line 104
    invoke-virtual {v0, v1}, Lbyq;->a(I)Lbyq;

    move-result-object v0

    invoke-virtual {v0, v7}, Lbyq;->a(Z)Lbyq;

    move-result-object v0

    .line 105
    invoke-virtual {p0}, Lcct;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lbyq;->c(Z)Lbyq;

    move-result-object v0

    invoke-virtual {v0, v7}, Lbyq;->d(Z)Lbyq;

    move-result-object v1

    .line 106
    new-instance v0, Lzx;

    move-object v2, p0

    move v5, v4

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lzx;-><init>(Lbyq;Laac;Ljava/lang/String;ZZLjava/lang/Object;)V

    iput-object v0, p0, Lcct;->a:Lzx;

    .line 112
    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    iget-object v1, p0, Lcct;->a:Lzx;

    iget-boolean v2, p0, Lcct;->h:Z

    invoke-virtual {v0, v1, v2}, Lbsn;->a(Lbrv;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    iput-object v3, p0, Lcct;->a:Lzx;

    .line 120
    :goto_0
    return-void

    .line 117
    :cond_0
    new-instance v1, Lcca;

    invoke-virtual {p0}, Lcct;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lakn;

    invoke-direct {v1, v0}, Lcca;-><init>(Lakn;)V

    iget-object v0, p0, Lcct;->e:Landroid/widget/ImageView;

    invoke-virtual {v1}, Lcca;->a()Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcct;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcct;->f:Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected a(Lyj;ZLjava/lang/String;II)V
    .locals 1

    .prologue
    .line 93
    if-lez p4, :cond_0

    if-lez p5, :cond_0

    .line 94
    iget-object v0, p0, Lcct;->f:Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;

    invoke-virtual {v0, p4, p5}, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->a(II)V

    .line 96
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcct;->a(Lyj;ZLjava/lang/String;)V

    .line 97
    return-void
.end method

.method protected a()Z
    .locals 1

    .prologue
    .line 281
    const/4 v0, 0x1

    return v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 123
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    sget v0, Lg;->w:I

    invoke-virtual {p0, v0}, Lcct;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 126
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 129
    :cond_0
    return-void
.end method

.method protected b()Z
    .locals 1

    .prologue
    .line 285
    const/4 v0, 0x0

    return v0
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 263
    iget-object v0, p0, Lcct;->a:Lzx;

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcct;->a:Lzx;

    invoke-virtual {v0}, Lzx;->b()V

    .line 265
    iput-object v1, p0, Lcct;->a:Lzx;

    .line 267
    :cond_0
    iget-object v0, p0, Lcct;->f:Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 268
    iget-object v0, p0, Lcct;->b:Lbzn;

    if-eqz v0, :cond_1

    .line 269
    iget-object v0, p0, Lcct;->b:Lbzn;

    invoke-virtual {v0}, Lbzn;->b()V

    .line 270
    iput-object v1, p0, Lcct;->b:Lbzn;

    .line 272
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcct;->h:Z

    .line 274
    iget-object v0, p0, Lcct;->d:Lccm;

    if-eqz v0, :cond_2

    .line 275
    iget-object v0, p0, Lcct;->d:Lccm;

    invoke-virtual {v0}, Lccm;->c()V

    .line 276
    iput-object v1, p0, Lcct;->d:Lccm;

    .line 278
    :cond_2
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcct;->d:Lccm;

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcct;->d:Lccm;

    invoke-virtual {v0}, Lccm;->a()V

    .line 256
    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcct;->d:Lccm;

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcct;->d:Lccm;

    invoke-virtual {v0}, Lccm;->b()V

    .line 249
    :cond_0
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 137
    iget-boolean v0, p0, Lcct;->h:Z

    if-eqz v0, :cond_0

    .line 138
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcct;->h:Z

    .line 139
    iget-object v0, p0, Lcct;->a:Lzx;

    if-eqz v0, :cond_0

    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    iget-object v1, p0, Lcct;->a:Lzx;

    invoke-virtual {v0, v1}, Lbsn;->a(Lbrv;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    const/4 v0, 0x0

    iput-object v0, p0, Lcct;->a:Lzx;

    .line 144
    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcct;->d:Lccm;

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcct;->d:Lccm;

    invoke-virtual {v0}, Lccm;->a()V

    .line 309
    :cond_0
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 310
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcct;->d:Lccm;

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcct;->d:Lccm;

    invoke-virtual {v0}, Lccm;->b()V

    .line 301
    :cond_0
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 302
    return-void
.end method
