.class public Lbiw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:[I

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lesc;Lern;)V
    .locals 7

    .prologue
    .line 712
    iget-object v2, p2, Lern;->f:Ljava/lang/String;

    iget-object v3, p2, Lern;->g:Ljava/lang/String;

    iget-object v0, p2, Lern;->p:Lepu;

    .line 713
    invoke-static {v0}, Lbiw;->a(Lepu;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p2, Lern;->d:Ljava/lang/String;

    const-string v6, "hangouts/*"

    move-object v0, p0

    move-object v1, p1

    .line 712
    invoke-direct/range {v0 .. v6}, Lbiw;-><init>(Lesc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 715
    return-void
.end method

.method public constructor <init>(Lesc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 699
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 700
    iget-object v2, p1, Lesc;->b:Lepu;

    .line 701
    iget-object v0, v2, Lepu;->b:[I

    iput-object v0, p0, Lbiw;->a:[I

    .line 702
    iget-object v0, v2, Lepu;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v2, Lepu;->c:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lbiw;->b:Ljava/lang/String;

    .line 703
    iget-object v0, v2, Lepu;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, v2, Lepu;->d:Ljava/lang/String;

    :goto_1
    iput-object v0, p0, Lbiw;->c:Ljava/lang/String;

    .line 704
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    iput-object p2, p0, Lbiw;->d:Ljava/lang/String;

    .line 705
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    :goto_3
    iput-object p3, p0, Lbiw;->e:Ljava/lang/String;

    .line 706
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    :goto_4
    iput-object p4, p0, Lbiw;->f:Ljava/lang/String;

    .line 707
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    :goto_5
    iput-object p5, p0, Lbiw;->g:Ljava/lang/String;

    .line 708
    iput-object p6, p0, Lbiw;->h:Ljava/lang/String;

    .line 709
    return-void

    :cond_0
    move-object v0, v1

    .line 702
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 703
    goto :goto_1

    :cond_2
    move-object p2, v1

    .line 704
    goto :goto_2

    :cond_3
    move-object p3, v1

    .line 705
    goto :goto_3

    :cond_4
    move-object p4, v1

    .line 706
    goto :goto_4

    :cond_5
    move-object p5, v1

    .line 707
    goto :goto_5
.end method

.method protected constructor <init>([ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 730
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 731
    iput-object p1, p0, Lbiw;->a:[I

    .line 732
    iput-object v0, p0, Lbiw;->b:Ljava/lang/String;

    .line 733
    iput-object v0, p0, Lbiw;->c:Ljava/lang/String;

    .line 734
    iput-object p2, p0, Lbiw;->d:Ljava/lang/String;

    .line 735
    iput-object v0, p0, Lbiw;->e:Ljava/lang/String;

    .line 736
    iput-object p3, p0, Lbiw;->f:Ljava/lang/String;

    .line 737
    iput-object p4, p0, Lbiw;->g:Ljava/lang/String;

    .line 738
    iput-object p5, p0, Lbiw;->h:Ljava/lang/String;

    .line 739
    return-void
.end method

.method protected static a(Lepu;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 718
    if-eqz p0, :cond_0

    .line 719
    sget-object v0, Leqn;->b:Lepo;

    .line 720
    invoke-virtual {p0, v0}, Lepu;->getExtension(Lepo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leqn;

    .line 721
    if-eqz v0, :cond_0

    .line 722
    iget-object v0, v0, Leqn;->d:Ljava/lang/String;

    .line 725
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a([Lesc;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lesc;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbiw;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 643
    array-length v0, p0

    if-lez v0, :cond_6

    .line 644
    array-length v6, p0

    move v5, v3

    move-object v1, v4

    :goto_0
    if-ge v5, v6, :cond_7

    aget-object v7, p0, v5

    .line 645
    iget-object v8, v7, Lesc;->b:Lepu;

    move v2, v3

    :goto_1
    iget-object v0, v8, Lepu;->b:[I

    array-length v0, v0

    if-ge v2, v0, :cond_5

    iget-object v0, v8, Lepu;->b:[I

    aget v0, v0, v2

    const/16 v9, 0xf9

    if-ne v0, v9, :cond_0

    sget-object v0, Lerj;->b:Lepo;

    invoke-virtual {v8, v0}, Lepu;->getExtension(Lepo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lerj;

    if-eqz v0, :cond_4

    new-instance v2, Lbiy;

    invoke-direct {v2, v7, v0}, Lbiy;-><init>(Lesc;Lerj;)V

    .line 647
    :goto_2
    if-eqz v2, :cond_9

    .line 648
    if-nez v1, :cond_8

    .line 649
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 651
    :goto_3
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 644
    :goto_4
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move-object v1, v0

    goto :goto_0

    .line 645
    :cond_0
    const/16 v9, 0x154

    if-ne v0, v9, :cond_1

    sget-object v0, Leqs;->b:Lepo;

    invoke-virtual {v8, v0}, Lepu;->getExtension(Lepo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leqs;

    if-eqz v0, :cond_4

    iget-object v9, v0, Leqs;->p:Lepu;

    if-eqz v9, :cond_4

    new-instance v2, Lbix;

    invoke-direct {v2, v7, v0}, Lbix;-><init>(Lesc;Leqs;)V

    goto :goto_2

    :cond_1
    const/16 v9, 0x153

    if-ne v0, v9, :cond_2

    sget-object v0, Leqn;->b:Lepo;

    invoke-virtual {v8, v0}, Lepu;->getExtension(Lepo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leqn;

    if-eqz v0, :cond_4

    new-instance v2, Lbiy;

    invoke-direct {v2, v7, v0}, Lbiy;-><init>(Lesc;Leqn;)V

    goto :goto_2

    :cond_2
    const/16 v9, 0x1b6

    if-ne v0, v9, :cond_3

    sget-object v0, Leqv;->b:Lepo;

    invoke-virtual {v8, v0}, Lepu;->getExtension(Lepo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leqv;

    if-eqz v0, :cond_4

    iget-object v9, v0, Leqv;->R:Ljava/lang/String;

    if-eqz v9, :cond_4

    new-instance v2, Lbja;

    invoke-direct {v2, v7, v0}, Lbja;-><init>(Lesc;Leqv;)V

    goto :goto_2

    :cond_3
    const/16 v9, 0x14f

    if-ne v0, v9, :cond_4

    sget-object v0, Lern;->b:Lepo;

    invoke-virtual {v8, v0}, Lepu;->getExtension(Lepo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lern;

    if-eqz v0, :cond_4

    new-instance v2, Lbiw;

    invoke-direct {v2, v7, v0}, Lbiw;-><init>(Lesc;Lern;)V

    goto :goto_2

    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    :cond_5
    const-string v0, "Babel"

    const-string v2, "Received invalid attachment"

    invoke-static {v0, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v4

    goto :goto_2

    :cond_6
    move-object v1, v4

    .line 655
    :cond_7
    return-object v1

    :cond_8
    move-object v0, v1

    goto :goto_3

    :cond_9
    move-object v0, v1

    goto :goto_4
.end method
