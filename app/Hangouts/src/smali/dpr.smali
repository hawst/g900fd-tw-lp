.class public final Ldpr;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldpr;


# instance fields
.field public b:[Lesg;

.field public c:Ldtr;

.field public d:Ldtq;

.field public e:Ldsc;

.field public f:Ldxb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4649
    const/4 v0, 0x0

    new-array v0, v0, [Ldpr;

    sput-object v0, Ldpr;->a:[Ldpr;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4650
    invoke-direct {p0}, Lepn;-><init>()V

    .line 4653
    sget-object v0, Lesg;->a:[Lesg;

    iput-object v0, p0, Ldpr;->b:[Lesg;

    .line 4656
    iput-object v1, p0, Ldpr;->c:Ldtr;

    .line 4659
    iput-object v1, p0, Ldpr;->d:Ldtq;

    .line 4662
    iput-object v1, p0, Ldpr;->e:Ldsc;

    .line 4665
    iput-object v1, p0, Ldpr;->f:Ldxb;

    .line 4650
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 4696
    iget-object v1, p0, Ldpr;->b:[Lesg;

    if-eqz v1, :cond_1

    .line 4697
    iget-object v2, p0, Ldpr;->b:[Lesg;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 4698
    if-eqz v4, :cond_0

    .line 4699
    const/4 v5, 0x1

    .line 4700
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 4697
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4704
    :cond_1
    iget-object v1, p0, Ldpr;->c:Ldtr;

    if-eqz v1, :cond_2

    .line 4705
    const/4 v1, 0x2

    iget-object v2, p0, Ldpr;->c:Ldtr;

    .line 4706
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4708
    :cond_2
    iget-object v1, p0, Ldpr;->d:Ldtq;

    if-eqz v1, :cond_3

    .line 4709
    const/4 v1, 0x3

    iget-object v2, p0, Ldpr;->d:Ldtq;

    .line 4710
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4712
    :cond_3
    iget-object v1, p0, Ldpr;->e:Ldsc;

    if-eqz v1, :cond_4

    .line 4713
    const/4 v1, 0x4

    iget-object v2, p0, Ldpr;->e:Ldsc;

    .line 4714
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4716
    :cond_4
    iget-object v1, p0, Ldpr;->f:Ldxb;

    if-eqz v1, :cond_5

    .line 4717
    const/4 v1, 0x5

    iget-object v2, p0, Ldpr;->f:Ldxb;

    .line 4718
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4720
    :cond_5
    iget-object v1, p0, Ldpr;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4721
    iput v0, p0, Ldpr;->cachedSize:I

    .line 4722
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4646
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldpr;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldpr;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldpr;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldpr;->b:[Lesg;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lesg;

    iget-object v3, p0, Ldpr;->b:[Lesg;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldpr;->b:[Lesg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldpr;->b:[Lesg;

    :goto_2
    iget-object v2, p0, Ldpr;->b:[Lesg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldpr;->b:[Lesg;

    new-instance v3, Lesg;

    invoke-direct {v3}, Lesg;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldpr;->b:[Lesg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldpr;->b:[Lesg;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldpr;->b:[Lesg;

    new-instance v3, Lesg;

    invoke-direct {v3}, Lesg;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldpr;->b:[Lesg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldpr;->c:Ldtr;

    if-nez v0, :cond_5

    new-instance v0, Ldtr;

    invoke-direct {v0}, Ldtr;-><init>()V

    iput-object v0, p0, Ldpr;->c:Ldtr;

    :cond_5
    iget-object v0, p0, Ldpr;->c:Ldtr;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldpr;->d:Ldtq;

    if-nez v0, :cond_6

    new-instance v0, Ldtq;

    invoke-direct {v0}, Ldtq;-><init>()V

    iput-object v0, p0, Ldpr;->d:Ldtq;

    :cond_6
    iget-object v0, p0, Ldpr;->d:Ldtq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Ldpr;->e:Ldsc;

    if-nez v0, :cond_7

    new-instance v0, Ldsc;

    invoke-direct {v0}, Ldsc;-><init>()V

    iput-object v0, p0, Ldpr;->e:Ldsc;

    :cond_7
    iget-object v0, p0, Ldpr;->e:Ldsc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Ldpr;->f:Ldxb;

    if-nez v0, :cond_8

    new-instance v0, Ldxb;

    invoke-direct {v0}, Ldxb;-><init>()V

    iput-object v0, p0, Ldpr;->f:Ldxb;

    :cond_8
    iget-object v0, p0, Ldpr;->f:Ldxb;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 4670
    iget-object v0, p0, Ldpr;->b:[Lesg;

    if-eqz v0, :cond_1

    .line 4671
    iget-object v1, p0, Ldpr;->b:[Lesg;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 4672
    if-eqz v3, :cond_0

    .line 4673
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 4671
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4677
    :cond_1
    iget-object v0, p0, Ldpr;->c:Ldtr;

    if-eqz v0, :cond_2

    .line 4678
    const/4 v0, 0x2

    iget-object v1, p0, Ldpr;->c:Ldtr;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 4680
    :cond_2
    iget-object v0, p0, Ldpr;->d:Ldtq;

    if-eqz v0, :cond_3

    .line 4681
    const/4 v0, 0x3

    iget-object v1, p0, Ldpr;->d:Ldtq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 4683
    :cond_3
    iget-object v0, p0, Ldpr;->e:Ldsc;

    if-eqz v0, :cond_4

    .line 4684
    const/4 v0, 0x4

    iget-object v1, p0, Ldpr;->e:Ldsc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 4686
    :cond_4
    iget-object v0, p0, Ldpr;->f:Ldxb;

    if-eqz v0, :cond_5

    .line 4687
    const/4 v0, 0x5

    iget-object v1, p0, Ldpr;->f:Ldxb;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 4689
    :cond_5
    iget-object v0, p0, Ldpr;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 4691
    return-void
.end method
