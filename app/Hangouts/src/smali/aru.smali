.class public final Laru;
.super Ls;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ls;-><init>()V

    return-void
.end method

.method static q()Laru;
    .locals 1

    .prologue
    .line 22
    new-instance v0, Laru;

    invoke-direct {v0}, Laru;-><init>()V

    return-object v0
.end method


# virtual methods
.method public f()Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 27
    invoke-virtual {p0}, Laru;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 28
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Laru;->getActivity()Ly;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lh;->ee:I

    .line 29
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lh;->ef:I

    .line 30
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lh;->ec:I

    .line 31
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lh;->ed:I

    .line 33
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 36
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 37
    return-object v0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 42
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 43
    const-string v0, "babel_google_voice_add_balance_url"

    const-string v1, "https://www.google.com/voice/m/credit?p=add_credit&rd=1&b=1"

    .line 44
    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 43
    invoke-static {v0}, Lbbl;->h(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 47
    invoke-virtual {p0, v0}, Laru;->startActivity(Landroid/content/Intent;)V

    .line 49
    :cond_0
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    if-nez v0, :cond_0

    .line 55
    invoke-virtual {p0}, Laru;->getActivity()Ly;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    .line 58
    if-eqz v0, :cond_0

    .line 59
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->p()V

    .line 62
    :cond_0
    return-void
.end method
