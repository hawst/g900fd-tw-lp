.class public final Lbyc;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static final p:Ljava/text/SimpleDateFormat;


# instance fields
.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:J

.field private f:J

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:I

.field private j:Z

.field private k:I

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 72
    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbyc;->a:Ljava/lang/String;

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "date,logType,extraData,messageTime,screenOn,activity,activeClient,notificationLevel,conversationId,sizeInBytes,accountName,accountID"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lbyc;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbyc;->b:Ljava/lang/String;

    .line 96
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd HH:mm:ss.SSSZ"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lbyc;->p:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    invoke-virtual {p0}, Lbyc;->a()Lbyc;

    .line 101
    return-void
.end method

.method static synthetic a(Lbyc;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 45
    invoke-static {}, Lbya;->d()Landroid/os/PowerManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    iput-boolean v0, p0, Lbyc;->g:Z

    invoke-static {}, Lbya;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lbya;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lbyc;->h:Ljava/lang/String;

    iget-wide v0, p0, Lbyc;->e:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lbyc;->e:J

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lbyc;->c:Ljava/lang/String;

    if-nez v1, :cond_1

    const-string v1, "Babel"

    const-string v2, "null datatype in DataLog.build"

    invoke-static {v1, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "(unknown)"

    iput-object v1, p0, Lbyc;->c:Ljava/lang/String;

    :cond_1
    sget-object v1, Lbyc;->p:Ljava/text/SimpleDateFormat;

    new-instance v2, Ljava/util/Date;

    iget-wide v3, p0, Lbyc;->e:J

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lbyc;->e:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "),"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbyc;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbyc;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lbyc;->f:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lbyc;->g:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbyc;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lbyc;->i:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lbyc;->j:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lbyc;->k:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbyc;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lbyc;->o:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbyc;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbyc;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lbyc;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    const-string v0, "--"

    goto/16 :goto_0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lbyc;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()Lbyc;
    .locals 5

    .prologue
    const-wide/16 v3, -0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lbyc;->c:Ljava/lang/String;

    .line 108
    const-string v0, ""

    iput-object v0, p0, Lbyc;->d:Ljava/lang/String;

    .line 109
    iput-wide v3, p0, Lbyc;->e:J

    .line 110
    iput-wide v3, p0, Lbyc;->f:J

    .line 111
    iput-boolean v1, p0, Lbyc;->g:Z

    .line 112
    const-string v0, ""

    iput-object v0, p0, Lbyc;->h:Ljava/lang/String;

    .line 113
    iput v2, p0, Lbyc;->i:I

    .line 114
    iput-boolean v1, p0, Lbyc;->j:Z

    .line 115
    iput v2, p0, Lbyc;->k:I

    .line 116
    const-string v0, ""

    iput-object v0, p0, Lbyc;->l:Ljava/lang/String;

    .line 117
    const-string v0, ""

    iput-object v0, p0, Lbyc;->m:Ljava/lang/String;

    .line 118
    const-string v0, ""

    iput-object v0, p0, Lbyc;->n:Ljava/lang/String;

    .line 119
    iput v1, p0, Lbyc;->o:I

    .line 120
    return-object p0
.end method

.method public a(I)Lbyc;
    .locals 0

    .prologue
    .line 148
    iput p1, p0, Lbyc;->i:I

    .line 149
    return-object p0
.end method

.method public a(J)Lbyc;
    .locals 0

    .prologue
    .line 138
    iput-wide p1, p0, Lbyc;->e:J

    .line 139
    return-object p0
.end method

.method public a(Lbdk;)Lbyc;
    .locals 1

    .prologue
    .line 168
    invoke-virtual {p1}, Lbdk;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbyc;->m:Ljava/lang/String;

    .line 169
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lbyc;
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lbyc;->c:Ljava/lang/String;

    .line 125
    return-object p0
.end method

.method public a(Lyj;)Lbyc;
    .locals 1

    .prologue
    .line 178
    invoke-virtual {p1}, Lyj;->c()Lbdk;

    move-result-object v0

    invoke-virtual {v0}, Lbdk;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbyc;->m:Ljava/lang/String;

    .line 179
    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbyc;->n:Ljava/lang/String;

    .line 180
    return-object p0
.end method

.method public a(Z)Lbyc;
    .locals 0

    .prologue
    .line 153
    iput-boolean p1, p0, Lbyc;->j:Z

    .line 154
    return-object p0
.end method

.method public b(I)Lbyc;
    .locals 0

    .prologue
    .line 158
    iput p1, p0, Lbyc;->k:I

    .line 159
    return-object p0
.end method

.method public b(J)Lbyc;
    .locals 0

    .prologue
    .line 143
    iput-wide p1, p0, Lbyc;->f:J

    .line 144
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lbyc;
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lbyc;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    iput-object p1, p0, Lbyc;->d:Ljava/lang/String;

    .line 134
    :goto_0
    return-object p0

    .line 132
    :cond_0
    iget-object v0, p0, Lbyc;->d:Ljava/lang/String;

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbyc;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 190
    invoke-static {p0}, Lbya;->a(Lbyc;)V

    .line 191
    return-void
.end method

.method public c(I)Lbyc;
    .locals 0

    .prologue
    .line 185
    iput p1, p0, Lbyc;->o:I

    .line 186
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lbyc;
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lbyc;->l:Ljava/lang/String;

    .line 164
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lbyc;
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lbyc;->n:Ljava/lang/String;

    .line 174
    return-object p0
.end method
