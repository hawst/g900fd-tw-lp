.class public final Ldvv;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldvv;


# instance fields
.field public b:Ldvn;

.field public c:Ljava/lang/Integer;

.field public d:[Ldtv;

.field public e:Ldrr;

.field public f:Ldqa;

.field public g:Ljava/lang/Long;

.field public h:Ljava/lang/String;

.field public i:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5398
    const/4 v0, 0x0

    new-array v0, v0, [Ldvv;

    sput-object v0, Ldvv;->a:[Ldvv;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5399
    invoke-direct {p0}, Lepn;-><init>()V

    .line 5411
    iput-object v1, p0, Ldvv;->b:Ldvn;

    .line 5414
    iput-object v1, p0, Ldvv;->c:Ljava/lang/Integer;

    .line 5417
    sget-object v0, Ldtv;->a:[Ldtv;

    iput-object v0, p0, Ldvv;->d:[Ldtv;

    .line 5420
    iput-object v1, p0, Ldvv;->e:Ldrr;

    .line 5423
    iput-object v1, p0, Ldvv;->f:Ldqa;

    .line 5399
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 5469
    iget-object v0, p0, Ldvv;->b:Ldvn;

    if-eqz v0, :cond_8

    .line 5470
    const/4 v0, 0x1

    iget-object v2, p0, Ldvv;->b:Ldvn;

    .line 5471
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5473
    :goto_0
    iget-object v2, p0, Ldvv;->g:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 5474
    const/4 v2, 0x2

    iget-object v3, p0, Ldvv;->g:Ljava/lang/Long;

    .line 5475
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 5477
    :cond_0
    iget-object v2, p0, Ldvv;->h:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 5478
    const/4 v2, 0x3

    iget-object v3, p0, Ldvv;->h:Ljava/lang/String;

    .line 5479
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5481
    :cond_1
    iget-object v2, p0, Ldvv;->d:[Ldtv;

    if-eqz v2, :cond_3

    .line 5482
    iget-object v2, p0, Ldvv;->d:[Ldtv;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 5483
    if-eqz v4, :cond_2

    .line 5484
    const/4 v5, 0x4

    .line 5485
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 5482
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 5489
    :cond_3
    iget-object v1, p0, Ldvv;->i:[B

    if-eqz v1, :cond_4

    .line 5490
    const/4 v1, 0x5

    iget-object v2, p0, Ldvv;->i:[B

    .line 5491
    invoke-static {v1, v2}, Lepl;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 5493
    :cond_4
    iget-object v1, p0, Ldvv;->e:Ldrr;

    if-eqz v1, :cond_5

    .line 5494
    const/4 v1, 0x6

    iget-object v2, p0, Ldvv;->e:Ldrr;

    .line 5495
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5497
    :cond_5
    iget-object v1, p0, Ldvv;->f:Ldqa;

    if-eqz v1, :cond_6

    .line 5498
    const/4 v1, 0x7

    iget-object v2, p0, Ldvv;->f:Ldqa;

    .line 5499
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5501
    :cond_6
    iget-object v1, p0, Ldvv;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 5502
    const/16 v1, 0x8

    iget-object v2, p0, Ldvv;->c:Ljava/lang/Integer;

    .line 5503
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5505
    :cond_7
    iget-object v1, p0, Ldvv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5506
    iput v0, p0, Ldvv;->cachedSize:I

    .line 5507
    return v0

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5395
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldvv;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldvv;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldvv;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldvv;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldvv;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldvv;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldvv;->g:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldvv;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldvv;->d:[Ldtv;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldtv;

    iget-object v3, p0, Ldvv;->d:[Ldtv;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldvv;->d:[Ldtv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldvv;->d:[Ldtv;

    :goto_2
    iget-object v2, p0, Ldvv;->d:[Ldtv;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldvv;->d:[Ldtv;

    new-instance v3, Ldtv;

    invoke-direct {v3}, Ldtv;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldvv;->d:[Ldtv;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldvv;->d:[Ldtv;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldvv;->d:[Ldtv;

    new-instance v3, Ldtv;

    invoke-direct {v3}, Ldtv;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldvv;->d:[Ldtv;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Ldvv;->i:[B

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Ldvv;->e:Ldrr;

    if-nez v0, :cond_6

    new-instance v0, Ldrr;

    invoke-direct {v0}, Ldrr;-><init>()V

    iput-object v0, p0, Ldvv;->e:Ldrr;

    :cond_6
    iget-object v0, p0, Ldvv;->e:Ldrr;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Ldvv;->f:Ldqa;

    if-nez v0, :cond_7

    new-instance v0, Ldqa;

    invoke-direct {v0}, Ldqa;-><init>()V

    iput-object v0, p0, Ldvv;->f:Ldqa;

    :cond_7
    iget-object v0, p0, Ldvv;->f:Ldqa;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_8

    const/4 v2, 0x1

    if-eq v0, v2, :cond_8

    const/4 v2, 0x2

    if-eq v0, v2, :cond_8

    const/4 v2, 0x3

    if-eq v0, v2, :cond_8

    const/4 v2, 0x4

    if-eq v0, v2, :cond_8

    const/4 v2, 0x5

    if-ne v0, v2, :cond_9

    :cond_8
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldvv;->c:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_9
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldvv;->c:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 5434
    iget-object v0, p0, Ldvv;->b:Ldvn;

    if-eqz v0, :cond_0

    .line 5435
    const/4 v0, 0x1

    iget-object v1, p0, Ldvv;->b:Ldvn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5437
    :cond_0
    iget-object v0, p0, Ldvv;->g:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 5438
    const/4 v0, 0x2

    iget-object v1, p0, Ldvv;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 5440
    :cond_1
    iget-object v0, p0, Ldvv;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 5441
    const/4 v0, 0x3

    iget-object v1, p0, Ldvv;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 5443
    :cond_2
    iget-object v0, p0, Ldvv;->d:[Ldtv;

    if-eqz v0, :cond_4

    .line 5444
    iget-object v1, p0, Ldvv;->d:[Ldtv;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 5445
    if-eqz v3, :cond_3

    .line 5446
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 5444
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5450
    :cond_4
    iget-object v0, p0, Ldvv;->i:[B

    if-eqz v0, :cond_5

    .line 5451
    const/4 v0, 0x5

    iget-object v1, p0, Ldvv;->i:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 5453
    :cond_5
    iget-object v0, p0, Ldvv;->e:Ldrr;

    if-eqz v0, :cond_6

    .line 5454
    const/4 v0, 0x6

    iget-object v1, p0, Ldvv;->e:Ldrr;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5456
    :cond_6
    iget-object v0, p0, Ldvv;->f:Ldqa;

    if-eqz v0, :cond_7

    .line 5457
    const/4 v0, 0x7

    iget-object v1, p0, Ldvv;->f:Ldqa;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5459
    :cond_7
    iget-object v0, p0, Ldvv;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 5460
    const/16 v0, 0x8

    iget-object v1, p0, Ldvv;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 5462
    :cond_8
    iget-object v0, p0, Ldvv;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 5464
    return-void
.end method
