.class public final Ldie;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldie;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Lezc;

.field public d:Ljava/lang/Boolean;

.field public e:Leka;

.field public f:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Ldie;

    sput-object v0, Ldie;->a:[Ldie;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 15
    iput-object v0, p0, Ldie;->c:Lezc;

    .line 20
    iput-object v0, p0, Ldie;->e:Leka;

    .line 23
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldie;->f:[Ljava/lang/String;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 52
    iget-object v0, p0, Ldie;->c:Lezc;

    if-eqz v0, :cond_5

    .line 53
    const/4 v0, 0x1

    iget-object v2, p0, Ldie;->c:Lezc;

    .line 54
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 56
    :goto_0
    iget-object v2, p0, Ldie;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    .line 57
    const/4 v2, 0x2

    iget-object v3, p0, Ldie;->d:Ljava/lang/Boolean;

    .line 58
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 60
    :cond_0
    iget-object v2, p0, Ldie;->e:Leka;

    if-eqz v2, :cond_1

    .line 61
    const/4 v2, 0x3

    iget-object v3, p0, Ldie;->e:Leka;

    .line 62
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 64
    :cond_1
    iget-object v2, p0, Ldie;->b:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 65
    const/4 v2, 0x4

    iget-object v3, p0, Ldie;->b:Ljava/lang/String;

    .line 66
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 68
    :cond_2
    iget-object v2, p0, Ldie;->f:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Ldie;->f:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 70
    iget-object v3, p0, Ldie;->f:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    .line 72
    invoke-static {v5}, Lepl;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 70
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 74
    :cond_3
    add-int/2addr v0, v2

    .line 75
    iget-object v1, p0, Ldie;->f:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 77
    :cond_4
    iget-object v1, p0, Ldie;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    iput v0, p0, Ldie;->cachedSize:I

    .line 79
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldie;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldie;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldie;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldie;->c:Lezc;

    if-nez v0, :cond_2

    new-instance v0, Lezc;

    invoke-direct {v0}, Lezc;-><init>()V

    iput-object v0, p0, Ldie;->c:Lezc;

    :cond_2
    iget-object v0, p0, Ldie;->c:Lezc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldie;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldie;->e:Leka;

    if-nez v0, :cond_3

    new-instance v0, Leka;

    invoke-direct {v0}, Leka;-><init>()V

    iput-object v0, p0, Ldie;->e:Leka;

    :cond_3
    iget-object v0, p0, Ldie;->e:Leka;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldie;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldie;->f:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Ldie;->f:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldie;->f:[Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Ldie;->f:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Ldie;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iget-object v1, p0, Ldie;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 28
    iget-object v0, p0, Ldie;->c:Lezc;

    if-eqz v0, :cond_0

    .line 29
    const/4 v0, 0x1

    iget-object v1, p0, Ldie;->c:Lezc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 31
    :cond_0
    iget-object v0, p0, Ldie;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 32
    const/4 v0, 0x2

    iget-object v1, p0, Ldie;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 34
    :cond_1
    iget-object v0, p0, Ldie;->e:Leka;

    if-eqz v0, :cond_2

    .line 35
    const/4 v0, 0x3

    iget-object v1, p0, Ldie;->e:Leka;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 37
    :cond_2
    iget-object v0, p0, Ldie;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 38
    const/4 v0, 0x4

    iget-object v1, p0, Ldie;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 40
    :cond_3
    iget-object v0, p0, Ldie;->f:[Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 41
    iget-object v1, p0, Ldie;->f:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 42
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Lepl;->a(ILjava/lang/String;)V

    .line 41
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 45
    :cond_4
    iget-object v0, p0, Ldie;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 47
    return-void
.end method
