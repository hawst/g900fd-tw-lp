.class public final Leop;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leop;


# instance fields
.field public b:Ljava/lang/Long;

.field public c:[Leor;

.field public d:[Leoq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    new-array v0, v0, [Leop;

    sput-object v0, Leop;->a:[Leop;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 150
    invoke-direct {p0}, Lepn;-><init>()V

    .line 382
    sget-object v0, Leor;->a:[Leor;

    iput-object v0, p0, Leop;->c:[Leor;

    .line 385
    sget-object v0, Leoq;->a:[Leoq;

    iput-object v0, p0, Leop;->d:[Leoq;

    .line 150
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 414
    iget-object v0, p0, Leop;->b:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 415
    const/4 v0, 0x1

    iget-object v2, p0, Leop;->b:Ljava/lang/Long;

    .line 416
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lepl;->e(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 418
    :goto_0
    iget-object v2, p0, Leop;->c:[Leor;

    if-eqz v2, :cond_1

    .line 419
    iget-object v3, p0, Leop;->c:[Leor;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 420
    if-eqz v5, :cond_0

    .line 421
    const/4 v6, 0x2

    .line 422
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 419
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 426
    :cond_1
    iget-object v2, p0, Leop;->d:[Leoq;

    if-eqz v2, :cond_3

    .line 427
    iget-object v2, p0, Leop;->d:[Leoq;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 428
    if-eqz v4, :cond_2

    .line 429
    const/4 v5, 0x3

    .line 430
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 427
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 434
    :cond_3
    iget-object v1, p0, Leop;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 435
    iput v0, p0, Leop;->cachedSize:I

    .line 436
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 146
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Leop;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Leop;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Leop;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Leop;->b:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leop;->c:[Leor;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leor;

    iget-object v3, p0, Leop;->c:[Leor;

    if-eqz v3, :cond_2

    iget-object v3, p0, Leop;->c:[Leor;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Leop;->c:[Leor;

    :goto_2
    iget-object v2, p0, Leop;->c:[Leor;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Leop;->c:[Leor;

    new-instance v3, Leor;

    invoke-direct {v3}, Leor;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leop;->c:[Leor;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Leop;->c:[Leor;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Leop;->c:[Leor;

    new-instance v3, Leor;

    invoke-direct {v3}, Leor;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leop;->c:[Leor;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leop;->d:[Leoq;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Leoq;

    iget-object v3, p0, Leop;->d:[Leoq;

    if-eqz v3, :cond_5

    iget-object v3, p0, Leop;->d:[Leoq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Leop;->d:[Leoq;

    :goto_4
    iget-object v2, p0, Leop;->d:[Leoq;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Leop;->d:[Leoq;

    new-instance v3, Leoq;

    invoke-direct {v3}, Leoq;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leop;->d:[Leoq;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Leop;->d:[Leoq;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Leop;->d:[Leoq;

    new-instance v3, Leoq;

    invoke-direct {v3}, Leoq;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leop;->d:[Leoq;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 390
    iget-object v1, p0, Leop;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 391
    const/4 v1, 0x1

    iget-object v2, p0, Leop;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->b(IJ)V

    .line 393
    :cond_0
    iget-object v1, p0, Leop;->c:[Leor;

    if-eqz v1, :cond_2

    .line 394
    iget-object v2, p0, Leop;->c:[Leor;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 395
    if-eqz v4, :cond_1

    .line 396
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 394
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 400
    :cond_2
    iget-object v1, p0, Leop;->d:[Leoq;

    if-eqz v1, :cond_4

    .line 401
    iget-object v1, p0, Leop;->d:[Leoq;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 402
    if-eqz v3, :cond_3

    .line 403
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 401
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 407
    :cond_4
    iget-object v0, p0, Leop;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 409
    return-void
.end method
