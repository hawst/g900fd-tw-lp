.class public Lbhs;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method private constructor <init>(Ldzi;)V
    .locals 3

    .prologue
    .line 3858
    invoke-direct {p0}, Lbfz;-><init>()V

    .line 3859
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 3860
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ReportCallPerfStatsResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3863
    :cond_0
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 3867
    new-instance v0, Ldzi;

    invoke-direct {v0}, Ldzi;-><init>()V

    .line 3868
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldzi;

    .line 3869
    iget-object v1, v0, Ldzi;->b:Ldvn;

    invoke-static {v1}, Lbhs;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3870
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldzi;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 3872
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbhs;

    invoke-direct {v1, v0}, Lbhs;-><init>(Ldzi;)V

    move-object v0, v1

    goto :goto_0
.end method
