.class public abstract Lece;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field b:I

.field c:I

.field d:Lecj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lecj",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field e:Ljava/util/concurrent/atomic/AtomicReferenceArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReferenceArray",
            "<",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field f:Lcom/google/common/cache/LocalCache$ReferenceEntry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field g:Ledg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/cache/LocalCache",
            "<TK;TV;>.edg;"
        }
    .end annotation
.end field

.field h:Ledg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/cache/LocalCache",
            "<TK;TV;>.edg;"
        }
    .end annotation
.end field

.field final synthetic i:Lcom/google/common/cache/LocalCache;


# direct methods
.method constructor <init>(Lcom/google/common/cache/LocalCache;)V
    .locals 1

    .prologue
    .line 4194
    iput-object p1, p0, Lece;->i:Lcom/google/common/cache/LocalCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4195
    iget-object v0, p1, Lcom/google/common/cache/LocalCache;->e:[Lecj;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lece;->b:I

    .line 4196
    const/4 v0, -0x1

    iput v0, p0, Lece;->c:I

    .line 4197
    invoke-direct {p0}, Lece;->d()V

    .line 4198
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 4203
    const/4 v0, 0x0

    iput-object v0, p0, Lece;->g:Ledg;

    .line 4205
    invoke-virtual {p0}, Lece;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4223
    :cond_0
    :goto_0
    return-void

    .line 4209
    :cond_1
    invoke-virtual {p0}, Lece;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4213
    :cond_2
    iget v0, p0, Lece;->b:I

    if-ltz v0, :cond_0

    .line 4214
    iget-object v0, p0, Lece;->i:Lcom/google/common/cache/LocalCache;

    iget-object v0, v0, Lcom/google/common/cache/LocalCache;->e:[Lecj;

    iget v1, p0, Lece;->b:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lece;->b:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lece;->d:Lecj;

    .line 4215
    iget-object v0, p0, Lece;->d:Lecj;

    iget v0, v0, Lecj;->b:I

    if-eqz v0, :cond_2

    .line 4216
    iget-object v0, p0, Lece;->d:Lecj;

    iget-object v0, v0, Lecj;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iput-object v0, p0, Lece;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 4217
    iget-object v0, p0, Lece;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lece;->c:I

    .line 4218
    invoke-virtual {p0}, Lece;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0
.end method


# virtual methods
.method a()Z
    .locals 1

    .prologue
    .line 4229
    iget-object v0, p0, Lece;->f:Lcom/google/common/cache/LocalCache$ReferenceEntry;

    if-eqz v0, :cond_1

    .line 4230
    iget-object v0, p0, Lece;->f:Lcom/google/common/cache/LocalCache$ReferenceEntry;

    invoke-interface {v0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNext()Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    iput-object v0, p0, Lece;->f:Lcom/google/common/cache/LocalCache$ReferenceEntry;

    :goto_0
    iget-object v0, p0, Lece;->f:Lcom/google/common/cache/LocalCache$ReferenceEntry;

    if-eqz v0, :cond_1

    .line 4231
    iget-object v0, p0, Lece;->f:Lcom/google/common/cache/LocalCache$ReferenceEntry;

    invoke-virtual {p0, v0}, Lece;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4232
    const/4 v0, 0x1

    .line 4236
    :goto_1
    return v0

    .line 4230
    :cond_0
    iget-object v0, p0, Lece;->f:Lcom/google/common/cache/LocalCache$ReferenceEntry;

    invoke-interface {v0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNext()Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    iput-object v0, p0, Lece;->f:Lcom/google/common/cache/LocalCache$ReferenceEntry;

    goto :goto_0

    .line 4236
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method a(Lcom/google/common/cache/LocalCache$ReferenceEntry;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 4259
    :try_start_0
    iget-object v0, p0, Lece;->i:Lcom/google/common/cache/LocalCache;

    iget-object v0, v0, Lcom/google/common/cache/LocalCache;->q:Lebm;

    invoke-virtual {v0}, Lebm;->a()J

    move-result-wide v0

    .line 4260
    invoke-interface {p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 4261
    iget-object v3, p0, Lece;->i:Lcom/google/common/cache/LocalCache;

    invoke-virtual {v3, p1, v0, v1}, Lcom/google/common/cache/LocalCache;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;J)Ljava/lang/Object;

    move-result-object v0

    .line 4262
    if-eqz v0, :cond_0

    .line 4263
    new-instance v1, Ledg;

    iget-object v3, p0, Lece;->i:Lcom/google/common/cache/LocalCache;

    invoke-direct {v1, v3, v2, v0}, Ledg;-><init>(Lcom/google/common/cache/LocalCache;Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lece;->g:Ledg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4264
    iget-object v0, p0, Lece;->d:Lecj;

    invoke-virtual {v0}, Lecj;->b()V

    const/4 v0, 0x1

    .line 4267
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lece;->d:Lecj;

    invoke-virtual {v0}, Lecj;->b()V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lece;->d:Lecj;

    invoke-virtual {v1}, Lecj;->b()V

    throw v0
.end method

.method b()Z
    .locals 3

    .prologue
    .line 4243
    :cond_0
    iget v0, p0, Lece;->c:I

    if-ltz v0, :cond_2

    .line 4244
    iget-object v0, p0, Lece;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget v1, p0, Lece;->c:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lece;->c:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/cache/LocalCache$ReferenceEntry;

    iput-object v0, p0, Lece;->f:Lcom/google/common/cache/LocalCache$ReferenceEntry;

    if-eqz v0, :cond_0

    .line 4245
    iget-object v0, p0, Lece;->f:Lcom/google/common/cache/LocalCache$ReferenceEntry;

    invoke-virtual {p0, v0}, Lece;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lece;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4246
    :cond_1
    const/4 v0, 0x1

    .line 4250
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c()Ledg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/cache/LocalCache",
            "<TK;TV;>.edg;"
        }
    .end annotation

    .prologue
    .line 4279
    iget-object v0, p0, Lece;->g:Ledg;

    if-nez v0, :cond_0

    .line 4280
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 4282
    :cond_0
    iget-object v0, p0, Lece;->g:Ledg;

    iput-object v0, p0, Lece;->h:Ledg;

    .line 4283
    invoke-direct {p0}, Lece;->d()V

    .line 4284
    iget-object v0, p0, Lece;->h:Ledg;

    return-object v0
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 4275
    iget-object v0, p0, Lece;->g:Ledg;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 4288
    iget-object v0, p0, Lece;->h:Ledg;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lm;->b(Z)V

    .line 4289
    iget-object v0, p0, Lece;->i:Lcom/google/common/cache/LocalCache;

    iget-object v1, p0, Lece;->h:Ledg;

    invoke-virtual {v1}, Ledg;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/cache/LocalCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4290
    const/4 v0, 0x0

    iput-object v0, p0, Lece;->h:Ledg;

    .line 4291
    return-void

    .line 4288
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
