.class public Labj;
.super Lakl;
.source "PG"

# interfaces
.implements Laeo;
.implements Lamk;


# instance fields
.field protected a:Lcom/google/android/apps/hangouts/views/AudienceView;

.field protected b:Lxm;

.field protected c:Lame;

.field private d:Lyj;

.field private e:Laen;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lakl;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->a()V

    .line 105
    return-void
.end method

.method public a(Laen;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Labj;->e:Laen;

    .line 182
    return-void
.end method

.method public a(Laod;)V
    .locals 6

    .prologue
    .line 193
    invoke-interface {p1}, Laod;->c()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 195
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    iput-object v0, p0, Labj;->d:Lyj;

    .line 196
    iget-object v0, p0, Labj;->d:Lyj;

    if-nez v0, :cond_0

    .line 197
    const-string v0, "Babel"

    const-string v1, "Account is no longer valid in AudienceFragment"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    :goto_0
    return-void

    .line 202
    :cond_0
    iget-object v0, p0, Labj;->c:Lame;

    if-eqz v0, :cond_1

    .line 203
    iget-object v0, p0, Labj;->c:Lame;

    invoke-virtual {v0}, Lame;->i()V

    .line 206
    :cond_1
    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Labj;->getActivity()Ly;

    move-result-object v0

    sget v2, Lf;->hQ:I

    invoke-direct {v1, v0, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 208
    new-instance v0, Lame;

    .line 209
    invoke-virtual {p0}, Labj;->getLoaderManager()Lav;

    move-result-object v3

    iget-object v4, p0, Labj;->d:Lyj;

    invoke-interface {p1}, Laod;->d()I

    move-result v5

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lame;-><init>(Landroid/content/Context;Lt;Lav;Lyj;I)V

    iput-object v0, p0, Labj;->c:Lame;

    .line 210
    iget-object v0, p0, Labj;->c:Lame;

    invoke-virtual {v0, p0}, Lame;->a(Lamk;)V

    .line 211
    iget-object v0, p0, Labj;->c:Lame;

    invoke-virtual {v0}, Lame;->g()V

    .line 212
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    iget-object v1, p0, Labj;->d:Lyj;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(Lyj;)V

    .line 213
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    iget-object v1, p0, Labj;->c:Lame;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(Lame;)V

    .line 214
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-interface {p1}, Laod;->z_()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(Z)V

    .line 215
    iget-object v0, p0, Labj;->d:Lyj;

    invoke-virtual {v0}, Lyj;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lh;->kg:I

    :goto_1
    iget-object v1, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(I)V

    .line 216
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->e()V

    .line 217
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->requestFocus()Z

    goto :goto_0

    .line 215
    :cond_2
    sget v0, Lh;->kh:I

    goto :goto_1
.end method

.method public a(Laoe;)V
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(Laoe;)V

    .line 222
    return-void
.end method

.method public final a(Lxm;)V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(Lxm;)V

    .line 190
    return-void
.end method

.method public a(Lxo;)V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(Lxo;)V

    .line 153
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->d()V

    .line 154
    return-void
.end method

.method public a(Lxs;)V
    .locals 1

    .prologue
    .line 161
    if-eqz p1, :cond_0

    .line 162
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(Lxs;)V

    .line 165
    :cond_0
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->d()V

    .line 166
    return-void
.end method

.method public b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 125
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    if-nez v0, :cond_0

    move v0, v1

    .line 144
    :goto_0
    return v0

    .line 129
    :cond_0
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->b()Lxm;

    move-result-object v0

    .line 130
    if-nez v0, :cond_1

    move v0, v1

    .line 131
    goto :goto_0

    .line 132
    :cond_1
    invoke-virtual {v0}, Lxm;->b()I

    move-result v3

    if-lez v3, :cond_2

    move v0, v2

    .line 133
    goto :goto_0

    .line 135
    :cond_2
    invoke-virtual {v0}, Lxm;->c()Ljava/util/List;

    move-result-object v0

    .line 136
    if-eqz v0, :cond_4

    .line 137
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxo;

    .line 138
    invoke-virtual {v0}, Lxo;->e()I

    move-result v0

    if-lez v0, :cond_3

    move v0, v2

    .line 139
    goto :goto_0

    :cond_4
    move v0, v1

    .line 144
    goto :goto_0
.end method

.method public c()Laen;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Labj;->e:Laen;

    return-object v0
.end method

.method public final d()Lxm;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->b()Lxm;

    move-result-object v0

    return-object v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->f()V

    .line 226
    return-void
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 89
    sget v0, Lf;->ec:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Labj;->c:Lame;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Labj;->c:Lame;

    invoke-virtual {v0}, Lame;->i()V

    .line 83
    :cond_0
    invoke-super {p0}, Lakl;->onDestroy()V

    .line 84
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 74
    invoke-super {p0}, Lakl;->onPause()V

    .line 75
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(Lame;)V

    .line 76
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0}, Lakl;->onResume()V

    .line 63
    iget-object v0, p0, Labj;->b:Lxm;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Labj;->b:Lxm;

    invoke-virtual {p0, v0}, Labj;->a(Lxm;)V

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Labj;->b:Lxm;

    .line 68
    :cond_0
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    iget-object v1, p0, Labj;->c:Lame;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(Lame;)V

    .line 69
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->requestFocus()Z

    .line 70
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0}, Lakl;->onStart()V

    .line 46
    iget-object v0, p0, Labj;->c:Lame;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Labj;->c:Lame;

    invoke-virtual {v0}, Lame;->h()V

    .line 49
    :cond_0
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->d()V

    .line 50
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 54
    invoke-super {p0}, Lakl;->onStop()V

    .line 55
    iget-object v0, p0, Labj;->c:Lame;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Labj;->c:Lame;

    invoke-virtual {v0}, Lame;->j()V

    .line 58
    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 94
    invoke-super {p0, p1, p2}, Lakl;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 95
    sget v0, Lg;->z:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/AudienceView;

    iput-object v0, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    .line 100
    iget-object v0, p0, Labj;->a:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(Landroid/view/View;)V

    .line 101
    return-void
.end method
