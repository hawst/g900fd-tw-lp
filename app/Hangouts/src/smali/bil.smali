.class public Lbil;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:J

.field private final h:Z

.field private final i:Z

.field private final j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[B>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ldxg;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2792
    iget-object v1, p1, Ldxg;->b:Ldvn;

    const-wide/16 v2, -0x1

    invoke-direct {p0, v1, v2, v3}, Lbfz;-><init>(Ldvn;J)V

    .line 2787
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lbil;->j:Ljava/util/ArrayList;

    .line 2793
    iget-object v1, p1, Ldxg;->d:Ljava/lang/Long;

    invoke-static {v1}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v1

    iput-wide v1, p0, Lbil;->g:J

    .line 2795
    iget-object v1, p1, Ldxg;->c:Ljava/lang/Boolean;

    invoke-static {v1, v0}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v1

    iput-boolean v1, p0, Lbil;->h:Z

    .line 2797
    iget-object v1, p1, Ldxg;->f:Ljava/lang/Boolean;

    invoke-static {v1, v0}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v1

    iput-boolean v1, p0, Lbil;->i:Z

    .line 2800
    iget-object v1, p1, Ldxg;->e:[Ldql;

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 2801
    iget-object v4, p0, Lbil;->j:Ljava/util/ArrayList;

    invoke-static {v3}, Ldql;->toByteArray(Lepr;)[B

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2800
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2804
    :cond_0
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_1

    .line 2805
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SyncAllNewEventsResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2807
    :cond_1
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 2811
    new-instance v0, Ldxg;

    invoke-direct {v0}, Ldxg;-><init>()V

    .line 2812
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldxg;

    .line 2813
    iget-object v1, v0, Ldxg;->b:Ldvn;

    invoke-static {v1}, Lbil;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2814
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldxg;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 2816
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbil;

    invoke-direct {v1, v0}, Lbil;-><init>(Ldxg;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 29

    .prologue
    .line 2843
    const/4 v3, 0x0

    .line 2844
    const/4 v10, 0x0

    .line 2845
    const/4 v2, 0x0

    .line 2847
    invoke-static {}, Lbya;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2848
    new-instance v4, Lbyc;

    invoke-direct {v4}, Lbyc;-><init>()V

    const-string v5, "sane_response"

    .line 2849
    invoke-virtual {v4, v5}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    move-result-object v4

    .line 2850
    invoke-virtual/range {p1 .. p1}, Lyt;->f()Lyj;

    move-result-object v5

    invoke-virtual {v4, v5}, Lbyc;->a(Lyj;)Lbyc;

    move-result-object v4

    .line 2851
    invoke-virtual {v4}, Lbyc;->b()V

    .line 2854
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lbil;->h:Z

    if-eqz v4, :cond_2

    .line 2855
    invoke-super/range {p0 .. p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 2857
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Force clear cache and resync for account:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2858
    invoke-virtual/range {p1 .. p1}, Lyt;->f()Lyj;

    move-result-object v4

    invoke-virtual {v4}, Lyj;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2857
    invoke-static {v2, v3}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 2859
    invoke-virtual/range {p1 .. p1}, Lyt;->a()V

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lyt;->r()V

    const-string v2, "last_successful_sync_time"

    const-wide/16 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Lyt;->h(Ljava/lang/String;J)V

    invoke-virtual/range {p1 .. p1}, Lyt;->t()V

    invoke-virtual/range {p1 .. p1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual/range {p1 .. p1}, Lyt;->c()V

    invoke-virtual/range {p1 .. p1}, Lyt;->f()Lyj;

    move-result-object v3

    invoke-static {v3}, Lbpf;->a(Lyj;)Lbpf;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lbil;->b:Lbea;

    check-cast v2, Lbfs;

    invoke-virtual {v2}, Lbfs;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lbpf;->c(Ljava/lang/String;)Z

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v3, v2, v4, v5, v6}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ZZIZ)V

    invoke-virtual {v3}, Lyj;->x()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v3}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v2

    const-string v4, "sms_last_sync_time_millis"

    invoke-virtual {v2, v4}, Lbsx;->e(Ljava/lang/String;)V

    invoke-static {v3}, Lbwf;->b(Lyj;)V

    .line 3091
    :cond_1
    return-void

    .line 2859
    :catchall_0
    move-exception v2

    invoke-virtual/range {p1 .. p1}, Lyt;->c()V

    throw v2

    .line 2863
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lbil;->b:Lbea;

    if-eqz v4, :cond_22

    .line 2864
    move-object/from16 v0, p0

    iget-object v2, v0, Lbil;->b:Lbea;

    check-cast v2, Lbfs;

    iget-boolean v3, v2, Lbfs;->c:Z

    .line 2866
    move-object/from16 v0, p0

    iget-object v2, v0, Lbil;->b:Lbea;

    check-cast v2, Lbfs;

    invoke-virtual {v2}, Lbfs;->i()Ljava/lang/String;

    move-result-object v2

    move-object v14, v2

    move v2, v3

    .line 2869
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lbil;->j:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbil;->c:Lbht;

    iget-wide v4, v4, Lbht;->d:J

    .line 2870
    invoke-static {v3, v2, v4, v5}, Lbck;->a(Ljava/util/ArrayList;ZJ)Ljava/util/List;

    move-result-object v4

    .line 2873
    invoke-super/range {p0 .. p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 2874
    const-wide/16 v2, 0x0

    .line 2875
    sget-boolean v5, Lyp;->a:Z

    if-eqz v5, :cond_21

    .line 2876
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    move-wide v15, v2

    .line 2878
    :goto_1
    const-string v2, "Babel"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2879
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "processSyncAllNewEventsResponse: count "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2880
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2879
    invoke-static {v2, v3}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2883
    :cond_3
    new-instance v21, Ljava/util/HashSet;

    invoke-direct/range {v21 .. v21}, Ljava/util/HashSet;-><init>()V

    .line 2884
    new-instance v22, Ljava/util/HashSet;

    invoke-direct/range {v22 .. v22}, Ljava/util/HashSet;-><init>()V

    .line 2885
    invoke-virtual/range {p1 .. p1}, Lyt;->a()V

    .line 2886
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v5, 0x3e8

    mul-long v23, v2, v5

    .line 2888
    :try_start_1
    const-string v2, "last_successful_sync_time"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lyt;->O(Ljava/lang/String;)J

    .line 2891
    const/4 v12, 0x1

    .line 2894
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lbil;->i:Z

    if-eqz v2, :cond_6

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    const-string v3, "babel_separate_gcr_threshold"

    const/4 v5, 0x5

    .line 2895
    invoke-static {v3, v5}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v3

    if-le v2, v3, :cond_6

    .line 2901
    const-string v2, "Babel"

    const-string v3, "Fall back to cold start, create SRC request from warm sync"

    invoke-static {v2, v3}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2904
    new-instance v2, Lbft;

    const/4 v3, 0x5

    invoke-direct {v2, v3, v14}, Lbft;-><init>(ILjava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lbnl;->a(Lbea;)V

    .line 2907
    new-instance v2, Lbft;

    const/4 v3, 0x6

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lbft;-><init>(ILjava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lbnl;->a(Lbea;)V

    .line 2910
    invoke-virtual/range {p1 .. p1}, Lyt;->r()V

    .line 2911
    invoke-virtual/range {p1 .. p1}, Lyt;->n()V

    .line 2912
    const-string v2, "last_successful_sync_time"

    const-wide/16 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Lyt;->h(Ljava/lang/String;J)V

    .line 2913
    const/4 v2, 0x1

    .line 3073
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lyt;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3075
    invoke-virtual/range {p1 .. p1}, Lyt;->c()V

    .line 3077
    sget-boolean v3, Lyp;->a:Z

    if-eqz v3, :cond_4

    .line 3078
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    .line 3079
    const-string v5, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "processSANE took "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long/2addr v3, v15

    invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3082
    :cond_4
    if-eqz v2, :cond_5

    .line 3083
    invoke-static/range {p1 .. p1}, Lyp;->d(Lyt;)V

    .line 3085
    :cond_5
    invoke-virtual/range {v21 .. v21}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 3086
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lyp;->d(Lyt;Ljava/lang/String;)V

    goto :goto_3

    .line 2915
    :cond_6
    :try_start_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v19

    .line 2917
    invoke-virtual/range {p1 .. p1}, Lyt;->f()Lyj;

    move-result-object v2

    invoke-static {v2}, Lbpf;->a(Lyj;)Lbpf;

    move-result-object v25

    .line 2919
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v26

    move-wide/from16 v3, v19

    :goto_4
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lbck;

    move-object v11, v0

    .line 2920
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    sub-long/2addr v5, v3

    .line 2921
    invoke-static {}, Lzo;->b()J

    move-result-wide v7

    cmp-long v2, v5, v7

    if-ltz v2, :cond_20

    .line 2922
    invoke-virtual/range {p1 .. p1}, Lyt;->d()V

    .line 2923
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    move-wide/from16 v19, v2

    .line 2926
    :goto_5
    iget-object v0, v11, Lbck;->a:Ljava/lang/String;

    move-object/from16 v27, v0

    .line 2927
    iget-wide v2, v11, Lbck;->g:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    .line 2928
    sget-boolean v2, Lbfz;->a:Z

    if-eqz v2, :cond_7

    .line 2929
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Conversation "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " has leaveTimestamp "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v11, Lbck;->g:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2932
    :cond_7
    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lyt;->i(Ljava/lang/String;)V

    move-wide/from16 v3, v19

    .line 2933
    goto :goto_4

    .line 2936
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lbil;->i:Z

    if-nez v2, :cond_1f

    .line 2938
    iget-object v0, v11, Lbck;->c:Ljava/util/List;

    move-object/from16 v17, v0

    .line 2939
    const/4 v2, 0x0

    .line 2940
    if-eqz v17, :cond_1e

    .line 2941
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v2

    move v3, v2

    .line 2946
    :goto_6
    const-string v2, "babel_crashifmissedpush"

    const/4 v4, 0x0

    .line 2947
    invoke-static {v2, v4}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v2

    .line 2950
    if-eqz v2, :cond_a

    if-lez v3, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lbil;->b:Lbea;

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lbil;->b:Lbea;

    check-cast v2, Lbfs;

    .line 2953
    invoke-virtual {v2}, Lbfs;->h()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2954
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Got unexpected missed events in SANE "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " sorttime "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v11, Lbck;->b:Lbjc;

    iget-wide v4, v4, Lbjc;->t:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 2957
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbjg;

    .line 2958
    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "  "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v2, Lbjg;->m:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " time "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v2, Lbjg;->e:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_7

    .line 3075
    :catchall_1
    move-exception v2

    invoke-virtual/range {p1 .. p1}, Lyt;->c()V

    throw v2

    .line 2960
    :cond_9
    :try_start_3
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Got unexpected missed events in SANE "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2967
    :cond_a
    sget-boolean v2, Lyp;->a:Z

    if-eqz v2, :cond_c

    .line 2968
    const-string v2, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "syncing conversation "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " events "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2978
    :cond_b
    :goto_8
    iget-object v2, v11, Lbck;->b:Lbjc;

    if-eqz v2, :cond_1d

    .line 2979
    iget-object v3, v11, Lbck;->b:Lbjc;

    iget-wide v4, v11, Lbck;->h:J

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p1

    move-object/from16 v7, p2

    invoke-static/range {v2 .. v9}, Lyp;->a(Lyt;Lbjc;JLjava/lang/String;Lbnl;Lys;Z)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 2984
    const/4 v10, 0x1

    move v13, v10

    .line 2992
    :goto_9
    if-eqz v17, :cond_10

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_10

    .line 2993
    const/4 v5, 0x0

    .line 2994
    const/4 v2, 0x0

    .line 2995
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    :goto_a
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbjg;

    .line 2996
    instance-of v4, v3, Lbji;

    if-eqz v4, :cond_d

    .line 2997
    const/4 v4, 0x1

    move/from16 v17, v2

    move/from16 v18, v4

    .line 3005
    :goto_b
    invoke-virtual/range {p0 .. p0}, Lbil;->d()J

    move-result-wide v5

    invoke-virtual/range {p0 .. p0}, Lbil;->e()J

    move-result-wide v7

    move-object/from16 v2, p1

    move-object/from16 v4, p2

    move-wide/from16 v9, v23

    .line 3004
    invoke-static/range {v2 .. v10}, Lyp;->a(Lyt;Lbjg;Lbnl;JJJ)V

    move/from16 v2, v17

    move/from16 v5, v18

    .line 3006
    goto :goto_a

    .line 2970
    :cond_c
    if-lez v3, :cond_b

    const-string v2, "Babel"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2971
    const-string v2, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "syncing conversation "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " events "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    .line 2998
    :cond_d
    instance-of v4, v3, Lbiv;

    if-eqz v4, :cond_1c

    .line 2999
    const/4 v4, 0x1

    .line 3000
    invoke-virtual/range {p1 .. p1}, Lyt;->f()Lyj;

    move-result-object v2

    invoke-static {v2, v14}, Lbil;->a(Lyj;Ljava/lang/String;)V

    .line 3001
    move-object v0, v3

    check-cast v0, Lbiv;

    move-object v2, v0

    const/4 v6, 0x2

    invoke-virtual {v2, v6}, Lbiv;->a(I)V

    move/from16 v17, v4

    move/from16 v18, v5

    goto :goto_b

    .line 3011
    :cond_e
    if-eqz v2, :cond_f

    .line 3012
    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 3014
    :cond_f
    if-eqz v5, :cond_10

    .line 3015
    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 3019
    :cond_10
    sget-boolean v2, Lyp;->a:Z

    if-eqz v2, :cond_11

    .line 3020
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mustQuerySeparately: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, v11, Lbck;->d:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_11
    move/from16 v18, v13

    .line 3024
    :goto_c
    iget-object v2, v11, Lbck;->b:Lbjc;

    if-nez v2, :cond_12

    iget-object v2, v11, Lbck;->a:Ljava/lang/String;

    .line 3025
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lyt;->o(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_15

    :cond_12
    const/4 v2, 0x1

    .line 3027
    :goto_d
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lbil;->i:Z

    if-nez v3, :cond_13

    if-nez v2, :cond_1b

    .line 3029
    :cond_13
    const/16 v17, 0x0

    .line 3030
    sget-boolean v2, Lyp;->a:Z

    if-eqz v2, :cond_14

    .line 3031
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "requesting more events for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v11, Lbck;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3042
    :cond_14
    new-instance v2, Lbek;

    iget-object v3, v11, Lbck;->a:Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v11, 0x0

    const/4 v13, 0x0

    move-object v10, v14

    invoke-direct/range {v2 .. v13}, Lbek;-><init>(Ljava/lang/String;ZZZ[BJLjava/lang/String;JLbjg;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lbnl;->a(Lbea;)V

    .line 3053
    move-object/from16 v0, v25

    invoke-virtual {v0, v14}, Lbpf;->a(Ljava/lang/String;)Z

    move/from16 v2, v17

    :goto_e
    move-wide/from16 v3, v19

    move v12, v2

    move/from16 v10, v18

    .line 3055
    goto/16 :goto_4

    .line 3025
    :cond_15
    const/4 v2, 0x0

    goto :goto_d

    .line 3057
    :cond_16
    if-eqz v12, :cond_18

    .line 3058
    sget-boolean v2, Lyp;->a:Z

    if-eqz v2, :cond_17

    .line 3059
    const-string v2, "Babel"

    const-string v3, "sync finished"

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3061
    :cond_17
    const-string v2, "last_successful_sync_time"

    move-object/from16 v0, p0

    iget-wide v3, v0, Lbil;->g:J

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Lyt;->h(Ljava/lang/String;J)V

    .line 3064
    move-object/from16 v0, v25

    invoke-virtual {v0, v14}, Lbpf;->c(Ljava/lang/String;)Z

    move v2, v10

    goto/16 :goto_2

    .line 3066
    :cond_18
    sget-boolean v2, Lyp;->a:Z

    if-eqz v2, :cond_19

    .line 3067
    const-string v2, "Babel"

    const-string v3, "sync not finished"

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3069
    :cond_19
    const-string v2, "in_progress_sync_time"

    move-object/from16 v0, p0

    iget-wide v3, v0, Lbil;->g:J

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Lyt;->h(Ljava/lang/String;J)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move v2, v10

    goto/16 :goto_2

    .line 3088
    :cond_1a
    invoke-virtual/range {v22 .. v22}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 3089
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lyp;->c(Lyt;Ljava/lang/String;)V

    goto :goto_f

    :cond_1b
    move v2, v12

    goto :goto_e

    :cond_1c
    move/from16 v17, v2

    move/from16 v18, v5

    goto/16 :goto_b

    :cond_1d
    move v13, v10

    goto/16 :goto_9

    :cond_1e
    move v3, v2

    goto/16 :goto_6

    :cond_1f
    move/from16 v18, v10

    goto/16 :goto_c

    :cond_20
    move-wide/from16 v19, v3

    goto/16 :goto_5

    :cond_21
    move-wide v15, v2

    goto/16 :goto_1

    :cond_22
    move-object v14, v2

    move v2, v3

    goto/16 :goto_0
.end method
