.class public Lbdg;
.super Lbcz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final a:Z

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final h:I


# direct methods
.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 120
    invoke-direct {p0}, Lbcz;-><init>()V

    .line 121
    iput-boolean p1, p0, Lbdg;->a:Z

    .line 122
    iput-object p2, p0, Lbdg;->b:Ljava/lang/String;

    .line 123
    iput-object p3, p0, Lbdg;->c:Ljava/lang/String;

    .line 124
    iput p4, p0, Lbdg;->h:I

    .line 125
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 5

    .prologue
    .line 129
    new-instance v0, Lddo;

    invoke-direct {v0}, Lddo;-><init>()V

    .line 130
    new-instance v1, Lddl;

    invoke-direct {v1}, Lddl;-><init>()V

    .line 131
    iget-boolean v2, p0, Lbdg;->a:Z

    if-eqz v2, :cond_0

    .line 132
    iget v2, p0, Lbdg;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lddl;->b:Ljava/lang/Integer;

    .line 141
    :goto_0
    iput-object v1, v0, Lddo;->b:Lddl;

    .line 143
    new-instance v1, Lddj;

    invoke-direct {v1}, Lddj;-><init>()V

    .line 145
    iput-object v0, v1, Lddj;->c:Lddo;

    .line 146
    return-object v1

    .line 134
    :cond_0
    iget-object v2, p0, Lbdg;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    invoke-static {v2}, Lcwz;->b(Z)V

    .line 135
    new-instance v2, Lddm;

    invoke-direct {v2}, Lddm;-><init>()V

    .line 136
    iget-object v3, p0, Lbdg;->b:Ljava/lang/String;

    iput-object v3, v2, Lddm;->b:Ljava/lang/String;

    .line 137
    iget-object v3, p0, Lbdg;->c:Ljava/lang/String;

    iput-object v3, v2, Lddm;->c:Ljava/lang/String;

    .line 138
    iget v3, p0, Lbdg;->h:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lddm;->d:Ljava/lang/Integer;

    .line 139
    const/4 v3, 0x1

    new-array v3, v3, [Lddm;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    iput-object v3, v1, Lddl;->d:[Lddm;

    goto :goto_0
.end method

.method public a(Lyj;Lbph;)V
    .locals 3

    .prologue
    .line 156
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SetChatAclSettingRequest failed for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 157
    invoke-virtual {p1}, Lyj;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 156
    invoke-static {v0, v1, p2}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 158
    return-void
.end method

.method public a(JI)Z
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    const-string v0, "setchatacls"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    const-string v0, "ui_queue"

    return-object v0
.end method
