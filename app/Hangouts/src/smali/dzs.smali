.class public final Ldzs;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldzs;


# instance fields
.field public b:Ldzu;

.field public c:Ldzt;

.field public d:Ldzv;

.field public e:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Ldzs;

    sput-object v0, Ldzs;->a:[Ldzs;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 288
    iput-object v0, p0, Ldzs;->b:Ldzu;

    .line 291
    iput-object v0, p0, Ldzs;->c:Ldzt;

    .line 294
    iput-object v0, p0, Ldzs;->d:Ldzv;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 319
    const/4 v0, 0x0

    .line 320
    iget-object v1, p0, Ldzs;->b:Ldzu;

    if-eqz v1, :cond_0

    .line 321
    const/4 v0, 0x1

    iget-object v1, p0, Ldzs;->b:Ldzu;

    .line 322
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 324
    :cond_0
    iget-object v1, p0, Ldzs;->c:Ldzt;

    if-eqz v1, :cond_1

    .line 325
    const/4 v1, 0x2

    iget-object v2, p0, Ldzs;->c:Ldzt;

    .line 326
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 328
    :cond_1
    iget-object v1, p0, Ldzs;->d:Ldzv;

    if-eqz v1, :cond_2

    .line 329
    const/4 v1, 0x3

    iget-object v2, p0, Ldzs;->d:Ldzv;

    .line 330
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 332
    :cond_2
    iget-object v1, p0, Ldzs;->e:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 333
    const/4 v1, 0x4

    iget-object v2, p0, Ldzs;->e:Ljava/lang/Long;

    .line 334
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 336
    :cond_3
    iget-object v1, p0, Ldzs;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 337
    iput v0, p0, Ldzs;->cachedSize:I

    .line 338
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldzs;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldzs;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldzs;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldzs;->b:Ldzu;

    if-nez v0, :cond_2

    new-instance v0, Ldzu;

    invoke-direct {v0}, Ldzu;-><init>()V

    iput-object v0, p0, Ldzs;->b:Ldzu;

    :cond_2
    iget-object v0, p0, Ldzs;->b:Ldzu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldzs;->c:Ldzt;

    if-nez v0, :cond_3

    new-instance v0, Ldzt;

    invoke-direct {v0}, Ldzt;-><init>()V

    iput-object v0, p0, Ldzs;->c:Ldzt;

    :cond_3
    iget-object v0, p0, Ldzs;->c:Ldzt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldzs;->d:Ldzv;

    if-nez v0, :cond_4

    new-instance v0, Ldzv;

    invoke-direct {v0}, Ldzv;-><init>()V

    iput-object v0, p0, Ldzs;->d:Ldzv;

    :cond_4
    iget-object v0, p0, Ldzs;->d:Ldzv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldzs;->e:Ljava/lang/Long;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 301
    iget-object v0, p0, Ldzs;->b:Ldzu;

    if-eqz v0, :cond_0

    .line 302
    const/4 v0, 0x1

    iget-object v1, p0, Ldzs;->b:Ldzu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 304
    :cond_0
    iget-object v0, p0, Ldzs;->c:Ldzt;

    if-eqz v0, :cond_1

    .line 305
    const/4 v0, 0x2

    iget-object v1, p0, Ldzs;->c:Ldzt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 307
    :cond_1
    iget-object v0, p0, Ldzs;->d:Ldzv;

    if-eqz v0, :cond_2

    .line 308
    const/4 v0, 0x3

    iget-object v1, p0, Ldzs;->d:Ldzv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 310
    :cond_2
    iget-object v0, p0, Ldzs;->e:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 311
    const/4 v0, 0x4

    iget-object v1, p0, Ldzs;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 313
    :cond_3
    iget-object v0, p0, Ldzs;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 315
    return-void
.end method
