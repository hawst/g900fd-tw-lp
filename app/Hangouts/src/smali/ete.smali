.class public final Lete;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lete;


# instance fields
.field public b:Lesw;

.field public c:Ljava/lang/Integer;

.field public d:Letf;

.field public e:Ljava/lang/Integer;

.field public f:[Letb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 309
    const/4 v0, 0x0

    new-array v0, v0, [Lete;

    sput-object v0, Lete;->a:[Lete;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 310
    invoke-direct {p0}, Lepn;-><init>()V

    .line 408
    iput-object v0, p0, Lete;->b:Lesw;

    .line 411
    iput-object v0, p0, Lete;->c:Ljava/lang/Integer;

    .line 414
    iput-object v0, p0, Lete;->d:Letf;

    .line 419
    sget-object v0, Letb;->a:[Letb;

    iput-object v0, p0, Lete;->f:[Letb;

    .line 310
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 450
    iget-object v0, p0, Lete;->b:Lesw;

    if-eqz v0, :cond_5

    .line 451
    const/4 v0, 0x2

    iget-object v2, p0, Lete;->b:Lesw;

    .line 452
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 454
    :goto_0
    iget-object v2, p0, Lete;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 455
    const/4 v2, 0x5

    iget-object v3, p0, Lete;->c:Ljava/lang/Integer;

    .line 456
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 458
    :cond_0
    iget-object v2, p0, Lete;->d:Letf;

    if-eqz v2, :cond_1

    .line 459
    const/4 v2, 0x6

    iget-object v3, p0, Lete;->d:Letf;

    .line 460
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 462
    :cond_1
    iget-object v2, p0, Lete;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 463
    const/4 v2, 0x7

    iget-object v3, p0, Lete;->e:Ljava/lang/Integer;

    .line 464
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 466
    :cond_2
    iget-object v2, p0, Lete;->f:[Letb;

    if-eqz v2, :cond_4

    .line 467
    iget-object v2, p0, Lete;->f:[Letb;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 468
    if-eqz v4, :cond_3

    .line 469
    const/16 v5, 0x8

    .line 470
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 467
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 474
    :cond_4
    iget-object v1, p0, Lete;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 475
    iput v0, p0, Lete;->cachedSize:I

    .line 476
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 306
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lete;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lete;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lete;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lete;->b:Lesw;

    if-nez v0, :cond_2

    new-instance v0, Lesw;

    invoke-direct {v0}, Lesw;-><init>()V

    iput-object v0, p0, Lete;->b:Lesw;

    :cond_2
    iget-object v0, p0, Lete;->b:Lesw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lete;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lete;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lete;->d:Letf;

    if-nez v0, :cond_5

    new-instance v0, Letf;

    invoke-direct {v0}, Letf;-><init>()V

    iput-object v0, p0, Lete;->d:Letf;

    :cond_5
    iget-object v0, p0, Lete;->d:Letf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lete;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lete;->f:[Letb;

    if-nez v0, :cond_7

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Letb;

    iget-object v3, p0, Lete;->f:[Letb;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lete;->f:[Letb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Lete;->f:[Letb;

    :goto_2
    iget-object v2, p0, Lete;->f:[Letb;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Lete;->f:[Letb;

    new-instance v3, Letb;

    invoke-direct {v3}, Letb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lete;->f:[Letb;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lete;->f:[Letb;

    array-length v0, v0

    goto :goto_1

    :cond_8
    iget-object v2, p0, Lete;->f:[Letb;

    new-instance v3, Letb;

    invoke-direct {v3}, Letb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lete;->f:[Letb;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x28 -> :sswitch_2
        0x32 -> :sswitch_3
        0x38 -> :sswitch_4
        0x42 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 424
    iget-object v0, p0, Lete;->b:Lesw;

    if-eqz v0, :cond_0

    .line 425
    const/4 v0, 0x2

    iget-object v1, p0, Lete;->b:Lesw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 427
    :cond_0
    iget-object v0, p0, Lete;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 428
    const/4 v0, 0x5

    iget-object v1, p0, Lete;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 430
    :cond_1
    iget-object v0, p0, Lete;->d:Letf;

    if-eqz v0, :cond_2

    .line 431
    const/4 v0, 0x6

    iget-object v1, p0, Lete;->d:Letf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 433
    :cond_2
    iget-object v0, p0, Lete;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 434
    const/4 v0, 0x7

    iget-object v1, p0, Lete;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 436
    :cond_3
    iget-object v0, p0, Lete;->f:[Letb;

    if-eqz v0, :cond_5

    .line 437
    iget-object v1, p0, Lete;->f:[Letb;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 438
    if-eqz v3, :cond_4

    .line 439
    const/16 v4, 0x8

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 437
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 443
    :cond_5
    iget-object v0, p0, Lete;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 445
    return-void
.end method
