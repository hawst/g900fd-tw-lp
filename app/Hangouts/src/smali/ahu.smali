.class public final Lahu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Laac;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 0

    .prologue
    .line 6324
    iput-object p1, p0, Lahu;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;B)V
    .locals 0

    .prologue
    .line 6324
    invoke-direct {p0, p1}, Lahu;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    return-void
.end method


# virtual methods
.method public a(Lbzn;Lbyd;ZLzx;Z)V
    .locals 3

    .prologue
    .line 6330
    iget-object v0, p0, Lahu;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->av(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lzx;

    move-result-object v0

    if-eq p4, v0, :cond_1

    .line 6332
    if-eqz p1, :cond_0

    .line 6333
    invoke-virtual {p1}, Lbzn;->b()V

    .line 6365
    :cond_0
    :goto_0
    return-void

    .line 6338
    :cond_1
    iget-object v0, p0, Lahu;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aw(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lzx;

    .line 6341
    if-eqz p3, :cond_4

    .line 6342
    if-eqz p2, :cond_3

    .line 6343
    iget-object v0, p0, Lahu;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    new-instance v1, Lccm;

    invoke-direct {v1, p2}, Lccm;-><init>(Lbyd;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lccm;)Lccm;

    .line 6344
    iget-object v0, p0, Lahu;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lahu;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lccm;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 6345
    iget-object v0, p0, Lahu;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lccm;

    move-result-object v0

    invoke-virtual {v0}, Lccm;->a()V

    .line 6346
    iget-object v0, p0, Lahu;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {p2}, Lbyd;->c()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;I)I

    .line 6347
    iget-object v0, p0, Lahu;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {p2}, Lbyd;->d()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->e(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;I)I

    .line 6350
    iget-object v0, p0, Lahu;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->M(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 6351
    iget-object v0, p0, Lahu;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    const-string v1, "image/gif"

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->e(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 6359
    :cond_2
    :goto_1
    iget-object v0, p0, Lahu;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ay(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    goto :goto_0

    .line 6354
    :cond_3
    invoke-virtual {p1}, Lbzn;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 6355
    iget-object v1, p0, Lahu;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;I)I

    .line 6356
    iget-object v1, p0, Lahu;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->e(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;I)I

    .line 6357
    iget-object v1, p0, Lahu;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 6361
    :cond_4
    iget-object v0, p0, Lahu;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->L()V

    .line 6362
    iget-object v0, p0, Lahu;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lh;->t:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 6363
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0
.end method
