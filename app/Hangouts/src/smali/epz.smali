.class public final Lepz;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lepz;


# instance fields
.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:[Ljava/lang/String;

.field public g:[Leqa;

.field public h:[Lepx;

.field public i:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 555
    const/4 v0, 0x0

    new-array v0, v0, [Lepz;

    sput-object v0, Lepz;->a:[Lepz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 556
    invoke-direct {p0}, Lepn;-><init>()V

    .line 567
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Lepz;->f:[Ljava/lang/String;

    .line 570
    sget-object v0, Leqa;->a:[Leqa;

    iput-object v0, p0, Lepz;->g:[Leqa;

    .line 573
    sget-object v0, Lepx;->a:[Lepx;

    iput-object v0, p0, Lepz;->h:[Lepx;

    .line 556
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 621
    iget-object v0, p0, Lepz;->b:Ljava/lang/Long;

    if-eqz v0, :cond_a

    .line 622
    const/4 v0, 0x1

    iget-object v2, p0, Lepz;->b:Ljava/lang/Long;

    .line 623
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lepl;->d(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 625
    :goto_0
    iget-object v2, p0, Lepz;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 626
    const/4 v2, 0x2

    iget-object v3, p0, Lepz;->c:Ljava/lang/String;

    .line 627
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 629
    :cond_0
    iget-object v2, p0, Lepz;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 630
    const/4 v2, 0x3

    iget-object v3, p0, Lepz;->d:Ljava/lang/String;

    .line 631
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 633
    :cond_1
    iget-object v2, p0, Lepz;->e:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 634
    const/4 v2, 0x4

    iget-object v3, p0, Lepz;->e:Ljava/lang/String;

    .line 635
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 637
    :cond_2
    iget-object v2, p0, Lepz;->f:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lepz;->f:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 639
    iget-object v4, p0, Lepz;->f:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_3

    aget-object v6, v4, v2

    .line 641
    invoke-static {v6}, Lepl;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 639
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 643
    :cond_3
    add-int/2addr v0, v3

    .line 644
    iget-object v2, p0, Lepz;->f:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 646
    :cond_4
    iget-object v2, p0, Lepz;->g:[Leqa;

    if-eqz v2, :cond_6

    .line 647
    iget-object v3, p0, Lepz;->g:[Leqa;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    .line 648
    if-eqz v5, :cond_5

    .line 649
    const/4 v6, 0x6

    .line 650
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 647
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 654
    :cond_6
    iget-object v2, p0, Lepz;->h:[Lepx;

    if-eqz v2, :cond_8

    .line 655
    iget-object v2, p0, Lepz;->h:[Lepx;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 656
    if-eqz v4, :cond_7

    .line 657
    const/4 v5, 0x7

    .line 658
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 655
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 662
    :cond_8
    iget-object v1, p0, Lepz;->i:[B

    if-eqz v1, :cond_9

    .line 663
    const/16 v1, 0x8

    iget-object v2, p0, Lepz;->i:[B

    .line 664
    invoke-static {v1, v2}, Lepl;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 666
    :cond_9
    iget-object v1, p0, Lepz;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 667
    iput v0, p0, Lepz;->cachedSize:I

    .line 668
    return v0

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 552
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lepz;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lepz;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lepz;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lepz;->b:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepz;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepz;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepz;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lepz;->f:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lepz;->f:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lepz;->f:[Ljava/lang/String;

    :goto_1
    iget-object v2, p0, Lepz;->f:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lepz;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lepz;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lepz;->g:[Leqa;

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Leqa;

    iget-object v3, p0, Lepz;->g:[Leqa;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lepz;->g:[Leqa;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lepz;->g:[Leqa;

    :goto_3
    iget-object v2, p0, Lepz;->g:[Leqa;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lepz;->g:[Leqa;

    new-instance v3, Leqa;

    invoke-direct {v3}, Leqa;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lepz;->g:[Leqa;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lepz;->g:[Leqa;

    array-length v0, v0

    goto :goto_2

    :cond_5
    iget-object v2, p0, Lepz;->g:[Leqa;

    new-instance v3, Leqa;

    invoke-direct {v3}, Leqa;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lepz;->g:[Leqa;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lepz;->h:[Lepx;

    if-nez v0, :cond_7

    move v0, v1

    :goto_4
    add-int/2addr v2, v0

    new-array v2, v2, [Lepx;

    iget-object v3, p0, Lepz;->h:[Lepx;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lepz;->h:[Lepx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Lepz;->h:[Lepx;

    :goto_5
    iget-object v2, p0, Lepz;->h:[Lepx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Lepz;->h:[Lepx;

    new-instance v3, Lepx;

    invoke-direct {v3}, Lepx;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lepz;->h:[Lepx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_7
    iget-object v0, p0, Lepz;->h:[Lepx;

    array-length v0, v0

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lepz;->h:[Lepx;

    new-instance v3, Lepx;

    invoke-direct {v3}, Lepx;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lepz;->h:[Lepx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Lepz;->i:[B

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 580
    iget-object v1, p0, Lepz;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 581
    const/4 v1, 0x1

    iget-object v2, p0, Lepz;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->a(IJ)V

    .line 583
    :cond_0
    iget-object v1, p0, Lepz;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 584
    const/4 v1, 0x2

    iget-object v2, p0, Lepz;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 586
    :cond_1
    iget-object v1, p0, Lepz;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 587
    const/4 v1, 0x3

    iget-object v2, p0, Lepz;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 589
    :cond_2
    iget-object v1, p0, Lepz;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 590
    const/4 v1, 0x4

    iget-object v2, p0, Lepz;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 592
    :cond_3
    iget-object v1, p0, Lepz;->f:[Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 593
    iget-object v2, p0, Lepz;->f:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 594
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 593
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 597
    :cond_4
    iget-object v1, p0, Lepz;->g:[Leqa;

    if-eqz v1, :cond_6

    .line 598
    iget-object v2, p0, Lepz;->g:[Leqa;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 599
    if-eqz v4, :cond_5

    .line 600
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 598
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 604
    :cond_6
    iget-object v1, p0, Lepz;->h:[Lepx;

    if-eqz v1, :cond_8

    .line 605
    iget-object v1, p0, Lepz;->h:[Lepx;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    .line 606
    if-eqz v3, :cond_7

    .line 607
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 605
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 611
    :cond_8
    iget-object v0, p0, Lepz;->i:[B

    if-eqz v0, :cond_9

    .line 612
    const/16 v0, 0x8

    iget-object v1, p0, Lepz;->i:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 614
    :cond_9
    iget-object v0, p0, Lepz;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 616
    return-void
.end method
