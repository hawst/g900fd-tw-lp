.class public final Ldxv;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldxv;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ldye;

.field public i:Ljava/lang/Integer;

.field public j:Ldya;

.field public k:Ldxy;

.field public l:Ldxz;

.field public m:Ldyb;

.field public n:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Ldxv;

    sput-object v0, Ldxv;->a:[Ldxv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 925
    iput-object v0, p0, Ldxv;->e:Ljava/lang/Integer;

    .line 932
    iput-object v0, p0, Ldxv;->h:Ldye;

    .line 935
    iput-object v0, p0, Ldxv;->i:Ljava/lang/Integer;

    .line 938
    iput-object v0, p0, Ldxv;->j:Ldya;

    .line 941
    iput-object v0, p0, Ldxv;->k:Ldxy;

    .line 944
    iput-object v0, p0, Ldxv;->l:Ldxz;

    .line 947
    iput-object v0, p0, Ldxv;->m:Ldyb;

    .line 950
    iput-object v0, p0, Ldxv;->n:Ljava/lang/Integer;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1000
    const/4 v0, 0x0

    .line 1001
    iget-object v1, p0, Ldxv;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1002
    const/4 v0, 0x1

    iget-object v1, p0, Ldxv;->b:Ljava/lang/String;

    .line 1003
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1005
    :cond_0
    iget-object v1, p0, Ldxv;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1006
    const/4 v1, 0x2

    iget-object v2, p0, Ldxv;->c:Ljava/lang/String;

    .line 1007
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1009
    :cond_1
    iget-object v1, p0, Ldxv;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1010
    const/4 v1, 0x3

    iget-object v2, p0, Ldxv;->d:Ljava/lang/String;

    .line 1011
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1013
    :cond_2
    iget-object v1, p0, Ldxv;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1014
    const/4 v1, 0x4

    iget-object v2, p0, Ldxv;->e:Ljava/lang/Integer;

    .line 1015
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1017
    :cond_3
    iget-object v1, p0, Ldxv;->g:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1018
    const/4 v1, 0x5

    iget-object v2, p0, Ldxv;->g:Ljava/lang/String;

    .line 1019
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1021
    :cond_4
    iget-object v1, p0, Ldxv;->h:Ldye;

    if-eqz v1, :cond_5

    .line 1022
    const/4 v1, 0x6

    iget-object v2, p0, Ldxv;->h:Ldye;

    .line 1023
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1025
    :cond_5
    iget-object v1, p0, Ldxv;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 1026
    const/4 v1, 0x7

    iget-object v2, p0, Ldxv;->i:Ljava/lang/Integer;

    .line 1027
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1029
    :cond_6
    iget-object v1, p0, Ldxv;->j:Ldya;

    if-eqz v1, :cond_7

    .line 1030
    const/16 v1, 0x8

    iget-object v2, p0, Ldxv;->j:Ldya;

    .line 1031
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1033
    :cond_7
    iget-object v1, p0, Ldxv;->k:Ldxy;

    if-eqz v1, :cond_8

    .line 1034
    const/16 v1, 0x9

    iget-object v2, p0, Ldxv;->k:Ldxy;

    .line 1035
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1037
    :cond_8
    iget-object v1, p0, Ldxv;->f:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 1038
    const/16 v1, 0xa

    iget-object v2, p0, Ldxv;->f:Ljava/lang/String;

    .line 1039
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1041
    :cond_9
    iget-object v1, p0, Ldxv;->l:Ldxz;

    if-eqz v1, :cond_a

    .line 1042
    const/16 v1, 0xb

    iget-object v2, p0, Ldxv;->l:Ldxz;

    .line 1043
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1045
    :cond_a
    iget-object v1, p0, Ldxv;->m:Ldyb;

    if-eqz v1, :cond_b

    .line 1046
    const/16 v1, 0xc

    iget-object v2, p0, Ldxv;->m:Ldyb;

    .line 1047
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1049
    :cond_b
    iget-object v1, p0, Ldxv;->n:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 1050
    const/16 v1, 0xd

    iget-object v2, p0, Ldxv;->n:Ljava/lang/Integer;

    .line 1051
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1053
    :cond_c
    iget-object v1, p0, Ldxv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1054
    iput v0, p0, Ldxv;->cachedSize:I

    .line 1055
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldxv;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldxv;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldxv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldxv;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldxv;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldxv;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    if-ne v0, v5, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldxv;->e:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldxv;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldxv;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ldxv;->h:Ldye;

    if-nez v0, :cond_4

    new-instance v0, Ldye;

    invoke-direct {v0}, Ldye;-><init>()V

    iput-object v0, p0, Ldxv;->h:Ldye;

    :cond_4
    iget-object v0, p0, Ldxv;->h:Ldye;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_5

    if-eq v0, v3, :cond_5

    if-eq v0, v4, :cond_5

    if-ne v0, v5, :cond_6

    :cond_5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldxv;->i:Ljava/lang/Integer;

    goto :goto_0

    :cond_6
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldxv;->i:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Ldxv;->j:Ldya;

    if-nez v0, :cond_7

    new-instance v0, Ldya;

    invoke-direct {v0}, Ldya;-><init>()V

    iput-object v0, p0, Ldxv;->j:Ldya;

    :cond_7
    iget-object v0, p0, Ldxv;->j:Ldya;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Ldxv;->k:Ldxy;

    if-nez v0, :cond_8

    new-instance v0, Ldxy;

    invoke-direct {v0}, Ldxy;-><init>()V

    iput-object v0, p0, Ldxv;->k:Ldxy;

    :cond_8
    iget-object v0, p0, Ldxv;->k:Ldxy;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldxv;->f:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Ldxv;->l:Ldxz;

    if-nez v0, :cond_9

    new-instance v0, Ldxz;

    invoke-direct {v0}, Ldxz;-><init>()V

    iput-object v0, p0, Ldxv;->l:Ldxz;

    :cond_9
    iget-object v0, p0, Ldxv;->l:Ldxz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Ldxv;->m:Ldyb;

    if-nez v0, :cond_a

    new-instance v0, Ldyb;

    invoke-direct {v0}, Ldyb;-><init>()V

    iput-object v0, p0, Ldxv;->m:Ldyb;

    :cond_a
    iget-object v0, p0, Ldxv;->m:Ldyb;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_b

    if-eq v0, v3, :cond_b

    if-eq v0, v4, :cond_b

    if-eq v0, v5, :cond_b

    const/4 v1, 0x4

    if-eq v0, v1, :cond_b

    const/4 v1, 0x5

    if-eq v0, v1, :cond_b

    const/4 v1, 0x6

    if-eq v0, v1, :cond_b

    const/4 v1, 0x7

    if-eq v0, v1, :cond_b

    const/16 v1, 0x8

    if-ne v0, v1, :cond_c

    :cond_b
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldxv;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_c
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldxv;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 955
    iget-object v0, p0, Ldxv;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 956
    const/4 v0, 0x1

    iget-object v1, p0, Ldxv;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 958
    :cond_0
    iget-object v0, p0, Ldxv;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 959
    const/4 v0, 0x2

    iget-object v1, p0, Ldxv;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 961
    :cond_1
    iget-object v0, p0, Ldxv;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 962
    const/4 v0, 0x3

    iget-object v1, p0, Ldxv;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 964
    :cond_2
    iget-object v0, p0, Ldxv;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 965
    const/4 v0, 0x4

    iget-object v1, p0, Ldxv;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 967
    :cond_3
    iget-object v0, p0, Ldxv;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 968
    const/4 v0, 0x5

    iget-object v1, p0, Ldxv;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 970
    :cond_4
    iget-object v0, p0, Ldxv;->h:Ldye;

    if-eqz v0, :cond_5

    .line 971
    const/4 v0, 0x6

    iget-object v1, p0, Ldxv;->h:Ldye;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 973
    :cond_5
    iget-object v0, p0, Ldxv;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 974
    const/4 v0, 0x7

    iget-object v1, p0, Ldxv;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 976
    :cond_6
    iget-object v0, p0, Ldxv;->j:Ldya;

    if-eqz v0, :cond_7

    .line 977
    const/16 v0, 0x8

    iget-object v1, p0, Ldxv;->j:Ldya;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 979
    :cond_7
    iget-object v0, p0, Ldxv;->k:Ldxy;

    if-eqz v0, :cond_8

    .line 980
    const/16 v0, 0x9

    iget-object v1, p0, Ldxv;->k:Ldxy;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 982
    :cond_8
    iget-object v0, p0, Ldxv;->f:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 983
    const/16 v0, 0xa

    iget-object v1, p0, Ldxv;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 985
    :cond_9
    iget-object v0, p0, Ldxv;->l:Ldxz;

    if-eqz v0, :cond_a

    .line 986
    const/16 v0, 0xb

    iget-object v1, p0, Ldxv;->l:Ldxz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 988
    :cond_a
    iget-object v0, p0, Ldxv;->m:Ldyb;

    if-eqz v0, :cond_b

    .line 989
    const/16 v0, 0xc

    iget-object v1, p0, Ldxv;->m:Ldyb;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 991
    :cond_b
    iget-object v0, p0, Ldxv;->n:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    .line 992
    const/16 v0, 0xd

    iget-object v1, p0, Ldxv;->n:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 994
    :cond_c
    iget-object v0, p0, Ldxv;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 996
    return-void
.end method
