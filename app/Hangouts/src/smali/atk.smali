.class public final Latk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lato;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Latk;->a:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 225
    iget-object v0, p0, Latk;->a:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->b(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 226
    :try_start_0
    iget-object v0, p0, Latk;->a:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->e(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;)V

    .line 229
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v0, v2, :cond_0

    .line 230
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 231
    iget-object v2, p0, Latk;->a:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->f(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;)Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 233
    iget-object v2, p0, Latk;->a:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget-object v3, p0, Latk;->a:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->f(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;)Landroid/widget/FrameLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v3

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v2, v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;Z)Z

    .line 237
    :cond_0
    iget-object v0, p0, Latk;->a:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a:Latd;

    invoke-virtual {v0}, Latd;->h()Laqv;

    move-result-object v0

    invoke-virtual {v0}, Laqv;->i()V

    .line 238
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 233
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 238
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
