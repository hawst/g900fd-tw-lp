.class public final Laos;
.super Lpb;
.source "PG"

# interfaces
.implements Lpc;
.implements Lpd;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;Landroid/content/Context;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 48
    iput-object p1, p0, Laos;->a:Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;

    .line 49
    invoke-direct {p0, p2, p3}, Lpb;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 50
    invoke-virtual {p0}, Laos;->a()Landroid/view/Menu;

    move-result-object v1

    .line 51
    invoke-virtual {p0}, Laos;->b()Landroid/view/MenuInflater;

    move-result-object v0

    sget v2, Lf;->gV:I

    invoke-virtual {v0, v2, v1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 54
    sget-object v0, Laor;->a:[I

    invoke-static {p1}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->b(Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;)Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 62
    :pswitch_0
    sget-object v0, Lcom/google/android/libraries/hangouts/video/AudioDevice;->SPEAKERPHONE:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    .line 72
    :goto_0
    sget v2, Lg;->ev:I

    sget-object v3, Lcom/google/android/libraries/hangouts/video/AudioDevice;->SPEAKERPHONE:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-direct {p0, v1, v2, v3, v0}, Laos;->a(Landroid/view/Menu;ILcom/google/android/libraries/hangouts/video/AudioDevice;Lcom/google/android/libraries/hangouts/video/AudioDevice;)V

    .line 74
    sget v2, Lg;->ew:I

    sget-object v3, Lcom/google/android/libraries/hangouts/video/AudioDevice;->WIRED_HEADSET:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-direct {p0, v1, v2, v3, v0}, Laos;->a(Landroid/view/Menu;ILcom/google/android/libraries/hangouts/video/AudioDevice;Lcom/google/android/libraries/hangouts/video/AudioDevice;)V

    .line 76
    sget v2, Lg;->eu:I

    sget-object v3, Lcom/google/android/libraries/hangouts/video/AudioDevice;->EARPIECE:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-direct {p0, v1, v2, v3, v0}, Laos;->a(Landroid/view/Menu;ILcom/google/android/libraries/hangouts/video/AudioDevice;Lcom/google/android/libraries/hangouts/video/AudioDevice;)V

    .line 78
    sget v2, Lg;->et:I

    sget-object v3, Lcom/google/android/libraries/hangouts/video/AudioDevice;->BLUETOOTH_HEADSET:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-direct {p0, v1, v2, v3, v0}, Laos;->a(Landroid/view/Menu;ILcom/google/android/libraries/hangouts/video/AudioDevice;Lcom/google/android/libraries/hangouts/video/AudioDevice;)V

    .line 81
    invoke-virtual {p0, p0}, Laos;->a(Lpd;)V

    .line 82
    invoke-virtual {p0, p0}, Laos;->a(Lpc;)V

    .line 83
    return-void

    .line 58
    :pswitch_1
    sget-object v0, Lcom/google/android/libraries/hangouts/video/AudioDevice;->BLUETOOTH_HEADSET:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    goto :goto_0

    .line 65
    :pswitch_2
    sget-object v0, Lcom/google/android/libraries/hangouts/video/AudioDevice;->EARPIECE:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    goto :goto_0

    .line 68
    :pswitch_3
    sget-object v0, Lcom/google/android/libraries/hangouts/video/AudioDevice;->WIRED_HEADSET:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    goto :goto_0

    .line 54
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Landroid/view/Menu;ILcom/google/android/libraries/hangouts/video/AudioDevice;Lcom/google/android/libraries/hangouts/video/AudioDevice;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 88
    invoke-interface {p1, p2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 92
    iget-object v0, p0, Laos;->a:Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->b(Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;)Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->a(Lcom/google/android/libraries/hangouts/video/AudioDeviceState;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Laos;->a:Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;

    .line 93
    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->c(Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 92
    :goto_0
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 94
    iget-object v0, p0, Laos;->a:Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->c(Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 98
    if-ne p4, p3, :cond_0

    .line 99
    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 101
    :cond_0
    return-void

    .line 93
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 105
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    .line 106
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lg;->ev:I

    if-ne v1, v2, :cond_1

    .line 107
    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->SPEAKERPHONE:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->setAudioDevice(Lcom/google/android/libraries/hangouts/video/AudioDevice;)V

    .line 115
    :cond_0
    :goto_0
    iget-object v0, p0, Laos;->a:Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->d(Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;)Laos;

    .line 116
    const/4 v0, 0x1

    return v0

    .line 108
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lg;->ew:I

    if-ne v1, v2, :cond_2

    .line 109
    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->WIRED_HEADSET:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->setAudioDevice(Lcom/google/android/libraries/hangouts/video/AudioDevice;)V

    goto :goto_0

    .line 110
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lg;->eu:I

    if-ne v1, v2, :cond_3

    .line 111
    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->EARPIECE:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->setAudioDevice(Lcom/google/android/libraries/hangouts/video/AudioDevice;)V

    goto :goto_0

    .line 112
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lg;->et:I

    if-ne v1, v2, :cond_0

    .line 113
    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->BLUETOOTH_HEADSET:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->setAudioDevice(Lcom/google/android/libraries/hangouts/video/AudioDevice;)V

    goto :goto_0
.end method

.method public k_()V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Laos;->a:Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->d(Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;)Laos;

    .line 122
    iget-object v0, p0, Laos;->a:Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->e(Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;)Lkg;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Laos;->a:Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->e(Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;)Lkg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lkg;->a(Z)V

    .line 125
    :cond_0
    return-void
.end method
