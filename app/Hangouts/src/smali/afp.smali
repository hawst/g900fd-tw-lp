.class public final Lafp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 0

    .prologue
    .line 4008
    iput-object p1, p0, Lafp;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 4013
    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aa()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4014
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " onScroll   mInitialLoadFinished: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lafp;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 4015
    invoke-static {v4}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->O(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Z

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "  mMessagesLimit: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lafp;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 4016
    invoke-static {v4}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->P(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "  firstVisible: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "  mFirstLocalEventTimestamp: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lafp;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 4018
    invoke-static {v4}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Q(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 4014
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4021
    :cond_0
    iget-object v0, p0, Lafp;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0, p2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;I)V

    .line 4023
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 4024
    if-eqz v0, :cond_1

    .line 4025
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 4026
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getHeight()I

    move-result v4

    .line 4027
    add-int v1, p2, p3

    if-ne v1, p4, :cond_2

    move v1, v2

    .line 4028
    :goto_0
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xb

    if-lt v5, v6, :cond_4

    .line 4030
    if-eqz v1, :cond_3

    iget-object v5, p0, Lafp;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v5}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->n(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->d()Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lafp;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v5}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->R()Z

    move-result v5

    if-nez v5, :cond_3

    .line 4031
    iget-object v5, p0, Lafp;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v5}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->n(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    move-result-object v5

    sub-int/2addr v0, v4

    invoke-virtual {v5, v0}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(I)V

    .line 4052
    :goto_1
    iget-object v0, p0, Lafp;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    if-nez v1, :cond_7

    :goto_2
    invoke-virtual {v0, v2}, Laad;->a(Z)V

    .line 4054
    :cond_1
    return-void

    :cond_2
    move v1, v3

    .line 4027
    goto :goto_0

    .line 4033
    :cond_3
    iget-object v0, p0, Lafp;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->n(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a()V

    goto :goto_1

    .line 4038
    :cond_4
    if-eqz v1, :cond_6

    .line 4039
    if-gt v0, v4, :cond_5

    iget-object v0, p0, Lafp;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->R()Z

    move-result v0

    if-nez v0, :cond_5

    .line 4041
    iget-object v0, p0, Lafp;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->n(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->c()V

    goto :goto_1

    .line 4043
    :cond_5
    iget-object v0, p0, Lafp;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->n(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->b()V

    goto :goto_1

    .line 4046
    :cond_6
    iget-object v0, p0, Lafp;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->n(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->b()V

    goto :goto_1

    :cond_7
    move v2, v3

    .line 4052
    goto :goto_2
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    .line 4062
    packed-switch p2, :pswitch_data_0

    .line 4080
    :cond_0
    :goto_0
    return-void

    .line 4065
    :pswitch_0
    iget-object v0, p0, Lafp;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Laad;->b(Z)V

    .line 4066
    iget-object v0, p0, Lafp;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v0

    check-cast v0, Lakn;

    invoke-virtual {v0}, Lakn;->l()V

    goto :goto_0

    .line 4070
    :pswitch_1
    iget-object v0, p0, Lafp;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Laad;->b(Z)V

    .line 4074
    iget-object v0, p0, Lafp;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;

    .line 4075
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->s()V

    goto :goto_1

    .line 4062
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
