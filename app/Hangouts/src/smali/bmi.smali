.class public final Lbmi;
.super Lbmf;
.source "PG"


# direct methods
.method public constructor <init>(Lyj;Lbmh;)V
    .locals 11

    .prologue
    const/4 v3, 0x2

    const/4 v10, 0x0

    const/4 v8, 0x1

    .line 478
    invoke-direct {p0, p1, p2}, Lbmf;-><init>(Lyj;Lbmh;)V

    .line 479
    iget-object v0, p2, Lbmh;->b:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lbmj;

    .line 480
    iget-object v0, v6, Lbmj;->h:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lbmk;

    .line 481
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v9

    .line 483
    iget v0, v6, Lbmj;->i:I

    if-ne v0, v8, :cond_2

    .line 484
    sget-boolean v0, Lbmi;->a:Z

    if-eqz v0, :cond_0

    .line 485
    const-string v0, "Babel"

    const-string v1, "[Notifications] SINGLE INVITE, NO MESSAGES."

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    :cond_0
    iget-object v0, v6, Lbmj;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lbmi;->a(Ljava/lang/String;)V

    .line 488
    iget-object v0, v6, Lbmj;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lbmi;->b(Ljava/lang/String;)V

    .line 489
    iget-object v0, p0, Lbmi;->m:Lyj;

    iget-object v1, v6, Lbmj;->a:Ljava/lang/String;

    iget-object v2, v7, Lbmk;->c:Lbdk;

    iget v3, v6, Lbmj;->e:I

    iget-wide v4, v6, Lbmj;->g:J

    invoke-static/range {v0 .. v5}, Lbbl;->a(Lyj;Ljava/lang/String;Lbdk;IJ)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lbmi;->f:Landroid/content/Intent;

    .line 492
    iget-object v0, p0, Lbmi;->f:Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 493
    iget-object v0, v7, Lbmk;->a:Ljava/lang/String;

    iput-object v0, p0, Lbmi;->d:Ljava/lang/String;

    .line 494
    iget-boolean v0, v6, Lbmj;->c:Z

    if-eqz v0, :cond_1

    .line 495
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->hu:I

    iget v2, v6, Lbmj;->d:I

    new-array v3, v8, [Ljava/lang/Object;

    iget v4, v6, Lbmj;->d:I

    .line 497
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v10

    .line 495
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbmi;->e:Ljava/lang/CharSequence;

    .line 533
    :goto_0
    return-void

    .line 499
    :cond_1
    sget v0, Lh;->jE:I

    invoke-virtual {v9, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbmi;->e:Ljava/lang/CharSequence;

    goto :goto_0

    .line 503
    :cond_2
    sget-boolean v0, Lbmi;->a:Z

    if-eqz v0, :cond_3

    .line 504
    const-string v0, "Babel"

    const-string v1, "[Notifications] MULTIPLE INVITES, NO MESSAGES"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    :cond_3
    iget-object v0, p0, Lbmi;->m:Lyj;

    invoke-static {v0}, Lbbl;->e(Lyj;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lbmi;->f:Landroid/content/Intent;

    .line 508
    iget-object v0, p0, Lbmi;->f:Landroid/content/Intent;

    const-string v1, "reset_chat_notifications"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 509
    sget v0, Lh;->jD:I

    new-array v1, v8, [Ljava/lang/Object;

    iget v2, v6, Lbmj;->i:I

    .line 511
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    .line 509
    invoke-virtual {v9, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbmi;->d:Ljava/lang/String;

    .line 516
    iget-object v0, v6, Lbmj;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_4

    .line 517
    iget-object v0, v6, Lbmj;->h:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmk;

    .line 518
    sget v1, Lh;->jF:I

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, v7, Lbmk;->a:Ljava/lang/String;

    aput-object v3, v2, v10

    iget-object v0, v0, Lbmk;->a:Ljava/lang/String;

    aput-object v0, v2, v8

    invoke-virtual {v9, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbmi;->e:Ljava/lang/CharSequence;

    .line 531
    :goto_1
    iput-boolean v8, p0, Lbmi;->i:Z

    goto :goto_0

    .line 522
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v0, v7, Lbmk;->b:Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move v1, v8

    .line 524
    :goto_2
    iget-object v0, v6, Lbmj;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 525
    const-string v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 526
    iget-object v0, v6, Lbmj;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmk;

    .line 527
    iget-object v0, v0, Lbmk;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 524
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 529
    :cond_5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbmi;->e:Ljava/lang/CharSequence;

    goto :goto_1
.end method


# virtual methods
.method protected a(Lbk;)Lbu;
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v3, 0x0

    .line 537
    iget-object v0, p0, Lbmi;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lbk;->a(Ljava/lang/CharSequence;)Lbk;

    move-result-object v0

    .line 538
    invoke-virtual {p0}, Lbmi;->n()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbk;->c(Ljava/lang/CharSequence;)Lbk;

    move-result-object v0

    iget-object v1, p0, Lbmi;->e:Ljava/lang/CharSequence;

    .line 539
    invoke-virtual {v0, v1}, Lbk;->b(Ljava/lang/CharSequence;)Lbk;

    .line 541
    iget-object v0, p0, Lbmi;->j:Lbmh;

    iget-object v0, v0, Lbmh;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmj;

    .line 542
    iget v1, v0, Lbmj;->i:I

    if-ne v1, v11, :cond_0

    .line 543
    new-instance v1, Lbj;

    invoke-direct {v1, p1}, Lbj;-><init>(Lbk;)V

    iget-object v2, p0, Lbmi;->e:Ljava/lang/CharSequence;

    .line 544
    invoke-virtual {v1, v2}, Lbj;->b(Ljava/lang/CharSequence;)Lbj;

    move-result-object v1

    .line 565
    :goto_0
    iget-wide v2, v0, Lbmj;->g:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {p1, v2, v3}, Lbk;->a(J)Lbk;

    .line 566
    return-object v1

    .line 546
    :cond_0
    new-instance v5, Lbm;

    invoke-direct {v5, p1}, Lbm;-><init>(Lbk;)V

    .line 548
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v6

    move v2, v3

    .line 549
    :goto_1
    iget-object v1, v0, Lbmj;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_2

    .line 550
    iget-object v1, v0, Lbmj;->h:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbmk;

    .line 552
    iget-boolean v4, v1, Lbmk;->e:Z

    if-eqz v4, :cond_1

    .line 553
    iget-object v4, p0, Lbmi;->m:Lyj;

    invoke-virtual {v1, v4}, Lbmk;->a(Lyj;)I

    move-result v4

    .line 554
    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lf;->hu:I

    new-array v9, v11, [Ljava/lang/Object;

    .line 556
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v3

    .line 554
    invoke-virtual {v7, v8, v4, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 560
    :goto_2
    iget-object v1, v1, Lbmk;->a:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v1, v4, v7, v3}, Lbne;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v5, v1}, Lbm;->b(Ljava/lang/CharSequence;)Lbm;

    .line 549
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 558
    :cond_1
    sget v4, Lh;->hD:I

    invoke-virtual {v6, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_2
    move-object v1, v5

    .line 563
    goto :goto_0
.end method
