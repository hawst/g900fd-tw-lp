.class public final Ldgf;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldgf;


# instance fields
.field public b:Ldfi;

.field public c:Lden;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6393
    const/4 v0, 0x0

    new-array v0, v0, [Ldgf;

    sput-object v0, Ldgf;->a:[Ldgf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6394
    invoke-direct {p0}, Lepn;-><init>()V

    .line 6397
    iput-object v0, p0, Ldgf;->b:Ldfi;

    .line 6400
    iput-object v0, p0, Ldgf;->c:Lden;

    .line 6394
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 6427
    const/4 v0, 0x0

    .line 6428
    iget-object v1, p0, Ldgf;->b:Ldfi;

    if-eqz v1, :cond_0

    .line 6429
    const/4 v0, 0x1

    iget-object v1, p0, Ldgf;->b:Ldfi;

    .line 6430
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6432
    :cond_0
    iget-object v1, p0, Ldgf;->c:Lden;

    if-eqz v1, :cond_1

    .line 6433
    const/4 v1, 0x2

    iget-object v2, p0, Ldgf;->c:Lden;

    .line 6434
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6436
    :cond_1
    iget-object v1, p0, Ldgf;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 6437
    const/4 v1, 0x3

    iget-object v2, p0, Ldgf;->d:Ljava/lang/String;

    .line 6438
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6440
    :cond_2
    iget-object v1, p0, Ldgf;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 6441
    const/4 v1, 0x4

    iget-object v2, p0, Ldgf;->e:Ljava/lang/Boolean;

    .line 6442
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6444
    :cond_3
    iget-object v1, p0, Ldgf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6445
    iput v0, p0, Ldgf;->cachedSize:I

    .line 6446
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6390
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldgf;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldgf;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldgf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldgf;->b:Ldfi;

    if-nez v0, :cond_2

    new-instance v0, Ldfi;

    invoke-direct {v0}, Ldfi;-><init>()V

    iput-object v0, p0, Ldgf;->b:Ldfi;

    :cond_2
    iget-object v0, p0, Ldgf;->b:Ldfi;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldgf;->c:Lden;

    if-nez v0, :cond_3

    new-instance v0, Lden;

    invoke-direct {v0}, Lden;-><init>()V

    iput-object v0, p0, Ldgf;->c:Lden;

    :cond_3
    iget-object v0, p0, Ldgf;->c:Lden;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgf;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldgf;->e:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 6409
    iget-object v0, p0, Ldgf;->b:Ldfi;

    if-eqz v0, :cond_0

    .line 6410
    const/4 v0, 0x1

    iget-object v1, p0, Ldgf;->b:Ldfi;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6412
    :cond_0
    iget-object v0, p0, Ldgf;->c:Lden;

    if-eqz v0, :cond_1

    .line 6413
    const/4 v0, 0x2

    iget-object v1, p0, Ldgf;->c:Lden;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6415
    :cond_1
    iget-object v0, p0, Ldgf;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 6416
    const/4 v0, 0x3

    iget-object v1, p0, Ldgf;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 6418
    :cond_2
    iget-object v0, p0, Ldgf;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 6419
    const/4 v0, 0x4

    iget-object v1, p0, Ldgf;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 6421
    :cond_3
    iget-object v0, p0, Ldgf;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 6423
    return-void
.end method
