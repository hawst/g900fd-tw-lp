.class public final Lcah;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public a:Lxs;

.field public b:Lxo;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcah;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 68
    instance-of v1, p1, Lcah;

    if-nez v1, :cond_1

    .line 76
    :cond_0
    :goto_0
    return v0

    .line 71
    :cond_1
    check-cast p1, Lcah;

    .line 72
    iget-object v1, p0, Lcah;->a:Lxs;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcah;->a:Lxs;

    if-eqz v1, :cond_2

    .line 73
    iget-object v0, p0, Lcah;->a:Lxs;

    invoke-virtual {v0}, Lxs;->b()Lbcx;

    move-result-object v0

    iget-object v1, p1, Lcah;->a:Lxs;

    invoke-virtual {v1}, Lxs;->b()Lbcx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbcx;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 75
    :cond_2
    iget-object v1, p0, Lcah;->b:Lxo;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcah;->b:Lxo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcah;->b:Lxo;

    .line 76
    invoke-virtual {v1}, Lxo;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcah;->b:Lxo;

    invoke-virtual {v2}, Lxo;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcah;->a:Lxs;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcah;->a:Lxs;

    invoke-virtual {v0}, Lxs;->b()Lbcx;

    move-result-object v0

    invoke-virtual {v0}, Lbcx;->hashCode()I

    move-result v0

    .line 87
    :goto_0
    return v0

    .line 84
    :cond_0
    iget-object v0, p0, Lcah;->b:Lxo;

    if-eqz v0, :cond_1

    .line 85
    iget-object v0, p0, Lcah;->b:Lxo;

    invoke-virtual {v0}, Lxo;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 87
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
