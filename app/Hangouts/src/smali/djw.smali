.class public final Ldjw;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldjw;


# instance fields
.field public b:[Ldjy;

.field public c:Ldjy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 403
    const/4 v0, 0x0

    new-array v0, v0, [Ldjw;

    sput-object v0, Ldjw;->a:[Ldjw;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 404
    invoke-direct {p0}, Lepn;-><init>()V

    .line 407
    sget-object v0, Ldjy;->a:[Ldjy;

    iput-object v0, p0, Ldjw;->b:[Ldjy;

    .line 410
    const/4 v0, 0x0

    iput-object v0, p0, Ldjw;->c:Ldjy;

    .line 404
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 432
    iget-object v1, p0, Ldjw;->b:[Ldjy;

    if-eqz v1, :cond_1

    .line 433
    iget-object v2, p0, Ldjw;->b:[Ldjy;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 434
    if-eqz v4, :cond_0

    .line 435
    const/4 v5, 0x1

    .line 436
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 433
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 440
    :cond_1
    iget-object v1, p0, Ldjw;->c:Ldjy;

    if-eqz v1, :cond_2

    .line 441
    const/4 v1, 0x2

    iget-object v2, p0, Ldjw;->c:Ldjy;

    .line 442
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 444
    :cond_2
    iget-object v1, p0, Ldjw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 445
    iput v0, p0, Ldjw;->cachedSize:I

    .line 446
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 400
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldjw;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldjw;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldjw;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldjw;->b:[Ldjy;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldjy;

    iget-object v3, p0, Ldjw;->b:[Ldjy;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldjw;->b:[Ldjy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldjw;->b:[Ldjy;

    :goto_2
    iget-object v2, p0, Ldjw;->b:[Ldjy;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldjw;->b:[Ldjy;

    new-instance v3, Ldjy;

    invoke-direct {v3}, Ldjy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldjw;->b:[Ldjy;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldjw;->b:[Ldjy;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldjw;->b:[Ldjy;

    new-instance v3, Ldjy;

    invoke-direct {v3}, Ldjy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldjw;->b:[Ldjy;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldjw;->c:Ldjy;

    if-nez v0, :cond_5

    new-instance v0, Ldjy;

    invoke-direct {v0}, Ldjy;-><init>()V

    iput-object v0, p0, Ldjw;->c:Ldjy;

    :cond_5
    iget-object v0, p0, Ldjw;->c:Ldjy;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 415
    iget-object v0, p0, Ldjw;->b:[Ldjy;

    if-eqz v0, :cond_1

    .line 416
    iget-object v1, p0, Ldjw;->b:[Ldjy;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 417
    if-eqz v3, :cond_0

    .line 418
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 416
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 422
    :cond_1
    iget-object v0, p0, Ldjw;->c:Ldjy;

    if-eqz v0, :cond_2

    .line 423
    const/4 v0, 0x2

    iget-object v1, p0, Ldjw;->c:Ldjy;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 425
    :cond_2
    iget-object v0, p0, Ldjw;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 427
    return-void
.end method
