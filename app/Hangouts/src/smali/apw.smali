.class public final Lapw;
.super Lapo;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-direct {p0}, Lapo;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;B)V
    .locals 0

    .prologue
    .line 190
    invoke-direct {p0, p1}, Lapw;-><init>(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)V

    return-void
.end method

.method private a(Ls;Lapx;)V
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->i(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Z

    .line 308
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-virtual {p2}, Lapx;->L()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;Z)Z

    .line 309
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->getFragmentManager()Lae;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Ls;->a(Lae;Ljava/lang/String;)V

    .line 310
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 3

    .prologue
    .line 254
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->j(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)V

    .line 255
    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_0

    .line 256
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;Ljava/lang/String;Z)V

    .line 258
    :cond_0
    return-void
.end method

.method public a(Lapx;)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 261
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 262
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 304
    :cond_0
    :goto_0
    return-void

    .line 266
    :cond_1
    invoke-virtual {p1}, Lapx;->O()I

    move-result v0

    .line 267
    const-string v1, "Babel"

    invoke-static {v1, v4}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 268
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HangoutFragment.onHangoutEnded: reason="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    :cond_2
    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 271
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->b(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->p()V

    .line 303
    :goto_1
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->b(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    move-result-object v0

    iget-object v1, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->l(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/hangouts/video/ExitHistory;->setExitReported(Landroid/content/Context;Lcom/google/android/libraries/hangouts/video/HangoutRequest;)V

    goto :goto_0

    .line 273
    :cond_3
    iget-object v1, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v1, v4}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;I)V

    .line 275
    const/16 v1, 0x3f6

    if-ne v0, v1, :cond_4

    .line 276
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->e()Lae;

    move-result-object v0

    .line 277
    const-string v1, "out_of_balance_dialog_fragment_tag"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lt;

    move-result-object v0

    .line 280
    instance-of v0, v0, Laru;

    if-nez v0, :cond_0

    .line 282
    invoke-static {}, Laru;->q()Laru;

    move-result-object v0

    .line 283
    iget-object v1, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->getFragmentManager()Lae;

    move-result-object v1

    const-string v2, "out_of_balance_dialog_fragment_tag"

    invoke-virtual {v0, v1, v2}, Laru;->a(Lae;Ljava/lang/String;)V

    goto :goto_0

    .line 286
    :cond_4
    const/16 v1, 0x13

    if-ne v0, v1, :cond_5

    .line 287
    new-instance v0, Lart;

    invoke-direct {v0}, Lart;-><init>()V

    invoke-direct {p0, v0, p1}, Lapw;->a(Ls;Lapx;)V

    goto :goto_0

    .line 289
    :cond_5
    const/16 v1, 0x19

    if-ne v0, v1, :cond_6

    .line 290
    new-instance v0, Laoq;

    invoke-direct {v0}, Laoq;-><init>()V

    invoke-direct {p0, v0, p1}, Lapw;->a(Ls;Lapx;)V

    goto/16 :goto_0

    .line 292
    :cond_6
    const/16 v1, 0x1a

    if-ne v0, v1, :cond_7

    .line 293
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->k(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Lbme;

    move-result-object v0

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x5cf

    .line 294
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 293
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 295
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->i(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Z

    .line 296
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    iget-object v1, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->getActivity()Ly;

    const-string v1, "g_plus_upgrade_hangout"

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 300
    :cond_7
    iget-object v1, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-virtual {p1}, Lapx;->P()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;Ljava/lang/String;I)V

    goto/16 :goto_1
.end method

.method public b(I)Z
    .locals 1

    .prologue
    .line 330
    const/4 v0, 0x7

    if-eq p1, v0, :cond_0

    .line 331
    const/4 v0, 0x0

    .line 334
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 314
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 315
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 324
    :cond_0
    :goto_0
    return-void

    .line 318
    :cond_1
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->e(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Lapk;

    move-result-object v0

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    invoke-virtual {v0}, Lapx;->O()I

    move-result v0

    .line 319
    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->b(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->p()V

    goto :goto_0
.end method

.method public onAudioUpdated(Lcom/google/android/libraries/hangouts/video/AudioDeviceState;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/hangouts/video/AudioDeviceState;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/libraries/hangouts/video/AudioDevice;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 249
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->j(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)V

    .line 250
    return-void
.end method

.method public onAuthUserActionRequired(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 207
    :goto_0
    return-void

    .line 202
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v0

    .line 203
    const v1, -0x10000001

    and-int/2addr v0, v1

    .line 204
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 205
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->i(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Z

    .line 206
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public onConversationIdChanged(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 228
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 229
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 234
    :goto_0
    return-void

    .line 233
    :cond_0
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    iget-object v1, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->e(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Lapk;

    move-result-object v1

    invoke-virtual {v1}, Lapk;->c()Lapx;

    move-result-object v1

    invoke-virtual {v1}, Lapx;->c()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;Lcom/google/android/libraries/hangouts/video/HangoutRequest;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    goto :goto_0
.end method

.method public onEndpointEvent(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;)V
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->b(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->u_()V

    .line 194
    return-void
.end method

.method public onHangoutIdResolved(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ZZ)V
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 215
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 224
    :goto_0
    return-void

    .line 219
    :cond_0
    if-eqz p3, :cond_1

    .line 220
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->b(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->u_()V

    .line 223
    :cond_1
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    iget-object v1, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->e(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Lapk;

    move-result-object v1

    invoke-virtual {v1}, Lapk;->c()Lapx;

    move-result-object v1

    invoke-virtual {v1}, Lapx;->c()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;Lcom/google/android/libraries/hangouts/video/HangoutRequest;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    goto :goto_0
.end method

.method public onMediaStarted(Lcom/google/android/libraries/hangouts/video/CallState;)V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 239
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 244
    :goto_0
    return-void

    .line 243
    :cond_0
    iget-object v0, p0, Lapw;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a()V

    goto :goto_0
.end method
