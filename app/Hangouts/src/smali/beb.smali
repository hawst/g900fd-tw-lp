.class public Lbeb;
.super Lbei;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbcx;",
            ">;"
        }
    .end annotation
.end field

.field public final b:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lbcx;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 2038
    invoke-direct {p0, p2, p1}, Lbei;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2039
    iput-object p3, p0, Lbeb;->a:Ljava/util/List;

    .line 2040
    iput-wide p4, p0, Lbeb;->b:J

    .line 2041
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2045
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 2046
    const-string v0, "Babel_RequestWriter"

    const-string v1, "addUsers build protobuf"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2049
    :cond_0
    new-instance v0, Ldrx;

    invoke-direct {v0}, Ldrx;-><init>()V

    .line 2051
    iget-object v1, p0, Lbeb;->i:Ljava/lang/String;

    .line 2052
    invoke-static {v1}, Lyt;->c(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Ldrx;->c:Ljava/lang/Long;

    .line 2053
    iget-object v1, p0, Lbeb;->c:Ljava/lang/String;

    invoke-static {v1}, Lbea;->d(Ljava/lang/String;)Ldqf;

    move-result-object v1

    iput-object v1, v0, Ldrx;->b:Ldqf;

    .line 2054
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldrx;->f:Ljava/lang/Integer;

    .line 2056
    new-instance v4, Ldph;

    invoke-direct {v4}, Ldph;-><init>()V

    .line 2057
    iput-object v0, v4, Ldph;->c:Ldrx;

    .line 2059
    iget-object v0, p0, Lbeb;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 2060
    iget-object v0, p0, Lbeb;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v5, v0, [Ldth;

    move v1, v2

    .line 2061
    :goto_0
    iget-object v0, p0, Lbeb;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2062
    iget-object v0, p0, Lbeb;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbcx;

    .line 2063
    iget-object v3, v0, Lbcx;->c:Ljava/lang/String;

    if-nez v3, :cond_1

    iget-object v3, v0, Lbcx;->e:Ljava/lang/String;

    .line 2064
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    const/4 v3, 0x1

    .line 2063
    :goto_1
    invoke-static {v3}, Lcwz;->a(Z)V

    .line 2065
    invoke-virtual {v0}, Lbcx;->c()Ldth;

    move-result-object v0

    aput-object v0, v5, v1

    .line 2061
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v3, v2

    .line 2064
    goto :goto_1

    .line 2067
    :cond_3
    iput-object v5, v4, Ldph;->d:[Ldth;

    .line 2069
    :cond_4
    invoke-virtual {p0, p1, p2}, Lbeb;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v0

    iput-object v0, v4, Ldph;->b:Ldvm;

    .line 2072
    return-object v4
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2077
    const-string v0, "conversations/adduser"

    return-object v0
.end method
