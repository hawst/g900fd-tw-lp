.class public final Levb;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Levb;


# instance fields
.field public b:Lexn;

.field public c:[Leuz;

.field public d:Lexh;

.field public e:Levt;

.field public f:Letr;

.field public g:Lexc;

.field public h:Lewb;

.field public i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 694
    const/4 v0, 0x0

    new-array v0, v0, [Levb;

    sput-object v0, Levb;->a:[Levb;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 695
    invoke-direct {p0}, Lepn;-><init>()V

    .line 698
    iput-object v1, p0, Levb;->b:Lexn;

    .line 701
    sget-object v0, Leuz;->a:[Leuz;

    iput-object v0, p0, Levb;->c:[Leuz;

    .line 704
    iput-object v1, p0, Levb;->d:Lexh;

    .line 707
    iput-object v1, p0, Levb;->e:Levt;

    .line 710
    iput-object v1, p0, Levb;->f:Letr;

    .line 713
    iput-object v1, p0, Levb;->g:Lexc;

    .line 716
    iput-object v1, p0, Levb;->h:Lewb;

    .line 695
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 758
    iget-object v1, p0, Levb;->c:[Leuz;

    if-eqz v1, :cond_1

    .line 759
    iget-object v2, p0, Levb;->c:[Leuz;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 760
    if-eqz v4, :cond_0

    .line 761
    const/4 v5, 0x1

    .line 762
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 759
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 766
    :cond_1
    iget-object v1, p0, Levb;->d:Lexh;

    if-eqz v1, :cond_2

    .line 767
    const/4 v1, 0x2

    iget-object v2, p0, Levb;->d:Lexh;

    .line 768
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 770
    :cond_2
    iget-object v1, p0, Levb;->b:Lexn;

    if-eqz v1, :cond_3

    .line 771
    const/4 v1, 0x3

    iget-object v2, p0, Levb;->b:Lexn;

    .line 772
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 774
    :cond_3
    iget-object v1, p0, Levb;->e:Levt;

    if-eqz v1, :cond_4

    .line 775
    const/4 v1, 0x4

    iget-object v2, p0, Levb;->e:Levt;

    .line 776
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 778
    :cond_4
    iget-object v1, p0, Levb;->f:Letr;

    if-eqz v1, :cond_5

    .line 779
    const/4 v1, 0x5

    iget-object v2, p0, Levb;->f:Letr;

    .line 780
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 782
    :cond_5
    iget-object v1, p0, Levb;->g:Lexc;

    if-eqz v1, :cond_6

    .line 783
    const/4 v1, 0x6

    iget-object v2, p0, Levb;->g:Lexc;

    .line 784
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 786
    :cond_6
    iget-object v1, p0, Levb;->h:Lewb;

    if-eqz v1, :cond_7

    .line 787
    const/4 v1, 0x7

    iget-object v2, p0, Levb;->h:Lewb;

    .line 788
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 790
    :cond_7
    iget-object v1, p0, Levb;->i:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 791
    const/16 v1, 0x8

    iget-object v2, p0, Levb;->i:Ljava/lang/String;

    .line 792
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 794
    :cond_8
    iget-object v1, p0, Levb;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 795
    iput v0, p0, Levb;->cachedSize:I

    .line 796
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 691
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Levb;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Levb;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Levb;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Levb;->c:[Leuz;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leuz;

    iget-object v3, p0, Levb;->c:[Leuz;

    if-eqz v3, :cond_2

    iget-object v3, p0, Levb;->c:[Leuz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Levb;->c:[Leuz;

    :goto_2
    iget-object v2, p0, Levb;->c:[Leuz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Levb;->c:[Leuz;

    new-instance v3, Leuz;

    invoke-direct {v3}, Leuz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Levb;->c:[Leuz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Levb;->c:[Leuz;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Levb;->c:[Leuz;

    new-instance v3, Leuz;

    invoke-direct {v3}, Leuz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Levb;->c:[Leuz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Levb;->d:Lexh;

    if-nez v0, :cond_5

    new-instance v0, Lexh;

    invoke-direct {v0}, Lexh;-><init>()V

    iput-object v0, p0, Levb;->d:Lexh;

    :cond_5
    iget-object v0, p0, Levb;->d:Lexh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Levb;->b:Lexn;

    if-nez v0, :cond_6

    new-instance v0, Lexn;

    invoke-direct {v0}, Lexn;-><init>()V

    iput-object v0, p0, Levb;->b:Lexn;

    :cond_6
    iget-object v0, p0, Levb;->b:Lexn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Levb;->e:Levt;

    if-nez v0, :cond_7

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Levb;->e:Levt;

    :cond_7
    iget-object v0, p0, Levb;->e:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Levb;->f:Letr;

    if-nez v0, :cond_8

    new-instance v0, Letr;

    invoke-direct {v0}, Letr;-><init>()V

    iput-object v0, p0, Levb;->f:Letr;

    :cond_8
    iget-object v0, p0, Levb;->f:Letr;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Levb;->g:Lexc;

    if-nez v0, :cond_9

    new-instance v0, Lexc;

    invoke-direct {v0}, Lexc;-><init>()V

    iput-object v0, p0, Levb;->g:Lexc;

    :cond_9
    iget-object v0, p0, Levb;->g:Lexc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Levb;->h:Lewb;

    if-nez v0, :cond_a

    new-instance v0, Lewb;

    invoke-direct {v0}, Lewb;-><init>()V

    iput-object v0, p0, Levb;->h:Lewb;

    :cond_a
    iget-object v0, p0, Levb;->h:Lewb;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levb;->i:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 723
    iget-object v0, p0, Levb;->c:[Leuz;

    if-eqz v0, :cond_1

    .line 724
    iget-object v1, p0, Levb;->c:[Leuz;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 725
    if-eqz v3, :cond_0

    .line 726
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 724
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 730
    :cond_1
    iget-object v0, p0, Levb;->d:Lexh;

    if-eqz v0, :cond_2

    .line 731
    const/4 v0, 0x2

    iget-object v1, p0, Levb;->d:Lexh;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 733
    :cond_2
    iget-object v0, p0, Levb;->b:Lexn;

    if-eqz v0, :cond_3

    .line 734
    const/4 v0, 0x3

    iget-object v1, p0, Levb;->b:Lexn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 736
    :cond_3
    iget-object v0, p0, Levb;->e:Levt;

    if-eqz v0, :cond_4

    .line 737
    const/4 v0, 0x4

    iget-object v1, p0, Levb;->e:Levt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 739
    :cond_4
    iget-object v0, p0, Levb;->f:Letr;

    if-eqz v0, :cond_5

    .line 740
    const/4 v0, 0x5

    iget-object v1, p0, Levb;->f:Letr;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 742
    :cond_5
    iget-object v0, p0, Levb;->g:Lexc;

    if-eqz v0, :cond_6

    .line 743
    const/4 v0, 0x6

    iget-object v1, p0, Levb;->g:Lexc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 745
    :cond_6
    iget-object v0, p0, Levb;->h:Lewb;

    if-eqz v0, :cond_7

    .line 746
    const/4 v0, 0x7

    iget-object v1, p0, Levb;->h:Lewb;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 748
    :cond_7
    iget-object v0, p0, Levb;->i:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 749
    const/16 v0, 0x8

    iget-object v1, p0, Levb;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 751
    :cond_8
    iget-object v0, p0, Levb;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 753
    return-void
.end method
