.class public final Lbqi;
.super Lbmu;
.source "PG"


# static fields
.field private static final a:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lbqi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lbqi;->a:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method private constructor <init>(Lyj;JJ)V
    .locals 0

    .prologue
    .line 54
    invoke-direct/range {p0 .. p5}, Lbmu;-><init>(Lyj;JJ)V

    .line 55
    return-void
.end method

.method public static a(Lyj;)Lbqi;
    .locals 7

    .prologue
    .line 27
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v6

    .line 28
    sget-object v0, Lbqi;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqi;

    .line 29
    if-nez v0, :cond_0

    .line 30
    new-instance v0, Lbqi;

    const-string v1, "babel_sms_background_sync_lowmark_millis"

    const-wide/32 v2, 0x5265c00

    .line 31
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v2

    const-string v1, "babel_sms_background_sync_highmark_millis"

    const-wide/32 v4, 0xa4cb800

    .line 34
    invoke-static {v1, v4, v5}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v4

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lbqi;-><init>(Lyj;JJ)V

    .line 38
    sget-object v1, Lbqi;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v6, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v0, Lbqi;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqi;

    .line 42
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 62
    invoke-static {}, Lbkb;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbqi;->b:Lyj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbqi;->b:Lyj;

    .line 64
    invoke-virtual {v0}, Lyj;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lbqi;->b:Lyj;

    invoke-static {v0}, Lbwf;->c(Lyj;)V

    .line 67
    :cond_0
    return-void
.end method

.method protected a(J)V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method protected i()J
    .locals 4

    .prologue
    const-wide/16 v0, -0x1

    .line 71
    iget-object v2, p0, Lbqi;->b:Lyj;

    if-eqz v2, :cond_0

    .line 72
    iget-object v2, p0, Lbqi;->b:Lyj;

    invoke-static {v2}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v2

    const-string v3, "sms_last_full_sync_time_millis"

    invoke-virtual {v2, v3, v0, v1}, Lbsx;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 75
    :cond_0
    return-wide v0
.end method
