.class final Lbwk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbwj;


# instance fields
.field private a:Landroid/database/Cursor;

.field private final b:Lyt;


# direct methods
.method public constructor <init>(Lyt;JJ)V
    .locals 9

    .prologue
    const-wide/16 v4, 0x3e8

    .line 254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255
    iput-object p1, p0, Lbwk;->b:Lyt;

    .line 257
    :try_start_0
    iget-object v0, p0, Lbwk;->b:Lyt;

    invoke-virtual {v0}, Lyt;->e()Lzr;

    move-result-object v6

    const-string v7, "messages"

    .line 259
    invoke-static {}, Lbwm;->a()[Ljava/lang/String;

    move-result-object v8

    .line 261
    invoke-static {}, Lbwf;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "timestamp"

    mul-long v2, p2, v4

    mul-long/2addr v4, p4

    .line 260
    invoke-static/range {v0 .. v5}, Lbwf;->a(Ljava/lang/String;Ljava/lang/String;JJ)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "timestamp DESC"

    move-object v0, v6

    move-object v1, v7

    move-object v2, v8

    .line 257
    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lbwk;->a:Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 276
    return-void

    .line 270
    :catch_0
    move-exception v0

    .line 271
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SmsSyncManager A#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lbwk;->b:Lyt;

    invoke-virtual {v3}, Lyt;->f()Lyj;

    move-result-object v3

    invoke-virtual {v3}, Lyj;->j()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": failed to query local sms/mms "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 275
    throw v0
.end method


# virtual methods
.method public a()Lbvh;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 281
    iget-object v1, p0, Lbwk;->a:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbwk;->a:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 282
    iget-object v6, p0, Lbwk;->a:Landroid/database/Cursor;

    if-nez v6, :cond_1

    .line 284
    :cond_0
    :goto_0
    return-object v0

    .line 282
    :cond_1
    new-instance v0, Lbwl;

    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    const/4 v3, 0x3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v4, 0x2

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lbwf;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Lbwf;->a(Landroid/net/Uri;)J

    move-result-wide v4

    const/4 v7, 0x1

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, Lbwl;-><init>(JIJJ)V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lbwk;->a:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lbwk;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 291
    const/4 v0, 0x0

    iput-object v0, p0, Lbwk;->a:Landroid/database/Cursor;

    .line 293
    :cond_0
    return-void
.end method
