.class public final Lewx;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lewx;


# instance fields
.field public b:Levt;

.field public c:Letr;

.field public d:Ljava/lang/String;

.field public e:[Lexw;

.field public f:Ljava/lang/String;

.field public g:[Lety;

.field public h:[Lexb;

.field public i:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    new-array v0, v0, [Lewx;

    sput-object v0, Lewx;->a:[Lewx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 161
    invoke-direct {p0}, Lepn;-><init>()V

    .line 164
    iput-object v0, p0, Lewx;->b:Levt;

    .line 167
    iput-object v0, p0, Lewx;->c:Letr;

    .line 172
    sget-object v0, Lexw;->a:[Lexw;

    iput-object v0, p0, Lewx;->e:[Lexw;

    .line 177
    sget-object v0, Lety;->a:[Lety;

    iput-object v0, p0, Lewx;->g:[Lety;

    .line 180
    sget-object v0, Lexb;->a:[Lexb;

    iput-object v0, p0, Lewx;->h:[Lexb;

    .line 161
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 230
    iget-object v0, p0, Lewx;->b:Levt;

    if-eqz v0, :cond_a

    .line 231
    const/4 v0, 0x1

    iget-object v2, p0, Lewx;->b:Levt;

    .line 232
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 234
    :goto_0
    iget-object v2, p0, Lewx;->c:Letr;

    if-eqz v2, :cond_0

    .line 235
    const/4 v2, 0x2

    iget-object v3, p0, Lewx;->c:Letr;

    .line 236
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 238
    :cond_0
    iget-object v2, p0, Lewx;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 239
    const/4 v2, 0x3

    iget-object v3, p0, Lewx;->d:Ljava/lang/String;

    .line 240
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 242
    :cond_1
    iget-object v2, p0, Lewx;->e:[Lexw;

    if-eqz v2, :cond_3

    .line 243
    iget-object v3, p0, Lewx;->e:[Lexw;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 244
    if-eqz v5, :cond_2

    .line 245
    const/4 v6, 0x4

    .line 246
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 243
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 250
    :cond_3
    iget-object v2, p0, Lewx;->f:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 251
    const/4 v2, 0x5

    iget-object v3, p0, Lewx;->f:Ljava/lang/String;

    .line 252
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 254
    :cond_4
    iget-object v2, p0, Lewx;->g:[Lety;

    if-eqz v2, :cond_6

    .line 255
    iget-object v3, p0, Lewx;->g:[Lety;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    .line 256
    if-eqz v5, :cond_5

    .line 257
    const/4 v6, 0x6

    .line 258
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 255
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 262
    :cond_6
    iget-object v2, p0, Lewx;->h:[Lexb;

    if-eqz v2, :cond_8

    .line 263
    iget-object v2, p0, Lewx;->h:[Lexb;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 264
    if-eqz v4, :cond_7

    .line 265
    const/4 v5, 0x7

    .line 266
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 263
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 270
    :cond_8
    iget-object v1, p0, Lewx;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 271
    const/16 v1, 0x8

    iget-object v2, p0, Lewx;->i:Ljava/lang/Integer;

    .line 272
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 274
    :cond_9
    iget-object v1, p0, Lewx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 275
    iput v0, p0, Lewx;->cachedSize:I

    .line 276
    return v0

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 157
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lewx;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lewx;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lewx;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lewx;->b:Levt;

    if-nez v0, :cond_2

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Lewx;->b:Levt;

    :cond_2
    iget-object v0, p0, Lewx;->b:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lewx;->c:Letr;

    if-nez v0, :cond_3

    new-instance v0, Letr;

    invoke-direct {v0}, Letr;-><init>()V

    iput-object v0, p0, Lewx;->c:Letr;

    :cond_3
    iget-object v0, p0, Lewx;->c:Letr;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewx;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lewx;->e:[Lexw;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lexw;

    iget-object v3, p0, Lewx;->e:[Lexw;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lewx;->e:[Lexw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Lewx;->e:[Lexw;

    :goto_2
    iget-object v2, p0, Lewx;->e:[Lexw;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Lewx;->e:[Lexw;

    new-instance v3, Lexw;

    invoke-direct {v3}, Lexw;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lewx;->e:[Lexw;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lewx;->e:[Lexw;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lewx;->e:[Lexw;

    new-instance v3, Lexw;

    invoke-direct {v3}, Lexw;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lewx;->e:[Lexw;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewx;->f:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lewx;->g:[Lety;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lety;

    iget-object v3, p0, Lewx;->g:[Lety;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lewx;->g:[Lety;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Lewx;->g:[Lety;

    :goto_4
    iget-object v2, p0, Lewx;->g:[Lety;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Lewx;->g:[Lety;

    new-instance v3, Lety;

    invoke-direct {v3}, Lety;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lewx;->g:[Lety;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    iget-object v0, p0, Lewx;->g:[Lety;

    array-length v0, v0

    goto :goto_3

    :cond_9
    iget-object v2, p0, Lewx;->g:[Lety;

    new-instance v3, Lety;

    invoke-direct {v3}, Lety;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lewx;->g:[Lety;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lewx;->h:[Lexb;

    if-nez v0, :cond_b

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lexb;

    iget-object v3, p0, Lewx;->h:[Lexb;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lewx;->h:[Lexb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    iput-object v2, p0, Lewx;->h:[Lexb;

    :goto_6
    iget-object v2, p0, Lewx;->h:[Lexb;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_c

    iget-object v2, p0, Lewx;->h:[Lexb;

    new-instance v3, Lexb;

    invoke-direct {v3}, Lexb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lewx;->h:[Lexb;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_b
    iget-object v0, p0, Lewx;->h:[Lexb;

    array-length v0, v0

    goto :goto_5

    :cond_c
    iget-object v2, p0, Lewx;->h:[Lexb;

    new-instance v3, Lexb;

    invoke-direct {v3}, Lexb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lewx;->h:[Lexb;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lewx;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 187
    iget-object v1, p0, Lewx;->b:Levt;

    if-eqz v1, :cond_0

    .line 188
    const/4 v1, 0x1

    iget-object v2, p0, Lewx;->b:Levt;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 190
    :cond_0
    iget-object v1, p0, Lewx;->c:Letr;

    if-eqz v1, :cond_1

    .line 191
    const/4 v1, 0x2

    iget-object v2, p0, Lewx;->c:Letr;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 193
    :cond_1
    iget-object v1, p0, Lewx;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 194
    const/4 v1, 0x3

    iget-object v2, p0, Lewx;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 196
    :cond_2
    iget-object v1, p0, Lewx;->e:[Lexw;

    if-eqz v1, :cond_4

    .line 197
    iget-object v2, p0, Lewx;->e:[Lexw;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 198
    if-eqz v4, :cond_3

    .line 199
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 197
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 203
    :cond_4
    iget-object v1, p0, Lewx;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 204
    const/4 v1, 0x5

    iget-object v2, p0, Lewx;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 206
    :cond_5
    iget-object v1, p0, Lewx;->g:[Lety;

    if-eqz v1, :cond_7

    .line 207
    iget-object v2, p0, Lewx;->g:[Lety;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 208
    if-eqz v4, :cond_6

    .line 209
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 207
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 213
    :cond_7
    iget-object v1, p0, Lewx;->h:[Lexb;

    if-eqz v1, :cond_9

    .line 214
    iget-object v1, p0, Lewx;->h:[Lexb;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_9

    aget-object v3, v1, v0

    .line 215
    if-eqz v3, :cond_8

    .line 216
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 214
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 220
    :cond_9
    iget-object v0, p0, Lewx;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 221
    const/16 v0, 0x8

    iget-object v1, p0, Lewx;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 223
    :cond_a
    iget-object v0, p0, Lewx;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 225
    return-void
.end method
