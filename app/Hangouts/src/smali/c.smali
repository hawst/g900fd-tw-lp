.class public final Lc;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lc;


# instance fields
.field public b:Ld;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/String;

.field public g:Ldoy;

.field public h:Ldow;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/Long;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 257
    const/4 v0, 0x0

    new-array v0, v0, [Lc;

    sput-object v0, Lc;->a:[Lc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 258
    invoke-direct {p0}, Lepn;-><init>()V

    .line 261
    iput-object v0, p0, Lc;->b:Ld;

    .line 272
    iput-object v0, p0, Lc;->g:Ldoy;

    .line 275
    iput-object v0, p0, Lc;->h:Ldow;

    .line 258
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 342
    const/4 v0, 0x0

    .line 343
    iget-object v1, p0, Lc;->b:Ld;

    if-eqz v1, :cond_0

    .line 344
    const/4 v0, 0x1

    iget-object v1, p0, Lc;->b:Ld;

    .line 345
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 347
    :cond_0
    iget-object v1, p0, Lc;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 348
    const/4 v1, 0x2

    iget-object v2, p0, Lc;->c:Ljava/lang/String;

    .line 349
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 351
    :cond_1
    iget-object v1, p0, Lc;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 352
    const/4 v1, 0x3

    iget-object v2, p0, Lc;->d:Ljava/lang/String;

    .line 353
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 355
    :cond_2
    iget-object v1, p0, Lc;->e:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 356
    const/4 v1, 0x4

    iget-object v2, p0, Lc;->e:Ljava/lang/Long;

    .line 357
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 359
    :cond_3
    iget-object v1, p0, Lc;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 360
    const/4 v1, 0x5

    iget-object v2, p0, Lc;->f:Ljava/lang/String;

    .line 361
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 363
    :cond_4
    iget-object v1, p0, Lc;->g:Ldoy;

    if-eqz v1, :cond_5

    .line 364
    const/4 v1, 0x6

    iget-object v2, p0, Lc;->g:Ldoy;

    .line 365
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 367
    :cond_5
    iget-object v1, p0, Lc;->i:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 368
    const/4 v1, 0x7

    iget-object v2, p0, Lc;->i:Ljava/lang/String;

    .line 369
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 371
    :cond_6
    iget-object v1, p0, Lc;->j:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 372
    const/16 v1, 0x8

    iget-object v2, p0, Lc;->j:Ljava/lang/String;

    .line 373
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 375
    :cond_7
    iget-object v1, p0, Lc;->k:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 376
    const/16 v1, 0x9

    iget-object v2, p0, Lc;->k:Ljava/lang/String;

    .line 377
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 379
    :cond_8
    iget-object v1, p0, Lc;->l:Ljava/lang/Long;

    if-eqz v1, :cond_9

    .line 380
    const/16 v1, 0xa

    iget-object v2, p0, Lc;->l:Ljava/lang/Long;

    .line 381
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 383
    :cond_9
    iget-object v1, p0, Lc;->m:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 384
    const/16 v1, 0xb

    iget-object v2, p0, Lc;->m:Ljava/lang/String;

    .line 385
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 387
    :cond_a
    iget-object v1, p0, Lc;->h:Ldow;

    if-eqz v1, :cond_b

    .line 388
    const/16 v1, 0xc

    iget-object v2, p0, Lc;->h:Ldow;

    .line 389
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 391
    :cond_b
    iget-object v1, p0, Lc;->n:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 392
    const/16 v1, 0xd

    iget-object v2, p0, Lc;->n:Ljava/lang/String;

    .line 393
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 395
    :cond_c
    iget-object v1, p0, Lc;->o:Ljava/lang/Long;

    if-eqz v1, :cond_d

    .line 396
    const/16 v1, 0xe

    iget-object v2, p0, Lc;->o:Ljava/lang/Long;

    .line 397
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 399
    :cond_d
    iget-object v1, p0, Lc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 400
    iput v0, p0, Lc;->cachedSize:I

    .line 401
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 254
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lc;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lc;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lc;->b:Ld;

    if-nez v0, :cond_2

    new-instance v0, Ld;

    invoke-direct {v0}, Ld;-><init>()V

    iput-object v0, p0, Lc;->b:Ld;

    :cond_2
    iget-object v0, p0, Lc;->b:Ld;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lc;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lc;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lc;->e:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lc;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lc;->g:Ldoy;

    if-nez v0, :cond_3

    new-instance v0, Ldoy;

    invoke-direct {v0}, Ldoy;-><init>()V

    iput-object v0, p0, Lc;->g:Ldoy;

    :cond_3
    iget-object v0, p0, Lc;->g:Ldoy;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lc;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lc;->j:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lc;->k:Ljava/lang/String;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lc;->l:Ljava/lang/Long;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lc;->m:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lc;->h:Ldow;

    if-nez v0, :cond_4

    new-instance v0, Ldow;

    invoke-direct {v0}, Ldow;-><init>()V

    iput-object v0, p0, Lc;->h:Ldow;

    :cond_4
    iget-object v0, p0, Lc;->h:Ldow;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lc;->n:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lc;->o:Ljava/lang/Long;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 294
    iget-object v0, p0, Lc;->b:Ld;

    if-eqz v0, :cond_0

    .line 295
    const/4 v0, 0x1

    iget-object v1, p0, Lc;->b:Ld;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 297
    :cond_0
    iget-object v0, p0, Lc;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 298
    const/4 v0, 0x2

    iget-object v1, p0, Lc;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 300
    :cond_1
    iget-object v0, p0, Lc;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 301
    const/4 v0, 0x3

    iget-object v1, p0, Lc;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 303
    :cond_2
    iget-object v0, p0, Lc;->e:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 304
    const/4 v0, 0x4

    iget-object v1, p0, Lc;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 306
    :cond_3
    iget-object v0, p0, Lc;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 307
    const/4 v0, 0x5

    iget-object v1, p0, Lc;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 309
    :cond_4
    iget-object v0, p0, Lc;->g:Ldoy;

    if-eqz v0, :cond_5

    .line 310
    const/4 v0, 0x6

    iget-object v1, p0, Lc;->g:Ldoy;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 312
    :cond_5
    iget-object v0, p0, Lc;->i:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 313
    const/4 v0, 0x7

    iget-object v1, p0, Lc;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 315
    :cond_6
    iget-object v0, p0, Lc;->j:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 316
    const/16 v0, 0x8

    iget-object v1, p0, Lc;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 318
    :cond_7
    iget-object v0, p0, Lc;->k:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 319
    const/16 v0, 0x9

    iget-object v1, p0, Lc;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 321
    :cond_8
    iget-object v0, p0, Lc;->l:Ljava/lang/Long;

    if-eqz v0, :cond_9

    .line 322
    const/16 v0, 0xa

    iget-object v1, p0, Lc;->l:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 324
    :cond_9
    iget-object v0, p0, Lc;->m:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 325
    const/16 v0, 0xb

    iget-object v1, p0, Lc;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 327
    :cond_a
    iget-object v0, p0, Lc;->h:Ldow;

    if-eqz v0, :cond_b

    .line 328
    const/16 v0, 0xc

    iget-object v1, p0, Lc;->h:Ldow;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 330
    :cond_b
    iget-object v0, p0, Lc;->n:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 331
    const/16 v0, 0xd

    iget-object v1, p0, Lc;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 333
    :cond_c
    iget-object v0, p0, Lc;->o:Ljava/lang/Long;

    if-eqz v0, :cond_d

    .line 334
    const/16 v0, 0xe

    iget-object v1, p0, Lc;->o:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 336
    :cond_d
    iget-object v0, p0, Lc;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 338
    return-void
.end method
