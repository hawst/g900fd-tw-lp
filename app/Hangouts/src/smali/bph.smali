.class public final Lbph;
.super Ljava/lang/Exception;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Lbph;

.field public static final b:Lbph;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final c:Z

.field private final d:I

.field private final e:Ljava/lang/String;

.field private final f:J

.field private final g:Lbxl;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2096
    new-instance v0, Lbph;

    const/16 v1, 0x78

    invoke-direct {v0, v1}, Lbph;-><init>(I)V

    sput-object v0, Lbph;->a:Lbph;

    .line 2098
    new-instance v0, Lbph;

    const/16 v1, 0x79

    invoke-direct {v0, v1}, Lbph;-><init>(I)V

    sput-object v0, Lbph;->b:Lbph;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2139
    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v6, v2

    invoke-direct/range {v0 .. v6}, Lbph;-><init>(ILjava/lang/Exception;JZLjava/lang/String;)V

    .line 2140
    return-void
.end method

.method public constructor <init>(IJLjava/lang/String;)V
    .locals 7

    .prologue
    .line 2143
    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-wide v3, p2

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lbph;-><init>(ILjava/lang/Exception;JZLjava/lang/String;)V

    .line 2144
    return-void
.end method

.method public constructor <init>(ILjava/lang/Exception;)V
    .locals 7

    .prologue
    .line 2135
    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lbph;-><init>(ILjava/lang/Exception;JZLjava/lang/String;)V

    .line 2136
    return-void
.end method

.method public constructor <init>(ILjava/lang/Exception;JZLjava/lang/String;)V
    .locals 3

    .prologue
    const/16 v2, 0x64

    .line 2109
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 2113
    if-lt p1, v2, :cond_0

    const/16 v0, 0x89

    if-le p1, v0, :cond_1

    .line 2114
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "error code out of range: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2116
    :cond_1
    iput-boolean p5, p0, Lbph;->c:Z

    .line 2117
    iput p1, p0, Lbph;->d:I

    .line 2118
    if-eqz p2, :cond_3

    .line 2119
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " -- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lbph;->c:Z

    if-eqz v0, :cond_2

    const-string v0, "[INJECTED] -- "

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbph;->e:Ljava/lang/String;

    .line 2125
    :goto_1
    if-ne p1, v2, :cond_5

    instance-of v0, p2, Lbxl;

    if-eqz v0, :cond_5

    .line 2127
    check-cast p2, Lbxl;

    iput-object p2, p0, Lbph;->g:Lbxl;

    .line 2131
    :goto_2
    iput-wide p3, p0, Lbph;->f:J

    .line 2132
    return-void

    .line 2119
    :cond_2
    const-string v0, ""

    goto :goto_0

    .line 2122
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v0, p0, Lbph;->c:Z

    if-eqz v0, :cond_4

    const-string v0, "[INJECTED] -- "

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbph;->e:Ljava/lang/String;

    goto :goto_1

    :cond_4
    const-string v0, ""

    goto :goto_3

    .line 2129
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lbph;->g:Lbxl;

    goto :goto_2
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 7

    .prologue
    .line 2147
    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lbph;-><init>(ILjava/lang/Exception;JZLjava/lang/String;)V

    .line 2148
    return-void
.end method

.method static synthetic a(Lbph;)J
    .locals 2

    .prologue
    .line 1989
    iget-wide v0, p0, Lbph;->f:J

    return-wide v0
.end method


# virtual methods
.method public a()Z
    .locals 2

    .prologue
    .line 2161
    iget v0, p0, Lbph;->d:I

    const/16 v1, 0x68

    if-eq v0, v1, :cond_0

    iget v0, p0, Lbph;->d:I

    const/16 v1, 0x67

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2180
    iget v2, p0, Lbph;->d:I

    packed-switch v2, :pswitch_data_0

    .line 2253
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    move v0, v1

    .line 2189
    goto :goto_0

    :pswitch_2
    move v0, v1

    .line 2198
    goto :goto_0

    .line 2204
    :pswitch_3
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->l()I

    move-result v2

    if-ge p1, v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_4
    move v0, v1

    .line 2220
    goto :goto_0

    :pswitch_5
    move v0, v1

    .line 2231
    goto :goto_0

    .line 2180
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 2169
    iget v0, p0, Lbph;->d:I

    packed-switch v0, :pswitch_data_0

    .line 2175
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2173
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 2169
    nop

    :pswitch_data_0
    .packed-switch 0x6a
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 2261
    iget v0, p0, Lbph;->d:I

    const/16 v1, 0x68

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Lbxl;
    .locals 1

    .prologue
    .line 2268
    iget-object v0, p0, Lbph;->g:Lbxl;

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 2272
    iget v0, p0, Lbph;->d:I

    return v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2277
    iget-object v0, p0, Lbph;->e:Ljava/lang/String;

    return-object v0
.end method
