.class final Ladp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Ladn;

.field private final b:Laei;

.field private final c:I

.field private final d:Landroid/view/View;


# direct methods
.method public constructor <init>(Ladn;ILjava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Laeh;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 219
    iput-object p1, p0, Ladp;->a:Ladn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 220
    iput p2, p0, Ladp;->c:I

    .line 222
    invoke-virtual {p1}, Ladn;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 223
    sget v1, Lf;->es:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ladp;->d:Landroid/view/View;

    .line 224
    iget-object v0, p0, Ladp;->d:Landroid/view/View;

    sget v1, Lg;->fm:I

    .line 225
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 226
    if-nez p2, :cond_0

    sget v1, Lh;->U:I

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 231
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 232
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeh;

    .line 233
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 226
    :cond_0
    sget v1, Lh;->V:I

    goto :goto_0

    .line 235
    :cond_1
    new-instance v0, Laei;

    invoke-virtual {p1}, Ladn;->getActivity()Ly;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Laei;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Ladp;->b:Laei;

    .line 237
    iget-object v0, p0, Ladp;->d:Landroid/view/View;

    sget v1, Lg;->fl:I

    .line 238
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 239
    iget-object v1, p0, Ladp;->b:Laei;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 240
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 241
    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Ladp;->d:Landroid/view/View;

    return-object v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 249
    iget-object v0, p0, Ladp;->a:Ladn;

    invoke-virtual {v0}, Ladn;->b()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Ladp;->a:Ladn;

    invoke-virtual {v0}, Ladn;->b()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 253
    :cond_0
    iget-object v0, p0, Ladp;->b:Laei;

    invoke-virtual {v0, p3}, Laei;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laee;

    .line 254
    instance-of v1, v0, Laeh;

    invoke-static {v1}, Lcwz;->a(Z)V

    .line 256
    instance-of v1, v0, Laed;

    if-eqz v1, :cond_1

    .line 257
    iget-object v0, p0, Ladp;->a:Ladn;

    invoke-static {v0}, Ladn;->g(Ladn;)Ladq;

    move-result-object v0

    invoke-interface {v0}, Ladq;->j()V

    .line 263
    :goto_0
    return-void

    .line 259
    :cond_1
    check-cast v0, Laeh;

    iget-object v1, v0, Laeh;->a:Ljava/lang/String;

    .line 260
    iget v0, p0, Ladp;->c:I

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 261
    :goto_1
    iget-object v2, p0, Ladp;->a:Ladn;

    invoke-static {v2}, Ladn;->g(Ladn;)Ladq;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Ladq;->a(ZLjava/lang/String;)V

    goto :goto_0

    .line 260
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
