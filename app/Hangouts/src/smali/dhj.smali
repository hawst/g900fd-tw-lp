.class public final Ldhj;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldhj;


# instance fields
.field public b:Ljava/lang/String;

.field public c:[Ldhk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6559
    const/4 v0, 0x0

    new-array v0, v0, [Ldhj;

    sput-object v0, Ldhj;->a:[Ldhj;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6560
    invoke-direct {p0}, Lepn;-><init>()V

    .line 6565
    sget-object v0, Ldhk;->a:[Ldhk;

    iput-object v0, p0, Ldhj;->c:[Ldhk;

    .line 6560
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 6587
    iget-object v0, p0, Ldhj;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 6588
    const/4 v0, 0x1

    iget-object v2, p0, Ldhj;->b:Ljava/lang/String;

    .line 6589
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6591
    :goto_0
    iget-object v2, p0, Ldhj;->c:[Ldhk;

    if-eqz v2, :cond_1

    .line 6592
    iget-object v2, p0, Ldhj;->c:[Ldhk;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 6593
    if-eqz v4, :cond_0

    .line 6594
    const/4 v5, 0x2

    .line 6595
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 6592
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6599
    :cond_1
    iget-object v1, p0, Ldhj;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6600
    iput v0, p0, Ldhj;->cachedSize:I

    .line 6601
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6556
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldhj;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldhj;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldhj;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldhj;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldhj;->c:[Ldhk;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldhk;

    iget-object v3, p0, Ldhj;->c:[Ldhk;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldhj;->c:[Ldhk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldhj;->c:[Ldhk;

    :goto_2
    iget-object v2, p0, Ldhj;->c:[Ldhk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldhj;->c:[Ldhk;

    new-instance v3, Ldhk;

    invoke-direct {v3}, Ldhk;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldhj;->c:[Ldhk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldhj;->c:[Ldhk;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldhj;->c:[Ldhk;

    new-instance v3, Ldhk;

    invoke-direct {v3}, Ldhk;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldhj;->c:[Ldhk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 6570
    iget-object v0, p0, Ldhj;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 6571
    const/4 v0, 0x1

    iget-object v1, p0, Ldhj;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 6573
    :cond_0
    iget-object v0, p0, Ldhj;->c:[Ldhk;

    if-eqz v0, :cond_2

    .line 6574
    iget-object v1, p0, Ldhj;->c:[Ldhk;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 6575
    if-eqz v3, :cond_1

    .line 6576
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 6574
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6580
    :cond_2
    iget-object v0, p0, Ldhj;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 6582
    return-void
.end method
