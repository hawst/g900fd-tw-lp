.class public final Lbf;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Lbn;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 601
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_0

    .line 602
    new-instance v0, Lbo;

    invoke-direct {v0}, Lbo;-><init>()V

    sput-object v0, Lbf;->a:Lbn;

    .line 616
    :goto_0
    return-void

    .line 603
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 604
    new-instance v0, Lbt;

    invoke-direct {v0}, Lbt;-><init>()V

    sput-object v0, Lbf;->a:Lbn;

    goto :goto_0

    .line 605
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_2

    .line 606
    new-instance v0, Lbs;

    invoke-direct {v0}, Lbs;-><init>()V

    sput-object v0, Lbf;->a:Lbn;

    goto :goto_0

    .line 607
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_3

    .line 608
    new-instance v0, Lbr;

    invoke-direct {v0}, Lbr;-><init>()V

    sput-object v0, Lbf;->a:Lbn;

    goto :goto_0

    .line 609
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_4

    .line 610
    new-instance v0, Lbq;

    invoke-direct {v0}, Lbq;-><init>()V

    sput-object v0, Lbf;->a:Lbn;

    goto :goto_0

    .line 611
    :cond_4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_5

    .line 612
    new-instance v0, Lbp;

    invoke-direct {v0}, Lbp;-><init>()V

    sput-object v0, Lbf;->a:Lbn;

    goto :goto_0

    .line 614
    :cond_5
    new-instance v0, Lbn;

    invoke-direct {v0}, Lbn;-><init>()V

    sput-object v0, Lbf;->a:Lbn;

    goto :goto_0
.end method

.method public static a(Landroid/app/Notification;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 2395
    sget-object v0, Lbf;->a:Lbn;

    invoke-virtual {v0, p0}, Lbn;->a(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a()Lbn;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lbf;->a:Lbn;

    return-object v0
.end method

.method static synthetic a(Lbd;Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 40
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbg;

    invoke-interface {p0, v0}, Lbd;->a(Lbx;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(Lbe;Lbu;)V
    .locals 7

    .prologue
    .line 40
    if-eqz p1, :cond_0

    instance-of v0, p1, Lbj;

    if-eqz v0, :cond_1

    check-cast p1, Lbj;

    iget-object v0, p1, Lbj;->e:Ljava/lang/CharSequence;

    iget-boolean v1, p1, Lbj;->g:Z

    iget-object v2, p1, Lbj;->f:Ljava/lang/CharSequence;

    iget-object v3, p1, Lbj;->a:Ljava/lang/CharSequence;

    invoke-static {p0, v0, v1, v2, v3}, Lbz;->a(Lbe;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p1, Lbm;

    if-eqz v0, :cond_2

    check-cast p1, Lbm;

    iget-object v0, p1, Lbm;->e:Ljava/lang/CharSequence;

    iget-boolean v1, p1, Lbm;->g:Z

    iget-object v2, p1, Lbm;->f:Ljava/lang/CharSequence;

    iget-object v3, p1, Lbm;->a:Ljava/util/ArrayList;

    invoke-static {p0, v0, v1, v2, v3}, Lbz;->a(Lbe;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lbi;

    if-eqz v0, :cond_0

    check-cast p1, Lbi;

    iget-object v1, p1, Lbi;->e:Ljava/lang/CharSequence;

    iget-boolean v2, p1, Lbi;->g:Z

    iget-object v3, p1, Lbi;->f:Ljava/lang/CharSequence;

    iget-object v4, p1, Lbi;->a:Landroid/graphics/Bitmap;

    iget-object v5, p1, Lbi;->b:Landroid/graphics/Bitmap;

    iget-boolean v6, p1, Lbi;->c:Z

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lbz;->a(Lbe;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)V

    goto :goto_0
.end method
