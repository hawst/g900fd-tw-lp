.class public final Letz;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Letz;


# instance fields
.field public b:Ljava/lang/String;

.field public c:[Leua;

.field public d:[Leua;

.field public e:Ljava/lang/Boolean;

.field public f:Letp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Letz;

    sput-object v0, Letz;->a:[Letz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 88
    sget-object v0, Leua;->a:[Leua;

    iput-object v0, p0, Letz;->c:[Leua;

    .line 91
    sget-object v0, Leua;->a:[Leua;

    iput-object v0, p0, Letz;->d:[Leua;

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Letz;->f:Letp;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 131
    iget-object v0, p0, Letz;->b:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 132
    const/4 v0, 0x1

    iget-object v2, p0, Letz;->b:Ljava/lang/String;

    .line 133
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 135
    :goto_0
    iget-object v2, p0, Letz;->c:[Leua;

    if-eqz v2, :cond_1

    .line 136
    iget-object v3, p0, Letz;->c:[Leua;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 137
    if-eqz v5, :cond_0

    .line 138
    const/4 v6, 0x2

    .line 139
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 136
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 143
    :cond_1
    iget-object v2, p0, Letz;->d:[Leua;

    if-eqz v2, :cond_3

    .line 144
    iget-object v2, p0, Letz;->d:[Leua;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 145
    if-eqz v4, :cond_2

    .line 146
    const/4 v5, 0x3

    .line 147
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 144
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 151
    :cond_3
    iget-object v1, p0, Letz;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 152
    const/4 v1, 0x4

    iget-object v2, p0, Letz;->e:Ljava/lang/Boolean;

    .line 153
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 155
    :cond_4
    iget-object v1, p0, Letz;->f:Letp;

    if-eqz v1, :cond_5

    .line 156
    const/4 v1, 0x5

    iget-object v2, p0, Letz;->f:Letp;

    .line 157
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 159
    :cond_5
    iget-object v1, p0, Letz;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 160
    iput v0, p0, Letz;->cachedSize:I

    .line 161
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Letz;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Letz;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Letz;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Letz;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Letz;->c:[Leua;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leua;

    iget-object v3, p0, Letz;->c:[Leua;

    if-eqz v3, :cond_2

    iget-object v3, p0, Letz;->c:[Leua;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Letz;->c:[Leua;

    :goto_2
    iget-object v2, p0, Letz;->c:[Leua;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Letz;->c:[Leua;

    new-instance v3, Leua;

    invoke-direct {v3}, Leua;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Letz;->c:[Leua;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Letz;->c:[Leua;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Letz;->c:[Leua;

    new-instance v3, Leua;

    invoke-direct {v3}, Leua;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Letz;->c:[Leua;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Letz;->d:[Leua;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Leua;

    iget-object v3, p0, Letz;->d:[Leua;

    if-eqz v3, :cond_5

    iget-object v3, p0, Letz;->d:[Leua;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Letz;->d:[Leua;

    :goto_4
    iget-object v2, p0, Letz;->d:[Leua;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Letz;->d:[Leua;

    new-instance v3, Leua;

    invoke-direct {v3}, Leua;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Letz;->d:[Leua;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Letz;->d:[Leua;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Letz;->d:[Leua;

    new-instance v3, Leua;

    invoke-direct {v3}, Leua;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Letz;->d:[Leua;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Letz;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Letz;->f:Letp;

    if-nez v0, :cond_8

    new-instance v0, Letp;

    invoke-direct {v0}, Letp;-><init>()V

    iput-object v0, p0, Letz;->f:Letp;

    :cond_8
    iget-object v0, p0, Letz;->f:Letp;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 101
    iget-object v1, p0, Letz;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 102
    const/4 v1, 0x1

    iget-object v2, p0, Letz;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 104
    :cond_0
    iget-object v1, p0, Letz;->c:[Leua;

    if-eqz v1, :cond_2

    .line 105
    iget-object v2, p0, Letz;->c:[Leua;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 106
    if-eqz v4, :cond_1

    .line 107
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 105
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 111
    :cond_2
    iget-object v1, p0, Letz;->d:[Leua;

    if-eqz v1, :cond_4

    .line 112
    iget-object v1, p0, Letz;->d:[Leua;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 113
    if-eqz v3, :cond_3

    .line 114
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 112
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 118
    :cond_4
    iget-object v0, p0, Letz;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 119
    const/4 v0, 0x4

    iget-object v1, p0, Letz;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 121
    :cond_5
    iget-object v0, p0, Letz;->f:Letp;

    if-eqz v0, :cond_6

    .line 122
    const/4 v0, 0x5

    iget-object v1, p0, Letz;->f:Letp;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 124
    :cond_6
    iget-object v0, p0, Letz;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 126
    return-void
.end method
