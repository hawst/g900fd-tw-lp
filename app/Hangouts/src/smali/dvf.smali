.class public final Ldvf;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldvf;


# instance fields
.field public b:Ldvm;

.field public c:Ldrx;

.field public d:Ldui;

.field public e:[B

.field public f:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6189
    const/4 v0, 0x0

    new-array v0, v0, [Ldvf;

    sput-object v0, Ldvf;->a:[Ldvf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6190
    invoke-direct {p0}, Lepn;-><init>()V

    .line 6193
    iput-object v0, p0, Ldvf;->b:Ldvm;

    .line 6196
    iput-object v0, p0, Ldvf;->c:Ldrx;

    .line 6199
    iput-object v0, p0, Ldvf;->d:Ldui;

    .line 6190
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 6229
    const/4 v0, 0x0

    .line 6230
    iget-object v1, p0, Ldvf;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 6231
    const/4 v0, 0x1

    iget-object v1, p0, Ldvf;->b:Ldvm;

    .line 6232
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6234
    :cond_0
    iget-object v1, p0, Ldvf;->e:[B

    if-eqz v1, :cond_1

    .line 6235
    const/4 v1, 0x2

    iget-object v2, p0, Ldvf;->e:[B

    .line 6236
    invoke-static {v1, v2}, Lepl;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 6238
    :cond_1
    iget-object v1, p0, Ldvf;->d:Ldui;

    if-eqz v1, :cond_2

    .line 6239
    const/4 v1, 0x3

    iget-object v2, p0, Ldvf;->d:Ldui;

    .line 6240
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6242
    :cond_2
    iget-object v1, p0, Ldvf;->f:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 6243
    const/4 v1, 0x4

    iget-object v2, p0, Ldvf;->f:Ljava/lang/Long;

    .line 6244
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 6246
    :cond_3
    iget-object v1, p0, Ldvf;->c:Ldrx;

    if-eqz v1, :cond_4

    .line 6247
    const/4 v1, 0x5

    iget-object v2, p0, Ldvf;->c:Ldrx;

    .line 6248
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6250
    :cond_4
    iget-object v1, p0, Ldvf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6251
    iput v0, p0, Ldvf;->cachedSize:I

    .line 6252
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6186
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldvf;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldvf;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldvf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldvf;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldvf;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldvf;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Ldvf;->e:[B

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldvf;->d:Ldui;

    if-nez v0, :cond_3

    new-instance v0, Ldui;

    invoke-direct {v0}, Ldui;-><init>()V

    iput-object v0, p0, Ldvf;->d:Ldui;

    :cond_3
    iget-object v0, p0, Ldvf;->d:Ldui;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldvf;->f:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldvf;->c:Ldrx;

    if-nez v0, :cond_4

    new-instance v0, Ldrx;

    invoke-direct {v0}, Ldrx;-><init>()V

    iput-object v0, p0, Ldvf;->c:Ldrx;

    :cond_4
    iget-object v0, p0, Ldvf;->c:Ldrx;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 6208
    iget-object v0, p0, Ldvf;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 6209
    const/4 v0, 0x1

    iget-object v1, p0, Ldvf;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6211
    :cond_0
    iget-object v0, p0, Ldvf;->e:[B

    if-eqz v0, :cond_1

    .line 6212
    const/4 v0, 0x2

    iget-object v1, p0, Ldvf;->e:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 6214
    :cond_1
    iget-object v0, p0, Ldvf;->d:Ldui;

    if-eqz v0, :cond_2

    .line 6215
    const/4 v0, 0x3

    iget-object v1, p0, Ldvf;->d:Ldui;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6217
    :cond_2
    iget-object v0, p0, Ldvf;->f:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 6218
    const/4 v0, 0x4

    iget-object v1, p0, Ldvf;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 6220
    :cond_3
    iget-object v0, p0, Ldvf;->c:Ldrx;

    if-eqz v0, :cond_4

    .line 6221
    const/4 v0, 0x5

    iget-object v1, p0, Ldvf;->c:Ldrx;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6223
    :cond_4
    iget-object v0, p0, Ldvf;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 6225
    return-void
.end method
