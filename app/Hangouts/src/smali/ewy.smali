.class public final Lewy;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lewy;


# instance fields
.field public b:Lexn;

.field public c:[Lewx;

.field public d:Levt;

.field public e:Lexc;

.field public f:Lewb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lewy;

    sput-object v0, Lewy;->a:[Lewy;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 13
    iput-object v1, p0, Lewy;->b:Lexn;

    .line 16
    sget-object v0, Lewx;->a:[Lewx;

    iput-object v0, p0, Lewy;->c:[Lewx;

    .line 19
    iput-object v1, p0, Lewy;->d:Levt;

    .line 22
    iput-object v1, p0, Lewy;->e:Lexc;

    .line 25
    iput-object v1, p0, Lewy;->f:Lewb;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 56
    iget-object v0, p0, Lewy;->b:Lexn;

    if-eqz v0, :cond_5

    .line 57
    const/4 v0, 0x1

    iget-object v2, p0, Lewy;->b:Lexn;

    .line 58
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 60
    :goto_0
    iget-object v2, p0, Lewy;->c:[Lewx;

    if-eqz v2, :cond_1

    .line 61
    iget-object v2, p0, Lewy;->c:[Lewx;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 62
    if-eqz v4, :cond_0

    .line 63
    const/4 v5, 0x2

    .line 64
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 61
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 68
    :cond_1
    iget-object v1, p0, Lewy;->d:Levt;

    if-eqz v1, :cond_2

    .line 69
    const/4 v1, 0x3

    iget-object v2, p0, Lewy;->d:Levt;

    .line 70
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    :cond_2
    iget-object v1, p0, Lewy;->e:Lexc;

    if-eqz v1, :cond_3

    .line 73
    const/4 v1, 0x4

    iget-object v2, p0, Lewy;->e:Lexc;

    .line 74
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    :cond_3
    iget-object v1, p0, Lewy;->f:Lewb;

    if-eqz v1, :cond_4

    .line 77
    const/4 v1, 0x5

    iget-object v2, p0, Lewy;->f:Lewb;

    .line 78
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    :cond_4
    iget-object v1, p0, Lewy;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    iput v0, p0, Lewy;->cachedSize:I

    .line 82
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lewy;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lewy;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lewy;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lewy;->b:Lexn;

    if-nez v0, :cond_2

    new-instance v0, Lexn;

    invoke-direct {v0}, Lexn;-><init>()V

    iput-object v0, p0, Lewy;->b:Lexn;

    :cond_2
    iget-object v0, p0, Lewy;->b:Lexn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lewy;->c:[Lewx;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lewx;

    iget-object v3, p0, Lewy;->c:[Lewx;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lewy;->c:[Lewx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lewy;->c:[Lewx;

    :goto_2
    iget-object v2, p0, Lewy;->c:[Lewx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lewy;->c:[Lewx;

    new-instance v3, Lewx;

    invoke-direct {v3}, Lewx;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lewy;->c:[Lewx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lewy;->c:[Lewx;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lewy;->c:[Lewx;

    new-instance v3, Lewx;

    invoke-direct {v3}, Lewx;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lewy;->c:[Lewx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lewy;->d:Levt;

    if-nez v0, :cond_6

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Lewy;->d:Levt;

    :cond_6
    iget-object v0, p0, Lewy;->d:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Lewy;->e:Lexc;

    if-nez v0, :cond_7

    new-instance v0, Lexc;

    invoke-direct {v0}, Lexc;-><init>()V

    iput-object v0, p0, Lewy;->e:Lexc;

    :cond_7
    iget-object v0, p0, Lewy;->e:Lexc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lewy;->f:Lewb;

    if-nez v0, :cond_8

    new-instance v0, Lewb;

    invoke-direct {v0}, Lewb;-><init>()V

    iput-object v0, p0, Lewy;->f:Lewb;

    :cond_8
    iget-object v0, p0, Lewy;->f:Lewb;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 30
    iget-object v0, p0, Lewy;->b:Lexn;

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x1

    iget-object v1, p0, Lewy;->b:Lexn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 33
    :cond_0
    iget-object v0, p0, Lewy;->c:[Lewx;

    if-eqz v0, :cond_2

    .line 34
    iget-object v1, p0, Lewy;->c:[Lewx;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 35
    if-eqz v3, :cond_1

    .line 36
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 34
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 40
    :cond_2
    iget-object v0, p0, Lewy;->d:Levt;

    if-eqz v0, :cond_3

    .line 41
    const/4 v0, 0x3

    iget-object v1, p0, Lewy;->d:Levt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 43
    :cond_3
    iget-object v0, p0, Lewy;->e:Lexc;

    if-eqz v0, :cond_4

    .line 44
    const/4 v0, 0x4

    iget-object v1, p0, Lewy;->e:Lexc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 46
    :cond_4
    iget-object v0, p0, Lewy;->f:Lewb;

    if-eqz v0, :cond_5

    .line 47
    const/4 v0, 0x5

    iget-object v1, p0, Lewy;->f:Lewb;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 49
    :cond_5
    iget-object v0, p0, Lewy;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 51
    return-void
.end method
