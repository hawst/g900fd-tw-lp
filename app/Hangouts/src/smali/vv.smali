.class public final Lvv;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lvv;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:[Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 520
    const/4 v0, 0x0

    new-array v0, v0, [Lvv;

    sput-object v0, Lvv;->a:[Lvv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 521
    invoke-direct {p0}, Lepn;-><init>()V

    .line 528
    sget-object v0, Lept;->m:[Ljava/lang/Integer;

    iput-object v0, p0, Lvv;->d:[Ljava/lang/Integer;

    .line 531
    const/4 v0, 0x0

    iput-object v0, p0, Lvv;->e:Ljava/lang/Integer;

    .line 521
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 562
    iget-object v0, p0, Lvv;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 563
    const/4 v0, 0x1

    iget-object v2, p0, Lvv;->b:Ljava/lang/Integer;

    .line 564
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 566
    :goto_0
    iget-object v2, p0, Lvv;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 567
    const/4 v2, 0x2

    iget-object v3, p0, Lvv;->c:Ljava/lang/Integer;

    .line 568
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 570
    :cond_0
    iget-object v2, p0, Lvv;->d:[Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lvv;->d:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 572
    iget-object v3, p0, Lvv;->d:[Ljava/lang/Integer;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 574
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 572
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 576
    :cond_1
    add-int/2addr v0, v2

    .line 577
    iget-object v1, p0, Lvv;->d:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 579
    :cond_2
    iget-object v1, p0, Lvv;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 580
    const/4 v1, 0x4

    iget-object v2, p0, Lvv;->e:Ljava/lang/Integer;

    .line 581
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 583
    :cond_3
    iget-object v1, p0, Lvv;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 584
    const/4 v1, 0x5

    iget-object v2, p0, Lvv;->f:Ljava/lang/Boolean;

    .line 585
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 587
    :cond_4
    iget-object v1, p0, Lvv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 588
    iput v0, p0, Lvv;->cachedSize:I

    .line 589
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 517
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lvv;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lvv;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lvv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lvv;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lvv;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Lvv;->d:[Ljava/lang/Integer;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    iget-object v2, p0, Lvv;->d:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lvv;->d:[Ljava/lang/Integer;

    :goto_1
    iget-object v1, p0, Lvv;->d:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lvv;->d:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lvv;->d:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-eq v0, v1, :cond_3

    const/4 v1, 0x7

    if-eq v0, v1, :cond_3

    const/16 v1, 0x8

    if-eq v0, v1, :cond_3

    const/16 v1, 0x9

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa

    if-eq v0, v1, :cond_3

    const/16 v1, 0xb

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc

    if-eq v0, v1, :cond_3

    const/16 v1, 0xd

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe

    if-eq v0, v1, :cond_3

    const/16 v1, 0xf

    if-eq v0, v1, :cond_3

    const/16 v1, 0x10

    if-eq v0, v1, :cond_3

    const/16 v1, 0x11

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lvv;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_4
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lvv;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lvv;->f:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 538
    iget-object v0, p0, Lvv;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 539
    const/4 v0, 0x1

    iget-object v1, p0, Lvv;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 541
    :cond_0
    iget-object v0, p0, Lvv;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 542
    const/4 v0, 0x2

    iget-object v1, p0, Lvv;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 544
    :cond_1
    iget-object v0, p0, Lvv;->d:[Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 545
    iget-object v1, p0, Lvv;->d:[Ljava/lang/Integer;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 546
    const/4 v4, 0x3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 545
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 549
    :cond_2
    iget-object v0, p0, Lvv;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 550
    const/4 v0, 0x4

    iget-object v1, p0, Lvv;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 552
    :cond_3
    iget-object v0, p0, Lvv;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 553
    const/4 v0, 0x5

    iget-object v1, p0, Lvv;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 555
    :cond_4
    iget-object v0, p0, Lvv;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 557
    return-void
.end method
