.class final Lbsp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lbsv;

.field final synthetic b:[B


# direct methods
.method constructor <init>(Lbsv;[B)V
    .locals 0

    .prologue
    .line 357
    iput-object p1, p0, Lbsp;->a:Lbsv;

    iput-object p2, p0, Lbsp;->b:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 361
    :try_start_0
    const-string v0, "MediaLoader decode"

    invoke-static {v0}, Lbzq;->a(Ljava/lang/String;)V

    .line 362
    invoke-static {}, Lbsn;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363
    invoke-static {}, Lbsn;->e()Lbsn;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "decodingBytes start "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbsp;->a:Lbsv;

    .line 364
    invoke-virtual {v2}, Lbsv;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 363
    invoke-static {v0, v1}, Lbsn;->a(Lbsn;Ljava/lang/String;)V

    .line 366
    :cond_0
    iget-object v0, p0, Lbsp;->a:Lbsv;

    iget-object v1, p0, Lbsp;->b:[B

    .line 367
    invoke-virtual {v0, v1}, Lbsv;->a([B)Lbsu;

    move-result-object v0

    .line 368
    invoke-static {}, Lbsn;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 369
    invoke-static {}, Lbsn;->e()Lbsn;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "decodingBytes finish "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lbsp;->a:Lbsv;

    .line 370
    invoke-virtual {v3}, Lbsv;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 369
    invoke-static {v1, v2}, Lbsn;->a(Lbsn;Ljava/lang/String;)V

    .line 372
    :cond_1
    if-eqz v0, :cond_2

    .line 373
    iget-object v1, p0, Lbsp;->a:Lbsv;

    invoke-static {v1, v0}, Lbsn;->a(Lbsv;Lbsu;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 376
    :cond_2
    invoke-static {}, Lbzq;->a()V

    .line 377
    return-void

    .line 376
    :catchall_0
    move-exception v0

    invoke-static {}, Lbzq;->a()V

    throw v0
.end method
