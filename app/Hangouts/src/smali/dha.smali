.class public final Ldha;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldha;


# instance fields
.field public b:Ljava/lang/Double;

.field public c:Ljava/lang/Double;

.field public d:Ljava/lang/Double;

.field public e:Ljava/lang/Double;

.field public f:Ljava/lang/Double;

.field public g:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6949
    const/4 v0, 0x0

    new-array v0, v0, [Ldha;

    sput-object v0, Ldha;->a:[Ldha;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6950
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 6987
    const/4 v0, 0x1

    iget-object v1, p0, Ldha;->b:Ljava/lang/Double;

    .line 6989
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 6990
    const/4 v1, 0x2

    iget-object v2, p0, Ldha;->c:Ljava/lang/Double;

    .line 6991
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 6992
    iget-object v1, p0, Ldha;->d:Ljava/lang/Double;

    if-eqz v1, :cond_0

    .line 6993
    const/4 v1, 0x3

    iget-object v2, p0, Ldha;->d:Ljava/lang/Double;

    .line 6994
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 6996
    :cond_0
    iget-object v1, p0, Ldha;->e:Ljava/lang/Double;

    if-eqz v1, :cond_1

    .line 6997
    const/4 v1, 0x4

    iget-object v2, p0, Ldha;->e:Ljava/lang/Double;

    .line 6998
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 7000
    :cond_1
    iget-object v1, p0, Ldha;->f:Ljava/lang/Double;

    if-eqz v1, :cond_2

    .line 7001
    const/4 v1, 0x5

    iget-object v2, p0, Ldha;->f:Ljava/lang/Double;

    .line 7002
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 7004
    :cond_2
    iget-object v1, p0, Ldha;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 7005
    const/4 v1, 0x6

    iget-object v2, p0, Ldha;->g:Ljava/lang/Boolean;

    .line 7006
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7008
    :cond_3
    iget-object v1, p0, Ldha;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7009
    iput v0, p0, Ldha;->cachedSize:I

    .line 7010
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6946
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldha;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldha;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldha;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->b()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Ldha;->b:Ljava/lang/Double;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->b()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Ldha;->c:Ljava/lang/Double;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->b()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Ldha;->d:Ljava/lang/Double;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->b()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Ldha;->e:Ljava/lang/Double;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->b()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Ldha;->f:Ljava/lang/Double;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldha;->g:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
        0x19 -> :sswitch_3
        0x21 -> :sswitch_4
        0x29 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 6967
    const/4 v0, 0x1

    iget-object v1, p0, Ldha;->b:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(ID)V

    .line 6968
    const/4 v0, 0x2

    iget-object v1, p0, Ldha;->c:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(ID)V

    .line 6969
    iget-object v0, p0, Ldha;->d:Ljava/lang/Double;

    if-eqz v0, :cond_0

    .line 6970
    const/4 v0, 0x3

    iget-object v1, p0, Ldha;->d:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(ID)V

    .line 6972
    :cond_0
    iget-object v0, p0, Ldha;->e:Ljava/lang/Double;

    if-eqz v0, :cond_1

    .line 6973
    const/4 v0, 0x4

    iget-object v1, p0, Ldha;->e:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(ID)V

    .line 6975
    :cond_1
    iget-object v0, p0, Ldha;->f:Ljava/lang/Double;

    if-eqz v0, :cond_2

    .line 6976
    const/4 v0, 0x5

    iget-object v1, p0, Ldha;->f:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(ID)V

    .line 6978
    :cond_2
    iget-object v0, p0, Ldha;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 6979
    const/4 v0, 0x6

    iget-object v1, p0, Ldha;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 6981
    :cond_3
    iget-object v0, p0, Ldha;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 6983
    return-void
.end method
