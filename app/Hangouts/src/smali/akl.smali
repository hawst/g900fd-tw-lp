.class public abstract Lakl;
.super Lt;
.source "PG"


# instance fields
.field private a:Z

.field private b:Z

.field private final c:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lt;-><init>()V

    .line 53
    new-instance v0, Lakm;

    invoke-direct {v0, p0}, Lakm;-><init>(Lakl;)V

    iput-object v0, p0, Lakl;->c:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 145
    invoke-virtual {p0}, Lakl;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 147
    sget v0, Lg;->ec:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 148
    sget v0, Lg;->eb:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 150
    :cond_0
    return-void
.end method

.method protected a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 183
    sget v0, Lg;->ec:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 184
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    .line 185
    return-void
.end method

.method protected ae()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lakl;->a:Z

    return v0
.end method

.method protected af()V
    .locals 1

    .prologue
    .line 133
    invoke-virtual {p0}, Lakl;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lakl;->ae()Z

    move-result v0

    if-nez v0, :cond_0

    .line 134
    invoke-virtual {p0}, Lakl;->getView()Landroid/view/View;

    move-result-object v0

    .line 135
    if-eqz v0, :cond_0

    .line 136
    invoke-virtual {p0, v0}, Lakl;->a(Landroid/view/View;)V

    .line 139
    :cond_0
    return-void
.end method

.method protected ag()V
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lakl;->c:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 192
    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 172
    invoke-virtual {p0}, Lakl;->ag()V

    .line 173
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 174
    return-void
.end method

.method public c(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 158
    invoke-virtual {p0}, Lakl;->ag()V

    .line 159
    invoke-virtual {p0}, Lakl;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 161
    sget v0, Lg;->ec:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 162
    sget v0, Lg;->eb:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 164
    :cond_0
    return-void
.end method

.method public d(Landroid/os/Bundle;)Lyj;
    .locals 3

    .prologue
    .line 195
    const-string v0, "account_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 196
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 197
    if-nez v0, :cond_0

    .line 198
    const-string v1, "Babel"

    const-string v2, "Delete conversation called for unknown account"

    invoke-static {v1, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    :cond_0
    return-object v0
.end method

.method public e(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 119
    iget-boolean v0, p0, Lakl;->b:Z

    if-eqz v0, :cond_1

    .line 120
    iget-object v0, p0, Lakl;->c:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lakl;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lakl;->c:Landroid/os/Handler;

    const-wide/16 v1, 0x320

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    invoke-virtual {p0, p1}, Lakl;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public f(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 204
    sget v0, Lh;->jj:I

    .line 205
    invoke-virtual {p0, v0}, Lakl;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lh;->ji:I

    .line 206
    invoke-virtual {p0, v1}, Lakl;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lh;->jh:I

    .line 207
    invoke-virtual {p0, v2}, Lakl;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lh;->ab:I

    .line 208
    invoke-virtual {p0, v3}, Lakl;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 204
    invoke-static {v0, v1, v2, v3}, Labe;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Labe;

    move-result-object v0

    .line 209
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Labe;->setTargetFragment(Lt;I)V

    .line 210
    invoke-virtual {v0}, Labe;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    invoke-virtual {p0}, Lakl;->getFragmentManager()Lae;

    move-result-object v1

    const-string v2, "delete_conversation"

    invoke-virtual {v0, v1, v2}, Labe;->a(Lae;Ljava/lang/String;)V

    .line 212
    return-void
.end method

.method public abstract isEmpty()Z
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0, p1}, Lt;->onCreate(Landroid/os/Bundle;)V

    .line 67
    if-eqz p1, :cond_0

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lakl;->b:Z

    .line 70
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-virtual {p1, p4, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 101
    invoke-super {p0}, Lt;->onPause()V

    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lakl;->a:Z

    .line 104
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0}, Lt;->onResume()V

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lakl;->a:Z

    .line 94
    return-void
.end method
