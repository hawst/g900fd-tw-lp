.class public final Ldne;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldne;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/Integer;

.field public k:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1673
    const/4 v0, 0x0

    new-array v0, v0, [Ldne;

    sput-object v0, Ldne;->a:[Ldne;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1674
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1735
    const/4 v0, 0x0

    .line 1736
    iget-object v1, p0, Ldne;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1737
    const/4 v0, 0x1

    iget-object v1, p0, Ldne;->b:Ljava/lang/String;

    .line 1738
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1740
    :cond_0
    iget-object v1, p0, Ldne;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1741
    const/4 v1, 0x2

    iget-object v2, p0, Ldne;->c:Ljava/lang/String;

    .line 1742
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1744
    :cond_1
    iget-object v1, p0, Ldne;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1745
    const/4 v1, 0x3

    iget-object v2, p0, Ldne;->d:Ljava/lang/String;

    .line 1746
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1748
    :cond_2
    iget-object v1, p0, Ldne;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1749
    const/4 v1, 0x4

    iget-object v2, p0, Ldne;->e:Ljava/lang/String;

    .line 1750
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1752
    :cond_3
    iget-object v1, p0, Ldne;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1753
    const/4 v1, 0x5

    iget-object v2, p0, Ldne;->f:Ljava/lang/String;

    .line 1754
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1756
    :cond_4
    iget-object v1, p0, Ldne;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1757
    const/4 v1, 0x6

    iget-object v2, p0, Ldne;->g:Ljava/lang/String;

    .line 1758
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1760
    :cond_5
    iget-object v1, p0, Ldne;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 1761
    const/4 v1, 0x7

    iget-object v2, p0, Ldne;->h:Ljava/lang/String;

    .line 1762
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1764
    :cond_6
    iget-object v1, p0, Ldne;->i:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 1765
    const/16 v1, 0x8

    iget-object v2, p0, Ldne;->i:Ljava/lang/String;

    .line 1766
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1768
    :cond_7
    iget-object v1, p0, Ldne;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 1769
    const/16 v1, 0x9

    iget-object v2, p0, Ldne;->j:Ljava/lang/Integer;

    .line 1770
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1772
    :cond_8
    iget-object v1, p0, Ldne;->k:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 1773
    const/16 v1, 0xa

    iget-object v2, p0, Ldne;->k:Ljava/lang/String;

    .line 1774
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1776
    :cond_9
    iget-object v1, p0, Ldne;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1777
    iput v0, p0, Ldne;->cachedSize:I

    .line 1778
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1670
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldne;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldne;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldne;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldne;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldne;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldne;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldne;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldne;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldne;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldne;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldne;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldne;->j:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldne;->k:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1699
    iget-object v0, p0, Ldne;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1700
    const/4 v0, 0x1

    iget-object v1, p0, Ldne;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1702
    :cond_0
    iget-object v0, p0, Ldne;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1703
    const/4 v0, 0x2

    iget-object v1, p0, Ldne;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1705
    :cond_1
    iget-object v0, p0, Ldne;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1706
    const/4 v0, 0x3

    iget-object v1, p0, Ldne;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1708
    :cond_2
    iget-object v0, p0, Ldne;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1709
    const/4 v0, 0x4

    iget-object v1, p0, Ldne;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1711
    :cond_3
    iget-object v0, p0, Ldne;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1712
    const/4 v0, 0x5

    iget-object v1, p0, Ldne;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1714
    :cond_4
    iget-object v0, p0, Ldne;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1715
    const/4 v0, 0x6

    iget-object v1, p0, Ldne;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1717
    :cond_5
    iget-object v0, p0, Ldne;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1718
    const/4 v0, 0x7

    iget-object v1, p0, Ldne;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1720
    :cond_6
    iget-object v0, p0, Ldne;->i:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1721
    const/16 v0, 0x8

    iget-object v1, p0, Ldne;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1723
    :cond_7
    iget-object v0, p0, Ldne;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 1724
    const/16 v0, 0x9

    iget-object v1, p0, Ldne;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1726
    :cond_8
    iget-object v0, p0, Ldne;->k:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 1727
    const/16 v0, 0xa

    iget-object v1, p0, Ldne;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1729
    :cond_9
    iget-object v0, p0, Ldne;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1731
    return-void
.end method
