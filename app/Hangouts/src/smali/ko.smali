.class Lko;
.super Lkk;
.source "PG"

# interfaces
.implements Lmb;
.implements Lmq;


# static fields
.field private static final d:[I


# instance fields
.field private e:Landroid/support/v7/internal/widget/ActionBarView;

.field private f:Lly;

.field private g:Lma;

.field private h:Loz;

.field private i:Z

.field private j:Ljava/lang/CharSequence;

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 52
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lf;->n:I

    aput v2, v0, v1

    sput-object v0, Lko;->d:[I

    return-void
.end method

.method constructor <init>(Lkj;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lkk;-><init>(Lkj;)V

    .line 78
    return-void
.end method

.method private n()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 596
    iget-boolean v2, p0, Lko;->n:Z

    if-eqz v2, :cond_0

    .line 659
    :goto_0
    return v0

    .line 601
    :cond_0
    iget-object v2, p0, Lko;->g:Lma;

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lko;->o:Z

    if-eqz v2, :cond_6

    .line 602
    :cond_1
    iget-object v2, p0, Lko;->g:Lma;

    if-nez v2, :cond_2

    .line 603
    new-instance v2, Lma;

    invoke-virtual {p0}, Lko;->l()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lma;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lko;->g:Lma;

    iget-object v2, p0, Lko;->g:Lma;

    invoke-virtual {v2, p0}, Lma;->a(Lmb;)V

    iget-object v2, p0, Lko;->g:Lma;

    if-nez v2, :cond_2

    move v0, v1

    .line 604
    goto :goto_0

    .line 608
    :cond_2
    iget-object v2, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    if-eqz v2, :cond_3

    .line 609
    iget-object v2, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    iget-object v3, p0, Lko;->g:Lma;

    invoke-virtual {v2, v3, p0}, Landroid/support/v7/internal/widget/ActionBarView;->a(Ldw;Lmq;)V

    .line 614
    :cond_3
    iget-object v2, p0, Lko;->g:Lma;

    invoke-virtual {v2}, Lma;->g()V

    .line 617
    iget-object v2, p0, Lko;->a:Lkj;

    iget-object v3, p0, Lko;->g:Lma;

    invoke-virtual {v2, v1, v3}, Lkj;->a(ILandroid/view/Menu;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 619
    iput-object v4, p0, Lko;->g:Lma;

    .line 621
    iget-object v0, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    if-eqz v0, :cond_4

    .line 623
    iget-object v0, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, v4, p0}, Landroid/support/v7/internal/widget/ActionBarView;->a(Ldw;Lmq;)V

    :cond_4
    move v0, v1

    .line 626
    goto :goto_0

    .line 629
    :cond_5
    iput-boolean v1, p0, Lko;->o:Z

    .line 634
    :cond_6
    iget-object v2, p0, Lko;->g:Lma;

    invoke-virtual {v2}, Lma;->g()V

    .line 638
    iget-object v2, p0, Lko;->p:Landroid/os/Bundle;

    if-eqz v2, :cond_7

    .line 639
    iget-object v2, p0, Lko;->g:Lma;

    iget-object v3, p0, Lko;->p:Landroid/os/Bundle;

    invoke-virtual {v2, v3}, Lma;->b(Landroid/os/Bundle;)V

    .line 640
    iput-object v4, p0, Lko;->p:Landroid/os/Bundle;

    .line 644
    :cond_7
    iget-object v2, p0, Lko;->a:Lkj;

    iget-object v3, p0, Lko;->g:Lma;

    invoke-virtual {v2, v1, v4, v3}, Lkj;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 645
    iget-object v0, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    if-eqz v0, :cond_8

    .line 648
    iget-object v0, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, v4, p0}, Landroid/support/v7/internal/widget/ActionBarView;->a(Ldw;Lmq;)V

    .line 650
    :cond_8
    iget-object v0, p0, Lko;->g:Lma;

    invoke-virtual {v0}, Lma;->h()V

    move v0, v1

    .line 651
    goto :goto_0

    .line 654
    :cond_9
    iget-object v1, p0, Lko;->g:Lma;

    invoke-virtual {v1}, Lma;->h()V

    .line 657
    iput-boolean v0, p0, Lko;->n:Z

    goto :goto_0
.end method


# virtual methods
.method public a()Lkd;
    .locals 3

    .prologue
    .line 82
    invoke-virtual {p0}, Lko;->m()V

    .line 83
    new-instance v0, Lkv;

    iget-object v1, p0, Lko;->a:Lkj;

    iget-object v2, p0, Lko;->a:Lkj;

    invoke-direct {v0, v1, v2}, Lkv;-><init>(Lkj;Lke;)V

    return-object v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 125
    invoke-virtual {p0}, Lko;->m()V

    .line 126
    iget-object v0, p0, Lko;->a:Lkj;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Lkj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 127
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 128
    iget-object v1, p0, Lko;->a:Lkj;

    invoke-virtual {v1}, Lkj;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 129
    iget-object v0, p0, Lko;->a:Lkj;

    invoke-virtual {v0}, Lkj;->x_()V

    .line 130
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 116
    invoke-virtual {p0}, Lko;->m()V

    .line 117
    iget-object v0, p0, Lko;->a:Lkj;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Lkj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 118
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 119
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 120
    iget-object v0, p0, Lko;->a:Lkj;

    invoke-virtual {v0}, Lkj;->x_()V

    .line 121
    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 134
    invoke-virtual {p0}, Lko;->m()V

    .line 135
    iget-object v0, p0, Lko;->a:Lkj;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Lkj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 136
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 137
    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 138
    iget-object v0, p0, Lko;->a:Lkj;

    invoke-virtual {v0}, Lkj;->x_()V

    .line 139
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    if-eqz v0, :cond_0

    .line 314
    iget-object v0, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->b(Ljava/lang/CharSequence;)V

    .line 318
    :goto_0
    return-void

    .line 316
    :cond_0
    iput-object p1, p0, Lko;->j:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public a(Lma;)V
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->b()Z

    .line 363
    :cond_0
    :goto_0
    return-void

    .line 362
    :cond_1
    iget-object v0, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->d()Z

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lma;->close()V

    goto :goto_0
.end method

.method public a(Lma;Z)V
    .locals 1

    .prologue
    .line 367
    iget-boolean v0, p0, Lko;->m:Z

    if-eqz v0, :cond_0

    .line 374
    :goto_0
    return-void

    .line 370
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lko;->m:Z

    .line 371
    iget-object v0, p0, Lko;->a:Lkj;

    invoke-virtual {v0}, Lkj;->closeOptionsMenu()V

    .line 372
    iget-object v0, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->g()V

    .line 373
    const/4 v0, 0x0

    iput-boolean v0, p0, Lko;->m:Z

    goto :goto_0
.end method

.method public a(ILandroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 333
    if-eqz p1, :cond_0

    .line 334
    iget-object v0, p0, Lko;->a:Lkj;

    invoke-virtual {v0, p1, p2}, Lkj;->a(ILandroid/view/Menu;)Z

    move-result v0

    .line 336
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ILandroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 349
    if-nez p1, :cond_0

    .line 350
    invoke-static {p2}, Lf;->a(Landroid/view/MenuItem;)Landroid/view/MenuItem;

    move-result-object p2

    .line 352
    :cond_0
    iget-object v0, p0, Lko;->a:Lkj;

    invoke-virtual {v0, p1, p2}, Lkj;->a(ILandroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public a(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 341
    if-eqz p1, :cond_0

    .line 342
    iget-object v0, p0, Lko;->a:Lkj;

    invoke-virtual {v0, p1, p2, p3}, Lkj;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    .line 344
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a_(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 357
    iget-object v0, p0, Lko;->a:Lkj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lkj;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public b(I)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 324
    if-nez p1, :cond_0

    invoke-direct {p0}, Lko;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 325
    iget-object v1, p0, Lko;->a:Lkj;

    iget-object v2, p0, Lko;->g:Lma;

    if-nez v2, :cond_1

    :goto_0
    check-cast v0, Landroid/view/View;

    .line 328
    :cond_0
    return-object v0

    .line 325
    :cond_1
    iget-object v0, p0, Lko;->f:Lly;

    if-nez v0, :cond_2

    sget-object v0, Lle;->L:[I

    invoke-virtual {v1, v0}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v2, 0x4

    sget v3, Lf;->av:I

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v0, Lly;

    sget v3, Lf;->al:I

    invoke-direct {v0, v3, v2}, Lly;-><init>(II)V

    iput-object v0, p0, Lko;->f:Lly;

    iget-object v0, p0, Lko;->f:Lly;

    invoke-virtual {v0, p0}, Lly;->a(Lmq;)V

    iget-object v0, p0, Lko;->g:Lma;

    iget-object v2, p0, Lko;->f:Lly;

    invoke-virtual {v0, v2}, Lma;->a(Lmp;)V

    :goto_1
    iget-object v0, p0, Lko;->f:Lly;

    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Lly;->a(Landroid/view/ViewGroup;)Lmr;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lko;->f:Lly;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lly;->c(Z)V

    goto :goto_1
.end method

.method public b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 143
    invoke-virtual {p0}, Lko;->m()V

    .line 144
    iget-object v0, p0, Lko;->a:Lkj;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Lkj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 145
    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 146
    iget-object v0, p0, Lko;->a:Lkj;

    invoke-virtual {v0}, Lkj;->x_()V

    .line 147
    return-void
.end method

.method public b(Lma;)Z
    .locals 1

    .prologue
    .line 378
    const/4 v0, 0x0

    return v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lko;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lko;->i:Z

    if-eqz v0, :cond_0

    .line 93
    invoke-virtual {p0}, Lko;->b()Lkd;

    move-result-object v0

    check-cast v0, Lkv;

    .line 94
    invoke-virtual {v0}, Lkv;->i()V

    .line 96
    :cond_0
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 100
    invoke-virtual {p0}, Lko;->b()Lkd;

    move-result-object v0

    check-cast v0, Lkv;

    .line 101
    if-eqz v0, :cond_0

    .line 102
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lkv;->d(Z)V

    .line 104
    :cond_0
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 108
    invoke-virtual {p0}, Lko;->b()Lkd;

    move-result-object v0

    check-cast v0, Lkv;

    .line 109
    if-eqz v0, :cond_0

    .line 110
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lkv;->d(Z)V

    .line 112
    :cond_0
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 406
    iget-object v0, p0, Lko;->g:Lma;

    if-eqz v0, :cond_1

    .line 407
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 408
    iget-object v1, p0, Lko;->g:Lma;

    invoke-virtual {v1, v0}, Lma;->a(Landroid/os/Bundle;)V

    .line 409
    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 410
    iput-object v0, p0, Lko;->p:Landroid/os/Bundle;

    .line 413
    :cond_0
    iget-object v0, p0, Lko;->g:Lma;

    invoke-virtual {v0}, Lma;->g()V

    .line 414
    iget-object v0, p0, Lko;->g:Lma;

    invoke-virtual {v0}, Lma;->clear()V

    .line 416
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lko;->o:Z

    .line 419
    iget-object v0, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    if-eqz v0, :cond_2

    .line 420
    const/4 v0, 0x0

    iput-boolean v0, p0, Lko;->n:Z

    .line 421
    invoke-direct {p0}, Lko;->n()Z

    .line 423
    :cond_2
    return-void
.end method

.method public h()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 467
    iget-object v1, p0, Lko;->h:Loz;

    if-eqz v1, :cond_0

    .line 468
    iget-object v1, p0, Lko;->h:Loz;

    invoke-virtual {v1}, Loz;->a()V

    .line 478
    :goto_0
    return v0

    .line 473
    :cond_0
    iget-object v1, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarView;->k()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 474
    iget-object v1, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarView;->l()V

    goto :goto_0

    .line 478
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method i()I
    .locals 1

    .prologue
    .line 506
    sget v0, Lf;->n:I

    return v0
.end method

.method public j()V
    .locals 0

    .prologue
    .line 152
    return-void
.end method

.method final m()V
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/4 v11, 0x6

    const/4 v10, 0x5

    const/4 v3, 0x0

    const/4 v4, -0x1

    .line 155
    iget-boolean v0, p0, Lko;->i:Z

    if-nez v0, :cond_8

    .line 156
    iget-boolean v0, p0, Lko;->b:Z

    if-eqz v0, :cond_b

    .line 157
    iget-boolean v0, p0, Lko;->c:Z

    if-eqz v0, :cond_9

    .line 158
    iget-object v0, p0, Lko;->a:Lkj;

    sget v1, Lf;->Y:I

    invoke-virtual {v0, v1}, Lkj;->a_(I)V

    .line 162
    :goto_0
    iget-object v0, p0, Lko;->a:Lkj;

    sget v1, Lf;->z:I

    invoke-virtual {v0, v1}, Lkj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarView;

    iput-object v0, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    .line 163
    iget-object v0, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    iget-object v1, p0, Lko;->a:Lkj;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/view/Window$Callback;)V

    .line 168
    iget-boolean v0, p0, Lko;->k:Z

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->h()V

    .line 171
    :cond_0
    iget-boolean v0, p0, Lko;->l:Z

    if-eqz v0, :cond_1

    .line 172
    iget-object v0, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->i()V

    .line 178
    :cond_1
    const-string v0, "splitActionBarWhenNarrow"

    invoke-virtual {p0}, Lko;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 182
    if-eqz v6, :cond_a

    .line 183
    iget-object v0, p0, Lko;->a:Lkj;

    invoke-virtual {v0}, Lkj;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->u:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    move v2, v0

    .line 192
    :goto_1
    iget-object v0, p0, Lko;->a:Lkj;

    sget v1, Lf;->S:I

    invoke-virtual {v0, v1}, Lkj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContainer;

    .line 194
    if-eqz v0, :cond_2

    .line 195
    iget-object v1, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/support/v7/internal/widget/ActionBarContainer;)V

    .line 196
    iget-object v1, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ActionBarView;->a(Z)V

    .line 197
    iget-object v1, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v1, v6}, Landroid/support/v7/internal/widget/ActionBarView;->b(Z)V

    .line 199
    iget-object v1, p0, Lko;->a:Lkj;

    sget v7, Lf;->G:I

    invoke-virtual {v1, v7}, Lkj;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/widget/ActionBarContextView;

    .line 201
    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(Landroid/support/v7/internal/widget/ActionBarContainer;)V

    .line 202
    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(Z)V

    .line 203
    invoke-virtual {v1, v6}, Landroid/support/v7/internal/widget/ActionBarContextView;->b(Z)V

    .line 211
    :cond_2
    :goto_2
    iget-object v0, p0, Lko;->a:Lkj;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Lkj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 212
    invoke-virtual {v0, v4}, Landroid/view/View;->setId(I)V

    .line 213
    iget-object v0, p0, Lko;->a:Lkj;

    sget v1, Lf;->A:I

    invoke-virtual {v0, v1}, Lkj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 214
    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 217
    iget-object v0, p0, Lko;->j:Ljava/lang/CharSequence;

    if-eqz v0, :cond_3

    .line 218
    iget-object v0, p0, Lko;->e:Landroid/support/v7/internal/widget/ActionBarView;

    iget-object v1, p0, Lko;->j:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarView;->b(Ljava/lang/CharSequence;)V

    .line 219
    iput-object v3, p0, Lko;->j:Ljava/lang/CharSequence;

    .line 222
    :cond_3
    iget-object v0, p0, Lko;->a:Lkj;

    sget-object v1, Lle;->c:[I

    invoke-virtual {v0, v1}, Lkj;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v6

    const/4 v0, 0x3

    invoke-virtual {v6, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_14

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const/4 v1, 0x3

    invoke-virtual {v6, v1, v0}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :goto_3
    invoke-virtual {v6, v10}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_13

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {v6, v10, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :goto_4
    invoke-virtual {v6, v11}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_12

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {v6, v11, v2}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :goto_5
    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v7

    if-eqz v7, :cond_4

    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    const/4 v7, 0x4

    invoke-virtual {v6, v7, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_4
    iget-object v7, p0, Lko;->a:Lkj;

    invoke-virtual {v7}, Lkj;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v8, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v9, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v8, v9, :cond_5

    const/4 v5, 0x1

    :cond_5
    if-eqz v5, :cond_c

    :goto_6
    if-eqz v1, :cond_11

    iget v0, v1, Landroid/util/TypedValue;->type:I

    if-eqz v0, :cond_11

    iget v0, v1, Landroid/util/TypedValue;->type:I

    if-ne v0, v10, :cond_d

    invoke-virtual {v1, v7}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    move v1, v0

    :goto_7
    if-eqz v5, :cond_e

    :goto_8
    if-eqz v2, :cond_10

    iget v0, v2, Landroid/util/TypedValue;->type:I

    if-eqz v0, :cond_10

    iget v0, v2, Landroid/util/TypedValue;->type:I

    if-ne v0, v10, :cond_f

    invoke-virtual {v2, v7}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    :goto_9
    if-ne v1, v4, :cond_6

    if-eq v0, v4, :cond_7

    :cond_6
    iget-object v2, p0, Lko;->a:Lkj;

    invoke-virtual {v2}, Lkj;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Landroid/view/Window;->setLayout(II)V

    :cond_7
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    .line 224
    const/4 v0, 0x1

    iput-boolean v0, p0, Lko;->i:Z

    .line 227
    iget-object v0, p0, Lko;->a:Lkj;

    invoke-virtual {v0}, Lkj;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lkp;

    invoke-direct {v1, p0}, Lkp;-><init>(Lko;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 234
    :cond_8
    return-void

    .line 160
    :cond_9
    iget-object v0, p0, Lko;->a:Lkj;

    sget v1, Lf;->X:I

    invoke-virtual {v0, v1}, Lkj;->a_(I)V

    goto/16 :goto_0

    .line 186
    :cond_a
    iget-object v0, p0, Lko;->a:Lkj;

    sget-object v1, Lle;->c:[I

    invoke-virtual {v0, v1}, Lkj;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 187
    const/4 v0, 0x2

    invoke-virtual {v1, v0, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 189
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    move v2, v0

    goto/16 :goto_1

    .line 206
    :cond_b
    iget-object v0, p0, Lko;->a:Lkj;

    sget v1, Lf;->ao:I

    invoke-virtual {v0, v1}, Lkj;->a_(I)V

    goto/16 :goto_2

    :cond_c
    move-object v1, v0

    .line 222
    goto :goto_6

    :cond_d
    iget v0, v1, Landroid/util/TypedValue;->type:I

    if-ne v0, v11, :cond_11

    iget v0, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    iget v8, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v8, v8

    invoke-virtual {v1, v0, v8}, Landroid/util/TypedValue;->getFraction(FF)F

    move-result v0

    float-to-int v0, v0

    move v1, v0

    goto :goto_7

    :cond_e
    move-object v2, v3

    goto :goto_8

    :cond_f
    iget v0, v2, Landroid/util/TypedValue;->type:I

    if-ne v0, v11, :cond_10

    iget v0, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v0, v0

    iget v3, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v3, v3

    invoke-virtual {v2, v0, v3}, Landroid/util/TypedValue;->getFraction(FF)F

    move-result v0

    float-to-int v0, v0

    goto :goto_9

    :cond_10
    move v0, v4

    goto :goto_9

    :cond_11
    move v1, v4

    goto/16 :goto_7

    :cond_12
    move-object v2, v3

    goto/16 :goto_5

    :cond_13
    move-object v1, v3

    goto/16 :goto_4

    :cond_14
    move-object v0, v3

    goto/16 :goto_3
.end method
