.class public final Lens;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lens;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Lemz;

.field public d:Lenp;

.field public e:Lenr;

.field public f:Lenk;

.field public g:Lems;

.field public h:Lemj;

.field public i:Lenq;

.field public j:[Lenm;

.field public k:Lema;

.field public l:Lenl;

.field public m:Lenn;

.field public n:Lent;

.field public o:Lemo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lens;

    sput-object v0, Lens;->a:[Lens;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 15
    iput-object v1, p0, Lens;->c:Lemz;

    .line 18
    iput-object v1, p0, Lens;->d:Lenp;

    .line 21
    iput-object v1, p0, Lens;->e:Lenr;

    .line 24
    iput-object v1, p0, Lens;->f:Lenk;

    .line 27
    iput-object v1, p0, Lens;->g:Lems;

    .line 30
    iput-object v1, p0, Lens;->h:Lemj;

    .line 33
    iput-object v1, p0, Lens;->i:Lenq;

    .line 36
    sget-object v0, Lenm;->a:[Lenm;

    iput-object v0, p0, Lens;->j:[Lenm;

    .line 39
    iput-object v1, p0, Lens;->k:Lema;

    .line 42
    iput-object v1, p0, Lens;->l:Lenl;

    .line 45
    iput-object v1, p0, Lens;->m:Lenn;

    .line 48
    iput-object v1, p0, Lens;->n:Lent;

    .line 51
    iput-object v1, p0, Lens;->o:Lemo;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 109
    iget-object v0, p0, Lens;->b:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 110
    const/4 v0, 0x1

    iget-object v2, p0, Lens;->b:Ljava/lang/String;

    .line 111
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 113
    :goto_0
    iget-object v2, p0, Lens;->d:Lenp;

    if-eqz v2, :cond_0

    .line 114
    const/4 v2, 0x2

    iget-object v3, p0, Lens;->d:Lenp;

    .line 115
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 117
    :cond_0
    iget-object v2, p0, Lens;->g:Lems;

    if-eqz v2, :cond_1

    .line 118
    const/4 v2, 0x3

    iget-object v3, p0, Lens;->g:Lems;

    .line 119
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 121
    :cond_1
    iget-object v2, p0, Lens;->h:Lemj;

    if-eqz v2, :cond_2

    .line 122
    const/4 v2, 0x4

    iget-object v3, p0, Lens;->h:Lemj;

    .line 123
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 125
    :cond_2
    iget-object v2, p0, Lens;->e:Lenr;

    if-eqz v2, :cond_3

    .line 126
    const/4 v2, 0x5

    iget-object v3, p0, Lens;->e:Lenr;

    .line 127
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 129
    :cond_3
    iget-object v2, p0, Lens;->c:Lemz;

    if-eqz v2, :cond_4

    .line 130
    const/4 v2, 0x6

    iget-object v3, p0, Lens;->c:Lemz;

    .line 131
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 133
    :cond_4
    iget-object v2, p0, Lens;->i:Lenq;

    if-eqz v2, :cond_5

    .line 134
    const/4 v2, 0x7

    iget-object v3, p0, Lens;->i:Lenq;

    .line 135
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 137
    :cond_5
    iget-object v2, p0, Lens;->f:Lenk;

    if-eqz v2, :cond_6

    .line 138
    const/16 v2, 0x8

    iget-object v3, p0, Lens;->f:Lenk;

    .line 139
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 141
    :cond_6
    iget-object v2, p0, Lens;->j:[Lenm;

    if-eqz v2, :cond_8

    .line 142
    iget-object v2, p0, Lens;->j:[Lenm;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 143
    if-eqz v4, :cond_7

    .line 144
    const/16 v5, 0x9

    .line 145
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 142
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 149
    :cond_8
    iget-object v1, p0, Lens;->k:Lema;

    if-eqz v1, :cond_9

    .line 150
    const/16 v1, 0xa

    iget-object v2, p0, Lens;->k:Lema;

    .line 151
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 153
    :cond_9
    iget-object v1, p0, Lens;->l:Lenl;

    if-eqz v1, :cond_a

    .line 154
    const/16 v1, 0xb

    iget-object v2, p0, Lens;->l:Lenl;

    .line 155
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 157
    :cond_a
    iget-object v1, p0, Lens;->m:Lenn;

    if-eqz v1, :cond_b

    .line 158
    const/16 v1, 0xc

    iget-object v2, p0, Lens;->m:Lenn;

    .line 159
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 161
    :cond_b
    iget-object v1, p0, Lens;->n:Lent;

    if-eqz v1, :cond_c

    .line 162
    const/16 v1, 0xd

    iget-object v2, p0, Lens;->n:Lent;

    .line 163
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 165
    :cond_c
    iget-object v1, p0, Lens;->o:Lemo;

    if-eqz v1, :cond_d

    .line 166
    const/16 v1, 0xe

    iget-object v2, p0, Lens;->o:Lemo;

    .line 167
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 169
    :cond_d
    iget-object v1, p0, Lens;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    iput v0, p0, Lens;->cachedSize:I

    .line 171
    return v0

    :cond_e
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lens;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lens;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lens;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lens;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lens;->d:Lenp;

    if-nez v0, :cond_2

    new-instance v0, Lenp;

    invoke-direct {v0}, Lenp;-><init>()V

    iput-object v0, p0, Lens;->d:Lenp;

    :cond_2
    iget-object v0, p0, Lens;->d:Lenp;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lens;->g:Lems;

    if-nez v0, :cond_3

    new-instance v0, Lems;

    invoke-direct {v0}, Lems;-><init>()V

    iput-object v0, p0, Lens;->g:Lems;

    :cond_3
    iget-object v0, p0, Lens;->g:Lems;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lens;->h:Lemj;

    if-nez v0, :cond_4

    new-instance v0, Lemj;

    invoke-direct {v0}, Lemj;-><init>()V

    iput-object v0, p0, Lens;->h:Lemj;

    :cond_4
    iget-object v0, p0, Lens;->h:Lemj;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lens;->e:Lenr;

    if-nez v0, :cond_5

    new-instance v0, Lenr;

    invoke-direct {v0}, Lenr;-><init>()V

    iput-object v0, p0, Lens;->e:Lenr;

    :cond_5
    iget-object v0, p0, Lens;->e:Lenr;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lens;->c:Lemz;

    if-nez v0, :cond_6

    new-instance v0, Lemz;

    invoke-direct {v0}, Lemz;-><init>()V

    iput-object v0, p0, Lens;->c:Lemz;

    :cond_6
    iget-object v0, p0, Lens;->c:Lemz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lens;->i:Lenq;

    if-nez v0, :cond_7

    new-instance v0, Lenq;

    invoke-direct {v0}, Lenq;-><init>()V

    iput-object v0, p0, Lens;->i:Lenq;

    :cond_7
    iget-object v0, p0, Lens;->i:Lenq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lens;->f:Lenk;

    if-nez v0, :cond_8

    new-instance v0, Lenk;

    invoke-direct {v0}, Lenk;-><init>()V

    iput-object v0, p0, Lens;->f:Lenk;

    :cond_8
    iget-object v0, p0, Lens;->f:Lenk;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lens;->j:[Lenm;

    if-nez v0, :cond_a

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lenm;

    iget-object v3, p0, Lens;->j:[Lenm;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lens;->j:[Lenm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_9
    iput-object v2, p0, Lens;->j:[Lenm;

    :goto_2
    iget-object v2, p0, Lens;->j:[Lenm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_b

    iget-object v2, p0, Lens;->j:[Lenm;

    new-instance v3, Lenm;

    invoke-direct {v3}, Lenm;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lens;->j:[Lenm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_a
    iget-object v0, p0, Lens;->j:[Lenm;

    array-length v0, v0

    goto :goto_1

    :cond_b
    iget-object v2, p0, Lens;->j:[Lenm;

    new-instance v3, Lenm;

    invoke-direct {v3}, Lenm;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lens;->j:[Lenm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lens;->k:Lema;

    if-nez v0, :cond_c

    new-instance v0, Lema;

    invoke-direct {v0}, Lema;-><init>()V

    iput-object v0, p0, Lens;->k:Lema;

    :cond_c
    iget-object v0, p0, Lens;->k:Lema;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lens;->l:Lenl;

    if-nez v0, :cond_d

    new-instance v0, Lenl;

    invoke-direct {v0}, Lenl;-><init>()V

    iput-object v0, p0, Lens;->l:Lenl;

    :cond_d
    iget-object v0, p0, Lens;->l:Lenl;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lens;->m:Lenn;

    if-nez v0, :cond_e

    new-instance v0, Lenn;

    invoke-direct {v0}, Lenn;-><init>()V

    iput-object v0, p0, Lens;->m:Lenn;

    :cond_e
    iget-object v0, p0, Lens;->m:Lenn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lens;->n:Lent;

    if-nez v0, :cond_f

    new-instance v0, Lent;

    invoke-direct {v0}, Lent;-><init>()V

    iput-object v0, p0, Lens;->n:Lent;

    :cond_f
    iget-object v0, p0, Lens;->n:Lent;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Lens;->o:Lemo;

    if-nez v0, :cond_10

    new-instance v0, Lemo;

    invoke-direct {v0}, Lemo;-><init>()V

    iput-object v0, p0, Lens;->o:Lemo;

    :cond_10
    iget-object v0, p0, Lens;->o:Lemo;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 56
    iget-object v0, p0, Lens;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 57
    const/4 v0, 0x1

    iget-object v1, p0, Lens;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 59
    :cond_0
    iget-object v0, p0, Lens;->d:Lenp;

    if-eqz v0, :cond_1

    .line 60
    const/4 v0, 0x2

    iget-object v1, p0, Lens;->d:Lenp;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 62
    :cond_1
    iget-object v0, p0, Lens;->g:Lems;

    if-eqz v0, :cond_2

    .line 63
    const/4 v0, 0x3

    iget-object v1, p0, Lens;->g:Lems;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 65
    :cond_2
    iget-object v0, p0, Lens;->h:Lemj;

    if-eqz v0, :cond_3

    .line 66
    const/4 v0, 0x4

    iget-object v1, p0, Lens;->h:Lemj;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 68
    :cond_3
    iget-object v0, p0, Lens;->e:Lenr;

    if-eqz v0, :cond_4

    .line 69
    const/4 v0, 0x5

    iget-object v1, p0, Lens;->e:Lenr;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 71
    :cond_4
    iget-object v0, p0, Lens;->c:Lemz;

    if-eqz v0, :cond_5

    .line 72
    const/4 v0, 0x6

    iget-object v1, p0, Lens;->c:Lemz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 74
    :cond_5
    iget-object v0, p0, Lens;->i:Lenq;

    if-eqz v0, :cond_6

    .line 75
    const/4 v0, 0x7

    iget-object v1, p0, Lens;->i:Lenq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 77
    :cond_6
    iget-object v0, p0, Lens;->f:Lenk;

    if-eqz v0, :cond_7

    .line 78
    const/16 v0, 0x8

    iget-object v1, p0, Lens;->f:Lenk;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 80
    :cond_7
    iget-object v0, p0, Lens;->j:[Lenm;

    if-eqz v0, :cond_9

    .line 81
    iget-object v1, p0, Lens;->j:[Lenm;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_9

    aget-object v3, v1, v0

    .line 82
    if-eqz v3, :cond_8

    .line 83
    const/16 v4, 0x9

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 81
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 87
    :cond_9
    iget-object v0, p0, Lens;->k:Lema;

    if-eqz v0, :cond_a

    .line 88
    const/16 v0, 0xa

    iget-object v1, p0, Lens;->k:Lema;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 90
    :cond_a
    iget-object v0, p0, Lens;->l:Lenl;

    if-eqz v0, :cond_b

    .line 91
    const/16 v0, 0xb

    iget-object v1, p0, Lens;->l:Lenl;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 93
    :cond_b
    iget-object v0, p0, Lens;->m:Lenn;

    if-eqz v0, :cond_c

    .line 94
    const/16 v0, 0xc

    iget-object v1, p0, Lens;->m:Lenn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 96
    :cond_c
    iget-object v0, p0, Lens;->n:Lent;

    if-eqz v0, :cond_d

    .line 97
    const/16 v0, 0xd

    iget-object v1, p0, Lens;->n:Lent;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 99
    :cond_d
    iget-object v0, p0, Lens;->o:Lemo;

    if-eqz v0, :cond_e

    .line 100
    const/16 v0, 0xe

    iget-object v1, p0, Lens;->o:Lemo;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 102
    :cond_e
    iget-object v0, p0, Lens;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 104
    return-void
.end method
