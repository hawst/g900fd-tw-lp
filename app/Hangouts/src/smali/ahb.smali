.class public final Lahb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lccw;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

.field private b:Z

.field private c:I


# direct methods
.method private constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6701
    iput-object p1, p0, Lahb;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6702
    iput-boolean v0, p0, Lahb;->b:Z

    .line 6703
    iput v0, p0, Lahb;->c:I

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;B)V
    .locals 0

    .prologue
    .line 6701
    invoke-direct {p0, p1}, Lahb;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 6707
    const/4 v0, 0x1

    iput-boolean v0, p0, Lahb;->b:Z

    .line 6708
    iget v0, p0, Lahb;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lahb;->c:I

    .line 6710
    iget-object v0, p0, Lahb;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v0

    if-nez v0, :cond_1

    .line 6724
    :cond_0
    :goto_0
    return-void

    .line 6717
    :cond_1
    iget-object v0, p0, Lahb;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getLoaderManager()Lav;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lav;->b(I)Ldg;

    move-result-object v0

    .line 6718
    if-eqz v0, :cond_0

    .line 6719
    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aa()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 6720
    const-string v1, "Babel"

    const-string v2, "[ConversationFragment] startBlocking: stop loading"

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6722
    :cond_2
    invoke-virtual {v0}, Ldg;->r()V

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 6728
    iget v0, p0, Lahb;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lahb;->c:I

    .line 6729
    iget v0, p0, Lahb;->c:I

    if-lez v0, :cond_1

    .line 6747
    :cond_0
    :goto_0
    return-void

    .line 6733
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lahb;->b:Z

    .line 6735
    iget-object v0, p0, Lahb;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6740
    iget-object v0, p0, Lahb;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getLoaderManager()Lav;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lav;->b(I)Ldg;

    move-result-object v0

    .line 6741
    if-eqz v0, :cond_0

    .line 6742
    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aa()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 6743
    const-string v1, "Babel"

    const-string v2, "[ConversationFragment] endBlocking: restart loading"

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6745
    :cond_2
    invoke-virtual {v0}, Ldg;->p()V

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 6751
    iget-boolean v0, p0, Lahb;->b:Z

    return v0
.end method
