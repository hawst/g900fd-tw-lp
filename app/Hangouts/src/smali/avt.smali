.class public final Lavt;
.super Landroid/preference/Preference;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Landroid/widget/CompoundButton;


# instance fields
.field private c:Z

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 41
    sput-object v0, Lavt;->a:Ljava/lang/String;

    .line 42
    sput-object v0, Lavt;->b:Landroid/widget/CompoundButton;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    sget v0, Lf;->bB:I

    invoke-direct {p0, p1, v0}, Lavt;-><init>(Landroid/content/Context;I)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;B)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lavt;-><init>(Landroid/content/Context;)V

    .line 39
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lavt;->c:Z

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lavt;->d:Z

    .line 31
    return-void
.end method

.method private static a(Landroid/widget/CompoundButton;)V
    .locals 2

    .prologue
    .line 80
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 81
    const v1, 0x1020016

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 82
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 83
    invoke-virtual {p0, v0}, Landroid/widget/CompoundButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 84
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 91
    invoke-virtual {p0}, Lavt;->getKey()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lavt;->a:Ljava/lang/String;

    .line 92
    return-void
.end method

.method public getView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 48
    invoke-super {p0, p1, p2}, Landroid/preference/Preference;->getView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 50
    sget v0, Lg;->o:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 51
    if-eqz v0, :cond_1

    instance-of v2, v0, Landroid/widget/RadioButton;

    if-eqz v2, :cond_1

    .line 52
    check-cast v0, Landroid/widget/RadioButton;

    .line 53
    iget-boolean v2, p0, Lavt;->d:Z

    if-eqz v2, :cond_3

    .line 54
    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 56
    invoke-virtual {p0}, Lavt;->getKey()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lavt;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 57
    if-eqz v2, :cond_0

    .line 58
    sput-object v0, Lavt;->b:Landroid/widget/CompoundButton;

    .line 59
    invoke-virtual {p0}, Lavt;->getKey()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lavt;->a:Ljava/lang/String;

    .line 62
    :cond_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lavt;->c:Z

    .line 63
    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 64
    const/4 v2, 0x0

    iput-boolean v2, p0, Lavt;->c:Z

    .line 68
    :goto_0
    invoke-static {v0}, Lavt;->a(Landroid/widget/CompoundButton;)V

    .line 71
    :cond_1
    sget v0, Lg;->hs:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 72
    if-eqz v0, :cond_2

    instance-of v2, v0, Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_2

    .line 73
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    :cond_2
    return-object v1

    .line 66
    :cond_3
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ID: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lavt;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 96
    iget-boolean v0, p0, Lavt;->c:Z

    if-eqz v0, :cond_0

    .line 112
    :goto_0
    return-void

    .line 100
    :cond_0
    if-eqz p2, :cond_2

    .line 101
    sget-object v0, Lavt;->b:Landroid/widget/CompoundButton;

    if-eqz v0, :cond_1

    .line 102
    sget-object v0, Lavt;->b:Landroid/widget/CompoundButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 104
    :cond_1
    sput-object p1, Lavt;->b:Landroid/widget/CompoundButton;

    .line 105
    invoke-virtual {p0}, Lavt;->getKey()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lavt;->a:Ljava/lang/String;

    .line 106
    sget-object v0, Lavt;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lavt;->callChangeListener(Ljava/lang/Object;)Z

    .line 111
    :goto_1
    invoke-static {p1}, Lavt;->a(Landroid/widget/CompoundButton;)V

    goto :goto_0

    .line 108
    :cond_2
    sput-object v2, Lavt;->b:Landroid/widget/CompoundButton;

    .line 109
    sput-object v2, Lavt;->a:Ljava/lang/String;

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 115
    if-eqz p1, :cond_0

    sget v0, Lg;->hs:I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 116
    invoke-virtual {p0}, Lavt;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 117
    if-eqz v0, :cond_0

    .line 118
    invoke-virtual {p0}, Lavt;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbbl;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 121
    :cond_0
    return-void
.end method

.method public setSelectable(Z)V
    .locals 0

    .prologue
    .line 124
    iput-boolean p1, p0, Lavt;->d:Z

    .line 125
    return-void
.end method
