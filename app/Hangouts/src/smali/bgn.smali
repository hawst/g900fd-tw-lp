.class public Lbgn;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:[B

.field private final h:[B


# direct methods
.method private constructor <init>(Ldyk;)V
    .locals 3

    .prologue
    .line 4066
    iget-object v0, p1, Ldyk;->b:Ldvn;

    const-wide/16 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 4067
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 4068
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetBroadcastInfoResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4070
    :cond_0
    iget-object v0, p1, Ldyk;->c:Ldxv;

    invoke-static {v0}, Ldxv;->toByteArray(Lepr;)[B

    move-result-object v0

    iput-object v0, p0, Lbgn;->g:[B

    .line 4071
    iget-object v0, p1, Ldyk;->d:Ldzg;

    invoke-static {v0}, Ldzg;->toByteArray(Lepr;)[B

    move-result-object v0

    iput-object v0, p0, Lbgn;->h:[B

    .line 4072
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 4076
    new-instance v0, Ldyk;

    invoke-direct {v0}, Ldyk;-><init>()V

    .line 4077
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldyk;

    .line 4078
    iget-object v1, v0, Ldyk;->b:Ldvn;

    invoke-static {v1}, Lbgn;->a(Ldvn;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Ldyk;->c:Ldxv;

    if-nez v1, :cond_1

    .line 4079
    :cond_0
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldyk;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 4081
    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Lbgn;

    invoke-direct {v1, v0}, Lbgn;-><init>(Ldyk;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4085
    invoke-virtual {p0}, Lbgn;->g()Ldxv;

    move-result-object v0

    .line 4086
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Ldxv;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public g()Ldxv;
    .locals 3

    .prologue
    .line 4091
    :try_start_0
    new-instance v0, Ldxv;

    invoke-direct {v0}, Ldxv;-><init>()V

    iget-object v1, p0, Lbgn;->g:[B

    invoke-static {v0, v1}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldxv;
    :try_end_0
    .catch Lepq; {:try_start_0 .. :try_end_0} :catch_0

    .line 4094
    :goto_0
    return-object v0

    .line 4092
    :catch_0
    move-exception v0

    .line 4093
    const-string v1, "Babel"

    const-string v2, "Broadcast parse failed"

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 4094
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Ldzg;
    .locals 3

    .prologue
    .line 4100
    :try_start_0
    new-instance v0, Ldzg;

    invoke-direct {v0}, Ldzg;-><init>()V

    iget-object v1, p0, Lbgn;->h:[B

    invoke-static {v0, v1}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldzg;
    :try_end_0
    .catch Lepq; {:try_start_0 .. :try_end_0} :catch_0

    .line 4103
    :goto_0
    return-object v0

    .line 4101
    :catch_0
    move-exception v0

    .line 4102
    const-string v1, "Babel"

    const-string v2, "SyncMetadata parse failed"

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 4103
    const/4 v0, 0x0

    goto :goto_0
.end method
