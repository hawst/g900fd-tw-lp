.class public final Lbqt;
.super Lbqu;
.source "PG"


# instance fields
.field private final l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lbjd;)V
    .locals 16

    .prologue
    .line 333
    move-object/from16 v0, p1

    iget-object v2, v0, Lbjd;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v3, v0, Lbjd;->d:Lbdk;

    move-object/from16 v0, p1

    iget-wide v4, v0, Lbjd;->e:J

    move-object/from16 v0, p1

    iget-wide v6, v0, Lbjd;->n:J

    move-object/from16 v0, p1

    iget-object v8, v0, Lbjd;->m:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v9, v0, Lbjd;->l:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v10, v0, Lbjd;->g:I

    move-object/from16 v0, p1

    iget v11, v0, Lbjd;->h:I

    move-object/from16 v0, p1

    iget-wide v12, v0, Lbjd;->i:J

    const/4 v14, 0x4

    move-object/from16 v0, p1

    iget-object v15, v0, Lbjd;->t:[B

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v15}, Lbqu;-><init>(Ljava/lang/String;Lbdk;JJLjava/lang/String;Ljava/lang/String;IIJI[B)V

    .line 338
    move-object/from16 v0, p1

    iget-object v1, v0, Lbjd;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lbqt;->l:Ljava/lang/String;

    .line 339
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lbdk;JJLjava/lang/String;Ljava/lang/String;)V
    .locals 15

    .prologue
    .line 344
    const/4 v8, 0x0

    const/4 v9, -0x1

    const/4 v10, 0x1

    const-wide/16 v11, 0x0

    const/4 v13, 0x1

    const/4 v14, 0x0

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-wide/from16 v3, p3

    move-wide/from16 v5, p5

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v14}, Lbqu;-><init>(Ljava/lang/String;Lbdk;JJLjava/lang/String;Ljava/lang/String;IIJI[B)V

    .line 347
    invoke-static/range {p8 .. p8}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbqt;->l:Ljava/lang/String;

    .line 348
    return-void
.end method

.method private c(Lyt;Lbnl;)V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 381
    iget-object v0, p0, Lbqt;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 382
    iget-object v1, p0, Lbqt;->f:Ljava/lang/String;

    iget-object v2, p0, Lbqt;->a:Ljava/lang/String;

    iget v3, p0, Lbqt;->k:I

    iget-object v4, p0, Lbqt;->g:Lbdk;

    const/4 v5, 0x3

    iget-wide v6, p0, Lbqt;->h:J

    iget-wide v8, p0, Lbqt;->i:J

    iget v10, p0, Lbqt;->c:I

    iget-object v11, p0, Lbqt;->l:Ljava/lang/String;

    move-object v0, p1

    invoke-virtual/range {v0 .. v12}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;ILbdk;IJJILjava/lang/String;Ljava/lang/String;)J

    move-result-wide v8

    .line 387
    iget-object v1, p0, Lbqt;->f:Ljava/lang/String;

    iget-wide v2, p0, Lbqt;->h:J

    iget-wide v4, p0, Lbqt;->i:J

    const/4 v6, 0x5

    iget-object v7, p0, Lbqt;->g:Lbdk;

    iget v10, p0, Lbqt;->k:I

    iget-object v11, p0, Lbqt;->l:Ljava/lang/String;

    move-object v0, p1

    invoke-virtual/range {v0 .. v12}, Lyt;->a(Ljava/lang/String;JJILbdk;JILjava/lang/String;Ljava/lang/String;)Z

    .line 391
    iget-object v0, p0, Lbqt;->f:Ljava/lang/String;

    invoke-static {p1, v0}, Lyp;->d(Lyt;Ljava/lang/String;)V

    .line 393
    iget-wide v0, p0, Lbqt;->h:J

    invoke-virtual {p2, v0, v1}, Lbnl;->a(J)V

    .line 396
    :cond_0
    invoke-virtual {p0, p1}, Lbqt;->a(Lyt;)V

    .line 397
    return-void
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 2

    .prologue
    .line 357
    invoke-virtual {p1}, Lyt;->a()V

    .line 359
    :try_start_0
    invoke-direct {p0, p1, p2}, Lbqt;->c(Lyt;Lbnl;)V

    .line 360
    iget-object v0, p0, Lbqt;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Lbqt;->f:Ljava/lang/String;

    iget-object v1, p0, Lbqt;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lyt;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    :cond_0
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 365
    invoke-virtual {p1}, Lyt;->c()V

    .line 366
    return-void

    .line 365
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lyt;->c()V

    throw v0
.end method

.method public b(Lyt;Lbnl;)V
    .locals 0

    .prologue
    .line 376
    invoke-direct {p0, p1, p2}, Lbqt;->c(Lyt;Lbnl;)V

    .line 377
    return-void
.end method
