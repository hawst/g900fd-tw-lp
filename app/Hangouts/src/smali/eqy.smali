.class public final Leqy;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leqy;


# instance fields
.field public b:Leqq;

.field public c:Lerl;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Leqq;

.field public g:Lerl;

.field public h:Ljava/lang/Boolean;

.field public i:[Lerc;

.field public j:Ljava/lang/Boolean;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/Integer;

.field public m:Ldch;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 784
    const/4 v0, 0x0

    new-array v0, v0, [Leqy;

    sput-object v0, Leqy;->a:[Leqy;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 785
    invoke-direct {p0}, Lepn;-><init>()V

    .line 794
    iput-object v1, p0, Leqy;->b:Leqq;

    .line 797
    iput-object v1, p0, Leqy;->c:Lerl;

    .line 800
    iput-object v1, p0, Leqy;->d:Ljava/lang/Integer;

    .line 805
    iput-object v1, p0, Leqy;->f:Leqq;

    .line 808
    iput-object v1, p0, Leqy;->g:Lerl;

    .line 813
    sget-object v0, Lerc;->a:[Lerc;

    iput-object v0, p0, Leqy;->i:[Lerc;

    .line 820
    iput-object v1, p0, Leqy;->l:Ljava/lang/Integer;

    .line 823
    iput-object v1, p0, Leqy;->m:Ldch;

    .line 785
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 875
    iget-object v0, p0, Leqy;->b:Leqq;

    if-eqz v0, :cond_c

    .line 876
    const/4 v0, 0x1

    iget-object v2, p0, Leqy;->b:Leqq;

    .line 877
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 879
    :goto_0
    iget-object v2, p0, Leqy;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 880
    const/4 v2, 0x2

    iget-object v3, p0, Leqy;->d:Ljava/lang/Integer;

    .line 881
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 883
    :cond_0
    iget-object v2, p0, Leqy;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 884
    const/4 v2, 0x3

    iget-object v3, p0, Leqy;->e:Ljava/lang/Integer;

    .line 885
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 887
    :cond_1
    iget-object v2, p0, Leqy;->f:Leqq;

    if-eqz v2, :cond_2

    .line 888
    const/4 v2, 0x4

    iget-object v3, p0, Leqy;->f:Leqq;

    .line 889
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 891
    :cond_2
    iget-object v2, p0, Leqy;->h:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    .line 892
    const/4 v2, 0x5

    iget-object v3, p0, Leqy;->h:Ljava/lang/Boolean;

    .line 893
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 895
    :cond_3
    iget-object v2, p0, Leqy;->i:[Lerc;

    if-eqz v2, :cond_5

    .line 896
    iget-object v2, p0, Leqy;->i:[Lerc;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 897
    if-eqz v4, :cond_4

    .line 898
    const/4 v5, 0x6

    .line 899
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 896
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 903
    :cond_5
    iget-object v1, p0, Leqy;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 904
    const/4 v1, 0x7

    iget-object v2, p0, Leqy;->j:Ljava/lang/Boolean;

    .line 905
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 907
    :cond_6
    iget-object v1, p0, Leqy;->k:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 908
    const/16 v1, 0x8

    iget-object v2, p0, Leqy;->k:Ljava/lang/String;

    .line 909
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 911
    :cond_7
    iget-object v1, p0, Leqy;->c:Lerl;

    if-eqz v1, :cond_8

    .line 912
    const/16 v1, 0x9

    iget-object v2, p0, Leqy;->c:Lerl;

    .line 913
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 915
    :cond_8
    iget-object v1, p0, Leqy;->m:Ldch;

    if-eqz v1, :cond_9

    .line 916
    const/16 v1, 0xa

    iget-object v2, p0, Leqy;->m:Ldch;

    .line 917
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 919
    :cond_9
    iget-object v1, p0, Leqy;->g:Lerl;

    if-eqz v1, :cond_a

    .line 920
    const/16 v1, 0xb

    iget-object v2, p0, Leqy;->g:Lerl;

    .line 921
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 923
    :cond_a
    iget-object v1, p0, Leqy;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 924
    const/16 v1, 0xc

    iget-object v2, p0, Leqy;->l:Ljava/lang/Integer;

    .line 925
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 927
    :cond_b
    iget-object v1, p0, Leqy;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 928
    iput v0, p0, Leqy;->cachedSize:I

    .line 929
    return v0

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 781
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Leqy;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Leqy;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Leqy;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Leqy;->b:Leqq;

    if-nez v0, :cond_2

    new-instance v0, Leqq;

    invoke-direct {v0}, Leqq;-><init>()V

    iput-object v0, p0, Leqy;->b:Leqq;

    :cond_2
    iget-object v0, p0, Leqy;->b:Leqq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_3

    if-eq v0, v5, :cond_3

    if-eq v0, v6, :cond_3

    const/4 v2, 0x4

    if-eq v0, v2, :cond_3

    const/4 v2, 0x5

    if-eq v0, v2, :cond_3

    const/4 v2, 0x6

    if-eq v0, v2, :cond_3

    const/4 v2, 0x7

    if-ne v0, v2, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leqy;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leqy;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leqy;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Leqy;->f:Leqq;

    if-nez v0, :cond_5

    new-instance v0, Leqq;

    invoke-direct {v0}, Leqq;-><init>()V

    iput-object v0, p0, Leqy;->f:Leqq;

    :cond_5
    iget-object v0, p0, Leqy;->f:Leqq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Leqy;->h:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leqy;->i:[Lerc;

    if-nez v0, :cond_7

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lerc;

    iget-object v3, p0, Leqy;->i:[Lerc;

    if-eqz v3, :cond_6

    iget-object v3, p0, Leqy;->i:[Lerc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Leqy;->i:[Lerc;

    :goto_2
    iget-object v2, p0, Leqy;->i:[Lerc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Leqy;->i:[Lerc;

    new-instance v3, Lerc;

    invoke-direct {v3}, Lerc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leqy;->i:[Lerc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    iget-object v0, p0, Leqy;->i:[Lerc;

    array-length v0, v0

    goto :goto_1

    :cond_8
    iget-object v2, p0, Leqy;->i:[Lerc;

    new-instance v3, Lerc;

    invoke-direct {v3}, Lerc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leqy;->i:[Lerc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Leqy;->j:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leqy;->k:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Leqy;->c:Lerl;

    if-nez v0, :cond_9

    new-instance v0, Lerl;

    invoke-direct {v0}, Lerl;-><init>()V

    iput-object v0, p0, Leqy;->c:Lerl;

    :cond_9
    iget-object v0, p0, Leqy;->c:Lerl;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Leqy;->m:Ldch;

    if-nez v0, :cond_a

    new-instance v0, Ldch;

    invoke-direct {v0}, Ldch;-><init>()V

    iput-object v0, p0, Leqy;->m:Ldch;

    :cond_a
    iget-object v0, p0, Leqy;->m:Ldch;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Leqy;->g:Lerl;

    if-nez v0, :cond_b

    new-instance v0, Lerl;

    invoke-direct {v0}, Lerl;-><init>()V

    iput-object v0, p0, Leqy;->g:Lerl;

    :cond_b
    iget-object v0, p0, Leqy;->g:Lerl;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_c

    if-eq v0, v5, :cond_c

    if-ne v0, v6, :cond_d

    :cond_c
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leqy;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_d
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leqy;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 828
    iget-object v0, p0, Leqy;->b:Leqq;

    if-eqz v0, :cond_0

    .line 829
    const/4 v0, 0x1

    iget-object v1, p0, Leqy;->b:Leqq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 831
    :cond_0
    iget-object v0, p0, Leqy;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 832
    const/4 v0, 0x2

    iget-object v1, p0, Leqy;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 834
    :cond_1
    iget-object v0, p0, Leqy;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 835
    const/4 v0, 0x3

    iget-object v1, p0, Leqy;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 837
    :cond_2
    iget-object v0, p0, Leqy;->f:Leqq;

    if-eqz v0, :cond_3

    .line 838
    const/4 v0, 0x4

    iget-object v1, p0, Leqy;->f:Leqq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 840
    :cond_3
    iget-object v0, p0, Leqy;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 841
    const/4 v0, 0x5

    iget-object v1, p0, Leqy;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 843
    :cond_4
    iget-object v0, p0, Leqy;->i:[Lerc;

    if-eqz v0, :cond_6

    .line 844
    iget-object v1, p0, Leqy;->i:[Lerc;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 845
    if-eqz v3, :cond_5

    .line 846
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 844
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 850
    :cond_6
    iget-object v0, p0, Leqy;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 851
    const/4 v0, 0x7

    iget-object v1, p0, Leqy;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 853
    :cond_7
    iget-object v0, p0, Leqy;->k:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 854
    const/16 v0, 0x8

    iget-object v1, p0, Leqy;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 856
    :cond_8
    iget-object v0, p0, Leqy;->c:Lerl;

    if-eqz v0, :cond_9

    .line 857
    const/16 v0, 0x9

    iget-object v1, p0, Leqy;->c:Lerl;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 859
    :cond_9
    iget-object v0, p0, Leqy;->m:Ldch;

    if-eqz v0, :cond_a

    .line 860
    const/16 v0, 0xa

    iget-object v1, p0, Leqy;->m:Ldch;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 862
    :cond_a
    iget-object v0, p0, Leqy;->g:Lerl;

    if-eqz v0, :cond_b

    .line 863
    const/16 v0, 0xb

    iget-object v1, p0, Leqy;->g:Lerl;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 865
    :cond_b
    iget-object v0, p0, Leqy;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    .line 866
    const/16 v0, 0xc

    iget-object v1, p0, Leqy;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 868
    :cond_c
    iget-object v0, p0, Leqy;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 870
    return-void
.end method
