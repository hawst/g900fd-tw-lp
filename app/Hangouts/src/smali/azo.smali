.class final Lazo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/Intent;

.field final synthetic b:Lazn;


# direct methods
.method constructor <init>(Lazn;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 276
    iput-object p1, p0, Lazo;->b:Lazn;

    iput-object p2, p0, Lazo;->a:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 279
    iget-object v0, p0, Lazo;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 281
    const-string v1, "___time"

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;->a()Ljava/text/SimpleDateFormat;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 282
    iget-object v1, p0, Lazo;->b:Lazn;

    iget-object v1, v1, Lazn;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;->a(Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 283
    iget-object v1, p0, Lazo;->b:Lazn;

    iget-object v1, v1, Lazn;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;->b(Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "conversation_id"

    .line 285
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lazo;->b:Lazn;

    iget-object v2, v2, Lazn;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;

    .line 287
    invoke-static {v2}, Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;->b(Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;)Ljava/lang/String;

    move-result-object v2

    .line 284
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 289
    :cond_0
    iget-object v1, p0, Lazo;->b:Lazn;

    iget-object v1, v1, Lazn;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;->c(Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;)Landroid/widget/ArrayAdapter;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 290
    iget-object v1, p0, Lazo;->b:Lazn;

    iget-object v1, v1, Lazn;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;->c(Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;)Landroid/widget/ArrayAdapter;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 291
    iget-object v0, p0, Lazo;->b:Lazn;

    iget-object v0, v0, Lazn;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;->c(Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 294
    :cond_1
    :goto_0
    iget-object v0, p0, Lazo;->b:Lazn;

    iget-object v0, v0, Lazn;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;->a(Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0x3e8

    if-le v0, v1, :cond_2

    .line 295
    iget-object v0, p0, Lazo;->b:Lazn;

    iget-object v0, v0, Lazn;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;->a(Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 296
    iget-object v1, p0, Lazo;->b:Lazn;

    iget-object v1, v1, Lazn;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;->c(Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;)Landroid/widget/ArrayAdapter;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ArrayAdapter;->remove(Ljava/lang/Object;)V

    goto :goto_0

    .line 298
    :cond_2
    return-void
.end method
