.class public final Ldyk;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldyk;


# instance fields
.field public b:Ldvn;

.field public c:Ldxv;

.field public d:Ldzg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1866
    const/4 v0, 0x0

    new-array v0, v0, [Ldyk;

    sput-object v0, Ldyk;->a:[Ldyk;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1867
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1870
    iput-object v0, p0, Ldyk;->b:Ldvn;

    .line 1873
    iput-object v0, p0, Ldyk;->c:Ldxv;

    .line 1876
    iput-object v0, p0, Ldyk;->d:Ldzg;

    .line 1867
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1896
    const/4 v0, 0x0

    .line 1897
    iget-object v1, p0, Ldyk;->b:Ldvn;

    if-eqz v1, :cond_0

    .line 1898
    const/4 v0, 0x1

    iget-object v1, p0, Ldyk;->b:Ldvn;

    .line 1899
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1901
    :cond_0
    iget-object v1, p0, Ldyk;->c:Ldxv;

    if-eqz v1, :cond_1

    .line 1902
    const/4 v1, 0x2

    iget-object v2, p0, Ldyk;->c:Ldxv;

    .line 1903
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1905
    :cond_1
    iget-object v1, p0, Ldyk;->d:Ldzg;

    if-eqz v1, :cond_2

    .line 1906
    const/4 v1, 0x3

    iget-object v2, p0, Ldyk;->d:Ldzg;

    .line 1907
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1909
    :cond_2
    iget-object v1, p0, Ldyk;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1910
    iput v0, p0, Ldyk;->cachedSize:I

    .line 1911
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1863
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldyk;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldyk;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldyk;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldyk;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldyk;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldyk;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldyk;->c:Ldxv;

    if-nez v0, :cond_3

    new-instance v0, Ldxv;

    invoke-direct {v0}, Ldxv;-><init>()V

    iput-object v0, p0, Ldyk;->c:Ldxv;

    :cond_3
    iget-object v0, p0, Ldyk;->c:Ldxv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldyk;->d:Ldzg;

    if-nez v0, :cond_4

    new-instance v0, Ldzg;

    invoke-direct {v0}, Ldzg;-><init>()V

    iput-object v0, p0, Ldyk;->d:Ldzg;

    :cond_4
    iget-object v0, p0, Ldyk;->d:Ldzg;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1881
    iget-object v0, p0, Ldyk;->b:Ldvn;

    if-eqz v0, :cond_0

    .line 1882
    const/4 v0, 0x1

    iget-object v1, p0, Ldyk;->b:Ldvn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1884
    :cond_0
    iget-object v0, p0, Ldyk;->c:Ldxv;

    if-eqz v0, :cond_1

    .line 1885
    const/4 v0, 0x2

    iget-object v1, p0, Ldyk;->c:Ldxv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1887
    :cond_1
    iget-object v0, p0, Ldyk;->d:Ldzg;

    if-eqz v0, :cond_2

    .line 1888
    const/4 v0, 0x3

    iget-object v1, p0, Ldyk;->d:Ldzg;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1890
    :cond_2
    iget-object v0, p0, Ldyk;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1892
    return-void
.end method
