.class public final enum Ltf;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ltf;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ltf;

.field public static final enum b:Ltf;

.field public static final enum c:Ltf;

.field public static final enum d:Ltf;

.field public static final enum e:Ltf;

.field public static final enum f:Ltf;

.field public static final enum g:Ltf;

.field public static final enum h:Ltf;

.field public static final enum i:Ltf;

.field public static final enum j:Ltf;

.field public static final enum k:Ltf;

.field public static final enum l:Ltf;

.field public static final enum m:Ltf;

.field public static final enum n:Ltf;

.field private static final synthetic o:[Ltf;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 87
    new-instance v0, Ltf;

    const-string v1, "NAME"

    invoke-direct {v0, v1, v3}, Ltf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltf;->a:Ltf;

    .line 88
    new-instance v0, Ltf;

    const-string v1, "PHONE"

    invoke-direct {v0, v1, v4}, Ltf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltf;->b:Ltf;

    .line 89
    new-instance v0, Ltf;

    const-string v1, "EMAIL"

    invoke-direct {v0, v1, v5}, Ltf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltf;->c:Ltf;

    .line 90
    new-instance v0, Ltf;

    const-string v1, "POSTAL_ADDRESS"

    invoke-direct {v0, v1, v6}, Ltf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltf;->d:Ltf;

    .line 91
    new-instance v0, Ltf;

    const-string v1, "ORGANIZATION"

    invoke-direct {v0, v1, v7}, Ltf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltf;->e:Ltf;

    .line 92
    new-instance v0, Ltf;

    const-string v1, "IM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Ltf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltf;->f:Ltf;

    .line 93
    new-instance v0, Ltf;

    const-string v1, "PHOTO"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Ltf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltf;->g:Ltf;

    .line 94
    new-instance v0, Ltf;

    const-string v1, "WEBSITE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Ltf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltf;->h:Ltf;

    .line 95
    new-instance v0, Ltf;

    const-string v1, "SIP"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Ltf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltf;->i:Ltf;

    .line 96
    new-instance v0, Ltf;

    const-string v1, "NICKNAME"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Ltf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltf;->j:Ltf;

    .line 97
    new-instance v0, Ltf;

    const-string v1, "NOTE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Ltf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltf;->k:Ltf;

    .line 98
    new-instance v0, Ltf;

    const-string v1, "BIRTHDAY"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Ltf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltf;->l:Ltf;

    .line 99
    new-instance v0, Ltf;

    const-string v1, "ANNIVERSARY"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Ltf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltf;->m:Ltf;

    .line 100
    new-instance v0, Ltf;

    const-string v1, "ANDROID_CUSTOM"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Ltf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltf;->n:Ltf;

    .line 86
    const/16 v0, 0xe

    new-array v0, v0, [Ltf;

    sget-object v1, Ltf;->a:Ltf;

    aput-object v1, v0, v3

    sget-object v1, Ltf;->b:Ltf;

    aput-object v1, v0, v4

    sget-object v1, Ltf;->c:Ltf;

    aput-object v1, v0, v5

    sget-object v1, Ltf;->d:Ltf;

    aput-object v1, v0, v6

    sget-object v1, Ltf;->e:Ltf;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Ltf;->f:Ltf;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Ltf;->g:Ltf;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Ltf;->h:Ltf;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Ltf;->i:Ltf;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Ltf;->j:Ltf;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Ltf;->k:Ltf;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Ltf;->l:Ltf;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Ltf;->m:Ltf;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Ltf;->n:Ltf;

    aput-object v2, v0, v1

    sput-object v0, Ltf;->o:[Ltf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ltf;
    .locals 1

    .prologue
    .line 86
    const-class v0, Ltf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ltf;

    return-object v0
.end method

.method public static values()[Ltf;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Ltf;->o:[Ltf;

    invoke-virtual {v0}, [Ltf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ltf;

    return-object v0
.end method
