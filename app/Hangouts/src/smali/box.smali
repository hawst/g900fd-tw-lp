.class public final Lbox;
.super Lbnj;
.source "PG"


# static fields
.field private static final d:Z


# instance fields
.field private final a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lbys;->k:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbox;->d:Z

    return-void
.end method

.method public constructor <init>(Lyj;Z)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lbnj;-><init>(Lyj;)V

    .line 39
    iput-boolean p2, p0, Lbox;->a:Z

    .line 40
    return-void
.end method


# virtual methods
.method public a()V
    .locals 13

    .prologue
    .line 47
    invoke-static {}, Lbxp;->a()J

    move-result-wide v1

    .line 49
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 51
    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v11

    .line 55
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xd

    if-lt v3, v4, :cond_1

    .line 56
    iget v6, v11, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    .line 58
    :goto_0
    iget-object v3, p0, Lbox;->b:Lyj;

    .line 59
    invoke-static {v0, v3}, Lf;->b(Landroid/content/Context;Lyj;)Z

    move-result v9

    .line 60
    sget-boolean v0, Lbox;->d:Z

    if-eqz v0, :cond_0

    .line 61
    const-string v0, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "RegisterAccountOperation, userWantsIncomingPhoneCalls: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    :cond_0
    iget-object v0, p0, Lbox;->b:Lyj;

    .line 66
    invoke-static {v0}, Lbtf;->a(Lyj;)Z

    move-result v12

    .line 68
    invoke-static {}, Lbld;->c()Lbld;

    move-result-object v0

    invoke-virtual {v0}, Lbld;->f()Ljava/lang/String;

    move-result-object v0

    .line 69
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 70
    const-string v0, "Babel"

    const-string v1, "Register account with invalid gcm registration id"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lbox;->b:Lyj;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbkb;->a(Lyj;Ljava/lang/Exception;)V

    .line 83
    :goto_1
    return-void

    .line 56
    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    .line 77
    :cond_2
    invoke-static {}, Lape;->b()Z

    move-result v3

    .line 78
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iget-boolean v5, p0, Lbox;->a:Z

    .line 79
    invoke-static {}, Lbkb;->m()Z

    move-result v7

    iget-object v8, p0, Lbox;->b:Lyj;

    invoke-virtual {v8}, Lyj;->w()Z

    move-result v8

    iget v10, v11, Landroid/content/res/Configuration;->mcc:I

    iget v11, v11, Landroid/content/res/Configuration;->mnc:I

    .line 75
    invoke-static/range {v0 .. v12}, Lbev;->a(Ljava/lang/String;JZLjava/lang/String;ZIZZZIIZ)Lbev;

    move-result-object v0

    .line 82
    iget-object v1, p0, Lbox;->c:Lbnl;

    invoke-virtual {v1, v0}, Lbnl;->a(Lbea;)V

    goto :goto_1
.end method
