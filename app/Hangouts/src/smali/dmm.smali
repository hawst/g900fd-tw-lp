.class public final Ldmm;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldmm;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7876
    const/4 v0, 0x0

    new-array v0, v0, [Ldmm;

    sput-object v0, Ldmm;->a:[Ldmm;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7877
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 7903
    const/4 v0, 0x0

    .line 7904
    iget-object v1, p0, Ldmm;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 7905
    const/4 v0, 0x1

    iget-object v1, p0, Ldmm;->b:Ljava/lang/String;

    .line 7906
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7908
    :cond_0
    iget-object v1, p0, Ldmm;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 7909
    const/4 v1, 0x2

    iget-object v2, p0, Ldmm;->c:Ljava/lang/Boolean;

    .line 7910
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7912
    :cond_1
    iget-object v1, p0, Ldmm;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 7913
    const/4 v1, 0x3

    iget-object v2, p0, Ldmm;->d:Ljava/lang/String;

    .line 7914
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7916
    :cond_2
    iget-object v1, p0, Ldmm;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7917
    iput v0, p0, Ldmm;->cachedSize:I

    .line 7918
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 7873
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldmm;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldmm;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldmm;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldmm;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldmm;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldmm;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 7888
    iget-object v0, p0, Ldmm;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 7889
    const/4 v0, 0x1

    iget-object v1, p0, Ldmm;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 7891
    :cond_0
    iget-object v0, p0, Ldmm;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 7892
    const/4 v0, 0x2

    iget-object v1, p0, Ldmm;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 7894
    :cond_1
    iget-object v0, p0, Ldmm;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 7895
    const/4 v0, 0x3

    iget-object v1, p0, Ldmm;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 7897
    :cond_2
    iget-object v0, p0, Ldmm;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 7899
    return-void
.end method
