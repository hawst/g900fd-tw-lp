.class public Lbdw;
.super Lbdp;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final a:[B


# direct methods
.method public constructor <init>([B)V
    .locals 0

    .prologue
    .line 413
    invoke-direct {p0}, Lbdp;-><init>()V

    .line 414
    iput-object p1, p0, Lbdw;->a:[B

    .line 415
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 3

    .prologue
    .line 419
    new-instance v1, Ldzm;

    invoke-direct {v1}, Ldzm;-><init>()V

    .line 421
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lbdw;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v0

    iput-object v0, v1, Ldzm;->b:Ldvm;

    .line 422
    new-instance v0, Ldzk;

    invoke-direct {v0}, Ldzk;-><init>()V

    iget-object v2, p0, Lbdw;->a:[B

    invoke-static {v0, v2}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldzk;

    iput-object v0, v1, Ldzm;->c:Ldzk;
    :try_end_0
    .catch Lepq; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 428
    :goto_0
    return-object v0

    .line 424
    :catch_0
    move-exception v0

    .line 425
    const-string v1, "Babel"

    const-string v2, "Parse failed"

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 426
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 433
    const-string v0, "hangout_invitations/modify"

    return-object v0
.end method

.method public d()J
    .locals 3

    .prologue
    .line 438
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method
