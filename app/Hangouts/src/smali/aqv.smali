.class public final Laqv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Laow;
.implements Lapu;
.implements Lasd;
.implements Lasf;


# instance fields
.field private final A:Landroid/widget/ImageButton;

.field private final B:Landroid/widget/ImageButton;

.field private final C:Landroid/widget/ImageButton;

.field private final D:Landroid/widget/ImageButton;

.field private final E:Landroid/widget/ImageButton;

.field private final F:Landroid/widget/ImageButton;

.field private final G:Landroid/widget/Button;

.field private H:Z

.field private final I:Landroid/view/View;

.field private final J:Landroid/view/View;

.field private final K:Landroid/view/View;

.field private final L:Landroid/view/View;

.field private final M:Landroid/view/View$OnClickListener;

.field private final N:Landroid/view/View$OnClickListener;

.field private final O:Landroid/view/View$OnClickListener;

.field private final P:Landroid/view/View$OnClickListener;

.field private final Q:Landroid/view/View$OnClickListener;

.field a:Lapv;

.field b:Laro;

.field c:Larn;

.field d:Z

.field e:Z

.field f:I

.field final g:Lbme;

.field final h:Lapk;

.field final i:Landroid/view/ViewGroup;

.field final j:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

.field k:Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;

.field l:Z

.field private m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

.field private n:Ljava/lang/Runnable;

.field private final o:Larm;

.field private final p:Landroid/view/View;

.field private final q:Landroid/view/View;

.field private final r:Landroid/view/ViewGroup;

.field private s:Lauj;

.field private final t:Landroid/os/Handler;

.field private u:Z

.field private v:Z

.field private final w:Ljava/lang/Runnable;

.field private final x:Landroid/view/ViewGroup;

.field private final y:Landroid/widget/LinearLayout;

.field private final z:Landroid/widget/ImageButton;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    iput v2, p0, Laqv;->f:I

    .line 128
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    iput-object v0, p0, Laqv;->h:Lapk;

    .line 129
    new-instance v0, Larm;

    invoke-direct {v0, p0, v2}, Larm;-><init>(Laqv;B)V

    iput-object v0, p0, Laqv;->o:Larm;

    .line 136
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Laqv;->t:Landroid/os/Handler;

    .line 138
    iput-boolean v1, p0, Laqv;->v:Z

    .line 139
    new-instance v0, Laqw;

    invoke-direct {v0, p0}, Laqw;-><init>(Laqv;)V

    iput-object v0, p0, Laqv;->w:Ljava/lang/Runnable;

    .line 164
    iput-boolean v1, p0, Laqv;->H:Z

    .line 270
    new-instance v0, Larg;

    invoke-direct {v0, p0}, Larg;-><init>(Laqv;)V

    iput-object v0, p0, Laqv;->M:Landroid/view/View$OnClickListener;

    .line 290
    new-instance v0, Larh;

    invoke-direct {v0, p0}, Larh;-><init>(Laqv;)V

    iput-object v0, p0, Laqv;->N:Landroid/view/View$OnClickListener;

    .line 303
    new-instance v0, Lari;

    invoke-direct {v0, p0}, Lari;-><init>(Laqv;)V

    iput-object v0, p0, Laqv;->O:Landroid/view/View$OnClickListener;

    .line 317
    new-instance v0, Larj;

    invoke-direct {v0, p0}, Larj;-><init>(Laqv;)V

    iput-object v0, p0, Laqv;->P:Landroid/view/View$OnClickListener;

    .line 330
    new-instance v0, Lark;

    invoke-direct {v0, p0}, Lark;-><init>(Laqv;)V

    iput-object v0, p0, Laqv;->Q:Landroid/view/View$OnClickListener;

    .line 173
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbme;

    invoke-static {v0, v1}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    iput-object v0, p0, Laqv;->g:Lbme;

    .line 175
    iput-object p2, p0, Laqv;->p:Landroid/view/View;

    .line 176
    invoke-virtual {p1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->getActivity()Ly;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    iput-object v0, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    .line 178
    sget v0, Lg;->cr:I

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    iput-object v0, p0, Laqv;->j:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    .line 180
    sget v0, Lg;->dc:I

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Laqv;->i:Landroid/view/ViewGroup;

    .line 181
    sget v0, Lg;->cH:I

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laqv;->q:Landroid/view/View;

    .line 182
    sget v0, Lg;->cK:I

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Laqv;->r:Landroid/view/ViewGroup;

    .line 185
    iget-object v0, p0, Laqv;->q:Landroid/view/View;

    sget v1, Lg;->dk:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Laqv;->y:Landroid/widget/LinearLayout;

    .line 186
    iget-object v0, p0, Laqv;->y:Landroid/widget/LinearLayout;

    sget v1, Lg;->cT:I

    .line 187
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Laqv;->z:Landroid/widget/ImageButton;

    .line 188
    iget-object v0, p0, Laqv;->z:Landroid/widget/ImageButton;

    iget-object v1, p0, Laqv;->M:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    iget-object v0, p0, Laqv;->z:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 190
    iget-object v0, p0, Laqv;->y:Landroid/widget/LinearLayout;

    sget v1, Lg;->cV:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Laqv;->A:Landroid/widget/ImageButton;

    .line 191
    iput-boolean v2, p0, Laqv;->d:Z

    .line 192
    iput-boolean v2, p0, Laqv;->e:Z

    .line 194
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 195
    iget-object v0, p0, Laqv;->y:Landroid/widget/LinearLayout;

    sget v1, Lg;->dl:I

    .line 196
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;

    iput-object v0, p0, Laqv;->k:Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;

    .line 197
    iget-object v0, p0, Laqv;->k:Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;

    new-instance v1, Lare;

    invoke-direct {v1, p0}, Lare;-><init>(Laqv;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->a(Lkg;)V

    .line 207
    :cond_0
    iget-object v0, p0, Laqv;->y:Landroid/widget/LinearLayout;

    sget v1, Lg;->cU:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Laqv;->B:Landroid/widget/ImageButton;

    .line 209
    iget-object v0, p0, Laqv;->B:Landroid/widget/ImageButton;

    iget-object v1, p0, Laqv;->N:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    iget-object v0, p0, Laqv;->y:Landroid/widget/LinearLayout;

    sget v1, Lg;->cS:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Laqv;->C:Landroid/widget/ImageButton;

    .line 212
    iget-object v0, p0, Laqv;->C:Landroid/widget/ImageButton;

    iget-object v1, p0, Laqv;->P:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    iget-object v0, p0, Laqv;->y:Landroid/widget/LinearLayout;

    sget v1, Lg;->cW:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Laqv;->F:Landroid/widget/ImageButton;

    .line 215
    iget-object v0, p0, Laqv;->F:Landroid/widget/ImageButton;

    iget-object v1, p0, Laqv;->O:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 217
    sget v0, Lg;->cY:I

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Laqv;->E:Landroid/widget/ImageButton;

    .line 218
    sget v0, Lg;->cX:I

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Laqv;->D:Landroid/widget/ImageButton;

    .line 221
    sget v0, Lg;->dX:I

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Laqv;->G:Landroid/widget/Button;

    .line 222
    iget-object v0, p0, Laqv;->G:Landroid/widget/Button;

    iget-object v1, p0, Laqv;->Q:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 224
    iget-object v0, p0, Laqv;->q:Landroid/view/View;

    sget v1, Lg;->cI:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Laqv;->x:Landroid/view/ViewGroup;

    .line 227
    iget-object v0, p0, Laqv;->q:Landroid/view/View;

    sget v1, Lg;->dh:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laqv;->I:Landroid/view/View;

    .line 228
    iget-object v0, p0, Laqv;->I:Landroid/view/View;

    sget v1, Lg;->cZ:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laqv;->J:Landroid/view/View;

    .line 230
    iget-object v0, p0, Laqv;->I:Landroid/view/View;

    sget v1, Lg;->db:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laqv;->K:Landroid/view/View;

    .line 232
    iget-object v0, p0, Laqv;->I:Landroid/view/View;

    sget v1, Lg;->da:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laqv;->L:Landroid/view/View;

    .line 235
    iget-object v0, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->g()Lkd;

    move-result-object v0

    new-instance v1, Larf;

    invoke-direct {v1, p0}, Larf;-><init>(Laqv;)V

    invoke-virtual {v0, v1}, Lkd;->a(Lkg;)V

    .line 242
    return-void
.end method

.method private a(IZ)V
    .locals 6

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    const/4 v5, 0x1

    .line 789
    iget-object v3, p0, Laqv;->y:Landroid/widget/LinearLayout;

    if-ne p1, v5, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 790
    iget-object v0, p0, Laqv;->I:Landroid/view/View;

    const/4 v3, 0x2

    if-ne p1, v3, :cond_0

    move v2, v1

    :cond_0
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 793
    if-eqz p2, :cond_3

    .line 795
    invoke-direct {p0}, Laqv;->p()V

    .line 797
    iget-object v0, p0, Laqv;->p:Landroid/view/View;

    invoke-static {v0}, Lbxp;->b(Landroid/view/View;)V

    .line 805
    :goto_1
    iget-boolean v0, p0, Laqv;->v:Z

    if-nez v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-ge v0, v2, :cond_4

    iput-boolean v5, p0, Laqv;->v:Z

    invoke-virtual {p0}, Laqv;->k()V

    .line 809
    :cond_1
    :goto_2
    iget-object v0, p0, Laqv;->r:Landroid/view/ViewGroup;

    .line 810
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 811
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 812
    iget-object v1, p0, Laqv;->r:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 814
    invoke-virtual {p0}, Laqv;->f()V

    .line 816
    iput p1, p0, Laqv;->f:I

    .line 817
    return-void

    :cond_2
    move v0, v2

    .line 789
    goto :goto_0

    .line 800
    :cond_3
    iput-boolean v5, p0, Laqv;->v:Z

    .line 801
    invoke-virtual {p0}, Laqv;->k()V

    goto :goto_1

    .line 805
    :cond_4
    invoke-direct {p0}, Laqv;->o()V

    invoke-direct {p0}, Laqv;->m()I

    move-result v0

    neg-int v2, v0

    rsub-int/lit8 v3, v2, 0x0

    iget-object v0, p0, Laqv;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    new-instance v4, Larb;

    invoke-direct {v4, p0, v0, v2, v3}, Larb;-><init>(Laqv;Landroid/widget/RelativeLayout$LayoutParams;II)V

    iget-object v0, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lf;->dN:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v2, v0

    invoke-virtual {v4, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Laqv;->s:Lauj;

    invoke-virtual {v0, v4}, Lauj;->trackAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Laqv;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    iput-boolean v5, p0, Laqv;->v:Z

    goto :goto_2
.end method

.method static synthetic a(Laqv;Z)V
    .locals 0

    .prologue
    .line 59
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Laqv;->g()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Laqv;->f()V

    goto :goto_0
.end method

.method private m()I
    .locals 2

    .prologue
    .line 571
    iget-object v0, p0, Laqv;->x:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 572
    if-eqz v0, :cond_0

    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ltz v1, :cond_0

    .line 573
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 575
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Laqv;->n()I

    move-result v0

    goto :goto_0
.end method

.method private n()I
    .locals 2

    .prologue
    .line 580
    iget-object v0, p0, Laqv;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 581
    iget-object v1, p0, Laqv;->h:Lapk;

    invoke-virtual {v1}, Lapk;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lf;->dd:I

    .line 582
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 584
    :goto_0
    return v0

    .line 582
    :cond_0
    sget v1, Lf;->de:I

    .line 583
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0
.end method

.method private o()V
    .locals 1

    .prologue
    .line 588
    iget-object v0, p0, Laqv;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 589
    if-eqz v0, :cond_0

    .line 590
    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 592
    :cond_0
    return-void
.end method

.method private p()V
    .locals 1

    .prologue
    .line 821
    iget-boolean v0, p0, Laqv;->H:Z

    if-eqz v0, :cond_0

    .line 822
    invoke-direct {p0}, Laqv;->s()V

    .line 824
    :cond_0
    return-void
.end method

.method private q()V
    .locals 1

    .prologue
    .line 1011
    iget-object v0, p0, Laqv;->n:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1012
    iget-object v0, p0, Laqv;->n:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1013
    const/4 v0, 0x0

    iput-object v0, p0, Laqv;->n:Ljava/lang/Runnable;

    .line 1015
    :cond_0
    return-void
.end method

.method private r()V
    .locals 1

    .prologue
    .line 1027
    iget-object v0, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->g()Lkd;

    move-result-object v0

    invoke-virtual {v0}, Lkd;->g()V

    .line 1028
    return-void
.end method

.method private s()V
    .locals 1

    .prologue
    .line 1031
    iget-object v0, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->g()Lkd;

    move-result-object v0

    invoke-virtual {v0}, Lkd;->f()V

    .line 1032
    return-void
.end method


# virtual methods
.method a(F)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 640
    iget-object v0, p0, Laqv;->q:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 641
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 780
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 782
    iget v0, p0, Laqv;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 783
    invoke-virtual {p0, v2}, Laqv;->e(Z)V

    .line 785
    :cond_0
    return-void
.end method

.method public a(Lapv;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 726
    iput-object p1, p0, Laqv;->a:Lapv;

    .line 727
    iget-object v0, p0, Laqv;->k:Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;

    if-eqz v0, :cond_1

    .line 728
    iget-object v3, p0, Laqv;->k:Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;

    .line 729
    iget-object v0, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    const-string v4, "phone"

    invoke-virtual {v0, v4}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v4

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    if-nez v0, :cond_0

    if-eqz v4, :cond_2

    invoke-virtual {v4, v2}, Landroid/bluetooth/BluetoothAdapter;->getProfileConnectionState(I)I

    move-result v0

    const/4 v4, 0x2

    if-ne v0, v4, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    move v0, v1

    .line 728
    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->setVisibility(I)V

    .line 730
    iget-object v0, p0, Laqv;->k:Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->a()V

    .line 733
    :cond_1
    invoke-static {}, Lbxm;->b()Z

    move-result v0

    iput-boolean v0, p0, Laqv;->u:Z

    .line 735
    invoke-virtual {p0}, Laqv;->a()Z

    move-result v0

    iput-boolean v0, p0, Laqv;->l:Z

    .line 737
    invoke-virtual {p0}, Laqv;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 739
    iput-boolean v1, p0, Laqv;->v:Z

    .line 740
    invoke-virtual {p0}, Laqv;->d()V

    .line 746
    :goto_2
    iget-object v0, p0, Laqv;->h:Lapk;

    iget-object v2, p0, Laqv;->o:Larm;

    invoke-virtual {v0, v2}, Lapk;->a(Lapo;)V

    .line 748
    new-instance v0, Lard;

    invoke-direct {v0, p0, p1}, Lard;-><init>(Laqv;Lapv;)V

    .line 756
    iget-object v2, p0, Laqv;->E:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 757
    iget-object v2, p0, Laqv;->D:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 759
    invoke-virtual {p0}, Laqv;->b()V

    .line 763
    invoke-virtual {p0, v1}, Laqv;->d(Z)V

    .line 766
    invoke-virtual {p0}, Laqv;->k()V

    .line 767
    return-void

    :cond_2
    move v0, v1

    .line 729
    goto :goto_0

    :cond_3
    const/16 v0, 0x8

    goto :goto_1

    .line 743
    :cond_4
    iput-boolean v2, p0, Laqv;->v:Z

    .line 744
    invoke-virtual {p0, v2}, Laqv;->e(Z)V

    goto :goto_2
.end method

.method public a(Larn;)V
    .locals 0

    .prologue
    .line 935
    iput-object p1, p0, Laqv;->c:Larn;

    .line 936
    return-void
.end method

.method public a(Laro;)V
    .locals 0

    .prologue
    .line 928
    iput-object p1, p0, Laqv;->b:Laro;

    .line 929
    return-void
.end method

.method public a(Lauj;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Laqv;->s:Lauj;

    .line 268
    return-void
.end method

.method public a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 10

    .prologue
    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 484
    iget-object v0, p0, Laqv;->g:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v3, 0x62d

    .line 485
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 484
    invoke-virtual {v0, v3}, Laux;->a(Ljava/lang/Integer;)V

    .line 487
    invoke-direct {p0}, Laqv;->q()V

    .line 488
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isConnecting()Z

    move-result v0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 489
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getEndpointMucJid()Ljava/lang/String;

    move-result-object v6

    .line 490
    instance-of v7, p1, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    .line 491
    instance-of v8, p1, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    .line 493
    if-nez v7, :cond_5

    move v0, v1

    .line 494
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfUser()Z

    move-result v3

    if-nez v3, :cond_6

    if-nez v7, :cond_6

    move v3, v1

    .line 495
    :goto_1
    iget-object v9, p0, Laqv;->J:Landroid/view/View;

    if-eqz v0, :cond_7

    move v4, v2

    :goto_2
    invoke-virtual {v9, v4}, Landroid/view/View;->setVisibility(I)V

    .line 498
    iget-object v9, p0, Laqv;->K:Landroid/view/View;

    if-eqz v3, :cond_8

    move v4, v2

    :goto_3
    invoke-virtual {v9, v4}, Landroid/view/View;->setVisibility(I)V

    .line 499
    iget-object v4, p0, Laqv;->L:Landroid/view/View;

    if-eqz v7, :cond_0

    move v5, v2

    :cond_0
    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 501
    if-eqz v0, :cond_1

    .line 502
    iget-object v0, p0, Laqv;->J:Landroid/view/View;

    new-instance v4, Laqx;

    invoke-direct {v4, p0, v6}, Laqx;-><init>(Laqv;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 513
    :cond_1
    if-eqz v3, :cond_3

    .line 514
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isMediaBlocked()Z

    move-result v0

    if-nez v0, :cond_2

    if-nez v8, :cond_9

    .line 515
    :cond_2
    iget-object v0, p0, Laqv;->K:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 530
    :cond_3
    :goto_4
    if-eqz v7, :cond_4

    .line 531
    iget-object v0, p0, Laqv;->L:Landroid/view/View;

    new-instance v1, Laqz;

    invoke-direct {v1, p0, v6}, Laqz;-><init>(Laqv;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 542
    :cond_4
    const/4 v0, 0x2

    invoke-direct {p0, v0, v2}, Laqv;->a(IZ)V

    .line 543
    return-void

    :cond_5
    move v0, v2

    .line 493
    goto :goto_0

    :cond_6
    move v3, v2

    .line 494
    goto :goto_1

    :cond_7
    move v4, v5

    .line 495
    goto :goto_2

    :cond_8
    move v4, v5

    .line 498
    goto :goto_3

    .line 517
    :cond_9
    iget-object v0, p0, Laqv;->K:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 518
    iget-object v0, p0, Laqv;->K:Landroid/view/View;

    new-instance v1, Laqy;

    invoke-direct {v1, p0, p1}, Laqy;-><init>(Laqv;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_4
.end method

.method public a(Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;)V
    .locals 3

    .prologue
    .line 546
    invoke-static {p1}, Laqi;->a(Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;)Laqi;

    move-result-object v0

    .line 547
    iget-object v1, p0, Laqv;->a:Lapv;

    invoke-virtual {v1}, Lapv;->e()Lae;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Laqi;->a(Lae;Ljava/lang/String;)V

    .line 548
    return-void
.end method

.method a(Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;)V
    .locals 4

    .prologue
    .line 463
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    .line 464
    :goto_0
    if-eqz v0, :cond_0

    .line 465
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;->getEndpointMucJid()Ljava/lang/String;

    move-result-object v1

    .line 466
    iget-object v2, p0, Laqv;->A:Landroid/widget/ImageButton;

    new-instance v3, Larl;

    invoke-direct {v3, p0, v1}, Larl;-><init>(Laqv;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 475
    :cond_0
    iget-object v1, p0, Laqv;->A:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 476
    return-void

    .line 463
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 1004
    iget-object v0, p0, Laqv;->n:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1005
    iget-object v0, p0, Laqv;->n:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1007
    :cond_0
    iput-object p1, p0, Laqv;->n:Ljava/lang/Runnable;

    .line 1008
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 981
    if-eqz p1, :cond_1

    .line 983
    invoke-direct {p0}, Laqv;->r()V

    .line 984
    iget-object v0, p0, Laqv;->p:Landroid/view/View;

    invoke-static {v0}, Lbxp;->a(Landroid/view/View;)V

    .line 993
    :cond_0
    :goto_0
    return-void

    .line 985
    :cond_1
    iget v0, p0, Laqv;->f:I

    if-eqz v0, :cond_0

    .line 987
    invoke-direct {p0}, Laqv;->p()V

    .line 988
    iget-object v0, p0, Laqv;->p:Landroid/view/View;

    invoke-static {v0}, Lbxp;->b(Landroid/view/View;)V

    .line 991
    invoke-virtual {p0}, Laqv;->k()V

    goto :goto_0
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 997
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getLocalState()Lcom/google/android/libraries/hangouts/video/LocalState;

    move-result-object v0

    .line 998
    if-eqz v0, :cond_0

    .line 999
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/LocalState;->getAudioDeviceState()Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->EARPIECE_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()V
    .locals 13

    .prologue
    const/4 v1, 0x0

    const/16 v6, 0x8

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 418
    iget-object v0, p0, Laqv;->h:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    .line 419
    if-nez v0, :cond_3

    move-object v0, v1

    .line 420
    :goto_0
    if-nez v0, :cond_4

    move-object v0, v1

    .line 422
    :goto_1
    iget-object v2, p0, Laqv;->h:Lapk;

    invoke-virtual {v2}, Lapk;->f()Z

    move-result v11

    .line 423
    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    if-ne v2, v3, :cond_5

    move v10, v3

    .line 424
    :goto_2
    if-nez v0, :cond_6

    move v2, v3

    .line 425
    :goto_3
    iget-object v5, p0, Laqv;->a:Lapv;

    invoke-virtual {v5}, Lapv;->h()I

    move-result v5

    .line 426
    and-int/lit8 v7, v5, 0x1

    if-eqz v7, :cond_8

    move v7, v3

    .line 428
    :goto_4
    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_9

    move v5, v3

    .line 430
    :goto_5
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/CameraInterface;->getInstance()Lcom/google/android/libraries/hangouts/video/CameraInterface;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/libraries/hangouts/video/CameraInterface;->getCameraCount()I

    move-result v12

    .line 432
    if-eqz v7, :cond_a

    if-nez v10, :cond_0

    if-eqz v2, :cond_a

    :cond_0
    move v9, v3

    .line 434
    :goto_6
    if-nez v9, :cond_b

    if-eqz v5, :cond_b

    if-nez v11, :cond_b

    if-lez v12, :cond_b

    move v8, v3

    .line 436
    :goto_7
    if-nez v9, :cond_c

    if-eqz v5, :cond_c

    if-eqz v11, :cond_c

    if-lez v12, :cond_c

    move v7, v3

    .line 438
    :goto_8
    if-nez v9, :cond_d

    if-eqz v5, :cond_d

    if-nez v11, :cond_d

    if-le v12, v3, :cond_d

    move v2, v3

    .line 441
    :goto_9
    iget-object v12, p0, Laqv;->B:Landroid/widget/ImageButton;

    if-eqz v8, :cond_e

    move v5, v4

    :goto_a
    invoke-virtual {v12, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 442
    iget-object v8, p0, Laqv;->F:Landroid/widget/ImageButton;

    if-eqz v7, :cond_f

    move v5, v4

    :goto_b
    invoke-virtual {v8, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 443
    iget-object v5, p0, Laqv;->C:Landroid/widget/ImageButton;

    if-eqz v2, :cond_10

    move v2, v4

    :goto_c
    invoke-virtual {v5, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 444
    iget-object v5, p0, Laqv;->E:Landroid/widget/ImageButton;

    if-nez v11, :cond_11

    move v2, v4

    :goto_d
    invoke-virtual {v5, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 445
    iget-object v5, p0, Laqv;->D:Landroid/widget/ImageButton;

    if-eqz v11, :cond_12

    move v2, v4

    :goto_e
    invoke-virtual {v5, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 446
    iget-object v2, p0, Laqv;->A:Landroid/widget/ImageButton;

    if-eqz v9, :cond_1

    move v6, v4

    :cond_1
    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 450
    if-eqz v10, :cond_13

    if-eqz v9, :cond_13

    .line 451
    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 452
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 453
    invoke-interface {v0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    aget-object v0, v0, v4

    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 454
    instance-of v1, v0, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    if-eqz v1, :cond_2

    .line 455
    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    invoke-virtual {p0, v0}, Laqv;->a(Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;)V

    .line 460
    :cond_2
    :goto_f
    return-void

    .line 419
    :cond_3
    invoke-virtual {v0}, Lapx;->o()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    goto/16 :goto_0

    .line 421
    :cond_4
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getRemoteEndpoints()Ljava/util/Collection;

    move-result-object v0

    goto/16 :goto_1

    :cond_5
    move v10, v4

    .line 423
    goto/16 :goto_2

    .line 424
    :cond_6
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    if-nez v2, :cond_7

    move v2, v3

    goto/16 :goto_3

    :cond_7
    move v2, v4

    goto/16 :goto_3

    :cond_8
    move v7, v4

    .line 426
    goto/16 :goto_4

    :cond_9
    move v5, v4

    .line 428
    goto/16 :goto_5

    :cond_a
    move v9, v4

    .line 432
    goto/16 :goto_6

    :cond_b
    move v8, v4

    .line 434
    goto/16 :goto_7

    :cond_c
    move v7, v4

    .line 436
    goto/16 :goto_8

    :cond_d
    move v2, v4

    .line 438
    goto/16 :goto_9

    :cond_e
    move v5, v6

    .line 441
    goto/16 :goto_a

    :cond_f
    move v5, v6

    .line 442
    goto :goto_b

    :cond_10
    move v2, v6

    .line 443
    goto :goto_c

    :cond_11
    move v2, v6

    .line 444
    goto :goto_d

    :cond_12
    move v2, v6

    .line 445
    goto :goto_e

    .line 458
    :cond_13
    invoke-virtual {p0, v1}, Laqv;->a(Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;)V

    goto :goto_f
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 1018
    if-eqz p1, :cond_0

    .line 1019
    invoke-direct {p0}, Laqv;->r()V

    .line 1023
    :goto_0
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Laqv;->H:Z

    .line 1024
    return-void

    .line 1021
    :cond_0
    invoke-direct {p0}, Laqv;->s()V

    goto :goto_0

    .line 1023
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public c()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 551
    iget-object v0, p0, Laqv;->h:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v1

    .line 552
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lapx;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    move v3, v5

    .line 553
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lapx;->K()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    sget v1, Lh;->jH:I

    .line 554
    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 558
    :goto_1
    iget-object v0, p0, Laqv;->a:Lapv;

    .line 559
    invoke-virtual {v0}, Lapv;->a()Lyj;

    move-result-object v0

    if-eqz v3, :cond_2

    const/4 v4, 0x5

    :goto_2
    move-object v3, v2

    .line 558
    invoke-static/range {v0 .. v5}, Lbbl;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;II)Landroid/content/Intent;

    move-result-object v0

    .line 567
    iget-object v1, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->startActivity(Landroid/content/Intent;)V

    .line 568
    return-void

    .line 552
    :cond_0
    const/4 v0, 0x0

    move v3, v0

    goto :goto_0

    .line 554
    :cond_1
    iget-object v0, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    sget v1, Lh;->jG:I

    .line 556
    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 559
    :cond_2
    const/4 v4, 0x6

    goto :goto_2
.end method

.method public c(Z)V
    .locals 3

    .prologue
    .line 349
    if-eqz p1, :cond_1

    .line 350
    iget-object v0, p0, Laqv;->z:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->bS:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 351
    iget-object v0, p0, Laqv;->z:Landroid/widget/ImageButton;

    iget-object v1, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    .line 352
    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lh;->eK:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 351
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 353
    iget-object v0, p0, Laqv;->z:Landroid/widget/ImageButton;

    iget-object v1, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    .line 354
    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->co:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 353
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    .line 362
    :goto_0
    iget-boolean v0, p0, Laqv;->d:Z

    if-eq v0, p1, :cond_0

    .line 363
    iput-boolean p1, p0, Laqv;->d:Z

    .line 364
    iget-object v0, p0, Laqv;->g:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v1

    iget-boolean v0, p0, Laqv;->d:Z

    if-eqz v0, :cond_2

    const/16 v0, 0xb5

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Laux;->a(Ljava/lang/Integer;)V

    .line 367
    iget-boolean v0, p0, Laqv;->d:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    .line 368
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->eJ:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 370
    :goto_2
    iget-object v1, p0, Laqv;->z:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Lf;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityManager;Ljava/lang/CharSequence;)V

    .line 373
    :cond_0
    return-void

    .line 356
    :cond_1
    iget-object v0, p0, Laqv;->z:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->bR:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 357
    iget-object v0, p0, Laqv;->z:Landroid/widget/ImageButton;

    iget-object v1, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    .line 358
    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lh;->eJ:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 357
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 359
    iget-object v0, p0, Laqv;->z:Landroid/widget/ImageButton;

    iget-object v1, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    .line 360
    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->cK:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 359
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    goto :goto_0

    .line 364
    :cond_2
    const/16 v0, 0xb7

    goto :goto_1

    .line 368
    :cond_3
    iget-object v0, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    .line 369
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->eK:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public d()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 683
    invoke-virtual {p0}, Laqv;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 684
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Laqv;->e(Z)V

    .line 707
    :goto_0
    return-void

    .line 688
    :cond_0
    invoke-direct {p0}, Laqv;->r()V

    .line 689
    iget-object v0, p0, Laqv;->p:Landroid/view/View;

    invoke-static {v0}, Lbxp;->a(Landroid/view/View;)V

    .line 692
    iget-boolean v0, p0, Laqv;->v:Z

    if-eqz v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_2

    iput-boolean v4, p0, Laqv;->v:Z

    invoke-virtual {p0}, Laqv;->k()V

    .line 695
    :cond_1
    :goto_1
    iget-object v0, p0, Laqv;->r:Landroid/view/ViewGroup;

    .line 696
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 697
    iget-object v1, p0, Laqv;->r:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 699
    sget v2, Lf;->di:I

    .line 700
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sget v3, Lf;->dc:I

    .line 701
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int v1, v2, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 702
    iget-object v1, p0, Laqv;->r:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 704
    iput v4, p0, Laqv;->f:I

    .line 705
    invoke-virtual {p0}, Laqv;->g()V

    .line 706
    invoke-direct {p0}, Laqv;->q()V

    goto :goto_0

    .line 692
    :cond_2
    invoke-direct {p0}, Laqv;->o()V

    invoke-direct {p0}, Laqv;->m()I

    move-result v0

    iget-object v1, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->dc:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    neg-int v1, v0

    iget-object v0, p0, Laqv;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    new-instance v2, Lara;

    invoke-direct {v2, p0, v0, v1}, Lara;-><init>(Laqv;Landroid/widget/RelativeLayout$LayoutParams;I)V

    iget-object v0, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->dN:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Laqv;->s:Lauj;

    invoke-virtual {v0, v2}, Lauj;->trackAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Laqv;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    iput-boolean v4, p0, Laqv;->v:Z

    goto :goto_1
.end method

.method public d(Z)V
    .locals 3

    .prologue
    .line 376
    if-eqz p1, :cond_1

    .line 377
    iget-object v0, p0, Laqv;->B:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->cv:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 378
    iget-object v0, p0, Laqv;->B:Landroid/widget/ImageButton;

    iget-object v1, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    .line 379
    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lh;->eP:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 378
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 380
    iget-object v0, p0, Laqv;->B:Landroid/widget/ImageButton;

    iget-object v1, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    .line 381
    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->co:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 380
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    .line 390
    :goto_0
    iget-boolean v0, p0, Laqv;->e:Z

    if-eq v0, p1, :cond_0

    .line 391
    iput-boolean p1, p0, Laqv;->e:Z

    .line 392
    iget-object v0, p0, Laqv;->g:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v1

    iget-boolean v0, p0, Laqv;->e:Z

    if-eqz v0, :cond_2

    const/16 v0, 0xb6

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Laux;->a(Ljava/lang/Integer;)V

    .line 395
    iget-boolean v0, p0, Laqv;->e:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    .line 396
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->eO:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 398
    :goto_2
    iget-object v1, p0, Laqv;->B:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Lf;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityManager;Ljava/lang/CharSequence;)V

    .line 401
    :cond_0
    return-void

    .line 383
    :cond_1
    iget-object v0, p0, Laqv;->B:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->ct:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 384
    iget-object v0, p0, Laqv;->B:Landroid/widget/ImageButton;

    iget-object v1, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    .line 385
    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lh;->eO:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 384
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 386
    iget-object v0, p0, Laqv;->B:Landroid/widget/ImageButton;

    iget-object v1, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    .line 387
    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->cK:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 386
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    goto :goto_0

    .line 392
    :cond_2
    const/16 v0, 0xb8

    goto :goto_1

    .line 396
    :cond_3
    iget-object v0, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    .line 397
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->eP:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public e(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 405
    invoke-direct {p0}, Laqv;->q()V

    .line 407
    invoke-virtual {p0}, Laqv;->b()V

    .line 408
    invoke-virtual {p0}, Laqv;->h()V

    .line 409
    invoke-virtual {p0}, Laqv;->k()V

    .line 411
    iget-object v1, p0, Laqv;->a:Lapv;

    iget-object v0, p0, Laqv;->a:Lapv;

    invoke-virtual {v0}, Lapv;->c()I

    move-result v0

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lapv;->a(I)V

    .line 413
    invoke-direct {p0, v2, p1}, Laqv;->a(IZ)V

    .line 414
    return-void

    .line 411
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method e()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 827
    iget-object v1, p0, Laqv;->h:Lapk;

    invoke-virtual {v1}, Lapk;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 845
    :cond_0
    :goto_0
    return v0

    .line 831
    :cond_1
    iget-object v1, p0, Laqv;->h:Lapk;

    invoke-virtual {v1}, Lapk;->c()Lapx;

    move-result-object v1

    .line 832
    if-eqz v1, :cond_2

    .line 833
    invoke-virtual {v1}, Lapx;->J()I

    move-result v1

    .line 834
    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_0

    .line 839
    :cond_2
    iget-object v1, p0, Laqv;->a:Lapv;

    invoke-virtual {v1}, Lapv;->j()Z

    move-result v1

    if-nez v1, :cond_0

    .line 842
    iget-boolean v1, p0, Laqv;->l:Z

    if-nez v1, :cond_0

    .line 845
    iget-object v1, p0, Laqv;->h:Lapk;

    invoke-virtual {v1}, Lapk;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Laqv;->u:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method f()V
    .locals 4

    .prologue
    .line 849
    const-string v0, "Babel"

    const-string v1, "restartAutoHideTimerIfDismissible"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 850
    invoke-virtual {p0}, Laqv;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 851
    invoke-virtual {p0}, Laqv;->g()V

    .line 852
    iget-object v0, p0, Laqv;->t:Landroid/os/Handler;

    iget-object v1, p0, Laqv;->w:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 854
    :cond_0
    return-void
.end method

.method g()V
    .locals 2

    .prologue
    .line 857
    const-string v0, "Babel"

    const-string v1, "clearAutoHideTimer"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 858
    iget-object v0, p0, Laqv;->t:Landroid/os/Handler;

    iget-object v1, p0, Laqv;->w:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 859
    return-void
.end method

.method h()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 870
    iget-object v0, p0, Laqv;->C:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 871
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/CameraInterface;->getInstance()Lcom/google/android/libraries/hangouts/video/CameraInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraInterface;->getCurrentCameraId()I

    move-result v2

    .line 872
    new-instance v0, Lapa;

    invoke-direct {v0}, Lapa;-><init>()V

    .line 873
    iget-object v0, p0, Laqv;->C:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 874
    invoke-static {v2}, Lapa;->a(I)I

    move-result v0

    new-instance v3, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v3}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    invoke-static {v0, v3}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    iget v0, v3, Landroid/hardware/Camera$CameraInfo;->facing:I

    packed-switch v0, :pswitch_data_0

    sget v0, Lh;->eL:I

    :goto_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 875
    iget-object v3, p0, Laqv;->C:Landroid/widget/ImageButton;

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 877
    :cond_0
    iget-object v3, p0, Laqv;->C:Landroid/widget/ImageButton;

    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    invoke-static {v2, v0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->cw:I

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 880
    :cond_1
    return-void

    .line 874
    :pswitch_0
    sget v0, Lh;->eN:I

    goto :goto_0

    :pswitch_1
    sget v0, Lh;->eM:I

    goto :goto_0

    .line 877
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->cs:I

    goto :goto_2

    .line 874
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public i()V
    .locals 0

    .prologue
    .line 920
    invoke-virtual {p0}, Laqv;->f()V

    .line 921
    return-void
.end method

.method public j()I
    .locals 1

    .prologue
    .line 939
    iget v0, p0, Laqv;->f:I

    return v0
.end method

.method k()V
    .locals 3

    .prologue
    .line 944
    invoke-direct {p0}, Laqv;->o()V

    .line 947
    iget-object v0, p0, Laqv;->x:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 948
    invoke-direct {p0}, Laqv;->n()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 949
    iget-object v1, p0, Laqv;->x:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 952
    iget-object v0, p0, Laqv;->i:Landroid/view/ViewGroup;

    .line 953
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 954
    iget-boolean v1, p0, Laqv;->v:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    :goto_0
    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 955
    iget-object v1, p0, Laqv;->i:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 956
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 957
    iget-boolean v0, p0, Laqv;->v:Z

    if-eqz v0, :cond_2

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_1
    invoke-virtual {p0, v0}, Laqv;->a(F)V

    .line 959
    :cond_0
    iget-object v1, p0, Laqv;->q:Landroid/view/View;

    iget-object v0, p0, Laqv;->m:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v0, p0, Laqv;->h:Lapk;

    .line 960
    invoke-virtual {v0}, Lapk;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lf;->cl:I

    .line 959
    :goto_2
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 962
    iget-object v0, p0, Laqv;->j:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->e()V

    .line 963
    return-void

    .line 954
    :cond_1
    invoke-direct {p0}, Laqv;->n()I

    move-result v1

    neg-int v1, v1

    goto :goto_0

    .line 957
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 960
    :cond_3
    sget v0, Lf;->cJ:I

    goto :goto_2
.end method

.method public l()V
    .locals 1

    .prologue
    .line 970
    iget v0, p0, Laqv;->f:I

    if-nez v0, :cond_0

    .line 971
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Laqv;->e(Z)V

    .line 975
    :goto_0
    return-void

    .line 973
    :cond_0
    invoke-virtual {p0}, Laqv;->d()V

    goto :goto_0
.end method

.method public m_()V
    .locals 2

    .prologue
    .line 771
    iget-object v0, p0, Laqv;->k:Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;

    if-eqz v0, :cond_0

    .line 772
    iget-object v0, p0, Laqv;->k:Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->b()V

    .line 774
    :cond_0
    iget-object v0, p0, Laqv;->h:Lapk;

    iget-object v1, p0, Laqv;->o:Larm;

    invoke-virtual {v0, v1}, Lapk;->b(Lapo;)V

    .line 775
    invoke-virtual {p0}, Laqv;->g()V

    .line 776
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    .line 711
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    .line 712
    invoke-virtual {p0}, Laqv;->b()V

    .line 721
    :goto_0
    invoke-virtual {p0}, Laqv;->k()V

    .line 722
    return-void

    .line 714
    :cond_0
    iget-object v0, p0, Laqv;->y:Landroid/widget/LinearLayout;

    new-instance v1, Larc;

    invoke-direct {v1, p0}, Larc;-><init>(Laqv;)V

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    goto :goto_0
.end method
