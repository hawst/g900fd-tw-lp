.class public final Ldpo;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldpo;


# instance fields
.field public b:Ldui;

.field public c:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25791
    const/4 v0, 0x0

    new-array v0, v0, [Ldpo;

    sput-object v0, Ldpo;->a:[Ldpo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25792
    invoke-direct {p0}, Lepn;-><init>()V

    .line 25795
    iput-object v0, p0, Ldpo;->b:Ldui;

    .line 25798
    iput-object v0, p0, Ldpo;->c:Ljava/lang/Integer;

    .line 25792
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 25815
    const/4 v0, 0x0

    .line 25816
    iget-object v1, p0, Ldpo;->b:Ldui;

    if-eqz v1, :cond_0

    .line 25817
    const/4 v0, 0x1

    iget-object v1, p0, Ldpo;->b:Ldui;

    .line 25818
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 25820
    :cond_0
    iget-object v1, p0, Ldpo;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 25821
    const/4 v1, 0x2

    iget-object v2, p0, Ldpo;->c:Ljava/lang/Integer;

    .line 25822
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 25824
    :cond_1
    iget-object v1, p0, Ldpo;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25825
    iput v0, p0, Ldpo;->cachedSize:I

    .line 25826
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 25788
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldpo;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldpo;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldpo;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldpo;->b:Ldui;

    if-nez v0, :cond_2

    new-instance v0, Ldui;

    invoke-direct {v0}, Ldui;-><init>()V

    iput-object v0, p0, Ldpo;->b:Ldui;

    :cond_2
    iget-object v0, p0, Ldpo;->b:Ldui;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpo;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpo;->c:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 25803
    iget-object v0, p0, Ldpo;->b:Ldui;

    if-eqz v0, :cond_0

    .line 25804
    const/4 v0, 0x1

    iget-object v1, p0, Ldpo;->b:Ldui;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 25806
    :cond_0
    iget-object v0, p0, Ldpo;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 25807
    const/4 v0, 0x2

    iget-object v1, p0, Ldpo;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 25809
    :cond_1
    iget-object v0, p0, Ldpo;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 25811
    return-void
.end method
