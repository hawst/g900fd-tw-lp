.class public final Ldpy;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldpy;


# instance fields
.field public b:[Ldpw;

.field public c:Ldqz;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23393
    const/4 v0, 0x0

    new-array v0, v0, [Ldpy;

    sput-object v0, Ldpy;->a:[Ldpy;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23394
    invoke-direct {p0}, Lepn;-><init>()V

    .line 23397
    sget-object v0, Ldpw;->a:[Ldpw;

    iput-object v0, p0, Ldpy;->b:[Ldpw;

    .line 23400
    iput-object v1, p0, Ldpy;->c:Ldqz;

    .line 23405
    iput-object v1, p0, Ldpy;->e:Ljava/lang/Integer;

    .line 23394
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 23433
    iget-object v1, p0, Ldpy;->b:[Ldpw;

    if-eqz v1, :cond_1

    .line 23434
    iget-object v2, p0, Ldpy;->b:[Ldpw;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 23435
    if-eqz v4, :cond_0

    .line 23436
    const/4 v5, 0x1

    .line 23437
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 23434
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 23441
    :cond_1
    iget-object v1, p0, Ldpy;->c:Ldqz;

    if-eqz v1, :cond_2

    .line 23442
    const/4 v1, 0x2

    iget-object v2, p0, Ldpy;->c:Ldqz;

    .line 23443
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23445
    :cond_2
    iget-object v1, p0, Ldpy;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 23446
    const/4 v1, 0x3

    iget-object v2, p0, Ldpy;->d:Ljava/lang/Boolean;

    .line 23447
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 23449
    :cond_3
    iget-object v1, p0, Ldpy;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 23450
    const/4 v1, 0x4

    iget-object v2, p0, Ldpy;->e:Ljava/lang/Integer;

    .line 23451
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 23453
    :cond_4
    iget-object v1, p0, Ldpy;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23454
    iput v0, p0, Ldpy;->cachedSize:I

    .line 23455
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 23390
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldpy;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldpy;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldpy;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldpy;->b:[Ldpw;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldpw;

    iget-object v3, p0, Ldpy;->b:[Ldpw;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldpy;->b:[Ldpw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldpy;->b:[Ldpw;

    :goto_2
    iget-object v2, p0, Ldpy;->b:[Ldpw;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldpy;->b:[Ldpw;

    new-instance v3, Ldpw;

    invoke-direct {v3}, Ldpw;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldpy;->b:[Ldpw;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldpy;->b:[Ldpw;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldpy;->b:[Ldpw;

    new-instance v3, Ldpw;

    invoke-direct {v3}, Ldpw;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldpy;->b:[Ldpw;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldpy;->c:Ldqz;

    if-nez v0, :cond_5

    new-instance v0, Ldqz;

    invoke-direct {v0}, Ldqz;-><init>()V

    iput-object v0, p0, Ldpy;->c:Ldqz;

    :cond_5
    iget-object v0, p0, Ldpy;->c:Ldqz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldpy;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_6

    const/4 v2, 0x1

    if-eq v0, v2, :cond_6

    const/4 v2, 0x2

    if-ne v0, v2, :cond_7

    :cond_6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpy;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpy;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 23410
    iget-object v0, p0, Ldpy;->b:[Ldpw;

    if-eqz v0, :cond_1

    .line 23411
    iget-object v1, p0, Ldpy;->b:[Ldpw;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 23412
    if-eqz v3, :cond_0

    .line 23413
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 23411
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 23417
    :cond_1
    iget-object v0, p0, Ldpy;->c:Ldqz;

    if-eqz v0, :cond_2

    .line 23418
    const/4 v0, 0x2

    iget-object v1, p0, Ldpy;->c:Ldqz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 23420
    :cond_2
    iget-object v0, p0, Ldpy;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 23421
    const/4 v0, 0x3

    iget-object v1, p0, Ldpy;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 23423
    :cond_3
    iget-object v0, p0, Ldpy;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 23424
    const/4 v0, 0x4

    iget-object v1, p0, Ldpy;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 23426
    :cond_4
    iget-object v0, p0, Ldpy;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 23428
    return-void
.end method
