.class public abstract Lbtf;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static volatile D:Ljava/lang/Boolean;

.field private static final e:Z


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Z

.field protected final a:Lbtz;

.field protected final b:Landroid/content/res/Resources;

.field protected c:Lyj;

.field protected d:Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;

.field private final f:Lbme;

.field private final g:Landroid/app/Activity;

.field private final h:Ljava/lang/String;

.field private i:Lcom/google/android/apps/hangouts/phone/AvatarPreference;

.field private j:Landroid/preference/Preference;

.field private k:Lbnb;

.field private l:Landroid/preference/Preference;

.field private m:Landroid/preference/Preference;

.field private n:Landroid/preference/PreferenceCategory;

.field private o:Landroid/preference/PreferenceCategory;

.field private p:Landroid/preference/PreferenceCategory;

.field private q:Landroid/preference/Preference;

.field private r:Landroid/preference/Preference;

.field private s:Landroid/preference/Preference;

.field private t:Landroid/preference/Preference;

.field private u:Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

.field private v:Z

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lbys;->m:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbtf;->e:Z

    .line 1358
    const/4 v0, 0x0

    sput-object v0, Lbtf;->D:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lbtz;Landroid/app/Activity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    invoke-static {p1}, Lcwz;->b(Ljava/lang/Object;)V

    .line 147
    invoke-static {p2}, Lcwz;->b(Ljava/lang/Object;)V

    .line 149
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbme;

    invoke-static {v0, v1}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    iput-object v0, p0, Lbtf;->f:Lbme;

    .line 151
    iput-object p1, p0, Lbtf;->a:Lbtz;

    .line 152
    iput-object p2, p0, Lbtf;->g:Landroid/app/Activity;

    .line 153
    iget-object v0, p0, Lbtf;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    .line 154
    iput-object p3, p0, Lbtf;->h:Ljava/lang/String;

    .line 155
    iget-object v0, p0, Lbtf;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 156
    invoke-static {p3}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    iput-object v0, p0, Lbtf;->c:Lyj;

    .line 158
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbtf;->C:Z

    .line 159
    return-void
.end method

.method static synthetic a(Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .prologue
    .line 71
    sput-object p0, Lbtf;->D:Ljava/lang/Boolean;

    return-object p0
.end method

.method private static a(Ljava/util/List;Lyj;)Lyj;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lyj;",
            ")",
            "Lyj;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1298
    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v4

    .line 1299
    const/4 v3, 0x0

    .line 1301
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 1302
    const-string v0, "Babel"

    const-string v1, "Signed in account list should have at least two items"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move v1, v2

    .line 1305
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1306
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1307
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_3

    .line 1308
    add-int/lit8 v0, v1, 0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1314
    :goto_1
    if-nez v0, :cond_1

    .line 1315
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1318
    :cond_1
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    return-object v0

    .line 1305
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    move-object v0, v3

    goto :goto_1
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 1283
    const/4 v0, 0x0

    invoke-static {v0}, Lbbl;->b(Lyj;)Landroid/content/Intent;

    move-result-object v0

    .line 1285
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 1286
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setResult(I)V

    .line 1287
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 1288
    return-void
.end method

.method static synthetic a(Lbtf;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lbtf;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lbtf;)Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbtf;->C:Z

    return v0
.end method

.method public static a(Lyj;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1386
    const-string v1, "babel_device_presence"

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1389
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    .line 1390
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1393
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lf;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1392
    invoke-virtual {v1, v3, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1395
    sget v1, Lh;->kX:I

    .line 1396
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1397
    sget v3, Lf;->bS:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    .line 1399
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1401
    :cond_0
    return v0
.end method

.method static synthetic b(Lbtf;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lbtf;->g:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic b(Lbtf;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lbtf;->A:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v1, Lh;->lz:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lbtf;->g:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lh;->lK:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lh;->lJ:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :cond_1
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 582
    iget-object v0, p0, Lbtf;->u:Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

    if-nez v0, :cond_0

    .line 616
    :goto_0
    return-void

    .line 586
    :cond_0
    iget-boolean v0, p0, Lbtf;->z:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbtf;->A:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 587
    iget-object v0, p0, Lbtf;->B:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 588
    iget-object v0, p0, Lbtf;->u:Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lh;->lB:I

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lbtf;->B:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 592
    :cond_1
    iget-object v0, p0, Lbtf;->u:Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lh;->lA:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 595
    :cond_2
    iget-boolean v0, p0, Lbtf;->v:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lbtf;->x:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 597
    iget-object v0, p0, Lbtf;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lbtf;->y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 598
    iget-object v0, p0, Lbtf;->u:Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lh;->lF:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lbtf;->w:Ljava/lang/String;

    aput-object v4, v3, v5

    iget-object v4, p0, Lbtf;->y:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 601
    :cond_3
    iget-object v0, p0, Lbtf;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 602
    iget-object v0, p0, Lbtf;->u:Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lh;->lE:I

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lbtf;->w:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 605
    :cond_4
    iget-object v0, p0, Lbtf;->y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 606
    iget-object v0, p0, Lbtf;->u:Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lh;->lG:I

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lbtf;->y:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 610
    :cond_5
    iget-object v0, p0, Lbtf;->u:Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lh;->lD:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 614
    :cond_6
    iget-object v0, p0, Lbtf;->u:Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

    sget v1, Lh;->lx:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;->setSummary(I)V

    goto/16 :goto_0
.end method

.method static synthetic c(Lbtf;)Landroid/preference/Preference;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lbtf;->s:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic d(Lbtf;)Landroid/preference/Preference;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lbtf;->t:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic e(Lbtf;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 71
    iget-object v0, p0, Lbtf;->j:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbtf;->k:Lbnb;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbtf;->k:Lbnb;

    iget v0, v0, Lbnb;->a:I

    const/16 v1, 0x66

    if-eq v0, v1, :cond_0

    invoke-static {}, Lbmz;->a()Lbmz;

    move-result-object v0

    invoke-virtual {v0}, Lbmz;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbtf;->g:Landroid/app/Activity;

    sget v1, Lh;->iw:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lbtf;->c:Lyj;

    invoke-static {v0, v2}, Lbbl;->a(Lyj;Z)Landroid/content/Intent;

    move-result-object v0

    const v1, 0x4008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lbtf;->a:Lbtz;

    invoke-interface {v1, v0}, Lbtz;->a(Landroid/content/Intent;)V

    iget-object v0, p0, Lbtf;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method static synthetic f(Lbtf;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 71
    iget-object v2, p0, Lbtf;->c:Lyj;

    invoke-static {v2}, Lbkb;->l(Lyj;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->c(Z)V

    invoke-static {v0}, Lbkb;->f(Z)Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v0, :cond_2

    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v3

    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "currentAccount "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mAccount "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lbtf;->c:Lyj;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lbtf;->c:Lyj;

    invoke-static {v4, v1}, Lbkb;->b(Lyj;Z)V

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lbtf;->c:Lyj;

    invoke-virtual {v4}, Lyj;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v2, v3}, Lbtf;->a(Ljava/util/List;Lyj;)Lyj;

    move-result-object v1

    invoke-static {v1}, Lbkb;->e(Lyj;)V

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lbtf;->g:Landroid/app/Activity;

    invoke-static {v0}, Lbtf;->a(Landroid/app/Activity;)V

    :cond_1
    return-void

    :cond_2
    invoke-static {}, Lbkb;->m()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lbtf;->c:Lyj;

    invoke-static {v2, v1}, Lbkb;->b(Lyj;Z)V

    invoke-static {}, Lbkb;->p()Lyj;

    move-result-object v1

    invoke-static {v1}, Lbkb;->e(Lyj;)V

    goto :goto_0

    :cond_3
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lbtf;->g:Landroid/app/Activity;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->fT:I

    new-array v5, v0, [Ljava/lang/Object;

    iget-object v6, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v7, Lh;->q:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->fS:I

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v5, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v6, Lh;->q:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v1

    invoke-virtual {v3, v4, v0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    sget v0, Lh;->ab:I

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    sget v0, Lh;->hZ:I

    new-instance v3, Lbtq;

    invoke-direct {v3, p0}, Lbtq;-><init>(Lbtf;)V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    move v0, v1

    goto :goto_0
.end method

.method static synthetic g(Lbtf;)Lbme;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lbtf;->f:Lbme;

    return-object v0
.end method

.method public static j()V
    .locals 1

    .prologue
    .line 1361
    const/4 v0, 0x0

    sput-object v0, Lbtf;->D:Ljava/lang/Boolean;

    .line 1362
    return-void
.end method

.method public static k()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1365
    sget-object v2, Lbtf;->D:Ljava/lang/Boolean;

    if-nez v2, :cond_0

    .line 1366
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    .line 1367
    const-string v3, "smsmms"

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 1370
    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1371
    sget v4, Lh;->cy:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1373
    sget v5, Lf;->bL:I

    .line 1374
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    .line 1376
    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "babel_enable_merged_conversations"

    .line 1377
    invoke-static {v2, v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1376
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lbtf;->D:Ljava/lang/Boolean;

    .line 1381
    :cond_0
    sget-object v0, Lbtf;->D:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    return v0

    :cond_1
    move v0, v1

    .line 1377
    goto :goto_0
.end method

.method static synthetic l()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lbtf;->D:Ljava/lang/Boolean;

    return-object v0
.end method

.method private m()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 465
    iget-object v0, p0, Lbtf;->u:Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

    if-nez v0, :cond_0

    .line 562
    :goto_0
    return-void

    .line 471
    :cond_0
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v0

    .line 472
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lyj;->X()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 473
    iput-boolean v7, p0, Lbtf;->v:Z

    .line 475
    invoke-static {}, Lbzd;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbtf;->w:Ljava/lang/String;

    .line 476
    invoke-static {}, Lbzd;->g()Ljava/lang/String;

    move-result-object v1

    .line 477
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 478
    invoke-static {v1}, Lbzd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lbtf;->x:Ljava/lang/String;

    .line 479
    invoke-static {v1}, Lbzd;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbtf;->y:Ljava/lang/String;

    .line 494
    :goto_1
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lyj;->N()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 495
    iput-boolean v7, p0, Lbtf;->z:Z

    .line 496
    invoke-virtual {v0}, Lyj;->J()Ljava/lang/String;

    move-result-object v0

    .line 500
    if-eqz v0, :cond_5

    .line 501
    invoke-static {v0}, Lbzd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbtf;->A:Ljava/lang/String;

    .line 502
    invoke-static {v0}, Lbzd;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbtf;->B:Ljava/lang/String;

    .line 516
    :goto_2
    iget-object v0, p0, Lbtf;->u:Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 517
    if-eqz v0, :cond_1

    iget-object v1, p0, Lbtf;->A:Ljava/lang/String;

    .line 518
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lbtf;->x:Ljava/lang/String;

    .line 519
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 520
    :cond_1
    iget-object v0, p0, Lbtf;->u:Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lh;->lz:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;->setValue(Ljava/lang/String;)V

    .line 523
    :cond_2
    iget-boolean v0, p0, Lbtf;->v:Z

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lbtf;->z:Z

    if-eqz v0, :cond_a

    .line 525
    iget-object v0, p0, Lbtf;->n:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lbtf;->u:Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 528
    iget-object v1, p0, Lbtf;->u:Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

    new-array v2, v9, [Ljava/lang/String;

    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v3, Lh;->ly:I

    .line 529
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v3, Lh;->lA:I

    .line 530
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v7

    iget-object v0, p0, Lbtf;->w:Ljava/lang/String;

    .line 531
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v3, Lh;->lD:I

    .line 532
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 533
    :goto_3
    aput-object v0, v2, v8

    .line 528
    invoke-virtual {v1, v2}, Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 538
    iget-object v1, p0, Lbtf;->u:Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

    new-array v2, v9, [Ljava/lang/String;

    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v3, Lh;->lx:I

    .line 539
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    iget-object v0, p0, Lbtf;->B:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lbtf;->B:Ljava/lang/String;

    .line 541
    :goto_4
    aput-object v0, v2, v7

    iget-object v0, p0, Lbtf;->y:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lbtf;->y:Ljava/lang/String;

    .line 543
    :goto_5
    aput-object v0, v2, v8

    .line 538
    invoke-virtual {v1, v2}, Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;->a([Ljava/lang/CharSequence;)V

    .line 546
    iget-object v0, p0, Lbtf;->A:Ljava/lang/String;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 547
    iget-object v0, p0, Lbtf;->x:Ljava/lang/String;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 550
    iget-object v0, p0, Lbtf;->u:Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

    new-array v1, v9, [Ljava/lang/String;

    iget-object v2, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v3, Lh;->lz:I

    .line 551
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    iget-object v2, p0, Lbtf;->A:Ljava/lang/String;

    aput-object v2, v1, v7

    iget-object v2, p0, Lbtf;->x:Ljava/lang/String;

    aput-object v2, v1, v8

    .line 550
    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 557
    iget-object v0, p0, Lbtf;->u:Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbtf;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 482
    :cond_3
    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lh;->lI:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbtf;->x:Ljava/lang/String;

    .line 483
    iput-object v3, p0, Lbtf;->y:Ljava/lang/String;

    goto/16 :goto_1

    .line 486
    :cond_4
    iput-boolean v6, p0, Lbtf;->v:Z

    .line 487
    iput-object v3, p0, Lbtf;->w:Ljava/lang/String;

    .line 488
    iput-object v3, p0, Lbtf;->x:Ljava/lang/String;

    .line 489
    iput-object v3, p0, Lbtf;->y:Ljava/lang/String;

    goto/16 :goto_1

    .line 505
    :cond_5
    iput-boolean v6, p0, Lbtf;->z:Z

    .line 506
    iput-object v3, p0, Lbtf;->A:Ljava/lang/String;

    .line 507
    iput-object v3, p0, Lbtf;->B:Ljava/lang/String;

    goto/16 :goto_2

    .line 510
    :cond_6
    iput-boolean v6, p0, Lbtf;->z:Z

    .line 511
    iput-object v3, p0, Lbtf;->A:Ljava/lang/String;

    .line 512
    iput-object v3, p0, Lbtf;->B:Ljava/lang/String;

    goto/16 :goto_2

    .line 532
    :cond_7
    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v3, Lh;->lE:I

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lbtf;->w:Ljava/lang/String;

    aput-object v5, v4, v6

    .line 533
    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 539
    :cond_8
    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v3, Lh;->lH:I

    .line 541
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_9
    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v3, Lh;->lH:I

    .line 543
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 560
    :cond_a
    iget-object v0, p0, Lbtf;->n:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lbtf;->u:Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0
.end method

.method private n()V
    .locals 3

    .prologue
    .line 1049
    iget-object v0, p0, Lbtf;->i:Lcom/google/android/apps/hangouts/phone/AvatarPreference;

    if-nez v0, :cond_0

    .line 1050
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lh;->dj:I

    .line 1051
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1050
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/phone/AvatarPreference;

    iput-object v0, p0, Lbtf;->i:Lcom/google/android/apps/hangouts/phone/AvatarPreference;

    .line 1053
    :cond_0
    iget-object v0, p0, Lbtf;->i:Lcom/google/android/apps/hangouts/phone/AvatarPreference;

    if-nez v0, :cond_1

    .line 1062
    :goto_0
    return-void

    .line 1056
    :cond_1
    iget-object v0, p0, Lbtf;->c:Lyj;

    invoke-virtual {v0}, Lyj;->F()Ljava/lang/String;

    move-result-object v0

    .line 1057
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1059
    iget-object v1, p0, Lbtf;->i:Lcom/google/android/apps/hangouts/phone/AvatarPreference;

    sget v2, Lh;->di:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/hangouts/phone/AvatarPreference;->setSummary(I)V

    .line 1061
    :cond_2
    iget-object v1, p0, Lbtf;->i:Lcom/google/android/apps/hangouts/phone/AvatarPreference;

    iget-object v2, p0, Lbtf;->c:Lyj;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/hangouts/phone/AvatarPreference;->a(Ljava/lang/String;Lyj;)V

    goto :goto_0
.end method

.method private o()Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1112
    invoke-static {}, Lbmz;->b()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 1137
    :goto_0
    return v0

    .line 1116
    :cond_0
    iget-object v0, p0, Lbtf;->j:Landroid/preference/Preference;

    if-nez v0, :cond_1

    move v0, v2

    .line 1117
    goto :goto_0

    .line 1120
    :cond_1
    iget-object v0, p0, Lbtf;->c:Lyj;

    invoke-static {v0}, Lbkb;->f(Lyj;)I

    move-result v0

    const/16 v4, 0x66

    if-eq v0, v4, :cond_2

    move v0, v2

    .line 1122
    goto :goto_0

    .line 1125
    :cond_2
    invoke-static {}, Lbmz;->a()Lbmz;

    move-result-object v0

    .line 1127
    invoke-static {}, Lbmz;->c()Z

    move-result v4

    if-nez v4, :cond_3

    move v0, v2

    .line 1129
    goto :goto_0

    .line 1132
    :cond_3
    iget-object v4, p0, Lbtf;->c:Lyj;

    invoke-virtual {v0, v4}, Lbmz;->a(Lyj;)Lbnb;

    move-result-object v0

    iput-object v0, p0, Lbtf;->k:Lbnb;

    .line 1134
    iget-object v4, p0, Lbtf;->j:Landroid/preference/Preference;

    iget-object v0, p0, Lbtf;->k:Lbnb;

    iget v0, v0, Lbnb;->a:I

    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1135
    iget-object v0, p0, Lbtf;->j:Landroid/preference/Preference;

    iget-object v4, p0, Lbtf;->k:Lbnb;

    iget-object v5, v4, Lbnb;->b:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    iget v4, v4, Lbnb;->a:I

    packed-switch v4, :pswitch_data_1

    :cond_4
    :goto_2
    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    move v0, v3

    .line 1137
    goto :goto_0

    .line 1134
    :pswitch_0
    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v5, Lh;->iB:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v5, Lh;->iC:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v5, Lh;->iD:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1135
    :pswitch_3
    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->iy:I

    new-array v6, v3, [Ljava/lang/Object;

    aput-object v5, v6, v2

    invoke-virtual {v1, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :pswitch_4
    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->iz:I

    new-array v6, v3, [Ljava/lang/Object;

    aput-object v5, v6, v2

    invoke-virtual {v1, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :pswitch_5
    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->iA:I

    new-array v6, v3, [Ljava/lang/Object;

    aput-object v5, v6, v2

    invoke-virtual {v1, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 1134
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 1135
    :pswitch_data_1
    .packed-switch 0x64
        :pswitch_4
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method private p()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1323
    const/4 v1, 0x1

    .line 1325
    invoke-static {}, Lbvm;->a()Lsm;

    move-result-object v2

    invoke-virtual {v2}, Lsm;->w()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1339
    :cond_0
    :goto_0
    return v0

    .line 1330
    :cond_1
    :try_start_0
    iget-object v2, p0, Lbtf;->g:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 1331
    const-string v3, "com.android.cellbroadcastreceiver"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 1336
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(IZLjava/lang/Integer;IIILandroid/content/SharedPreferences;)Landroid/preference/Preference;
.end method

.method public a()Lyj;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lbtf;->c:Lyj;

    return-object v0
.end method

.method public a(I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 197
    iget-object v0, p0, Lbtf;->a:Lbtz;

    const-string v1, "smsmms"

    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lbtf;->a:Lbtz;

    invoke-interface {v0, p1}, Lbtz;->a(I)V

    .line 200
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->lU:I

    .line 201
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 200
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lbtf;->n:Landroid/preference/PreferenceCategory;

    .line 202
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->lW:I

    .line 203
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 202
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lbtf;->o:Landroid/preference/PreferenceCategory;

    .line 204
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->lT:I

    .line 205
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 204
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lbtf;->p:Landroid/preference/PreferenceCategory;

    .line 206
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->lP:I

    .line 207
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 206
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lbtf;->l:Landroid/preference/Preference;

    .line 208
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->lQ:I

    .line 209
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 208
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lbtf;->m:Landroid/preference/Preference;

    .line 211
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->cz:I

    .line 212
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 211
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 213
    iget-object v1, p0, Lbtf;->a:Lbtz;

    iget-object v4, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v5, Lh;->cy:I

    .line 214
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 213
    invoke-interface {v1, v4}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 216
    const-string v4, "babel_enable_merged_conversations"

    invoke-static {v4, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 218
    new-instance v4, Lbtg;

    invoke-direct {v4, p0}, Lbtg;-><init>(Lbtf;)V

    invoke-virtual {v1, v4}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 238
    :goto_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-lt v1, v4, :cond_6

    .line 240
    iget-object v1, p0, Lbtf;->n:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 263
    :goto_1
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->lk:I

    .line 264
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 263
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 265
    if-eqz v0, :cond_0

    .line 266
    invoke-static {v3, v3, v2}, Lbkb;->a(ZZZ)Ljava/util/List;

    move-result-object v1

    .line 267
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_7

    .line 269
    iget-object v1, p0, Lbtf;->n:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 276
    :cond_0
    :goto_2
    invoke-static {}, Lbvm;->a()Lsm;

    move-result-object v0

    invoke-virtual {v0}, Lsm;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lbzd;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 277
    :cond_1
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->ds:I

    .line 278
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 277
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 279
    iget-object v1, p0, Lbtf;->p:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 283
    :cond_2
    invoke-static {}, Lbvm;->a()Lsm;

    move-result-object v0

    invoke-virtual {v0}, Lsm;->r()Z

    move-result v0

    if-nez v0, :cond_3

    .line 284
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->lo:I

    .line 285
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 284
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 286
    iget-object v1, p0, Lbtf;->p:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 290
    :cond_3
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->oX:I

    .line 291
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 290
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 292
    invoke-direct {p0}, Lbtf;->p()Z

    move-result v1

    if-nez v1, :cond_8

    .line 293
    iget-object v1, p0, Lbtf;->p:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 317
    :goto_3
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->ln:I

    .line 318
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 317
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 320
    invoke-static {}, Lbvx;->g()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-static {}, Lxw;->b()Z

    move-result v1

    if-nez v1, :cond_9

    .line 325
    iget-object v1, p0, Lbtf;->p:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 332
    :goto_4
    iget-object v0, p0, Lbtf;->g:Landroid/app/Activity;

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    .line 335
    if-eqz v0, :cond_a

    move v1, v2

    .line 336
    :goto_5
    if-eqz v1, :cond_10

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_10

    .line 337
    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    .line 339
    :goto_6
    iget-object v1, p0, Lbtf;->a:Lbtz;

    iget-object v4, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v5, Lh;->lu:I

    .line 340
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 339
    invoke-interface {v1, v4}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 341
    if-nez v0, :cond_4

    .line 342
    iget-object v0, p0, Lbtf;->n:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 345
    :cond_4
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->lt:I

    .line 347
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 346
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;

    .line 348
    new-instance v1, Lbtt;

    invoke-direct {v1, p0, v0}, Lbtt;-><init>(Lbtf;Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 359
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v1

    .line 358
    invoke-static {v1, v3}, Lbne;->b(Lyj;I)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->a(Landroid/net/Uri;)V

    .line 363
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->nJ:I

    .line 364
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 363
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 365
    invoke-static {}, Lf;->w()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 366
    invoke-static {}, Lbvx;->g()Z

    move-result v1

    if-nez v1, :cond_b

    move v1, v2

    :goto_7
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 367
    new-instance v1, Lbtu;

    invoke-direct {v1, p0}, Lbtu;-><init>(Lbtf;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 383
    :goto_8
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v3, Lh;->lC:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

    iput-object v0, p0, Lbtf;->u:Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

    const-string v0, "babel_gv_sms"

    invoke-static {v0, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lbtf;->n:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lbtf;->u:Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iput-object v6, p0, Lbtf;->u:Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

    .line 386
    :goto_9
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lh;->M:I

    .line 387
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 386
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 388
    new-instance v1, Lbtv;

    invoke-direct {v1, p0}, Lbtv;-><init>(Lbtf;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 401
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lh;->cm:I

    .line 402
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 401
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 403
    invoke-static {}, Lf;->w()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 404
    new-instance v1, Lbtw;

    invoke-direct {v1, p0}, Lbtw;-><init>(Lbtf;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 414
    :goto_a
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lh;->cl:I

    .line 415
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 414
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 416
    invoke-static {}, Lf;->w()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 417
    new-instance v1, Lbtx;

    invoke-direct {v1, p0}, Lbtx;-><init>(Lbtf;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 427
    :goto_b
    return-void

    .line 235
    :cond_5
    iget-object v4, p0, Lbtf;->n:Landroid/preference/PreferenceCategory;

    invoke-virtual {v4, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    .line 243
    :cond_6
    iget-object v1, p0, Lbtf;->n:Landroid/preference/PreferenceCategory;

    iget-object v4, p0, Lbtf;->l:Landroid/preference/Preference;

    invoke-virtual {v1, v4}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 244
    iput-object v6, p0, Lbtf;->l:Landroid/preference/Preference;

    .line 245
    iget-object v1, p0, Lbtf;->n:Landroid/preference/PreferenceCategory;

    iget-object v4, p0, Lbtf;->m:Landroid/preference/Preference;

    invoke-virtual {v1, v4}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 246
    iput-object v6, p0, Lbtf;->m:Landroid/preference/Preference;

    .line 249
    new-instance v1, Lbtr;

    invoke-direct {v1, p0}, Lbtr;-><init>(Lbtf;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto/16 :goto_1

    .line 271
    :cond_7
    invoke-static {}, Lbbl;->k()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 295
    :cond_8
    new-instance v1, Lbts;

    invoke-direct {v1, p0}, Lbts;-><init>(Lbtf;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto/16 :goto_3

    .line 327
    :cond_9
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->ln:I

    .line 328
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 327
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 329
    invoke-static {}, Lbbl;->j()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setIntent(Landroid/content/Intent;)V

    goto/16 :goto_4

    :cond_a
    move v1, v3

    .line 335
    goto/16 :goto_5

    :cond_b
    move v1, v3

    .line 366
    goto/16 :goto_7

    .line 379
    :cond_c
    iget-object v1, p0, Lbtf;->p:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_8

    .line 383
    :cond_d
    invoke-direct {p0}, Lbtf;->m()V

    iget-object v0, p0, Lbtf;->u:Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;

    new-instance v1, Lbty;

    invoke-direct {v1, p0}, Lbty;-><init>(Lbtf;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto/16 :goto_9

    .line 412
    :cond_e
    iget-object v1, p0, Lbtf;->p:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_a

    .line 425
    :cond_f
    iget-object v1, p0, Lbtf;->p:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_b

    :cond_10
    move v0, v1

    goto/16 :goto_6
.end method

.method protected a(Landroid/preference/Preference;Landroid/content/SharedPreferences;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 669
    const/4 v0, 0x1

    .line 672
    if-eqz p4, :cond_0

    .line 673
    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v1, Lf;->bR:I

    .line 674
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 673
    invoke-interface {p2, p4, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 676
    :cond_0
    const-string v1, "ANY_RINGTONE_NOT_SILENT"

    invoke-interface {p2, p5, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 677
    iget-object v2, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v3, Lf;->bR:I

    .line 678
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    .line 677
    invoke-interface {p2, p6, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 680
    sget-boolean v3, Lbtf;->e:Z

    if-eqz v3, :cond_1

    .line 681
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Notification Screen init key="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/preference/Preference;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " isEnabled="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " current: notifications="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ringtone="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " vibrate="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    :cond_1
    if-nez v0, :cond_2

    .line 690
    sget v0, Lh;->hT:I

    .line 700
    :goto_0
    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(I)V

    .line 701
    return-void

    .line 691
    :cond_2
    if-eqz v1, :cond_3

    if-eqz v2, :cond_3

    .line 692
    sget v0, Lh;->iH:I

    goto :goto_0

    .line 693
    :cond_3
    if-eqz v1, :cond_4

    .line 694
    sget v0, Lh;->iI:I

    goto :goto_0

    .line 695
    :cond_4
    if-eqz v2, :cond_5

    .line 696
    sget v0, Lh;->op:I

    goto :goto_0

    .line 698
    :cond_5
    sget v0, Lh;->kZ:I

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1041
    iget-object v0, p0, Lbtf;->c:Lyj;

    invoke-static {v0, p1}, Lym;->a(Lyj;Ljava/lang/String;)V

    .line 1042
    invoke-direct {p0}, Lbtf;->n()V

    .line 1043
    return-void
.end method

.method public b()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 170
    iget-object v1, p0, Lbtf;->h:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 186
    :cond_0
    :goto_0
    return v0

    .line 176
    :cond_1
    iget-object v1, p0, Lbtf;->h:Ljava/lang/String;

    invoke-static {v1}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v1

    iput-object v1, p0, Lbtf;->c:Lyj;

    .line 177
    iget-object v1, p0, Lbtf;->c:Lyj;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lbtf;->c:Lyj;

    invoke-static {v1}, Lbkb;->f(Lyj;)I

    move-result v1

    const/16 v2, 0x66

    if-eq v1, v2, :cond_0

    .line 181
    :cond_2
    const-string v0, "Babel"

    const-string v1, "Settings page resumed with invalid account. Go to Babel home"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    iget-object v0, p0, Lbtf;->g:Landroid/app/Activity;

    invoke-static {v0}, Lbtf;->a(Landroid/app/Activity;)V

    .line 183
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 190
    iget-boolean v0, p0, Lbtf;->C:Z

    return v0
.end method

.method public d()V
    .locals 4

    .prologue
    .line 622
    invoke-static {}, Lbkb;->m()Z

    move-result v1

    .line 623
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v2, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v3, Lh;->lk:I

    .line 624
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 623
    invoke-interface {v0, v2}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 625
    if-eqz v0, :cond_0

    .line 627
    invoke-static {}, Lbkb;->o()Lyj;

    move-result-object v2

    .line 628
    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 629
    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 631
    :cond_0
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v2, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v3, Lh;->lt:I

    .line 632
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 631
    invoke-interface {v0, v2}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/RingtonePreference;

    .line 633
    if-eqz v0, :cond_1

    .line 634
    invoke-virtual {v0, v1}, Landroid/preference/RingtonePreference;->setEnabled(Z)V

    .line 636
    :cond_1
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v2, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v3, Lh;->lu:I

    .line 637
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 636
    invoke-interface {v0, v2}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 638
    if-eqz v0, :cond_2

    .line 639
    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 643
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v0, v2, :cond_3

    .line 644
    invoke-static {}, Lbkb;->m()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 645
    iget-object v0, p0, Lbtf;->n:Landroid/preference/PreferenceCategory;

    iget-object v2, p0, Lbtf;->m:Landroid/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 646
    iget-object v0, p0, Lbtf;->n:Landroid/preference/PreferenceCategory;

    iget-object v2, p0, Lbtf;->l:Landroid/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 651
    :goto_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->e()V

    .line 655
    :cond_3
    iget-object v0, p0, Lbtf;->o:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setEnabled(Z)V

    .line 656
    iget-object v0, p0, Lbtf;->p:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setEnabled(Z)V

    .line 659
    invoke-direct {p0}, Lbtf;->m()V

    .line 660
    return-void

    .line 648
    :cond_4
    iget-object v0, p0, Lbtf;->n:Landroid/preference/PreferenceCategory;

    iget-object v2, p0, Lbtf;->m:Landroid/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 649
    iget-object v0, p0, Lbtf;->n:Landroid/preference/PreferenceCategory;

    iget-object v2, p0, Lbtf;->l:Landroid/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method public e()V
    .locals 13

    .prologue
    .line 708
    iget-object v0, p0, Lbtf;->c:Lyj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbtf;->c:Lyj;

    invoke-virtual {v0}, Lyj;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1031
    :cond_0
    :goto_0
    return-void

    .line 712
    :cond_1
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->c:Lyj;

    .line 713
    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lf;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 712
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/String;)V

    .line 717
    iget-object v0, p0, Lbtf;->g:Landroid/app/Activity;

    iget-object v1, p0, Lbtf;->c:Lyj;

    .line 718
    invoke-static {v0, v1}, Lf;->b(Landroid/content/Context;Lyj;)Z

    move-result v9

    .line 721
    iget-object v0, p0, Lbtf;->a:Lbtz;

    sget v1, Lf;->ib:I

    invoke-interface {v0, v1}, Lbtz;->a(I)V

    .line 723
    iget-object v0, p0, Lbtf;->a:Lbtz;

    invoke-interface {v0}, Lbtz;->a()Landroid/preference/PreferenceScreen;

    move-result-object v10

    .line 724
    iget-object v0, p0, Lbtf;->c:Lyj;

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf;->k(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 728
    sget v1, Lh;->ap:I

    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lf;->bF:I

    .line 730
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    sget v0, Lh;->al:I

    .line 731
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget v4, Lh;->am:I

    const/4 v5, 0x2

    sget v6, Lh;->ao:I

    move-object v0, p0

    .line 728
    invoke-virtual/range {v0 .. v7}, Lbtf;->a(IZLjava/lang/Integer;IIILandroid/content/SharedPreferences;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lbtf;->q:Landroid/preference/Preference;

    .line 738
    sget v1, Lh;->ov:I

    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lf;->bO:I

    .line 740
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    sget v0, Lh;->eX:I

    .line 741
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget v4, Lh;->fq:I

    const/4 v5, 0x1

    sget v6, Lh;->fs:I

    move-object v0, p0

    .line 738
    invoke-virtual/range {v0 .. v7}, Lbtf;->a(IZLjava/lang/Integer;IIILandroid/content/SharedPreferences;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lbtf;->r:Landroid/preference/Preference;

    .line 747
    const-string v0, "babel_gv_sms"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 751
    iget-object v1, p0, Lbtf;->c:Lyj;

    invoke-virtual {v1}, Lyj;->N()Z

    move-result v2

    .line 752
    if-eqz v0, :cond_5

    iget-object v0, p0, Lbtf;->c:Lyj;

    .line 753
    invoke-virtual {v0}, Lyj;->K()Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz v2, :cond_5

    :cond_2
    const/4 v0, 0x1

    move v8, v0

    .line 755
    :goto_1
    sget v1, Lh;->dt:I

    const/4 v3, 0x0

    sget v4, Lh;->dv:I

    const/4 v5, 0x2

    sget v6, Lh;->dw:I

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Lbtf;->a(IZLjava/lang/Integer;IIILandroid/content/SharedPreferences;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lbtf;->s:Landroid/preference/Preference;

    .line 764
    sget v1, Lh;->dx:I

    const/4 v3, 0x0

    sget v4, Lh;->dy:I

    const/4 v5, 0x2

    sget v6, Lh;->dz:I

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Lbtf;->a(IZLjava/lang/Integer;IIILandroid/content/SharedPreferences;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lbtf;->t:Landroid/preference/Preference;

    .line 774
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v3, Lh;->dr:I

    .line 775
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 774
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 778
    iget-object v1, p0, Lbtf;->a:Lbtz;

    iget-object v3, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->dp:I

    .line 779
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 778
    invoke-interface {v1, v3}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    .line 782
    iget-object v3, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->dn:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 784
    iget-object v4, p0, Lbtf;->a:Lbtz;

    .line 785
    invoke-interface {v4, v3}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    .line 790
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lbtf;->c:Lyj;

    invoke-static {v4, v5}, Lf;->a(Landroid/content/Context;Lyj;)Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lbtf;->c:Lyj;

    .line 791
    invoke-virtual {v4}, Lyj;->H()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x1

    move v5, v4

    .line 793
    :goto_2
    if-nez v8, :cond_7

    if-nez v5, :cond_7

    .line 794
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 795
    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 796
    const/4 v1, 0x1

    iput-boolean v1, p0, Lbtf;->C:Z

    .line 797
    invoke-virtual {v10, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 847
    :goto_3
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xf

    if-ge v1, v3, :cond_3

    .line 849
    if-eqz v2, :cond_b

    .line 851
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setEnabled(Z)V

    .line 859
    :cond_3
    :goto_4
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lh;->dj:I

    .line 860
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 859
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/phone/AvatarPreference;

    iput-object v0, p0, Lbtf;->i:Lcom/google/android/apps/hangouts/phone/AvatarPreference;

    .line 861
    iget-object v0, p0, Lbtf;->i:Lcom/google/android/apps/hangouts/phone/AvatarPreference;

    new-instance v1, Lbtj;

    invoke-direct {v1, p0}, Lbtj;-><init>(Lbtf;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/phone/AvatarPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 879
    invoke-direct {p0}, Lbtf;->n()V

    .line 882
    invoke-virtual {p0}, Lbtf;->f()V

    .line 884
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lh;->kX:I

    .line 885
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 884
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 887
    iget-object v1, p0, Lbtf;->c:Lyj;

    invoke-virtual {v1}, Lyj;->u()Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, "babel_device_presence"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 890
    new-instance v1, Lbtk;

    invoke-direct {v1, p0}, Lbtk;-><init>(Lbtf;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 909
    :goto_5
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lh;->fV:I

    .line 910
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 909
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 913
    const-string v1, "babel_enable_last_seen"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_4

    .line 915
    iget-object v1, p0, Lbtf;->a:Lbtz;

    invoke-interface {v1, v0}, Lbtz;->a(Landroid/preference/Preference;)V

    .line 918
    :cond_4
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lh;->Q:I

    .line 919
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 918
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 921
    iget-object v1, p0, Lbtf;->c:Lyj;

    invoke-virtual {v1}, Lyj;->S()Z

    move-result v1

    if-eqz v1, :cond_d

    iget-object v1, p0, Lbtf;->c:Lyj;

    invoke-virtual {v1}, Lyj;->O()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 922
    iget-object v1, p0, Lbtf;->c:Lyj;

    invoke-virtual {v1}, Lyj;->T()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 923
    new-instance v1, Lbtl;

    invoke-direct {v1, p0}, Lbtl;-><init>(Lbtf;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 937
    :goto_6
    invoke-virtual {p0}, Lbtf;->g()V

    .line 940
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lh;->h:I

    .line 941
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 940
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 942
    iget-object v1, p0, Lbtf;->c:Lyj;

    invoke-virtual {v1}, Lyj;->g()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 944
    iget-object v1, p0, Lbtf;->a:Lbtz;

    iget-object v2, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v3, Lh;->dk:I

    .line 945
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 944
    invoke-interface {v1, v2}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 946
    iget-object v2, p0, Lbtf;->a:Lbtz;

    iget-object v3, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->dl:I

    .line 947
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 946
    invoke-interface {v2, v3}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    .line 948
    iget-object v3, p0, Lbtf;->c:Lyj;

    invoke-virtual {v3}, Lyj;->l()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 951
    iget-object v3, p0, Lbtf;->c:Lyj;

    .line 952
    invoke-virtual {v3}, Lyj;->c()Lbdk;

    move-result-object v3

    iget-object v3, v3, Lbdk;->a:Ljava/lang/String;

    .line 951
    invoke-static {v3}, Lbbl;->c(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 953
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 961
    :goto_7
    iget-object v1, p0, Lbtf;->a:Lbtz;

    iget-object v2, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v3, Lh;->ix:I

    .line 962
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 961
    invoke-interface {v1, v2}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lbtf;->j:Landroid/preference/Preference;

    .line 963
    invoke-direct {p0}, Lbtf;->o()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 964
    iget-object v0, p0, Lbtf;->j:Landroid/preference/Preference;

    new-instance v1, Lbtm;

    invoke-direct {v1, p0}, Lbtm;-><init>(Lbtf;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 980
    :goto_8
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lh;->la:I

    .line 981
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 980
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 982
    new-instance v1, Lbtn;

    invoke-direct {v1, p0}, Lbtn;-><init>(Lbtf;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 993
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lh;->fz:I

    .line 994
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 993
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 995
    new-instance v1, Lbto;

    invoke-direct {v1, p0}, Lbto;-><init>(Lbtf;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1011
    :goto_9
    iget-object v0, p0, Lbtf;->a:Lbtz;

    iget-object v1, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v2, Lh;->fH:I

    .line 1012
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1011
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 1013
    new-instance v1, Lbtp;

    invoke-direct {v1, p0}, Lbtp;-><init>(Lbtf;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto/16 :goto_0

    .line 753
    :cond_5
    const/4 v0, 0x0

    move v8, v0

    goto/16 :goto_1

    .line 791
    :cond_6
    const/4 v4, 0x0

    move v5, v4

    goto/16 :goto_2

    .line 799
    :cond_7
    const-string v4, ""

    .line 800
    iget-object v6, p0, Lbtf;->c:Lyj;

    invoke-virtual {v6}, Lyj;->J()Ljava/lang/String;

    move-result-object v6

    .line 801
    if-eqz v6, :cond_8

    .line 802
    invoke-static {v6}, Lbzd;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 804
    :cond_8
    iget-object v6, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v7, Lh;->dq:I

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v4, v11, v12

    invoke-virtual {v6, v7, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 807
    if-eqz v8, :cond_9

    .line 808
    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 809
    new-instance v4, Lbth;

    invoke-direct {v4, p0}, Lbth;-><init>(Lbtf;)V

    invoke-virtual {v1, v4}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 827
    :goto_a
    if-eqz v5, :cond_a

    .line 828
    invoke-virtual {v3, v9}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 830
    new-instance v1, Lbti;

    invoke-direct {v1, p0}, Lbti;-><init>(Lbtf;)V

    invoke-virtual {v3, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto/16 :goto_3

    .line 822
    :cond_9
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 823
    iget-object v1, p0, Lbtf;->s:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 824
    iget-object v1, p0, Lbtf;->t:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_a

    .line 843
    :cond_a
    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_3

    .line 854
    :cond_b
    invoke-virtual {v10, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_4

    .line 905
    :cond_c
    iget-object v1, p0, Lbtf;->a:Lbtz;

    invoke-interface {v1, v0}, Lbtz;->a(Landroid/preference/Preference;)V

    goto/16 :goto_5

    .line 933
    :cond_d
    invoke-virtual {v10, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_6

    .line 956
    :cond_e
    iget-object v3, p0, Lbtf;->c:Lyj;

    invoke-static {v3}, Lbbl;->h(Lyj;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 957
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_7

    .line 975
    :cond_f
    iget-object v1, p0, Lbtf;->j:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 976
    const/4 v0, 0x0

    iput-object v0, p0, Lbtf;->j:Landroid/preference/Preference;

    goto/16 :goto_8

    .line 1007
    :cond_10
    invoke-virtual {v10, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_9
.end method

.method protected abstract f()V
.end method

.method protected abstract g()V
.end method

.method public h()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1065
    invoke-direct {p0}, Lbtf;->o()Z

    .line 1066
    invoke-virtual {p0}, Lbtf;->i()V

    .line 1067
    iget-object v0, p0, Lbtf;->c:Lyj;

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf;->k(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v2

    iget-object v0, p0, Lbtf;->q:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lbtf;->q:Landroid/preference/Preference;

    iget-object v0, p0, Lbtf;->q:Landroid/preference/Preference;

    invoke-virtual {v0}, Landroid/preference/Preference;->isEnabled()Z

    move-result v3

    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->al:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v5, Lh;->am:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v6, Lh;->ao:I

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lbtf;->a(Landroid/preference/Preference;Landroid/content/SharedPreferences;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lbtf;->r:Landroid/preference/Preference;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lbtf;->r:Landroid/preference/Preference;

    iget-object v0, p0, Lbtf;->r:Landroid/preference/Preference;

    invoke-virtual {v0}, Landroid/preference/Preference;->isEnabled()Z

    move-result v3

    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->eX:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v5, Lh;->fq:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v6, Lh;->fs:I

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lbtf;->a(Landroid/preference/Preference;Landroid/content/SharedPreferences;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lbtf;->s:Landroid/preference/Preference;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lbtf;->s:Landroid/preference/Preference;

    iget-object v0, p0, Lbtf;->s:Landroid/preference/Preference;

    invoke-virtual {v0}, Landroid/preference/Preference;->isEnabled()Z

    move-result v3

    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->dv:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->dw:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    move-object v4, v7

    invoke-virtual/range {v0 .. v6}, Lbtf;->a(Landroid/preference/Preference;Landroid/content/SharedPreferences;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lbtf;->t:Landroid/preference/Preference;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lbtf;->t:Landroid/preference/Preference;

    iget-object v0, p0, Lbtf;->t:Landroid/preference/Preference;

    invoke-virtual {v0}, Landroid/preference/Preference;->isEnabled()Z

    move-result v3

    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->dy:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lbtf;->b:Landroid/content/res/Resources;

    sget v4, Lh;->dz:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    move-object v4, v7

    invoke-virtual/range {v0 .. v6}, Lbtf;->a(Landroid/preference/Preference;Landroid/content/SharedPreferences;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1068
    :cond_3
    return-void
.end method

.method public i()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1145
    iget-object v1, p0, Lbtf;->d:Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lbtf;->c:Lyj;

    if-eqz v1, :cond_2

    .line 1146
    iget-object v1, p0, Lbtf;->c:Lyj;

    invoke-static {v1}, Lcwz;->b(Ljava/lang/Object;)V

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lbtf;->c:Lyj;

    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lf;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lh;->kI:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lh;->kF:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lh;->kG:I

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v5, ""

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2, v4, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    if-eqz v0, :cond_3

    .line 1147
    iget-object v0, p0, Lbtf;->d:Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;

    sget v1, Lh;->kP:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;->setSummary(I)V

    .line 1152
    :cond_2
    :goto_0
    return-void

    .line 1149
    :cond_3
    iget-object v0, p0, Lbtf;->d:Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;

    sget v1, Lh;->kJ:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;->setSummary(I)V

    goto :goto_0
.end method
