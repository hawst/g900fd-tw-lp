.class public final Ldef;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldef;


# instance fields
.field public b:Ldfi;

.field public c:[Ldgn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 961
    const/4 v0, 0x0

    new-array v0, v0, [Ldef;

    sput-object v0, Ldef;->a:[Ldef;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 962
    invoke-direct {p0}, Lepn;-><init>()V

    .line 965
    const/4 v0, 0x0

    iput-object v0, p0, Ldef;->b:Ldfi;

    .line 968
    sget-object v0, Ldgn;->a:[Ldgn;

    iput-object v0, p0, Ldef;->c:[Ldgn;

    .line 962
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 990
    iget-object v0, p0, Ldef;->b:Ldfi;

    if-eqz v0, :cond_2

    .line 991
    const/4 v0, 0x1

    iget-object v2, p0, Ldef;->b:Ldfi;

    .line 992
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 994
    :goto_0
    iget-object v2, p0, Ldef;->c:[Ldgn;

    if-eqz v2, :cond_1

    .line 995
    iget-object v2, p0, Ldef;->c:[Ldgn;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 996
    if-eqz v4, :cond_0

    .line 997
    const/4 v5, 0x2

    .line 998
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 995
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1002
    :cond_1
    iget-object v1, p0, Ldef;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1003
    iput v0, p0, Ldef;->cachedSize:I

    .line 1004
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 958
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldef;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldef;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldef;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldef;->b:Ldfi;

    if-nez v0, :cond_2

    new-instance v0, Ldfi;

    invoke-direct {v0}, Ldfi;-><init>()V

    iput-object v0, p0, Ldef;->b:Ldfi;

    :cond_2
    iget-object v0, p0, Ldef;->b:Ldfi;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldef;->c:[Ldgn;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldgn;

    iget-object v3, p0, Ldef;->c:[Ldgn;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldef;->c:[Ldgn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldef;->c:[Ldgn;

    :goto_2
    iget-object v2, p0, Ldef;->c:[Ldgn;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldef;->c:[Ldgn;

    new-instance v3, Ldgn;

    invoke-direct {v3}, Ldgn;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldef;->c:[Ldgn;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldef;->c:[Ldgn;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldef;->c:[Ldgn;

    new-instance v3, Ldgn;

    invoke-direct {v3}, Ldgn;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldef;->c:[Ldgn;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 973
    iget-object v0, p0, Ldef;->b:Ldfi;

    if-eqz v0, :cond_0

    .line 974
    const/4 v0, 0x1

    iget-object v1, p0, Ldef;->b:Ldfi;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 976
    :cond_0
    iget-object v0, p0, Ldef;->c:[Ldgn;

    if-eqz v0, :cond_2

    .line 977
    iget-object v1, p0, Ldef;->c:[Ldgn;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 978
    if-eqz v3, :cond_1

    .line 979
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 977
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 983
    :cond_2
    iget-object v0, p0, Ldef;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 985
    return-void
.end method
