.class public final Ldmf;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldmf;


# instance fields
.field public b:[I

.field public c:[I

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6516
    const/4 v0, 0x0

    new-array v0, v0, [Ldmf;

    sput-object v0, Ldmf;->a:[Ldmf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6517
    invoke-direct {p0}, Lepn;-><init>()V

    .line 6520
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Ldmf;->b:[I

    .line 6523
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Ldmf;->c:[I

    .line 6517
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 6555
    iget-object v0, p0, Ldmf;->b:[I

    if-eqz v0, :cond_5

    iget-object v0, p0, Ldmf;->b:[I

    array-length v0, v0

    if-lez v0, :cond_5

    .line 6557
    iget-object v3, p0, Ldmf;->b:[I

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget v5, v3, v0

    .line 6559
    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 6557
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6562
    :cond_0
    iget-object v0, p0, Ldmf;->b:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    .line 6564
    :goto_1
    iget-object v2, p0, Ldmf;->c:[I

    if-eqz v2, :cond_2

    iget-object v2, p0, Ldmf;->c:[I

    array-length v2, v2

    if-lez v2, :cond_2

    .line 6566
    iget-object v3, p0, Ldmf;->c:[I

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_1

    aget v5, v3, v1

    .line 6568
    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 6566
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 6570
    :cond_1
    add-int/2addr v0, v2

    .line 6571
    iget-object v1, p0, Ldmf;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6573
    :cond_2
    iget-object v1, p0, Ldmf;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 6574
    const/4 v1, 0x3

    iget-object v2, p0, Ldmf;->d:Ljava/lang/Boolean;

    .line 6575
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6577
    :cond_3
    iget-object v1, p0, Ldmf;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 6578
    const/4 v1, 0x4

    iget-object v2, p0, Ldmf;->e:Ljava/lang/Boolean;

    .line 6579
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6581
    :cond_4
    iget-object v1, p0, Ldmf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6582
    iput v0, p0, Ldmf;->cachedSize:I

    .line 6583
    return v0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 6513
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldmf;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldmf;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldmf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldmf;->b:[I

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [I

    iget-object v2, p0, Ldmf;->b:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldmf;->b:[I

    :goto_1
    iget-object v1, p0, Ldmf;->b:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Ldmf;->b:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Ldmf;->b:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldmf;->c:[I

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [I

    iget-object v2, p0, Ldmf;->c:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldmf;->c:[I

    :goto_2
    iget-object v1, p0, Ldmf;->c:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Ldmf;->c:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v1, p0, Ldmf;->c:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldmf;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldmf;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 6532
    iget-object v1, p0, Ldmf;->b:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldmf;->b:[I

    array-length v1, v1

    if-lez v1, :cond_0

    .line 6533
    iget-object v2, p0, Ldmf;->b:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    .line 6534
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lepl;->a(II)V

    .line 6533
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 6537
    :cond_0
    iget-object v1, p0, Ldmf;->c:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Ldmf;->c:[I

    array-length v1, v1

    if-lez v1, :cond_1

    .line 6538
    iget-object v1, p0, Ldmf;->c:[I

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    .line 6539
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 6538
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 6542
    :cond_1
    iget-object v0, p0, Ldmf;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 6543
    const/4 v0, 0x3

    iget-object v1, p0, Ldmf;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 6545
    :cond_2
    iget-object v0, p0, Ldmf;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 6546
    const/4 v0, 0x4

    iget-object v1, p0, Ldmf;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 6548
    :cond_3
    iget-object v0, p0, Ldmf;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 6550
    return-void
.end method
