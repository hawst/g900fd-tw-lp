.class public final Ldgy;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldgy;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ldlb;

.field public f:Ldku;

.field public g:Lesh;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/Long;

.field public j:Ljava/lang/Long;

.field public k:Ldhl;

.field public l:Ljava/lang/Integer;

.field public m:Ljava/lang/Integer;

.field public n:Ljava/lang/String;

.field public o:Ldin;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4192
    const/4 v0, 0x0

    new-array v0, v0, [Ldgy;

    sput-object v0, Ldgy;->a:[Ldgy;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4193
    invoke-direct {p0}, Lepn;-><init>()V

    .line 4214
    iput-object v0, p0, Ldgy;->e:Ldlb;

    .line 4217
    iput-object v0, p0, Ldgy;->f:Ldku;

    .line 4220
    iput-object v0, p0, Ldgy;->g:Lesh;

    .line 4229
    iput-object v0, p0, Ldgy;->k:Ldhl;

    .line 4232
    iput-object v0, p0, Ldgy;->l:Ljava/lang/Integer;

    .line 4235
    iput-object v0, p0, Ldgy;->m:Ljava/lang/Integer;

    .line 4240
    iput-object v0, p0, Ldgy;->o:Ldin;

    .line 4193
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 4287
    const/4 v0, 0x1

    iget-object v1, p0, Ldgy;->b:Ljava/lang/String;

    .line 4289
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4290
    const/4 v1, 0x2

    iget-object v2, p0, Ldgy;->c:Ljava/lang/String;

    .line 4291
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4292
    const/4 v1, 0x3

    iget-object v2, p0, Ldgy;->h:Ljava/lang/String;

    .line 4293
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4294
    iget-object v1, p0, Ldgy;->k:Ldhl;

    if-eqz v1, :cond_0

    .line 4295
    const/4 v1, 0x4

    iget-object v2, p0, Ldgy;->k:Ldhl;

    .line 4296
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4298
    :cond_0
    iget-object v1, p0, Ldgy;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 4299
    const/4 v1, 0x5

    iget-object v2, p0, Ldgy;->l:Ljava/lang/Integer;

    .line 4300
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4302
    :cond_1
    iget-object v1, p0, Ldgy;->m:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 4303
    const/4 v1, 0x6

    iget-object v2, p0, Ldgy;->m:Ljava/lang/Integer;

    .line 4304
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4306
    :cond_2
    iget-object v1, p0, Ldgy;->n:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 4307
    const/4 v1, 0x7

    iget-object v2, p0, Ldgy;->n:Ljava/lang/String;

    .line 4308
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4310
    :cond_3
    iget-object v1, p0, Ldgy;->o:Ldin;

    if-eqz v1, :cond_4

    .line 4311
    const/16 v1, 0x8

    iget-object v2, p0, Ldgy;->o:Ldin;

    .line 4312
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4314
    :cond_4
    iget-object v1, p0, Ldgy;->d:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 4315
    const/16 v1, 0x9

    iget-object v2, p0, Ldgy;->d:Ljava/lang/String;

    .line 4316
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4318
    :cond_5
    iget-object v1, p0, Ldgy;->i:Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 4319
    const/16 v1, 0xa

    iget-object v2, p0, Ldgy;->i:Ljava/lang/Long;

    .line 4320
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4322
    :cond_6
    iget-object v1, p0, Ldgy;->j:Ljava/lang/Long;

    if-eqz v1, :cond_7

    .line 4323
    const/16 v1, 0xb

    iget-object v2, p0, Ldgy;->j:Ljava/lang/Long;

    .line 4324
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4326
    :cond_7
    iget-object v1, p0, Ldgy;->e:Ldlb;

    if-eqz v1, :cond_8

    .line 4327
    const/16 v1, 0xc

    iget-object v2, p0, Ldgy;->e:Ldlb;

    .line 4328
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4330
    :cond_8
    iget-object v1, p0, Ldgy;->f:Ldku;

    if-eqz v1, :cond_9

    .line 4331
    const/16 v1, 0xd

    iget-object v2, p0, Ldgy;->f:Ldku;

    .line 4332
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4334
    :cond_9
    iget-object v1, p0, Ldgy;->g:Lesh;

    if-eqz v1, :cond_a

    .line 4335
    const/16 v1, 0xe

    iget-object v2, p0, Ldgy;->g:Lesh;

    .line 4336
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4338
    :cond_a
    iget-object v1, p0, Ldgy;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4339
    iput v0, p0, Ldgy;->cachedSize:I

    .line 4340
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 4189
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldgy;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldgy;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldgy;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgy;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgy;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgy;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldgy;->k:Ldhl;

    if-nez v0, :cond_2

    new-instance v0, Ldhl;

    invoke-direct {v0}, Ldhl;-><init>()V

    iput-object v0, p0, Ldgy;->k:Ldhl;

    :cond_2
    iget-object v0, p0, Ldgy;->k:Ldhl;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_3

    if-eq v0, v3, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgy;->l:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgy;->l:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_5

    if-ne v0, v3, :cond_6

    :cond_5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgy;->m:Ljava/lang/Integer;

    goto :goto_0

    :cond_6
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgy;->m:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgy;->n:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Ldgy;->o:Ldin;

    if-nez v0, :cond_7

    new-instance v0, Ldin;

    invoke-direct {v0}, Ldin;-><init>()V

    iput-object v0, p0, Ldgy;->o:Ldin;

    :cond_7
    iget-object v0, p0, Ldgy;->o:Ldin;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgy;->d:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldgy;->i:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldgy;->j:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Ldgy;->e:Ldlb;

    if-nez v0, :cond_8

    new-instance v0, Ldlb;

    invoke-direct {v0}, Ldlb;-><init>()V

    iput-object v0, p0, Ldgy;->e:Ldlb;

    :cond_8
    iget-object v0, p0, Ldgy;->e:Ldlb;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Ldgy;->f:Ldku;

    if-nez v0, :cond_9

    new-instance v0, Ldku;

    invoke-direct {v0}, Ldku;-><init>()V

    iput-object v0, p0, Ldgy;->f:Ldku;

    :cond_9
    iget-object v0, p0, Ldgy;->f:Ldku;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Ldgy;->g:Lesh;

    if-nez v0, :cond_a

    new-instance v0, Lesh;

    invoke-direct {v0}, Lesh;-><init>()V

    iput-object v0, p0, Ldgy;->g:Lesh;

    :cond_a
    iget-object v0, p0, Ldgy;->g:Lesh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 4245
    const/4 v0, 0x1

    iget-object v1, p0, Ldgy;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4246
    const/4 v0, 0x2

    iget-object v1, p0, Ldgy;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4247
    const/4 v0, 0x3

    iget-object v1, p0, Ldgy;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4248
    iget-object v0, p0, Ldgy;->k:Ldhl;

    if-eqz v0, :cond_0

    .line 4249
    const/4 v0, 0x4

    iget-object v1, p0, Ldgy;->k:Ldhl;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 4251
    :cond_0
    iget-object v0, p0, Ldgy;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 4252
    const/4 v0, 0x5

    iget-object v1, p0, Ldgy;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 4254
    :cond_1
    iget-object v0, p0, Ldgy;->m:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 4255
    const/4 v0, 0x6

    iget-object v1, p0, Ldgy;->m:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 4257
    :cond_2
    iget-object v0, p0, Ldgy;->n:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 4258
    const/4 v0, 0x7

    iget-object v1, p0, Ldgy;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4260
    :cond_3
    iget-object v0, p0, Ldgy;->o:Ldin;

    if-eqz v0, :cond_4

    .line 4261
    const/16 v0, 0x8

    iget-object v1, p0, Ldgy;->o:Ldin;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 4263
    :cond_4
    iget-object v0, p0, Ldgy;->d:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 4264
    const/16 v0, 0x9

    iget-object v1, p0, Ldgy;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4266
    :cond_5
    iget-object v0, p0, Ldgy;->i:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 4267
    const/16 v0, 0xa

    iget-object v1, p0, Ldgy;->i:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 4269
    :cond_6
    iget-object v0, p0, Ldgy;->j:Ljava/lang/Long;

    if-eqz v0, :cond_7

    .line 4270
    const/16 v0, 0xb

    iget-object v1, p0, Ldgy;->j:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 4272
    :cond_7
    iget-object v0, p0, Ldgy;->e:Ldlb;

    if-eqz v0, :cond_8

    .line 4273
    const/16 v0, 0xc

    iget-object v1, p0, Ldgy;->e:Ldlb;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 4275
    :cond_8
    iget-object v0, p0, Ldgy;->f:Ldku;

    if-eqz v0, :cond_9

    .line 4276
    const/16 v0, 0xd

    iget-object v1, p0, Ldgy;->f:Ldku;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 4278
    :cond_9
    iget-object v0, p0, Ldgy;->g:Lesh;

    if-eqz v0, :cond_a

    .line 4279
    const/16 v0, 0xe

    iget-object v1, p0, Ldgy;->g:Lesh;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 4281
    :cond_a
    iget-object v0, p0, Ldgy;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 4283
    return-void
.end method
