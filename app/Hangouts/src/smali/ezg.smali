.class public final Lezg;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lezg;


# instance fields
.field public b:Ljava/lang/Boolean;

.field public c:Lezf;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/String;

.field public f:Lezh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    new-array v0, v0, [Lezg;

    sput-object v0, Lezg;->a:[Lezg;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 96
    invoke-direct {p0}, Lepn;-><init>()V

    .line 101
    iput-object v0, p0, Lezg;->c:Lezf;

    .line 104
    iput-object v0, p0, Lezg;->d:Ljava/lang/Integer;

    .line 109
    iput-object v0, p0, Lezg;->f:Lezh;

    .line 96
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 135
    const/4 v0, 0x0

    .line 136
    iget-object v1, p0, Lezg;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 137
    const/4 v0, 0x1

    iget-object v1, p0, Lezg;->b:Ljava/lang/Boolean;

    .line 138
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 140
    :cond_0
    iget-object v1, p0, Lezg;->f:Lezh;

    if-eqz v1, :cond_1

    .line 141
    const/4 v1, 0x2

    iget-object v2, p0, Lezg;->f:Lezh;

    .line 142
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 144
    :cond_1
    iget-object v1, p0, Lezg;->c:Lezf;

    if-eqz v1, :cond_2

    .line 145
    const/4 v1, 0x3

    iget-object v2, p0, Lezg;->c:Lezf;

    .line 146
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 148
    :cond_2
    iget-object v1, p0, Lezg;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 149
    const/4 v1, 0x4

    iget-object v2, p0, Lezg;->d:Ljava/lang/Integer;

    .line 150
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 152
    :cond_3
    iget-object v1, p0, Lezg;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 153
    const/4 v1, 0x5

    iget-object v2, p0, Lezg;->e:Ljava/lang/String;

    .line 154
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 156
    :cond_4
    iget-object v1, p0, Lezg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 157
    iput v0, p0, Lezg;->cachedSize:I

    .line 158
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 92
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lezg;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lezg;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lezg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lezg;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lezg;->f:Lezh;

    if-nez v0, :cond_2

    new-instance v0, Lezh;

    invoke-direct {v0}, Lezh;-><init>()V

    iput-object v0, p0, Lezg;->f:Lezh;

    :cond_2
    iget-object v0, p0, Lezg;->f:Lezh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lezg;->c:Lezf;

    if-nez v0, :cond_3

    new-instance v0, Lezf;

    invoke-direct {v0}, Lezf;-><init>()V

    iput-object v0, p0, Lezg;->c:Lezf;

    :cond_3
    iget-object v0, p0, Lezg;->c:Lezf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_4

    const v1, 0x30de125

    if-eq v0, v1, :cond_4

    const v1, 0x3c0b096

    if-eq v0, v1, :cond_4

    const v1, 0x3ed5811

    if-eq v0, v1, :cond_4

    const v1, 0x25e915d

    if-eq v0, v1, :cond_4

    const v1, 0x28f0446

    if-eq v0, v1, :cond_4

    const v1, 0x28531db

    if-eq v0, v1, :cond_4

    const v1, 0x2bca2e8

    if-eq v0, v1, :cond_4

    const v1, 0x26cd5de    # 1.7399919E-37f

    if-eq v0, v1, :cond_4

    const v1, 0x26dae09

    if-eq v0, v1, :cond_4

    const v1, 0x29ed066

    if-eq v0, v1, :cond_4

    const v1, 0x2a6615e

    if-eq v0, v1, :cond_4

    const v1, 0x38f2118

    if-eq v0, v1, :cond_4

    const v1, 0x2b00a9d

    if-eq v0, v1, :cond_4

    const v1, 0x26dc540

    if-eq v0, v1, :cond_4

    const v1, 0x3bf218f

    if-eq v0, v1, :cond_4

    const v1, 0x2b3504b

    if-eq v0, v1, :cond_4

    const v1, 0x4561965

    if-eq v0, v1, :cond_4

    const v1, 0x2a5ab17

    if-eq v0, v1, :cond_4

    const v1, 0x13122db

    if-eq v0, v1, :cond_4

    const v1, 0x2920300

    if-eq v0, v1, :cond_4

    const v1, 0x28fba42

    if-eq v0, v1, :cond_4

    const v1, 0x28ef1ba

    if-eq v0, v1, :cond_4

    const v1, 0x2624fdb

    if-eq v0, v1, :cond_4

    const v1, 0x433fe13

    if-eq v0, v1, :cond_4

    const v1, 0x369acd5

    if-eq v0, v1, :cond_4

    const v1, 0x32cbbd0

    if-eq v0, v1, :cond_4

    const v1, 0x271ac36

    if-eq v0, v1, :cond_4

    const v1, 0x28fc6da

    if-eq v0, v1, :cond_4

    const v1, 0x45a5ece

    if-eq v0, v1, :cond_4

    const v1, 0x2e5737b

    if-eq v0, v1, :cond_4

    const v1, 0x282baca

    if-eq v0, v1, :cond_4

    const v1, 0x3811a34

    if-eq v0, v1, :cond_4

    const v1, 0x2e573d6

    if-eq v0, v1, :cond_4

    const v1, 0x2e57430

    if-eq v0, v1, :cond_4

    const v1, 0x2e57483

    if-eq v0, v1, :cond_4

    const v1, 0x2edfbe9

    if-eq v0, v1, :cond_4

    const v1, 0x2e57524

    if-eq v0, v1, :cond_4

    const v1, 0x2e5756f

    if-eq v0, v1, :cond_4

    const v1, 0x3a1d294

    if-eq v0, v1, :cond_4

    const v1, 0x2e575b2

    if-ne v0, v1, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lezg;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lezg;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lezg;->e:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lezg;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 115
    const/4 v0, 0x1

    iget-object v1, p0, Lezg;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 117
    :cond_0
    iget-object v0, p0, Lezg;->f:Lezh;

    if-eqz v0, :cond_1

    .line 118
    const/4 v0, 0x2

    iget-object v1, p0, Lezg;->f:Lezh;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 120
    :cond_1
    iget-object v0, p0, Lezg;->c:Lezf;

    if-eqz v0, :cond_2

    .line 121
    const/4 v0, 0x3

    iget-object v1, p0, Lezg;->c:Lezf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 123
    :cond_2
    iget-object v0, p0, Lezg;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 124
    const/4 v0, 0x4

    iget-object v1, p0, Lezg;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 126
    :cond_3
    iget-object v0, p0, Lezg;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 127
    const/4 v0, 0x5

    iget-object v1, p0, Lezg;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 129
    :cond_4
    iget-object v0, p0, Lezg;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 131
    return-void
.end method
