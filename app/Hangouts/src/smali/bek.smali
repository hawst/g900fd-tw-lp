.class public Lbek;
.super Lbed;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x3L


# instance fields
.field public final a:Z

.field public final b:Z

.field public final i:Z

.field public final j:[B

.field public final k:J

.field public final l:Ljava/lang/String;

.field public final m:I

.field public final n:J

.field public final o:Lbjg;

.field public p:Ljava/lang/String;

.field public final q:Lbee;


# direct methods
.method public constructor <init>(Lbee;)V
    .locals 5

    .prologue
    const-wide/16 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3782
    invoke-direct {p0, v1}, Lbed;-><init>(Ljava/lang/String;)V

    .line 3783
    iput-object p1, p0, Lbek;->q:Lbee;

    .line 3784
    iput-boolean v2, p0, Lbek;->a:Z

    .line 3785
    iput-boolean v2, p0, Lbek;->b:Z

    .line 3786
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbek;->i:Z

    .line 3791
    iput v2, p0, Lbek;->m:I

    .line 3792
    iput-object v1, p0, Lbek;->j:[B

    .line 3793
    iput-wide v3, p0, Lbek;->k:J

    .line 3794
    iput-object v1, p0, Lbek;->l:Ljava/lang/String;

    .line 3795
    iput-object v1, p0, Lbek;->o:Lbjg;

    .line 3796
    iput-object v1, p0, Lbek;->p:Ljava/lang/String;

    .line 3797
    iput-wide v3, p0, Lbek;->n:J

    .line 3798
    return-void
.end method

.method public constructor <init>(Lbee;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 3803
    invoke-direct {p0, p1}, Lbek;-><init>(Lbee;)V

    .line 3804
    iput-object p2, p0, Lbek;->p:Ljava/lang/String;

    .line 3805
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZZZ[BJLjava/lang/String;JLbjg;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3763
    invoke-direct {p0, p1}, Lbed;-><init>(Ljava/lang/String;)V

    .line 3764
    iput-boolean p2, p0, Lbek;->a:Z

    .line 3765
    iput-boolean p3, p0, Lbek;->b:Z

    .line 3766
    iput-boolean p4, p0, Lbek;->i:Z

    .line 3767
    iput-object p5, p0, Lbek;->j:[B

    .line 3768
    iput-wide p6, p0, Lbek;->k:J

    .line 3769
    iput-object p8, p0, Lbek;->l:Ljava/lang/String;

    .line 3770
    const-string v0, "babel_smaxevperconv"

    const/16 v1, 0x14

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lbek;->m:I

    .line 3773
    iput-object p11, p0, Lbek;->o:Lbjg;

    .line 3774
    iput-object v2, p0, Lbek;->q:Lbee;

    .line 3775
    iput-wide p9, p0, Lbek;->n:J

    .line 3776
    iput-object v2, p0, Lbek;->p:Ljava/lang/String;

    .line 3777
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 9

    .prologue
    const-wide/16 v7, 0x0

    .line 3809
    const-string v0, "Babel_RequestWriter"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3810
    const-string v0, "Babel_RequestWriter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetConversationRequest build protobuf:  conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbek;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " includeConversationMetadata="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lbek;->a:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " includeEvents="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lbek;->b:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " continuationToken="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbek;->j:[B

    .line 3814
    invoke-static {v2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " continuationEventTimestamp="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lbek;->k:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " maxEventsPerConversation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lbek;->m:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3810
    invoke-static {v0, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3818
    :cond_0
    new-instance v3, Ldsk;

    invoke-direct {v3}, Ldsk;-><init>()V

    .line 3820
    iget-object v0, p0, Lbek;->c:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 3821
    new-instance v0, Ldqk;

    invoke-direct {v0}, Ldqk;-><init>()V

    iput-object v0, v3, Ldsk;->c:Ldqk;

    .line 3822
    iget-object v0, v3, Ldsk;->c:Ldqk;

    iget-object v1, p0, Lbek;->c:Ljava/lang/String;

    invoke-static {v1}, Lbea;->d(Ljava/lang/String;)Ldqf;

    move-result-object v1

    iput-object v1, v0, Ldqk;->b:Ldqf;

    .line 3836
    :goto_0
    iget-boolean v0, p0, Lbek;->a:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v3, Ldsk;->d:Ljava/lang/Boolean;

    .line 3837
    iget-boolean v0, p0, Lbek;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v3, Ldsk;->e:Ljava/lang/Boolean;

    .line 3838
    iget-boolean v0, p0, Lbek;->i:Z

    if-eqz v0, :cond_1

    .line 3839
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Ldsk;->h:Ljava/lang/Integer;

    .line 3841
    :cond_1
    invoke-virtual {p0, p1, p2}, Lbek;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v0

    iput-object v0, v3, Ldsk;->b:Ldvm;

    .line 3842
    iget-object v0, p0, Lbek;->j:[B

    if-nez v0, :cond_2

    iget-wide v0, p0, Lbek;->k:J

    cmp-long v0, v0, v7

    if-eqz v0, :cond_5

    .line 3843
    :cond_2
    new-instance v0, Ldrv;

    invoke-direct {v0}, Ldrv;-><init>()V

    .line 3844
    iget-object v1, p0, Lbek;->j:[B

    if-eqz v1, :cond_3

    .line 3845
    iget-object v1, p0, Lbek;->j:[B

    iget-object v2, p0, Lbek;->j:[B

    array-length v2, v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    iput-object v1, v0, Ldrv;->c:[B

    .line 3848
    :cond_3
    iget-wide v1, p0, Lbek;->k:J

    cmp-long v1, v1, v7

    if-eqz v1, :cond_4

    .line 3849
    iget-wide v1, p0, Lbek;->k:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Ldrv;->b:Ljava/lang/Long;

    .line 3851
    :cond_4
    iput-object v0, v3, Ldsk;->f:Ldrv;

    .line 3853
    :cond_5
    iget v0, p0, Lbek;->m:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Ldsk;->g:Ljava/lang/Integer;

    .line 3854
    return-object v3

    .line 3823
    :cond_6
    iget-object v0, p0, Lbek;->q:Lbee;

    if-eqz v0, :cond_8

    .line 3824
    new-instance v4, Ldqk;

    invoke-direct {v4}, Ldqk;-><init>()V

    .line 3825
    iget-object v0, p0, Lbek;->q:Lbee;

    iget-object v0, v0, Lbee;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ldth;

    iput-object v0, v4, Ldqk;->c:[Ldth;

    .line 3826
    const/4 v0, 0x0

    .line 3827
    iget-object v1, p0, Lbek;->q:Lbee;

    iget-object v1, v1, Lbee;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 3828
    iget-object v6, v4, Ldqk;->c:[Ldth;

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v0}, Lbdh;->h()Ldth;

    move-result-object v0

    aput-object v0, v6, v1

    move v1, v2

    .line 3829
    goto :goto_1

    .line 3830
    :cond_7
    iput-object v4, v3, Ldsk;->c:Ldqk;

    goto/16 :goto_0

    .line 3832
    :cond_8
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "must specify either conversationId or participantsQuery"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lyj;Lbph;)V
    .locals 3

    .prologue
    .line 3890
    invoke-super {p0, p1, p2}, Lbed;->a(Lyj;Lbph;)V

    .line 3891
    iget-object v0, p0, Lbek;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3893
    invoke-static {p1}, Lbpf;->a(Lyj;)Lbpf;

    move-result-object v0

    .line 3894
    iget-object v1, p0, Lbek;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbpf;->d(Ljava/lang/String;)Z

    .line 3899
    :cond_0
    iget-object v0, p0, Lbek;->q:Lbee;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbek;->p:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 3900
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 3901
    iget-object v1, p0, Lbek;->q:Lbee;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 3902
    invoke-static {p1}, Lbkb;->o(Lyj;)Lbki;

    move-result-object v1

    .line 3903
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->m()I

    move-result v2

    .line 3902
    invoke-virtual {v1, v0, v2}, Lbki;->a(Ljava/util/Collection;I)V

    .line 3906
    :cond_1
    iget-object v0, p0, Lbek;->p:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3907
    iget-object v0, p0, Lbek;->p:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Lyj;Ljava/lang/String;)I

    .line 3911
    :cond_2
    invoke-virtual {p2}, Lbph;->e()I

    move-result v0

    const/16 v1, 0x6f

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lbek;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 3913
    iget-object v0, p0, Lbek;->c:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->t(Lyj;Ljava/lang/String;)I

    .line 3915
    :cond_3
    return-void
.end method

.method public a(JI)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3869
    iget-object v2, p0, Lbek;->q:Lbee;

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public a(Lbea;)Z
    .locals 4

    .prologue
    .line 3919
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3920
    check-cast p1, Lbek;

    .line 3921
    iget-object v0, p0, Lbek;->c:Ljava/lang/String;

    iget-object v1, p1, Lbek;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbek;->a:Z

    iget-boolean v1, p1, Lbek;->a:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lbek;->b:Z

    iget-boolean v1, p1, Lbek;->b:Z

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lbek;->k:J

    iget-wide v2, p1, Lbek;->k:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lbek;->q:Lbee;

    if-nez v0, :cond_0

    iget-object v0, p1, Lbek;->q:Lbee;

    if-nez v0, :cond_0

    iget-object v0, p0, Lbek;->l:Ljava/lang/String;

    iget-object v1, p1, Lbek;->l:Ljava/lang/String;

    .line 3929
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbek;->o:Lbjg;

    if-nez v0, :cond_0

    iget-object v0, p1, Lbek;->o:Lbjg;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3885
    const-string v0, "conversations/getconversation"

    return-object v0
.end method

.method public d()J
    .locals 3

    .prologue
    .line 3874
    iget-boolean v0, p0, Lbek;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbek;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbek;->q:Lbee;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 3876
    invoke-super {p0}, Lbed;->d()J

    move-result-wide v0

    .line 3879
    :goto_1
    return-wide v0

    .line 3874
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3879
    :cond_1
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3c

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    goto :goto_1
.end method
