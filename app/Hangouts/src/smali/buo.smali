.class public Lbuo;
.super Lbuc;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private b:Lbup;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lbuc;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 26
    invoke-super {p0, p1}, Lbuc;->onCreate(Landroid/os/Bundle;)V

    .line 28
    invoke-virtual {p0}, Lbuo;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 29
    new-instance v0, Lbup;

    const-string v2, "notification_pref_key"

    .line 30
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ringtone_pref_key"

    .line 31
    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "ringtone_type"

    .line 32
    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v5, "vibrate_pref_key"

    .line 33
    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lbup;-><init>(Lbtz;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    iput-object v0, p0, Lbuo;->b:Lbup;

    .line 34
    iget-object v0, p0, Lbuo;->b:Lbup;

    invoke-virtual {p0}, Lbuo;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbup;->a(Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lbuo;->b:Lbup;

    const-string v0, "Babel"

    const-string v1, "NotificationSettingsUI onDestroy"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    invoke-super {p0}, Lbuc;->onDestroy()V

    .line 47
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0}, Lbuc;->onResume()V

    .line 40
    iget-object v0, p0, Lbuo;->b:Lbup;

    const-string v0, "Babel"

    const-string v1, "NotificationSettingsUI onResume"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    return-void
.end method
