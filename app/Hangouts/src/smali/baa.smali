.class public final Lbaa;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/phone/EsApplication;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/phone/EsApplication;)V
    .locals 0

    .prologue
    .line 474
    iput-object p1, p0, Lbaa;->a:Lcom/google/android/apps/hangouts/phone/EsApplication;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 477
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 478
    new-instance v0, Lbab;

    invoke-direct {v0, p0}, Lbab;-><init>(Lbaa;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 484
    invoke-virtual {v0, v1}, Lbab;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 491
    :cond_0
    :goto_0
    return-void

    .line 485
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 486
    iget-object v0, p0, Lbaa;->a:Lcom/google/android/apps/hangouts/phone/EsApplication;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Lcom/google/android/apps/hangouts/phone/EsApplication;)V

    goto :goto_0

    .line 487
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.net.wifi.supplicant.CONNECTION_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p0, Lbaa;->a:Lcom/google/android/apps/hangouts/phone/EsApplication;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Lcom/google/android/apps/hangouts/phone/EsApplication;)V

    goto :goto_0
.end method
