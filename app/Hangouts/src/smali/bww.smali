.class final Lbww;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lbwv;


# direct methods
.method constructor <init>(Lbwv;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lbww;->a:Lbwv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 7

    .prologue
    .line 155
    invoke-static {}, Lbwv;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    const-string v0, "service connected"

    invoke-static {v0}, Lbwv;->a(Ljava/lang/String;)V

    .line 159
    :cond_0
    iget-object v0, p0, Lbww;->a:Lbwv;

    invoke-static {v0}, Lbwv;->a(Lbwv;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwx;

    .line 161
    :try_start_0
    iget-object v1, p0, Lbww;->a:Lbwv;

    iget-wide v3, v0, Lbwx;->a:J

    invoke-static {p2, v3, v4}, Lbwv;->a(Landroid/os/IBinder;J)Landroid/os/IBinder;

    move-result-object v1

    .line 162
    if-nez v1, :cond_1

    .line 163
    const-string v1, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "got null ImSession for account id "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, v0, Lbwx;->a:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 170
    :catch_0
    move-exception v1

    .line 171
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "failed to logout for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v5, v0, Lbwx;->a:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 166
    :cond_1
    :try_start_1
    invoke-static {}, Lbwv;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 167
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Calling session.logout for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, v0, Lbwx;->a:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbwv;->a(Ljava/lang/String;)V

    .line 169
    :cond_2
    iget-object v3, p0, Lbww;->a:Lbwv;

    invoke-static {v1}, Lbwv;->a(Landroid/os/IBinder;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 174
    :cond_3
    iget-object v0, p0, Lbww;->a:Lbwv;

    invoke-static {v0}, Lbwv;->b(Lbwv;)V

    .line 175
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    .prologue
    .line 179
    invoke-static {}, Lbwv;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    const-string v0, "service disconnected..."

    invoke-static {v0}, Lbwv;->a(Ljava/lang/String;)V

    .line 182
    :cond_0
    return-void
.end method
