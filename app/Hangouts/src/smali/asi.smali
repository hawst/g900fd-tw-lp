.class public final Lasi;
.super Lbor;
.source "PG"

# interfaces
.implements Lbrj;


# static fields
.field private static final a:Z


# instance fields
.field private final b:Lapx;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lbdh;",
            "Lyb;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lyb;",
            "Lbdh;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;",
            "Lyb;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lyb;",
            "Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Laqd;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lbdh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lbys;->i:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lasi;->a:Z

    return-void
.end method

.method public constructor <init>(Lapx;Lbdh;)V
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Lbor;-><init>()V

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lasi;->c:Ljava/util/Map;

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lasi;->d:Ljava/util/Map;

    .line 78
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lasi;->e:Ljava/util/Map;

    .line 80
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lasi;->f:Ljava/util/Map;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lasi;->g:Ljava/util/ArrayList;

    .line 90
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lasi;->h:Ljava/util/Map;

    .line 91
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lasi;->i:Ljava/util/Map;

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lasi;->j:Ljava/util/ArrayList;

    .line 100
    iput-object p1, p0, Lasi;->b:Lapx;

    .line 101
    iput-object p2, p0, Lasi;->k:Lbdh;

    .line 102
    iget-object v0, p0, Lasi;->k:Lbdh;

    invoke-direct {p0, v0}, Lasi;->a(Lbdh;)V

    .line 103
    return-void
.end method

.method private a(I)V
    .locals 4

    .prologue
    .line 324
    iget-object v0, p0, Lasi;->h:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 325
    iget-object v1, p0, Lasi;->i:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    sget-boolean v1, Lasi;->a:Z

    if-eqz v1, :cond_0

    .line 328
    const-string v1, "PSTN_META"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "removed request, id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for ph="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lasi;->h:Ljava/util/Map;

    .line 329
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, ", list empty, stop listening"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 328
    invoke-static {v1, v0}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    :cond_0
    iget-object v0, p0, Lasi;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 333
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    .line 335
    :cond_1
    return-void

    .line 329
    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method private a(Lbdh;)V
    .locals 3

    .prologue
    .line 343
    if-eqz p1, :cond_0

    iget v0, p1, Lbdh;->a:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lbdh;->c:Ljava/lang/String;

    .line 344
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 345
    iget-boolean v0, p1, Lbdh;->r:Z

    if-nez v0, :cond_1

    .line 346
    iget-object v0, p1, Lbdh;->c:Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lasi;->b:Lapx;

    .line 347
    invoke-virtual {v2}, Lapx;->j()Lyj;

    move-result-object v2

    .line 346
    invoke-static {v0, v1, v2, p0}, Lbrf;->a(Ljava/lang/String;ZLyj;Lbrj;)Lyb;

    move-result-object v0

    .line 348
    if-eqz v0, :cond_0

    .line 349
    iget-object v1, p0, Lasi;->c:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    iget-object v1, p0, Lasi;->d:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    sget-boolean v0, Lasi;->a:Z

    if-eqz v0, :cond_0

    .line 352
    const-string v0, "PSTN_META"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sending participant request for ph="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lbdh;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    :cond_0
    :goto_0
    return-void

    .line 358
    :cond_1
    iget-object v0, p0, Lasi;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 359
    sget-boolean v0, Lasi;->a:Z

    if-eqz v0, :cond_0

    .line 360
    const-string v0, "PSTN_META"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "participant is resolved, ph="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lbdh;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 407
    iget-object v0, p0, Lasi;->b:Lapx;

    invoke-virtual {v0}, Lapx;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapo;

    .line 408
    invoke-virtual {v0, p1}, Lapo;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 410
    :cond_0
    return-void
.end method

.method private c(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)Lbdh;
    .locals 5

    .prologue
    .line 367
    instance-of v0, p1, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 368
    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    .line 369
    iget-object v1, p0, Lasi;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbdh;

    .line 370
    iget-object v3, v1, Lbdh;->q:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;->getPstnJid()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lf;->d(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 371
    sget-boolean v0, Lasi;->a:Z

    if-eqz v0, :cond_1

    .line 372
    const-string v0, "PSTN_META"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "found participant request for endpoint, name="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 373
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 372
    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    :cond_1
    :goto_0
    return-object v1

    .line 378
    :cond_2
    sget-boolean v0, Lasi;->a:Z

    if-eqz v0, :cond_3

    .line 379
    const-string v0, "PSTN_META"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "didn\'t find participant request for endpoint, name="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 380
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 379
    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private d(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)Lbdh;
    .locals 5

    .prologue
    .line 387
    instance-of v0, p1, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 388
    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    .line 389
    iget-object v1, p0, Lasi;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbdh;

    .line 390
    iget-object v3, v1, Lbdh;->q:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;->getPstnJid()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lf;->d(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 391
    sget-boolean v0, Lasi;->a:Z

    if-eqz v0, :cond_1

    .line 392
    const-string v0, "PSTN_META"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "found resolved participant for endpoint, name="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 393
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 392
    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    :cond_1
    :goto_0
    return-object v1

    .line 398
    :cond_2
    sget-boolean v0, Lasi;->a:Z

    if-eqz v0, :cond_3

    .line 399
    const-string v0, "PSTN_META"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "didn\'t find resolved participant for endpoint, name="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 400
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 399
    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 413
    invoke-static {}, Lcwz;->a()V

    .line 414
    iget-object v0, p0, Lasi;->b:Lapx;

    invoke-virtual {v0}, Lapx;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapo;

    .line 415
    invoke-virtual {v0}, Lapo;->i()V

    goto :goto_0

    .line 417
    :cond_0
    return-void
.end method


# virtual methods
.method public a(ILyj;Lbea;Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 314
    sget-boolean v0, Lasi;->a:Z

    if-eqz v0, :cond_0

    .line 315
    const-string v1, "PSTN_META"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "request failed, id="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", will"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lasi;->h:Ljava/util/Map;

    .line 316
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, ""

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " remove request."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 315
    invoke-static {v1, v0}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    :cond_0
    iget-object v0, p0, Lasi;->h:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 319
    invoke-direct {p0, p1}, Lasi;->a(I)V

    .line 321
    :cond_1
    return-void

    .line 316
    :cond_2
    const-string v0, " not"

    goto :goto_0
.end method

.method public a(ILyj;Lbos;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 276
    invoke-static {}, Lcwz;->a()V

    .line 277
    sget-boolean v0, Lasi;->a:Z

    if-eqz v0, :cond_0

    .line 278
    const-string v0, "PSTN_META"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "received response for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", was_requested="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lasi;->h:Ljava/util/Map;

    .line 279
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 278
    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    :cond_0
    iget-object v0, p0, Lasi;->h:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 283
    iget-object v0, p0, Lasi;->h:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 284
    invoke-direct {p0, p1}, Lasi;->a(I)V

    .line 286
    iget-object v1, p0, Lasi;->b:Lapx;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lasi;->b:Lapx;

    invoke-virtual {v1}, Lapx;->j()Lyj;

    move-result-object v1

    invoke-virtual {p2, v1}, Lyj;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 287
    invoke-virtual {p3}, Lbos;->c()Lbfz;

    move-result-object v1

    .line 288
    instance-of v2, v1, Lbgo;

    invoke-static {v2}, Lcwz;->a(Z)V

    .line 289
    check-cast v1, Lbgo;

    .line 291
    invoke-virtual {v1}, Lbgo;->i()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x0

    .line 294
    :goto_0
    invoke-virtual {v1}, Lbgo;->f()I

    move-result v4

    if-nez v4, :cond_4

    .line 295
    new-instance v1, Laqd;

    invoke-direct {v1, v0, v2}, Laqd;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    :goto_1
    iget-object v3, p0, Lasi;->j:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 302
    sget-boolean v1, Lasi;->a:Z

    if-eqz v1, :cond_1

    .line 303
    const-string v1, "PSTN_META"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "valid response for ph="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", rate="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    :cond_1
    invoke-direct {p0}, Lasi;->d()V

    .line 309
    :cond_2
    return-void

    .line 291
    :cond_3
    invoke-virtual {v1}, Lbgo;->h()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 297
    :cond_4
    new-instance v4, Laqf;

    .line 298
    invoke-virtual {v1}, Lbgo;->f()I

    move-result v5

    if-ne v5, v3, :cond_5

    .line 299
    :goto_2
    invoke-virtual {v1}, Lbgo;->g()I

    move-result v1

    invoke-direct {v4, v0, v2, v3, v1}, Laqf;-><init>(Ljava/lang/String;Ljava/lang/String;ZI)V

    move-object v1, v4

    goto :goto_1

    .line 298
    :cond_5
    const/4 v3, 0x0

    goto :goto_2
.end method

.method a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 4

    .prologue
    .line 135
    invoke-direct {p0, p1}, Lasi;->c(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)Lbdh;

    move-result-object v0

    .line 136
    if-eqz v0, :cond_2

    .line 137
    sget-boolean v1, Lasi;->a:Z

    if-eqz v1, :cond_0

    .line 138
    const-string v1, "PSTN_META"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Pending entity not null, moving participant request to endpoint ep="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 139
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 138
    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    :cond_0
    iget-object v1, p0, Lasi;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyb;

    .line 144
    iget-object v1, p0, Lasi;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    iget-object v1, p0, Lasi;->e:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    iget-object v1, p0, Lasi;->f:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    :cond_1
    :goto_0
    return-void

    .line 148
    :cond_2
    invoke-direct {p0, p1}, Lasi;->d(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)Lbdh;

    move-result-object v1

    .line 149
    sget-boolean v0, Lasi;->a:Z

    if-eqz v0, :cond_3

    .line 150
    const-string v2, "PSTN_META"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "no pending request for ep="

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v1, :cond_4

    const-string v0, " and no resolved request"

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    :cond_3
    if-eqz v1, :cond_1

    .line 156
    invoke-static {v1}, Lash;->a(Lbdh;)Lash;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->setAttachedData(Ljava/lang/Object;)V

    .line 157
    iget-object v0, v1, Lbdh;->q:Ljava/lang/String;

    invoke-direct {p0, v0}, Lasi;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 150
    :cond_4
    const-string v0, " now attaching data"

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ILaan;Lyj;)V
    .locals 5

    .prologue
    .line 448
    sget-boolean v0, Lasi;->a:Z

    if-eqz v0, :cond_0

    .line 449
    const-string v0, "PSTN_META"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "set PSTN contact info for name="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    :cond_0
    iget-object v0, p0, Lasi;->d:Ljava/util/Map;

    invoke-interface {v0, p4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 458
    iget-object v0, p0, Lasi;->d:Ljava/util/Map;

    invoke-interface {v0, p4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 459
    iget-object v1, p0, Lasi;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 460
    iput-object p1, v0, Lbdh;->e:Ljava/lang/String;

    .line 461
    iput-object p2, v0, Lbdh;->h:Ljava/lang/String;

    .line 462
    const/4 v1, 0x1

    iput-boolean v1, v0, Lbdh;->r:Z

    .line 463
    iput p3, v0, Lbdh;->t:I

    .line 464
    sget-boolean v1, Lasi;->a:Z

    if-eqz v1, :cond_1

    .line 465
    const-string v1, "PSTN_META"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "resolved participant, but no endpoint yet. name="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    :cond_1
    iget-object v1, p0, Lasi;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 468
    iget-object v0, v0, Lbdh;->q:Ljava/lang/String;

    invoke-direct {p0, v0}, Lasi;->a(Ljava/lang/String;)V

    .line 488
    :cond_2
    :goto_0
    return-void

    .line 469
    :cond_3
    iget-object v0, p0, Lasi;->f:Ljava/util/Map;

    invoke-interface {v0, p4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 472
    iget-object v0, p0, Lasi;->f:Ljava/util/Map;

    .line 473
    invoke-interface {v0, p4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    .line 474
    iget-object v1, p0, Lasi;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 475
    invoke-virtual {p4}, Laan;->e()Ljava/lang/String;

    move-result-object v1

    .line 476
    iget-object v2, p0, Lasi;->k:Lbdh;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lasi;->k:Lbdh;

    iget-object v2, v2, Lbdh;->q:Ljava/lang/String;

    .line 477
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;->getPstnJid()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lf;->d(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 478
    iget-object v1, p0, Lasi;->k:Lbdh;

    invoke-virtual {v1}, Lbdh;->c()Ljava/lang/String;

    move-result-object v1

    .line 480
    :cond_4
    sget-boolean v2, Lasi;->a:Z

    if-eqz v2, :cond_5

    .line 481
    const-string v2, "PSTN_META"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "resolved participant, has endpoint. name="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", ep="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 482
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", ph="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 481
    invoke-static {v2, v3}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    :cond_5
    new-instance v2, Lash;

    invoke-direct {v2, p1, v1, p2, p3}, Lash;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;->setAttachedData(Ljava/lang/Object;)V

    .line 486
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;->getPstnJid()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lasi;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lyb;Ljava/lang/String;Lyj;)V
    .locals 2

    .prologue
    .line 422
    const-string v0, "PSTN_META"

    const-string v1, "didn\'t request non-PSTN contact info!"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    return-void
.end method

.method a(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 106
    invoke-static {}, Lcwz;->a()V

    .line 107
    iget-object v0, p0, Lasi;->b:Lapx;

    invoke-virtual {v0}, Lapx;->j()Lyj;

    move-result-object v1

    .line 108
    if-nez v1, :cond_1

    .line 109
    const-string v0, "PSTN_META"

    const-string v1, "Unexpected null account in handleOutgoingInvite"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_0
    return-void

    .line 113
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 114
    if-eqz v0, :cond_2

    iget v3, v0, Lbdh;->a:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_2

    iget-object v3, v0, Lbdh;->c:Ljava/lang/String;

    .line 116
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 117
    invoke-direct {p0, v0}, Lasi;->a(Lbdh;)V

    .line 120
    iget-object v0, v0, Lbdh;->c:Ljava/lang/String;

    invoke-static {v0}, Lbzd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 121
    if-eqz v0, :cond_2

    .line 122
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbor;)V

    .line 123
    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 124
    iget-object v4, p0, Lasi;->h:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    iget-object v4, p0, Lasi;->i:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-boolean v4, Lasi;->a:Z

    if-eqz v4, :cond_2

    .line 127
    const-string v4, "PSTN_META"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "requesting rate for "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " request id="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lyb;)V
    .locals 4

    .prologue
    .line 427
    sget-boolean v0, Lasi;->a:Z

    if-eqz v0, :cond_0

    .line 428
    const-string v0, "PSTN_META"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "contact info failed for request="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lyb;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    :cond_0
    iget-object v0, p0, Lasi;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 431
    iget-object v0, p0, Lasi;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 432
    iget-object v1, p0, Lasi;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 433
    sget-boolean v1, Lasi;->a:Z

    if-eqz v1, :cond_1

    .line 434
    const-string v1, "PSTN_META"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "removed waiting entity, name="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lbdh;->e:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    :cond_1
    :goto_0
    return-void

    .line 436
    :cond_2
    iget-object v0, p0, Lasi;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 437
    iget-object v0, p0, Lasi;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 438
    iget-object v1, p0, Lasi;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 439
    sget-boolean v1, Lasi;->a:Z

    if-eqz v1, :cond_1

    .line 440
    const-string v1, "PSTN_META"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "removed waiting endpoint, name="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method b()V
    .locals 3

    .prologue
    .line 244
    sget-boolean v0, Lasi;->a:Z

    if-eqz v0, :cond_0

    .line 245
    const-string v0, "PSTN_META"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Hangout ended, sizes for pending reqPart="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lasi;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", pending reqEP="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lasi;->e:Ljava/util/Map;

    .line 246
    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", resolvedPart="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lasi;->g:Ljava/util/ArrayList;

    .line 247
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", rateReq="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lasi;->h:Ljava/util/Map;

    .line 248
    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", callinfo="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lasi;->j:Ljava/util/ArrayList;

    .line 249
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 245
    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    :cond_0
    iget-object v0, p0, Lasi;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyb;

    .line 253
    invoke-virtual {v0}, Lyb;->b()V

    goto :goto_0

    .line 256
    :cond_1
    iget-object v0, p0, Lasi;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyb;

    .line 257
    invoke-virtual {v0}, Lyb;->b()V

    goto :goto_1

    .line 260
    :cond_2
    iget-object v0, p0, Lasi;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 261
    iget-object v0, p0, Lasi;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 262
    iget-object v0, p0, Lasi;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 263
    iget-object v0, p0, Lasi;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 264
    iget-object v0, p0, Lasi;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 265
    iget-object v0, p0, Lasi;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 266
    iget-object v0, p0, Lasi;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 267
    iget-object v0, p0, Lasi;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 268
    invoke-direct {p0}, Lasi;->d()V

    .line 270
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    .line 271
    return-void
.end method

.method b(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 5

    .prologue
    .line 163
    invoke-static {}, Lcwz;->a()V

    .line 164
    const/4 v1, 0x0

    .line 169
    iget-object v0, p0, Lasi;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 170
    iget-object v0, p0, Lasi;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyb;

    .line 172
    invoke-virtual {v0}, Lyb;->d()Lbcn;

    move-result-object v2

    .line 173
    if-eqz v2, :cond_0

    .line 174
    iget-object v1, v2, Lbcn;->d:Ljava/lang/String;

    .line 176
    :cond_0
    sget-boolean v2, Lasi;->a:Z

    if-eqz v2, :cond_1

    .line 177
    const-string v2, "PSTN_META"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Endpoint exits, cancelling request id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lyb;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", ph="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    :cond_1
    invoke-virtual {v0}, Lyb;->b()V

    .line 181
    iget-object v2, p0, Lasi;->e:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    iget-object v2, p0, Lasi;->f:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    :cond_2
    :goto_0
    if-eqz v1, :cond_4

    .line 209
    invoke-static {v1}, Lbzd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 211
    iget-object v0, p0, Lasi;->i:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 212
    iget-object v0, p0, Lasi;->i:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 213
    sget-boolean v2, Lasi;->a:Z

    if-eqz v2, :cond_3

    .line 214
    const-string v2, "PSTN_META"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Endpoint exits, removing pending rate request, requestId="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", ph="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    :cond_3
    iget-object v1, p0, Lasi;->h:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    iget-object v0, p0, Lasi;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 220
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    .line 221
    sget-boolean v0, Lasi;->a:Z

    if-eqz v0, :cond_4

    .line 222
    const-string v0, "PSTN_META"

    const-string v1, "Endpoint exits, no more rate requests"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    :cond_4
    :goto_1
    return-void

    .line 184
    :cond_5
    invoke-direct {p0, p1}, Lasi;->c(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)Lbdh;

    move-result-object v0

    .line 185
    if-eqz v0, :cond_7

    .line 186
    iget-object v1, v0, Lbdh;->c:Ljava/lang/String;

    .line 187
    iget-object v2, p0, Lasi;->c:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyb;

    .line 188
    sget-boolean v2, Lasi;->a:Z

    if-eqz v2, :cond_6

    .line 189
    const-string v2, "PSTN_META"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Endpoint exits, has pending participant, cancelling request id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 190
    invoke-virtual {v0}, Lyb;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", ph="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 189
    invoke-static {v2, v3}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    :cond_6
    iget-object v2, p0, Lasi;->d:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    invoke-virtual {v0}, Lyb;->b()V

    goto/16 :goto_0

    .line 196
    :cond_7
    invoke-direct {p0, p1}, Lasi;->d(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)Lbdh;

    move-result-object v0

    .line 197
    if-eqz v0, :cond_2

    .line 198
    iget-object v1, v0, Lbdh;->c:Ljava/lang/String;

    .line 199
    iget-object v2, p0, Lasi;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 200
    sget-boolean v0, Lasi;->a:Z

    if-eqz v0, :cond_2

    .line 201
    const-string v0, "PSTN_META"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Endpoint exits, removing resolved participant, ph="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 226
    :cond_8
    if-eqz v2, :cond_4

    .line 227
    iget-object v0, p0, Lasi;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laqd;

    .line 228
    invoke-virtual {v0}, Laqd;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 229
    sget-boolean v2, Lasi;->a:Z

    if-eqz v2, :cond_a

    .line 230
    const-string v2, "PSTN_META"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Endpoint exits, removing resolved rate request, ph="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    :cond_a
    iget-object v1, p0, Lasi;->j:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 234
    invoke-direct {p0}, Lasi;->d()V

    goto/16 :goto_1
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Laqd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 338
    invoke-static {}, Lcwz;->a()V

    .line 339
    iget-object v0, p0, Lasi;->j:Ljava/util/ArrayList;

    return-object v0
.end method
