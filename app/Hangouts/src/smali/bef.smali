.class public Lbef;
.super Lbed;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:J

.field public final b:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;J[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 4122
    invoke-direct {p0, p1}, Lbed;-><init>(Ljava/lang/String;)V

    .line 4123
    iput-wide p2, p0, Lbef;->a:J

    .line 4124
    iput-object p4, p0, Lbef;->b:[Ljava/lang/String;

    .line 4125
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 4

    .prologue
    .line 4129
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 4130
    const-string v0, "Babel_RequestWriter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Delete conversation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbef;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lbef;->a:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4132
    :cond_0
    new-instance v0, Ldqu;

    invoke-direct {v0}, Ldqu;-><init>()V

    .line 4134
    invoke-virtual {p0, p1, p2}, Lbef;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v1

    iput-object v1, v0, Ldqu;->b:Ldvm;

    .line 4135
    iget-object v1, p0, Lbef;->c:Ljava/lang/String;

    invoke-static {v1}, Lbea;->d(Ljava/lang/String;)Ldqf;

    move-result-object v1

    iput-object v1, v0, Ldqu;->d:Ldqf;

    .line 4136
    iget-object v1, p0, Lbef;->b:[Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 4137
    iget-object v1, p0, Lbef;->b:[Ljava/lang/String;

    iput-object v1, v0, Ldqu;->f:[Ljava/lang/String;

    .line 4138
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldqu;->c:Ljava/lang/Integer;

    .line 4143
    :goto_0
    return-object v0

    .line 4140
    :cond_1
    iget-wide v1, p0, Lbef;->a:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Ldqu;->e:Ljava/lang/Long;

    .line 4141
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldqu;->c:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public a(Lyj;Lbph;)V
    .locals 3

    .prologue
    .line 4163
    const-string v0, "Babel_RequestWriter"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4164
    const-string v0, "Babel_RequestWriter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DeleteConversationRequest: expired for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbef;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4166
    :cond_0
    invoke-super {p0, p1, p2}, Lbed;->a(Lyj;Lbph;)V

    .line 4167
    iget-boolean v0, p0, Lbef;->h:Z

    if-nez v0, :cond_1

    .line 4171
    iget-object v0, p0, Lbef;->c:Ljava/lang/String;

    iget-object v1, p0, Lbef;->b:[Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4173
    :cond_1
    return-void
.end method

.method public a(JI)Z
    .locals 1

    .prologue
    .line 4158
    const/4 v0, 0x0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4148
    const-string v0, "conversations/deleteconversation"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4153
    const-string v0, "event_queue"

    return-object v0
.end method
