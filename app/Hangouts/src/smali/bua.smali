.class public final Lbua;
.super Lbtf;
.source "PG"


# static fields
.field private static final e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lbys;->m:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbua;->e:Z

    return-void
.end method

.method public constructor <init>(Lbtz;Landroid/app/Activity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Lbtf;-><init>(Lbtz;Landroid/app/Activity;Ljava/lang/String;)V

    .line 28
    return-void
.end method


# virtual methods
.method protected a(IZLjava/lang/Integer;IIILandroid/content/SharedPreferences;)Landroid/preference/Preference;
    .locals 7

    .prologue
    .line 34
    iget-object v0, p0, Lbua;->a:Lbtz;

    iget-object v1, p0, Lbua;->b:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 36
    const/4 v4, 0x0

    .line 37
    if-eqz p3, :cond_0

    .line 38
    iget-object v0, p0, Lbua;->b:Landroid/content/res/Resources;

    .line 39
    const/4 v2, 0x0

    invoke-static {p3, v2}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 41
    :cond_0
    iget-object v0, p0, Lbua;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 42
    iget-object v0, p0, Lbua;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 44
    iget-object v0, p0, Lbua;->c:Lyj;

    .line 45
    invoke-static {v0, v4, v5, v6}, Lbbl;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 44
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 49
    invoke-virtual {v1, p2}, Landroid/preference/Preference;->setEnabled(Z)V

    move-object v0, p0

    move-object v2, p7

    move v3, p2

    .line 52
    invoke-virtual/range {v0 .. v6}, Lbua;->a(Landroid/preference/Preference;Landroid/content/SharedPreferences;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    return-object v1
.end method

.method protected f()V
    .locals 3

    .prologue
    .line 60
    iget-object v0, p0, Lbua;->a:Lbtz;

    iget-object v1, p0, Lbua;->b:Landroid/content/res/Resources;

    sget v2, Lh;->kK:I

    .line 61
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 60
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;

    iput-object v0, p0, Lbua;->d:Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;

    .line 63
    const-string v0, "babel_richstatus"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbua;->c:Lyj;

    .line 65
    invoke-virtual {v0}, Lyj;->u()Z

    move-result v0

    if-nez v0, :cond_0

    .line 66
    iget-object v0, p0, Lbua;->d:Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;

    iget-object v1, p0, Lbua;->c:Lyj;

    .line 67
    invoke-static {v1}, Lbbl;->i(Lyj;)Landroid/content/Intent;

    move-result-object v1

    .line 66
    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;->setIntent(Landroid/content/Intent;)V

    .line 69
    invoke-virtual {p0}, Lbua;->i()V

    .line 73
    :goto_0
    return-void

    .line 71
    :cond_0
    iget-object v0, p0, Lbua;->a:Lbtz;

    iget-object v1, p0, Lbua;->d:Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;

    invoke-interface {v0, v1}, Lbtz;->a(Landroid/preference/Preference;)V

    goto :goto_0
.end method

.method protected g()V
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lbua;->a:Lbtz;

    iget-object v1, p0, Lbua;->b:Landroid/content/res/Resources;

    sget v2, Lh;->bu:I

    .line 79
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 78
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 81
    iget-object v1, p0, Lbua;->c:Lyj;

    invoke-static {v1}, Lbbl;->j(Lyj;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setIntent(Landroid/content/Intent;)V

    .line 82
    return-void
.end method
