.class public final Lji;
.super Lfh;
.source "PG"


# instance fields
.field final synthetic b:Landroid/support/v4/widget/DrawerLayout;

.field private final c:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/support/v4/widget/DrawerLayout;)V
    .locals 1

    .prologue
    .line 1667
    iput-object p1, p0, Lji;->b:Landroid/support/v4/widget/DrawerLayout;

    invoke-direct {p0}, Lfh;-><init>()V

    .line 1668
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lji;->c:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Lii;)V
    .locals 4

    .prologue
    .line 1672
    invoke-static {p2}, Lii;->a(Lii;)Lii;

    move-result-object v1

    .line 1673
    invoke-super {p0, p1, v1}, Lfh;->a(Landroid/view/View;Lii;)V

    .line 1675
    const-class v0, Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lii;->b(Ljava/lang/CharSequence;)V

    .line 1676
    invoke-virtual {p2, p1}, Lii;->a(Landroid/view/View;)V

    .line 1677
    invoke-static {p1}, Lgz;->e(Landroid/view/View;)Landroid/view/ViewParent;

    move-result-object v0

    .line 1678
    instance-of v2, v0, Landroid/view/View;

    if-eqz v2, :cond_0

    .line 1679
    check-cast v0, Landroid/view/View;

    invoke-virtual {p2, v0}, Lii;->c(Landroid/view/View;)V

    .line 1681
    :cond_0
    iget-object v0, p0, Lji;->c:Landroid/graphics/Rect;

    invoke-virtual {v1, v0}, Lii;->a(Landroid/graphics/Rect;)V

    invoke-virtual {p2, v0}, Lii;->b(Landroid/graphics/Rect;)V

    invoke-virtual {v1, v0}, Lii;->c(Landroid/graphics/Rect;)V

    invoke-virtual {p2, v0}, Lii;->d(Landroid/graphics/Rect;)V

    invoke-virtual {v1}, Lii;->e()Z

    move-result v0

    invoke-virtual {p2, v0}, Lii;->c(Z)V

    invoke-virtual {v1}, Lii;->k()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Lii;->a(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Lii;->l()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Lii;->b(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Lii;->m()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Lii;->c(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Lii;->j()Z

    move-result v0

    invoke-virtual {p2, v0}, Lii;->h(Z)V

    invoke-virtual {v1}, Lii;->h()Z

    move-result v0

    invoke-virtual {p2, v0}, Lii;->f(Z)V

    invoke-virtual {v1}, Lii;->c()Z

    move-result v0

    invoke-virtual {p2, v0}, Lii;->a(Z)V

    invoke-virtual {v1}, Lii;->d()Z

    move-result v0

    invoke-virtual {p2, v0}, Lii;->b(Z)V

    invoke-virtual {v1}, Lii;->f()Z

    move-result v0

    invoke-virtual {p2, v0}, Lii;->d(Z)V

    invoke-virtual {v1}, Lii;->g()Z

    move-result v0

    invoke-virtual {p2, v0}, Lii;->e(Z)V

    invoke-virtual {v1}, Lii;->i()Z

    move-result v0

    invoke-virtual {p2, v0}, Lii;->g(Z)V

    invoke-virtual {v1}, Lii;->b()I

    move-result v0

    invoke-virtual {p2, v0}, Lii;->a(I)V

    .line 1683
    invoke-virtual {v1}, Lii;->n()V

    .line 1685
    check-cast p1, Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v4/widget/DrawerLayout;->l(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p2, v2}, Lii;->b(Landroid/view/View;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1686
    :cond_2
    return-void
.end method

.method public a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 1732
    invoke-static {p2}, Landroid/support/v4/widget/DrawerLayout;->l(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1733
    invoke-super {p0, p1, p2, p3}, Lfh;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    .line 1735
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 3

    .prologue
    .line 1702
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_1

    .line 1703
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    .line 1704
    iget-object v1, p0, Lji;->b:Landroid/support/v4/widget/DrawerLayout;

    invoke-static {v1}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/support/v4/widget/DrawerLayout;)Landroid/view/View;

    move-result-object v1

    .line 1705
    if-eqz v1, :cond_0

    .line 1706
    iget-object v2, p0, Lji;->b:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v2, v1}, Landroid/support/v4/widget/DrawerLayout;->e(Landroid/view/View;)I

    move-result v1

    .line 1707
    iget-object v2, p0, Lji;->b:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v2, v1}, Landroid/support/v4/widget/DrawerLayout;->a(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1708
    if-eqz v1, :cond_0

    .line 1709
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1713
    :cond_0
    const/4 v0, 0x1

    .line 1716
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Lfh;->b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 1690
    invoke-super {p0, p1, p2}, Lfh;->d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1692
    const-class v0, Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 1693
    return-void
.end method
