.class public final Lvh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Lus;

    invoke-direct {v0, p0, p1}, Lus;-><init>(Lvh;Landroid/os/Handler;)V

    iput-object v0, p0, Lvh;->a:Ljava/util/concurrent/Executor;

    .line 42
    return-void
.end method


# virtual methods
.method public a(Lva;Lve;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lva",
            "<*>;",
            "Lve",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lvh;->a(Lva;Lve;Ljava/lang/Runnable;)V

    .line 56
    return-void
.end method

.method public a(Lva;Lve;Ljava/lang/Runnable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lva",
            "<*>;",
            "Lve",
            "<*>;",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p1}, Lva;->l()V

    .line 61
    const-string v0, "post-response"

    invoke-virtual {p1, v0}, Lva;->a(Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lvh;->a:Ljava/util/concurrent/Executor;

    new-instance v1, Lut;

    invoke-direct {v1, p0, p1, p2, p3}, Lut;-><init>(Lvh;Lva;Lve;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 63
    return-void
.end method

.method public a(Lva;Lvl;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lva",
            "<*>;",
            "Lvl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 67
    const-string v0, "post-error"

    invoke-virtual {p1, v0}, Lva;->a(Ljava/lang/String;)V

    .line 68
    invoke-static {p2}, Lve;->a(Lvl;)Lve;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lvh;->a:Ljava/util/concurrent/Executor;

    new-instance v2, Lut;

    const/4 v3, 0x0

    invoke-direct {v2, p0, p1, v0, v3}, Lut;-><init>(Lvh;Lva;Lve;Ljava/lang/Runnable;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 70
    return-void
.end method
