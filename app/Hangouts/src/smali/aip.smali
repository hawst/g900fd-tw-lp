.class public final Laip;
.super Lbai;
.source "PG"


# instance fields
.field final synthetic j:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

.field private final k:Landroid/content/Context;

.field private final l:Lyj;

.field private final m:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

.field private n:J

.field private o:J

.field private p:J


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;Landroid/content/Context;Lyj;Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 645
    iput-object p1, p0, Laip;->j:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    .line 646
    invoke-direct {p0, p2}, Lbai;-><init>(Landroid/content/Context;)V

    .line 641
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Laip;->n:J

    .line 950
    iput-wide v2, p0, Laip;->o:J

    .line 951
    iput-wide v2, p0, Laip;->p:J

    .line 647
    iput-object p2, p0, Laip;->k:Landroid/content/Context;

    .line 648
    iput-object p3, p0, Laip;->l:Lyj;

    .line 649
    iput-object p4, p0, Laip;->m:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    .line 650
    return-void
.end method

.method private b(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 12

    .prologue
    .line 860
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v0

    iget-object v1, p0, Laip;->l:Lyj;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Lyj;)J

    move-result-wide v4

    .line 862
    invoke-static {p1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteSetListItem;

    .line 864
    const/16 v1, 0x18

    .line 865
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 866
    const/16 v2, 0x13

    .line 867
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 871
    if-eqz v2, :cond_0

    if-nez v1, :cond_1

    .line 917
    :cond_0
    :goto_0
    return-void

    .line 874
    :cond_1
    const-string v3, "\\|"

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 875
    const-string v1, "\\|"

    invoke-virtual {v2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 877
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 879
    const v2, 0x7fffffff

    .line 880
    array-length v1, v6

    add-int/lit8 v1, v1, -0x1

    move v3, v1

    :goto_1
    if-ltz v3, :cond_5

    .line 881
    aget-object v9, v6, v3

    .line 885
    array-length v1, v7

    if-ge v3, v1, :cond_4

    aget-object v1, v7, v3

    .line 887
    :goto_2
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 889
    const v1, 0x7fffffff

    if-ne v2, v1, :cond_6

    cmp-long v1, v4, v10

    if-ltz v1, :cond_6

    .line 890
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    .line 893
    :goto_3
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 894
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_2

    .line 895
    const-string v2, ", "

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 897
    :cond_2
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 880
    :cond_3
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    move v2, v1

    goto :goto_1

    .line 885
    :cond_4
    const-string v1, "0"

    goto :goto_2

    .line 901
    :cond_5
    iget-object v1, p0, Laip;->k:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 902
    sget v1, Lg;->dP:I

    .line 903
    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteSetListItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 906
    sget v4, Lh;->gr:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/16 v7, 0x16

    .line 907
    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->b(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 906
    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 909
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 910
    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v4, 0x1

    invoke-direct {v1, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v4, 0x0

    .line 912
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    const/4 v5, 0x0

    .line 910
    invoke-virtual {v3, v1, v4, v2, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 913
    sget v1, Lg;->dO:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteSetListItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 914
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 916
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteSetListItem;->setSelected(Z)V

    goto/16 :goto_0

    :cond_6
    move v1, v2

    goto :goto_3
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 654
    const-string v0, "layout_inflater"

    .line 655
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 657
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    invoke-virtual {p0, v1}, Laip;->getItemViewType(I)I

    move-result v2

    .line 658
    packed-switch v2, :pswitch_data_0

    move-object v0, v3

    .line 686
    :goto_0
    return-object v0

    .line 660
    :pswitch_0
    sget v1, Lf;->eK:I

    .line 672
    :goto_1
    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->s()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 673
    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "newView viewType: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    packed-switch v2, :pswitch_data_1

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "UNKNOWN viewType: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " id: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 674
    sget v2, Lf;->eK:I

    if-ne v1, v2, :cond_2

    const-string v2, "conversation_list_item_view"

    :goto_3
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 673
    invoke-static {v4, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    :cond_0
    invoke-virtual {v0, v1, v3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 677
    instance-of v1, v2, Lcom/google/android/apps/hangouts/views/ConversationListItemView;

    if-eqz v1, :cond_1

    move-object v1, v2

    .line 678
    check-cast v1, Lcom/google/android/apps/hangouts/views/ConversationListItemView;

    .line 679
    iget-object v3, p0, Laip;->m:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->c(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)Lair;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/hangouts/views/ConversationListItemView;->a(Lair;)V

    .line 682
    :cond_1
    sget v1, Lf;->eL:I

    .line 683
    invoke-virtual {v0, v1, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;

    .line 685
    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->a(Landroid/view/View;)V

    goto :goto_0

    .line 663
    :pswitch_1
    sget v1, Lf;->fJ:I

    goto :goto_1

    .line 666
    :pswitch_2
    sget v1, Lf;->fL:I

    goto :goto_1

    .line 673
    :pswitch_3
    const-string v2, "VIEW_TYPE_CONVERSATION"

    goto :goto_2

    :pswitch_4
    const-string v2, "VIEW_TYPE_INVITATION"

    goto :goto_2

    :pswitch_5
    const-string v2, "VIEW_TYPE_INVITATION_SET"

    goto :goto_2

    .line 674
    :cond_2
    sget v2, Lf;->fJ:I

    if-ne v1, v2, :cond_3

    const-string v2, "invite_list_item_view"

    goto :goto_3

    :cond_3
    sget v2, Lf;->fL:I

    if-ne v1, v2, :cond_4

    const-string v2, "invite_set_list_item_view"

    goto :goto_3

    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "UNKNOWN viewId: "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 658
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 673
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public a(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 12

    .prologue
    .line 962
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    move-object v0, p1

    .line 964
    check-cast v0, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;

    .line 965
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->g()V

    .line 967
    :cond_0
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    invoke-virtual {p0, v0}, Laip;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 978
    const-string v0, "Babel"

    const-string v1, "bindView called with cursor at unknown position."

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 981
    :cond_1
    :goto_0
    check-cast p1, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;

    invoke-virtual {p1}, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->f()Landroid/view/View;

    move-result-object v0

    .line 982
    instance-of v1, v0, Lbzz;

    if-eqz v1, :cond_2

    .line 983
    check-cast v0, Lbzz;

    .line 985
    iget-object v1, p0, Laip;->m:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    iget-object v1, v1, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a:Laij;

    invoke-virtual {v0, v1}, Lbzz;->a(Laij;)V

    .line 986
    invoke-static {p2}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->c(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbzz;->a(Ljava/lang/String;)V

    .line 987
    const/4 v1, 0x4

    .line 988
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 987
    invoke-virtual {v0, v1, v2}, Lbzz;->a(J)V

    .line 990
    :cond_2
    return-void

    .line 969
    :pswitch_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    const/16 v0, 0xe

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcwz;->b(Z)V

    invoke-static {p1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/ConversationListItemView;

    iget-object v1, p0, Laip;->m:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)I

    move-result v1

    const/4 v4, 0x3

    if-ne v1, v4, :cond_5

    const/4 v1, 0x1

    :goto_2
    iget-object v4, p0, Laip;->l:Lyj;

    const/4 v5, 0x0

    invoke-virtual {v0, p2, v4, v1, v5}, Lcom/google/android/apps/hangouts/views/ConversationListItemView;->a(Landroid/database/Cursor;Lyj;ZLjava/lang/Object;)V

    const/4 v1, 0x4

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iget-wide v6, p0, Laip;->n:J

    cmp-long v1, v6, v4

    if-lez v1, :cond_3

    iput-wide v4, p0, Laip;->n:J

    :cond_3
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/ConversationListItemView;->setSelected(Z)V

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->s()Z

    move-result v4

    if-eqz v4, :cond_1

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iget-wide v2, p0, Laip;->o:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Laip;->o:J

    iget-wide v2, p0, Laip;->p:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Laip;->p:J

    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BIND: CURRENT: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AVERAGE: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v3, p0, Laip;->o:J

    iget-wide v5, p0, Laip;->p:J

    div-long/2addr v3, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v3, p0, Laip;->p:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_2

    .line 972
    :pswitch_1
    invoke-direct {p0, p1, p2}, Laip;->b(Landroid/view/View;Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 975
    :pswitch_2
    invoke-static {p1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;

    invoke-static {p2}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->c(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v6

    const/4 v1, 0x3

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    const/4 v1, 0x1

    move v3, v1

    :goto_3
    sget v1, Lg;->dT:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v2, Lg;->dQ:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v4, p0, Laip;->j:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-virtual {v4}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    if-eqz v3, :cond_c

    const/4 v3, 0x7

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    const/4 v4, 0x6

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/16 v4, 0x12

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_9

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_7

    sget v4, Lh;->fJ:I

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lf;->ho:I

    add-int/lit8 v8, v3, 0x1

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v9, v10

    invoke-virtual {v7, v5, v8, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :goto_4
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_5
    const/16 v1, 0x14

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Laip;->l:Lyj;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->a(Ljava/lang/String;Lyj;)V

    iget-object v1, p0, Laip;->l:Lyj;

    const/16 v2, 0x12

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x10

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v6, v2, v3}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->setSelected(Z)V

    iget-object v1, p0, Laip;->j:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)I

    move-result v1

    if-nez v1, :cond_d

    const/4 v1, 0x1

    :goto_6
    if-eqz v1, :cond_e

    sget v1, Lf;->cs:I

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->d:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    sget v1, Lg;->dT:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    sget v1, Lg;->dQ:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    :cond_6
    const/4 v1, 0x0

    move v3, v1

    goto/16 :goto_3

    :cond_7
    if-nez v3, :cond_8

    sget v3, Lh;->fO:I

    invoke-virtual {v7, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    :cond_8
    sget v5, Lf;->hq:I

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v7, v5, v3, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    :cond_9
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_a

    sget v4, Lf;->ho:I

    add-int/lit8 v8, v3, 0x1

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v9, v10

    invoke-virtual {v7, v4, v8, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v4, v5

    goto/16 :goto_4

    :cond_a
    if-nez v3, :cond_b

    sget v3, Lh;->fN:I

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v4, v8, v9

    invoke-virtual {v7, v3, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v4, v5

    goto/16 :goto_4

    :cond_b
    sget v8, Lf;->hp:I

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    aput-object v4, v9, v10

    invoke-virtual {v7, v8, v3, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v4, v5

    goto/16 :goto_4

    :cond_c
    const/16 v3, 0x12

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v1, 0x8

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    packed-switch v1, :pswitch_data_1

    :pswitch_3
    sget v1, Lh;->fK:I

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_5

    :pswitch_4
    sget v1, Lh;->fL:I

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_5

    :pswitch_5
    sget v1, Lh;->fM:I

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_5

    :cond_d
    const/4 v1, 0x0

    goto/16 :goto_6

    :cond_e
    sget v1, Lf;->cr:I

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->c:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    sget v1, Lg;->dT:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    sget v1, Lg;->dQ:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 967
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    .line 975
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public c()V
    .locals 4

    .prologue
    .line 1000
    iget-wide v0, p0, Laip;->n:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1002
    iget-object v0, p0, Laip;->l:Lyj;

    iget-wide v1, p0, Laip;->n:J

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;J)I

    .line 1005
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 995
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Laip;->n:J

    .line 996
    return-void
.end method

.method public getItemViewType(I)I
    .locals 6

    .prologue
    const/16 v5, 0xe

    const/4 v0, 0x2

    const/4 v1, 0x1

    .line 721
    invoke-virtual {p0}, Laip;->a()Landroid/database/Cursor;

    move-result-object v2

    .line 722
    invoke-interface {v2, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 723
    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->s()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 724
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getItemViewType position "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 725
    invoke-interface {v2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 724
    invoke-static {v3}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d(Ljava/lang/String;)V

    .line 727
    :cond_0
    invoke-interface {v2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-ne v3, v0, :cond_2

    .line 728
    const/4 v0, 0x0

    .line 733
    :cond_1
    :goto_0
    return v0

    .line 730
    :cond_2
    const/16 v3, 0x16

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-gt v2, v1, :cond_1

    move v0, v1

    .line 733
    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 716
    const/4 v0, 0x3

    return v0
.end method
