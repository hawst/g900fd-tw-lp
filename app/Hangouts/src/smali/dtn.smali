.class public final Ldtn;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldtn;


# instance fields
.field public b:[Ldtm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3239
    const/4 v0, 0x0

    new-array v0, v0, [Ldtn;

    sput-object v0, Ldtn;->a:[Ldtn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3240
    invoke-direct {p0}, Lepn;-><init>()V

    .line 3243
    sget-object v0, Ldtm;->a:[Ldtm;

    iput-object v0, p0, Ldtn;->b:[Ldtm;

    .line 3240
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 3262
    iget-object v1, p0, Ldtn;->b:[Ldtm;

    if-eqz v1, :cond_1

    .line 3263
    iget-object v2, p0, Ldtn;->b:[Ldtm;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 3264
    if-eqz v4, :cond_0

    .line 3265
    const/4 v5, 0x1

    .line 3266
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 3263
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3270
    :cond_1
    iget-object v1, p0, Ldtn;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3271
    iput v0, p0, Ldtn;->cachedSize:I

    .line 3272
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3236
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldtn;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldtn;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldtn;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldtn;->b:[Ldtm;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldtm;

    iget-object v3, p0, Ldtn;->b:[Ldtm;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldtn;->b:[Ldtm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldtn;->b:[Ldtm;

    :goto_2
    iget-object v2, p0, Ldtn;->b:[Ldtm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldtn;->b:[Ldtm;

    new-instance v3, Ldtm;

    invoke-direct {v3}, Ldtm;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldtn;->b:[Ldtm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldtn;->b:[Ldtm;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldtn;->b:[Ldtm;

    new-instance v3, Ldtm;

    invoke-direct {v3}, Ldtm;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldtn;->b:[Ldtm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 3248
    iget-object v0, p0, Ldtn;->b:[Ldtm;

    if-eqz v0, :cond_1

    .line 3249
    iget-object v1, p0, Ldtn;->b:[Ldtm;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 3250
    if-eqz v3, :cond_0

    .line 3251
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 3249
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3255
    :cond_1
    iget-object v0, p0, Ldtn;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 3257
    return-void
.end method
