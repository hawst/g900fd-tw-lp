.class public final Ldvq;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldvq;


# instance fields
.field public b:Ldue;

.field public c:[Ldtb;

.field public d:[Ldvo;

.field public e:Ldxa;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15778
    const/4 v0, 0x0

    new-array v0, v0, [Ldvq;

    sput-object v0, Ldvq;->a:[Ldvq;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 15779
    invoke-direct {p0}, Lepn;-><init>()V

    .line 15782
    iput-object v1, p0, Ldvq;->b:Ldue;

    .line 15785
    sget-object v0, Ldtb;->a:[Ldtb;

    iput-object v0, p0, Ldvq;->c:[Ldtb;

    .line 15788
    sget-object v0, Ldvo;->a:[Ldvo;

    iput-object v0, p0, Ldvq;->d:[Ldvo;

    .line 15791
    iput-object v1, p0, Ldvq;->e:Ldxa;

    .line 15779
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 15823
    iget-object v0, p0, Ldvq;->b:Ldue;

    if-eqz v0, :cond_5

    .line 15824
    const/4 v0, 0x1

    iget-object v2, p0, Ldvq;->b:Ldue;

    .line 15825
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 15827
    :goto_0
    iget-object v2, p0, Ldvq;->c:[Ldtb;

    if-eqz v2, :cond_1

    .line 15828
    iget-object v3, p0, Ldvq;->c:[Ldtb;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 15829
    if-eqz v5, :cond_0

    .line 15830
    const/4 v6, 0x2

    .line 15831
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 15828
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 15835
    :cond_1
    iget-object v2, p0, Ldvq;->d:[Ldvo;

    if-eqz v2, :cond_3

    .line 15836
    iget-object v2, p0, Ldvq;->d:[Ldvo;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 15837
    if-eqz v4, :cond_2

    .line 15838
    const/4 v5, 0x3

    .line 15839
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 15836
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 15843
    :cond_3
    iget-object v1, p0, Ldvq;->e:Ldxa;

    if-eqz v1, :cond_4

    .line 15844
    const/4 v1, 0x4

    iget-object v2, p0, Ldvq;->e:Ldxa;

    .line 15845
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15847
    :cond_4
    iget-object v1, p0, Ldvq;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15848
    iput v0, p0, Ldvq;->cachedSize:I

    .line 15849
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 15775
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldvq;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldvq;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldvq;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldvq;->b:Ldue;

    if-nez v0, :cond_2

    new-instance v0, Ldue;

    invoke-direct {v0}, Ldue;-><init>()V

    iput-object v0, p0, Ldvq;->b:Ldue;

    :cond_2
    iget-object v0, p0, Ldvq;->b:Ldue;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldvq;->c:[Ldtb;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldtb;

    iget-object v3, p0, Ldvq;->c:[Ldtb;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldvq;->c:[Ldtb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldvq;->c:[Ldtb;

    :goto_2
    iget-object v2, p0, Ldvq;->c:[Ldtb;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldvq;->c:[Ldtb;

    new-instance v3, Ldtb;

    invoke-direct {v3}, Ldtb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldvq;->c:[Ldtb;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldvq;->c:[Ldtb;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldvq;->c:[Ldtb;

    new-instance v3, Ldtb;

    invoke-direct {v3}, Ldtb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldvq;->c:[Ldtb;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldvq;->d:[Ldvo;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldvo;

    iget-object v3, p0, Ldvq;->d:[Ldvo;

    if-eqz v3, :cond_6

    iget-object v3, p0, Ldvq;->d:[Ldvo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Ldvq;->d:[Ldvo;

    :goto_4
    iget-object v2, p0, Ldvq;->d:[Ldvo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Ldvq;->d:[Ldvo;

    new-instance v3, Ldvo;

    invoke-direct {v3}, Ldvo;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldvq;->d:[Ldvo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Ldvq;->d:[Ldvo;

    array-length v0, v0

    goto :goto_3

    :cond_8
    iget-object v2, p0, Ldvq;->d:[Ldvo;

    new-instance v3, Ldvo;

    invoke-direct {v3}, Ldvo;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldvq;->d:[Ldvo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Ldvq;->e:Ldxa;

    if-nez v0, :cond_9

    new-instance v0, Ldxa;

    invoke-direct {v0}, Ldxa;-><init>()V

    iput-object v0, p0, Ldvq;->e:Ldxa;

    :cond_9
    iget-object v0, p0, Ldvq;->e:Ldxa;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 15796
    iget-object v1, p0, Ldvq;->b:Ldue;

    if-eqz v1, :cond_0

    .line 15797
    const/4 v1, 0x1

    iget-object v2, p0, Ldvq;->b:Ldue;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 15799
    :cond_0
    iget-object v1, p0, Ldvq;->c:[Ldtb;

    if-eqz v1, :cond_2

    .line 15800
    iget-object v2, p0, Ldvq;->c:[Ldtb;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 15801
    if-eqz v4, :cond_1

    .line 15802
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 15800
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 15806
    :cond_2
    iget-object v1, p0, Ldvq;->d:[Ldvo;

    if-eqz v1, :cond_4

    .line 15807
    iget-object v1, p0, Ldvq;->d:[Ldvo;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 15808
    if-eqz v3, :cond_3

    .line 15809
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 15807
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 15813
    :cond_4
    iget-object v0, p0, Ldvq;->e:Ldxa;

    if-eqz v0, :cond_5

    .line 15814
    const/4 v0, 0x4

    iget-object v1, p0, Ldvq;->e:Ldxa;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 15816
    :cond_5
    iget-object v0, p0, Ldvq;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 15818
    return-void
.end method
