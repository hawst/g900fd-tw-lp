.class public final Lajg;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final k:Z


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;

.field public c:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lajk;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

.field public f:Lyj;

.field public g:Lbdh;

.field public h:I

.field private i:Landroid/os/Handler;

.field private j:Ljava/lang/String;

.field private final l:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lajk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 854
    const-string v0, "babel_conversation_variant_use_dynamic_signals"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lajg;->k:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;ZLbdh;)V
    .locals 2

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lajg;->c:Ljava/util/HashSet;

    .line 75
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lajg;->i:Landroid/os/Handler;

    .line 1061
    new-instance v0, Lajj;

    invoke-direct {v0, p0}, Lajj;-><init>(Lajg;)V

    iput-object v0, p0, Lajg;->l:Ljava/util/Comparator;

    .line 151
    iput-object p1, p0, Lajg;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 152
    iput-object p4, p0, Lajg;->g:Lbdh;

    .line 153
    iput-object p2, p0, Lajg;->b:Ljava/lang/String;

    .line 154
    iput-boolean p3, p0, Lajg;->a:Z

    .line 155
    return-void
.end method

.method private static a(Lbdh;ZJ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 948
    iget-object v0, p0, Lbdh;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 949
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s [timestamp=%d]"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lbdh;->c:Ljava/lang/String;

    aput-object v3, v2, v4

    .line 951
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v5

    .line 949
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 953
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s [reachable=%b; timestamp=%d]"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    .line 954
    invoke-virtual {p0}, Lbdh;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    .line 955
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    .line 956
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    .line 953
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/util/Collection;Lbsc;)Ljava/util/Collection;
    .locals 29
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lahf;",
            ">;",
            "Lbsc;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lajk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 356
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lajg;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a()Lyj;

    move-result-object v22

    .line 357
    if-nez v22, :cond_0

    .line 358
    const/4 v2, 0x0

    .line 463
    :goto_0
    return-object v2

    .line 361
    :cond_0
    new-instance v23, Les;

    invoke-direct/range {v23 .. v23}, Les;-><init>()V

    .line 366
    invoke-virtual/range {v22 .. v22}, Lyj;->c()Lbdk;

    move-result-object v2

    .line 367
    invoke-static {}, Lbzd;->g()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 373
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 365
    invoke-static/range {v2 .. v9}, Lbdh;->a(Lbdk;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lbdh;

    move-result-object v24

    .line 376
    new-instance v25, Lyt;

    move-object/from16 v0, v25

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lyt;-><init>(Lyj;)V

    .line 377
    new-instance v26, Lajl;

    invoke-direct/range {v26 .. v26}, Lajl;-><init>()V

    .line 381
    invoke-interface/range {p2 .. p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :cond_1
    :goto_1
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lahf;

    move-object/from16 v21, v0

    .line 384
    move-object/from16 v0, v21

    iget-object v2, v0, Lahf;->a:Ljava/lang/String;

    .line 385
    move-object/from16 v0, v25

    invoke-static {v0, v2}, Lajg;->a(Lyt;Ljava/lang/String;)Lyv;

    move-result-object v9

    .line 386
    if-nez v9, :cond_2

    .line 388
    const/4 v2, 0x0

    goto :goto_0

    .line 392
    :cond_2
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lajg;->f:Lyj;

    invoke-virtual {v2}, Lyj;->u()Z

    move-result v5

    iget-object v2, v9, Lyv;->h:Ljava/util/List;

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    iget-object v3, v9, Lyv;->h:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v2

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbdh;

    if-nez v3, :cond_4

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Lbdh;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v2, 0x1

    move v3, v2

    goto :goto_2

    :cond_3
    if-eqz v5, :cond_4

    iget-object v3, v2, Lbdh;->c:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, v2, Lbdh;->c:Ljava/lang/String;

    move-object/from16 v0, v24

    iget-object v7, v0, Lbdh;->c:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v2, 0x1

    move v3, v2

    goto :goto_2

    :cond_4
    move-object v4, v2

    .line 396
    :cond_5
    if-eqz v4, :cond_6

    iget-object v2, v4, Lbdh;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, v4, Lbdh;->c:Ljava/lang/String;

    move-object/from16 v0, v25

    move-object/from16 v1, p3

    invoke-virtual {v0, v2, v1}, Lyt;->b(Ljava/lang/String;Lbsc;)Laea;

    move-result-object v10

    .line 400
    :goto_3
    if-eqz v10, :cond_8

    .line 401
    invoke-virtual {v10}, Laea;->j()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Laeh;

    iget-object v5, v2, Laeh;->a:Ljava/lang/String;

    invoke-static {v5}, Lbzd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v5, v2}, Les;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    .line 461
    :catch_0
    move-exception v2

    .line 462
    const-string v3, "Babel"

    const-string v4, ">>>>>>>>>> got exception computing variants"

    invoke-static {v3, v4, v2}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 463
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 396
    :cond_6
    if-eqz v4, :cond_7

    :try_start_1
    invoke-virtual {v4}, Lbdh;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    invoke-virtual {v4}, Lbdh;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lbsc;->a(Ljava/lang/String;)Laea;

    move-result-object v10

    goto :goto_3

    :cond_7
    const/4 v10, 0x0

    goto :goto_3

    .line 415
    :cond_8
    if-eqz v4, :cond_c

    if-eqz v10, :cond_9

    if-eqz v9, :cond_c

    .line 416
    :cond_9
    iget-object v2, v4, Lbdh;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    iget-object v2, v4, Lbdh;->c:Ljava/lang/String;

    invoke-static {v2}, Lbzd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Les;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Laeh;

    move-object v8, v0

    new-instance v2, Lajk;

    move-object/from16 v0, v21

    iget-object v3, v0, Lahf;->a:Ljava/lang/String;

    iget-object v5, v4, Lbdh;->c:Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, v4, Lbdh;->c:Ljava/lang/String;

    if-eqz v8, :cond_a

    invoke-virtual {v8}, Laeh;->f()Ljava/lang/String;

    move-result-object v8

    :goto_5
    const/4 v10, 0x0

    move-object/from16 v0, v21

    iget v11, v0, Lahf;->b:I

    invoke-direct/range {v2 .. v11}, Lajk;-><init>(Ljava/lang/String;Lbdh;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lyv;Laea;I)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Lajl;->a(Lajk;)V

    goto/16 :goto_1

    :cond_a
    const/4 v8, 0x0

    goto :goto_5

    :cond_b
    invoke-virtual {v4}, Lbdh;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Lajk;

    move-object/from16 v0, v21

    iget-object v3, v0, Lahf;->a:Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, v4, Lbdh;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lajg;->g:Lbdh;

    iget-object v7, v7, Lbdh;->e:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, v21

    iget v11, v0, Lahf;->b:I

    invoke-direct/range {v2 .. v11}, Lajk;-><init>(Ljava/lang/String;Lbdh;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lyv;Laea;I)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Lajl;->a(Lajk;)V

    goto/16 :goto_1

    .line 422
    :cond_c
    if-eqz v10, :cond_1

    .line 423
    invoke-virtual {v10}, Laea;->c()Ljava/lang/String;

    move-result-object v2

    .line 426
    invoke-virtual/range {v22 .. v22}, Lyj;->u()Z

    move-result v3

    if-nez v3, :cond_1

    .line 427
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 428
    invoke-virtual/range {v24 .. v24}, Lbdh;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 429
    invoke-virtual {v10}, Laea;->k()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Laef;

    iget-object v7, v2, Laef;->a:Ljava/lang/String;

    :goto_6
    if-eqz v4, :cond_12

    const-string v2, ""

    const-string v3, ""

    if-eqz v4, :cond_1e

    iget-object v5, v4, Lbdh;->c:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1e

    iget-object v2, v4, Lbdh;->c:Ljava/lang/String;

    invoke-static {v2}, Lbzd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1d

    if-eqz v10, :cond_1d

    invoke-virtual {v10}, Laea;->g()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1d

    invoke-virtual {v10}, Laea;->g()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_d
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Laee;

    instance-of v8, v2, Laeh;

    if-eqz v8, :cond_d

    check-cast v2, Laeh;

    invoke-virtual {v2}, Laeh;->e()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_d

    invoke-virtual {v2}, Laeh;->e()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lbzd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_d

    invoke-virtual {v2}, Laeh;->f()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_d

    invoke-virtual {v2}, Laeh;->f()Ljava/lang/String;

    move-result-object v2

    move-object v3, v5

    :goto_7
    new-instance v8, Lajm;

    move-object/from16 v0, p0

    invoke-direct {v8, v0, v3, v2}, Lajm;-><init>(Lajg;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v21

    iget v2, v0, Lahf;->b:I

    invoke-static {v2}, Lf;->c(I)Z

    move-result v11

    new-instance v2, Lajk;

    move-object/from16 v0, v21

    iget-object v3, v0, Lahf;->a:Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, v4, Lbdh;->g:Ljava/lang/String;

    if-eqz v11, :cond_e

    iget-object v7, v8, Lajm;->a:Ljava/lang/String;

    :cond_e
    if-eqz v11, :cond_11

    iget-object v8, v8, Lajm;->b:Ljava/lang/String;

    :goto_8
    move-object/from16 v0, v21

    iget v11, v0, Lahf;->b:I

    invoke-direct/range {v2 .. v11}, Lajk;-><init>(Ljava/lang/String;Lbdh;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lyv;Laea;I)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Lajl;->a(Lajk;)V

    goto/16 :goto_1

    :cond_f
    if-eqz v4, :cond_10

    iget-object v2, v4, Lbdh;->e:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_10

    iget-object v7, v4, Lbdh;->e:Ljava/lang/String;

    goto/16 :goto_6

    :cond_10
    const/4 v7, 0x0

    goto/16 :goto_6

    :cond_11
    const/4 v8, 0x0

    goto :goto_8

    :cond_12
    invoke-virtual {v10}, Laea;->c()Ljava/lang/String;

    move-result-object v11

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v10}, Laea;->e()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-virtual {v10}, Laea;->f()Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x0

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v20

    move-object v12, v11

    invoke-static/range {v11 .. v20}, Lbdh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lbdh;

    move-result-object v4

    iget-object v2, v4, Lbdh;->b:Lbdk;

    const/4 v3, 0x0

    const/4 v5, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v2, v3, v5}, Lyt;->a(Lbdk;ZI)Lzi;

    move-result-object v2

    if-eqz v2, :cond_13

    iget-object v2, v2, Lzi;->b:Ljava/lang/String;

    :goto_9
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Lajk;

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {v10}, Laea;->e()Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, v21

    iget v11, v0, Lahf;->b:I

    invoke-direct/range {v2 .. v11}, Lajk;-><init>(Ljava/lang/String;Lbdh;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lyv;Laea;I)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Lajl;->a(Lajk;)V

    goto/16 :goto_1

    :cond_13
    iget-object v2, v4, Lbdh;->b:Lbdk;

    const/4 v3, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v2, v3}, Lyt;->a(Lbdk;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_9

    .line 449
    :cond_14
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    invoke-virtual/range {v26 .. v26}, Lajl;->a()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_15
    :goto_a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_17

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lajk;

    iget v3, v2, Lajk;->i:I

    invoke-static {v3}, Lf;->c(I)Z

    move-result v3

    if-eqz v3, :cond_15

    iget-object v3, v2, Lajk;->e:Ljava/lang/String;

    invoke-static {v3}, Lbzd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v12, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_16

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v12, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_16
    invoke-interface {v12, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashSet;

    iget v2, v2, Lajk;->i:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_17
    invoke-virtual/range {v23 .. v23}, Les;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_18
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1a

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-interface {v12, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    invoke-interface {v12, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v3, v2}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    :cond_19
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_18

    const/4 v2, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v2, v5}, Lyt;->a(Lbdk;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_18

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_b
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_18

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/lang/Integer;

    move-object v7, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v5, v2, v3}, Lbdh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbdh;

    move-result-object v4

    new-instance v2, Lajk;

    const/4 v3, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Les;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Laeh;

    invoke-virtual {v8}, Laeh;->f()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v7, v11}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v11

    move-object v7, v5

    invoke-direct/range {v2 .. v11}, Lajk;-><init>(Ljava/lang/String;Lbdh;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lyv;Laea;I)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Lajl;->a(Lajk;)V

    goto :goto_b

    .line 456
    :cond_1a
    invoke-virtual/range {v26 .. v26}, Lajl;->a()Ljava/util/Collection;

    move-result-object v12

    .line 458
    invoke-interface {v12}, Ljava/util/Collection;->size()I

    move-result v2

    if-nez v2, :cond_1b

    move-object/from16 v0, p0

    iget-object v2, v0, Lajg;->b:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-static {v0, v2}, Lajg;->a(Lyt;Ljava/lang/String;)Lyv;

    move-result-object v9

    new-instance v2, Lajk;

    if-nez v9, :cond_1c

    move-object/from16 v0, p0

    iget-object v3, v0, Lajg;->b:Ljava/lang/String;

    :goto_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lajg;->g:Lbdh;

    move-object/from16 v0, p0

    iget-object v5, v0, Lajg;->g:Lbdh;

    iget-object v5, v5, Lbdh;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lajg;->g:Lbdh;

    iget-object v6, v6, Lbdh;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lajg;->g:Lbdh;

    iget-object v7, v7, Lbdh;->e:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lajg;->g:Lbdh;

    iget v11, v11, Lbdh;->t:I

    invoke-direct/range {v2 .. v11}, Lajk;-><init>(Ljava/lang/String;Lbdh;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lyv;Laea;I)V

    const/4 v3, 0x1

    iput-boolean v3, v2, Lajk;->j:Z

    invoke-interface {v12, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_1b
    move-object v2, v12

    .line 460
    goto/16 :goto_0

    .line 458
    :cond_1c
    iget-object v3, v9, Lyv;->s:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_c

    :cond_1d
    move-object v2, v3

    move-object v3, v5

    goto/16 :goto_7

    :cond_1e
    move-object/from16 v28, v3

    move-object v3, v2

    move-object/from16 v2, v28

    goto/16 :goto_7
.end method

.method private static a(Lyt;Ljava/lang/String;)Lyv;
    .locals 3

    .prologue
    .line 815
    invoke-virtual {p0, p1}, Lyt;->e(Ljava/lang/String;)Lyv;

    move-result-object v0

    .line 816
    if-nez v0, :cond_0

    .line 817
    invoke-static {p1}, Lyt;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 818
    invoke-static {p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 819
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 820
    invoke-virtual {p0, v1}, Lyt;->e(Ljava/lang/String;)Lyv;

    move-result-object v0

    .line 825
    :cond_0
    return-object v0
.end method

.method static synthetic a(Lajg;Ljava/lang/String;Ljava/util/Collection;)V
    .locals 4

    .prologue
    .line 64
    const/4 v0, 0x0

    iput v0, p0, Lajg;->h:I

    const/4 v0, 0x0

    iput-object v0, p0, Lajg;->d:Ljava/util/Collection;

    iget-object v0, p0, Lajg;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lajg;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a()Lyj;

    move-result-object v0

    iput-object v0, p0, Lajg;->f:Lyj;

    iget-object v0, p0, Lajg;->f:Lyj;

    if-eqz v0, :cond_1

    invoke-static {}, Lbsc;->a()Lbsc;

    move-result-object v1

    :try_start_0
    invoke-direct {p0, p1, p2, v1}, Lajg;->a(Ljava/lang/String;Ljava/util/Collection;Lbsc;)Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p0, v0}, Lajg;->a(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lajg;->i:Landroid/os/Handler;

    new-instance v3, Laji;

    invoke-direct {v3, p0, v0}, Laji;-><init>(Lajg;Ljava/util/Collection;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-virtual {v1}, Lbsc;->b()V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lbsc;->b()V

    throw v0
.end method

.method static synthetic a(Lajg;Ljava/util/Collection;)Z
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lajg;->a(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method private static a(Lbdh;Lbzh;Lyj;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 930
    iget-object v1, p0, Lbdh;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 940
    :cond_0
    :goto_0
    return v0

    .line 934
    :cond_1
    invoke-virtual {p0}, Lbdh;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lbzh;->a(Ljava/lang/String;Lyj;)Lbdn;

    move-result-object v1

    .line 937
    if-eqz v1, :cond_0

    .line 940
    iget-boolean v2, v1, Lbdn;->b:Z

    if-eqz v2, :cond_2

    iget-boolean v1, v1, Lbdn;->a:Z

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/util/Collection;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lajk;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 207
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lajg;->f:Lyj;

    iget-object v2, p0, Lajg;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a()Lyj;

    move-result-object v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lajg;->b:Ljava/lang/String;

    iget-object v2, p0, Lajg;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 209
    invoke-virtual {v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->N()Ljava/lang/String;

    move-result-object v2

    .line 208
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 212
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lajg;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1058
    iput-object p1, p0, Lajg;->j:Ljava/lang/String;

    .line 1059
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lahf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 167
    new-instance v0, Lajh;

    invoke-direct {v0, p0, p1, p2}, Lajh;-><init>(Lajg;Ljava/lang/String;Ljava/util/Collection;)V

    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->executeOnThreadPool(Ljava/lang/Runnable;)V

    .line 173
    return-void
.end method

.method public a()[Lajk;
    .locals 25

    .prologue
    .line 1136
    move-object/from16 v0, p0

    iget-object v2, v0, Lajg;->d:Ljava/util/Collection;

    if-nez v2, :cond_0

    .line 1137
    const/4 v2, 0x0

    new-array v2, v2, [Lajk;

    .line 1148
    :goto_0
    return-object v2

    .line 1139
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lajg;->d:Ljava/util/Collection;

    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lajg;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->V()I

    move-result v4

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lajk;

    iget v3, v2, Lajk;->i:I

    invoke-static {v3}, Lf;->c(I)Z

    move-result v3

    if-nez v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lajg;->f:Lyj;

    invoke-virtual {v3}, Lyj;->u()Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v3, 0x1

    :goto_2
    if-eqz v3, :cond_1

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    iget v3, v2, Lajk;->i:I

    if-ne v3, v4, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lajg;->f:Lyj;

    invoke-virtual {v3}, Lyj;->v()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    goto :goto_2

    :cond_3
    const/4 v3, 0x0

    goto :goto_2

    .line 1141
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lajg;->j:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-interface/range {v17 .. v17}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lajk;

    iget-object v4, v2, Lajk;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lajg;->j:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    iput-boolean v4, v2, Lajk;->j:Z

    goto :goto_3

    :cond_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lajg;->a:Z

    if-eqz v2, :cond_8

    invoke-interface/range {v17 .. v17}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lajk;

    iget-object v4, v2, Lajk;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lajg;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v3, "picking variant based on preferred"

    const-string v4, "Babel"

    invoke-static {v4, v3}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    iput-boolean v3, v2, Lajk;->j:Z

    .line 1144
    :cond_7
    :goto_4
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lajk;

    .line 1145
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1146
    move-object/from16 v0, p0

    iget-object v3, v0, Lajg;->l:Ljava/util/Comparator;

    invoke-static {v2, v3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    goto/16 :goto_0

    .line 1141
    :cond_8
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->f()I

    move-result v2

    if-nez v2, :cond_b

    const/4 v2, 0x1

    move v3, v2

    :goto_5
    const/4 v4, 0x0

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    const-string v5, "connectivity"

    invoke-virtual {v2, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    move v4, v2

    :cond_9
    invoke-static {}, Lbzh;->b()Lbzh;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v2, v0, Lajg;->e:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a()Lyj;

    move-result-object v19

    const/4 v9, 0x0

    invoke-interface/range {v17 .. v17}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :goto_6
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1c

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lajk;

    if-eqz v9, :cond_a

    if-nez v2, :cond_c

    move-object v2, v9

    :cond_a
    :goto_7
    move-object v9, v2

    goto :goto_6

    :cond_b
    const/4 v2, 0x0

    move v3, v2

    goto :goto_5

    :cond_c
    iget-object v5, v9, Lajk;->b:Lbdh;

    iget-object v5, v5, Lbdh;->c:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_f

    const/4 v5, 0x1

    :goto_8
    iget-object v6, v2, Lajk;->b:Lbdh;

    iget-object v6, v6, Lbdh;->c:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_10

    const/4 v6, 0x1

    :goto_9
    iget-object v7, v9, Lajk;->b:Lbdh;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v7, v0, v1}, Lajg;->a(Lbdh;Lbzh;Lyj;)Z

    move-result v12

    iget-object v7, v2, Lajk;->b:Lbdh;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v7, v0, v1}, Lajg;->a(Lbdh;Lbzh;Lyj;)Z

    move-result v11

    iget-object v8, v9, Lajk;->g:Lyv;

    iget-object v10, v2, Lajk;->g:Lyv;

    if-eqz v8, :cond_11

    const/4 v7, 0x1

    move/from16 v16, v7

    :goto_a
    if-eqz v10, :cond_12

    const/4 v7, 0x1

    move v15, v7

    :goto_b
    if-eqz v16, :cond_13

    iget-wide v7, v8, Lyv;->u:J

    move-wide v13, v7

    :goto_c
    if-eqz v15, :cond_14

    iget-wide v7, v10, Lyv;->u:J

    :goto_d
    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v21, "compare variants -- networkConnection=%b; phoneConnection=%b %s vs %s"

    const/16 v22, 0x4

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x2

    iget-object v0, v9, Lajk;->b:Lbdh;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-static {v0, v12, v13, v14}, Lajg;->a(Lbdh;ZJ)Ljava/lang/String;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x3

    iget-object v0, v2, Lajk;->b:Lbdh;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-static {v0, v11, v7, v8}, Lajg;->a(Lbdh;ZJ)Ljava/lang/String;

    move-result-object v24

    aput-object v24, v22, v23

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-static {v10, v0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    const-string v21, "Babel"

    move-object/from16 v0, v21

    invoke-static {v0, v10}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eq v5, v6, :cond_18

    sget-boolean v10, Lajg;->k:Z

    if-eqz v10, :cond_18

    if-eqz v5, :cond_15

    move v10, v11

    :goto_e
    if-eqz v3, :cond_16

    if-eqz v4, :cond_d

    if-nez v10, :cond_16

    :cond_d
    if-eqz v6, :cond_e

    const-wide/16 v21, 0x0

    cmp-long v10, v7, v21

    if-gtz v10, :cond_a

    :cond_e
    if-eqz v5, :cond_16

    const-wide/16 v21, 0x0

    cmp-long v10, v13, v21

    if-lez v10, :cond_16

    move-object v2, v9

    goto/16 :goto_7

    :cond_f
    const/4 v5, 0x0

    goto/16 :goto_8

    :cond_10
    const/4 v6, 0x0

    goto/16 :goto_9

    :cond_11
    const/4 v7, 0x0

    move/from16 v16, v7

    goto :goto_a

    :cond_12
    const/4 v7, 0x0

    move v15, v7

    goto :goto_b

    :cond_13
    const-wide/16 v7, 0x0

    move-wide v13, v7

    goto :goto_c

    :cond_14
    const-wide/16 v7, 0x0

    goto :goto_d

    :cond_15
    move v10, v12

    goto :goto_e

    :cond_16
    if-nez v3, :cond_18

    if-eqz v4, :cond_18

    if-eqz v6, :cond_17

    if-eqz v12, :cond_17

    const-wide/16 v21, 0x0

    cmp-long v10, v13, v21

    if-lez v10, :cond_17

    move-object v2, v9

    goto/16 :goto_7

    :cond_17
    if-eqz v5, :cond_18

    if-eqz v11, :cond_18

    const-wide/16 v10, 0x0

    cmp-long v10, v7, v10

    if-gtz v10, :cond_a

    :cond_18
    move/from16 v0, v16

    if-eq v0, v15, :cond_19

    if-eqz v16, :cond_a

    move-object v2, v9

    goto/16 :goto_7

    :cond_19
    if-eqz v16, :cond_1b

    cmp-long v10, v13, v7

    if-lez v10, :cond_1a

    move-object v2, v9

    goto/16 :goto_7

    :cond_1a
    cmp-long v7, v7, v13

    if-gtz v7, :cond_a

    :cond_1b
    if-eq v5, v6, :cond_a

    if-nez v5, :cond_a

    move-object v2, v9

    goto/16 :goto_7

    :cond_1c
    if-eqz v9, :cond_7

    const/4 v2, 0x1

    iput-boolean v2, v9, Lajk;->j:Z

    goto/16 :goto_4
.end method

.method public b()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1155
    invoke-virtual {p0}, Lajg;->a()[Lajk;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 1156
    iget-object v4, v4, Lajk;->c:Ljava/lang/String;

    invoke-static {v4}, Lsi;->d(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1157
    const/4 v0, 0x1

    .line 1160
    :cond_0
    return v0

    .line 1155
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
