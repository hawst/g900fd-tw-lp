.class public final Levm;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Levm;


# instance fields
.field public b:[Levl;

.field public c:[Levl;

.field public d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x0

    new-array v0, v0, [Levm;

    sput-object v0, Levm;->a:[Levm;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 176
    invoke-direct {p0}, Lepn;-><init>()V

    .line 179
    sget-object v0, Levl;->a:[Levl;

    iput-object v0, p0, Levm;->b:[Levl;

    .line 182
    sget-object v0, Levl;->a:[Levl;

    iput-object v0, p0, Levm;->c:[Levl;

    .line 176
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 213
    iget-object v0, p0, Levm;->b:[Levl;

    if-eqz v0, :cond_1

    .line 214
    iget-object v3, p0, Levm;->b:[Levl;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 215
    if-eqz v5, :cond_0

    .line 216
    const/4 v6, 0x1

    .line 217
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 214
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 221
    :cond_2
    iget-object v2, p0, Levm;->c:[Levl;

    if-eqz v2, :cond_4

    .line 222
    iget-object v2, p0, Levm;->c:[Levl;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 223
    if-eqz v4, :cond_3

    .line 224
    const/4 v5, 0x2

    .line 225
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 222
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 229
    :cond_4
    iget-object v1, p0, Levm;->d:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 230
    const/4 v1, 0x3

    iget-object v2, p0, Levm;->d:Ljava/lang/String;

    .line 231
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 233
    :cond_5
    iget-object v1, p0, Levm;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 234
    iput v0, p0, Levm;->cachedSize:I

    .line 235
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 172
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Levm;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Levm;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Levm;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Levm;->b:[Levl;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Levl;

    iget-object v3, p0, Levm;->b:[Levl;

    if-eqz v3, :cond_2

    iget-object v3, p0, Levm;->b:[Levl;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Levm;->b:[Levl;

    :goto_2
    iget-object v2, p0, Levm;->b:[Levl;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Levm;->b:[Levl;

    new-instance v3, Levl;

    invoke-direct {v3}, Levl;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Levm;->b:[Levl;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Levm;->b:[Levl;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Levm;->b:[Levl;

    new-instance v3, Levl;

    invoke-direct {v3}, Levl;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Levm;->b:[Levl;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Levm;->c:[Levl;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Levl;

    iget-object v3, p0, Levm;->c:[Levl;

    if-eqz v3, :cond_5

    iget-object v3, p0, Levm;->c:[Levl;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Levm;->c:[Levl;

    :goto_4
    iget-object v2, p0, Levm;->c:[Levl;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Levm;->c:[Levl;

    new-instance v3, Levl;

    invoke-direct {v3}, Levl;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Levm;->c:[Levl;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Levm;->c:[Levl;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Levm;->c:[Levl;

    new-instance v3, Levl;

    invoke-direct {v3}, Levl;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Levm;->c:[Levl;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levm;->d:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 189
    iget-object v1, p0, Levm;->b:[Levl;

    if-eqz v1, :cond_1

    .line 190
    iget-object v2, p0, Levm;->b:[Levl;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 191
    if-eqz v4, :cond_0

    .line 192
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 190
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 196
    :cond_1
    iget-object v1, p0, Levm;->c:[Levl;

    if-eqz v1, :cond_3

    .line 197
    iget-object v1, p0, Levm;->c:[Levl;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 198
    if-eqz v3, :cond_2

    .line 199
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 197
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 203
    :cond_3
    iget-object v0, p0, Levm;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 204
    const/4 v0, 0x3

    iget-object v1, p0, Levm;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 206
    :cond_4
    iget-object v0, p0, Levm;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 208
    return-void
.end method
