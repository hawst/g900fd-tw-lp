.class public abstract Llv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lmp;


# instance fields
.field private a:Lmq;

.field private b:I

.field private c:I

.field private d:I

.field protected f:Landroid/content/Context;

.field protected g:Landroid/content/Context;

.field protected h:Lma;

.field protected i:Landroid/view/LayoutInflater;

.field protected j:Landroid/view/LayoutInflater;

.field protected k:Lmr;


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Llv;->f:Landroid/content/Context;

    .line 58
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Llv;->i:Landroid/view/LayoutInflater;

    .line 59
    iput p2, p0, Llv;->b:I

    .line 60
    iput p3, p0, Llv;->c:I

    .line 61
    return-void
.end method


# virtual methods
.method public a(Lme;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 175
    instance-of v0, p2, Lms;

    if-eqz v0, :cond_0

    .line 176
    check-cast p2, Lms;

    move-object v0, p2

    .line 180
    :goto_0
    invoke-virtual {p0, p1, v0}, Llv;->a(Lme;Lms;)V

    .line 181
    check-cast v0, Landroid/view/View;

    return-object v0

    .line 178
    :cond_0
    invoke-virtual {p0, p3}, Llv;->b(Landroid/view/ViewGroup;)Lms;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;)Lmr;
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Llv;->k:Lmr;

    if-nez v0, :cond_0

    .line 72
    iget-object v0, p0, Llv;->i:Landroid/view/LayoutInflater;

    iget v1, p0, Llv;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmr;

    iput-object v0, p0, Llv;->k:Lmr;

    .line 73
    iget-object v0, p0, Llv;->k:Lmr;

    iget-object v1, p0, Llv;->h:Lma;

    invoke-interface {v0, v1}, Lmr;->a(Lma;)V

    .line 74
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Llv;->c(Z)V

    .line 77
    :cond_0
    iget-object v0, p0, Llv;->k:Lmr;

    return-object v0
.end method

.method public a(Landroid/content/Context;Lma;)V
    .locals 1

    .prologue
    .line 65
    iput-object p1, p0, Llv;->g:Landroid/content/Context;

    .line 66
    iget-object v0, p0, Llv;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Llv;->j:Landroid/view/LayoutInflater;

    .line 67
    iput-object p2, p0, Llv;->h:Lma;

    .line 68
    return-void
.end method

.method protected a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 131
    if-eqz v0, :cond_0

    .line 132
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 134
    :cond_0
    iget-object v0, p0, Llv;->k:Lmr;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 135
    return-void
.end method

.method public a(Lma;Z)V
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Llv;->a:Lmq;

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Llv;->a:Lmq;

    invoke-interface {v0, p1, p2}, Lmq;->a(Lma;Z)V

    .line 207
    :cond_0
    return-void
.end method

.method public abstract a(Lme;Lms;)V
.end method

.method public a(Lmq;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Llv;->a:Lmq;

    .line 151
    return-void
.end method

.method protected a(Landroid/view/ViewGroup;I)Z
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 146
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lme;)Z
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lmu;)Z
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Llv;->a:Lmq;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Llv;->a:Lmq;

    invoke-interface {v0, p1}, Lmq;->b(Lma;)Z

    move-result v0

    .line 213
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/view/ViewGroup;)Lms;
    .locals 3

    .prologue
    .line 159
    iget-object v0, p0, Llv;->i:Landroid/view/LayoutInflater;

    iget v1, p0, Llv;->c:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lms;

    return-object v0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 233
    iput p1, p0, Llv;->d:I

    .line 234
    return-void
.end method

.method public b(Lme;)Z
    .locals 1

    .prologue
    .line 221
    const/4 v0, 0x0

    return v0
.end method

.method public c(Z)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 84
    iget-object v0, p0, Llv;->k:Lmr;

    check-cast v0, Landroid/view/ViewGroup;

    .line 85
    if-nez v0, :cond_1

    .line 121
    :cond_0
    return-void

    .line 90
    :cond_1
    iget-object v1, p0, Llv;->h:Lma;

    if-eqz v1, :cond_7

    .line 91
    iget-object v1, p0, Llv;->h:Lma;

    invoke-virtual {v1}, Lma;->l()V

    .line 92
    iget-object v1, p0, Llv;->h:Lma;

    invoke-virtual {v1}, Lma;->k()Ljava/util/ArrayList;

    move-result-object v7

    .line 93
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v6, v5

    move v4, v5

    .line 94
    :goto_0
    if-ge v6, v8, :cond_5

    .line 95
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lme;

    .line 96
    invoke-virtual {p0, v1}, Llv;->a(Lme;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 97
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 98
    instance-of v2, v3, Lms;

    if-eqz v2, :cond_4

    move-object v2, v3

    check-cast v2, Lms;

    invoke-interface {v2}, Lms;->a()Lme;

    move-result-object v2

    .line 100
    :goto_1
    invoke-virtual {p0, v1, v3, v0}, Llv;->a(Lme;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 101
    if-eq v1, v2, :cond_2

    .line 103
    invoke-virtual {v9, v5}, Landroid/view/View;->setPressed(Z)V

    .line 107
    :cond_2
    if-eq v9, v3, :cond_3

    .line 108
    invoke-virtual {p0, v9, v4}, Llv;->a(Landroid/view/View;I)V

    .line 110
    :cond_3
    add-int/lit8 v1, v4, 0x1

    .line 94
    :goto_2
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v4, v1

    goto :goto_0

    .line 98
    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    .line 116
    :cond_5
    :goto_3
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v4, v1, :cond_0

    .line 117
    invoke-virtual {p0, v0, v4}, Llv;->a(Landroid/view/ViewGroup;I)Z

    move-result v1

    if-nez v1, :cond_5

    .line 118
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_6
    move v1, v4

    goto :goto_2

    :cond_7
    move v4, v5

    goto :goto_3
.end method

.method public c(Lme;)Z
    .locals 1

    .prologue
    .line 225
    const/4 v0, 0x0

    return v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x0

    return v0
.end method
