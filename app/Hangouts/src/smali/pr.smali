.class public Lpr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Law;
.implements Lhz;
.implements Lkg;
.implements Lpo;
.implements Lqe;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Law",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lhz;",
        "Lkg;",
        "Lpo;",
        "Lqe;"
    }
.end annotation


# static fields
.field public static a:I

.field public static b:I


# instance fields
.field private A:Ljava/lang/String;

.field private B:I

.field private C:Ljava/lang/String;

.field private D:[Ljava/lang/String;

.field private final E:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lpq;",
            ">;"
        }
    .end annotation
.end field

.field private final F:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lpp;",
            ">;"
        }
    .end annotation
.end field

.field private G:Z

.field private H:Z

.field private I:Z

.field private J:J

.field private final K:Ljava/lang/Runnable;

.field final c:Lqa;

.field d:I

.field protected e:I

.field protected f:Z

.field protected g:Landroid/view/View;

.field protected h:Landroid/view/View;

.field public i:Lcom/android/ex/photo/PhotoViewPager;

.field public j:Landroid/widget/ImageView;

.field protected k:Lqi;

.field protected l:Z

.field protected m:Z

.field protected n:F

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field protected q:Z

.field protected r:I

.field protected s:I

.field protected t:I

.field protected u:I

.field protected v:Z

.field protected w:Z

.field public x:Lqb;

.field public final y:Landroid/os/Handler;

.field private final z:Landroid/view/View$OnSystemUiVisibilityChangeListener;


# direct methods
.method public constructor <init>(Lqa;)V
    .locals 2

    .prologue
    .line 190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    const/4 v0, -0x1

    iput v0, p0, Lpr;->e:I

    .line 151
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lpr;->E:Ljava/util/Map;

    .line 154
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lpr;->F:Ljava/util/Set;

    .line 161
    const/4 v0, 0x1

    iput-boolean v0, p0, Lpr;->m:Z

    .line 180
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lpr;->y:Landroid/os/Handler;

    .line 696
    new-instance v0, Lpt;

    invoke-direct {v0, p0}, Lpt;-><init>(Lpr;)V

    iput-object v0, p0, Lpr;->K:Ljava/lang/Runnable;

    .line 191
    iput-object p1, p0, Lpr;->c:Lqa;

    .line 194
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 195
    const/4 v0, 0x0

    iput-object v0, p0, Lpr;->z:Landroid/view/View$OnSystemUiVisibilityChangeListener;

    .line 207
    :goto_0
    return-void

    .line 197
    :cond_0
    new-instance v0, Lps;

    invoke-direct {v0, p0}, Lps;-><init>(Lpr;)V

    iput-object v0, p0, Lpr;->z:Landroid/view/View$OnSystemUiVisibilityChangeListener;

    goto :goto_0
.end method

.method private static a(IIIF)I
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 1045
    int-to-float v0, p2

    int-to-float v1, p2

    mul-float/2addr v1, p3

    sub-float/2addr v0, v1

    div-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1051
    int-to-float v1, p2

    mul-float/2addr v1, p3

    int-to-float v2, p1

    sub-float/2addr v1, v2

    div-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 1053
    sub-int v0, p0, v0

    sub-int/2addr v0, v1

    return v0
.end method

.method private static final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 768
    if-nez p0, :cond_0

    .line 769
    const-string p0, ""

    .line 771
    :cond_0
    return-object p0
.end method

.method private declared-synchronized a(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 593
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lpr;->F:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpp;

    .line 594
    invoke-interface {v0, p1}, Lpp;->a(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 593
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 596
    :cond_0
    monitor-exit p0

    return-void
.end method

.method static synthetic a(Lpr;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62
    iget-object v0, p0, Lpr;->c:Lqa;

    invoke-interface {v0}, Lqa;->finish()V

    iget-object v0, p0, Lpr;->c:Lqa;

    invoke-interface {v0, v1, v1}, Lqa;->overridePendingTransition(II)V

    return-void
.end method

.method static synthetic a(Lpr;Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .prologue
    .line 62
    iget-boolean v0, p0, Lpr;->I:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lpr;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lpr;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lpr;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, Lpy;

    invoke-direct {v2, p0, v0}, Lpy;-><init>(Lpr;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lpr;->c:Lqa;

    invoke-interface {v0}, Lqa;->f()Lav;

    move-result-object v0

    const/16 v1, 0x64

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lav;->a(ILandroid/os/Bundle;Law;)Ldg;

    :cond_1
    return-void

    :cond_2
    invoke-direct {p0}, Lpr;->v()V

    goto :goto_0
.end method

.method static synthetic b(Lpr;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lpr;->v()V

    return-void
.end method

.method private t()V
    .locals 4

    .prologue
    .line 685
    iget-object v0, p0, Lpr;->y:Landroid/os/Handler;

    iget-object v1, p0, Lpr;->K:Ljava/lang/Runnable;

    iget-wide v2, p0, Lpr;->J:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 686
    return-void
.end method

.method private u()V
    .locals 2

    .prologue
    .line 689
    iget-object v0, p0, Lpr;->y:Landroid/os/Handler;

    iget-object v1, p0, Lpr;->K:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 690
    return-void
.end method

.method private v()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    const-wide/16 v6, 0xfa

    const/4 v5, 0x0

    .line 865
    iget-object v0, p0, Lpr;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 866
    iget-object v1, p0, Lpr;->g:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 871
    iget-object v2, p0, Lpr;->j:Landroid/widget/ImageView;

    invoke-virtual {v2, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 877
    iget v2, p0, Lpr;->t:I

    int-to-float v2, v2

    int-to-float v3, v0

    div-float/2addr v2, v3

    .line 878
    iget v3, p0, Lpr;->u:I

    int-to-float v3, v3

    int-to-float v4, v1

    div-float/2addr v3, v4

    .line 879
    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 881
    iget v3, p0, Lpr;->r:I

    iget v4, p0, Lpr;->t:I

    invoke-static {v3, v4, v0, v2}, Lpr;->a(IIIF)I

    move-result v0

    .line 883
    iget v3, p0, Lpr;->s:I

    iget v4, p0, Lpr;->u:I

    invoke-static {v3, v4, v1, v2}, Lpr;->a(IIIF)I

    move-result v1

    .line 886
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 887
    const/16 v4, 0xe

    if-lt v3, v4, :cond_1

    .line 888
    iget-object v4, p0, Lpr;->h:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/view/View;->setAlpha(F)V

    .line 889
    iget-object v4, p0, Lpr;->h:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 890
    iget-object v4, p0, Lpr;->h:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 892
    iget-object v4, p0, Lpr;->j:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 893
    iget-object v4, p0, Lpr;->j:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 894
    iget-object v2, p0, Lpr;->j:Landroid/widget/ImageView;

    int-to-float v0, v0

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setTranslationX(F)V

    .line 895
    iget-object v0, p0, Lpr;->j:Landroid/widget/ImageView;

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 897
    new-instance v0, Lpu;

    invoke-direct {v0, p0}, Lpu;-><init>(Lpr;)V

    .line 903
    iget-object v1, p0, Lpr;->j:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 904
    invoke-virtual {v1, v5}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 905
    const/16 v2, 0x10

    if-lt v3, v2, :cond_0

    .line 906
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 910
    :goto_0
    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 943
    :goto_1
    return-void

    .line 908
    :cond_0
    iget-object v2, p0, Lpr;->y:Landroid/os/Handler;

    invoke-virtual {v2, v0, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 912
    :cond_1
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v5, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 913
    invoke-virtual {v3, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 914
    iget-object v4, p0, Lpr;->h:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 915
    iget-object v3, p0, Lpr;->h:Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/View;->setVisibility(I)V

    .line 917
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-direct {v3, v0, v1, v5, v5}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 919
    invoke-virtual {v3, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 920
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    invoke-direct {v0, v2, v2, v5, v5}, Landroid/view/animation/ScaleAnimation;-><init>(FFFF)V

    .line 921
    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 923
    new-instance v1, Landroid/view/animation/AnimationSet;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 924
    invoke-virtual {v1, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 925
    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 926
    new-instance v0, Lpv;

    invoke-direct {v0, p0}, Lpv;-><init>(Lpr;)V

    .line 940
    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 941
    iget-object v0, p0, Lpr;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1
.end method


# virtual methods
.method public a(ILjava/lang/String;)Ldg;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Ldg",
            "<",
            "Lqn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 496
    packed-switch p1, :pswitch_data_0

    .line 502
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 500
    :pswitch_0
    new-instance v0, Lql;

    iget-object v1, p0, Lpr;->c:Lqa;

    invoke-interface {v1}, Lqa;->l()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lql;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 496
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/content/Context;Lae;F)Lqi;
    .locals 2

    .prologue
    .line 211
    new-instance v0, Lqi;

    iget-boolean v1, p0, Lpr;->w:Z

    invoke-direct {v0, p1, p2, p3, v1}, Lqi;-><init>(Landroid/content/Context;Lae;FZ)V

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 467
    iget-boolean v0, p0, Lpr;->l:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lpr;->a(ZZ)V

    .line 468
    return-void

    .line 467
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 614
    iput p1, p0, Lpr;->B:I

    .line 615
    invoke-virtual {p0, p1}, Lpr;->d(I)V

    .line 616
    return-void
.end method

.method public a(IF)V
    .locals 4

    .prologue
    .line 600
    float-to-double v0, p2

    const-wide v2, 0x3f1a36e2eb1c432dL    # 1.0E-4

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    .line 601
    iget-object v0, p0, Lpr;->E:Ljava/util/Map;

    add-int/lit8 v1, p1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpq;

    .line 602
    if-eqz v0, :cond_0

    .line 603
    invoke-interface {v0}, Lpq;->c()V

    .line 605
    :cond_0
    iget-object v0, p0, Lpr;->E:Ljava/util/Map;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpq;

    .line 606
    if-eqz v0, :cond_1

    .line 607
    invoke-interface {v0}, Lpq;->c()V

    .line 610
    :cond_1
    return-void
.end method

.method public a(ILpq;)V
    .locals 2

    .prologue
    .line 439
    iget-object v0, p0, Lpr;->E:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 440
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 219
    sget v0, Lpr;->b:I

    if-nez v0, :cond_0

    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    iget-object v0, p0, Lpr;->c:Lqa;

    invoke-interface {v0}, Lqa;->l()Landroid/content/Context;

    move-result-object v0

    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    sget v2, Lqq;->a:I

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    sget-object v0, Lpz;->a:[I

    add-int/lit8 v2, v2, -0x1

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    iget v0, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    sput v0, Lpr;->b:I

    .line 220
    :cond_0
    :goto_0
    iget-object v0, p0, Lpr;->c:Lqa;

    invoke-interface {v0}, Lqa;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "activity"

    .line 221
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 222
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    sput v0, Lpr;->a:I

    .line 224
    iget-object v0, p0, Lpr;->c:Lqa;

    invoke-interface {v0}, Lqa;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 226
    const-string v1, "photos_uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 227
    const-string v1, "photos_uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lpr;->A:Ljava/lang/String;

    .line 229
    :cond_1
    const-string v1, "scale_up_animation"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 230
    iput-boolean v6, p0, Lpr;->q:Z

    .line 231
    const-string v1, "start_x_extra"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lpr;->r:I

    .line 232
    const-string v1, "start_y_extra"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lpr;->s:I

    .line 233
    const-string v1, "start_width_extra"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lpr;->t:I

    .line 234
    const-string v1, "start_height_extra"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lpr;->u:I

    .line 236
    :cond_2
    const-string v1, "action_bar_hidden_initially"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lpr;->v:Z

    .line 238
    const-string v1, "display_thumbs_fullscreen"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lpr;->w:Z

    .line 244
    const-string v1, "projection"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 245
    const-string v1, "projection"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lpr;->D:[Ljava/lang/String;

    .line 251
    :goto_1
    const-string v1, "max_scale"

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lpr;->n:F

    .line 252
    iput-object v3, p0, Lpr;->C:Ljava/lang/String;

    .line 253
    iput v4, p0, Lpr;->B:I

    .line 260
    const-string v1, "photo_index"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 261
    const-string v1, "photo_index"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lpr;->B:I

    .line 263
    :cond_3
    const-string v1, "initial_photo_uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 264
    const-string v1, "initial_photo_uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpr;->C:Ljava/lang/String;

    .line 266
    :cond_4
    iput-boolean v6, p0, Lpr;->f:Z

    .line 268
    if-eqz p1, :cond_9

    .line 269
    const-string v0, "com.android.ex.PhotoViewFragment.CURRENT_URI"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpr;->C:Ljava/lang/String;

    .line 270
    const-string v0, "com.android.ex.PhotoViewFragment.CURRENT_INDEX"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lpr;->B:I

    .line 271
    const-string v0, "com.android.ex.PhotoViewFragment.FULLSCREEN"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lpr;->l:Z

    .line 272
    const-string v0, "com.android.ex.PhotoViewFragment.ACTIONBARTITLE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpr;->o:Ljava/lang/String;

    .line 273
    const-string v0, "com.android.ex.PhotoViewFragment.ACTIONBARSUBTITLE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpr;->p:Ljava/lang/String;

    .line 274
    const-string v0, "com.android.ex.PhotoViewFragment.SCALEANIMATIONFINISHED"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lpr;->I:Z

    .line 280
    :goto_2
    iget-object v0, p0, Lpr;->c:Lqa;

    sget v1, Lf;->aQ:I

    invoke-interface {v0, v1}, Lqa;->setContentView(I)V

    .line 283
    iget-object v0, p0, Lpr;->c:Lqa;

    invoke-interface {v0}, Lqa;->l()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lpr;->c:Lqa;

    .line 284
    invoke-interface {v1}, Lqa;->e()Lae;

    move-result-object v1

    iget v2, p0, Lpr;->n:F

    .line 283
    invoke-virtual {p0, v0, v1, v2}, Lpr;->a(Landroid/content/Context;Lae;F)Lqi;

    move-result-object v0

    iput-object v0, p0, Lpr;->k:Lqi;

    .line 285
    iget-object v0, p0, Lpr;->c:Lqa;

    invoke-interface {v0}, Lqa;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 286
    sget v0, Lf;->aI:I

    invoke-virtual {p0, v0}, Lpr;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lpr;->g:Landroid/view/View;

    .line 287
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_5

    .line 288
    iget-object v0, p0, Lpr;->g:Landroid/view/View;

    invoke-virtual {p0}, Lpr;->s()Landroid/view/View$OnSystemUiVisibilityChangeListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 290
    :cond_5
    sget v0, Lf;->aH:I

    invoke-virtual {p0, v0}, Lpr;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lpr;->h:Landroid/view/View;

    .line 291
    sget v0, Lf;->aJ:I

    invoke-virtual {p0, v0}, Lpr;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lpr;->j:Landroid/widget/ImageView;

    .line 292
    sget v0, Lf;->aN:I

    invoke-virtual {p0, v0}, Lpr;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/ex/photo/PhotoViewPager;

    iput-object v0, p0, Lpr;->i:Lcom/android/ex/photo/PhotoViewPager;

    .line 293
    iget-object v0, p0, Lpr;->i:Lcom/android/ex/photo/PhotoViewPager;

    iget-object v2, p0, Lpr;->k:Lqi;

    invoke-virtual {v0, v2}, Lcom/android/ex/photo/PhotoViewPager;->a(Lgq;)V

    .line 294
    iget-object v0, p0, Lpr;->i:Lcom/android/ex/photo/PhotoViewPager;

    invoke-virtual {v0, p0}, Lcom/android/ex/photo/PhotoViewPager;->a(Lhz;)V

    .line 295
    iget-object v0, p0, Lpr;->i:Lcom/android/ex/photo/PhotoViewPager;

    invoke-virtual {v0, p0}, Lcom/android/ex/photo/PhotoViewPager;->a(Lqe;)V

    .line 296
    iget-object v0, p0, Lpr;->i:Lcom/android/ex/photo/PhotoViewPager;

    sget v2, Lf;->aC:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/android/ex/photo/PhotoViewPager;->b(I)V

    .line 298
    new-instance v0, Lqb;

    invoke-direct {v0, p0, v5}, Lqb;-><init>(Lpr;B)V

    iput-object v0, p0, Lpr;->x:Lqb;

    .line 299
    iget-boolean v0, p0, Lpr;->q:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lpr;->I:Z

    if-eqz v0, :cond_a

    .line 302
    :cond_6
    iget-object v0, p0, Lpr;->c:Lqa;

    invoke-interface {v0}, Lqa;->f()Lav;

    move-result-object v0

    const/16 v2, 0x64

    invoke-virtual {v0, v2, v3, p0}, Lav;->a(ILandroid/os/Bundle;Law;)Ldg;

    .line 305
    iget-object v0, p0, Lpr;->h:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 318
    :goto_3
    sget v0, Lf;->aP:I

    .line 319
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lpr;->J:J

    .line 321
    iget-object v0, p0, Lpr;->c:Lqa;

    invoke-interface {v0}, Lqa;->m()Lpk;

    move-result-object v0

    .line 322
    if-eqz v0, :cond_7

    .line 323
    invoke-virtual {v0}, Lpk;->a()V

    .line 324
    invoke-virtual {v0, p0}, Lpk;->a(Lkg;)V

    .line 325
    invoke-virtual {v0}, Lpk;->b()V

    .line 328
    invoke-virtual {p0, v0}, Lpr;->a(Lpk;)V

    .line 331
    :cond_7
    iget-boolean v0, p0, Lpr;->q:Z

    if-nez v0, :cond_b

    .line 332
    iget-boolean v0, p0, Lpr;->l:Z

    invoke-virtual {p0, v0}, Lpr;->b(Z)V

    .line 338
    :goto_4
    return-void

    .line 219
    :pswitch_0
    iget v0, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    mul-int/lit16 v0, v0, 0x320

    div-int/lit16 v0, v0, 0x3e8

    sput v0, Lpr;->b:I

    goto/16 :goto_0

    .line 247
    :cond_8
    iput-object v3, p0, Lpr;->D:[Ljava/lang/String;

    goto/16 :goto_1

    .line 277
    :cond_9
    iget-boolean v0, p0, Lpr;->v:Z

    iput-boolean v0, p0, Lpr;->l:Z

    goto/16 :goto_2

    .line 311
    :cond_a
    iget-object v0, p0, Lpr;->i:Lcom/android/ex/photo/PhotoViewPager;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/android/ex/photo/PhotoViewPager;->setVisibility(I)V

    .line 312
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 313
    const-string v2, "image_uri"

    iget-object v3, p0, Lpr;->C:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    iget-object v2, p0, Lpr;->c:Lqa;

    invoke-interface {v2}, Lqa;->f()Lav;

    move-result-object v2

    const/4 v3, 0x2

    iget-object v4, p0, Lpr;->x:Lqb;

    invoke-virtual {v2, v3, v0, v4}, Lav;->a(ILandroid/os/Bundle;Law;)Ldg;

    goto :goto_3

    .line 336
    :cond_b
    invoke-virtual {p0, v5}, Lpr;->b(Z)V

    goto :goto_4

    .line 219
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ldg;Landroid/database/Cursor;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/16 v8, 0xb

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 508
    invoke-virtual {p1}, Ldg;->l()I

    move-result v0

    .line 509
    const/16 v1, 0x64

    if-ne v0, v1, :cond_1

    .line 510
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 511
    :cond_0
    iput-boolean v7, p0, Lpr;->f:Z

    .line 512
    iget-object v0, p0, Lpr;->k:Lqi;

    invoke-virtual {v0, v6}, Lqi;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 572
    :cond_1
    :goto_0
    return-void

    .line 514
    :cond_2
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iput v0, p0, Lpr;->e:I

    .line 515
    iget-object v0, p0, Lpr;->C:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 518
    const-string v0, "uri"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 520
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v8, :cond_4

    .line 521
    iget-object v0, p0, Lpr;->C:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 522
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->clearQuery()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 528
    :goto_1
    const/4 v1, -0x1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move v1, v2

    .line 529
    :goto_2
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 530
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 532
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v5, v8, :cond_5

    .line 533
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->clearQuery()Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 537
    :goto_3
    if-eqz v0, :cond_6

    invoke-virtual {v0, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 538
    iput v1, p0, Lpr;->B:I

    .line 547
    :cond_3
    iget-boolean v0, p0, Lpr;->m:Z

    if-eqz v0, :cond_7

    .line 548
    iput-boolean v7, p0, Lpr;->G:Z

    .line 549
    iget-object v0, p0, Lpr;->k:Lqi;

    invoke-virtual {v0, v6}, Lqi;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0

    .line 524
    :cond_4
    iget-object v0, p0, Lpr;->C:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 525
    invoke-virtual {v0, v6}, Landroid/net/Uri$Builder;->query(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    .line 535
    :cond_5
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/net/Uri$Builder;->query(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    goto :goto_3

    .line 541
    :cond_6
    add-int/lit8 v1, v1, 0x1

    .line 542
    goto :goto_2

    .line 552
    :cond_7
    iget-boolean v0, p0, Lpr;->f:Z

    .line 553
    iput-boolean v2, p0, Lpr;->f:Z

    .line 555
    iget-object v1, p0, Lpr;->k:Lqi;

    invoke-virtual {v1, p2}, Lqi;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 556
    iget-object v1, p0, Lpr;->i:Lcom/android/ex/photo/PhotoViewPager;

    invoke-virtual {v1}, Lcom/android/ex/photo/PhotoViewPager;->b()Lgq;

    move-result-object v1

    if-nez v1, :cond_8

    .line 557
    iget-object v1, p0, Lpr;->i:Lcom/android/ex/photo/PhotoViewPager;

    iget-object v3, p0, Lpr;->k:Lqi;

    invoke-virtual {v1, v3}, Lcom/android/ex/photo/PhotoViewPager;->a(Lgq;)V

    .line 559
    :cond_8
    invoke-direct {p0, p2}, Lpr;->a(Landroid/database/Cursor;)V

    .line 562
    iget v1, p0, Lpr;->B:I

    if-gez v1, :cond_9

    .line 563
    iput v2, p0, Lpr;->B:I

    .line 566
    :cond_9
    iget-object v1, p0, Lpr;->i:Lcom/android/ex/photo/PhotoViewPager;

    iget v3, p0, Lpr;->B:I

    invoke-virtual {v1, v3, v2}, Lcom/android/ex/photo/PhotoViewPager;->a(IZ)V

    .line 567
    if-eqz v0, :cond_1

    .line 568
    iget v0, p0, Lpr;->B:I

    invoke-virtual {p0, v0}, Lpr;->d(I)V

    goto/16 :goto_0
.end method

.method public final a(Lpk;)V
    .locals 1

    .prologue
    .line 755
    if-nez p1, :cond_0

    .line 760
    :goto_0
    return-void

    .line 758
    :cond_0
    iget-object v0, p0, Lpr;->o:Ljava/lang/String;

    invoke-static {v0}, Lpr;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lpk;->a(Ljava/lang/CharSequence;)V

    .line 759
    iget-object v0, p0, Lpr;->p:Ljava/lang/String;

    invoke-static {v0}, Lpr;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lpk;->b(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public declared-synchronized a(Lpp;)V
    .locals 1

    .prologue
    .line 449
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lpr;->F:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 450
    monitor-exit p0

    return-void

    .line 449
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lqj;Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 821
    iget-object v0, p0, Lpr;->j:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 822
    invoke-virtual {p1}, Lqj;->q()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lpr;->C:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 823
    iget-object v0, p0, Lpr;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 834
    iget-object v0, p0, Lpr;->i:Lcom/android/ex/photo/PhotoViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/ex/photo/PhotoViewPager;->setVisibility(I)V

    .line 837
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 803
    if-eqz p1, :cond_0

    .line 804
    invoke-direct {p0}, Lpr;->u()V

    .line 808
    :goto_0
    return-void

    .line 806
    :cond_0
    invoke-direct {p0}, Lpr;->t()V

    goto :goto_0
.end method

.method protected a(ZZ)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 664
    iget-boolean v0, p0, Lpr;->l:Z

    if-eq p1, v0, :cond_1

    move v0, v1

    .line 665
    :goto_0
    iput-boolean p1, p0, Lpr;->l:Z

    .line 667
    iget-boolean v3, p0, Lpr;->l:Z

    if-eqz v3, :cond_2

    .line 668
    invoke-virtual {p0, v1}, Lpr;->b(Z)V

    .line 669
    invoke-direct {p0}, Lpr;->u()V

    .line 677
    :cond_0
    :goto_1
    if-eqz v0, :cond_3

    .line 678
    iget-object v0, p0, Lpr;->E:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpq;

    .line 679
    iget-boolean v2, p0, Lpr;->l:Z

    invoke-interface {v0}, Lpq;->a()V

    goto :goto_2

    :cond_1
    move v0, v2

    .line 664
    goto :goto_0

    .line 671
    :cond_2
    invoke-virtual {p0, v2}, Lpr;->b(Z)V

    .line 672
    if-eqz p2, :cond_0

    .line 673
    invoke-direct {p0}, Lpr;->t()V

    goto :goto_1

    .line 682
    :cond_3
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 428
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 433
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 430
    :pswitch_0
    iget-object v0, p0, Lpr;->c:Lqa;

    invoke-interface {v0}, Lqa;->finish()V

    .line 431
    const/4 v0, 0x1

    goto :goto_0

    .line 428
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lt;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 624
    iget-object v1, p0, Lpr;->i:Lcom/android/ex/photo/PhotoViewPager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lpr;->k:Lqi;

    if-nez v1, :cond_1

    .line 627
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lpr;->i:Lcom/android/ex/photo/PhotoViewPager;

    invoke-virtual {v1}, Lcom/android/ex/photo/PhotoViewPager;->c()I

    move-result v1

    iget-object v2, p0, Lpr;->k:Lqi;

    invoke-virtual {v2, p1}, Lqi;->c(Ljava/lang/Object;)I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()Lqi;
    .locals 1

    .prologue
    .line 850
    iget-object v0, p0, Lpr;->k:Lqi;

    return-object v0
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 444
    iget-object v0, p0, Lpr;->E:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 445
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 419
    const-string v0, "com.android.ex.PhotoViewFragment.CURRENT_URI"

    iget-object v1, p0, Lpr;->C:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    const-string v0, "com.android.ex.PhotoViewFragment.CURRENT_INDEX"

    iget v1, p0, Lpr;->B:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 421
    const-string v0, "com.android.ex.PhotoViewFragment.FULLSCREEN"

    iget-boolean v1, p0, Lpr;->l:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 422
    const-string v0, "com.android.ex.PhotoViewFragment.ACTIONBARTITLE"

    iget-object v1, p0, Lpr;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    const-string v0, "com.android.ex.PhotoViewFragment.ACTIONBARSUBTITLE"

    iget-object v1, p0, Lpr;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    const-string v0, "com.android.ex.PhotoViewFragment.SCALEANIMATIONFINISHED"

    iget-boolean v1, p0, Lpr;->I:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 425
    return-void
.end method

.method public declared-synchronized b(Lpp;)V
    .locals 1

    .prologue
    .line 454
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lpr;->F:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 455
    monitor-exit p0

    return-void

    .line 454
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected b(Z)V
    .locals 0

    .prologue
    .line 693
    invoke-virtual {p0, p1}, Lpr;->c(Z)V

    .line 694
    return-void
.end method

.method public b(Lt;)Z
    .locals 2

    .prologue
    .line 459
    iget-object v0, p0, Lpr;->i:Lcom/android/ex/photo/PhotoViewPager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lpr;->k:Lqi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lpr;->k:Lqi;

    invoke-virtual {v0}, Lqi;->b()I

    move-result v0

    if-nez v0, :cond_1

    .line 460
    :cond_0
    iget-boolean v0, p0, Lpr;->l:Z

    .line 462
    :goto_0
    return v0

    :cond_1
    iget-boolean v0, p0, Lpr;->l:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lpr;->i:Lcom/android/ex/photo/PhotoViewPager;

    invoke-virtual {v0}, Lcom/android/ex/photo/PhotoViewPager;->c()I

    move-result v0

    iget-object v1, p0, Lpr;->k:Lqi;

    invoke-virtual {v1, p1}, Lqi;->c(Ljava/lang/Object;)I

    move-result v1

    if-eq v0, v1, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected c(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lpr;->c:Lqa;

    invoke-interface {v0, p1}, Lqa;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public c()Lqa;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lpr;->c:Lqa;

    return-object v0
.end method

.method public c(Z)V
    .locals 8

    .prologue
    const/16 v7, 0x10

    const/16 v6, 0xb

    const/16 v5, 0x13

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1160
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 1161
    if-ge v4, v7, :cond_1

    move v3, v1

    .line 1162
    :goto_0
    if-eqz p1, :cond_b

    .line 1163
    invoke-virtual {p0}, Lpr;->p()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lpr;->q()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1170
    :cond_0
    if-gt v4, v5, :cond_3

    if-ne v4, v5, :cond_8

    .line 1171
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-eq v2, v5, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "kitkatIsSecondary user is only callable on KitKat"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v3, v0

    .line 1161
    goto :goto_0

    .line 1171
    :cond_2
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    const v5, 0x186a0

    if-le v2, v5, :cond_7

    move v2, v1

    :goto_1
    if-nez v2, :cond_8

    .line 1172
    :cond_3
    const/16 v0, 0xf06

    .line 1194
    :cond_4
    :goto_2
    if-eqz v3, :cond_5

    .line 1195
    invoke-virtual {p0}, Lpr;->o()V

    .line 1216
    :cond_5
    :goto_3
    if-lt v4, v6, :cond_6

    .line 1217
    iput v0, p0, Lpr;->d:I

    .line 1218
    invoke-virtual {p0}, Lpr;->r()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 1220
    :cond_6
    return-void

    :cond_7
    move v2, v0

    .line 1171
    goto :goto_1

    .line 1178
    :cond_8
    if-lt v4, v7, :cond_9

    .line 1184
    const/16 v0, 0x505

    goto :goto_2

    .line 1188
    :cond_9
    const/16 v2, 0xe

    if-lt v4, v2, :cond_a

    move v0, v1

    .line 1189
    goto :goto_2

    .line 1190
    :cond_a
    if-lt v4, v6, :cond_4

    move v0, v1

    .line 1191
    goto :goto_2

    .line 1198
    :cond_b
    if-lt v4, v5, :cond_d

    .line 1199
    const/16 v0, 0x700

    .line 1211
    :cond_c
    :goto_4
    if-eqz v3, :cond_5

    .line 1212
    invoke-virtual {p0}, Lpr;->n()V

    goto :goto_3

    .line 1202
    :cond_d
    if-lt v4, v7, :cond_e

    .line 1203
    const/16 v0, 0x500

    goto :goto_4

    .line 1205
    :cond_e
    const/16 v1, 0xe

    if-ge v4, v1, :cond_c

    .line 1207
    if-lt v4, v6, :cond_c

    goto :goto_4
.end method

.method public d()V
    .locals 0

    .prologue
    .line 377
    return-void
.end method

.method public d(I)V
    .locals 2

    .prologue
    .line 705
    iget-object v0, p0, Lpr;->E:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpq;

    .line 706
    if-eqz v0, :cond_0

    .line 707
    invoke-interface {v0}, Lpq;->b()V

    .line 709
    :cond_0
    invoke-virtual {p0}, Lpr;->l()Landroid/database/Cursor;

    move-result-object v0

    .line 710
    iput p1, p0, Lpr;->B:I

    .line 714
    const-string v1, "uri"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 715
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpr;->C:Ljava/lang/String;

    .line 716
    invoke-virtual {p0}, Lpr;->k()V

    .line 719
    invoke-direct {p0}, Lpr;->u()V

    .line 720
    invoke-direct {p0}, Lpr;->t()V

    .line 721
    return-void
.end method

.method public e()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 380
    iget-boolean v0, p0, Lpr;->l:Z

    invoke-virtual {p0, v0, v1}, Lpr;->a(ZZ)V

    .line 382
    iput-boolean v1, p0, Lpr;->m:Z

    .line 383
    iget-boolean v0, p0, Lpr;->G:Z

    if-eqz v0, :cond_0

    .line 384
    iput-boolean v1, p0, Lpr;->G:Z

    .line 385
    iget-object v0, p0, Lpr;->c:Lqa;

    invoke-interface {v0}, Lqa;->f()Lav;

    move-result-object v0

    const/16 v1, 0x64

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lav;->a(ILandroid/os/Bundle;Law;)Ldg;

    .line 387
    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 390
    const/4 v0, 0x1

    iput-boolean v0, p0, Lpr;->m:Z

    .line 391
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 393
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 396
    const/4 v0, 0x1

    iput-boolean v0, p0, Lpr;->H:Z

    .line 397
    return-void
.end method

.method public i()Z
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v0, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    const-wide/16 v6, 0xfa

    .line 406
    iget-boolean v1, p0, Lpr;->l:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lpr;->v:Z

    if-nez v1, :cond_1

    .line 407
    invoke-virtual {p0}, Lpr;->a()V

    .line 415
    :goto_0
    const/4 v0, 0x1

    :cond_0
    return v0

    .line 409
    :cond_1
    iget-boolean v1, p0, Lpr;->q:Z

    if-eqz v1, :cond_0

    .line 410
    iget-object v1, p0, Lpr;->c:Lqa;

    invoke-interface {v1}, Lqa;->getIntent()Landroid/content/Intent;

    iget-object v1, p0, Lpr;->g:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lpr;->g:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iget v3, p0, Lpr;->t:I

    int-to-float v3, v3

    int-to-float v4, v1

    div-float/2addr v3, v4

    iget v4, p0, Lpr;->u:I

    int-to-float v4, v4

    int-to-float v5, v2

    div-float/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    iget v4, p0, Lpr;->r:I

    iget v5, p0, Lpr;->t:I

    invoke-static {v4, v5, v1, v3}, Lpr;->a(IIIF)I

    move-result v1

    iget v4, p0, Lpr;->s:I

    iget v5, p0, Lpr;->u:I

    invoke-static {v4, v5, v2, v3}, Lpr;->a(IIIF)I

    move-result v2

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xe

    if-lt v4, v5, :cond_4

    iget-object v5, p0, Lpr;->h:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/ViewPropertyAnimator;->start()V

    iget-object v5, p0, Lpr;->h:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    new-instance v5, Lpw;

    invoke-direct {v5, p0}, Lpw;-><init>(Lpr;)V

    iget-object v0, p0, Lpr;->j:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lpr;->j:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, v2

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    :goto_1
    const/16 v1, 0x10

    if-lt v4, v1, :cond_3

    invoke-virtual {v0, v5}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    :goto_2
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lpr;->i:Lcom/android/ex/photo/PhotoViewPager;

    invoke-virtual {v0}, Lcom/android/ex/photo/PhotoViewPager;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, v2

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lpr;->y:Landroid/os/Handler;

    invoke-virtual {v1, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_2

    :cond_4
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v8, v9}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v1, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v2, p0, Lpr;->h:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v1, p0, Lpr;->h:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Landroid/view/animation/ScaleAnimation;

    invoke-direct {v0, v8, v8, v3, v3}, Landroid/view/animation/ScaleAnimation;-><init>(FFFF)V

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    new-instance v1, Lpx;

    invoke-direct {v1, p0}, Lpx;-><init>(Lpr;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, p0, Lpr;->j:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lpr;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    :cond_5
    iget-object v1, p0, Lpr;->i:Lcom/android/ex/photo/PhotoViewPager;

    invoke-virtual {v1, v0}, Lcom/android/ex/photo/PhotoViewPager;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0
.end method

.method public j()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 640
    iget-object v1, p0, Lpr;->E:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    move v2, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpq;

    .line 641
    if-nez v2, :cond_0

    .line 642
    invoke-interface {v0}, Lpq;->d()Z

    move-result v2

    .line 644
    :cond_0
    if-nez v1, :cond_5

    .line 645
    invoke-interface {v0}, Lpq;->e()Z

    move-result v0

    :goto_1
    move v1, v0

    .line 647
    goto :goto_0

    .line 649
    :cond_1
    if-eqz v2, :cond_3

    .line 650
    if-eqz v1, :cond_2

    .line 651
    sget v0, Lqd;->d:I

    .line 657
    :goto_2
    return v0

    .line 653
    :cond_2
    sget v0, Lqd;->b:I

    goto :goto_2

    .line 654
    :cond_3
    if-eqz v1, :cond_4

    .line 655
    sget v0, Lqd;->c:I

    goto :goto_2

    .line 657
    :cond_4
    sget v0, Lqd;->a:I

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public k()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 727
    iget-object v0, p0, Lpr;->i:Lcom/android/ex/photo/PhotoViewPager;

    invoke-virtual {v0}, Lcom/android/ex/photo/PhotoViewPager;->c()I

    move-result v0

    add-int/lit8 v3, v0, 0x1

    .line 728
    iget v0, p0, Lpr;->e:I

    if-ltz v0, :cond_1

    move v0, v1

    .line 730
    :goto_0
    invoke-virtual {p0}, Lpr;->l()Landroid/database/Cursor;

    move-result-object v4

    .line 731
    if-eqz v4, :cond_2

    .line 734
    const-string v5, "_display_name"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 735
    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lpr;->o:Ljava/lang/String;

    .line 740
    :goto_1
    iget-boolean v4, p0, Lpr;->f:Z

    if-nez v4, :cond_0

    if-eqz v0, :cond_0

    if-gtz v3, :cond_3

    .line 741
    :cond_0
    iput-object v6, p0, Lpr;->p:Ljava/lang/String;

    .line 747
    :goto_2
    iget-object v0, p0, Lpr;->c:Lqa;

    invoke-interface {v0}, Lqa;->m()Lpk;

    move-result-object v0

    invoke-virtual {p0, v0}, Lpr;->a(Lpk;)V

    .line 748
    return-void

    :cond_1
    move v0, v2

    .line 728
    goto :goto_0

    .line 737
    :cond_2
    iput-object v6, p0, Lpr;->o:Ljava/lang/String;

    goto :goto_1

    .line 743
    :cond_3
    iget-object v0, p0, Lpr;->c:Lqa;

    invoke-interface {v0}, Lqa;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v4, Lf;->aT:I

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    .line 744
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v2

    iget v2, p0, Lpr;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    .line 743
    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpr;->p:Ljava/lang/String;

    goto :goto_2
.end method

.method public l()Landroid/database/Cursor;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 781
    iget-object v1, p0, Lpr;->i:Lcom/android/ex/photo/PhotoViewPager;

    if-nez v1, :cond_1

    .line 794
    :cond_0
    :goto_0
    return-object v0

    .line 785
    :cond_1
    iget-object v1, p0, Lpr;->i:Lcom/android/ex/photo/PhotoViewPager;

    invoke-virtual {v1}, Lcom/android/ex/photo/PhotoViewPager;->c()I

    move-result v2

    .line 786
    iget-object v1, p0, Lpr;->k:Lqi;

    invoke-virtual {v1}, Lqi;->e()Landroid/database/Cursor;

    move-result-object v1

    .line 788
    if-eqz v1, :cond_0

    .line 792
    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-object v0, v1

    .line 794
    goto :goto_0
.end method

.method public m()V
    .locals 2

    .prologue
    .line 854
    const/4 v0, 0x1

    iput-boolean v0, p0, Lpr;->I:Z

    .line 855
    iget-object v0, p0, Lpr;->i:Lcom/android/ex/photo/PhotoViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/ex/photo/PhotoViewPager;->setVisibility(I)V

    .line 856
    iget-boolean v0, p0, Lpr;->l:Z

    invoke-virtual {p0, v0}, Lpr;->b(Z)V

    .line 857
    return-void
.end method

.method public n()V
    .locals 1

    .prologue
    .line 1093
    iget-object v0, p0, Lpr;->c:Lqa;

    invoke-interface {v0}, Lqa;->m()Lpk;

    move-result-object v0

    invoke-virtual {v0}, Lpk;->c()V

    .line 1094
    return-void
.end method

.method public o()V
    .locals 1

    .prologue
    .line 1097
    iget-object v0, p0, Lpr;->c:Lqa;

    invoke-interface {v0}, Lqa;->m()Lpk;

    move-result-object v0

    invoke-virtual {v0}, Lpk;->d()V

    .line 1098
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Ldg;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 488
    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    .line 489
    new-instance v0, Lqo;

    iget-object v1, p0, Lpr;->c:Lqa;

    invoke-interface {v1}, Lqa;->l()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lpr;->A:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lpr;->D:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lqo;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;)V

    .line 491
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Ldg;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 62
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lpr;->a(Ldg;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Ldg;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 579
    iget-boolean v0, p0, Lpr;->H:Z

    if-nez v0, :cond_0

    .line 582
    iget-object v0, p0, Lpr;->k:Lqi;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lqi;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 584
    :cond_0
    return-void
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 1101
    iget-boolean v0, p0, Lpr;->q:Z

    return v0
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 1105
    iget-boolean v0, p0, Lpr;->I:Z

    return v0
.end method

.method public r()Landroid/view/View;
    .locals 1

    .prologue
    .line 1109
    iget-object v0, p0, Lpr;->g:Landroid/view/View;

    return-object v0
.end method

.method public s()Landroid/view/View$OnSystemUiVisibilityChangeListener;
    .locals 1

    .prologue
    .line 1238
    iget-object v0, p0, Lpr;->z:Landroid/view/View$OnSystemUiVisibilityChangeListener;

    return-object v0
.end method
