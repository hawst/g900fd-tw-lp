.class public final Ldoz;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldoz;


# instance fields
.field public b:[Ldou;

.field public c:[Ljava/lang/Long;

.field public d:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2109
    const/4 v0, 0x0

    new-array v0, v0, [Ldoz;

    sput-object v0, Ldoz;->a:[Ldoz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2110
    invoke-direct {p0}, Lepn;-><init>()V

    .line 2113
    sget-object v0, Ldou;->a:[Ldou;

    iput-object v0, p0, Ldoz;->b:[Ldou;

    .line 2116
    sget-object v0, Lept;->n:[Ljava/lang/Long;

    iput-object v0, p0, Ldoz;->c:[Ljava/lang/Long;

    .line 2119
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldoz;->d:[Ljava/lang/String;

    .line 2110
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2148
    iget-object v0, p0, Ldoz;->b:[Ldou;

    if-eqz v0, :cond_1

    .line 2149
    iget-object v3, p0, Ldoz;->b:[Ldou;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 2150
    if-eqz v5, :cond_0

    .line 2151
    const/4 v6, 0x1

    .line 2152
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 2149
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2156
    :cond_2
    iget-object v2, p0, Ldoz;->c:[Ljava/lang/Long;

    if-eqz v2, :cond_4

    iget-object v2, p0, Ldoz;->c:[Ljava/lang/Long;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 2158
    iget-object v4, p0, Ldoz;->c:[Ljava/lang/Long;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_3

    aget-object v6, v4, v2

    .line 2160
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Lepl;->c(J)I

    move-result v6

    add-int/2addr v3, v6

    .line 2158
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2162
    :cond_3
    add-int/2addr v0, v3

    .line 2163
    iget-object v2, p0, Ldoz;->c:[Ljava/lang/Long;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2165
    :cond_4
    iget-object v2, p0, Ldoz;->d:[Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Ldoz;->d:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 2167
    iget-object v3, p0, Ldoz;->d:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_5

    aget-object v5, v3, v1

    .line 2169
    invoke-static {v5}, Lepl;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 2167
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2171
    :cond_5
    add-int/2addr v0, v2

    .line 2172
    iget-object v1, p0, Ldoz;->d:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2174
    :cond_6
    iget-object v1, p0, Ldoz;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2175
    iput v0, p0, Ldoz;->cachedSize:I

    .line 2176
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2106
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldoz;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldoz;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldoz;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldoz;->b:[Ldou;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldou;

    iget-object v3, p0, Ldoz;->b:[Ldou;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldoz;->b:[Ldou;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldoz;->b:[Ldou;

    :goto_2
    iget-object v2, p0, Ldoz;->b:[Ldou;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldoz;->b:[Ldou;

    new-instance v3, Ldou;

    invoke-direct {v3}, Ldou;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldoz;->b:[Ldou;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldoz;->b:[Ldou;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldoz;->b:[Ldou;

    new-instance v3, Ldou;

    invoke-direct {v3}, Ldou;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldoz;->b:[Ldou;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldoz;->c:[Ljava/lang/Long;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Long;

    iget-object v3, p0, Ldoz;->c:[Ljava/lang/Long;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldoz;->c:[Ljava/lang/Long;

    :goto_3
    iget-object v2, p0, Ldoz;->c:[Ljava/lang/Long;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldoz;->c:[Ljava/lang/Long;

    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    iget-object v2, p0, Ldoz;->c:[Ljava/lang/Long;

    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldoz;->d:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Ldoz;->d:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldoz;->d:[Ljava/lang/String;

    :goto_4
    iget-object v2, p0, Ldoz;->d:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ldoz;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v2, p0, Ldoz;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 2124
    iget-object v1, p0, Ldoz;->b:[Ldou;

    if-eqz v1, :cond_1

    .line 2125
    iget-object v2, p0, Ldoz;->b:[Ldou;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2126
    if-eqz v4, :cond_0

    .line 2127
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 2125
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2131
    :cond_1
    iget-object v1, p0, Ldoz;->c:[Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 2132
    iget-object v2, p0, Ldoz;->c:[Ljava/lang/Long;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 2133
    const/4 v5, 0x2

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, v5, v6, v7}, Lepl;->b(IJ)V

    .line 2132
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2136
    :cond_2
    iget-object v1, p0, Ldoz;->d:[Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 2137
    iget-object v1, p0, Ldoz;->d:[Ljava/lang/String;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 2138
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->a(ILjava/lang/String;)V

    .line 2137
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2141
    :cond_3
    iget-object v0, p0, Ldoz;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 2143
    return-void
.end method
