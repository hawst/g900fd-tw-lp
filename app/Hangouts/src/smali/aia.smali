.class public final Laia;
.super Ls;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private Y:Landroid/widget/CheckBox;

.field private Z:Landroid/widget/CheckBox;

.field private aa:Laic;

.field private ab:Lbme;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ls;-><init>()V

    .line 45
    return-void
.end method

.method public static a(Ljava/lang/String;)Laia;
    .locals 3

    .prologue
    .line 50
    new-instance v0, Laia;

    invoke-direct {v0}, Laia;-><init>()V

    .line 52
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 53
    const-string v2, "dialog_inviter_name"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    invoke-virtual {v0, v1}, Laia;->setArguments(Landroid/os/Bundle;)V

    .line 55
    return-object v0
.end method

.method static synthetic a(Laia;)Laic;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Laia;->aa:Laic;

    return-object v0
.end method

.method static synthetic b(Laia;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Laia;->Y:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic c(Laia;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Laia;->Z:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic d(Laia;)Lbme;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Laia;->ab:Lbme;

    return-object v0
.end method


# virtual methods
.method public a(Laic;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Laia;->aa:Laic;

    .line 109
    return-void
.end method

.method public f()Landroid/app/Dialog;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 60
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbme;

    invoke-static {v0, v1}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    iput-object v0, p0, Laia;->ab:Lbme;

    .line 61
    invoke-virtual {p0}, Laia;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 62
    invoke-virtual {p0}, Laia;->getActivity()Ly;

    move-result-object v0

    sget v2, Lf;->fI:I

    invoke-static {v0, v2, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 63
    sget v0, Lg;->cQ:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Laia;->Z:Landroid/widget/CheckBox;

    .line 64
    sget v0, Lg;->cO:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Laia;->Y:Landroid/widget/CheckBox;

    .line 66
    invoke-virtual {p0}, Laia;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "dialog_inviter_name"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 68
    sget v0, Lg;->cP:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 70
    sget v4, Lh;->es:I

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v3, v5, v6

    .line 71
    invoke-virtual {v1, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    .line 70
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    sget v0, Lg;->cN:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 75
    sget v4, Lh;->et:I

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v3, v5, v6

    .line 76
    invoke-virtual {v1, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    .line 75
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Laia;->getActivity()Ly;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 80
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, Lh;->hZ:I

    .line 81
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Laib;

    invoke-direct {v3, p0}, Laib;-><init>(Laia;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, Lh;->ab:I

    .line 100
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 101
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    .prologue
    .line 39
    return-void
.end method
