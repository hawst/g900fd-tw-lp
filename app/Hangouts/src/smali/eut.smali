.class public final Leut;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leut;


# instance fields
.field public b:[Letw;

.field public c:[Letw;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;

.field public g:[Letw;

.field public h:[Leuu;

.field public i:[Leuz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Leut;

    sput-object v0, Leut;->a:[Leut;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 13
    sget-object v0, Letw;->a:[Letw;

    iput-object v0, p0, Leut;->b:[Letw;

    .line 16
    sget-object v0, Letw;->a:[Letw;

    iput-object v0, p0, Leut;->c:[Letw;

    .line 25
    sget-object v0, Letw;->a:[Letw;

    iput-object v0, p0, Leut;->g:[Letw;

    .line 28
    sget-object v0, Leuu;->a:[Leuu;

    iput-object v0, p0, Leut;->h:[Leuu;

    .line 31
    sget-object v0, Leuz;->a:[Leuz;

    iput-object v0, p0, Leut;->i:[Leuz;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 87
    iget-object v0, p0, Leut;->b:[Letw;

    if-eqz v0, :cond_1

    .line 88
    iget-object v3, p0, Leut;->b:[Letw;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 89
    if-eqz v5, :cond_0

    .line 90
    const/4 v6, 0x1

    .line 91
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 88
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 95
    :cond_2
    iget-object v2, p0, Leut;->c:[Letw;

    if-eqz v2, :cond_4

    .line 96
    iget-object v3, p0, Leut;->c:[Letw;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 97
    if-eqz v5, :cond_3

    .line 98
    const/4 v6, 0x2

    .line 99
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 96
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 103
    :cond_4
    iget-object v2, p0, Leut;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    .line 104
    const/4 v2, 0x3

    iget-object v3, p0, Leut;->d:Ljava/lang/Integer;

    .line 105
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 107
    :cond_5
    iget-object v2, p0, Leut;->g:[Letw;

    if-eqz v2, :cond_7

    .line 108
    iget-object v3, p0, Leut;->g:[Letw;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 109
    if-eqz v5, :cond_6

    .line 110
    const/4 v6, 0x4

    .line 111
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 108
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 115
    :cond_7
    iget-object v2, p0, Leut;->h:[Leuu;

    if-eqz v2, :cond_9

    .line 116
    iget-object v3, p0, Leut;->h:[Leuu;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_9

    aget-object v5, v3, v2

    .line 117
    if-eqz v5, :cond_8

    .line 118
    const/4 v6, 0x5

    .line 119
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 116
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 123
    :cond_9
    iget-object v2, p0, Leut;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_a

    .line 124
    const/4 v2, 0x6

    iget-object v3, p0, Leut;->e:Ljava/lang/Integer;

    .line 125
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 127
    :cond_a
    iget-object v2, p0, Leut;->i:[Leuz;

    if-eqz v2, :cond_c

    .line 128
    iget-object v2, p0, Leut;->i:[Leuz;

    array-length v3, v2

    :goto_4
    if-ge v1, v3, :cond_c

    aget-object v4, v2, v1

    .line 129
    if-eqz v4, :cond_b

    .line 130
    const/4 v5, 0x7

    .line 131
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 128
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 135
    :cond_c
    iget-object v1, p0, Leut;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 136
    const/16 v1, 0x8

    iget-object v2, p0, Leut;->f:Ljava/lang/Integer;

    .line 137
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 139
    :cond_d
    iget-object v1, p0, Leut;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 140
    iput v0, p0, Leut;->cachedSize:I

    .line 141
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Leut;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Leut;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Leut;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leut;->b:[Letw;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Letw;

    iget-object v3, p0, Leut;->b:[Letw;

    if-eqz v3, :cond_2

    iget-object v3, p0, Leut;->b:[Letw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Leut;->b:[Letw;

    :goto_2
    iget-object v2, p0, Leut;->b:[Letw;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Leut;->b:[Letw;

    new-instance v3, Letw;

    invoke-direct {v3}, Letw;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leut;->b:[Letw;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Leut;->b:[Letw;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Leut;->b:[Letw;

    new-instance v3, Letw;

    invoke-direct {v3}, Letw;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leut;->b:[Letw;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leut;->c:[Letw;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Letw;

    iget-object v3, p0, Leut;->c:[Letw;

    if-eqz v3, :cond_5

    iget-object v3, p0, Leut;->c:[Letw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Leut;->c:[Letw;

    :goto_4
    iget-object v2, p0, Leut;->c:[Letw;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Leut;->c:[Letw;

    new-instance v3, Letw;

    invoke-direct {v3}, Letw;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leut;->c:[Letw;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Leut;->c:[Letw;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Leut;->c:[Letw;

    new-instance v3, Letw;

    invoke-direct {v3}, Letw;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leut;->c:[Letw;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leut;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leut;->g:[Letw;

    if-nez v0, :cond_9

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Letw;

    iget-object v3, p0, Leut;->g:[Letw;

    if-eqz v3, :cond_8

    iget-object v3, p0, Leut;->g:[Letw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    iput-object v2, p0, Leut;->g:[Letw;

    :goto_6
    iget-object v2, p0, Leut;->g:[Letw;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Leut;->g:[Letw;

    new-instance v3, Letw;

    invoke-direct {v3}, Letw;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leut;->g:[Letw;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    iget-object v0, p0, Leut;->g:[Letw;

    array-length v0, v0

    goto :goto_5

    :cond_a
    iget-object v2, p0, Leut;->g:[Letw;

    new-instance v3, Letw;

    invoke-direct {v3}, Letw;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leut;->g:[Letw;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leut;->h:[Leuu;

    if-nez v0, :cond_c

    move v0, v1

    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Leuu;

    iget-object v3, p0, Leut;->h:[Leuu;

    if-eqz v3, :cond_b

    iget-object v3, p0, Leut;->h:[Leuu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    iput-object v2, p0, Leut;->h:[Leuu;

    :goto_8
    iget-object v2, p0, Leut;->h:[Leuu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    iget-object v2, p0, Leut;->h:[Leuu;

    new-instance v3, Leuu;

    invoke-direct {v3}, Leuu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leut;->h:[Leuu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_c
    iget-object v0, p0, Leut;->h:[Leuu;

    array-length v0, v0

    goto :goto_7

    :cond_d
    iget-object v2, p0, Leut;->h:[Leuu;

    new-instance v3, Leuu;

    invoke-direct {v3}, Leuu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leut;->h:[Leuu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leut;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leut;->i:[Leuz;

    if-nez v0, :cond_f

    move v0, v1

    :goto_9
    add-int/2addr v2, v0

    new-array v2, v2, [Leuz;

    iget-object v3, p0, Leut;->i:[Leuz;

    if-eqz v3, :cond_e

    iget-object v3, p0, Leut;->i:[Leuz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_e
    iput-object v2, p0, Leut;->i:[Leuz;

    :goto_a
    iget-object v2, p0, Leut;->i:[Leuz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_10

    iget-object v2, p0, Leut;->i:[Leuz;

    new-instance v3, Leuz;

    invoke-direct {v3}, Leuz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leut;->i:[Leuz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_f
    iget-object v0, p0, Leut;->i:[Leuz;

    array-length v0, v0

    goto :goto_9

    :cond_10
    iget-object v2, p0, Leut;->i:[Leuz;

    new-instance v3, Leuz;

    invoke-direct {v3}, Leuz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leut;->i:[Leuz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leut;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 36
    iget-object v1, p0, Leut;->b:[Letw;

    if-eqz v1, :cond_1

    .line 37
    iget-object v2, p0, Leut;->b:[Letw;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 38
    if-eqz v4, :cond_0

    .line 39
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 37
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 43
    :cond_1
    iget-object v1, p0, Leut;->c:[Letw;

    if-eqz v1, :cond_3

    .line 44
    iget-object v2, p0, Leut;->c:[Letw;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 45
    if-eqz v4, :cond_2

    .line 46
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 44
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 50
    :cond_3
    iget-object v1, p0, Leut;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 51
    const/4 v1, 0x3

    iget-object v2, p0, Leut;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 53
    :cond_4
    iget-object v1, p0, Leut;->g:[Letw;

    if-eqz v1, :cond_6

    .line 54
    iget-object v2, p0, Leut;->g:[Letw;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 55
    if-eqz v4, :cond_5

    .line 56
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 54
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 60
    :cond_6
    iget-object v1, p0, Leut;->h:[Leuu;

    if-eqz v1, :cond_8

    .line 61
    iget-object v2, p0, Leut;->h:[Leuu;

    array-length v3, v2

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 62
    if-eqz v4, :cond_7

    .line 63
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 61
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 67
    :cond_8
    iget-object v1, p0, Leut;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 68
    const/4 v1, 0x6

    iget-object v2, p0, Leut;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 70
    :cond_9
    iget-object v1, p0, Leut;->i:[Leuz;

    if-eqz v1, :cond_b

    .line 71
    iget-object v1, p0, Leut;->i:[Leuz;

    array-length v2, v1

    :goto_4
    if-ge v0, v2, :cond_b

    aget-object v3, v1, v0

    .line 72
    if-eqz v3, :cond_a

    .line 73
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 71
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 77
    :cond_b
    iget-object v0, p0, Leut;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    .line 78
    const/16 v0, 0x8

    iget-object v1, p0, Leut;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 80
    :cond_c
    iget-object v0, p0, Leut;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 82
    return-void
.end method
