.class final Lapj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Laph;


# direct methods
.method constructor <init>(Laph;)V
    .locals 0

    .prologue
    .line 510
    iput-object p1, p0, Lapj;->a:Laph;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    .line 513
    iget-object v0, p0, Lapj;->a:Laph;

    invoke-static {}, Lape;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v14

    new-instance v0, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;

    const-string v1, "andr_babel"

    invoke-static {}, Lbci;->a()Lbci;

    move-result-object v2

    iget-wide v2, v2, Lbci;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getIncomingPrimaryVideoSpec()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v3

    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getOutgoingNoEffectsVideoSpec()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v4

    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getOutgoingWithEffectsVideoSpec()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v5

    new-instance v6, Landroid/content/ComponentName;

    const-class v7, Lcom/google/android/apps/hangouts/hangout/VideoChatOutputReceiver;

    invoke-direct {v6, v14, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v7, Landroid/content/ComponentName;

    const-class v8, Lcom/google/android/apps/hangouts/hangout/HangoutUtils$NativeCrashReceiver;

    invoke-direct {v7, v14, v8}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {}, Lape;->r()[[Ljava/lang/String;

    move-result-object v8

    invoke-static {}, Lape;->m()Z

    move-result v9

    invoke-static {}, Lape;->p()Z

    move-result v10

    invoke-static {}, Lf;->l()Ljava/lang/String;

    move-result-object v11

    const-string v12, "babel_hangout_check_connectivity"

    const/4 v13, 0x1

    invoke-static {v12, v13}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v12

    invoke-static {}, Lbdp;->h()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lf;->o(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-direct/range {v0 .. v13}, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/VideoSpecification;Lcom/google/android/libraries/hangouts/video/VideoSpecification;Lcom/google/android/libraries/hangouts/video/VideoSpecification;Landroid/content/ComponentName;Landroid/content/ComponentName;[[Ljava/lang/String;ZZLjava/lang/String;ILjava/lang/String;)V

    invoke-static {v14, v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->initialize(Landroid/content/Context;Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;)V

    sget-object v0, Lapk;->a:Lapk;

    invoke-static {v0}, Lcwz;->a(Ljava/lang/Object;)V

    new-instance v0, Lapk;

    invoke-direct {v0}, Lapk;-><init>()V

    sput-object v0, Lapk;->a:Lapk;

    :cond_0
    invoke-static {}, Lape;->s()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lape;->t()Z

    invoke-static {}, Lape;->s()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
