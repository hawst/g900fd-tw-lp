.class public Lbfg;
.super Lbep;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z

.field public final c:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 2192
    invoke-direct {p0}, Lbep;-><init>()V

    .line 2193
    iput-object p1, p0, Lbfg;->a:Ljava/lang/String;

    .line 2194
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbfg;->b:Z

    .line 2195
    iput p2, p0, Lbfg;->c:I

    .line 2196
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 2

    .prologue
    .line 2200
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 2201
    const-string v0, "Babel_RequestWriter"

    const-string v1, "setActiveClient build protobuf"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2204
    :cond_0
    new-instance v0, Ldvy;

    invoke-direct {v0}, Ldvy;-><init>()V

    .line 2207
    invoke-virtual {p0, p1, p2}, Lbfg;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v1

    iput-object v1, v0, Ldvy;->b:Ldvm;

    .line 2208
    iget-object v1, p0, Lbfg;->a:Ljava/lang/String;

    iput-object v1, v0, Ldvy;->d:Ljava/lang/String;

    .line 2209
    iget-boolean v1, p0, Lbfg;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Ldvy;->c:Ljava/lang/Boolean;

    .line 2210
    iget v1, p0, Lbfg;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldvy;->e:Ljava/lang/Integer;

    .line 2211
    return-object v0
.end method

.method public a(Lyj;Lbph;)V
    .locals 4

    .prologue
    .line 2227
    invoke-static {p1}, Lbpx;->a(Lyj;)Lbpx;

    move-result-object v0

    .line 2228
    const-string v1, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SetActiveClientOperation failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2229
    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2228
    invoke-static {v1, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 2230
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbpx;->a(I)V

    .line 2231
    return-void
.end method

.method public a(Lbea;)Z
    .locals 2

    .prologue
    .line 2240
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2241
    const/4 v0, 0x1

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2216
    const-string v0, "clients/setactiveclient"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2235
    const-string v0, "background_queue"

    return-object v0
.end method

.method public d()J
    .locals 3

    .prologue
    .line 2222
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x78

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method
