.class public final Ldwu;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldwu;


# instance fields
.field public b:Ldvn;

.field public c:Ldul;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22620
    const/4 v0, 0x0

    new-array v0, v0, [Ldwu;

    sput-object v0, Ldwu;->a:[Ldwu;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22621
    invoke-direct {p0}, Lepn;-><init>()V

    .line 22624
    iput-object v0, p0, Ldwu;->b:Ldvn;

    .line 22627
    iput-object v0, p0, Ldwu;->c:Ldul;

    .line 22621
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 22654
    const/4 v0, 0x0

    .line 22655
    iget-object v1, p0, Ldwu;->b:Ldvn;

    if-eqz v1, :cond_0

    .line 22656
    const/4 v0, 0x1

    iget-object v1, p0, Ldwu;->b:Ldvn;

    .line 22657
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 22659
    :cond_0
    iget-object v1, p0, Ldwu;->c:Ldul;

    if-eqz v1, :cond_1

    .line 22660
    const/4 v1, 0x2

    iget-object v2, p0, Ldwu;->c:Ldul;

    .line 22661
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22663
    :cond_1
    iget-object v1, p0, Ldwu;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 22664
    const/4 v1, 0x3

    iget-object v2, p0, Ldwu;->d:Ljava/lang/Boolean;

    .line 22665
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 22667
    :cond_2
    iget-object v1, p0, Ldwu;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 22668
    const/4 v1, 0x4

    iget-object v2, p0, Ldwu;->e:Ljava/lang/Integer;

    .line 22669
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 22671
    :cond_3
    iget-object v1, p0, Ldwu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22672
    iput v0, p0, Ldwu;->cachedSize:I

    .line 22673
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 22617
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldwu;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldwu;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldwu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldwu;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldwu;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldwu;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldwu;->c:Ldul;

    if-nez v0, :cond_3

    new-instance v0, Ldul;

    invoke-direct {v0}, Ldul;-><init>()V

    iput-object v0, p0, Ldwu;->c:Ldul;

    :cond_3
    iget-object v0, p0, Ldwu;->c:Ldul;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldwu;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwu;->e:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 22636
    iget-object v0, p0, Ldwu;->b:Ldvn;

    if-eqz v0, :cond_0

    .line 22637
    const/4 v0, 0x1

    iget-object v1, p0, Ldwu;->b:Ldvn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 22639
    :cond_0
    iget-object v0, p0, Ldwu;->c:Ldul;

    if-eqz v0, :cond_1

    .line 22640
    const/4 v0, 0x2

    iget-object v1, p0, Ldwu;->c:Ldul;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 22642
    :cond_1
    iget-object v0, p0, Ldwu;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 22643
    const/4 v0, 0x3

    iget-object v1, p0, Ldwu;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 22645
    :cond_2
    iget-object v0, p0, Ldwu;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 22646
    const/4 v0, 0x4

    iget-object v1, p0, Ldwu;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->c(II)V

    .line 22648
    :cond_3
    iget-object v0, p0, Ldwu;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 22650
    return-void
.end method
