.class public final Lanl;
.super Landroid/database/MatrixCursor;
.source "PG"

# interfaces
.implements Ladk;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 60
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "DisplayName"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "PhoneNumber"

    aput-object v2, v0, v1

    sput-object v0, Lanl;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lanl;->a:[Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lanl;->b:Z

    .line 74
    return-void
.end method


# virtual methods
.method public a()Lcvw;
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Ladb;)V
    .locals 0

    .prologue
    .line 105
    return-void
.end method

.method public a(Ljava/lang/String;Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 77
    iget-boolean v0, p0, Lanl;->b:Z

    if-nez v0, :cond_0

    .line 78
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 79
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lh;->T:I

    new-array v3, v4, [Ljava/lang/Object;

    aput-object p1, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    aput-object p1, v0, v4

    .line 78
    invoke-virtual {p0, v0}, Lanl;->addRow([Ljava/lang/Object;)V

    .line 81
    iput-boolean v4, p0, Lanl;->b:Z

    .line 85
    :goto_0
    return-void

    .line 83
    :cond_0
    const-string v0, "Babel"

    const-string v1, "InputCallContactCursor.setPhone() was called more than once!"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b()Laea;
    .locals 4

    .prologue
    .line 94
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lanl;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 95
    new-instance v1, Laea;

    new-instance v2, Laeh;

    const-string v3, ""

    invoke-direct {v2, v0, v3}, Laeh;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Laea;-><init>(Laeh;)V

    return-object v1
.end method

.method public c()Ladm;
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x2

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x1

    return v0
.end method
