.class public final Ldon;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldon;


# instance fields
.field public b:[Ldoo;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Ldog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3028
    const/4 v0, 0x0

    new-array v0, v0, [Ldon;

    sput-object v0, Ldon;->a:[Ldon;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3029
    invoke-direct {p0}, Lepn;-><init>()V

    .line 3495
    sget-object v0, Ldoo;->a:[Ldoo;

    iput-object v0, p0, Ldon;->b:[Ldoo;

    .line 3498
    iput-object v1, p0, Ldon;->c:Ljava/lang/Integer;

    .line 3505
    iput-object v1, p0, Ldon;->f:Ldog;

    .line 3029
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 3536
    iget-object v1, p0, Ldon;->b:[Ldoo;

    if-eqz v1, :cond_1

    .line 3537
    iget-object v2, p0, Ldon;->b:[Ldoo;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 3538
    if-eqz v4, :cond_0

    .line 3539
    const/4 v5, 0x1

    .line 3540
    invoke-static {v5, v4}, Lepl;->c(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 3537
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3544
    :cond_1
    iget-object v1, p0, Ldon;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 3545
    const/16 v1, 0xf

    iget-object v2, p0, Ldon;->c:Ljava/lang/Integer;

    .line 3546
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3548
    :cond_2
    iget-object v1, p0, Ldon;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 3549
    const/16 v1, 0x10

    iget-object v2, p0, Ldon;->d:Ljava/lang/Integer;

    .line 3550
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3552
    :cond_3
    iget-object v1, p0, Ldon;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 3553
    const/16 v1, 0x11

    iget-object v2, p0, Ldon;->e:Ljava/lang/Integer;

    .line 3554
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3556
    :cond_4
    iget-object v1, p0, Ldon;->f:Ldog;

    if-eqz v1, :cond_5

    .line 3557
    const/16 v1, 0x12

    iget-object v2, p0, Ldon;->f:Ldog;

    .line 3558
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3560
    :cond_5
    iget-object v1, p0, Ldon;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3561
    iput v0, p0, Ldon;->cachedSize:I

    .line 3562
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 3025
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldon;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldon;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldon;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xb

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldon;->b:[Ldoo;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldoo;

    iget-object v3, p0, Ldon;->b:[Ldoo;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldon;->b:[Ldoo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldon;->b:[Ldoo;

    :goto_2
    iget-object v2, p0, Ldon;->b:[Ldoo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldon;->b:[Ldoo;

    new-instance v3, Ldoo;

    invoke-direct {v3}, Ldoo;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldon;->b:[Ldoo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2, v4}, Lepk;->a(Lepr;I)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldon;->b:[Ldoo;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldon;->b:[Ldoo;

    new-instance v3, Ldoo;

    invoke-direct {v3}, Ldoo;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldon;->b:[Ldoo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0, v4}, Lepk;->a(Lepr;I)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_5

    const/4 v2, 0x2

    if-eq v0, v2, :cond_5

    const/4 v2, 0x3

    if-ne v0, v2, :cond_6

    :cond_5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldon;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_6
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldon;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldon;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldon;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Ldon;->f:Ldog;

    if-nez v0, :cond_7

    new-instance v0, Ldog;

    invoke-direct {v0}, Ldog;-><init>()V

    iput-object v0, p0, Ldon;->f:Ldog;

    :cond_7
    iget-object v0, p0, Ldon;->f:Ldog;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xb -> :sswitch_1
        0x78 -> :sswitch_2
        0x80 -> :sswitch_3
        0x88 -> :sswitch_4
        0x92 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 3510
    iget-object v0, p0, Ldon;->b:[Ldoo;

    if-eqz v0, :cond_1

    .line 3511
    iget-object v1, p0, Ldon;->b:[Ldoo;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 3512
    if-eqz v3, :cond_0

    .line 3513
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lepl;->a(ILepr;)V

    .line 3511
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3517
    :cond_1
    iget-object v0, p0, Ldon;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 3518
    const/16 v0, 0xf

    iget-object v1, p0, Ldon;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 3520
    :cond_2
    iget-object v0, p0, Ldon;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 3521
    const/16 v0, 0x10

    iget-object v1, p0, Ldon;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 3523
    :cond_3
    iget-object v0, p0, Ldon;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 3524
    const/16 v0, 0x11

    iget-object v1, p0, Ldon;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 3526
    :cond_4
    iget-object v0, p0, Ldon;->f:Ldog;

    if-eqz v0, :cond_5

    .line 3527
    const/16 v0, 0x12

    iget-object v1, p0, Ldon;->f:Ldog;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 3529
    :cond_5
    iget-object v0, p0, Ldon;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 3531
    return-void
.end method
