.class public final Lepu;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lepu;


# instance fields
.field public b:[I

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Lepv;

.field public f:Leqd;

.field public g:Ljava/lang/String;

.field public h:Lepz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lepu;

    sput-object v0, Lepu;->a:[Lepu;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 13
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Lepu;->b:[I

    .line 20
    iput-object v1, p0, Lepu;->e:Lepv;

    .line 23
    iput-object v1, p0, Lepu;->f:Leqd;

    .line 28
    iput-object v1, p0, Lepu;->h:Lepz;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 63
    iget-object v1, p0, Lepu;->b:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lepu;->b:[I

    array-length v1, v1

    if-lez v1, :cond_1

    .line 65
    iget-object v2, p0, Lepu;->b:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    .line 67
    invoke-static {v4}, Lepl;->f(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 65
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 70
    :cond_0
    iget-object v0, p0, Lepu;->b:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 72
    :cond_1
    iget-object v1, p0, Lepu;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 73
    const/4 v1, 0x2

    iget-object v2, p0, Lepu;->c:Ljava/lang/String;

    .line 74
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    :cond_2
    iget-object v1, p0, Lepu;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 77
    const/4 v1, 0x3

    iget-object v2, p0, Lepu;->d:Ljava/lang/String;

    .line 78
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    :cond_3
    iget-object v1, p0, Lepu;->e:Lepv;

    if-eqz v1, :cond_4

    .line 81
    const/4 v1, 0x4

    iget-object v2, p0, Lepu;->e:Lepv;

    .line 82
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    :cond_4
    iget-object v1, p0, Lepu;->f:Leqd;

    if-eqz v1, :cond_5

    .line 85
    const/4 v1, 0x5

    iget-object v2, p0, Lepu;->f:Leqd;

    .line 86
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 88
    :cond_5
    iget-object v1, p0, Lepu;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 89
    const/4 v1, 0x6

    iget-object v2, p0, Lepu;->g:Ljava/lang/String;

    .line 90
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    :cond_6
    iget-object v1, p0, Lepu;->h:Lepz;

    if-eqz v1, :cond_7

    .line 93
    const/4 v1, 0x7

    iget-object v2, p0, Lepu;->h:Lepz;

    .line 94
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    :cond_7
    iget-object v1, p0, Lepu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    iput v0, p0, Lepu;->cachedSize:I

    .line 98
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lepu;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lepu;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lepu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Lepu;->b:[I

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [I

    iget-object v2, p0, Lepu;->b:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lepu;->b:[I

    :goto_1
    iget-object v1, p0, Lepu;->b:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lepu;->b:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lepu;->b:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepu;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepu;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lepu;->e:Lepv;

    if-nez v0, :cond_3

    new-instance v0, Lepv;

    invoke-direct {v0}, Lepv;-><init>()V

    iput-object v0, p0, Lepu;->e:Lepv;

    :cond_3
    iget-object v0, p0, Lepu;->e:Lepv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lepu;->f:Leqd;

    if-nez v0, :cond_4

    new-instance v0, Leqd;

    invoke-direct {v0}, Leqd;-><init>()V

    iput-object v0, p0, Lepu;->f:Leqd;

    :cond_4
    iget-object v0, p0, Lepu;->f:Leqd;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepu;->g:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lepu;->h:Lepz;

    if-nez v0, :cond_5

    new-instance v0, Lepz;

    invoke-direct {v0}, Lepz;-><init>()V

    iput-object v0, p0, Lepu;->h:Lepz;

    :cond_5
    iget-object v0, p0, Lepu;->h:Lepz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 33
    iget-object v0, p0, Lepu;->b:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lepu;->b:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 34
    iget-object v1, p0, Lepu;->b:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    .line 35
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 34
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 38
    :cond_0
    iget-object v0, p0, Lepu;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 39
    const/4 v0, 0x2

    iget-object v1, p0, Lepu;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 41
    :cond_1
    iget-object v0, p0, Lepu;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 42
    const/4 v0, 0x3

    iget-object v1, p0, Lepu;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 44
    :cond_2
    iget-object v0, p0, Lepu;->e:Lepv;

    if-eqz v0, :cond_3

    .line 45
    const/4 v0, 0x4

    iget-object v1, p0, Lepu;->e:Lepv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 47
    :cond_3
    iget-object v0, p0, Lepu;->f:Leqd;

    if-eqz v0, :cond_4

    .line 48
    const/4 v0, 0x5

    iget-object v1, p0, Lepu;->f:Leqd;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 50
    :cond_4
    iget-object v0, p0, Lepu;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 51
    const/4 v0, 0x6

    iget-object v1, p0, Lepu;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 53
    :cond_5
    iget-object v0, p0, Lepu;->h:Lepz;

    if-eqz v0, :cond_6

    .line 54
    const/4 v0, 0x7

    iget-object v1, p0, Lepu;->h:Lepz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 56
    :cond_6
    iget-object v0, p0, Lepu;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 58
    return-void
.end method
