.class public final Ldlv;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldlv;


# instance fields
.field public b:[Ldmn;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7335
    const/4 v0, 0x0

    new-array v0, v0, [Ldlv;

    sput-object v0, Ldlv;->a:[Ldlv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7336
    invoke-direct {p0}, Lepn;-><init>()V

    .line 7339
    sget-object v0, Ldmn;->a:[Ldmn;

    iput-object v0, p0, Ldlv;->b:[Ldmn;

    .line 7336
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 7366
    iget-object v1, p0, Ldlv;->b:[Ldmn;

    if-eqz v1, :cond_1

    .line 7367
    iget-object v2, p0, Ldlv;->b:[Ldmn;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 7368
    if-eqz v4, :cond_0

    .line 7369
    const/4 v5, 0x1

    .line 7370
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 7367
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 7374
    :cond_1
    const/4 v1, 0x2

    iget-object v2, p0, Ldlv;->c:Ljava/lang/Integer;

    .line 7375
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7376
    iget-object v1, p0, Ldlv;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 7377
    const/4 v1, 0x3

    iget-object v2, p0, Ldlv;->d:Ljava/lang/Boolean;

    .line 7378
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7380
    :cond_2
    iget-object v1, p0, Ldlv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7381
    iput v0, p0, Ldlv;->cachedSize:I

    .line 7382
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 7332
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldlv;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldlv;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldlv;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldlv;->b:[Ldmn;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldmn;

    iget-object v3, p0, Ldlv;->b:[Ldmn;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldlv;->b:[Ldmn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldlv;->b:[Ldmn;

    :goto_2
    iget-object v2, p0, Ldlv;->b:[Ldmn;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldlv;->b:[Ldmn;

    new-instance v3, Ldmn;

    invoke-direct {v3}, Ldmn;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldlv;->b:[Ldmn;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldlv;->b:[Ldmn;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldlv;->b:[Ldmn;

    new-instance v3, Ldmn;

    invoke-direct {v3}, Ldmn;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldlv;->b:[Ldmn;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldlv;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldlv;->d:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 7348
    iget-object v0, p0, Ldlv;->b:[Ldmn;

    if-eqz v0, :cond_1

    .line 7349
    iget-object v1, p0, Ldlv;->b:[Ldmn;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 7350
    if-eqz v3, :cond_0

    .line 7351
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 7349
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7355
    :cond_1
    const/4 v0, 0x2

    iget-object v1, p0, Ldlv;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 7356
    iget-object v0, p0, Ldlv;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 7357
    const/4 v0, 0x3

    iget-object v1, p0, Ldlv;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 7359
    :cond_2
    iget-object v0, p0, Ldlv;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 7361
    return-void
.end method
