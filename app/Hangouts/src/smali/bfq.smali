.class public abstract Lbfq;
.super Lbfp;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field protected final p:J

.field public final q:Ljava/lang/String;

.field public final r:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 976
    invoke-direct {p0}, Lbfp;-><init>()V

    .line 977
    iput-object p1, p0, Lbfq;->r:Ljava/lang/String;

    .line 978
    iput-object p2, p0, Lbfq;->q:Ljava/lang/String;

    .line 979
    iput-wide p3, p0, Lbfq;->p:J

    .line 980
    return-void
.end method


# virtual methods
.method public a(Lyj;Lbph;)V
    .locals 4

    .prologue
    .line 984
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 985
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onFailed "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbfq;->r:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbfq;->q:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 989
    :cond_0
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v1

    .line 990
    iget-object v2, p0, Lbfq;->r:Ljava/lang/String;

    iget-object v3, p0, Lbfq;->q:Ljava/lang/String;

    if-eqz p2, :cond_1

    .line 994
    invoke-virtual {p2}, Lbph;->e()I

    move-result v0

    .line 990
    :goto_0
    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;Ljava/lang/String;I)I

    .line 995
    return-void

    .line 994
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1000
    iget-object v0, p0, Lbfq;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public d()J
    .locals 3

    .prologue
    .line 1005
    const-string v0, "babel_pending_sms_message_failure_duration"

    const-wide/32 v1, 0x493e0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method
