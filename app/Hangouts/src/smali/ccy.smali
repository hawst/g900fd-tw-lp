.class public final Lccy;
.super Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/hangouts/video/SafeAsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/google/android/apps/hangouts/views/MessageListItemView;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/views/MessageListItemView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 360
    iput-object p1, p0, Lccy;->c:Lcom/google/android/apps/hangouts/views/MessageListItemView;

    iput-object p2, p0, Lccy;->a:Ljava/lang/String;

    iput-object p3, p0, Lccy;->b:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;-><init>()V

    .line 361
    iget-object v0, p0, Lccy;->c:Lcom/google/android/apps/hangouts/views/MessageListItemView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Lcom/google/android/apps/hangouts/views/MessageListItemView;)Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->N()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lccy;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected synthetic doInBackgroundTimed([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 360
    iget-object v0, p0, Lccy;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lccy;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->b(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lccy;->c:Lcom/google/android/apps/hangouts/views/MessageListItemView;

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Lcom/google/android/apps/hangouts/views/MessageListItemView;Ljava/lang/Long;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lccy;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lccy;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->c(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lccy;->c:Lcom/google/android/apps/hangouts/views/MessageListItemView;

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Lcom/google/android/apps/hangouts/views/MessageListItemView;Ljava/lang/Long;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 360
    check-cast p1, Landroid/net/Uri;

    iget-object v0, p0, Lccy;->c:Lcom/google/android/apps/hangouts/views/MessageListItemView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Lcom/google/android/apps/hangouts/views/MessageListItemView;)Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lccy;->c:Lcom/google/android/apps/hangouts/views/MessageListItemView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Lcom/google/android/apps/hangouts/views/MessageListItemView;)Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->N()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lccy;->d:Ljava/lang/String;

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    iget-object v0, p0, Lccy;->c:Lcom/google/android/apps/hangouts/views/MessageListItemView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lccy;->c:Lcom/google/android/apps/hangouts/views/MessageListItemView;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->b(Lcom/google/android/apps/hangouts/views/MessageListItemView;)Lcom/google/android/apps/hangouts/views/AvatarView;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, p1, v2, v3}, Landroid/provider/ContactsContract$QuickContact;->showQuickContact(Landroid/content/Context;Landroid/view/View;Landroid/net/Uri;I[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Babel"

    const-string v2, "Couldn\'t find the contactId when quick contact badge clicked."

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lccy;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lccy;->c:Lcom/google/android/apps/hangouts/views/MessageListItemView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/widget/QuickContactBadge;

    invoke-direct {v1, v0}, Landroid/widget/QuickContactBadge;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lccy;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/widget/QuickContactBadge;->assignContactFromPhone(Ljava/lang/String;Z)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xf

    if-lt v0, v2, :cond_0

    invoke-virtual {v1}, Landroid/widget/QuickContactBadge;->callOnClick()Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lccy;->c:Lcom/google/android/apps/hangouts/views/MessageListItemView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lh;->hv:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
