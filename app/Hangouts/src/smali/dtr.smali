.class public final Ldtr;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldtr;


# instance fields
.field public b:Ldts;

.field public c:Ldtt;

.field public d:Ldtu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4800
    const/4 v0, 0x0

    new-array v0, v0, [Ldtr;

    sput-object v0, Ldtr;->a:[Ldtr;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4801
    invoke-direct {p0}, Lepn;-><init>()V

    .line 5023
    iput-object v0, p0, Ldtr;->b:Ldts;

    .line 5026
    iput-object v0, p0, Ldtr;->c:Ldtt;

    .line 5029
    iput-object v0, p0, Ldtr;->d:Ldtu;

    .line 4801
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 5049
    const/4 v0, 0x0

    .line 5050
    iget-object v1, p0, Ldtr;->c:Ldtt;

    if-eqz v1, :cond_0

    .line 5051
    const/4 v0, 0x1

    iget-object v1, p0, Ldtr;->c:Ldtt;

    .line 5052
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5054
    :cond_0
    iget-object v1, p0, Ldtr;->d:Ldtu;

    if-eqz v1, :cond_1

    .line 5055
    const/4 v1, 0x2

    iget-object v2, p0, Ldtr;->d:Ldtu;

    .line 5056
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5058
    :cond_1
    iget-object v1, p0, Ldtr;->b:Ldts;

    if-eqz v1, :cond_2

    .line 5059
    const/4 v1, 0x3

    iget-object v2, p0, Ldtr;->b:Ldts;

    .line 5060
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5062
    :cond_2
    iget-object v1, p0, Ldtr;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5063
    iput v0, p0, Ldtr;->cachedSize:I

    .line 5064
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 4797
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldtr;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldtr;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldtr;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldtr;->c:Ldtt;

    if-nez v0, :cond_2

    new-instance v0, Ldtt;

    invoke-direct {v0}, Ldtt;-><init>()V

    iput-object v0, p0, Ldtr;->c:Ldtt;

    :cond_2
    iget-object v0, p0, Ldtr;->c:Ldtt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldtr;->d:Ldtu;

    if-nez v0, :cond_3

    new-instance v0, Ldtu;

    invoke-direct {v0}, Ldtu;-><init>()V

    iput-object v0, p0, Ldtr;->d:Ldtu;

    :cond_3
    iget-object v0, p0, Ldtr;->d:Ldtu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldtr;->b:Ldts;

    if-nez v0, :cond_4

    new-instance v0, Ldts;

    invoke-direct {v0}, Ldts;-><init>()V

    iput-object v0, p0, Ldtr;->b:Ldts;

    :cond_4
    iget-object v0, p0, Ldtr;->b:Ldts;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 5034
    iget-object v0, p0, Ldtr;->c:Ldtt;

    if-eqz v0, :cond_0

    .line 5035
    const/4 v0, 0x1

    iget-object v1, p0, Ldtr;->c:Ldtt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5037
    :cond_0
    iget-object v0, p0, Ldtr;->d:Ldtu;

    if-eqz v0, :cond_1

    .line 5038
    const/4 v0, 0x2

    iget-object v1, p0, Ldtr;->d:Ldtu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5040
    :cond_1
    iget-object v0, p0, Ldtr;->b:Ldts;

    if-eqz v0, :cond_2

    .line 5041
    const/4 v0, 0x3

    iget-object v1, p0, Ldtr;->b:Ldts;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5043
    :cond_2
    iget-object v0, p0, Ldtr;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 5045
    return-void
.end method
