.class public final Ldff;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldff;


# instance fields
.field public b:Ldfd;

.field public c:Ldfp;

.field public d:Ldef;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Integer;

.field public g:Leux;

.field public h:Ljava/lang/Integer;

.field public i:Lddu;

.field public j:Ldfg;

.field public k:Ljava/lang/Boolean;

.field public l:Ldme;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1061
    const/4 v0, 0x0

    new-array v0, v0, [Ldff;

    sput-object v0, Ldff;->a:[Ldff;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1062
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1077
    iput-object v0, p0, Ldff;->b:Ldfd;

    .line 1080
    iput-object v0, p0, Ldff;->c:Ldfp;

    .line 1083
    iput-object v0, p0, Ldff;->d:Ldef;

    .line 1088
    iput-object v0, p0, Ldff;->f:Ljava/lang/Integer;

    .line 1091
    iput-object v0, p0, Ldff;->g:Leux;

    .line 1094
    iput-object v0, p0, Ldff;->h:Ljava/lang/Integer;

    .line 1097
    iput-object v0, p0, Ldff;->i:Lddu;

    .line 1100
    iput-object v0, p0, Ldff;->j:Ldfg;

    .line 1105
    iput-object v0, p0, Ldff;->l:Ldme;

    .line 1062
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1149
    const/4 v0, 0x0

    .line 1150
    iget-object v1, p0, Ldff;->b:Ldfd;

    if-eqz v1, :cond_0

    .line 1151
    const/4 v0, 0x1

    iget-object v1, p0, Ldff;->b:Ldfd;

    .line 1152
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1154
    :cond_0
    iget-object v1, p0, Ldff;->c:Ldfp;

    if-eqz v1, :cond_1

    .line 1155
    const/4 v1, 0x3

    iget-object v2, p0, Ldff;->c:Ldfp;

    .line 1156
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1158
    :cond_1
    iget-object v1, p0, Ldff;->d:Ldef;

    if-eqz v1, :cond_2

    .line 1159
    const/4 v1, 0x4

    iget-object v2, p0, Ldff;->d:Ldef;

    .line 1160
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1162
    :cond_2
    iget-object v1, p0, Ldff;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1163
    const/4 v1, 0x5

    iget-object v2, p0, Ldff;->e:Ljava/lang/String;

    .line 1164
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1166
    :cond_3
    iget-object v1, p0, Ldff;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 1167
    const/4 v1, 0x6

    iget-object v2, p0, Ldff;->f:Ljava/lang/Integer;

    .line 1168
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1170
    :cond_4
    iget-object v1, p0, Ldff;->g:Leux;

    if-eqz v1, :cond_5

    .line 1171
    const/16 v1, 0x8

    iget-object v2, p0, Ldff;->g:Leux;

    .line 1172
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1174
    :cond_5
    iget-object v1, p0, Ldff;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 1175
    const/16 v1, 0x9

    iget-object v2, p0, Ldff;->h:Ljava/lang/Integer;

    .line 1176
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1178
    :cond_6
    iget-object v1, p0, Ldff;->i:Lddu;

    if-eqz v1, :cond_7

    .line 1179
    const/16 v1, 0xa

    iget-object v2, p0, Ldff;->i:Lddu;

    .line 1180
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1182
    :cond_7
    iget-object v1, p0, Ldff;->j:Ldfg;

    if-eqz v1, :cond_8

    .line 1183
    const/16 v1, 0xb

    iget-object v2, p0, Ldff;->j:Ldfg;

    .line 1184
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1186
    :cond_8
    iget-object v1, p0, Ldff;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    .line 1187
    const/16 v1, 0xc

    iget-object v2, p0, Ldff;->k:Ljava/lang/Boolean;

    .line 1188
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1190
    :cond_9
    iget-object v1, p0, Ldff;->l:Ldme;

    if-eqz v1, :cond_a

    .line 1191
    const/16 v1, 0xd

    iget-object v2, p0, Ldff;->l:Ldme;

    .line 1192
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1194
    :cond_a
    iget-object v1, p0, Ldff;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1195
    iput v0, p0, Ldff;->cachedSize:I

    .line 1196
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1058
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldff;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldff;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldff;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldff;->b:Ldfd;

    if-nez v0, :cond_2

    new-instance v0, Ldfd;

    invoke-direct {v0}, Ldfd;-><init>()V

    iput-object v0, p0, Ldff;->b:Ldfd;

    :cond_2
    iget-object v0, p0, Ldff;->b:Ldfd;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldff;->c:Ldfp;

    if-nez v0, :cond_3

    new-instance v0, Ldfp;

    invoke-direct {v0}, Ldfp;-><init>()V

    iput-object v0, p0, Ldff;->c:Ldfp;

    :cond_3
    iget-object v0, p0, Ldff;->c:Ldfp;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldff;->d:Ldef;

    if-nez v0, :cond_4

    new-instance v0, Ldef;

    invoke-direct {v0}, Ldef;-><init>()V

    iput-object v0, p0, Ldff;->d:Ldef;

    :cond_4
    iget-object v0, p0, Ldff;->d:Ldef;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldff;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_5

    if-eq v0, v3, :cond_5

    if-ne v0, v4, :cond_6

    :cond_5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldff;->f:Ljava/lang/Integer;

    goto :goto_0

    :cond_6
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldff;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ldff;->g:Leux;

    if-nez v0, :cond_7

    new-instance v0, Leux;

    invoke-direct {v0}, Leux;-><init>()V

    iput-object v0, p0, Ldff;->g:Leux;

    :cond_7
    iget-object v0, p0, Ldff;->g:Leux;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_8

    if-eq v0, v3, :cond_8

    if-ne v0, v4, :cond_9

    :cond_8
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldff;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_9
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldff;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Ldff;->i:Lddu;

    if-nez v0, :cond_a

    new-instance v0, Lddu;

    invoke-direct {v0}, Lddu;-><init>()V

    iput-object v0, p0, Ldff;->i:Lddu;

    :cond_a
    iget-object v0, p0, Ldff;->i:Lddu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Ldff;->j:Ldfg;

    if-nez v0, :cond_b

    new-instance v0, Ldfg;

    invoke-direct {v0}, Ldfg;-><init>()V

    iput-object v0, p0, Ldff;->j:Ldfg;

    :cond_b
    iget-object v0, p0, Ldff;->j:Ldfg;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldff;->k:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Ldff;->l:Ldme;

    if-nez v0, :cond_c

    new-instance v0, Ldme;

    invoke-direct {v0}, Ldme;-><init>()V

    iput-object v0, p0, Ldff;->l:Ldme;

    :cond_c
    iget-object v0, p0, Ldff;->l:Ldme;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x30 -> :sswitch_5
        0x42 -> :sswitch_6
        0x48 -> :sswitch_7
        0x52 -> :sswitch_8
        0x5a -> :sswitch_9
        0x60 -> :sswitch_a
        0x6a -> :sswitch_b
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1110
    iget-object v0, p0, Ldff;->b:Ldfd;

    if-eqz v0, :cond_0

    .line 1111
    const/4 v0, 0x1

    iget-object v1, p0, Ldff;->b:Ldfd;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1113
    :cond_0
    iget-object v0, p0, Ldff;->c:Ldfp;

    if-eqz v0, :cond_1

    .line 1114
    const/4 v0, 0x3

    iget-object v1, p0, Ldff;->c:Ldfp;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1116
    :cond_1
    iget-object v0, p0, Ldff;->d:Ldef;

    if-eqz v0, :cond_2

    .line 1117
    const/4 v0, 0x4

    iget-object v1, p0, Ldff;->d:Ldef;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1119
    :cond_2
    iget-object v0, p0, Ldff;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1120
    const/4 v0, 0x5

    iget-object v1, p0, Ldff;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1122
    :cond_3
    iget-object v0, p0, Ldff;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 1123
    const/4 v0, 0x6

    iget-object v1, p0, Ldff;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1125
    :cond_4
    iget-object v0, p0, Ldff;->g:Leux;

    if-eqz v0, :cond_5

    .line 1126
    const/16 v0, 0x8

    iget-object v1, p0, Ldff;->g:Leux;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1128
    :cond_5
    iget-object v0, p0, Ldff;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 1129
    const/16 v0, 0x9

    iget-object v1, p0, Ldff;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1131
    :cond_6
    iget-object v0, p0, Ldff;->i:Lddu;

    if-eqz v0, :cond_7

    .line 1132
    const/16 v0, 0xa

    iget-object v1, p0, Ldff;->i:Lddu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1134
    :cond_7
    iget-object v0, p0, Ldff;->j:Ldfg;

    if-eqz v0, :cond_8

    .line 1135
    const/16 v0, 0xb

    iget-object v1, p0, Ldff;->j:Ldfg;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1137
    :cond_8
    iget-object v0, p0, Ldff;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    .line 1138
    const/16 v0, 0xc

    iget-object v1, p0, Ldff;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 1140
    :cond_9
    iget-object v0, p0, Ldff;->l:Ldme;

    if-eqz v0, :cond_a

    .line 1141
    const/16 v0, 0xd

    iget-object v1, p0, Ldff;->l:Ldme;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1143
    :cond_a
    iget-object v0, p0, Ldff;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1145
    return-void
.end method
