.class public Lbdb;
.super Lbcz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Lbcz;-><init>()V

    .line 78
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 2

    .prologue
    .line 82
    new-instance v0, Lddn;

    invoke-direct {v0}, Lddn;-><init>()V

    .line 83
    new-instance v1, Ldcz;

    invoke-direct {v1}, Ldcz;-><init>()V

    .line 85
    iput-object v0, v1, Ldcz;->c:Lddn;

    .line 86
    return-object v1
.end method

.method public a(Lyj;Lbph;)V
    .locals 3

    .prologue
    .line 96
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetChatAclSettingsOperation failed for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 97
    invoke-virtual {p1}, Lyj;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 96
    invoke-static {v0, v1, p2}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 98
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    const-string v0, "getchatacls"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    const-string v0, "ui_queue"

    return-object v0
.end method
