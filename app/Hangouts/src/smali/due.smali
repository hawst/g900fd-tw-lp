.class public final Ldue;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldue;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15931
    const/4 v0, 0x0

    new-array v0, v0, [Ldue;

    sput-object v0, Ldue;->a:[Ldue;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15932
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 15953
    const/4 v0, 0x0

    .line 15954
    iget-object v1, p0, Ldue;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 15955
    const/4 v0, 0x1

    iget-object v1, p0, Ldue;->b:Ljava/lang/String;

    .line 15956
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 15958
    :cond_0
    iget-object v1, p0, Ldue;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 15959
    const/4 v1, 0x2

    iget-object v2, p0, Ldue;->c:Ljava/lang/Long;

    .line 15960
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 15962
    :cond_1
    iget-object v1, p0, Ldue;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15963
    iput v0, p0, Ldue;->cachedSize:I

    .line 15964
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 15928
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldue;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldue;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldue;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldue;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldue;->c:Ljava/lang/Long;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 15941
    iget-object v0, p0, Ldue;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 15942
    const/4 v0, 0x1

    iget-object v1, p0, Ldue;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 15944
    :cond_0
    iget-object v0, p0, Ldue;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 15945
    const/4 v0, 0x2

    iget-object v1, p0, Ldue;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 15947
    :cond_1
    iget-object v0, p0, Ldue;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 15949
    return-void
.end method
