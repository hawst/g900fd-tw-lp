.class public final Ldgs;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldgs;


# instance fields
.field public b:Ljava/lang/Boolean;

.field public c:Ldgu;

.field public d:[Ljava/lang/Long;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/String;

.field public h:Ldgt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    new-array v0, v0, [Ldgs;

    sput-object v0, Ldgs;->a:[Ldgs;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 109
    invoke-direct {p0}, Lepn;-><init>()V

    .line 114
    iput-object v1, p0, Ldgs;->c:Ldgu;

    .line 117
    sget-object v0, Lept;->n:[Ljava/lang/Long;

    iput-object v0, p0, Ldgs;->d:[Ljava/lang/Long;

    .line 126
    iput-object v1, p0, Ldgs;->h:Ldgt;

    .line 109
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 161
    iget-object v0, p0, Ldgs;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 162
    const/4 v0, 0x1

    iget-object v2, p0, Ldgs;->b:Ljava/lang/Boolean;

    .line 163
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 165
    :goto_0
    iget-object v2, p0, Ldgs;->c:Ldgu;

    if-eqz v2, :cond_0

    .line 166
    const/4 v2, 0x2

    iget-object v3, p0, Ldgs;->c:Ldgu;

    .line 167
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 169
    :cond_0
    iget-object v2, p0, Ldgs;->d:[Ljava/lang/Long;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ldgs;->d:[Ljava/lang/Long;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 171
    iget-object v3, p0, Ldgs;->d:[Ljava/lang/Long;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 173
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Lepl;->c(J)I

    move-result v5

    add-int/2addr v2, v5

    .line 171
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 175
    :cond_1
    add-int/2addr v0, v2

    .line 176
    iget-object v1, p0, Ldgs;->d:[Ljava/lang/Long;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 178
    :cond_2
    iget-object v1, p0, Ldgs;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 179
    const/4 v1, 0x4

    iget-object v2, p0, Ldgs;->e:Ljava/lang/String;

    .line 180
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 182
    :cond_3
    iget-object v1, p0, Ldgs;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 183
    const/4 v1, 0x5

    iget-object v2, p0, Ldgs;->f:Ljava/lang/Integer;

    .line 184
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 186
    :cond_4
    iget-object v1, p0, Ldgs;->h:Ldgt;

    if-eqz v1, :cond_5

    .line 187
    const/4 v1, 0x6

    iget-object v2, p0, Ldgs;->h:Ldgt;

    .line 188
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 190
    :cond_5
    iget-object v1, p0, Ldgs;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 191
    const/4 v1, 0x7

    iget-object v2, p0, Ldgs;->g:Ljava/lang/String;

    .line 192
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 194
    :cond_6
    iget-object v1, p0, Ldgs;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 195
    iput v0, p0, Ldgs;->cachedSize:I

    .line 196
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 105
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldgs;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldgs;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldgs;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldgs;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldgs;->c:Ldgu;

    if-nez v0, :cond_2

    new-instance v0, Ldgu;

    invoke-direct {v0}, Ldgu;-><init>()V

    iput-object v0, p0, Ldgs;->c:Ldgu;

    :cond_2
    iget-object v0, p0, Ldgs;->c:Ldgu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldgs;->d:[Ljava/lang/Long;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Long;

    iget-object v2, p0, Ldgs;->d:[Ljava/lang/Long;

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldgs;->d:[Ljava/lang/Long;

    :goto_1
    iget-object v1, p0, Ldgs;->d:[Ljava/lang/Long;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Ldgs;->d:[Ljava/lang/Long;

    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Ldgs;->d:[Ljava/lang/Long;

    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgs;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgs;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Ldgs;->h:Ldgt;

    if-nez v0, :cond_4

    new-instance v0, Ldgt;

    invoke-direct {v0}, Ldgt;-><init>()V

    iput-object v0, p0, Ldgs;->h:Ldgt;

    :cond_4
    iget-object v0, p0, Ldgs;->h:Ldgt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgs;->g:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 7

    .prologue
    .line 131
    iget-object v0, p0, Ldgs;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 132
    const/4 v0, 0x1

    iget-object v1, p0, Ldgs;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 134
    :cond_0
    iget-object v0, p0, Ldgs;->c:Ldgu;

    if-eqz v0, :cond_1

    .line 135
    const/4 v0, 0x2

    iget-object v1, p0, Ldgs;->c:Ldgu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 137
    :cond_1
    iget-object v0, p0, Ldgs;->d:[Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 138
    iget-object v1, p0, Ldgs;->d:[Ljava/lang/Long;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 139
    const/4 v4, 0x3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {p1, v4, v5, v6}, Lepl;->b(IJ)V

    .line 138
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 142
    :cond_2
    iget-object v0, p0, Ldgs;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 143
    const/4 v0, 0x4

    iget-object v1, p0, Ldgs;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 145
    :cond_3
    iget-object v0, p0, Ldgs;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 146
    const/4 v0, 0x5

    iget-object v1, p0, Ldgs;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 148
    :cond_4
    iget-object v0, p0, Ldgs;->h:Ldgt;

    if-eqz v0, :cond_5

    .line 149
    const/4 v0, 0x6

    iget-object v1, p0, Ldgs;->h:Ldgt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 151
    :cond_5
    iget-object v0, p0, Ldgs;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 152
    const/4 v0, 0x7

    iget-object v1, p0, Ldgs;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 154
    :cond_6
    iget-object v0, p0, Ldgs;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 156
    return-void
.end method
