.class public final Ldmx;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldmx;


# instance fields
.field public b:Ldmw;

.field public c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    new-array v0, v0, [Ldmx;

    sput-object v0, Ldmx;->a:[Ldmx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Lepn;-><init>()V

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Ldmx;->b:Ldmw;

    .line 70
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 92
    const/4 v0, 0x0

    .line 93
    iget-object v1, p0, Ldmx;->b:Ldmw;

    if-eqz v1, :cond_0

    .line 94
    const/4 v0, 0x1

    iget-object v1, p0, Ldmx;->b:Ldmw;

    .line 95
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 97
    :cond_0
    iget-object v1, p0, Ldmx;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 98
    const/4 v1, 0x2

    iget-object v2, p0, Ldmx;->c:Ljava/lang/String;

    .line 99
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    :cond_1
    iget-object v1, p0, Ldmx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    iput v0, p0, Ldmx;->cachedSize:I

    .line 103
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 66
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldmx;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldmx;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldmx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldmx;->b:Ldmw;

    if-nez v0, :cond_2

    new-instance v0, Ldmw;

    invoke-direct {v0}, Ldmw;-><init>()V

    iput-object v0, p0, Ldmx;->b:Ldmw;

    :cond_2
    iget-object v0, p0, Ldmx;->b:Ldmw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldmx;->c:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Ldmx;->b:Ldmw;

    if-eqz v0, :cond_0

    .line 81
    const/4 v0, 0x1

    iget-object v1, p0, Ldmx;->b:Ldmw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 83
    :cond_0
    iget-object v0, p0, Ldmx;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 84
    const/4 v0, 0x2

    iget-object v1, p0, Ldmx;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 86
    :cond_1
    iget-object v0, p0, Ldmx;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 88
    return-void
.end method
