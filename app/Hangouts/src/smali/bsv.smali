.class public abstract Lbsv;
.super Lbrv;
.source "PG"


# instance fields
.field final a:Z

.field public b:Lbyu;

.field final d:Ljava/lang/String;

.field e:Lbsw;

.field private final f:Ljava/lang/Object;


# direct methods
.method public constructor <init>(ZLbyu;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lbrv;-><init>()V

    .line 94
    iput-object p4, p0, Lbsv;->f:Ljava/lang/Object;

    .line 95
    iput-boolean p1, p0, Lbsv;->a:Z

    .line 96
    iput-object p2, p0, Lbsv;->b:Lbyu;

    .line 97
    iput-object p3, p0, Lbsv;->d:Ljava/lang/String;

    .line 98
    return-void
.end method


# virtual methods
.method public abstract a([B)Lbsu;
.end method

.method public abstract a(Lbsu;)V
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x1

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lbsv;->b:Lbyu;

    invoke-virtual {v0}, Lbyu;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract c_()Lbsm;
.end method

.method public abstract e()Z
.end method

.method public abstract f()Z
.end method

.method public abstract g()V
.end method

.method public abstract h()V
.end method

.method public k()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lbsv;->f:Ljava/lang/Object;

    return-object v0
.end method

.method public l()Lyj;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lbsv;->b:Lbyu;

    invoke-virtual {v0}, Lbyu;->m()Lyj;

    move-result-object v0

    return-object v0
.end method

.method public m()Lbyu;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lbsv;->b:Lbyu;

    return-object v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 132
    iget-boolean v0, p0, Lbsv;->a:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " MediaUrl:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lbsv;->b:Lbyu;

    if-nez v0, :cond_0

    const-string v0, " None"

    .line 149
    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cacheable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 150
    invoke-virtual {p0}, Lbsv;->f()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cancelled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lbsv;->j()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " shouldUseLoaderQueue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 151
    invoke-virtual {p0}, Lbsv;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 148
    :cond_0
    iget-object v0, p0, Lbsv;->b:Lbyu;

    .line 149
    invoke-virtual {v0}, Lbyu;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
