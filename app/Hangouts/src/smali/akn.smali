.class public abstract Lakn;
.super Lkj;
.source "PG"

# interfaces
.implements Lbop;
.implements Lcwv;


# static fields
.field private static volatile A:Z

.field private static final r:Z


# instance fields
.field public final o:Lbme;

.field protected p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbbx;",
            ">;"
        }
    .end annotation
.end field

.field final q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:I

.field private w:Lajs;

.field private x:Z

.field private y:Z

.field private z:Lyj;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 67
    sget-object v0, Lbys;->d:Lcyp;

    sput-boolean v1, Lakn;->r:Z

    .line 110
    sput-boolean v1, Lakn;->A:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 119
    invoke-direct {p0}, Lkj;-><init>()V

    .line 92
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbme;

    invoke-static {v0, v1}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    iput-object v0, p0, Lakn;->o:Lbme;

    .line 673
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lakn;->q:Ljava/util/ArrayList;

    .line 120
    return-void
.end method

.method static synthetic a(Lakn;I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-virtual {p0}, Lakn;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_1

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {v0, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    iget v0, v2, Landroid/graphics/Point;->y:I

    :goto_0
    invoke-virtual {p0}, Lakn;->g()Lkd;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lkd;->e()I

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Lkd;->e()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    sub-int/2addr v0, v2

    const/16 v2, 0x64

    if-le v0, v2, :cond_5

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lakn;->t:Z

    sget-boolean v0, Lakn;->r:Z

    if-eqz v0, :cond_3

    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Emoji: onDisplayHeightChanged softkeyboardvisible="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lakn;->t:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " emojiGalleryVisible="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lakn;->s:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-boolean v0, p0, Lakn;->t:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lakn;->s:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0, v1}, Lakn;->c(Z)V

    :cond_4
    iget-boolean v0, p0, Lakn;->t:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lakn;->s:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lakn;->u:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lakn;->o()V

    iput-boolean v1, p0, Lakn;->u:Z

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public static b(Z)V
    .locals 0

    .prologue
    .line 113
    sput-boolean p0, Lakn;->A:Z

    .line 114
    return-void
.end method

.method private n()Lyj;
    .locals 3

    .prologue
    .line 273
    invoke-virtual {p0}, Lakn;->k()Lyj;

    move-result-object v0

    .line 274
    if-nez v0, :cond_0

    .line 275
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Account is null for activity "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    const/4 v0, 0x0

    .line 280
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    goto :goto_0
.end method

.method private o()V
    .locals 3

    .prologue
    .line 545
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 546
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Emoji: Showing gallery softkeyboardvisible="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lakn;->t:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " emojiGalleryVisible="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lakn;->s:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    :cond_0
    invoke-virtual {p0}, Lakn;->e()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->a()Lao;

    move-result-object v1

    .line 550
    sget v0, Lf;->bc:I

    sget v2, Lf;->bd:I

    invoke-virtual {v1, v0, v2}, Lao;->a(II)Lao;

    .line 551
    iget-object v0, p0, Lakn;->w:Lajs;

    if-nez v0, :cond_1

    .line 552
    const-class v0, Lajs;

    .line 553
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 552
    invoke-static {p0, v0}, Lt;->instantiate(Landroid/content/Context;Ljava/lang/String;)Lt;

    move-result-object v0

    check-cast v0, Lajs;

    iput-object v0, p0, Lakn;->w:Lajs;

    .line 554
    iget v0, p0, Lakn;->v:I

    iget-object v2, p0, Lakn;->w:Lajs;

    invoke-virtual {v1, v0, v2}, Lao;->a(ILt;)Lao;

    .line 558
    :goto_0
    invoke-virtual {v1}, Lao;->b()I

    .line 560
    const/4 v0, 0x1

    iput-boolean v0, p0, Lakn;->s:Z

    .line 561
    invoke-static {}, Lavy;->h()V

    .line 562
    return-void

    .line 556
    :cond_1
    iget-object v0, p0, Lakn;->w:Lajs;

    invoke-virtual {v1, v0}, Lao;->c(Lt;)Lao;

    goto :goto_0
.end method

.method private p()V
    .locals 3

    .prologue
    .line 570
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 571
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Emoji: hideKeyboard softkeyboardvisible="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lakn;->t:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " emojiGalleryVisible="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lakn;->s:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    :cond_0
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lakn;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 576
    if-eqz v0, :cond_1

    .line 577
    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInput(II)V

    .line 579
    :cond_1
    return-void
.end method


# virtual methods
.method public a(II)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 442
    iput p2, p0, Lakn;->v:I

    .line 446
    new-instance v0, Lakp;

    invoke-direct {v0, p0, p0}, Lakp;-><init>(Lakn;Landroid/content/Context;)V

    .line 454
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 456
    invoke-virtual {p0}, Lakn;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 457
    return-object v0
.end method

.method public a(Landroid/widget/EditText;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 598
    const-string v0, "Babel"

    invoke-static {v0, v3}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Emoji: attachment button clicked softkeyboardvisible="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lakn;->t:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " emojiGalleryVisible="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lakn;->s:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    :cond_0
    iget-boolean v0, p0, Lakn;->s:Z

    if-eqz v0, :cond_3

    .line 603
    const-string v0, "Babel"

    invoke-static {v0, v3}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Emoji: showKeyboard softkeyboardvisible="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lakn;->t:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " emojiGalleryVisible="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lakn;->s:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lakn;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_2

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 612
    :cond_2
    :goto_0
    return-void

    .line 605
    :cond_3
    iget-boolean v0, p0, Lakn;->t:Z

    if-eqz v0, :cond_4

    .line 606
    invoke-direct {p0}, Lakn;->p()V

    .line 607
    const/4 v0, 0x1

    iput-boolean v0, p0, Lakn;->u:Z

    goto :goto_0

    .line 609
    :cond_4
    invoke-direct {p0}, Lakn;->o()V

    goto :goto_0
.end method

.method public a(Lt;)V
    .locals 3

    .prologue
    .line 501
    invoke-super {p0, p1}, Lkj;->a(Lt;)V

    .line 502
    instance-of v0, p1, Lajs;

    if-eqz v0, :cond_2

    .line 503
    const/4 v0, 0x1

    iput-boolean v0, p0, Lakn;->s:Z

    .line 504
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 505
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Emoji: gallery attached softkeyboardvisible="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lakn;->t:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " emojiGalleryVisible="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lakn;->s:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    :cond_0
    iget-object v0, p0, Lakn;->w:Lajs;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lakn;->w:Lajs;

    if-eq v0, p1, :cond_1

    .line 510
    const-string v0, "Babel"

    const-string v1, "EmojiGalleryFragment being replaced!"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    :cond_1
    check-cast p1, Lajs;

    iput-object p1, p0, Lakn;->w:Lajs;

    .line 513
    :cond_2
    return-void
.end method

.method protected a(Lyj;)V
    .locals 4

    .prologue
    .line 390
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lbkb;->b(Lyj;Z)V

    .line 392
    invoke-virtual {p0}, Lakn;->k()Lyj;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 394
    const/4 v0, 0x0

    invoke-static {v0}, Lbkb;->g(Z)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 395
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 396
    if-eqz v0, :cond_0

    invoke-static {v0}, Lbkb;->f(Lyj;)I

    move-result v2

    const/16 v3, 0x66

    if-ne v2, v3, :cond_0

    .line 398
    invoke-static {v0}, Lbbl;->b(Lyj;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lakn;->startActivity(Landroid/content/Intent;)V

    .line 399
    invoke-virtual {p0}, Lakn;->finish()V

    .line 407
    :cond_1
    :goto_0
    return-void

    .line 405
    :cond_2
    invoke-virtual {p0}, Lakn;->finish()V

    goto :goto_0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 662
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lyj;Lbxl;)Z
    .locals 3

    .prologue
    .line 311
    iget-object v0, p2, Lbxl;->a:Landroid/content/Intent;

    .line 314
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lakn;->k()Lyj;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 315
    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v1

    const v2, -0x10000001

    and-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 316
    iput-object p1, p0, Lakn;->z:Lyj;

    .line 317
    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lakn;->startActivityForResult(Landroid/content/Intent;I)V

    .line 318
    const/4 v0, 0x1

    .line 321
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 250
    invoke-virtual {p0}, Lakn;->t_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    :goto_0
    return-void

    .line 253
    :cond_0
    new-instance v0, Lako;

    invoke-direct {v0, p0}, Lako;-><init>(Lakn;)V

    invoke-static {p1, p0, v0}, Lcfz;->a(ILandroid/app/Activity;Landroid/content/DialogInterface$OnCancelListener;)Z

    goto :goto_0
.end method

.method public b(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 686
    iget-object v0, p0, Lakn;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 687
    return-void
.end method

.method public c(Z)V
    .locals 3

    .prologue
    .line 521
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 522
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Emoji: hideEmojiGallery softkeyboardvisible="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lakn;->t:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " emojiGalleryVisible="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lakn;->s:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    :cond_0
    invoke-virtual {p0}, Lakn;->e()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->a()Lao;

    move-result-object v1

    .line 526
    sget v0, Lf;->bc:I

    sget v2, Lf;->bd:I

    invoke-virtual {v1, v0, v2}, Lao;->a(II)Lao;

    .line 527
    iget-object v0, p0, Lakn;->w:Lajs;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 528
    if-eqz p1, :cond_1

    .line 529
    const-class v0, Lajq;

    .line 530
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lt;->instantiate(Landroid/content/Context;Ljava/lang/String;)Lt;

    move-result-object v0

    check-cast v0, Lajq;

    .line 531
    iget v2, p0, Lakn;->v:I

    invoke-virtual {v1, v2, v0}, Lao;->a(ILt;)Lao;

    .line 535
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lakn;->w:Lajs;

    .line 536
    const/4 v0, 0x0

    iput-boolean v0, p0, Lakn;->s:Z

    .line 537
    invoke-virtual {v1}, Lao;->b()I

    .line 538
    return-void

    .line 533
    :cond_1
    iget-object v0, p0, Lakn;->w:Lajs;

    invoke-virtual {v1, v0}, Lao;->a(Lt;)Lao;

    goto :goto_0
.end method

.method public abstract k()Lyj;
.end method

.method public l()V
    .locals 3

    .prologue
    .line 618
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 619
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Emoj: hideKeyboardandEmoji clicked softkeyboardvisible="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lakn;->t:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " emojiGalleryVisible="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lakn;->s:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    :cond_0
    iget-boolean v0, p0, Lakn;->s:Z

    if-eqz v0, :cond_1

    .line 623
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lakn;->c(Z)V

    .line 625
    :cond_1
    iget-boolean v0, p0, Lakn;->t:Z

    if-eqz v0, :cond_2

    .line 626
    invoke-direct {p0}, Lakn;->p()V

    .line 628
    :cond_2
    return-void
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 666
    iget-boolean v0, p0, Lakn;->s:Z

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    .line 329
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "EsFragmentActivity.onActivityResult "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    invoke-super {p0, p1, p2, p3}, Lkj;->onActivityResult(IILandroid/content/Intent;)V

    .line 331
    packed-switch p1, :pswitch_data_0

    .line 381
    :cond_0
    :goto_0
    return-void

    .line 333
    :pswitch_0
    iget-object v0, p0, Lakn;->z:Lyj;

    if-eqz v0, :cond_0

    .line 334
    if-eq p2, v3, :cond_1

    .line 337
    iget-object v0, p0, Lakn;->z:Lyj;

    invoke-virtual {p0, v0}, Lakn;->a(Lyj;)V

    .line 342
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lakn;->z:Lyj;

    goto :goto_0

    .line 340
    :cond_1
    iget-object v0, p0, Lakn;->z:Lyj;

    invoke-static {v0, v2}, Lbkb;->a(Lyj;Z)V

    goto :goto_1

    .line 348
    :pswitch_1
    if-ne p2, v3, :cond_0

    .line 349
    if-eqz p3, :cond_0

    .line 350
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "new_conversation_created"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351
    invoke-virtual {p0}, Lakn;->finish()V

    goto :goto_0

    .line 370
    :pswitch_2
    const-string v0, "Babel"

    const-string v1, "Triggering contacts cache refresh"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    invoke-static {v2}, Lbsc;->a(Z)V

    goto :goto_0

    .line 377
    :pswitch_3
    iput-boolean v2, p0, Lakn;->x:Z

    goto :goto_0

    .line 331
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 124
    invoke-super {p0, p1}, Lkj;->onCreate(Landroid/os/Bundle;)V

    .line 127
    invoke-static {p0, p0}, Lcwt;->a(Landroid/content/Context;Lcwv;)V

    .line 130
    invoke-virtual {p0}, Lakn;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbbx;

    .line 129
    invoke-static {v0, v1}, Lcyj;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lakn;->p:Ljava/util/List;

    .line 131
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 632
    iget-object v0, p0, Lakn;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbbx;

    .line 633
    invoke-virtual {p0}, Lakn;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    invoke-interface {v0, v2, p1}, Lbbx;->a(Landroid/view/MenuInflater;Landroid/view/Menu;)V

    goto :goto_0

    .line 635
    :cond_0
    invoke-super {p0, p1}, Lkj;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 226
    invoke-super {p0}, Lkj;->onDestroy()V

    .line 227
    const/4 v0, 0x1

    iput-boolean v0, p0, Lakn;->y:Z

    .line 228
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 151
    invoke-super {p0, p1}, Lkj;->onNewIntent(Landroid/content/Intent;)V

    .line 155
    invoke-virtual {p0, p1}, Lakn;->setIntent(Landroid/content/Intent;)V

    .line 156
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 648
    invoke-virtual {p0, p1}, Lakn;->a(Landroid/view/MenuItem;)Z

    move-result v0

    .line 649
    if-nez v0, :cond_1

    .line 650
    iget-object v1, p0, Lakn;->p:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbbx;

    .line 651
    invoke-interface {v0, p0, p1}, Lbbx;->a(Landroid/app/Activity;Landroid/view/MenuItem;)Z

    move-result v0

    .line 652
    if-eqz v0, :cond_0

    .line 653
    :cond_1
    if-nez v0, :cond_2

    invoke-super {p0, p1}, Lkj;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 206
    invoke-super {p0}, Lkj;->onPause()V

    .line 208
    invoke-direct {p0}, Lakn;->n()Lyj;

    move-result-object v0

    .line 209
    if-nez v0, :cond_1

    .line 210
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "OnPause: Account is empty for activity "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 211
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 210
    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    :goto_0
    invoke-static {}, Lbya;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    invoke-static {}, Lbya;->a()V

    .line 220
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbop;)V

    .line 221
    invoke-static {}, Lbrc;->c()Lbrc;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbrc;->a(Z)V

    .line 222
    return-void

    .line 213
    :cond_1
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbpx;->a(Lyj;Z)V

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 640
    iget-object v0, p0, Lakn;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 643
    :cond_0
    invoke-super {p0, p1}, Lkj;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 160
    invoke-super {p0}, Lkj;->onResume()V

    .line 162
    sget-boolean v0, Lakn;->A:Z

    if-eqz v0, :cond_1

    .line 164
    sput-boolean v3, Lakn;->A:Z

    .line 165
    const/4 v0, 0x0

    invoke-static {v0}, Lbbl;->b(Lyj;)Landroid/content/Intent;

    move-result-object v0

    .line 166
    const v1, 0x4008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 167
    invoke-virtual {p0, v0}, Lakn;->startActivity(Landroid/content/Intent;)V

    .line 168
    invoke-virtual {p0}, Lakn;->finish()V

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    invoke-static {v1, v3}, Lf;->a(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-boolean v0, p0, Lakn;->x:Z

    if-eqz v0, :cond_2

    .line 180
    invoke-static {p0, p0}, Lcwt;->a(Landroid/content/Context;Lcwv;)V

    .line 182
    :cond_2
    iput-boolean v3, p0, Lakn;->x:Z

    .line 184
    invoke-direct {p0}, Lakn;->n()Lyj;

    move-result-object v0

    .line 185
    if-nez v0, :cond_4

    .line 186
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "OnResume: Account is empty for activity "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 187
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 186
    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :goto_1
    invoke-static {}, Lbya;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 193
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbya;->a(Ljava/lang/String;)V

    .line 196
    :cond_3
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbop;)V

    .line 197
    invoke-static {v3}, Lbkb;->d(Z)V

    .line 198
    invoke-static {}, Lbrc;->c()Lbrc;

    move-result-object v0

    invoke-virtual {v0, v3}, Lbrc;->a(Z)V

    .line 201
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->l()I

    goto :goto_0

    .line 189
    :cond_4
    invoke-static {v0, v1}, Lbpx;->a(Lyj;Z)V

    goto :goto_1
.end method

.method public onStart()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 135
    invoke-super {p0}, Lkj;->onStart()V

    .line 136
    sget-boolean v0, Lakn;->A:Z

    if-eqz v0, :cond_1

    .line 138
    const/4 v0, 0x0

    sput-boolean v0, Lakn;->A:Z

    .line 139
    invoke-static {v2}, Lbbl;->b(Lyj;)Landroid/content/Intent;

    move-result-object v0

    .line 140
    const v1, 0x4008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 141
    invoke-virtual {p0, v0}, Lakn;->startActivity(Landroid/content/Intent;)V

    .line 142
    invoke-virtual {p0}, Lakn;->finish()V

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 146
    :cond_1
    invoke-virtual {p0}, Lakn;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "Babel"

    const-string v1, "Account is no longer valid, go to home screen"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lakn;->setContentView(Landroid/view/View;)V

    invoke-static {v2}, Lbbl;->b(Lyj;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lakn;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lakn;->finish()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 677
    invoke-super {p0}, Lkj;->onStop()V

    .line 679
    iget-object v0, p0, Lakn;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 680
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 682
    :cond_0
    iget-object v0, p0, Lakn;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 683
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3

    .prologue
    .line 287
    invoke-super {p0, p1}, Lkj;->onWindowFocusChanged(Z)V

    .line 288
    if-eqz p1, :cond_1

    .line 289
    invoke-virtual {p0}, Lakn;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 290
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 291
    if-nez v0, :cond_0

    .line 292
    const-string v0, "Babel"

    const-string v1, "onWindowFocusChanged: intent acct is null, use current acct"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    invoke-virtual {p0}, Lakn;->k()Lyj;

    move-result-object v0

    .line 295
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lyj;->u()Z

    move-result v1

    if-nez v1, :cond_1

    .line 296
    invoke-static {v0}, Lbkb;->f(Lyj;)I

    move-result v1

    const/16 v2, 0x66

    if-ne v1, v2, :cond_1

    .line 298
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->h(Lyj;)V

    .line 301
    :cond_1
    return-void
.end method

.method public t_()Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 235
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 236
    invoke-super {p0}, Lkj;->isDestroyed()Z

    move-result v0

    .line 238
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lakn;->y:Z

    goto :goto_0
.end method
