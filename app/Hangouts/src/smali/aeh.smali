.class public Laeh;
.super Laee;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final a:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 639
    invoke-direct {p0}, Laee;-><init>()V

    .line 640
    iput-object p1, p0, Laeh;->a:Ljava/lang/String;

    .line 641
    iput-object p2, p0, Laeh;->d:Ljava/lang/String;

    .line 642
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 651
    iget-object v0, p0, Laeh;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 652
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Laeh;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Laeh;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 654
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Laeh;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public b()Laee;
    .locals 3

    .prologue
    .line 668
    new-instance v0, Laeh;

    iget-object v1, p0, Laeh;->a:Ljava/lang/String;

    iget-object v2, p0, Laeh;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Laeh;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 659
    iget-object v0, p0, Laeh;->a:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 663
    iget-object v0, p0, Laeh;->d:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 646
    iget-object v0, p0, Laeh;->a:Ljava/lang/String;

    return-object v0
.end method
