.class public final Lewm;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lewm;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Levq;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Long;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:[Ljava/lang/String;

.field public k:Ljava/lang/Boolean;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/Integer;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 391
    const/4 v0, 0x0

    new-array v0, v0, [Lewm;

    sput-object v0, Lewm;->a:[Lewm;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 392
    invoke-direct {p0}, Lepn;-><init>()V

    .line 405
    iput-object v1, p0, Lewm;->d:Levq;

    .line 418
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Lewm;->j:[Ljava/lang/String;

    .line 425
    iput-object v1, p0, Lewm;->m:Ljava/lang/Integer;

    .line 392
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 495
    iget-object v0, p0, Lewm;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    .line 496
    const/4 v0, 0x1

    iget-object v2, p0, Lewm;->b:Ljava/lang/Integer;

    .line 497
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 499
    :goto_0
    iget-object v2, p0, Lewm;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 500
    const/4 v2, 0x2

    iget-object v3, p0, Lewm;->c:Ljava/lang/Integer;

    .line 501
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 503
    :cond_0
    iget-object v2, p0, Lewm;->e:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 504
    const/4 v2, 0x3

    iget-object v3, p0, Lewm;->e:Ljava/lang/String;

    .line 505
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 507
    :cond_1
    iget-object v2, p0, Lewm;->f:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 508
    iget-object v2, p0, Lewm;->f:Ljava/lang/Integer;

    .line 509
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x4

    invoke-static {v3}, Lepl;->g(I)I

    move-result v3

    invoke-static {v2}, Lepl;->j(I)I

    move-result v2

    invoke-static {v2}, Lepl;->i(I)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 511
    :cond_2
    iget-object v2, p0, Lewm;->g:Ljava/lang/Long;

    if-eqz v2, :cond_3

    .line 512
    iget-object v2, p0, Lewm;->g:Ljava/lang/Long;

    .line 513
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v4, 0x5

    invoke-static {v4}, Lepl;->g(I)I

    move-result v4

    invoke-static {v2, v3}, Lepl;->d(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Lepl;->c(J)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 515
    :cond_3
    iget-object v2, p0, Lewm;->k:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    .line 516
    const/4 v2, 0x6

    iget-object v3, p0, Lewm;->k:Ljava/lang/Boolean;

    .line 517
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 519
    :cond_4
    iget-object v2, p0, Lewm;->h:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 520
    const/4 v2, 0x7

    iget-object v3, p0, Lewm;->h:Ljava/lang/String;

    .line 521
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 523
    :cond_5
    iget-object v2, p0, Lewm;->l:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 524
    const/16 v2, 0x8

    iget-object v3, p0, Lewm;->l:Ljava/lang/String;

    .line 525
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 527
    :cond_6
    iget-object v2, p0, Lewm;->m:Ljava/lang/Integer;

    if-eqz v2, :cond_7

    .line 528
    const/16 v2, 0x9

    iget-object v3, p0, Lewm;->m:Ljava/lang/Integer;

    .line 529
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 531
    :cond_7
    iget-object v2, p0, Lewm;->n:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 532
    const/16 v2, 0xa

    iget-object v3, p0, Lewm;->n:Ljava/lang/String;

    .line 533
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 535
    :cond_8
    iget-object v2, p0, Lewm;->d:Levq;

    if-eqz v2, :cond_9

    .line 536
    const/16 v2, 0xb

    iget-object v3, p0, Lewm;->d:Levq;

    .line 537
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 539
    :cond_9
    iget-object v2, p0, Lewm;->o:Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 540
    const/16 v2, 0xc

    iget-object v3, p0, Lewm;->o:Ljava/lang/String;

    .line 541
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 543
    :cond_a
    iget-object v2, p0, Lewm;->i:Ljava/lang/String;

    if-eqz v2, :cond_b

    .line 544
    const/16 v2, 0xd

    iget-object v3, p0, Lewm;->i:Ljava/lang/String;

    .line 545
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 547
    :cond_b
    iget-object v2, p0, Lewm;->p:Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 548
    const/16 v2, 0xe

    iget-object v3, p0, Lewm;->p:Ljava/lang/String;

    .line 549
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 551
    :cond_c
    iget-object v2, p0, Lewm;->j:[Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lewm;->j:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_e

    .line 553
    iget-object v3, p0, Lewm;->j:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_d

    aget-object v5, v3, v1

    .line 555
    invoke-static {v5}, Lepl;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 553
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 557
    :cond_d
    add-int/2addr v0, v2

    .line 558
    iget-object v1, p0, Lewm;->j:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 560
    :cond_e
    iget-object v1, p0, Lewm;->q:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    .line 561
    const/16 v1, 0x11

    iget-object v2, p0, Lewm;->q:Ljava/lang/Boolean;

    .line 562
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 564
    :cond_f
    iget-object v1, p0, Lewm;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 565
    iput v0, p0, Lewm;->cachedSize:I

    .line 566
    return v0

    :cond_10
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 388
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lewm;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lewm;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lewm;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lewm;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lewm;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewm;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lewm;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->o()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lewm;->g:Ljava/lang/Long;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lewm;->k:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewm;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewm;->l:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lewm;->m:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lewm;->m:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewm;->n:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lewm;->d:Levq;

    if-nez v0, :cond_4

    new-instance v0, Levq;

    invoke-direct {v0}, Levq;-><init>()V

    iput-object v0, p0, Lewm;->d:Levq;

    :cond_4
    iget-object v0, p0, Lewm;->d:Levq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewm;->o:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewm;->i:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewm;->p:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_f
    const/16 v0, 0x82

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Lewm;->j:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lewm;->j:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lewm;->j:[Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Lewm;->j:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_5

    iget-object v1, p0, Lewm;->j:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lewm;->j:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lewm;->q:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x82 -> :sswitch_f
        0x88 -> :sswitch_10
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 438
    iget-object v0, p0, Lewm;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 439
    const/4 v0, 0x1

    iget-object v1, p0, Lewm;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->d(II)V

    .line 441
    :cond_0
    iget-object v0, p0, Lewm;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 442
    const/4 v0, 0x2

    iget-object v1, p0, Lewm;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->d(II)V

    .line 444
    :cond_1
    iget-object v0, p0, Lewm;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 445
    const/4 v0, 0x3

    iget-object v1, p0, Lewm;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 447
    :cond_2
    iget-object v0, p0, Lewm;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 448
    iget-object v0, p0, Lewm;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lepl;->a(I)V

    .line 450
    :cond_3
    iget-object v0, p0, Lewm;->g:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 451
    iget-object v0, p0, Lewm;->g:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lepl;->a(J)V

    .line 453
    :cond_4
    iget-object v0, p0, Lewm;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 454
    const/4 v0, 0x6

    iget-object v1, p0, Lewm;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 456
    :cond_5
    iget-object v0, p0, Lewm;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 457
    const/4 v0, 0x7

    iget-object v1, p0, Lewm;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 459
    :cond_6
    iget-object v0, p0, Lewm;->l:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 460
    const/16 v0, 0x8

    iget-object v1, p0, Lewm;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 462
    :cond_7
    iget-object v0, p0, Lewm;->m:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 463
    const/16 v0, 0x9

    iget-object v1, p0, Lewm;->m:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 465
    :cond_8
    iget-object v0, p0, Lewm;->n:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 466
    const/16 v0, 0xa

    iget-object v1, p0, Lewm;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 468
    :cond_9
    iget-object v0, p0, Lewm;->d:Levq;

    if-eqz v0, :cond_a

    .line 469
    const/16 v0, 0xb

    iget-object v1, p0, Lewm;->d:Levq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 471
    :cond_a
    iget-object v0, p0, Lewm;->o:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 472
    const/16 v0, 0xc

    iget-object v1, p0, Lewm;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 474
    :cond_b
    iget-object v0, p0, Lewm;->i:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 475
    const/16 v0, 0xd

    iget-object v1, p0, Lewm;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 477
    :cond_c
    iget-object v0, p0, Lewm;->p:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 478
    const/16 v0, 0xe

    iget-object v1, p0, Lewm;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 480
    :cond_d
    iget-object v0, p0, Lewm;->j:[Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 481
    iget-object v1, p0, Lewm;->j:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 482
    const/16 v4, 0x10

    invoke-virtual {p1, v4, v3}, Lepl;->a(ILjava/lang/String;)V

    .line 481
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 485
    :cond_e
    iget-object v0, p0, Lewm;->q:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    .line 486
    const/16 v0, 0x11

    iget-object v1, p0, Lewm;->q:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 488
    :cond_f
    iget-object v0, p0, Lewm;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 490
    return-void
.end method
