.class public final Leql;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leql;

.field public static final b:Lepo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepo",
            "<",
            "Leql;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Leqc;

.field public i:Lerv;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/Integer;

.field public m:Ljava/lang/Integer;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Lepu;

.field public q:Lepu;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Leql;

    sput-object v0, Leql;->a:[Leql;

    .line 13
    const v0, 0x1a42a50

    new-instance v1, Leqm;

    invoke-direct {v1}, Leqm;-><init>()V

    .line 14
    invoke-static {v0, v1}, Lepo;->a(ILepp;)Lepo;

    move-result-object v0

    sput-object v0, Leql;->b:Lepo;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 27
    iput-object v0, p0, Leql;->h:Leqc;

    .line 30
    iput-object v0, p0, Leql;->i:Lerv;

    .line 45
    iput-object v0, p0, Leql;->p:Lepu;

    .line 48
    iput-object v0, p0, Leql;->q:Lepu;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 104
    const/4 v0, 0x0

    .line 105
    iget-object v1, p0, Leql;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 106
    const/4 v0, 0x1

    iget-object v1, p0, Leql;->c:Ljava/lang/String;

    .line 107
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 109
    :cond_0
    iget-object v1, p0, Leql;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 110
    const/4 v1, 0x2

    iget-object v2, p0, Leql;->d:Ljava/lang/String;

    .line 111
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 113
    :cond_1
    iget-object v1, p0, Leql;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 114
    const/4 v1, 0x3

    iget-object v2, p0, Leql;->e:Ljava/lang/String;

    .line 115
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 117
    :cond_2
    iget-object v1, p0, Leql;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 118
    const/4 v1, 0x4

    iget-object v2, p0, Leql;->f:Ljava/lang/String;

    .line 119
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 121
    :cond_3
    iget-object v1, p0, Leql;->g:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 122
    const/4 v1, 0x5

    iget-object v2, p0, Leql;->g:Ljava/lang/String;

    .line 123
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 125
    :cond_4
    iget-object v1, p0, Leql;->h:Leqc;

    if-eqz v1, :cond_5

    .line 126
    const/4 v1, 0x6

    iget-object v2, p0, Leql;->h:Leqc;

    .line 127
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 129
    :cond_5
    iget-object v1, p0, Leql;->i:Lerv;

    if-eqz v1, :cond_6

    .line 130
    const/4 v1, 0x7

    iget-object v2, p0, Leql;->i:Lerv;

    .line 131
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 133
    :cond_6
    iget-object v1, p0, Leql;->j:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 134
    const/16 v1, 0x8

    iget-object v2, p0, Leql;->j:Ljava/lang/String;

    .line 135
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 137
    :cond_7
    iget-object v1, p0, Leql;->k:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 138
    const/16 v1, 0x9

    iget-object v2, p0, Leql;->k:Ljava/lang/String;

    .line 139
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    :cond_8
    iget-object v1, p0, Leql;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 142
    const/16 v1, 0xa

    iget-object v2, p0, Leql;->l:Ljava/lang/Integer;

    .line 143
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 145
    :cond_9
    iget-object v1, p0, Leql;->m:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    .line 146
    const/16 v1, 0xb

    iget-object v2, p0, Leql;->m:Ljava/lang/Integer;

    .line 147
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 149
    :cond_a
    iget-object v1, p0, Leql;->n:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 150
    const/16 v1, 0xc

    iget-object v2, p0, Leql;->n:Ljava/lang/String;

    .line 151
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 153
    :cond_b
    iget-object v1, p0, Leql;->o:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 154
    const/16 v1, 0xd

    iget-object v2, p0, Leql;->o:Ljava/lang/String;

    .line 155
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 157
    :cond_c
    iget-object v1, p0, Leql;->p:Lepu;

    if-eqz v1, :cond_d

    .line 158
    const/16 v1, 0xe

    iget-object v2, p0, Leql;->p:Lepu;

    .line 159
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 161
    :cond_d
    iget-object v1, p0, Leql;->q:Lepu;

    if-eqz v1, :cond_e

    .line 162
    const/16 v1, 0xf

    iget-object v2, p0, Leql;->q:Lepu;

    .line 163
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 165
    :cond_e
    iget-object v1, p0, Leql;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 166
    iput v0, p0, Leql;->cachedSize:I

    .line 167
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leql;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leql;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leql;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leql;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leql;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leql;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leql;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leql;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Leql;->h:Leqc;

    if-nez v0, :cond_2

    new-instance v0, Leqc;

    invoke-direct {v0}, Leqc;-><init>()V

    iput-object v0, p0, Leql;->h:Leqc;

    :cond_2
    iget-object v0, p0, Leql;->h:Leqc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Leql;->i:Lerv;

    if-nez v0, :cond_3

    new-instance v0, Lerv;

    invoke-direct {v0}, Lerv;-><init>()V

    iput-object v0, p0, Leql;->i:Lerv;

    :cond_3
    iget-object v0, p0, Leql;->i:Lerv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leql;->j:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leql;->k:Ljava/lang/String;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leql;->l:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leql;->m:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leql;->n:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leql;->o:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Leql;->p:Lepu;

    if-nez v0, :cond_4

    new-instance v0, Lepu;

    invoke-direct {v0}, Lepu;-><init>()V

    iput-object v0, p0, Leql;->p:Lepu;

    :cond_4
    iget-object v0, p0, Leql;->p:Lepu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Leql;->q:Lepu;

    if-nez v0, :cond_5

    new-instance v0, Lepu;

    invoke-direct {v0}, Lepu;-><init>()V

    iput-object v0, p0, Leql;->q:Lepu;

    :cond_5
    iget-object v0, p0, Leql;->q:Lepu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Leql;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 54
    const/4 v0, 0x1

    iget-object v1, p0, Leql;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 56
    :cond_0
    iget-object v0, p0, Leql;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 57
    const/4 v0, 0x2

    iget-object v1, p0, Leql;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 59
    :cond_1
    iget-object v0, p0, Leql;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 60
    const/4 v0, 0x3

    iget-object v1, p0, Leql;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 62
    :cond_2
    iget-object v0, p0, Leql;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 63
    const/4 v0, 0x4

    iget-object v1, p0, Leql;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 65
    :cond_3
    iget-object v0, p0, Leql;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 66
    const/4 v0, 0x5

    iget-object v1, p0, Leql;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 68
    :cond_4
    iget-object v0, p0, Leql;->h:Leqc;

    if-eqz v0, :cond_5

    .line 69
    const/4 v0, 0x6

    iget-object v1, p0, Leql;->h:Leqc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 71
    :cond_5
    iget-object v0, p0, Leql;->i:Lerv;

    if-eqz v0, :cond_6

    .line 72
    const/4 v0, 0x7

    iget-object v1, p0, Leql;->i:Lerv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 74
    :cond_6
    iget-object v0, p0, Leql;->j:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 75
    const/16 v0, 0x8

    iget-object v1, p0, Leql;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 77
    :cond_7
    iget-object v0, p0, Leql;->k:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 78
    const/16 v0, 0x9

    iget-object v1, p0, Leql;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 80
    :cond_8
    iget-object v0, p0, Leql;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 81
    const/16 v0, 0xa

    iget-object v1, p0, Leql;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 83
    :cond_9
    iget-object v0, p0, Leql;->m:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 84
    const/16 v0, 0xb

    iget-object v1, p0, Leql;->m:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 86
    :cond_a
    iget-object v0, p0, Leql;->n:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 87
    const/16 v0, 0xc

    iget-object v1, p0, Leql;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 89
    :cond_b
    iget-object v0, p0, Leql;->o:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 90
    const/16 v0, 0xd

    iget-object v1, p0, Leql;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 92
    :cond_c
    iget-object v0, p0, Leql;->p:Lepu;

    if-eqz v0, :cond_d

    .line 93
    const/16 v0, 0xe

    iget-object v1, p0, Leql;->p:Lepu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 95
    :cond_d
    iget-object v0, p0, Leql;->q:Lepu;

    if-eqz v0, :cond_e

    .line 96
    const/16 v0, 0xf

    iget-object v1, p0, Leql;->q:Lepu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 98
    :cond_e
    iget-object v0, p0, Leql;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 100
    return-void
.end method
