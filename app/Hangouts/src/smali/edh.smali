.class public abstract enum Ledh;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ledh;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ledh;

.field public static final enum b:Ledh;

.field public static final enum c:Ledh;

.field public static final enum d:Ledh;

.field public static final enum e:Ledh;

.field private static final synthetic f:[Ledh;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    new-instance v0, Ledi;

    const-string v1, "EXPLICIT"

    invoke-direct {v0, v1}, Ledi;-><init>(Ljava/lang/String;)V

    sput-object v0, Ledh;->a:Ledh;

    .line 54
    new-instance v0, Ledj;

    const-string v1, "REPLACED"

    invoke-direct {v0, v1}, Ledj;-><init>(Ljava/lang/String;)V

    sput-object v0, Ledh;->b:Ledh;

    .line 67
    new-instance v0, Ledk;

    const-string v1, "COLLECTED"

    invoke-direct {v0, v1}, Ledk;-><init>(Ljava/lang/String;)V

    sput-object v0, Ledh;->c:Ledh;

    .line 79
    new-instance v0, Ledl;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1}, Ledl;-><init>(Ljava/lang/String;)V

    sput-object v0, Ledh;->d:Ledh;

    .line 91
    new-instance v0, Ledm;

    const-string v1, "SIZE"

    invoke-direct {v0, v1}, Ledm;-><init>(Ljava/lang/String;)V

    sput-object v0, Ledh;->e:Ledh;

    .line 32
    const/4 v0, 0x5

    new-array v0, v0, [Ledh;

    const/4 v1, 0x0

    sget-object v2, Ledh;->a:Ledh;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Ledh;->b:Ledh;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Ledh;->c:Ledh;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Ledh;->d:Ledh;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Ledh;->e:Ledh;

    aput-object v2, v0, v1

    sput-object v0, Ledh;->f:[Ledh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ledh;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ledh;
    .locals 1

    .prologue
    .line 32
    const-class v0, Ledh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ledh;

    return-object v0
.end method

.method public static values()[Ledh;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Ledh;->f:[Ledh;

    invoke-virtual {v0}, [Ledh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ledh;

    return-object v0
.end method


# virtual methods
.method abstract a()Z
.end method
