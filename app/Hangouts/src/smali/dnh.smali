.class public final Ldnh;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldnh;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/Double;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/Integer;

.field public k:Ljava/lang/Integer;

.field public l:Ljava/lang/Boolean;

.field public m:Ljava/lang/Boolean;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/Integer;

.field public p:Ljava/lang/Boolean;

.field public q:Ljava/lang/Boolean;

.field public r:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3568
    const/4 v0, 0x0

    new-array v0, v0, [Ldnh;

    sput-object v0, Ldnh;->a:[Ldnh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3569
    invoke-direct {p0}, Lepn;-><init>()V

    .line 3572
    const/4 v0, 0x0

    iput-object v0, p0, Ldnh;->b:Ljava/lang/Integer;

    .line 3569
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 3666
    const/4 v0, 0x0

    .line 3667
    iget-object v1, p0, Ldnh;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3668
    const/4 v0, 0x1

    iget-object v1, p0, Ldnh;->d:Ljava/lang/String;

    .line 3669
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3671
    :cond_0
    iget-object v1, p0, Ldnh;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3672
    const/4 v1, 0x2

    iget-object v2, p0, Ldnh;->e:Ljava/lang/String;

    .line 3673
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3675
    :cond_1
    iget-object v1, p0, Ldnh;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 3676
    const/4 v1, 0x3

    iget-object v2, p0, Ldnh;->f:Ljava/lang/String;

    .line 3677
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3679
    :cond_2
    iget-object v1, p0, Ldnh;->g:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 3680
    const/4 v1, 0x4

    iget-object v2, p0, Ldnh;->g:Ljava/lang/String;

    .line 3681
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3683
    :cond_3
    iget-object v1, p0, Ldnh;->h:Ljava/lang/Double;

    if-eqz v1, :cond_4

    .line 3684
    const/4 v1, 0x5

    iget-object v2, p0, Ldnh;->h:Ljava/lang/Double;

    .line 3685
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 3687
    :cond_4
    iget-object v1, p0, Ldnh;->i:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 3688
    const/4 v1, 0x6

    iget-object v2, p0, Ldnh;->i:Ljava/lang/String;

    .line 3689
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3691
    :cond_5
    iget-object v1, p0, Ldnh;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 3692
    const/4 v1, 0x7

    iget-object v2, p0, Ldnh;->j:Ljava/lang/Integer;

    .line 3693
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3695
    :cond_6
    iget-object v1, p0, Ldnh;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 3696
    const/16 v1, 0x8

    iget-object v2, p0, Ldnh;->k:Ljava/lang/Integer;

    .line 3697
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3699
    :cond_7
    iget-object v1, p0, Ldnh;->l:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 3700
    const/16 v1, 0x9

    iget-object v2, p0, Ldnh;->l:Ljava/lang/Boolean;

    .line 3701
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3703
    :cond_8
    iget-object v1, p0, Ldnh;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 3704
    const/16 v1, 0xa

    iget-object v2, p0, Ldnh;->b:Ljava/lang/Integer;

    .line 3705
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3707
    :cond_9
    iget-object v1, p0, Ldnh;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    .line 3708
    const/16 v1, 0xb

    iget-object v2, p0, Ldnh;->c:Ljava/lang/Integer;

    .line 3709
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3711
    :cond_a
    iget-object v1, p0, Ldnh;->m:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 3712
    const/16 v1, 0xc

    iget-object v2, p0, Ldnh;->m:Ljava/lang/Boolean;

    .line 3713
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3715
    :cond_b
    iget-object v1, p0, Ldnh;->n:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 3716
    const/16 v1, 0xd

    iget-object v2, p0, Ldnh;->n:Ljava/lang/String;

    .line 3717
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3719
    :cond_c
    iget-object v1, p0, Ldnh;->o:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 3720
    const/16 v1, 0xe

    iget-object v2, p0, Ldnh;->o:Ljava/lang/Integer;

    .line 3721
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3723
    :cond_d
    iget-object v1, p0, Ldnh;->p:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    .line 3724
    const/16 v1, 0xf

    iget-object v2, p0, Ldnh;->p:Ljava/lang/Boolean;

    .line 3725
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3727
    :cond_e
    iget-object v1, p0, Ldnh;->q:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    .line 3728
    const/16 v1, 0x10

    iget-object v2, p0, Ldnh;->q:Ljava/lang/Boolean;

    .line 3729
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3731
    :cond_f
    iget-object v1, p0, Ldnh;->r:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 3732
    const/16 v1, 0x11

    iget-object v2, p0, Ldnh;->r:Ljava/lang/String;

    .line 3733
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3735
    :cond_10
    iget-object v1, p0, Ldnh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3736
    iput v0, p0, Ldnh;->cachedSize:I

    .line 3737
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 3565
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldnh;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldnh;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldnh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnh;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnh;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnh;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnh;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->b()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Ldnh;->h:Ljava/lang/Double;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnh;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldnh;->j:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldnh;->k:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldnh;->l:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldnh;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldnh;->b:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldnh;->c:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldnh;->m:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnh;->n:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldnh;->o:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldnh;->p:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldnh;->q:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnh;->r:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x29 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x8a -> :sswitch_11
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 3609
    iget-object v0, p0, Ldnh;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3610
    const/4 v0, 0x1

    iget-object v1, p0, Ldnh;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3612
    :cond_0
    iget-object v0, p0, Ldnh;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3613
    const/4 v0, 0x2

    iget-object v1, p0, Ldnh;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3615
    :cond_1
    iget-object v0, p0, Ldnh;->f:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3616
    const/4 v0, 0x3

    iget-object v1, p0, Ldnh;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3618
    :cond_2
    iget-object v0, p0, Ldnh;->g:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 3619
    const/4 v0, 0x4

    iget-object v1, p0, Ldnh;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3621
    :cond_3
    iget-object v0, p0, Ldnh;->h:Ljava/lang/Double;

    if-eqz v0, :cond_4

    .line 3622
    const/4 v0, 0x5

    iget-object v1, p0, Ldnh;->h:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(ID)V

    .line 3624
    :cond_4
    iget-object v0, p0, Ldnh;->i:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 3625
    const/4 v0, 0x6

    iget-object v1, p0, Ldnh;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3627
    :cond_5
    iget-object v0, p0, Ldnh;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 3628
    const/4 v0, 0x7

    iget-object v1, p0, Ldnh;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 3630
    :cond_6
    iget-object v0, p0, Ldnh;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 3631
    const/16 v0, 0x8

    iget-object v1, p0, Ldnh;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 3633
    :cond_7
    iget-object v0, p0, Ldnh;->l:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 3634
    const/16 v0, 0x9

    iget-object v1, p0, Ldnh;->l:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 3636
    :cond_8
    iget-object v0, p0, Ldnh;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 3637
    const/16 v0, 0xa

    iget-object v1, p0, Ldnh;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 3639
    :cond_9
    iget-object v0, p0, Ldnh;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 3640
    const/16 v0, 0xb

    iget-object v1, p0, Ldnh;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 3642
    :cond_a
    iget-object v0, p0, Ldnh;->m:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    .line 3643
    const/16 v0, 0xc

    iget-object v1, p0, Ldnh;->m:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 3645
    :cond_b
    iget-object v0, p0, Ldnh;->n:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 3646
    const/16 v0, 0xd

    iget-object v1, p0, Ldnh;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3648
    :cond_c
    iget-object v0, p0, Ldnh;->o:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 3649
    const/16 v0, 0xe

    iget-object v1, p0, Ldnh;->o:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 3651
    :cond_d
    iget-object v0, p0, Ldnh;->p:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    .line 3652
    const/16 v0, 0xf

    iget-object v1, p0, Ldnh;->p:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 3654
    :cond_e
    iget-object v0, p0, Ldnh;->q:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    .line 3655
    const/16 v0, 0x10

    iget-object v1, p0, Ldnh;->q:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 3657
    :cond_f
    iget-object v0, p0, Ldnh;->r:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 3658
    const/16 v0, 0x11

    iget-object v1, p0, Ldnh;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3660
    :cond_10
    iget-object v0, p0, Ldnh;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 3662
    return-void
.end method
