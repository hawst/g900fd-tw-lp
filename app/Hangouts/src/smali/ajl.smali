.class public final Lajl;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lajk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 239
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lajl;->a:Ljava/util/Map;

    .line 242
    return-void
.end method


# virtual methods
.method a()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lajk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 276
    iget-object v0, p0, Lajl;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method a(Lajk;)V
    .locals 9

    .prologue
    const/4 v7, 0x1

    .line 251
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s, TransportId: {%d}"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lajk;->b:Lbdh;

    iget-object v4, v4, Lbdh;->b:Lbdk;

    invoke-virtual {v4}, Lbdk;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    iget v3, p1, Lajk;->i:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 253
    iget-object v0, p0, Lajl;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajk;

    .line 254
    if-nez v0, :cond_0

    .line 255
    iget-object v0, p0, Lajl;->a:Ljava/util/Map;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    :goto_0
    return-void

    .line 257
    :cond_0
    iget-object v2, p0, Lajl;->a:Ljava/util/Map;

    if-nez v0, :cond_2

    :cond_1
    :goto_1
    invoke-interface {v2, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    iget-object v3, v0, Lajk;->g:Lyv;

    iget-object v4, p1, Lajk;->g:Lyv;

    if-eqz v4, :cond_4

    if-eqz v3, :cond_1

    iget v5, v3, Lyv;->r:I

    iget v6, v4, Lyv;->r:I

    if-eq v5, v6, :cond_3

    iget v5, v3, Lyv;->r:I

    if-eq v5, v7, :cond_4

    iget v5, v4, Lyv;->r:I

    if-eq v5, v7, :cond_1

    :cond_3
    iget-wide v5, v3, Lyv;->q:J

    iget-wide v7, v4, Lyv;->q:J

    cmp-long v5, v5, v7

    if-gtz v5, :cond_4

    iget-wide v5, v3, Lyv;->q:J

    iget-wide v3, v4, Lyv;->q:J

    cmp-long v3, v5, v3

    if-ltz v3, :cond_1

    iget-object v3, v0, Lajk;->h:Laea;

    iget-object v4, p1, Lajk;->h:Laea;

    if-eqz v4, :cond_4

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Laea;->k()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v4}, Laea;->k()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-gt v4, v3, :cond_1

    :cond_4
    move-object p1, v0

    goto :goto_1
.end method
