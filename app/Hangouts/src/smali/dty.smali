.class public final Ldty;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldty;


# instance fields
.field public b:Ldvm;

.field public c:Ldqf;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16904
    const/4 v0, 0x0

    new-array v0, v0, [Ldty;

    sput-object v0, Ldty;->a:[Ldty;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16905
    invoke-direct {p0}, Lepn;-><init>()V

    .line 16908
    iput-object v0, p0, Ldty;->b:Ldvm;

    .line 16911
    iput-object v0, p0, Ldty;->c:Ldqf;

    .line 16914
    iput-object v0, p0, Ldty;->d:Ljava/lang/Integer;

    .line 16905
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 16939
    const/4 v0, 0x0

    .line 16940
    iget-object v1, p0, Ldty;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 16941
    const/4 v0, 0x1

    iget-object v1, p0, Ldty;->b:Ldvm;

    .line 16942
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 16944
    :cond_0
    iget-object v1, p0, Ldty;->c:Ldqf;

    if-eqz v1, :cond_1

    .line 16945
    const/4 v1, 0x2

    iget-object v2, p0, Ldty;->c:Ldqf;

    .line 16946
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16948
    :cond_1
    iget-object v1, p0, Ldty;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 16949
    const/4 v1, 0x3

    iget-object v2, p0, Ldty;->d:Ljava/lang/Integer;

    .line 16950
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 16952
    :cond_2
    iget-object v1, p0, Ldty;->e:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 16953
    const/4 v1, 0x4

    iget-object v2, p0, Ldty;->e:Ljava/lang/Long;

    .line 16954
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 16956
    :cond_3
    iget-object v1, p0, Ldty;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16957
    iput v0, p0, Ldty;->cachedSize:I

    .line 16958
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 16901
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldty;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldty;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldty;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldty;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldty;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldty;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldty;->c:Ldqf;

    if-nez v0, :cond_3

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldty;->c:Ldqf;

    :cond_3
    iget-object v0, p0, Ldty;->c:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldty;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldty;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldty;->e:Ljava/lang/Long;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 16921
    iget-object v0, p0, Ldty;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 16922
    const/4 v0, 0x1

    iget-object v1, p0, Ldty;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 16924
    :cond_0
    iget-object v0, p0, Ldty;->c:Ldqf;

    if-eqz v0, :cond_1

    .line 16925
    const/4 v0, 0x2

    iget-object v1, p0, Ldty;->c:Ldqf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 16927
    :cond_1
    iget-object v0, p0, Ldty;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 16928
    const/4 v0, 0x3

    iget-object v1, p0, Ldty;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 16930
    :cond_2
    iget-object v0, p0, Ldty;->e:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 16931
    const/4 v0, 0x4

    iget-object v1, p0, Ldty;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 16933
    :cond_3
    iget-object v0, p0, Ldty;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 16935
    return-void
.end method
