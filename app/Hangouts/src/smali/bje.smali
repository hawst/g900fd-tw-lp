.class public final Lbje;
.super Lbiq;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:J

.field public final d:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Ldqt;)V
    .locals 2

    .prologue
    .line 1277
    invoke-direct {p0}, Lbiq;-><init>()V

    .line 1278
    iget-object v0, p1, Ldqt;->b:Ldqf;

    iget-object v0, v0, Ldqf;->b:Ljava/lang/String;

    iput-object v0, p0, Lbje;->b:Ljava/lang/String;

    .line 1279
    iget-object v0, p1, Ldqt;->c:Ldqs;

    if-nez v0, :cond_1

    .line 1280
    const-string v0, "Babel"

    const-string v1, "DeleteConversationNotification without deteleAction"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 1281
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbje;->c:J

    .line 1290
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lbje;->d:[Ljava/lang/String;

    .line 1293
    :goto_0
    return-void

    .line 1284
    :cond_1
    iget-object v0, p1, Ldqt;->c:Ldqs;

    iget-object v0, v0, Ldqs;->d:Ljava/lang/Long;

    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    iput-wide v0, p0, Lbje;->c:J

    .line 1286
    iget-object v0, p1, Ldqt;->c:Ldqs;

    iget-object v0, v0, Ldqs;->b:Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1288
    iget-object v0, p1, Ldqt;->c:Ldqs;

    iget-object v0, v0, Ldqs;->e:[Ljava/lang/String;

    iput-object v0, p0, Lbje;->d:[Ljava/lang/String;

    goto :goto_0
.end method
