.class public final Laao;
.super Landroid/database/CursorWrapper;
.source "PG"

# interfaces
.implements Ladk;


# instance fields
.field private a:Ladb;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 27
    return-void
.end method


# virtual methods
.method public a()Lcvw;
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 41
    const/4 v0, 0x0

    .line 42
    iget-object v1, p0, Laao;->a:Ladb;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Laao;->e()I

    move-result v1

    if-eq v1, v2, :cond_0

    .line 47
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Laao;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 49
    invoke-virtual {p0, v2}, Laao;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 48
    invoke-static {v2}, Lbzd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 53
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 55
    iget-object v0, p0, Laao;->a:Ladb;

    invoke-virtual {v0, v1}, Ladb;->a(Ljava/lang/String;)Lcvw;

    move-result-object v0

    .line 63
    :cond_0
    :goto_0
    return-object v0

    .line 56
    :cond_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 57
    iget-object v0, p0, Laao;->a:Ladb;

    invoke-virtual {v0, v2}, Ladb;->b(Ljava/lang/String;)Lcvw;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ladb;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Laao;->a:Ladb;

    .line 32
    return-void
.end method

.method public b()Laea;
    .locals 4

    .prologue
    .line 91
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Laao;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 92
    new-instance v1, Laea;

    new-instance v2, Laeh;

    const-string v3, ""

    invoke-direct {v2, v0, v3}, Laeh;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Laea;-><init>(Laeh;)V

    .line 93
    return-object v1
.end method

.method public c()Ladm;
    .locals 6

    .prologue
    .line 101
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Laao;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 102
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Laao;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 103
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Laao;->getLong(I)J

    move-result-wide v3

    .line 104
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Laao;->getInt(I)I

    move-result v5

    .line 105
    new-instance v0, Ladm;

    invoke-direct/range {v0 .. v5}, Ladm;-><init>(Ljava/lang/String;Ljava/lang/String;JI)V

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Laao;->getInt(I)I

    move-result v0

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x1

    return v0
.end method
