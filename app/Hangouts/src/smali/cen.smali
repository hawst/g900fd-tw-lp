.class public final Lcen;
.super Landroid/widget/ArrayAdapter;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/views/TransportSpinner;

.field private final b:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/views/TransportSpinner;Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 381
    iput-object p1, p0, Lcen;->a:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    .line 382
    invoke-direct {p0, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 383
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcen;->b:Landroid/view/LayoutInflater;

    .line 384
    return-void
.end method

.method private static a(I)I
    .locals 1

    .prologue
    .line 387
    const/4 v0, 0x2

    if-ne p0, v0, :cond_0

    .line 388
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->bX:I

    .line 390
    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->bW:I

    goto :goto_0
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/16 v10, 0x8

    const/4 v9, 0x0

    .line 446
    invoke-virtual {p0, p1}, Lcen;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 448
    instance-of v2, v0, Lajk;

    if-eqz v2, :cond_7

    .line 449
    iget-object v2, p0, Lcen;->b:Landroid/view/LayoutInflater;

    sget v3, Lf;->eE:I

    invoke-virtual {v2, v3, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 451
    check-cast v0, Lajk;

    .line 453
    iget-object v2, v0, Lajk;->b:Lbdh;

    if-eqz v2, :cond_2

    iget-object v1, v0, Lajk;->b:Lbdh;

    .line 454
    invoke-virtual {v1}, Lbdh;->a()Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    .line 455
    :goto_0
    invoke-virtual {v0}, Lajk;->a()I

    move-result v7

    .line 457
    sget v1, Lg;->du:I

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 458
    sget v2, Lg;->gU:I

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 459
    sget v3, Lg;->hm:I

    invoke-virtual {v6, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 460
    sget v4, Lg;->hh:I

    invoke-virtual {v6, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 462
    invoke-static {v7}, Lf;->c(I)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 463
    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 464
    invoke-static {v7}, Lcen;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 465
    iget-object v1, p0, Lcen;->a:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->f(Lcom/google/android/apps/hangouts/views/TransportSpinner;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget v1, Lh;->gP:I

    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    .line 466
    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 467
    iget-object v1, v0, Lajk;->e:Ljava/lang/String;

    invoke-static {v1}, Lbzd;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 468
    iget-object v1, p0, Lcen;->a:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    .line 469
    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->ck:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 468
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 471
    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 472
    iget-object v1, v0, Lajk;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 473
    iget-object v0, v0, Lajk;->f:Ljava/lang/String;

    .line 474
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 475
    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 476
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    move-object v0, v6

    .line 516
    :goto_2
    if-nez v0, :cond_1

    .line 520
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lcen;->b:Landroid/view/LayoutInflater;

    invoke-virtual {v1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 521
    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 523
    :cond_1
    return-object v0

    :cond_2
    move-object v5, v1

    .line 454
    goto/16 :goto_0

    .line 465
    :cond_3
    sget v1, Lh;->lj:I

    goto :goto_1

    .line 479
    :cond_4
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 482
    iget-object v7, p0, Lcen;->a:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    invoke-static {v7}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->c(Lcom/google/android/apps/hangouts/views/TransportSpinner;)Ljava/util/HashMap;

    move-result-object v7

    invoke-virtual {v7, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 483
    iget-object v7, p0, Lcen;->a:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    invoke-static {v7, v5, v1}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a(Lcom/google/android/apps/hangouts/views/TransportSpinner;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 484
    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 485
    iget-object v1, v0, Lajk;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 486
    iget-object v0, v0, Lajk;->e:Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 490
    :goto_3
    iget-object v0, p0, Lcen;->a:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    .line 491
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->fu:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 490
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 494
    iget-object v0, p0, Lcen;->a:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    .line 495
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->cj:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 494
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_5
    move-object v0, v6

    .line 497
    goto :goto_2

    .line 488
    :cond_6
    iget-object v0, v0, Lajk;->d:Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 497
    :cond_7
    instance-of v2, v0, Lceo;

    if-eqz v2, :cond_a

    .line 498
    check-cast v0, Lceo;

    .line 500
    iget-object v1, p0, Lcen;->b:Landroid/view/LayoutInflater;

    sget v2, Lf;->eG:I

    invoke-virtual {v1, v2, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 502
    sget v1, Lg;->hm:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 503
    sget v2, Lg;->du:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 504
    invoke-virtual {v0}, Lceo;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 505
    invoke-virtual {v0}, Lceo;->b()I

    move-result v1

    .line 506
    if-nez v1, :cond_9

    .line 507
    invoke-virtual {v2, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 512
    :goto_4
    instance-of v1, v0, Landroid/view/View$OnClickListener;

    if-eqz v1, :cond_8

    .line 513
    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_8
    move-object v0, v3

    goto/16 :goto_2

    .line 509
    :cond_9
    invoke-virtual {v2, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 510
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_4

    :cond_a
    move-object v0, v1

    goto/16 :goto_2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/16 v8, 0x8

    const/4 v5, 0x0

    .line 396
    invoke-virtual {p0, p1}, Lcen;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 398
    instance-of v1, v0, Lajk;

    if-eqz v1, :cond_a

    .line 399
    check-cast v0, Lajk;

    move-object v3, v0

    .line 402
    :goto_0
    iget-object v0, p0, Lcen;->a:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a(Lcom/google/android/apps/hangouts/views/TransportSpinner;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 403
    iget-object v0, p0, Lcen;->a:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    iget-object v1, p0, Lcen;->b:Landroid/view/LayoutInflater;

    sget v2, Lf;->gA:I

    invoke-virtual {v1, v2, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a(Lcom/google/android/apps/hangouts/views/TransportSpinner;Landroid/view/View;)Landroid/view/View;

    .line 406
    :cond_0
    iget-object v0, p0, Lcen;->a:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a(Lcom/google/android/apps/hangouts/views/TransportSpinner;)Landroid/view/View;

    move-result-object v0

    sget v1, Lg;->K:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 407
    iget-object v1, p0, Lcen;->a:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a(Lcom/google/android/apps/hangouts/views/TransportSpinner;)Landroid/view/View;

    move-result-object v1

    sget v2, Lg;->hN:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 408
    iget-object v2, p0, Lcen;->a:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a(Lcom/google/android/apps/hangouts/views/TransportSpinner;)Landroid/view/View;

    move-result-object v2

    sget v6, Lg;->gU:I

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 412
    if-eqz v3, :cond_2

    iget-object v6, v3, Lajk;->c:Ljava/lang/String;

    .line 413
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, v3, Lajk;->b:Lbdh;

    if-eqz v6, :cond_2

    :cond_1
    iget-object v6, p0, Lcen;->a:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    .line 414
    invoke-static {v6}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->e(Lcom/google/android/apps/hangouts/views/TransportSpinner;)[Lajk;

    move-result-object v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcen;->a:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    invoke-static {v6}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->e(Lcom/google/android/apps/hangouts/views/TransportSpinner;)[Lajk;

    move-result-object v6

    array-length v6, v6

    const/4 v7, 0x2

    if-ge v6, v7, :cond_3

    .line 415
    :cond_2
    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 420
    :goto_1
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lajk;->a()I

    move-result v0

    .line 423
    :goto_2
    if-nez v0, :cond_5

    .line 425
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->bU:I

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 426
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 441
    :goto_3
    iget-object v0, p0, Lcen;->a:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a(Lcom/google/android/apps/hangouts/views/TransportSpinner;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 417
    :cond_3
    sget v6, Lcom/google/android/apps/hangouts/R$drawable;->i:I

    invoke-virtual {v0, v6}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    goto :goto_1

    :cond_4
    move v0, v5

    .line 420
    goto :goto_2

    .line 427
    :cond_5
    invoke-static {v0}, Lf;->c(I)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 428
    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 429
    invoke-static {v0}, Lcen;->a(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 430
    iget-object v0, p0, Lcen;->a:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->f(Lcom/google/android/apps/hangouts/views/TransportSpinner;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget v0, Lh;->gP:I

    :goto_4
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 431
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 430
    :cond_6
    sget v0, Lh;->lj:I

    goto :goto_4

    .line 432
    :cond_7
    invoke-static {v0}, Lf;->d(I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 433
    iget-object v0, p0, Lcen;->a:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    iget-object v5, v3, Lajk;->b:Lbdh;

    if-eqz v5, :cond_8

    iget-object v3, v3, Lajk;->b:Lbdh;

    invoke-virtual {v3}, Lbdh;->a()Ljava/lang/String;

    move-result-object v4

    :cond_8
    invoke-static {v0, v4, v1}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a(Lcom/google/android/apps/hangouts/views/TransportSpinner;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 435
    const-string v0, ""

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 436
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 438
    :cond_9
    const-string v0, "Babel"

    const-string v1, "Unexpected variant"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_a
    move-object v3, v4

    goto/16 :goto_0
.end method
