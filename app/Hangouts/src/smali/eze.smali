.class public final Leze;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leze;


# instance fields
.field public b:[Lepe;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:[Leyu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x0

    new-array v0, v0, [Leze;

    sput-object v0, Leze;->a:[Leze;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 267
    invoke-direct {p0}, Lepn;-><init>()V

    .line 270
    sget-object v0, Lepe;->a:[Lepe;

    iput-object v0, p0, Leze;->b:[Lepe;

    .line 277
    sget-object v0, Leyu;->a:[Leyu;

    iput-object v0, p0, Leze;->e:[Leyu;

    .line 267
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 309
    iget-object v0, p0, Leze;->b:[Lepe;

    if-eqz v0, :cond_1

    .line 310
    iget-object v3, p0, Leze;->b:[Lepe;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 311
    if-eqz v5, :cond_0

    .line 312
    const/4 v6, 0x1

    .line 313
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 310
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 317
    :cond_2
    iget-object v2, p0, Leze;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 318
    const/4 v2, 0x2

    iget-object v3, p0, Leze;->c:Ljava/lang/Integer;

    .line 319
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 321
    :cond_3
    iget-object v2, p0, Leze;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    .line 322
    const/4 v2, 0x3

    iget-object v3, p0, Leze;->d:Ljava/lang/Integer;

    .line 323
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 325
    :cond_4
    iget-object v2, p0, Leze;->e:[Leyu;

    if-eqz v2, :cond_6

    .line 326
    iget-object v2, p0, Leze;->e:[Leyu;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 327
    if-eqz v4, :cond_5

    .line 328
    const/4 v5, 0x4

    .line 329
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 326
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 333
    :cond_6
    iget-object v1, p0, Leze;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 334
    iput v0, p0, Leze;->cachedSize:I

    .line 335
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 263
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Leze;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Leze;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Leze;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leze;->b:[Lepe;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lepe;

    iget-object v3, p0, Leze;->b:[Lepe;

    if-eqz v3, :cond_2

    iget-object v3, p0, Leze;->b:[Lepe;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Leze;->b:[Lepe;

    :goto_2
    iget-object v2, p0, Leze;->b:[Lepe;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Leze;->b:[Lepe;

    new-instance v3, Lepe;

    invoke-direct {v3}, Lepe;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leze;->b:[Lepe;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Leze;->b:[Lepe;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Leze;->b:[Lepe;

    new-instance v3, Lepe;

    invoke-direct {v3}, Lepe;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leze;->b:[Lepe;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leze;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leze;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leze;->e:[Leyu;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Leyu;

    iget-object v3, p0, Leze;->e:[Leyu;

    if-eqz v3, :cond_5

    iget-object v3, p0, Leze;->e:[Leyu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Leze;->e:[Leyu;

    :goto_4
    iget-object v2, p0, Leze;->e:[Leyu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Leze;->e:[Leyu;

    new-instance v3, Leyu;

    invoke-direct {v3}, Leyu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leze;->e:[Leyu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Leze;->e:[Leyu;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Leze;->e:[Leyu;

    new-instance v3, Leyu;

    invoke-direct {v3}, Leyu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leze;->e:[Leyu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 282
    iget-object v1, p0, Leze;->b:[Lepe;

    if-eqz v1, :cond_1

    .line 283
    iget-object v2, p0, Leze;->b:[Lepe;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 284
    if-eqz v4, :cond_0

    .line 285
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 283
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 289
    :cond_1
    iget-object v1, p0, Leze;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 290
    const/4 v1, 0x2

    iget-object v2, p0, Leze;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 292
    :cond_2
    iget-object v1, p0, Leze;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 293
    const/4 v1, 0x3

    iget-object v2, p0, Leze;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 295
    :cond_3
    iget-object v1, p0, Leze;->e:[Leyu;

    if-eqz v1, :cond_5

    .line 296
    iget-object v1, p0, Leze;->e:[Leyu;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 297
    if-eqz v3, :cond_4

    .line 298
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 296
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 302
    :cond_5
    iget-object v0, p0, Leze;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 304
    return-void
.end method
