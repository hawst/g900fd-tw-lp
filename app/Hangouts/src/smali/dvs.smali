.class public final Ldvs;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldvs;


# instance fields
.field public b:Ldvn;

.field public c:[Ldro;

.field public d:Ljava/lang/Boolean;

.field public e:[Ldqj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19907
    const/4 v0, 0x0

    new-array v0, v0, [Ldvs;

    sput-object v0, Ldvs;->a:[Ldvs;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19908
    invoke-direct {p0}, Lepn;-><init>()V

    .line 19911
    const/4 v0, 0x0

    iput-object v0, p0, Ldvs;->b:Ldvn;

    .line 19914
    sget-object v0, Ldro;->a:[Ldro;

    iput-object v0, p0, Ldvs;->c:[Ldro;

    .line 19919
    sget-object v0, Ldqj;->a:[Ldqj;

    iput-object v0, p0, Ldvs;->e:[Ldqj;

    .line 19908
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 19951
    iget-object v0, p0, Ldvs;->b:Ldvn;

    if-eqz v0, :cond_5

    .line 19952
    const/4 v0, 0x1

    iget-object v2, p0, Ldvs;->b:Ldvn;

    .line 19953
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 19955
    :goto_0
    iget-object v2, p0, Ldvs;->c:[Ldro;

    if-eqz v2, :cond_1

    .line 19956
    iget-object v3, p0, Ldvs;->c:[Ldro;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 19957
    if-eqz v5, :cond_0

    .line 19958
    const/4 v6, 0x2

    .line 19959
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 19956
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 19963
    :cond_1
    iget-object v2, p0, Ldvs;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 19964
    const/4 v2, 0x3

    iget-object v3, p0, Ldvs;->d:Ljava/lang/Boolean;

    .line 19965
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 19967
    :cond_2
    iget-object v2, p0, Ldvs;->e:[Ldqj;

    if-eqz v2, :cond_4

    .line 19968
    iget-object v2, p0, Ldvs;->e:[Ldqj;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 19969
    if-eqz v4, :cond_3

    .line 19970
    const/4 v5, 0x4

    .line 19971
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 19968
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 19975
    :cond_4
    iget-object v1, p0, Ldvs;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19976
    iput v0, p0, Ldvs;->cachedSize:I

    .line 19977
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 19904
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldvs;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldvs;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldvs;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldvs;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldvs;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldvs;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldvs;->c:[Ldro;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldro;

    iget-object v3, p0, Ldvs;->c:[Ldro;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldvs;->c:[Ldro;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldvs;->c:[Ldro;

    :goto_2
    iget-object v2, p0, Ldvs;->c:[Ldro;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldvs;->c:[Ldro;

    new-instance v3, Ldro;

    invoke-direct {v3}, Ldro;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldvs;->c:[Ldro;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldvs;->c:[Ldro;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldvs;->c:[Ldro;

    new-instance v3, Ldro;

    invoke-direct {v3}, Ldro;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldvs;->c:[Ldro;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldvs;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldvs;->e:[Ldqj;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldqj;

    iget-object v3, p0, Ldvs;->e:[Ldqj;

    if-eqz v3, :cond_6

    iget-object v3, p0, Ldvs;->e:[Ldqj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Ldvs;->e:[Ldqj;

    :goto_4
    iget-object v2, p0, Ldvs;->e:[Ldqj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Ldvs;->e:[Ldqj;

    new-instance v3, Ldqj;

    invoke-direct {v3}, Ldqj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldvs;->e:[Ldqj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Ldvs;->e:[Ldqj;

    array-length v0, v0

    goto :goto_3

    :cond_8
    iget-object v2, p0, Ldvs;->e:[Ldqj;

    new-instance v3, Ldqj;

    invoke-direct {v3}, Ldqj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldvs;->e:[Ldqj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 19924
    iget-object v1, p0, Ldvs;->b:Ldvn;

    if-eqz v1, :cond_0

    .line 19925
    const/4 v1, 0x1

    iget-object v2, p0, Ldvs;->b:Ldvn;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 19927
    :cond_0
    iget-object v1, p0, Ldvs;->c:[Ldro;

    if-eqz v1, :cond_2

    .line 19928
    iget-object v2, p0, Ldvs;->c:[Ldro;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 19929
    if-eqz v4, :cond_1

    .line 19930
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 19928
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 19934
    :cond_2
    iget-object v1, p0, Ldvs;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 19935
    const/4 v1, 0x3

    iget-object v2, p0, Ldvs;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 19937
    :cond_3
    iget-object v1, p0, Ldvs;->e:[Ldqj;

    if-eqz v1, :cond_5

    .line 19938
    iget-object v1, p0, Ldvs;->e:[Ldqj;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 19939
    if-eqz v3, :cond_4

    .line 19940
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 19938
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 19944
    :cond_5
    iget-object v0, p0, Ldvs;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 19946
    return-void
.end method
