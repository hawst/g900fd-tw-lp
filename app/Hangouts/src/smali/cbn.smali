.class public final Lcbn;
.super Lcax;
.source "PG"

# interfaces
.implements Laep;
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final b:Z


# instance fields
.field protected final a:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Lcom/google/android/apps/hangouts/views/AvatarView;

.field private final e:Landroid/widget/ImageView;

.field private final f:Lcom/google/android/apps/hangouts/views/ContactDetailItemView;

.field private final g:Landroid/widget/LinearLayout;

.field private final h:Landroid/content/Context;

.field private final i:Lt;

.field private final j:Lyj;

.field private final k:Z

.field private final l:I

.field private m:Lcbo;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Laea;

.field private r:I

.field private s:Z

.field private t:Z

.field private u:Ljava/lang/String;

.field private final v:Landroid/text/SpannableStringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lbys;->s:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcbn;->b:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lt;Lyj;ZI)V
    .locals 7

    .prologue
    .line 100
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcbn;-><init>(Landroid/content/Context;Lt;Lyj;ZIB)V

    .line 101
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lt;Lyj;ZIB)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 105
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcax;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 91
    iput v1, p0, Lcbn;->r:I

    .line 92
    iput-boolean v1, p0, Lcbn;->s:Z

    .line 93
    iput-boolean v1, p0, Lcbn;->t:Z

    .line 96
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcbn;->v:Landroid/text/SpannableStringBuilder;

    .line 106
    iput-object p1, p0, Lcbn;->h:Landroid/content/Context;

    .line 107
    iput-object p2, p0, Lcbn;->i:Lt;

    .line 108
    iput-object p3, p0, Lcbn;->j:Lyj;

    .line 110
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lf;->eB:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcbn;->a:Landroid/view/View;

    .line 111
    iget-object v0, p0, Lcbn;->a:Landroid/view/View;

    sget v1, Lg;->eH:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcbn;->c:Landroid/widget/TextView;

    .line 112
    iget-object v0, p0, Lcbn;->a:Landroid/view/View;

    sget v1, Lg;->E:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/AvatarView;

    iput-object v0, p0, Lcbn;->d:Lcom/google/android/apps/hangouts/views/AvatarView;

    .line 113
    iget-object v0, p0, Lcbn;->a:Landroid/view/View;

    sget v1, Lg;->gT:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcbn;->e:Landroid/widget/ImageView;

    .line 115
    iget-object v0, p0, Lcbn;->a:Landroid/view/View;

    sget v1, Lg;->aB:I

    .line 116
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/ContactDetailItemView;

    iput-object v0, p0, Lcbn;->f:Lcom/google/android/apps/hangouts/views/ContactDetailItemView;

    .line 117
    iget-object v0, p0, Lcbn;->a:Landroid/view/View;

    sget v1, Lg;->fl:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcbn;->g:Landroid/widget/LinearLayout;

    .line 118
    iput-boolean p4, p0, Lcbn;->k:Z

    .line 119
    iput p5, p0, Lcbn;->l:I

    .line 121
    iget-boolean v0, p0, Lcbn;->k:Z

    if-eqz v0, :cond_0

    .line 124
    invoke-virtual {p0, p0}, Lcbn;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    :cond_0
    return-void
.end method

.method private a(Laea;)V
    .locals 1

    .prologue
    .line 393
    iget-boolean v0, p0, Lcbn;->k:Z

    if-nez v0, :cond_1

    .line 395
    invoke-super {p0, p0}, Lcax;->onClick(Landroid/view/View;)V

    .line 401
    :cond_0
    :goto_0
    return-void

    .line 397
    :cond_1
    iget-object v0, p0, Lcbn;->m:Lcbo;

    if-eqz v0, :cond_0

    .line 398
    iget-object v0, p0, Lcbn;->m:Lcbo;

    invoke-interface {v0, p1}, Lcbo;->a(Laea;)V

    goto :goto_0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 404
    iget-boolean v0, p0, Lcbn;->k:Z

    if-nez v0, :cond_1

    .line 406
    invoke-super {p0}, Lcax;->onCancel()V

    .line 412
    :cond_0
    :goto_0
    return-void

    .line 408
    :cond_1
    iget-object v0, p0, Lcbn;->m:Lcbo;

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Lcbn;->m:Lcbo;

    invoke-interface {v0}, Lcbo;->k()V

    goto :goto_0
.end method


# virtual methods
.method public a()Laea;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcbn;->q:Laea;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 525
    iput p1, p0, Lcbn;->r:I

    .line 526
    return-void
.end method

.method protected a(Laeh;)V
    .locals 2

    .prologue
    .line 417
    iget-object v0, p0, Lcbn;->m:Lcbo;

    if-eqz v0, :cond_0

    .line 418
    iget-object v0, p0, Lcbn;->q:Laea;

    invoke-virtual {v0, p1}, Laea;->a(Laee;)V

    .line 419
    iget-object v0, p0, Lcbn;->m:Lcbo;

    iget-object v1, p0, Lcbn;->q:Laea;

    invoke-interface {v0, v1}, Lcbo;->a(Laea;)V

    .line 421
    :cond_0
    return-void
.end method

.method public a(Lcbo;)V
    .locals 0

    .prologue
    .line 389
    iput-object p1, p0, Lcbn;->m:Lcbo;

    .line 390
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 517
    if-nez p1, :cond_0

    .line 518
    const/4 v0, 0x0

    iput-object v0, p0, Lcbn;->u:Ljava/lang/String;

    .line 522
    :goto_0
    return-void

    .line 520
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcbn;->u:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Laea;ZZ)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 160
    iput-object p1, p0, Lcbn;->n:Ljava/lang/String;

    .line 161
    invoke-static {p2}, Lsi;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 162
    invoke-static {}, Lec;->a()Lec;

    move-result-object v0

    .line 163
    sget-object v1, Lel;->a:Lek;

    invoke-virtual {v0, p2, v1}, Lec;->a(Ljava/lang/String;Lek;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcbn;->o:Ljava/lang/String;

    .line 167
    :goto_0
    iput-object p3, p0, Lcbn;->p:Ljava/lang/String;

    .line 168
    if-eqz p4, :cond_1

    .line 169
    invoke-virtual {p4}, Laea;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    const-string v0, "Babel"

    const-string v1, "ContactDetails name (%s) does not equal name parameter (%s)"

    new-array v2, v6, [Ljava/lang/Object;

    .line 172
    invoke-virtual {p4}, Laea;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    aput-object p2, v2, v5

    .line 170
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :cond_0
    invoke-virtual {p4}, Laea;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 175
    const-string v0, "Babel"

    const-string v1, "ContactDetails avatarUrl (%s) does not equal avatarUrl parameter (%s)"

    new-array v2, v6, [Ljava/lang/Object;

    .line 177
    invoke-virtual {p4}, Laea;->f()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    aput-object p3, v2, v5

    .line 175
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    :cond_1
    iput-object p4, p0, Lcbn;->q:Laea;

    .line 181
    iput-boolean p6, p0, Lcbn;->s:Z

    .line 182
    invoke-virtual {p0, p5, v4}, Lcbn;->setChecked(ZZ)V

    .line 184
    invoke-virtual {p0}, Lcbn;->e()V

    .line 185
    return-void

    .line 165
    :cond_2
    iput-object p2, p0, Lcbn;->o:Ljava/lang/String;

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcbn;->n:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 191
    iget v0, p0, Lcbn;->r:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcbn;->r:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 193
    :cond_0
    iget-object v0, p0, Lcbn;->q:Laea;

    invoke-virtual {v0}, Laea;->j()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeh;

    .line 194
    iget-object v0, v0, Laeh;->a:Ljava/lang/String;

    invoke-static {v0}, Lbzd;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 196
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcbn;->o:Ljava/lang/String;

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcbn;->p:Ljava/lang/String;

    return-object v0
.end method

.method public e()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 222
    sget-boolean v0, Lcbn;->b:Z

    if-eqz v0, :cond_0

    .line 223
    const-string v0, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Redrawing contact item: mName="

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcbn;->o:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", mChecked="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcbn;->isChecked()Z

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :cond_0
    iget-object v0, p0, Lcbn;->c:Landroid/widget/TextView;

    iget-object v3, p0, Lcbn;->o:Ljava/lang/String;

    iget-object v5, p0, Lcbn;->v:Landroid/text/SpannableStringBuilder;

    iget-object v6, p0, Lcbn;->u:Ljava/lang/String;

    invoke-virtual {p0, v0, v3, v5, v6}, Lcbn;->a(Landroid/widget/TextView;Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V

    .line 227
    iget-object v3, p0, Lcbn;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcbn;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v4, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 229
    iget-object v0, p0, Lcbn;->d:Lcom/google/android/apps/hangouts/views/AvatarView;

    iget-object v3, p0, Lcbn;->p:Ljava/lang/String;

    iget-object v5, p0, Lcbn;->j:Lyj;

    iget-boolean v6, p0, Lcbn;->s:Z

    invoke-virtual {v0, v3, v5, v6}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Ljava/lang/String;Lyj;Z)V

    .line 236
    iget-boolean v0, p0, Lcbn;->k:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcbn;->l:I

    .line 237
    invoke-static {v0}, Lf;->d(I)Z

    move-result v0

    if-nez v0, :cond_2

    move v3, v1

    .line 238
    :goto_1
    iput-boolean v2, p0, Lcbn;->t:Z

    .line 239
    iget-object v0, p0, Lcbn;->q:Laea;

    if-eqz v0, :cond_f

    .line 240
    iget-object v0, p0, Lcbn;->q:Laea;

    invoke-virtual {v0}, Laea;->j()Ljava/util/ArrayList;

    move-result-object v6

    .line 241
    iget-object v0, p0, Lcbn;->q:Laea;

    .line 242
    invoke-virtual {v0}, Laea;->n()Laee;

    move-result-object v0

    .line 243
    if-eqz v0, :cond_3

    move-object v4, v6

    .line 285
    :goto_2
    instance-of v1, v0, Laeg;

    .line 286
    if-eqz v0, :cond_c

    if-nez v1, :cond_c

    .line 287
    iget-object v1, p0, Lcbn;->f:Lcom/google/android/apps/hangouts/views/ContactDetailItemView;

    iget-object v5, p0, Lcbn;->u:Ljava/lang/String;

    invoke-virtual {v1, v0, v5}, Lcom/google/android/apps/hangouts/views/ContactDetailItemView;->a(Laee;Ljava/lang/String;)V

    .line 289
    iget-object v0, p0, Lcbn;->f:Lcom/google/android/apps/hangouts/views/ContactDetailItemView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/ContactDetailItemView;->setVisibility(I)V

    .line 294
    :goto_3
    invoke-virtual {p0, v2}, Lcbn;->a(Z)V

    .line 296
    if-eqz v3, :cond_d

    if-eqz v4, :cond_d

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_d

    .line 299
    iget-object v0, p0, Lcbn;->g:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcbn;->u:Ljava/lang/String;

    invoke-virtual {p0, v0, v4, v1}, Lcbn;->a(Landroid/view/ViewGroup;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 307
    :goto_4
    iget-boolean v0, p0, Lcbn;->t:Z

    if-eqz v0, :cond_e

    .line 308
    iget-object v0, p0, Lcbn;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 312
    :goto_5
    return-void

    :cond_1
    move v0, v2

    .line 227
    goto :goto_0

    :cond_2
    move v3, v2

    .line 237
    goto :goto_1

    .line 246
    :cond_3
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeh;

    .line 247
    :goto_6
    iget-object v5, p0, Lcbn;->q:Laea;

    invoke-virtual {v5}, Laea;->i()Laef;

    move-result-object v5

    .line 248
    if-nez v3, :cond_6

    .line 250
    if-eqz v0, :cond_5

    :goto_7
    move-object v4, v6

    goto :goto_2

    :cond_4
    move-object v0, v4

    .line 246
    goto :goto_6

    :cond_5
    move-object v0, v5

    .line 250
    goto :goto_7

    .line 257
    :cond_6
    iget v7, p0, Lcbn;->l:I

    if-nez v7, :cond_7

    move v7, v1

    :goto_8
    if-eqz v7, :cond_9

    .line 258
    if-eqz v5, :cond_8

    move-object v0, v5

    move-object v4, v6

    .line 261
    goto :goto_2

    :cond_7
    move v7, v2

    .line 257
    goto :goto_8

    .line 262
    :cond_8
    if-eqz v0, :cond_b

    .line 266
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 267
    iput-boolean v1, p0, Lcbn;->t:Z

    move-object v4, v6

    goto :goto_2

    .line 270
    :cond_9
    if-eqz v0, :cond_a

    .line 274
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 275
    iput-boolean v1, p0, Lcbn;->t:Z

    move-object v4, v6

    goto :goto_2

    .line 277
    :cond_a
    const-string v0, "Babel"

    const-string v1, "In SMS mode, there should be at least one phonefor contact in SMS mode"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    move-object v0, v4

    move-object v4, v6

    goto :goto_2

    .line 291
    :cond_c
    iget-object v0, p0, Lcbn;->f:Lcom/google/android/apps/hangouts/views/ContactDetailItemView;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/hangouts/views/ContactDetailItemView;->setVisibility(I)V

    goto :goto_3

    .line 303
    :cond_d
    iget-object v0, p0, Lcbn;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 304
    iget-object v0, p0, Lcbn;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_4

    .line 310
    :cond_e
    iget-object v0, p0, Lcbn;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_5

    :cond_f
    move-object v0, v4

    goto/16 :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 320
    invoke-virtual {p0}, Lcbn;->isChecked()Z

    move-result v0

    if-nez v0, :cond_2

    .line 321
    iget-object v0, p0, Lcbn;->q:Laea;

    if-nez v0, :cond_1

    .line 324
    iget-object v0, p0, Lcbn;->o:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcbn;->o:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget v2, p0, Lcbn;->r:I

    packed-switch v2, :pswitch_data_0

    :cond_0
    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcbn;->q:Laea;

    .line 326
    :cond_1
    iget-object v0, p0, Lcbn;->q:Laea;

    if-eqz v0, :cond_9

    .line 327
    iget v0, p0, Lcbn;->r:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 329
    iget-object v0, p0, Lcbn;->q:Laea;

    invoke-virtual {v0}, Laea;->j()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeh;

    .line 330
    iget-object v1, p0, Lcbn;->q:Laea;

    invoke-virtual {v1, v0}, Laea;->a(Laee;)V

    .line 385
    :cond_2
    :goto_2
    iget-object v0, p0, Lcbn;->q:Laea;

    invoke-direct {p0, v0}, Lcbn;->a(Laea;)V

    .line 386
    :goto_3
    return-void

    :cond_3
    move-object v0, v1

    .line 324
    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Laea;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Laea;

    new-instance v4, Laef;

    const-string v6, ""

    invoke-direct {v4, v0, v6}, Laef;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v2, v4}, Laea;-><init>(Laef;)V

    move-object v0, v2

    goto :goto_1

    :pswitch_1
    invoke-static {v0}, Laea;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Laea;

    new-instance v4, Laeh;

    const-string v6, ""

    invoke-direct {v4, v2, v6}, Laeh;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v4}, Laea;-><init>(Laeh;)V

    goto :goto_1

    :pswitch_2
    new-instance v2, Laea;

    new-instance v4, Laeh;

    const-string v6, ""

    invoke-direct {v4, v0, v6}, Laeh;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v2, v4}, Laea;-><init>(Laeh;)V

    move-object v0, v2

    goto :goto_1

    .line 332
    :cond_4
    iget v0, p0, Lcbn;->l:I

    invoke-static {v0}, Lf;->c(I)Z

    move-result v0

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcbn;->t:Z

    if-eqz v0, :cond_7

    .line 334
    :cond_5
    iget-object v0, p0, Lcbn;->q:Laea;

    invoke-virtual {v0}, Laea;->j()Ljava/util/ArrayList;

    move-result-object v0

    .line 335
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 350
    :cond_6
    new-instance v0, Laen;

    iget-object v1, p0, Lcbn;->i:Lt;

    iget-object v2, p0, Lcbn;->j:Lyj;

    iget-object v3, p0, Lcbn;->q:Laea;

    iget v5, p0, Lcbn;->l:I

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Laen;-><init>(Lt;Lyj;Laea;Laep;I)V

    .line 352
    invoke-virtual {v0}, Laen;->b()V

    goto :goto_3

    .line 337
    :pswitch_3
    invoke-virtual {p0}, Lcbn;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lh;->it:I

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 338
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_3

    .line 341
    :pswitch_4
    iget-boolean v1, p0, Lcbn;->k:Z

    if-eqz v1, :cond_6

    .line 344
    iget-object v1, p0, Lcbn;->q:Laea;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laee;

    invoke-virtual {v1, v0}, Laea;->a(Laee;)V

    goto/16 :goto_2

    .line 355
    :cond_7
    iget v0, p0, Lcbn;->l:I

    .line 358
    iget-boolean v1, p0, Lcbn;->k:Z

    if-eqz v1, :cond_a

    .line 359
    if-nez v0, :cond_8

    move v1, v5

    :goto_4
    if-eqz v1, :cond_a

    .line 364
    :goto_5
    new-instance v0, Laen;

    iget-object v1, p0, Lcbn;->i:Lt;

    iget-object v2, p0, Lcbn;->j:Lyj;

    iget-object v3, p0, Lcbn;->q:Laea;

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Laen;-><init>(Lt;Lyj;Laea;Laep;I)V

    .line 366
    invoke-virtual {v0}, Laen;->b()V

    goto/16 :goto_3

    :cond_8
    move v1, v3

    .line 359
    goto :goto_4

    .line 372
    :cond_9
    iget-object v0, p0, Lcbn;->h:Landroid/content/Context;

    .line 373
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lh;->aH:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 375
    iget v0, p0, Lcbn;->r:I

    packed-switch v0, :pswitch_data_2

    iget-object v0, p0, Lcbn;->h:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lh;->aG:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_6
    iget-object v3, p0, Lcbn;->h:Landroid/content/Context;

    .line 376
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lh;->aC:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 372
    invoke-static {v2, v0, v3, v1}, Labe;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Labe;

    move-result-object v0

    .line 380
    iget-object v1, p0, Lcbn;->i:Lt;

    invoke-virtual {v1}, Lt;->getFragmentManager()Lae;

    move-result-object v1

    const-string v2, "contact_error"

    invoke-virtual {v0, v1, v2}, Labe;->a(Lae;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 375
    :pswitch_5
    iget-object v0, p0, Lcbn;->h:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v4, Lh;->aD:I

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcbn;->h:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lh;->aE:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    :pswitch_6
    iget-object v0, p0, Lcbn;->h:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v4, Lh;->aD:I

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcbn;->h:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lh;->aF:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    :cond_a
    move v5, v0

    goto/16 :goto_5

    .line 324
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 335
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 375
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public onContactLookupComplete(Laea;Laee;)V
    .locals 3

    .prologue
    .line 479
    if-nez p2, :cond_0

    .line 480
    invoke-direct {p0}, Lcbn;->f()V

    .line 514
    :goto_0
    return-void

    .line 484
    :cond_0
    invoke-virtual {p2}, Laee;->c()Lbdh;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 485
    invoke-virtual {p2}, Laee;->c()Lbdh;

    move-result-object v0

    iget-object v0, v0, Lbdh;->b:Lbdk;

    iget-object v1, p0, Lcbn;->j:Lyj;

    .line 486
    invoke-virtual {v1}, Lyj;->c()Lbdk;

    move-result-object v1

    .line 485
    invoke-virtual {v0, v1}, Lbdk;->a(Lbdk;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 487
    const-string v0, "Babel"

    const-string v1, "Selected yourself. Ignore."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    iget-object v0, p0, Lcbn;->h:Landroid/content/Context;

    sget v1, Lh;->aY:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 489
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 490
    invoke-direct {p0}, Lcbn;->f()V

    goto :goto_0

    .line 494
    :cond_1
    invoke-virtual {p1, p2}, Laea;->a(Laee;)V

    .line 495
    iget-object v0, p0, Lcbn;->q:Laea;

    if-eq p1, v0, :cond_4

    .line 502
    iget-object v0, p0, Lcbn;->q:Laea;

    if-eqz v0, :cond_2

    .line 503
    invoke-virtual {p1}, Laea;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcbn;->q:Laea;

    invoke-virtual {v1}, Laea;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 504
    :cond_2
    const-string v0, "Babel"

    const-string v1, "Contact lookup was for a different contact, skip selecting"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 510
    :cond_3
    iget-object v0, p0, Lcbn;->q:Laea;

    invoke-virtual {v0, p2}, Laea;->a(Laee;)V

    .line 513
    :cond_4
    invoke-direct {p0, p1}, Lcbn;->a(Laea;)V

    goto :goto_0
.end method

.method public resumeLoading()V
    .locals 1

    .prologue
    .line 208
    iget-boolean v0, p0, Lcbn;->s:Z

    if-eqz v0, :cond_0

    .line 209
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcbn;->s:Z

    .line 210
    iget-object v0, p0, Lcbn;->d:Lcom/google/android/apps/hangouts/views/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/AvatarView;->a()V

    .line 212
    :cond_0
    return-void
.end method
