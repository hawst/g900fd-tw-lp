.class public final Ldpi;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldpi;


# instance fields
.field public b:Ldvn;

.field public c:[Ldtg;

.field public d:Ldrr;

.field public e:Ldqa;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Long;

.field public h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5847
    const/4 v0, 0x0

    new-array v0, v0, [Ldpi;

    sput-object v0, Ldpi;->a:[Ldpi;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5848
    invoke-direct {p0}, Lepn;-><init>()V

    .line 5857
    iput-object v1, p0, Ldpi;->b:Ldvn;

    .line 5860
    sget-object v0, Ldtg;->a:[Ldtg;

    iput-object v0, p0, Ldpi;->c:[Ldtg;

    .line 5863
    iput-object v1, p0, Ldpi;->d:Ldrr;

    .line 5866
    iput-object v1, p0, Ldpi;->e:Ldqa;

    .line 5869
    iput-object v1, p0, Ldpi;->f:Ljava/lang/Integer;

    .line 5848
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 5910
    iget-object v0, p0, Ldpi;->b:Ldvn;

    if-eqz v0, :cond_7

    .line 5911
    const/4 v0, 0x1

    iget-object v2, p0, Ldpi;->b:Ldvn;

    .line 5912
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5914
    :goto_0
    iget-object v2, p0, Ldpi;->c:[Ldtg;

    if-eqz v2, :cond_1

    .line 5915
    iget-object v2, p0, Ldpi;->c:[Ldtg;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 5916
    if-eqz v4, :cond_0

    .line 5917
    const/4 v5, 0x2

    .line 5918
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 5915
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 5922
    :cond_1
    iget-object v1, p0, Ldpi;->g:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 5923
    const/4 v1, 0x3

    iget-object v2, p0, Ldpi;->g:Ljava/lang/Long;

    .line 5924
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 5926
    :cond_2
    iget-object v1, p0, Ldpi;->h:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 5927
    const/4 v1, 0x4

    iget-object v2, p0, Ldpi;->h:Ljava/lang/String;

    .line 5928
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5930
    :cond_3
    iget-object v1, p0, Ldpi;->d:Ldrr;

    if-eqz v1, :cond_4

    .line 5931
    const/4 v1, 0x5

    iget-object v2, p0, Ldpi;->d:Ldrr;

    .line 5932
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5934
    :cond_4
    iget-object v1, p0, Ldpi;->e:Ldqa;

    if-eqz v1, :cond_5

    .line 5935
    const/4 v1, 0x6

    iget-object v2, p0, Ldpi;->e:Ldqa;

    .line 5936
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5938
    :cond_5
    iget-object v1, p0, Ldpi;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 5939
    const/16 v1, 0x8

    iget-object v2, p0, Ldpi;->f:Ljava/lang/Integer;

    .line 5940
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5942
    :cond_6
    iget-object v1, p0, Ldpi;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5943
    iput v0, p0, Ldpi;->cachedSize:I

    .line 5944
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5844
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldpi;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldpi;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldpi;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldpi;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldpi;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldpi;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldpi;->c:[Ldtg;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldtg;

    iget-object v3, p0, Ldpi;->c:[Ldtg;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldpi;->c:[Ldtg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldpi;->c:[Ldtg;

    :goto_2
    iget-object v2, p0, Ldpi;->c:[Ldtg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldpi;->c:[Ldtg;

    new-instance v3, Ldtg;

    invoke-direct {v3}, Ldtg;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldpi;->c:[Ldtg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldpi;->c:[Ldtg;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldpi;->c:[Ldtg;

    new-instance v3, Ldtg;

    invoke-direct {v3}, Ldtg;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldpi;->c:[Ldtg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldpi;->g:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldpi;->h:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Ldpi;->d:Ldrr;

    if-nez v0, :cond_6

    new-instance v0, Ldrr;

    invoke-direct {v0}, Ldrr;-><init>()V

    iput-object v0, p0, Ldpi;->d:Ldrr;

    :cond_6
    iget-object v0, p0, Ldpi;->d:Ldrr;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Ldpi;->e:Ldqa;

    if-nez v0, :cond_7

    new-instance v0, Ldqa;

    invoke-direct {v0}, Ldqa;-><init>()V

    iput-object v0, p0, Ldpi;->e:Ldqa;

    :cond_7
    iget-object v0, p0, Ldpi;->e:Ldqa;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_8

    const/4 v2, 0x1

    if-eq v0, v2, :cond_8

    const/4 v2, 0x2

    if-ne v0, v2, :cond_9

    :cond_8
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpi;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_9
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpi;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x40 -> :sswitch_7
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 5878
    iget-object v0, p0, Ldpi;->b:Ldvn;

    if-eqz v0, :cond_0

    .line 5879
    const/4 v0, 0x1

    iget-object v1, p0, Ldpi;->b:Ldvn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5881
    :cond_0
    iget-object v0, p0, Ldpi;->c:[Ldtg;

    if-eqz v0, :cond_2

    .line 5882
    iget-object v1, p0, Ldpi;->c:[Ldtg;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 5883
    if-eqz v3, :cond_1

    .line 5884
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 5882
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5888
    :cond_2
    iget-object v0, p0, Ldpi;->g:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 5889
    const/4 v0, 0x3

    iget-object v1, p0, Ldpi;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 5891
    :cond_3
    iget-object v0, p0, Ldpi;->h:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 5892
    const/4 v0, 0x4

    iget-object v1, p0, Ldpi;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 5894
    :cond_4
    iget-object v0, p0, Ldpi;->d:Ldrr;

    if-eqz v0, :cond_5

    .line 5895
    const/4 v0, 0x5

    iget-object v1, p0, Ldpi;->d:Ldrr;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5897
    :cond_5
    iget-object v0, p0, Ldpi;->e:Ldqa;

    if-eqz v0, :cond_6

    .line 5898
    const/4 v0, 0x6

    iget-object v1, p0, Ldpi;->e:Ldqa;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5900
    :cond_6
    iget-object v0, p0, Ldpi;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 5901
    const/16 v0, 0x8

    iget-object v1, p0, Ldpi;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 5903
    :cond_7
    iget-object v0, p0, Ldpi;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 5905
    return-void
.end method
