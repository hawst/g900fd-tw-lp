.class public final Ldwv;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldwv;


# instance fields
.field public b:Ldww;

.field public c:Ldqg;

.field public d:Ldrw;

.field public e:Ldwf;

.field public f:Ldwo;

.field public g:Ldwc;

.field public h:Ldvj;

.field public i:Ldxu;

.field public j:Ldqm;

.field public k:Ldrk;

.field public l:Ldqt;

.field public m:Ldqe;

.field public n:Ldqa;

.field public o:Ldvt;

.field public p:Lduv;

.field public q:Ldpn;

.field public r:Ldtd;

.field public s:Ldpy;

.field public t:Ldvp;

.field public u:Ldpz;

.field public v:Ldqq;

.field public w:Ldsj;

.field public x:Ldqd;

.field public y:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26277
    const/4 v0, 0x0

    new-array v0, v0, [Ldwv;

    sput-object v0, Ldwv;->a:[Ldwv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26278
    invoke-direct {p0}, Lepn;-><init>()V

    .line 26281
    iput-object v0, p0, Ldwv;->b:Ldww;

    .line 26284
    iput-object v0, p0, Ldwv;->c:Ldqg;

    .line 26287
    iput-object v0, p0, Ldwv;->d:Ldrw;

    .line 26290
    iput-object v0, p0, Ldwv;->e:Ldwf;

    .line 26293
    iput-object v0, p0, Ldwv;->f:Ldwo;

    .line 26296
    iput-object v0, p0, Ldwv;->g:Ldwc;

    .line 26299
    iput-object v0, p0, Ldwv;->h:Ldvj;

    .line 26302
    iput-object v0, p0, Ldwv;->i:Ldxu;

    .line 26305
    iput-object v0, p0, Ldwv;->j:Ldqm;

    .line 26308
    iput-object v0, p0, Ldwv;->k:Ldrk;

    .line 26311
    iput-object v0, p0, Ldwv;->l:Ldqt;

    .line 26314
    iput-object v0, p0, Ldwv;->m:Ldqe;

    .line 26317
    iput-object v0, p0, Ldwv;->n:Ldqa;

    .line 26320
    iput-object v0, p0, Ldwv;->o:Ldvt;

    .line 26323
    iput-object v0, p0, Ldwv;->p:Lduv;

    .line 26326
    iput-object v0, p0, Ldwv;->q:Ldpn;

    .line 26329
    iput-object v0, p0, Ldwv;->r:Ldtd;

    .line 26332
    iput-object v0, p0, Ldwv;->s:Ldpy;

    .line 26335
    iput-object v0, p0, Ldwv;->t:Ldvp;

    .line 26338
    iput-object v0, p0, Ldwv;->u:Ldpz;

    .line 26341
    iput-object v0, p0, Ldwv;->v:Ldqq;

    .line 26344
    iput-object v0, p0, Ldwv;->w:Ldsj;

    .line 26347
    iput-object v0, p0, Ldwv;->x:Ldqd;

    .line 26278
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 26432
    const/4 v0, 0x0

    .line 26433
    iget-object v1, p0, Ldwv;->b:Ldww;

    if-eqz v1, :cond_0

    .line 26434
    const/4 v0, 0x1

    iget-object v1, p0, Ldwv;->b:Ldww;

    .line 26435
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 26437
    :cond_0
    iget-object v1, p0, Ldwv;->c:Ldqg;

    if-eqz v1, :cond_1

    .line 26438
    const/4 v1, 0x2

    iget-object v2, p0, Ldwv;->c:Ldqg;

    .line 26439
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26441
    :cond_1
    iget-object v1, p0, Ldwv;->d:Ldrw;

    if-eqz v1, :cond_2

    .line 26442
    const/4 v1, 0x3

    iget-object v2, p0, Ldwv;->d:Ldrw;

    .line 26443
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26445
    :cond_2
    iget-object v1, p0, Ldwv;->e:Ldwf;

    if-eqz v1, :cond_3

    .line 26446
    const/4 v1, 0x4

    iget-object v2, p0, Ldwv;->e:Ldwf;

    .line 26447
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26449
    :cond_3
    iget-object v1, p0, Ldwv;->f:Ldwo;

    if-eqz v1, :cond_4

    .line 26450
    const/4 v1, 0x5

    iget-object v2, p0, Ldwv;->f:Ldwo;

    .line 26451
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26453
    :cond_4
    iget-object v1, p0, Ldwv;->g:Ldwc;

    if-eqz v1, :cond_5

    .line 26454
    const/4 v1, 0x6

    iget-object v2, p0, Ldwv;->g:Ldwc;

    .line 26455
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26457
    :cond_5
    iget-object v1, p0, Ldwv;->h:Ldvj;

    if-eqz v1, :cond_6

    .line 26458
    const/4 v1, 0x7

    iget-object v2, p0, Ldwv;->h:Ldvj;

    .line 26459
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26461
    :cond_6
    iget-object v1, p0, Ldwv;->i:Ldxu;

    if-eqz v1, :cond_7

    .line 26462
    const/16 v1, 0x8

    iget-object v2, p0, Ldwv;->i:Ldxu;

    .line 26463
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26465
    :cond_7
    iget-object v1, p0, Ldwv;->y:[B

    if-eqz v1, :cond_8

    .line 26466
    const/16 v1, 0xa

    iget-object v2, p0, Ldwv;->y:[B

    .line 26467
    invoke-static {v1, v2}, Lepl;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 26469
    :cond_8
    iget-object v1, p0, Ldwv;->j:Ldqm;

    if-eqz v1, :cond_9

    .line 26470
    const/16 v1, 0xb

    iget-object v2, p0, Ldwv;->j:Ldqm;

    .line 26471
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26473
    :cond_9
    iget-object v1, p0, Ldwv;->k:Ldrk;

    if-eqz v1, :cond_a

    .line 26474
    const/16 v1, 0xc

    iget-object v2, p0, Ldwv;->k:Ldrk;

    .line 26475
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26477
    :cond_a
    iget-object v1, p0, Ldwv;->n:Ldqa;

    if-eqz v1, :cond_b

    .line 26478
    const/16 v1, 0xd

    iget-object v2, p0, Ldwv;->n:Ldqa;

    .line 26479
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26481
    :cond_b
    iget-object v1, p0, Ldwv;->o:Ldvt;

    if-eqz v1, :cond_c

    .line 26482
    const/16 v1, 0xe

    iget-object v2, p0, Ldwv;->o:Ldvt;

    .line 26483
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26485
    :cond_c
    iget-object v1, p0, Ldwv;->l:Ldqt;

    if-eqz v1, :cond_d

    .line 26486
    const/16 v1, 0xf

    iget-object v2, p0, Ldwv;->l:Ldqt;

    .line 26487
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26489
    :cond_d
    iget-object v1, p0, Ldwv;->p:Lduv;

    if-eqz v1, :cond_e

    .line 26490
    const/16 v1, 0x10

    iget-object v2, p0, Ldwv;->p:Lduv;

    .line 26491
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26493
    :cond_e
    iget-object v1, p0, Ldwv;->q:Ldpn;

    if-eqz v1, :cond_f

    .line 26494
    const/16 v1, 0x11

    iget-object v2, p0, Ldwv;->q:Ldpn;

    .line 26495
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26497
    :cond_f
    iget-object v1, p0, Ldwv;->r:Ldtd;

    if-eqz v1, :cond_10

    .line 26498
    const/16 v1, 0x12

    iget-object v2, p0, Ldwv;->r:Ldtd;

    .line 26499
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26501
    :cond_10
    iget-object v1, p0, Ldwv;->s:Ldpy;

    if-eqz v1, :cond_11

    .line 26502
    const/16 v1, 0x13

    iget-object v2, p0, Ldwv;->s:Ldpy;

    .line 26503
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26505
    :cond_11
    iget-object v1, p0, Ldwv;->t:Ldvp;

    if-eqz v1, :cond_12

    .line 26506
    const/16 v1, 0x14

    iget-object v2, p0, Ldwv;->t:Ldvp;

    .line 26507
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26509
    :cond_12
    iget-object v1, p0, Ldwv;->u:Ldpz;

    if-eqz v1, :cond_13

    .line 26510
    const/16 v1, 0x15

    iget-object v2, p0, Ldwv;->u:Ldpz;

    .line 26511
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26513
    :cond_13
    iget-object v1, p0, Ldwv;->v:Ldqq;

    if-eqz v1, :cond_14

    .line 26514
    const/16 v1, 0x16

    iget-object v2, p0, Ldwv;->v:Ldqq;

    .line 26515
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26517
    :cond_14
    iget-object v1, p0, Ldwv;->m:Ldqe;

    if-eqz v1, :cond_15

    .line 26518
    const/16 v1, 0x17

    iget-object v2, p0, Ldwv;->m:Ldqe;

    .line 26519
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26521
    :cond_15
    iget-object v1, p0, Ldwv;->w:Ldsj;

    if-eqz v1, :cond_16

    .line 26522
    const/16 v1, 0x19

    iget-object v2, p0, Ldwv;->w:Ldsj;

    .line 26523
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26525
    :cond_16
    iget-object v1, p0, Ldwv;->x:Ldqd;

    if-eqz v1, :cond_17

    .line 26526
    const/16 v1, 0x1a

    iget-object v2, p0, Ldwv;->x:Ldqd;

    .line 26527
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26529
    :cond_17
    iget-object v1, p0, Ldwv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26530
    iput v0, p0, Ldwv;->cachedSize:I

    .line 26531
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 26274
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldwv;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldwv;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldwv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldwv;->b:Ldww;

    if-nez v0, :cond_2

    new-instance v0, Ldww;

    invoke-direct {v0}, Ldww;-><init>()V

    iput-object v0, p0, Ldwv;->b:Ldww;

    :cond_2
    iget-object v0, p0, Ldwv;->b:Ldww;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldwv;->c:Ldqg;

    if-nez v0, :cond_3

    new-instance v0, Ldqg;

    invoke-direct {v0}, Ldqg;-><init>()V

    iput-object v0, p0, Ldwv;->c:Ldqg;

    :cond_3
    iget-object v0, p0, Ldwv;->c:Ldqg;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldwv;->d:Ldrw;

    if-nez v0, :cond_4

    new-instance v0, Ldrw;

    invoke-direct {v0}, Ldrw;-><init>()V

    iput-object v0, p0, Ldwv;->d:Ldrw;

    :cond_4
    iget-object v0, p0, Ldwv;->d:Ldrw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldwv;->e:Ldwf;

    if-nez v0, :cond_5

    new-instance v0, Ldwf;

    invoke-direct {v0}, Ldwf;-><init>()V

    iput-object v0, p0, Ldwv;->e:Ldwf;

    :cond_5
    iget-object v0, p0, Ldwv;->e:Ldwf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldwv;->f:Ldwo;

    if-nez v0, :cond_6

    new-instance v0, Ldwo;

    invoke-direct {v0}, Ldwo;-><init>()V

    iput-object v0, p0, Ldwv;->f:Ldwo;

    :cond_6
    iget-object v0, p0, Ldwv;->f:Ldwo;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ldwv;->g:Ldwc;

    if-nez v0, :cond_7

    new-instance v0, Ldwc;

    invoke-direct {v0}, Ldwc;-><init>()V

    iput-object v0, p0, Ldwv;->g:Ldwc;

    :cond_7
    iget-object v0, p0, Ldwv;->g:Ldwc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Ldwv;->h:Ldvj;

    if-nez v0, :cond_8

    new-instance v0, Ldvj;

    invoke-direct {v0}, Ldvj;-><init>()V

    iput-object v0, p0, Ldwv;->h:Ldvj;

    :cond_8
    iget-object v0, p0, Ldwv;->h:Ldvj;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Ldwv;->i:Ldxu;

    if-nez v0, :cond_9

    new-instance v0, Ldxu;

    invoke-direct {v0}, Ldxu;-><init>()V

    iput-object v0, p0, Ldwv;->i:Ldxu;

    :cond_9
    iget-object v0, p0, Ldwv;->i:Ldxu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Ldwv;->y:[B

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Ldwv;->j:Ldqm;

    if-nez v0, :cond_a

    new-instance v0, Ldqm;

    invoke-direct {v0}, Ldqm;-><init>()V

    iput-object v0, p0, Ldwv;->j:Ldqm;

    :cond_a
    iget-object v0, p0, Ldwv;->j:Ldqm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Ldwv;->k:Ldrk;

    if-nez v0, :cond_b

    new-instance v0, Ldrk;

    invoke-direct {v0}, Ldrk;-><init>()V

    iput-object v0, p0, Ldwv;->k:Ldrk;

    :cond_b
    iget-object v0, p0, Ldwv;->k:Ldrk;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Ldwv;->n:Ldqa;

    if-nez v0, :cond_c

    new-instance v0, Ldqa;

    invoke-direct {v0}, Ldqa;-><init>()V

    iput-object v0, p0, Ldwv;->n:Ldqa;

    :cond_c
    iget-object v0, p0, Ldwv;->n:Ldqa;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Ldwv;->o:Ldvt;

    if-nez v0, :cond_d

    new-instance v0, Ldvt;

    invoke-direct {v0}, Ldvt;-><init>()V

    iput-object v0, p0, Ldwv;->o:Ldvt;

    :cond_d
    iget-object v0, p0, Ldwv;->o:Ldvt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Ldwv;->l:Ldqt;

    if-nez v0, :cond_e

    new-instance v0, Ldqt;

    invoke-direct {v0}, Ldqt;-><init>()V

    iput-object v0, p0, Ldwv;->l:Ldqt;

    :cond_e
    iget-object v0, p0, Ldwv;->l:Ldqt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Ldwv;->p:Lduv;

    if-nez v0, :cond_f

    new-instance v0, Lduv;

    invoke-direct {v0}, Lduv;-><init>()V

    iput-object v0, p0, Ldwv;->p:Lduv;

    :cond_f
    iget-object v0, p0, Ldwv;->p:Lduv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_10
    iget-object v0, p0, Ldwv;->q:Ldpn;

    if-nez v0, :cond_10

    new-instance v0, Ldpn;

    invoke-direct {v0}, Ldpn;-><init>()V

    iput-object v0, p0, Ldwv;->q:Ldpn;

    :cond_10
    iget-object v0, p0, Ldwv;->q:Ldpn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_11
    iget-object v0, p0, Ldwv;->r:Ldtd;

    if-nez v0, :cond_11

    new-instance v0, Ldtd;

    invoke-direct {v0}, Ldtd;-><init>()V

    iput-object v0, p0, Ldwv;->r:Ldtd;

    :cond_11
    iget-object v0, p0, Ldwv;->r:Ldtd;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_12
    iget-object v0, p0, Ldwv;->s:Ldpy;

    if-nez v0, :cond_12

    new-instance v0, Ldpy;

    invoke-direct {v0}, Ldpy;-><init>()V

    iput-object v0, p0, Ldwv;->s:Ldpy;

    :cond_12
    iget-object v0, p0, Ldwv;->s:Ldpy;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_13
    iget-object v0, p0, Ldwv;->t:Ldvp;

    if-nez v0, :cond_13

    new-instance v0, Ldvp;

    invoke-direct {v0}, Ldvp;-><init>()V

    iput-object v0, p0, Ldwv;->t:Ldvp;

    :cond_13
    iget-object v0, p0, Ldwv;->t:Ldvp;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_14
    iget-object v0, p0, Ldwv;->u:Ldpz;

    if-nez v0, :cond_14

    new-instance v0, Ldpz;

    invoke-direct {v0}, Ldpz;-><init>()V

    iput-object v0, p0, Ldwv;->u:Ldpz;

    :cond_14
    iget-object v0, p0, Ldwv;->u:Ldpz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_15
    iget-object v0, p0, Ldwv;->v:Ldqq;

    if-nez v0, :cond_15

    new-instance v0, Ldqq;

    invoke-direct {v0}, Ldqq;-><init>()V

    iput-object v0, p0, Ldwv;->v:Ldqq;

    :cond_15
    iget-object v0, p0, Ldwv;->v:Ldqq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_16
    iget-object v0, p0, Ldwv;->m:Ldqe;

    if-nez v0, :cond_16

    new-instance v0, Ldqe;

    invoke-direct {v0}, Ldqe;-><init>()V

    iput-object v0, p0, Ldwv;->m:Ldqe;

    :cond_16
    iget-object v0, p0, Ldwv;->m:Ldqe;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_17
    iget-object v0, p0, Ldwv;->w:Ldsj;

    if-nez v0, :cond_17

    new-instance v0, Ldsj;

    invoke-direct {v0}, Ldsj;-><init>()V

    iput-object v0, p0, Ldwv;->w:Ldsj;

    :cond_17
    iget-object v0, p0, Ldwv;->w:Ldsj;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_18
    iget-object v0, p0, Ldwv;->x:Ldqd;

    if-nez v0, :cond_18

    new-instance v0, Ldqd;

    invoke-direct {v0}, Ldqd;-><init>()V

    iput-object v0, p0, Ldwv;->x:Ldqd;

    :cond_18
    iget-object v0, p0, Ldwv;->x:Ldqd;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
        0x82 -> :sswitch_f
        0x8a -> :sswitch_10
        0x92 -> :sswitch_11
        0x9a -> :sswitch_12
        0xa2 -> :sswitch_13
        0xaa -> :sswitch_14
        0xb2 -> :sswitch_15
        0xba -> :sswitch_16
        0xca -> :sswitch_17
        0xd2 -> :sswitch_18
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 26354
    iget-object v0, p0, Ldwv;->b:Ldww;

    if-eqz v0, :cond_0

    .line 26355
    const/4 v0, 0x1

    iget-object v1, p0, Ldwv;->b:Ldww;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26357
    :cond_0
    iget-object v0, p0, Ldwv;->c:Ldqg;

    if-eqz v0, :cond_1

    .line 26358
    const/4 v0, 0x2

    iget-object v1, p0, Ldwv;->c:Ldqg;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26360
    :cond_1
    iget-object v0, p0, Ldwv;->d:Ldrw;

    if-eqz v0, :cond_2

    .line 26361
    const/4 v0, 0x3

    iget-object v1, p0, Ldwv;->d:Ldrw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26363
    :cond_2
    iget-object v0, p0, Ldwv;->e:Ldwf;

    if-eqz v0, :cond_3

    .line 26364
    const/4 v0, 0x4

    iget-object v1, p0, Ldwv;->e:Ldwf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26366
    :cond_3
    iget-object v0, p0, Ldwv;->f:Ldwo;

    if-eqz v0, :cond_4

    .line 26367
    const/4 v0, 0x5

    iget-object v1, p0, Ldwv;->f:Ldwo;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26369
    :cond_4
    iget-object v0, p0, Ldwv;->g:Ldwc;

    if-eqz v0, :cond_5

    .line 26370
    const/4 v0, 0x6

    iget-object v1, p0, Ldwv;->g:Ldwc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26372
    :cond_5
    iget-object v0, p0, Ldwv;->h:Ldvj;

    if-eqz v0, :cond_6

    .line 26373
    const/4 v0, 0x7

    iget-object v1, p0, Ldwv;->h:Ldvj;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26375
    :cond_6
    iget-object v0, p0, Ldwv;->i:Ldxu;

    if-eqz v0, :cond_7

    .line 26376
    const/16 v0, 0x8

    iget-object v1, p0, Ldwv;->i:Ldxu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26378
    :cond_7
    iget-object v0, p0, Ldwv;->y:[B

    if-eqz v0, :cond_8

    .line 26379
    const/16 v0, 0xa

    iget-object v1, p0, Ldwv;->y:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 26381
    :cond_8
    iget-object v0, p0, Ldwv;->j:Ldqm;

    if-eqz v0, :cond_9

    .line 26382
    const/16 v0, 0xb

    iget-object v1, p0, Ldwv;->j:Ldqm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26384
    :cond_9
    iget-object v0, p0, Ldwv;->k:Ldrk;

    if-eqz v0, :cond_a

    .line 26385
    const/16 v0, 0xc

    iget-object v1, p0, Ldwv;->k:Ldrk;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26387
    :cond_a
    iget-object v0, p0, Ldwv;->n:Ldqa;

    if-eqz v0, :cond_b

    .line 26388
    const/16 v0, 0xd

    iget-object v1, p0, Ldwv;->n:Ldqa;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26390
    :cond_b
    iget-object v0, p0, Ldwv;->o:Ldvt;

    if-eqz v0, :cond_c

    .line 26391
    const/16 v0, 0xe

    iget-object v1, p0, Ldwv;->o:Ldvt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26393
    :cond_c
    iget-object v0, p0, Ldwv;->l:Ldqt;

    if-eqz v0, :cond_d

    .line 26394
    const/16 v0, 0xf

    iget-object v1, p0, Ldwv;->l:Ldqt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26396
    :cond_d
    iget-object v0, p0, Ldwv;->p:Lduv;

    if-eqz v0, :cond_e

    .line 26397
    const/16 v0, 0x10

    iget-object v1, p0, Ldwv;->p:Lduv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26399
    :cond_e
    iget-object v0, p0, Ldwv;->q:Ldpn;

    if-eqz v0, :cond_f

    .line 26400
    const/16 v0, 0x11

    iget-object v1, p0, Ldwv;->q:Ldpn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26402
    :cond_f
    iget-object v0, p0, Ldwv;->r:Ldtd;

    if-eqz v0, :cond_10

    .line 26403
    const/16 v0, 0x12

    iget-object v1, p0, Ldwv;->r:Ldtd;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26405
    :cond_10
    iget-object v0, p0, Ldwv;->s:Ldpy;

    if-eqz v0, :cond_11

    .line 26406
    const/16 v0, 0x13

    iget-object v1, p0, Ldwv;->s:Ldpy;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26408
    :cond_11
    iget-object v0, p0, Ldwv;->t:Ldvp;

    if-eqz v0, :cond_12

    .line 26409
    const/16 v0, 0x14

    iget-object v1, p0, Ldwv;->t:Ldvp;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26411
    :cond_12
    iget-object v0, p0, Ldwv;->u:Ldpz;

    if-eqz v0, :cond_13

    .line 26412
    const/16 v0, 0x15

    iget-object v1, p0, Ldwv;->u:Ldpz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26414
    :cond_13
    iget-object v0, p0, Ldwv;->v:Ldqq;

    if-eqz v0, :cond_14

    .line 26415
    const/16 v0, 0x16

    iget-object v1, p0, Ldwv;->v:Ldqq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26417
    :cond_14
    iget-object v0, p0, Ldwv;->m:Ldqe;

    if-eqz v0, :cond_15

    .line 26418
    const/16 v0, 0x17

    iget-object v1, p0, Ldwv;->m:Ldqe;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26420
    :cond_15
    iget-object v0, p0, Ldwv;->w:Ldsj;

    if-eqz v0, :cond_16

    .line 26421
    const/16 v0, 0x19

    iget-object v1, p0, Ldwv;->w:Ldsj;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26423
    :cond_16
    iget-object v0, p0, Ldwv;->x:Ldqd;

    if-eqz v0, :cond_17

    .line 26424
    const/16 v0, 0x1a

    iget-object v1, p0, Ldwv;->x:Ldqd;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26426
    :cond_17
    iget-object v0, p0, Ldwv;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 26428
    return-void
.end method
