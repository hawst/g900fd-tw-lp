.class public final Leme;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leme;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Lemf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1298
    const/4 v0, 0x0

    new-array v0, v0, [Leme;

    sput-object v0, Leme;->a:[Leme;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1299
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1398
    iput-object v0, p0, Leme;->b:Ljava/lang/Integer;

    .line 1403
    iput-object v0, p0, Leme;->d:Lemf;

    .line 1299
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1423
    const/4 v0, 0x0

    .line 1424
    iget-object v1, p0, Leme;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1425
    const/4 v0, 0x1

    iget-object v1, p0, Leme;->b:Ljava/lang/Integer;

    .line 1426
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1428
    :cond_0
    iget-object v1, p0, Leme;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1429
    const/4 v1, 0x2

    iget-object v2, p0, Leme;->c:Ljava/lang/String;

    .line 1430
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1432
    :cond_1
    iget-object v1, p0, Leme;->d:Lemf;

    if-eqz v1, :cond_2

    .line 1433
    const/4 v1, 0x3

    iget-object v2, p0, Leme;->d:Lemf;

    .line 1434
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1436
    :cond_2
    iget-object v1, p0, Leme;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1437
    iput v0, p0, Leme;->cachedSize:I

    .line 1438
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1295
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leme;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leme;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leme;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leme;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leme;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leme;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Leme;->d:Lemf;

    if-nez v0, :cond_4

    new-instance v0, Lemf;

    invoke-direct {v0}, Lemf;-><init>()V

    iput-object v0, p0, Leme;->d:Lemf;

    :cond_4
    iget-object v0, p0, Leme;->d:Lemf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1408
    iget-object v0, p0, Leme;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1409
    const/4 v0, 0x1

    iget-object v1, p0, Leme;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1411
    :cond_0
    iget-object v0, p0, Leme;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1412
    const/4 v0, 0x2

    iget-object v1, p0, Leme;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1414
    :cond_1
    iget-object v0, p0, Leme;->d:Lemf;

    if-eqz v0, :cond_2

    .line 1415
    const/4 v0, 0x3

    iget-object v1, p0, Leme;->d:Lemf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1417
    :cond_2
    iget-object v0, p0, Leme;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1419
    return-void
.end method
