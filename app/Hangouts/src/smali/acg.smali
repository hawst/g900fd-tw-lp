.class public final Lacg;
.super Ls;
.source "PG"


# instance fields
.field private Y:Landroid/content/DialogInterface$OnCancelListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0}, Ls;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Lacg;->Y:Landroid/content/DialogInterface$OnCancelListener;

    .line 184
    return-void
.end method

.method public f()Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 160
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lacg;->getActivity()Ly;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 161
    invoke-virtual {p0}, Lacg;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "message"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 162
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 163
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 164
    return-object v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lacg;->Y:Landroid/content/DialogInterface$OnCancelListener;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lacg;->Y:Landroid/content/DialogInterface$OnCancelListener;

    invoke-interface {v0, p1}, Landroid/content/DialogInterface$OnCancelListener;->onCancel(Landroid/content/DialogInterface;)V

    .line 180
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 152
    invoke-super {p0, p1}, Ls;->onCreate(Landroid/os/Bundle;)V

    .line 153
    invoke-virtual {p0}, Lacg;->getParentFragment()Lt;

    move-result-object v0

    if-nez v0, :cond_0

    .line 154
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lacg;->setRetainInstance(Z)V

    .line 156
    :cond_0
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 169
    invoke-virtual {p0}, Lacg;->b()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lacg;->getRetainInstance()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    invoke-virtual {p0}, Lacg;->b()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 172
    :cond_0
    invoke-super {p0}, Ls;->onDestroyView()V

    .line 173
    return-void
.end method
