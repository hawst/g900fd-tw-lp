.class public final Lash;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint$EndpointDataProvider;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lash;->a:Ljava/lang/String;

    .line 27
    iput-object p2, p0, Lash;->b:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Lash;->c:Ljava/lang/String;

    .line 29
    iput p4, p0, Lash;->d:I

    .line 30
    return-void
.end method

.method public static a(Lbdh;)Lash;
    .locals 5

    .prologue
    .line 55
    invoke-virtual {p0}, Lbdh;->d()Z

    move-result v0

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 56
    new-instance v0, Lash;

    iget-object v1, p0, Lbdh;->e:Ljava/lang/String;

    iget-object v2, p0, Lbdh;->c:Ljava/lang/String;

    iget-object v3, p0, Lbdh;->h:Ljava/lang/String;

    iget v4, p0, Lbdh;->t:I

    invoke-direct {v0, v1, v2, v3, v4}, Lash;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lash;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lash;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lash;->d:I

    return v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lash;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    iget-object v0, p0, Lash;->a:Ljava/lang/String;

    .line 38
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lash;->b:Ljava/lang/String;

    goto :goto_0
.end method
