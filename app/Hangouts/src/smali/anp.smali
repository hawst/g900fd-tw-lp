.class final Lanp;
.super Ls;
.source "PG"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Ls;-><init>()V

    .line 109
    return-void
.end method

.method public static q()Lanp;
    .locals 1

    .prologue
    .line 82
    new-instance v0, Lanp;

    invoke-direct {v0}, Lanp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public f()Landroid/app/Dialog;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 87
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lanp;->getActivity()Ly;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 89
    invoke-virtual {p0}, Lanp;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 90
    sget v2, Lf;->gw:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 92
    sget v0, Lg;->cq:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 94
    invoke-virtual {p0}, Lanp;->getActivity()Ly;

    move-result-object v3

    invoke-virtual {v3}, Ly;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {}, Lbwb;->b()Lbwc;

    move-result-object v4

    invoke-static {v4}, Lbwb;->a(Lbwc;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    sget v6, Lh;->by:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget v6, Lh;->bD:I

    new-array v7, v9, [Ljava/lang/Object;

    aput-object v4, v7, v8

    invoke-virtual {v3, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget v6, Lh;->C:I

    new-array v7, v9, [Ljava/lang/Object;

    aput-object v4, v7, v8

    invoke-virtual {v3, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    new-instance v3, Lanq;

    invoke-virtual {p0}, Lanp;->getActivity()Ly;

    move-result-object v4

    invoke-direct {v3, p0, v4, v5}, Lanq;-><init>(Lanp;Landroid/content/Context;Ljava/util/List;)V

    .line 96
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 98
    sget v0, Lh;->lL:I

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 99
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 101
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0}, Lanp;->getTargetFragment()Lt;

    move-result-object v0

    check-cast v0, Lano;

    invoke-virtual {v0}, Lano;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->finish()V

    .line 107
    return-void
.end method
