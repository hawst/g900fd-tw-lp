.class public final Ldsw;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldsw;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:[Ldui;

.field public d:Ljava/lang/Long;

.field public e:Ldqf;

.field public f:Ljava/lang/Long;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6848
    const/4 v0, 0x0

    new-array v0, v0, [Ldsw;

    sput-object v0, Ldsw;->a:[Ldsw;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6849
    invoke-direct {p0}, Lepn;-><init>()V

    .line 6852
    iput-object v1, p0, Ldsw;->b:Ljava/lang/Integer;

    .line 6855
    sget-object v0, Ldui;->a:[Ldui;

    iput-object v0, p0, Ldsw;->c:[Ldui;

    .line 6860
    iput-object v1, p0, Ldsw;->e:Ldqf;

    .line 6867
    iput-object v1, p0, Ldsw;->h:Ljava/lang/Integer;

    .line 6849
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 6904
    iget-object v0, p0, Ldsw;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 6905
    const/4 v0, 0x1

    iget-object v2, p0, Ldsw;->b:Ljava/lang/Integer;

    .line 6906
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6908
    :goto_0
    iget-object v2, p0, Ldsw;->c:[Ldui;

    if-eqz v2, :cond_1

    .line 6909
    iget-object v2, p0, Ldsw;->c:[Ldui;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 6910
    if-eqz v4, :cond_0

    .line 6911
    const/4 v5, 0x2

    .line 6912
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 6909
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6916
    :cond_1
    iget-object v1, p0, Ldsw;->d:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 6917
    const/4 v1, 0x3

    iget-object v2, p0, Ldsw;->d:Ljava/lang/Long;

    .line 6918
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 6920
    :cond_2
    iget-object v1, p0, Ldsw;->e:Ldqf;

    if-eqz v1, :cond_3

    .line 6921
    const/4 v1, 0x4

    iget-object v2, p0, Ldsw;->e:Ldqf;

    .line 6922
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6924
    :cond_3
    iget-object v1, p0, Ldsw;->f:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 6925
    const/4 v1, 0x5

    iget-object v2, p0, Ldsw;->f:Ljava/lang/Long;

    .line 6926
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 6928
    :cond_4
    iget-object v1, p0, Ldsw;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 6929
    const/4 v1, 0x6

    iget-object v2, p0, Ldsw;->g:Ljava/lang/Boolean;

    .line 6930
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6932
    :cond_5
    iget-object v1, p0, Ldsw;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 6933
    const/4 v1, 0x7

    iget-object v2, p0, Ldsw;->h:Ljava/lang/Integer;

    .line 6934
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6936
    :cond_6
    iget-object v1, p0, Ldsw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6937
    iput v0, p0, Ldsw;->cachedSize:I

    .line 6938
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 6845
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldsw;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldsw;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldsw;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x6

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-ne v0, v2, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldsw;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldsw;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldsw;->c:[Ldui;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldui;

    iget-object v3, p0, Ldsw;->c:[Ldui;

    if-eqz v3, :cond_4

    iget-object v3, p0, Ldsw;->c:[Ldui;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Ldsw;->c:[Ldui;

    :goto_2
    iget-object v2, p0, Ldsw;->c:[Ldui;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ldsw;->c:[Ldui;

    new-instance v3, Ldui;

    invoke-direct {v3}, Ldui;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldsw;->c:[Ldui;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Ldsw;->c:[Ldui;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Ldsw;->c:[Ldui;

    new-instance v3, Ldui;

    invoke-direct {v3}, Ldui;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldsw;->c:[Ldui;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldsw;->d:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Ldsw;->e:Ldqf;

    if-nez v0, :cond_7

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldsw;->e:Ldqf;

    :cond_7
    iget-object v0, p0, Ldsw;->e:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldsw;->f:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldsw;->g:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_8

    if-eq v0, v5, :cond_8

    if-ne v0, v6, :cond_9

    :cond_8
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldsw;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_9
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldsw;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 6872
    iget-object v0, p0, Ldsw;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 6873
    const/4 v0, 0x1

    iget-object v1, p0, Ldsw;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 6875
    :cond_0
    iget-object v0, p0, Ldsw;->c:[Ldui;

    if-eqz v0, :cond_2

    .line 6876
    iget-object v1, p0, Ldsw;->c:[Ldui;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 6877
    if-eqz v3, :cond_1

    .line 6878
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 6876
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6882
    :cond_2
    iget-object v0, p0, Ldsw;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 6883
    const/4 v0, 0x3

    iget-object v1, p0, Ldsw;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 6885
    :cond_3
    iget-object v0, p0, Ldsw;->e:Ldqf;

    if-eqz v0, :cond_4

    .line 6886
    const/4 v0, 0x4

    iget-object v1, p0, Ldsw;->e:Ldqf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6888
    :cond_4
    iget-object v0, p0, Ldsw;->f:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 6889
    const/4 v0, 0x5

    iget-object v1, p0, Ldsw;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 6891
    :cond_5
    iget-object v0, p0, Ldsw;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 6892
    const/4 v0, 0x6

    iget-object v1, p0, Ldsw;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 6894
    :cond_6
    iget-object v0, p0, Ldsw;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 6895
    const/4 v0, 0x7

    iget-object v1, p0, Ldsw;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 6897
    :cond_7
    iget-object v0, p0, Ldsw;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 6899
    return-void
.end method
