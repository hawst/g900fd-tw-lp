.class public final Lbpq;
.super Lbnj;
.source "PG"


# static fields
.field public static final a:Z


# instance fields
.field private final d:Ljava/lang/String;

.field private final e:[B

.field private final f:J

.field private final g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lbys;->k:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbpq;->a:Z

    return-void
.end method

.method public constructor <init>(Lyj;Ljava/lang/String;[BJZ)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lbnj;-><init>(Lyj;)V

    .line 35
    iput-object p2, p0, Lbpq;->d:Ljava/lang/String;

    .line 36
    iput-object p3, p0, Lbpq;->e:[B

    .line 37
    iput-wide p4, p0, Lbpq;->f:J

    .line 38
    iput-boolean p6, p0, Lbpq;->g:Z

    .line 39
    return-void
.end method


# virtual methods
.method public a()V
    .locals 7

    .prologue
    .line 43
    new-instance v0, Lyt;

    iget-object v1, p0, Lbpq;->b:Lyj;

    invoke-direct {v0, v1}, Lyt;-><init>(Lyj;)V

    .line 44
    iget-boolean v1, p0, Lbpq;->g:Z

    if-nez v1, :cond_2

    iget-wide v1, p0, Lbpq;->f:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_2

    .line 46
    iget-wide v1, p0, Lbpq;->f:J

    invoke-virtual {v0, v1, v2}, Lyt;->b(J)Lzg;

    move-result-object v1

    .line 47
    if-eqz v1, :cond_1

    iget v1, v1, Lzg;->f:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 50
    sget-boolean v0, Lbpq;->a:Z

    if-eqz v0, :cond_0

    .line 51
    const-string v0, "Babel"

    const-string v1, "RetrieveMmsOperation: ignore repeated download"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 57
    :cond_1
    iget-wide v1, p0, Lbpq;->f:J

    .line 60
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 57
    invoke-virtual {v0, v1, v2, v3, v4}, Lyt;->b(JJ)V

    .line 61
    iget-wide v1, p0, Lbpq;->f:J

    invoke-static {v0, v1, v2}, Lyp;->b(Lyt;J)V

    .line 64
    :cond_2
    iget-object v6, p0, Lbpq;->c:Lbnl;

    new-instance v0, Lbez;

    iget-object v1, p0, Lbpq;->d:Ljava/lang/String;

    iget-object v2, p0, Lbpq;->e:[B

    iget-wide v3, p0, Lbpq;->f:J

    iget-boolean v5, p0, Lbpq;->g:Z

    invoke-direct/range {v0 .. v5}, Lbez;-><init>(Ljava/lang/String;[BJZ)V

    invoke-virtual {v6, v0}, Lbnl;->a(Lbea;)V

    goto :goto_0
.end method
