.class public Lbhf;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method private constructor <init>(Ldyi;)V
    .locals 3

    .prologue
    .line 4112
    iget-object v0, p1, Ldyi;->b:Ldvn;

    const-wide/16 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 4113
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 4114
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ModifyBroadcastInfoResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4117
    :cond_0
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 3

    .prologue
    .line 4121
    new-instance v0, Ldyi;

    invoke-direct {v0}, Ldyi;-><init>()V

    .line 4122
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldyi;

    .line 4123
    iget-object v1, v0, Ldyi;->b:Ldvn;

    iget-object v1, v1, Ldvn;->b:Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v1

    const/16 v2, 0x9

    if-eq v1, v2, :cond_0

    iget-object v1, v0, Ldyi;->b:Ldvn;

    .line 4125
    invoke-static {v1}, Lbhf;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4126
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldyi;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 4128
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbhf;

    invoke-direct {v1, v0}, Lbhf;-><init>(Ldyi;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public f()Z
    .locals 2

    .prologue
    .line 4132
    iget-object v0, p0, Lbhf;->c:Lbht;

    iget v0, v0, Lbht;->b:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
