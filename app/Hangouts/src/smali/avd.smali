.class public final Lavd;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lavf;

.field b:Z

.field final synthetic c:Lavb;


# direct methods
.method private constructor <init>(Lavb;Lavf;)V
    .locals 0

    .prologue
    .line 750
    iput-object p1, p0, Lavd;->c:Lavb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 751
    iput-object p2, p0, Lavd;->a:Lavf;

    .line 752
    return-void
.end method

.method synthetic constructor <init>(Lavb;Lavf;B)V
    .locals 0

    .prologue
    .line 746
    invoke-direct {p0, p1, p2}, Lavd;-><init>(Lavb;Lavf;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/io/OutputStream;
    .locals 5

    .prologue
    .line 784
    iget-object v1, p0, Lavd;->c:Lavb;

    monitor-enter v1

    .line 785
    :try_start_0
    iget-object v0, p0, Lavd;->a:Lavf;

    iget-object v0, v0, Lavf;->d:Lavd;

    if-eq v0, p0, :cond_0

    .line 786
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 789
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 788
    :cond_0
    :try_start_1
    new-instance v0, Lave;

    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v3, p0, Lavd;->a:Lavf;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lavf;->b(I)Ljava/io/File;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/4 v3, 0x0

    invoke-direct {v0, p0, v2, v3}, Lave;-><init>(Lavd;Ljava/io/OutputStream;B)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 810
    iget-boolean v0, p0, Lavd;->b:Z

    if-eqz v0, :cond_0

    .line 811
    iget-object v0, p0, Lavd;->c:Lavb;

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lavb;->a(Lavb;Lavd;Z)V

    .line 812
    iget-object v0, p0, Lavd;->c:Lavb;

    iget-object v1, p0, Lavd;->a:Lavf;

    iget-object v1, v1, Lavf;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lavb;->c(Ljava/lang/String;)Z

    .line 816
    :goto_0
    return-void

    .line 814
    :cond_0
    iget-object v0, p0, Lavd;->c:Lavb;

    const/4 v1, 0x1

    invoke-static {v0, p0, v1}, Lavb;->a(Lavb;Lavd;Z)V

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 823
    iget-object v0, p0, Lavd;->c:Lavb;

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lavb;->a(Lavb;Lavd;Z)V

    .line 824
    return-void
.end method
