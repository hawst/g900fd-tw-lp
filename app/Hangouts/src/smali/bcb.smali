.class public final Lbcb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ltt;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;

.field private b:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 651
    iput-object p1, p0, Lbcb;->a:Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 652
    iput-object p2, p0, Lbcb;->b:Landroid/os/Handler;

    .line 653
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 656
    iget-object v0, p0, Lbcb;->a:Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;

    const-string v0, "onStart"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;->b(Ljava/lang/String;)V

    .line 657
    return-void
.end method

.method public a(Lsy;)V
    .locals 4

    .prologue
    .line 660
    iget-object v0, p0, Lbcb;->a:Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onEntryCreated, display name is "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lsy;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;->b(Ljava/lang/String;)V

    .line 661
    iget-object v0, p1, Lsy;->a:Ljava/util/List;

    .line 662
    if-eqz v0, :cond_0

    .line 663
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltl;

    .line 664
    iget-object v2, p0, Lbcb;->a:Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onEntryCreated, "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ltl;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 667
    :cond_0
    iget-object v0, p0, Lbcb;->a:Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;

    const-string v0, "onEntryCreated, entry.getPhoneList() is null"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;->b(Ljava/lang/String;)V

    .line 670
    :cond_1
    iget-object v0, p0, Lbcb;->b:Landroid/os/Handler;

    if-eqz v0, :cond_2

    .line 671
    iget-object v0, p0, Lbcb;->b:Landroid/os/Handler;

    const/16 v1, 0x3eb

    .line 672
    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 673
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 675
    :cond_2
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 678
    iget-object v0, p0, Lbcb;->a:Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;

    const-string v0, "onEnd"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;->b(Ljava/lang/String;)V

    .line 679
    iget-object v0, p0, Lbcb;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 680
    iget-object v0, p0, Lbcb;->b:Landroid/os/Handler;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 682
    :cond_0
    return-void
.end method
