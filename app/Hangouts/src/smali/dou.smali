.class public final Ldou;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldou;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/Boolean;

.field public i:Ljava/lang/Boolean;

.field public j:Ljava/lang/Boolean;

.field public k:Ljava/lang/Boolean;

.field public l:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1905
    const/4 v0, 0x0

    new-array v0, v0, [Ldou;

    sput-object v0, Ldou;->a:[Ldou;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1906
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1917
    const/4 v0, 0x0

    iput-object v0, p0, Ldou;->b:Ljava/lang/Integer;

    .line 1906
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 1979
    const/4 v0, 0x1

    iget-object v1, p0, Ldou;->b:Ljava/lang/Integer;

    .line 1981
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1982
    iget-object v1, p0, Ldou;->c:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 1983
    const/4 v1, 0x2

    iget-object v2, p0, Ldou;->c:Ljava/lang/Long;

    .line 1984
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1986
    :cond_0
    iget-object v1, p0, Ldou;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 1987
    const/4 v1, 0x4

    iget-object v2, p0, Ldou;->f:Ljava/lang/Integer;

    .line 1988
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1990
    :cond_1
    iget-object v1, p0, Ldou;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 1991
    const/4 v1, 0x5

    iget-object v2, p0, Ldou;->g:Ljava/lang/Integer;

    .line 1992
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1994
    :cond_2
    iget-object v1, p0, Ldou;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 1995
    const/4 v1, 0x6

    iget-object v2, p0, Ldou;->h:Ljava/lang/Boolean;

    .line 1996
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1998
    :cond_3
    iget-object v1, p0, Ldou;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 1999
    const/4 v1, 0x7

    iget-object v2, p0, Ldou;->i:Ljava/lang/Boolean;

    .line 2000
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2002
    :cond_4
    iget-object v1, p0, Ldou;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 2003
    const/16 v1, 0x8

    iget-object v2, p0, Ldou;->j:Ljava/lang/Boolean;

    .line 2004
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2006
    :cond_5
    iget-object v1, p0, Ldou;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 2007
    const/16 v1, 0xb

    iget-object v2, p0, Ldou;->l:Ljava/lang/Integer;

    .line 2008
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2010
    :cond_6
    iget-object v1, p0, Ldou;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 2011
    const/16 v1, 0xc

    iget-object v2, p0, Ldou;->e:Ljava/lang/Integer;

    .line 2012
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2014
    :cond_7
    iget-object v1, p0, Ldou;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 2015
    const/16 v1, 0xd

    iget-object v2, p0, Ldou;->k:Ljava/lang/Boolean;

    .line 2016
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2018
    :cond_8
    iget-object v1, p0, Ldou;->d:Ljava/lang/Long;

    if-eqz v1, :cond_9

    .line 2019
    const/16 v1, 0xe

    iget-object v2, p0, Ldou;->d:Ljava/lang/Long;

    .line 2020
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2022
    :cond_9
    iget-object v1, p0, Ldou;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2023
    iput v0, p0, Ldou;->cachedSize:I

    .line 2024
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1902
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldou;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldou;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldou;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldou;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldou;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldou;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldou;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldou;->g:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldou;->h:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldou;->i:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldou;->j:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldou;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldou;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldou;->k:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldou;->d:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x38 -> :sswitch_6
        0x40 -> :sswitch_7
        0x58 -> :sswitch_8
        0x60 -> :sswitch_9
        0x68 -> :sswitch_a
        0x70 -> :sswitch_b
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 1942
    const/4 v0, 0x1

    iget-object v1, p0, Ldou;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1943
    iget-object v0, p0, Ldou;->c:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1944
    const/4 v0, 0x2

    iget-object v1, p0, Ldou;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 1946
    :cond_0
    iget-object v0, p0, Ldou;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1947
    const/4 v0, 0x4

    iget-object v1, p0, Ldou;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1949
    :cond_1
    iget-object v0, p0, Ldou;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1950
    const/4 v0, 0x5

    iget-object v1, p0, Ldou;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1952
    :cond_2
    iget-object v0, p0, Ldou;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 1953
    const/4 v0, 0x6

    iget-object v1, p0, Ldou;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 1955
    :cond_3
    iget-object v0, p0, Ldou;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 1956
    const/4 v0, 0x7

    iget-object v1, p0, Ldou;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 1958
    :cond_4
    iget-object v0, p0, Ldou;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 1959
    const/16 v0, 0x8

    iget-object v1, p0, Ldou;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 1961
    :cond_5
    iget-object v0, p0, Ldou;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 1962
    const/16 v0, 0xb

    iget-object v1, p0, Ldou;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1964
    :cond_6
    iget-object v0, p0, Ldou;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 1965
    const/16 v0, 0xc

    iget-object v1, p0, Ldou;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1967
    :cond_7
    iget-object v0, p0, Ldou;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 1968
    const/16 v0, 0xd

    iget-object v1, p0, Ldou;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 1970
    :cond_8
    iget-object v0, p0, Ldou;->d:Ljava/lang/Long;

    if-eqz v0, :cond_9

    .line 1971
    const/16 v0, 0xe

    iget-object v1, p0, Ldou;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 1973
    :cond_9
    iget-object v0, p0, Ldou;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1975
    return-void
.end method
