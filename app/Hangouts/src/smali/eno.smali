.class public final Leno;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leno;


# instance fields
.field public b:[B

.field public c:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1216
    const/4 v0, 0x0

    new-array v0, v0, [Leno;

    sput-object v0, Leno;->a:[Leno;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1217
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1238
    const/4 v0, 0x0

    .line 1239
    iget-object v1, p0, Leno;->b:[B

    if-eqz v1, :cond_0

    .line 1240
    const/4 v0, 0x1

    iget-object v1, p0, Leno;->b:[B

    .line 1241
    invoke-static {v0, v1}, Lepl;->b(I[B)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1243
    :cond_0
    iget-object v1, p0, Leno;->c:[B

    if-eqz v1, :cond_1

    .line 1244
    const/4 v1, 0x2

    iget-object v2, p0, Leno;->c:[B

    .line 1245
    invoke-static {v1, v2}, Lepl;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 1247
    :cond_1
    iget-object v1, p0, Leno;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1248
    iput v0, p0, Leno;->cachedSize:I

    .line 1249
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1213
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leno;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leno;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leno;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Leno;->b:[B

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Leno;->c:[B

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1226
    iget-object v0, p0, Leno;->b:[B

    if-eqz v0, :cond_0

    .line 1227
    const/4 v0, 0x1

    iget-object v1, p0, Leno;->b:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 1229
    :cond_0
    iget-object v0, p0, Leno;->c:[B

    if-eqz v0, :cond_1

    .line 1230
    const/4 v0, 0x2

    iget-object v1, p0, Leno;->c:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 1232
    :cond_1
    iget-object v0, p0, Leno;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1234
    return-void
.end method
