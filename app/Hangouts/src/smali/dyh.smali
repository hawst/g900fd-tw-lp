.class public final Ldyh;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldyh;


# instance fields
.field public b:Ldvm;

.field public c:Ldxv;

.field public d:Ldzg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1580
    const/4 v0, 0x0

    new-array v0, v0, [Ldyh;

    sput-object v0, Ldyh;->a:[Ldyh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1581
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1584
    iput-object v0, p0, Ldyh;->b:Ldvm;

    .line 1587
    iput-object v0, p0, Ldyh;->c:Ldxv;

    .line 1590
    iput-object v0, p0, Ldyh;->d:Ldzg;

    .line 1581
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1610
    const/4 v0, 0x0

    .line 1611
    iget-object v1, p0, Ldyh;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 1612
    const/4 v0, 0x1

    iget-object v1, p0, Ldyh;->b:Ldvm;

    .line 1613
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1615
    :cond_0
    iget-object v1, p0, Ldyh;->c:Ldxv;

    if-eqz v1, :cond_1

    .line 1616
    const/4 v1, 0x2

    iget-object v2, p0, Ldyh;->c:Ldxv;

    .line 1617
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1619
    :cond_1
    iget-object v1, p0, Ldyh;->d:Ldzg;

    if-eqz v1, :cond_2

    .line 1620
    const/4 v1, 0x3

    iget-object v2, p0, Ldyh;->d:Ldzg;

    .line 1621
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1623
    :cond_2
    iget-object v1, p0, Ldyh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1624
    iput v0, p0, Ldyh;->cachedSize:I

    .line 1625
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1577
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldyh;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldyh;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldyh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldyh;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldyh;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldyh;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldyh;->c:Ldxv;

    if-nez v0, :cond_3

    new-instance v0, Ldxv;

    invoke-direct {v0}, Ldxv;-><init>()V

    iput-object v0, p0, Ldyh;->c:Ldxv;

    :cond_3
    iget-object v0, p0, Ldyh;->c:Ldxv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldyh;->d:Ldzg;

    if-nez v0, :cond_4

    new-instance v0, Ldzg;

    invoke-direct {v0}, Ldzg;-><init>()V

    iput-object v0, p0, Ldyh;->d:Ldzg;

    :cond_4
    iget-object v0, p0, Ldyh;->d:Ldzg;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1595
    iget-object v0, p0, Ldyh;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 1596
    const/4 v0, 0x1

    iget-object v1, p0, Ldyh;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1598
    :cond_0
    iget-object v0, p0, Ldyh;->c:Ldxv;

    if-eqz v0, :cond_1

    .line 1599
    const/4 v0, 0x2

    iget-object v1, p0, Ldyh;->c:Ldxv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1601
    :cond_1
    iget-object v0, p0, Ldyh;->d:Ldzg;

    if-eqz v0, :cond_2

    .line 1602
    const/4 v0, 0x3

    iget-object v1, p0, Ldyh;->d:Ldzg;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1604
    :cond_2
    iget-object v0, p0, Ldyh;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1606
    return-void
.end method
