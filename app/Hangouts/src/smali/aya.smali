.class public final Laya;
.super Lpr;
.source "PG"


# instance fields
.field private final A:Ljava/lang/Runnable;

.field private z:Lyj;


# direct methods
.method public constructor <init>(Lqa;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lpr;-><init>(Lqa;)V

    .line 190
    new-instance v0, Layb;

    invoke-direct {v0, p0}, Layb;-><init>(Laya;)V

    iput-object v0, p0, Laya;->A:Ljava/lang/Runnable;

    .line 64
    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;)Ldg;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Ldg",
            "<",
            "Lqn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127
    new-instance v0, Laxy;

    invoke-virtual {p0}, Laya;->c()Lqa;

    move-result-object v1

    invoke-interface {v1}, Lqa;->l()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Laya;->z:Lyj;

    invoke-direct {v0, v1, p2, p1, v2}, Laxy;-><init>(Landroid/content/Context;Ljava/lang/String;ILyj;)V

    return-object v0
.end method

.method public a(Landroid/content/Context;Lae;F)Lqi;
    .locals 1

    .prologue
    .line 69
    new-instance v0, Laxz;

    invoke-direct {v0, p1, p2, p3}, Laxz;-><init>(Landroid/content/Context;Lae;F)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 120
    invoke-super {p0, p1}, Lpr;->a(Landroid/os/Bundle;)V

    .line 122
    invoke-virtual {p0}, Laya;->c()Lqa;

    move-result-object v0

    invoke-interface {v0}, Lqa;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 121
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    iput-object v0, p0, Laya;->z:Lyj;

    .line 123
    return-void
.end method

.method public a(Lqj;Z)V
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Laya;->j:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    .line 204
    invoke-virtual {p1}, Lqj;->u()I

    move-result v0

    iget-object v1, p0, Laya;->i:Lcom/android/ex/photo/PhotoViewPager;

    invoke-virtual {v1}, Lcom/android/ex/photo/PhotoViewPager;->c()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 205
    if-nez p2, :cond_0

    .line 210
    const-string v0, "Babel"

    const-string v1, "Failed to load fragment image"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :cond_0
    invoke-virtual {p1}, Lqj;->r()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 213
    instance-of v1, v0, Lccm;

    if-eqz v1, :cond_2

    .line 214
    check-cast v0, Lccm;

    iget-object v1, p0, Laya;->A:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lccm;->a(Ljava/lang/Runnable;)V

    .line 219
    :cond_1
    :goto_0
    return-void

    .line 216
    :cond_2
    iget-object v0, p0, Laya;->A:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public d()V
    .locals 0

    .prologue
    .line 132
    invoke-super {p0}, Lpr;->d()V

    .line 133
    invoke-static {}, Layc;->a()V

    .line 134
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 138
    invoke-super {p0}, Lpr;->g()V

    .line 139
    iget-object v0, p0, Laya;->y:Landroid/os/Handler;

    invoke-static {v0}, Layc;->a(Landroid/os/Handler;)V

    .line 140
    return-void
.end method

.method public k()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x1

    .line 147
    invoke-virtual {p0}, Laya;->l()Landroid/database/Cursor;

    move-result-object v1

    .line 148
    if-eqz v1, :cond_4

    .line 149
    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 150
    const/4 v2, 0x5

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 151
    const/4 v3, 0x7

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    .line 152
    const/4 v5, 0x6

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 153
    const/4 v6, 0x2

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 155
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 156
    iput-object v2, p0, Laya;->o:Ljava/lang/String;

    .line 157
    invoke-static {v3, v4}, Lf;->b(J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laya;->p:Ljava/lang/String;

    .line 169
    :goto_0
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 171
    const-string v1, "image_uri"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    invoke-virtual {p0}, Laya;->c()Lqa;

    move-result-object v1

    invoke-interface {v1}, Lqa;->f()Lav;

    move-result-object v1

    iget-object v2, p0, Laya;->x:Lqb;

    invoke-virtual {v1, v7, v0, v2}, Lav;->b(ILandroid/os/Bundle;Law;)Ldg;

    .line 180
    :cond_0
    :goto_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 181
    invoke-virtual {p0}, Laya;->c()Lqa;

    move-result-object v0

    invoke-interface {v0}, Lqa;->m()Lpk;

    move-result-object v0

    invoke-virtual {p0, v0}, Laya;->a(Lpk;)V

    .line 183
    :cond_1
    return-void

    .line 161
    :cond_2
    sget-object v6, Lbvx;->b:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 162
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 164
    :cond_3
    iput-object v0, p0, Laya;->o:Ljava/lang/String;

    .line 165
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3, v4}, Lf;->b(J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laya;->p:Ljava/lang/String;

    goto :goto_0

    .line 176
    :cond_4
    iput-object v0, p0, Laya;->o:Ljava/lang/String;

    .line 177
    iput-object v0, p0, Laya;->p:Ljava/lang/String;

    goto :goto_1
.end method
