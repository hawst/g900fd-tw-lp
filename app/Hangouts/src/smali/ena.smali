.class public final Lena;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lena;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 366
    const/4 v0, 0x0

    new-array v0, v0, [Lena;

    sput-object v0, Lena;->a:[Lena;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 367
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 393
    const/4 v0, 0x0

    .line 394
    iget-object v1, p0, Lena;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 395
    const/4 v0, 0x1

    iget-object v1, p0, Lena;->b:Ljava/lang/String;

    .line 396
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 398
    :cond_0
    iget-object v1, p0, Lena;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 399
    const/4 v1, 0x2

    iget-object v2, p0, Lena;->c:Ljava/lang/Long;

    .line 400
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 402
    :cond_1
    iget-object v1, p0, Lena;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 403
    const/4 v1, 0x3

    iget-object v2, p0, Lena;->d:Ljava/lang/String;

    .line 404
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 406
    :cond_2
    iget-object v1, p0, Lena;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 407
    iput v0, p0, Lena;->cachedSize:I

    .line 408
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 363
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lena;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lena;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lena;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lena;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lena;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lena;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x11 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 378
    iget-object v0, p0, Lena;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 379
    const/4 v0, 0x1

    iget-object v1, p0, Lena;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 381
    :cond_0
    iget-object v0, p0, Lena;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 382
    const/4 v0, 0x2

    iget-object v1, p0, Lena;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->c(IJ)V

    .line 384
    :cond_1
    iget-object v0, p0, Lena;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 385
    const/4 v0, 0x3

    iget-object v1, p0, Lena;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 387
    :cond_2
    iget-object v0, p0, Lena;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 389
    return-void
.end method
