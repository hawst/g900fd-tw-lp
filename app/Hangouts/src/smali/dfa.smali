.class public final Ldfa;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldfa;


# instance fields
.field public b:Ldfi;

.field public c:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1475
    const/4 v0, 0x0

    new-array v0, v0, [Ldfa;

    sput-object v0, Ldfa;->a:[Ldfa;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1476
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1479
    const/4 v0, 0x0

    iput-object v0, p0, Ldfa;->b:Ldfi;

    .line 1476
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1498
    const/4 v0, 0x0

    .line 1499
    iget-object v1, p0, Ldfa;->b:Ldfi;

    if-eqz v1, :cond_0

    .line 1500
    const/4 v0, 0x1

    iget-object v1, p0, Ldfa;->b:Ldfi;

    .line 1501
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1503
    :cond_0
    iget-object v1, p0, Ldfa;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 1504
    const/4 v1, 0x2

    iget-object v2, p0, Ldfa;->c:Ljava/lang/Integer;

    .line 1505
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1507
    :cond_1
    iget-object v1, p0, Ldfa;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1508
    iput v0, p0, Ldfa;->cachedSize:I

    .line 1509
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1472
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldfa;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldfa;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldfa;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldfa;->b:Ldfi;

    if-nez v0, :cond_2

    new-instance v0, Ldfi;

    invoke-direct {v0}, Ldfi;-><init>()V

    iput-object v0, p0, Ldfa;->b:Ldfi;

    :cond_2
    iget-object v0, p0, Ldfa;->b:Ldfi;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldfa;->c:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1486
    iget-object v0, p0, Ldfa;->b:Ldfi;

    if-eqz v0, :cond_0

    .line 1487
    const/4 v0, 0x1

    iget-object v1, p0, Ldfa;->b:Ldfi;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1489
    :cond_0
    iget-object v0, p0, Ldfa;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1490
    const/4 v0, 0x2

    iget-object v1, p0, Ldfa;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1492
    :cond_1
    iget-object v0, p0, Ldfa;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1494
    return-void
.end method
