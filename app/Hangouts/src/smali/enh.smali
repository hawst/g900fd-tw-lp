.class public final Lenh;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lenh;


# instance fields
.field public b:Ljava/lang/Boolean;

.field public c:Leny;

.field public d:Leol;

.field public e:Lekk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 461
    const/4 v0, 0x0

    new-array v0, v0, [Lenh;

    sput-object v0, Lenh;->a:[Lenh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 462
    invoke-direct {p0}, Lepn;-><init>()V

    .line 467
    iput-object v0, p0, Lenh;->c:Leny;

    .line 470
    iput-object v0, p0, Lenh;->d:Leol;

    .line 473
    iput-object v0, p0, Lenh;->e:Lekk;

    .line 462
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 496
    const/4 v0, 0x0

    .line 497
    iget-object v1, p0, Lenh;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 498
    const/4 v0, 0x1

    iget-object v1, p0, Lenh;->b:Ljava/lang/Boolean;

    .line 499
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 501
    :cond_0
    iget-object v1, p0, Lenh;->c:Leny;

    if-eqz v1, :cond_1

    .line 502
    const/4 v1, 0x2

    iget-object v2, p0, Lenh;->c:Leny;

    .line 503
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 505
    :cond_1
    iget-object v1, p0, Lenh;->d:Leol;

    if-eqz v1, :cond_2

    .line 506
    const/4 v1, 0x3

    iget-object v2, p0, Lenh;->d:Leol;

    .line 507
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 509
    :cond_2
    iget-object v1, p0, Lenh;->e:Lekk;

    if-eqz v1, :cond_3

    .line 510
    const/4 v1, 0x4

    iget-object v2, p0, Lenh;->e:Lekk;

    .line 511
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 513
    :cond_3
    iget-object v1, p0, Lenh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 514
    iput v0, p0, Lenh;->cachedSize:I

    .line 515
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 458
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lenh;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lenh;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lenh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lenh;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lenh;->c:Leny;

    if-nez v0, :cond_2

    new-instance v0, Leny;

    invoke-direct {v0}, Leny;-><init>()V

    iput-object v0, p0, Lenh;->c:Leny;

    :cond_2
    iget-object v0, p0, Lenh;->c:Leny;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lenh;->d:Leol;

    if-nez v0, :cond_3

    new-instance v0, Leol;

    invoke-direct {v0}, Leol;-><init>()V

    iput-object v0, p0, Lenh;->d:Leol;

    :cond_3
    iget-object v0, p0, Lenh;->d:Leol;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lenh;->e:Lekk;

    if-nez v0, :cond_4

    new-instance v0, Lekk;

    invoke-direct {v0}, Lekk;-><init>()V

    iput-object v0, p0, Lenh;->e:Lekk;

    :cond_4
    iget-object v0, p0, Lenh;->e:Lekk;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 478
    iget-object v0, p0, Lenh;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 479
    const/4 v0, 0x1

    iget-object v1, p0, Lenh;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 481
    :cond_0
    iget-object v0, p0, Lenh;->c:Leny;

    if-eqz v0, :cond_1

    .line 482
    const/4 v0, 0x2

    iget-object v1, p0, Lenh;->c:Leny;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 484
    :cond_1
    iget-object v0, p0, Lenh;->d:Leol;

    if-eqz v0, :cond_2

    .line 485
    const/4 v0, 0x3

    iget-object v1, p0, Lenh;->d:Leol;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 487
    :cond_2
    iget-object v0, p0, Lenh;->e:Lekk;

    if-eqz v0, :cond_3

    .line 488
    const/4 v0, 0x4

    iget-object v1, p0, Lenh;->e:Lekk;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 490
    :cond_3
    iget-object v0, p0, Lenh;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 492
    return-void
.end method
