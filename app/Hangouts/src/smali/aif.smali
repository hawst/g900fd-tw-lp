.class public final Laif;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)V
    .locals 0

    .prologue
    .line 1205
    iput-object p1, p0, Laif;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 1209
    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1210
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onScroll initialLoadFinished="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Laif;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->e(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mExhaustedConversationsToLoad="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Laif;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    .line 1211
    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->f(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mLoadingOlderConversations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Laif;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    .line 1212
    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->g(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1210
    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d(Ljava/lang/String;)V

    .line 1214
    :cond_0
    iget-object v0, p0, Laif;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->e(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    add-int v0, p2, p3

    if-ne v0, p4, :cond_1

    .line 1215
    const-string v0, "Checking for more conversations"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->e(Ljava/lang/String;)V

    .line 1216
    iget-object v0, p0, Laif;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1217
    iget-object v0, p0, Laif;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    iget-object v1, p0, Laif;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;Landroid/view/View;)V

    .line 1220
    :cond_1
    iget-object v0, p0, Laif;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->i(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)V

    .line 1221
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 1225
    if-eqz p2, :cond_0

    .line 1226
    iget-object v0, p0, Laif;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->r()V

    .line 1228
    :cond_0
    return-void
.end method
