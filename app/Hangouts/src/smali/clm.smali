.class final Lclm;
.super Lcht;

# interfaces
.implements Lcvb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lchr",
        "<",
        "Lclb;",
        ">.cht<",
        "Lcgk",
        "<",
        "Lcvb;",
        ">;>;",
        "Lcvb;"
    }
.end annotation


# instance fields
.field final synthetic b:Lclj;

.field private final c:Lcom/google/android/gms/common/api/Status;

.field private final d:Landroid/os/ParcelFileDescriptor;


# direct methods
.method public constructor <init>(Lclj;Lcgk;Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcgk",
            "<",
            "Lcvb;",
            ">;",
            "Lcom/google/android/gms/common/api/Status;",
            "Landroid/os/ParcelFileDescriptor;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lclm;->b:Lclj;

    invoke-direct {p0, p1, p2}, Lcht;-><init>(Lchr;Ljava/lang/Object;)V

    iput-object p3, p0, Lclm;->c:Lcom/google/android/gms/common/api/Status;

    iput-object p4, p0, Lclm;->d:Landroid/os/ParcelFileDescriptor;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 0

    invoke-virtual {p0}, Lclm;->b()V

    return-void
.end method

.method protected synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcgk;

    if-eqz p1, :cond_0

    invoke-interface {p1, p0}, Lcgk;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lclm;->d:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lclm;->d:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public c()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, Lclm;->c:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public g()Landroid/os/ParcelFileDescriptor;
    .locals 1

    iget-object v0, p0, Lclm;->d:Landroid/os/ParcelFileDescriptor;

    return-object v0
.end method
