.class public final Lec;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Lek;

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field private static final d:Lec;

.field private static final e:Lec;


# instance fields
.field private final f:Z

.field private final g:I

.field private final h:Lek;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 83
    sget-object v0, Lel;->c:Lek;

    sput-object v0, Lec;->a:Lek;

    .line 113
    const/16 v0, 0x200e

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lec;->b:Ljava/lang/String;

    .line 118
    const/16 v0, 0x200f

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lec;->c:Ljava/lang/String;

    .line 215
    new-instance v0, Lec;

    const/4 v1, 0x0

    sget-object v2, Lec;->a:Lek;

    invoke-direct {v0, v1, v3, v2}, Lec;-><init>(ZILek;)V

    sput-object v0, Lec;->d:Lec;

    .line 220
    new-instance v0, Lec;

    const/4 v1, 0x1

    sget-object v2, Lec;->a:Lek;

    invoke-direct {v0, v1, v3, v2}, Lec;-><init>(ZILek;)V

    sput-object v0, Lec;->e:Lec;

    return-void
.end method

.method private constructor <init>(ZILek;)V
    .locals 0

    .prologue
    .line 260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 261
    iput-boolean p1, p0, Lec;->f:Z

    .line 262
    iput p2, p0, Lec;->g:I

    .line 263
    iput-object p3, p0, Lec;->h:Lek;

    .line 264
    return-void
.end method

.method synthetic constructor <init>(ZILek;B)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0, p1, p2, p3}, Lec;-><init>(ZILek;)V

    return-void
.end method

.method public static a()Lec;
    .locals 1

    .prologue
    .line 234
    new-instance v0, Led;

    invoke-direct {v0}, Led;-><init>()V

    invoke-virtual {v0}, Led;->a()Lec;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ljava/util/Locale;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 78
    invoke-static {p0}, Ler;->a(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 462
    new-instance v0, Lee;

    invoke-direct {v0, p0}, Lee;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lee;->b()I

    move-result v0

    return v0
.end method

.method static synthetic b()Lek;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lec;->a:Lek;

    return-object v0
.end method

.method private static c(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 479
    new-instance v0, Lee;

    invoke-direct {v0, p0}, Lee;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lee;->a()I

    move-result v0

    return v0
.end method

.method static synthetic c()Lec;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lec;->e:Lec;

    return-object v0
.end method

.method static synthetic d()Lec;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lec;->d:Lec;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Lek;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v1, 0x1

    .line 403
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p2, p1, v0}, Lek;->a(Ljava/lang/CharSequence;I)Z

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v0, p0, Lec;->g:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    if-eqz v2, :cond_4

    sget-object v0, Lel;->b:Lek;

    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v0, p1, v4}, Lek;->a(Ljava/lang/CharSequence;I)Z

    move-result v0

    iget-boolean v4, p0, Lec;->f:Z

    if-nez v4, :cond_5

    if-nez v0, :cond_0

    invoke-static {p1}, Lec;->c(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v1, :cond_5

    :cond_0
    sget-object v0, Lec;->b:Ljava/lang/String;

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-boolean v0, p0, Lec;->f:Z

    if-eq v2, v0, :cond_9

    if-eqz v2, :cond_8

    const/16 v0, 0x202b

    :goto_3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x202c

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_4
    if-eqz v2, :cond_a

    sget-object v0, Lel;->b:Lek;

    :goto_5
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, p1, v2}, Lek;->a(Ljava/lang/CharSequence;I)Z

    move-result v0

    iget-boolean v2, p0, Lec;->f:Z

    if-nez v2, :cond_b

    if-nez v0, :cond_2

    invoke-static {p1}, Lec;->b(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v1, :cond_b

    :cond_2
    sget-object v0, Lec;->b:Ljava/lang/String;

    :goto_6
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    sget-object v0, Lel;->a:Lek;

    goto :goto_1

    :cond_5
    iget-boolean v4, p0, Lec;->f:Z

    if-eqz v4, :cond_7

    if-eqz v0, :cond_6

    invoke-static {p1}, Lec;->c(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v5, :cond_7

    :cond_6
    sget-object v0, Lec;->c:Ljava/lang/String;

    goto :goto_2

    :cond_7
    const-string v0, ""

    goto :goto_2

    :cond_8
    const/16 v0, 0x202a

    goto :goto_3

    :cond_9
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_a
    sget-object v0, Lel;->a:Lek;

    goto :goto_5

    :cond_b
    iget-boolean v1, p0, Lec;->f:Z

    if-eqz v1, :cond_d

    if-eqz v0, :cond_c

    invoke-static {p1}, Lec;->b(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v5, :cond_d

    :cond_c
    sget-object v0, Lec;->c:Ljava/lang/String;

    goto :goto_6

    :cond_d
    const-string v0, ""

    goto :goto_6
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 343
    iget-object v0, p0, Lec;->h:Lek;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lek;->a(Ljava/lang/CharSequence;I)Z

    move-result v0

    return v0
.end method
