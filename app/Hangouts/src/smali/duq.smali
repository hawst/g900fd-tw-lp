.class public final Lduq;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lduq;


# instance fields
.field public b:Lduo;

.field public c:Ldus;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24191
    const/4 v0, 0x0

    new-array v0, v0, [Lduq;

    sput-object v0, Lduq;->a:[Lduq;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24192
    invoke-direct {p0}, Lepn;-><init>()V

    .line 24195
    iput-object v0, p0, Lduq;->b:Lduo;

    .line 24198
    iput-object v0, p0, Lduq;->c:Ldus;

    .line 24192
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 24215
    const/4 v0, 0x0

    .line 24216
    iget-object v1, p0, Lduq;->b:Lduo;

    if-eqz v1, :cond_0

    .line 24217
    const/4 v0, 0x1

    iget-object v1, p0, Lduq;->b:Lduo;

    .line 24218
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 24220
    :cond_0
    iget-object v1, p0, Lduq;->c:Ldus;

    if-eqz v1, :cond_1

    .line 24221
    const/4 v1, 0x2

    iget-object v2, p0, Lduq;->c:Ldus;

    .line 24222
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24224
    :cond_1
    iget-object v1, p0, Lduq;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24225
    iput v0, p0, Lduq;->cachedSize:I

    .line 24226
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 24188
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lduq;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lduq;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lduq;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lduq;->b:Lduo;

    if-nez v0, :cond_2

    new-instance v0, Lduo;

    invoke-direct {v0}, Lduo;-><init>()V

    iput-object v0, p0, Lduq;->b:Lduo;

    :cond_2
    iget-object v0, p0, Lduq;->b:Lduo;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lduq;->c:Ldus;

    if-nez v0, :cond_3

    new-instance v0, Ldus;

    invoke-direct {v0}, Ldus;-><init>()V

    iput-object v0, p0, Lduq;->c:Ldus;

    :cond_3
    iget-object v0, p0, Lduq;->c:Ldus;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 24203
    iget-object v0, p0, Lduq;->b:Lduo;

    if-eqz v0, :cond_0

    .line 24204
    const/4 v0, 0x1

    iget-object v1, p0, Lduq;->b:Lduo;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 24206
    :cond_0
    iget-object v0, p0, Lduq;->c:Ldus;

    if-eqz v0, :cond_1

    .line 24207
    const/4 v0, 0x2

    iget-object v1, p0, Lduq;->c:Ldus;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 24209
    :cond_1
    iget-object v0, p0, Lduq;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 24211
    return-void
.end method
