.class public final Lauf;
.super Latu;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Latu",
        "<",
        "Lcom/google/android/libraries/hangouts/video/SelfRendererICS;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Latd;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;Lasx;Ljava/lang/Object;IILandroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 26
    invoke-direct/range {p0 .. p8}, Latu;-><init>(Latd;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;Lasx;Ljava/lang/Object;IILandroid/view/ViewGroup;)V

    .line 28
    invoke-virtual {p1}, Latd;->e()Lcom/google/android/libraries/hangouts/video/RendererManager;

    move-result-object v0

    iget-object v1, p0, Lauf;->G:Latz;

    .line 29
    invoke-virtual {p0}, Lauf;->B()Lcom/google/android/libraries/hangouts/video/CameraSpecification;

    move-result-object v2

    .line 28
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/RendererManager;->createSelfRenderer(Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;Lcom/google/android/libraries/hangouts/video/CameraSpecification;)Lcom/google/android/libraries/hangouts/video/SelfRendererICS;

    move-result-object v0

    iput-object v0, p0, Lauf;->H:Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    .line 30
    return-void
.end method


# virtual methods
.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 40
    iget-object v1, p0, Lauf;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 41
    :try_start_0
    invoke-super {p0, p1}, Latu;->onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 42
    iget v0, p0, Lauf;->p:I

    if-ne v0, v2, :cond_0

    iget v0, p0, Lauf;->q:I

    if-ne v0, v2, :cond_0

    iget-boolean v0, p0, Lauf;->k:Z

    if-nez v0, :cond_0

    .line 43
    iget-object v0, p0, Lauf;->o:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    iget-object v0, p0, Lauf;->h:Laui;

    iget-object v2, p0, Lauf;->o:Landroid/graphics/Rect;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2, v3}, Laui;->a(Landroid/graphics/Rect;F)V

    .line 47
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 1

    .prologue
    .line 34
    invoke-super {p0, p1, p2}, Latu;->onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lauf;->J:Z

    .line 36
    return-void
.end method

.method protected z()Latz;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Latu",
            "<",
            "Lcom/google/android/libraries/hangouts/video/SelfRendererICS;",
            ">.atz;"
        }
    .end annotation

    .prologue
    .line 52
    new-instance v0, Laug;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Laug;-><init>(Lauf;B)V

    return-object v0
.end method
