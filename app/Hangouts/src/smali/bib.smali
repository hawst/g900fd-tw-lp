.class public Lbib;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method private constructor <init>(Lddk;)V
    .locals 1

    .prologue
    .line 1238
    iget-object v0, p1, Lddk;->b:Ldcw;

    invoke-direct {p0, v0}, Lbfz;-><init>(Ldcw;)V

    .line 1239
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 4

    .prologue
    .line 1243
    new-instance v0, Lddk;

    invoke-direct {v0}, Lddk;-><init>()V

    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Lddk;

    .line 1245
    iget-object v1, v0, Lddk;->b:Ldcw;

    invoke-static {v1}, Lbib;->a(Ldcw;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1246
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SetChatAclSettingResponse.processResponse: request failed for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1248
    new-instance v1, Lbgk;

    iget-object v0, v0, Lddk;->b:Ldcw;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldcw;)V

    move-object v0, v1

    .line 1250
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbib;

    invoke-direct {v1, v0}, Lbib;-><init>(Lddk;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 3

    .prologue
    .line 1257
    invoke-super {p0, p1, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 1258
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 1259
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SetChatAclSettingResponse.processResponse: success=,account="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1260
    invoke-virtual {p1}, Lyt;->f()Lyj;

    move-result-object v2

    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1259
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1262
    :cond_0
    return-void
.end method
