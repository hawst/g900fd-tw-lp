.class public final Leox;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leox;


# instance fields
.field public b:Lemt;

.field public c:Ljava/lang/String;

.field public d:Leov;

.field public e:[Leov;

.field public f:[Leov;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 318
    const/4 v0, 0x0

    new-array v0, v0, [Leox;

    sput-object v0, Leox;->a:[Leox;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 319
    invoke-direct {p0}, Lepn;-><init>()V

    .line 322
    iput-object v0, p0, Leox;->b:Lemt;

    .line 327
    iput-object v0, p0, Leox;->d:Leov;

    .line 330
    sget-object v0, Leov;->a:[Leov;

    iput-object v0, p0, Leox;->e:[Leov;

    .line 333
    sget-object v0, Leov;->a:[Leov;

    iput-object v0, p0, Leox;->f:[Leov;

    .line 319
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 368
    iget-object v0, p0, Leox;->b:Lemt;

    if-eqz v0, :cond_6

    .line 369
    const/4 v0, 0x1

    iget-object v2, p0, Leox;->b:Lemt;

    .line 370
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 372
    :goto_0
    iget-object v2, p0, Leox;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 373
    const/4 v2, 0x2

    iget-object v3, p0, Leox;->c:Ljava/lang/String;

    .line 374
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 376
    :cond_0
    iget-object v2, p0, Leox;->d:Leov;

    if-eqz v2, :cond_1

    .line 377
    const/4 v2, 0x3

    iget-object v3, p0, Leox;->d:Leov;

    .line 378
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 380
    :cond_1
    iget-object v2, p0, Leox;->e:[Leov;

    if-eqz v2, :cond_3

    .line 381
    iget-object v3, p0, Leox;->e:[Leov;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 382
    if-eqz v5, :cond_2

    .line 383
    const/4 v6, 0x5

    .line 384
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 381
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 388
    :cond_3
    iget-object v2, p0, Leox;->f:[Leov;

    if-eqz v2, :cond_5

    .line 389
    iget-object v2, p0, Leox;->f:[Leov;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 390
    if-eqz v4, :cond_4

    .line 391
    const/4 v5, 0x6

    .line 392
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 389
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 396
    :cond_5
    iget-object v1, p0, Leox;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 397
    iput v0, p0, Leox;->cachedSize:I

    .line 398
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 315
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Leox;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Leox;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Leox;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Leox;->b:Lemt;

    if-nez v0, :cond_2

    new-instance v0, Lemt;

    invoke-direct {v0}, Lemt;-><init>()V

    iput-object v0, p0, Leox;->b:Lemt;

    :cond_2
    iget-object v0, p0, Leox;->b:Lemt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leox;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Leox;->d:Leov;

    if-nez v0, :cond_3

    new-instance v0, Leov;

    invoke-direct {v0}, Leov;-><init>()V

    iput-object v0, p0, Leox;->d:Leov;

    :cond_3
    iget-object v0, p0, Leox;->d:Leov;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leox;->e:[Leov;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leov;

    iget-object v3, p0, Leox;->e:[Leov;

    if-eqz v3, :cond_4

    iget-object v3, p0, Leox;->e:[Leov;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Leox;->e:[Leov;

    :goto_2
    iget-object v2, p0, Leox;->e:[Leov;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Leox;->e:[Leov;

    new-instance v3, Leov;

    invoke-direct {v3}, Leov;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leox;->e:[Leov;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Leox;->e:[Leov;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Leox;->e:[Leov;

    new-instance v3, Leov;

    invoke-direct {v3}, Leov;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leox;->e:[Leov;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leox;->f:[Leov;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Leov;

    iget-object v3, p0, Leox;->f:[Leov;

    if-eqz v3, :cond_7

    iget-object v3, p0, Leox;->f:[Leov;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Leox;->f:[Leov;

    :goto_4
    iget-object v2, p0, Leox;->f:[Leov;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Leox;->f:[Leov;

    new-instance v3, Leov;

    invoke-direct {v3}, Leov;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leox;->f:[Leov;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    iget-object v0, p0, Leox;->f:[Leov;

    array-length v0, v0

    goto :goto_3

    :cond_9
    iget-object v2, p0, Leox;->f:[Leov;

    new-instance v3, Leov;

    invoke-direct {v3}, Leov;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leox;->f:[Leov;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 338
    iget-object v1, p0, Leox;->b:Lemt;

    if-eqz v1, :cond_0

    .line 339
    const/4 v1, 0x1

    iget-object v2, p0, Leox;->b:Lemt;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 341
    :cond_0
    iget-object v1, p0, Leox;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 342
    const/4 v1, 0x2

    iget-object v2, p0, Leox;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 344
    :cond_1
    iget-object v1, p0, Leox;->d:Leov;

    if-eqz v1, :cond_2

    .line 345
    const/4 v1, 0x3

    iget-object v2, p0, Leox;->d:Leov;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 347
    :cond_2
    iget-object v1, p0, Leox;->e:[Leov;

    if-eqz v1, :cond_4

    .line 348
    iget-object v2, p0, Leox;->e:[Leov;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 349
    if-eqz v4, :cond_3

    .line 350
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 348
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 354
    :cond_4
    iget-object v1, p0, Leox;->f:[Leov;

    if-eqz v1, :cond_6

    .line 355
    iget-object v1, p0, Leox;->f:[Leov;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 356
    if-eqz v3, :cond_5

    .line 357
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 355
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 361
    :cond_6
    iget-object v0, p0, Leox;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 363
    return-void
.end method
