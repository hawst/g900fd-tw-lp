.class public final Lepy;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lepy;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 770
    const/4 v0, 0x0

    new-array v0, v0, [Lepy;

    sput-object v0, Lepy;->a:[Lepy;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 771
    invoke-direct {p0}, Lepn;-><init>()V

    .line 779
    const/4 v0, 0x0

    iput-object v0, p0, Lepy;->b:Ljava/lang/Integer;

    .line 771
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 808
    const/4 v0, 0x0

    .line 809
    iget-object v1, p0, Lepy;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 810
    const/4 v0, 0x1

    iget-object v1, p0, Lepy;->b:Ljava/lang/Integer;

    .line 811
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 813
    :cond_0
    iget-object v1, p0, Lepy;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 814
    const/4 v1, 0x2

    iget-object v2, p0, Lepy;->c:Ljava/lang/String;

    .line 815
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 817
    :cond_1
    iget-object v1, p0, Lepy;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 818
    const/4 v1, 0x3

    iget-object v2, p0, Lepy;->d:Ljava/lang/String;

    .line 819
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 821
    :cond_2
    iget-object v1, p0, Lepy;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 822
    const/4 v1, 0x4

    iget-object v2, p0, Lepy;->e:Ljava/lang/String;

    .line 823
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 825
    :cond_3
    iget-object v1, p0, Lepy;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 826
    iput v0, p0, Lepy;->cachedSize:I

    .line 827
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 767
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lepy;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lepy;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lepy;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lepy;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lepy;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepy;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepy;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepy;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 790
    iget-object v0, p0, Lepy;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 791
    const/4 v0, 0x1

    iget-object v1, p0, Lepy;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 793
    :cond_0
    iget-object v0, p0, Lepy;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 794
    const/4 v0, 0x2

    iget-object v1, p0, Lepy;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 796
    :cond_1
    iget-object v0, p0, Lepy;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 797
    const/4 v0, 0x3

    iget-object v1, p0, Lepy;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 799
    :cond_2
    iget-object v0, p0, Lepy;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 800
    const/4 v0, 0x4

    iget-object v1, p0, Lepy;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 802
    :cond_3
    iget-object v0, p0, Lepy;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 804
    return-void
.end method
