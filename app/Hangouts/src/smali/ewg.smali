.class public final Lewg;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lewg;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Levt;

.field public e:[Ljava/lang/String;

.field public f:Leuf;

.field public g:Leuf;

.field public h:Leuf;

.field public i:[I

.field public j:Ljava/lang/Integer;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/Boolean;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lewg;

    sput-object v0, Lewg;->a:[Lewg;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 22
    iput-object v1, p0, Lewg;->d:Levt;

    .line 25
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Lewg;->e:[Ljava/lang/String;

    .line 28
    iput-object v1, p0, Lewg;->f:Leuf;

    .line 31
    iput-object v1, p0, Lewg;->g:Leuf;

    .line 34
    iput-object v1, p0, Lewg;->h:Leuf;

    .line 37
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Lewg;->i:[I

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 122
    iget-object v0, p0, Lewg;->b:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 123
    const/4 v0, 0x1

    iget-object v2, p0, Lewg;->b:Ljava/lang/String;

    .line 124
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 126
    :goto_0
    iget-object v2, p0, Lewg;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 127
    const/4 v2, 0x2

    iget-object v3, p0, Lewg;->c:Ljava/lang/String;

    .line 128
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 130
    :cond_0
    iget-object v2, p0, Lewg;->d:Levt;

    if-eqz v2, :cond_1

    .line 131
    const/4 v2, 0x3

    iget-object v3, p0, Lewg;->d:Levt;

    .line 132
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 134
    :cond_1
    iget-object v2, p0, Lewg;->e:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lewg;->e:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 136
    iget-object v4, p0, Lewg;->e:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_2

    aget-object v6, v4, v2

    .line 138
    invoke-static {v6}, Lepl;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 136
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 140
    :cond_2
    add-int/2addr v0, v3

    .line 141
    iget-object v2, p0, Lewg;->e:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 143
    :cond_3
    iget-object v2, p0, Lewg;->f:Leuf;

    if-eqz v2, :cond_4

    .line 144
    const/4 v2, 0x5

    iget-object v3, p0, Lewg;->f:Leuf;

    .line 145
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 147
    :cond_4
    iget-object v2, p0, Lewg;->g:Leuf;

    if-eqz v2, :cond_5

    .line 148
    const/4 v2, 0x6

    iget-object v3, p0, Lewg;->g:Leuf;

    .line 149
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 151
    :cond_5
    iget-object v2, p0, Lewg;->h:Leuf;

    if-eqz v2, :cond_6

    .line 152
    const/4 v2, 0x7

    iget-object v3, p0, Lewg;->h:Leuf;

    .line 153
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 155
    :cond_6
    iget-object v2, p0, Lewg;->i:[I

    if-eqz v2, :cond_8

    iget-object v2, p0, Lewg;->i:[I

    array-length v2, v2

    if-lez v2, :cond_8

    .line 157
    iget-object v3, p0, Lewg;->i:[I

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_7

    aget v5, v3, v1

    .line 159
    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 157
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 161
    :cond_7
    add-int/2addr v0, v2

    .line 162
    iget-object v1, p0, Lewg;->i:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 164
    :cond_8
    iget-object v1, p0, Lewg;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 165
    const/16 v1, 0x9

    iget-object v2, p0, Lewg;->j:Ljava/lang/Integer;

    .line 166
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 168
    :cond_9
    iget-object v1, p0, Lewg;->k:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 169
    const/16 v1, 0xa

    iget-object v2, p0, Lewg;->k:Ljava/lang/String;

    .line 170
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    :cond_a
    iget-object v1, p0, Lewg;->l:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 173
    const/16 v1, 0xb

    iget-object v2, p0, Lewg;->l:Ljava/lang/String;

    .line 174
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 176
    :cond_b
    iget-object v1, p0, Lewg;->m:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 177
    const/16 v1, 0xc

    iget-object v2, p0, Lewg;->m:Ljava/lang/String;

    .line 178
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 180
    :cond_c
    iget-object v1, p0, Lewg;->n:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    .line 181
    const/16 v1, 0xd

    iget-object v2, p0, Lewg;->n:Ljava/lang/Boolean;

    .line 182
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 184
    :cond_d
    iget-object v1, p0, Lewg;->o:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 185
    const/16 v1, 0xe

    iget-object v2, p0, Lewg;->o:Ljava/lang/String;

    .line 186
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 188
    :cond_e
    iget-object v1, p0, Lewg;->p:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 189
    const/16 v1, 0xf

    iget-object v2, p0, Lewg;->p:Ljava/lang/String;

    .line 190
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 192
    :cond_f
    iget-object v1, p0, Lewg;->q:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 193
    const/16 v1, 0x10

    iget-object v2, p0, Lewg;->q:Ljava/lang/String;

    .line 194
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 196
    :cond_10
    iget-object v1, p0, Lewg;->r:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    .line 197
    const/16 v1, 0x11

    iget-object v2, p0, Lewg;->r:Ljava/lang/Boolean;

    .line 198
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 200
    :cond_11
    iget-object v1, p0, Lewg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 201
    iput v0, p0, Lewg;->cachedSize:I

    .line 202
    return v0

    :cond_12
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lewg;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lewg;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lewg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewg;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewg;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lewg;->d:Levt;

    if-nez v0, :cond_2

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Lewg;->d:Levt;

    :cond_2
    iget-object v0, p0, Lewg;->d:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Lewg;->e:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lewg;->e:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lewg;->e:[Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Lewg;->e:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lewg;->e:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lewg;->e:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lewg;->f:Leuf;

    if-nez v0, :cond_4

    new-instance v0, Leuf;

    invoke-direct {v0}, Leuf;-><init>()V

    iput-object v0, p0, Lewg;->f:Leuf;

    :cond_4
    iget-object v0, p0, Lewg;->f:Leuf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lewg;->g:Leuf;

    if-nez v0, :cond_5

    new-instance v0, Leuf;

    invoke-direct {v0}, Leuf;-><init>()V

    iput-object v0, p0, Lewg;->g:Leuf;

    :cond_5
    iget-object v0, p0, Lewg;->g:Leuf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lewg;->h:Leuf;

    if-nez v0, :cond_6

    new-instance v0, Leuf;

    invoke-direct {v0}, Leuf;-><init>()V

    iput-object v0, p0, Lewg;->h:Leuf;

    :cond_6
    iget-object v0, p0, Lewg;->h:Leuf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x40

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Lewg;->i:[I

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [I

    iget-object v2, p0, Lewg;->i:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lewg;->i:[I

    :goto_2
    iget-object v1, p0, Lewg;->i:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_7

    iget-object v1, p0, Lewg;->i:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    iget-object v1, p0, Lewg;->i:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lewg;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewg;->k:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewg;->l:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewg;->m:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lewg;->n:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewg;->o:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewg;->p:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewg;->q:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lewg;->r:Ljava/lang/Boolean;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x88 -> :sswitch_11
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 60
    iget-object v1, p0, Lewg;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 61
    const/4 v1, 0x1

    iget-object v2, p0, Lewg;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 63
    :cond_0
    iget-object v1, p0, Lewg;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 64
    const/4 v1, 0x2

    iget-object v2, p0, Lewg;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 66
    :cond_1
    iget-object v1, p0, Lewg;->d:Levt;

    if-eqz v1, :cond_2

    .line 67
    const/4 v1, 0x3

    iget-object v2, p0, Lewg;->d:Levt;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 69
    :cond_2
    iget-object v1, p0, Lewg;->e:[Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 70
    iget-object v2, p0, Lewg;->e:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 71
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 70
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 74
    :cond_3
    iget-object v1, p0, Lewg;->f:Leuf;

    if-eqz v1, :cond_4

    .line 75
    const/4 v1, 0x5

    iget-object v2, p0, Lewg;->f:Leuf;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 77
    :cond_4
    iget-object v1, p0, Lewg;->g:Leuf;

    if-eqz v1, :cond_5

    .line 78
    const/4 v1, 0x6

    iget-object v2, p0, Lewg;->g:Leuf;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 80
    :cond_5
    iget-object v1, p0, Lewg;->h:Leuf;

    if-eqz v1, :cond_6

    .line 81
    const/4 v1, 0x7

    iget-object v2, p0, Lewg;->h:Leuf;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 83
    :cond_6
    iget-object v1, p0, Lewg;->i:[I

    if-eqz v1, :cond_7

    iget-object v1, p0, Lewg;->i:[I

    array-length v1, v1

    if-lez v1, :cond_7

    .line 84
    iget-object v1, p0, Lewg;->i:[I

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_7

    aget v3, v1, v0

    .line 85
    const/16 v4, 0x8

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 84
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 88
    :cond_7
    iget-object v0, p0, Lewg;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 89
    const/16 v0, 0x9

    iget-object v1, p0, Lewg;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 91
    :cond_8
    iget-object v0, p0, Lewg;->k:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 92
    const/16 v0, 0xa

    iget-object v1, p0, Lewg;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 94
    :cond_9
    iget-object v0, p0, Lewg;->l:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 95
    const/16 v0, 0xb

    iget-object v1, p0, Lewg;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 97
    :cond_a
    iget-object v0, p0, Lewg;->m:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 98
    const/16 v0, 0xc

    iget-object v1, p0, Lewg;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 100
    :cond_b
    iget-object v0, p0, Lewg;->n:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    .line 101
    const/16 v0, 0xd

    iget-object v1, p0, Lewg;->n:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 103
    :cond_c
    iget-object v0, p0, Lewg;->o:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 104
    const/16 v0, 0xe

    iget-object v1, p0, Lewg;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 106
    :cond_d
    iget-object v0, p0, Lewg;->p:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 107
    const/16 v0, 0xf

    iget-object v1, p0, Lewg;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 109
    :cond_e
    iget-object v0, p0, Lewg;->q:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 110
    const/16 v0, 0x10

    iget-object v1, p0, Lewg;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 112
    :cond_f
    iget-object v0, p0, Lewg;->r:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    .line 113
    const/16 v0, 0x11

    iget-object v1, p0, Lewg;->r:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 115
    :cond_10
    iget-object v0, p0, Lewg;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 117
    return-void
.end method
