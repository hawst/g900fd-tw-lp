.class final Lbuq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic a:Lbup;


# direct methods
.method constructor <init>(Lbup;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lbuq;->a:Lbup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 97
    check-cast p2, Ljava/lang/String;

    .line 101
    iget-object v0, p0, Lbuq;->a:Lbup;

    iget v0, v0, Lbup;->b:I

    if-ne v0, v2, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbuq;->a:Lbup;

    iget-object v0, v0, Lbup;->c:Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;

    .line 102
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 103
    iget-object v0, p0, Lbuq;->a:Lbup;

    iget-object v0, v0, Lbup;->a:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x63f

    .line 104
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 103
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 107
    :cond_0
    iget-object v0, p0, Lbuq;->a:Lbup;

    iget-object v0, v0, Lbup;->c:Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->a(Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lbuq;->a:Lbup;

    iput-boolean v2, v0, Lbup;->d:Z

    .line 109
    return v2
.end method
