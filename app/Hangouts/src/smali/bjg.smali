.class public Lbjg;
.super Lbis;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field public final g:I

.field public final h:I

.field public final i:J

.field public final j:I

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;

.field public final m:Ljava/lang/String;

.field public final n:J

.field public final o:Z

.field public final p:Z

.field public final q:Z

.field public final r:I

.field public final s:Ljava/lang/String;

.field public final t:[B


# direct methods
.method public constructor <init>(Ldrr;IJ)V
    .locals 8

    .prologue
    const/16 v4, 0xa

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 131
    iget-object v0, p1, Ldrr;->b:Ldqf;

    iget-object v0, v0, Ldqf;->b:Ljava/lang/String;

    new-instance v1, Lbdk;

    iget-object v5, p1, Ldrr;->c:Ldui;

    invoke-direct {v1, v5, v7}, Lbdk;-><init>(Ldui;Ljava/lang/String;)V

    iget-object v5, p1, Ldrr;->d:Ljava/lang/Long;

    .line 133
    invoke-static {v5}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v5

    .line 131
    invoke-direct {p0, v0, v1, v5, v6}, Lbis;-><init>(Ljava/lang/String;Lbdk;J)V

    .line 134
    iput p2, p0, Lbjg;->h:I

    .line 135
    iput-wide p3, p0, Lbjg;->i:J

    .line 136
    iget-object v0, p1, Ldrr;->g:Ljava/lang/Integer;

    invoke-static {v0, v2}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    iput v0, p0, Lbjg;->j:I

    .line 137
    iget-object v0, p1, Ldrr;->e:Ljava/lang/String;

    iput-object v0, p0, Lbjg;->m:Ljava/lang/String;

    .line 138
    iget-object v0, p1, Ldrr;->o:Ljava/lang/Long;

    .line 139
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    const-wide/16 v5, 0x0

    cmp-long v0, v0, v5

    if-nez v0, :cond_2

    const-wide/16 v0, -0x1

    .line 140
    :goto_0
    iput-wide v0, p0, Lbjg;->n:J

    .line 141
    iget-object v0, p1, Ldrr;->p:Ljava/lang/Boolean;

    invoke-static {v0, v2}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbjg;->o:Z

    .line 143
    iget-object v0, p1, Ldrr;->r:Ljava/lang/Boolean;

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbjg;->q:Z

    .line 145
    iget-boolean v0, p0, Lbjg;->q:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Ldrr;->q:Ljava/lang/Integer;

    invoke-static {v0, v2}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    if-ne v0, v3, :cond_3

    :cond_0
    move v0, v3

    :goto_1
    iput-boolean v0, p0, Lbjg;->p:Z

    .line 148
    iget-object v0, p1, Ldrr;->f:Ldrs;

    if-eqz v0, :cond_8

    .line 149
    iget-object v1, p1, Ldrr;->f:Ldrs;

    .line 150
    iget-object v0, v1, Ldrs;->b:Ldui;

    iget-object v0, v0, Ldui;->c:Ljava/lang/String;

    iput-object v0, p0, Lbjg;->k:Ljava/lang/String;

    .line 151
    iget-object v0, v1, Ldrs;->c:Ljava/lang/Long;

    .line 153
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v5

    .line 152
    invoke-static {v5, v6}, Lyt;->a(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbjg;->l:Ljava/lang/String;

    .line 155
    const/4 v0, 0x2

    if-ne p2, v0, :cond_4

    const-string v0, "Babel"

    const-string v2, ">>> Other client is active, downgrade notification"

    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v4

    .line 156
    :goto_2
    iget-boolean v2, p0, Lbjg;->o:Z

    if-nez v2, :cond_1

    .line 157
    if-eq v0, v4, :cond_1

    .line 158
    const-string v2, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " event received hasAdvancesSortTimestamp = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p1, Ldrr;->p:Ljava/lang/Boolean;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " advanceSortTimestamp = false, but notificationLevel = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " convId = "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v5, p1, Ldrr;->b:Ldqf;

    iget-object v5, v5, Ldqf;->b:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " timestamp = "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v5, p1, Ldrr;->d:Ljava/lang/Long;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v4

    .line 168
    :cond_1
    iput v0, p0, Lbjg;->g:I

    .line 170
    iget-object v0, v1, Ldrs;->e:Ldxe;

    if-eqz v0, :cond_5

    .line 171
    iget-object v0, v1, Ldrs;->e:Ldxe;

    .line 172
    invoke-static {v0}, Lepr;->toByteArray(Lepr;)[B

    move-result-object v0

    iput-object v0, p0, Lbjg;->t:[B

    .line 176
    :goto_3
    iget-object v0, p1, Ldrr;->s:Ldqw;

    .line 177
    if-eqz v0, :cond_7

    .line 178
    iget-object v1, v0, Ldqw;->b:Ljava/lang/Integer;

    invoke-static {v1, v3}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v1

    iput v1, p0, Lbjg;->r:I

    .line 180
    iget-object v1, v0, Ldqw;->c:Leir;

    if-eqz v1, :cond_6

    .line 181
    iget-object v0, v0, Ldqw;->c:Leir;

    iget-object v0, v0, Leir;->b:Ljava/lang/String;

    iput-object v0, p0, Lbjg;->s:Ljava/lang/String;

    .line 197
    :goto_4
    return-void

    .line 139
    :cond_2
    iget-object v0, p1, Ldrr;->o:Ljava/lang/Long;

    .line 140
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 145
    goto/16 :goto_1

    .line 155
    :cond_4
    iget-object v0, v1, Ldrs;->d:Ljava/lang/Integer;

    invoke-static {v0, v2}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    goto/16 :goto_2

    .line 174
    :cond_5
    iput-object v7, p0, Lbjg;->t:[B

    goto :goto_3

    .line 183
    :cond_6
    iput-object v7, p0, Lbjg;->s:Ljava/lang/String;

    goto :goto_4

    .line 186
    :cond_7
    iput v3, p0, Lbjg;->r:I

    .line 187
    iput-object v7, p0, Lbjg;->s:Ljava/lang/String;

    goto :goto_4

    .line 190
    :cond_8
    iput-object v7, p0, Lbjg;->k:Ljava/lang/String;

    .line 191
    iput-object v7, p0, Lbjg;->l:Ljava/lang/String;

    .line 192
    iput v4, p0, Lbjg;->g:I

    .line 193
    iput-object v7, p0, Lbjg;->t:[B

    .line 194
    iput v3, p0, Lbjg;->r:I

    .line 195
    iput-object v7, p0, Lbjg;->s:Ljava/lang/String;

    goto :goto_4
.end method

.method protected constructor <init>(Ljava/lang/String;Lbdk;JLjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 201
    invoke-direct {p0, p1, p2, p3, p4}, Lbis;-><init>(Ljava/lang/String;Lbdk;J)V

    .line 202
    iput v4, p0, Lbjg;->j:I

    .line 203
    iput-object p5, p0, Lbjg;->m:Ljava/lang/String;

    .line 204
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbjg;->n:J

    .line 205
    iput-object v2, p0, Lbjg;->k:Ljava/lang/String;

    .line 206
    iput-object v2, p0, Lbjg;->l:Ljava/lang/String;

    .line 207
    const/16 v0, 0xa

    iput v0, p0, Lbjg;->g:I

    .line 208
    iput v3, p0, Lbjg;->h:I

    .line 209
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbjg;->i:J

    .line 210
    iput-boolean v3, p0, Lbjg;->o:Z

    .line 211
    iput-boolean v3, p0, Lbjg;->p:Z

    .line 212
    iput-boolean v4, p0, Lbjg;->q:Z

    .line 213
    iput-object v2, p0, Lbjg;->t:[B

    .line 214
    iput v4, p0, Lbjg;->r:I

    .line 215
    iput-object v2, p0, Lbjg;->s:Ljava/lang/String;

    .line 216
    return-void
.end method

.method public static a(Ldrr;IJ)Lbjg;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 246
    iget-object v0, p0, Ldrr;->h:Ldpq;

    if-eqz v0, :cond_0

    .line 247
    new-instance v0, Lbiv;

    iget-object v5, p0, Ldrr;->h:Ldpq;

    move-object v1, p0

    move v2, p1

    move-wide v3, p2

    invoke-direct/range {v0 .. v5}, Lbiv;-><init>(Ldrr;IJLdpq;)V

    .line 263
    :goto_0
    return-object v0

    .line 249
    :cond_0
    iget-object v0, p0, Ldrr;->i:Ldtw;

    if-eqz v0, :cond_1

    .line 250
    new-instance v0, Lbji;

    iget-object v5, p0, Ldrr;->i:Ldtw;

    move-object v1, p0

    move v2, p1

    move-wide v3, p2

    invoke-direct/range {v0 .. v6}, Lbji;-><init>(Ldrr;IJLdtw;B)V

    goto :goto_0

    .line 252
    :cond_1
    iget-object v0, p0, Ldrr;->j:Ldqi;

    if-eqz v0, :cond_2

    .line 253
    new-instance v0, Lbjd;

    iget-object v5, p0, Ldrr;->j:Ldqi;

    move-object v1, p0

    move v2, p1

    move-wide v3, p2

    invoke-direct/range {v0 .. v6}, Lbjd;-><init>(Ldrr;IJLdqi;B)V

    goto :goto_0

    .line 255
    :cond_2
    iget-object v0, p0, Ldrr;->k:Ldsw;

    if-eqz v0, :cond_3

    .line 256
    new-instance v0, Lbjh;

    iget-object v5, p0, Ldrr;->k:Ldsw;

    move-object v1, p0

    move v2, p1

    move-wide v3, p2

    invoke-direct/range {v0 .. v5}, Lbjh;-><init>(Ldrr;IJLdsw;)V

    goto :goto_0

    .line 258
    :cond_3
    iget-object v0, p0, Ldrr;->l:Lduh;

    if-eqz v0, :cond_4

    .line 259
    new-instance v0, Lbjk;

    iget-object v5, p0, Ldrr;->l:Lduh;

    move-object v1, p0

    move v2, p1

    move-wide v3, p2

    invoke-direct/range {v0 .. v5}, Lbjk;-><init>(Ldrr;IJLduh;)V

    goto :goto_0

    .line 262
    :cond_4
    const-string v0, "Babel"

    const-string v1, "Received Empty Event. Probably not implemented by server yet."

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a([Ldrr;ZJ)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ldrr;",
            "ZJ)",
            "Ljava/util/List",
            "<",
            "Lbjg;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 276
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 277
    array-length v4, p0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, p0, v2

    .line 280
    if-eqz p1, :cond_1

    const/4 v0, 0x2

    :goto_1
    invoke-static {v5, v0, p2, p3}, Lbjg;->a(Ldrr;IJ)Lbjg;

    move-result-object v0

    .line 285
    if-eqz v0, :cond_0

    .line 286
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 277
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 280
    goto :goto_1

    .line 289
    :cond_2
    return-object v3
.end method


# virtual methods
.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbdk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 227
    const/4 v0, 0x0

    return-object v0
.end method
