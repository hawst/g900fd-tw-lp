.class public final Laiz;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Laja;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 444
    iput-object p1, p0, Laiz;->b:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    .line 445
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 446
    iput-object p2, p0, Laiz;->a:Ljava/util/ArrayList;

    .line 447
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Laiz;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Laiz;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 503
    neg-int v0, p1

    int-to-long v0, v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 451
    iget-object v0, p0, Laiz;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    .line 453
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v1

    sget v2, Lg;->aT:I

    if-ne v1, v2, :cond_0

    .line 459
    :goto_0
    iput-object p2, v0, Laja;->h:Landroid/view/View;

    .line 460
    sget v1, Lg;->hz:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 461
    sget v2, Lg;->hi:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 462
    sget v3, Lg;->du:I

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 463
    sget v4, Lg;->al:I

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 464
    iget-object v5, v0, Laja;->a:Ljava/lang/String;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 465
    iget-object v1, v0, Laja;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 466
    const-string v1, ""

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 467
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 472
    :goto_1
    iget v1, v0, Laja;->c:I

    if-nez v1, :cond_2

    .line 473
    iget v1, v0, Laja;->c:I

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 474
    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 479
    :goto_2
    iget-boolean v1, v0, Laja;->d:Z

    if-eqz v1, :cond_3

    .line 480
    invoke-virtual {v4, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 481
    iget-boolean v1, v0, Laja;->e:Z

    invoke-virtual {v4, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 485
    :goto_3
    invoke-virtual {v0}, Laja;->a()V

    .line 486
    iget v0, v0, Laja;->g:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 487
    iget-object v0, p0, Laiz;->b:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 488
    return-object p2

    .line 456
    :cond_0
    iget-object v1, p0, Laiz;->b:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getActivity()Ly;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lf;->eM:I

    invoke-virtual {v1, v2, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 469
    :cond_1
    iget-object v1, v0, Laja;->b:Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 470
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 476
    :cond_2
    iget v1, v0, Laja;->c:I

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 477
    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 483
    :cond_3
    invoke-virtual {v4, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_3
.end method
