.class public final Ldiq;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldiq;


# instance fields
.field public A:Ljava/lang/Integer;

.field public B:[Lepu;

.field public C:[Ljava/lang/String;

.field public D:[I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:[Ldis;

.field public f:[Ldis;

.field public g:[Ljava/lang/String;

.field public h:Ljava/lang/Integer;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/Boolean;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/Boolean;

.field public o:Ldir;

.field public p:Ljava/lang/Boolean;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/Integer;

.field public t:Ljava/lang/Boolean;

.field public u:Ljava/lang/Boolean;

.field public v:Ljava/lang/String;

.field public w:Ljava/lang/String;

.field public x:Ljava/lang/Boolean;

.field public y:[Leqk;

.field public z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    new-array v0, v0, [Ldiq;

    sput-object v0, Ldiq;->a:[Ldiq;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 74
    invoke-direct {p0}, Lepn;-><init>()V

    .line 194
    sget-object v0, Ldis;->a:[Ldis;

    iput-object v0, p0, Ldiq;->e:[Ldis;

    .line 197
    sget-object v0, Ldis;->a:[Ldis;

    iput-object v0, p0, Ldiq;->f:[Ldis;

    .line 200
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldiq;->g:[Ljava/lang/String;

    .line 203
    iput-object v1, p0, Ldiq;->h:Ljava/lang/Integer;

    .line 218
    iput-object v1, p0, Ldiq;->o:Ldir;

    .line 227
    iput-object v1, p0, Ldiq;->s:Ljava/lang/Integer;

    .line 240
    sget-object v0, Leqk;->a:[Leqk;

    iput-object v0, p0, Ldiq;->y:[Leqk;

    .line 247
    sget-object v0, Lepu;->a:[Lepu;

    iput-object v0, p0, Ldiq;->B:[Lepu;

    .line 250
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldiq;->C:[Ljava/lang/String;

    .line 253
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Ldiq;->D:[I

    .line 74
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 369
    const/4 v0, 0x1

    iget-object v2, p0, Ldiq;->b:Ljava/lang/String;

    .line 371
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 372
    const/4 v2, 0x2

    iget-object v3, p0, Ldiq;->c:Ljava/lang/String;

    .line 373
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 374
    iget-object v2, p0, Ldiq;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 375
    const/4 v2, 0x3

    iget-object v3, p0, Ldiq;->d:Ljava/lang/String;

    .line 376
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 378
    :cond_0
    iget-object v2, p0, Ldiq;->e:[Ldis;

    if-eqz v2, :cond_2

    .line 379
    iget-object v3, p0, Ldiq;->e:[Ldis;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 380
    if-eqz v5, :cond_1

    .line 381
    const/4 v6, 0x4

    .line 382
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 379
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 386
    :cond_2
    iget-object v2, p0, Ldiq;->f:[Ldis;

    if-eqz v2, :cond_4

    .line 387
    iget-object v3, p0, Ldiq;->f:[Ldis;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 388
    if-eqz v5, :cond_3

    .line 389
    const/4 v6, 0x5

    .line 390
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 387
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 394
    :cond_4
    iget-object v2, p0, Ldiq;->g:[Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Ldiq;->g:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 396
    iget-object v4, p0, Ldiq;->g:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_5

    aget-object v6, v4, v2

    .line 398
    invoke-static {v6}, Lepl;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 396
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 400
    :cond_5
    add-int/2addr v0, v3

    .line 401
    iget-object v2, p0, Ldiq;->g:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 403
    :cond_6
    iget-object v2, p0, Ldiq;->h:Ljava/lang/Integer;

    if-eqz v2, :cond_7

    .line 404
    const/4 v2, 0x7

    iget-object v3, p0, Ldiq;->h:Ljava/lang/Integer;

    .line 405
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 407
    :cond_7
    iget-object v2, p0, Ldiq;->i:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 408
    const/16 v2, 0x8

    iget-object v3, p0, Ldiq;->i:Ljava/lang/String;

    .line 409
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 411
    :cond_8
    iget-object v2, p0, Ldiq;->j:Ljava/lang/Boolean;

    if-eqz v2, :cond_9

    .line 412
    const/16 v2, 0x9

    iget-object v3, p0, Ldiq;->j:Ljava/lang/Boolean;

    .line 413
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 415
    :cond_9
    iget-object v2, p0, Ldiq;->k:Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 416
    const/16 v2, 0xa

    iget-object v3, p0, Ldiq;->k:Ljava/lang/String;

    .line 417
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 419
    :cond_a
    iget-object v2, p0, Ldiq;->l:Ljava/lang/String;

    if-eqz v2, :cond_b

    .line 420
    const/16 v2, 0xb

    iget-object v3, p0, Ldiq;->l:Ljava/lang/String;

    .line 421
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 423
    :cond_b
    iget-object v2, p0, Ldiq;->m:Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 424
    const/16 v2, 0xc

    iget-object v3, p0, Ldiq;->m:Ljava/lang/String;

    .line 425
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 427
    :cond_c
    iget-object v2, p0, Ldiq;->n:Ljava/lang/Boolean;

    if-eqz v2, :cond_d

    .line 428
    const/16 v2, 0xd

    iget-object v3, p0, Ldiq;->n:Ljava/lang/Boolean;

    .line 429
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 431
    :cond_d
    iget-object v2, p0, Ldiq;->o:Ldir;

    if-eqz v2, :cond_e

    .line 432
    const/16 v2, 0xe

    iget-object v3, p0, Ldiq;->o:Ldir;

    .line 433
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 435
    :cond_e
    iget-object v2, p0, Ldiq;->p:Ljava/lang/Boolean;

    if-eqz v2, :cond_f

    .line 436
    const/16 v2, 0xf

    iget-object v3, p0, Ldiq;->p:Ljava/lang/Boolean;

    .line 437
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 439
    :cond_f
    iget-object v2, p0, Ldiq;->q:Ljava/lang/String;

    if-eqz v2, :cond_10

    .line 440
    const/16 v2, 0x10

    iget-object v3, p0, Ldiq;->q:Ljava/lang/String;

    .line 441
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 443
    :cond_10
    iget-object v2, p0, Ldiq;->r:Ljava/lang/String;

    if-eqz v2, :cond_11

    .line 444
    const/16 v2, 0x11

    iget-object v3, p0, Ldiq;->r:Ljava/lang/String;

    .line 445
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 447
    :cond_11
    iget-object v2, p0, Ldiq;->s:Ljava/lang/Integer;

    if-eqz v2, :cond_12

    .line 448
    const/16 v2, 0x12

    iget-object v3, p0, Ldiq;->s:Ljava/lang/Integer;

    .line 449
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 451
    :cond_12
    iget-object v2, p0, Ldiq;->t:Ljava/lang/Boolean;

    if-eqz v2, :cond_13

    .line 452
    const/16 v2, 0x13

    iget-object v3, p0, Ldiq;->t:Ljava/lang/Boolean;

    .line 453
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 455
    :cond_13
    iget-object v2, p0, Ldiq;->u:Ljava/lang/Boolean;

    if-eqz v2, :cond_14

    .line 456
    const/16 v2, 0x14

    iget-object v3, p0, Ldiq;->u:Ljava/lang/Boolean;

    .line 457
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 459
    :cond_14
    iget-object v2, p0, Ldiq;->v:Ljava/lang/String;

    if-eqz v2, :cond_15

    .line 460
    const/16 v2, 0x15

    iget-object v3, p0, Ldiq;->v:Ljava/lang/String;

    .line 461
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 463
    :cond_15
    iget-object v2, p0, Ldiq;->w:Ljava/lang/String;

    if-eqz v2, :cond_16

    .line 464
    const/16 v2, 0x16

    iget-object v3, p0, Ldiq;->w:Ljava/lang/String;

    .line 465
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 467
    :cond_16
    iget-object v2, p0, Ldiq;->x:Ljava/lang/Boolean;

    if-eqz v2, :cond_17

    .line 468
    const/16 v2, 0x17

    iget-object v3, p0, Ldiq;->x:Ljava/lang/Boolean;

    .line 469
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 471
    :cond_17
    iget-object v2, p0, Ldiq;->y:[Leqk;

    if-eqz v2, :cond_19

    .line 472
    iget-object v3, p0, Ldiq;->y:[Leqk;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_19

    aget-object v5, v3, v2

    .line 473
    if-eqz v5, :cond_18

    .line 474
    const/16 v6, 0x18

    .line 475
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 472
    :cond_18
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 479
    :cond_19
    iget-object v2, p0, Ldiq;->z:Ljava/lang/String;

    if-eqz v2, :cond_1a

    .line 480
    const/16 v2, 0x19

    iget-object v3, p0, Ldiq;->z:Ljava/lang/String;

    .line 481
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 483
    :cond_1a
    iget-object v2, p0, Ldiq;->A:Ljava/lang/Integer;

    if-eqz v2, :cond_1b

    .line 484
    const/16 v2, 0x1a

    iget-object v3, p0, Ldiq;->A:Ljava/lang/Integer;

    .line 485
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 487
    :cond_1b
    iget-object v2, p0, Ldiq;->B:[Lepu;

    if-eqz v2, :cond_1d

    .line 488
    iget-object v3, p0, Ldiq;->B:[Lepu;

    array-length v4, v3

    move v2, v1

    :goto_4
    if-ge v2, v4, :cond_1d

    aget-object v5, v3, v2

    .line 489
    if-eqz v5, :cond_1c

    .line 490
    const/16 v6, 0x1b

    .line 491
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 488
    :cond_1c
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 495
    :cond_1d
    iget-object v2, p0, Ldiq;->C:[Ljava/lang/String;

    if-eqz v2, :cond_1f

    iget-object v2, p0, Ldiq;->C:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1f

    .line 497
    iget-object v4, p0, Ldiq;->C:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_5
    if-ge v2, v5, :cond_1e

    aget-object v6, v4, v2

    .line 499
    invoke-static {v6}, Lepl;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 497
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 501
    :cond_1e
    add-int/2addr v0, v3

    .line 502
    iget-object v2, p0, Ldiq;->C:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 504
    :cond_1f
    iget-object v2, p0, Ldiq;->D:[I

    if-eqz v2, :cond_21

    iget-object v2, p0, Ldiq;->D:[I

    array-length v2, v2

    if-lez v2, :cond_21

    .line 506
    iget-object v3, p0, Ldiq;->D:[I

    array-length v4, v3

    move v2, v1

    :goto_6
    if-ge v1, v4, :cond_20

    aget v5, v3, v1

    .line 508
    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 506
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 510
    :cond_20
    add-int/2addr v0, v2

    .line 511
    iget-object v1, p0, Ldiq;->D:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 513
    :cond_21
    iget-object v1, p0, Ldiq;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 514
    iput v0, p0, Ldiq;->cachedSize:I

    .line 515
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 70
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldiq;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldiq;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldiq;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldiq;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldiq;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldiq;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldiq;->e:[Ldis;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldis;

    iget-object v3, p0, Ldiq;->e:[Ldis;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldiq;->e:[Ldis;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldiq;->e:[Ldis;

    :goto_2
    iget-object v2, p0, Ldiq;->e:[Ldis;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldiq;->e:[Ldis;

    new-instance v3, Ldis;

    invoke-direct {v3}, Ldis;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldiq;->e:[Ldis;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldiq;->e:[Ldis;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldiq;->e:[Ldis;

    new-instance v3, Ldis;

    invoke-direct {v3}, Ldis;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldiq;->e:[Ldis;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldiq;->f:[Ldis;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldis;

    iget-object v3, p0, Ldiq;->f:[Ldis;

    if-eqz v3, :cond_5

    iget-object v3, p0, Ldiq;->f:[Ldis;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Ldiq;->f:[Ldis;

    :goto_4
    iget-object v2, p0, Ldiq;->f:[Ldis;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Ldiq;->f:[Ldis;

    new-instance v3, Ldis;

    invoke-direct {v3}, Ldis;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldiq;->f:[Ldis;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Ldiq;->f:[Ldis;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Ldiq;->f:[Ldis;

    new-instance v3, Ldis;

    invoke-direct {v3}, Ldis;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldiq;->f:[Ldis;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldiq;->g:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Ldiq;->g:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldiq;->g:[Ljava/lang/String;

    :goto_5
    iget-object v2, p0, Ldiq;->g:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Ldiq;->g:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_8
    iget-object v2, p0, Ldiq;->g:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_9

    if-eq v0, v4, :cond_9

    const/4 v2, 0x2

    if-eq v0, v2, :cond_9

    const/4 v2, 0x3

    if-ne v0, v2, :cond_a

    :cond_9
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldiq;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_a
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldiq;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldiq;->i:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldiq;->j:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldiq;->k:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldiq;->l:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldiq;->m:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldiq;->n:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Ldiq;->o:Ldir;

    if-nez v0, :cond_b

    new-instance v0, Ldir;

    invoke-direct {v0}, Ldir;-><init>()V

    iput-object v0, p0, Ldiq;->o:Ldir;

    :cond_b
    iget-object v0, p0, Ldiq;->o:Ldir;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldiq;->p:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldiq;->q:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldiq;->r:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_c

    if-ne v0, v4, :cond_d

    :cond_c
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldiq;->s:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_d
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldiq;->s:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldiq;->t:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldiq;->u:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldiq;->v:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_16
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldiq;->w:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_17
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldiq;->x:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_18
    const/16 v0, 0xc2

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldiq;->y:[Leqk;

    if-nez v0, :cond_f

    move v0, v1

    :goto_6
    add-int/2addr v2, v0

    new-array v2, v2, [Leqk;

    iget-object v3, p0, Ldiq;->y:[Leqk;

    if-eqz v3, :cond_e

    iget-object v3, p0, Ldiq;->y:[Leqk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_e
    iput-object v2, p0, Ldiq;->y:[Leqk;

    :goto_7
    iget-object v2, p0, Ldiq;->y:[Leqk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_10

    iget-object v2, p0, Ldiq;->y:[Leqk;

    new-instance v3, Leqk;

    invoke-direct {v3}, Leqk;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldiq;->y:[Leqk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_f
    iget-object v0, p0, Ldiq;->y:[Leqk;

    array-length v0, v0

    goto :goto_6

    :cond_10
    iget-object v2, p0, Ldiq;->y:[Leqk;

    new-instance v3, Leqk;

    invoke-direct {v3}, Leqk;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldiq;->y:[Leqk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_19
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldiq;->z:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_1a
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldiq;->A:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_1b
    const/16 v0, 0xda

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldiq;->B:[Lepu;

    if-nez v0, :cond_12

    move v0, v1

    :goto_8
    add-int/2addr v2, v0

    new-array v2, v2, [Lepu;

    iget-object v3, p0, Ldiq;->B:[Lepu;

    if-eqz v3, :cond_11

    iget-object v3, p0, Ldiq;->B:[Lepu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_11
    iput-object v2, p0, Ldiq;->B:[Lepu;

    :goto_9
    iget-object v2, p0, Ldiq;->B:[Lepu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_13

    iget-object v2, p0, Ldiq;->B:[Lepu;

    new-instance v3, Lepu;

    invoke-direct {v3}, Lepu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldiq;->B:[Lepu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_12
    iget-object v0, p0, Ldiq;->B:[Lepu;

    array-length v0, v0

    goto :goto_8

    :cond_13
    iget-object v2, p0, Ldiq;->B:[Lepu;

    new-instance v3, Lepu;

    invoke-direct {v3}, Lepu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldiq;->B:[Lepu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_1c
    const/16 v0, 0xe2

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldiq;->C:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Ldiq;->C:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldiq;->C:[Ljava/lang/String;

    :goto_a
    iget-object v2, p0, Ldiq;->C:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_14

    iget-object v2, p0, Ldiq;->C:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_14
    iget-object v2, p0, Ldiq;->C:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_1d
    const/16 v0, 0xe8

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldiq;->D:[I

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [I

    iget-object v3, p0, Ldiq;->D:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldiq;->D:[I

    :goto_b
    iget-object v2, p0, Ldiq;->D:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_15

    iget-object v2, p0, Ldiq;->D:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    :cond_15
    iget-object v2, p0, Ldiq;->D:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x78 -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x90 -> :sswitch_12
        0x98 -> :sswitch_13
        0xa0 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb2 -> :sswitch_16
        0xb8 -> :sswitch_17
        0xc2 -> :sswitch_18
        0xca -> :sswitch_19
        0xd0 -> :sswitch_1a
        0xda -> :sswitch_1b
        0xe2 -> :sswitch_1c
        0xe8 -> :sswitch_1d
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 258
    const/4 v1, 0x1

    iget-object v2, p0, Ldiq;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 259
    const/4 v1, 0x2

    iget-object v2, p0, Ldiq;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 260
    iget-object v1, p0, Ldiq;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 261
    const/4 v1, 0x3

    iget-object v2, p0, Ldiq;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 263
    :cond_0
    iget-object v1, p0, Ldiq;->e:[Ldis;

    if-eqz v1, :cond_2

    .line 264
    iget-object v2, p0, Ldiq;->e:[Ldis;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 265
    if-eqz v4, :cond_1

    .line 266
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 264
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 270
    :cond_2
    iget-object v1, p0, Ldiq;->f:[Ldis;

    if-eqz v1, :cond_4

    .line 271
    iget-object v2, p0, Ldiq;->f:[Ldis;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 272
    if-eqz v4, :cond_3

    .line 273
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 271
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 277
    :cond_4
    iget-object v1, p0, Ldiq;->g:[Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 278
    iget-object v2, p0, Ldiq;->g:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 279
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 278
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 282
    :cond_5
    iget-object v1, p0, Ldiq;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 283
    const/4 v1, 0x7

    iget-object v2, p0, Ldiq;->h:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 285
    :cond_6
    iget-object v1, p0, Ldiq;->i:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 286
    const/16 v1, 0x8

    iget-object v2, p0, Ldiq;->i:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 288
    :cond_7
    iget-object v1, p0, Ldiq;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 289
    const/16 v1, 0x9

    iget-object v2, p0, Ldiq;->j:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 291
    :cond_8
    iget-object v1, p0, Ldiq;->k:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 292
    const/16 v1, 0xa

    iget-object v2, p0, Ldiq;->k:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 294
    :cond_9
    iget-object v1, p0, Ldiq;->l:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 295
    const/16 v1, 0xb

    iget-object v2, p0, Ldiq;->l:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 297
    :cond_a
    iget-object v1, p0, Ldiq;->m:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 298
    const/16 v1, 0xc

    iget-object v2, p0, Ldiq;->m:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 300
    :cond_b
    iget-object v1, p0, Ldiq;->n:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    .line 301
    const/16 v1, 0xd

    iget-object v2, p0, Ldiq;->n:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 303
    :cond_c
    iget-object v1, p0, Ldiq;->o:Ldir;

    if-eqz v1, :cond_d

    .line 304
    const/16 v1, 0xe

    iget-object v2, p0, Ldiq;->o:Ldir;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 306
    :cond_d
    iget-object v1, p0, Ldiq;->p:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    .line 307
    const/16 v1, 0xf

    iget-object v2, p0, Ldiq;->p:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 309
    :cond_e
    iget-object v1, p0, Ldiq;->q:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 310
    const/16 v1, 0x10

    iget-object v2, p0, Ldiq;->q:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 312
    :cond_f
    iget-object v1, p0, Ldiq;->r:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 313
    const/16 v1, 0x11

    iget-object v2, p0, Ldiq;->r:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 315
    :cond_10
    iget-object v1, p0, Ldiq;->s:Ljava/lang/Integer;

    if-eqz v1, :cond_11

    .line 316
    const/16 v1, 0x12

    iget-object v2, p0, Ldiq;->s:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 318
    :cond_11
    iget-object v1, p0, Ldiq;->t:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    .line 319
    const/16 v1, 0x13

    iget-object v2, p0, Ldiq;->t:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 321
    :cond_12
    iget-object v1, p0, Ldiq;->u:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    .line 322
    const/16 v1, 0x14

    iget-object v2, p0, Ldiq;->u:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 324
    :cond_13
    iget-object v1, p0, Ldiq;->v:Ljava/lang/String;

    if-eqz v1, :cond_14

    .line 325
    const/16 v1, 0x15

    iget-object v2, p0, Ldiq;->v:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 327
    :cond_14
    iget-object v1, p0, Ldiq;->w:Ljava/lang/String;

    if-eqz v1, :cond_15

    .line 328
    const/16 v1, 0x16

    iget-object v2, p0, Ldiq;->w:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 330
    :cond_15
    iget-object v1, p0, Ldiq;->x:Ljava/lang/Boolean;

    if-eqz v1, :cond_16

    .line 331
    const/16 v1, 0x17

    iget-object v2, p0, Ldiq;->x:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 333
    :cond_16
    iget-object v1, p0, Ldiq;->y:[Leqk;

    if-eqz v1, :cond_18

    .line 334
    iget-object v2, p0, Ldiq;->y:[Leqk;

    array-length v3, v2

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_18

    aget-object v4, v2, v1

    .line 335
    if-eqz v4, :cond_17

    .line 336
    const/16 v5, 0x18

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 334
    :cond_17
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 340
    :cond_18
    iget-object v1, p0, Ldiq;->z:Ljava/lang/String;

    if-eqz v1, :cond_19

    .line 341
    const/16 v1, 0x19

    iget-object v2, p0, Ldiq;->z:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 343
    :cond_19
    iget-object v1, p0, Ldiq;->A:Ljava/lang/Integer;

    if-eqz v1, :cond_1a

    .line 344
    const/16 v1, 0x1a

    iget-object v2, p0, Ldiq;->A:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 346
    :cond_1a
    iget-object v1, p0, Ldiq;->B:[Lepu;

    if-eqz v1, :cond_1c

    .line 347
    iget-object v2, p0, Ldiq;->B:[Lepu;

    array-length v3, v2

    move v1, v0

    :goto_4
    if-ge v1, v3, :cond_1c

    aget-object v4, v2, v1

    .line 348
    if-eqz v4, :cond_1b

    .line 349
    const/16 v5, 0x1b

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 347
    :cond_1b
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 353
    :cond_1c
    iget-object v1, p0, Ldiq;->C:[Ljava/lang/String;

    if-eqz v1, :cond_1d

    .line 354
    iget-object v2, p0, Ldiq;->C:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_5
    if-ge v1, v3, :cond_1d

    aget-object v4, v2, v1

    .line 355
    const/16 v5, 0x1c

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 354
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 358
    :cond_1d
    iget-object v1, p0, Ldiq;->D:[I

    if-eqz v1, :cond_1e

    iget-object v1, p0, Ldiq;->D:[I

    array-length v1, v1

    if-lez v1, :cond_1e

    .line 359
    iget-object v1, p0, Ldiq;->D:[I

    array-length v2, v1

    :goto_6
    if-ge v0, v2, :cond_1e

    aget v3, v1, v0

    .line 360
    const/16 v4, 0x1d

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 359
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 363
    :cond_1e
    iget-object v0, p0, Ldiq;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 365
    return-void
.end method
