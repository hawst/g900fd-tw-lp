.class public final Lbow;
.super Lbmu;
.source "PG"


# static fields
.field private static final a:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lbow;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private d:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lbow;->a:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method private constructor <init>(Lyj;JJ)V
    .locals 2

    .prologue
    .line 78
    invoke-direct/range {p0 .. p5}, Lbmu;-><init>(Lyj;JJ)V

    .line 36
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbow;->d:J

    .line 79
    return-void
.end method

.method public static a(Ljava/lang/String;)Lbow;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lbow;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbow;

    return-object v0
.end method

.method public static a(Lyj;)Lbow;
    .locals 7

    .prologue
    .line 42
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v6

    .line 43
    sget-object v0, Lbow;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbow;

    .line 44
    if-nez v0, :cond_0

    .line 46
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "babel_refresh_participants_lomark_seconds"

    const v2, 0x11940

    .line 45
    invoke-static {v0, v1, v2}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v2, v0

    .line 51
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "babel_refresh_participants_highmark_seconds"

    const v4, 0x15180

    .line 50
    invoke-static {v0, v1, v4}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v4, v0

    .line 56
    new-instance v0, Lbow;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lbow;-><init>(Lyj;JJ)V

    .line 63
    sget-object v1, Lbow;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v6, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lbow;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbow;

    .line 66
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 87
    iget-object v0, p0, Lbow;->c:Lbnl;

    invoke-virtual {v0}, Lbnl;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 89
    new-instance v0, Lyt;

    iget-object v1, p0, Lbow;->b:Lyj;

    invoke-direct {v0, v1}, Lyt;-><init>(Lyj;)V

    .line 90
    invoke-virtual {v0}, Lyt;->l()Ljava/util/List;

    move-result-object v0

    .line 91
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 92
    new-instance v1, Lbel;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {v1, v0, v2, v3, v4}, Lbel;-><init>(Ljava/util/List;Ljava/lang/String;ZZ)V

    .line 94
    iget-object v0, p0, Lbow;->c:Lbnl;

    invoke-virtual {v0, v1}, Lbnl;->a(Lbea;)V

    .line 96
    :cond_0
    return-void
.end method

.method protected a(J)V
    .locals 4

    .prologue
    .line 123
    iput-wide p1, p0, Lbow;->d:J

    .line 124
    new-instance v1, Lyt;

    iget-object v0, p0, Lbow;->b:Lyj;

    invoke-direct {v1, v0}, Lyt;-><init>(Lyj;)V

    .line 125
    invoke-virtual {v1}, Lyt;->a()V

    .line 127
    :try_start_0
    const-string v0, "refresh_participants_time"

    iget-wide v2, p0, Lbow;->d:J

    invoke-virtual {v1, v0, v2, v3}, Lyt;->h(Ljava/lang/String;J)V

    .line 129
    invoke-virtual {v1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    invoke-virtual {v1}, Lyt;->c()V

    .line 132
    return-void

    .line 131
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lyt;->c()V

    throw v0
.end method

.method protected i()J
    .locals 4

    .prologue
    .line 104
    iget-wide v0, p0, Lbow;->d:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 105
    new-instance v1, Lyt;

    iget-object v0, p0, Lbow;->b:Lyj;

    invoke-direct {v1, v0}, Lyt;-><init>(Lyj;)V

    .line 106
    invoke-virtual {v1}, Lyt;->a()V

    .line 108
    :try_start_0
    const-string v0, "refresh_participants_time"

    invoke-virtual {v1, v0}, Lyt;->O(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lbow;->d:J

    .line 110
    invoke-virtual {v1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    invoke-virtual {v1}, Lyt;->c()V

    .line 115
    :cond_0
    iget-wide v0, p0, Lbow;->d:J

    return-wide v0

    .line 112
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lyt;->c()V

    throw v0
.end method
