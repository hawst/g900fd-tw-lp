.class public final enum Ldbc;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldbc;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ldbc;

.field private static final synthetic b:[Ldbc;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 71
    new-instance v0, Ldbc;

    const-string v1, "IGNORE_CASE"

    invoke-direct {v0, v1}, Ldbc;-><init>(Ljava/lang/String;)V

    sput-object v0, Ldbc;->a:Ldbc;

    .line 68
    const/4 v0, 0x1

    new-array v0, v0, [Ldbc;

    const/4 v1, 0x0

    sget-object v2, Ldbc;->a:Ldbc;

    aput-object v2, v0, v1

    sput-object v0, Ldbc;->b:[Ldbc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 68
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldbc;
    .locals 1

    .prologue
    .line 68
    const-class v0, Ldbc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbc;

    return-object v0
.end method

.method public static values()[Ldbc;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Ldbc;->b:[Ldbc;

    invoke-virtual {v0}, [Ldbc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbc;

    return-object v0
.end method
