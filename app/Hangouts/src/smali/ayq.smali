.class public abstract Layq;
.super Lakn;
.source "PG"

# interfaces
.implements Lair;


# instance fields
.field public r:Lyj;

.field public s:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lakn;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lahc;)V
    .locals 3

    .prologue
    .line 54
    iget-object v0, p0, Layq;->r:Lyj;

    iget-object v1, p1, Lahc;->a:Ljava/lang/String;

    iget v2, p1, Lahc;->d:I

    invoke-static {v0, v1, v2}, Lbbl;->a(Lyj;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 56
    const-string v1, "conversation_parameters"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 57
    const-string v1, "conversation_opened_impression"

    const/16 v2, 0x662

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 59
    invoke-virtual {p0, v0}, Layq;->startActivity(Landroid/content/Intent;)V

    .line 60
    return-void
.end method

.method public a(Lbdk;Ljava/lang/String;IJ)V
    .locals 1

    .prologue
    .line 71
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Ljava/lang/String;ZII)V
    .locals 1

    .prologue
    .line 65
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b_(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Layq;->r:Lyj;

    invoke-static {v0, p1}, Lapk;->a(Lyj;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected k()Lyj;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Layq;->r:Lyj;

    return-object v0
.end method

.method public o_()V
    .locals 1

    .prologue
    .line 76
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 28
    invoke-super {p0, p1}, Lakn;->onCreate(Landroid/os/Bundle;)V

    .line 29
    sget v0, Lf;->eI:I

    invoke-virtual {p0, v0}, Layq;->setContentView(I)V

    .line 31
    invoke-virtual {p0}, Layq;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 32
    const-string v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 33
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    iput-object v0, p0, Layq;->r:Lyj;

    .line 35
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v0

    .line 36
    iget-object v1, p0, Layq;->r:Lyj;

    if-nez v1, :cond_1

    .line 37
    const-string v1, "Babel"

    const-string v2, "Account was null from the intent so fetching active."

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    iput-object v0, p0, Layq;->r:Lyj;

    .line 46
    :cond_0
    :goto_0
    invoke-virtual {p0}, Layq;->e()Lae;

    move-result-object v0

    sget v1, Lg;->aQ:I

    .line 47
    invoke-virtual {v0, v1}, Lae;->a(I)Lt;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    iput-object v0, p0, Layq;->s:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    .line 48
    iget-object v0, p0, Layq;->s:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a(Lair;)V

    .line 49
    iget-object v0, p0, Layq;->s:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    iget-object v1, p0, Layq;->r:Lyj;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a(Lyj;)V

    .line 50
    return-void

    .line 39
    :cond_1
    iget-object v1, p0, Layq;->r:Lyj;

    if-eq v0, v1, :cond_0

    .line 42
    iget-object v0, p0, Layq;->o:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x693

    .line 43
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 42
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    goto :goto_0
.end method
