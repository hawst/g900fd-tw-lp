.class public final Lesb;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lesb;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lesb;

    sput-object v0, Lesb;->a:[Lesb;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 13
    iput-object v0, p0, Lesb;->b:Ljava/lang/Integer;

    .line 16
    iput-object v0, p0, Lesb;->c:Ljava/lang/Integer;

    .line 19
    iput-object v0, p0, Lesb;->d:Ljava/lang/Integer;

    .line 22
    iput-object v0, p0, Lesb;->e:Ljava/lang/Integer;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 45
    const/4 v0, 0x0

    .line 46
    iget-object v1, p0, Lesb;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 47
    const/4 v0, 0x1

    iget-object v1, p0, Lesb;->b:Ljava/lang/Integer;

    .line 48
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 50
    :cond_0
    iget-object v1, p0, Lesb;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 51
    const/4 v1, 0x2

    iget-object v2, p0, Lesb;->c:Ljava/lang/Integer;

    .line 52
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    :cond_1
    iget-object v1, p0, Lesb;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 55
    const/4 v1, 0x3

    iget-object v2, p0, Lesb;->d:Ljava/lang/Integer;

    .line 56
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    :cond_2
    iget-object v1, p0, Lesb;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 59
    const/4 v1, 0x4

    iget-object v2, p0, Lesb;->e:Ljava/lang/Integer;

    .line 60
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    :cond_3
    iget-object v1, p0, Lesb;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    iput v0, p0, Lesb;->cachedSize:I

    .line 64
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 7

    .prologue
    const/16 v6, 0x6e

    const/16 v5, 0x64

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lesb;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lesb;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lesb;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    if-ne v0, v5, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lesb;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lesb;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    const/4 v1, 0x6

    if-eq v0, v1, :cond_4

    const/4 v1, 0x7

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd

    if-eq v0, v1, :cond_4

    const/16 v1, 0xe

    if-eq v0, v1, :cond_4

    const/16 v1, 0x50

    if-eq v0, v1, :cond_4

    const/16 v1, 0x51

    if-eq v0, v1, :cond_4

    if-eq v0, v5, :cond_4

    const/16 v1, 0x65

    if-eq v0, v1, :cond_4

    const/16 v1, 0x66

    if-eq v0, v1, :cond_4

    const/16 v1, 0x67

    if-eq v0, v1, :cond_4

    const/16 v1, 0x68

    if-eq v0, v1, :cond_4

    const/16 v1, 0x77

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa0

    if-eq v0, v1, :cond_4

    const/16 v1, 0x69

    if-eq v0, v1, :cond_4

    const/16 v1, 0x81

    if-eq v0, v1, :cond_4

    const/16 v1, 0x6a

    if-eq v0, v1, :cond_4

    const/16 v1, 0x6b

    if-eq v0, v1, :cond_4

    const/16 v1, 0x6c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x6d

    if-eq v0, v1, :cond_4

    if-eq v0, v6, :cond_4

    const/16 v1, 0x6f

    if-eq v0, v1, :cond_4

    const/16 v1, 0x70

    if-eq v0, v1, :cond_4

    const/16 v1, 0x71

    if-eq v0, v1, :cond_4

    const/16 v1, 0x72

    if-eq v0, v1, :cond_4

    const/16 v1, 0x73

    if-eq v0, v1, :cond_4

    const/16 v1, 0x74

    if-eq v0, v1, :cond_4

    const/16 v1, 0x76

    if-eq v0, v1, :cond_4

    const/16 v1, 0x78

    if-eq v0, v1, :cond_4

    const/16 v1, 0x79

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7a

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7b

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7d

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7e

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7f

    if-eq v0, v1, :cond_4

    const/16 v1, 0x80

    if-eq v0, v1, :cond_4

    const/16 v1, 0x82

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x83

    if-eq v0, v1, :cond_4

    const/16 v1, 0x84

    if-eq v0, v1, :cond_4

    const/16 v1, 0x85

    if-eq v0, v1, :cond_4

    const/16 v1, 0x86

    if-eq v0, v1, :cond_4

    const/16 v1, 0x87

    if-eq v0, v1, :cond_4

    const/16 v1, 0x88

    if-eq v0, v1, :cond_4

    const/16 v1, 0x89

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8a

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8b

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8d

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8e

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8f

    if-eq v0, v1, :cond_4

    const/16 v1, 0x90

    if-eq v0, v1, :cond_4

    const/16 v1, 0x91

    if-eq v0, v1, :cond_4

    const/16 v1, 0x92

    if-eq v0, v1, :cond_4

    const/16 v1, 0x93

    if-eq v0, v1, :cond_4

    const/16 v1, 0x94

    if-eq v0, v1, :cond_4

    const/16 v1, 0x95

    if-eq v0, v1, :cond_4

    const/16 v1, 0x96

    if-eq v0, v1, :cond_4

    const/16 v1, 0x97

    if-eq v0, v1, :cond_4

    const/16 v1, 0x98

    if-eq v0, v1, :cond_4

    const/16 v1, 0x99

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9a

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9b

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9d

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9e

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9f

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa1

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa2

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa3

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa4

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa5

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa6

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa7

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xca

    if-eq v0, v1, :cond_4

    const/16 v1, 0xcb

    if-eq v0, v1, :cond_4

    const/16 v1, 0xcc

    if-eq v0, v1, :cond_4

    const/16 v1, 0xcd

    if-eq v0, v1, :cond_4

    const/16 v1, 0xce

    if-eq v0, v1, :cond_4

    const/16 v1, 0xcf

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd0

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd1

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd2

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd3

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd4

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd5

    if-eq v0, v1, :cond_4

    const/16 v1, 0x12c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x12d

    if-eq v0, v1, :cond_4

    const/16 v1, 0x75

    if-eq v0, v1, :cond_4

    const/16 v1, 0x190

    if-eq v0, v1, :cond_4

    const/16 v1, 0x191

    if-eq v0, v1, :cond_4

    const/16 v1, 0x192

    if-eq v0, v1, :cond_4

    const/16 v1, 0x193

    if-eq v0, v1, :cond_4

    const/16 v1, 0x194

    if-eq v0, v1, :cond_4

    const/16 v1, 0x195

    if-eq v0, v1, :cond_4

    const/16 v1, 0x196

    if-eq v0, v1, :cond_4

    const/16 v1, 0x197

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3e9

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3ea

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3eb

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3ec

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3ed

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3ee

    if-ne v0, v1, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lesb;->c:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lesb;->c:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_6

    if-eq v0, v3, :cond_6

    if-eq v0, v4, :cond_6

    const/16 v1, 0xa

    if-ne v0, v1, :cond_7

    :cond_6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lesb;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_7
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lesb;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v6, :cond_8

    if-eq v0, v5, :cond_8

    const/16 v1, 0x5a

    if-eq v0, v1, :cond_8

    const/16 v1, 0x32

    if-eq v0, v1, :cond_8

    const/16 v1, 0x28

    if-eq v0, v1, :cond_8

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_8

    const/16 v1, 0x14

    if-ne v0, v1, :cond_9

    :cond_8
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lesb;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_9
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lesb;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lesb;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 28
    const/4 v0, 0x1

    iget-object v1, p0, Lesb;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 30
    :cond_0
    iget-object v0, p0, Lesb;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 31
    const/4 v0, 0x2

    iget-object v1, p0, Lesb;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 33
    :cond_1
    iget-object v0, p0, Lesb;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 34
    const/4 v0, 0x3

    iget-object v1, p0, Lesb;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 36
    :cond_2
    iget-object v0, p0, Lesb;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 37
    const/4 v0, 0x4

    iget-object v1, p0, Lesb;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 39
    :cond_3
    iget-object v0, p0, Lesb;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 41
    return-void
.end method
