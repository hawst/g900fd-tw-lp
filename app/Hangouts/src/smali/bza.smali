.class public final Lbza;
.super Ljava/util/HashSet;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashSet",
        "<",
        "Lbdk;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    .line 22
    return-void
.end method


# virtual methods
.method public contains(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 30
    if-eqz p1, :cond_0

    instance-of v0, p1, Lbdk;

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 39
    :goto_0
    return v0

    .line 33
    :cond_1
    check-cast p1, Lbdk;

    .line 34
    invoke-virtual {p0}, Lbza;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdk;

    .line 35
    invoke-virtual {v0, p1}, Lbdk;->a(Lbdk;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 36
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 39
    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 44
    if-eqz p1, :cond_0

    instance-of v0, p1, Lbdk;

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 58
    :goto_0
    return v0

    .line 47
    :cond_1
    check-cast p1, Lbdk;

    .line 48
    const/4 v2, 0x0

    .line 49
    invoke-virtual {p0}, Lbza;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdk;

    .line 50
    invoke-virtual {p1, v0}, Lbdk;->a(Lbdk;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 55
    :goto_1
    if-eqz v0, :cond_3

    .line 56
    invoke-super {p0, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_3
    move v0, v1

    .line 58
    goto :goto_0

    :cond_4
    move-object v0, v2

    goto :goto_1
.end method
