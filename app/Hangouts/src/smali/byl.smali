.class public final Lbyl;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lbyh;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/ImageView;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:[I

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/graphics/BitmapFactory$Options;

.field private final f:Ljava/lang/StringBuilder;

.field private final g:Landroid/graphics/Canvas;


# direct methods
.method public constructor <init>(Lbyh;[I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 292
    iput-object p1, p0, Lbyl;->a:Lbyh;

    .line 293
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 294
    iput-object p2, p0, Lbyl;->c:[I

    .line 295
    invoke-static {}, Lbyh;->b()Landroid/graphics/BitmapFactory$Options;

    move-result-object v0

    iput-object v0, p0, Lbyl;->e:Landroid/graphics/BitmapFactory$Options;

    .line 296
    array-length v0, p2

    .line 297
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, Lbyl;->b:Ljava/util/Map;

    .line 299
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lbyl;->d:Ljava/util/Map;

    .line 300
    invoke-static {p1}, Lbyh;->a(Lbyh;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 301
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lbyl;->f:Ljava/lang/StringBuilder;

    .line 302
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    iput-object v0, p0, Lbyl;->g:Landroid/graphics/Canvas;

    .line 307
    :goto_0
    return-void

    .line 304
    :cond_0
    iput-object v2, p0, Lbyl;->f:Ljava/lang/StringBuilder;

    .line 305
    iput-object v2, p0, Lbyl;->g:Landroid/graphics/Canvas;

    goto :goto_0
.end method

.method private varargs a([Ljava/lang/Integer;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 417
    aget-object v0, p1, v1

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v2

    .line 418
    iget-object v3, p0, Lbyl;->b:Ljava/util/Map;

    monitor-enter v3

    .line 419
    :try_start_0
    iget-object v0, p0, Lbyl;->b:Ljava/util/Map;

    .line 420
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 421
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lbyl;->isCancelled()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    if-nez v1, :cond_2

    .line 422
    iget-object v1, p0, Lbyl;->d:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 423
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 424
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 426
    iget-object v2, p0, Lbyl;->a:Lbyh;

    invoke-static {v0}, Lbyh;->a(Landroid/widget/ImageView;)Lbyl;

    move-result-object v2

    .line 427
    if-ne v2, p0, :cond_2

    .line 428
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 432
    :cond_2
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method


# virtual methods
.method public a(ILandroid/widget/ImageView;)V
    .locals 3

    .prologue
    .line 310
    iget-object v0, p0, Lbyl;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lbyl;->a([Ljava/lang/Integer;)V

    .line 312
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 13

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 283
    iget-object v6, p0, Lbyl;->c:[I

    array-length v7, v6

    move v4, v5

    :goto_0
    if-ge v4, v7, :cond_7

    aget v8, v6, v4

    invoke-virtual {p0}, Lbyl;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lbyl;->a:Lbyh;

    invoke-static {v0}, Lbyh;->b(Lbyh;)Lex;

    move-result-object v0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lex;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbyl;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lbyl;->a:Lbyh;

    invoke-static {v0}, Lbyh;->a(Lbyh;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lbyh;->d()Lbxu;

    move-result-object v0

    iget-object v1, p0, Lbyl;->a:Lbyh;

    invoke-static {v1}, Lbyh;->c(Lbyh;)I

    move-result v1

    iget-object v3, p0, Lbyl;->a:Lbyh;

    invoke-static {v3}, Lbyh;->c(Lbyh;)I

    move-result v3

    invoke-virtual {v0, v1, v3, v2}, Lbxu;->a(IILbxv;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/graphics/Bitmap;->eraseColor(I)V

    iget-object v1, p0, Lbyl;->g:Landroid/graphics/Canvas;

    invoke-virtual {v1, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lbyl;->f:Ljava/lang/StringBuilder;

    iget-object v3, p0, Lbyl;->f:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-virtual {v1, v5, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbyl;->f:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbyl;->g:Landroid/graphics/Canvas;

    iget-object v3, p0, Lbyl;->f:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v9, 0x0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    iget-object v11, p0, Lbyl;->a:Lbyh;

    invoke-static {v11}, Lbyh;->d(Lbyh;)Landroid/graphics/Rect;

    move-result-object v11

    iget v11, v11, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    iget-object v11, p0, Lbyl;->a:Lbyh;

    invoke-static {v11}, Lbyh;->e(Lbyh;)Landroid/graphics/Paint;

    move-result-object v11

    invoke-virtual {v1, v3, v9, v10, v11}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v1, p0, Lbyl;->a:Lbyh;

    invoke-static {v1}, Lbyh;->b(Lbyh;)Lex;

    move-result-object v1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, Lex;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbyl;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lbyl;->d:Ljava/util/Map;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-virtual {p0, v0}, Lbyl;->publishProgress([Ljava/lang/Object;)V

    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_0

    :cond_2
    const-wide/16 v0, 0x0

    invoke-static {}, Lbyh;->e()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    :cond_3
    invoke-static {}, Lccc;->a()Lccc;

    move-result-object v3

    invoke-virtual {v3, v8}, Lccc;->a(I)I

    move-result v3

    const/4 v9, -0x1

    if-ne v3, v9, :cond_4

    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Invalid resourceId for codePoint: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    goto :goto_1

    :cond_4
    invoke-static {}, Lbyh;->d()Lbxu;

    move-result-object v9

    iget-object v10, p0, Lbyl;->e:Landroid/graphics/BitmapFactory$Options;

    iget-object v11, p0, Lbyl;->e:Landroid/graphics/BitmapFactory$Options;

    iget v11, v11, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    iget-object v12, p0, Lbyl;->e:Landroid/graphics/BitmapFactory$Options;

    iget v12, v12, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    invoke-virtual {v9, v3, v10, v11, v12}, Lbxu;->a(ILandroid/graphics/BitmapFactory$Options;II)Landroid/graphics/Bitmap;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v9, p0, Lbyl;->a:Lbyh;

    invoke-static {v9}, Lbyh;->b(Lbyh;)Lex;

    move-result-object v9

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10, v3}, Lex;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    invoke-static {}, Lbyh;->e()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v9

    sub-long v0, v9, v0

    const-string v9, "Babel"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Emoji Bitmap decodingTim="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v9, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    move-object v0, v3

    goto/16 :goto_1

    :cond_7
    return-object v2
.end method

.method protected synthetic onCancelled(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 283
    iget-object v2, p0, Lbyl;->c:[I

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget v0, v2, v1

    iget-object v4, p0, Lbyl;->a:Lbyh;

    invoke-static {v4}, Lbyh;->b(Lbyh;)Lex;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lex;->b(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lbyl;->d:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    invoke-static {}, Lbyh;->d()Lbxu;

    move-result-object v4

    invoke-virtual {v4, v0}, Lbxu;->a(Landroid/graphics/Bitmap;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 283
    check-cast p1, [Ljava/lang/Integer;

    invoke-direct {p0, p1}, Lbyl;->a([Ljava/lang/Integer;)V

    return-void
.end method
