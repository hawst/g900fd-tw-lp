.class public final Leie;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leie;

.field public static final b:Lepo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepo",
            "<",
            "Leie;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/Long;

.field public e:Lfab;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Leie;

    sput-object v0, Leie;->a:[Leie;

    .line 13
    const v0, 0xca4e15

    new-instance v1, Leif;

    invoke-direct {v1}, Leif;-><init>()V

    .line 14
    invoke-static {v0, v1}, Lepo;->a(ILepp;)Lepo;

    move-result-object v0

    sput-object v0, Leie;->b:Lepo;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Leie;->e:Lfab;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 37
    const/4 v0, 0x1

    iget-object v1, p0, Leie;->c:Ljava/lang/Long;

    .line 39
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 40
    const/4 v1, 0x2

    iget-object v2, p0, Leie;->d:Ljava/lang/Long;

    .line 41
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 42
    iget-object v1, p0, Leie;->e:Lfab;

    if-eqz v1, :cond_0

    .line 43
    const/4 v1, 0x3

    iget-object v2, p0, Leie;->e:Lfab;

    .line 44
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46
    :cond_0
    iget-object v1, p0, Leie;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47
    iput v0, p0, Leie;->cachedSize:I

    .line 48
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leie;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leie;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leie;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Leie;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Leie;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Leie;->e:Lfab;

    if-nez v0, :cond_2

    new-instance v0, Lfab;

    invoke-direct {v0}, Lfab;-><init>()V

    iput-object v0, p0, Leie;->e:Lfab;

    :cond_2
    iget-object v0, p0, Leie;->e:Lfab;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 26
    const/4 v0, 0x1

    iget-object v1, p0, Leie;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->c(IJ)V

    .line 27
    const/4 v0, 0x2

    iget-object v1, p0, Leie;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->c(IJ)V

    .line 28
    iget-object v0, p0, Leie;->e:Lfab;

    if-eqz v0, :cond_0

    .line 29
    const/4 v0, 0x3

    iget-object v1, p0, Leie;->e:Lfab;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 31
    :cond_0
    iget-object v0, p0, Leie;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 33
    return-void
.end method
