.class public final Ldsf;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldsf;


# instance fields
.field public b:Ldse;

.field public c:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20627
    const/4 v0, 0x0

    new-array v0, v0, [Ldsf;

    sput-object v0, Ldsf;->a:[Ldsf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20628
    invoke-direct {p0}, Lepn;-><init>()V

    .line 20631
    const/4 v0, 0x0

    iput-object v0, p0, Ldsf;->b:Ldse;

    .line 20628
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 20650
    const/4 v0, 0x0

    .line 20651
    iget-object v1, p0, Ldsf;->b:Ldse;

    if-eqz v1, :cond_0

    .line 20652
    const/4 v0, 0x1

    iget-object v1, p0, Ldsf;->b:Ldse;

    .line 20653
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 20655
    :cond_0
    iget-object v1, p0, Ldsf;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 20656
    const/4 v1, 0x2

    iget-object v2, p0, Ldsf;->c:Ljava/lang/Boolean;

    .line 20657
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 20659
    :cond_1
    iget-object v1, p0, Ldsf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20660
    iput v0, p0, Ldsf;->cachedSize:I

    .line 20661
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 20624
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldsf;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldsf;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldsf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldsf;->b:Ldse;

    if-nez v0, :cond_2

    new-instance v0, Ldse;

    invoke-direct {v0}, Ldse;-><init>()V

    iput-object v0, p0, Ldsf;->b:Ldse;

    :cond_2
    iget-object v0, p0, Ldsf;->b:Ldse;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldsf;->c:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 20638
    iget-object v0, p0, Ldsf;->b:Ldse;

    if-eqz v0, :cond_0

    .line 20639
    const/4 v0, 0x1

    iget-object v1, p0, Ldsf;->b:Ldse;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 20641
    :cond_0
    iget-object v0, p0, Ldsf;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 20642
    const/4 v0, 0x2

    iget-object v1, p0, Ldsf;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 20644
    :cond_1
    iget-object v0, p0, Ldsf;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 20646
    return-void
.end method
