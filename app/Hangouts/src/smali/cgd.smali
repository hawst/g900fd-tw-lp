.class public final Lcgd;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<O::",
        "Lft;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lcgf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcgf",
            "<*TO;>;"
        }
    .end annotation
.end field

.field private final b:Lcgg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcgg",
            "<*>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lsr;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>(Lcgf;Lcgg;[Lsr;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "Lcge;",
            ">(",
            "Lcgf",
            "<TC;TO;>;",
            "Lcgg",
            "<TC;>;[",
            "Lsr;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcgd;->a:Lcgf;

    iput-object p2, p0, Lcgd;->b:Lcgg;

    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcgd;->c:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public a()Lcgf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcgf",
            "<*TO;>;"
        }
    .end annotation

    iget-object v0, p0, Lcgd;->a:Lcgf;

    return-object v0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lsr;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcgd;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public c()Lcgg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcgg",
            "<*>;"
        }
    .end annotation

    iget-object v0, p0, Lcgd;->b:Lcgg;

    return-object v0
.end method
