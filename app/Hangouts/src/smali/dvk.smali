.class public final Ldvk;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldvk;


# instance fields
.field public b:Ldvm;

.field public c:Ldqf;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/Integer;

.field public f:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16592
    const/4 v0, 0x0

    new-array v0, v0, [Ldvk;

    sput-object v0, Ldvk;->a:[Ldvk;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16593
    invoke-direct {p0}, Lepn;-><init>()V

    .line 16596
    iput-object v0, p0, Ldvk;->b:Ldvm;

    .line 16599
    iput-object v0, p0, Ldvk;->c:Ldqf;

    .line 16604
    iput-object v0, p0, Ldvk;->e:Ljava/lang/Integer;

    .line 16607
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Ldvk;->f:[I

    .line 16593
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 16636
    iget-object v0, p0, Ldvk;->b:Ldvm;

    if-eqz v0, :cond_5

    .line 16637
    const/4 v0, 0x1

    iget-object v2, p0, Ldvk;->b:Ldvm;

    .line 16638
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 16640
    :goto_0
    iget-object v2, p0, Ldvk;->c:Ldqf;

    if-eqz v2, :cond_0

    .line 16641
    const/4 v2, 0x2

    iget-object v3, p0, Ldvk;->c:Ldqf;

    .line 16642
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 16644
    :cond_0
    iget-object v2, p0, Ldvk;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 16645
    const/4 v2, 0x3

    iget-object v3, p0, Ldvk;->e:Ljava/lang/Integer;

    .line 16646
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 16648
    :cond_1
    iget-object v2, p0, Ldvk;->d:Ljava/lang/Long;

    if-eqz v2, :cond_2

    .line 16649
    const/4 v2, 0x4

    iget-object v3, p0, Ldvk;->d:Ljava/lang/Long;

    .line 16650
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 16652
    :cond_2
    iget-object v2, p0, Ldvk;->f:[I

    if-eqz v2, :cond_4

    iget-object v2, p0, Ldvk;->f:[I

    array-length v2, v2

    if-lez v2, :cond_4

    .line 16654
    iget-object v3, p0, Ldvk;->f:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_3

    aget v5, v3, v1

    .line 16656
    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 16654
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 16658
    :cond_3
    add-int/2addr v0, v2

    .line 16659
    iget-object v1, p0, Ldvk;->f:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 16661
    :cond_4
    iget-object v1, p0, Ldvk;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16662
    iput v0, p0, Ldvk;->cachedSize:I

    .line 16663
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 16589
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldvk;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldvk;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldvk;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldvk;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldvk;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldvk;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldvk;->c:Ldqf;

    if-nez v0, :cond_3

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldvk;->c:Ldqf;

    :cond_3
    iget-object v0, p0, Ldvk;->c:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldvk;->e:Ljava/lang/Integer;

    goto :goto_0

    :cond_5
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldvk;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldvk;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x28

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldvk;->f:[I

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [I

    iget-object v2, p0, Ldvk;->f:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldvk;->f:[I

    :goto_1
    iget-object v1, p0, Ldvk;->f:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_6

    iget-object v1, p0, Ldvk;->f:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    iget-object v1, p0, Ldvk;->f:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 16612
    iget-object v0, p0, Ldvk;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 16613
    const/4 v0, 0x1

    iget-object v1, p0, Ldvk;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 16615
    :cond_0
    iget-object v0, p0, Ldvk;->c:Ldqf;

    if-eqz v0, :cond_1

    .line 16616
    const/4 v0, 0x2

    iget-object v1, p0, Ldvk;->c:Ldqf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 16618
    :cond_1
    iget-object v0, p0, Ldvk;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 16619
    const/4 v0, 0x3

    iget-object v1, p0, Ldvk;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 16621
    :cond_2
    iget-object v0, p0, Ldvk;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 16622
    const/4 v0, 0x4

    iget-object v1, p0, Ldvk;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 16624
    :cond_3
    iget-object v0, p0, Ldvk;->f:[I

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldvk;->f:[I

    array-length v0, v0

    if-lez v0, :cond_4

    .line 16625
    iget-object v1, p0, Ldvk;->f:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget v3, v1, v0

    .line 16626
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 16625
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 16629
    :cond_4
    iget-object v0, p0, Ldvk;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 16631
    return-void
.end method
