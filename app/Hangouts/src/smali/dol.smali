.class public final Ldol;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldol;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:[Ldom;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2267
    const/4 v0, 0x0

    new-array v0, v0, [Ldol;

    sput-object v0, Ldol;->a:[Ldol;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2268
    invoke-direct {p0}, Lepn;-><init>()V

    .line 2348
    sget-object v0, Ldom;->a:[Ldom;

    iput-object v0, p0, Ldol;->f:[Ldom;

    .line 2268
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2379
    iget-object v0, p0, Ldol;->b:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 2380
    const/4 v0, 0x1

    iget-object v2, p0, Ldol;->b:Ljava/lang/String;

    .line 2381
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2383
    :goto_0
    iget-object v2, p0, Ldol;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 2384
    const/4 v2, 0x2

    iget-object v3, p0, Ldol;->c:Ljava/lang/String;

    .line 2385
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2387
    :cond_0
    iget-object v2, p0, Ldol;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 2388
    const/4 v2, 0x3

    iget-object v3, p0, Ldol;->d:Ljava/lang/Integer;

    .line 2389
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2391
    :cond_1
    iget-object v2, p0, Ldol;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 2392
    const/4 v2, 0x4

    iget-object v3, p0, Ldol;->e:Ljava/lang/Integer;

    .line 2393
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2395
    :cond_2
    iget-object v2, p0, Ldol;->f:[Ldom;

    if-eqz v2, :cond_4

    .line 2396
    iget-object v2, p0, Ldol;->f:[Ldom;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 2397
    if-eqz v4, :cond_3

    .line 2398
    const/4 v5, 0x5

    .line 2399
    invoke-static {v5, v4}, Lepl;->c(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2396
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2403
    :cond_4
    iget-object v1, p0, Ldol;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2404
    iput v0, p0, Ldol;->cachedSize:I

    .line 2405
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v1, 0x0

    .line 2264
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldol;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldol;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldol;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldol;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldol;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldol;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldol;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2b

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldol;->f:[Ldom;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldom;

    iget-object v3, p0, Ldol;->f:[Ldom;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldol;->f:[Ldom;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldol;->f:[Ldom;

    :goto_2
    iget-object v2, p0, Ldol;->f:[Ldom;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldol;->f:[Ldom;

    new-instance v3, Ldom;

    invoke-direct {v3}, Ldom;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldol;->f:[Ldom;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2, v4}, Lepk;->a(Lepr;I)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldol;->f:[Ldom;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldol;->f:[Ldom;

    new-instance v3, Ldom;

    invoke-direct {v3}, Ldom;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldol;->f:[Ldom;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0, v4}, Lepk;->a(Lepr;I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2b -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 2353
    iget-object v0, p0, Ldol;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2354
    const/4 v0, 0x1

    iget-object v1, p0, Ldol;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 2356
    :cond_0
    iget-object v0, p0, Ldol;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2357
    const/4 v0, 0x2

    iget-object v1, p0, Ldol;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 2359
    :cond_1
    iget-object v0, p0, Ldol;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2360
    const/4 v0, 0x3

    iget-object v1, p0, Ldol;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 2362
    :cond_2
    iget-object v0, p0, Ldol;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 2363
    const/4 v0, 0x4

    iget-object v1, p0, Ldol;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 2365
    :cond_3
    iget-object v0, p0, Ldol;->f:[Ldom;

    if-eqz v0, :cond_5

    .line 2366
    iget-object v1, p0, Ldol;->f:[Ldom;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 2367
    if-eqz v3, :cond_4

    .line 2368
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Lepl;->a(ILepr;)V

    .line 2366
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2372
    :cond_5
    iget-object v0, p0, Ldol;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 2374
    return-void
.end method
