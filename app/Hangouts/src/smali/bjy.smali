.class public final Lbjy;
.super Lbnj;
.source "PG"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbjz;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Z

.field private final e:Z

.field private f:I


# direct methods
.method constructor <init>(Lyj;Ljava/util/List;ZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyj;",
            "Ljava/util/List",
            "<",
            "Lbjz;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lbnj;-><init>(Lyj;)V

    .line 49
    iput-object p2, p0, Lbjy;->a:Ljava/util/List;

    .line 50
    iput-boolean p3, p0, Lbjy;->d:Z

    .line 51
    iput-boolean p4, p0, Lbjy;->e:Z

    .line 52
    return-void
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const/4 v3, 0x1

    .line 60
    new-instance v4, Lyt;

    iget-object v0, p0, Lbjy;->b:Lyj;

    invoke-direct {v4, v0}, Lyt;-><init>(Lyj;)V

    .line 61
    iget-object v0, p0, Lbjy;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjz;

    .line 62
    iget-object v1, v0, Lbjz;->a:Ljava/lang/String;

    invoke-virtual {v4, v1}, Lyt;->e(Ljava/lang/String;)Lyv;

    move-result-object v1

    .line 68
    if-eqz v1, :cond_1

    .line 69
    iget-wide v1, v1, Lyv;->u:J

    iget-wide v6, v0, Lbjz;->b:J

    invoke-static {v1, v2, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    .line 74
    :goto_1
    iget-object v6, v0, Lbjz;->a:Ljava/lang/String;

    invoke-static {v6}, Lyt;->a(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-boolean v6, p0, Lbjy;->e:Z

    if-nez v6, :cond_2

    .line 76
    new-instance v6, Lbec;

    iget-object v0, v0, Lbjz;->a:Ljava/lang/String;

    iget-boolean v7, p0, Lbjy;->d:Z

    invoke-direct {v6, v0, v1, v2, v7}, Lbec;-><init>(Ljava/lang/String;JZ)V

    .line 78
    iget-object v0, p0, Lbjy;->c:Lbnl;

    invoke-virtual {v0, v6}, Lbnl;->a(Lbea;)V

    goto :goto_0

    .line 71
    :cond_1
    iget-wide v1, v0, Lbjz;->b:J

    goto :goto_1

    .line 79
    :cond_2
    iget v0, p0, Lbjy;->f:I

    if-ltz v0, :cond_0

    .line 82
    invoke-static {}, Lbgc;->f()Lbgc;

    move-result-object v0

    .line 83
    new-instance v1, Lbos;

    iget v2, p0, Lbjy;->f:I

    invoke-direct {v1, v2, v3, v0}, Lbos;-><init>(IILbfz;)V

    .line 85
    iget v0, p0, Lbjy;->f:I

    iget-object v2, p0, Lbjy;->b:Lyj;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(ILyj;Lbos;)V

    goto :goto_0

    .line 89
    :cond_3
    iget-boolean v0, p0, Lbjy;->d:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x2

    move v1, v0

    :goto_2
    invoke-virtual {v4}, Lyt;->a()V

    :try_start_0
    iget-object v0, p0, Lbjy;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjz;

    const-string v3, "Babel"

    const/4 v5, 0x3

    invoke-static {v3, v5}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "updateConversationViewLocally conversationId: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v0, Lbjz;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    new-instance v3, Lbjq;

    iget-object v0, v0, Lbjz;->a:Ljava/lang/String;

    invoke-direct {v3, v0, v1}, Lbjq;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lbka;

    invoke-direct {v0, v3}, Lbka;-><init>(Lbjq;)V

    iget-object v3, p0, Lbjy;->c:Lbnl;

    invoke-virtual {v0, v4}, Lbka;->a(Lyt;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lyt;->c()V

    throw v0

    :cond_5
    move v1, v3

    goto :goto_2

    :cond_6
    :try_start_1
    invoke-virtual {v4}, Lyt;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v4}, Lyt;->c()V

    invoke-static {v4}, Lyp;->d(Lyt;)V

    .line 95
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 55
    iput p1, p0, Lbjy;->f:I

    .line 56
    return-void
.end method
