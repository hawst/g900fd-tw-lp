.class public final Lcox;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcpp;

.field private b:Lcpj;


# direct methods
.method public constructor <init>(Lcpp;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lg;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpp;

    iput-object v0, p0, Lcox;->a:Lcpp;

    return-void
.end method


# virtual methods
.method public a()Lcpp;
    .locals 1

    iget-object v0, p0, Lcox;->a:Lcpp;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcsx;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcox;->a:Lcpp;

    invoke-interface {v0, p1}, Lcpp;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lctv;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Lcsx;

    invoke-direct {v0, v1}, Lcsx;-><init>(Lctv;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lv;

    invoke-direct {v1, v0}, Lv;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lcow;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcox;->a:Lcpp;

    invoke-virtual {p1}, Lcow;->a()Lcjc;

    move-result-object v1

    invoke-interface {v0, v1}, Lcpp;->a(Lcjc;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lv;

    invoke-direct {v1, v0}, Lv;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lcpb;)V
    .locals 2

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcox;->a:Lcpp;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcpp;->a(Lcqz;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcox;->a:Lcpp;

    new-instance v1, Lcpa;

    invoke-direct {v1, p0, p1}, Lcpa;-><init>(Lcox;Lcpb;)V

    invoke-interface {v0, v1}, Lcpp;->a(Lcqz;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lv;

    invoke-direct {v1, v0}, Lv;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lcpc;)V
    .locals 2

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcox;->a:Lcpp;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcpp;->a(Lcsa;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcox;->a:Lcpp;

    new-instance v1, Lcoy;

    invoke-direct {v1, p0, p1}, Lcoy;-><init>(Lcox;Lcpc;)V

    invoke-interface {v0, v1}, Lcpp;->a(Lcsa;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lv;

    invoke-direct {v1, v0}, Lv;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lcpd;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcox;->a:Lcpp;

    new-instance v1, Lcoz;

    invoke-direct {v1, p0, p1}, Lcoz;-><init>(Lcox;Lcpd;)V

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcpp;->a(Lcsp;Lcjc;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lv;

    invoke-direct {v1, v0}, Lv;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Z)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcox;->a:Lcpp;

    invoke-interface {v0, p1}, Lcpp;->c(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lv;

    invoke-direct {v1, v0}, Lv;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final b()Lcom/google/android/gms/maps/model/CameraPosition;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcox;->a:Lcpp;

    invoke-interface {v0}, Lcpp;->a()Lcom/google/android/gms/maps/model/CameraPosition;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lv;

    invoke-direct {v1, v0}, Lv;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final b(Lcow;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcox;->a:Lcpp;

    invoke-virtual {p1}, Lcow;->a()Lcjc;

    move-result-object v1

    invoke-interface {v0, v1}, Lcpp;->b(Lcjc;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lv;

    invoke-direct {v1, v0}, Lv;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final c()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcox;->a:Lcpp;

    invoke-interface {v0}, Lcpp;->e()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lv;

    invoke-direct {v1, v0}, Lv;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final d()Lcpj;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcox;->b:Lcpj;

    if-nez v0, :cond_0

    new-instance v0, Lcpj;

    iget-object v1, p0, Lcox;->a:Lcpp;

    invoke-interface {v1}, Lcpp;->k()Lcqn;

    move-result-object v1

    invoke-direct {v0, v1}, Lcpj;-><init>(Lcqn;)V

    iput-object v0, p0, Lcox;->b:Lcpj;

    :cond_0
    iget-object v0, p0, Lcox;->b:Lcpj;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lv;

    invoke-direct {v1, v0}, Lv;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method
