.class public final Letf;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Letf;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Lesw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 320
    const/4 v0, 0x0

    new-array v0, v0, [Letf;

    sput-object v0, Letf;->a:[Letf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 321
    invoke-direct {p0}, Lepn;-><init>()V

    .line 328
    const/4 v0, 0x0

    iput-object v0, p0, Letf;->d:Lesw;

    .line 321
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 348
    const/4 v0, 0x0

    .line 349
    iget-object v1, p0, Letf;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 350
    const/4 v0, 0x1

    iget-object v1, p0, Letf;->b:Ljava/lang/Integer;

    .line 351
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 353
    :cond_0
    iget-object v1, p0, Letf;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 354
    const/4 v1, 0x2

    iget-object v2, p0, Letf;->c:Ljava/lang/Integer;

    .line 355
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 357
    :cond_1
    iget-object v1, p0, Letf;->d:Lesw;

    if-eqz v1, :cond_2

    .line 358
    const/4 v1, 0x3

    iget-object v2, p0, Letf;->d:Lesw;

    .line 359
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 361
    :cond_2
    iget-object v1, p0, Letf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 362
    iput v0, p0, Letf;->cachedSize:I

    .line 363
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 317
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Letf;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Letf;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Letf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Letf;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Letf;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Letf;->d:Lesw;

    if-nez v0, :cond_2

    new-instance v0, Lesw;

    invoke-direct {v0}, Lesw;-><init>()V

    iput-object v0, p0, Letf;->d:Lesw;

    :cond_2
    iget-object v0, p0, Letf;->d:Lesw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Letf;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 334
    const/4 v0, 0x1

    iget-object v1, p0, Letf;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 336
    :cond_0
    iget-object v0, p0, Letf;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 337
    const/4 v0, 0x2

    iget-object v1, p0, Letf;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 339
    :cond_1
    iget-object v0, p0, Letf;->d:Lesw;

    if-eqz v0, :cond_2

    .line 340
    const/4 v0, 0x3

    iget-object v1, p0, Letf;->d:Lesw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 342
    :cond_2
    iget-object v0, p0, Letf;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 344
    return-void
.end method
