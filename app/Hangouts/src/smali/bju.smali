.class public final Lbju;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Lbdk;

.field public b:Lbdn;


# direct methods
.method constructor <init>(Lduw;)V
    .locals 4

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Lbdk;

    iget-object v1, p1, Lduw;->b:Ldui;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbdk;-><init>(Ldui;Ljava/lang/String;)V

    iput-object v0, p0, Lbju;->a:Lbdk;

    .line 21
    iget-object v0, p1, Lduw;->c:Ldut;

    if-eqz v0, :cond_1

    .line 22
    iget-object v0, p1, Lduw;->c:Ldut;

    .line 23
    new-instance v1, Lbdn;

    invoke-direct {v1, v0}, Lbdn;-><init>(Ldut;)V

    iput-object v1, p0, Lbju;->b:Lbdn;

    .line 29
    :cond_0
    :goto_0
    return-void

    .line 24
    :cond_1
    iget-object v0, p1, Lduw;->d:Lduu;

    if-eqz v0, :cond_0

    .line 25
    new-instance v0, Lbdo;

    iget-object v1, p1, Lduw;->d:Lduu;

    invoke-direct {v0, v1}, Lbdo;-><init>(Lduu;)V

    .line 26
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Received presence error for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lbju;->a:Lbdk;

    iget-object v3, v3, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Details: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Lbdo;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
