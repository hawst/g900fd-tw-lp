.class public abstract Lasr;
.super Lasw;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/hangout/ToastView;

.field private final b:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

.field private final d:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;


# direct methods
.method constructor <init>(Lcom/google/android/apps/hangouts/hangout/ToastView;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 1

    .prologue
    .line 194
    iput-object p1, p0, Lasr;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    .line 195
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lasw;-><init>(Lcom/google/android/apps/hangouts/hangout/ToastView;I)V

    .line 196
    iput-object p2, p0, Lasr;->b:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 197
    iput-object p3, p0, Lasr;->d:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 198
    return-void
.end method


# virtual methods
.method protected abstract a()[I
.end method

.method b()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 205
    iget-object v0, p0, Lasr;->b:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    if-nez v0, :cond_0

    .line 206
    const/4 v0, 0x0

    .line 224
    :goto_0
    return-object v0

    .line 208
    :cond_0
    iget-object v0, p0, Lasr;->b:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    .line 210
    iget-object v1, p0, Lasr;->d:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lasr;->d:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    iget-object v2, p0, Lasr;->b:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->hasSameMucJid(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 211
    iget-object v1, p0, Lasr;->d:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    .line 212
    iget-object v2, p0, Lasr;->b:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 213
    iget-object v0, p0, Lasr;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/ToastView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 214
    invoke-virtual {p0}, Lasr;->a()[I

    move-result-object v2

    aget v2, v2, v5

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v1, v3, v5

    .line 213
    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 216
    :cond_1
    iget-object v2, p0, Lasr;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    invoke-virtual {v2}, Lcom/google/android/apps/hangouts/hangout/ToastView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Lasr;->a()[I

    move-result-object v3

    aget v3, v3, v6

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    aput-object v0, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 219
    :cond_2
    iget-object v1, p0, Lasr;->b:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 220
    iget-object v1, p0, Lasr;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/hangout/ToastView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lasr;->a()[I

    move-result-object v2

    aget v2, v2, v4

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 222
    :cond_3
    iget-object v1, p0, Lasr;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/hangout/ToastView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lasr;->a()[I

    move-result-object v2

    const/4 v3, 0x3

    aget v2, v2, v3

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method
