.class public Lbgy;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private final i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lbgz;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ldde;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 4920
    iget-object v0, p1, Ldde;->b:Ldcw;

    invoke-direct {p0, v0}, Lbfz;-><init>(Ldcw;)V

    .line 4892
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbgy;->i:Ljava/util/ArrayList;

    .line 4922
    iget-object v0, p1, Ldde;->c:Ldgr;

    iget-object v0, v0, Ldgr;->b:Ldga;

    .line 4923
    if-eqz v0, :cond_a

    .line 4924
    iget-object v3, v0, Ldga;->b:Ljava/lang/String;

    iput-object v3, p0, Lbgy;->g:Ljava/lang/String;

    .line 4926
    iget-object v3, v0, Ldga;->f:Ldgk;

    .line 4927
    if-eqz v3, :cond_2

    .line 4928
    iget-object v4, v3, Ldgk;->g:Ldew;

    .line 4929
    if-eqz v4, :cond_0

    .line 4930
    iget-object v4, v4, Ldew;->c:[Ldev;

    array-length v5, v4

    if-lez v5, :cond_0

    aget-object v4, v4, v2

    .line 4931
    new-instance v5, Lbgz;

    invoke-direct {v5}, Lbgz;-><init>()V

    .line 4932
    iget-object v6, v4, Ldev;->b:Ljava/lang/String;

    iput-object v6, v5, Lbgz;->a:Ljava/lang/String;

    .line 4933
    iget-object v6, v4, Ldev;->c:Ljava/lang/String;

    iput-object v6, v5, Lbgz;->b:Ljava/lang/String;

    .line 4934
    iget-object v4, v4, Ldev;->d:Ldep;

    .line 4935
    if-nez v4, :cond_5

    .line 4936
    invoke-static {v7}, Lbgz;->a(Ldei;)Ljava/util/Calendar;

    move-result-object v4

    iput-object v4, v5, Lbgz;->d:Ljava/util/Calendar;

    .line 4937
    invoke-static {v7}, Lbgz;->a(Ldei;)Ljava/util/Calendar;

    move-result-object v4

    iput-object v4, v5, Lbgz;->e:Ljava/util/Calendar;

    .line 4944
    :goto_0
    iput v1, v5, Lbgz;->c:I

    .line 4945
    iget-object v4, p0, Lbgy;->i:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4946
    :cond_0
    iget-object v4, v3, Ldgk;->h:Ldeu;

    .line 4951
    if-eqz v4, :cond_1

    .line 4952
    iget-object v4, v4, Ldeu;->c:[Ldet;

    array-length v5, v4

    if-lez v5, :cond_1

    aget-object v4, v4, v2

    .line 4953
    new-instance v5, Lbgz;

    invoke-direct {v5}, Lbgz;-><init>()V

    .line 4954
    iget-object v6, v4, Ldet;->b:Ljava/lang/String;

    iput-object v6, v5, Lbgz;->a:Ljava/lang/String;

    .line 4955
    iget-object v4, v4, Ldet;->d:Ldep;

    .line 4956
    if-nez v4, :cond_6

    .line 4957
    invoke-static {v7}, Lbgz;->a(Ldei;)Ljava/util/Calendar;

    move-result-object v4

    iput-object v4, v5, Lbgz;->d:Ljava/util/Calendar;

    .line 4958
    invoke-static {v7}, Lbgz;->a(Ldei;)Ljava/util/Calendar;

    move-result-object v4

    iput-object v4, v5, Lbgz;->e:Ljava/util/Calendar;

    .line 4965
    :goto_1
    const/4 v4, 0x2

    iput v4, v5, Lbgz;->c:I

    .line 4966
    iget-object v4, p0, Lbgy;->i:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4967
    :cond_1
    iget-object v3, v3, Ldgk;->i:Ldfh;

    .line 4972
    if-eqz v3, :cond_2

    .line 4973
    iget-object v3, v3, Ldfh;->c:Ljava/lang/String;

    .line 4974
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 4975
    new-instance v4, Lbgz;

    invoke-direct {v4}, Lbgz;-><init>()V

    .line 4976
    iput-object v3, v4, Lbgz;->a:Ljava/lang/String;

    .line 4977
    const/4 v3, 0x3

    iput v3, v4, Lbgz;->c:I

    .line 4978
    iget-object v3, p0, Lbgy;->i:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4983
    :cond_2
    iget-object v0, v0, Ldga;->e:Ldek;

    .line 4984
    if-eqz v0, :cond_a

    .line 4985
    iget-object v0, v0, Ldek;->k:Ldft;

    .line 4986
    if-eqz v0, :cond_a

    .line 4987
    iget-object v3, v0, Ldft;->f:Ldfu;

    .line 4989
    if-eqz v3, :cond_7

    iget-object v0, v3, Ldfu;->b:Ljava/lang/String;

    .line 4993
    :goto_2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 4994
    new-instance v3, Landroid/net/Uri$Builder;

    invoke-direct {v3}, Landroid/net/Uri$Builder;-><init>()V

    .line 4995
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 4996
    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 4997
    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    .line 4998
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    const/4 v5, 0x6

    if-ne v0, v5, :cond_8

    .line 4999
    :goto_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_9

    .line 5000
    const/4 v0, 0x4

    if-ne v2, v0, :cond_3

    if-nez v1, :cond_4

    .line 5001
    :cond_3
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 5004
    invoke-virtual {v3, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 4999
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 4939
    :cond_5
    iget-object v6, v4, Ldep;->b:Ldei;

    .line 4940
    invoke-static {v6}, Lbgz;->a(Ldei;)Ljava/util/Calendar;

    move-result-object v6

    iput-object v6, v5, Lbgz;->d:Ljava/util/Calendar;

    .line 4941
    iget-object v4, v4, Ldep;->c:Ldei;

    .line 4942
    invoke-static {v4}, Lbgz;->a(Ldei;)Ljava/util/Calendar;

    move-result-object v4

    iput-object v4, v5, Lbgz;->e:Ljava/util/Calendar;

    goto/16 :goto_0

    .line 4960
    :cond_6
    iget-object v6, v4, Ldep;->b:Ldei;

    .line 4961
    invoke-static {v6}, Lbgz;->a(Ldei;)Ljava/util/Calendar;

    move-result-object v6

    iput-object v6, v5, Lbgz;->d:Ljava/util/Calendar;

    .line 4962
    iget-object v4, v4, Ldep;->c:Ldei;

    .line 4963
    invoke-static {v4}, Lbgz;->a(Ldei;)Ljava/util/Calendar;

    move-result-object v4

    iput-object v4, v5, Lbgz;->e:Ljava/util/Calendar;

    goto/16 :goto_1

    .line 4989
    :cond_7
    iget-object v0, v0, Ldft;->g:Ljava/lang/String;

    goto :goto_2

    :cond_8
    move v1, v2

    .line 4998
    goto :goto_3

    .line 5006
    :cond_9
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbgy;->h:Ljava/lang/String;

    .line 5010
    :cond_a
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 5026
    new-instance v0, Ldde;

    invoke-direct {v0}, Ldde;-><init>()V

    .line 5027
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldde;

    .line 5028
    iget-object v1, v0, Ldde;->b:Ldcw;

    invoke-static {v1}, Lbgy;->a(Ldcw;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5029
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldde;->b:Ldcw;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldcw;)V

    move-object v0, v1

    .line 5031
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbgy;

    invoke-direct {v1, v0}, Lbgy;-><init>(Ldde;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public f()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 5013
    iget-object v0, p0, Lbgy;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbgy;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5017
    iget-object v0, p0, Lbgy;->g:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lbgz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5021
    iget-object v0, p0, Lbgy;->i:Ljava/util/ArrayList;

    return-object v0
.end method
