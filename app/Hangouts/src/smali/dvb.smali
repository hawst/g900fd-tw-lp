.class public final Ldvb;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldvb;


# instance fields
.field public b:Ldvm;

.field public c:[Ldpj;

.field public d:[Ldsx;

.field public e:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8555
    const/4 v0, 0x0

    new-array v0, v0, [Ldvb;

    sput-object v0, Ldvb;->a:[Ldvb;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8556
    invoke-direct {p0}, Lepn;-><init>()V

    .line 8559
    const/4 v0, 0x0

    iput-object v0, p0, Ldvb;->b:Ldvm;

    .line 8562
    sget-object v0, Ldpj;->a:[Ldpj;

    iput-object v0, p0, Ldvb;->c:[Ldpj;

    .line 8565
    sget-object v0, Ldsx;->a:[Ldsx;

    iput-object v0, p0, Ldvb;->d:[Ldsx;

    .line 8556
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 8599
    iget-object v0, p0, Ldvb;->b:Ldvm;

    if-eqz v0, :cond_5

    .line 8600
    const/4 v0, 0x1

    iget-object v2, p0, Ldvb;->b:Ldvm;

    .line 8601
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8603
    :goto_0
    iget-object v2, p0, Ldvb;->c:[Ldpj;

    if-eqz v2, :cond_1

    .line 8604
    iget-object v3, p0, Ldvb;->c:[Ldpj;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 8605
    if-eqz v5, :cond_0

    .line 8606
    const/4 v6, 0x2

    .line 8607
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 8604
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 8611
    :cond_1
    iget-object v2, p0, Ldvb;->d:[Ldsx;

    if-eqz v2, :cond_3

    .line 8612
    iget-object v2, p0, Ldvb;->d:[Ldsx;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 8613
    if-eqz v4, :cond_2

    .line 8614
    const/4 v5, 0x3

    .line 8615
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 8612
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 8619
    :cond_3
    iget-object v1, p0, Ldvb;->e:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 8620
    const/4 v1, 0x4

    iget-object v2, p0, Ldvb;->e:Ljava/lang/Long;

    .line 8621
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 8623
    :cond_4
    iget-object v1, p0, Ldvb;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8624
    iput v0, p0, Ldvb;->cachedSize:I

    .line 8625
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8552
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldvb;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldvb;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldvb;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldvb;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldvb;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldvb;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldvb;->c:[Ldpj;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldpj;

    iget-object v3, p0, Ldvb;->c:[Ldpj;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldvb;->c:[Ldpj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldvb;->c:[Ldpj;

    :goto_2
    iget-object v2, p0, Ldvb;->c:[Ldpj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldvb;->c:[Ldpj;

    new-instance v3, Ldpj;

    invoke-direct {v3}, Ldpj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldvb;->c:[Ldpj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldvb;->c:[Ldpj;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldvb;->c:[Ldpj;

    new-instance v3, Ldpj;

    invoke-direct {v3}, Ldpj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldvb;->c:[Ldpj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldvb;->d:[Ldsx;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldsx;

    iget-object v3, p0, Ldvb;->d:[Ldsx;

    if-eqz v3, :cond_6

    iget-object v3, p0, Ldvb;->d:[Ldsx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Ldvb;->d:[Ldsx;

    :goto_4
    iget-object v2, p0, Ldvb;->d:[Ldsx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Ldvb;->d:[Ldsx;

    new-instance v3, Ldsx;

    invoke-direct {v3}, Ldsx;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldvb;->d:[Ldsx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Ldvb;->d:[Ldsx;

    array-length v0, v0

    goto :goto_3

    :cond_8
    iget-object v2, p0, Ldvb;->d:[Ldsx;

    new-instance v3, Ldsx;

    invoke-direct {v3}, Ldsx;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldvb;->d:[Ldsx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldvb;->e:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 8572
    iget-object v1, p0, Ldvb;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 8573
    const/4 v1, 0x1

    iget-object v2, p0, Ldvb;->b:Ldvm;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 8575
    :cond_0
    iget-object v1, p0, Ldvb;->c:[Ldpj;

    if-eqz v1, :cond_2

    .line 8576
    iget-object v2, p0, Ldvb;->c:[Ldpj;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 8577
    if-eqz v4, :cond_1

    .line 8578
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 8576
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 8582
    :cond_2
    iget-object v1, p0, Ldvb;->d:[Ldsx;

    if-eqz v1, :cond_4

    .line 8583
    iget-object v1, p0, Ldvb;->d:[Ldsx;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 8584
    if-eqz v3, :cond_3

    .line 8585
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 8583
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 8589
    :cond_4
    iget-object v0, p0, Ldvb;->e:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 8590
    const/4 v0, 0x4

    iget-object v1, p0, Ldvb;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 8592
    :cond_5
    iget-object v0, p0, Ldvb;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 8594
    return-void
.end method
