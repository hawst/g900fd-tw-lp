.class public final Lesn;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lesn;

.field public static final b:Lepo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepo",
            "<",
            "Lesn;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public c:Ljava/lang/Integer;

.field public d:Lesm;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lesn;

    sput-object v0, Lesn;->a:[Lesn;

    .line 13
    const v0, 0x2cc1e93    # 2.9992655E-37f

    new-instance v1, Leso;

    invoke-direct {v1}, Leso;-><init>()V

    .line 14
    invoke-static {v0, v1}, Lepo;->a(ILepp;)Lepo;

    move-result-object v0

    sput-object v0, Lesn;->b:Lepo;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 22
    iput-object v0, p0, Lesn;->c:Ljava/lang/Integer;

    .line 25
    iput-object v0, p0, Lesn;->d:Lesm;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 42
    const/4 v0, 0x0

    .line 43
    iget-object v1, p0, Lesn;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 44
    const/4 v0, 0x1

    iget-object v1, p0, Lesn;->c:Ljava/lang/Integer;

    .line 45
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 47
    :cond_0
    iget-object v1, p0, Lesn;->d:Lesm;

    if-eqz v1, :cond_1

    .line 48
    const/4 v1, 0x2

    iget-object v2, p0, Lesn;->d:Lesm;

    .line 49
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51
    :cond_1
    iget-object v1, p0, Lesn;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52
    iput v0, p0, Lesn;->cachedSize:I

    .line 53
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lesn;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lesn;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lesn;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lesn;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lesn;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lesn;->d:Lesm;

    if-nez v0, :cond_4

    new-instance v0, Lesm;

    invoke-direct {v0}, Lesm;-><init>()V

    iput-object v0, p0, Lesn;->d:Lesm;

    :cond_4
    iget-object v0, p0, Lesn;->d:Lesm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lesn;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x1

    iget-object v1, p0, Lesn;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 33
    :cond_0
    iget-object v0, p0, Lesn;->d:Lesm;

    if-eqz v0, :cond_1

    .line 34
    const/4 v0, 0x2

    iget-object v1, p0, Lesn;->d:Lesm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 36
    :cond_1
    iget-object v0, p0, Lesn;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 38
    return-void
.end method
