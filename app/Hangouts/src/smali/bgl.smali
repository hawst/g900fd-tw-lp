.class public Lbgl;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method private constructor <init>(Ldsi;)V
    .locals 3

    .prologue
    .line 4278
    iget-object v0, p1, Ldsi;->b:Ldvn;

    const-wide/16 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 4279
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 4280
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FinishPhoneVerificationResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4283
    :cond_0
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 4287
    new-instance v0, Ldsi;

    invoke-direct {v0}, Ldsi;-><init>()V

    .line 4288
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldsi;

    .line 4289
    iget-object v1, v0, Ldsi;->b:Ldvn;

    invoke-static {v1}, Lbgl;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4290
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldsi;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 4292
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbgl;

    invoke-direct {v1, v0}, Lbgl;-><init>(Ldsi;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 3

    .prologue
    .line 4299
    invoke-super {p0, p1, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 4302
    const/4 v0, 0x1

    invoke-static {v0}, Lbkb;->d(Z)V

    .line 4303
    invoke-static {}, Lbmz;->a()Lbmz;

    move-result-object v0

    const/16 v1, 0x68

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lbmz;->a(IZ)V

    .line 4305
    return-void
.end method
