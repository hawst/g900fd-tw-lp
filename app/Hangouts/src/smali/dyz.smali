.class public final Ldyz;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldyz;


# instance fields
.field public b:Ldvn;

.field public c:Ldyq;

.field public d:Ldzg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1740
    const/4 v0, 0x0

    new-array v0, v0, [Ldyz;

    sput-object v0, Ldyz;->a:[Ldyz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1741
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1744
    iput-object v0, p0, Ldyz;->b:Ldvn;

    .line 1747
    iput-object v0, p0, Ldyz;->c:Ldyq;

    .line 1750
    iput-object v0, p0, Ldyz;->d:Ldzg;

    .line 1741
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1770
    const/4 v0, 0x0

    .line 1771
    iget-object v1, p0, Ldyz;->b:Ldvn;

    if-eqz v1, :cond_0

    .line 1772
    const/4 v0, 0x1

    iget-object v1, p0, Ldyz;->b:Ldvn;

    .line 1773
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1775
    :cond_0
    iget-object v1, p0, Ldyz;->c:Ldyq;

    if-eqz v1, :cond_1

    .line 1776
    const/4 v1, 0x2

    iget-object v2, p0, Ldyz;->c:Ldyq;

    .line 1777
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1779
    :cond_1
    iget-object v1, p0, Ldyz;->d:Ldzg;

    if-eqz v1, :cond_2

    .line 1780
    const/4 v1, 0x3

    iget-object v2, p0, Ldyz;->d:Ldzg;

    .line 1781
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1783
    :cond_2
    iget-object v1, p0, Ldyz;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1784
    iput v0, p0, Ldyz;->cachedSize:I

    .line 1785
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1737
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldyz;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldyz;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldyz;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldyz;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldyz;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldyz;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldyz;->c:Ldyq;

    if-nez v0, :cond_3

    new-instance v0, Ldyq;

    invoke-direct {v0}, Ldyq;-><init>()V

    iput-object v0, p0, Ldyz;->c:Ldyq;

    :cond_3
    iget-object v0, p0, Ldyz;->c:Ldyq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldyz;->d:Ldzg;

    if-nez v0, :cond_4

    new-instance v0, Ldzg;

    invoke-direct {v0}, Ldzg;-><init>()V

    iput-object v0, p0, Ldyz;->d:Ldzg;

    :cond_4
    iget-object v0, p0, Ldyz;->d:Ldzg;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1755
    iget-object v0, p0, Ldyz;->b:Ldvn;

    if-eqz v0, :cond_0

    .line 1756
    const/4 v0, 0x1

    iget-object v1, p0, Ldyz;->b:Ldvn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1758
    :cond_0
    iget-object v0, p0, Ldyz;->c:Ldyq;

    if-eqz v0, :cond_1

    .line 1759
    const/4 v0, 0x2

    iget-object v1, p0, Ldyz;->c:Ldyq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1761
    :cond_1
    iget-object v0, p0, Ldyz;->d:Ldzg;

    if-eqz v0, :cond_2

    .line 1762
    const/4 v0, 0x3

    iget-object v1, p0, Ldyz;->d:Ldzg;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1764
    :cond_2
    iget-object v0, p0, Ldyz;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1766
    return-void
.end method
