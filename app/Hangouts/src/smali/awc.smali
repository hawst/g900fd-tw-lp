.class public final Lawc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lki;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)V
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lawc;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lawc;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lf;->b(Landroid/view/View;)V

    .line 257
    return-void
.end method

.method public a(Lkh;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 227
    iget-object v0, p0, Lawc;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->a(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)V

    .line 229
    iget-object v0, p0, Lawc;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->b(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    invoke-virtual {p1}, Lkh;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->a(I)V

    .line 231
    iget-object v0, p0, Lawc;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->u_()V

    .line 236
    iget-object v0, p0, Lawc;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->c(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lawc;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->d(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Z

    .line 252
    :goto_0
    return-void

    .line 241
    :cond_0
    invoke-virtual {p1}, Lkh;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 242
    const-string v2, "people"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 243
    const/16 v0, 0x609

    move v2, v0

    .line 249
    :goto_1
    if-eqz v2, :cond_3

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 250
    iget-object v0, p0, Lawc;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Lbme;

    move-result-object v0

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    goto :goto_0

    .line 244
    :cond_1
    const-string v2, "conversations"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 245
    const/16 v0, 0x60a

    move v2, v0

    goto :goto_1

    .line 246
    :cond_2
    const-string v2, "phone_calls"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 247
    const/16 v0, 0x60b

    move v2, v0

    goto :goto_1

    :cond_3
    move v0, v1

    .line 249
    goto :goto_2

    :cond_4
    move v2, v1

    goto :goto_1
.end method

.method public b()V
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lawc;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->a(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)V

    .line 262
    return-void
.end method
