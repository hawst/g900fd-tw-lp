.class public Lceb;
.super Lcax;
.source "PG"

# interfaces
.implements Laep;
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static A:Ljava/lang/String;

.field private static B:Ljava/lang/String;

.field private static C:Ljava/lang/String;

.field private static D:Ljava/lang/String;

.field private static final d:Z

.field private static z:Landroid/graphics/drawable/Drawable;


# instance fields
.field a:Lyj;

.field protected b:Z

.field protected c:I

.field private e:Lbcx;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Laea;

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:Lcec;

.field private final o:Z

.field private final p:I

.field private final q:Lt;

.field private final r:Landroid/widget/TextView;

.field private final s:Landroid/widget/TextView;

.field private final t:Landroid/widget/ImageView;

.field private final u:Landroid/text/SpannableStringBuilder;

.field private final v:Lcom/google/android/apps/hangouts/views/AvatarView;

.field private final w:Lcom/google/android/apps/hangouts/views/PresenceView;

.field private final x:Landroid/widget/ImageView;

.field private final y:Landroid/widget/LinearLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lbys;->s:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lceb;->d:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lt;ZI)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 132
    invoke-direct {p0, p1, p2}, Lcax;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    iput-boolean v2, p0, Lceb;->m:Z

    .line 81
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lceb;->u:Landroid/text/SpannableStringBuilder;

    .line 133
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lf;->gh:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 135
    sget v0, Lg;->eH:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lceb;->r:Landroid/widget/TextView;

    .line 136
    sget v0, Lg;->bk:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lceb;->s:Landroid/widget/TextView;

    .line 137
    sget v0, Lg;->bl:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lceb;->t:Landroid/widget/ImageView;

    .line 138
    sget v0, Lg;->E:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/AvatarView;

    iput-object v0, p0, Lceb;->v:Lcom/google/android/apps/hangouts/views/AvatarView;

    .line 139
    sget v0, Lg;->fw:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/PresenceView;

    iput-object v0, p0, Lceb;->w:Lcom/google/android/apps/hangouts/views/PresenceView;

    .line 140
    sget v0, Lg;->gT:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lceb;->x:Landroid/widget/ImageView;

    .line 141
    sget v0, Lg;->fl:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lceb;->y:Landroid/widget/LinearLayout;

    .line 143
    sget-object v0, Lceb;->z:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 144
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->o:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lceb;->z:Landroid/graphics/drawable/Drawable;

    .line 148
    :cond_0
    sget-object v0, Lceb;->A:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 149
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->im:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lceb;->A:Ljava/lang/String;

    .line 152
    :cond_1
    sget-object v0, Lceb;->B:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 153
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->il:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lceb;->B:Ljava/lang/String;

    .line 156
    :cond_2
    sget-object v0, Lceb;->C:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 157
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->lj:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lceb;->C:Ljava/lang/String;

    .line 161
    :cond_3
    sget-object v0, Lceb;->D:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 162
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->cA:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lceb;->D:Ljava/lang/String;

    .line 165
    :cond_4
    iput-boolean p4, p0, Lceb;->o:Z

    .line 166
    iget-boolean v0, p0, Lceb;->o:Z

    if-eqz v0, :cond_5

    .line 169
    invoke-virtual {p0, p0}, Lceb;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    :cond_5
    iput p5, p0, Lceb;->p:I

    .line 172
    iput-object p3, p0, Lceb;->q:Lt;

    .line 176
    iget-boolean v0, p0, Lceb;->o:Z

    if-eqz v0, :cond_6

    iget v0, p0, Lceb;->p:I

    invoke-static {v0}, Lf;->c(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 177
    iget-object v0, p0, Lceb;->w:Lcom/google/android/apps/hangouts/views/PresenceView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/hangouts/views/PresenceView;->setVisibility(I)V

    .line 178
    iget-object v0, p0, Lceb;->x:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 183
    :goto_0
    return-void

    .line 180
    :cond_6
    iget-object v0, p0, Lceb;->w:Lcom/google/android/apps/hangouts/views/PresenceView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/PresenceView;->setVisibility(I)V

    .line 181
    iget-object v0, p0, Lceb;->x:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lt;ZI)V
    .locals 6

    .prologue
    .line 127
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lceb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lt;ZI)V

    .line 128
    return-void
.end method

.method private b(Laeh;)V
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lceb;->n:Lcec;

    if-eqz v0, :cond_0

    .line 463
    iget-object v0, p0, Lceb;->n:Lcec;

    invoke-interface {v0, p0, p1}, Lcec;->a(Lceb;Laeh;)V

    .line 465
    :cond_0
    return-void
.end method

.method public static createInstance(Landroid/content/Context;Lt;ZI)Lceb;
    .locals 4

    .prologue
    .line 111
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 113
    :try_start_0
    const-string v0, "ced"

    .line 114
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Landroid/content/Context;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lt;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    .line 115
    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    .line 116
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lceb;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :goto_0
    return-object v0

    .line 117
    :catch_0
    move-exception v0

    .line 118
    const-string v1, "Babel"

    const-string v2, "Cannot instantiate"

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 122
    :cond_0
    new-instance v0, Lceb;

    invoke-direct {v0, p0, p1, p2, p3}, Lceb;-><init>(Landroid/content/Context;Lt;ZI)V

    goto :goto_0
.end method


# virtual methods
.method protected a(Laeh;)V
    .locals 0

    .prologue
    .line 425
    invoke-direct {p0, p1}, Lceb;->b(Laeh;)V

    .line 426
    return-void
.end method

.method public getContactDetails()Laea;
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lceb;->h:Laea;

    return-object v0
.end method

.method public getContactName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lceb;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getInviteeId()Lbcx;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lceb;->e:Lbcx;

    return-object v0
.end method

.method public getProfilePhotoUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lceb;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getWellFormedEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lceb;->j:Ljava/lang/String;

    return-object v0
.end method

.method public getWellFormedSms()Ljava/lang/String;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lceb;->k:Ljava/lang/String;

    return-object v0
.end method

.method public isDismissable()Z
    .locals 1

    .prologue
    .line 217
    iget-boolean v0, p0, Lceb;->m:Z

    return v0
.end method

.method public isLocked(Laoe;)Z
    .locals 3

    .prologue
    .line 485
    iget-object v0, p0, Lceb;->e:Lbcx;

    iget-object v0, v0, Lbcx;->a:Ljava/lang/String;

    iget-object v1, p0, Lceb;->e:Lbcx;

    iget-object v1, v1, Lbcx;->d:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Laoe;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isPlusPage()Z
    .locals 1

    .prologue
    .line 254
    iget-boolean v0, p0, Lceb;->i:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 288
    iget-boolean v1, p0, Lceb;->o:Z

    if-nez v1, :cond_5

    .line 289
    invoke-virtual {p0}, Lceb;->isChecked()Z

    move-result v1

    if-nez v1, :cond_3

    iget v1, p0, Lceb;->p:I

    invoke-static {v1}, Lf;->c(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 291
    iget-object v1, p0, Lceb;->h:Laea;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lceb;->h:Laea;

    invoke-virtual {v0}, Laea;->j()Ljava/util/ArrayList;

    move-result-object v0

    .line 293
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 294
    :cond_1
    invoke-virtual {p0}, Lceb;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lh;->it:I

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 295
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 329
    :goto_0
    return-void

    .line 299
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 301
    iget-object v1, p0, Lceb;->h:Laea;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laee;

    invoke-virtual {v1, v0}, Laea;->a(Laee;)V

    .line 313
    :cond_3
    invoke-super {p0, p1}, Lcax;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 304
    :cond_4
    new-instance v0, Laen;

    iget-object v1, p0, Lceb;->q:Lt;

    iget-object v2, p0, Lceb;->a:Lyj;

    iget-object v3, p0, Lceb;->h:Laea;

    iget v5, p0, Lceb;->p:I

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Laen;-><init>(Lt;Lyj;Laea;Laep;I)V

    .line 306
    invoke-virtual {v0}, Laen;->b()V

    goto :goto_0

    .line 317
    :cond_5
    iget v1, p0, Lceb;->p:I

    invoke-static {v1}, Lf;->c(I)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 318
    iget-object v1, p0, Lceb;->h:Laea;

    if-eqz v1, :cond_6

    iget-object v0, p0, Lceb;->h:Laea;

    invoke-virtual {v0}, Laea;->h()Laeh;

    move-result-object v0

    .line 319
    :cond_6
    if-nez v0, :cond_7

    .line 320
    invoke-virtual {p0}, Lceb;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lh;->it:I

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 321
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 324
    :cond_7
    invoke-direct {p0, v0}, Lceb;->b(Laeh;)V

    goto :goto_0

    .line 326
    :cond_8
    invoke-direct {p0, v0}, Lceb;->b(Laeh;)V

    goto :goto_0
.end method

.method public onContactLookupComplete(Laea;Laee;)V
    .locals 3

    .prologue
    .line 436
    iget-object v0, p0, Lceb;->h:Laea;

    if-eq p1, v0, :cond_0

    .line 437
    const-string v0, "Babel"

    const-string v1, "Call back is called on a different ContactDetails object."

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    sget-boolean v0, Lceb;->d:Z

    if-eqz v0, :cond_0

    .line 439
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContactDetails for current view: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lceb;->h:Laea;

    invoke-virtual {v2}, Laea;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContactDetails for call back: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Laea;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    :cond_0
    if-nez p2, :cond_1

    .line 459
    :goto_0
    return-void

    .line 450
    :cond_1
    instance-of v0, p2, Laeh;

    if-nez v0, :cond_2

    .line 451
    const-string v0, "Babel"

    const-string v1, "Selected contact detail item should be a phone."

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 455
    :cond_2
    iget-object v0, p0, Lceb;->h:Laea;

    invoke-virtual {v0, p2}, Laea;->a(Laee;)V

    .line 458
    invoke-super {p0, p0}, Lcax;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method public reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 469
    invoke-super {p0}, Lcax;->reset()V

    .line 470
    iget-object v0, p0, Lceb;->t:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 471
    iget-object v0, p0, Lceb;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 472
    iget-object v0, p0, Lceb;->y:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 475
    iget-boolean v0, p0, Lceb;->o:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lceb;->p:I

    invoke-static {v0}, Lf;->c(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 476
    :cond_0
    iget-object v0, p0, Lceb;->w:Lcom/google/android/apps/hangouts/views/PresenceView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/PresenceView;->a()V

    .line 478
    :cond_1
    iput-object v2, p0, Lceb;->e:Lbcx;

    .line 479
    iput-object v2, p0, Lceb;->h:Laea;

    .line 480
    const/4 v0, 0x0

    iput-boolean v0, p0, Lceb;->m:Z

    .line 481
    return-void
.end method

.method public resumeLoading()V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lceb;->v:Lcom/google/android/apps/hangouts/views/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/AvatarView;->a()V

    .line 226
    return-void
.end method

.method public setContactDetails(Laea;Z)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 387
    iput-object p1, p0, Lceb;->h:Laea;

    .line 389
    invoke-virtual {p1}, Laea;->j()Ljava/util/ArrayList;

    move-result-object v1

    .line 390
    invoke-virtual {p1}, Laea;->k()Ljava/util/ArrayList;

    move-result-object v0

    .line 391
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 413
    :cond_0
    :goto_0
    return-void

    .line 395
    :cond_1
    if-eqz p2, :cond_3

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 396
    iget-object v0, p0, Lceb;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 397
    iget-object v2, p0, Lceb;->s:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeh;

    iget-object v0, v0, Laeh;->a:Ljava/lang/String;

    invoke-static {v0}, Lbzd;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 398
    iget-object v0, p0, Lceb;->t:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 401
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 410
    :cond_2
    :goto_1
    iget-boolean v0, p0, Lceb;->o:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lceb;->p:I

    invoke-static {v0}, Lf;->d(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 411
    iget-object v0, p0, Lceb;->y:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lceb;->l:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2}, Lceb;->a(Landroid/view/ViewGroup;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_0

    .line 402
    :cond_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 403
    iget-object v2, p0, Lceb;->s:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 404
    iget-object v2, p0, Lceb;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laef;

    iget-object v0, v0, Laef;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 405
    iget-object v0, p0, Lceb;->t:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public setContactName(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 241
    iput-object p1, p0, Lceb;->g:Ljava/lang/String;

    .line 242
    iget-object v0, p0, Lceb;->r:Landroid/widget/TextView;

    iget-object v1, p0, Lceb;->u:Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, Lceb;->l:Ljava/lang/String;

    invoke-virtual {p0, v0, p1, v1, v2}, Lceb;->a(Landroid/widget/TextView;Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V

    .line 243
    return-void
.end method

.method public setDetails(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 367
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 368
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 369
    iget-object v1, p0, Lceb;->t:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 370
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 373
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 374
    sget-object v1, Lceb;->D:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 376
    :cond_1
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 378
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 379
    iget-object v1, p0, Lceb;->s:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 380
    iget-object v1, p0, Lceb;->s:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 382
    :cond_3
    return-void
.end method

.method public setDismissable(Z)V
    .locals 0

    .prologue
    .line 210
    iput-boolean p1, p0, Lceb;->m:Z

    .line 211
    return-void
.end method

.method public setHighlightedText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 233
    if-nez p1, :cond_0

    .line 234
    const/4 v0, 0x0

    iput-object v0, p0, Lceb;->l:Ljava/lang/String;

    .line 238
    :goto_0
    return-void

    .line 236
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lceb;->l:Ljava/lang/String;

    goto :goto_0
.end method

.method public setInviteeId(Lbcx;Ljava/lang/String;ZLyj;ZZ)V
    .locals 3

    .prologue
    .line 188
    iput-object p2, p0, Lceb;->f:Ljava/lang/String;

    .line 189
    iput-object p4, p0, Lceb;->a:Lyj;

    .line 190
    iget-object v0, p0, Lceb;->e:Lbcx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lceb;->e:Lbcx;

    invoke-virtual {v0, p1}, Lbcx;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 191
    :cond_0
    iput-object p1, p0, Lceb;->e:Lbcx;

    .line 192
    iget-object v0, p0, Lceb;->v:Lcom/google/android/apps/hangouts/views/AvatarView;

    invoke-virtual {v0, p2, p4, p5}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Ljava/lang/String;Lyj;Z)V

    .line 196
    :cond_1
    iget-boolean v0, p0, Lceb;->o:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lceb;->p:I

    invoke-static {v0}, Lf;->c(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 197
    :cond_2
    if-eqz p6, :cond_5

    iget-object v0, p0, Lceb;->e:Lbcx;

    if-eqz v0, :cond_5

    .line 198
    iget-object v1, p0, Lceb;->w:Lcom/google/android/apps/hangouts/views/PresenceView;

    iget-object v0, p0, Lceb;->e:Lbcx;

    invoke-static {v0}, Lbdk;->a(Lbcx;)Lbdk;

    move-result-object v2

    iget-object v0, p1, Lbcx;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0, p4}, Lcom/google/android/apps/hangouts/views/PresenceView;->a(Lbdk;ZLyj;)V

    .line 204
    :cond_3
    :goto_1
    return-void

    .line 198
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 201
    :cond_5
    iget-object v0, p0, Lceb;->w:Lcom/google/android/apps/hangouts/views/PresenceView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/PresenceView;->a()V

    goto :goto_1
.end method

.method public setPersonSelectedListener(Lcec;)V
    .locals 0

    .prologue
    .line 420
    iput-object p1, p0, Lceb;->n:Lcec;

    .line 421
    return-void
.end method

.method public setPlusPage(Z)V
    .locals 0

    .prologue
    .line 250
    iput-boolean p1, p0, Lceb;->i:Z

    .line 251
    return-void
.end method

.method public setWellFormedEmail(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 262
    iput-object p1, p0, Lceb;->j:Ljava/lang/String;

    .line 263
    iget-object v0, p0, Lceb;->r:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 264
    return-void
.end method

.method public setWellFormedSms(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 275
    iput-object p1, p0, Lceb;->k:Ljava/lang/String;

    .line 276
    iget-object v0, p0, Lceb;->r:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 277
    return-void
.end method

.method public updateContentDescription()V
    .locals 2

    .prologue
    .line 335
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 336
    iget-object v0, p0, Lceb;->r:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lceb;->r:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 342
    :cond_0
    iget-boolean v0, p0, Lceb;->o:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lceb;->p:I

    invoke-static {v0}, Lf;->c(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 343
    :cond_1
    iget-object v0, p0, Lceb;->w:Lcom/google/android/apps/hangouts/views/PresenceView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/PresenceView;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 344
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 345
    sget-object v0, Lceb;->D:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 347
    :cond_2
    iget-object v0, p0, Lceb;->w:Lcom/google/android/apps/hangouts/views/PresenceView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/PresenceView;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lceb;->A:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 351
    :cond_3
    iget-object v0, p0, Lceb;->w:Lcom/google/android/apps/hangouts/views/PresenceView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/PresenceView;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 352
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 353
    sget-object v0, Lceb;->D:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 355
    :cond_4
    sget-object v0, Lceb;->C:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 358
    :cond_5
    iget-object v0, p0, Lceb;->r:Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 359
    return-void

    .line 347
    :cond_6
    sget-object v0, Lceb;->B:Ljava/lang/String;

    goto :goto_0
.end method
