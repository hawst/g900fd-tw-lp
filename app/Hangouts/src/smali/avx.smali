.class public final Lavx;
.super Landroid/os/Handler;
.source "PG"


# instance fields
.field private a:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;)V
    .locals 0

    .prologue
    .line 294
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 295
    iput-object p1, p0, Lavx;->a:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;

    .line 296
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 300
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 313
    :goto_0
    return-void

    .line 302
    :pswitch_0
    iget-object v0, p0, Lavx;->a:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;->c(Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;)V

    .line 303
    iget-object v0, p0, Lavx;->a:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    .line 304
    iget-object v0, p0, Lavx;->a:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;->e()Z

    .line 305
    iget-object v0, p0, Lavx;->a:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;->dismissDialog(I)V

    .line 306
    iget-object v0, p0, Lavx;->a:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;

    iget-object v1, p0, Lavx;->a:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;

    .line 307
    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lh;->kE:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 306
    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 310
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 300
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
