.class public final Ldse;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldse;


# instance fields
.field public b:Ldui;

.field public c:Ldqf;

.field public d:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20533
    const/4 v0, 0x0

    new-array v0, v0, [Ldse;

    sput-object v0, Ldse;->a:[Ldse;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20534
    invoke-direct {p0}, Lepn;-><init>()V

    .line 20537
    iput-object v0, p0, Ldse;->b:Ldui;

    .line 20540
    iput-object v0, p0, Ldse;->c:Ldqf;

    .line 20534
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 20562
    const/4 v0, 0x0

    .line 20563
    iget-object v1, p0, Ldse;->b:Ldui;

    if-eqz v1, :cond_0

    .line 20564
    const/4 v0, 0x1

    iget-object v1, p0, Ldse;->b:Ldui;

    .line 20565
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 20567
    :cond_0
    iget-object v1, p0, Ldse;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 20568
    const/4 v1, 0x2

    iget-object v2, p0, Ldse;->d:Ljava/lang/Integer;

    .line 20569
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 20571
    :cond_1
    iget-object v1, p0, Ldse;->c:Ldqf;

    if-eqz v1, :cond_2

    .line 20572
    const/4 v1, 0x3

    iget-object v2, p0, Ldse;->c:Ldqf;

    .line 20573
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20575
    :cond_2
    iget-object v1, p0, Ldse;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20576
    iput v0, p0, Ldse;->cachedSize:I

    .line 20577
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 20530
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldse;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldse;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldse;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldse;->b:Ldui;

    if-nez v0, :cond_2

    new-instance v0, Ldui;

    invoke-direct {v0}, Ldui;-><init>()V

    iput-object v0, p0, Ldse;->b:Ldui;

    :cond_2
    iget-object v0, p0, Ldse;->b:Ldui;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldse;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldse;->c:Ldqf;

    if-nez v0, :cond_3

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldse;->c:Ldqf;

    :cond_3
    iget-object v0, p0, Ldse;->c:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 20547
    iget-object v0, p0, Ldse;->b:Ldui;

    if-eqz v0, :cond_0

    .line 20548
    const/4 v0, 0x1

    iget-object v1, p0, Ldse;->b:Ldui;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 20550
    :cond_0
    iget-object v0, p0, Ldse;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 20551
    const/4 v0, 0x2

    iget-object v1, p0, Ldse;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 20553
    :cond_1
    iget-object v0, p0, Ldse;->c:Ldqf;

    if-eqz v0, :cond_2

    .line 20554
    const/4 v0, 0x3

    iget-object v1, p0, Ldse;->c:Ldqf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 20556
    :cond_2
    iget-object v0, p0, Ldse;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 20558
    return-void
.end method
