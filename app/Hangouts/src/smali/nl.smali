.class final Lnl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lnk;


# instance fields
.field final synthetic a:Lni;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lnj;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lni;)V
    .locals 1

    .prologue
    .line 930
    iput-object p1, p0, Lnl;->a:Lni;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 933
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lnl;->b:Ljava/util/Map;

    return-void
.end method

.method synthetic constructor <init>(Lni;B)V
    .locals 0

    .prologue
    .line 930
    invoke-direct {p0, p1}, Lnl;-><init>(Lni;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lnj;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lnm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 938
    iget-object v4, p0, Lnl;->b:Ljava/util/Map;

    .line 940
    invoke-interface {v4}, Ljava/util/Map;->clear()V

    .line 942
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 943
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 944
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnj;

    .line 945
    const/4 v3, 0x0

    iput v3, v0, Lnj;->b:F

    .line 946
    iget-object v3, v0, Lnj;->a:Landroid/content/pm/ResolveInfo;

    iget-object v3, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 947
    invoke-interface {v4, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 943
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 950
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 951
    const/high16 v2, 0x3f800000    # 1.0f

    move v3, v0

    .line 952
    :goto_1
    if-ltz v3, :cond_1

    .line 953
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnm;

    .line 954
    iget-object v1, v0, Lnm;->a:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 955
    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnj;

    .line 956
    if-eqz v1, :cond_2

    .line 957
    iget v5, v1, Lnj;->b:F

    iget v0, v0, Lnm;->c:F

    mul-float/2addr v0, v2

    add-float/2addr v0, v5

    iput v0, v1, Lnj;->b:F

    .line 958
    const v0, 0x3f733333    # 0.95f

    mul-float/2addr v0, v2

    .line 952
    :goto_2
    add-int/lit8 v1, v3, -0x1

    move v3, v1

    move v2, v0

    goto :goto_1

    .line 962
    :cond_1
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 969
    return-void

    :cond_2
    move v0, v2

    goto :goto_2
.end method
