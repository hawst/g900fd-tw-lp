.class public final Labu;
.super Lalw;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lalw",
        "<",
        "Labw;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/View;Labw;)V
    .locals 1

    .prologue
    .line 33
    sget v0, Lg;->P:I

    invoke-direct {p0, p1, v0, p2}, Lalw;-><init>(Landroid/view/View;ILjava/lang/Object;)V

    .line 34
    return-void
.end method


# virtual methods
.method protected a()Z
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Labu;->b:Ljava/lang/Object;

    check-cast v0, Labw;

    invoke-interface {v0}, Labw;->h()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b()V
    .locals 9

    .prologue
    const/16 v6, 0x8

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 69
    iget-object v0, p0, Labu;->a:Landroid/view/ViewGroup;

    sget v1, Lg;->Q:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 70
    iget-object v1, p0, Labu;->a:Landroid/view/ViewGroup;

    sget v2, Lg;->R:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 71
    iget-object v2, p0, Labu;->a:Landroid/view/ViewGroup;

    sget v3, Lg;->hU:I

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 72
    iget-object v2, p0, Labu;->a:Landroid/view/ViewGroup;

    sget v4, Lg;->S:I

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 73
    iget-object v2, p0, Labu;->b:Ljava/lang/Object;

    check-cast v2, Labw;

    invoke-interface {v2}, Labw;->h()I

    move-result v2

    if-ne v2, v7, :cond_1

    .line 74
    iget-object v2, p0, Labu;->b:Ljava/lang/Object;

    check-cast v2, Labw;

    invoke-interface {v2}, Labw;->g()Lbdh;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 75
    iget-object v2, p0, Labu;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lh;->O:I

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v2, p0, Labu;->b:Ljava/lang/Object;

    check-cast v2, Labw;

    .line 76
    invoke-interface {v2}, Labw;->g()Lbdh;

    move-result-object v2

    iget-object v2, v2, Lbdh;->e:Ljava/lang/String;

    aput-object v2, v7, v8

    .line 75
    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    :goto_0
    iget-object v0, p0, Labu;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v2, Lh;->P:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 83
    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 90
    :goto_1
    return-void

    .line 79
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 85
    :cond_1
    iget-object v2, p0, Labu;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v5, Lh;->K:I

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v0, p0, Labu;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v2, Lh;->L:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 88
    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method protected c()V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Labu;->a:Landroid/view/ViewGroup;

    sget v1, Lg;->hU:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 39
    new-instance v1, Labv;

    invoke-direct {v1, p0}, Labv;-><init>(Labu;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    return-void
.end method
