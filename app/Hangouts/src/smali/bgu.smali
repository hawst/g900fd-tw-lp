.class public Lbgu;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ldzb;)V
    .locals 3

    .prologue
    .line 3774
    invoke-direct {p0}, Lbfz;-><init>()V

    .line 3775
    iget-object v0, p1, Ldzb;->c:Ljava/lang/String;

    iput-object v0, p0, Lbgu;->g:Ljava/lang/String;

    .line 3776
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 3777
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetHangoutIdResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3779
    :cond_0
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 3783
    new-instance v0, Ldzb;

    invoke-direct {v0}, Ldzb;-><init>()V

    .line 3784
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldzb;

    .line 3785
    iget-object v1, v0, Ldzb;->b:Ldvn;

    invoke-static {v1}, Lbgu;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3786
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldzb;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 3788
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbgu;

    invoke-direct {v1, v0}, Lbgu;-><init>(Ldzb;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3793
    iget-object v0, p0, Lbgu;->g:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3797
    iget-object v0, p0, Lbgu;->b:Lbea;

    check-cast v0, Lbdt;

    iget-object v0, v0, Lbdt;->a:Ljava/lang/String;

    return-object v0
.end method
