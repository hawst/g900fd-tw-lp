.class public final Lamz;
.super Labe;
.source "PG"

# interfaces
.implements Labf;


# instance fields
.field private final Z:Lyj;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lyj;)V
    .locals 3

    .prologue
    .line 21
    invoke-direct {p0}, Labe;-><init>()V

    .line 22
    iput-object p2, p0, Lamz;->Z:Lyj;

    .line 24
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 25
    const-string v1, "title"

    sget v2, Lh;->kB:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    const-string v1, "message"

    sget v2, Lh;->kA:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    const-string v1, "positive"

    sget v2, Lh;->au:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    const-string v1, "negative"

    sget v2, Lh;->ab:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    invoke-virtual {p0, v0}, Lamz;->setArguments(Landroid/os/Bundle;)V

    .line 30
    const/4 v0, 0x0

    invoke-virtual {p0, p0, v0}, Lamz;->setTargetFragment(Lt;I)V

    .line 31
    return-void
.end method


# virtual methods
.method public a(Lae;)V
    .locals 1

    .prologue
    .line 34
    const-string v0, "clear_recent_calls"

    invoke-virtual {p0, p1, v0}, Lamz;->a(Lae;Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 39
    const-string v0, "clear_recent_calls"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lamz;->Z:Lyj;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Lyj;)V

    .line 42
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 46
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 54
    return-void
.end method
