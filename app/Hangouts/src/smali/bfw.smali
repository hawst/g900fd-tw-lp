.class public Lbfw;
.super Lbep;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:[Lbfy;

.field public final c:I

.field public final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbfx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;[Lbfy;ILjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Lbfy;",
            "I",
            "Ljava/util/List",
            "<",
            "Lbfx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3209
    invoke-direct {p0}, Lbep;-><init>()V

    .line 3210
    iput-object p1, p0, Lbfw;->a:Ljava/lang/String;

    .line 3211
    iput-object p2, p0, Lbfw;->b:[Lbfy;

    .line 3212
    iput p3, p0, Lbfw;->c:I

    .line 3213
    iput-object p4, p0, Lbfw;->h:Ljava/util/List;

    .line 3214
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 3218
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 3219
    const-string v0, "Babel_RequestWriter"

    const-string v2, "Build proto for UploadAnalyticsRequest"

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3222
    :cond_0
    new-instance v4, Ldvb;

    invoke-direct {v4}, Ldvb;-><init>()V

    .line 3224
    const/4 v0, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2, p1, p2}, Lbfw;->a(Ldpt;ZLjava/lang/String;I)Ldvm;

    move-result-object v0

    iput-object v0, v4, Ldvb;->b:Ldvm;

    .line 3226
    iget-object v0, p0, Lbfw;->b:[Lbfy;

    if-eqz v0, :cond_2

    iget v0, p0, Lbfw;->c:I

    if-lez v0, :cond_2

    .line 3227
    iget v0, p0, Lbfw;->c:I

    new-array v0, v0, [Ldsx;

    iput-object v0, v4, Ldvb;->d:[Ldsx;

    move v0, v1

    move v2, v1

    .line 3228
    :goto_0
    iget-object v3, p0, Lbfw;->b:[Lbfy;

    array-length v3, v3

    if-ge v2, v3, :cond_2

    iget v3, p0, Lbfw;->c:I

    if-ge v0, v3, :cond_2

    .line 3230
    iget-object v3, p0, Lbfw;->b:[Lbfy;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lbfy;->b()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v3, v5, v7

    if-lez v3, :cond_1

    .line 3231
    iget-object v5, v4, Ldvb;->d:[Ldsx;

    add-int/lit8 v3, v0, 0x1

    iget-object v6, p0, Lbfw;->b:[Lbfy;

    aget-object v6, v6, v2

    invoke-virtual {v6}, Lbfy;->c()Ldsx;

    move-result-object v6

    aput-object v6, v5, v0

    move v0, v3

    .line 3229
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 3235
    :cond_2
    iget-object v0, p0, Lbfw;->h:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbfw;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 3236
    iget-object v0, p0, Lbfw;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ldpj;

    iput-object v0, v4, Ldvb;->c:[Ldpj;

    .line 3238
    iget-object v0, p0, Lbfw;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbfx;

    .line 3239
    iget-object v3, v4, Ldvb;->c:[Ldpj;

    invoke-virtual {v0}, Lbfx;->a()Ldpj;

    move-result-object v0

    aput-object v0, v3, v1

    .line 3240
    add-int/lit8 v1, v1, 0x1

    .line 3241
    goto :goto_1

    .line 3243
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Ldvb;->e:Ljava/lang/Long;

    .line 3244
    return-object v4
.end method

.method public a(Lyj;Lbph;)V
    .locals 4

    .prologue
    .line 3259
    iget-object v0, p0, Lbfw;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3260
    iget-object v0, p0, Lbfw;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->v(Lyj;Ljava/lang/String;)V

    .line 3262
    :cond_0
    invoke-static {p1}, Lbqw;->a(Lyj;)Lbqw;

    move-result-object v0

    .line 3263
    invoke-virtual {v0}, Lbqw;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3264
    const-string v1, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 3265
    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3264
    invoke-static {v1, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 3266
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbqw;->a(I)V

    .line 3268
    :cond_1
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3249
    const-string v0, "analytics/recordanalyticsevents"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3254
    const-string v0, "background_queue"

    return-object v0
.end method
