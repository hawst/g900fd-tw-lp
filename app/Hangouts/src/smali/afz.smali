.class public final Lafz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcbl;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 0

    .prologue
    .line 5471
    iput-object p1, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 5674
    iget-object v0, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d()I

    move-result v0

    return v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 5645
    iget-object v0, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    new-instance v1, Lagb;

    invoke-direct {v1, p0, p1}, Lagb;-><init>(Lafz;I)V

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lahn;)V

    .line 5664
    return-void
.end method

.method public a(Lajk;)V
    .locals 1

    .prologue
    .line 5684
    iget-object v0, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lajk;)V

    .line 5685
    return-void
.end method

.method public a(Landroid/widget/EditText;)V
    .locals 1

    .prologue
    .line 5640
    iget-object v0, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->j(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lahm;

    move-result-object v0

    invoke-interface {v0, p1}, Lahm;->a(Landroid/widget/EditText;)V

    .line 5641
    return-void
.end method

.method public a(Lcom/google/android/gms/maps/model/MarkerOptions;Lcom/google/android/gms/maps/model/CameraPosition;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 5705
    const-string v0, "Babel"

    const-string v1, "onShareLocation"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5706
    iget-object v0, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Y(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/SquareMapView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 5708
    const-string v0, "Babel"

    const-string v1, "Confirmation map not ready, set it up"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 5709
    iget-object v0, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ak(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    .line 5712
    :cond_0
    iget-object v0, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0, p1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 5713
    iget-object v0, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0, p2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/maps/model/CameraPosition;

    .line 5715
    iget-object v0, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->F(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(I)V

    .line 5716
    iget-object v0, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->al(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 5717
    iget-object v0, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 5718
    iget-object v0, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 5719
    iget-object v0, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Y(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/SquareMapView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/SquareMapView;->setVisibility(I)V

    .line 5721
    iget-object v0, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Y(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/SquareMapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/SquareMapView;->a()Lcox;

    move-result-object v0

    .line 5722
    invoke-virtual {v0}, Lcox;->c()V

    .line 5723
    invoke-static {p2}, Lf;->a(Lcom/google/android/gms/maps/model/CameraPosition;)Lcow;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcox;->a(Lcow;)V

    .line 5724
    invoke-virtual {v0, p1}, Lcox;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcsx;

    .line 5725
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 18

    .prologue
    .line 5474
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->T(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Z

    .line 5475
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->U(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    .line 5477
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->v(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    .line 5486
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyj;

    move-result-object v4

    .line 5487
    if-nez v4, :cond_0

    .line 5488
    const-string v1, "Babel"

    const-string v2, "onSendTextMessage with null account; bailing"

    invoke-static {v1, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 5620
    :goto_0
    return-void

    .line 5493
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Q()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->B(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/EasterEggView;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 5494
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/hangouts/views/EasterEggView;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 5495
    if-eqz v1, :cond_2

    .line 5496
    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/EasterEggView;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 5498
    move-object/from16 v0, p0

    iget-object v2, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyj;

    move-result-object v2

    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->k(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 5503
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->B(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/EasterEggView;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyj;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v4}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyj;

    move-result-object v4

    invoke-virtual {v4}, Lyj;->c()Lbdk;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/apps/hangouts/views/EasterEggView;->a(Lyj;Ljava/lang/String;)V

    goto :goto_0

    .line 5511
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->V(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 5512
    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->W(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lbwt;

    move-result-object v1

    invoke-virtual {v1}, Lbwt;->f()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    .line 5513
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->M(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->X(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)I

    move-result v3

    invoke-static {v3}, Lf;->d(I)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-static {v2}, Lf;->d(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-static {v2}, Lf;->c(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-static {v2}, Lf;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_3
    const/4 v2, 0x0

    :goto_2
    if-eqz v2, :cond_4

    if-eqz v1, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 5514
    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->R()Z

    move-result v1

    if-nez v1, :cond_8

    .line 5515
    :cond_4
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 5516
    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->R()Z

    move-result v1

    if-eqz v1, :cond_7

    sget v1, Lh;->gB:I

    :goto_3
    const/4 v3, 0x1

    .line 5515
    invoke-static {v2, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    .line 5520
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 5512
    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    .line 5513
    :cond_6
    const/4 v2, 0x1

    goto :goto_2

    .line 5516
    :cond_7
    sget v1, Lh;->gA:I

    goto :goto_3

    .line 5524
    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v1, v1, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    check-cast v1, Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v3

    .line 5525
    add-int/lit8 v2, v3, -0x14

    .line 5531
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v1, v1, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    check-cast v1, Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v1

    if-ge v1, v2, :cond_9

    .line 5532
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v1, v1, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    check-cast v1, Landroid/widget/ListView;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    .line 5535
    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Y(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/SquareMapView;

    move-result-object v1

    if-eqz v1, :cond_b

    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Y(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/SquareMapView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/views/SquareMapView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_b

    .line 5536
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    const-string v2, "hangouts/location"

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->e(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 5537
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->R()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 5539
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    move-object/from16 v0, p0

    iget-object v2, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Z(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->f(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 5540
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->G(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_a

    .line 5542
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->e(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 5545
    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Y(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/SquareMapView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/views/SquareMapView;->a()Lcox;

    move-result-object v1

    .line 5546
    move-object/from16 v0, p0

    iget-object v2, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v1}, Lcox;->b()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/maps/model/CameraPosition;

    .line 5550
    :cond_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v1, v1, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    check-cast v1, Landroid/widget/ListView;

    add-int/lit8 v5, v3, -0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v2, v2, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    check-cast v2, Landroid/widget/ListView;

    .line 5551
    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v2

    sub-int v2, v5, v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 5552
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v1, v1, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    check-cast v1, Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeight()I

    move-result v1

    .line 5554
    if-lez v3, :cond_e

    if-eqz v2, :cond_c

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    if-le v2, v1, :cond_e

    .line 5555
    :cond_c
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->G(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v8

    .line 5556
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aa(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)I

    move-result v9

    .line 5557
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ab(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v10

    .line 5558
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ac(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)I

    move-result v11

    .line 5559
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ad(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)I

    move-result v12

    .line 5560
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->W(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lbwt;

    move-result-object v1

    invoke-virtual {v1}, Lbwt;->c()Ljava/lang/String;

    move-result-object v5

    .line 5561
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->W(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lbwt;

    move-result-object v1

    invoke-virtual {v1}, Lbwt;->f()Z

    move-result v14

    .line 5562
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->M(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v13

    .line 5563
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->V(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v3

    .line 5564
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ae(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)I

    move-result v6

    .line 5565
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->af(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v15

    .line 5566
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ag(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v16

    .line 5567
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v1, v1, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    check-cast v1, Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v2, v2, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    check-cast v2, Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    .line 5571
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->q(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/os/Handler;

    move-result-object v17

    new-instance v1, Laga;

    move-object/from16 v2, p0

    move-object/from16 v7, p1

    invoke-direct/range {v1 .. v16}, Laga;-><init>(Lafz;Ljava/lang/String;Lyj;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;ZLcom/google/android/gms/maps/model/MarkerOptions;Lcom/google/android/gms/maps/model/CameraPosition;)V

    const-wide/16 v2, 0x1f4

    move-object/from16 v0, v17

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 5603
    :goto_4
    invoke-static {}, Lavy;->f()V

    .line 5604
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    move-object/from16 v0, p0

    iget-object v2, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v2, v2, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->c:Lahp;

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lahp;)V

    .line 5605
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->G(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_d

    .line 5606
    invoke-static {}, Lavy;->g()V

    .line 5608
    :cond_d
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->W(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lbwt;

    move-result-object v1

    invoke-virtual {v1}, Lbwt;->b()V

    .line 5609
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Z)V

    .line 5610
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->L()V

    .line 5611
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    new-instance v2, Lyh;

    invoke-direct {v2}, Lyh;-><init>()V

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lyh;)V

    .line 5612
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v1

    invoke-virtual {v1}, Ly;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "share_intent"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 5613
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->G(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d(Ljava/lang/String;)V

    .line 5614
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    .line 5618
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ai(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Z

    .line 5619
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->U(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    goto/16 :goto_0

    .line 5588
    :cond_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->V(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_f

    .line 5589
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 5590
    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->k(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->V(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 5591
    invoke-static {v3}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->W(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lbwt;

    move-result-object v3

    invoke-virtual {v3}, Lbwt;->c()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 5592
    invoke-static {v5}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ae(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)I

    move-result v5

    .line 5589
    invoke-static {v4, v1, v2, v3, v5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I

    goto/16 :goto_4

    .line 5594
    :cond_f
    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v1

    invoke-virtual {v1}, Ly;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lbqz;

    invoke-static {v1, v2}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbqz;

    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 5595
    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->k(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->G(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aa(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)I

    move-result v8

    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 5596
    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ab(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ac(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)I

    move-result v10

    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ad(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)I

    move-result v11

    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 5597
    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->M(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->W(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lbwt;

    move-result-object v1

    invoke-virtual {v1}, Lbwt;->c()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 5598
    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->W(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lbwt;

    move-result-object v1

    invoke-virtual {v1}, Lbwt;->f()Z

    move-result v14

    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->af(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v1, v0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ag(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v16

    const/16 v17, 0x0

    move-object/from16 v6, p1

    .line 5594
    invoke-interface/range {v3 .. v17}, Lbqz;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;Ljava/lang/String;ZLcom/google/android/gms/maps/model/MarkerOptions;Lcom/google/android/gms/maps/model/CameraPosition;I)V

    goto/16 :goto_4
.end method

.method public a(Ljava/lang/String;JLjava/lang/String;I)V
    .locals 7

    .prologue
    .line 5669
    iget-object v0, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    const/4 v6, 0x0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move v5, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;JLjava/lang/String;ILyh;)V

    .line 5670
    return-void
.end method

.method public b()Lajk;
    .locals 1

    .prologue
    .line 5679
    iget-object v0, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lajk;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 5689
    iget-object v0, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->S()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5690
    iget-object v0, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Z)V

    .line 5692
    :cond_0
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 5696
    iget-object v0, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->S()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5697
    iget-object v0, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->W(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lbwt;

    move-result-object v0

    invoke-virtual {v0}, Lbwt;->f()Z

    move-result v0

    .line 5699
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5729
    iget-object v0, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ao(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 5734
    iget-object v0, p0, Lafz;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->J()V

    .line 5735
    return-void
.end method
