.class public final Ldrl;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldrl;


# instance fields
.field public b:Ldvm;

.field public c:Ldqf;

.field public d:Ldrj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7480
    const/4 v0, 0x0

    new-array v0, v0, [Ldrl;

    sput-object v0, Ldrl;->a:[Ldrl;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7481
    invoke-direct {p0}, Lepn;-><init>()V

    .line 7484
    iput-object v0, p0, Ldrl;->b:Ldvm;

    .line 7487
    iput-object v0, p0, Ldrl;->c:Ldqf;

    .line 7490
    iput-object v0, p0, Ldrl;->d:Ldrj;

    .line 7481
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 7510
    const/4 v0, 0x0

    .line 7511
    iget-object v1, p0, Ldrl;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 7512
    const/4 v0, 0x1

    iget-object v1, p0, Ldrl;->b:Ldvm;

    .line 7513
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7515
    :cond_0
    iget-object v1, p0, Ldrl;->c:Ldqf;

    if-eqz v1, :cond_1

    .line 7516
    const/4 v1, 0x2

    iget-object v2, p0, Ldrl;->c:Ldqf;

    .line 7517
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7519
    :cond_1
    iget-object v1, p0, Ldrl;->d:Ldrj;

    if-eqz v1, :cond_2

    .line 7520
    const/4 v1, 0x3

    iget-object v2, p0, Ldrl;->d:Ldrj;

    .line 7521
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7523
    :cond_2
    iget-object v1, p0, Ldrl;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7524
    iput v0, p0, Ldrl;->cachedSize:I

    .line 7525
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 7477
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldrl;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldrl;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldrl;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldrl;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldrl;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldrl;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldrl;->c:Ldqf;

    if-nez v0, :cond_3

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldrl;->c:Ldqf;

    :cond_3
    iget-object v0, p0, Ldrl;->c:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldrl;->d:Ldrj;

    if-nez v0, :cond_4

    new-instance v0, Ldrj;

    invoke-direct {v0}, Ldrj;-><init>()V

    iput-object v0, p0, Ldrl;->d:Ldrj;

    :cond_4
    iget-object v0, p0, Ldrl;->d:Ldrj;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 7495
    iget-object v0, p0, Ldrl;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 7496
    const/4 v0, 0x1

    iget-object v1, p0, Ldrl;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7498
    :cond_0
    iget-object v0, p0, Ldrl;->c:Ldqf;

    if-eqz v0, :cond_1

    .line 7499
    const/4 v0, 0x2

    iget-object v1, p0, Ldrl;->c:Ldqf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7501
    :cond_1
    iget-object v0, p0, Ldrl;->d:Ldrj;

    if-eqz v0, :cond_2

    .line 7502
    const/4 v0, 0x3

    iget-object v1, p0, Ldrl;->d:Ldrj;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7504
    :cond_2
    iget-object v0, p0, Ldrl;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 7506
    return-void
.end method
