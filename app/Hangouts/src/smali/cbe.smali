.class final Lcbe;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcbd;


# direct methods
.method constructor <init>(Lcbd;)V
    .locals 0

    .prologue
    .line 398
    iput-object p1, p0, Lcbe;->a:Lcbd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    .line 401
    iget-object v0, p0, Lcbe;->a:Lcbd;

    iget-object v0, v0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->e(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lbxn;

    move-result-object v0

    invoke-virtual {v0, p2}, Lbxn;->a(I)I

    move-result v0

    .line 402
    packed-switch v0, :pswitch_data_0

    .line 504
    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 505
    return-void

    .line 405
    :pswitch_0
    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->c(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbbl;->b(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 408
    :try_start_0
    iget-object v0, p0, Lcbe;->a:Lcbd;

    iget-object v0, v0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lt;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    if-eqz v0, :cond_1

    .line 409
    iget-object v0, p0, Lcbe;->a:Lcbd;

    iget-object v0, v0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lt;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d(I)V

    .line 412
    :cond_1
    iget-object v0, p0, Lcbe;->a:Lcbd;

    iget-object v0, v0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lt;

    move-result-object v0

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Lt;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 414
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcbe;->a:Lcbd;

    iget-object v0, v0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lh;->ac:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 415
    iget-object v2, p0, Lcbe;->a:Lcbd;

    iget-object v2, v2, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    .line 416
    invoke-static {v2}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lt;

    move-result-object v2

    invoke-virtual {v2}, Lt;->getActivity()Ly;

    move-result-object v2

    .line 415
    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 417
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 424
    :pswitch_1
    invoke-static {v5}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->c(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcbe;->a:Lcbd;

    iget-object v2, v2, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    .line 426
    invoke-static {v2}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->f(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lcbl;

    move-result-object v2

    invoke-interface {v2}, Lcbl;->a()I

    move-result v2

    .line 425
    invoke-static {v2}, Lf;->b(I)Z

    move-result v2

    .line 423
    invoke-static {v0, v2}, Lbbl;->a(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 429
    :try_start_1
    iget-object v2, p0, Lcbe;->a:Lcbd;

    iget-object v2, v2, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lt;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v0, v3}, Lt;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 431
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcbe;->a:Lcbd;

    iget-object v0, v0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lh;->ac:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 432
    iget-object v2, p0, Lcbe;->a:Lcbd;

    iget-object v2, v2, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    .line 433
    invoke-static {v2}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lt;

    move-result-object v2

    invoke-virtual {v2}, Lt;->getActivity()Ly;

    move-result-object v2

    .line 432
    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 434
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 440
    :pswitch_2
    iget-object v0, p0, Lcbe;->a:Lcbd;

    iget-object v0, v0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->c(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lyj;

    move-result-object v0

    invoke-static {v0}, Lbbl;->f(Lyj;)Landroid/content/Intent;

    move-result-object v1

    .line 442
    :try_start_2
    sget-boolean v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a:Z

    if-eqz v0, :cond_2

    .line 443
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "use G+ photo picker "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    :cond_2
    iget-object v0, p0, Lcbe;->a:Lcbd;

    iget-object v0, v0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lt;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    if-eqz v0, :cond_3

    .line 446
    iget-object v0, p0, Lcbe;->a:Lcbd;

    iget-object v0, v0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lt;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d(I)V

    .line 449
    :cond_3
    iget-object v0, p0, Lcbe;->a:Lcbd;

    iget-object v0, v0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lt;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lt;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 453
    :catch_2
    move-exception v0

    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "photo picker "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 459
    :pswitch_3
    iget-object v0, p0, Lcbe;->a:Lcbd;

    iget-object v0, v0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lt;

    move-result-object v0

    if-eqz v0, :cond_0

    move v2, v1

    .line 460
    :goto_1
    if-gt v2, v5, :cond_0

    .line 467
    iget-object v0, p0, Lcbe;->a:Lcbd;

    iget-object v0, v0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    .line 468
    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->f(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lcbl;

    move-result-object v0

    invoke-interface {v0}, Lcbl;->a()I

    move-result v0

    .line 467
    invoke-static {v0}, Lf;->b(I)Z

    move-result v0

    if-eqz v0, :cond_6

    if-eq v2, v5, :cond_6

    .line 469
    invoke-static {}, Lbbl;->i()Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    .line 471
    :goto_2
    sget-boolean v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a:Z

    if-eqz v0, :cond_4

    .line 472
    const-string v0, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "use any photo picker "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    :cond_4
    :try_start_3
    iget-object v0, p0, Lcbe;->a:Lcbd;

    iget-object v0, v0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lt;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    if-eqz v0, :cond_5

    .line 476
    iget-object v0, p0, Lcbe;->a:Lcbd;

    iget-object v0, v0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lt;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d(I)V

    .line 479
    :cond_5
    iget-object v0, p0, Lcbe;->a:Lcbd;

    iget-object v0, v0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lt;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3}, Lt;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_3
    .catch Landroid/content/ActivityNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    .line 484
    :catch_3
    move-exception v0

    const-string v0, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "couldn\'t find any photo picker "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 470
    :cond_6
    invoke-static {}, Lbbl;->h()Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    .line 492
    :pswitch_4
    iget-object v0, p0, Lcbe;->a:Lcbd;

    iget-object v0, v0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lt;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 493
    invoke-static {}, Lbbl;->b()Landroid/content/Intent;

    move-result-object v0

    .line 494
    iget-object v1, p0, Lcbe;->a:Lcbd;

    iget-object v1, v1, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lt;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, Lt;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 499
    :cond_7
    invoke-static {}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->q()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mPhotoFragment not set. Skipping location sharing"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 402
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method
