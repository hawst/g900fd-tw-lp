.class public final Lxr;
.super Lva;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lva",
        "<",
        "Lbsm;",
        ">;"
    }
.end annotation


# static fields
.field private static final h:Ljava/lang/Object;


# instance fields
.field public final d:Lbyu;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Long;

.field private final g:Lvg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lvg",
            "<",
            "Lbsm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lxr;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lbyu;Lvg;Lvf;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbyu;",
            "Lvg",
            "<",
            "Lbsm;",
            ">;",
            "Lvf;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 58
    invoke-virtual {p1}, Lbyu;->p()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lva;-><init>(Ljava/lang/String;Lvf;)V

    .line 49
    iput-object v4, p0, Lxr;->f:Ljava/lang/Long;

    .line 59
    new-instance v0, Lvi;

    const/16 v1, 0x3e8

    const/4 v2, 0x2

    const/high16 v3, 0x40000000    # 2.0f

    invoke-direct {v0, v1, v2, v3}, Lvi;-><init>(IIF)V

    invoke-virtual {p0, v0}, Lxr;->a(Lvi;)V

    .line 63
    iput-object p2, p0, Lxr;->g:Lvg;

    .line 64
    iput-object p1, p0, Lxr;->d:Lbyu;

    .line 65
    iput-object v4, p0, Lxr;->e:Ljava/lang/String;

    .line 66
    iget-object v0, p0, Lxr;->d:Lbyu;

    invoke-virtual {v0}, Lbyu;->m()Lyj;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 67
    sget-object v0, Lbea;->e:Ljava/lang/String;

    invoke-static {v0}, Lauz;->a(Ljava/lang/String;)Lava;

    move-result-object v1

    .line 70
    :try_start_0
    iget-object v0, p0, Lxr;->d:Lbyu;

    invoke-virtual {v0}, Lbyu;->m()Lyj;

    move-result-object v0

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lava;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lxr;->e:Ljava/lang/String;
    :try_end_0
    .catch Lbph; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :goto_0
    iget-object v0, p0, Lxr;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lxr;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lava;->c(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lxr;->f:Ljava/lang/Long;

    .line 78
    :cond_0
    return-void

    .line 71
    :catch_0
    move-exception v0

    .line 72
    const-string v2, "Babel"

    const-string v3, "Error getting auth token"

    invoke-static {v2, v3, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method protected a(Lux;)Lve;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lux;",
            ")",
            "Lve",
            "<",
            "Lbsm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    sget-object v2, Lxr;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 104
    :try_start_0
    new-instance v3, Lup;

    invoke-direct {v3}, Lup;-><init>()V

    iget-object v0, p1, Lux;->b:[B

    iput-object v0, v3, Lup;->a:[B

    iget-object v0, p1, Lux;->c:Ljava/util/Map;

    iput-object v0, v3, Lup;->f:Ljava/util/Map;

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, v3, Lup;->d:J

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, v3, Lup;->e:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, v3, Lup;->c:J

    iget-object v0, p1, Lux;->c:Ljava/util/Map;

    const-string v1, "VolleyDiskCache"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    new-instance v4, Lbsm;

    iget-object v5, p1, Lux;->b:[B

    iget-object v0, p1, Lux;->c:Ljava/util/Map;

    const-string v6, "Content-Type"

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v4, v5, v0, v1}, Lbsm;-><init>([BLjava/lang/String;Z)V

    new-instance v0, Lve;

    invoke-direct {v0, v4, v3}, Lve;-><init>(Ljava/lang/Object;Lup;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    monitor-exit v2

    .line 108
    :goto_1
    return-object v0

    .line 104
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 105
    :catch_0
    move-exception v0

    .line 106
    const-string v1, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Caught OOM for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lux;->b:[B

    array-length v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " byte media, url="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 107
    invoke-virtual {p0}, Lxr;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 106
    invoke-static {v1, v3}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    new-instance v1, Luz;

    invoke-direct {v1, v0}, Luz;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1}, Lve;->a(Lvl;)Lve;

    move-result-object v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 110
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 30
    check-cast p1, Lbsm;

    iget-object v0, p0, Lxr;->g:Lvg;

    invoke-interface {v0, p1}, Lvg;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public g()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lxr;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 90
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    .line 91
    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bearer "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lxr;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public j()Lvc;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lvc;->a:Lvc;

    return-object v0
.end method
