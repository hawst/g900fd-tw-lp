.class public final Lqw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lcom/android/ex/photo/views/PhotoView;

.field private b:F

.field private c:F

.field private d:F

.field private e:J

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>(Lcom/android/ex/photo/views/PhotoView;)V
    .locals 0

    .prologue
    .line 1352
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1353
    iput-object p1, p0, Lqw;->a:Lcom/android/ex/photo/views/PhotoView;

    .line 1354
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 1377
    const/4 v0, 0x0

    iput-boolean v0, p0, Lqw;->f:Z

    .line 1378
    const/4 v0, 0x1

    iput-boolean v0, p0, Lqw;->g:Z

    .line 1379
    return-void
.end method

.method public run()V
    .locals 6

    .prologue
    .line 1383
    iget-boolean v0, p0, Lqw;->g:Z

    if-eqz v0, :cond_1

    .line 1409
    :cond_0
    :goto_0
    return-void

    .line 1387
    :cond_1
    iget v0, p0, Lqw;->c:F

    iget v1, p0, Lqw;->b:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_6

    .line 1388
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1389
    iget-wide v0, p0, Lqw;->e:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_7

    iget-wide v0, p0, Lqw;->e:J

    sub-long v0, v2, v0

    .line 1390
    :goto_1
    iget v4, p0, Lqw;->d:F

    long-to-float v0, v0

    mul-float/2addr v0, v4

    .line 1391
    iget v1, p0, Lqw;->c:F

    iget v4, p0, Lqw;->b:F

    cmpg-float v1, v1, v4

    if-gez v1, :cond_2

    iget v1, p0, Lqw;->c:F

    add-float/2addr v1, v0

    iget v4, p0, Lqw;->b:F

    cmpl-float v1, v1, v4

    if-gtz v1, :cond_3

    :cond_2
    iget v1, p0, Lqw;->c:F

    iget v4, p0, Lqw;->b:F

    cmpl-float v1, v1, v4

    if-lez v1, :cond_4

    iget v1, p0, Lqw;->c:F

    add-float/2addr v1, v0

    iget v4, p0, Lqw;->b:F

    cmpg-float v1, v1, v4

    if-gez v1, :cond_4

    .line 1395
    :cond_3
    iget v0, p0, Lqw;->b:F

    iget v1, p0, Lqw;->c:F

    sub-float/2addr v0, v1

    .line 1397
    :cond_4
    iget-object v1, p0, Lqw;->a:Lcom/android/ex/photo/views/PhotoView;

    invoke-static {v1, v0}, Lcom/android/ex/photo/views/PhotoView;->a(Lcom/android/ex/photo/views/PhotoView;F)V

    .line 1398
    iget v1, p0, Lqw;->c:F

    add-float/2addr v0, v1

    iput v0, p0, Lqw;->c:F

    .line 1399
    iget v0, p0, Lqw;->c:F

    iget v1, p0, Lqw;->b:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_5

    .line 1400
    invoke-virtual {p0}, Lqw;->a()V

    .line 1402
    :cond_5
    iput-wide v2, p0, Lqw;->e:J

    .line 1405
    :cond_6
    iget-boolean v0, p0, Lqw;->g:Z

    if-nez v0, :cond_0

    .line 1408
    iget-object v0, p0, Lqw;->a:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v0, p0}, Lcom/android/ex/photo/views/PhotoView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1389
    :cond_7
    const-wide/16 v0, 0x0

    goto :goto_1
.end method
