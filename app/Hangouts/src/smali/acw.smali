.class public final Lacw;
.super Lacy;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field a:Landroid/view/View;

.field b:Landroid/view/View;

.field final synthetic c:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 279
    iput-object p1, p0, Lacw;->c:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    .line 280
    sget v0, Lf;->en:I

    sget v1, Lg;->b:I

    invoke-direct {p0, p1, p2, v0, v1}, Lacy;-><init>(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;Landroid/view/ViewGroup;II)V

    .line 282
    iget-object v0, p0, Lacw;->d:Landroid/view/View;

    sget v1, Lg;->bx:I

    .line 283
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lacw;->a:Landroid/view/View;

    .line 284
    iget-object v0, p0, Lacw;->a:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 285
    iget-object v0, p0, Lacw;->d:Landroid/view/View;

    sget v1, Lg;->c:I

    .line 286
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lacw;->b:Landroid/view/View;

    .line 287
    iget-object v0, p0, Lacw;->b:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 288
    return-void
.end method


# virtual methods
.method a()Z
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lacw;->c:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->e(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)Lyj;

    move-result-object v0

    invoke-virtual {v0}, Lyj;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lacw;->c:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->e(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)Lyj;

    move-result-object v0

    invoke-virtual {v0}, Lyj;->p()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lacw;->c:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    .line 304
    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->e(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)Lyj;

    move-result-object v0

    invoke-virtual {v0}, Lyj;->n()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lacw;->a:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 293
    iget-object v0, p0, Lacw;->c:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->e(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)Lyj;

    move-result-object v0

    invoke-static {v0}, Lym;->e(Lyj;)V

    .line 294
    invoke-virtual {p0}, Lacw;->b()V

    .line 299
    :cond_0
    :goto_0
    return-void

    .line 295
    :cond_1
    iget-object v0, p0, Lacw;->b:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 296
    iget-object v0, p0, Lacw;->c:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->e(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)Lyj;

    move-result-object v0

    invoke-static {v0}, Lym;->f(Lyj;)V

    .line 297
    invoke-virtual {p0}, Lacw;->b()V

    goto :goto_0
.end method
