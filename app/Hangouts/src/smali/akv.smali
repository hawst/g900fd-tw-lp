.class public final Lakv;
.super Lbor;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)V
    .locals 0

    .prologue
    .line 194
    iput-object p1, p0, Lakv;->a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-direct {p0}, Lbor;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lyj;Lbgy;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v9, 0x1

    .line 200
    iget-object v0, p0, Lakv;->a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->a(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Lbdk;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lakv;->a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    .line 201
    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->a(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Lbdk;

    move-result-object v0

    iget-object v0, v0, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {p2}, Lbgy;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 264
    :cond_0
    :goto_0
    return-void

    .line 206
    :cond_1
    invoke-virtual {p2}, Lbgy;->h()Ljava/util/ArrayList;

    move-result-object v4

    .line 207
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lakv;->a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->b(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 208
    iget-object v0, p0, Lakv;->a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    move v2, v3

    .line 209
    :goto_1
    if-ge v2, v5, :cond_3

    .line 211
    const/4 v1, 0x0

    .line 212
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgz;

    .line 213
    iget v7, v0, Lbgz;->c:I

    packed-switch v7, :pswitch_data_0

    .line 228
    :goto_2
    iget-object v0, p0, Lakv;->a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->b(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 229
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 230
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 215
    :pswitch_0
    iget-object v0, v0, Lbgz;->a:Ljava/lang/String;

    move-object v1, v0

    .line 216
    goto :goto_2

    .line 218
    :pswitch_1
    iget-object v1, v0, Lbgz;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 219
    sget v1, Lh;->ni:I

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, v0, Lbgz;->b:Ljava/lang/String;

    aput-object v8, v7, v3

    iget-object v0, v0, Lbgz;->a:Ljava/lang/String;

    aput-object v0, v7, v9

    invoke-virtual {v6, v1, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    .line 221
    :cond_2
    sget v1, Lh;->oY:I

    new-array v7, v9, [Ljava/lang/Object;

    iget-object v0, v0, Lbgz;->a:Ljava/lang/String;

    aput-object v0, v7, v3

    invoke-virtual {v6, v1, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 223
    goto :goto_2

    .line 225
    :pswitch_2
    iget-object v0, v0, Lbgz;->a:Ljava/lang/String;

    move-object v1, v0

    goto :goto_2

    .line 234
    :cond_3
    invoke-virtual {p2}, Lbgy;->f()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 235
    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    .line 236
    new-instance v1, Lakw;

    invoke-direct {v1, p0}, Lakw;-><init>(Lakv;)V

    .line 255
    new-instance v2, Lzx;

    new-instance v3, Lbyq;

    .line 256
    invoke-virtual {p2}, Lbgy;->f()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, p1}, Lbyq;-><init>(Ljava/lang/String;Lyj;)V

    iget-object v4, p0, Lakv;->a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    .line 257
    invoke-static {v4}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->c(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v4

    iget-object v5, p0, Lakv;->a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    .line 258
    invoke-virtual {v5}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    const/high16 v6, 0x43000000    # 128.0f

    mul-float/2addr v5, v6

    float-to-int v5, v5

    .line 257
    invoke-virtual {v3, v4, v5}, Lbyq;->a(II)Lbyq;

    move-result-object v3

    .line 259
    invoke-virtual {v3, v9}, Lbyq;->a(Z)Lbyq;

    move-result-object v3

    invoke-virtual {v3, v9}, Lbyq;->d(Z)Lbyq;

    move-result-object v3

    iget-object v4, p0, Lakv;->a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    .line 260
    invoke-static {v4}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->a(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Lbdk;

    move-result-object v4

    invoke-direct {v2, v3, v1, v9, v4}, Lzx;-><init>(Lbyq;Laac;ZLjava/lang/Object;)V

    .line 262
    invoke-virtual {v0, v2}, Lbsn;->a(Lbrv;)Z

    goto/16 :goto_0

    .line 213
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
