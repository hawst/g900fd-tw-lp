.class public final Ldqb;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldqb;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/Integer;

.field public e:[I

.field public f:Ldui;

.field public g:Ldtn;

.field public h:Ldxt;

.field public i:Ljava/lang/Long;

.field public j:Ljava/lang/Long;

.field public k:Ljava/lang/Long;

.field public l:Ljava/lang/Integer;

.field public m:Ljava/lang/Boolean;

.field public n:[Ldqc;

.field public o:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3415
    const/4 v0, 0x0

    new-array v0, v0, [Ldqb;

    sput-object v0, Ldqb;->a:[Ldqb;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3416
    invoke-direct {p0}, Lepn;-><init>()V

    .line 3522
    iput-object v1, p0, Ldqb;->b:Ljava/lang/Integer;

    .line 3527
    iput-object v1, p0, Ldqb;->d:Ljava/lang/Integer;

    .line 3530
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Ldqb;->e:[I

    .line 3533
    iput-object v1, p0, Ldqb;->f:Ldui;

    .line 3536
    iput-object v1, p0, Ldqb;->g:Ldtn;

    .line 3539
    iput-object v1, p0, Ldqb;->h:Ldxt;

    .line 3548
    iput-object v1, p0, Ldqb;->l:Ljava/lang/Integer;

    .line 3553
    sget-object v0, Ldqc;->a:[Ldqc;

    iput-object v0, p0, Ldqb;->n:[Ldqc;

    .line 3556
    iput-object v1, p0, Ldqb;->o:Ljava/lang/Integer;

    .line 3416
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 3616
    iget-object v0, p0, Ldqb;->c:Ljava/lang/Long;

    if-eqz v0, :cond_f

    .line 3617
    const/4 v0, 0x2

    iget-object v2, p0, Ldqb;->c:Ljava/lang/Long;

    .line 3618
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lepl;->d(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3620
    :goto_0
    iget-object v2, p0, Ldqb;->h:Ldxt;

    if-eqz v2, :cond_0

    .line 3621
    const/4 v2, 0x7

    iget-object v3, p0, Ldqb;->h:Ldxt;

    .line 3622
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3624
    :cond_0
    iget-object v2, p0, Ldqb;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 3625
    const/16 v2, 0x8

    iget-object v3, p0, Ldqb;->b:Ljava/lang/Integer;

    .line 3626
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 3628
    :cond_1
    iget-object v2, p0, Ldqb;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 3629
    const/16 v2, 0x9

    iget-object v3, p0, Ldqb;->d:Ljava/lang/Integer;

    .line 3630
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 3632
    :cond_2
    iget-object v2, p0, Ldqb;->e:[I

    if-eqz v2, :cond_4

    iget-object v2, p0, Ldqb;->e:[I

    array-length v2, v2

    if-lez v2, :cond_4

    .line 3634
    iget-object v4, p0, Ldqb;->e:[I

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_3

    aget v6, v4, v2

    .line 3636
    invoke-static {v6}, Lepl;->f(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 3634
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3638
    :cond_3
    add-int/2addr v0, v3

    .line 3639
    iget-object v2, p0, Ldqb;->e:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 3641
    :cond_4
    iget-object v2, p0, Ldqb;->f:Ldui;

    if-eqz v2, :cond_5

    .line 3642
    const/16 v2, 0xb

    iget-object v3, p0, Ldqb;->f:Ldui;

    .line 3643
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3645
    :cond_5
    iget-object v2, p0, Ldqb;->i:Ljava/lang/Long;

    if-eqz v2, :cond_6

    .line 3646
    const/16 v2, 0xc

    iget-object v3, p0, Ldqb;->i:Ljava/lang/Long;

    .line 3647
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 3649
    :cond_6
    iget-object v2, p0, Ldqb;->k:Ljava/lang/Long;

    if-eqz v2, :cond_7

    .line 3650
    const/16 v2, 0xd

    iget-object v3, p0, Ldqb;->k:Ljava/lang/Long;

    .line 3651
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 3653
    :cond_7
    iget-object v2, p0, Ldqb;->j:Ljava/lang/Long;

    if-eqz v2, :cond_8

    .line 3654
    const/16 v2, 0xe

    iget-object v3, p0, Ldqb;->j:Ljava/lang/Long;

    .line 3655
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 3657
    :cond_8
    iget-object v2, p0, Ldqb;->l:Ljava/lang/Integer;

    if-eqz v2, :cond_9

    .line 3658
    const/16 v2, 0xf

    iget-object v3, p0, Ldqb;->l:Ljava/lang/Integer;

    .line 3659
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 3661
    :cond_9
    iget-object v2, p0, Ldqb;->m:Ljava/lang/Boolean;

    if-eqz v2, :cond_a

    .line 3662
    const/16 v2, 0x10

    iget-object v3, p0, Ldqb;->m:Ljava/lang/Boolean;

    .line 3663
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 3665
    :cond_a
    iget-object v2, p0, Ldqb;->n:[Ldqc;

    if-eqz v2, :cond_c

    .line 3666
    iget-object v2, p0, Ldqb;->n:[Ldqc;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_c

    aget-object v4, v2, v1

    .line 3667
    if-eqz v4, :cond_b

    .line 3668
    const/16 v5, 0x11

    .line 3669
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 3666
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3673
    :cond_c
    iget-object v1, p0, Ldqb;->o:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 3674
    const/16 v1, 0x12

    iget-object v2, p0, Ldqb;->o:Ljava/lang/Integer;

    .line 3675
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3677
    :cond_d
    iget-object v1, p0, Ldqb;->g:Ldtn;

    if-eqz v1, :cond_e

    .line 3678
    const/16 v1, 0x13

    iget-object v2, p0, Ldqb;->g:Ldtn;

    .line 3679
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3681
    :cond_e
    iget-object v1, p0, Ldqb;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3682
    iput v0, p0, Ldqb;->cachedSize:I

    .line 3683
    return v0

    :cond_f
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 8

    .prologue
    const/16 v7, 0xa

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 3412
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldqb;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldqb;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldqb;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldqb;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldqb;->h:Ldxt;

    if-nez v0, :cond_2

    new-instance v0, Ldxt;

    invoke-direct {v0}, Ldxt;-><init>()V

    iput-object v0, p0, Ldqb;->h:Ldxt;

    :cond_2
    iget-object v0, p0, Ldqb;->h:Ldxt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_3

    if-eq v0, v4, :cond_3

    if-eq v0, v5, :cond_3

    if-ne v0, v6, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqb;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqb;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v7, :cond_5

    const/16 v2, 0x14

    if-eq v0, v2, :cond_5

    const/16 v2, 0x1e

    if-ne v0, v2, :cond_6

    :cond_5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqb;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_6
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqb;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x50

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldqb;->e:[I

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [I

    iget-object v3, p0, Ldqb;->e:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldqb;->e:[I

    :goto_1
    iget-object v2, p0, Ldqb;->e:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Ldqb;->e:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_7
    iget-object v2, p0, Ldqb;->e:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Ldqb;->f:Ldui;

    if-nez v0, :cond_8

    new-instance v0, Ldui;

    invoke-direct {v0}, Ldui;-><init>()V

    iput-object v0, p0, Ldqb;->f:Ldui;

    :cond_8
    iget-object v0, p0, Ldqb;->f:Ldui;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldqb;->i:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldqb;->k:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldqb;->j:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_9

    if-eq v0, v4, :cond_9

    if-ne v0, v5, :cond_a

    :cond_9
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqb;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_a
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqb;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldqb;->m:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_c
    const/16 v0, 0x8a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldqb;->n:[Ldqc;

    if-nez v0, :cond_c

    move v0, v1

    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Ldqc;

    iget-object v3, p0, Ldqb;->n:[Ldqc;

    if-eqz v3, :cond_b

    iget-object v3, p0, Ldqb;->n:[Ldqc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    iput-object v2, p0, Ldqb;->n:[Ldqc;

    :goto_3
    iget-object v2, p0, Ldqb;->n:[Ldqc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    iget-object v2, p0, Ldqb;->n:[Ldqc;

    new-instance v3, Ldqc;

    invoke-direct {v3}, Ldqc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldqb;->n:[Ldqc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_c
    iget-object v0, p0, Ldqb;->n:[Ldqc;

    array-length v0, v0

    goto :goto_2

    :cond_d
    iget-object v2, p0, Ldqb;->n:[Ldqc;

    new-instance v3, Ldqc;

    invoke-direct {v3}, Ldqc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldqb;->n:[Ldqc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_e

    if-eq v0, v4, :cond_e

    if-eq v0, v5, :cond_e

    if-ne v0, v6, :cond_f

    :cond_e
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqb;->o:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_f
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqb;->o:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Ldqb;->g:Ldtn;

    if-nez v0, :cond_10

    new-instance v0, Ldtn;

    invoke-direct {v0}, Ldtn;-><init>()V

    iput-object v0, p0, Ldqb;->g:Ldtn;

    :cond_10
    iget-object v0, p0, Ldqb;->g:Ldtn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x3a -> :sswitch_2
        0x40 -> :sswitch_3
        0x48 -> :sswitch_4
        0x50 -> :sswitch_5
        0x5a -> :sswitch_6
        0x60 -> :sswitch_7
        0x68 -> :sswitch_8
        0x70 -> :sswitch_9
        0x78 -> :sswitch_a
        0x80 -> :sswitch_b
        0x8a -> :sswitch_c
        0x90 -> :sswitch_d
        0x9a -> :sswitch_e
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 3561
    iget-object v1, p0, Ldqb;->c:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 3562
    const/4 v1, 0x2

    iget-object v2, p0, Ldqb;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->a(IJ)V

    .line 3564
    :cond_0
    iget-object v1, p0, Ldqb;->h:Ldxt;

    if-eqz v1, :cond_1

    .line 3565
    const/4 v1, 0x7

    iget-object v2, p0, Ldqb;->h:Ldxt;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 3567
    :cond_1
    iget-object v1, p0, Ldqb;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 3568
    const/16 v1, 0x8

    iget-object v2, p0, Ldqb;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 3570
    :cond_2
    iget-object v1, p0, Ldqb;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 3571
    const/16 v1, 0x9

    iget-object v2, p0, Ldqb;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 3573
    :cond_3
    iget-object v1, p0, Ldqb;->e:[I

    if-eqz v1, :cond_4

    iget-object v1, p0, Ldqb;->e:[I

    array-length v1, v1

    if-lez v1, :cond_4

    .line 3574
    iget-object v2, p0, Ldqb;->e:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget v4, v2, v1

    .line 3575
    const/16 v5, 0xa

    invoke-virtual {p1, v5, v4}, Lepl;->a(II)V

    .line 3574
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3578
    :cond_4
    iget-object v1, p0, Ldqb;->f:Ldui;

    if-eqz v1, :cond_5

    .line 3579
    const/16 v1, 0xb

    iget-object v2, p0, Ldqb;->f:Ldui;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 3581
    :cond_5
    iget-object v1, p0, Ldqb;->i:Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 3582
    const/16 v1, 0xc

    iget-object v2, p0, Ldqb;->i:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->a(IJ)V

    .line 3584
    :cond_6
    iget-object v1, p0, Ldqb;->k:Ljava/lang/Long;

    if-eqz v1, :cond_7

    .line 3585
    const/16 v1, 0xd

    iget-object v2, p0, Ldqb;->k:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->a(IJ)V

    .line 3587
    :cond_7
    iget-object v1, p0, Ldqb;->j:Ljava/lang/Long;

    if-eqz v1, :cond_8

    .line 3588
    const/16 v1, 0xe

    iget-object v2, p0, Ldqb;->j:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->a(IJ)V

    .line 3590
    :cond_8
    iget-object v1, p0, Ldqb;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 3591
    const/16 v1, 0xf

    iget-object v2, p0, Ldqb;->l:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 3593
    :cond_9
    iget-object v1, p0, Ldqb;->m:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 3594
    const/16 v1, 0x10

    iget-object v2, p0, Ldqb;->m:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 3596
    :cond_a
    iget-object v1, p0, Ldqb;->n:[Ldqc;

    if-eqz v1, :cond_c

    .line 3597
    iget-object v1, p0, Ldqb;->n:[Ldqc;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_c

    aget-object v3, v1, v0

    .line 3598
    if-eqz v3, :cond_b

    .line 3599
    const/16 v4, 0x11

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 3597
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3603
    :cond_c
    iget-object v0, p0, Ldqb;->o:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 3604
    const/16 v0, 0x12

    iget-object v1, p0, Ldqb;->o:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 3606
    :cond_d
    iget-object v0, p0, Ldqb;->g:Ldtn;

    if-eqz v0, :cond_e

    .line 3607
    const/16 v0, 0x13

    iget-object v1, p0, Ldqb;->g:Ldtn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 3609
    :cond_e
    iget-object v0, p0, Ldqb;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 3611
    return-void
.end method
