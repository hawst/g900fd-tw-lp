.class public final Lesv;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lesv;


# instance fields
.field public b:Lesw;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/String;

.field public f:[Ljava/lang/String;

.field public g:[Ljava/lang/String;

.field public h:Ljava/lang/Boolean;

.field public i:[Letb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    new-array v0, v0, [Lesv;

    sput-object v0, Lesv;->a:[Lesv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0}, Lepn;-><init>()V

    .line 99
    const/4 v0, 0x0

    iput-object v0, p0, Lesv;->b:Lesw;

    .line 108
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Lesv;->f:[Ljava/lang/String;

    .line 111
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Lesv;->g:[Ljava/lang/String;

    .line 116
    sget-object v0, Letb;->a:[Letb;

    iput-object v0, p0, Lesv;->i:[Letb;

    .line 96
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 160
    iget-object v0, p0, Lesv;->b:Lesw;

    if-eqz v0, :cond_a

    .line 161
    const/16 v0, 0x15

    iget-object v2, p0, Lesv;->b:Lesw;

    .line 162
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 164
    :goto_0
    iget-object v2, p0, Lesv;->c:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    .line 165
    const/16 v2, 0x19

    iget-object v3, p0, Lesv;->c:Ljava/lang/Boolean;

    .line 166
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 168
    :cond_0
    iget-object v2, p0, Lesv;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 169
    const/16 v2, 0x1a

    iget-object v3, p0, Lesv;->d:Ljava/lang/Boolean;

    .line 170
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 172
    :cond_1
    iget-object v2, p0, Lesv;->e:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 173
    const/16 v2, 0x1b

    iget-object v3, p0, Lesv;->e:Ljava/lang/String;

    .line 174
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 176
    :cond_2
    iget-object v2, p0, Lesv;->f:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lesv;->f:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 178
    iget-object v4, p0, Lesv;->f:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_3

    aget-object v6, v4, v2

    .line 180
    invoke-static {v6}, Lepl;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 178
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 182
    :cond_3
    add-int/2addr v0, v3

    .line 183
    iget-object v2, p0, Lesv;->f:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 185
    :cond_4
    iget-object v2, p0, Lesv;->g:[Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lesv;->g:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 187
    iget-object v4, p0, Lesv;->g:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_5

    aget-object v6, v4, v2

    .line 189
    invoke-static {v6}, Lepl;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 187
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 191
    :cond_5
    add-int/2addr v0, v3

    .line 192
    iget-object v2, p0, Lesv;->g:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 194
    :cond_6
    iget-object v2, p0, Lesv;->h:Ljava/lang/Boolean;

    if-eqz v2, :cond_7

    .line 195
    const/16 v2, 0x1e

    iget-object v3, p0, Lesv;->h:Ljava/lang/Boolean;

    .line 196
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 198
    :cond_7
    iget-object v2, p0, Lesv;->i:[Letb;

    if-eqz v2, :cond_9

    .line 199
    iget-object v2, p0, Lesv;->i:[Letb;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_9

    aget-object v4, v2, v1

    .line 200
    if-eqz v4, :cond_8

    .line 201
    const/16 v5, 0x1f

    .line 202
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 199
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 206
    :cond_9
    iget-object v1, p0, Lesv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 207
    iput v0, p0, Lesv;->cachedSize:I

    .line 208
    return v0

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 92
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lesv;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lesv;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lesv;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lesv;->b:Lesw;

    if-nez v0, :cond_2

    new-instance v0, Lesw;

    invoke-direct {v0}, Lesw;-><init>()V

    iput-object v0, p0, Lesv;->b:Lesw;

    :cond_2
    iget-object v0, p0, Lesv;->b:Lesw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lesv;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lesv;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lesv;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0xe2

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lesv;->f:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lesv;->f:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lesv;->f:[Ljava/lang/String;

    :goto_1
    iget-object v2, p0, Lesv;->f:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lesv;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lesv;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    :sswitch_6
    const/16 v0, 0xea

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lesv;->g:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lesv;->g:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lesv;->g:[Ljava/lang/String;

    :goto_2
    iget-object v2, p0, Lesv;->g:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lesv;->g:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v2, p0, Lesv;->g:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lesv;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0xfa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lesv;->i:[Letb;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Letb;

    iget-object v3, p0, Lesv;->i:[Letb;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lesv;->i:[Letb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Lesv;->i:[Letb;

    :goto_4
    iget-object v2, p0, Lesv;->i:[Letb;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Lesv;->i:[Letb;

    new-instance v3, Letb;

    invoke-direct {v3}, Letb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lesv;->i:[Letb;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lesv;->i:[Letb;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lesv;->i:[Letb;

    new-instance v3, Letb;

    invoke-direct {v3}, Letb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lesv;->i:[Letb;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xaa -> :sswitch_1
        0xc8 -> :sswitch_2
        0xd0 -> :sswitch_3
        0xda -> :sswitch_4
        0xe2 -> :sswitch_5
        0xea -> :sswitch_6
        0xf0 -> :sswitch_7
        0xfa -> :sswitch_8
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 121
    iget-object v1, p0, Lesv;->b:Lesw;

    if-eqz v1, :cond_0

    .line 122
    const/16 v1, 0x15

    iget-object v2, p0, Lesv;->b:Lesw;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 124
    :cond_0
    iget-object v1, p0, Lesv;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 125
    const/16 v1, 0x19

    iget-object v2, p0, Lesv;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 127
    :cond_1
    iget-object v1, p0, Lesv;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 128
    const/16 v1, 0x1a

    iget-object v2, p0, Lesv;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 130
    :cond_2
    iget-object v1, p0, Lesv;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 131
    const/16 v1, 0x1b

    iget-object v2, p0, Lesv;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 133
    :cond_3
    iget-object v1, p0, Lesv;->f:[Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 134
    iget-object v2, p0, Lesv;->f:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 135
    const/16 v5, 0x1c

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 134
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 138
    :cond_4
    iget-object v1, p0, Lesv;->g:[Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 139
    iget-object v2, p0, Lesv;->g:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 140
    const/16 v5, 0x1d

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 139
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 143
    :cond_5
    iget-object v1, p0, Lesv;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 144
    const/16 v1, 0x1e

    iget-object v2, p0, Lesv;->h:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 146
    :cond_6
    iget-object v1, p0, Lesv;->i:[Letb;

    if-eqz v1, :cond_8

    .line 147
    iget-object v1, p0, Lesv;->i:[Letb;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    .line 148
    if-eqz v3, :cond_7

    .line 149
    const/16 v4, 0x1f

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 147
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 153
    :cond_8
    iget-object v0, p0, Lesv;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 155
    return-void
.end method
