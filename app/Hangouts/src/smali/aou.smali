.class public abstract Laou;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Lapu;


# instance fields
.field private final a:Lapk;

.field private b:Laov;

.field private c:Laow;

.field private d:Z

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 68
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    iput-object v0, p0, Laou;->a:Lapk;

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Laou;->d:Z

    .line 76
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 77
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 97
    iput p1, p0, Laou;->e:I

    .line 98
    invoke-virtual {p0}, Laou;->b()V

    .line 99
    return-void
.end method

.method public a(Laow;)V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Laou;->c:Laow;

    invoke-static {v0}, Lcwz;->a(Ljava/lang/Object;)V

    .line 133
    iput-object p1, p0, Laou;->c:Laow;

    .line 134
    invoke-virtual {p0}, Laou;->f()V

    .line 135
    return-void
.end method

.method public a(Lapv;)V
    .locals 2

    .prologue
    .line 81
    invoke-virtual {p1}, Lapv;->c()I

    move-result v0

    iput v0, p0, Laou;->e:I

    .line 82
    new-instance v0, Laov;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Laov;-><init>(Laou;B)V

    iput-object v0, p0, Laou;->b:Laov;

    .line 83
    iget-object v0, p0, Laou;->a:Lapk;

    iget-object v1, p0, Laou;->b:Laov;

    invoke-virtual {v0, v1}, Lapk;->a(Lapo;)V

    .line 84
    invoke-virtual {p0}, Laou;->b()V

    .line 85
    return-void
.end method

.method public abstract a(Lapx;)V
.end method

.method b()V
    .locals 3

    .prologue
    .line 106
    iget v0, p0, Laou;->e:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 107
    invoke-virtual {p0}, Laou;->d()V

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    iget-object v0, p0, Laou;->a:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    .line 112
    if-eqz v0, :cond_0

    .line 113
    iget-boolean v1, p0, Laou;->d:Z

    .line 114
    const/4 v2, 0x0

    iput-boolean v2, p0, Laou;->d:Z

    .line 115
    invoke-virtual {v0}, Lapx;->N()Z

    move-result v2

    if-nez v2, :cond_3

    .line 116
    invoke-virtual {v0}, Lapx;->y()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lapx;->D()I

    move-result v2

    if-nez v2, :cond_3

    .line 118
    const/4 v2, 0x1

    iput-boolean v2, p0, Laou;->d:Z

    .line 119
    if-nez v1, :cond_2

    .line 120
    invoke-virtual {p0, v0}, Laou;->a(Lapx;)V

    .line 126
    :cond_2
    :goto_1
    invoke-virtual {p0}, Laou;->f()V

    goto :goto_0

    .line 124
    :cond_3
    invoke-virtual {p0}, Laou;->d()V

    goto :goto_1
.end method

.method public c()V
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x0

    iput-object v0, p0, Laou;->c:Laow;

    .line 139
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    iput-boolean v0, p0, Laou;->d:Z

    .line 145
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 151
    iget-boolean v0, p0, Laou;->d:Z

    return v0
.end method

.method f()V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Laou;->c:Laow;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Laou;->c:Laow;

    invoke-virtual {p0}, Laou;->e()Z

    move-result v1

    invoke-interface {v0, v1}, Laow;->b(Z)V

    .line 158
    :cond_0
    return-void
.end method

.method public m_()V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Laou;->b:Laov;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Laou;->a:Lapk;

    iget-object v1, p0, Laou;->b:Laov;

    invoke-virtual {v0, v1}, Lapk;->b(Lapo;)V

    .line 91
    const/4 v0, 0x0

    iput-object v0, p0, Laou;->b:Laov;

    .line 93
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 103
    return-void
.end method
