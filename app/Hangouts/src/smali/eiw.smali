.class public final Leiw;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leiw;


# instance fields
.field public b:Ljava/lang/Float;

.field public c:Ljava/lang/Float;

.field public d:[Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 181
    const/4 v0, 0x0

    new-array v0, v0, [Leiw;

    sput-object v0, Leiw;->a:[Leiw;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 182
    invoke-direct {p0}, Lepn;-><init>()V

    .line 189
    sget-object v0, Lept;->m:[Ljava/lang/Integer;

    iput-object v0, p0, Leiw;->d:[Ljava/lang/Integer;

    .line 182
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 212
    iget-object v0, p0, Leiw;->b:Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 213
    const/4 v0, 0x1

    iget-object v2, p0, Leiw;->b:Ljava/lang/Float;

    .line 214
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 216
    :goto_0
    iget-object v2, p0, Leiw;->c:Ljava/lang/Float;

    if-eqz v2, :cond_0

    .line 217
    const/4 v2, 0x2

    iget-object v3, p0, Leiw;->c:Ljava/lang/Float;

    .line 218
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 220
    :cond_0
    iget-object v2, p0, Leiw;->d:[Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Leiw;->d:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 222
    iget-object v3, p0, Leiw;->d:[Ljava/lang/Integer;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 224
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 222
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 226
    :cond_1
    add-int/2addr v0, v2

    .line 227
    iget-object v1, p0, Leiw;->d:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 229
    :cond_2
    iget-object v1, p0, Leiw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 230
    iput v0, p0, Leiw;->cachedSize:I

    .line 231
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 178
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leiw;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leiw;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leiw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Leiw;->b:Ljava/lang/Float;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Leiw;->c:Ljava/lang/Float;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Leiw;->d:[Ljava/lang/Integer;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    iget-object v2, p0, Leiw;->d:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Leiw;->d:[Ljava/lang/Integer;

    :goto_1
    iget-object v1, p0, Leiw;->d:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Leiw;->d:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Leiw;->d:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 194
    iget-object v0, p0, Leiw;->b:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 195
    const/4 v0, 0x1

    iget-object v1, p0, Leiw;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 197
    :cond_0
    iget-object v0, p0, Leiw;->c:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 198
    const/4 v0, 0x2

    iget-object v1, p0, Leiw;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 200
    :cond_1
    iget-object v0, p0, Leiw;->d:[Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 201
    iget-object v1, p0, Leiw;->d:[Ljava/lang/Integer;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 202
    const/4 v4, 0x3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 201
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 205
    :cond_2
    iget-object v0, p0, Leiw;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 207
    return-void
.end method
