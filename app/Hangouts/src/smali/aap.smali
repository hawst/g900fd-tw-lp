.class public final Laap;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcfv;
.implements Lcfw;
.implements Lcof;
.implements Lcpb;
.implements Lcpc;


# static fields
.field private static final h:Lcom/google/android/gms/location/LocationRequest;


# instance fields
.field private final a:Laaq;

.field private b:Lcox;

.field private c:Z

.field private final d:F

.field private e:Lcoe;

.field private f:Z

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 61
    invoke-static {}, Lcom/google/android/gms/location/LocationRequest;->a()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    const-string v1, "babel_location_update_highmark"

    const/16 v2, 0x1388

    .line 62
    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/location/LocationRequest;->a(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    const-string v1, "babel_location_update_lowmark"

    const/16 v2, 0x10

    .line 65
    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/location/LocationRequest;->b(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lcom/google/android/gms/location/LocationRequest;->b()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lcom/google/android/gms/location/LocationRequest;->c()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    sput-object v0, Laap;->h:Lcom/google/android/gms/location/LocationRequest;

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Laaq;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-boolean v0, p0, Laap;->c:Z

    .line 49
    iput-boolean v0, p0, Laap;->f:Z

    .line 54
    iput-boolean v0, p0, Laap;->g:Z

    .line 73
    new-instance v0, Lcoe;

    invoke-direct {v0, p1, p0, p0}, Lcoe;-><init>(Landroid/content/Context;Lcfv;Lcfw;)V

    iput-object v0, p0, Laap;->e:Lcoe;

    .line 78
    const/high16 v0, 0x41800000    # 16.0f

    iput v0, p0, Laap;->d:F

    .line 80
    iput-object p2, p0, Laap;->a:Laaq;

    .line 81
    return-void
.end method

.method private a(Lcom/google/android/gms/maps/model/CameraPosition;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 236
    iget-boolean v0, p0, Laap;->c:Z

    if-nez v0, :cond_0

    .line 238
    iput-boolean v2, p0, Laap;->g:Z

    .line 239
    iget-object v0, p0, Laap;->b:Lcox;

    invoke-static {p1}, Lf;->a(Lcom/google/android/gms/maps/model/CameraPosition;)Lcow;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcox;->a(Lcow;)V

    .line 240
    iput-boolean v2, p0, Laap;->c:Z

    .line 241
    iget-object v0, p0, Laap;->a:Laaq;

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Laap;->a:Laaq;

    invoke-interface {v0}, Laaq;->a()V

    .line 245
    :cond_0
    return-void
.end method

.method public static a()Z
    .locals 2

    .prologue
    .line 84
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "location"

    .line 85
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 87
    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "network"

    .line 88
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/location/Location;)V
    .locals 5

    .prologue
    .line 122
    iget-boolean v0, p0, Laap;->c:Z

    if-nez v0, :cond_0

    .line 123
    invoke-direct {p0, p1}, Laap;->c(Landroid/location/Location;)Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    invoke-direct {p0, v0}, Laap;->a(Lcom/google/android/gms/maps/model/CameraPosition;)V

    .line 132
    :goto_0
    return-void

    .line 125
    :cond_0
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 128
    const/4 v1, 0x1

    iput-boolean v1, p0, Laap;->g:Z

    .line 130
    iget-object v1, p0, Laap;->b:Lcox;

    invoke-static {v0}, Lf;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcow;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcox;->b(Lcow;)V

    goto :goto_0
.end method

.method private c(Landroid/location/Location;)Lcom/google/android/gms/maps/model/CameraPosition;
    .locals 5

    .prologue
    .line 251
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 252
    iget v1, p0, Laap;->d:F

    invoke-static {v0, v1}, Lcom/google/android/gms/maps/model/CameraPosition;->a(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/location/Location;)V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Laap;->b:Lcox;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Laap;->f:Z

    if-nez v0, :cond_0

    .line 154
    invoke-direct {p0, p1}, Laap;->b(Landroid/location/Location;)V

    .line 156
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/gms/maps/SupportMapFragment;Lcom/google/android/gms/maps/model/CameraPosition;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 97
    iget-object v0, p0, Laap;->b:Lcox;

    if-nez v0, :cond_2

    .line 98
    invoke-virtual {p1}, Lcom/google/android/gms/maps/SupportMapFragment;->b()Lcox;

    move-result-object v0

    iput-object v0, p0, Laap;->b:Lcox;

    .line 100
    iget-object v0, p0, Laap;->b:Lcox;

    if-eqz v0, :cond_1

    .line 102
    if-eqz p2, :cond_0

    .line 104
    invoke-direct {p0, p2}, Laap;->a(Lcom/google/android/gms/maps/model/CameraPosition;)V

    .line 106
    :cond_0
    iget-object v0, p0, Laap;->b:Lcox;

    invoke-virtual {v0, v1}, Lcox;->a(Z)V

    .line 107
    iget-object v0, p0, Laap;->b:Lcox;

    invoke-virtual {v0, p0}, Lcox;->a(Lcpc;)V

    .line 108
    iget-object v0, p0, Laap;->b:Lcox;

    invoke-virtual {v0, p0}, Lcox;->a(Lcpb;)V

    .line 110
    iget-object v0, p0, Laap;->b:Lcox;

    invoke-virtual {v0}, Lcox;->d()Lcpj;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Lcpj;->d()V

    .line 112
    invoke-virtual {v0}, Lcpj;->c()V

    .line 113
    invoke-virtual {v0}, Lcpj;->b()V

    .line 114
    invoke-virtual {v0}, Lcpj;->a()V

    .line 119
    :cond_1
    :goto_0
    return-void

    .line 117
    :cond_2
    iget-object v0, p0, Laap;->b:Lcox;

    invoke-virtual {v0, v1}, Lcox;->a(Z)V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Laap;->e:Lcoe;

    invoke-virtual {v0}, Lcoe;->b()V

    .line 93
    return-void
.end method

.method public c()Lcom/google/android/gms/maps/model/CameraPosition;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Laap;->b:Lcox;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Laap;->b:Lcox;

    invoke-virtual {v0}, Lcox;->b()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Laap;->b:Lcox;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Laap;->b:Lcox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcox;->a(Z)V

    .line 144
    :cond_0
    iget-object v0, p0, Laap;->e:Lcoe;

    invoke-virtual {v0}, Lcoe;->d()V

    .line 145
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 165
    iget-boolean v0, p0, Laap;->g:Z

    if-nez v0, :cond_0

    .line 167
    const-string v0, "Babel"

    const-string v1, "Camera moved by user"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    const/4 v0, 0x1

    iput-boolean v0, p0, Laap;->f:Z

    .line 174
    :goto_0
    return-void

    .line 171
    :cond_0
    const-string v0, "Babel"

    const-string v1, "Camera moved by app"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const/4 v0, 0x0

    iput-boolean v0, p0, Laap;->g:Z

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 181
    iget-object v1, p0, Laap;->e:Lcoe;

    if-eqz v1, :cond_2

    .line 182
    invoke-static {}, Laap;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 183
    iget-object v1, p0, Laap;->a:Laaq;

    if-eqz v1, :cond_0

    .line 184
    iget-object v1, p0, Laap;->a:Laaq;

    invoke-interface {v1}, Laaq;->b()V

    .line 196
    :cond_0
    :goto_0
    return v0

    .line 189
    :cond_1
    iget-object v1, p0, Laap;->e:Lcoe;

    invoke-virtual {v1}, Lcoe;->a()Landroid/location/Location;

    move-result-object v1

    .line 191
    if-eqz v1, :cond_0

    .line 192
    invoke-direct {p0, v1}, Laap;->b(Landroid/location/Location;)V

    goto :goto_0

    .line 196
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onConnected(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 204
    iget-boolean v0, p0, Laap;->c:Z

    if-nez v0, :cond_0

    .line 205
    iget-object v0, p0, Laap;->e:Lcoe;

    invoke-virtual {v0}, Lcoe;->a()Landroid/location/Location;

    move-result-object v0

    .line 206
    if-eqz v0, :cond_0

    .line 207
    invoke-direct {p0, v0}, Laap;->c(Landroid/location/Location;)Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    invoke-direct {p0, v0}, Laap;->a(Lcom/google/android/gms/maps/model/CameraPosition;)V

    .line 210
    :cond_0
    iget-object v0, p0, Laap;->e:Lcoe;

    sget-object v1, Laap;->h:Lcom/google/android/gms/location/LocationRequest;

    invoke-virtual {v0, v1, p0}, Lcoe;->a(Lcom/google/android/gms/location/LocationRequest;Lcof;)V

    .line 213
    return-void
.end method

.method public onConnectionFailed(Lcft;)V
    .locals 0

    .prologue
    .line 229
    return-void
.end method

.method public onDisconnected()V
    .locals 0

    .prologue
    .line 221
    return-void
.end method
