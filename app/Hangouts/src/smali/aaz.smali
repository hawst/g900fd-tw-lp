.class public final Laaz;
.super Ls;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 120
    invoke-direct {p0}, Ls;-><init>()V

    .line 121
    return-void
.end method

.method static synthetic a(Laaz;)Laay;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Laaz;->getTargetFragment()Lt;

    move-result-object v0

    check-cast v0, Laay;

    return-object v0
.end method

.method public static a(ZIIIZZZZZ)Laaz;
    .locals 3

    .prologue
    .line 95
    new-instance v0, Laaz;

    invoke-direct {v0}, Laaz;-><init>()V

    .line 99
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 100
    const-string v2, "exclude_enterprise"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 102
    const-string v2, "dialog_title_id"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 103
    const-string v2, "select_all_id"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 104
    const-string v2, "select_none_id"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 105
    const-string v2, "show_account_state"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 106
    const-string v2, "show_cancel"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 107
    const-string v2, "show_sms_accounts_only"

    invoke-virtual {v1, v2, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 108
    const-string v2, "exclude_plus_pages"

    invoke-virtual {v1, v2, p7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 109
    const-string v2, "include_sms_only_account"

    invoke-virtual {v1, v2, p8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 111
    invoke-virtual {v0, v1}, Laaz;->setArguments(Landroid/os/Bundle;)V

    .line 113
    return-object v0
.end method


# virtual methods
.method public f()Landroid/app/Dialog;
    .locals 13

    .prologue
    .line 128
    invoke-static {}, Laay;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    const-string v0, "Babel"

    const-string v1, "AccountPickerDialogFragment.onCreateDialog()"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :cond_0
    new-instance v9, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Laaz;->getActivity()Ly;

    move-result-object v0

    invoke-direct {v9, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 132
    invoke-virtual {p0}, Laaz;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 133
    sget v1, Lf;->dX:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    .line 135
    sget v0, Lg;->fp:I

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/widget/ListView;

    .line 139
    invoke-virtual {p0}, Laaz;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "exclude_enterprise"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 140
    invoke-virtual {p0}, Laaz;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "show_account_state"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 141
    invoke-virtual {p0}, Laaz;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "show_cancel"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v11

    .line 142
    invoke-virtual {p0}, Laaz;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "show_sms_accounts_only"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 143
    invoke-virtual {p0}, Laaz;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "exclude_plus_pages"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 144
    invoke-virtual {p0}, Laaz;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v5, "include_sms_only_account"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v12

    .line 145
    invoke-virtual {p0}, Laaz;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v5, "dialog_title_id"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 146
    if-gez v0, :cond_5

    .line 147
    sget v0, Lh;->cZ:I

    move v8, v0

    .line 149
    :goto_0
    invoke-virtual {p0}, Laaz;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v5, "select_all_id"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 150
    invoke-virtual {p0}, Laaz;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v6, "select_none_id"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 152
    invoke-static {v1, v2, v3}, Lbkb;->a(ZZZ)Ljava/util/List;

    move-result-object v3

    .line 154
    if-eqz v12, :cond_1

    .line 155
    sget v0, Lh;->lw:I

    invoke-virtual {p0, v0}, Laaz;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    :cond_1
    if-ltz v5, :cond_2

    .line 158
    const-string v0, "*"

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    :cond_2
    if-ltz v6, :cond_3

    .line 161
    const-string v0, "--None--"

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    :cond_3
    new-instance v0, Labb;

    invoke-virtual {p0}, Laaz;->getActivity()Ly;

    move-result-object v2

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Labb;-><init>(Laaz;Landroid/content/Context;Ljava/util/List;ZII)V

    .line 166
    invoke-virtual {v7, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 168
    invoke-virtual {v9, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 169
    if-eqz v11, :cond_4

    .line 170
    const/high16 v0, 0x1040000

    invoke-virtual {p0, v0}, Laaz;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Laba;

    invoke-direct {v1, p0}, Laba;-><init>(Laaz;)V

    invoke-virtual {v9, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 181
    :cond_4
    invoke-virtual {v9}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    :cond_5
    move v8, v0

    goto :goto_0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 186
    invoke-virtual {p0}, Laaz;->getTargetFragment()Lt;

    move-result-object v0

    check-cast v0, Laay;

    invoke-static {v0}, Laay;->a(Laay;)Labd;

    move-result-object v0

    invoke-interface {v0}, Labd;->n_()V

    .line 187
    return-void
.end method
