.class public final Ldgz;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldgz;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Float;

.field public f:Ljava/lang/Float;

.field public g:Ljava/lang/Float;

.field public h:Ljava/lang/Long;

.field public i:Ljava/lang/Float;

.field public j:Ljava/lang/Boolean;

.field public k:Ljava/lang/Float;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/Long;

.field public n:Ljava/lang/Integer;

.field public o:Ljava/lang/Integer;

.field public p:Ljava/lang/Integer;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6655
    const/4 v0, 0x0

    new-array v0, v0, [Ldgz;

    sput-object v0, Ldgz;->a:[Ldgz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6656
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 6762
    const/4 v0, 0x0

    .line 6763
    iget-object v1, p0, Ldgz;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6764
    const/4 v0, 0x1

    iget-object v1, p0, Ldgz;->b:Ljava/lang/String;

    .line 6765
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6767
    :cond_0
    iget-object v1, p0, Ldgz;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 6768
    const/4 v1, 0x2

    iget-object v2, p0, Ldgz;->c:Ljava/lang/String;

    .line 6769
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6771
    :cond_1
    iget-object v1, p0, Ldgz;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 6772
    const/4 v1, 0x3

    iget-object v2, p0, Ldgz;->d:Ljava/lang/Integer;

    .line 6773
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6775
    :cond_2
    iget-object v1, p0, Ldgz;->e:Ljava/lang/Float;

    if-eqz v1, :cond_3

    .line 6776
    const/4 v1, 0x4

    iget-object v2, p0, Ldgz;->e:Ljava/lang/Float;

    .line 6777
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 6779
    :cond_3
    iget-object v1, p0, Ldgz;->f:Ljava/lang/Float;

    if-eqz v1, :cond_4

    .line 6780
    const/4 v1, 0x5

    iget-object v2, p0, Ldgz;->f:Ljava/lang/Float;

    .line 6781
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 6783
    :cond_4
    iget-object v1, p0, Ldgz;->g:Ljava/lang/Float;

    if-eqz v1, :cond_5

    .line 6784
    const/4 v1, 0x6

    iget-object v2, p0, Ldgz;->g:Ljava/lang/Float;

    .line 6785
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 6787
    :cond_5
    iget-object v1, p0, Ldgz;->h:Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 6788
    const/4 v1, 0x7

    iget-object v2, p0, Ldgz;->h:Ljava/lang/Long;

    .line 6789
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 6791
    :cond_6
    iget-object v1, p0, Ldgz;->i:Ljava/lang/Float;

    if-eqz v1, :cond_7

    .line 6792
    const/16 v1, 0x8

    iget-object v2, p0, Ldgz;->i:Ljava/lang/Float;

    .line 6793
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 6795
    :cond_7
    iget-object v1, p0, Ldgz;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 6796
    const/16 v1, 0x9

    iget-object v2, p0, Ldgz;->j:Ljava/lang/Boolean;

    .line 6797
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6799
    :cond_8
    iget-object v1, p0, Ldgz;->k:Ljava/lang/Float;

    if-eqz v1, :cond_9

    .line 6800
    const/16 v1, 0xa

    iget-object v2, p0, Ldgz;->k:Ljava/lang/Float;

    .line 6801
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 6803
    :cond_9
    iget-object v1, p0, Ldgz;->l:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 6804
    const/16 v1, 0xb

    iget-object v2, p0, Ldgz;->l:Ljava/lang/String;

    .line 6805
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6807
    :cond_a
    iget-object v1, p0, Ldgz;->m:Ljava/lang/Long;

    if-eqz v1, :cond_b

    .line 6808
    const/16 v1, 0xc

    iget-object v2, p0, Ldgz;->m:Ljava/lang/Long;

    .line 6809
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 6811
    :cond_b
    iget-object v1, p0, Ldgz;->n:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 6812
    const/16 v1, 0xd

    iget-object v2, p0, Ldgz;->n:Ljava/lang/Integer;

    .line 6813
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6815
    :cond_c
    iget-object v1, p0, Ldgz;->o:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 6816
    const/16 v1, 0xe

    iget-object v2, p0, Ldgz;->o:Ljava/lang/Integer;

    .line 6817
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6819
    :cond_d
    iget-object v1, p0, Ldgz;->p:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    .line 6820
    const/16 v1, 0xf

    iget-object v2, p0, Ldgz;->p:Ljava/lang/Integer;

    .line 6821
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6823
    :cond_e
    iget-object v1, p0, Ldgz;->q:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 6824
    const/16 v1, 0x10

    iget-object v2, p0, Ldgz;->q:Ljava/lang/String;

    .line 6825
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6827
    :cond_f
    iget-object v1, p0, Ldgz;->r:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 6828
    const/16 v1, 0x11

    iget-object v2, p0, Ldgz;->r:Ljava/lang/String;

    .line 6829
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6831
    :cond_10
    iget-object v1, p0, Ldgz;->s:Ljava/lang/String;

    if-eqz v1, :cond_11

    .line 6832
    const/16 v1, 0x12

    iget-object v2, p0, Ldgz;->s:Ljava/lang/String;

    .line 6833
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6835
    :cond_11
    iget-object v1, p0, Ldgz;->t:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 6836
    const/16 v1, 0x13

    iget-object v2, p0, Ldgz;->t:Ljava/lang/String;

    .line 6837
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6839
    :cond_12
    iget-object v1, p0, Ldgz;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6840
    iput v0, p0, Ldgz;->cachedSize:I

    .line 6841
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6652
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldgz;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldgz;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldgz;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgz;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgz;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgz;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Ldgz;->e:Ljava/lang/Float;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Ldgz;->f:Ljava/lang/Float;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Ldgz;->g:Ljava/lang/Float;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldgz;->h:Ljava/lang/Long;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Ldgz;->i:Ljava/lang/Float;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldgz;->j:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Ldgz;->k:Ljava/lang/Float;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgz;->l:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldgz;->m:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgz;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgz;->o:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgz;->p:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgz;->q:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgz;->r:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgz;->s:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgz;->t:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
        0x35 -> :sswitch_6
        0x38 -> :sswitch_7
        0x45 -> :sswitch_8
        0x48 -> :sswitch_9
        0x55 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 6699
    iget-object v0, p0, Ldgz;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 6700
    const/4 v0, 0x1

    iget-object v1, p0, Ldgz;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 6702
    :cond_0
    iget-object v0, p0, Ldgz;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 6703
    const/4 v0, 0x2

    iget-object v1, p0, Ldgz;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 6705
    :cond_1
    iget-object v0, p0, Ldgz;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 6706
    const/4 v0, 0x3

    iget-object v1, p0, Ldgz;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 6708
    :cond_2
    iget-object v0, p0, Ldgz;->e:Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 6709
    const/4 v0, 0x4

    iget-object v1, p0, Ldgz;->e:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 6711
    :cond_3
    iget-object v0, p0, Ldgz;->f:Ljava/lang/Float;

    if-eqz v0, :cond_4

    .line 6712
    const/4 v0, 0x5

    iget-object v1, p0, Ldgz;->f:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 6714
    :cond_4
    iget-object v0, p0, Ldgz;->g:Ljava/lang/Float;

    if-eqz v0, :cond_5

    .line 6715
    const/4 v0, 0x6

    iget-object v1, p0, Ldgz;->g:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 6717
    :cond_5
    iget-object v0, p0, Ldgz;->h:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 6718
    const/4 v0, 0x7

    iget-object v1, p0, Ldgz;->h:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 6720
    :cond_6
    iget-object v0, p0, Ldgz;->i:Ljava/lang/Float;

    if-eqz v0, :cond_7

    .line 6721
    const/16 v0, 0x8

    iget-object v1, p0, Ldgz;->i:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 6723
    :cond_7
    iget-object v0, p0, Ldgz;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 6724
    const/16 v0, 0x9

    iget-object v1, p0, Ldgz;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 6726
    :cond_8
    iget-object v0, p0, Ldgz;->k:Ljava/lang/Float;

    if-eqz v0, :cond_9

    .line 6727
    const/16 v0, 0xa

    iget-object v1, p0, Ldgz;->k:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 6729
    :cond_9
    iget-object v0, p0, Ldgz;->l:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 6730
    const/16 v0, 0xb

    iget-object v1, p0, Ldgz;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 6732
    :cond_a
    iget-object v0, p0, Ldgz;->m:Ljava/lang/Long;

    if-eqz v0, :cond_b

    .line 6733
    const/16 v0, 0xc

    iget-object v1, p0, Ldgz;->m:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 6735
    :cond_b
    iget-object v0, p0, Ldgz;->n:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    .line 6736
    const/16 v0, 0xd

    iget-object v1, p0, Ldgz;->n:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 6738
    :cond_c
    iget-object v0, p0, Ldgz;->o:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 6739
    const/16 v0, 0xe

    iget-object v1, p0, Ldgz;->o:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 6741
    :cond_d
    iget-object v0, p0, Ldgz;->p:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    .line 6742
    const/16 v0, 0xf

    iget-object v1, p0, Ldgz;->p:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 6744
    :cond_e
    iget-object v0, p0, Ldgz;->q:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 6745
    const/16 v0, 0x10

    iget-object v1, p0, Ldgz;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 6747
    :cond_f
    iget-object v0, p0, Ldgz;->r:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 6748
    const/16 v0, 0x11

    iget-object v1, p0, Ldgz;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 6750
    :cond_10
    iget-object v0, p0, Ldgz;->s:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 6751
    const/16 v0, 0x12

    iget-object v1, p0, Ldgz;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 6753
    :cond_11
    iget-object v0, p0, Ldgz;->t:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 6754
    const/16 v0, 0x13

    iget-object v1, p0, Ldgz;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 6756
    :cond_12
    iget-object v0, p0, Ldgz;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 6758
    return-void
.end method
