.class public final Ldth;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldth;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Leir;

.field public f:Ljava/lang/String;

.field public g:[Ldti;

.field public h:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1190
    const/4 v0, 0x0

    new-array v0, v0, [Ldth;

    sput-object v0, Ldth;->a:[Ldth;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1191
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1482
    const/4 v0, 0x0

    iput-object v0, p0, Ldth;->e:Leir;

    .line 1487
    sget-object v0, Ldti;->a:[Ldti;

    iput-object v0, p0, Ldth;->g:[Ldti;

    .line 1191
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1526
    iget-object v0, p0, Ldth;->b:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1527
    const/4 v0, 0x1

    iget-object v2, p0, Ldth;->b:Ljava/lang/String;

    .line 1528
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1530
    :goto_0
    iget-object v2, p0, Ldth;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 1531
    const/4 v2, 0x2

    iget-object v3, p0, Ldth;->c:Ljava/lang/String;

    .line 1532
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1534
    :cond_0
    iget-object v2, p0, Ldth;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 1535
    const/4 v2, 0x3

    iget-object v3, p0, Ldth;->d:Ljava/lang/String;

    .line 1536
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1538
    :cond_1
    iget-object v2, p0, Ldth;->f:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 1539
    const/4 v2, 0x4

    iget-object v3, p0, Ldth;->f:Ljava/lang/String;

    .line 1540
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1542
    :cond_2
    iget-object v2, p0, Ldth;->h:[B

    if-eqz v2, :cond_3

    .line 1543
    const/4 v2, 0x5

    iget-object v3, p0, Ldth;->h:[B

    .line 1544
    invoke-static {v2, v3}, Lepl;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 1546
    :cond_3
    iget-object v2, p0, Ldth;->g:[Ldti;

    if-eqz v2, :cond_5

    .line 1547
    iget-object v2, p0, Ldth;->g:[Ldti;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 1548
    if-eqz v4, :cond_4

    .line 1549
    const/4 v5, 0x6

    .line 1550
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1547
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1554
    :cond_5
    iget-object v1, p0, Ldth;->e:Leir;

    if-eqz v1, :cond_6

    .line 1555
    const/4 v1, 0x7

    iget-object v2, p0, Ldth;->e:Leir;

    .line 1556
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1558
    :cond_6
    iget-object v1, p0, Ldth;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1559
    iput v0, p0, Ldth;->cachedSize:I

    .line 1560
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1187
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldth;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldth;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldth;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldth;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldth;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldth;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldth;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Ldth;->h:[B

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldth;->g:[Ldti;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldti;

    iget-object v3, p0, Ldth;->g:[Ldti;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldth;->g:[Ldti;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldth;->g:[Ldti;

    :goto_2
    iget-object v2, p0, Ldth;->g:[Ldti;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldth;->g:[Ldti;

    new-instance v3, Ldti;

    invoke-direct {v3}, Ldti;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldth;->g:[Ldti;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldth;->g:[Ldti;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldth;->g:[Ldti;

    new-instance v3, Ldti;

    invoke-direct {v3}, Ldti;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldth;->g:[Ldti;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Ldth;->e:Leir;

    if-nez v0, :cond_5

    new-instance v0, Leir;

    invoke-direct {v0}, Leir;-><init>()V

    iput-object v0, p0, Ldth;->e:Leir;

    :cond_5
    iget-object v0, p0, Ldth;->e:Leir;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 1494
    iget-object v0, p0, Ldth;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1495
    const/4 v0, 0x1

    iget-object v1, p0, Ldth;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1497
    :cond_0
    iget-object v0, p0, Ldth;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1498
    const/4 v0, 0x2

    iget-object v1, p0, Ldth;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1500
    :cond_1
    iget-object v0, p0, Ldth;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1501
    const/4 v0, 0x3

    iget-object v1, p0, Ldth;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1503
    :cond_2
    iget-object v0, p0, Ldth;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1504
    const/4 v0, 0x4

    iget-object v1, p0, Ldth;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1506
    :cond_3
    iget-object v0, p0, Ldth;->h:[B

    if-eqz v0, :cond_4

    .line 1507
    const/4 v0, 0x5

    iget-object v1, p0, Ldth;->h:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 1509
    :cond_4
    iget-object v0, p0, Ldth;->g:[Ldti;

    if-eqz v0, :cond_6

    .line 1510
    iget-object v1, p0, Ldth;->g:[Ldti;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 1511
    if-eqz v3, :cond_5

    .line 1512
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 1510
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1516
    :cond_6
    iget-object v0, p0, Ldth;->e:Leir;

    if-eqz v0, :cond_7

    .line 1517
    const/4 v0, 0x7

    iget-object v1, p0, Ldth;->e:Leir;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1519
    :cond_7
    iget-object v0, p0, Ldth;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1521
    return-void
.end method
