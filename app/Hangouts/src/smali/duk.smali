.class public final Lduk;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lduk;


# instance fields
.field public b:[Lduj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17837
    const/4 v0, 0x0

    new-array v0, v0, [Lduk;

    sput-object v0, Lduk;->a:[Lduk;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17838
    invoke-direct {p0}, Lepn;-><init>()V

    .line 17841
    sget-object v0, Lduj;->a:[Lduj;

    iput-object v0, p0, Lduk;->b:[Lduj;

    .line 17838
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 17860
    iget-object v1, p0, Lduk;->b:[Lduj;

    if-eqz v1, :cond_1

    .line 17861
    iget-object v2, p0, Lduk;->b:[Lduj;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 17862
    if-eqz v4, :cond_0

    .line 17863
    const/4 v5, 0x1

    .line 17864
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 17861
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 17868
    :cond_1
    iget-object v1, p0, Lduk;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17869
    iput v0, p0, Lduk;->cachedSize:I

    .line 17870
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 17834
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lduk;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lduk;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lduk;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lduk;->b:[Lduj;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lduj;

    iget-object v3, p0, Lduk;->b:[Lduj;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lduk;->b:[Lduj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lduk;->b:[Lduj;

    :goto_2
    iget-object v2, p0, Lduk;->b:[Lduj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lduk;->b:[Lduj;

    new-instance v3, Lduj;

    invoke-direct {v3}, Lduj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lduk;->b:[Lduj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lduk;->b:[Lduj;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lduk;->b:[Lduj;

    new-instance v3, Lduj;

    invoke-direct {v3}, Lduj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lduk;->b:[Lduj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 17846
    iget-object v0, p0, Lduk;->b:[Lduj;

    if-eqz v0, :cond_1

    .line 17847
    iget-object v1, p0, Lduk;->b:[Lduj;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 17848
    if-eqz v3, :cond_0

    .line 17849
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 17847
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 17853
    :cond_1
    iget-object v0, p0, Lduk;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 17855
    return-void
.end method
