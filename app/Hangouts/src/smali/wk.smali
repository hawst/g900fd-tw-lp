.class public abstract Lwk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lwl;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/Object;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lwk;->a:Ljava/util/ArrayList;

    .line 34
    const/4 v0, 0x0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 35
    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lwk;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public a(J)V
    .locals 2

    .prologue
    .line 121
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 122
    invoke-virtual {p0}, Lwk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    invoke-virtual {v0, p1, p2}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 127
    :goto_0
    return-void

    .line 124
    :cond_0
    invoke-virtual {p0}, Lwk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwz;

    .line 125
    invoke-virtual {v0, p1, p2}, Lwz;->a(J)V

    goto :goto_0
.end method

.method protected a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lwk;->b:Ljava/lang/Object;

    invoke-static {v0}, Lcwz;->a(Ljava/lang/Object;)V

    .line 51
    iput-object p1, p0, Lwk;->b:Ljava/lang/Object;

    .line 52
    return-void
.end method

.method public a(Lwl;)V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lwk;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 207
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lwk;->a:Ljava/util/ArrayList;

    .line 209
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    .line 212
    new-instance v1, Lwn;

    invoke-direct {v1, p1, p0}, Lwn;-><init>(Lwl;Lwk;)V

    invoke-virtual {p0}, Lwk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    move-object v0, v1

    .line 219
    :goto_0
    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, p1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 221
    iget-object v0, p0, Lwk;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    return-void

    .line 214
    :cond_1
    new-instance v1, Lxa;

    invoke-direct {v1, p1, p0}, Lxa;-><init>(Lwl;Lwk;)V

    .line 215
    invoke-virtual {p0}, Lwk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwz;

    .line 216
    invoke-virtual {v0, v1}, Lwz;->a(Lxa;)V

    move-object v0, v1

    .line 217
    goto :goto_0
.end method

.method public a(Lwt;)V
    .locals 2

    .prologue
    .line 171
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 172
    invoke-virtual {p0}, Lwk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    new-instance v1, Lwu;

    invoke-direct {v1, p1}, Lwu;-><init>(Lwt;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 177
    :goto_0
    return-void

    .line 174
    :cond_0
    invoke-virtual {p0}, Lwk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwz;

    new-instance v1, Lwv;

    invoke-direct {v1, p1}, Lwv;-><init>(Lwt;)V

    .line 175
    invoke-virtual {v0, v1}, Lwz;->a(Lwt;)V

    goto :goto_0
.end method

.method public b(J)Lwk;
    .locals 2

    .prologue
    .line 135
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 136
    invoke-virtual {p0}, Lwk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p1, p2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 141
    :goto_0
    return-object p0

    .line 138
    :cond_0
    invoke-virtual {p0}, Lwk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxg;

    .line 139
    invoke-virtual {v0, p1, p2}, Lxg;->b(J)Lxg;

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 62
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 63
    invoke-virtual {p0}, Lwk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 68
    :goto_0
    return-void

    .line 65
    :cond_0
    invoke-virtual {p0}, Lwk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwz;

    .line 66
    invoke-virtual {v0}, Lwz;->a()V

    goto :goto_0
.end method

.method public b(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 359
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 360
    invoke-virtual {p0}, Lwk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    invoke-virtual {v0, p1}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 365
    :goto_0
    return-void

    .line 362
    :cond_0
    invoke-virtual {p0}, Lwk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwz;

    .line 363
    invoke-virtual {v0, p1}, Lwz;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 77
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 78
    invoke-virtual {p0}, Lwk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 83
    :goto_0
    return-void

    .line 80
    :cond_0
    invoke-virtual {p0}, Lwk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwz;

    .line 81
    invoke-virtual {v0}, Lwz;->b()V

    goto :goto_0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lwk;->g()Lwk;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 91
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 92
    invoke-virtual {p0}, Lwk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    .line 97
    :goto_0
    return-void

    .line 94
    :cond_0
    invoke-virtual {p0}, Lwk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwz;

    .line 95
    invoke-virtual {v0}, Lwz;->c()V

    goto :goto_0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 191
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 192
    invoke-virtual {p0}, Lwk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    .line 195
    :goto_0
    return v0

    .line 194
    :cond_0
    invoke-virtual {p0}, Lwk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwz;

    .line 195
    invoke-virtual {v0}, Lwz;->d()Z

    move-result v0

    goto :goto_0
.end method

.method public f()V
    .locals 4

    .prologue
    .line 283
    iget-object v0, p0, Lwk;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 284
    iget-object v0, p0, Lwk;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 285
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v1, v3, :cond_0

    .line 286
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {p0}, Lwk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    check-cast v1, Lwn;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->removeListener(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0

    .line 288
    :cond_0
    invoke-virtual {p0}, Lwk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lwz;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lxa;

    .line 289
    invoke-virtual {v1, v0}, Lwz;->b(Lxa;)V

    goto :goto_0

    .line 292
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lwk;->a:Ljava/util/ArrayList;

    .line 294
    :cond_2
    return-void
.end method

.method public g()Lwk;
    .locals 1

    .prologue
    .line 298
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    invoke-direct {v0}, Ljava/lang/CloneNotSupportedException;-><init>()V

    throw v0
.end method
