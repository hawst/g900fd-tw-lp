.class public final Ldxx;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldxx;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 409
    const/4 v0, 0x0

    new-array v0, v0, [Ldxx;

    sput-object v0, Ldxx;->a:[Ldxx;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 410
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 436
    const/4 v0, 0x0

    .line 437
    iget-object v1, p0, Ldxx;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 438
    const/4 v0, 0x1

    iget-object v1, p0, Ldxx;->b:Ljava/lang/String;

    .line 439
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 441
    :cond_0
    iget-object v1, p0, Ldxx;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 442
    const/4 v1, 0x2

    iget-object v2, p0, Ldxx;->c:Ljava/lang/Long;

    .line 443
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 445
    :cond_1
    iget-object v1, p0, Ldxx;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 446
    const/4 v1, 0x3

    iget-object v2, p0, Ldxx;->d:Ljava/lang/String;

    .line 447
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 449
    :cond_2
    iget-object v1, p0, Ldxx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 450
    iput v0, p0, Ldxx;->cachedSize:I

    .line 451
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 406
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldxx;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldxx;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldxx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldxx;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldxx;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldxx;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 421
    iget-object v0, p0, Ldxx;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 422
    const/4 v0, 0x1

    iget-object v1, p0, Ldxx;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 424
    :cond_0
    iget-object v0, p0, Ldxx;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 425
    const/4 v0, 0x2

    iget-object v1, p0, Ldxx;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 427
    :cond_1
    iget-object v0, p0, Ldxx;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 428
    const/4 v0, 0x3

    iget-object v1, p0, Ldxx;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 430
    :cond_2
    iget-object v0, p0, Ldxx;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 432
    return-void
.end method
