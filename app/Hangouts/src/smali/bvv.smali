.class public final Lbvv;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lbys;->n:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbvv;->a:Z

    return-void
.end method

.method private static a(Landroid/content/Context;Lsc;)Lbvw;
    .locals 7

    .prologue
    .line 137
    sget-boolean v0, Lbvv;->a:Z

    if-eqz v0, :cond_1

    .line 138
    const-string v0, "Babel"

    const-string v1, "MmsTransactions.sendSendReq"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "====> from="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lsc;->d()Lrh;

    move-result-object v2

    invoke-virtual {v2}, Lrh;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    invoke-virtual {p1}, Lsc;->f()[Lrh;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 141
    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "====> to="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lrh;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 143
    :cond_0
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "====> size="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lsc;->h()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "====> parts="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lsc;->e()Lrm;

    move-result-object v2

    invoke-virtual {v2}, Lrm;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    :cond_1
    :try_start_0
    invoke-static {p0}, Lbvq;->b(Landroid/content/Context;)Lbwq;

    move-result-object v0

    .line 150
    const/4 v1, 0x0

    invoke-static {p0, v0, p1, v1}, Lbvq;->a(Landroid/content/Context;Lbwq;Lri;Ljava/lang/String;)Lri;

    move-result-object v0

    .line 152
    if-nez v0, :cond_2

    .line 153
    new-instance v0, Lbvu;

    const-string v1, "MmsTransactions: send: get empty response"

    invoke-direct {v0, v1}, Lbvu;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 239
    :catch_0
    move-exception v0

    .line 243
    :try_start_1
    new-instance v1, Lbvu;

    invoke-direct {v1, v0}, Lbvu;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 246
    :catchall_0
    move-exception v0

    invoke-static {p0}, Lbvq;->c(Landroid/content/Context;)V

    throw v0

    .line 156
    :cond_2
    :try_start_2
    instance-of v1, v0, Lsb;

    if-nez v1, :cond_3

    .line 157
    new-instance v0, Lbvu;

    const-string v1, "MmsTransactions: send: get invalid response type"

    invoke-direct {v0, v1}, Lbvu;-><init>(Ljava/lang/String;)V

    throw v0

    .line 160
    :cond_3
    check-cast v0, Lsb;

    .line 161
    invoke-virtual {v0}, Lsb;->f()I

    move-result v1

    .line 162
    sparse-switch v1, :sswitch_data_0

    .line 236
    new-instance v0, Lbvu;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MmsTransactions: send: resp error "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbvu;-><init>(Ljava/lang/String;)V

    throw v0

    .line 164
    :sswitch_0
    new-instance v1, Lbvw;

    invoke-direct {v1, p1, v0}, Lbvw;-><init>(Lsc;Lsb;)V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 246
    invoke-static {p0}, Lbvq;->c(Landroid/content/Context;)V

    return-object v1

    .line 168
    :sswitch_1
    :try_start_3
    new-instance v0, Lbvp;

    const/16 v2, 0x77

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mms error response "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lbvp;-><init>(ILjava/lang/String;)V

    throw v0

    .line 174
    :sswitch_2
    new-instance v0, Lbvp;

    const/16 v2, 0x80

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mms error response "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lbvp;-><init>(ILjava/lang/String;)V

    throw v0

    .line 181
    :sswitch_3
    new-instance v0, Lbvp;

    const/16 v2, 0x7f

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mms error response "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lbvp;-><init>(ILjava/lang/String;)V

    throw v0

    .line 187
    :sswitch_4
    new-instance v0, Lbvp;

    const/16 v2, 0x81

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mms error response "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lbvp;-><init>(ILjava/lang/String;)V

    throw v0

    .line 194
    :sswitch_5
    new-instance v0, Lbvp;

    const/16 v2, 0x7d

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mms error response "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lbvp;-><init>(ILjava/lang/String;)V

    throw v0

    .line 199
    :sswitch_6
    new-instance v0, Lbvp;

    const/16 v2, 0x7e

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mms error response "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lbvp;-><init>(ILjava/lang/String;)V

    throw v0

    .line 205
    :sswitch_7
    new-instance v0, Lbvp;

    const/16 v2, 0x7c

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mms error response "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lbvp;-><init>(ILjava/lang/String;)V

    throw v0

    .line 210
    :sswitch_8
    new-instance v0, Lbvp;

    const/16 v2, 0x82

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mms error response "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lbvp;-><init>(ILjava/lang/String;)V

    throw v0

    .line 218
    :sswitch_9
    new-instance v0, Lbvp;

    const/16 v2, 0x83

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mms error response "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lbvp;-><init>(ILjava/lang/String;)V

    throw v0

    .line 223
    :sswitch_a
    new-instance v0, Lbvp;

    const/16 v2, 0x84

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mms error response "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lbvp;-><init>(ILjava/lang/String;)V

    throw v0

    .line 228
    :sswitch_b
    new-instance v0, Lbvp;

    const/16 v2, 0x85

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mms error response "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lbvp;-><init>(ILjava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 162
    nop

    :sswitch_data_0
    .sparse-switch
        0x80 -> :sswitch_0
        0x81 -> :sswitch_1
        0x82 -> :sswitch_7
        0x83 -> :sswitch_2
        0x84 -> :sswitch_5
        0x85 -> :sswitch_3
        0x86 -> :sswitch_6
        0x87 -> :sswitch_4
        0x88 -> :sswitch_8
        0xc1 -> :sswitch_5
        0xc2 -> :sswitch_3
        0xe0 -> :sswitch_1
        0xe1 -> :sswitch_7
        0xe2 -> :sswitch_2
        0xe3 -> :sswitch_5
        0xe4 -> :sswitch_3
        0xe5 -> :sswitch_4
        0xe6 -> :sswitch_9
        0xe7 -> :sswitch_9
        0xe8 -> :sswitch_9
        0xe9 -> :sswitch_9
        0xea -> :sswitch_a
        0xeb -> :sswitch_b
    .end sparse-switch
.end method

.method public static a(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Lbvw;
    .locals 4

    .prologue
    .line 115
    invoke-static {p0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 116
    if-eqz p1, :cond_0

    array-length v0, p1

    if-gtz v0, :cond_1

    .line 117
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MmsTransactions: send: no recipient"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :cond_1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 120
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MmsTransactions: forwardMessage: no mms uri"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_2
    :try_start_0
    new-instance v0, Lsc;

    invoke-direct {v0}, Lsc;-><init>()V

    invoke-static {p0}, Lsi;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v2, Lrh;

    invoke-direct {v2, v1}, Lrh;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lsc;->a(Lrh;)V

    :cond_3
    invoke-static {p1}, Lrh;->a([Ljava/lang/String;)[Lrh;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0, v1}, Lsc;->a([Lrh;)V

    :cond_4
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    new-instance v1, Lrh;

    invoke-direct {v1, p2}, Lrh;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lsc;->b(Lrh;)V

    :cond_5
    const-wide/32 v1, 0xf4240

    div-long v1, p4, v1

    invoke-virtual {v0, v1, v2}, Lsc;->a(J)V

    invoke-static {p0, p3}, Lbvx;->a(Landroid/content/Context;Ljava/lang/String;)Lbvz;

    move-result-object v1

    iget v2, v1, Lbvz;->a:I

    if-gtz v2, :cond_6

    const/16 v2, 0x418

    iput v2, v1, Lbvz;->a:I

    :cond_6
    iget-object v2, v1, Lbvz;->b:Lrm;

    invoke-virtual {v0, v2}, Lsc;->a(Lrm;)V

    iget v1, v1, Lbvz;->a:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lsc;->b(J)V

    const-string v1, "personal"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lsc;->a([B)V

    invoke-virtual {v0}, Lsc;->j()V

    invoke-virtual {v0}, Lsc;->g()V

    invoke-virtual {v0}, Lsc;->i()V

    invoke-virtual {v0}, Lsc;->k()V
    :try_end_0
    .catch Lrc; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    invoke-static {p0, v0}, Lbvv;->a(Landroid/content/Context;Lsc;)Lbvw;

    move-result-object v0

    return-object v0

    .line 127
    :catch_0
    move-exception v0

    .line 128
    new-instance v1, Lbvp;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fail to create forward SenedReq: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lbvp;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIJ)Lbvw;
    .locals 8

    .prologue
    .line 56
    invoke-static {p0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 57
    if-eqz p1, :cond_0

    array-length v0, p1

    if-gtz v0, :cond_1

    .line 58
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MmsTransactions: send: no recipient"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 61
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 62
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 63
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MmsTransactions: send: no content (subject or text or image)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_2
    array-length v0, p1

    new-array v1, v0, [Ljava/lang/String;

    .line 70
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_4

    .line 71
    aget-object v2, p1, v0

    .line 72
    aget-object v3, p1, v0

    invoke-static {v3}, Laea;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 74
    aput-object v2, v1, v0

    .line 70
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :cond_3
    invoke-static {v2}, Lbvv;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_1

    .line 79
    :cond_4
    new-instance v7, Lsc;

    invoke-direct {v7}, Lsc;-><init>()V

    invoke-static {p0}, Lsi;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    new-instance v2, Lrh;

    invoke-direct {v2, v0}, Lrh;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Lsc;->a(Lrh;)V

    :cond_5
    invoke-static {v1}, Lrh;->a([Ljava/lang/String;)[Lrh;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v7, v0}, Lsc;->a([Lrh;)V

    :cond_6
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    new-instance v0, Lrh;

    invoke-direct {v0, p2}, Lrh;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Lsc;->b(Lrh;)V

    :cond_7
    const-wide/32 v0, 0xf4240

    div-long v0, p9, v0

    invoke-virtual {v7, v0, v1}, Lsc;->a(J)V

    move-object v0, p0

    move-object v1, p3

    move-object v2, p4

    move-object v3, p5

    move v4, p6

    move v5, p7

    move/from16 v6, p8

    invoke-static/range {v0 .. v6}, Lbvx;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)Lbvz;

    move-result-object v0

    iget v1, v0, Lbvz;->a:I

    if-gtz v1, :cond_8

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MmsTransactions: send: zero size body"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    iget-object v1, v0, Lbvz;->b:Lrm;

    invoke-virtual {v7, v1}, Lsc;->a(Lrm;)V

    iget v0, v0, Lbvz;->a:I

    int-to-long v0, v0

    invoke-virtual {v7, v0, v1}, Lsc;->b(J)V

    const-string v0, "personal"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v7, v0}, Lsc;->a([B)V

    invoke-virtual {v7}, Lsc;->j()V

    invoke-virtual {v7}, Lsc;->g()V

    invoke-virtual {v7}, Lsc;->i()V

    invoke-virtual {v7}, Lsc;->k()V

    .line 83
    invoke-static {p0, v7}, Lbvv;->a(Landroid/content/Context;Lsc;)Lbvw;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 91
    if-nez p0, :cond_0

    .line 92
    const/4 v0, 0x0

    .line 102
    :goto_0
    return-object v0

    .line 94
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 95
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 96
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_3

    .line 97
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 98
    invoke-static {v3}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v4

    if-nez v4, :cond_1

    const/16 v4, 0x2b

    if-eq v3, v4, :cond_1

    const/16 v4, 0x2a

    if-eq v3, v4, :cond_1

    const/16 v4, 0x23

    if-ne v3, v4, :cond_2

    .line 99
    :cond_1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 96
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 102
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;[BLjava/lang/String;)Lsa;
    .locals 3

    .prologue
    .line 359
    sget-boolean v0, Lbvv;->a:Z

    if-eqz v0, :cond_0

    .line 360
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MmsTransaction.retrieveMessage: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 363
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MmsTransactions: retrieve: empty URL"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 368
    :cond_1
    :try_start_0
    invoke-static {p0}, Lbvq;->b(Landroid/content/Context;)Lbwq;

    move-result-object v1

    .line 370
    invoke-static {p0, v1, p2}, Lbvq;->a(Landroid/content/Context;Lbwq;Ljava/lang/String;)Lri;

    move-result-object v0

    .line 372
    if-nez v0, :cond_2

    .line 373
    new-instance v0, Lbvp;

    const/16 v1, 0x89

    const-string v2, "MmsTransactions: retrieve: get empty or invalid response"

    invoke-direct {v0, v1, v2}, Lbvp;-><init>(ILjava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 390
    :catchall_0
    move-exception v0

    invoke-static {p0}, Lbvq;->c(Landroid/content/Context;)V

    throw v0

    .line 378
    :cond_2
    :try_start_1
    instance-of v2, v0, Lsa;

    if-nez v2, :cond_3

    .line 379
    const/16 v0, 0x84

    invoke-static {p0, v1, p1, p2, v0}, Lbvv;->a(Landroid/content/Context;Lbwq;[BLjava/lang/String;I)V

    .line 381
    new-instance v0, Lbvu;

    const-string v1, "MmsTransactions: retrieve: get invalid response type"

    invoke-direct {v0, v1}, Lbvu;-><init>(Ljava/lang/String;)V

    throw v0

    .line 384
    :cond_3
    const/16 v2, 0x81

    invoke-static {p0, v1, p1, p2, v2}, Lbvv;->a(Landroid/content/Context;Lbwq;[BLjava/lang/String;I)V

    .line 387
    check-cast v0, Lsa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 390
    invoke-static {p0}, Lbvq;->c(Landroid/content/Context;)V

    return-object v0
.end method

.method private static a(Landroid/content/Context;Lbwq;[BLjava/lang/String;I)V
    .locals 3

    .prologue
    .line 398
    sget-boolean v0, Lbvv;->a:Z

    if-eqz v0, :cond_0

    .line 399
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MmsTransaction.sendNotifyResponse: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    :cond_0
    new-instance v0, Lrl;

    invoke-direct {v0, p2, p4}, Lrl;-><init>([BI)V

    .line 406
    invoke-static {}, Lbvm;->a()Lsm;

    move-result-object v1

    invoke-virtual {v1}, Lsm;->s()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 405
    :goto_0
    invoke-static {p0, p1, v0, p3}, Lbvq;->a(Landroid/content/Context;Lbwq;Lri;Ljava/lang/String;)Lri;

    .line 407
    return-void

    .line 406
    :cond_1
    const/4 p3, 0x0

    goto :goto_0
.end method
