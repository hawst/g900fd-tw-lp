.class public final Lenr;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lenr;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Lenz;

.field public d:Leoo;

.field public e:Lekn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 622
    const/4 v0, 0x0

    new-array v0, v0, [Lenr;

    sput-object v0, Lenr;->a:[Lenr;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 623
    invoke-direct {p0}, Lepn;-><init>()V

    .line 633
    iput-object v0, p0, Lenr;->b:Ljava/lang/Integer;

    .line 636
    iput-object v0, p0, Lenr;->c:Lenz;

    .line 639
    iput-object v0, p0, Lenr;->d:Leoo;

    .line 642
    iput-object v0, p0, Lenr;->e:Lekn;

    .line 623
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 665
    const/4 v0, 0x0

    .line 666
    iget-object v1, p0, Lenr;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 667
    const/4 v0, 0x1

    iget-object v1, p0, Lenr;->b:Ljava/lang/Integer;

    .line 668
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 670
    :cond_0
    iget-object v1, p0, Lenr;->c:Lenz;

    if-eqz v1, :cond_1

    .line 671
    const/4 v1, 0x2

    iget-object v2, p0, Lenr;->c:Lenz;

    .line 672
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 674
    :cond_1
    iget-object v1, p0, Lenr;->d:Leoo;

    if-eqz v1, :cond_2

    .line 675
    const/4 v1, 0x3

    iget-object v2, p0, Lenr;->d:Leoo;

    .line 676
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 678
    :cond_2
    iget-object v1, p0, Lenr;->e:Lekn;

    if-eqz v1, :cond_3

    .line 679
    const/4 v1, 0x4

    iget-object v2, p0, Lenr;->e:Lekn;

    .line 680
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 682
    :cond_3
    iget-object v1, p0, Lenr;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 683
    iput v0, p0, Lenr;->cachedSize:I

    .line 684
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 619
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lenr;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lenr;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lenr;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lenr;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lenr;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lenr;->c:Lenz;

    if-nez v0, :cond_4

    new-instance v0, Lenz;

    invoke-direct {v0}, Lenz;-><init>()V

    iput-object v0, p0, Lenr;->c:Lenz;

    :cond_4
    iget-object v0, p0, Lenr;->c:Lenz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lenr;->d:Leoo;

    if-nez v0, :cond_5

    new-instance v0, Leoo;

    invoke-direct {v0}, Leoo;-><init>()V

    iput-object v0, p0, Lenr;->d:Leoo;

    :cond_5
    iget-object v0, p0, Lenr;->d:Leoo;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lenr;->e:Lekn;

    if-nez v0, :cond_6

    new-instance v0, Lekn;

    invoke-direct {v0}, Lekn;-><init>()V

    iput-object v0, p0, Lenr;->e:Lekn;

    :cond_6
    iget-object v0, p0, Lenr;->e:Lekn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 647
    iget-object v0, p0, Lenr;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 648
    const/4 v0, 0x1

    iget-object v1, p0, Lenr;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 650
    :cond_0
    iget-object v0, p0, Lenr;->c:Lenz;

    if-eqz v0, :cond_1

    .line 651
    const/4 v0, 0x2

    iget-object v1, p0, Lenr;->c:Lenz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 653
    :cond_1
    iget-object v0, p0, Lenr;->d:Leoo;

    if-eqz v0, :cond_2

    .line 654
    const/4 v0, 0x3

    iget-object v1, p0, Lenr;->d:Leoo;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 656
    :cond_2
    iget-object v0, p0, Lenr;->e:Lekn;

    if-eqz v0, :cond_3

    .line 657
    const/4 v0, 0x4

    iget-object v1, p0, Lenr;->e:Lekn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 659
    :cond_3
    iget-object v0, p0, Lenr;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 661
    return-void
.end method
