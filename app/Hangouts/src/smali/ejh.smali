.class public final Lejh;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lejh;


# instance fields
.field public b:Lejk;

.field public c:Lejj;

.field public d:Lejl;

.field public e:Leji;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lejh;

    sput-object v0, Lejh;->a:[Lejh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 302
    iput-object v0, p0, Lejh;->b:Lejk;

    .line 305
    iput-object v0, p0, Lejh;->c:Lejj;

    .line 308
    iput-object v0, p0, Lejh;->d:Lejl;

    .line 311
    iput-object v0, p0, Lejh;->e:Leji;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 334
    const/4 v0, 0x0

    .line 335
    iget-object v1, p0, Lejh;->b:Lejk;

    if-eqz v1, :cond_0

    .line 336
    const/4 v0, 0x1

    iget-object v1, p0, Lejh;->b:Lejk;

    .line 337
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 339
    :cond_0
    iget-object v1, p0, Lejh;->c:Lejj;

    if-eqz v1, :cond_1

    .line 340
    const/4 v1, 0x2

    iget-object v2, p0, Lejh;->c:Lejj;

    .line 341
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 343
    :cond_1
    iget-object v1, p0, Lejh;->d:Lejl;

    if-eqz v1, :cond_2

    .line 344
    const/4 v1, 0x3

    iget-object v2, p0, Lejh;->d:Lejl;

    .line 345
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 347
    :cond_2
    iget-object v1, p0, Lejh;->e:Leji;

    if-eqz v1, :cond_3

    .line 348
    const/4 v1, 0x4

    iget-object v2, p0, Lejh;->e:Leji;

    .line 349
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 351
    :cond_3
    iget-object v1, p0, Lejh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 352
    iput v0, p0, Lejh;->cachedSize:I

    .line 353
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lejh;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lejh;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lejh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lejh;->b:Lejk;

    if-nez v0, :cond_2

    new-instance v0, Lejk;

    invoke-direct {v0}, Lejk;-><init>()V

    iput-object v0, p0, Lejh;->b:Lejk;

    :cond_2
    iget-object v0, p0, Lejh;->b:Lejk;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lejh;->c:Lejj;

    if-nez v0, :cond_3

    new-instance v0, Lejj;

    invoke-direct {v0}, Lejj;-><init>()V

    iput-object v0, p0, Lejh;->c:Lejj;

    :cond_3
    iget-object v0, p0, Lejh;->c:Lejj;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lejh;->d:Lejl;

    if-nez v0, :cond_4

    new-instance v0, Lejl;

    invoke-direct {v0}, Lejl;-><init>()V

    iput-object v0, p0, Lejh;->d:Lejl;

    :cond_4
    iget-object v0, p0, Lejh;->d:Lejl;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lejh;->e:Leji;

    if-nez v0, :cond_5

    new-instance v0, Leji;

    invoke-direct {v0}, Leji;-><init>()V

    iput-object v0, p0, Lejh;->e:Leji;

    :cond_5
    iget-object v0, p0, Lejh;->e:Leji;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lejh;->b:Lejk;

    if-eqz v0, :cond_0

    .line 317
    const/4 v0, 0x1

    iget-object v1, p0, Lejh;->b:Lejk;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 319
    :cond_0
    iget-object v0, p0, Lejh;->c:Lejj;

    if-eqz v0, :cond_1

    .line 320
    const/4 v0, 0x2

    iget-object v1, p0, Lejh;->c:Lejj;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 322
    :cond_1
    iget-object v0, p0, Lejh;->d:Lejl;

    if-eqz v0, :cond_2

    .line 323
    const/4 v0, 0x3

    iget-object v1, p0, Lejh;->d:Lejl;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 325
    :cond_2
    iget-object v0, p0, Lejh;->e:Leji;

    if-eqz v0, :cond_3

    .line 326
    const/4 v0, 0x4

    iget-object v1, p0, Lejh;->e:Leji;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 328
    :cond_3
    iget-object v0, p0, Lejh;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 330
    return-void
.end method
