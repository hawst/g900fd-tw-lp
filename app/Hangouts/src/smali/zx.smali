.class public Lzx;
.super Lbsv;
.source "PG"

# interfaces
.implements Lbyg;


# static fields
.field private static final f:Z


# instance fields
.field private final g:Laac;

.field private final h:Z

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lbys;->c:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lzx;->f:Z

    return-void
.end method

.method public constructor <init>(Lbyq;Laac;Ljava/lang/String;ZZLjava/lang/Object;)V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0, p5, p1, p3, p6}, Lbsv;-><init>(ZLbyu;Ljava/lang/String;Ljava/lang/Object;)V

    .line 103
    const/4 v0, 0x0

    iput-boolean v0, p0, Lzx;->i:Z

    .line 95
    iput-object p2, p0, Lzx;->g:Laac;

    .line 96
    iput-boolean p4, p0, Lzx;->h:Z

    .line 97
    return-void
.end method

.method public constructor <init>(Lbyq;Laac;ZLjava/lang/Object;)V
    .locals 7

    .prologue
    .line 89
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lzx;-><init>(Lbyq;Laac;Ljava/lang/String;ZZLjava/lang/Object;)V

    .line 90
    return-void
.end method

.method static synthetic a(Lzx;)Laac;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lzx;->g:Laac;

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Lbsm;
    .locals 5

    .prologue
    .line 170
    sget-boolean v0, Lzx;->f:Z

    if-eqz v0, :cond_0

    .line 171
    const-string v0, "Babel_medialoader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "loading local image "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    :cond_0
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 174
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 178
    :try_start_0
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 179
    invoke-static {v2, v0}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 180
    const-string v0, "image/jpeg"

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    .line 185
    :goto_0
    if-eqz v0, :cond_2

    .line 186
    invoke-static {v0}, Lbyr;->a(Ljava/io/InputStream;)[B

    move-result-object v2

    .line 187
    new-instance v0, Lbsm;

    const/4 v3, 0x1

    invoke-direct {v0, v2, v1, v3}, Lbsm;-><init>([BLjava/lang/String;Z)V

    .line 192
    :goto_1
    return-object v0

    .line 182
    :cond_1
    invoke-virtual {v2, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 183
    invoke-static {v2, v0}, Lbyr;->b(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    goto :goto_0

    .line 189
    :catch_0
    move-exception v0

    .line 190
    const-string v1, "Babel_medialoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "exception reading image "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 192
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Lbzn;Lbyd;ZZ)V
    .locals 7

    .prologue
    .line 272
    if-eqz p3, :cond_0

    if-eqz p1, :cond_0

    .line 273
    invoke-virtual {p1}, Lbzn;->e()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    iget-object v0, p0, Lzx;->b:Lbyu;

    .line 274
    invoke-virtual {v0}, Lbyu;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    if-eqz p3, :cond_1

    if-nez p1, :cond_1

    if-eqz p2, :cond_1

    iget-object v0, p0, Lzx;->b:Lbyu;

    .line 275
    invoke-virtual {v0}, Lbyu;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    if-eqz p3, :cond_2

    iget-object v0, p0, Lzx;->b:Lbyu;

    .line 276
    invoke-virtual {v0}, Lbyu;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez p1, :cond_2

    if-eqz p2, :cond_3

    :cond_2
    if-nez p3, :cond_4

    if-nez p1, :cond_4

    if-nez p2, :cond_4

    :cond_3
    const/4 v0, 0x1

    .line 272
    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 279
    invoke-virtual {p0}, Lzx;->n()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 281
    :try_start_0
    const-string v0, "ImageRequest.deliverImageOnCorrectThread"

    invoke-static {v0}, Lbzq;->a(Ljava/lang/String;)V

    .line 282
    iget-object v0, p0, Lzx;->g:Laac;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p0

    move v5, p4

    invoke-interface/range {v0 .. v5}, Laac;->a(Lbzn;Lbyd;ZLzx;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 284
    invoke-static {}, Lbzq;->a()V

    .line 303
    :goto_1
    return-void

    .line 276
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 284
    :catchall_0
    move-exception v0

    invoke-static {}, Lbzq;->a()V

    throw v0

    .line 289
    :cond_5
    new-instance v6, Ljava/lang/Thread;

    new-instance v0, Laaa;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Laaa;-><init>(Lzx;Lbzn;Lbyd;ZZ)V

    const-string v1, "CACHED_BITMAP_TRANSFER_THREAD"

    invoke-direct {v6, v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 301
    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    goto :goto_1
.end method

.method private a(Lbyu;I)Z
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 205
    invoke-static {p1}, Lcwz;->b(Ljava/lang/Object;)V

    .line 206
    invoke-virtual {p1}, Lbyu;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 207
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 209
    sget-boolean v0, Lzx;->f:Z

    if-eqz v0, :cond_0

    .line 210
    const-string v0, "Babel_medialoader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "ImageRequest loadImageFromVolley retryCount="

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " this="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 211
    invoke-virtual {p0}, Lzx;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 210
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :cond_0
    const/4 v0, 0x5

    if-le p2, v0, :cond_1

    .line 215
    const-string v0, "Babel_medialoader"

    const-string v1, "Load image from volley retried several times and failed."

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v7

    .line 260
    :goto_0
    return v0

    .line 220
    :cond_1
    :try_start_0
    invoke-static {}, Lavk;->a()Lvd;

    move-result-object v9

    .line 222
    iget-boolean v0, p0, Lzx;->i:Z

    if-eqz v0, :cond_2

    invoke-virtual {v9}, Lvd;->b()Luo;

    move-result-object v0

    invoke-virtual {p1}, Lbyu;->p()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Luo;->a(Ljava/lang/String;)Lup;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v8

    .line 260
    :goto_1
    if-nez v0, :cond_3

    move v0, v8

    goto :goto_0

    .line 225
    :cond_2
    new-instance v10, Lxr;

    new-instance v0, Lzy;

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lzy;-><init>(Lzx;JLbyu;ILbsv;)V

    new-instance v1, Lzz;

    invoke-direct {v1, p0, p0}, Lzz;-><init>(Lzx;Lbsv;)V

    invoke-direct {v10, p1, v0, v1}, Lxr;-><init>(Lbyu;Lvg;Lvf;)V

    .line 254
    invoke-virtual {v9, v10}, Lvd;->a(Lva;)Lva;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v7

    .line 259
    goto :goto_1

    .line 256
    :catch_0
    move-exception v0

    .line 257
    const-string v1, "Babel_medialoader"

    const-string v2, "Failed to request image"

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v8

    .line 258
    goto :goto_1

    :cond_3
    move v0, v7

    .line 260
    goto :goto_0
.end method

.method static synthetic a(Lzx;Lbyu;I)Z
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lzx;->a(Lbyu;I)Z

    move-result v0

    return v0
.end method

.method static synthetic i()Z
    .locals 1

    .prologue
    .line 55
    sget-boolean v0, Lzx;->f:Z

    return v0
.end method


# virtual methods
.method public a([B)Lbsu;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 353
    sget-boolean v0, Lzx;->f:Z

    if-eqz v0, :cond_0

    .line 354
    const-string v0, "Babel_medialoader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "ImageRequest decodeBytes starting for request "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lzx;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    :cond_0
    invoke-static {p1}, Lbyd;->a([B)Z

    move-result v0

    if-nez v0, :cond_9

    .line 359
    if-eqz p1, :cond_4

    array-length v0, p1

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 360
    invoke-static {}, Lbyr;->a()Lbxs;

    move-result-object v4

    iget-object v0, p0, Lzx;->b:Lbyu;

    check-cast v0, Lbyq;

    iget-object v1, p0, Lzx;->b:Lbyu;

    check-cast v1, Lbyq;

    invoke-virtual {v1}, Lbyq;->g()I

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    array-length v5, p1

    int-to-long v5, v5

    invoke-static {v1, v5, v6}, Lf;->a(Ljava/io/InputStream;J)I

    move-result v1

    :cond_1
    invoke-virtual {v0}, Lbyq;->c()I

    move-result v5

    invoke-virtual {v0}, Lbyq;->d()I

    move-result v0

    invoke-virtual {v4, p1, v5, v0, v1}, Lbxs;->a([BIII)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-boolean v0, Lzx;->f:Z

    if-eqz v0, :cond_2

    const-string v5, "Babel_medialoader"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "decodeStaticImageBytes in bytes="

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_5

    :goto_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " bitmap out="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v1, :cond_6

    move-object v0, v3

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    if-nez v1, :cond_7

    invoke-static {p0}, Lbsn;->a(Lbsv;)V

    .line 379
    :cond_3
    :goto_3
    return-object v3

    :cond_4
    move v0, v2

    .line 359
    goto :goto_0

    .line 360
    :cond_5
    array-length v2, p1

    goto :goto_1

    :cond_6
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_2

    :cond_7
    invoke-virtual {p0, v1}, Lzx;->b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eq v0, v1, :cond_8

    invoke-virtual {v4, v1}, Lbxs;->a(Landroid/graphics/Bitmap;)V

    :cond_8
    new-instance v3, Lbzn;

    invoke-virtual {p0}, Lzx;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v0, v1}, Lbzn;-><init>(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    invoke-virtual {v3}, Lbzn;->a()V

    invoke-virtual {p0}, Lzx;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0, v3}, Lbxs;->a(Ljava/lang/String;Lbzn;)Lbzn;

    sget-boolean v0, Lzx;->f:Z

    if-eqz v0, :cond_3

    const-string v0, "Babel_medialoader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ImageRequest decodeBytes ended for request "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lzx;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 364
    :cond_9
    iget-object v0, p0, Lzx;->b:Lbyu;

    check-cast v0, Lbyq;

    invoke-virtual {v0}, Lbyq;->i()Z

    move-result v4

    .line 365
    if-eqz v4, :cond_a

    move-object v0, p0

    .line 366
    :goto_4
    new-instance v1, Lbyd;

    .line 367
    invoke-static {}, Lbyr;->a()Lbxs;

    move-result-object v5

    invoke-direct {v1, p1, v0, v5}, Lbyd;-><init>([BLbyg;Lbxs;)V

    .line 370
    invoke-virtual {v1}, Lbyd;->e()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 371
    invoke-static {p0}, Lbsn;->a(Lbsv;)V

    goto :goto_3

    :cond_a
    move-object v0, v3

    .line 365
    goto :goto_4

    .line 375
    :cond_b
    if-eqz v4, :cond_c

    .line 376
    invoke-virtual {v1, v2}, Lbyd;->a(Z)V

    goto :goto_3

    :cond_c
    move-object v3, v1

    .line 379
    goto :goto_3
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 388
    invoke-virtual {p0, p1}, Lzx;->b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 389
    if-eq v0, p1, :cond_0

    .line 390
    invoke-static {}, Lbyr;->a()Lbxs;

    move-result-object v1

    invoke-virtual {v1, p1}, Lbxs;->a(Landroid/graphics/Bitmap;)V

    .line 393
    :cond_0
    new-instance v1, Lbzn;

    invoke-virtual {p0}, Lzx;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lbzn;-><init>(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 394
    invoke-virtual {v1}, Lbzn;->a()V

    .line 395
    invoke-static {}, Lbyr;->a()Lbxs;

    move-result-object v0

    invoke-virtual {p0}, Lzx;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lbxs;->a(Ljava/lang/String;Lbzn;)Lbzn;

    .line 396
    new-instance v0, Ljava/lang/Thread;

    new-instance v2, Laab;

    invoke-direct {v2, p0, v1}, Laab;-><init>(Lzx;Lbzn;)V

    const-string v1, "STATIC_GIF_TRANSFER_THREAD"

    invoke-direct {v0, v2, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 401
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 402
    return-void
.end method

.method public a(Lbsu;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 503
    sget-boolean v0, Lzx;->f:Z

    if-eqz v0, :cond_0

    .line 504
    const-string v0, "Babel_medialoader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ImageRequest deliverDecoded for request "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lzx;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    :cond_0
    iget-object v0, p0, Lzx;->g:Laac;

    if-eqz v0, :cond_1

    .line 507
    instance-of v0, p1, Lbyd;

    if-eqz v0, :cond_2

    .line 508
    check-cast p1, Lbyd;

    .line 509
    invoke-direct {p0, v5, p1, v4, v3}, Lzx;->a(Lbzn;Lbyd;ZZ)V

    .line 516
    :cond_1
    :goto_0
    return-void

    .line 511
    :cond_2
    check-cast p1, Lbzn;

    .line 512
    invoke-virtual {p1}, Lbzn;->a()V

    .line 513
    invoke-direct {p0, p1, v5, v4, v3}, Lzx;->a(Lbzn;Lbyd;ZZ)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 537
    iget-object v0, p0, Lzx;->b:Lbyu;

    instance-of v0, v0, Lbyq;

    if-eqz v0, :cond_0

    .line 538
    iget-object v0, p0, Lzx;->b:Lbyu;

    check-cast v0, Lbyq;

    invoke-virtual {v0, p1}, Lbyq;->b(Z)Lbyq;

    .line 540
    :cond_0
    return-void
.end method

.method protected final b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 450
    invoke-static {}, Lbyr;->a()Lbxs;

    move-result-object v6

    .line 451
    iget-object v0, p0, Lzx;->b:Lbyu;

    check-cast v0, Lbyq;

    .line 453
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Lbyq;->c()I

    move-result v5

    if-ne v1, v5, :cond_0

    .line 454
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {v0}, Lbyq;->d()I

    move-result v5

    if-eq v1, v5, :cond_6

    :cond_0
    move v1, v3

    .line 455
    :goto_0
    invoke-virtual {v0}, Lbyq;->j()Z

    move-result v5

    if-eqz v5, :cond_1

    if-eqz v1, :cond_1

    move v2, v3

    .line 456
    :cond_1
    if-nez v2, :cond_2

    invoke-virtual {v0}, Lbyq;->k()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lbyq;->h()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 458
    :cond_2
    invoke-virtual {v0}, Lbyq;->h()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 459
    invoke-static {p1}, Lbyr;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object p1, v2

    .line 464
    :goto_1
    invoke-virtual {v0}, Lbyq;->c()I

    move-result v5

    invoke-virtual {v0}, Lbyq;->d()I

    move-result v7

    .line 463
    invoke-virtual {v6, v5, v7}, Lbxs;->b(II)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 465
    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 467
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 469
    if-eqz v1, :cond_7

    .line 472
    invoke-virtual {v0}, Lbyq;->c()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v1, v9

    .line 473
    invoke-virtual {v0}, Lbyq;->d()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    .line 474
    invoke-virtual {v8, v1, v9}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 475
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 476
    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 478
    :goto_2
    invoke-virtual {v7, p1, v8, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 480
    invoke-virtual {v0}, Lbyq;->k()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 481
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 482
    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->cm:I

    .line 483
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 484
    sget v8, Lf;->dI:I

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    .line 486
    invoke-virtual {v0}, Lbyq;->c()I

    move-result v8

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    sub-int/2addr v8, v9

    int-to-float v8, v8

    sub-float/2addr v8, v3

    .line 487
    invoke-virtual {v0}, Lbyq;->d()I

    move-result v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    sub-int/2addr v0, v9

    int-to-float v0, v0

    sub-float/2addr v0, v3

    .line 485
    invoke-virtual {v7, v1, v8, v0, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 492
    :cond_3
    if-eqz v2, :cond_4

    .line 493
    invoke-virtual {v6, v2}, Lbxs;->a(Landroid/graphics/Bitmap;)V

    :cond_4
    move-object p1, v5

    .line 496
    :cond_5
    return-object p1

    :cond_6
    move v1, v2

    .line 454
    goto/16 :goto_0

    :cond_7
    move-object v1, v4

    goto :goto_2

    :cond_8
    move-object v2, v4

    goto/16 :goto_1
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 109
    iput-boolean p1, p0, Lzx;->i:Z

    .line 110
    return-void
.end method

.method public c_()Lbsm;
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 126
    :try_start_0
    const-string v0, "MediaBytes.getMediaBytes"

    invoke-static {v0}, Lbzq;->a(Ljava/lang/String;)V

    .line 130
    iget-object v0, p0, Lzx;->b:Lbyu;

    .line 131
    invoke-virtual {v0}, Lbyu;->l()Ljava/lang/String;

    move-result-object v4

    .line 132
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v0, v1

    move-object v1, v3

    :goto_0
    move v2, v0

    move-object v0, v1

    .line 159
    :cond_0
    :goto_1
    if-eqz v2, :cond_8

    .line 160
    invoke-static {p0}, Lbsn;->a(Lbsv;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 165
    :goto_2
    invoke-static {}, Lbzq;->a()V

    return-object v3

    .line 134
    :cond_1
    :try_start_1
    instance-of v5, v0, Lbyq;

    if-eqz v5, :cond_3

    check-cast v0, Lbyq;

    invoke-virtual {v0}, Lbyq;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 136
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lbyr;->a(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 137
    if-nez v0, :cond_2

    move v0, v1

    move-object v1, v3

    .line 138
    goto :goto_0

    .line 141
    :cond_2
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 142
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x50

    invoke-virtual {v0, v4, v5, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 143
    new-instance v0, Lbsm;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    const-string v4, "image/jpeg"

    const/4 v5, 0x0

    invoke-direct {v0, v1, v4, v5}, Lbsm;-><init>([BLjava/lang/String;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 165
    :catchall_0
    move-exception v0

    invoke-static {}, Lbzq;->a()V

    throw v0

    .line 146
    :cond_3
    :try_start_2
    const-string v0, "content://"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "android.resource://"

    .line 147
    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 148
    :cond_4
    invoke-static {v4}, Lzx;->a(Ljava/lang/String;)Lbsm;

    move-result-object v0

    .line 149
    if-nez v0, :cond_0

    move v6, v1

    move-object v1, v0

    move v0, v6

    .line 150
    goto :goto_0

    .line 152
    :cond_5
    invoke-static {v4}, Lbrz;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 154
    invoke-static {}, Lbrz;->a()Lbrz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbrz;->a(Lbsv;)V

    move-object v0, v3

    goto :goto_1

    .line 156
    :cond_6
    iget-object v0, p0, Lzx;->b:Lbyu;

    const/4 v4, 0x0

    invoke-direct {p0, v0, v4}, Lzx;->a(Lbyu;I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    move-object v1, v3

    goto :goto_0

    :cond_7
    move v0, v2

    move-object v1, v3

    goto :goto_0

    :cond_8
    move-object v3, v0

    goto :goto_2
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lzx;->i:Z

    return v0
.end method

.method public e()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 311
    invoke-static {}, Lbyr;->a()Lbxs;

    move-result-object v1

    invoke-virtual {p0}, Lzx;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbxs;->a(Ljava/lang/String;)Lbzn;

    move-result-object v1

    .line 312
    if-eqz v1, :cond_1

    .line 313
    sget-boolean v2, Lzx;->f:Z

    if-eqz v2, :cond_0

    .line 314
    const-string v2, "Babel_medialoader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cache hit for image request: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lzx;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    :cond_0
    :goto_0
    if-eqz v1, :cond_2

    iget-object v2, p0, Lzx;->g:Laac;

    if-eqz v2, :cond_2

    .line 323
    const/4 v2, 0x0

    invoke-direct {p0, v1, v2, v0, v0}, Lzx;->a(Lbzn;Lbyd;ZZ)V

    .line 327
    :goto_1
    return v0

    .line 317
    :cond_1
    sget-boolean v2, Lzx;->f:Z

    if-eqz v2, :cond_0

    .line 318
    const-string v2, "Babel_medialoader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cache miss for image request: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lzx;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 327
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lzx;->b:Lbyu;

    invoke-virtual {v0}, Lbyu;->n()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 520
    iget-object v0, p0, Lzx;->g:Laac;

    if-eqz v0, :cond_0

    .line 521
    invoke-direct {p0, v2, v2, v1, v1}, Lzx;->a(Lbzn;Lbyd;ZZ)V

    .line 523
    :cond_0
    return-void
.end method

.method public h()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 527
    iget-object v0, p0, Lzx;->g:Laac;

    if-eqz v0, :cond_0

    .line 528
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v2, v2, v0, v1}, Lzx;->a(Lbzn;Lbyd;ZZ)V

    .line 530
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 336
    iget-object v0, p0, Lzx;->b:Lbyu;

    check-cast v0, Lbyq;

    .line 337
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lbsv;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ImageRequest:  ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lbyq;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 338
    invoke-virtual {v0}, Lbyq;->d()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") shouldUseLoaderQueue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 339
    invoke-virtual {p0}, Lzx;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
