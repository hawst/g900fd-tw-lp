.class public final Lclj;
.super Lchr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lchr",
        "<",
        "Lclb;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile i:Landroid/os/Bundle;

.field private static volatile j:Landroid/os/Bundle;


# instance fields
.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field private final h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcvp;",
            "Lclt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcgn;Lcfw;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lchr;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcgn;Lcfw;[Ljava/lang/String;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lclj;->h:Ljava/util/HashMap;

    iput-object p5, p0, Lclj;->f:Ljava/lang/String;

    iput-object p6, p0, Lclj;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcfv;Lcfw;Ljava/lang/String;)V
    .locals 7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lchu;

    invoke-direct {v3, p2}, Lchu;-><init>(Lcfv;)V

    new-instance v4, Lchx;

    invoke-direct {v4, p3}, Lchx;-><init>(Lcfw;)V

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lclj;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcgn;Lcfw;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;
    .locals 3

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/4 v1, 0x0

    invoke-static {p1}, Lclj;->b(Landroid/os/Bundle;)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/common/data/DataHolder;)Lcwf;
    .locals 4

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcwf;

    new-instance v1, Lcnd;

    sget-object v2, Lclj;->j:Landroid/os/Bundle;

    invoke-direct {v1, v2}, Lcnd;-><init>(Landroid/os/Bundle;)V

    new-instance v2, Lcnc;

    sget-object v3, Lclj;->i:Landroid/os/Bundle;

    invoke-direct {v2, v3}, Lcnc;-><init>(Landroid/os/Bundle;)V

    invoke-direct {v0, p0, v1, v2}, Lcwf;-><init>(Lcom/google/android/gms/common/data/DataHolder;Lcnd;Lcnc;)V

    goto :goto_0
.end method

.method static synthetic a(Lclj;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lclj;->h:Ljava/util/HashMap;

    return-object v0
.end method

.method private declared-synchronized a(Landroid/os/Bundle;)V
    .locals 2

    monitor-enter p0

    if-nez p1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_0
    const-string v0, "use_contactables_api"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Lcmt;->a(Z)V

    sget-object v0, Lcli;->a:Lcli;

    invoke-virtual {v0, p1}, Lcli;->a(Landroid/os/Bundle;)V

    const-string v0, "config.email_type_map"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    sput-object v0, Lclj;->i:Landroid/os/Bundle;

    const-string v0, "config.phone_type_map"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    sput-object v0, Lclj;->j:Landroid/os/Bundle;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static b(Landroid/os/Bundle;)Landroid/app/PendingIntent;
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "pendingIntent"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    goto :goto_0
.end method

.method static synthetic b(ILandroid/os/Bundle;)Lcft;
    .locals 2

    new-instance v0, Lcft;

    invoke-static {p1}, Lclj;->b(Landroid/os/Bundle;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcft;-><init>(ILandroid/app/PendingIntent;)V

    return-object v0
.end method

.method private b(Lcvp;)Lclt;
    .locals 3

    iget-object v1, p0, Lclj;->h:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lclj;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lclj;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lclt;

    monitor-exit v1

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lclt;

    invoke-direct {v0, p0, p1}, Lclt;-><init>(Lclj;Lcvp;)V

    iget-object v2, p0, Lclj;->h:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lclc;->a(Landroid/os/IBinder;)Lclb;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcgk;J)Lcig;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcgk",
            "<",
            "Lcvb;",
            ">;J)",
            "Lcig;"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-super {p0}, Lchr;->h()V

    new-instance v2, Lclv;

    invoke-direct {v2, p0, p1}, Lclv;-><init>(Lclj;Lcgk;)V

    :try_start_0
    invoke-super {p0}, Lchr;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lclb;

    const/4 v3, 0x1

    invoke-interface {v0, v2, p2, p3, v3}, Lclb;->b(Lcky;JZ)Lcig;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v2, v0, v1, v1}, Lclv;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public a(Lcgk;Ljava/lang/String;Ljava/lang/String;II)Lcig;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcgk",
            "<",
            "Lcvb;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II)",
            "Lcig;"
        }
    .end annotation

    const/4 v6, 0x0

    new-instance v1, Lclv;

    invoke-direct {v1, p0, p1}, Lclv;-><init>(Lclj;Lcgk;)V

    :try_start_0
    invoke-super {p0}, Lchr;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lclb;

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lclb;->b(Lcky;Ljava/lang/String;Ljava/lang/String;II)Lcig;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v6, v6}, Lclv;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V

    move-object v0, v6

    goto :goto_0
.end method

.method protected a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 1

    if-nez p1, :cond_0

    if-eqz p3, :cond_0

    const-string v0, "post_init_configuration"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lclj;->a(Landroid/os/Bundle;)V

    :cond_0
    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-super {p0, p1, p2, v0}, Lchr;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    return-void

    :cond_1
    const-string v0, "post_init_resolution"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcgk;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcgk",
            "<",
            "Lcuv;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v8, 0x0

    invoke-super {p0}, Lchr;->h()V

    new-instance v1, Lcls;

    invoke-direct {v1, p0, p1}, Lcls;-><init>(Lclj;Lcgk;)V

    :try_start_0
    invoke-super {p0}, Lchr;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lclb;

    const/4 v4, 0x0

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p2

    move-object v3, p3

    invoke-interface/range {v0 .. v7}, Lclb;->a(Lcky;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v8, v8}, Lcls;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method

.method public a(Lcgk;Ljava/lang/String;Ljava/lang/String;Lcvm;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcgk",
            "<",
            "Lcuy;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcvm;",
            ")V"
        }
    .end annotation

    if-nez p4, :cond_0

    sget-object p4, Lcvm;->a:Lcvm;

    :cond_0
    invoke-virtual/range {p4 .. p4}, Lcvm;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p4 .. p4}, Lcvm;->b()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual/range {p4 .. p4}, Lcvm;->c()I

    move-result v6

    invoke-virtual/range {p4 .. p4}, Lcvm;->d()Z

    move-result v7

    invoke-virtual/range {p4 .. p4}, Lcvm;->e()J

    move-result-wide v8

    invoke-virtual/range {p4 .. p4}, Lcvm;->f()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p4 .. p4}, Lcvm;->g()I

    move-result v11

    invoke-virtual/range {p4 .. p4}, Lcvm;->h()I

    move-result v12

    invoke-super {p0}, Lchr;->h()V

    new-instance v1, Lclw;

    invoke-direct {v1, p0, p1}, Lclw;-><init>(Lclj;Lcgk;)V

    :try_start_0
    invoke-super {p0}, Lchr;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lclb;

    if-nez v2, :cond_1

    const/4 v5, 0x0

    :goto_0
    const/4 v13, 0x0

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-interface/range {v0 .. v13}, Lclb;->a(Lcky;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;III)V

    :goto_1
    return-void

    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lclw;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_1
.end method

.method public a(Lcgk;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcgk",
            "<",
            "Lcuu;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-super {p0}, Lchr;->h()V

    iget-object v0, p0, Lchr;->a:Landroid/content/Context;

    new-instance v1, Lcln;

    invoke-direct {v1, p0, p1}, Lcln;-><init>(Lclj;Lcgk;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    sget-object v4, Lclj;->i:Landroid/os/Bundle;

    sget-object v5, Lclj;->j:Landroid/os/Bundle;

    const/4 v7, 0x0

    move-object/from16 v6, p4

    invoke-static/range {v0 .. v7}, Lcmt;->a(Landroid/content/Context;Lcmx;ZILandroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Lcmt;

    move-result-object v13

    new-instance v1, Lclr;

    invoke-direct {v1, p0, v13}, Lclr;-><init>(Lclj;Lcmt;)V

    :try_start_0
    invoke-super {p0}, Lchr;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lclb;

    const/4 v5, 0x7

    const/4 v6, 0x1

    const v7, 0x1fffff

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x3

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    invoke-interface/range {v0 .. v12}, Lclb;->a(Lcky;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;ZII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {v13}, Lcmt;->b()V

    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lclr;->a(ILandroid/os/Bundle;[Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method

.method public a(Lcgk;ZI)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcgk",
            "<",
            "Lcux;",
            ">;ZI)V"
        }
    .end annotation

    const/4 v7, 0x0

    invoke-super {p0}, Lchr;->h()V

    new-instance v1, Lclu;

    invoke-direct {v1, p0, p1}, Lclu;-><init>(Lclj;Lcgk;)V

    :try_start_0
    invoke-super {p0}, Lchr;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lclb;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move v3, p2

    move v6, p3

    invoke-interface/range {v0 .. v6}, Lclb;->a(Lcky;ZZLjava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v7, v7}, Lclu;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method

.method public a(Lcht;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lchr",
            "<",
            "Lclb;",
            ">.cht<*>;)V"
        }
    .end annotation

    iget-object v1, p0, Lchr;->c:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lchr;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lchr;->b:Landroid/os/Handler;

    iget-object v1, p0, Lchr;->b:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected a(Lcim;Lchv;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "social_client_application_id"

    iget-object v2, p0, Lclj;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "real_client_package_name"

    iget-object v2, p0, Lclj;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x5cc600

    iget-object v2, p0, Lchr;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lcim;->b(Lcij;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Lcvp;)V
    .locals 7

    iget-object v6, p0, Lclj;->h:Ljava/util/HashMap;

    monitor-enter v6

    :try_start_0
    invoke-super {p0}, Lchr;->h()V

    iget-object v0, p0, Lclj;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lclj;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    return-void

    :cond_0
    :try_start_2
    iget-object v0, p0, Lclj;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lclt;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-super {p0}, Lchr;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lclb;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lclb;->a(Lcky;ZLjava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_1
    :try_start_4
    iget-object v0, p0, Lclj;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :catch_0
    move-exception v0

    :try_start_5
    const-string v1, "PeopleClient"

    const-string v2, "Failed to unregister listener"

    invoke-static {v1, v2, v0}, Lf;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    :try_start_6
    iget-object v1, p0, Lclj;->h:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method public a(Lcvp;Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 8

    const/4 v6, 0x1

    invoke-super {p0}, Lchr;->h()V

    iget-object v7, p0, Lclj;->h:Ljava/util/HashMap;

    monitor-enter v7

    :try_start_0
    invoke-direct {p0, p1}, Lclj;->b(Lcvp;)Lclt;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :try_start_1
    invoke-super {p0}, Lchr;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lclb;

    const/4 v2, 0x1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Lclb;->a(Lcky;ZLjava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v7

    move v0, v6

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "PeopleClient"

    const-string v2, "Failed to register listener"

    invoke-static {v1, v2, v0}, Lf;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 8

    const/4 v7, 0x0

    invoke-super {p0}, Lchr;->h()V

    :try_start_0
    invoke-super {p0}, Lchr;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lclb;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    invoke-interface/range {v0 .. v6}, Lclb;->a(Ljava/lang/String;Ljava/lang/String;JZZ)Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "PeopleClient"

    const-string v2, "Service call failed."

    invoke-static {v1, v2, v0}, Lf;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v7

    goto :goto_0
.end method

.method public b()V
    .locals 8

    iget-object v6, p0, Lclj;->h:Ljava/util/HashMap;

    monitor-enter v6

    :try_start_0
    invoke-virtual {p0}, Lclj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lclj;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lclt;

    invoke-super {p0}, Lchr;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lclb;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lclb;->a(Lcky;ZLjava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_0
    :goto_1
    :try_start_1
    iget-object v0, p0, Lclj;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-super {p0}, Lchr;->b()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.people.service.START"

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.people.internal.IPeopleService"

    return-object v0
.end method
