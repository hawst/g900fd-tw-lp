.class public abstract Lakq;
.super Lakl;
.source "PG"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "ListViewType:Landroid/widget/AbsListView;",
        ">",
        "Lakl;",
        "Landroid/widget/AbsListView$OnScrollListener;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field public h:Landroid/widget/AbsListView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "T",
            "ListViewType;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 13
    invoke-direct {p0}, Lakl;-><init>()V

    .line 23
    iput v0, p0, Lakq;->c:I

    .line 24
    iput v0, p0, Lakq;->d:I

    return-void
.end method


# virtual methods
.method protected ah()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 94
    iget-object v0, p0, Lakq;->h:Landroid/widget/AbsListView;

    if-nez v0, :cond_0

    .line 109
    :goto_0
    return-void

    .line 102
    :cond_0
    iget-object v0, p0, Lakq;->h:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v0

    iput v0, p0, Lakq;->a:I

    .line 103
    iget-object v0, p0, Lakq;->h:Landroid/widget/AbsListView;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 104
    if-eqz v0, :cond_1

    .line 105
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Lakq;->b:I

    goto :goto_0

    .line 107
    :cond_1
    iput v1, p0, Lakq;->b:I

    goto :goto_0
.end method

.method public ai()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 116
    iget-object v0, p0, Lakq;->h:Landroid/widget/AbsListView;

    if-nez v0, :cond_1

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    iget-object v0, p0, Lakq;->h:Landroid/widget/AbsListView;

    instance-of v0, v0, Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget v0, p0, Lakq;->b:I

    if-nez v0, :cond_2

    iget v0, p0, Lakq;->a:I

    if-eqz v0, :cond_0

    .line 121
    :cond_2
    iget-object v0, p0, Lakq;->h:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    iget v1, p0, Lakq;->a:I

    iget v2, p0, Lakq;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 124
    iput v3, p0, Lakq;->a:I

    .line 125
    iput v3, p0, Lakq;->b:I

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-super {p0, p1}, Lakl;->onCreate(Landroid/os/Bundle;)V

    .line 30
    if-eqz p1, :cond_0

    .line 31
    const-string v0, "scroll_pos"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lakq;->a:I

    .line 32
    const-string v0, "scroll_off"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lakq;->b:I

    .line 37
    :goto_0
    return-void

    .line 34
    :cond_0
    iput v0, p0, Lakq;->a:I

    .line 35
    iput v0, p0, Lakq;->b:I

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;
    .locals 2

    .prologue
    .line 43
    invoke-super {p0, p1, p2, p3, p4}, Lakl;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v1

    .line 46
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView;

    iput-object v0, p0, Lakq;->h:Landroid/widget/AbsListView;

    .line 47
    iget-object v0, p0, Lakq;->h:Landroid/widget/AbsListView;

    invoke-virtual {v0, p0}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 49
    return-object v1
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-super {p0}, Lakl;->onDestroyView()V

    .line 55
    iget-object v0, p0, Lakq;->h:Landroid/widget/AbsListView;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 56
    iput-object v1, p0, Lakq;->h:Landroid/widget/AbsListView;

    .line 57
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 61
    invoke-super {p0, p1}, Lakl;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 63
    invoke-virtual {p0}, Lakq;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lakq;->h:Landroid/widget/AbsListView;

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {p0}, Lakq;->ah()V

    .line 65
    const-string v0, "scroll_pos"

    iget v1, p0, Lakq;->a:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 66
    const-string v0, "scroll_off"

    iget v1, p0, Lakq;->b:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 68
    :cond_0
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 72
    if-lez p4, :cond_1

    .line 73
    add-int v0, p2, p3

    .line 74
    if-lt v0, p4, :cond_0

    iget v1, p0, Lakq;->c:I

    if-ne v0, v1, :cond_0

    iget v1, p0, Lakq;->d:I

    .line 76
    :cond_0
    iput v0, p0, Lakq;->c:I

    .line 79
    iput p4, p0, Lakq;->d:I

    .line 81
    :cond_1
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 85
    return-void
.end method
