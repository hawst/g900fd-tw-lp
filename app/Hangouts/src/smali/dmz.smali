.class public final Ldmz;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldmz;


# instance fields
.field public b:Ldna;

.field public c:Ldnh;

.field public d:Ljava/lang/Boolean;

.field public e:Ldnk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3844
    const/4 v0, 0x0

    new-array v0, v0, [Ldmz;

    sput-object v0, Ldmz;->a:[Ldmz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3845
    invoke-direct {p0}, Lepn;-><init>()V

    .line 3848
    iput-object v0, p0, Ldmz;->b:Ldna;

    .line 3851
    iput-object v0, p0, Ldmz;->c:Ldnh;

    .line 3856
    iput-object v0, p0, Ldmz;->e:Ldnk;

    .line 3845
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 3879
    const/4 v0, 0x0

    .line 3880
    iget-object v1, p0, Ldmz;->b:Ldna;

    if-eqz v1, :cond_0

    .line 3881
    const/4 v0, 0x1

    iget-object v1, p0, Ldmz;->b:Ldna;

    .line 3882
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3884
    :cond_0
    iget-object v1, p0, Ldmz;->c:Ldnh;

    if-eqz v1, :cond_1

    .line 3885
    const/4 v1, 0x2

    iget-object v2, p0, Ldmz;->c:Ldnh;

    .line 3886
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3888
    :cond_1
    iget-object v1, p0, Ldmz;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 3889
    const/4 v1, 0x3

    iget-object v2, p0, Ldmz;->d:Ljava/lang/Boolean;

    .line 3890
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3892
    :cond_2
    iget-object v1, p0, Ldmz;->e:Ldnk;

    if-eqz v1, :cond_3

    .line 3893
    const/4 v1, 0x5

    iget-object v2, p0, Ldmz;->e:Ldnk;

    .line 3894
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3896
    :cond_3
    iget-object v1, p0, Ldmz;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3897
    iput v0, p0, Ldmz;->cachedSize:I

    .line 3898
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 3841
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldmz;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldmz;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldmz;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldmz;->b:Ldna;

    if-nez v0, :cond_2

    new-instance v0, Ldna;

    invoke-direct {v0}, Ldna;-><init>()V

    iput-object v0, p0, Ldmz;->b:Ldna;

    :cond_2
    iget-object v0, p0, Ldmz;->b:Ldna;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldmz;->c:Ldnh;

    if-nez v0, :cond_3

    new-instance v0, Ldnh;

    invoke-direct {v0}, Ldnh;-><init>()V

    iput-object v0, p0, Ldmz;->c:Ldnh;

    :cond_3
    iget-object v0, p0, Ldmz;->c:Ldnh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldmz;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldmz;->e:Ldnk;

    if-nez v0, :cond_4

    new-instance v0, Ldnk;

    invoke-direct {v0}, Ldnk;-><init>()V

    iput-object v0, p0, Ldmz;->e:Ldnk;

    :cond_4
    iget-object v0, p0, Ldmz;->e:Ldnk;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 3861
    iget-object v0, p0, Ldmz;->b:Ldna;

    if-eqz v0, :cond_0

    .line 3862
    const/4 v0, 0x1

    iget-object v1, p0, Ldmz;->b:Ldna;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 3864
    :cond_0
    iget-object v0, p0, Ldmz;->c:Ldnh;

    if-eqz v0, :cond_1

    .line 3865
    const/4 v0, 0x2

    iget-object v1, p0, Ldmz;->c:Ldnh;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 3867
    :cond_1
    iget-object v0, p0, Ldmz;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 3868
    const/4 v0, 0x3

    iget-object v1, p0, Ldmz;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 3870
    :cond_2
    iget-object v0, p0, Ldmz;->e:Ldnk;

    if-eqz v0, :cond_3

    .line 3871
    const/4 v0, 0x5

    iget-object v1, p0, Ldmz;->e:Ldnk;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 3873
    :cond_3
    iget-object v0, p0, Ldmz;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 3875
    return-void
.end method
