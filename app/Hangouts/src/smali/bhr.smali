.class public Lbhr;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method private constructor <init>(Ldvl;)V
    .locals 3

    .prologue
    .line 1945
    iget-object v0, p1, Ldvl;->b:Ldvn;

    const-wide/16 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 1946
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 1950
    new-instance v0, Ldvl;

    invoke-direct {v0}, Ldvl;-><init>()V

    .line 1951
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldvl;

    .line 1952
    iget-object v1, v0, Ldvl;->b:Ldvn;

    invoke-static {v1}, Lbhr;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1953
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldvl;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 1955
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbhr;

    invoke-direct {v1, v0}, Lbhr;-><init>(Ldvl;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 3

    .prologue
    .line 1962
    invoke-super {p0, p1, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 1963
    iget-object v0, p0, Lbhr;->b:Lbea;

    check-cast v0, Lbey;

    iget-object v1, v0, Lbey;->c:Ljava/lang/String;

    .line 1964
    iget-object v0, p0, Lbhr;->b:Lbea;

    check-cast v0, Lbey;

    iget v0, v0, Lbey;->b:I

    .line 1965
    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 1966
    invoke-virtual {p1, v1}, Lyt;->W(Ljava/lang/String;)V

    .line 1968
    invoke-static {p2, p1, v1}, Lyp;->a(Lbnl;Lyt;Ljava/lang/String;)V

    .line 1969
    :cond_0
    invoke-static {p1}, Lyp;->d(Lyt;)V

    .line 1973
    return-void
.end method
