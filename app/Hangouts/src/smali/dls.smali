.class public final Ldls;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldls;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/Integer;

.field public j:[Ldlr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1572
    const/4 v0, 0x0

    new-array v0, v0, [Ldls;

    sput-object v0, Ldls;->a:[Ldls;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1573
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1592
    const/4 v0, 0x0

    iput-object v0, p0, Ldls;->b:Ljava/lang/Integer;

    .line 1609
    sget-object v0, Ldlr;->a:[Ldlr;

    iput-object v0, p0, Ldls;->j:[Ldlr;

    .line 1573
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1652
    iget-object v0, p0, Ldls;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 1653
    const/4 v0, 0x1

    iget-object v2, p0, Ldls;->b:Ljava/lang/Integer;

    .line 1654
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1656
    :goto_0
    iget-object v2, p0, Ldls;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 1657
    const/4 v2, 0x2

    iget-object v3, p0, Ldls;->c:Ljava/lang/String;

    .line 1658
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1660
    :cond_0
    iget-object v2, p0, Ldls;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 1661
    const/4 v2, 0x3

    iget-object v3, p0, Ldls;->d:Ljava/lang/String;

    .line 1662
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1664
    :cond_1
    iget-object v2, p0, Ldls;->e:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 1665
    const/4 v2, 0x4

    iget-object v3, p0, Ldls;->e:Ljava/lang/String;

    .line 1666
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1668
    :cond_2
    iget-object v2, p0, Ldls;->f:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 1669
    const/4 v2, 0x5

    iget-object v3, p0, Ldls;->f:Ljava/lang/String;

    .line 1670
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1672
    :cond_3
    iget-object v2, p0, Ldls;->g:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 1673
    const/4 v2, 0x6

    iget-object v3, p0, Ldls;->g:Ljava/lang/String;

    .line 1674
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1676
    :cond_4
    iget-object v2, p0, Ldls;->j:[Ldlr;

    if-eqz v2, :cond_6

    .line 1677
    iget-object v2, p0, Ldls;->j:[Ldlr;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 1678
    if-eqz v4, :cond_5

    .line 1679
    const/4 v5, 0x7

    .line 1680
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1677
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1684
    :cond_6
    iget-object v1, p0, Ldls;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 1685
    const/16 v1, 0x8

    iget-object v2, p0, Ldls;->h:Ljava/lang/String;

    .line 1686
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1688
    :cond_7
    iget-object v1, p0, Ldls;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 1689
    const/16 v1, 0x9

    iget-object v2, p0, Ldls;->i:Ljava/lang/Integer;

    .line 1690
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1692
    :cond_8
    iget-object v1, p0, Ldls;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1693
    iput v0, p0, Ldls;->cachedSize:I

    .line 1694
    return v0

    :cond_9
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1569
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldls;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldls;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldls;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/4 v2, 0x6

    if-eq v0, v2, :cond_2

    const/4 v2, 0x7

    if-eq v0, v2, :cond_2

    const/16 v2, 0x8

    if-eq v0, v2, :cond_2

    const/16 v2, 0x9

    if-eq v0, v2, :cond_2

    const/16 v2, 0xa

    if-eq v0, v2, :cond_2

    const/16 v2, 0xb

    if-eq v0, v2, :cond_2

    const/16 v2, 0xc

    if-eq v0, v2, :cond_2

    const/16 v2, 0xd

    if-ne v0, v2, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldls;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldls;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldls;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldls;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldls;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldls;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldls;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldls;->j:[Ldlr;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldlr;

    iget-object v3, p0, Ldls;->j:[Ldlr;

    if-eqz v3, :cond_4

    iget-object v3, p0, Ldls;->j:[Ldlr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Ldls;->j:[Ldlr;

    :goto_2
    iget-object v2, p0, Ldls;->j:[Ldlr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ldls;->j:[Ldlr;

    new-instance v3, Ldlr;

    invoke-direct {v3}, Ldlr;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldls;->j:[Ldlr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Ldls;->j:[Ldlr;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Ldls;->j:[Ldlr;

    new-instance v3, Ldlr;

    invoke-direct {v3}, Ldlr;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldls;->j:[Ldlr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldls;->h:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldls;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 1614
    iget-object v0, p0, Ldls;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1615
    const/4 v0, 0x1

    iget-object v1, p0, Ldls;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1617
    :cond_0
    iget-object v0, p0, Ldls;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1618
    const/4 v0, 0x2

    iget-object v1, p0, Ldls;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1620
    :cond_1
    iget-object v0, p0, Ldls;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1621
    const/4 v0, 0x3

    iget-object v1, p0, Ldls;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1623
    :cond_2
    iget-object v0, p0, Ldls;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1624
    const/4 v0, 0x4

    iget-object v1, p0, Ldls;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1626
    :cond_3
    iget-object v0, p0, Ldls;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1627
    const/4 v0, 0x5

    iget-object v1, p0, Ldls;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1629
    :cond_4
    iget-object v0, p0, Ldls;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1630
    const/4 v0, 0x6

    iget-object v1, p0, Ldls;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1632
    :cond_5
    iget-object v0, p0, Ldls;->j:[Ldlr;

    if-eqz v0, :cond_7

    .line 1633
    iget-object v1, p0, Ldls;->j:[Ldlr;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 1634
    if-eqz v3, :cond_6

    .line 1635
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 1633
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1639
    :cond_7
    iget-object v0, p0, Ldls;->h:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 1640
    const/16 v0, 0x8

    iget-object v1, p0, Ldls;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1642
    :cond_8
    iget-object v0, p0, Ldls;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 1643
    const/16 v0, 0x9

    iget-object v1, p0, Ldls;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1645
    :cond_9
    iget-object v0, p0, Ldls;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1647
    return-void
.end method
