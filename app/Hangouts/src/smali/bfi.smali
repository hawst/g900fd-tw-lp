.class public Lbfi;
.super Lbep;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:J


# direct methods
.method public constructor <init>(J)V
    .locals 0

    .prologue
    .line 4181
    invoke-direct {p0}, Lbep;-><init>()V

    .line 4182
    iput-wide p1, p0, Lbfi;->a:J

    .line 4183
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 9

    .prologue
    const-wide/16 v3, 0x0

    .line 4187
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 4188
    const-string v0, "Babel_RequestWriter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SetDndPresenceRequest build protobuf "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v5, p0, Lbfi;->a:J

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4191
    :cond_0
    new-instance v5, Ldwk;

    invoke-direct {v5}, Ldwk;-><init>()V

    .line 4192
    new-instance v6, Ldrf;

    invoke-direct {v6}, Ldrf;-><init>()V

    .line 4195
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v7, p0, Lbfi;->a:J

    .line 4196
    invoke-virtual {v1, v7, v8}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    sub-long/2addr v1, v7

    .line 4195
    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v1

    .line 4197
    cmp-long v0, v1, v3

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v6, Ldrf;->b:Ljava/lang/Boolean;

    .line 4198
    cmp-long v0, v1, v3

    if-lez v0, :cond_2

    move-wide v0, v1

    :goto_1
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v6, Ldrf;->c:Ljava/lang/Long;

    .line 4199
    iput-object v6, v5, Ldwk;->d:Ldrf;

    .line 4200
    invoke-virtual {p0, p1, p2}, Lbfi;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v0

    iput-object v0, v5, Ldwk;->b:Ldvm;

    .line 4201
    return-object v5

    .line 4197
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-wide v0, v3

    .line 4198
    goto :goto_1
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4206
    const-string v0, "presence/setpresence"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4211
    const-string v0, "event_queue"

    return-object v0
.end method
