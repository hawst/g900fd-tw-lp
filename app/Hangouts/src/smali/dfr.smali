.class public final Ldfr;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldfr;


# instance fields
.field public b:Ldfi;

.field public c:[Ldfq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3948
    const/4 v0, 0x0

    new-array v0, v0, [Ldfr;

    sput-object v0, Ldfr;->a:[Ldfr;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3949
    invoke-direct {p0}, Lepn;-><init>()V

    .line 3952
    const/4 v0, 0x0

    iput-object v0, p0, Ldfr;->b:Ldfi;

    .line 3955
    sget-object v0, Ldfq;->a:[Ldfq;

    iput-object v0, p0, Ldfr;->c:[Ldfq;

    .line 3949
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 3977
    iget-object v0, p0, Ldfr;->b:Ldfi;

    if-eqz v0, :cond_2

    .line 3978
    const/4 v0, 0x1

    iget-object v2, p0, Ldfr;->b:Ldfi;

    .line 3979
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3981
    :goto_0
    iget-object v2, p0, Ldfr;->c:[Ldfq;

    if-eqz v2, :cond_1

    .line 3982
    iget-object v2, p0, Ldfr;->c:[Ldfq;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 3983
    if-eqz v4, :cond_0

    .line 3984
    const/4 v5, 0x2

    .line 3985
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 3982
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3989
    :cond_1
    iget-object v1, p0, Ldfr;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3990
    iput v0, p0, Ldfr;->cachedSize:I

    .line 3991
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3945
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldfr;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldfr;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldfr;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldfr;->b:Ldfi;

    if-nez v0, :cond_2

    new-instance v0, Ldfi;

    invoke-direct {v0}, Ldfi;-><init>()V

    iput-object v0, p0, Ldfr;->b:Ldfi;

    :cond_2
    iget-object v0, p0, Ldfr;->b:Ldfi;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldfr;->c:[Ldfq;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldfq;

    iget-object v3, p0, Ldfr;->c:[Ldfq;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldfr;->c:[Ldfq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldfr;->c:[Ldfq;

    :goto_2
    iget-object v2, p0, Ldfr;->c:[Ldfq;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldfr;->c:[Ldfq;

    new-instance v3, Ldfq;

    invoke-direct {v3}, Ldfq;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldfr;->c:[Ldfq;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldfr;->c:[Ldfq;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldfr;->c:[Ldfq;

    new-instance v3, Ldfq;

    invoke-direct {v3}, Ldfq;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldfr;->c:[Ldfq;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 3960
    iget-object v0, p0, Ldfr;->b:Ldfi;

    if-eqz v0, :cond_0

    .line 3961
    const/4 v0, 0x1

    iget-object v1, p0, Ldfr;->b:Ldfi;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 3963
    :cond_0
    iget-object v0, p0, Ldfr;->c:[Ldfq;

    if-eqz v0, :cond_2

    .line 3964
    iget-object v1, p0, Ldfr;->c:[Ldfq;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 3965
    if-eqz v3, :cond_1

    .line 3966
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 3964
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3970
    :cond_2
    iget-object v0, p0, Ldfr;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 3972
    return-void
.end method
