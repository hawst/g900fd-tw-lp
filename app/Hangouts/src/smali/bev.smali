.class public Lbev;
.super Lbep;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public final c:J

.field public final h:Ljava/lang/String;

.field public final i:Z

.field public final j:Z

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;

.field public final m:Z

.field public final n:I

.field public final o:Z

.field public final p:Z

.field public final q:I

.field public final r:I

.field public final s:Z

.field public t:Ljava/lang/String;


# direct methods
.method private constructor <init>(IJLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZIZZZIIZ)V
    .locals 2

    .prologue
    .line 2415
    invoke-direct {p0}, Lbep;-><init>()V

    .line 2416
    iput p1, p0, Lbev;->a:I

    .line 2417
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbev;->b:Ljava/lang/String;

    .line 2418
    iput-wide p2, p0, Lbev;->c:J

    .line 2419
    iput-object p4, p0, Lbev;->h:Ljava/lang/String;

    .line 2420
    iput-boolean p5, p0, Lbev;->i:Z

    .line 2421
    iput-object p6, p0, Lbev;->k:Ljava/lang/String;

    .line 2422
    iput-object p7, p0, Lbev;->l:Ljava/lang/String;

    .line 2423
    iput-boolean p8, p0, Lbev;->m:Z

    .line 2424
    iput p9, p0, Lbev;->n:I

    .line 2425
    iput-boolean p10, p0, Lbev;->o:Z

    .line 2426
    iput-boolean p11, p0, Lbev;->p:Z

    .line 2427
    iput-boolean p12, p0, Lbev;->j:Z

    .line 2429
    iput p13, p0, Lbev;->q:I

    .line 2431
    move/from16 v0, p14

    iput v0, p0, Lbev;->r:I

    .line 2433
    move/from16 v0, p15

    iput-boolean v0, p0, Lbev;->s:Z

    .line 2434
    return-void
.end method

.method public static a(Ljava/lang/String;JLjava/lang/String;)Lbev;
    .locals 16

    .prologue
    .line 2450
    new-instance v0, Lbev;

    const/4 v1, 0x2

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-wide/from16 v2, p1

    move-object/from16 v4, p0

    move-object/from16 v6, p3

    invoke-direct/range {v0 .. v15}, Lbev;-><init>(IJLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZIZZZIIZ)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)Lbev;
    .locals 16

    .prologue
    .line 2458
    new-instance v0, Lbev;

    const/4 v1, 0x2

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-wide/from16 v2, p1

    move-object/from16 v4, p0

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    invoke-direct/range {v0 .. v15}, Lbev;-><init>(IJLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZIZZZIIZ)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;JZLjava/lang/String;ZIZZZIIZ)Lbev;
    .locals 16

    .prologue
    .line 2442
    new-instance v0, Lbev;

    const/4 v1, 0x1

    const/4 v7, 0x0

    move-wide/from16 v2, p1

    move-object/from16 v4, p0

    move/from16 v5, p3

    move-object/from16 v6, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move/from16 v10, p7

    move/from16 v11, p8

    move/from16 v12, p9

    move/from16 v13, p10

    move/from16 v14, p11

    move/from16 v15, p12

    invoke-direct/range {v0 .. v15}, Lbev;-><init>(IJLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZIZZZIIZ)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2475
    new-instance v1, Ldvd;

    invoke-direct {v1}, Ldvd;-><init>()V

    .line 2478
    invoke-virtual {p0, p1, p2}, Lbev;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v0

    iput-object v0, v1, Ldvd;->b:Ldvm;

    .line 2479
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldvd;->c:Ljava/lang/Integer;

    .line 2480
    iget-object v0, p0, Lbev;->k:Ljava/lang/String;

    iput-object v0, v1, Ldvd;->t:Ljava/lang/String;

    .line 2481
    iget v0, p0, Lbev;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldvd;->d:Ljava/lang/Integer;

    .line 2482
    iget-object v0, p0, Lbev;->h:Ljava/lang/String;

    iput-object v0, v1, Ldvd;->r:Ljava/lang/String;

    .line 2483
    iget-wide v2, p0, Lbev;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, Ldvd;->q:Ljava/lang/Long;

    .line 2484
    iget-object v0, p0, Lbev;->b:Ljava/lang/String;

    iput-object v0, v1, Ldvd;->e:Ljava/lang/String;

    .line 2485
    iget-boolean v0, p0, Lbev;->s:Z

    .line 2486
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Ldvd;->E:Ljava/lang/Boolean;

    .line 2487
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2488
    const-string v2, "com.google.chat.MESSAGING"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2489
    iget v2, p0, Lbev;->a:I

    if-ne v2, v4, :cond_0

    iget-boolean v2, p0, Lbev;->i:Z

    if-eqz v2, :cond_0

    .line 2490
    const-string v2, "com.google.hangout.RING"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2491
    const-string v2, "com.google.hangout.VOICEONLY"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2492
    iget-boolean v2, p0, Lbev;->j:Z

    if-eqz v2, :cond_0

    .line 2493
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lf;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2494
    const-string v2, "com.google.hangout.PSTN_RING"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2498
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v1, Ldvd;->s:[Ljava/lang/String;

    .line 2499
    iget-object v0, p0, Lbev;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2500
    const-string v0, "Babel_RequestWriter"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2501
    const-string v0, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unregistering removed account:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lbev;->l:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2504
    :cond_1
    iget-object v0, p0, Lbev;->l:Ljava/lang/String;

    iput-object v0, v1, Ldvd;->D:Ljava/lang/String;

    .line 2506
    :cond_2
    iget v0, p0, Lbev;->n:I

    if-lez v0, :cond_3

    .line 2507
    iget v0, p0, Lbev;->n:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldvd;->v:Ljava/lang/Integer;

    .line 2509
    :cond_3
    iget-boolean v0, p0, Lbev;->o:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lbev;->p:Z

    if-eqz v0, :cond_5

    .line 2510
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, v1, Ldvd;->z:[Ljava/lang/String;

    .line 2511
    iget-object v0, v1, Ldvd;->z:[Ljava/lang/String;

    const-string v2, "com.google.chat.DEVICE_SMS_ENABLED"

    aput-object v2, v0, v5

    .line 2512
    iget-object v0, v1, Ldvd;->z:[Ljava/lang/String;

    const-string v2, "com.google.chat.SMS_ACCOUNT"

    aput-object v2, v0, v4

    .line 2517
    :cond_4
    :goto_0
    iget v0, p0, Lbev;->q:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldvd;->A:Ljava/lang/Integer;

    .line 2518
    iget v0, p0, Lbev;->r:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldvd;->B:Ljava/lang/Integer;

    .line 2519
    return-object v1

    .line 2513
    :cond_5
    iget-boolean v0, p0, Lbev;->o:Z

    if-eqz v0, :cond_4

    .line 2514
    new-array v0, v4, [Ljava/lang/String;

    iput-object v0, v1, Ldvd;->z:[Ljava/lang/String;

    .line 2515
    iget-object v0, v1, Ldvd;->z:[Ljava/lang/String;

    const-string v2, "com.google.chat.DEVICE_SMS_ENABLED"

    aput-object v2, v0, v5

    goto :goto_0
.end method

.method public a(Lyj;Lbph;)V
    .locals 3

    .prologue
    .line 2582
    iget v0, p0, Lbev;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2583
    invoke-static {p1, p2}, Lbkb;->a(Lyj;Ljava/lang/Exception;)V

    .line 2589
    :goto_0
    return-void

    .line 2586
    :cond_0
    const-string v0, "Babel_RequestWriter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unregistering account failed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2587
    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2586
    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(JI)Z
    .locals 1

    .prologue
    .line 2524
    iget-boolean v0, p0, Lbev;->m:Z

    return v0
.end method

.method public a(Lbea;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2538
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v2, v3}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2539
    check-cast p1, Lbev;

    .line 2540
    iget-object v2, p0, Lbev;->l:Ljava/lang/String;

    iget-object v3, p1, Lbev;->l:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2572
    :cond_0
    :goto_0
    return v1

    .line 2544
    :cond_1
    iget v2, p0, Lbev;->a:I

    iget v3, p1, Lbev;->a:I

    if-eq v2, v3, :cond_3

    .line 2548
    sget-boolean v1, Lbea;->d:Z

    if-eqz v1, :cond_2

    .line 2549
    const-string v1, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Replacing a DeviceRegistrationRequest with different type:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p1, Lbev;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move v1, v0

    .line 2554
    goto :goto_0

    .line 2557
    :cond_3
    iget-boolean v2, p0, Lbev;->m:Z

    iget-boolean v3, p1, Lbev;->m:Z

    if-eq v2, v3, :cond_5

    .line 2561
    iget-boolean v2, p0, Lbev;->m:Z

    if-nez v2, :cond_4

    :goto_1
    move v1, v0

    .line 2567
    :goto_2
    if-eqz v1, :cond_0

    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 2568
    const-string v0, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Replacing a DeviceRegistrationRequest. Old withRetry="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p1, Lbev;->m:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". New withRetry="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lbev;->m:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 2561
    goto :goto_1

    :cond_5
    move v1, v0

    .line 2564
    goto :goto_2
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2577
    const-string v0, "devices/registerdevice"

    return-object v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 2529
    iget-boolean v0, p0, Lbev;->m:Z

    if-eqz v0, :cond_0

    .line 2530
    invoke-static {}, Lbkb;->b()J

    move-result-wide v0

    .line 2533
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 2470
    iget-boolean v0, p0, Lbev;->m:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()V
    .locals 2

    .prologue
    .line 2593
    invoke-super {p0}, Lbep;->s()V

    .line 2594
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 2596
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "babel_register_device_request_update_url"

    .line 2595
    invoke-static {v0, v1}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbev;->t:Ljava/lang/String;

    .line 2598
    return-void
.end method
