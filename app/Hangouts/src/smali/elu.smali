.class public final Lelu;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lelu;


# instance fields
.field public b:Lelt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1165
    const/4 v0, 0x0

    new-array v0, v0, [Lelu;

    sput-object v0, Lelu;->a:[Lelu;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1166
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1169
    const/4 v0, 0x0

    iput-object v0, p0, Lelu;->b:Lelt;

    .line 1166
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 2

    .prologue
    .line 1183
    const/4 v0, 0x0

    .line 1184
    iget-object v1, p0, Lelu;->b:Lelt;

    if-eqz v1, :cond_0

    .line 1185
    const/4 v0, 0x1

    iget-object v1, p0, Lelu;->b:Lelt;

    .line 1186
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1188
    :cond_0
    iget-object v1, p0, Lelu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1189
    iput v0, p0, Lelu;->cachedSize:I

    .line 1190
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1162
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lelu;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lelu;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lelu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lelu;->b:Lelt;

    if-nez v0, :cond_2

    new-instance v0, Lelt;

    invoke-direct {v0}, Lelt;-><init>()V

    iput-object v0, p0, Lelu;->b:Lelt;

    :cond_2
    iget-object v0, p0, Lelu;->b:Lelt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1174
    iget-object v0, p0, Lelu;->b:Lelt;

    if-eqz v0, :cond_0

    .line 1175
    const/4 v0, 0x1

    iget-object v1, p0, Lelu;->b:Lelt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1177
    :cond_0
    iget-object v0, p0, Lelu;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1179
    return-void
.end method
