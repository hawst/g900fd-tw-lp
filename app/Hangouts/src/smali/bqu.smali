.class public abstract Lbqu;
.super Lbqr;
.source "PG"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:I

.field public final e:J

.field public final f:Ljava/lang/String;

.field public final g:Lbdk;

.field public final h:J

.field public i:J

.field public final j:[Ldxc;

.field public k:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Lbdk;JJLjava/lang/String;Ljava/lang/String;IIJI[B)V
    .locals 4

    .prologue
    .line 109
    invoke-direct {p0}, Lbqr;-><init>()V

    .line 111
    iput-object p1, p0, Lbqu;->f:Ljava/lang/String;

    .line 112
    iput-object p2, p0, Lbqu;->g:Lbdk;

    .line 113
    iput-wide p3, p0, Lbqu;->h:J

    .line 114
    iput-wide p5, p0, Lbqu;->i:J

    .line 115
    iput-object p7, p0, Lbqu;->a:Ljava/lang/String;

    .line 116
    iput-object p8, p0, Lbqu;->b:Ljava/lang/String;

    .line 117
    iput p9, p0, Lbqu;->c:I

    .line 118
    iput p10, p0, Lbqu;->d:I

    .line 119
    iput-wide p11, p0, Lbqu;->e:J

    .line 120
    move/from16 v0, p13

    iput v0, p0, Lbqu;->k:I

    .line 122
    const/4 v2, 0x0

    .line 124
    if-eqz p14, :cond_0

    .line 125
    :try_start_0
    new-instance v1, Ldxe;

    invoke-direct {v1}, Ldxe;-><init>()V

    move-object/from16 v0, p14

    invoke-static {v1, v0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v1

    check-cast v1, Ldxe;

    iget-object v1, v1, Ldxe;->b:[Ldxc;
    :try_end_0
    .catch Lepq; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move-object v2, v1

    .line 133
    :goto_1
    iput-object v2, p0, Lbqu;->j:[Ldxc;

    .line 134
    return-void

    .line 129
    :catch_0
    move-exception v1

    const-string v1, "Babel"

    const-string v3, "Invalid protobuf set in Event and failed to process in EventProcessor."

    invoke-static {v1, v3}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    move-object v1, v2

    goto :goto_0
.end method

.method public static final a([Ldxc;Ljava/lang/String;Ljava/lang/String;JLyt;)V
    .locals 8

    .prologue
    .line 147
    const-string v0, "babel_disable_nlp_stickers"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 150
    if-nez v0, :cond_0

    if-nez p0, :cond_1

    .line 227
    :cond_0
    return-void

    .line 154
    :cond_1
    array-length v2, p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p0, v1

    .line 155
    new-instance v4, Lyz;

    invoke-direct {v4}, Lyz;-><init>()V

    .line 156
    iput-object p1, v4, Lyz;->a:Ljava/lang/String;

    .line 157
    iput-object p2, v4, Lyz;->b:Ljava/lang/String;

    .line 158
    iget-object v0, v3, Ldxc;->b:Ljava/lang/String;

    iput-object v0, v4, Lyz;->c:Ljava/lang/String;

    .line 159
    iput-wide p3, v4, Lyz;->d:J

    .line 160
    iget-object v0, v3, Ldxc;->c:Ljava/lang/Long;

    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v5

    iput-wide v5, v4, Lyz;->e:J

    .line 162
    iget-object v0, v3, Ldxc;->d:Ljava/lang/Integer;

    const/4 v5, 0x0

    invoke-static {v0, v5}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    iput v0, v4, Lyz;->f:I

    .line 166
    iget-object v0, v3, Ldxc;->d:Ljava/lang/Integer;

    const/4 v5, 0x0

    invoke-static {v0, v5}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    const/16 v5, 0x11

    if-ne v0, v5, :cond_3

    .line 167
    const-string v0, "armeabi-v7a"

    sget-object v5, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "armeabi-v7a"

    sget-object v5, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_2
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_6

    .line 168
    :cond_3
    iget-object v0, v3, Ldxc;->d:Ljava/lang/Integer;

    const/4 v5, 0x0

    invoke-static {v0, v5}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    const/16 v5, 0x11

    if-ne v0, v5, :cond_5

    iget-object v0, v3, Ldxc;->f:Ldrn;

    if-eqz v0, :cond_5

    .line 176
    iget-object v5, v3, Ldxc;->f:Ldrn;

    .line 179
    iget-object v0, v5, Ldrn;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    iget-object v0, v5, Ldrn;->e:Ljava/lang/Integer;

    .line 180
    const/4 v6, 0x0

    invoke-static {v0, v6}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    :goto_2
    iput v0, v4, Lyz;->h:I

    .line 183
    iget-object v0, v5, Ldrn;->d:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, v5, Ldrn;->d:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 186
    iget-object v0, v3, Ldxc;->f:Ldrn;

    iget-object v0, v0, Ldrn;->d:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    .line 189
    const-string v3, "\\."

    invoke-static {v0, v3}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 190
    array-length v5, v3

    if-nez v5, :cond_4

    .line 191
    const-string v5, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Malformed unsuffixed URL has been passed as a GEM suggestion asset. unsuffixedUrl: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    :cond_4
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 197
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 198
    sparse-switch v0, :sswitch_data_0

    .line 212
    const-string v0, ""

    .line 215
    :goto_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    array-length v6, v3

    add-int/lit8 v6, v6, -0x2

    aget-object v6, v3, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 217
    array-length v5, v3

    add-int/lit8 v5, v5, -0x2

    aput-object v0, v3, v5

    .line 219
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "https:"

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "."

    invoke-static {v5, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lyz;->g:Ljava/lang/String;

    .line 225
    :cond_5
    invoke-virtual {p5, v4}, Lyt;->a(Lyz;)J

    .line 154
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 167
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 180
    :cond_8
    const/4 v0, 0x1

    goto :goto_2

    .line 200
    :sswitch_0
    const-string v0, "_mdpi"

    goto :goto_3

    .line 203
    :sswitch_1
    const-string v0, "_hdpi"

    goto :goto_3

    .line 206
    :sswitch_2
    const-string v0, "_xhdpi"

    goto :goto_3

    .line 209
    :sswitch_3
    const-string v0, "_xxhdpi"

    goto :goto_3

    .line 198
    :sswitch_data_0
    .sparse-switch
        0xa0 -> :sswitch_0
        0xf0 -> :sswitch_1
        0x140 -> :sswitch_2
        0x1e0 -> :sswitch_3
    .end sparse-switch
.end method


# virtual methods
.method protected final a(Lyt;)V
    .locals 6

    .prologue
    .line 142
    iget-object v0, p0, Lbqu;->j:[Ldxc;

    iget-object v1, p0, Lbqu;->f:Ljava/lang/String;

    iget-object v2, p0, Lbqu;->a:Ljava/lang/String;

    iget-wide v3, p0, Lbqu;->h:J

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lbqu;->a([Ldxc;Ljava/lang/String;Ljava/lang/String;JLyt;)V

    .line 143
    return-void
.end method
