.class public final Ldhw;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldhw;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Integer;

.field public e:Ldht;

.field public f:Ljava/lang/String;

.field public g:Ldig;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/Long;

.field public j:Ljava/lang/Long;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ldhu;

.field public n:Ljava/lang/String;

.field public o:Ldic;

.field public p:[Ldif;

.field public q:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Ldhw;

    sput-object v0, Ldhw;->a:[Ldhw;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 25
    iput-object v0, p0, Ldhw;->d:Ljava/lang/Integer;

    .line 28
    iput-object v0, p0, Ldhw;->e:Ldht;

    .line 33
    iput-object v0, p0, Ldhw;->g:Ldig;

    .line 46
    iput-object v0, p0, Ldhw;->m:Ldhu;

    .line 51
    iput-object v0, p0, Ldhw;->o:Ldic;

    .line 54
    sget-object v0, Ldif;->a:[Ldif;

    iput-object v0, p0, Ldhw;->p:[Ldif;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 120
    iget-object v0, p0, Ldhw;->c:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 121
    const/4 v0, 0x1

    iget-object v2, p0, Ldhw;->c:Ljava/lang/String;

    .line 122
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 124
    :goto_0
    iget-object v2, p0, Ldhw;->h:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 125
    const/4 v2, 0x2

    iget-object v3, p0, Ldhw;->h:Ljava/lang/String;

    .line 126
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 128
    :cond_0
    iget-object v2, p0, Ldhw;->i:Ljava/lang/Long;

    if-eqz v2, :cond_1

    .line 129
    const/4 v2, 0x3

    iget-object v3, p0, Ldhw;->i:Ljava/lang/Long;

    .line 130
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 132
    :cond_1
    iget-object v2, p0, Ldhw;->k:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 133
    const/4 v2, 0x4

    iget-object v3, p0, Ldhw;->k:Ljava/lang/String;

    .line 134
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 136
    :cond_2
    iget-object v2, p0, Ldhw;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 137
    const/4 v2, 0x5

    iget-object v3, p0, Ldhw;->d:Ljava/lang/Integer;

    .line 138
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 140
    :cond_3
    iget-object v2, p0, Ldhw;->m:Ldhu;

    if-eqz v2, :cond_4

    .line 141
    const/4 v2, 0x6

    iget-object v3, p0, Ldhw;->m:Ldhu;

    .line 142
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 144
    :cond_4
    iget-object v2, p0, Ldhw;->n:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 145
    const/4 v2, 0x7

    iget-object v3, p0, Ldhw;->n:Ljava/lang/String;

    .line 146
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 148
    :cond_5
    iget-object v2, p0, Ldhw;->e:Ldht;

    if-eqz v2, :cond_6

    .line 149
    const/16 v2, 0x8

    iget-object v3, p0, Ldhw;->e:Ldht;

    .line 150
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 152
    :cond_6
    iget-object v2, p0, Ldhw;->o:Ldic;

    if-eqz v2, :cond_7

    .line 153
    const/16 v2, 0x9

    iget-object v3, p0, Ldhw;->o:Ldic;

    .line 154
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 156
    :cond_7
    iget-object v2, p0, Ldhw;->p:[Ldif;

    if-eqz v2, :cond_9

    .line 157
    iget-object v2, p0, Ldhw;->p:[Ldif;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_9

    aget-object v4, v2, v1

    .line 158
    if-eqz v4, :cond_8

    .line 159
    const/16 v5, 0xa

    .line 160
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 157
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 164
    :cond_9
    iget-object v1, p0, Ldhw;->j:Ljava/lang/Long;

    if-eqz v1, :cond_a

    .line 165
    const/16 v1, 0xb

    iget-object v2, p0, Ldhw;->j:Ljava/lang/Long;

    .line 166
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 168
    :cond_a
    iget-object v1, p0, Ldhw;->b:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 169
    const/16 v1, 0xc

    iget-object v2, p0, Ldhw;->b:Ljava/lang/String;

    .line 170
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    :cond_b
    iget-object v1, p0, Ldhw;->f:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 173
    const/16 v1, 0xd

    iget-object v2, p0, Ldhw;->f:Ljava/lang/String;

    .line 174
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 176
    :cond_c
    iget-object v1, p0, Ldhw;->l:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 177
    const/16 v1, 0xe

    iget-object v2, p0, Ldhw;->l:Ljava/lang/String;

    .line 178
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 180
    :cond_d
    iget-object v1, p0, Ldhw;->q:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 181
    const/16 v1, 0xf

    iget-object v2, p0, Ldhw;->q:Ljava/lang/String;

    .line 182
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 184
    :cond_e
    iget-object v1, p0, Ldhw;->g:Ldig;

    if-eqz v1, :cond_f

    .line 185
    const/16 v1, 0x10

    iget-object v2, p0, Ldhw;->g:Ldig;

    .line 186
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 188
    :cond_f
    iget-object v1, p0, Ldhw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 189
    iput v0, p0, Ldhw;->cachedSize:I

    .line 190
    return v0

    :cond_10
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldhw;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldhw;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldhw;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldhw;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldhw;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldhw;->i:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldhw;->k:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-ne v0, v2, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldhw;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldhw;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ldhw;->m:Ldhu;

    if-nez v0, :cond_4

    new-instance v0, Ldhu;

    invoke-direct {v0}, Ldhu;-><init>()V

    iput-object v0, p0, Ldhw;->m:Ldhu;

    :cond_4
    iget-object v0, p0, Ldhw;->m:Ldhu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldhw;->n:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Ldhw;->e:Ldht;

    if-nez v0, :cond_5

    new-instance v0, Ldht;

    invoke-direct {v0}, Ldht;-><init>()V

    iput-object v0, p0, Ldhw;->e:Ldht;

    :cond_5
    iget-object v0, p0, Ldhw;->e:Ldht;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Ldhw;->o:Ldic;

    if-nez v0, :cond_6

    new-instance v0, Ldic;

    invoke-direct {v0}, Ldic;-><init>()V

    iput-object v0, p0, Ldhw;->o:Ldic;

    :cond_6
    iget-object v0, p0, Ldhw;->o:Ldic;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_a
    const/16 v0, 0x52

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldhw;->p:[Ldif;

    if-nez v0, :cond_8

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldif;

    iget-object v3, p0, Ldhw;->p:[Ldif;

    if-eqz v3, :cond_7

    iget-object v3, p0, Ldhw;->p:[Ldif;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Ldhw;->p:[Ldif;

    :goto_2
    iget-object v2, p0, Ldhw;->p:[Ldif;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Ldhw;->p:[Ldif;

    new-instance v3, Ldif;

    invoke-direct {v3}, Ldif;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldhw;->p:[Ldif;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_8
    iget-object v0, p0, Ldhw;->p:[Ldif;

    array-length v0, v0

    goto :goto_1

    :cond_9
    iget-object v2, p0, Ldhw;->p:[Ldif;

    new-instance v3, Ldif;

    invoke-direct {v3}, Ldif;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldhw;->p:[Ldif;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldhw;->j:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldhw;->b:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldhw;->f:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldhw;->l:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldhw;->q:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_10
    iget-object v0, p0, Ldhw;->g:Ldig;

    if-nez v0, :cond_a

    new-instance v0, Ldig;

    invoke-direct {v0}, Ldig;-><init>()V

    iput-object v0, p0, Ldhw;->g:Ldig;

    :cond_a
    iget-object v0, p0, Ldhw;->g:Ldig;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 61
    iget-object v0, p0, Ldhw;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 62
    const/4 v0, 0x1

    iget-object v1, p0, Ldhw;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 64
    :cond_0
    iget-object v0, p0, Ldhw;->h:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 65
    const/4 v0, 0x2

    iget-object v1, p0, Ldhw;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 67
    :cond_1
    iget-object v0, p0, Ldhw;->i:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 68
    const/4 v0, 0x3

    iget-object v1, p0, Ldhw;->i:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 70
    :cond_2
    iget-object v0, p0, Ldhw;->k:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 71
    const/4 v0, 0x4

    iget-object v1, p0, Ldhw;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 73
    :cond_3
    iget-object v0, p0, Ldhw;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 74
    const/4 v0, 0x5

    iget-object v1, p0, Ldhw;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 76
    :cond_4
    iget-object v0, p0, Ldhw;->m:Ldhu;

    if-eqz v0, :cond_5

    .line 77
    const/4 v0, 0x6

    iget-object v1, p0, Ldhw;->m:Ldhu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 79
    :cond_5
    iget-object v0, p0, Ldhw;->n:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 80
    const/4 v0, 0x7

    iget-object v1, p0, Ldhw;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 82
    :cond_6
    iget-object v0, p0, Ldhw;->e:Ldht;

    if-eqz v0, :cond_7

    .line 83
    const/16 v0, 0x8

    iget-object v1, p0, Ldhw;->e:Ldht;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 85
    :cond_7
    iget-object v0, p0, Ldhw;->o:Ldic;

    if-eqz v0, :cond_8

    .line 86
    const/16 v0, 0x9

    iget-object v1, p0, Ldhw;->o:Ldic;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 88
    :cond_8
    iget-object v0, p0, Ldhw;->p:[Ldif;

    if-eqz v0, :cond_a

    .line 89
    iget-object v1, p0, Ldhw;->p:[Ldif;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_a

    aget-object v3, v1, v0

    .line 90
    if-eqz v3, :cond_9

    .line 91
    const/16 v4, 0xa

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 89
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 95
    :cond_a
    iget-object v0, p0, Ldhw;->j:Ljava/lang/Long;

    if-eqz v0, :cond_b

    .line 96
    const/16 v0, 0xb

    iget-object v1, p0, Ldhw;->j:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 98
    :cond_b
    iget-object v0, p0, Ldhw;->b:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 99
    const/16 v0, 0xc

    iget-object v1, p0, Ldhw;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 101
    :cond_c
    iget-object v0, p0, Ldhw;->f:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 102
    const/16 v0, 0xd

    iget-object v1, p0, Ldhw;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 104
    :cond_d
    iget-object v0, p0, Ldhw;->l:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 105
    const/16 v0, 0xe

    iget-object v1, p0, Ldhw;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 107
    :cond_e
    iget-object v0, p0, Ldhw;->q:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 108
    const/16 v0, 0xf

    iget-object v1, p0, Ldhw;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 110
    :cond_f
    iget-object v0, p0, Ldhw;->g:Ldig;

    if-eqz v0, :cond_10

    .line 111
    const/16 v0, 0x10

    iget-object v1, p0, Ldhw;->g:Ldig;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 113
    :cond_10
    iget-object v0, p0, Ldhw;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 115
    return-void
.end method
