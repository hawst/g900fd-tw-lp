.class public final Leib;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leib;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:[Leil;

.field public d:Ljava/lang/Integer;

.field public e:Leie;

.field public f:Leid;

.field public g:Leii;

.field public h:Lfab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Leib;

    sput-object v0, Leib;->a:[Leib;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 24
    iput-object v1, p0, Leib;->b:Ljava/lang/Integer;

    .line 27
    sget-object v0, Leil;->a:[Leil;

    iput-object v0, p0, Leib;->c:[Leil;

    .line 32
    iput-object v1, p0, Leib;->e:Leie;

    .line 35
    iput-object v1, p0, Leib;->f:Leid;

    .line 38
    iput-object v1, p0, Leib;->g:Leii;

    .line 41
    iput-object v1, p0, Leib;->h:Lfab;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 78
    iget-object v0, p0, Leib;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 79
    const/4 v0, 0x1

    iget-object v2, p0, Leib;->b:Ljava/lang/Integer;

    .line 80
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 82
    :goto_0
    iget-object v2, p0, Leib;->c:[Leil;

    if-eqz v2, :cond_1

    .line 83
    iget-object v2, p0, Leib;->c:[Leil;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 84
    if-eqz v4, :cond_0

    .line 85
    const/4 v5, 0x3

    .line 86
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 83
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 90
    :cond_1
    iget-object v1, p0, Leib;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 91
    const/4 v1, 0x4

    iget-object v2, p0, Leib;->d:Ljava/lang/Integer;

    .line 92
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 94
    :cond_2
    iget-object v1, p0, Leib;->e:Leie;

    if-eqz v1, :cond_3

    .line 95
    const/4 v1, 0x5

    iget-object v2, p0, Leib;->e:Leie;

    .line 96
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    :cond_3
    iget-object v1, p0, Leib;->f:Leid;

    if-eqz v1, :cond_4

    .line 99
    const/4 v1, 0x6

    iget-object v2, p0, Leib;->f:Leid;

    .line 100
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    :cond_4
    iget-object v1, p0, Leib;->g:Leii;

    if-eqz v1, :cond_5

    .line 103
    const/16 v1, 0xe

    iget-object v2, p0, Leib;->g:Leii;

    .line 104
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 106
    :cond_5
    iget-object v1, p0, Leib;->h:Lfab;

    if-eqz v1, :cond_6

    .line 107
    const/16 v1, 0xf

    iget-object v2, p0, Leib;->h:Lfab;

    .line 108
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 110
    :cond_6
    iget-object v1, p0, Leib;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 111
    iput v0, p0, Leib;->cachedSize:I

    .line 112
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 6

    .prologue
    const/16 v5, 0x1a

    const/16 v4, 0x11

    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Leib;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Leib;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Leib;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_2

    const/16 v2, 0x12

    if-eq v0, v2, :cond_2

    const/16 v2, 0x15

    if-eq v0, v2, :cond_2

    const/16 v2, 0x16

    if-eq v0, v2, :cond_2

    const/16 v2, 0x17

    if-eq v0, v2, :cond_2

    const/16 v2, 0x18

    if-eq v0, v2, :cond_2

    const/16 v2, 0x19

    if-eq v0, v2, :cond_2

    if-ne v0, v5, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leib;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leib;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-static {p1, v5}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leib;->c:[Leil;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leil;

    iget-object v3, p0, Leib;->c:[Leil;

    if-eqz v3, :cond_4

    iget-object v3, p0, Leib;->c:[Leil;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Leib;->c:[Leil;

    :goto_2
    iget-object v2, p0, Leib;->c:[Leil;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Leib;->c:[Leil;

    new-instance v3, Leil;

    invoke-direct {v3}, Leil;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leib;->c:[Leil;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Leib;->c:[Leil;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Leib;->c:[Leil;

    new-instance v3, Leil;

    invoke-direct {v3}, Leil;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leib;->c:[Leil;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leib;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Leib;->e:Leie;

    if-nez v0, :cond_7

    new-instance v0, Leie;

    invoke-direct {v0}, Leie;-><init>()V

    iput-object v0, p0, Leib;->e:Leie;

    :cond_7
    iget-object v0, p0, Leib;->e:Leie;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Leib;->f:Leid;

    if-nez v0, :cond_8

    new-instance v0, Leid;

    invoke-direct {v0}, Leid;-><init>()V

    iput-object v0, p0, Leib;->f:Leid;

    :cond_8
    iget-object v0, p0, Leib;->f:Leid;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Leib;->g:Leii;

    if-nez v0, :cond_9

    new-instance v0, Leii;

    invoke-direct {v0}, Leii;-><init>()V

    iput-object v0, p0, Leib;->g:Leii;

    :cond_9
    iget-object v0, p0, Leib;->g:Leii;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Leib;->h:Lfab;

    if-nez v0, :cond_a

    new-instance v0, Lfab;

    invoke-direct {v0}, Lfab;-><init>()V

    iput-object v0, p0, Leib;->h:Lfab;

    :cond_a
    iget-object v0, p0, Leib;->h:Lfab;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x1a -> :sswitch_2
        0x20 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x72 -> :sswitch_6
        0x7a -> :sswitch_7
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 46
    iget-object v0, p0, Leib;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 47
    const/4 v0, 0x1

    iget-object v1, p0, Leib;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 49
    :cond_0
    iget-object v0, p0, Leib;->c:[Leil;

    if-eqz v0, :cond_2

    .line 50
    iget-object v1, p0, Leib;->c:[Leil;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 51
    if-eqz v3, :cond_1

    .line 52
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 50
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 56
    :cond_2
    iget-object v0, p0, Leib;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 57
    const/4 v0, 0x4

    iget-object v1, p0, Leib;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 59
    :cond_3
    iget-object v0, p0, Leib;->e:Leie;

    if-eqz v0, :cond_4

    .line 60
    const/4 v0, 0x5

    iget-object v1, p0, Leib;->e:Leie;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 62
    :cond_4
    iget-object v0, p0, Leib;->f:Leid;

    if-eqz v0, :cond_5

    .line 63
    const/4 v0, 0x6

    iget-object v1, p0, Leib;->f:Leid;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 65
    :cond_5
    iget-object v0, p0, Leib;->g:Leii;

    if-eqz v0, :cond_6

    .line 66
    const/16 v0, 0xe

    iget-object v1, p0, Leib;->g:Leii;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 68
    :cond_6
    iget-object v0, p0, Leib;->h:Lfab;

    if-eqz v0, :cond_7

    .line 69
    const/16 v0, 0xf

    iget-object v1, p0, Leib;->h:Lfab;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 71
    :cond_7
    iget-object v0, p0, Leib;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 73
    return-void
.end method
