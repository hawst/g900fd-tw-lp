.class public Lbyq;
.super Lbyu;
.source "PG"


# instance fields
.field private a:I

.field private e:I

.field private f:Z

.field private g:Z

.field private h:I

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lyj;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lbyu;-><init>(Ljava/lang/String;Lyj;)V

    .line 34
    return-void
.end method

.method public static a(IIZZZ)Ljava/lang/StringBuilder;
    .locals 3

    .prologue
    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 181
    if-eqz p4, :cond_0

    .line 182
    const-string v1, "d"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    :goto_0
    return-object v0

    .line 184
    :cond_0
    if-ne p1, p0, :cond_3

    .line 185
    const-string v1, "s"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 186
    if-eqz p2, :cond_1

    .line 187
    const-string v1, "-c"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    :cond_1
    :goto_1
    if-eqz p3, :cond_2

    .line 197
    const-string v1, "-k"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    :cond_2
    const-string v1, "-no"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 190
    :cond_3
    const-string v1, "w"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-h"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 191
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 192
    if-eqz p2, :cond_1

    .line 193
    const-string v1, "-n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method


# virtual methods
.method public a(I)Lbyq;
    .locals 0

    .prologue
    .line 44
    iput p1, p0, Lbyq;->e:I

    .line 45
    iput p1, p0, Lbyq;->a:I

    .line 46
    return-object p0
.end method

.method public a(II)Lbyq;
    .locals 0

    .prologue
    .line 38
    iput p1, p0, Lbyq;->e:I

    .line 39
    iput p2, p0, Lbyq;->a:I

    .line 40
    return-object p0
.end method

.method public a(Z)Lbyq;
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lbyq;->f:Z

    .line 51
    return-object p0
.end method

.method public a()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 210
    iget v1, p0, Lbyq;->e:I

    iget v2, p0, Lbyq;->a:I

    iget-boolean v3, p0, Lbyq;->f:Z

    iget-boolean v4, p0, Lbyq;->i:Z

    iget-boolean v5, p0, Lbyq;->d:Z

    invoke-static {v1, v2, v3, v4, v5}, Lbyq;->a(IIZZZ)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 213
    invoke-virtual {p0}, Lbyq;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object v1, p0

    .line 218
    :goto_0
    iput-object v0, v1, Lbyq;->b:Ljava/lang/String;

    .line 221
    iget-boolean v0, p0, Lbyq;->k:Z

    if-eqz v0, :cond_0

    .line 222
    const-string v0, "-sms"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    :cond_0
    iget-boolean v0, p0, Lbyq;->j:Z

    if-eqz v0, :cond_1

    .line 225
    const-string v0, "-force"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    :cond_1
    iget-boolean v0, p0, Lbyq;->l:Z

    if-eqz v0, :cond_2

    .line 228
    const-string v0, "-rounded"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lbyq;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbyq;->c:Ljava/lang/String;

    .line 231
    return-void

    .line 215
    :cond_3
    invoke-virtual {p0}, Lbyq;->l()Ljava/lang/String;

    move-result-object v1

    const-string v3, "content://"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 216
    invoke-virtual {p0}, Lbyq;->l()Ljava/lang/String;

    move-result-object v0

    move-object v1, p0

    goto :goto_0

    .line 218
    :cond_4
    invoke-virtual {p0}, Lbyq;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    move-object v1, p0

    goto :goto_0

    :cond_5
    invoke-static {v1}, Lbrz;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    move-object v0, v1

    move-object v1, p0

    goto :goto_0

    :cond_6
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    const-string v4, "content"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v0, 0x1

    :cond_7
    :goto_1
    if-eqz v0, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v1, p0

    goto/16 :goto_0

    :cond_8
    if-nez v3, :cond_7

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "https:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_9
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lbam;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public b()Lbyq;
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbyq;->d:Z

    .line 76
    return-object p0
.end method

.method public b(I)Lbyq;
    .locals 0

    .prologue
    .line 65
    iput p1, p0, Lbyq;->h:I

    .line 66
    return-object p0
.end method

.method public b(Z)Lbyq;
    .locals 0

    .prologue
    .line 55
    iput-boolean p1, p0, Lbyq;->l:Z

    .line 56
    return-object p0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lbyq;->e:I

    return v0
.end method

.method public c(Z)Lbyq;
    .locals 0

    .prologue
    .line 60
    iput-boolean p1, p0, Lbyq;->g:Z

    .line 61
    return-object p0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lbyq;->a:I

    return v0
.end method

.method public d(Z)Lbyq;
    .locals 0

    .prologue
    .line 70
    iput-boolean p1, p0, Lbyq;->i:Z

    .line 71
    return-object p0
.end method

.method public e(Z)Lbyq;
    .locals 0

    .prologue
    .line 80
    iput-boolean p1, p0, Lbyq;->j:Z

    .line 81
    return-object p0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lbyq;->f:Z

    return v0
.end method

.method public f(Z)Lbyq;
    .locals 0

    .prologue
    .line 85
    iput-boolean p1, p0, Lbyq;->k:Z

    .line 86
    return-object p0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lbyq;->g:Z

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lbyq;->h:I

    return v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lbyq;->l:Z

    return v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Lbyq;->i:Z

    return v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lbyq;->j:Z

    return v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Lbyq;->k:Z

    return v0
.end method
