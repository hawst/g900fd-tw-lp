.class public final Ldkp;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldkp;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:Ldkl;

.field public f:Ldkn;

.field public g:Ldks;

.field public h:Ldkm;

.field public i:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    new-array v0, v0, [Ldkp;

    sput-object v0, Ldkp;->a:[Ldkp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16
    invoke-direct {p0}, Lepn;-><init>()V

    .line 40
    iput-object v0, p0, Ldkp;->c:Ljava/lang/Integer;

    .line 43
    iput-object v0, p0, Ldkp;->d:Ljava/lang/Integer;

    .line 46
    iput-object v0, p0, Ldkp;->e:Ldkl;

    .line 49
    iput-object v0, p0, Ldkp;->f:Ldkn;

    .line 52
    iput-object v0, p0, Ldkp;->g:Ldks;

    .line 55
    iput-object v0, p0, Ldkp;->h:Ldkm;

    .line 58
    iput-object v0, p0, Ldkp;->i:Ljava/lang/Integer;

    .line 16
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 93
    const/4 v0, 0x0

    .line 94
    iget-object v1, p0, Ldkp;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 95
    const/4 v0, 0x1

    iget-object v1, p0, Ldkp;->b:Ljava/lang/String;

    .line 96
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 98
    :cond_0
    iget-object v1, p0, Ldkp;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 99
    const/4 v1, 0x2

    iget-object v2, p0, Ldkp;->c:Ljava/lang/Integer;

    .line 100
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    :cond_1
    iget-object v1, p0, Ldkp;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 103
    const/4 v1, 0x3

    iget-object v2, p0, Ldkp;->d:Ljava/lang/Integer;

    .line 104
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 106
    :cond_2
    iget-object v1, p0, Ldkp;->e:Ldkl;

    if-eqz v1, :cond_3

    .line 107
    const/4 v1, 0x4

    iget-object v2, p0, Ldkp;->e:Ldkl;

    .line 108
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 110
    :cond_3
    iget-object v1, p0, Ldkp;->f:Ldkn;

    if-eqz v1, :cond_4

    .line 111
    const/4 v1, 0x5

    iget-object v2, p0, Ldkp;->f:Ldkn;

    .line 112
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 114
    :cond_4
    iget-object v1, p0, Ldkp;->g:Ldks;

    if-eqz v1, :cond_5

    .line 115
    const/4 v1, 0x6

    iget-object v2, p0, Ldkp;->g:Ldks;

    .line 116
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 118
    :cond_5
    iget-object v1, p0, Ldkp;->h:Ldkm;

    if-eqz v1, :cond_6

    .line 119
    const/4 v1, 0x7

    iget-object v2, p0, Ldkp;->h:Ldkm;

    .line 120
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 122
    :cond_6
    iget-object v1, p0, Ldkp;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 123
    const/16 v1, 0x8

    iget-object v2, p0, Ldkp;->i:Ljava/lang/Integer;

    .line 124
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 126
    :cond_7
    iget-object v1, p0, Ldkp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 127
    iput v0, p0, Ldkp;->cachedSize:I

    .line 128
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 12
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldkp;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldkp;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldkp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldkp;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_2

    if-eq v0, v3, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldkp;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldkp;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_4

    if-ne v0, v3, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldkp;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldkp;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldkp;->e:Ldkl;

    if-nez v0, :cond_6

    new-instance v0, Ldkl;

    invoke-direct {v0}, Ldkl;-><init>()V

    iput-object v0, p0, Ldkp;->e:Ldkl;

    :cond_6
    iget-object v0, p0, Ldkp;->e:Ldkl;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldkp;->f:Ldkn;

    if-nez v0, :cond_7

    new-instance v0, Ldkn;

    invoke-direct {v0}, Ldkn;-><init>()V

    iput-object v0, p0, Ldkp;->f:Ldkn;

    :cond_7
    iget-object v0, p0, Ldkp;->f:Ldkn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Ldkp;->g:Ldks;

    if-nez v0, :cond_8

    new-instance v0, Ldks;

    invoke-direct {v0}, Ldks;-><init>()V

    iput-object v0, p0, Ldkp;->g:Ldks;

    :cond_8
    iget-object v0, p0, Ldkp;->g:Ldks;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Ldkp;->h:Ldkm;

    if-nez v0, :cond_9

    new-instance v0, Ldkm;

    invoke-direct {v0}, Ldkm;-><init>()V

    iput-object v0, p0, Ldkp;->h:Ldkm;

    :cond_9
    iget-object v0, p0, Ldkp;->h:Ldkm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_a

    if-eq v0, v2, :cond_a

    if-ne v0, v3, :cond_b

    :cond_a
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldkp;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_b
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldkp;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Ldkp;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 64
    const/4 v0, 0x1

    iget-object v1, p0, Ldkp;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 66
    :cond_0
    iget-object v0, p0, Ldkp;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 67
    const/4 v0, 0x2

    iget-object v1, p0, Ldkp;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 69
    :cond_1
    iget-object v0, p0, Ldkp;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 70
    const/4 v0, 0x3

    iget-object v1, p0, Ldkp;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 72
    :cond_2
    iget-object v0, p0, Ldkp;->e:Ldkl;

    if-eqz v0, :cond_3

    .line 73
    const/4 v0, 0x4

    iget-object v1, p0, Ldkp;->e:Ldkl;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 75
    :cond_3
    iget-object v0, p0, Ldkp;->f:Ldkn;

    if-eqz v0, :cond_4

    .line 76
    const/4 v0, 0x5

    iget-object v1, p0, Ldkp;->f:Ldkn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 78
    :cond_4
    iget-object v0, p0, Ldkp;->g:Ldks;

    if-eqz v0, :cond_5

    .line 79
    const/4 v0, 0x6

    iget-object v1, p0, Ldkp;->g:Ldks;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 81
    :cond_5
    iget-object v0, p0, Ldkp;->h:Ldkm;

    if-eqz v0, :cond_6

    .line 82
    const/4 v0, 0x7

    iget-object v1, p0, Ldkp;->h:Ldkm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 84
    :cond_6
    iget-object v0, p0, Ldkp;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 85
    const/16 v0, 0x8

    iget-object v1, p0, Ldkp;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 87
    :cond_7
    iget-object v0, p0, Ldkp;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 89
    return-void
.end method
