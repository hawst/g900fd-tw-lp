.class public final Ldzm;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldzm;


# instance fields
.field public b:Ldvm;

.field public c:Ldzk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 796
    const/4 v0, 0x0

    new-array v0, v0, [Ldzm;

    sput-object v0, Ldzm;->a:[Ldzm;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 797
    invoke-direct {p0}, Lepn;-><init>()V

    .line 800
    iput-object v0, p0, Ldzm;->b:Ldvm;

    .line 803
    iput-object v0, p0, Ldzm;->c:Ldzk;

    .line 797
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 820
    const/4 v0, 0x0

    .line 821
    iget-object v1, p0, Ldzm;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 822
    const/4 v0, 0x1

    iget-object v1, p0, Ldzm;->b:Ldvm;

    .line 823
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 825
    :cond_0
    iget-object v1, p0, Ldzm;->c:Ldzk;

    if-eqz v1, :cond_1

    .line 826
    const/4 v1, 0x2

    iget-object v2, p0, Ldzm;->c:Ldzk;

    .line 827
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 829
    :cond_1
    iget-object v1, p0, Ldzm;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 830
    iput v0, p0, Ldzm;->cachedSize:I

    .line 831
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 793
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldzm;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldzm;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldzm;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldzm;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldzm;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldzm;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldzm;->c:Ldzk;

    if-nez v0, :cond_3

    new-instance v0, Ldzk;

    invoke-direct {v0}, Ldzk;-><init>()V

    iput-object v0, p0, Ldzm;->c:Ldzk;

    :cond_3
    iget-object v0, p0, Ldzm;->c:Ldzk;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 808
    iget-object v0, p0, Ldzm;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 809
    const/4 v0, 0x1

    iget-object v1, p0, Ldzm;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 811
    :cond_0
    iget-object v0, p0, Ldzm;->c:Ldzk;

    if-eqz v0, :cond_1

    .line 812
    const/4 v0, 0x2

    iget-object v1, p0, Ldzm;->c:Ldzk;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 814
    :cond_1
    iget-object v0, p0, Ldzm;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 816
    return-void
.end method
