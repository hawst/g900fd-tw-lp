.class final Lbuw;
.super Lack;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lack",
        "<",
        "Lbfn;",
        "Lbii;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic d:Lbus;

.field private final e:I

.field private final f:Z


# direct methods
.method protected constructor <init>(Lbus;IZ)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lbuw;->d:Lbus;

    invoke-direct {p0}, Lack;-><init>()V

    .line 68
    iput p2, p0, Lbuw;->e:I

    .line 69
    iput-boolean p3, p0, Lbuw;->f:Z

    .line 70
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lbuw;->d:Lbus;

    iget-object v0, v0, Lbus;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->kL:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lbos;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 84
    invoke-super {p0, p1}, Lack;->a(Lbos;)V

    .line 87
    invoke-virtual {p1}, Lbos;->c()Lbfz;

    move-result-object v0

    check-cast v0, Lbii;

    .line 89
    invoke-virtual {v0}, Lbii;->f()Ljava/lang/Boolean;

    move-result-object v2

    .line 90
    if-eqz v2, :cond_0

    .line 91
    iget-object v1, p0, Lbuw;->d:Lbus;

    iget-object v1, v1, Lbus;->a:Lbtz;

    iget-object v3, p0, Lbuw;->d:Lbus;

    .line 92
    iget-object v3, v3, Lbus;->e:Ljava/lang/String;

    .line 91
    invoke-interface {v1, v3}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    .line 93
    invoke-static {v2, v4}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 96
    :cond_0
    invoke-virtual {v0}, Lbii;->g()Ljava/lang/Boolean;

    move-result-object v1

    .line 97
    if-eqz v1, :cond_1

    .line 98
    iget-object v0, p0, Lbuw;->d:Lbus;

    iget-object v0, v0, Lbus;->a:Lbtz;

    iget-object v2, p0, Lbuw;->d:Lbus;

    .line 99
    iget-object v2, v2, Lbus;->f:Ljava/lang/String;

    .line 98
    invoke-interface {v0, v2}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 100
    invoke-static {v1, v4}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 102
    :cond_1
    return-void
.end method

.method protected a(Ljava/lang/Exception;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 110
    invoke-super {p0, p1}, Lack;->a(Ljava/lang/Exception;)V

    .line 111
    iget v0, p0, Lbuw;->e:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lbuw;->d:Lbus;

    iget-object v0, v0, Lbus;->a:Lbtz;

    iget-object v3, p0, Lbuw;->d:Lbus;

    iget-object v3, v3, Lbus;->e:Ljava/lang/String;

    invoke-interface {v0, v3}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iget-boolean v3, p0, Lbuw;->f:Z

    if-nez v3, :cond_1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 112
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v1, v2

    .line 111
    goto :goto_0

    :cond_2
    iget v0, p0, Lbuw;->e:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lbuw;->d:Lbus;

    iget-object v0, v0, Lbus;->a:Lbtz;

    iget-object v3, p0, Lbuw;->d:Lbus;

    iget-object v3, v3, Lbus;->f:Ljava/lang/String;

    invoke-interface {v0, v3}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iget-boolean v3, p0, Lbuw;->f:Z

    if-nez v3, :cond_3

    :goto_2
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method public b()I
    .locals 3

    .prologue
    .line 74
    iget-object v0, p0, Lbuw;->d:Lbus;

    iget-object v0, v0, Lbus;->b:Lyj;

    iget v1, p0, Lbuw;->e:I

    iget-boolean v2, p0, Lbuw;->f:Z

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lyj;IZ)I

    move-result v0

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 123
    invoke-static {}, Lbuw;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    iget-object v0, p0, Lbuw;->d:Lbus;

    iget-object v0, v0, Lbus;->c:Landroid/app/Activity;

    sget v1, Lh;->kN:I

    invoke-static {v0, v1}, Lf;->a(Landroid/content/Context;I)V

    .line 128
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v0, p0, Lbuw;->d:Lbus;

    iget-object v0, v0, Lbus;->c:Landroid/app/Activity;

    sget v1, Lh;->kM:I

    invoke-static {v0, v1}, Lf;->a(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public e()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lbfn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    const-class v0, Lbfn;

    return-object v0
.end method

.method public f()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lbii;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    const-class v0, Lbii;

    return-object v0
.end method

.method public h()V
    .locals 0

    .prologue
    .line 116
    invoke-super {p0}, Lack;->h()V

    .line 119
    return-void
.end method
