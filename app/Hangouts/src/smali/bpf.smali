.class public final Lbpf;
.super Lbmu;
.source "PG"


# static fields
.field private static final a:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lbpf;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/Random;


# instance fields
.field private e:J

.field private f:I

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:J

.field private k:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lbpf;->a:Ljava/util/concurrent/ConcurrentHashMap;

    .line 48
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lbpf;->d:Ljava/util/Random;

    return-void
.end method

.method private constructor <init>(Lyj;JJ)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v0, 0x0

    .line 129
    invoke-direct/range {p0 .. p5}, Lbmu;-><init>(Lyj;JJ)V

    .line 52
    iput-wide v4, p0, Lbpf;->e:J

    .line 60
    iput v0, p0, Lbpf;->f:I

    .line 63
    iput-boolean v0, p0, Lbpf;->g:Z

    .line 66
    iput-boolean v0, p0, Lbpf;->h:Z

    .line 69
    iput-boolean v0, p0, Lbpf;->i:Z

    .line 73
    iput-wide v4, p0, Lbpf;->j:J

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lbpf;->k:Ljava/lang/String;

    .line 130
    invoke-virtual {p1}, Lyj;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lbpf;->j:J

    .line 152
    :goto_0
    return-void

    .line 134
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 135
    invoke-static {p1}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v2

    .line 136
    const-string v3, "warm_sync_deferral_limit"

    invoke-virtual {v2, v3, v4, v5}, Lbsx;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 138
    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 141
    const-string v2, "babel_maxsynctickledelay"

    const v3, 0xea60

    invoke-static {v2, v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lbpf;->j:J

    goto :goto_0

    .line 147
    :cond_1
    const-string v2, "babel_synctickledelay"

    const/16 v3, 0x2710

    invoke-static {v2, v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lbpf;->j:J

    goto :goto_0
.end method

.method public static a(Lyj;)Lbpf;
    .locals 7

    .prologue
    .line 89
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v6

    .line 90
    sget-object v0, Lbpf;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpf;

    .line 91
    if-nez v0, :cond_0

    .line 92
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "babel_warm_sync_lowmark_seconds"

    const/16 v3, 0x1c20

    invoke-static {v1, v2, v3}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v2, v1

    .line 97
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "babel_warm_sync_highmark_seconds"

    const v4, 0x93a80

    invoke-static {v0, v1, v4}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v4, v0

    .line 102
    new-instance v0, Lbpf;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lbpf;-><init>(Lyj;JJ)V

    .line 109
    sget-object v1, Lbpf;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v6, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lbpf;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpf;

    .line 112
    :cond_0
    return-object v0
.end method

.method private a(Ljava/lang/String;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 376
    monitor-enter p0

    .line 377
    :try_start_0
    iget-object v1, p0, Lbpf;->k:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbpf;->k:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 378
    invoke-virtual {p0}, Lbpf;->g()Z

    move-result v0

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 379
    invoke-virtual {p0, p2}, Lbpf;->a(I)V

    .line 380
    const/4 v0, 0x0

    iput-object v0, p0, Lbpf;->k:Ljava/lang/String;

    .line 381
    const/4 v0, 0x0

    iput v0, p0, Lbpf;->f:I

    .line 382
    const/4 v0, 0x1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385
    :goto_0
    return v0

    .line 384
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static k()V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 155
    sget-object v0, Lbpf;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpf;

    .line 156
    iget-object v1, v0, Lbpf;->b:Lyj;

    invoke-static {v1}, Lbkb;->o(Lyj;)Lbki;

    move-result-object v5

    .line 157
    if-nez v5, :cond_1

    .line 158
    const-string v1, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Account is not valid. Skip parasite operation:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lbpf;->b:Lyj;

    .line 159
    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 158
    invoke-static {v1, v0}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 162
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 163
    iget-wide v8, v0, Lbpf;->j:J

    cmp-long v1, v6, v8

    if-gez v1, :cond_3

    move v1, v2

    .line 164
    :goto_1
    if-nez v1, :cond_0

    invoke-virtual {v0}, Lbpf;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 165
    invoke-static {}, Lbya;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 166
    new-instance v1, Lbyc;

    invoke-direct {v1}, Lbyc;-><init>()V

    const-string v8, "rtcs_warm_sync_queued"

    .line 167
    invoke-virtual {v1, v8}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    move-result-object v1

    iget-object v8, v0, Lbpf;->b:Lyj;

    .line 168
    invoke-virtual {v1, v8}, Lbyc;->a(Lyj;)Lbyc;

    move-result-object v1

    .line 169
    invoke-virtual {v1}, Lbyc;->b()V

    .line 171
    :cond_2
    const-string v1, "babel_maxsynctickledelay"

    const v8, 0xea60

    invoke-static {v1, v8}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    int-to-long v8, v1

    .line 175
    add-long/2addr v6, v8

    iput-wide v6, v0, Lbpf;->j:J

    .line 177
    invoke-virtual {v0, v2}, Lbpf;->a(Z)V

    .line 178
    invoke-virtual {v0, v3}, Lbpf;->b(Z)V

    .line 179
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lbpf;->a(I)V

    .line 180
    invoke-virtual {v0}, Lbpf;->a()V

    .line 181
    invoke-virtual {v0, v5, v3}, Lbpf;->a(Lbki;I)Z

    goto :goto_0

    :cond_3
    move v1, v3

    .line 163
    goto :goto_1

    .line 184
    :cond_4
    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 277
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long v1, v0, v2

    .line 278
    iget-boolean v0, p0, Lbpf;->i:Z

    if-eqz v0, :cond_0

    const-string v0, "from_background_polling:"

    .line 279
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v3, Lbpf;->d:Ljava/util/Random;

    const/16 v4, 0x3e8

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    int-to-long v3, v3

    add-long/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 280
    monitor-enter p0

    .line 282
    :try_start_0
    iput-object v0, p0, Lbpf;->k:Ljava/lang/String;

    .line 284
    iget-object v1, p0, Lbpf;->c:Lbnl;

    invoke-virtual {v1}, Lbnl;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 285
    const/4 v1, 0x0

    iput v1, p0, Lbpf;->f:I

    .line 286
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 289
    new-instance v1, Lyt;

    iget-object v2, p0, Lbpf;->b:Lyj;

    invoke-direct {v1, v2}, Lyt;-><init>(Lyj;)V

    iget-object v2, p0, Lbpf;->c:Lbnl;

    iget-boolean v3, p0, Lbpf;->g:Z

    iget-boolean v4, p0, Lbpf;->h:Z

    invoke-static {v1, v2, v3, v4, v0}, Lyp;->a(Lyt;Lbnl;ZZLjava/lang/String;)V

    .line 291
    return-void

    .line 278
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 286
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(J)V
    .locals 4

    .prologue
    .line 319
    iput-wide p1, p0, Lbpf;->e:J

    .line 320
    new-instance v1, Lyt;

    iget-object v0, p0, Lbpf;->b:Lyj;

    invoke-direct {v1, v0}, Lyt;-><init>(Lyj;)V

    .line 321
    invoke-virtual {v1}, Lyt;->a()V

    .line 323
    :try_start_0
    const-string v0, "last_warm_sync_localtime"

    iget-wide v2, p0, Lbpf;->e:J

    invoke-virtual {v1, v0, v2, v3}, Lyt;->h(Ljava/lang/String;J)V

    .line 325
    invoke-virtual {v1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 327
    invoke-virtual {v1}, Lyt;->c()V

    .line 328
    return-void

    .line 327
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lyt;->c()V

    throw v0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 465
    iput-boolean p1, p0, Lbpf;->g:Z

    .line 466
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 358
    const-string v1, "Babel"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Increment pending requests for "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". Current number of pending request "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lbpf;->f:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lbpf;->k:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "[IDLE]"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    monitor-enter p0

    .line 362
    :try_start_0
    iget-object v0, p0, Lbpf;->k:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbpf;->k:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 363
    iget v0, p0, Lbpf;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbpf;->f:I

    .line 364
    const/4 v0, 0x1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 367
    :goto_1
    return v0

    .line 358
    :cond_0
    iget-object v0, p0, Lbpf;->k:Ljava/lang/String;

    goto :goto_0

    .line 366
    :cond_1
    monitor-exit p0

    .line 367
    const/4 v0, 0x0

    goto :goto_1

    .line 366
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(I)J
    .locals 16

    .prologue
    .line 218
    const-wide v1, 0x7fffffffffffffffL

    .line 219
    invoke-virtual/range {p0 .. p0}, Lbpf;->l()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 220
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    .line 221
    const-string v1, "babel_maxsynctickledelay"

    const v2, 0xea60

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v1

    int-to-long v11, v1

    .line 226
    if-gtz p1, :cond_4

    .line 228
    const-wide/16 v1, 0x0

    move-wide v5, v1

    move-wide v1, v3

    .line 241
    :goto_0
    add-long v9, v3, v5

    .line 242
    move-object/from16 v0, p0

    iget-object v7, v0, Lbpf;->b:Lyj;

    invoke-static {v7}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v13

    .line 243
    const-string v7, "warm_sync_deferral_limit"

    const-wide/16 v14, -0x1

    invoke-virtual {v13, v7, v14, v15}, Lbsx;->a(Ljava/lang/String;J)J

    move-result-wide v7

    .line 246
    const-wide/16 v14, 0x0

    cmp-long v14, v7, v14

    if-ltz v14, :cond_0

    cmp-long v14, v7, v1

    if-lez v14, :cond_1

    .line 247
    :cond_0
    const-string v14, "warm_sync_deferral_limit"

    invoke-virtual {v13, v14, v1, v2}, Lbsx;->b(Ljava/lang/String;J)V

    .line 250
    :cond_1
    const-wide/16 v1, 0x0

    cmp-long v1, v7, v1

    if-ltz v1, :cond_6

    cmp-long v1, v9, v7

    if-lez v1, :cond_6

    .line 253
    const-wide/16 v1, 0x0

    sub-long v3, v7, v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v5

    move-wide v1, v7

    .line 255
    :goto_1
    invoke-static {}, Lbya;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 256
    new-instance v3, Lbyc;

    invoke-direct {v3}, Lbyc;-><init>()V

    const-string v4, "rtcs_set_sane_alarm"

    .line 257
    invoke-virtual {v3, v4}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lbpf;->b:Lyj;

    .line 258
    invoke-virtual {v3, v4}, Lbyc;->a(Lyj;)Lbyc;

    move-result-object v3

    .line 259
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    add-long/2addr v7, v5

    invoke-virtual {v3, v7, v8}, Lbyc;->b(J)Lbyc;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "delay="

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 260
    invoke-virtual {v3, v4}, Lbyc;->b(Ljava/lang/String;)Lbyc;

    move-result-object v3

    .line 261
    invoke-virtual {v3}, Lbyc;->b()V

    .line 264
    :cond_2
    move-object/from16 v0, p0

    iget-wide v3, v0, Lbpf;->j:J

    add-long v5, v1, v11

    cmp-long v3, v3, v5

    if-gez v3, :cond_3

    .line 265
    add-long v3, v1, v11

    move-object/from16 v0, p0

    iput-wide v3, v0, Lbpf;->j:J

    .line 268
    :cond_3
    return-wide v1

    .line 230
    :cond_4
    const/4 v1, 0x1

    move/from16 v0, p1

    if-ne v0, v1, :cond_5

    .line 232
    const-string v1, "babel_synctickledelay"

    const/16 v2, 0x2710

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v1

    int-to-long v5, v1

    .line 235
    add-long v1, v3, v11

    goto/16 :goto_0

    .line 238
    :cond_5
    const/4 v1, 0x1

    shl-long v5, v11, v1

    .line 239
    add-long v1, v3, v5

    goto/16 :goto_0

    :cond_6
    move-wide v1, v9

    goto :goto_1
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 469
    iput-boolean p1, p0, Lbpf;->h:Z

    .line 470
    return-void
.end method

.method public b(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 395
    const-string v3, "Babel"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "Decrement pending requests for "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ". Current number of pending request "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v4, p0, Lbpf;->f:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " for "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, p0, Lbpf;->k:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, "[IDLE]"

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    monitor-enter p0

    .line 400
    :try_start_0
    iget-object v0, p0, Lbpf;->k:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbpf;->k:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 401
    iget v0, p0, Lbpf;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbpf;->f:I

    .line 402
    iget v0, p0, Lbpf;->f:I

    if-ltz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 403
    iget v0, p0, Lbpf;->f:I

    if-nez v0, :cond_3

    .line 404
    invoke-virtual {p0}, Lbpf;->g()Z

    move-result v0

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 405
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lbpf;->a(I)V

    .line 406
    const/4 v0, 0x0

    iput-object v0, p0, Lbpf;->k:Ljava/lang/String;

    .line 407
    const/4 v0, 0x0

    iput v0, p0, Lbpf;->f:I

    move v0, v1

    .line 411
    :goto_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412
    if-eqz v0, :cond_0

    .line 413
    sget-boolean v1, Lyp;->a:Z

    if-eqz v1, :cond_0

    .line 414
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RequestWarmSyncOperation completed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    :cond_0
    return v0

    .line 395
    :cond_1
    iget-object v0, p0, Lbpf;->k:Ljava/lang/String;

    goto :goto_0

    :cond_2
    move v0, v2

    .line 402
    goto :goto_1

    .line 411
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move v0, v2

    goto :goto_2
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 473
    iput-boolean p1, p0, Lbpf;->i:Z

    .line 474
    return-void
.end method

.method public c(Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 426
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Lbpf;->a(Ljava/lang/String;I)Z

    move-result v1

    .line 427
    if-nez v1, :cond_0

    .line 429
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "RequestWarmSync: Mismatched key upon SANE completion "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_2

    const-string v0, "null"

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " != "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lbpf;->k:Ljava/lang/String;

    if-nez v0, :cond_3

    const-string v0, "null"

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lbpf;->a(J)V

    .line 434
    :cond_0
    sget-boolean v0, Lyp;->a:Z

    if-eqz v0, :cond_1

    .line 435
    if-eqz v1, :cond_4

    .line 436
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RequestWarmSyncOperation complete: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    :cond_1
    :goto_2
    return v1

    :cond_2
    move-object v0, p1

    .line 429
    goto :goto_0

    :cond_3
    iget-object v0, p0, Lbpf;->k:Ljava/lang/String;

    goto :goto_1

    .line 438
    :cond_4
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RequestWarmSyncOperation complete after restart "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public d(Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 450
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lbpf;->a(Ljava/lang/String;I)Z

    move-result v0

    .line 451
    sget-boolean v1, Lyp;->a:Z

    if-eqz v1, :cond_0

    .line 452
    if-eqz v0, :cond_1

    .line 453
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RequestWarmSyncOperation failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    :cond_0
    :goto_0
    return v0

    .line 455
    :cond_1
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RequestWarmSyncOperation failed after restart "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected i()J
    .locals 4

    .prologue
    .line 299
    iget-wide v0, p0, Lbpf;->e:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 300
    new-instance v1, Lyt;

    iget-object v0, p0, Lbpf;->b:Lyj;

    invoke-direct {v1, v0}, Lyt;-><init>(Lyj;)V

    .line 301
    invoke-virtual {v1}, Lyt;->a()V

    .line 303
    :try_start_0
    const-string v0, "last_warm_sync_localtime"

    invoke-virtual {v1, v0}, Lyt;->O(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lbpf;->e:J

    .line 305
    invoke-virtual {v1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 307
    invoke-virtual {v1}, Lyt;->c()V

    .line 311
    :cond_0
    iget-wide v0, p0, Lbpf;->e:J

    return-wide v0

    .line 307
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lyt;->c()V

    throw v0
.end method

.method public l()Z
    .locals 4

    .prologue
    .line 187
    iget-wide v0, p0, Lbpf;->j:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()Z
    .locals 6

    .prologue
    .line 195
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 196
    iget-object v2, p0, Lbpf;->b:Lyj;

    invoke-static {v2}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v2

    .line 197
    const-string v3, "warm_sync_deferral_limit"

    const-wide/16 v4, -0x1

    invoke-virtual {v2, v3, v4, v5}, Lbsx;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 199
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-ltz v4, :cond_0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lbpf;->b:Lyj;

    invoke-static {v0}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v0

    .line 207
    const-string v1, "warm_sync_deferral_limit"

    invoke-virtual {v0, v1}, Lbsx;->e(Ljava/lang/String;)V

    .line 208
    return-void
.end method
