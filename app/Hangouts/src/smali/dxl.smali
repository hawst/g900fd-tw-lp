.class public final Ldxl;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldxl;


# instance fields
.field public b:Ldqf;

.field public c:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9925
    const/4 v0, 0x0

    new-array v0, v0, [Ldxl;

    sput-object v0, Ldxl;->a:[Ldxl;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9926
    invoke-direct {p0}, Lepn;-><init>()V

    .line 9929
    const/4 v0, 0x0

    iput-object v0, p0, Ldxl;->b:Ldqf;

    .line 9926
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 9948
    const/4 v0, 0x0

    .line 9949
    iget-object v1, p0, Ldxl;->b:Ldqf;

    if-eqz v1, :cond_0

    .line 9950
    const/4 v0, 0x1

    iget-object v1, p0, Ldxl;->b:Ldqf;

    .line 9951
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9953
    :cond_0
    iget-object v1, p0, Ldxl;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 9954
    const/4 v1, 0x2

    iget-object v2, p0, Ldxl;->c:Ljava/lang/Long;

    .line 9955
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9957
    :cond_1
    iget-object v1, p0, Ldxl;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9958
    iput v0, p0, Ldxl;->cachedSize:I

    .line 9959
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 9922
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldxl;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldxl;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldxl;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldxl;->b:Ldqf;

    if-nez v0, :cond_2

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldxl;->b:Ldqf;

    :cond_2
    iget-object v0, p0, Ldxl;->b:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldxl;->c:Ljava/lang/Long;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 9936
    iget-object v0, p0, Ldxl;->b:Ldqf;

    if-eqz v0, :cond_0

    .line 9937
    const/4 v0, 0x1

    iget-object v1, p0, Ldxl;->b:Ldqf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 9939
    :cond_0
    iget-object v0, p0, Ldxl;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 9940
    const/4 v0, 0x2

    iget-object v1, p0, Ldxl;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 9942
    :cond_1
    iget-object v0, p0, Ldxl;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 9944
    return-void
.end method
