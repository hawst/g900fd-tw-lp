.class public final Ldvu;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldvu;


# instance fields
.field public b:Ldvm;

.field public c:Ldrx;

.field public d:[Ldru;

.field public e:[Ldpr;

.field public f:Ldth;

.field public g:Ldtx;

.field public h:Ldry;

.field public i:Ldtq;

.field public j:Ldsc;

.field public k:Ldxb;

.field public l:[B

.field public m:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5117
    const/4 v0, 0x0

    new-array v0, v0, [Ldvu;

    sput-object v0, Ldvu;->a:[Ldvu;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5118
    invoke-direct {p0}, Lepn;-><init>()V

    .line 5121
    iput-object v1, p0, Ldvu;->b:Ldvm;

    .line 5124
    iput-object v1, p0, Ldvu;->c:Ldrx;

    .line 5127
    sget-object v0, Ldru;->a:[Ldru;

    iput-object v0, p0, Ldvu;->d:[Ldru;

    .line 5130
    sget-object v0, Ldpr;->a:[Ldpr;

    iput-object v0, p0, Ldvu;->e:[Ldpr;

    .line 5133
    iput-object v1, p0, Ldvu;->f:Ldth;

    .line 5136
    iput-object v1, p0, Ldvu;->g:Ldtx;

    .line 5139
    iput-object v1, p0, Ldvu;->h:Ldry;

    .line 5142
    iput-object v1, p0, Ldvu;->i:Ldtq;

    .line 5145
    iput-object v1, p0, Ldvu;->j:Ldsc;

    .line 5148
    iput-object v1, p0, Ldvu;->k:Ldxb;

    .line 5118
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 5208
    iget-object v0, p0, Ldvu;->b:Ldvm;

    if-eqz v0, :cond_d

    .line 5209
    const/4 v0, 0x1

    iget-object v2, p0, Ldvu;->b:Ldvm;

    .line 5210
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5212
    :goto_0
    iget-object v2, p0, Ldvu;->l:[B

    if-eqz v2, :cond_0

    .line 5213
    const/4 v2, 0x2

    iget-object v3, p0, Ldvu;->l:[B

    .line 5214
    invoke-static {v2, v3}, Lepl;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 5216
    :cond_0
    iget-object v2, p0, Ldvu;->m:Ljava/lang/Long;

    if-eqz v2, :cond_1

    .line 5217
    const/4 v2, 0x3

    iget-object v3, p0, Ldvu;->m:Ljava/lang/Long;

    .line 5218
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 5220
    :cond_1
    iget-object v2, p0, Ldvu;->d:[Ldru;

    if-eqz v2, :cond_3

    .line 5221
    iget-object v3, p0, Ldvu;->d:[Ldru;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 5222
    if-eqz v5, :cond_2

    .line 5223
    const/4 v6, 0x5

    .line 5224
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 5221
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 5228
    :cond_3
    iget-object v2, p0, Ldvu;->g:Ldtx;

    if-eqz v2, :cond_4

    .line 5229
    const/4 v2, 0x6

    iget-object v3, p0, Ldvu;->g:Ldtx;

    .line 5230
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5232
    :cond_4
    iget-object v2, p0, Ldvu;->h:Ldry;

    if-eqz v2, :cond_5

    .line 5233
    const/4 v2, 0x7

    iget-object v3, p0, Ldvu;->h:Ldry;

    .line 5234
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5236
    :cond_5
    iget-object v2, p0, Ldvu;->c:Ldrx;

    if-eqz v2, :cond_6

    .line 5237
    const/16 v2, 0x8

    iget-object v3, p0, Ldvu;->c:Ldrx;

    .line 5238
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5240
    :cond_6
    iget-object v2, p0, Ldvu;->f:Ldth;

    if-eqz v2, :cond_7

    .line 5241
    const/16 v2, 0x9

    iget-object v3, p0, Ldvu;->f:Ldth;

    .line 5242
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5244
    :cond_7
    iget-object v2, p0, Ldvu;->i:Ldtq;

    if-eqz v2, :cond_8

    .line 5245
    const/16 v2, 0xa

    iget-object v3, p0, Ldvu;->i:Ldtq;

    .line 5246
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5248
    :cond_8
    iget-object v2, p0, Ldvu;->j:Ldsc;

    if-eqz v2, :cond_9

    .line 5249
    const/16 v2, 0xb

    iget-object v3, p0, Ldvu;->j:Ldsc;

    .line 5250
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5252
    :cond_9
    iget-object v2, p0, Ldvu;->e:[Ldpr;

    if-eqz v2, :cond_b

    .line 5253
    iget-object v2, p0, Ldvu;->e:[Ldpr;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_b

    aget-object v4, v2, v1

    .line 5254
    if-eqz v4, :cond_a

    .line 5255
    const/16 v5, 0xc

    .line 5256
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 5253
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 5260
    :cond_b
    iget-object v1, p0, Ldvu;->k:Ldxb;

    if-eqz v1, :cond_c

    .line 5261
    const/16 v1, 0xd

    iget-object v2, p0, Ldvu;->k:Ldxb;

    .line 5262
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5264
    :cond_c
    iget-object v1, p0, Ldvu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5265
    iput v0, p0, Ldvu;->cachedSize:I

    .line 5266
    return v0

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5114
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldvu;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldvu;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldvu;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldvu;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldvu;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldvu;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Ldvu;->l:[B

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldvu;->m:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldvu;->d:[Ldru;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldru;

    iget-object v3, p0, Ldvu;->d:[Ldru;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldvu;->d:[Ldru;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldvu;->d:[Ldru;

    :goto_2
    iget-object v2, p0, Ldvu;->d:[Ldru;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldvu;->d:[Ldru;

    new-instance v3, Ldru;

    invoke-direct {v3}, Ldru;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldvu;->d:[Ldru;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldvu;->d:[Ldru;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldvu;->d:[Ldru;

    new-instance v3, Ldru;

    invoke-direct {v3}, Ldru;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldvu;->d:[Ldru;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Ldvu;->g:Ldtx;

    if-nez v0, :cond_6

    new-instance v0, Ldtx;

    invoke-direct {v0}, Ldtx;-><init>()V

    iput-object v0, p0, Ldvu;->g:Ldtx;

    :cond_6
    iget-object v0, p0, Ldvu;->g:Ldtx;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Ldvu;->h:Ldry;

    if-nez v0, :cond_7

    new-instance v0, Ldry;

    invoke-direct {v0}, Ldry;-><init>()V

    iput-object v0, p0, Ldvu;->h:Ldry;

    :cond_7
    iget-object v0, p0, Ldvu;->h:Ldry;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Ldvu;->c:Ldrx;

    if-nez v0, :cond_8

    new-instance v0, Ldrx;

    invoke-direct {v0}, Ldrx;-><init>()V

    iput-object v0, p0, Ldvu;->c:Ldrx;

    :cond_8
    iget-object v0, p0, Ldvu;->c:Ldrx;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Ldvu;->f:Ldth;

    if-nez v0, :cond_9

    new-instance v0, Ldth;

    invoke-direct {v0}, Ldth;-><init>()V

    iput-object v0, p0, Ldvu;->f:Ldth;

    :cond_9
    iget-object v0, p0, Ldvu;->f:Ldth;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Ldvu;->i:Ldtq;

    if-nez v0, :cond_a

    new-instance v0, Ldtq;

    invoke-direct {v0}, Ldtq;-><init>()V

    iput-object v0, p0, Ldvu;->i:Ldtq;

    :cond_a
    iget-object v0, p0, Ldvu;->i:Ldtq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Ldvu;->j:Ldsc;

    if-nez v0, :cond_b

    new-instance v0, Ldsc;

    invoke-direct {v0}, Ldsc;-><init>()V

    iput-object v0, p0, Ldvu;->j:Ldsc;

    :cond_b
    iget-object v0, p0, Ldvu;->j:Ldsc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_b
    const/16 v0, 0x62

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldvu;->e:[Ldpr;

    if-nez v0, :cond_d

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldpr;

    iget-object v3, p0, Ldvu;->e:[Ldpr;

    if-eqz v3, :cond_c

    iget-object v3, p0, Ldvu;->e:[Ldpr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_c
    iput-object v2, p0, Ldvu;->e:[Ldpr;

    :goto_4
    iget-object v2, p0, Ldvu;->e:[Ldpr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_e

    iget-object v2, p0, Ldvu;->e:[Ldpr;

    new-instance v3, Ldpr;

    invoke-direct {v3}, Ldpr;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldvu;->e:[Ldpr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_d
    iget-object v0, p0, Ldvu;->e:[Ldpr;

    array-length v0, v0

    goto :goto_3

    :cond_e
    iget-object v2, p0, Ldvu;->e:[Ldpr;

    new-instance v3, Ldpr;

    invoke-direct {v3}, Ldpr;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldvu;->e:[Ldpr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Ldvu;->k:Ldxb;

    if-nez v0, :cond_f

    new-instance v0, Ldxb;

    invoke-direct {v0}, Ldxb;-><init>()V

    iput-object v0, p0, Ldvu;->k:Ldxb;

    :cond_f
    iget-object v0, p0, Ldvu;->k:Ldxb;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 5157
    iget-object v1, p0, Ldvu;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 5158
    const/4 v1, 0x1

    iget-object v2, p0, Ldvu;->b:Ldvm;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 5160
    :cond_0
    iget-object v1, p0, Ldvu;->l:[B

    if-eqz v1, :cond_1

    .line 5161
    const/4 v1, 0x2

    iget-object v2, p0, Ldvu;->l:[B

    invoke-virtual {p1, v1, v2}, Lepl;->a(I[B)V

    .line 5163
    :cond_1
    iget-object v1, p0, Ldvu;->m:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 5164
    const/4 v1, 0x3

    iget-object v2, p0, Ldvu;->m:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->a(IJ)V

    .line 5166
    :cond_2
    iget-object v1, p0, Ldvu;->d:[Ldru;

    if-eqz v1, :cond_4

    .line 5167
    iget-object v2, p0, Ldvu;->d:[Ldru;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 5168
    if-eqz v4, :cond_3

    .line 5169
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 5167
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5173
    :cond_4
    iget-object v1, p0, Ldvu;->g:Ldtx;

    if-eqz v1, :cond_5

    .line 5174
    const/4 v1, 0x6

    iget-object v2, p0, Ldvu;->g:Ldtx;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 5176
    :cond_5
    iget-object v1, p0, Ldvu;->h:Ldry;

    if-eqz v1, :cond_6

    .line 5177
    const/4 v1, 0x7

    iget-object v2, p0, Ldvu;->h:Ldry;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 5179
    :cond_6
    iget-object v1, p0, Ldvu;->c:Ldrx;

    if-eqz v1, :cond_7

    .line 5180
    const/16 v1, 0x8

    iget-object v2, p0, Ldvu;->c:Ldrx;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 5182
    :cond_7
    iget-object v1, p0, Ldvu;->f:Ldth;

    if-eqz v1, :cond_8

    .line 5183
    const/16 v1, 0x9

    iget-object v2, p0, Ldvu;->f:Ldth;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 5185
    :cond_8
    iget-object v1, p0, Ldvu;->i:Ldtq;

    if-eqz v1, :cond_9

    .line 5186
    const/16 v1, 0xa

    iget-object v2, p0, Ldvu;->i:Ldtq;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 5188
    :cond_9
    iget-object v1, p0, Ldvu;->j:Ldsc;

    if-eqz v1, :cond_a

    .line 5189
    const/16 v1, 0xb

    iget-object v2, p0, Ldvu;->j:Ldsc;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 5191
    :cond_a
    iget-object v1, p0, Ldvu;->e:[Ldpr;

    if-eqz v1, :cond_c

    .line 5192
    iget-object v1, p0, Ldvu;->e:[Ldpr;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_c

    aget-object v3, v1, v0

    .line 5193
    if-eqz v3, :cond_b

    .line 5194
    const/16 v4, 0xc

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 5192
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 5198
    :cond_c
    iget-object v0, p0, Ldvu;->k:Ldxb;

    if-eqz v0, :cond_d

    .line 5199
    const/16 v0, 0xd

    iget-object v1, p0, Ldvu;->k:Ldxb;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5201
    :cond_d
    iget-object v0, p0, Ldvu;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 5203
    return-void
.end method
