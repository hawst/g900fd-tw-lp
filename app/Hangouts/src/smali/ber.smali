.class public Lber;
.super Lbei;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:I

.field public final b:I

.field public final j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 4029
    invoke-direct {p0, p2, p1}, Lbei;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 4030
    iput p3, p0, Lber;->a:I

    .line 4031
    iput p4, p0, Lber;->b:I

    .line 4032
    iput-object p2, p0, Lber;->j:Ljava/lang/String;

    .line 4033
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 3

    .prologue
    .line 4037
    new-instance v0, Ldua;

    invoke-direct {v0}, Ldua;-><init>()V

    .line 4039
    new-instance v1, Ldrx;

    invoke-direct {v1}, Ldrx;-><init>()V

    .line 4041
    iget-object v2, p0, Lber;->i:Ljava/lang/String;

    .line 4042
    invoke-static {v2}, Lyt;->c(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Ldrx;->c:Ljava/lang/Long;

    .line 4043
    iget-object v2, p0, Lber;->j:Ljava/lang/String;

    invoke-static {v2}, Lbea;->d(Ljava/lang/String;)Ldqf;

    move-result-object v2

    iput-object v2, v1, Ldrx;->b:Ldqf;

    .line 4044
    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Ldrx;->f:Ljava/lang/Integer;

    .line 4046
    iget v2, p0, Lber;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v0, Ldua;->d:Ljava/lang/Integer;

    .line 4047
    iput-object v1, v0, Ldua;->c:Ldrx;

    .line 4048
    new-instance v1, Ldvm;

    invoke-direct {v1}, Ldvm;-><init>()V

    iput-object v1, v0, Ldua;->b:Ldvm;

    .line 4049
    iget-object v1, v0, Ldua;->b:Ldvm;

    iget-object v2, p0, Lber;->g:Lbci;

    invoke-virtual {v2}, Lbci;->c()Ldpv;

    move-result-object v2

    iput-object v2, v1, Ldvm;->b:Ldpv;

    .line 4050
    return-object v0
.end method

.method public a(Lyj;Lbph;)V
    .locals 3

    .prologue
    .line 4060
    invoke-super {p0, p1, p2}, Lbei;->a(Lyj;Lbph;)V

    .line 4061
    iget-boolean v0, p0, Lber;->h:Z

    if-nez v0, :cond_0

    .line 4062
    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lber;->j:Ljava/lang/String;

    iget v2, p0, Lber;->b:I

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;Ljava/lang/String;I)I

    .line 4065
    :cond_0
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4055
    const-string v0, "conversations/modifyotrstatus"

    return-object v0
.end method
