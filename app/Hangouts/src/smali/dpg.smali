.class public final Ldpg;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldpg;


# instance fields
.field public b:[B

.field public c:[B

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/Long;

.field public g:Ljava/lang/Long;

.field public h:Ljava/lang/Integer;

.field public i:Ljava/lang/Boolean;

.field public j:Ljava/lang/Long;

.field public k:Ljava/lang/Long;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 566
    const/4 v0, 0x0

    new-array v0, v0, [Ldpg;

    sput-object v0, Ldpg;->a:[Ldpg;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 567
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 654
    const/4 v0, 0x1

    iget-object v1, p0, Ldpg;->b:[B

    .line 656
    invoke-static {v0, v1}, Lepl;->b(I[B)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 657
    iget-object v1, p0, Ldpg;->c:[B

    if-eqz v1, :cond_0

    .line 658
    const/4 v1, 0x2

    iget-object v2, p0, Ldpg;->c:[B

    .line 659
    invoke-static {v1, v2}, Lepl;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 661
    :cond_0
    const/4 v1, 0x3

    iget-object v2, p0, Ldpg;->d:Ljava/lang/Integer;

    .line 662
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 663
    iget-object v1, p0, Ldpg;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 664
    const/4 v1, 0x5

    iget-object v2, p0, Ldpg;->e:Ljava/lang/Boolean;

    .line 665
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 667
    :cond_1
    iget-object v1, p0, Ldpg;->j:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 668
    const/4 v1, 0x6

    iget-object v2, p0, Ldpg;->j:Ljava/lang/Long;

    .line 669
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 671
    :cond_2
    iget-object v1, p0, Ldpg;->g:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 672
    const/16 v1, 0x8

    iget-object v2, p0, Ldpg;->g:Ljava/lang/Long;

    .line 673
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 675
    :cond_3
    iget-object v1, p0, Ldpg;->f:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 676
    const/16 v1, 0x9

    iget-object v2, p0, Ldpg;->f:Ljava/lang/Long;

    .line 677
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 679
    :cond_4
    iget-object v1, p0, Ldpg;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 680
    const/16 v1, 0xa

    iget-object v2, p0, Ldpg;->h:Ljava/lang/Integer;

    .line 681
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 683
    :cond_5
    iget-object v1, p0, Ldpg;->k:Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 684
    const/16 v1, 0xb

    iget-object v2, p0, Ldpg;->k:Ljava/lang/Long;

    .line 685
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 687
    :cond_6
    iget-object v1, p0, Ldpg;->l:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 688
    const/16 v1, 0xc

    iget-object v2, p0, Ldpg;->l:Ljava/lang/String;

    .line 689
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 691
    :cond_7
    iget-object v1, p0, Ldpg;->m:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 692
    const/16 v1, 0xd

    iget-object v2, p0, Ldpg;->m:Ljava/lang/String;

    .line 693
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 695
    :cond_8
    iget-object v1, p0, Ldpg;->n:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 696
    const/16 v1, 0xe

    iget-object v2, p0, Ldpg;->n:Ljava/lang/String;

    .line 697
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 699
    :cond_9
    iget-object v1, p0, Ldpg;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 700
    const/16 v1, 0xf

    iget-object v2, p0, Ldpg;->i:Ljava/lang/Boolean;

    .line 701
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 703
    :cond_a
    iget-object v1, p0, Ldpg;->o:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 704
    const/16 v1, 0x10

    iget-object v2, p0, Ldpg;->o:Ljava/lang/String;

    .line 705
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 707
    :cond_b
    iget-object v1, p0, Ldpg;->p:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 708
    const/16 v1, 0x11

    iget-object v2, p0, Ldpg;->p:Ljava/lang/String;

    .line 709
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 711
    :cond_c
    iget-object v1, p0, Ldpg;->q:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 712
    const/16 v1, 0x12

    iget-object v2, p0, Ldpg;->q:Ljava/lang/Integer;

    .line 713
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 715
    :cond_d
    iget-object v1, p0, Ldpg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 716
    iput v0, p0, Ldpg;->cachedSize:I

    .line 717
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 563
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldpg;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldpg;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldpg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Ldpg;->b:[B

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Ldpg;->c:[B

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpg;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldpg;->e:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldpg;->j:Ljava/lang/Long;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldpg;->g:Ljava/lang/Long;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldpg;->f:Ljava/lang/Long;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpg;->h:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldpg;->k:Ljava/lang/Long;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldpg;->l:Ljava/lang/String;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldpg;->m:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldpg;->n:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldpg;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldpg;->o:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldpg;->p:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lepk;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpg;->q:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x40 -> :sswitch_6
        0x48 -> :sswitch_7
        0x50 -> :sswitch_8
        0x58 -> :sswitch_9
        0x62 -> :sswitch_a
        0x6a -> :sswitch_b
        0x72 -> :sswitch_c
        0x78 -> :sswitch_d
        0x82 -> :sswitch_e
        0x8a -> :sswitch_f
        0x90 -> :sswitch_10
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 604
    const/4 v0, 0x1

    iget-object v1, p0, Ldpg;->b:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 605
    iget-object v0, p0, Ldpg;->c:[B

    if-eqz v0, :cond_0

    .line 606
    const/4 v0, 0x2

    iget-object v1, p0, Ldpg;->c:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 608
    :cond_0
    const/4 v0, 0x3

    iget-object v1, p0, Ldpg;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->c(II)V

    .line 609
    iget-object v0, p0, Ldpg;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 610
    const/4 v0, 0x5

    iget-object v1, p0, Ldpg;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 612
    :cond_1
    iget-object v0, p0, Ldpg;->j:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 613
    const/4 v0, 0x6

    iget-object v1, p0, Ldpg;->j:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 615
    :cond_2
    iget-object v0, p0, Ldpg;->g:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 616
    const/16 v0, 0x8

    iget-object v1, p0, Ldpg;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 618
    :cond_3
    iget-object v0, p0, Ldpg;->f:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 619
    const/16 v0, 0x9

    iget-object v1, p0, Ldpg;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 621
    :cond_4
    iget-object v0, p0, Ldpg;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 622
    const/16 v0, 0xa

    iget-object v1, p0, Ldpg;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->c(II)V

    .line 624
    :cond_5
    iget-object v0, p0, Ldpg;->k:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 625
    const/16 v0, 0xb

    iget-object v1, p0, Ldpg;->k:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 627
    :cond_6
    iget-object v0, p0, Ldpg;->l:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 628
    const/16 v0, 0xc

    iget-object v1, p0, Ldpg;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 630
    :cond_7
    iget-object v0, p0, Ldpg;->m:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 631
    const/16 v0, 0xd

    iget-object v1, p0, Ldpg;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 633
    :cond_8
    iget-object v0, p0, Ldpg;->n:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 634
    const/16 v0, 0xe

    iget-object v1, p0, Ldpg;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 636
    :cond_9
    iget-object v0, p0, Ldpg;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 637
    const/16 v0, 0xf

    iget-object v1, p0, Ldpg;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 639
    :cond_a
    iget-object v0, p0, Ldpg;->o:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 640
    const/16 v0, 0x10

    iget-object v1, p0, Ldpg;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 642
    :cond_b
    iget-object v0, p0, Ldpg;->p:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 643
    const/16 v0, 0x11

    iget-object v1, p0, Ldpg;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 645
    :cond_c
    iget-object v0, p0, Ldpg;->q:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 646
    const/16 v0, 0x12

    iget-object v1, p0, Ldpg;->q:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->c(II)V

    .line 648
    :cond_d
    iget-object v0, p0, Ldpg;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 650
    return-void
.end method
