.class public final Lcxc;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Lcxh;

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    invoke-static {}, Lcxc;->d()I

    move-result v0

    sput v0, Lcxc;->b:I

    return-void
.end method

.method public static a()I
    .locals 1

    .prologue
    .line 32
    sget v0, Lcxc;->b:I

    return v0
.end method

.method public static a(Lcxh;)I
    .locals 1

    .prologue
    .line 24
    invoke-static {}, Lcxc;->d()I

    move-result v0

    sput v0, Lcxc;->b:I

    .line 27
    sput-object p0, Lcxc;->a:Lcxh;

    .line 28
    sget v0, Lcxc;->b:I

    return v0
.end method

.method private static a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 42
    sget v0, Lcxc;->b:I

    if-lt p0, v0, :cond_0

    .line 43
    invoke-static {p0, p1, p2}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 46
    :cond_0
    sget-object v0, Lcxc;->a:Lcxh;

    .line 47
    if-eqz v0, :cond_1

    const/4 v1, 0x3

    if-lt p0, v1, :cond_1

    .line 48
    invoke-virtual {v0, p0, p1, p2}, Lcxh;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 50
    :cond_1
    return-void
.end method

.method public static a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 59
    invoke-static {p0}, Lcxc;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 61
    invoke-virtual {p3}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 62
    invoke-static {p3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 60
    invoke-static {p0, p1, v0}, Lcxc;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 64
    :cond_0
    return-void
.end method

.method public static varargs a(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 53
    invoke-static {p0}, Lcxc;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    invoke-static {p2, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcxc;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 56
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x2

    invoke-static {v0, p0, p1}, Lcxc;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 68
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x6

    invoke-static {v0, p0, p1, p2}, Lcxc;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 108
    return-void
.end method

.method public static varargs a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x2

    invoke-static {v0, p0, p1, p2}, Lcxc;->a(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 72
    return-void
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 37
    sget v0, Lcxc;->b:I

    if-ge p0, v0, :cond_0

    sget-object v0, Lcxc;->a:Lcxh;

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    if-lt p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x3

    invoke-static {p0, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 211
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "vclib."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x3

    invoke-static {v0, p0, p1}, Lcxc;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method public static varargs b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 95
    const/4 v0, 0x5

    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcxc;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 96
    return-void
.end method

.method public static b()Z
    .locals 2

    .prologue
    .line 153
    const/4 v0, 0x2

    sget v1, Lcxc;->b:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x4

    invoke-static {v0, p0, p1}, Lcxc;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 84
    return-void
.end method

.method public static c()Z
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x3

    invoke-static {v0}, Lcxc;->a(I)Z

    move-result v0

    return v0
.end method

.method private static d()I
    .locals 6

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x4

    const/4 v1, 0x3

    const/4 v0, 0x2

    .line 146
    const-string v4, "vclib"

    invoke-static {v4, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-static {v4, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-static {v4, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-static {v4, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v3

    goto :goto_0

    :cond_3
    const/4 v0, 0x6

    goto :goto_0
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x5

    invoke-static {v0, p0, p1}, Lcxc;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 92
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x6

    invoke-static {v0, p0, p1}, Lcxc;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method public static f(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x6

    invoke-static {v0, p0, p1}, Lcxc;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 119
    invoke-static {p0, p1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    invoke-static {}, Lcxc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 126
    :cond_0
    return-void
.end method
