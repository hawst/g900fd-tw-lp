.class public final Lawa;
.super Lbor;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lawa;->a:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    invoke-direct {p0}, Lbor;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILbkr;Lbos;)V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 291
    iget-object v0, p0, Lawa;->a:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->a(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;)I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 317
    :goto_0
    return-void

    .line 294
    :cond_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 295
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onConversationCreated called, error "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Lbos;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->c(Ljava/lang/String;)V

    .line 297
    :cond_1
    invoke-virtual {p3}, Lbos;->b()I

    move-result v0

    if-eq v0, v4, :cond_2

    .line 298
    iget-object v0, p0, Lawa;->a:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    sget v1, Lh;->cE:I

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->a(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;I)V

    goto :goto_0

    .line 302
    :cond_2
    iget-boolean v0, p2, Lbkr;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-object v1, p0, Lawa;->a:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->b(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 304
    iget-boolean v0, p2, Lbkr;->c:Z

    if-eqz v0, :cond_3

    .line 305
    new-instance v0, Lbbw;

    iget-object v1, p0, Lawa;->a:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    iget-object v2, p0, Lawa;->a:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->c(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;)Lyj;

    move-result-object v2

    iget-object v3, p2, Lbkr;->a:Ljava/lang/String;

    iget-object v6, p0, Lawa;->a:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    .line 309
    invoke-static {v6}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->d(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;)Z

    move-result v6

    iget-object v7, p0, Lawa;->a:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    .line 310
    invoke-static {v7}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->e(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;)I

    move-result v7

    move v8, v5

    invoke-direct/range {v0 .. v8}, Lbbw;-><init>(Landroid/app/Activity;Lyj;Ljava/lang/String;ZIZIZ)V

    new-array v1, v5, [Ljava/lang/Void;

    .line 311
    invoke-virtual {v0, v1}, Lbbw;->executeOnThreadPool([Ljava/lang/Object;)Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;

    .line 312
    iget-object v0, p0, Lawa;->a:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->setResult(I)V

    goto :goto_0

    .line 314
    :cond_3
    iget-object v0, p0, Lawa;->a:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    iget-object v1, p2, Lbkr;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->a(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 315
    iget-object v0, p0, Lawa;->a:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    iget v1, p2, Lbkr;->b:I

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->b(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;I)V

    goto :goto_0
.end method
