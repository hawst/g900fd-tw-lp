.class public final Ldla;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldla;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 535
    const/4 v0, 0x0

    new-array v0, v0, [Ldla;

    sput-object v0, Ldla;->a:[Ldla;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 536
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 562
    const/4 v0, 0x0

    .line 563
    iget-object v1, p0, Ldla;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 564
    const/4 v0, 0x1

    iget-object v1, p0, Ldla;->b:Ljava/lang/String;

    .line 565
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 567
    :cond_0
    iget-object v1, p0, Ldla;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 568
    const/4 v1, 0x2

    iget-object v2, p0, Ldla;->c:Ljava/lang/String;

    .line 569
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 571
    :cond_1
    iget-object v1, p0, Ldla;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 572
    const/4 v1, 0x3

    iget-object v2, p0, Ldla;->d:Ljava/lang/String;

    .line 573
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 575
    :cond_2
    iget-object v1, p0, Ldla;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 576
    iput v0, p0, Ldla;->cachedSize:I

    .line 577
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 532
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldla;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldla;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldla;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldla;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldla;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldla;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 547
    iget-object v0, p0, Ldla;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 548
    const/4 v0, 0x1

    iget-object v1, p0, Ldla;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 550
    :cond_0
    iget-object v0, p0, Ldla;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 551
    const/4 v0, 0x2

    iget-object v1, p0, Ldla;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 553
    :cond_1
    iget-object v0, p0, Ldla;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 554
    const/4 v0, 0x3

    iget-object v1, p0, Ldla;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 556
    :cond_2
    iget-object v0, p0, Ldla;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 558
    return-void
.end method
