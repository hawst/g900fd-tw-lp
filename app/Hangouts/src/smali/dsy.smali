.class public final Ldsy;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldsy;


# instance fields
.field public b:Ldqf;

.field public c:Ljava/lang/String;

.field public d:Ldxd;

.field public e:Ldwr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8363
    const/4 v0, 0x0

    new-array v0, v0, [Ldsy;

    sput-object v0, Ldsy;->a:[Ldsy;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8364
    invoke-direct {p0}, Lepn;-><init>()V

    .line 8367
    iput-object v0, p0, Ldsy;->b:Ldqf;

    .line 8372
    iput-object v0, p0, Ldsy;->d:Ldxd;

    .line 8375
    iput-object v0, p0, Ldsy;->e:Ldwr;

    .line 8364
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 8398
    const/4 v0, 0x0

    .line 8399
    iget-object v1, p0, Ldsy;->b:Ldqf;

    if-eqz v1, :cond_0

    .line 8400
    const/4 v0, 0x1

    iget-object v1, p0, Ldsy;->b:Ldqf;

    .line 8401
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8403
    :cond_0
    iget-object v1, p0, Ldsy;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 8404
    const/4 v1, 0x2

    iget-object v2, p0, Ldsy;->c:Ljava/lang/String;

    .line 8405
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8407
    :cond_1
    iget-object v1, p0, Ldsy;->d:Ldxd;

    if-eqz v1, :cond_2

    .line 8408
    const/4 v1, 0x3

    iget-object v2, p0, Ldsy;->d:Ldxd;

    .line 8409
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8411
    :cond_2
    iget-object v1, p0, Ldsy;->e:Ldwr;

    if-eqz v1, :cond_3

    .line 8412
    const/4 v1, 0x4

    iget-object v2, p0, Ldsy;->e:Ldwr;

    .line 8413
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8415
    :cond_3
    iget-object v1, p0, Ldsy;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8416
    iput v0, p0, Ldsy;->cachedSize:I

    .line 8417
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 8360
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldsy;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldsy;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldsy;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldsy;->b:Ldqf;

    if-nez v0, :cond_2

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldsy;->b:Ldqf;

    :cond_2
    iget-object v0, p0, Ldsy;->b:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldsy;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldsy;->d:Ldxd;

    if-nez v0, :cond_3

    new-instance v0, Ldxd;

    invoke-direct {v0}, Ldxd;-><init>()V

    iput-object v0, p0, Ldsy;->d:Ldxd;

    :cond_3
    iget-object v0, p0, Ldsy;->d:Ldxd;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldsy;->e:Ldwr;

    if-nez v0, :cond_4

    new-instance v0, Ldwr;

    invoke-direct {v0}, Ldwr;-><init>()V

    iput-object v0, p0, Ldsy;->e:Ldwr;

    :cond_4
    iget-object v0, p0, Ldsy;->e:Ldwr;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 8380
    iget-object v0, p0, Ldsy;->b:Ldqf;

    if-eqz v0, :cond_0

    .line 8381
    const/4 v0, 0x1

    iget-object v1, p0, Ldsy;->b:Ldqf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 8383
    :cond_0
    iget-object v0, p0, Ldsy;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 8384
    const/4 v0, 0x2

    iget-object v1, p0, Ldsy;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 8386
    :cond_1
    iget-object v0, p0, Ldsy;->d:Ldxd;

    if-eqz v0, :cond_2

    .line 8387
    const/4 v0, 0x3

    iget-object v1, p0, Ldsy;->d:Ldxd;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 8389
    :cond_2
    iget-object v0, p0, Ldsy;->e:Ldwr;

    if-eqz v0, :cond_3

    .line 8390
    const/4 v0, 0x4

    iget-object v1, p0, Ldsy;->e:Ldwr;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 8392
    :cond_3
    iget-object v0, p0, Ldsy;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 8394
    return-void
.end method
