.class public final Levy;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Levy;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Levz;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/Integer;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/Integer;

.field public k:Lewa;

.field public l:[Levz;

.field public m:Levq;

.field public n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Levy;

    sput-object v0, Levy;->a:[Levy;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 134
    iput-object v1, p0, Levy;->b:Ljava/lang/Integer;

    .line 137
    iput-object v1, p0, Levy;->c:Levz;

    .line 154
    iput-object v1, p0, Levy;->k:Lewa;

    .line 157
    sget-object v0, Levz;->a:[Levz;

    iput-object v0, p0, Levy;->l:[Levz;

    .line 160
    iput-object v1, p0, Levy;->m:Levq;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 217
    iget-object v0, p0, Levy;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 218
    const/4 v0, 0x1

    iget-object v2, p0, Levy;->b:Ljava/lang/Integer;

    .line 219
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 221
    :goto_0
    iget-object v2, p0, Levy;->c:Levz;

    if-eqz v2, :cond_0

    .line 222
    const/4 v2, 0x2

    iget-object v3, p0, Levy;->c:Levz;

    .line 223
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 225
    :cond_0
    iget-object v2, p0, Levy;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 226
    const/4 v2, 0x3

    iget-object v3, p0, Levy;->d:Ljava/lang/String;

    .line 227
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 229
    :cond_1
    iget-object v2, p0, Levy;->e:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 230
    const/4 v2, 0x4

    iget-object v3, p0, Levy;->e:Ljava/lang/String;

    .line 231
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 233
    :cond_2
    iget-object v2, p0, Levy;->g:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 234
    const/4 v2, 0x5

    iget-object v3, p0, Levy;->g:Ljava/lang/String;

    .line 235
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 237
    :cond_3
    iget-object v2, p0, Levy;->i:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 238
    const/4 v2, 0x6

    iget-object v3, p0, Levy;->i:Ljava/lang/String;

    .line 239
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 241
    :cond_4
    iget-object v2, p0, Levy;->j:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    .line 242
    const/4 v2, 0x7

    iget-object v3, p0, Levy;->j:Ljava/lang/Integer;

    .line 243
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 245
    :cond_5
    iget-object v2, p0, Levy;->k:Lewa;

    if-eqz v2, :cond_6

    .line 246
    const/16 v2, 0x8

    iget-object v3, p0, Levy;->k:Lewa;

    .line 247
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 249
    :cond_6
    iget-object v2, p0, Levy;->h:Ljava/lang/Integer;

    if-eqz v2, :cond_7

    .line 250
    const/16 v2, 0x9

    iget-object v3, p0, Levy;->h:Ljava/lang/Integer;

    .line 251
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 253
    :cond_7
    iget-object v2, p0, Levy;->l:[Levz;

    if-eqz v2, :cond_9

    .line 254
    iget-object v2, p0, Levy;->l:[Levz;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_9

    aget-object v4, v2, v1

    .line 255
    if-eqz v4, :cond_8

    .line 256
    const/16 v5, 0xa

    .line 257
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 254
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 261
    :cond_9
    iget-object v1, p0, Levy;->m:Levq;

    if-eqz v1, :cond_a

    .line 262
    const/16 v1, 0xb

    iget-object v2, p0, Levy;->m:Levq;

    .line 263
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 265
    :cond_a
    iget-object v1, p0, Levy;->f:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 266
    const/16 v1, 0xc

    iget-object v2, p0, Levy;->f:Ljava/lang/String;

    .line 267
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 269
    :cond_b
    iget-object v1, p0, Levy;->n:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 270
    const/16 v1, 0xd

    iget-object v2, p0, Levy;->n:Ljava/lang/String;

    .line 271
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 273
    :cond_c
    iget-object v1, p0, Levy;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 274
    iput v0, p0, Levy;->cachedSize:I

    .line 275
    return v0

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Levy;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Levy;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Levy;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Levy;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Levy;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Levy;->c:Levz;

    if-nez v0, :cond_4

    new-instance v0, Levz;

    invoke-direct {v0}, Levz;-><init>()V

    iput-object v0, p0, Levy;->c:Levz;

    :cond_4
    iget-object v0, p0, Levy;->c:Levz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levy;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levy;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levy;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levy;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Levy;->j:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Levy;->k:Lewa;

    if-nez v0, :cond_5

    new-instance v0, Lewa;

    invoke-direct {v0}, Lewa;-><init>()V

    iput-object v0, p0, Levy;->k:Lewa;

    :cond_5
    iget-object v0, p0, Levy;->k:Lewa;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Levy;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_a
    const/16 v0, 0x52

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Levy;->l:[Levz;

    if-nez v0, :cond_7

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Levz;

    iget-object v3, p0, Levy;->l:[Levz;

    if-eqz v3, :cond_6

    iget-object v3, p0, Levy;->l:[Levz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Levy;->l:[Levz;

    :goto_2
    iget-object v2, p0, Levy;->l:[Levz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Levy;->l:[Levz;

    new-instance v3, Levz;

    invoke-direct {v3}, Levz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Levy;->l:[Levz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    iget-object v0, p0, Levy;->l:[Levz;

    array-length v0, v0

    goto :goto_1

    :cond_8
    iget-object v2, p0, Levy;->l:[Levz;

    new-instance v3, Levz;

    invoke-direct {v3}, Levz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Levy;->l:[Levz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Levy;->m:Levq;

    if-nez v0, :cond_9

    new-instance v0, Levq;

    invoke-direct {v0}, Levq;-><init>()V

    iput-object v0, p0, Levy;->m:Levq;

    :cond_9
    iget-object v0, p0, Levy;->m:Levq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levy;->f:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levy;->n:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 167
    iget-object v0, p0, Levy;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 168
    const/4 v0, 0x1

    iget-object v1, p0, Levy;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 170
    :cond_0
    iget-object v0, p0, Levy;->c:Levz;

    if-eqz v0, :cond_1

    .line 171
    const/4 v0, 0x2

    iget-object v1, p0, Levy;->c:Levz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 173
    :cond_1
    iget-object v0, p0, Levy;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 174
    const/4 v0, 0x3

    iget-object v1, p0, Levy;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 176
    :cond_2
    iget-object v0, p0, Levy;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 177
    const/4 v0, 0x4

    iget-object v1, p0, Levy;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 179
    :cond_3
    iget-object v0, p0, Levy;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 180
    const/4 v0, 0x5

    iget-object v1, p0, Levy;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 182
    :cond_4
    iget-object v0, p0, Levy;->i:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 183
    const/4 v0, 0x6

    iget-object v1, p0, Levy;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 185
    :cond_5
    iget-object v0, p0, Levy;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 186
    const/4 v0, 0x7

    iget-object v1, p0, Levy;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 188
    :cond_6
    iget-object v0, p0, Levy;->k:Lewa;

    if-eqz v0, :cond_7

    .line 189
    const/16 v0, 0x8

    iget-object v1, p0, Levy;->k:Lewa;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 191
    :cond_7
    iget-object v0, p0, Levy;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 192
    const/16 v0, 0x9

    iget-object v1, p0, Levy;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 194
    :cond_8
    iget-object v0, p0, Levy;->l:[Levz;

    if-eqz v0, :cond_a

    .line 195
    iget-object v1, p0, Levy;->l:[Levz;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_a

    aget-object v3, v1, v0

    .line 196
    if-eqz v3, :cond_9

    .line 197
    const/16 v4, 0xa

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 195
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 201
    :cond_a
    iget-object v0, p0, Levy;->m:Levq;

    if-eqz v0, :cond_b

    .line 202
    const/16 v0, 0xb

    iget-object v1, p0, Levy;->m:Levq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 204
    :cond_b
    iget-object v0, p0, Levy;->f:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 205
    const/16 v0, 0xc

    iget-object v1, p0, Levy;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 207
    :cond_c
    iget-object v0, p0, Levy;->n:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 208
    const/16 v0, 0xd

    iget-object v1, p0, Levy;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 210
    :cond_d
    iget-object v0, p0, Levy;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 212
    return-void
.end method
