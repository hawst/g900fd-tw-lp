.class public final Lbpj;
.super Ljava/lang/Thread;
.source "PG"


# instance fields
.field public a:Landroid/os/Handler;

.field final synthetic b:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

.field private final c:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;)V
    .locals 1

    .prologue
    .line 547
    iput-object p1, p0, Lbpj;->b:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 548
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbpj;->c:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 570
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 572
    new-instance v0, Lbpl;

    iget-object v1, p0, Lbpj;->b:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-direct {v0, v1}, Lbpl;-><init>(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;)V

    .line 574
    iget-object v1, p0, Lbpj;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 575
    :try_start_0
    iput-object v0, p0, Lbpj;->a:Landroid/os/Handler;

    .line 576
    iget-object v0, p0, Lbpj;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 577
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 579
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 580
    return-void

    .line 577
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public start()V
    .locals 2

    .prologue
    .line 554
    invoke-super {p0}, Ljava/lang/Thread;->start()V

    .line 557
    iget-object v1, p0, Lbpj;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 558
    :goto_0
    :try_start_0
    iget-object v0, p0, Lbpj;->a:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 560
    :try_start_1
    iget-object v0, p0, Lbpj;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 563
    :catch_0
    move-exception v0

    goto :goto_0

    .line 565
    :cond_0
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
