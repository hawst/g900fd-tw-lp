.class public Lbdx;
.super Lbdp;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final a:[B

.field private final b:[B


# direct methods
.method public constructor <init>([B[B)V
    .locals 0

    .prologue
    .line 290
    invoke-direct {p0}, Lbdp;-><init>()V

    .line 291
    iput-object p1, p0, Lbdx;->a:[B

    .line 292
    iput-object p2, p0, Lbdx;->b:[B

    .line 293
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 3

    .prologue
    .line 297
    new-instance v1, Ldyh;

    invoke-direct {v1}, Ldyh;-><init>()V

    .line 298
    invoke-virtual {p0, p1, p2}, Lbdx;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v0

    iput-object v0, v1, Ldyh;->b:Ldvm;

    .line 300
    :try_start_0
    new-instance v0, Ldxv;

    invoke-direct {v0}, Ldxv;-><init>()V

    iget-object v2, p0, Lbdx;->a:[B

    invoke-static {v0, v2}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldxv;

    iput-object v0, v1, Ldyh;->c:Ldxv;

    .line 301
    new-instance v0, Ldzg;

    invoke-direct {v0}, Ldzg;-><init>()V

    iget-object v2, p0, Lbdx;->b:[B

    invoke-static {v0, v2}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldzg;

    iput-object v0, v1, Ldyh;->d:Ldzg;
    :try_end_0
    .catch Lepq; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 308
    :goto_0
    return-object v0

    .line 303
    :catch_0
    move-exception v0

    .line 304
    const-string v1, "Babel"

    const-string v2, "Parse failed"

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 305
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 313
    const-string v0, "broadcasts/modify"

    return-object v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 318
    invoke-static {}, Lapx;->A()J

    move-result-wide v0

    return-wide v0
.end method
