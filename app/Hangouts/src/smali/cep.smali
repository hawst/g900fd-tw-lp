.class public final Lcep;
.super Landroid/widget/ImageView;
.source "PG"

# interfaces
.implements Lcde;


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcep;-><init>(Landroid/content/Context;B)V

    .line 24
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcep;-><init>(Landroid/content/Context;C)V

    .line 28
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;C)V
    .locals 2

    .prologue
    .line 31
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method static synthetic a(Lcep;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcep;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcep;->a:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public a(Lyj;Ljava/lang/String;Lt;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 35
    invoke-virtual {p0, p2}, Lcep;->a(Ljava/lang/String;)V

    .line 36
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->cr:I

    invoke-virtual {p0, v0}, Lcep;->setImageResource(I)V

    .line 37
    const/4 v0, 0x5

    const/4 v1, 0x6

    invoke-virtual {p0, v2, v0, v2, v1}, Lcep;->setPadding(IIII)V

    .line 38
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcep;->setLongClickable(Z)V

    .line 39
    new-instance v0, Lceq;

    invoke-direct {v0, p0, p1, p3}, Lceq;-><init>(Lcep;Lyj;Lt;)V

    invoke-virtual {p0, v0}, Lcep;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    invoke-virtual {p0}, Lcep;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->nK:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 48
    invoke-virtual {p0, v0}, Lcep;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 49
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 53
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method public f()V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 65
    return-void
.end method
