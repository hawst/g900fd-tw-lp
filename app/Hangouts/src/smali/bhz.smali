.class public Lbhz;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:Ljava/lang/String;

.field private final h:J


# direct methods
.method public constructor <init>(Landroid/net/Uri;J)V
    .locals 1

    .prologue
    .line 579
    invoke-direct {p0}, Lbfz;-><init>()V

    .line 580
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lbhz;->g:Ljava/lang/String;

    .line 581
    iput-wide p2, p0, Lbhz;->h:J

    .line 582
    return-void

    .line 580
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 13

    .prologue
    .line 587
    invoke-super {p0, p1, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 589
    iget-object v0, p0, Lbhz;->b:Lbea;

    check-cast v0, Lbff;

    .line 590
    iget-object v1, v0, Lbff;->r:Ljava/lang/String;

    .line 591
    iget-object v2, v0, Lbff;->q:Ljava/lang/String;

    .line 592
    iget-object v3, p0, Lbhz;->g:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lbhz;->g:Ljava/lang/String;

    .line 595
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    :goto_0
    const-wide/16 v4, 0x0

    iget-wide v6, p0, Lbhz;->h:J

    const/4 v8, 0x0

    const-wide/16 v9, -0x1

    iget-wide v11, v0, Lbff;->c:J

    move-object v0, p1

    .line 592
    invoke-static/range {v0 .. v12}, Lyp;->a(Lyt;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;JJZJJ)V

    .line 601
    invoke-virtual {p1}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v0

    const-string v1, "sent_sms_count_since_last_upload"

    invoke-virtual {v0, v1}, Lbsx;->d(Ljava/lang/String;)V

    .line 603
    return-void

    .line 595
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method
