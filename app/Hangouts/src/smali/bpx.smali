.class public final Lbpx;
.super Lbmu;
.source "PG"


# static fields
.field private static final a:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lbpx;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private d:J

.field private volatile e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lbpx;->a:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method private constructor <init>(Lyj;JJ)V
    .locals 2

    .prologue
    .line 103
    invoke-direct/range {p0 .. p5}, Lbmu;-><init>(Lyj;JJ)V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbpx;->e:Z

    .line 104
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbpx;->d:J

    .line 105
    return-void
.end method

.method public static a(Ljava/lang/String;)Lbpx;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lbpx;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpx;

    return-object v0
.end method

.method public static a(Lyj;)Lbpx;
    .locals 7

    .prologue
    .line 53
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v6

    .line 54
    sget-object v0, Lbpx;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpx;

    .line 55
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lyj;->u()Z

    move-result v1

    if-nez v1, :cond_0

    .line 56
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "babel_ac_renew_lowmark_seconds"

    const/16 v3, 0x78

    invoke-static {v1, v2, v3}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v2, v1

    .line 61
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "babel_ac_renew_highmark_seconds"

    const/16 v4, 0x10e

    invoke-static {v0, v1, v4}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v4, v0

    .line 66
    new-instance v0, Lbpx;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lbpx;-><init>(Lyj;JJ)V

    .line 72
    sget-object v1, Lbpx;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v6, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lbpx;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpx;

    .line 75
    :cond_0
    return-object v0
.end method

.method public static a(Lyj;Z)V
    .locals 2

    .prologue
    .line 86
    sget-object v0, Lbpx;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpx;

    .line 87
    if-eqz v0, :cond_0

    .line 88
    iput-boolean p1, v0, Lbpx;->e:Z

    .line 90
    :cond_0
    return-void
.end method

.method public static b(Lyj;)Z
    .locals 2

    .prologue
    .line 93
    sget-object v0, Lbpx;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpx;

    .line 94
    if-eqz v0, :cond_0

    .line 95
    iget-boolean v0, v0, Lbpx;->e:Z

    .line 98
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 120
    iget-object v0, p0, Lbpx;->b:Lyj;

    invoke-virtual {v0}, Lyj;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    :goto_0
    return-void

    .line 123
    :cond_0
    iget-object v0, p0, Lbpx;->b:Lyj;

    invoke-static {v0}, Lbkb;->i(Lyj;)Ljava/lang/String;

    move-result-object v0

    .line 124
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 125
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Full jid not valid during setting active client account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbpx;->b:Lyj;

    .line 126
    invoke-virtual {v2}, Lyj;->Y()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 125
    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 131
    :cond_1
    iget-object v1, p0, Lbpx;->c:Lbnl;

    invoke-virtual {v1}, Lbnl;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 134
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "babel_ac_renew_cycle_seconds"

    const/16 v3, 0x12c

    .line 133
    invoke-static {v1, v2, v3}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 137
    iget-object v2, p0, Lbpx;->c:Lbnl;

    new-instance v3, Lbfg;

    invoke-direct {v3, v0, v1}, Lbfg;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2, v3}, Lbnl;->a(Lbea;)V

    goto :goto_0
.end method

.method protected a(J)V
    .locals 0

    .prologue
    .line 154
    iput-wide p1, p0, Lbpx;->d:J

    .line 155
    return-void
.end method

.method protected i()J
    .locals 2

    .prologue
    .line 146
    iget-wide v0, p0, Lbpx;->d:J

    return-wide v0
.end method
