.class public final Lehf;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lehf;


# instance fields
.field public A:[Lehj;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:[Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Lehh;

.field public g:Lehh;

.field public h:Lehh;

.field public i:[Ljava/lang/String;

.field public j:Ljava/lang/Boolean;

.field public k:Ljava/lang/Long;

.field public l:Ljava/lang/Integer;

.field public m:Ljava/lang/Integer;

.field public n:Ljava/lang/Integer;

.field public o:[Ljava/lang/Long;

.field public p:Ljava/lang/String;

.field public q:[Lehi;

.field public r:Ljava/lang/Boolean;

.field public s:[Ljava/lang/Integer;

.field public t:Ljava/lang/Integer;

.field public u:Lehg;

.field public v:Ljava/lang/String;

.field public w:Ljava/lang/Integer;

.field public x:Ljava/lang/Integer;

.field public y:Ljava/lang/Integer;

.field public z:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lehf;

    sput-object v0, Lehf;->a:[Lehf;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 379
    sget-object v0, Lept;->m:[Ljava/lang/Integer;

    iput-object v0, p0, Lehf;->d:[Ljava/lang/Integer;

    .line 384
    iput-object v1, p0, Lehf;->f:Lehh;

    .line 387
    iput-object v1, p0, Lehf;->g:Lehh;

    .line 390
    iput-object v1, p0, Lehf;->h:Lehh;

    .line 393
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Lehf;->i:[Ljava/lang/String;

    .line 404
    iput-object v1, p0, Lehf;->n:Ljava/lang/Integer;

    .line 407
    sget-object v0, Lept;->n:[Ljava/lang/Long;

    iput-object v0, p0, Lehf;->o:[Ljava/lang/Long;

    .line 412
    sget-object v0, Lehi;->a:[Lehi;

    iput-object v0, p0, Lehf;->q:[Lehi;

    .line 417
    sget-object v0, Lept;->m:[Ljava/lang/Integer;

    iput-object v0, p0, Lehf;->s:[Ljava/lang/Integer;

    .line 422
    iput-object v1, p0, Lehf;->u:Lehg;

    .line 435
    sget-object v0, Lehj;->a:[Lehj;

    iput-object v0, p0, Lehf;->A:[Lehj;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 541
    iget-object v0, p0, Lehf;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1f

    .line 542
    const/4 v0, 0x1

    iget-object v2, p0, Lehf;->b:Ljava/lang/Integer;

    .line 543
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 545
    :goto_0
    iget-object v2, p0, Lehf;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 546
    const/4 v2, 0x2

    iget-object v3, p0, Lehf;->c:Ljava/lang/Integer;

    .line 547
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 549
    :cond_0
    iget-object v2, p0, Lehf;->d:[Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lehf;->d:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 551
    iget-object v4, p0, Lehf;->d:[Ljava/lang/Integer;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 553
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Lepl;->f(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 551
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 555
    :cond_1
    add-int/2addr v0, v3

    .line 556
    iget-object v2, p0, Lehf;->d:[Ljava/lang/Integer;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 558
    :cond_2
    iget-object v2, p0, Lehf;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 559
    const/4 v2, 0x4

    iget-object v3, p0, Lehf;->e:Ljava/lang/Integer;

    .line 560
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 562
    :cond_3
    iget-object v2, p0, Lehf;->f:Lehh;

    if-eqz v2, :cond_4

    .line 563
    const/4 v2, 0x5

    iget-object v3, p0, Lehf;->f:Lehh;

    .line 564
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 566
    :cond_4
    iget-object v2, p0, Lehf;->g:Lehh;

    if-eqz v2, :cond_5

    .line 567
    const/4 v2, 0x6

    iget-object v3, p0, Lehf;->g:Lehh;

    .line 568
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 570
    :cond_5
    iget-object v2, p0, Lehf;->h:Lehh;

    if-eqz v2, :cond_6

    .line 571
    const/4 v2, 0x7

    iget-object v3, p0, Lehf;->h:Lehh;

    .line 572
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 574
    :cond_6
    iget-object v2, p0, Lehf;->i:[Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lehf;->i:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 576
    iget-object v4, p0, Lehf;->i:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_7

    aget-object v6, v4, v2

    .line 578
    invoke-static {v6}, Lepl;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 576
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 580
    :cond_7
    add-int/2addr v0, v3

    .line 581
    iget-object v2, p0, Lehf;->i:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 583
    :cond_8
    iget-object v2, p0, Lehf;->j:Ljava/lang/Boolean;

    if-eqz v2, :cond_9

    .line 584
    const/16 v2, 0x9

    iget-object v3, p0, Lehf;->j:Ljava/lang/Boolean;

    .line 585
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 587
    :cond_9
    iget-object v2, p0, Lehf;->k:Ljava/lang/Long;

    if-eqz v2, :cond_a

    .line 588
    const/16 v2, 0xa

    iget-object v3, p0, Lehf;->k:Ljava/lang/Long;

    .line 589
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 591
    :cond_a
    iget-object v2, p0, Lehf;->l:Ljava/lang/Integer;

    if-eqz v2, :cond_b

    .line 592
    const/16 v2, 0xb

    iget-object v3, p0, Lehf;->l:Ljava/lang/Integer;

    .line 593
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 595
    :cond_b
    iget-object v2, p0, Lehf;->m:Ljava/lang/Integer;

    if-eqz v2, :cond_c

    .line 596
    const/16 v2, 0xc

    iget-object v3, p0, Lehf;->m:Ljava/lang/Integer;

    .line 597
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 599
    :cond_c
    iget-object v2, p0, Lehf;->n:Ljava/lang/Integer;

    if-eqz v2, :cond_d

    .line 600
    const/16 v2, 0xd

    iget-object v3, p0, Lehf;->n:Ljava/lang/Integer;

    .line 601
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 603
    :cond_d
    iget-object v2, p0, Lehf;->o:[Ljava/lang/Long;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lehf;->o:[Ljava/lang/Long;

    array-length v2, v2

    if-lez v2, :cond_f

    .line 605
    iget-object v4, p0, Lehf;->o:[Ljava/lang/Long;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_3
    if-ge v2, v5, :cond_e

    aget-object v6, v4, v2

    .line 607
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Lepl;->c(J)I

    move-result v6

    add-int/2addr v3, v6

    .line 605
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 609
    :cond_e
    add-int/2addr v0, v3

    .line 610
    iget-object v2, p0, Lehf;->o:[Ljava/lang/Long;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 612
    :cond_f
    iget-object v2, p0, Lehf;->p:Ljava/lang/String;

    if-eqz v2, :cond_10

    .line 613
    const/16 v2, 0xf

    iget-object v3, p0, Lehf;->p:Ljava/lang/String;

    .line 614
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 616
    :cond_10
    iget-object v2, p0, Lehf;->q:[Lehi;

    if-eqz v2, :cond_12

    .line 617
    iget-object v3, p0, Lehf;->q:[Lehi;

    array-length v4, v3

    move v2, v1

    :goto_4
    if-ge v2, v4, :cond_12

    aget-object v5, v3, v2

    .line 618
    if-eqz v5, :cond_11

    .line 619
    const/16 v6, 0x10

    .line 620
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 617
    :cond_11
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 624
    :cond_12
    iget-object v2, p0, Lehf;->r:Ljava/lang/Boolean;

    if-eqz v2, :cond_13

    .line 625
    const/16 v2, 0x11

    iget-object v3, p0, Lehf;->r:Ljava/lang/Boolean;

    .line 626
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 628
    :cond_13
    iget-object v2, p0, Lehf;->s:[Ljava/lang/Integer;

    if-eqz v2, :cond_15

    iget-object v2, p0, Lehf;->s:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_15

    .line 630
    iget-object v4, p0, Lehf;->s:[Ljava/lang/Integer;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_5
    if-ge v2, v5, :cond_14

    aget-object v6, v4, v2

    .line 632
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Lepl;->f(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 630
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 634
    :cond_14
    add-int/2addr v0, v3

    .line 635
    iget-object v2, p0, Lehf;->s:[Ljava/lang/Integer;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 637
    :cond_15
    iget-object v2, p0, Lehf;->t:Ljava/lang/Integer;

    if-eqz v2, :cond_16

    .line 638
    const/16 v2, 0x13

    iget-object v3, p0, Lehf;->t:Ljava/lang/Integer;

    .line 639
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 641
    :cond_16
    iget-object v2, p0, Lehf;->u:Lehg;

    if-eqz v2, :cond_17

    .line 642
    const/16 v2, 0x14

    iget-object v3, p0, Lehf;->u:Lehg;

    .line 643
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 645
    :cond_17
    iget-object v2, p0, Lehf;->v:Ljava/lang/String;

    if-eqz v2, :cond_18

    .line 646
    const/16 v2, 0x15

    iget-object v3, p0, Lehf;->v:Ljava/lang/String;

    .line 647
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 649
    :cond_18
    iget-object v2, p0, Lehf;->w:Ljava/lang/Integer;

    if-eqz v2, :cond_19

    .line 650
    const/16 v2, 0x16

    iget-object v3, p0, Lehf;->w:Ljava/lang/Integer;

    .line 651
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 653
    :cond_19
    iget-object v2, p0, Lehf;->x:Ljava/lang/Integer;

    if-eqz v2, :cond_1a

    .line 654
    const/16 v2, 0x17

    iget-object v3, p0, Lehf;->x:Ljava/lang/Integer;

    .line 655
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 657
    :cond_1a
    iget-object v2, p0, Lehf;->y:Ljava/lang/Integer;

    if-eqz v2, :cond_1b

    .line 658
    const/16 v2, 0x18

    iget-object v3, p0, Lehf;->y:Ljava/lang/Integer;

    .line 659
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 661
    :cond_1b
    iget-object v2, p0, Lehf;->z:Ljava/lang/Integer;

    if-eqz v2, :cond_1c

    .line 662
    const/16 v2, 0x19

    iget-object v3, p0, Lehf;->z:Ljava/lang/Integer;

    .line 663
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 665
    :cond_1c
    iget-object v2, p0, Lehf;->A:[Lehj;

    if-eqz v2, :cond_1e

    .line 666
    iget-object v2, p0, Lehf;->A:[Lehj;

    array-length v3, v2

    :goto_6
    if-ge v1, v3, :cond_1e

    aget-object v4, v2, v1

    .line 667
    if-eqz v4, :cond_1d

    .line 668
    const/16 v5, 0x1a

    .line 669
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 666
    :cond_1d
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 673
    :cond_1e
    iget-object v1, p0, Lehf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 674
    iput v0, p0, Lehf;->cachedSize:I

    .line 675
    return v0

    :cond_1f
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lehf;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lehf;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lehf;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->h()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehf;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehf;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lehf;->d:[Ljava/lang/Integer;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Integer;

    iget-object v3, p0, Lehf;->d:[Ljava/lang/Integer;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lehf;->d:[Ljava/lang/Integer;

    :goto_1
    iget-object v2, p0, Lehf;->d:[Ljava/lang/Integer;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lehf;->d:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lehf;->d:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehf;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lehf;->f:Lehh;

    if-nez v0, :cond_3

    new-instance v0, Lehh;

    invoke-direct {v0}, Lehh;-><init>()V

    iput-object v0, p0, Lehf;->f:Lehh;

    :cond_3
    iget-object v0, p0, Lehf;->f:Lehh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lehf;->g:Lehh;

    if-nez v0, :cond_4

    new-instance v0, Lehh;

    invoke-direct {v0}, Lehh;-><init>()V

    iput-object v0, p0, Lehf;->g:Lehh;

    :cond_4
    iget-object v0, p0, Lehf;->g:Lehh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lehf;->h:Lehh;

    if-nez v0, :cond_5

    new-instance v0, Lehh;

    invoke-direct {v0}, Lehh;-><init>()V

    iput-object v0, p0, Lehf;->h:Lehh;

    :cond_5
    iget-object v0, p0, Lehf;->h:Lehh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lehf;->i:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lehf;->i:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lehf;->i:[Ljava/lang/String;

    :goto_2
    iget-object v2, p0, Lehf;->i:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Lehf;->i:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lehf;->i:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lehf;->j:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lehf;->k:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehf;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehf;->m:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v5, :cond_7

    const/4 v2, 0x2

    if-ne v0, v2, :cond_8

    :cond_7
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehf;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_8
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehf;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_e
    const/16 v0, 0x70

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lehf;->o:[Ljava/lang/Long;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Long;

    iget-object v3, p0, Lehf;->o:[Ljava/lang/Long;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lehf;->o:[Ljava/lang/Long;

    :goto_3
    iget-object v2, p0, Lehf;->o:[Ljava/lang/Long;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Lehf;->o:[Ljava/lang/Long;

    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_9
    iget-object v2, p0, Lehf;->o:[Ljava/lang/Long;

    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehf;->p:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_10
    const/16 v0, 0x82

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lehf;->q:[Lehi;

    if-nez v0, :cond_b

    move v0, v1

    :goto_4
    add-int/2addr v2, v0

    new-array v2, v2, [Lehi;

    iget-object v3, p0, Lehf;->q:[Lehi;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lehf;->q:[Lehi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    iput-object v2, p0, Lehf;->q:[Lehi;

    :goto_5
    iget-object v2, p0, Lehf;->q:[Lehi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_c

    iget-object v2, p0, Lehf;->q:[Lehi;

    new-instance v3, Lehi;

    invoke-direct {v3}, Lehi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lehf;->q:[Lehi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_b
    iget-object v0, p0, Lehf;->q:[Lehi;

    array-length v0, v0

    goto :goto_4

    :cond_c
    iget-object v2, p0, Lehf;->q:[Lehi;

    new-instance v3, Lehi;

    invoke-direct {v3}, Lehi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lehf;->q:[Lehi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lehf;->r:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_12
    const/16 v0, 0x90

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lehf;->s:[Ljava/lang/Integer;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Integer;

    iget-object v3, p0, Lehf;->s:[Ljava/lang/Integer;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lehf;->s:[Ljava/lang/Integer;

    :goto_6
    iget-object v2, p0, Lehf;->s:[Ljava/lang/Integer;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    iget-object v2, p0, Lehf;->s:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_d
    iget-object v2, p0, Lehf;->s:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehf;->t:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_14
    iget-object v0, p0, Lehf;->u:Lehg;

    if-nez v0, :cond_e

    new-instance v0, Lehg;

    invoke-direct {v0}, Lehg;-><init>()V

    iput-object v0, p0, Lehf;->u:Lehg;

    :cond_e
    iget-object v0, p0, Lehf;->u:Lehg;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehf;->v:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_16
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehf;->w:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_17
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehf;->x:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehf;->y:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_19
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehf;->z:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_1a
    const/16 v0, 0xd2

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lehf;->A:[Lehj;

    if-nez v0, :cond_10

    move v0, v1

    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Lehj;

    iget-object v3, p0, Lehf;->A:[Lehj;

    if-eqz v3, :cond_f

    iget-object v3, p0, Lehf;->A:[Lehj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_f
    iput-object v2, p0, Lehf;->A:[Lehj;

    :goto_8
    iget-object v2, p0, Lehf;->A:[Lehj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_11

    iget-object v2, p0, Lehf;->A:[Lehj;

    new-instance v3, Lehj;

    invoke-direct {v3}, Lehj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lehf;->A:[Lehj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_10
    iget-object v0, p0, Lehf;->A:[Lehj;

    array-length v0, v0

    goto :goto_7

    :cond_11
    iget-object v2, p0, Lehf;->A:[Lehj;

    new-instance v3, Lehj;

    invoke-direct {v3}, Lehj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lehf;->A:[Lehj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x88 -> :sswitch_11
        0x90 -> :sswitch_12
        0x98 -> :sswitch_13
        0xa2 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb0 -> :sswitch_16
        0xb8 -> :sswitch_17
        0xc0 -> :sswitch_18
        0xc8 -> :sswitch_19
        0xd2 -> :sswitch_1a
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 440
    iget-object v1, p0, Lehf;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 441
    const/4 v1, 0x1

    iget-object v2, p0, Lehf;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->b(II)V

    .line 443
    :cond_0
    iget-object v1, p0, Lehf;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 444
    const/4 v1, 0x2

    iget-object v2, p0, Lehf;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 446
    :cond_1
    iget-object v1, p0, Lehf;->d:[Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 447
    iget-object v2, p0, Lehf;->d:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 448
    const/4 v5, 0x3

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v5, v4}, Lepl;->a(II)V

    .line 447
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 451
    :cond_2
    iget-object v1, p0, Lehf;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 452
    const/4 v1, 0x4

    iget-object v2, p0, Lehf;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 454
    :cond_3
    iget-object v1, p0, Lehf;->f:Lehh;

    if-eqz v1, :cond_4

    .line 455
    const/4 v1, 0x5

    iget-object v2, p0, Lehf;->f:Lehh;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 457
    :cond_4
    iget-object v1, p0, Lehf;->g:Lehh;

    if-eqz v1, :cond_5

    .line 458
    const/4 v1, 0x6

    iget-object v2, p0, Lehf;->g:Lehh;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 460
    :cond_5
    iget-object v1, p0, Lehf;->h:Lehh;

    if-eqz v1, :cond_6

    .line 461
    const/4 v1, 0x7

    iget-object v2, p0, Lehf;->h:Lehh;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 463
    :cond_6
    iget-object v1, p0, Lehf;->i:[Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 464
    iget-object v2, p0, Lehf;->i:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 465
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 464
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 468
    :cond_7
    iget-object v1, p0, Lehf;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 469
    const/16 v1, 0x9

    iget-object v2, p0, Lehf;->j:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 471
    :cond_8
    iget-object v1, p0, Lehf;->k:Ljava/lang/Long;

    if-eqz v1, :cond_9

    .line 472
    const/16 v1, 0xa

    iget-object v2, p0, Lehf;->k:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->b(IJ)V

    .line 474
    :cond_9
    iget-object v1, p0, Lehf;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    .line 475
    const/16 v1, 0xb

    iget-object v2, p0, Lehf;->l:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 477
    :cond_a
    iget-object v1, p0, Lehf;->m:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 478
    const/16 v1, 0xc

    iget-object v2, p0, Lehf;->m:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 480
    :cond_b
    iget-object v1, p0, Lehf;->n:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 481
    const/16 v1, 0xd

    iget-object v2, p0, Lehf;->n:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 483
    :cond_c
    iget-object v1, p0, Lehf;->o:[Ljava/lang/Long;

    if-eqz v1, :cond_d

    .line 484
    iget-object v2, p0, Lehf;->o:[Ljava/lang/Long;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_d

    aget-object v4, v2, v1

    .line 485
    const/16 v5, 0xe

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, v5, v6, v7}, Lepl;->b(IJ)V

    .line 484
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 488
    :cond_d
    iget-object v1, p0, Lehf;->p:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 489
    const/16 v1, 0xf

    iget-object v2, p0, Lehf;->p:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 491
    :cond_e
    iget-object v1, p0, Lehf;->q:[Lehi;

    if-eqz v1, :cond_10

    .line 492
    iget-object v2, p0, Lehf;->q:[Lehi;

    array-length v3, v2

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_10

    aget-object v4, v2, v1

    .line 493
    if-eqz v4, :cond_f

    .line 494
    const/16 v5, 0x10

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 492
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 498
    :cond_10
    iget-object v1, p0, Lehf;->r:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    .line 499
    const/16 v1, 0x11

    iget-object v2, p0, Lehf;->r:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 501
    :cond_11
    iget-object v1, p0, Lehf;->s:[Ljava/lang/Integer;

    if-eqz v1, :cond_12

    .line 502
    iget-object v2, p0, Lehf;->s:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_4
    if-ge v1, v3, :cond_12

    aget-object v4, v2, v1

    .line 503
    const/16 v5, 0x12

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v5, v4}, Lepl;->a(II)V

    .line 502
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 506
    :cond_12
    iget-object v1, p0, Lehf;->t:Ljava/lang/Integer;

    if-eqz v1, :cond_13

    .line 507
    const/16 v1, 0x13

    iget-object v2, p0, Lehf;->t:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 509
    :cond_13
    iget-object v1, p0, Lehf;->u:Lehg;

    if-eqz v1, :cond_14

    .line 510
    const/16 v1, 0x14

    iget-object v2, p0, Lehf;->u:Lehg;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 512
    :cond_14
    iget-object v1, p0, Lehf;->v:Ljava/lang/String;

    if-eqz v1, :cond_15

    .line 513
    const/16 v1, 0x15

    iget-object v2, p0, Lehf;->v:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 515
    :cond_15
    iget-object v1, p0, Lehf;->w:Ljava/lang/Integer;

    if-eqz v1, :cond_16

    .line 516
    const/16 v1, 0x16

    iget-object v2, p0, Lehf;->w:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 518
    :cond_16
    iget-object v1, p0, Lehf;->x:Ljava/lang/Integer;

    if-eqz v1, :cond_17

    .line 519
    const/16 v1, 0x17

    iget-object v2, p0, Lehf;->x:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 521
    :cond_17
    iget-object v1, p0, Lehf;->y:Ljava/lang/Integer;

    if-eqz v1, :cond_18

    .line 522
    const/16 v1, 0x18

    iget-object v2, p0, Lehf;->y:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 524
    :cond_18
    iget-object v1, p0, Lehf;->z:Ljava/lang/Integer;

    if-eqz v1, :cond_19

    .line 525
    const/16 v1, 0x19

    iget-object v2, p0, Lehf;->z:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 527
    :cond_19
    iget-object v1, p0, Lehf;->A:[Lehj;

    if-eqz v1, :cond_1b

    .line 528
    iget-object v1, p0, Lehf;->A:[Lehj;

    array-length v2, v1

    :goto_5
    if-ge v0, v2, :cond_1b

    aget-object v3, v1, v0

    .line 529
    if-eqz v3, :cond_1a

    .line 530
    const/16 v4, 0x1a

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 528
    :cond_1a
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 534
    :cond_1b
    iget-object v0, p0, Lehf;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 536
    return-void
.end method
