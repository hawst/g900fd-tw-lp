.class public final Ldfx;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldfx;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ldfy;

.field public d:Ljava/lang/Integer;

.field public e:Ldfw;

.field public f:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5216
    const/4 v0, 0x0

    new-array v0, v0, [Ldfx;

    sput-object v0, Ldfx;->a:[Ldfx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5217
    invoke-direct {p0}, Lepn;-><init>()V

    .line 5222
    iput-object v0, p0, Ldfx;->c:Ldfy;

    .line 5225
    iput-object v0, p0, Ldfx;->d:Ljava/lang/Integer;

    .line 5228
    iput-object v0, p0, Ldfx;->e:Ldfw;

    .line 5217
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 5256
    const/4 v0, 0x0

    .line 5257
    iget-object v1, p0, Ldfx;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5258
    const/4 v0, 0x1

    iget-object v1, p0, Ldfx;->b:Ljava/lang/String;

    .line 5259
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5261
    :cond_0
    iget-object v1, p0, Ldfx;->c:Ldfy;

    if-eqz v1, :cond_1

    .line 5262
    const/4 v1, 0x2

    iget-object v2, p0, Ldfx;->c:Ldfy;

    .line 5263
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5265
    :cond_1
    iget-object v1, p0, Ldfx;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 5266
    const/4 v1, 0x3

    iget-object v2, p0, Ldfx;->d:Ljava/lang/Integer;

    .line 5267
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5269
    :cond_2
    iget-object v1, p0, Ldfx;->e:Ldfw;

    if-eqz v1, :cond_3

    .line 5270
    const/4 v1, 0x4

    iget-object v2, p0, Ldfx;->e:Ldfw;

    .line 5271
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5273
    :cond_3
    iget-object v1, p0, Ldfx;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 5274
    const/4 v1, 0x5

    iget-object v2, p0, Ldfx;->f:Ljava/lang/Integer;

    .line 5275
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5277
    :cond_4
    iget-object v1, p0, Ldfx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5278
    iput v0, p0, Ldfx;->cachedSize:I

    .line 5279
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 5213
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldfx;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldfx;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldfx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldfx;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldfx;->c:Ldfy;

    if-nez v0, :cond_2

    new-instance v0, Ldfy;

    invoke-direct {v0}, Ldfy;-><init>()V

    iput-object v0, p0, Ldfx;->c:Ldfy;

    :cond_2
    iget-object v0, p0, Ldfx;->c:Ldfy;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldfx;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldfx;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldfx;->e:Ldfw;

    if-nez v0, :cond_5

    new-instance v0, Ldfw;

    invoke-direct {v0}, Ldfw;-><init>()V

    iput-object v0, p0, Ldfx;->e:Ldfw;

    :cond_5
    iget-object v0, p0, Ldfx;->e:Ldfw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldfx;->f:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 5235
    iget-object v0, p0, Ldfx;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 5236
    const/4 v0, 0x1

    iget-object v1, p0, Ldfx;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 5238
    :cond_0
    iget-object v0, p0, Ldfx;->c:Ldfy;

    if-eqz v0, :cond_1

    .line 5239
    const/4 v0, 0x2

    iget-object v1, p0, Ldfx;->c:Ldfy;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5241
    :cond_1
    iget-object v0, p0, Ldfx;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 5242
    const/4 v0, 0x3

    iget-object v1, p0, Ldfx;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 5244
    :cond_2
    iget-object v0, p0, Ldfx;->e:Ldfw;

    if-eqz v0, :cond_3

    .line 5245
    const/4 v0, 0x4

    iget-object v1, p0, Ldfx;->e:Ldfw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5247
    :cond_3
    iget-object v0, p0, Ldfx;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 5248
    const/4 v0, 0x5

    iget-object v1, p0, Ldfx;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 5250
    :cond_4
    iget-object v0, p0, Ldfx;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 5252
    return-void
.end method
