.class public Lqj;
.super Lt;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Law;
.implements Lpp;
.implements Lpq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lt;",
        "Landroid/view/View$OnClickListener;",
        "Law",
        "<",
        "Lqn;",
        ">;",
        "Lpp;",
        "Lpq;"
    }
.end annotation


# instance fields
.field protected Y:Landroid/widget/ImageView;

.field protected Z:Lra;

.field public a:Ljava/lang/String;

.field protected aa:I

.field protected ab:Z

.field protected ac:Z

.field protected ad:Z

.field protected ae:Z

.field protected af:Landroid/view/View;

.field protected ag:Z

.field protected ah:Z

.field protected ai:Z

.field protected b:Ljava/lang/String;

.field public c:Landroid/content/Intent;

.field protected d:Lpo;

.field protected e:Lqi;

.field protected f:Landroid/content/BroadcastReceiver;

.field protected g:Lcom/android/ex/photo/views/PhotoView;

.field protected h:Landroid/widget/ImageView;

.field protected i:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 134
    invoke-direct {p0}, Lt;-><init>()V

    .line 122
    const/4 v0, 0x1

    iput-boolean v0, p0, Lqj;->ae:Z

    .line 136
    return-void
.end method

.method public static a(Landroid/content/Intent;IZ)Lqj;
    .locals 3

    .prologue
    .line 146
    new-instance v0, Lqj;

    invoke-direct {v0}, Lqj;-><init>()V

    .line 147
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "arg-intent"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "arg-position"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "arg-show-spinner"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Lqj;->setArguments(Landroid/os/Bundle;)V

    .line 148
    return-object v0
.end method

.method private v()V
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Lqj;->d:Lpo;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 519
    :goto_0
    invoke-virtual {p0, v0}, Lqj;->b(Z)V

    .line 520
    return-void

    .line 518
    :cond_0
    iget-object v0, p0, Lqj;->d:Lpo;

    invoke-interface {v0, p0}, Lpo;->b(Lt;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 455
    invoke-direct {p0}, Lqj;->v()V

    .line 456
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 531
    iget-object v0, p0, Lqj;->e:Lqi;

    if-nez v0, :cond_1

    .line 566
    :cond_0
    :goto_0
    return-void

    .line 541
    :cond_1
    iget v0, p0, Lqj;->aa:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lqj;->t()Z

    move-result v0

    if-nez v0, :cond_0

    .line 542
    iget-object v0, p0, Lqj;->d:Lpo;

    .line 544
    invoke-virtual {p0}, Lqj;->getLoaderManager()Lav;

    move-result-object v1

    .line 546
    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Lav;->b(I)Ldg;

    move-result-object v0

    .line 548
    if-eqz v0, :cond_2

    .line 549
    check-cast v0, Lqm;

    .line 550
    iget-object v2, p0, Lqj;->e:Lqi;

    invoke-virtual {v2, p1}, Lqi;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lqj;->a:Ljava/lang/String;

    .line 551
    iget-object v2, p0, Lqj;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Lqm;->a(Ljava/lang/String;)V

    .line 552
    invoke-interface {v0}, Lqm;->q()V

    .line 555
    :cond_2
    iget-boolean v0, p0, Lqj;->ag:Z

    if-nez v0, :cond_0

    .line 556
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Lav;->b(I)Ldg;

    move-result-object v0

    .line 558
    if-eqz v0, :cond_0

    .line 559
    check-cast v0, Lqm;

    .line 560
    iget-object v1, p0, Lqj;->e:Lqi;

    invoke-virtual {v1, p1}, Lqi;->c(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lqj;->b:Ljava/lang/String;

    .line 561
    iget-object v1, p0, Lqj;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lqm;->a(Ljava/lang/String;)V

    .line 562
    invoke-interface {v0}, Lqm;->q()V

    goto :goto_0
.end method

.method protected a(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 225
    sget v0, Lf;->aM:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/ex/photo/views/PhotoView;

    iput-object v0, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    .line 226
    iget-object v0, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    iget-object v1, p0, Lqj;->c:Landroid/content/Intent;

    const-string v2, "max_scale"

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/ex/photo/views/PhotoView;->a(F)V

    .line 227
    iget-object v0, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v0, p0}, Lcom/android/ex/photo/views/PhotoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 228
    iget-object v0, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    iget-boolean v1, p0, Lqj;->ab:Z

    invoke-virtual {v0, v1}, Lcom/android/ex/photo/views/PhotoView;->a(Z)V

    .line 229
    iget-object v0, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v0, v4}, Lcom/android/ex/photo/views/PhotoView;->b(Z)V

    .line 231
    sget v0, Lf;->aK:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lqj;->af:Landroid/view/View;

    .line 232
    sget v0, Lf;->aL:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lqj;->h:Landroid/widget/ImageView;

    .line 233
    iput-boolean v4, p0, Lqj;->ag:Z

    .line 234
    sget v0, Lf;->aG:I

    .line 235
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 236
    sget v1, Lf;->aE:I

    .line 237
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .line 238
    new-instance v2, Lra;

    invoke-direct {v2, v1, v0}, Lra;-><init>(Landroid/widget/ProgressBar;Landroid/widget/ProgressBar;)V

    iput-object v2, p0, Lqj;->Z:Lra;

    .line 239
    sget v0, Lf;->aF:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lqj;->i:Landroid/widget/TextView;

    .line 240
    sget v0, Lf;->aO:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lqj;->Y:Landroid/widget/ImageView;

    .line 243
    invoke-direct {p0}, Lqj;->v()V

    .line 244
    return-void
.end method

.method public a(Ldg;Lqn;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Lqn;",
            ">;",
            "Lqn;",
            ")V"
        }
    .end annotation

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 342
    invoke-virtual {p0}, Lqj;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lqj;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 393
    :cond_0
    :goto_0
    return-void

    .line 346
    :cond_1
    invoke-virtual {p0}, Lqj;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p2, v0}, Lqn;->a(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 348
    invoke-virtual {p1}, Ldg;->l()I

    move-result v1

    .line 349
    packed-switch v1, :pswitch_data_0

    .line 379
    :goto_1
    iget-boolean v1, p0, Lqj;->ae:Z

    if-nez v1, :cond_2

    .line 386
    iget-object v1, p0, Lqj;->Z:Lra;

    invoke-virtual {v1, v5}, Lra;->a(I)V

    .line 389
    :cond_2
    if-eqz v0, :cond_3

    .line 390
    iget-object v0, p0, Lqj;->d:Lpo;

    iget v0, p0, Lqj;->aa:I

    .line 392
    :cond_3
    invoke-direct {p0}, Lqj;->v()V

    goto :goto_0

    .line 351
    :pswitch_0
    iget-boolean v1, p0, Lqj;->ai:Z

    if-nez v1, :cond_6

    .line 352
    invoke-virtual {p0}, Lqj;->t()Z

    move-result v1

    if-nez v1, :cond_0

    .line 360
    if-nez v0, :cond_5

    .line 362
    iget-object v1, p0, Lqj;->h:Landroid/widget/ImageView;

    sget v2, Lf;->aD:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 363
    iput-boolean v3, p0, Lqj;->ag:Z

    .line 369
    :goto_2
    iget-object v1, p0, Lqj;->h:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 370
    invoke-virtual {p0}, Lqj;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->ax:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 371
    iget-object v1, p0, Lqj;->h:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 373
    :cond_4
    invoke-virtual {p0, v3}, Lqj;->a(Z)V

    goto :goto_1

    .line 366
    :cond_5
    iget-object v1, p0, Lqj;->h:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 367
    iput-boolean v4, p0, Lqj;->ag:Z

    goto :goto_2

    .line 378
    :cond_6
    :pswitch_1
    iget v1, p2, Lqn;->c:I

    if-ne v1, v4, :cond_7

    iput-boolean v3, p0, Lqj;->ae:Z

    iget-object v1, p0, Lqj;->i:Landroid/widget/TextView;

    sget v2, Lf;->aS:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lqj;->i:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lqj;->d:Lpo;

    invoke-interface {v1, p0, v3}, Lpo;->a(Lqj;Z)V

    goto :goto_1

    :cond_7
    invoke-virtual {p0}, Lqj;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p2, v1}, Lqn;->a(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_9

    iget-object v2, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v2, v1}, Lcom/android/ex/photo/views/PhotoView;->a(Landroid/graphics/drawable/Drawable;)V

    :cond_8
    invoke-virtual {p0, v4}, Lqj;->a(Z)V

    iget-object v1, p0, Lqj;->af:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v3, p0, Lqj;->ae:Z

    :cond_9
    iget-object v1, p0, Lqj;->d:Lpo;

    invoke-interface {v1, p0, v4}, Lpo;->a(Lqj;Z)V

    goto/16 :goto_1

    .line 349
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 431
    iget-object v0, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v0, p1}, Lcom/android/ex/photo/views/PhotoView;->b(Z)V

    .line 432
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 465
    iget-object v0, p0, Lqj;->d:Lpo;

    invoke-interface {v0, p0}, Lpo;->a(Lt;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 467
    invoke-virtual {p0}, Lqj;->s()V

    .line 476
    :goto_0
    return-void

    .line 469
    :cond_0
    invoke-virtual {p0}, Lqj;->t()Z

    move-result v0

    if-nez v0, :cond_1

    .line 471
    invoke-virtual {p0}, Lqj;->getLoaderManager()Lav;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lav;->b(ILandroid/os/Bundle;Law;)Ldg;

    .line 474
    :cond_1
    iget-object v0, p0, Lqj;->d:Lpo;

    goto :goto_0
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 526
    iput-boolean p1, p0, Lqj;->ab:Z

    .line 527
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 460
    invoke-virtual {p0}, Lqj;->s()V

    .line 461
    return-void
.end method

.method public d()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 489
    iget-object v1, p0, Lqj;->d:Lpo;

    invoke-interface {v1, p0}, Lpo;->a(Lt;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 494
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v1}, Lcom/android/ex/photo/views/PhotoView;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public e()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 499
    iget-object v1, p0, Lqj;->d:Lpo;

    invoke-interface {v1, p0}, Lpo;->a(Lt;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 504
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v1}, Lcom/android/ex/photo/views/PhotoView;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected f()Lpo;
    .locals 1

    .prologue
    .line 177
    invoke-virtual {p0}, Lqj;->getActivity()Ly;

    move-result-object v0

    check-cast v0, Lqa;

    invoke-interface {v0}, Lqa;->k()Lpr;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 162
    invoke-super {p0, p1}, Lt;->onActivityCreated(Landroid/os/Bundle;)V

    .line 163
    invoke-virtual {p0}, Lqj;->f()Lpo;

    move-result-object v0

    iput-object v0, p0, Lqj;->d:Lpo;

    .line 164
    iget-object v0, p0, Lqj;->d:Lpo;

    if-nez v0, :cond_0

    .line 165
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Activity must be a derived class of PhotoViewActivity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :cond_0
    iget-object v0, p0, Lqj;->d:Lpo;

    invoke-interface {v0}, Lpo;->b()Lqi;

    move-result-object v0

    iput-object v0, p0, Lqj;->e:Lqi;

    .line 169
    iget-object v0, p0, Lqj;->e:Lqi;

    if-nez v0, :cond_1

    .line 170
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Callback reported null adapter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 173
    :cond_1
    invoke-direct {p0}, Lqj;->v()V

    .line 174
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lqj;->d:Lpo;

    invoke-interface {v0}, Lpo;->a()V

    .line 451
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 188
    invoke-super {p0, p1}, Lt;->onCreate(Landroid/os/Bundle;)V

    .line 190
    invoke-virtual {p0}, Lqj;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 191
    if-nez v1, :cond_1

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    const-string v0, "arg-intent"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lqj;->c:Landroid/content/Intent;

    .line 195
    iget-object v0, p0, Lqj;->c:Landroid/content/Intent;

    const-string v2, "display_thumbs_fullscreen"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lqj;->ai:Z

    .line 198
    const-string v0, "arg-position"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lqj;->aa:I

    .line 199
    const-string v0, "arg-show-spinner"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lqj;->ad:Z

    .line 200
    const/4 v0, 0x1

    iput-boolean v0, p0, Lqj;->ae:Z

    .line 202
    if-eqz p1, :cond_2

    .line 203
    const-string v0, "com.android.mail.photo.fragments.PhotoViewFragment.INTENT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 204
    if-eqz v0, :cond_2

    .line 205
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lqj;->c:Landroid/content/Intent;

    .line 209
    :cond_2
    iget-object v0, p0, Lqj;->c:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lqj;->c:Landroid/content/Intent;

    const-string v1, "resolved_photo_uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lqj;->a:Ljava/lang/String;

    .line 211
    iget-object v0, p0, Lqj;->c:Landroid/content/Intent;

    const-string v1, "thumbnail_uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lqj;->b:Ljava/lang/String;

    .line 212
    iget-object v0, p0, Lqj;->c:Landroid/content/Intent;

    const-string v1, "watch_network"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lqj;->ac:Z

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Ldg;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldg",
            "<",
            "Lqn;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 322
    iget-boolean v1, p0, Lqj;->ad:Z

    if-eqz v1, :cond_0

    .line 334
    :goto_0
    return-object v0

    .line 326
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 334
    :goto_1
    iget-object v1, p0, Lqj;->d:Lpo;

    invoke-interface {v1, p1, v0}, Lpo;->a(ILjava/lang/String;)Ldg;

    move-result-object v0

    goto :goto_0

    .line 328
    :pswitch_0
    iget-object v0, p0, Lqj;->b:Ljava/lang/String;

    goto :goto_1

    .line 331
    :pswitch_1
    iget-object v0, p0, Lqj;->a:Ljava/lang/String;

    goto :goto_1

    .line 326
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 219
    sget v0, Lf;->aR:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 220
    invoke-virtual {p0, v0}, Lqj;->a(Landroid/view/View;)V

    .line 221
    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v0}, Lcom/android/ex/photo/views/PhotoView;->c()V

    .line 302
    const/4 v0, 0x0

    iput-object v0, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    .line 304
    :cond_0
    invoke-super {p0}, Lt;->onDestroyView()V

    .line 305
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x0

    iput-object v0, p0, Lqj;->d:Lpo;

    .line 183
    invoke-super {p0}, Lt;->onDetach()V

    .line 184
    return-void
.end method

.method public synthetic onLoadFinished(Ldg;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 55
    check-cast p2, Lqn;

    invoke-virtual {p0, p1, p2}, Lqj;->a(Ldg;Lqn;)V

    return-void
.end method

.method public onLoaderReset(Ldg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Lqn;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 446
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 288
    iget-boolean v0, p0, Lqj;->ac:Z

    if-eqz v0, :cond_0

    .line 289
    invoke-virtual {p0}, Lqj;->getActivity()Ly;

    move-result-object v0

    iget-object v1, p0, Lqj;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Ly;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 291
    :cond_0
    iget-object v0, p0, Lqj;->d:Lpo;

    invoke-interface {v0, p0}, Lpo;->b(Lpp;)V

    .line 292
    iget-object v0, p0, Lqj;->d:Lpo;

    iget v1, p0, Lqj;->aa:I

    invoke-interface {v0, v1}, Lpo;->b(I)V

    .line 293
    iget-object v0, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v0}, Lcom/android/ex/photo/views/PhotoView;->d()V

    .line 294
    :cond_1
    invoke-super {p0}, Lt;->onPause()V

    .line 295
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 248
    invoke-super {p0}, Lt;->onResume()V

    .line 249
    iget-object v0, p0, Lqj;->d:Lpo;

    iget v1, p0, Lqj;->aa:I

    invoke-interface {v0, v1, p0}, Lpo;->a(ILpq;)V

    .line 250
    iget-object v0, p0, Lqj;->d:Lpo;

    invoke-interface {v0, p0}, Lpo;->a(Lpp;)V

    .line 252
    iget-boolean v0, p0, Lqj;->ac:Z

    if-eqz v0, :cond_1

    .line 253
    iget-object v0, p0, Lqj;->f:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_0

    .line 254
    new-instance v0, Lqk;

    invoke-direct {v0, p0, v4}, Lqk;-><init>(Lqj;B)V

    iput-object v0, p0, Lqj;->f:Landroid/content/BroadcastReceiver;

    .line 256
    :cond_0
    invoke-virtual {p0}, Lqj;->getActivity()Ly;

    move-result-object v0

    iget-object v1, p0, Lqj;->f:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ly;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 259
    invoke-virtual {p0}, Lqj;->getActivity()Ly;

    move-result-object v0

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Ly;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 260
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 261
    if-eqz v0, :cond_3

    .line 262
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    iput-boolean v0, p0, Lqj;->ah:Z

    .line 270
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lqj;->t()Z

    move-result v0

    if-nez v0, :cond_2

    .line 271
    const/4 v0, 0x1

    iput-boolean v0, p0, Lqj;->ae:Z

    .line 272
    iget-object v0, p0, Lqj;->af:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 274
    invoke-virtual {p0}, Lqj;->getLoaderManager()Lav;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5, p0}, Lav;->a(ILandroid/os/Bundle;Law;)Ldg;

    .line 280
    invoke-virtual {p0}, Lqj;->getLoaderManager()Lav;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v5, p0}, Lav;->a(ILandroid/os/Bundle;Law;)Ldg;

    .line 283
    :cond_2
    return-void

    .line 266
    :cond_3
    iput-boolean v4, p0, Lqj;->ah:Z

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 313
    invoke-super {p0, p1}, Lt;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 315
    iget-object v0, p0, Lqj;->c:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 316
    const-string v0, "com.android.mail.photo.fragments.PhotoViewFragment.INTENT"

    iget-object v1, p0, Lqj;->c:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 318
    :cond_0
    return-void
.end method

.method public q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lqj;->a:Ljava/lang/String;

    return-object v0
.end method

.method public r()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v0}, Lcom/android/ex/photo/views/PhotoView;->e()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()V
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    if-eqz v0, :cond_0

    .line 483
    iget-object v0, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v0}, Lcom/android/ex/photo/views/PhotoView;->g()V

    .line 485
    :cond_0
    return-void
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 511
    iget-object v0, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lqj;->g:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v0}, Lcom/android/ex/photo/views/PhotoView;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public u()I
    .locals 1

    .prologue
    .line 569
    iget v0, p0, Lqj;->aa:I

    return v0
.end method
