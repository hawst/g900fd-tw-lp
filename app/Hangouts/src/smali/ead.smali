.class public final Lead;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lead;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Leae;

.field public d:Leac;

.field public e:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 324
    const/4 v0, 0x0

    new-array v0, v0, [Lead;

    sput-object v0, Lead;->a:[Lead;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 325
    invoke-direct {p0}, Lepn;-><init>()V

    .line 330
    iput-object v0, p0, Lead;->c:Leae;

    .line 333
    iput-object v0, p0, Lead;->d:Leac;

    .line 325
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 358
    const/4 v0, 0x0

    .line 359
    iget-object v1, p0, Lead;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 360
    const/4 v0, 0x1

    iget-object v1, p0, Lead;->b:Ljava/lang/String;

    .line 361
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 363
    :cond_0
    iget-object v1, p0, Lead;->c:Leae;

    if-eqz v1, :cond_1

    .line 364
    const/4 v1, 0x2

    iget-object v2, p0, Lead;->c:Leae;

    .line 365
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 367
    :cond_1
    iget-object v1, p0, Lead;->d:Leac;

    if-eqz v1, :cond_2

    .line 368
    const/4 v1, 0x5

    iget-object v2, p0, Lead;->d:Leac;

    .line 369
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 371
    :cond_2
    iget-object v1, p0, Lead;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 372
    const/4 v1, 0x6

    iget-object v2, p0, Lead;->e:Ljava/lang/Integer;

    .line 373
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 375
    :cond_3
    iget-object v1, p0, Lead;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 376
    iput v0, p0, Lead;->cachedSize:I

    .line 377
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 321
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lead;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lead;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lead;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lead;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lead;->c:Leae;

    if-nez v0, :cond_2

    new-instance v0, Leae;

    invoke-direct {v0}, Leae;-><init>()V

    iput-object v0, p0, Lead;->c:Leae;

    :cond_2
    iget-object v0, p0, Lead;->c:Leae;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lead;->d:Leac;

    if-nez v0, :cond_3

    new-instance v0, Leac;

    invoke-direct {v0}, Leac;-><init>()V

    iput-object v0, p0, Lead;->d:Leac;

    :cond_3
    iget-object v0, p0, Lead;->d:Leac;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lead;->e:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x2a -> :sswitch_3
        0x30 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 340
    iget-object v0, p0, Lead;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 341
    const/4 v0, 0x1

    iget-object v1, p0, Lead;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 343
    :cond_0
    iget-object v0, p0, Lead;->c:Leae;

    if-eqz v0, :cond_1

    .line 344
    const/4 v0, 0x2

    iget-object v1, p0, Lead;->c:Leae;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 346
    :cond_1
    iget-object v0, p0, Lead;->d:Leac;

    if-eqz v0, :cond_2

    .line 347
    const/4 v0, 0x5

    iget-object v1, p0, Lead;->d:Leac;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 349
    :cond_2
    iget-object v0, p0, Lead;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 350
    const/4 v0, 0x6

    iget-object v1, p0, Lead;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 352
    :cond_3
    iget-object v0, p0, Lead;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 354
    return-void
.end method
