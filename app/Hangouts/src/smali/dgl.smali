.class public final Ldgl;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldgl;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3209
    const/4 v0, 0x0

    new-array v0, v0, [Ldgl;

    sput-object v0, Ldgl;->a:[Ldgl;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3210
    invoke-direct {p0}, Lepn;-><init>()V

    .line 3223
    iput-object v0, p0, Ldgl;->b:Ljava/lang/Integer;

    .line 3226
    iput-object v0, p0, Ldgl;->c:Ljava/lang/Integer;

    .line 3210
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 3243
    const/4 v0, 0x0

    .line 3244
    iget-object v1, p0, Ldgl;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 3245
    const/4 v0, 0x1

    iget-object v1, p0, Ldgl;->b:Ljava/lang/Integer;

    .line 3246
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3248
    :cond_0
    iget-object v1, p0, Ldgl;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 3249
    const/4 v1, 0x2

    iget-object v2, p0, Ldgl;->c:Ljava/lang/Integer;

    .line 3250
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3252
    :cond_1
    iget-object v1, p0, Ldgl;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3253
    iput v0, p0, Ldgl;->cachedSize:I

    .line 3254
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 3206
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldgl;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldgl;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldgl;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgl;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgl;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-ne v0, v2, :cond_4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgl;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgl;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 3231
    iget-object v0, p0, Ldgl;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 3232
    const/4 v0, 0x1

    iget-object v1, p0, Ldgl;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 3234
    :cond_0
    iget-object v0, p0, Ldgl;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 3235
    const/4 v0, 0x2

    iget-object v1, p0, Ldgl;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 3237
    :cond_1
    iget-object v0, p0, Ldgl;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 3239
    return-void
.end method
