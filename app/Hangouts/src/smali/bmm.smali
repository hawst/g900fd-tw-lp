.class public final Lbmm;
.super Lbmf;
.source "PG"


# instance fields
.field public final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lyj;Lbmh;Lbmf;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 366
    invoke-direct {p0, p1, p2}, Lbmf;-><init>(Lyj;Lbmh;)V

    .line 361
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbmm;->k:Ljava/util/List;

    .line 367
    const/4 v0, 0x0

    iput-object v0, p0, Lbmm;->g:Ljava/lang/String;

    .line 368
    iput v2, p0, Lbmm;->h:I

    .line 371
    invoke-virtual {p3}, Lbmf;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbmm;->b:Ljava/lang/String;

    .line 372
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    sget v1, Lh;->ki:I

    new-array v3, v5, [Ljava/lang/Object;

    iget v4, p2, Lbmh;->a:I

    .line 373
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    .line 372
    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbmm;->d:Ljava/lang/String;

    .line 374
    iget-object v0, p3, Lbmf;->e:Ljava/lang/CharSequence;

    iput-object v0, p0, Lbmm;->c:Ljava/lang/CharSequence;

    .line 376
    iget-object v0, p0, Lbmm;->m:Lyj;

    invoke-static {v0}, Lbbl;->c(Lyj;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lbmm;->f:Landroid/content/Intent;

    .line 377
    iget-object v0, p0, Lbmm;->f:Landroid/content/Intent;

    const-string v1, "reset_chat_notifications"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 378
    iget-object v0, p0, Lbmm;->f:Landroid/content/Intent;

    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move v1, v2

    .line 383
    :goto_0
    iget-object v0, p2, Lbmh;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 384
    iget-object v0, p2, Lbmh;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmj;

    .line 385
    iget-object v3, v0, Lbmj;->h:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Lbml;

    if-eqz v3, :cond_0

    .line 386
    iget-object v3, v0, Lbmj;->a:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lbmm;->b(Ljava/lang/String;)V

    .line 389
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 390
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 391
    new-instance v4, Lbmh;

    iget v0, v0, Lbmj;->i:I

    invoke-direct {v4, v0, v3}, Lbmh;-><init>(ILjava/util/List;)V

    .line 393
    iget-object v0, p0, Lbmm;->k:Ljava/util/List;

    new-instance v3, Lbmg;

    invoke-direct {v3, p1, v4, v1}, Lbmg;-><init>(Lyj;Lbmh;I)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 383
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 395
    :cond_1
    return-void
.end method


# virtual methods
.method protected a(Lbk;)Lbu;
    .locals 12

    .prologue
    .line 399
    iget-object v0, p0, Lbmm;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lbk;->a(Ljava/lang/CharSequence;)Lbk;

    .line 400
    sget-boolean v0, Lbmm;->a:Z

    if-eqz v0, :cond_0

    .line 401
    const-string v0, "Babel"

    const-string v1, "Multiple Conversations"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    :cond_0
    new-instance v6, Lbm;

    invoke-direct {v6, p1}, Lbm;-><init>(Lbk;)V

    .line 405
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v7

    .line 407
    sget v0, Lh;->cA:I

    invoke-virtual {v7, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 408
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 409
    const-wide/16 v1, 0x0

    .line 410
    const/4 v0, 0x0

    move-wide v3, v1

    move v2, v0

    :goto_0
    iget-object v0, p0, Lbmm;->j:Lbmh;

    iget-object v0, v0, Lbmh;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_7

    .line 411
    iget-object v0, p0, Lbmm;->j:Lbmh;

    iget-object v0, v0, Lbmh;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmj;

    .line 412
    iget-wide v10, v0, Lbmj;->g:J

    cmp-long v1, v10, v3

    if-lez v1, :cond_1

    .line 413
    iget-wide v3, v0, Lbmj;->g:J

    .line 417
    :cond_1
    iget-object v1, v0, Lbmj;->h:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbmo;

    .line 418
    instance-of v5, v1, Lbmk;

    if-eqz v5, :cond_5

    .line 419
    check-cast v1, Lbmk;

    .line 424
    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v1, v1, Lbmk;->b:Ljava/lang/String;

    invoke-direct {v10, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 426
    const/4 v1, 0x1

    move v5, v1

    :goto_1
    iget-object v1, v0, Lbmj;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v5, v1, :cond_2

    .line 427
    const-string v1, ", "

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 428
    iget-object v1, v0, Lbmj;->h:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbmk;

    .line 429
    iget-object v1, v1, Lbmk;->b:Ljava/lang/String;

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 426
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    .line 432
    :cond_2
    sget v1, Lh;->jC:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget v0, v0, Lbmj;->i:I

    .line 434
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v11

    .line 432
    invoke-virtual {v7, v1, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 435
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 436
    const/4 v5, 0x0

    iput-object v5, p0, Lbmm;->g:Ljava/lang/String;

    .line 437
    const/4 v5, 0x0

    iput v5, p0, Lbmm;->h:I

    .line 452
    :goto_2
    iget-object v5, p0, Lbmm;->g:Ljava/lang/String;

    iget v10, p0, Lbmm;->h:I

    invoke-static {v1, v0, v5, v10}, Lbne;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v6, v0}, Lbm;->b(Ljava/lang/CharSequence;)Lbm;

    .line 454
    if-eqz v1, :cond_4

    .line 455
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 456
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 458
    :cond_3
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 438
    :cond_5
    iget-boolean v5, v0, Lbmj;->c:Z

    if-eqz v5, :cond_6

    .line 441
    iget-object v1, v0, Lbmj;->f:Ljava/lang/String;

    .line 442
    const/4 v0, 0x0

    .line 443
    const/4 v5, 0x0

    iput-object v5, p0, Lbmm;->g:Ljava/lang/String;

    .line 444
    const/4 v5, 0x0

    iput v5, p0, Lbmm;->h:I

    goto :goto_2

    .line 446
    :cond_6
    check-cast v1, Lbml;

    .line 447
    iget-object v0, v1, Lbml;->b:Ljava/lang/CharSequence;

    .line 448
    iget-object v5, v1, Lbml;->c:Ljava/lang/String;

    iput-object v5, p0, Lbmm;->g:Ljava/lang/String;

    .line 449
    iget v5, v1, Lbml;->d:I

    iput v5, p0, Lbmm;->h:I

    .line 450
    iget-object v1, v1, Lbml;->f:Ljava/lang/String;

    goto :goto_2

    .line 462
    :cond_7
    iput-object v9, p0, Lbmm;->e:Ljava/lang/CharSequence;

    .line 463
    invoke-virtual {p1, v9}, Lbk;->b(Ljava/lang/CharSequence;)Lbk;

    move-result-object v0

    .line 464
    invoke-virtual {p0}, Lbmm;->n()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbk;->c(Ljava/lang/CharSequence;)Lbk;

    move-result-object v0

    const-wide/16 v1, 0x3e8

    div-long v1, v3, v1

    .line 465
    invoke-virtual {v0, v1, v2}, Lbk;->a(J)Lbk;

    .line 467
    return-object v6
.end method
