.class Lmh;
.super Landroid/view/ActionProvider;
.source "PG"


# instance fields
.field final a:Lfr;

.field final synthetic b:Lmg;


# direct methods
.method public constructor <init>(Lmg;Lfr;)V
    .locals 2

    .prologue
    .line 395
    iput-object p1, p0, Lmh;->b:Lmg;

    .line 396
    invoke-virtual {p2}, Lfr;->a()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/view/ActionProvider;-><init>(Landroid/content/Context;)V

    .line 397
    iput-object p2, p0, Lmh;->a:Lfr;

    .line 399
    invoke-static {p1}, Lmg;->a(Lmg;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 400
    iget-object v0, p0, Lmh;->a:Lfr;

    new-instance v1, Lmi;

    invoke-direct {v1, p0, p1}, Lmi;-><init>(Lmh;Lmg;)V

    invoke-virtual {v0, v1}, Lfr;->a(Lft;)V

    .line 409
    :cond_0
    return-void
.end method


# virtual methods
.method public hasSubMenu()Z
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lmh;->a:Lfr;

    invoke-virtual {v0}, Lfr;->e()Z

    move-result v0

    return v0
.end method

.method public onCreateActionView()Landroid/view/View;
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lmh;->b:Lmg;

    invoke-static {v0}, Lmg;->a(Lmg;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416
    iget-object v0, p0, Lmh;->b:Lmg;

    invoke-virtual {v0}, Lmg;->c()Z

    .line 418
    :cond_0
    iget-object v0, p0, Lmh;->a:Lfr;

    invoke-virtual {v0}, Lfr;->b()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onPerformDefaultAction()Z
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lmh;->a:Lfr;

    const/4 v0, 0x0

    return v0
.end method

.method public onPrepareSubMenu(Landroid/view/SubMenu;)V
    .locals 2

    .prologue
    .line 433
    iget-object v0, p0, Lmh;->a:Lfr;

    iget-object v1, p0, Lmh;->b:Lmg;

    invoke-virtual {v1, p1}, Lmg;->a(Landroid/view/SubMenu;)Landroid/view/SubMenu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfr;->a(Landroid/view/SubMenu;)V

    .line 434
    return-void
.end method
