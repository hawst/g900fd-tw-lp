.class public final Lepe;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lepe;


# instance fields
.field public b:Lepf;

.field public c:Lepf;

.field public d:Lepi;

.field public e:[Leph;

.field public f:[Lepg;

.field public g:Ljava/lang/Float;

.field public h:Ljava/lang/Float;

.field public i:Ljava/lang/Float;

.field public j:Ljava/lang/Float;

.field public k:Ljava/lang/Float;

.field public l:Ljava/lang/Float;

.field public m:Ljava/lang/Float;

.field public n:Ljava/lang/Float;

.field public o:Ljava/lang/Float;

.field public p:Ljava/lang/Float;

.field public q:Ljava/lang/Float;

.field public r:Ljava/lang/Float;

.field public s:Ljava/lang/Float;

.field public t:Ljava/lang/Float;

.field public u:Ljava/lang/Float;

.field public v:Ljava/lang/Float;

.field public w:Ljava/lang/Float;

.field public x:[B

.field public y:[Lepj;

.field public z:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    new-array v0, v0, [Lepe;

    sput-object v0, Lepe;->a:[Lepe;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 114
    invoke-direct {p0}, Lepn;-><init>()V

    .line 508
    iput-object v0, p0, Lepe;->b:Lepf;

    .line 511
    iput-object v0, p0, Lepe;->c:Lepf;

    .line 514
    iput-object v0, p0, Lepe;->d:Lepi;

    .line 517
    sget-object v0, Leph;->a:[Leph;

    iput-object v0, p0, Lepe;->e:[Leph;

    .line 520
    sget-object v0, Lepg;->a:[Lepg;

    iput-object v0, p0, Lepe;->f:[Lepg;

    .line 559
    sget-object v0, Lepj;->a:[Lepj;

    iput-object v0, p0, Lepe;->y:[Lepj;

    .line 114
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 660
    iget-object v0, p0, Lepe;->b:Lepf;

    if-eqz v0, :cond_1b

    .line 661
    const/4 v0, 0x1

    iget-object v2, p0, Lepe;->b:Lepf;

    .line 662
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 664
    :goto_0
    iget-object v2, p0, Lepe;->e:[Leph;

    if-eqz v2, :cond_1

    .line 665
    iget-object v3, p0, Lepe;->e:[Leph;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 666
    if-eqz v5, :cond_0

    .line 667
    const/4 v6, 0x2

    .line 668
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 665
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 672
    :cond_1
    iget-object v2, p0, Lepe;->g:Ljava/lang/Float;

    if-eqz v2, :cond_2

    .line 673
    const/4 v2, 0x3

    iget-object v3, p0, Lepe;->g:Ljava/lang/Float;

    .line 674
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 676
    :cond_2
    iget-object v2, p0, Lepe;->h:Ljava/lang/Float;

    if-eqz v2, :cond_3

    .line 677
    const/4 v2, 0x4

    iget-object v3, p0, Lepe;->h:Ljava/lang/Float;

    .line 678
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 680
    :cond_3
    iget-object v2, p0, Lepe;->i:Ljava/lang/Float;

    if-eqz v2, :cond_4

    .line 681
    const/4 v2, 0x5

    iget-object v3, p0, Lepe;->i:Ljava/lang/Float;

    .line 682
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 684
    :cond_4
    iget-object v2, p0, Lepe;->k:Ljava/lang/Float;

    if-eqz v2, :cond_5

    .line 685
    const/4 v2, 0x6

    iget-object v3, p0, Lepe;->k:Ljava/lang/Float;

    .line 686
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 688
    :cond_5
    iget-object v2, p0, Lepe;->l:Ljava/lang/Float;

    if-eqz v2, :cond_6

    .line 689
    const/4 v2, 0x7

    iget-object v3, p0, Lepe;->l:Ljava/lang/Float;

    .line 690
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 692
    :cond_6
    iget-object v2, p0, Lepe;->x:[B

    if-eqz v2, :cond_7

    .line 693
    const/16 v2, 0x8

    iget-object v3, p0, Lepe;->x:[B

    .line 694
    invoke-static {v2, v3}, Lepl;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 696
    :cond_7
    iget-object v2, p0, Lepe;->j:Ljava/lang/Float;

    if-eqz v2, :cond_8

    .line 697
    const/16 v2, 0x9

    iget-object v3, p0, Lepe;->j:Ljava/lang/Float;

    .line 698
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 700
    :cond_8
    iget-object v2, p0, Lepe;->m:Ljava/lang/Float;

    if-eqz v2, :cond_9

    .line 701
    const/16 v2, 0xa

    iget-object v3, p0, Lepe;->m:Ljava/lang/Float;

    .line 702
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 704
    :cond_9
    iget-object v2, p0, Lepe;->n:Ljava/lang/Float;

    if-eqz v2, :cond_a

    .line 705
    const/16 v2, 0xb

    iget-object v3, p0, Lepe;->n:Ljava/lang/Float;

    .line 706
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 708
    :cond_a
    iget-object v2, p0, Lepe;->o:Ljava/lang/Float;

    if-eqz v2, :cond_b

    .line 709
    const/16 v2, 0xc

    iget-object v3, p0, Lepe;->o:Ljava/lang/Float;

    .line 710
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 712
    :cond_b
    iget-object v2, p0, Lepe;->p:Ljava/lang/Float;

    if-eqz v2, :cond_c

    .line 713
    const/16 v2, 0xd

    iget-object v3, p0, Lepe;->p:Ljava/lang/Float;

    .line 714
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 716
    :cond_c
    iget-object v2, p0, Lepe;->q:Ljava/lang/Float;

    if-eqz v2, :cond_d

    .line 717
    const/16 v2, 0xe

    iget-object v3, p0, Lepe;->q:Ljava/lang/Float;

    .line 718
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 720
    :cond_d
    iget-object v2, p0, Lepe;->r:Ljava/lang/Float;

    if-eqz v2, :cond_e

    .line 721
    const/16 v2, 0xf

    iget-object v3, p0, Lepe;->r:Ljava/lang/Float;

    .line 722
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 724
    :cond_e
    iget-object v2, p0, Lepe;->s:Ljava/lang/Float;

    if-eqz v2, :cond_f

    .line 725
    const/16 v2, 0x10

    iget-object v3, p0, Lepe;->s:Ljava/lang/Float;

    .line 726
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 728
    :cond_f
    iget-object v2, p0, Lepe;->t:Ljava/lang/Float;

    if-eqz v2, :cond_10

    .line 729
    const/16 v2, 0x11

    iget-object v3, p0, Lepe;->t:Ljava/lang/Float;

    .line 730
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 732
    :cond_10
    iget-object v2, p0, Lepe;->u:Ljava/lang/Float;

    if-eqz v2, :cond_11

    .line 733
    const/16 v2, 0x12

    iget-object v3, p0, Lepe;->u:Ljava/lang/Float;

    .line 734
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 736
    :cond_11
    iget-object v2, p0, Lepe;->v:Ljava/lang/Float;

    if-eqz v2, :cond_12

    .line 737
    const/16 v2, 0x13

    iget-object v3, p0, Lepe;->v:Ljava/lang/Float;

    .line 738
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 740
    :cond_12
    iget-object v2, p0, Lepe;->z:[B

    if-eqz v2, :cond_13

    .line 741
    const/16 v2, 0x14

    iget-object v3, p0, Lepe;->z:[B

    .line 742
    invoke-static {v2, v3}, Lepl;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 744
    :cond_13
    iget-object v2, p0, Lepe;->f:[Lepg;

    if-eqz v2, :cond_15

    .line 745
    iget-object v3, p0, Lepe;->f:[Lepg;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_15

    aget-object v5, v3, v2

    .line 746
    if-eqz v5, :cond_14

    .line 747
    const/16 v6, 0x15

    .line 748
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 745
    :cond_14
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 752
    :cond_15
    iget-object v2, p0, Lepe;->c:Lepf;

    if-eqz v2, :cond_16

    .line 753
    const/16 v2, 0x16

    iget-object v3, p0, Lepe;->c:Lepf;

    .line 754
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 756
    :cond_16
    iget-object v2, p0, Lepe;->y:[Lepj;

    if-eqz v2, :cond_18

    .line 757
    iget-object v2, p0, Lepe;->y:[Lepj;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_18

    aget-object v4, v2, v1

    .line 758
    if-eqz v4, :cond_17

    .line 759
    const/16 v5, 0x17

    .line 760
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 757
    :cond_17
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 764
    :cond_18
    iget-object v1, p0, Lepe;->w:Ljava/lang/Float;

    if-eqz v1, :cond_19

    .line 765
    const/16 v1, 0x18

    iget-object v2, p0, Lepe;->w:Ljava/lang/Float;

    .line 766
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 768
    :cond_19
    iget-object v1, p0, Lepe;->d:Lepi;

    if-eqz v1, :cond_1a

    .line 769
    const/16 v1, 0x19

    iget-object v2, p0, Lepe;->d:Lepi;

    .line 770
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 772
    :cond_1a
    iget-object v1, p0, Lepe;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 773
    iput v0, p0, Lepe;->cachedSize:I

    .line 774
    return v0

    :cond_1b
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 110
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lepe;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lepe;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lepe;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lepe;->b:Lepf;

    if-nez v0, :cond_2

    new-instance v0, Lepf;

    invoke-direct {v0}, Lepf;-><init>()V

    iput-object v0, p0, Lepe;->b:Lepf;

    :cond_2
    iget-object v0, p0, Lepe;->b:Lepf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lepe;->e:[Leph;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leph;

    iget-object v3, p0, Lepe;->e:[Leph;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lepe;->e:[Leph;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lepe;->e:[Leph;

    :goto_2
    iget-object v2, p0, Lepe;->e:[Leph;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lepe;->e:[Leph;

    new-instance v3, Leph;

    invoke-direct {v3}, Leph;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lepe;->e:[Leph;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lepe;->e:[Leph;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lepe;->e:[Leph;

    new-instance v3, Leph;

    invoke-direct {v3}, Leph;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lepe;->e:[Leph;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lepe;->g:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lepe;->h:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lepe;->i:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lepe;->k:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lepe;->l:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Lepe;->x:[B

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lepe;->j:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lepe;->m:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lepe;->n:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lepe;->o:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lepe;->p:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lepe;->q:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lepe;->r:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lepe;->s:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lepe;->t:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lepe;->u:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lepe;->v:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Lepe;->z:[B

    goto/16 :goto_0

    :sswitch_15
    const/16 v0, 0xaa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lepe;->f:[Lepg;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lepg;

    iget-object v3, p0, Lepe;->f:[Lepg;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lepe;->f:[Lepg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Lepe;->f:[Lepg;

    :goto_4
    iget-object v2, p0, Lepe;->f:[Lepg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Lepe;->f:[Lepg;

    new-instance v3, Lepg;

    invoke-direct {v3}, Lepg;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lepe;->f:[Lepg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lepe;->f:[Lepg;

    array-length v0, v0

    goto :goto_3

    :cond_8
    iget-object v2, p0, Lepe;->f:[Lepg;

    new-instance v3, Lepg;

    invoke-direct {v3}, Lepg;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lepe;->f:[Lepg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_16
    iget-object v0, p0, Lepe;->c:Lepf;

    if-nez v0, :cond_9

    new-instance v0, Lepf;

    invoke-direct {v0}, Lepf;-><init>()V

    iput-object v0, p0, Lepe;->c:Lepf;

    :cond_9
    iget-object v0, p0, Lepe;->c:Lepf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_17
    const/16 v0, 0xba

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lepe;->y:[Lepj;

    if-nez v0, :cond_b

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lepj;

    iget-object v3, p0, Lepe;->y:[Lepj;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lepe;->y:[Lepj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    iput-object v2, p0, Lepe;->y:[Lepj;

    :goto_6
    iget-object v2, p0, Lepe;->y:[Lepj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_c

    iget-object v2, p0, Lepe;->y:[Lepj;

    new-instance v3, Lepj;

    invoke-direct {v3}, Lepj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lepe;->y:[Lepj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_b
    iget-object v0, p0, Lepe;->y:[Lepj;

    array-length v0, v0

    goto :goto_5

    :cond_c
    iget-object v2, p0, Lepe;->y:[Lepj;

    new-instance v3, Lepj;

    invoke-direct {v3}, Lepj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lepe;->y:[Lepj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lepe;->w:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_19
    iget-object v0, p0, Lepe;->d:Lepi;

    if-nez v0, :cond_d

    new-instance v0, Lepi;

    invoke-direct {v0}, Lepi;-><init>()V

    iput-object v0, p0, Lepe;->d:Lepi;

    :cond_d
    iget-object v0, p0, Lepe;->d:Lepi;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
        0x35 -> :sswitch_6
        0x3d -> :sswitch_7
        0x42 -> :sswitch_8
        0x4d -> :sswitch_9
        0x55 -> :sswitch_a
        0x5d -> :sswitch_b
        0x65 -> :sswitch_c
        0x6d -> :sswitch_d
        0x75 -> :sswitch_e
        0x7d -> :sswitch_f
        0x85 -> :sswitch_10
        0x8d -> :sswitch_11
        0x95 -> :sswitch_12
        0x9d -> :sswitch_13
        0xa2 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
        0xc5 -> :sswitch_18
        0xca -> :sswitch_19
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 566
    iget-object v1, p0, Lepe;->b:Lepf;

    if-eqz v1, :cond_0

    .line 567
    const/4 v1, 0x1

    iget-object v2, p0, Lepe;->b:Lepf;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 569
    :cond_0
    iget-object v1, p0, Lepe;->e:[Leph;

    if-eqz v1, :cond_2

    .line 570
    iget-object v2, p0, Lepe;->e:[Leph;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 571
    if-eqz v4, :cond_1

    .line 572
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 570
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 576
    :cond_2
    iget-object v1, p0, Lepe;->g:Ljava/lang/Float;

    if-eqz v1, :cond_3

    .line 577
    const/4 v1, 0x3

    iget-object v2, p0, Lepe;->g:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IF)V

    .line 579
    :cond_3
    iget-object v1, p0, Lepe;->h:Ljava/lang/Float;

    if-eqz v1, :cond_4

    .line 580
    const/4 v1, 0x4

    iget-object v2, p0, Lepe;->h:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IF)V

    .line 582
    :cond_4
    iget-object v1, p0, Lepe;->i:Ljava/lang/Float;

    if-eqz v1, :cond_5

    .line 583
    const/4 v1, 0x5

    iget-object v2, p0, Lepe;->i:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IF)V

    .line 585
    :cond_5
    iget-object v1, p0, Lepe;->k:Ljava/lang/Float;

    if-eqz v1, :cond_6

    .line 586
    const/4 v1, 0x6

    iget-object v2, p0, Lepe;->k:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IF)V

    .line 588
    :cond_6
    iget-object v1, p0, Lepe;->l:Ljava/lang/Float;

    if-eqz v1, :cond_7

    .line 589
    const/4 v1, 0x7

    iget-object v2, p0, Lepe;->l:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IF)V

    .line 591
    :cond_7
    iget-object v1, p0, Lepe;->x:[B

    if-eqz v1, :cond_8

    .line 592
    const/16 v1, 0x8

    iget-object v2, p0, Lepe;->x:[B

    invoke-virtual {p1, v1, v2}, Lepl;->a(I[B)V

    .line 594
    :cond_8
    iget-object v1, p0, Lepe;->j:Ljava/lang/Float;

    if-eqz v1, :cond_9

    .line 595
    const/16 v1, 0x9

    iget-object v2, p0, Lepe;->j:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IF)V

    .line 597
    :cond_9
    iget-object v1, p0, Lepe;->m:Ljava/lang/Float;

    if-eqz v1, :cond_a

    .line 598
    const/16 v1, 0xa

    iget-object v2, p0, Lepe;->m:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IF)V

    .line 600
    :cond_a
    iget-object v1, p0, Lepe;->n:Ljava/lang/Float;

    if-eqz v1, :cond_b

    .line 601
    const/16 v1, 0xb

    iget-object v2, p0, Lepe;->n:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IF)V

    .line 603
    :cond_b
    iget-object v1, p0, Lepe;->o:Ljava/lang/Float;

    if-eqz v1, :cond_c

    .line 604
    const/16 v1, 0xc

    iget-object v2, p0, Lepe;->o:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IF)V

    .line 606
    :cond_c
    iget-object v1, p0, Lepe;->p:Ljava/lang/Float;

    if-eqz v1, :cond_d

    .line 607
    const/16 v1, 0xd

    iget-object v2, p0, Lepe;->p:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IF)V

    .line 609
    :cond_d
    iget-object v1, p0, Lepe;->q:Ljava/lang/Float;

    if-eqz v1, :cond_e

    .line 610
    const/16 v1, 0xe

    iget-object v2, p0, Lepe;->q:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IF)V

    .line 612
    :cond_e
    iget-object v1, p0, Lepe;->r:Ljava/lang/Float;

    if-eqz v1, :cond_f

    .line 613
    const/16 v1, 0xf

    iget-object v2, p0, Lepe;->r:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IF)V

    .line 615
    :cond_f
    iget-object v1, p0, Lepe;->s:Ljava/lang/Float;

    if-eqz v1, :cond_10

    .line 616
    const/16 v1, 0x10

    iget-object v2, p0, Lepe;->s:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IF)V

    .line 618
    :cond_10
    iget-object v1, p0, Lepe;->t:Ljava/lang/Float;

    if-eqz v1, :cond_11

    .line 619
    const/16 v1, 0x11

    iget-object v2, p0, Lepe;->t:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IF)V

    .line 621
    :cond_11
    iget-object v1, p0, Lepe;->u:Ljava/lang/Float;

    if-eqz v1, :cond_12

    .line 622
    const/16 v1, 0x12

    iget-object v2, p0, Lepe;->u:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IF)V

    .line 624
    :cond_12
    iget-object v1, p0, Lepe;->v:Ljava/lang/Float;

    if-eqz v1, :cond_13

    .line 625
    const/16 v1, 0x13

    iget-object v2, p0, Lepe;->v:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IF)V

    .line 627
    :cond_13
    iget-object v1, p0, Lepe;->z:[B

    if-eqz v1, :cond_14

    .line 628
    const/16 v1, 0x14

    iget-object v2, p0, Lepe;->z:[B

    invoke-virtual {p1, v1, v2}, Lepl;->a(I[B)V

    .line 630
    :cond_14
    iget-object v1, p0, Lepe;->f:[Lepg;

    if-eqz v1, :cond_16

    .line 631
    iget-object v2, p0, Lepe;->f:[Lepg;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_16

    aget-object v4, v2, v1

    .line 632
    if-eqz v4, :cond_15

    .line 633
    const/16 v5, 0x15

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 631
    :cond_15
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 637
    :cond_16
    iget-object v1, p0, Lepe;->c:Lepf;

    if-eqz v1, :cond_17

    .line 638
    const/16 v1, 0x16

    iget-object v2, p0, Lepe;->c:Lepf;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 640
    :cond_17
    iget-object v1, p0, Lepe;->y:[Lepj;

    if-eqz v1, :cond_19

    .line 641
    iget-object v1, p0, Lepe;->y:[Lepj;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_19

    aget-object v3, v1, v0

    .line 642
    if-eqz v3, :cond_18

    .line 643
    const/16 v4, 0x17

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 641
    :cond_18
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 647
    :cond_19
    iget-object v0, p0, Lepe;->w:Ljava/lang/Float;

    if-eqz v0, :cond_1a

    .line 648
    const/16 v0, 0x18

    iget-object v1, p0, Lepe;->w:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 650
    :cond_1a
    iget-object v0, p0, Lepe;->d:Lepi;

    if-eqz v0, :cond_1b

    .line 651
    const/16 v0, 0x19

    iget-object v1, p0, Lepe;->d:Lepi;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 653
    :cond_1b
    iget-object v0, p0, Lepe;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 655
    return-void
.end method
