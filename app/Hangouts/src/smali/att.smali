.class public final Latt;
.super Latc;
.source "PG"


# static fields
.field public static final a:Z


# instance fields
.field private final d:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

.field private final e:Lasx;

.field private final f:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lbys;->e:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Latt;->a:Z

    return-void
.end method

.method public constructor <init>(Latd;)V
    .locals 3

    .prologue
    .line 34
    invoke-direct {p0}, Latc;-><init>()V

    .line 31
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Latt;->f:Ljava/lang/Object;

    .line 35
    const-string v0, "Babel"

    const-string v1, "RootGLArea constructor"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    new-instance v0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    iget-object v1, p0, Latt;->f:Ljava/lang/Object;

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;-><init>(Latd;Ljava/lang/Object;)V

    iput-object v0, p0, Latt;->d:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    .line 37
    new-instance v0, Lasx;

    iget-object v1, p0, Latt;->d:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    iget-object v2, p0, Latt;->f:Ljava/lang/Object;

    invoke-direct {v0, p1, v1, v2}, Lasx;-><init>(Latd;Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;Ljava/lang/Object;)V

    iput-object v0, p0, Latt;->e:Lasx;

    .line 39
    iget-object v0, p0, Latt;->d:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    iget-object v1, p0, Latt;->e:Lasx;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a(Lasx;)V

    .line 40
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 49
    invoke-static {}, Lcwz;->a()V

    .line 50
    iget-object v0, p0, Latt;->d:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a()V

    .line 51
    iget-object v0, p0, Latt;->e:Lasx;

    invoke-virtual {v0}, Lasx;->a()V

    .line 52
    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Latt;->d:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a(Landroid/content/res/Configuration;)V

    .line 90
    iget-object v0, p0, Latt;->e:Lasx;

    invoke-virtual {v0, p1}, Lasx;->a(Landroid/content/res/Configuration;)V

    .line 91
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Lcwz;->a()V

    .line 74
    iget-object v0, p0, Latt;->d:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->b()V

    .line 75
    iget-object v0, p0, Latt;->e:Lasx;

    invoke-virtual {v0}, Lasx;->b()V

    .line 76
    return-void
.end method

.method c()Lasx;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Latt;->e:Lasx;

    return-object v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 57
    invoke-static {}, Lcwz;->a()V

    .line 58
    iget-object v0, p0, Latt;->d:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->h()V

    .line 59
    iget-object v0, p0, Latt;->e:Lasx;

    invoke-virtual {v0}, Lasx;->h()V

    .line 60
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 65
    invoke-static {}, Lcwz;->a()V

    .line 66
    iget-object v0, p0, Latt;->d:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->i()V

    .line 67
    iget-object v0, p0, Latt;->e:Lasx;

    invoke-virtual {v0}, Lasx;->i()V

    .line 68
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 81
    invoke-static {}, Lcwz;->a()V

    .line 82
    iget-object v0, p0, Latt;->d:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->j()V

    .line 83
    iget-object v0, p0, Latt;->e:Lasx;

    invoke-virtual {v0}, Lasx;->j()V

    .line 84
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Latt;->d:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->k()V

    .line 97
    iget-object v0, p0, Latt;->e:Lasx;

    invoke-virtual {v0}, Lasx;->k()V

    .line 98
    return-void
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 2

    .prologue
    .line 105
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 107
    iget-object v1, p0, Latt;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 108
    :try_start_0
    iget-object v0, p0, Latt;->e:Lasx;

    invoke-virtual {v0}, Lasx;->f()V

    .line 109
    iget-object v0, p0, Latt;->d:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->e()V

    .line 112
    iget-object v0, p0, Latt;->e:Lasx;

    invoke-virtual {v0, p1}, Lasx;->onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 113
    iget-object v0, p0, Latt;->d:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 114
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 3

    .prologue
    .line 127
    sget-boolean v0, Latt;->a:Z

    if-eqz v0, :cond_0

    .line 128
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RootGLArea.onSurfaceChanged "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :cond_0
    invoke-super {p0, p1, p2, p3}, Latc;->onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 132
    iget-object v0, p0, Latt;->d:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 133
    iget-object v0, p0, Latt;->e:Lasx;

    invoke-virtual {v0, p1, p2, p3}, Lasx;->onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 134
    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 2

    .prologue
    .line 119
    const-string v0, "Babel"

    const-string v1, "RootGLArea.onSurfaceCreated"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Latt;->d:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V

    .line 122
    iget-object v0, p0, Latt;->e:Lasx;

    invoke-virtual {v0, p1, p2}, Lasx;->onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V

    .line 123
    return-void
.end method
