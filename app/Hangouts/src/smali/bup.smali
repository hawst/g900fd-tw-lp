.class public final Lbup;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Lbme;

.field final b:I

.field c:Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;

.field d:Z

.field private e:Lbtz;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lbtz;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbup;->d:Z

    .line 53
    iput-object p1, p0, Lbup;->e:Lbtz;

    .line 54
    iget-object v0, p0, Lbup;->e:Lbtz;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 56
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbme;

    invoke-static {v0, v1}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    iput-object v0, p0, Lbup;->a:Lbme;

    .line 58
    iput-object p2, p0, Lbup;->f:Ljava/lang/String;

    .line 59
    iput-object p3, p0, Lbup;->g:Ljava/lang/String;

    .line 60
    iput-object p5, p0, Lbup;->h:Ljava/lang/String;

    .line 61
    iput p4, p0, Lbup;->b:I

    .line 62
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 65
    const-string v0, "Babel"

    const-string v1, "NotificationSettingsUI onCreate"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-static {p1}, Lbkb;->b(Ljava/lang/String;)Lyj;

    .line 68
    iget-object v0, p0, Lbup;->e:Lbtz;

    invoke-interface {v0}, Lbtz;->b()Landroid/preference/PreferenceManager;

    move-result-object v1

    .line 69
    invoke-static {p1}, Lf;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lbup;->e:Lbtz;

    invoke-interface {v0}, Lbtz;->c()Landroid/app/Activity;

    move-result-object v2

    .line 72
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 73
    invoke-virtual {v1, v2}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v4

    .line 74
    iget-object v0, p0, Lbup;->e:Lbtz;

    invoke-interface {v0, v4}, Lbtz;->a(Landroid/preference/PreferenceScreen;)V

    .line 77
    iget-object v0, p0, Lbup;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 78
    new-instance v0, Landroid/preference/CheckBoxPreference;

    invoke-direct {v0, v2}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    .line 79
    sget v5, Lh;->ls:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 80
    iget-object v5, p0, Lbup;->f:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    .line 81
    invoke-virtual {v0, v7}, Landroid/preference/CheckBoxPreference;->setPersistent(Z)V

    .line 82
    sget v5, Lf;->bU:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/preference/CheckBoxPreference;->setDefaultValue(Ljava/lang/Object;)V

    .line 84
    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 88
    :cond_0
    new-instance v0, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;

    invoke-direct {v0, v2}, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lbup;->c:Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;

    .line 89
    iget-object v0, p0, Lbup;->c:Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;

    sget v5, Lh;->an:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 90
    iget-object v0, p0, Lbup;->c:Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;

    iget-object v5, p0, Lbup;->g:Ljava/lang/String;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->setKey(Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lbup;->c:Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;

    iget v5, p0, Lbup;->b:I

    invoke-virtual {v0, v5}, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->setRingtoneType(I)V

    .line 92
    iget-object v0, p0, Lbup;->c:Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;

    invoke-virtual {v0, v7}, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->setPersistent(Z)V

    .line 93
    iget-object v0, p0, Lbup;->c:Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;

    new-instance v5, Lbuq;

    invoke-direct {v5, p0}, Lbuq;-><init>(Lbup;)V

    invoke-virtual {v0, v5}, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 113
    iget-object v0, p0, Lbup;->c:Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 114
    iget v0, p0, Lbup;->b:I

    if-ne v0, v7, :cond_1

    sget v0, Lf;->hN:I

    .line 117
    :goto_0
    iget-object v5, p0, Lbup;->c:Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;

    invoke-virtual {v1}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    iget-object v6, p0, Lbup;->g:Ljava/lang/String;

    .line 119
    invoke-static {v0}, Lf;->g(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 117
    invoke-interface {v1, v6, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->a(Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lbup;->c:Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;

    iget-object v1, p0, Lbup;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->setDependency(Ljava/lang/String;)V

    .line 124
    new-instance v0, Landroid/preference/CheckBoxPreference;

    invoke-direct {v0, v2}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    .line 125
    sget v1, Lh;->lv:I

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v1, p0, Lbup;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    .line 128
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setDefaultValue(Ljava/lang/Object;)V

    .line 129
    invoke-virtual {v0, v7}, Landroid/preference/CheckBoxPreference;->setPersistent(Z)V

    .line 130
    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 131
    iget-object v1, p0, Lbup;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setDependency(Ljava/lang/String;)V

    .line 132
    return-void

    .line 114
    :cond_1
    sget v0, Lf;->hO:I

    goto :goto_0
.end method
