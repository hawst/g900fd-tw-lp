.class public final Lexl;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lexl;


# instance fields
.field public b:Levt;

.field public c:Lexm;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/Integer;

.field public g:[Lexm;

.field public h:Levt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lexl;

    sput-object v0, Lexl;->a:[Lexl;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 100
    iput-object v1, p0, Lexl;->b:Levt;

    .line 103
    iput-object v1, p0, Lexl;->c:Lexm;

    .line 112
    sget-object v0, Lexm;->a:[Lexm;

    iput-object v0, p0, Lexl;->g:[Lexm;

    .line 115
    iput-object v1, p0, Lexl;->h:Levt;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 152
    iget-object v0, p0, Lexl;->b:Levt;

    if-eqz v0, :cond_7

    .line 153
    const/4 v0, 0x1

    iget-object v2, p0, Lexl;->b:Levt;

    .line 154
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 156
    :goto_0
    iget-object v2, p0, Lexl;->c:Lexm;

    if-eqz v2, :cond_0

    .line 157
    const/4 v2, 0x2

    iget-object v3, p0, Lexl;->c:Lexm;

    .line 158
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 160
    :cond_0
    iget-object v2, p0, Lexl;->d:Ljava/lang/Long;

    if-eqz v2, :cond_1

    .line 161
    const/4 v2, 0x3

    iget-object v3, p0, Lexl;->d:Ljava/lang/Long;

    .line 162
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 164
    :cond_1
    iget-object v2, p0, Lexl;->e:Ljava/lang/Long;

    if-eqz v2, :cond_2

    .line 165
    const/4 v2, 0x4

    iget-object v3, p0, Lexl;->e:Ljava/lang/Long;

    .line 166
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 168
    :cond_2
    iget-object v2, p0, Lexl;->f:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 169
    const/4 v2, 0x5

    iget-object v3, p0, Lexl;->f:Ljava/lang/Integer;

    .line 170
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 172
    :cond_3
    iget-object v2, p0, Lexl;->g:[Lexm;

    if-eqz v2, :cond_5

    .line 173
    iget-object v2, p0, Lexl;->g:[Lexm;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 174
    if-eqz v4, :cond_4

    .line 175
    const/4 v5, 0x6

    .line 176
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 173
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 180
    :cond_5
    iget-object v1, p0, Lexl;->h:Levt;

    if-eqz v1, :cond_6

    .line 181
    const/4 v1, 0x7

    iget-object v2, p0, Lexl;->h:Levt;

    .line 182
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 184
    :cond_6
    iget-object v1, p0, Lexl;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 185
    iput v0, p0, Lexl;->cachedSize:I

    .line 186
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lexl;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lexl;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lexl;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lexl;->b:Levt;

    if-nez v0, :cond_2

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Lexl;->b:Levt;

    :cond_2
    iget-object v0, p0, Lexl;->b:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lexl;->c:Lexm;

    if-nez v0, :cond_3

    new-instance v0, Lexm;

    invoke-direct {v0}, Lexm;-><init>()V

    iput-object v0, p0, Lexl;->c:Lexm;

    :cond_3
    iget-object v0, p0, Lexl;->c:Lexm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lexl;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lexl;->e:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lexl;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lexl;->g:[Lexm;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lexm;

    iget-object v3, p0, Lexl;->g:[Lexm;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lexl;->g:[Lexm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Lexl;->g:[Lexm;

    :goto_2
    iget-object v2, p0, Lexl;->g:[Lexm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Lexl;->g:[Lexm;

    new-instance v3, Lexm;

    invoke-direct {v3}, Lexm;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lexl;->g:[Lexm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lexl;->g:[Lexm;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lexl;->g:[Lexm;

    new-instance v3, Lexm;

    invoke-direct {v3}, Lexm;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lexl;->g:[Lexm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lexl;->h:Levt;

    if-nez v0, :cond_7

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Lexl;->h:Levt;

    :cond_7
    iget-object v0, p0, Lexl;->h:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 120
    iget-object v0, p0, Lexl;->b:Levt;

    if-eqz v0, :cond_0

    .line 121
    const/4 v0, 0x1

    iget-object v1, p0, Lexl;->b:Levt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 123
    :cond_0
    iget-object v0, p0, Lexl;->c:Lexm;

    if-eqz v0, :cond_1

    .line 124
    const/4 v0, 0x2

    iget-object v1, p0, Lexl;->c:Lexm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 126
    :cond_1
    iget-object v0, p0, Lexl;->d:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 127
    const/4 v0, 0x3

    iget-object v1, p0, Lexl;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 129
    :cond_2
    iget-object v0, p0, Lexl;->e:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 130
    const/4 v0, 0x4

    iget-object v1, p0, Lexl;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 132
    :cond_3
    iget-object v0, p0, Lexl;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 133
    const/4 v0, 0x5

    iget-object v1, p0, Lexl;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->c(II)V

    .line 135
    :cond_4
    iget-object v0, p0, Lexl;->g:[Lexm;

    if-eqz v0, :cond_6

    .line 136
    iget-object v1, p0, Lexl;->g:[Lexm;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 137
    if-eqz v3, :cond_5

    .line 138
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 136
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 142
    :cond_6
    iget-object v0, p0, Lexl;->h:Levt;

    if-eqz v0, :cond_7

    .line 143
    const/4 v0, 0x7

    iget-object v1, p0, Lexl;->h:Levt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 145
    :cond_7
    iget-object v0, p0, Lexl;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 147
    return-void
.end method
