.class public final Ldft;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldft;


# instance fields
.field public b:Ldfi;

.field public c:[Ldfu;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ldfu;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4462
    const/4 v0, 0x0

    new-array v0, v0, [Ldft;

    sput-object v0, Ldft;->a:[Ldft;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4463
    invoke-direct {p0}, Lepn;-><init>()V

    .line 4466
    iput-object v1, p0, Ldft;->b:Ldfi;

    .line 4469
    sget-object v0, Ldfu;->a:[Ldfu;

    iput-object v0, p0, Ldft;->c:[Ldfu;

    .line 4476
    iput-object v1, p0, Ldft;->f:Ldfu;

    .line 4463
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 4517
    iget-object v0, p0, Ldft;->b:Ldfi;

    if-eqz v0, :cond_7

    .line 4518
    const/4 v0, 0x1

    iget-object v2, p0, Ldft;->b:Ldfi;

    .line 4519
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4521
    :goto_0
    iget-object v2, p0, Ldft;->c:[Ldfu;

    if-eqz v2, :cond_1

    .line 4522
    iget-object v2, p0, Ldft;->c:[Ldfu;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 4523
    if-eqz v4, :cond_0

    .line 4524
    const/4 v5, 0x2

    .line 4525
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 4522
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4529
    :cond_1
    iget-object v1, p0, Ldft;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 4530
    const/4 v1, 0x3

    iget-object v2, p0, Ldft;->d:Ljava/lang/String;

    .line 4531
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4533
    :cond_2
    iget-object v1, p0, Ldft;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 4534
    const/4 v1, 0x4

    iget-object v2, p0, Ldft;->e:Ljava/lang/String;

    .line 4535
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4537
    :cond_3
    iget-object v1, p0, Ldft;->f:Ldfu;

    if-eqz v1, :cond_4

    .line 4538
    const/4 v1, 0x5

    iget-object v2, p0, Ldft;->f:Ldfu;

    .line 4539
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4541
    :cond_4
    iget-object v1, p0, Ldft;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 4542
    const/4 v1, 0x6

    iget-object v2, p0, Ldft;->g:Ljava/lang/String;

    .line 4543
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4545
    :cond_5
    iget-object v1, p0, Ldft;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 4546
    const/4 v1, 0x7

    iget-object v2, p0, Ldft;->h:Ljava/lang/String;

    .line 4547
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4549
    :cond_6
    iget-object v1, p0, Ldft;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4550
    iput v0, p0, Ldft;->cachedSize:I

    .line 4551
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4459
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldft;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldft;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldft;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldft;->b:Ldfi;

    if-nez v0, :cond_2

    new-instance v0, Ldfi;

    invoke-direct {v0}, Ldfi;-><init>()V

    iput-object v0, p0, Ldft;->b:Ldfi;

    :cond_2
    iget-object v0, p0, Ldft;->b:Ldfi;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldft;->c:[Ldfu;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldfu;

    iget-object v3, p0, Ldft;->c:[Ldfu;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldft;->c:[Ldfu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldft;->c:[Ldfu;

    :goto_2
    iget-object v2, p0, Ldft;->c:[Ldfu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldft;->c:[Ldfu;

    new-instance v3, Ldfu;

    invoke-direct {v3}, Ldfu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldft;->c:[Ldfu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldft;->c:[Ldfu;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldft;->c:[Ldfu;

    new-instance v3, Ldfu;

    invoke-direct {v3}, Ldfu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldft;->c:[Ldfu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldft;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldft;->e:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Ldft;->f:Ldfu;

    if-nez v0, :cond_6

    new-instance v0, Ldfu;

    invoke-direct {v0}, Ldfu;-><init>()V

    iput-object v0, p0, Ldft;->f:Ldfu;

    :cond_6
    iget-object v0, p0, Ldft;->f:Ldfu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldft;->g:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldft;->h:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 4485
    iget-object v0, p0, Ldft;->b:Ldfi;

    if-eqz v0, :cond_0

    .line 4486
    const/4 v0, 0x1

    iget-object v1, p0, Ldft;->b:Ldfi;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 4488
    :cond_0
    iget-object v0, p0, Ldft;->c:[Ldfu;

    if-eqz v0, :cond_2

    .line 4489
    iget-object v1, p0, Ldft;->c:[Ldfu;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 4490
    if-eqz v3, :cond_1

    .line 4491
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 4489
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4495
    :cond_2
    iget-object v0, p0, Ldft;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 4496
    const/4 v0, 0x3

    iget-object v1, p0, Ldft;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4498
    :cond_3
    iget-object v0, p0, Ldft;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 4499
    const/4 v0, 0x4

    iget-object v1, p0, Ldft;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4501
    :cond_4
    iget-object v0, p0, Ldft;->f:Ldfu;

    if-eqz v0, :cond_5

    .line 4502
    const/4 v0, 0x5

    iget-object v1, p0, Ldft;->f:Ldfu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 4504
    :cond_5
    iget-object v0, p0, Ldft;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 4505
    const/4 v0, 0x6

    iget-object v1, p0, Ldft;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4507
    :cond_6
    iget-object v0, p0, Ldft;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 4508
    const/4 v0, 0x7

    iget-object v1, p0, Ldft;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4510
    :cond_7
    iget-object v0, p0, Ldft;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 4512
    return-void
.end method
