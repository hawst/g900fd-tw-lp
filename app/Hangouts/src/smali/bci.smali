.class public final Lbci;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final d:Ljava/lang/Object;

.field private static e:Lbci;

.field private static final f:[I


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lbci;->d:Ljava/lang/Object;

    .line 94
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lbci;->f:[I

    return-void

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x0
    .end array-data
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v1, 0x0

    .line 43
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    .line 46
    :try_start_0
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 47
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 46
    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 49
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 50
    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    :goto_0
    invoke-static {v2}, Lbci;->a(Landroid/content/Context;)I

    move-result v2

    iput v2, p0, Lbci;->c:I

    .line 58
    if-nez v1, :cond_0

    .line 59
    const-string v1, "(unk)"

    .line 62
    :cond_0
    iput-object v1, p0, Lbci;->a:Ljava/lang/String;

    .line 63
    int-to-long v0, v0

    iput-wide v0, p0, Lbci;->b:J

    .line 64
    return-void

    .line 51
    :catch_0
    move-exception v0

    .line 52
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "couldn\'t get package info "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    .line 53
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;)I
    .locals 7

    .prologue
    .line 110
    const-class v0, Lbch;

    .line 111
    invoke-static {p0, v0}, Lcyj;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v1

    .line 112
    const/4 v0, 0x3

    .line 113
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbch;

    .line 114
    invoke-interface {v0}, Lbch;->a()I

    move-result v0

    .line 117
    sget-object v4, Lbci;->f:[I

    array-length v5, v4

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v5, :cond_0

    aget v6, v4, v2

    .line 118
    if-ne v6, v0, :cond_1

    move v1, v0

    .line 120
    goto :goto_0

    .line 121
    :cond_1
    if-eq v6, v1, :cond_0

    .line 122
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 126
    :cond_2
    return v1
.end method

.method public static a()Lbci;
    .locals 2

    .prologue
    .line 32
    sget-object v1, Lbci;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 33
    :try_start_0
    sget-object v0, Lbci;->e:Lbci;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lbci;

    invoke-direct {v0}, Lbci;-><init>()V

    sput-object v0, Lbci;->e:Lbci;

    .line 36
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 37
    sget-object v0, Lbci;->e:Lbci;

    return-object v0

    .line 36
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 77
    iget v1, p0, Lbci;->c:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ldpv;
    .locals 3

    .prologue
    .line 81
    new-instance v0, Ldpv;

    invoke-direct {v0}, Ldpv;-><init>()V

    .line 82
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldpv;->b:Ljava/lang/Integer;

    .line 83
    iget v1, p0, Lbci;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldpv;->c:Ljava/lang/Integer;

    .line 84
    iget-object v1, p0, Lbci;->a:Ljava/lang/String;

    iput-object v1, v0, Ldpv;->d:Ljava/lang/String;

    .line 85
    iget-wide v1, p0, Lbci;->b:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Ldpv;->e:Ljava/lang/Long;

    .line 86
    sget-object v1, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    iput-object v1, v0, Ldpv;->f:Ljava/lang/String;

    .line 87
    sget-object v1, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    iput-object v1, v0, Ldpv;->g:Ljava/lang/String;

    .line 88
    return-object v0
.end method
