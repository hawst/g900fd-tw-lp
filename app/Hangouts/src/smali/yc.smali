.class public final Lyc;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Lyj;

.field private b:Ljava/lang/String;

.field private final c:Lbyz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbyz",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Lbyz;

    invoke-direct {v0}, Lbyz;-><init>()V

    iput-object v0, p0, Lyc;->c:Lbyz;

    .line 40
    return-void
.end method

.method public static a(Lyj;Landroid/database/Cursor;)Lbdh;
    .locals 12

    .prologue
    .line 159
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 160
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 161
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 162
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 163
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 164
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 165
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 166
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 167
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 169
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 170
    const/4 v10, 0x0

    .line 171
    const/16 v0, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    sget-object v10, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 174
    :cond_0
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 176
    invoke-static/range {v0 .. v10}, Lbdh;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lbdh;

    move-result-object v0

    .line 180
    invoke-static {p0, v0, v11}, Laal;->a(Lyj;Lbdh;Ljava/lang/String;)V

    .line 181
    return-object v0
.end method

.method public static a(Lyj;Ljava/lang/String;I)Ldg;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyj;",
            "Ljava/lang/String;",
            "I)",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 97
    invoke-virtual {p0}, Lyj;->c()Lbdk;

    move-result-object v0

    iget-object v0, v0, Lbdk;->b:Ljava/lang/String;

    .line 98
    sget v1, Lyd;->a:I

    if-ne p2, v1, :cond_0

    .line 99
    const-string v5, "(chat_id!=? OR chat_id IS NULL)  AND active=1"

    .line 100
    const/4 v1, 0x1

    new-array v6, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v0, v6, v1

    .line 101
    const-string v7, "first_name ASC"

    .line 104
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 105
    const-string v1, "\\|"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 106
    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Lyj;Ljava/util/ArrayList;)Landroid/net/Uri;

    move-result-object v3

    .line 107
    new-instance v0, Lbaj;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    sget-object v4, Lye;->a:[Ljava/lang/String;

    move-object v2, p0

    invoke-direct/range {v0 .. v7}, Lbaj;-><init>(Landroid/content/Context;Lyj;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_0
    move-object v6, v7

    move-object v5, v7

    goto :goto_0
.end method

.method public static a(Lyj;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 88
    invoke-static {p0, p1}, Lcom/google/android/apps/hangouts/content/EsProvider;->f(Lyj;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 89
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 90
    return-void
.end method

.method public static b(Lyj;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 154
    sget v0, Lyd;->a:I

    invoke-static {p0, p1, v0}, Lyc;->b(Lyj;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private static b(Lyj;Ljava/lang/String;I)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 131
    invoke-virtual {p0}, Lyj;->c()Lbdk;

    move-result-object v0

    iget-object v0, v0, Lbdk;->b:Ljava/lang/String;

    .line 132
    sget v1, Lyd;->a:I

    if-ne p2, v1, :cond_4

    .line 133
    const-string v3, "(chat_id!=? OR chat_id IS NULL)  AND active=1"

    .line 134
    new-array v4, v2, [Ljava/lang/String;

    aput-object v0, v4, v7

    .line 136
    :goto_0
    invoke-static {p0, p1}, Lcom/google/android/apps/hangouts/content/EsProvider;->f(Lyj;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 139
    :try_start_0
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v8, "count(*) as count"

    aput-object v8, v2, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 141
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 142
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 145
    if-eqz v6, :cond_0

    .line 146
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 149
    :cond_0
    :goto_1
    return v0

    .line 145
    :cond_1
    if-eqz v6, :cond_2

    .line 146
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move v0, v7

    .line 149
    goto :goto_1

    .line 145
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 146
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :cond_4
    move-object v4, v6

    move-object v3, v6

    goto :goto_0
.end method

.method public static c(Lyj;Ljava/lang/String;)Lyc;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 186
    new-instance v6, Lyc;

    invoke-direct {v6}, Lyc;-><init>()V

    .line 187
    invoke-virtual {v6, p0, p1}, Lyc;->d(Lyj;Ljava/lang/String;)V

    .line 188
    sget v0, Lyd;->a:I

    iget-object v1, v6, Lyc;->a:Lyj;

    iget-object v2, v6, Lyc;->b:Ljava/lang/String;

    invoke-virtual {v1}, Lyj;->c()Lbdk;

    move-result-object v3

    iget-object v7, v3, Lbdk;->b:Ljava/lang/String;

    sget v3, Lyd;->a:I

    if-ne v0, v3, :cond_1

    const-string v3, "(chat_id!=? OR chat_id IS NULL)  AND active=1"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v7, v4, v0

    const-string v5, "first_name ASC"

    :goto_0
    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->f(Lyj;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lye;->a:[Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 189
    if-eqz v1, :cond_0

    .line 191
    :try_start_0
    sget v0, Lyd;->b:I

    invoke-virtual {v6, v1, v0}, Lyc;->a(Landroid/database/Cursor;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 193
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 196
    :cond_0
    return-object v6

    .line 193
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    move-object v4, v5

    move-object v3, v5

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/database/Cursor;I)I
    .locals 4

    .prologue
    .line 231
    iget-object v0, p0, Lyc;->c:Lbyz;

    invoke-virtual {v0}, Lbyz;->clear()V

    .line 232
    const/4 v0, 0x0

    .line 233
    if-eqz p1, :cond_2

    .line 234
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 236
    :cond_0
    iget-object v1, p0, Lyc;->a:Lyj;

    .line 237
    invoke-static {v1, p1}, Lyc;->a(Lyj;Landroid/database/Cursor;)Lbdh;

    move-result-object v2

    .line 238
    sget v1, Lyd;->b:I

    if-ne p2, v1, :cond_3

    const/4 v1, 0x1

    .line 240
    :goto_0
    if-eqz v1, :cond_1

    .line 241
    iget-object v1, p0, Lyc;->c:Lbyz;

    iget-object v3, v2, Lbdh;->b:Lbdk;

    invoke-virtual {v1, v3, v2}, Lbyz;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    add-int/lit8 v0, v0, 0x1

    .line 244
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 247
    :cond_2
    return v0

    .line 238
    :cond_3
    const/16 v1, 0xd

    .line 239
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    goto :goto_0
.end method

.method public a(I)Ldg;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 227
    iget-object v0, p0, Lyc;->a:Lyj;

    iget-object v1, p0, Lyc;->b:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lyc;->a(Lyj;Ljava/lang/String;I)Ldg;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lyc;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lbdk;)Z
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lyc;->c:Lbyz;

    invoke-virtual {v0, p1}, Lbyz;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b(Lbdk;)Lbdh;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lyc;->c:Lbyz;

    invoke-virtual {v0, p1}, Lbyz;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    return-object v0
.end method

.method public b()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251
    iget-object v0, p0, Lyc;->c:Lbyz;

    invoke-virtual {v0}, Lbyz;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lyc;->c:Lbyz;

    invoke-virtual {v0}, Lbyz;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Lbdk;)Z
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lyc;->a:Lyj;

    invoke-virtual {v0}, Lyj;->c()Lbdk;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbdk;->a(Lbdk;)Z

    move-result v0

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lyc;->c:Lbyz;

    invoke-virtual {v0}, Lbyz;->size()I

    move-result v0

    return v0
.end method

.method public d(Lbdk;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lyc;->c:Lbyz;

    invoke-virtual {v0, p1}, Lbyz;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 288
    if-eqz v0, :cond_0

    .line 289
    iget-object v0, v0, Lbdh;->h:Ljava/lang/String;

    .line 291
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Lyj;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 208
    const/4 v0, 0x0

    .line 209
    iget-object v2, p0, Lyc;->a:Lyj;

    if-eq v2, p1, :cond_0

    .line 210
    iput-object p1, p0, Lyc;->a:Lyj;

    move v0, v1

    .line 213
    :cond_0
    iget-object v2, p0, Lyc;->b:Ljava/lang/String;

    invoke-static {v2, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 215
    iput-object p2, p0, Lyc;->b:Ljava/lang/String;

    .line 217
    :goto_0
    if-eqz v1, :cond_1

    .line 218
    iget-object v0, p0, Lyc;->c:Lbyz;

    invoke-virtual {v0}, Lbyz;->clear()V

    .line 220
    :cond_1
    return-void

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method public e(Lbdk;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lyc;->c:Lbyz;

    invoke-virtual {v0, p1}, Lbyz;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 296
    if-eqz v0, :cond_0

    .line 297
    iget-object v0, v0, Lbdh;->e:Ljava/lang/String;

    .line 299
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 271
    iget-object v0, p0, Lyc;->c:Lbyz;

    invoke-virtual {v0}, Lbyz;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lyc;->c:Lbyz;

    invoke-virtual {v1}, Lbyz;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method
