.class public final Layu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Landroid/view/View;

.field final synthetic c:Landroid/view/View;

.field final synthetic d:Landroid/view/View;

.field final synthetic e:Lcom/google/android/apps/hangouts/phone/DebugActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/phone/DebugActivity;Ljava/lang/String;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 585
    iput-object p1, p0, Layu;->e:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    iput-object p2, p0, Layu;->a:Ljava/lang/String;

    iput-object p3, p0, Layu;->b:Landroid/view/View;

    iput-object p4, p0, Layu;->c:Landroid/view/View;

    iput-object p5, p0, Layu;->d:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v6, 0x8

    .line 588
    iget-object v0, p0, Layu;->e:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->e(Lcom/google/android/apps/hangouts/phone/DebugActivity;)Lyt;

    move-result-object v0

    invoke-virtual {v0}, Lyt;->e()Lzr;

    move-result-object v0

    const-string v1, "messages"

    const-string v3, "conversation_id=?"

    new-array v4, v8, [Ljava/lang/String;

    iget-object v5, p0, Layu;->a:Ljava/lang/String;

    aput-object v5, v4, v7

    const-string v5, "timestamp ASC"

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 597
    iget-object v0, p0, Layu;->e:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v3, Lf;->eZ:I

    invoke-virtual {v0, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 598
    iget-object v2, p0, Layu;->e:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    new-instance v3, Layv;

    invoke-direct {v3, p0}, Layv;-><init>(Layu;)V

    invoke-static {v2, v0, v1, v3}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->a(Lcom/google/android/apps/hangouts/phone/DebugActivity;Landroid/view/View;Landroid/database/Cursor;Ljava/lang/Runnable;)V

    .line 606
    iget-object v2, p0, Layu;->b:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 607
    iget-object v2, p0, Layu;->c:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 608
    iget-object v2, p0, Layu;->d:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 610
    const v2, 0x102000a

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 611
    iget-object v2, p0, Layu;->e:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    new-array v3, v8, [I

    const-string v4, "text"

    .line 612
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    aput v4, v3, v7

    .line 611
    invoke-static {v2, v1, v3}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->a(Lcom/google/android/apps/hangouts/phone/DebugActivity;Landroid/database/Cursor;[I)Landroid/widget/BaseAdapter;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 613
    iget-object v2, p0, Layu;->e:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v2, v1}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->a(Lcom/google/android/apps/hangouts/phone/DebugActivity;Landroid/database/Cursor;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 614
    return-void
.end method
