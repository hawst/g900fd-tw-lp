.class final Larm;
.super Lapo;
.source "PG"


# instance fields
.field final synthetic a:Laqv;


# direct methods
.method private constructor <init>(Laqv;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Larm;->a:Laqv;

    invoke-direct {p0}, Lapo;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Laqv;B)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1}, Larm;-><init>(Laqv;)V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Larm;->a:Laqv;

    invoke-virtual {v0}, Laqv;->b()V

    .line 74
    return-void
.end method

.method public a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 1

    .prologue
    .line 89
    instance-of v0, p1, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Larm;->a:Laqv;

    check-cast p1, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    invoke-virtual {v0, p1}, Laqv;->a(Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;)V

    .line 92
    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Larm;->a:Laqv;

    iget v0, v0, Laqv;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 67
    iget-object v0, p0, Larm;->a:Laqv;

    invoke-virtual {v0}, Laqv;->b()V

    .line 68
    iget-object v0, p0, Larm;->a:Laqv;

    invoke-virtual {v0}, Laqv;->f()V

    .line 69
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Larm;->a:Laqv;

    invoke-virtual {v0}, Laqv;->g()V

    .line 84
    iget-object v0, p0, Larm;->a:Laqv;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Laqv;->e(Z)V

    .line 85
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Larm;->a:Laqv;

    invoke-virtual {v0}, Laqv;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Laqv;->d()V

    .line 79
    :goto_0
    return-void

    .line 78
    :cond_0
    invoke-virtual {v0}, Laqv;->b()V

    invoke-virtual {v0}, Laqv;->k()V

    goto :goto_0
.end method

.method public onAudioUpdated(Lcom/google/android/libraries/hangouts/video/AudioDeviceState;Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/hangouts/video/AudioDeviceState;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/libraries/hangouts/video/AudioDevice;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 97
    if-eqz p2, :cond_0

    iget-object v0, p0, Larm;->a:Laqv;

    iget-object v0, v0, Laqv;->k:Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;

    if-eqz v0, :cond_0

    .line 98
    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v0

    if-le v0, v1, :cond_1

    move v0, v1

    .line 99
    :goto_0
    iget-object v3, p0, Larm;->a:Laqv;

    iget-object v3, v3, Laqv;->k:Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->setVisibility(I)V

    .line 101
    :cond_0
    iget-object v0, p0, Larm;->a:Laqv;

    sget-object v3, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->EARPIECE_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    if-ne p1, v3, :cond_3

    :goto_2
    iput-boolean v1, v0, Laqv;->l:Z

    .line 104
    iget-object v0, p0, Larm;->a:Laqv;

    invoke-virtual {v0}, Laqv;->f()V

    .line 105
    return-void

    :cond_1
    move v0, v2

    .line 98
    goto :goto_0

    .line 99
    :cond_2
    const/16 v0, 0x8

    goto :goto_1

    :cond_3
    move v1, v2

    .line 101
    goto :goto_2
.end method
