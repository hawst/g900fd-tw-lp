.class public final Ldug;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldug;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21962
    const/4 v0, 0x0

    new-array v0, v0, [Ldug;

    sput-object v0, Ldug;->a:[Ldug;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21963
    invoke-direct {p0}, Lepn;-><init>()V

    .line 21971
    const/4 v0, 0x0

    iput-object v0, p0, Ldug;->b:Ljava/lang/Integer;

    .line 21963
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 21995
    const/4 v0, 0x0

    .line 21996
    iget-object v1, p0, Ldug;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 21997
    const/4 v0, 0x1

    iget-object v1, p0, Ldug;->b:Ljava/lang/Integer;

    .line 21998
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 22000
    :cond_0
    iget-object v1, p0, Ldug;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 22001
    const/4 v1, 0x2

    iget-object v2, p0, Ldug;->c:Ljava/lang/String;

    .line 22002
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22004
    :cond_1
    iget-object v1, p0, Ldug;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 22005
    const/4 v1, 0x3

    iget-object v2, p0, Ldug;->d:Ljava/lang/String;

    .line 22006
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22008
    :cond_2
    iget-object v1, p0, Ldug;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22009
    iput v0, p0, Ldug;->cachedSize:I

    .line 22010
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 21959
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldug;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldug;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldug;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldug;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldug;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldug;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldug;->d:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 21980
    iget-object v0, p0, Ldug;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 21981
    const/4 v0, 0x1

    iget-object v1, p0, Ldug;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 21983
    :cond_0
    iget-object v0, p0, Ldug;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 21984
    const/4 v0, 0x2

    iget-object v1, p0, Ldug;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 21986
    :cond_1
    iget-object v0, p0, Ldug;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 21987
    const/4 v0, 0x3

    iget-object v1, p0, Ldug;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 21989
    :cond_2
    iget-object v0, p0, Ldug;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 21991
    return-void
.end method
