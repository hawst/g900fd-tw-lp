.class public final Ldpp;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldpp;


# instance fields
.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Integer;

.field public e:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14088
    const/4 v0, 0x0

    new-array v0, v0, [Ldpp;

    sput-object v0, Ldpp;->a:[Ldpp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14089
    invoke-direct {p0}, Lepn;-><init>()V

    .line 14103
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Ldpp;->e:[I

    .line 14089
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 14129
    iget-object v0, p0, Ldpp;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 14130
    const/4 v0, 0x1

    iget-object v2, p0, Ldpp;->b:Ljava/lang/Boolean;

    .line 14131
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 14133
    :goto_0
    iget-object v2, p0, Ldpp;->c:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    .line 14134
    const/4 v2, 0x2

    iget-object v3, p0, Ldpp;->c:Ljava/lang/Boolean;

    .line 14135
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 14137
    :cond_0
    iget-object v2, p0, Ldpp;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 14138
    const/4 v2, 0x3

    iget-object v3, p0, Ldpp;->d:Ljava/lang/Integer;

    .line 14139
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 14141
    :cond_1
    iget-object v2, p0, Ldpp;->e:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Ldpp;->e:[I

    array-length v2, v2

    if-lez v2, :cond_3

    .line 14143
    iget-object v3, p0, Ldpp;->e:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_2

    aget v5, v3, v1

    .line 14145
    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 14143
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 14147
    :cond_2
    add-int/2addr v0, v2

    .line 14148
    iget-object v1, p0, Ldpp;->e:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 14150
    :cond_3
    iget-object v1, p0, Ldpp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14151
    iput v0, p0, Ldpp;->cachedSize:I

    .line 14152
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 14085
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldpp;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldpp;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldpp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldpp;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldpp;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpp;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x20

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldpp;->e:[I

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [I

    iget-object v2, p0, Ldpp;->e:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldpp;->e:[I

    :goto_1
    iget-object v1, p0, Ldpp;->e:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Ldpp;->e:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Ldpp;->e:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 14108
    iget-object v0, p0, Ldpp;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 14109
    const/4 v0, 0x1

    iget-object v1, p0, Ldpp;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 14111
    :cond_0
    iget-object v0, p0, Ldpp;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 14112
    const/4 v0, 0x2

    iget-object v1, p0, Ldpp;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 14114
    :cond_1
    iget-object v0, p0, Ldpp;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 14115
    const/4 v0, 0x3

    iget-object v1, p0, Ldpp;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 14117
    :cond_2
    iget-object v0, p0, Ldpp;->e:[I

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldpp;->e:[I

    array-length v0, v0

    if-lez v0, :cond_3

    .line 14118
    iget-object v1, p0, Ldpp;->e:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget v3, v1, v0

    .line 14119
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 14118
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 14122
    :cond_3
    iget-object v0, p0, Ldpp;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 14124
    return-void
.end method
