.class public final Ldns;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldns;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1350
    const/4 v0, 0x0

    new-array v0, v0, [Ldns;

    sput-object v0, Ldns;->a:[Ldns;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1351
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1390
    const/4 v0, 0x0

    .line 1391
    iget-object v1, p0, Ldns;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1392
    const/4 v0, 0x1

    iget-object v1, p0, Ldns;->b:Ljava/lang/Integer;

    .line 1393
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1395
    :cond_0
    iget-object v1, p0, Ldns;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1396
    const/4 v1, 0x2

    iget-object v2, p0, Ldns;->c:Ljava/lang/String;

    .line 1397
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1399
    :cond_1
    const/4 v1, 0x3

    iget-object v2, p0, Ldns;->d:Ljava/lang/String;

    .line 1400
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1401
    iget-object v1, p0, Ldns;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 1402
    const/4 v1, 0x4

    iget-object v2, p0, Ldns;->e:Ljava/lang/Boolean;

    .line 1403
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1405
    :cond_2
    iget-object v1, p0, Ldns;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1406
    const/4 v1, 0x5

    iget-object v2, p0, Ldns;->f:Ljava/lang/String;

    .line 1407
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1409
    :cond_3
    iget-object v1, p0, Ldns;->g:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1410
    const/4 v1, 0x6

    iget-object v2, p0, Ldns;->g:Ljava/lang/String;

    .line 1411
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1413
    :cond_4
    iget-object v1, p0, Ldns;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1414
    iput v0, p0, Ldns;->cachedSize:I

    .line 1415
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1347
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldns;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldns;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldns;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldns;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldns;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldns;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldns;->e:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldns;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldns;->g:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1368
    iget-object v0, p0, Ldns;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1369
    const/4 v0, 0x1

    iget-object v1, p0, Ldns;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1371
    :cond_0
    iget-object v0, p0, Ldns;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1372
    const/4 v0, 0x2

    iget-object v1, p0, Ldns;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1374
    :cond_1
    const/4 v0, 0x3

    iget-object v1, p0, Ldns;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1375
    iget-object v0, p0, Ldns;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1376
    const/4 v0, 0x4

    iget-object v1, p0, Ldns;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 1378
    :cond_2
    iget-object v0, p0, Ldns;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1379
    const/4 v0, 0x5

    iget-object v1, p0, Ldns;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1381
    :cond_3
    iget-object v0, p0, Ldns;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1382
    const/4 v0, 0x6

    iget-object v1, p0, Ldns;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1384
    :cond_4
    iget-object v0, p0, Ldns;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1386
    return-void
.end method
