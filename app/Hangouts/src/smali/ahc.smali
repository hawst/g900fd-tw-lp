.class public final Lahc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lahc;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:Lyj;

.field public d:I

.field public e:Lyh;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Z

.field public i:J

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Z

.field public m:I

.field public n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3072
    new-instance v0, Lahd;

    invoke-direct {v0}, Lahd;-><init>()V

    sput-object v0, Lahc;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2998
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2988
    iput v0, p0, Lahc;->d:I

    .line 3026
    iput v0, p0, Lahc;->m:I

    .line 2999
    iput-object v1, p0, Lahc;->a:Ljava/lang/String;

    .line 3000
    iput-object v1, p0, Lahc;->c:Lyj;

    .line 3001
    iput v0, p0, Lahc;->d:I

    .line 3002
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lyj;I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 3007
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2988
    iput v0, p0, Lahc;->d:I

    .line 3026
    iput v0, p0, Lahc;->m:I

    .line 3008
    iput-object p1, p0, Lahc;->a:Ljava/lang/String;

    .line 3009
    iput-object p2, p0, Lahc;->c:Lyj;

    .line 3010
    iput p3, p0, Lahc;->d:I

    .line 3011
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lahc;->i:J

    .line 3012
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lyj;II)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 3018
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2988
    iput v0, p0, Lahc;->d:I

    .line 3026
    iput v0, p0, Lahc;->m:I

    .line 3019
    iput-object p1, p0, Lahc;->a:Ljava/lang/String;

    .line 3020
    iput-object p2, p0, Lahc;->c:Lyj;

    .line 3021
    iput p3, p0, Lahc;->d:I

    .line 3022
    iput p4, p0, Lahc;->m:I

    .line 3023
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lahc;->i:J

    .line 3024
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 3048
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 3030
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "[%s;%s;%d;%d;%s;%s;%s;%s;%s]"

    const/16 v0, 0x9

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v4, p0, Lahc;->a:Ljava/lang/String;

    aput-object v4, v3, v0

    const/4 v0, 0x1

    iget-object v4, p0, Lahc;->c:Lyj;

    aput-object v4, v3, v0

    const/4 v0, 0x2

    iget v4, p0, Lahc;->d:I

    .line 3035
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x3

    iget v4, p0, Lahc;->m:I

    .line 3036
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x4

    iget-object v4, p0, Lahc;->f:Ljava/lang/String;

    aput-object v4, v3, v0

    const/4 v0, 0x5

    iget-object v4, p0, Lahc;->g:Ljava/lang/String;

    aput-object v4, v3, v0

    const/4 v4, 0x6

    iget-object v0, p0, Lahc;->e:Lyh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lahc;->e:Lyh;

    .line 3039
    invoke-virtual {v0}, Lyh;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v3, v4

    const/4 v4, 0x7

    iget-object v0, p0, Lahc;->j:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lahc;->j:Ljava/lang/String;

    .line 3040
    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    aput-object v0, v3, v4

    const/16 v4, 0x8

    iget-object v0, p0, Lahc;->k:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lahc;->k:Ljava/lang/String;

    .line 3041
    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    aput-object v0, v3, v4

    .line 3030
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 3039
    :cond_0
    const-string v0, "no-drafts"

    goto :goto_0

    .line 3040
    :cond_1
    const-string v0, "no-chatRingtoneUri"

    goto :goto_1

    .line 3041
    :cond_2
    const-string v0, "no-hangoutRingtoneUri"

    goto :goto_2
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3053
    iget-object v0, p0, Lahc;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3054
    new-array v0, v3, [Z

    iget-boolean v1, p0, Lahc;->b:Z

    aput-boolean v1, v0, v2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 3060
    iget-object v0, p0, Lahc;->c:Lyj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lahc;->c:Lyj;

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3061
    iget v0, p0, Lahc;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3062
    iget v0, p0, Lahc;->m:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3063
    iget-object v0, p0, Lahc;->e:Lyh;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 3064
    iget-object v0, p0, Lahc;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3065
    iget-object v0, p0, Lahc;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3066
    const/4 v0, 0x2

    new-array v0, v0, [Z

    iget-boolean v1, p0, Lahc;->h:Z

    aput-boolean v1, v0, v2

    iget-boolean v1, p0, Lahc;->n:Z

    aput-boolean v1, v0, v3

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 3067
    iget-wide v0, p0, Lahc;->i:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 3068
    iget-object v0, p0, Lahc;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3069
    iget-object v0, p0, Lahc;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3070
    return-void

    .line 3060
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
