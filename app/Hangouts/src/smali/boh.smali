.class public final Lboh;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lyj;

.field final synthetic b:I

.field final synthetic c:Z

.field final synthetic d:J


# direct methods
.method public constructor <init>(Lyj;IZJ)V
    .locals 0

    .prologue
    .line 1665
    iput-object p1, p0, Lboh;->a:Lyj;

    iput p2, p0, Lboh;->b:I

    iput-boolean p3, p0, Lboh;->c:Z

    iput-wide p4, p0, Lboh;->d:J

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1668
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    .line 1669
    const-string v0, "alarm"

    .line 1670
    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 1671
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.apps.hangouts.UPDATE_NOTIFICATION"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1672
    const-string v3, "account_name"

    iget-object v4, p0, Lboh;->a:Lyj;

    invoke-virtual {v4}, Lyj;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1673
    const-string v3, "op"

    const/16 v4, 0x5d

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1674
    const-string v3, "notification_type"

    iget v4, p0, Lboh;->b:I

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1675
    const-string v3, "is_sms"

    iget-boolean v4, p0, Lboh;->c:Z

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1676
    iget-object v3, p0, Lboh;->a:Lyj;

    const/4 v4, 0x1

    const/16 v5, 0x64

    .line 1677
    invoke-static {v3, v4, v5, v7}, Lbzb;->a(Lyj;IILjava/lang/String;)I

    move-result v3

    const/high16 v4, 0x10000000

    .line 1676
    invoke-static {v1, v3, v2, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 1681
    const/4 v2, 0x2

    .line 1682
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    iget-wide v5, p0, Lboh;->d:J

    add-long/2addr v3, v5

    .line 1681
    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 1683
    return-object v7
.end method
