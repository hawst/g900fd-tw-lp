.class public final Ldww;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldww;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Lduf;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/Long;

.field public f:Ldpk;

.field public g:[B

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25496
    const/4 v0, 0x0

    new-array v0, v0, [Ldww;

    sput-object v0, Ldww;->a:[Ldww;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25497
    invoke-direct {p0}, Lepn;-><init>()V

    .line 25500
    iput-object v0, p0, Ldww;->b:Ljava/lang/Integer;

    .line 25503
    iput-object v0, p0, Ldww;->c:Lduf;

    .line 25510
    iput-object v0, p0, Ldww;->f:Ldpk;

    .line 25497
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 25551
    const/4 v0, 0x0

    .line 25552
    iget-object v1, p0, Ldww;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 25553
    const/4 v0, 0x1

    iget-object v1, p0, Ldww;->b:Ljava/lang/Integer;

    .line 25554
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 25556
    :cond_0
    iget-object v1, p0, Ldww;->g:[B

    if-eqz v1, :cond_1

    .line 25557
    const/4 v1, 0x2

    iget-object v2, p0, Ldww;->g:[B

    .line 25558
    invoke-static {v1, v2}, Lepl;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 25560
    :cond_1
    iget-object v1, p0, Ldww;->d:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 25561
    const/4 v1, 0x3

    iget-object v2, p0, Ldww;->d:Ljava/lang/Long;

    .line 25562
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 25564
    :cond_2
    iget-object v1, p0, Ldww;->c:Lduf;

    if-eqz v1, :cond_3

    .line 25565
    const/4 v1, 0x4

    iget-object v2, p0, Ldww;->c:Lduf;

    .line 25566
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25568
    :cond_3
    iget-object v1, p0, Ldww;->e:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 25569
    const/4 v1, 0x5

    iget-object v2, p0, Ldww;->e:Ljava/lang/Long;

    .line 25570
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 25572
    :cond_4
    iget-object v1, p0, Ldww;->f:Ldpk;

    if-eqz v1, :cond_5

    .line 25573
    const/4 v1, 0x6

    iget-object v2, p0, Ldww;->f:Ldpk;

    .line 25574
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25576
    :cond_5
    iget-object v1, p0, Ldww;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 25577
    const/4 v1, 0x7

    iget-object v2, p0, Ldww;->h:Ljava/lang/String;

    .line 25578
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25580
    :cond_6
    iget-object v1, p0, Ldww;->i:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 25581
    const/16 v1, 0x8

    iget-object v2, p0, Ldww;->i:Ljava/lang/String;

    .line 25582
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25584
    :cond_7
    iget-object v1, p0, Ldww;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25585
    iput v0, p0, Ldww;->cachedSize:I

    .line 25586
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 25493
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldww;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldww;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldww;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldww;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldww;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Ldww;->g:[B

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldww;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldww;->c:Lduf;

    if-nez v0, :cond_4

    new-instance v0, Lduf;

    invoke-direct {v0}, Lduf;-><init>()V

    iput-object v0, p0, Ldww;->c:Lduf;

    :cond_4
    iget-object v0, p0, Ldww;->c:Lduf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldww;->e:Ljava/lang/Long;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ldww;->f:Ldpk;

    if-nez v0, :cond_5

    new-instance v0, Ldpk;

    invoke-direct {v0}, Ldpk;-><init>()V

    iput-object v0, p0, Ldww;->f:Ldpk;

    :cond_5
    iget-object v0, p0, Ldww;->f:Ldpk;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldww;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldww;->i:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 25521
    iget-object v0, p0, Ldww;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 25522
    const/4 v0, 0x1

    iget-object v1, p0, Ldww;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 25524
    :cond_0
    iget-object v0, p0, Ldww;->g:[B

    if-eqz v0, :cond_1

    .line 25525
    const/4 v0, 0x2

    iget-object v1, p0, Ldww;->g:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 25527
    :cond_1
    iget-object v0, p0, Ldww;->d:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 25528
    const/4 v0, 0x3

    iget-object v1, p0, Ldww;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 25530
    :cond_2
    iget-object v0, p0, Ldww;->c:Lduf;

    if-eqz v0, :cond_3

    .line 25531
    const/4 v0, 0x4

    iget-object v1, p0, Ldww;->c:Lduf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 25533
    :cond_3
    iget-object v0, p0, Ldww;->e:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 25534
    const/4 v0, 0x5

    iget-object v1, p0, Ldww;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 25536
    :cond_4
    iget-object v0, p0, Ldww;->f:Ldpk;

    if-eqz v0, :cond_5

    .line 25537
    const/4 v0, 0x6

    iget-object v1, p0, Ldww;->f:Ldpk;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 25539
    :cond_5
    iget-object v0, p0, Ldww;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 25540
    const/4 v0, 0x7

    iget-object v1, p0, Ldww;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 25542
    :cond_6
    iget-object v0, p0, Ldww;->i:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 25543
    const/16 v0, 0x8

    iget-object v1, p0, Ldww;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 25545
    :cond_7
    iget-object v0, p0, Ldww;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 25547
    return-void
.end method
