.class public final Ldcm;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldcm;


# instance fields
.field public b:Ldcr;

.field public c:Lepu;

.field public d:Ldco;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/String;

.field public g:Ldcn;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/Boolean;

.field public j:Ljava/lang/Integer;

.field public k:Ldcq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 488
    const/4 v0, 0x0

    new-array v0, v0, [Ldcm;

    sput-object v0, Ldcm;->a:[Ldcm;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 489
    invoke-direct {p0}, Lepn;-><init>()V

    .line 637
    iput-object v0, p0, Ldcm;->b:Ldcr;

    .line 640
    iput-object v0, p0, Ldcm;->c:Lepu;

    .line 643
    iput-object v0, p0, Ldcm;->d:Ldco;

    .line 650
    iput-object v0, p0, Ldcm;->g:Ldcn;

    .line 657
    iput-object v0, p0, Ldcm;->j:Ljava/lang/Integer;

    .line 660
    iput-object v0, p0, Ldcm;->k:Ldcq;

    .line 489
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 701
    const/4 v0, 0x0

    .line 702
    iget-object v1, p0, Ldcm;->b:Ldcr;

    if-eqz v1, :cond_0

    .line 703
    const/4 v0, 0x1

    iget-object v1, p0, Ldcm;->b:Ldcr;

    .line 704
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 706
    :cond_0
    iget-object v1, p0, Ldcm;->c:Lepu;

    if-eqz v1, :cond_1

    .line 707
    const/4 v1, 0x2

    iget-object v2, p0, Ldcm;->c:Lepu;

    .line 708
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 710
    :cond_1
    iget-object v1, p0, Ldcm;->d:Ldco;

    if-eqz v1, :cond_2

    .line 711
    const/4 v1, 0x3

    iget-object v2, p0, Ldcm;->d:Ldco;

    .line 712
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 714
    :cond_2
    iget-object v1, p0, Ldcm;->e:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 715
    const/4 v1, 0x4

    iget-object v2, p0, Ldcm;->e:Ljava/lang/Long;

    .line 716
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 718
    :cond_3
    iget-object v1, p0, Ldcm;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 719
    const/4 v1, 0x5

    iget-object v2, p0, Ldcm;->f:Ljava/lang/String;

    .line 720
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 722
    :cond_4
    iget-object v1, p0, Ldcm;->g:Ldcn;

    if-eqz v1, :cond_5

    .line 723
    const/4 v1, 0x6

    iget-object v2, p0, Ldcm;->g:Ldcn;

    .line 724
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 726
    :cond_5
    iget-object v1, p0, Ldcm;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 727
    const/4 v1, 0x7

    iget-object v2, p0, Ldcm;->h:Ljava/lang/String;

    .line 728
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 730
    :cond_6
    iget-object v1, p0, Ldcm;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 731
    const/16 v1, 0x8

    iget-object v2, p0, Ldcm;->i:Ljava/lang/Boolean;

    .line 732
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 734
    :cond_7
    iget-object v1, p0, Ldcm;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 735
    const/16 v1, 0x9

    iget-object v2, p0, Ldcm;->j:Ljava/lang/Integer;

    .line 736
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 738
    :cond_8
    iget-object v1, p0, Ldcm;->k:Ldcq;

    if-eqz v1, :cond_9

    .line 739
    const/16 v1, 0xa

    iget-object v2, p0, Ldcm;->k:Ldcq;

    .line 740
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 742
    :cond_9
    iget-object v1, p0, Ldcm;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 743
    iput v0, p0, Ldcm;->cachedSize:I

    .line 744
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 485
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldcm;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldcm;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldcm;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldcm;->b:Ldcr;

    if-nez v0, :cond_2

    new-instance v0, Ldcr;

    invoke-direct {v0}, Ldcr;-><init>()V

    iput-object v0, p0, Ldcm;->b:Ldcr;

    :cond_2
    iget-object v0, p0, Ldcm;->b:Ldcr;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldcm;->c:Lepu;

    if-nez v0, :cond_3

    new-instance v0, Lepu;

    invoke-direct {v0}, Lepu;-><init>()V

    iput-object v0, p0, Ldcm;->c:Lepu;

    :cond_3
    iget-object v0, p0, Ldcm;->c:Lepu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldcm;->d:Ldco;

    if-nez v0, :cond_4

    new-instance v0, Ldco;

    invoke-direct {v0}, Ldco;-><init>()V

    iput-object v0, p0, Ldcm;->d:Ldco;

    :cond_4
    iget-object v0, p0, Ldcm;->d:Ldco;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldcm;->e:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldcm;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ldcm;->g:Ldcn;

    if-nez v0, :cond_5

    new-instance v0, Ldcn;

    invoke-direct {v0}, Ldcn;-><init>()V

    iput-object v0, p0, Ldcm;->g:Ldcn;

    :cond_5
    iget-object v0, p0, Ldcm;->g:Ldcn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldcm;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldcm;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_6

    const/4 v1, 0x1

    if-eq v0, v1, :cond_6

    const/4 v1, 0x2

    if-eq v0, v1, :cond_6

    const/4 v1, 0x3

    if-ne v0, v1, :cond_7

    :cond_6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldcm;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_7
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldcm;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Ldcm;->k:Ldcq;

    if-nez v0, :cond_8

    new-instance v0, Ldcq;

    invoke-direct {v0}, Ldcq;-><init>()V

    iput-object v0, p0, Ldcm;->k:Ldcq;

    :cond_8
    iget-object v0, p0, Ldcm;->k:Ldcq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 665
    iget-object v0, p0, Ldcm;->b:Ldcr;

    if-eqz v0, :cond_0

    .line 666
    const/4 v0, 0x1

    iget-object v1, p0, Ldcm;->b:Ldcr;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 668
    :cond_0
    iget-object v0, p0, Ldcm;->c:Lepu;

    if-eqz v0, :cond_1

    .line 669
    const/4 v0, 0x2

    iget-object v1, p0, Ldcm;->c:Lepu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 671
    :cond_1
    iget-object v0, p0, Ldcm;->d:Ldco;

    if-eqz v0, :cond_2

    .line 672
    const/4 v0, 0x3

    iget-object v1, p0, Ldcm;->d:Ldco;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 674
    :cond_2
    iget-object v0, p0, Ldcm;->e:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 675
    const/4 v0, 0x4

    iget-object v1, p0, Ldcm;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 677
    :cond_3
    iget-object v0, p0, Ldcm;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 678
    const/4 v0, 0x5

    iget-object v1, p0, Ldcm;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 680
    :cond_4
    iget-object v0, p0, Ldcm;->g:Ldcn;

    if-eqz v0, :cond_5

    .line 681
    const/4 v0, 0x6

    iget-object v1, p0, Ldcm;->g:Ldcn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 683
    :cond_5
    iget-object v0, p0, Ldcm;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 684
    const/4 v0, 0x7

    iget-object v1, p0, Ldcm;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 686
    :cond_6
    iget-object v0, p0, Ldcm;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 687
    const/16 v0, 0x8

    iget-object v1, p0, Ldcm;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 689
    :cond_7
    iget-object v0, p0, Ldcm;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 690
    const/16 v0, 0x9

    iget-object v1, p0, Ldcm;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 692
    :cond_8
    iget-object v0, p0, Ldcm;->k:Ldcq;

    if-eqz v0, :cond_9

    .line 693
    const/16 v0, 0xa

    iget-object v1, p0, Ldcm;->k:Ldcq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 695
    :cond_9
    iget-object v0, p0, Ldcm;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 697
    return-void
.end method
