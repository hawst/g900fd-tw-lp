.class public abstract Lepn;
.super Lepr;
.source "PG"


# instance fields
.field public unknownFieldData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Leps;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lepr;-><init>()V

    return-void
.end method


# virtual methods
.method public getExtension(Lepo;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lepo",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lepn;->unknownFieldData:Ljava/util/List;

    invoke-static {p1, v0}, Lept;->a(Lepo;Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lepn;->unknownFieldData:Ljava/util/List;

    invoke-static {v0}, Lept;->a(Ljava/util/List;)I

    move-result v0

    .line 51
    iput v0, p0, Lepn;->cachedSize:I

    .line 52
    return v0
.end method

.method public setExtension(Lepo;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lepo",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lepn;->unknownFieldData:Ljava/util/List;

    if-nez v0, :cond_0

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lepn;->unknownFieldData:Ljava/util/List;

    .line 69
    :cond_0
    iget-object v0, p0, Lepn;->unknownFieldData:Ljava/util/List;

    invoke-static {p1, p2, v0}, Lept;->a(Lepo;Ljava/lang/Object;Ljava/util/List;)V

    .line 70
    return-void
.end method
