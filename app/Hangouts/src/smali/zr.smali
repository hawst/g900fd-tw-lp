.class public final Lzr;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static b:Lzr;

.field private static c:Z

.field private static d:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/Stack",
            "<",
            "Lzt;",
            ">;>;"
        }
    .end annotation
.end field

.field private static e:[Ljava/lang/String;


# instance fields
.field private final a:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2802
    new-instance v0, Lzs;

    invoke-direct {v0}, Lzs;-><init>()V

    sput-object v0, Lzr;->d:Ljava/lang/ThreadLocal;

    .line 2810
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "took %d ms to %s"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "   took %d ms to %s"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "      took %d ms to %s"

    aput-object v2, v0, v1

    sput-object v0, Lzr;->e:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 2823
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2824
    invoke-static {}, Lzo;->h()Z

    move-result v0

    sput-boolean v0, Lzr;->c:Z

    .line 2825
    iput-object p1, p0, Lzr;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 2826
    return-void
.end method

.method synthetic constructor <init>(Landroid/database/sqlite/SQLiteDatabase;B)V
    .locals 0

    .prologue
    .line 2791
    invoke-direct {p0, p1}, Lzr;-><init>(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public static a(Landroid/database/sqlite/SQLiteQueryBuilder;Lzr;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 11

    .prologue
    .line 2983
    invoke-static {}, Lzo;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2984
    invoke-virtual {p1}, Lzr;->e()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-static/range {v0 .. v6}, Lzr;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2987
    :cond_0
    invoke-static {}, Lzo;->j()V

    .line 2988
    const-wide/16 v0, 0x0

    .line 2989
    sget-boolean v2, Lzr;->c:Z

    if-eqz v2, :cond_2

    .line 2990
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    move-wide v9, v0

    .line 2992
    :goto_0
    iget-object v1, p1, Lzr;->a:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2994
    sget-boolean v1, Lzr;->c:Z

    if-eqz v1, :cond_1

    .line 2995
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "query %s with %s ==> %d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 2998
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteQueryBuilder;->getTables()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p3, v3, v4

    const/4 v4, 0x2

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 2997
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2995
    invoke-static {v9, v10, v1}, Lzr;->a(JLjava/lang/String;)V

    .line 3001
    :cond_1
    return-object v0

    :cond_2
    move-wide v9, v0

    goto :goto_0
.end method

.method public static declared-synchronized a(Landroid/database/sqlite/SQLiteDatabase;)Lzr;
    .locals 2

    .prologue
    .line 2817
    const-class v1, Lzr;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lzr;->b:Lzr;

    if-eqz v0, :cond_0

    sget-object v0, Lzr;->b:Lzr;

    iget-object v0, v0, Lzr;->a:Landroid/database/sqlite/SQLiteDatabase;

    if-eq v0, p0, :cond_1

    .line 2818
    :cond_0
    new-instance v0, Lzr;

    invoke-direct {v0, p0}, Lzr;-><init>(Landroid/database/sqlite/SQLiteDatabase;)V

    sput-object v0, Lzr;->b:Lzr;

    .line 2820
    :cond_1
    sget-object v0, Lzr;->b:Lzr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2817
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(JLjava/lang/String;)V
    .locals 7

    .prologue
    .line 2829
    sget-object v0, Lzr;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    .line 2830
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 2831
    sub-long/2addr v1, p0

    .line 2832
    const-wide/16 v3, 0xfa

    cmp-long v3, v1, v3

    if-lez v3, :cond_0

    .line 2833
    const-string v3, "Babel"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v5, Lzr;->e:[Ljava/lang/String;

    sget-object v6, Lzr;->e:[Ljava/lang/String;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    .line 2834
    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    aget-object v0, v5, v0

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 2835
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v1, 0x1

    aput-object p2, v5, v1

    .line 2833
    invoke-static {v4, v0, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2838
    :cond_0
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v5, v4

    move-object v6, p5

    move-object v7, p6

    .line 2910
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2918
    invoke-static {}, Lzo;->i()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2943
    :cond_0
    :goto_0
    return-void

    .line 2921
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "explain query plan "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2923
    if-eqz v1, :cond_4

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2924
    const-string v2, "detail"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 2925
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2927
    :cond_2
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2928
    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2929
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2930
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_3

    .line 2931
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2933
    :cond_3
    const-string v2, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "for query "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\nplan is: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2934
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2933
    invoke-static {v2, v0}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2939
    :cond_4
    if-eqz v1, :cond_0

    .line 2940
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2936
    :catch_0
    move-exception v0

    .line 2937
    :try_start_1
    const-string v2, "Babel"

    const-string v3, "Query plan failed "

    invoke-static {v2, v3, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2939
    if-eqz v1, :cond_0

    .line 2940
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2939
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_5

    .line 2940
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 8

    .prologue
    .line 3021
    const-wide/16 v0, 0x0

    .line 3022
    sget-boolean v2, Lzr;->c:Z

    if-eqz v2, :cond_0

    .line 3023
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 3025
    :cond_0
    invoke-static {}, Lzo;->j()V

    .line 3026
    iget-object v2, p0, Lzr;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2, p1, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 3027
    sget-boolean v3, Lzr;->c:Z

    if-eqz v3, :cond_1

    .line 3028
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "update %s with %s ==> %d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    aput-object p3, v5, v6

    const/4 v6, 0x2

    .line 3029
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    .line 3028
    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lzr;->a(JLjava/lang/String;)V

    .line 3031
    :cond_1
    return v2
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v3, 0x1

    .line 3132
    const-wide/16 v0, 0x0

    .line 3133
    sget-boolean v2, Lzr;->c:Z

    if-eqz v2, :cond_0

    .line 3134
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 3136
    :cond_0
    invoke-static {}, Lzo;->j()V

    .line 3137
    iget-object v2, p0, Lzr;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2, p1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    .line 3138
    if-eqz p2, :cond_1

    .line 3139
    invoke-virtual {v2, v3, p2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 3142
    :cond_1
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_3

    .line 3143
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    move-result v2

    .line 3150
    :goto_0
    sget-boolean v4, Lzr;->c:Z

    if-eqz v4, :cond_2

    .line 3151
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "execSQLUpdateDelete %s"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v3, v6

    invoke-static {v4, v5, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lzr;->a(JLjava/lang/String;)V

    .line 3153
    :cond_2
    return v2

    .line 3145
    :cond_3
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    move v2, v3

    .line 3148
    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 8

    .prologue
    .line 3035
    const-wide/16 v0, 0x0

    .line 3036
    sget-boolean v2, Lzr;->c:Z

    if-eqz v2, :cond_0

    .line 3037
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 3039
    :cond_0
    invoke-static {}, Lzo;->j()V

    .line 3040
    iget-object v2, p0, Lzr;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2, p1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 3041
    sget-boolean v3, Lzr;->c:Z

    if-eqz v3, :cond_1

    .line 3042
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "delete from %s with %s ==> %d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    aput-object p2, v5, v6

    const/4 v6, 0x2

    .line 3044
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    .line 3043
    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 3042
    invoke-static {v0, v1, v3}, Lzr;->a(JLjava/lang/String;)V

    .line 3047
    :cond_1
    return v2
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    .line 3005
    const-wide/16 v0, 0x0

    .line 3006
    sget-boolean v2, Lzr;->c:Z

    if-eqz v2, :cond_0

    .line 3007
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 3009
    :cond_0
    invoke-static {}, Lzo;->j()V

    .line 3010
    iget-object v2, p0, Lzr;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 3011
    sget-boolean v3, Lzr;->c:Z

    if-eqz v3, :cond_1

    .line 3012
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "rawQuery %s ==> %d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    .line 3014
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 3012
    invoke-static {v0, v1, v3}, Lzr;->a(JLjava/lang/String;)V

    .line 3016
    :cond_1
    return-object v2
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7

    .prologue
    .line 2975
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 11

    .prologue
    .line 2948
    invoke-static {}, Lzo;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2949
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2950
    invoke-virtual {v0, p1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2951
    iget-object v1, p0, Lzr;->a:Landroid/database/sqlite/SQLiteDatabase;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-static/range {v0 .. v6}, Lzr;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2955
    :cond_0
    invoke-static {}, Lzo;->j()V

    .line 2956
    const-wide/16 v0, 0x0

    .line 2957
    sget-boolean v2, Lzr;->c:Z

    if-eqz v2, :cond_2

    .line 2958
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    move-wide v9, v0

    .line 2960
    :goto_0
    iget-object v0, p0, Lzr;->a:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2962
    sget-boolean v1, Lzr;->c:Z

    if-eqz v1, :cond_1

    .line 2963
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "query %s with %s ==> %d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p3, v3, v4

    const/4 v4, 0x2

    .line 2966
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 2965
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2963
    invoke-static {v9, v10, v1}, Lzr;->a(JLjava/lang/String;)V

    .line 2969
    :cond_1
    return-object v0

    :cond_2
    move-wide v9, v0

    goto :goto_0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 2841
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 2842
    sget-boolean v2, Lzr;->c:Z

    if-eqz v2, :cond_0

    .line 2843
    const-string v2, ">>> beginTransaction"

    invoke-static {v0, v1, v2}, Lzr;->a(JLjava/lang/String;)V

    .line 2847
    :cond_0
    new-instance v2, Lzt;

    invoke-direct {v2}, Lzt;-><init>()V

    .line 2848
    iput-wide v0, v2, Lzt;->a:J

    .line 2849
    sget-object v0, Lzr;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    invoke-virtual {v0, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2851
    iget-object v0, p0, Lzr;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2852
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 3120
    const-wide/16 v0, 0x0

    .line 3121
    sget-boolean v2, Lzr;->c:Z

    if-eqz v2, :cond_0

    .line 3122
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 3124
    :cond_0
    invoke-static {}, Lzo;->j()V

    .line 3125
    iget-object v2, p0, Lzr;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2, p1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3126
    sget-boolean v2, Lzr;->c:Z

    if-eqz v2, :cond_1

    .line 3127
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "execSQL %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lzr;->a(JLjava/lang/String;)V

    .line 3129
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 6

    .prologue
    .line 2895
    const-wide/16 v0, 0x0

    .line 2896
    sget-boolean v2, Lzr;->c:Z

    if-eqz v2, :cond_0

    .line 2897
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 2899
    :cond_0
    iget-object v2, p0, Lzr;->a:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v3, 0x0

    const/4 v4, 0x5

    invoke-virtual {v2, p1, v3, p2, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 2901
    sget-boolean v2, Lzr;->c:Z

    if-eqz v2, :cond_1

    .line 2902
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "insertWithOnConflict with "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lzr;->a(JLjava/lang/String;)V

    .line 2905
    :cond_1
    return-void
.end method

.method public a(Ljava/util/Locale;)V
    .locals 1

    .prologue
    .line 3104
    iget-object v0, p0, Lzr;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1}, Landroid/database/sqlite/SQLiteDatabase;->setLocale(Ljava/util/Locale;)V

    .line 3105
    return-void
.end method

.method public b(Ljava/lang/String;Landroid/content/ContentValues;)J
    .locals 8

    .prologue
    .line 3051
    const-wide/16 v0, 0x0

    .line 3052
    sget-boolean v2, Lzr;->c:Z

    if-eqz v2, :cond_0

    .line 3053
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 3055
    :cond_0
    invoke-static {}, Lzo;->j()V

    .line 3056
    iget-object v2, p0, Lzr;->a:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 3057
    sget-boolean v4, Lzr;->c:Z

    if-eqz v4, :cond_1

    .line 3058
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "insert to %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v4}, Lzr;->a(JLjava/lang/String;)V

    .line 3060
    :cond_1
    return-wide v2
.end method

.method public b()V
    .locals 2

    .prologue
    .line 2855
    sget-object v0, Lzr;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lzt;

    .line 2856
    const/4 v1, 0x1

    iput-boolean v1, v0, Lzt;->b:Z

    .line 2857
    iget-object v0, p0, Lzr;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 2858
    return-void
.end method

.method public c()V
    .locals 15

    .prologue
    const-wide/16 v1, 0x0

    const/4 v4, 0x0

    .line 2863
    sget-object v0, Lzr;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lzt;

    .line 2864
    iget-boolean v3, v0, Lzt;->b:Z

    if-nez v3, :cond_0

    .line 2865
    const-string v3, "Babel"

    const-string v5, "endTransaction without setting successful"

    invoke-static {v3, v5}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 2866
    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v5

    array-length v6, v5

    move v3, v4

    :goto_0
    if-ge v3, v6, :cond_0

    aget-object v7, v5, v3

    .line 2867
    const-string v8, "Babel"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "    "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 2866
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2870
    :cond_0
    sget-boolean v3, Lzr;->c:Z

    if-eqz v3, :cond_2

    .line 2871
    iget-wide v0, v0, Lzt;->a:J

    .line 2872
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 2874
    :goto_1
    iget-object v5, p0, Lzr;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2875
    sget-boolean v5, Lzr;->c:Z

    if-eqz v5, :cond_1

    .line 2876
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, ">>> endTransaction (total for this transaction: %d)"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    .line 2878
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v0, v8, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v7, v4

    .line 2876
    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lzr;->a(JLjava/lang/String;)V

    .line 2880
    :cond_1
    return-void

    :cond_2
    move-wide v11, v1

    move-wide v13, v1

    move-wide v2, v13

    move-wide v0, v11

    goto :goto_1
.end method

.method public d()V
    .locals 3

    .prologue
    .line 2883
    const-wide/16 v0, 0x0

    .line 2884
    sget-boolean v2, Lzr;->c:Z

    if-eqz v2, :cond_0

    .line 2885
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 2887
    :cond_0
    iget-object v2, p0, Lzr;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->yieldIfContendedSafely()Z

    move-result v2

    .line 2888
    if-eqz v2, :cond_1

    sget-boolean v2, Lzr;->c:Z

    if-eqz v2, :cond_1

    .line 2889
    const-string v2, "yieldTransaction"

    invoke-static {v0, v1, v2}, Lzr;->a(JLjava/lang/String;)V

    .line 2891
    :cond_1
    return-void
.end method

.method public e()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 3162
    iget-object v0, p0, Lzr;->a:Landroid/database/sqlite/SQLiteDatabase;

    return-object v0
.end method
