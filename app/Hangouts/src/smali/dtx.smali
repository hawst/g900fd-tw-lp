.class public final Ldtx;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldtx;


# instance fields
.field public b:[Lesg;

.field public c:[Lesc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2142
    const/4 v0, 0x0

    new-array v0, v0, [Ldtx;

    sput-object v0, Ldtx;->a:[Ldtx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2143
    invoke-direct {p0}, Lepn;-><init>()V

    .line 2146
    sget-object v0, Lesg;->a:[Lesg;

    iput-object v0, p0, Ldtx;->b:[Lesg;

    .line 2149
    sget-object v0, Lesc;->a:[Lesc;

    iput-object v0, p0, Ldtx;->c:[Lesc;

    .line 2143
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2175
    iget-object v0, p0, Ldtx;->b:[Lesg;

    if-eqz v0, :cond_1

    .line 2176
    iget-object v3, p0, Ldtx;->b:[Lesg;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 2177
    if-eqz v5, :cond_0

    .line 2178
    const/4 v6, 0x1

    .line 2179
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 2176
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2183
    :cond_2
    iget-object v2, p0, Ldtx;->c:[Lesc;

    if-eqz v2, :cond_4

    .line 2184
    iget-object v2, p0, Ldtx;->c:[Lesc;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 2185
    if-eqz v4, :cond_3

    .line 2186
    const/4 v5, 0x2

    .line 2187
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2184
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2191
    :cond_4
    iget-object v1, p0, Ldtx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2192
    iput v0, p0, Ldtx;->cachedSize:I

    .line 2193
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2139
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldtx;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldtx;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldtx;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldtx;->b:[Lesg;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lesg;

    iget-object v3, p0, Ldtx;->b:[Lesg;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldtx;->b:[Lesg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldtx;->b:[Lesg;

    :goto_2
    iget-object v2, p0, Ldtx;->b:[Lesg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldtx;->b:[Lesg;

    new-instance v3, Lesg;

    invoke-direct {v3}, Lesg;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldtx;->b:[Lesg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldtx;->b:[Lesg;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldtx;->b:[Lesg;

    new-instance v3, Lesg;

    invoke-direct {v3}, Lesg;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldtx;->b:[Lesg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldtx;->c:[Lesc;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lesc;

    iget-object v3, p0, Ldtx;->c:[Lesc;

    if-eqz v3, :cond_5

    iget-object v3, p0, Ldtx;->c:[Lesc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Ldtx;->c:[Lesc;

    :goto_4
    iget-object v2, p0, Ldtx;->c:[Lesc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Ldtx;->c:[Lesc;

    new-instance v3, Lesc;

    invoke-direct {v3}, Lesc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldtx;->c:[Lesc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Ldtx;->c:[Lesc;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Ldtx;->c:[Lesc;

    new-instance v3, Lesc;

    invoke-direct {v3}, Lesc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldtx;->c:[Lesc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2154
    iget-object v1, p0, Ldtx;->b:[Lesg;

    if-eqz v1, :cond_1

    .line 2155
    iget-object v2, p0, Ldtx;->b:[Lesg;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2156
    if-eqz v4, :cond_0

    .line 2157
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 2155
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2161
    :cond_1
    iget-object v1, p0, Ldtx;->c:[Lesc;

    if-eqz v1, :cond_3

    .line 2162
    iget-object v1, p0, Ldtx;->c:[Lesc;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 2163
    if-eqz v3, :cond_2

    .line 2164
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 2162
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2168
    :cond_3
    iget-object v0, p0, Ldtx;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 2170
    return-void
.end method
