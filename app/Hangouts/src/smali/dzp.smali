.class public final Ldzp;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldzp;


# instance fields
.field public b:Ldzr;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Integer;

.field public e:Ldzq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    new-array v0, v0, [Ldzp;

    sput-object v0, Ldzp;->a:[Ldzp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Lepn;-><init>()V

    .line 215
    iput-object v0, p0, Ldzp;->b:Ldzr;

    .line 220
    iput-object v0, p0, Ldzp;->d:Ljava/lang/Integer;

    .line 223
    iput-object v0, p0, Ldzp;->e:Ldzq;

    .line 29
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 246
    const/4 v0, 0x0

    .line 247
    iget-object v1, p0, Ldzp;->b:Ldzr;

    if-eqz v1, :cond_0

    .line 248
    const/4 v0, 0x1

    iget-object v1, p0, Ldzp;->b:Ldzr;

    .line 249
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 251
    :cond_0
    iget-object v1, p0, Ldzp;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 252
    const/4 v1, 0x2

    iget-object v2, p0, Ldzp;->c:Ljava/lang/String;

    .line 253
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 255
    :cond_1
    iget-object v1, p0, Ldzp;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 256
    const/4 v1, 0x3

    iget-object v2, p0, Ldzp;->d:Ljava/lang/Integer;

    .line 257
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 259
    :cond_2
    iget-object v1, p0, Ldzp;->e:Ldzq;

    if-eqz v1, :cond_3

    .line 260
    const/4 v1, 0x4

    iget-object v2, p0, Ldzp;->e:Ldzq;

    .line 261
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 263
    :cond_3
    iget-object v1, p0, Ldzp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 264
    iput v0, p0, Ldzp;->cachedSize:I

    .line 265
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 25
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldzp;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldzp;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldzp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldzp;->b:Ldzr;

    if-nez v0, :cond_2

    new-instance v0, Ldzr;

    invoke-direct {v0}, Ldzr;-><init>()V

    iput-object v0, p0, Ldzp;->b:Ldzr;

    :cond_2
    iget-object v0, p0, Ldzp;->b:Ldzr;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldzp;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldzp;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldzp;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldzp;->e:Ldzq;

    if-nez v0, :cond_5

    new-instance v0, Ldzq;

    invoke-direct {v0}, Ldzq;-><init>()V

    iput-object v0, p0, Ldzp;->e:Ldzq;

    :cond_5
    iget-object v0, p0, Ldzp;->e:Ldzq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 228
    iget-object v0, p0, Ldzp;->b:Ldzr;

    if-eqz v0, :cond_0

    .line 229
    const/4 v0, 0x1

    iget-object v1, p0, Ldzp;->b:Ldzr;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 231
    :cond_0
    iget-object v0, p0, Ldzp;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 232
    const/4 v0, 0x2

    iget-object v1, p0, Ldzp;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 234
    :cond_1
    iget-object v0, p0, Ldzp;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 235
    const/4 v0, 0x3

    iget-object v1, p0, Ldzp;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 237
    :cond_2
    iget-object v0, p0, Ldzp;->e:Ldzq;

    if-eqz v0, :cond_3

    .line 238
    const/4 v0, 0x4

    iget-object v1, p0, Ldzp;->e:Ldzq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 240
    :cond_3
    iget-object v0, p0, Ldzp;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 242
    return-void
.end method
