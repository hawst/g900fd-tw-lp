.class public final Lbus;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lbtz;

.field b:Lyj;

.field final c:Landroid/app/Activity;

.field d:Lach;

.field final e:Ljava/lang/String;

.field final f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private final h:Landroid/content/res/Resources;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Lbor;


# direct methods
.method public constructor <init>(Lbtz;)V
    .locals 2

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 315
    new-instance v0, Lbuv;

    invoke-direct {v0, p0}, Lbuv;-><init>(Lbus;)V

    iput-object v0, p0, Lbus;->k:Lbor;

    .line 162
    iput-object p1, p0, Lbus;->a:Lbtz;

    .line 163
    iget-object v0, p0, Lbus;->a:Lbtz;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 165
    iget-object v0, p0, Lbus;->a:Lbtz;

    invoke-interface {v0}, Lbtz;->c()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lbus;->c:Landroid/app/Activity;

    .line 166
    iget-object v0, p0, Lbus;->c:Landroid/app/Activity;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 168
    iget-object v0, p0, Lbus;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lbus;->h:Landroid/content/res/Resources;

    .line 170
    iget-object v0, p0, Lbus;->h:Landroid/content/res/Resources;

    sget v1, Lh;->kF:I

    .line 171
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbus;->e:Ljava/lang/String;

    .line 172
    iget-object v0, p0, Lbus;->h:Landroid/content/res/Resources;

    sget v1, Lh;->kG:I

    .line 173
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbus;->f:Ljava/lang/String;

    .line 174
    iget-object v0, p0, Lbus;->h:Landroid/content/res/Resources;

    sget v1, Lh;->kH:I

    .line 175
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbus;->i:Ljava/lang/String;

    .line 176
    iget-object v0, p0, Lbus;->h:Landroid/content/res/Resources;

    sget v1, Lh;->kI:I

    .line 177
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbus;->j:Ljava/lang/String;

    .line 178
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 305
    invoke-static {}, Lccc;->a()Lccc;

    move-result-object v0

    invoke-virtual {v0, p1}, Lccc;->a(I)I

    move-result v1

    .line 306
    const/4 v0, -0x1

    if-eq v1, v0, :cond_0

    .line 307
    iget-object v0, p0, Lbus;->a:Lbtz;

    iget-object v2, p0, Lbus;->i:Ljava/lang/String;

    .line 308
    invoke-interface {v0, v2}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;

    .line 309
    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;->a(I)V

    .line 313
    :goto_0
    return-void

    .line 311
    :cond_0
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to find mood icon for codePoint: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private d()Z
    .locals 2

    .prologue
    .line 270
    iget-object v0, p0, Lbus;->g:Ljava/lang/String;

    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    iput-object v0, p0, Lbus;->b:Lyj;

    .line 271
    iget-object v0, p0, Lbus;->b:Lyj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbus;->b:Lyj;

    invoke-static {v0}, Lbkb;->f(Lyj;)I

    move-result v0

    const/16 v1, 0x66

    if-eq v0, v1, :cond_1

    .line 273
    :cond_0
    const-string v0, "Babel"

    const-string v1, "Rich status settings page resumed with invalid account. Go to Babel home"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    iget-object v0, p0, Lbus;->c:Landroid/app/Activity;

    invoke-static {v0}, Lbtf;->a(Landroid/app/Activity;)V

    .line 276
    const/4 v0, 0x0

    .line 279
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 266
    invoke-direct {p0}, Lbus;->d()Z

    .line 267
    return-void
.end method

.method public a(II)V
    .locals 2

    .prologue
    .line 290
    if-nez p1, :cond_0

    if-eqz p2, :cond_0

    .line 291
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 292
    iget-object v0, p0, Lbus;->a:Lbtz;

    iget-object v1, p0, Lbus;->i:Ljava/lang/String;

    .line 293
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;

    .line 294
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;->a(I)V

    .line 302
    :cond_0
    :goto_0
    return-void

    .line 299
    :cond_1
    invoke-direct {p0, p2}, Lbus;->a(I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 181
    iget-object v0, p0, Lbus;->c:Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 182
    const-string v0, "Babel"

    const-string v1, "RichStatusSettingsUi: empty context. Abort."

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    iput-object p1, p0, Lbus;->g:Ljava/lang/String;

    .line 186
    iget-object v0, p0, Lbus;->g:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 187
    const-string v0, "Babel"

    const-string v1, "RichStatusSettingsUi: no account specified. Abort."

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    iget-object v0, p0, Lbus;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 192
    :cond_2
    invoke-direct {p0}, Lbus;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lbus;->a:Lbtz;

    iget-object v1, p0, Lbus;->b:Lyj;

    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lf;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/String;)V

    .line 197
    iget-object v0, p0, Lbus;->a:Lbtz;

    sget v1, Lf;->ih:I

    invoke-interface {v0, v1}, Lbtz;->a(I)V

    .line 199
    iget-object v0, p0, Lbus;->a:Lbtz;

    iget-object v1, p0, Lbus;->i:Ljava/lang/String;

    .line 200
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;

    .line 201
    new-instance v1, Lbut;

    invoke-direct {v1, p0}, Lbut;-><init>(Lbus;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 212
    iget-object v0, p0, Lbus;->a:Lbtz;

    invoke-interface {v0}, Lbtz;->b()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lbus;->j:Ljava/lang/String;

    const-string v2, ""

    .line 213
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 215
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 216
    invoke-virtual {v0, v3}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .line 217
    invoke-direct {p0, v0}, Lbus;->a(I)V

    .line 225
    :goto_1
    new-instance v0, Lbuu;

    invoke-direct {v0, p0}, Lbuu;-><init>(Lbus;)V

    .line 255
    iget-object v1, p0, Lbus;->a:Lbtz;

    iget-object v2, p0, Lbus;->e:Ljava/lang/String;

    invoke-interface {v1, v2}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 256
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 257
    iget-object v1, p0, Lbus;->a:Lbtz;

    iget-object v2, p0, Lbus;->f:Ljava/lang/String;

    invoke-interface {v1, v2}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 258
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 262
    iget-object v0, p0, Lbus;->b:Lyj;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lyj;Z)I

    goto/16 :goto_0

    .line 219
    :cond_3
    invoke-direct {p0, v3}, Lbus;->a(I)V

    goto :goto_1
.end method

.method public b()V
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lbus;->d:Lach;

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lbus;->d:Lach;

    invoke-interface {v0}, Lach;->a()V

    .line 285
    const/4 v0, 0x0

    iput-object v0, p0, Lbus;->d:Lach;

    .line 287
    :cond_0
    return-void
.end method

.method public c()Lbor;
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lbus;->k:Lbor;

    return-object v0
.end method
