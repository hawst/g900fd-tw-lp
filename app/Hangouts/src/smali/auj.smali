.class public final Lauj;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final VERBOSE:Z


# instance fields
.field private a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lbys;->e:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lauj;->VERBOSE:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput v0, p0, Lauj;->a:I

    return-void
.end method

.method static synthetic a(Lauj;)I
    .locals 2

    .prologue
    .line 12
    iget v0, p0, Lauj;->a:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lauj;->a:I

    return v0
.end method

.method static synthetic b(Lauj;)I
    .locals 2

    .prologue
    .line 12
    iget v0, p0, Lauj;->a:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lauj;->a:I

    return v0
.end method


# virtual methods
.method public isAnimating()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 18
    iget v0, p0, Lauj;->a:I

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 19
    iget v0, p0, Lauj;->a:I

    if-eqz v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 18
    goto :goto_0

    :cond_1
    move v1, v2

    .line 19
    goto :goto_1
.end method

.method public trackAnimation(Landroid/view/animation/Animation;)V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lauk;

    invoke-direct {v0, p0}, Lauk;-><init>(Lauj;)V

    invoke-virtual {p1, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 52
    return-void
.end method
