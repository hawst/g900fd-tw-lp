.class public final Lxe;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private a:F

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private d:Lwt;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 69
    const/4 v0, 0x0

    const/4 v1, 0x0

    const-class v2, Ljava/lang/Object;

    invoke-direct {p0, v0, v1, v2}, Lxe;-><init>(FLjava/lang/Object;Ljava/lang/Class;)V

    .line 70
    return-void
.end method

.method public constructor <init>(FF)V
    .locals 2

    .prologue
    .line 149
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-direct {p0, p1, v0, v1}, Lxe;-><init>(FLjava/lang/Object;Ljava/lang/Class;)V

    .line 150
    return-void
.end method

.method public constructor <init>(FI)V
    .locals 2

    .prologue
    .line 133
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-direct {p0, p1, v0, v1}, Lxe;-><init>(FLjava/lang/Object;Ljava/lang/Class;)V

    .line 134
    return-void
.end method

.method private constructor <init>(FLjava/lang/Object;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F",
            "Ljava/lang/Object;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lxe;->d:Lwt;

    .line 51
    iput p1, p0, Lxe;->a:F

    .line 52
    iput-object p2, p0, Lxe;->b:Ljava/lang/Object;

    .line 53
    iput-object p3, p0, Lxe;->c:Ljava/lang/Class;

    .line 54
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lxe;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Lxe;->b:Ljava/lang/Object;

    .line 184
    return-void
.end method

.method public b()F
    .locals 1

    .prologue
    .line 193
    iget v0, p0, Lxe;->a:F

    return v0
.end method

.method public c()Lwt;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lxe;->d:Lwt;

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lxe;->e()Lxe;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 232
    iget-object v0, p0, Lxe;->c:Ljava/lang/Class;

    return-object v0
.end method

.method public e()Lxe;
    .locals 4

    .prologue
    .line 237
    new-instance v0, Lxe;

    iget v1, p0, Lxe;->a:F

    iget-object v2, p0, Lxe;->b:Ljava/lang/Object;

    iget-object v3, p0, Lxe;->c:Ljava/lang/Class;

    invoke-direct {v0, v1, v2, v3}, Lxe;-><init>(FLjava/lang/Object;Ljava/lang/Class;)V

    .line 238
    iget-object v1, p0, Lxe;->d:Lwt;

    iput-object v1, v0, Lxe;->d:Lwt;

    .line 239
    return-object v0
.end method
