.class public final Lbcx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lbcn;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ldth;)V
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iget-object v0, p1, Ldth;->b:Ljava/lang/String;

    iput-object v0, p0, Lbcx;->a:Ljava/lang/String;

    .line 105
    iget-object v0, p1, Ldth;->d:Ljava/lang/String;

    iput-object v0, p0, Lbcx;->b:Ljava/lang/String;

    .line 106
    iget-object v0, p1, Ldth;->c:Ljava/lang/String;

    iput-object v0, p0, Lbcx;->c:Ljava/lang/String;

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lbcx;->e:Ljava/lang/String;

    .line 108
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lbcx;->a:Ljava/lang/String;

    .line 36
    iput-object p2, p0, Lbcx;->b:Ljava/lang/String;

    .line 37
    iput-object p3, p0, Lbcx;->c:Ljava/lang/String;

    .line 38
    iput-object p4, p0, Lbcx;->d:Ljava/lang/String;

    .line 39
    iput-object p5, p0, Lbcx;->e:Ljava/lang/String;

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lbcx;->f:Ljava/util/ArrayList;

    .line 41
    return-void
.end method

.method public static a(Lbdh;)Lbcx;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-virtual {p0}, Lbdh;->c()Ljava/lang/String;

    move-result-object v4

    .line 76
    iget-object v0, p0, Lbdh;->b:Lbdk;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lbdh;->b:Lbdk;

    iget-object v1, p0, Lbdh;->g:Ljava/lang/String;

    invoke-static {v0, v4, v1}, Lbcx;->a(Lbdk;Ljava/lang/String;Ljava/lang/String;)Lbcx;

    move-result-object v0

    .line 82
    :goto_0
    return-object v0

    .line 78
    :cond_0
    if-eqz v4, :cond_1

    .line 80
    new-instance v0, Lbcx;

    iget-object v5, p0, Lbdh;->g:Ljava/lang/String;

    move-object v2, v1

    move-object v3, v1

    invoke-direct/range {v0 .. v5}, Lbcx;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 82
    goto :goto_0
.end method

.method public static a(Lbdk;Ljava/lang/String;Ljava/lang/String;)Lbcx;
    .locals 6

    .prologue
    .line 67
    new-instance v0, Lbcx;

    iget-object v1, p0, Lbdk;->a:Ljava/lang/String;

    iget-object v2, p0, Lbdk;->b:Ljava/lang/String;

    const/4 v3, 0x0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lbcx;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lbcx;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 89
    new-instance v0, Lbcx;

    invoke-static {p0}, Lbdk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v3, v1

    move-object v4, p0

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, Lbcx;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lbcx;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 47
    new-instance v0, Lbcx;

    move-object v1, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lbcx;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 218
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 219
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "g:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 234
    :goto_0
    return-object v0

    .line 222
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 223
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "b:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 226
    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 227
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "c:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 230
    :cond_2
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 231
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "p:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 234
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Lbcx;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 59
    new-instance v0, Lbcx;

    move-object v2, v1

    move-object v3, p0

    move-object v4, v1

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lbcx;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a()Lbdk;
    .locals 4

    .prologue
    .line 111
    new-instance v0, Lbdk;

    iget-object v1, p0, Lbcx;->a:Ljava/lang/String;

    iget-object v2, p0, Lbcx;->b:Ljava/lang/String;

    iget-object v3, p0, Lbcx;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lbcn;)V
    .locals 1

    .prologue
    .line 115
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbcx;->f:Ljava/util/ArrayList;

    .line 116
    iget-object v0, p0, Lbcx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    return-void
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 188
    instance-of v0, p1, Lbcx;

    if-eqz v0, :cond_3

    .line 189
    check-cast p1, Lbcx;

    .line 190
    iget-object v0, p0, Lbcx;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lbcx;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lbcx;->b:Ljava/lang/String;

    iget-object v1, p1, Lbcx;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 203
    :goto_0
    return v0

    .line 193
    :cond_0
    iget-object v0, p0, Lbcx;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lbcx;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 195
    iget-object v0, p0, Lbcx;->a:Ljava/lang/String;

    iget-object v1, p1, Lbcx;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 196
    :cond_1
    iget-object v0, p0, Lbcx;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lbcx;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 198
    iget-object v0, p0, Lbcx;->c:Ljava/lang/String;

    iget-object v1, p1, Lbcx;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 199
    :cond_2
    iget-object v0, p0, Lbcx;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lbcx;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 200
    iget-object v0, p0, Lbcx;->d:Ljava/lang/String;

    iget-object v1, p1, Lbcx;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 203
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbcn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lbcx;->f:Ljava/util/ArrayList;

    return-object v0
.end method

.method public c()Ldth;
    .locals 7

    .prologue
    .line 124
    new-instance v2, Ldth;

    invoke-direct {v2}, Ldth;-><init>()V

    .line 127
    iget-object v0, p0, Lbcx;->a:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 128
    iget-object v0, p0, Lbcx;->a:Ljava/lang/String;

    iput-object v0, v2, Ldth;->b:Ljava/lang/String;

    .line 136
    :cond_0
    :goto_0
    iget-object v0, p0, Lbcx;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 137
    iget-object v0, p0, Lbcx;->e:Ljava/lang/String;

    iput-object v0, v2, Ldth;->f:Ljava/lang/String;

    .line 140
    :cond_1
    iget-object v0, p0, Lbcx;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lbcx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 141
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 142
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lbcx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 143
    iget-object v0, p0, Lbcx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbcn;

    .line 144
    iget-object v4, v0, Lbcn;->d:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 145
    new-instance v4, Ldti;

    invoke-direct {v4}, Ldti;-><init>()V

    .line 146
    new-instance v5, Ldtl;

    invoke-direct {v5}, Ldtl;-><init>()V

    iput-object v5, v4, Ldti;->b:Ldtl;

    .line 147
    iget-object v5, v4, Ldti;->b:Ldtl;

    new-instance v6, Leir;

    invoke-direct {v6}, Leir;-><init>()V

    iput-object v6, v5, Ldtl;->b:Leir;

    .line 148
    iget-object v5, v4, Ldti;->b:Ldtl;

    iget-object v5, v5, Ldtl;->b:Leir;

    iget-object v0, v0, Lbcn;->d:Ljava/lang/String;

    iput-object v0, v5, Leir;->b:Ljava/lang/String;

    .line 149
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    :cond_2
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 129
    :cond_3
    iget-object v0, p0, Lbcx;->c:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 130
    iget-object v0, p0, Lbcx;->c:Ljava/lang/String;

    iput-object v0, v2, Ldth;->c:Ljava/lang/String;

    goto :goto_0

    .line 131
    :cond_4
    iget-object v0, p0, Lbcx;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 132
    new-instance v0, Leir;

    invoke-direct {v0}, Leir;-><init>()V

    iput-object v0, v2, Ldth;->e:Leir;

    .line 133
    iget-object v0, v2, Ldth;->e:Leir;

    iget-object v1, p0, Lbcx;->d:Ljava/lang/String;

    invoke-static {v1}, Lbzd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Leir;->b:Ljava/lang/String;

    goto :goto_0

    .line 150
    :cond_5
    iget-object v4, v0, Lbcn;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 151
    new-instance v4, Ldti;

    invoke-direct {v4}, Ldti;-><init>()V

    .line 152
    new-instance v5, Ldtj;

    invoke-direct {v5}, Ldtj;-><init>()V

    iput-object v5, v4, Ldti;->c:Ldtj;

    .line 153
    iget-object v5, v4, Ldti;->c:Ldtj;

    iget-object v0, v0, Lbcn;->c:Ljava/lang/String;

    iput-object v0, v5, Ldtj;->b:Ljava/lang/String;

    .line 154
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 157
    :cond_6
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ldti;

    iput-object v0, v2, Ldth;->g:[Ldti;

    .line 158
    iget-object v0, v2, Ldth;->g:[Ldti;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 161
    :cond_7
    return-object v2
.end method

.method public d()Ljava/lang/String;
    .locals 4

    .prologue
    .line 207
    iget-object v0, p0, Lbcx;->a:Ljava/lang/String;

    iget-object v1, p0, Lbcx;->b:Ljava/lang/String;

    iget-object v2, p0, Lbcx;->c:Ljava/lang/String;

    iget-object v3, p0, Lbcx;->d:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lbcx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 166
    if-eqz p1, :cond_0

    instance-of v0, p1, Lbcx;

    if-eqz v0, :cond_0

    .line 167
    check-cast p1, Lbcx;

    .line 168
    invoke-virtual {p0, p1}, Lbcx;->a(Ljava/lang/Object;)Z

    move-result v0

    .line 170
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lbcx;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbcx;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbcx;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 239
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "InviteeId {chatId: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lbcx;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  gaiaId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbcx;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  circleId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbcx;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  phoneNumber: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbcx;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
