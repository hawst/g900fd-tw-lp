.class public final Ldvn;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldvn;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/Long;

.field public g:Ljava/lang/Long;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1023
    const/4 v0, 0x0

    new-array v0, v0, [Ldvn;

    sput-object v0, Ldvn;->a:[Ldvn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1024
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1027
    const/4 v0, 0x0

    iput-object v0, p0, Ldvn;->b:Ljava/lang/Integer;

    .line 1024
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 1076
    const/4 v0, 0x0

    .line 1077
    iget-object v1, p0, Ldvn;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1078
    const/4 v0, 0x1

    iget-object v1, p0, Ldvn;->b:Ljava/lang/Integer;

    .line 1079
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1081
    :cond_0
    iget-object v1, p0, Ldvn;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1082
    const/4 v1, 0x2

    iget-object v2, p0, Ldvn;->c:Ljava/lang/String;

    .line 1083
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1085
    :cond_1
    iget-object v1, p0, Ldvn;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1086
    const/4 v1, 0x3

    iget-object v2, p0, Ldvn;->d:Ljava/lang/String;

    .line 1087
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1089
    :cond_2
    iget-object v1, p0, Ldvn;->e:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 1090
    const/4 v1, 0x4

    iget-object v2, p0, Ldvn;->e:Ljava/lang/Long;

    .line 1091
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1093
    :cond_3
    iget-object v1, p0, Ldvn;->f:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 1094
    const/4 v1, 0x5

    iget-object v2, p0, Ldvn;->f:Ljava/lang/Long;

    .line 1095
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1097
    :cond_4
    iget-object v1, p0, Ldvn;->g:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 1098
    const/4 v1, 0x6

    iget-object v2, p0, Ldvn;->g:Ljava/lang/Long;

    .line 1099
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1101
    :cond_5
    iget-object v1, p0, Ldvn;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 1102
    const/4 v1, 0x7

    iget-object v2, p0, Ldvn;->h:Ljava/lang/String;

    .line 1103
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1105
    :cond_6
    iget-object v1, p0, Ldvn;->i:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 1106
    const/16 v1, 0x8

    iget-object v2, p0, Ldvn;->i:Ljava/lang/String;

    .line 1107
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1109
    :cond_7
    iget-object v1, p0, Ldvn;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1110
    iput v0, p0, Ldvn;->cachedSize:I

    .line 1111
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1020
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldvn;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldvn;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldvn;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldvn;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldvn;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldvn;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldvn;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldvn;->e:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldvn;->f:Ljava/lang/Long;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldvn;->g:Ljava/lang/Long;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldvn;->h:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldvn;->i:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 1046
    iget-object v0, p0, Ldvn;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1047
    const/4 v0, 0x1

    iget-object v1, p0, Ldvn;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1049
    :cond_0
    iget-object v0, p0, Ldvn;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1050
    const/4 v0, 0x2

    iget-object v1, p0, Ldvn;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1052
    :cond_1
    iget-object v0, p0, Ldvn;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1053
    const/4 v0, 0x3

    iget-object v1, p0, Ldvn;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1055
    :cond_2
    iget-object v0, p0, Ldvn;->e:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1056
    const/4 v0, 0x4

    iget-object v1, p0, Ldvn;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 1058
    :cond_3
    iget-object v0, p0, Ldvn;->f:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 1059
    const/4 v0, 0x5

    iget-object v1, p0, Ldvn;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 1061
    :cond_4
    iget-object v0, p0, Ldvn;->g:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 1062
    const/4 v0, 0x6

    iget-object v1, p0, Ldvn;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 1064
    :cond_5
    iget-object v0, p0, Ldvn;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1065
    const/4 v0, 0x7

    iget-object v1, p0, Ldvn;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1067
    :cond_6
    iget-object v0, p0, Ldvn;->i:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1068
    const/16 v0, 0x8

    iget-object v1, p0, Ldvn;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1070
    :cond_7
    iget-object v0, p0, Ldvn;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1072
    return-void
.end method
