.class public final Latp;
.super Late;
.source "PG"

# interfaces
.implements Laac;
.implements Lbri;


# instance fields
.field private final E:Lats;

.field private F:Lyb;

.field private G:Lzx;

.field private final H:Lcom/google/android/libraries/hangouts/video/Renderer;

.field private final I:Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;

.field private final J:Lcom/google/android/libraries/hangouts/video/Decoder;

.field private final K:Ljava/lang/Runnable;

.field private final L:Ljava/lang/Runnable;


# direct methods
.method private constructor <init>(Latd;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;Lasx;Ljava/lang/Object;IILandroid/widget/LinearLayout;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 155
    invoke-direct/range {p0 .. p8}, Late;-><init>(Latd;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;Lasx;Ljava/lang/Object;IILandroid/view/ViewGroup;)V

    .line 100
    new-instance v0, Lats;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lats;-><init>(Latp;B)V

    iput-object v0, p0, Latp;->E:Lats;

    .line 108
    new-instance v0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;

    invoke-direct {v0}, Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;-><init>()V

    iput-object v0, p0, Latp;->I:Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;

    .line 116
    new-instance v0, Latq;

    invoke-direct {v0, p0}, Latq;-><init>(Latp;)V

    iput-object v0, p0, Latp;->K:Ljava/lang/Runnable;

    .line 123
    new-instance v0, Latr;

    invoke-direct {v0, p0}, Latr;-><init>(Latp;)V

    iput-object v0, p0, Latp;->L:Ljava/lang/Runnable;

    .line 158
    invoke-static {}, Lape;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    invoke-virtual {p1}, Latd;->f()Lcom/google/android/libraries/hangouts/video/DecoderManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/DecoderManager;->createDecoder()Lcom/google/android/libraries/hangouts/video/Decoder;

    move-result-object v0

    iput-object v0, p0, Latp;->J:Lcom/google/android/libraries/hangouts/video/Decoder;

    .line 161
    iget-object v0, p0, Latp;->J:Lcom/google/android/libraries/hangouts/video/Decoder;

    if-nez v0, :cond_0

    .line 162
    const-string v0, "Babel"

    const-string v1, "The decoder manager was unable to create a hardware decoder."

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    :cond_0
    :goto_0
    iget-object v0, p0, Latp;->J:Lcom/google/android/libraries/hangouts/video/Decoder;

    if-eqz v0, :cond_2

    .line 170
    invoke-virtual {p1}, Latd;->e()Lcom/google/android/libraries/hangouts/video/RendererManager;

    move-result-object v0

    iget-object v1, p0, Latp;->J:Lcom/google/android/libraries/hangouts/video/Decoder;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/RendererManager;->createAcceleratedRemoteRenderer(Lcom/google/android/libraries/hangouts/video/Decoder;)Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;

    move-result-object v0

    iput-object v0, p0, Latp;->H:Lcom/google/android/libraries/hangouts/video/Renderer;

    .line 175
    :goto_1
    iget-object v0, p0, Latp;->d:Lapk;

    iget-object v1, p0, Latp;->E:Lats;

    invoke-virtual {v0, v1}, Lapk;->a(Lapo;)V

    .line 176
    return-void

    .line 165
    :cond_1
    iput-object v2, p0, Latp;->J:Lcom/google/android/libraries/hangouts/video/Decoder;

    goto :goto_0

    .line 172
    :cond_2
    invoke-virtual {p1}, Latd;->e()Lcom/google/android/libraries/hangouts/video/RendererManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/hangouts/video/RendererManager;->createRemoteRenderer(Lcom/google/android/libraries/hangouts/video/Renderer$RendererThreadCallback;)Lcom/google/android/libraries/hangouts/video/RemoteRenderer;

    move-result-object v0

    iput-object v0, p0, Latp;->H:Lcom/google/android/libraries/hangouts/video/Renderer;

    goto :goto_1
.end method

.method public constructor <init>(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Latd;Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;Lasx;Ljava/lang/Object;IILandroid/widget/LinearLayout;)V
    .locals 9

    .prologue
    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    .line 183
    invoke-direct/range {v0 .. v8}, Latp;-><init>(Latd;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;Lasx;Ljava/lang/Object;IILandroid/widget/LinearLayout;)V

    .line 185
    iget-object v0, p0, Latp;->B:Latg;

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Latg;->a(Ljava/lang/String;)V

    .line 190
    invoke-direct {p0}, Latp;->B()V

    .line 191
    invoke-direct {p0}, Latp;->D()V

    .line 192
    return-void
.end method

.method private B()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 273
    iget-object v0, p0, Latp;->w:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 307
    :cond_0
    :goto_0
    return-void

    .line 277
    :cond_1
    iget-object v0, p0, Latp;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    instance-of v0, v0, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    if-eqz v0, :cond_3

    .line 278
    iget-object v0, p0, Latp;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getAttachedData()Ljava/lang/Object;

    move-result-object v0

    .line 282
    if-eqz v0, :cond_0

    instance-of v1, v0, Lash;

    if-eqz v1, :cond_0

    .line 283
    check-cast v0, Lash;

    .line 286
    invoke-virtual {v0}, Lash;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 287
    invoke-virtual {v0}, Lash;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    iget-object v1, p0, Latp;->e:Latd;

    invoke-virtual {v1}, Latd;->c()Lapv;

    move-result-object v1

    invoke-virtual {v1}, Lapv;->a()Lyj;

    move-result-object v1

    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v2

    iget-object v3, p0, Latp;->G:Lzx;

    if-eqz v3, :cond_2

    iget-object v3, p0, Latp;->G:Lzx;

    invoke-virtual {v2, v3}, Lbsn;->b(Lbrv;)V

    :cond_2
    new-instance v3, Lzx;

    new-instance v4, Lbyq;

    invoke-direct {v4, v0, v1}, Lbyq;-><init>(Ljava/lang/String;Lyj;)V

    invoke-static {}, Lyn;->f()I

    move-result v0

    invoke-virtual {v4, v0}, Lbyq;->a(I)Lbyq;

    move-result-object v0

    invoke-virtual {v0, v5}, Lbyq;->d(Z)Lbyq;

    move-result-object v0

    invoke-direct {v3, v0, p0, v5, v6}, Lzx;-><init>(Lbyq;Laac;ZLjava/lang/Object;)V

    iput-object v3, p0, Latp;->G:Lzx;

    iget-object v0, p0, Latp;->G:Lzx;

    invoke-virtual {v2, v0}, Lbsn;->a(Lbrv;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object v6, p0, Latp;->G:Lzx;

    goto :goto_0

    .line 293
    :cond_3
    iget-object v0, p0, Latp;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    instance-of v0, v0, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 294
    iget-object v0, p0, Latp;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    .line 295
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;->getObfuscatedGaiaId()Ljava/lang/String;

    move-result-object v0

    .line 297
    iget-object v1, p0, Latp;->F:Lyb;

    if-eqz v1, :cond_4

    .line 299
    iget-object v1, p0, Latp;->F:Lyb;

    invoke-virtual {v1}, Lyb;->b()V

    .line 302
    :cond_4
    iget-object v1, p0, Latp;->e:Latd;

    invoke-virtual {v1}, Latd;->c()Lapv;

    move-result-object v1

    invoke-virtual {v1}, Lapv;->a()Lyj;

    move-result-object v1

    .line 303
    invoke-static {v1}, Lbrf;->a(Lyj;)Lbrf;

    move-result-object v1

    .line 304
    new-instance v2, Lyb;

    invoke-direct {v2, v0, p0}, Lyb;-><init>(Ljava/lang/String;Lbri;)V

    iput-object v2, p0, Latp;->F:Lyb;

    .line 305
    iget-object v0, p0, Latp;->F:Lyb;

    invoke-virtual {v1, v0}, Lbrf;->a(Lbrv;)Z

    goto/16 :goto_0
.end method

.method private C()V
    .locals 1

    .prologue
    .line 311
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Latp;->a(I)V

    .line 312
    invoke-direct {p0}, Latp;->B()V

    .line 313
    return-void
.end method

.method private D()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 317
    iget-object v1, p0, Latp;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getVideoSsrcs()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Latp;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 318
    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isVideoMuted()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Latp;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 319
    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isMediaBlocked()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Latp;->k:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Latp;->f:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    .line 320
    invoke-static {}, Lcxl;->b()I

    move-result v1

    if-eq v1, v0, :cond_1

    :goto_0
    if-nez v0, :cond_2

    .line 321
    :cond_0
    invoke-direct {p0}, Latp;->C()V

    .line 325
    :goto_1
    invoke-virtual {p0}, Latp;->w()V

    .line 326
    return-void

    .line 320
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 323
    :cond_2
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Latp;->a(I)V

    goto :goto_1
.end method

.method private a(I)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v7, 0x2

    .line 219
    invoke-static {p1, v0, v7}, Lcwz;->a(III)V

    .line 220
    iget-object v1, p0, Latp;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 221
    :try_start_0
    const-string v0, "Babel"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    const-string v0, "Babel"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "setMode: this=%s old=%d new=%d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Latp;->p:I

    .line 223
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 222
    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    :cond_0
    iget v0, p0, Latp;->p:I

    if-eq p1, v0, :cond_5

    .line 226
    if-ne p1, v7, :cond_6

    .line 227
    iget-boolean v0, p0, Latp;->k:Z

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p0, Latp;->e:Latd;

    iget-object v2, p0, Latp;->K:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Latd;->a(Ljava/lang/Runnable;)V

    .line 230
    :cond_1
    iget-object v0, p0, Latp;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getVideoSsrcs()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    iput v0, p0, Latp;->A:I

    .line 231
    iget v0, p0, Latp;->A:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v2}, Lcwz;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 232
    iget-object v0, p0, Latp;->J:Lcom/google/android/libraries/hangouts/video/Decoder;

    if-eqz v0, :cond_2

    .line 233
    iget-object v0, p0, Latp;->J:Lcom/google/android/libraries/hangouts/video/Decoder;

    iget v2, p0, Latp;->A:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/hangouts/video/Decoder;->setSourceId(I)V

    .line 238
    :cond_2
    :goto_0
    iget v0, p0, Latp;->p:I

    .line 239
    iput p1, p0, Latp;->p:I

    .line 240
    if-eq v0, v7, :cond_3

    if-ne p1, v7, :cond_4

    .line 242
    :cond_3
    iget-object v0, p0, Latp;->f:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->f()V

    .line 244
    :cond_4
    iget-object v0, p0, Latp;->g:Lasx;

    invoke-virtual {v0}, Lasx;->e()V

    .line 246
    :cond_5
    monitor-exit v1

    return-void

    .line 236
    :cond_6
    const/4 v0, 0x0

    iput v0, p0, Latp;->A:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 246
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Latp;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Latp;->D()V

    return-void
.end method

.method static synthetic b(Latp;)V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Latp;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    instance-of v0, v0, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    invoke-static {v0}, Lcwz;->a(Z)V

    invoke-direct {p0}, Latp;->C()V

    iget-object v0, p0, Latp;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getAttachedData()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lash;

    if-eqz v1, :cond_0

    check-cast v0, Lash;

    invoke-virtual {v0}, Lash;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Latp;->z:Z

    :cond_0
    iget-object v0, p0, Latp;->C:Landroid/widget/ImageButton;

    iget-object v1, p0, Latp;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public A()Lcom/google/android/libraries/hangouts/video/Renderer;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Latp;->H:Lcom/google/android/libraries/hangouts/video/Renderer;

    return-object v0
.end method

.method public a(Lbzn;Lbyd;ZLzx;Z)V
    .locals 1

    .prologue
    .line 472
    invoke-static {p2}, Lcwz;->a(Ljava/lang/Object;)V

    .line 473
    const/4 v0, 0x0

    iput-object v0, p0, Latp;->G:Lzx;

    .line 474
    if-eqz p3, :cond_0

    .line 475
    invoke-virtual {p1}, Lbzn;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 476
    invoke-virtual {p0, v0}, Latp;->a(Landroid/graphics/Bitmap;)V

    .line 478
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lyb;Ljava/lang/String;Lyj;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 429
    iput-object v3, p0, Latp;->F:Lyb;

    .line 430
    iget-object v0, p0, Latp;->G:Lzx;

    if-eqz v0, :cond_0

    .line 431
    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    iget-object v1, p0, Latp;->G:Lzx;

    invoke-virtual {v0, v1}, Lbsn;->b(Lbrv;)V

    .line 434
    :cond_0
    iget-object v0, p0, Latp;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    .line 435
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 437
    iget-object v0, p0, Latp;->B:Latg;

    invoke-virtual {v0, p4}, Latg;->b(Ljava/lang/String;)V

    .line 444
    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 445
    new-instance v0, Lzx;

    new-instance v1, Lbyq;

    invoke-direct {v1, p2, p5}, Lbyq;-><init>(Ljava/lang/String;Lyj;)V

    .line 447
    invoke-static {}, Lyn;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Lbyq;->a(I)Lbyq;

    move-result-object v1

    invoke-virtual {v1, v4}, Lbyq;->d(Z)Lbyq;

    move-result-object v1

    invoke-direct {v0, v1, p0, v4, v3}, Lzx;-><init>(Lbyq;Laac;ZLjava/lang/Object;)V

    iput-object v0, p0, Latp;->G:Lzx;

    .line 449
    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    iget-object v1, p0, Latp;->G:Lzx;

    invoke-virtual {v0, v1}, Lbsn;->a(Lbrv;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 451
    iput-object v3, p0, Latp;->G:Lzx;

    .line 454
    :cond_2
    return-void
.end method

.method public a(Lyb;)V
    .locals 1

    .prologue
    .line 462
    const/4 v0, 0x0

    iput-object v0, p0, Latp;->F:Lyb;

    .line 463
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 407
    iget-object v1, p0, Latp;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 408
    :try_start_0
    invoke-super {p0}, Late;->b()V

    .line 409
    iget-object v0, p0, Latp;->H:Lcom/google/android/libraries/hangouts/video/Renderer;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/Renderer;->release()V

    .line 410
    const/4 v0, 0x0

    iput v0, p0, Latp;->r:I

    .line 412
    iget-object v0, p0, Latp;->F:Lyb;

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Latp;->F:Lyb;

    invoke-virtual {v0}, Lyb;->b()V

    .line 414
    const/4 v0, 0x0

    iput-object v0, p0, Latp;->F:Lyb;

    .line 417
    :cond_0
    iget-object v0, p0, Latp;->G:Lzx;

    if-eqz v0, :cond_1

    .line 418
    iget-object v0, p0, Latp;->G:Lzx;

    invoke-virtual {v0}, Lzx;->b()V

    .line 419
    const/4 v0, 0x0

    iput-object v0, p0, Latp;->G:Lzx;

    .line 422
    :cond_1
    iget-object v0, p0, Latp;->d:Lapk;

    iget-object v2, p0, Latp;->E:Lats;

    invoke-virtual {v0, v2}, Lapk;->b(Lapo;)V

    .line 423
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method b(Z)V
    .locals 0

    .prologue
    .line 386
    invoke-super {p0, p1}, Late;->b(Z)V

    .line 387
    invoke-direct {p0}, Latp;->D()V

    .line 388
    return-void
.end method

.method public f()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Latp;->w:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    iget-object v0, p0, Latp;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    instance-of v0, v0, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    if-eqz v0, :cond_1

    .line 196
    iget-object v0, p0, Latp;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    instance-of v0, v0, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    invoke-static {v0}, Lcwz;->a(Z)V

    iget-object v0, p0, Latp;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getAttachedData()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lash;

    if-eqz v0, :cond_0

    iget-object v0, p0, Latp;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getAttachedData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lash;

    invoke-virtual {v0}, Lash;->c()I

    move-result v0

    :goto_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 197
    invoke-static {}, Lyn;->o()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 200
    :goto_1
    return-object v0

    .line 196
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 200
    :cond_1
    invoke-super {p0}, Late;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1
.end method

.method o()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 204
    iget-boolean v1, p0, Latp;->i:Z

    invoke-static {v1}, Lcwz;->a(Z)V

    .line 205
    iget v1, p0, Latp;->r:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcwz;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 206
    iget v1, p0, Latp;->r:I

    iget-object v2, p0, Latp;->H:Lcom/google/android/libraries/hangouts/video/Renderer;

    .line 207
    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/Renderer;->getOutputTextureName()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Latp;->H:Lcom/google/android/libraries/hangouts/video/Renderer;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/Renderer;->isExternalTexture()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 4

    .prologue
    .line 393
    iget-object v1, p0, Latp;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 394
    :try_start_0
    iget-boolean v0, p0, Latp;->n:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Latp;->k:Z

    if-nez v0, :cond_0

    .line 395
    iget-object v0, p0, Latp;->h:Laui;

    iget-object v2, p0, Latp;->o:Landroid/graphics/Rect;

    iget v3, p0, Latp;->l:F

    invoke-virtual {v0, v2, v3}, Laui;->a(Landroid/graphics/Rect;F)V

    .line 397
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method r()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 338
    iget-object v1, p0, Latp;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 340
    :try_start_0
    iget-boolean v0, p0, Latp;->i:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Latp;->q()V

    iget-object v0, p0, Latp;->H:Lcom/google/android/libraries/hangouts/video/Renderer;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/Renderer;->initializeGLContext()V

    .line 341
    :cond_0
    iget v0, p0, Latp;->p:I

    if-ne v0, v4, :cond_4

    .line 343
    iget-object v0, p0, Latp;->H:Lcom/google/android/libraries/hangouts/video/Renderer;

    const/4 v2, 0x0

    iget-object v3, p0, Latp;->I:Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/hangouts/video/Renderer;->drawTexture(Lcom/google/android/libraries/hangouts/video/Renderer$DrawInputParams;Lcom/google/android/libraries/hangouts/video/Renderer$DrawOutputParams;)Z

    .line 346
    iget v0, p0, Latp;->q:I

    if-eq v0, v4, :cond_1

    iget-object v0, p0, Latp;->I:Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;

    iget-boolean v0, v0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;->updatedTexture:Z

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Latp;->I:Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;

    iget-boolean v0, v0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;->frameSizeChanged:Z

    if-eqz v0, :cond_4

    .line 349
    :cond_2
    iget v0, p0, Latp;->q:I

    if-eq v0, v4, :cond_3

    .line 350
    iget-object v0, p0, Latp;->d:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    invoke-virtual {v0}, Lapx;->v()V

    .line 351
    iget-object v0, p0, Latp;->e:Latd;

    iget-object v2, p0, Latp;->L:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Latd;->a(Ljava/lang/Runnable;)V

    .line 353
    :cond_3
    const/4 v0, 0x2

    iput v0, p0, Latp;->q:I

    .line 355
    iget-object v0, p0, Latp;->H:Lcom/google/android/libraries/hangouts/video/Renderer;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/Renderer;->getOutputTextureName()I

    move-result v0

    iget-object v2, p0, Latp;->I:Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;->frameWidth:I

    iget-object v3, p0, Latp;->I:Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;

    iget v3, v3, Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;->frameHeight:I

    invoke-virtual {p0, v0, v2, v3}, Latp;->a(III)Z

    .line 361
    :cond_4
    iget v0, p0, Latp;->p:I

    if-ne v0, v4, :cond_5

    iget v0, p0, Latp;->q:I

    if-eq v0, v4, :cond_a

    .line 363
    :cond_5
    iget-boolean v0, p0, Latp;->z:Z

    if-nez v0, :cond_7

    .line 365
    iget v0, p0, Latp;->y:I

    if-eqz v0, :cond_6

    .line 366
    iget v0, p0, Latp;->y:I

    invoke-static {v0}, Lf;->i(I)V

    .line 369
    :cond_6
    invoke-virtual {p0}, Latp;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lf;->b(Landroid/graphics/Bitmap;)I

    move-result v0

    iput v0, p0, Latp;->y:I

    .line 372
    :cond_7
    iget-boolean v0, p0, Latp;->z:Z

    if-eqz v0, :cond_8

    iget v0, p0, Latp;->q:I

    if-eq v0, v5, :cond_9

    .line 373
    :cond_8
    const/4 v0, 0x1

    iput v0, p0, Latp;->q:I

    .line 375
    invoke-virtual {p0}, Latp;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 376
    iget v2, p0, Latp;->y:I

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {p0, v2, v3, v0}, Latp;->a(III)Z

    .line 378
    :cond_9
    const/4 v0, 0x1

    iput-boolean v0, p0, Latp;->z:Z

    .line 380
    :cond_a
    iget v0, p0, Latp;->r:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v2}, Lcwz;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 381
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method z()V
    .locals 3

    .prologue
    .line 133
    invoke-static {}, Lcwz;->a()V

    .line 134
    iget-object v1, p0, Latp;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 135
    :try_start_0
    iget-object v0, p0, Latp;->d:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    .line 136
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lapx;->N()Z

    move-result v0

    if-nez v0, :cond_0

    .line 137
    iget-object v0, p0, Latp;->g:Lasx;

    invoke-virtual {v0}, Lasx;->d()V

    .line 140
    :cond_0
    iget-object v0, p0, Latp;->C:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 141
    iget-object v0, p0, Latp;->C:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 142
    iget-object v0, p0, Latp;->D:Landroid/widget/ImageView;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 143
    iget-boolean v0, p0, Latp;->m:Z

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Latp;->f:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->g()V

    .line 146
    :cond_1
    iget-object v0, p0, Latp;->f:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a(Late;)V

    .line 147
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
