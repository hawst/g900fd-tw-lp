.class public final Lbkv;
.super Lbnj;
.source "PG"


# instance fields
.field private final a:[J


# direct methods
.method constructor <init>(Lyj;[J)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lbnj;-><init>(Lyj;)V

    .line 24
    iput-object p2, p0, Lbkv;->a:[J

    .line 25
    return-void
.end method


# virtual methods
.method public a()V
    .locals 19

    .prologue
    .line 29
    new-instance v9, Lyt;

    move-object/from16 v0, p0

    iget-object v1, v0, Lbkv;->b:Lyj;

    invoke-direct {v9, v1}, Lyt;-><init>(Lyj;)V

    .line 30
    invoke-virtual {v9}, Lyt;->a()V

    .line 42
    const-wide/16 v2, -0x1

    .line 43
    :try_start_0
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 44
    move-object/from16 v0, p0

    iget-object v11, v0, Lbkv;->a:[J

    array-length v12, v11

    const/4 v1, 0x0

    move v8, v1

    :goto_0
    if-ge v8, v12, :cond_9

    aget-wide v13, v11, v8

    .line 45
    invoke-virtual {v9, v13, v14}, Lyt;->b(J)Lzg;

    move-result-object v15

    .line 47
    const/4 v7, 0x0

    .line 48
    const/4 v6, 0x0

    .line 49
    const/4 v5, 0x1

    .line 50
    const/4 v4, 0x0

    .line 51
    const/4 v1, 0x0

    .line 52
    if-eqz v15, :cond_a

    .line 53
    iget-object v4, v15, Lzg;->b:Ljava/lang/String;

    .line 54
    iget v1, v15, Lzg;->w:I

    invoke-static {v1}, Lf;->b(I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 55
    const-class v1, Lbfq;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 57
    const/4 v7, 0x1

    .line 66
    :goto_1
    if-eqz v7, :cond_0

    .line 69
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->g()Landroid/content/Intent;

    move-result-object v7

    .line 70
    const-string v16, "cancel_class"

    move-object/from16 v0, v16

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 71
    const-string v1, "cancel_request"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v15, Lzg;->a:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "|"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "|"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lbkv;->b:Lyj;

    move-object/from16 v17, v0

    .line 74
    invoke-virtual/range {v17 .. v17}, Lyj;->b()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "|"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 71
    move-object/from16 v0, v16

    invoke-virtual {v7, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 77
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 79
    :cond_0
    if-eqz v6, :cond_1

    .line 80
    iget-object v1, v15, Lzg;->a:Ljava/lang/String;

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    invoke-virtual {v9, v4, v13, v14}, Lyt;->g(Ljava/lang/String;J)V

    .line 83
    iget-wide v6, v15, Lzg;->h:J

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    move-wide v2, v1

    .line 85
    :cond_1
    if-eqz v5, :cond_2

    .line 87
    invoke-static {v9, v13, v14}, Lyp;->a(Lyt;J)V

    .line 91
    :cond_2
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 92
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 93
    new-instance v5, Lbku;

    invoke-direct {v5, v4, v2, v3, v1}, Lbku;-><init>(Ljava/lang/String;J[Ljava/lang/String;)V

    .line 96
    move-object/from16 v0, p0

    iget-object v1, v0, Lbkv;->c:Lbnl;

    invoke-virtual {v5, v1}, Lbku;->a(Lbnl;)V

    .line 98
    :cond_3
    if-eqz v4, :cond_4

    .line 99
    invoke-static {v9, v4}, Lyp;->d(Lyt;Ljava/lang/String;)V

    .line 44
    :cond_4
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto/16 :goto_0

    .line 59
    :cond_5
    const-class v1, Lbfb;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    .line 60
    iget v1, v15, Lzg;->f:I

    const/4 v6, 0x2

    if-ne v1, v6, :cond_6

    const/4 v7, 0x1

    .line 61
    :goto_2
    iget v1, v15, Lzg;->f:I

    const/4 v6, 0x4

    if-ne v1, v6, :cond_7

    const/4 v6, 0x1

    .line 62
    :goto_3
    if-nez v7, :cond_8

    if-nez v6, :cond_8

    const/4 v1, 0x1

    :goto_4
    move-object/from16 v18, v5

    move v5, v1

    move-object/from16 v1, v18

    goto/16 :goto_1

    .line 60
    :cond_6
    const/4 v7, 0x0

    goto :goto_2

    .line 61
    :cond_7
    const/4 v6, 0x0

    goto :goto_3

    .line 62
    :cond_8
    const/4 v1, 0x0

    goto :goto_4

    .line 103
    :cond_9
    invoke-virtual {v9}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    invoke-virtual {v9}, Lyt;->c()V

    .line 106
    return-void

    .line 105
    :catchall_0
    move-exception v1

    invoke-virtual {v9}, Lyt;->c()V

    throw v1

    :cond_a
    move-object/from16 v18, v1

    move-object v1, v4

    move-object/from16 v4, v18

    goto/16 :goto_1
.end method
