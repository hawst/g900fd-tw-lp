.class public final Lduz;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lduz;


# instance fields
.field public b:Ldvm;

.field public c:[Ldui;

.field public d:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13616
    const/4 v0, 0x0

    new-array v0, v0, [Lduz;

    sput-object v0, Lduz;->a:[Lduz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13617
    invoke-direct {p0}, Lepn;-><init>()V

    .line 13633
    const/4 v0, 0x0

    iput-object v0, p0, Lduz;->b:Ldvm;

    .line 13636
    sget-object v0, Ldui;->a:[Ldui;

    iput-object v0, p0, Lduz;->c:[Ldui;

    .line 13639
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Lduz;->d:[I

    .line 13617
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 13666
    iget-object v0, p0, Lduz;->b:Ldvm;

    if-eqz v0, :cond_4

    .line 13667
    const/4 v0, 0x1

    iget-object v2, p0, Lduz;->b:Ldvm;

    .line 13668
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 13670
    :goto_0
    iget-object v2, p0, Lduz;->c:[Ldui;

    if-eqz v2, :cond_1

    .line 13671
    iget-object v3, p0, Lduz;->c:[Ldui;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 13672
    if-eqz v5, :cond_0

    .line 13673
    const/4 v6, 0x2

    .line 13674
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 13671
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 13678
    :cond_1
    iget-object v2, p0, Lduz;->d:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Lduz;->d:[I

    array-length v2, v2

    if-lez v2, :cond_3

    .line 13680
    iget-object v3, p0, Lduz;->d:[I

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_2

    aget v5, v3, v1

    .line 13682
    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 13680
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 13684
    :cond_2
    add-int/2addr v0, v2

    .line 13685
    iget-object v1, p0, Lduz;->d:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 13687
    :cond_3
    iget-object v1, p0, Lduz;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13688
    iput v0, p0, Lduz;->cachedSize:I

    .line 13689
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 13613
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lduz;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lduz;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lduz;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lduz;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Lduz;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Lduz;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lduz;->c:[Ldui;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldui;

    iget-object v3, p0, Lduz;->c:[Ldui;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lduz;->c:[Ldui;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lduz;->c:[Ldui;

    :goto_2
    iget-object v2, p0, Lduz;->c:[Ldui;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lduz;->c:[Ldui;

    new-instance v3, Ldui;

    invoke-direct {v3}, Ldui;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lduz;->c:[Ldui;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lduz;->c:[Ldui;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lduz;->c:[Ldui;

    new-instance v3, Ldui;

    invoke-direct {v3}, Ldui;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lduz;->c:[Ldui;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lduz;->d:[I

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [I

    iget-object v3, p0, Lduz;->d:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lduz;->d:[I

    :goto_3
    iget-object v2, p0, Lduz;->d:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Lduz;->d:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    iget-object v2, p0, Lduz;->d:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 13644
    iget-object v1, p0, Lduz;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 13645
    const/4 v1, 0x1

    iget-object v2, p0, Lduz;->b:Ldvm;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 13647
    :cond_0
    iget-object v1, p0, Lduz;->c:[Ldui;

    if-eqz v1, :cond_2

    .line 13648
    iget-object v2, p0, Lduz;->c:[Ldui;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 13649
    if-eqz v4, :cond_1

    .line 13650
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 13648
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 13654
    :cond_2
    iget-object v1, p0, Lduz;->d:[I

    if-eqz v1, :cond_3

    iget-object v1, p0, Lduz;->d:[I

    array-length v1, v1

    if-lez v1, :cond_3

    .line 13655
    iget-object v1, p0, Lduz;->d:[I

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget v3, v1, v0

    .line 13656
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 13655
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 13659
    :cond_3
    iget-object v0, p0, Lduz;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 13661
    return-void
.end method
