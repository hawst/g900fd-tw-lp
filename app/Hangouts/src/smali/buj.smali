.class final Lbuj;
.super Lack;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lack",
        "<",
        "Lbdg;",
        "Lbib;",
        ">;"
    }
.end annotation


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Lyj;

.field private final f:Landroid/preference/ListPreference;

.field private final g:Laaj;

.field private final h:Ljava/lang/String;

.field private i:Lbme;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lyj;Landroid/preference/ListPreference;Laaj;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 98
    invoke-direct {p0}, Lack;-><init>()V

    .line 99
    iput-object p1, p0, Lbuj;->d:Landroid/content/Context;

    .line 100
    iput-object p2, p0, Lbuj;->e:Lyj;

    .line 101
    iput-object p3, p0, Lbuj;->f:Landroid/preference/ListPreference;

    .line 102
    iput-object p4, p0, Lbuj;->g:Laaj;

    .line 103
    iput-object p5, p0, Lbuj;->h:Ljava/lang/String;

    .line 104
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbme;

    invoke-static {v0, v1}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    iput-object v0, p0, Lbuj;->i:Lbme;

    .line 105
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lbuj;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->ad:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 116
    invoke-super {p0, p1}, Lack;->a(Ljava/lang/Exception;)V

    .line 118
    iget-object v0, p0, Lbuj;->f:Landroid/preference/ListPreference;

    iget-object v1, p0, Lbuj;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lbuj;->f:Landroid/preference/ListPreference;

    iget-object v1, p0, Lbuj;->f:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lbuj;->f:Landroid/preference/ListPreference;

    iget-object v3, p0, Lbuj;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 119
    return-void
.end method

.method public b()I
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lbuj;->i:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x63a

    .line 110
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 109
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 111
    iget-object v0, p0, Lbuj;->e:Lyj;

    iget-object v1, p0, Lbuj;->g:Laaj;

    invoke-static {v0, v1}, Laai;->a(Lyj;Laaj;)I

    move-result v0

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 123
    invoke-static {}, Lbuj;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    iget-object v0, p0, Lbuj;->d:Landroid/content/Context;

    sget v1, Lh;->af:I

    invoke-static {v0, v1}, Lf;->a(Landroid/content/Context;I)V

    .line 128
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v0, p0, Lbuj;->d:Landroid/content/Context;

    sget v1, Lh;->ae:I

    invoke-static {v0, v1}, Lf;->a(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public d()V
    .locals 0

    .prologue
    .line 131
    return-void
.end method

.method public e()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lbdg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    const-class v0, Lbdg;

    return-object v0
.end method

.method public f()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lbib;",
            ">;"
        }
    .end annotation

    .prologue
    .line 146
    const-class v0, Lbib;

    return-object v0
.end method
