.class public final Ldnv;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldnv;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ldnw;

.field public d:Ljava/lang/Integer;

.field public e:Ldnu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 824
    const/4 v0, 0x0

    new-array v0, v0, [Ldnv;

    sput-object v0, Ldnv;->a:[Ldnv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 825
    invoke-direct {p0}, Lepn;-><init>()V

    .line 830
    iput-object v0, p0, Ldnv;->c:Ldnw;

    .line 833
    iput-object v0, p0, Ldnv;->d:Ljava/lang/Integer;

    .line 836
    iput-object v0, p0, Ldnv;->e:Ldnu;

    .line 825
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 859
    const/4 v0, 0x0

    .line 860
    iget-object v1, p0, Ldnv;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 861
    const/4 v0, 0x1

    iget-object v1, p0, Ldnv;->b:Ljava/lang/String;

    .line 862
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 864
    :cond_0
    iget-object v1, p0, Ldnv;->c:Ldnw;

    if-eqz v1, :cond_1

    .line 865
    const/4 v1, 0x2

    iget-object v2, p0, Ldnv;->c:Ldnw;

    .line 866
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 868
    :cond_1
    iget-object v1, p0, Ldnv;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 869
    const/4 v1, 0x3

    iget-object v2, p0, Ldnv;->d:Ljava/lang/Integer;

    .line 870
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 872
    :cond_2
    iget-object v1, p0, Ldnv;->e:Ldnu;

    if-eqz v1, :cond_3

    .line 873
    const/4 v1, 0x4

    iget-object v2, p0, Ldnv;->e:Ldnu;

    .line 874
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 876
    :cond_3
    iget-object v1, p0, Ldnv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 877
    iput v0, p0, Ldnv;->cachedSize:I

    .line 878
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 821
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldnv;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldnv;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldnv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnv;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldnv;->c:Ldnw;

    if-nez v0, :cond_2

    new-instance v0, Ldnw;

    invoke-direct {v0}, Ldnw;-><init>()V

    iput-object v0, p0, Ldnv;->c:Ldnw;

    :cond_2
    iget-object v0, p0, Ldnv;->c:Ldnw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldnv;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldnv;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldnv;->e:Ldnu;

    if-nez v0, :cond_5

    new-instance v0, Ldnu;

    invoke-direct {v0}, Ldnu;-><init>()V

    iput-object v0, p0, Ldnv;->e:Ldnu;

    :cond_5
    iget-object v0, p0, Ldnv;->e:Ldnu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 841
    iget-object v0, p0, Ldnv;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 842
    const/4 v0, 0x1

    iget-object v1, p0, Ldnv;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 844
    :cond_0
    iget-object v0, p0, Ldnv;->c:Ldnw;

    if-eqz v0, :cond_1

    .line 845
    const/4 v0, 0x2

    iget-object v1, p0, Ldnv;->c:Ldnw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 847
    :cond_1
    iget-object v0, p0, Ldnv;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 848
    const/4 v0, 0x3

    iget-object v1, p0, Ldnv;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 850
    :cond_2
    iget-object v0, p0, Ldnv;->e:Ldnu;

    if-eqz v0, :cond_3

    .line 851
    const/4 v0, 0x4

    iget-object v1, p0, Ldnv;->e:Ldnu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 853
    :cond_3
    iget-object v0, p0, Ldnv;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 855
    return-void
.end method
