.class public final Ldtp;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldtp;


# instance fields
.field public b:Ldqf;

.field public c:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9830
    const/4 v0, 0x0

    new-array v0, v0, [Ldtp;

    sput-object v0, Ldtp;->a:[Ldtp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9831
    invoke-direct {p0}, Lepn;-><init>()V

    .line 9834
    const/4 v0, 0x0

    iput-object v0, p0, Ldtp;->b:Ldqf;

    .line 9837
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldtp;->c:[Ljava/lang/String;

    .line 9831
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 9857
    iget-object v0, p0, Ldtp;->b:Ldqf;

    if-eqz v0, :cond_2

    .line 9858
    const/4 v0, 0x2

    iget-object v2, p0, Ldtp;->b:Ldqf;

    .line 9859
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9861
    :goto_0
    iget-object v2, p0, Ldtp;->c:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Ldtp;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 9863
    iget-object v3, p0, Ldtp;->c:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 9865
    invoke-static {v5}, Lepl;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 9863
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 9867
    :cond_0
    add-int/2addr v0, v2

    .line 9868
    iget-object v1, p0, Ldtp;->c:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 9870
    :cond_1
    iget-object v1, p0, Ldtp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9871
    iput v0, p0, Ldtp;->cachedSize:I

    .line 9872
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 9827
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldtp;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldtp;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldtp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldtp;->b:Ldqf;

    if-nez v0, :cond_2

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldtp;->b:Ldqf;

    :cond_2
    iget-object v0, p0, Ldtp;->b:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldtp;->c:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Ldtp;->c:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldtp;->c:[Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Ldtp;->c:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Ldtp;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Ldtp;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x22 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 9842
    iget-object v0, p0, Ldtp;->b:Ldqf;

    if-eqz v0, :cond_0

    .line 9843
    const/4 v0, 0x2

    iget-object v1, p0, Ldtp;->b:Ldqf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 9845
    :cond_0
    iget-object v0, p0, Ldtp;->c:[Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 9846
    iget-object v1, p0, Ldtp;->c:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 9847
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lepl;->a(ILjava/lang/String;)V

    .line 9846
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 9850
    :cond_1
    iget-object v0, p0, Ldtp;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 9852
    return-void
.end method
