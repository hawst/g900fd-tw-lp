.class public final Leng;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leng;


# instance fields
.field public b:Ljava/lang/Boolean;

.field public c:Leln;

.field public d:Lelh;

.field public e:Leli;

.field public f:Lelj;

.field public g:Lell;

.field public h:Lenf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 239
    const/4 v0, 0x0

    new-array v0, v0, [Leng;

    sput-object v0, Leng;->a:[Leng;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 240
    invoke-direct {p0}, Lepn;-><init>()V

    .line 245
    iput-object v0, p0, Leng;->c:Leln;

    .line 248
    iput-object v0, p0, Leng;->d:Lelh;

    .line 251
    iput-object v0, p0, Leng;->e:Leli;

    .line 254
    iput-object v0, p0, Leng;->f:Lelj;

    .line 257
    iput-object v0, p0, Leng;->g:Lell;

    .line 260
    iput-object v0, p0, Leng;->h:Lenf;

    .line 240
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 292
    const/4 v0, 0x0

    .line 293
    iget-object v1, p0, Leng;->c:Leln;

    if-eqz v1, :cond_0

    .line 294
    const/4 v0, 0x1

    iget-object v1, p0, Leng;->c:Leln;

    .line 295
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 297
    :cond_0
    iget-object v1, p0, Leng;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 298
    const/4 v1, 0x2

    iget-object v2, p0, Leng;->b:Ljava/lang/Boolean;

    .line 299
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 301
    :cond_1
    iget-object v1, p0, Leng;->d:Lelh;

    if-eqz v1, :cond_2

    .line 302
    const/4 v1, 0x3

    iget-object v2, p0, Leng;->d:Lelh;

    .line 303
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 305
    :cond_2
    iget-object v1, p0, Leng;->e:Leli;

    if-eqz v1, :cond_3

    .line 306
    const/4 v1, 0x4

    iget-object v2, p0, Leng;->e:Leli;

    .line 307
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 309
    :cond_3
    iget-object v1, p0, Leng;->f:Lelj;

    if-eqz v1, :cond_4

    .line 310
    const/4 v1, 0x5

    iget-object v2, p0, Leng;->f:Lelj;

    .line 311
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 313
    :cond_4
    iget-object v1, p0, Leng;->g:Lell;

    if-eqz v1, :cond_5

    .line 314
    const/4 v1, 0x6

    iget-object v2, p0, Leng;->g:Lell;

    .line 315
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 317
    :cond_5
    iget-object v1, p0, Leng;->h:Lenf;

    if-eqz v1, :cond_6

    .line 318
    const/4 v1, 0x7

    iget-object v2, p0, Leng;->h:Lenf;

    .line 319
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 321
    :cond_6
    iget-object v1, p0, Leng;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 322
    iput v0, p0, Leng;->cachedSize:I

    .line 323
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 236
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leng;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leng;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leng;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Leng;->c:Leln;

    if-nez v0, :cond_2

    new-instance v0, Leln;

    invoke-direct {v0}, Leln;-><init>()V

    iput-object v0, p0, Leng;->c:Leln;

    :cond_2
    iget-object v0, p0, Leng;->c:Leln;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Leng;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Leng;->d:Lelh;

    if-nez v0, :cond_3

    new-instance v0, Lelh;

    invoke-direct {v0}, Lelh;-><init>()V

    iput-object v0, p0, Leng;->d:Lelh;

    :cond_3
    iget-object v0, p0, Leng;->d:Lelh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Leng;->e:Leli;

    if-nez v0, :cond_4

    new-instance v0, Leli;

    invoke-direct {v0}, Leli;-><init>()V

    iput-object v0, p0, Leng;->e:Leli;

    :cond_4
    iget-object v0, p0, Leng;->e:Leli;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Leng;->f:Lelj;

    if-nez v0, :cond_5

    new-instance v0, Lelj;

    invoke-direct {v0}, Lelj;-><init>()V

    iput-object v0, p0, Leng;->f:Lelj;

    :cond_5
    iget-object v0, p0, Leng;->f:Lelj;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Leng;->g:Lell;

    if-nez v0, :cond_6

    new-instance v0, Lell;

    invoke-direct {v0}, Lell;-><init>()V

    iput-object v0, p0, Leng;->g:Lell;

    :cond_6
    iget-object v0, p0, Leng;->g:Lell;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Leng;->h:Lenf;

    if-nez v0, :cond_7

    new-instance v0, Lenf;

    invoke-direct {v0}, Lenf;-><init>()V

    iput-object v0, p0, Leng;->h:Lenf;

    :cond_7
    iget-object v0, p0, Leng;->h:Lenf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, Leng;->c:Leln;

    if-eqz v0, :cond_0

    .line 266
    const/4 v0, 0x1

    iget-object v1, p0, Leng;->c:Leln;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 268
    :cond_0
    iget-object v0, p0, Leng;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 269
    const/4 v0, 0x2

    iget-object v1, p0, Leng;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 271
    :cond_1
    iget-object v0, p0, Leng;->d:Lelh;

    if-eqz v0, :cond_2

    .line 272
    const/4 v0, 0x3

    iget-object v1, p0, Leng;->d:Lelh;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 274
    :cond_2
    iget-object v0, p0, Leng;->e:Leli;

    if-eqz v0, :cond_3

    .line 275
    const/4 v0, 0x4

    iget-object v1, p0, Leng;->e:Leli;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 277
    :cond_3
    iget-object v0, p0, Leng;->f:Lelj;

    if-eqz v0, :cond_4

    .line 278
    const/4 v0, 0x5

    iget-object v1, p0, Leng;->f:Lelj;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 280
    :cond_4
    iget-object v0, p0, Leng;->g:Lell;

    if-eqz v0, :cond_5

    .line 281
    const/4 v0, 0x6

    iget-object v1, p0, Leng;->g:Lell;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 283
    :cond_5
    iget-object v0, p0, Leng;->h:Lenf;

    if-eqz v0, :cond_6

    .line 284
    const/4 v0, 0x7

    iget-object v1, p0, Leng;->h:Lenf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 286
    :cond_6
    iget-object v0, p0, Leng;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 288
    return-void
.end method
