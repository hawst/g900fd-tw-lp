.class public abstract Lcnx;
.super Lcob;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Lcnx",
        "<TM;>;>",
        "Lcob;"
    }
.end annotation


# instance fields
.field public a:Lcny;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcob;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcnx;->a:Lcny;

    if-eqz v1, :cond_0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lcnx;->a:Lcny;

    invoke-virtual {v2}, Lcny;->a()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcnx;->a:Lcny;

    invoke-virtual {v2, v0}, Lcny;->a(I)Lcnz;

    move-result-object v2

    invoke-virtual {v2}, Lcnz;->a()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v1, v0

    :cond_1
    return v1
.end method

.method public a(Lcnw;)V
    .locals 2

    iget-object v0, p0, Lcnx;->a:Lcny;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcnx;->a:Lcny;

    invoke-virtual {v1}, Lcny;->a()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcnx;->a:Lcny;

    invoke-virtual {v1, v0}, Lcny;->a(I)Lcnz;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcnz;->a(Lcnw;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final a(Lcnx;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;)Z"
        }
    .end annotation

    iget-object v0, p0, Lcnx;->a:Lcny;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcnx;->a:Lcny;

    invoke-virtual {v0}, Lcny;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p1, Lcnx;->a:Lcny;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcnx;->a:Lcny;

    invoke-virtual {v0}, Lcny;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcnx;->a:Lcny;

    iget-object v1, p1, Lcnx;->a:Lcny;

    invoke-virtual {v0, v1}, Lcny;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    iget-object v0, p0, Lcnx;->a:Lcny;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcnx;->a:Lcny;

    invoke-virtual {v0}, Lcny;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcnx;->a:Lcny;

    invoke-virtual {v0}, Lcny;->hashCode()I

    move-result v0

    goto :goto_0
.end method
