.class public final Lcba;
.super Lcax;
.source "PG"


# instance fields
.field private final a:Landroid/widget/TextView;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private final e:Landroid/text/SpannableStringBuilder;

.field private f:I

.field private g:I

.field private final h:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcba;-><init>(Landroid/content/Context;B)V

    .line 35
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 2

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcax;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcba;->e:Landroid/text/SpannableStringBuilder;

    .line 39
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lf;->ew:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 40
    sget v0, Lg;->eH:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcba;->a:Landroid/widget/TextView;

    .line 41
    sget v0, Lg;->er:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcba;->h:Landroid/widget/TextView;

    .line 42
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcba;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 72
    if-nez p1, :cond_0

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcba;->d:Ljava/lang/String;

    .line 77
    :goto_0
    return-void

    .line 75
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcba;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 7

    .prologue
    .line 45
    iput-object p1, p0, Lcba;->b:Ljava/lang/String;

    .line 46
    iput p2, p0, Lcba;->f:I

    .line 47
    iput-object p3, p0, Lcba;->c:Ljava/lang/String;

    .line 48
    iput p4, p0, Lcba;->g:I

    .line 50
    iget-object v0, p0, Lcba;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcba;->e:Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, Lcba;->d:Ljava/lang/String;

    invoke-virtual {p0, v0, p3, v1, v2}, Lcba;->a(Landroid/widget/TextView;Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcba;->h:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcba;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->he:I

    iget v3, p0, Lcba;->g:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcba;->g:I

    .line 52
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 51
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcba;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcba;->f:I

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcba;->g:I

    return v0
.end method

.method public isLocked(Laoe;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81
    iget-object v0, p0, Lcba;->b:Ljava/lang/String;

    invoke-interface {p1, v1, v1, v0}, Laoe;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
