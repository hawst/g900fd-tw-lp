.class public final Lceg;
.super Landroid/view/animation/Animation;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/views/RichStatusView;

.field private final b:Z

.field private final c:Lceh;

.field private final d:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/views/RichStatusView;Lceh;IZ)V
    .locals 2

    .prologue
    .line 389
    iput-object p1, p0, Lceg;->a:Lcom/google/android/apps/hangouts/views/RichStatusView;

    .line 390
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 391
    iput-object p2, p0, Lceg;->c:Lceh;

    .line 392
    iput-boolean p4, p0, Lceg;->b:Z

    .line 393
    iget-object v0, p0, Lceg;->c:Lceh;

    iget-object v0, v0, Lceh;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 395
    iput p3, p0, Lceg;->d:I

    .line 399
    iget-object v0, p0, Lceg;->c:Lceh;

    iget-object v0, v0, Lceh;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget-boolean v0, p0, Lceg;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 400
    iget-object v0, p0, Lceg;->c:Lceh;

    iget-object v0, v0, Lceh;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 401
    return-void

    .line 399
    :cond_0
    iget v0, p0, Lceg;->d:I

    goto :goto_0
.end method


# virtual methods
.method public applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 406
    cmpl-float v0, p1, v1

    if-nez v0, :cond_1

    .line 408
    iget v0, p0, Lceg;->d:I

    .line 421
    :cond_0
    :goto_0
    iget-object v1, p0, Lceg;->c:Lceh;

    iget-object v1, v1, Lceh;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 422
    iget-object v0, p0, Lceg;->c:Lceh;

    iget-object v0, v0, Lceh;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 423
    return-void

    .line 411
    :cond_1
    iget-boolean v0, p0, Lceg;->b:Z

    if-nez v0, :cond_2

    .line 412
    sub-float p1, v1, p1

    .line 415
    :cond_2
    iget v0, p0, Lceg;->d:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    .line 416
    if-gtz v0, :cond_0

    .line 417
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public willChangeBounds()Z
    .locals 1

    .prologue
    .line 427
    const/4 v0, 0x1

    return v0
.end method
