.class public final Lcwf;
.super Lcwa;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcwa",
        "<",
        "Lcwe;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Lcnd;

.field private final c:Lcnc;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;Lcnd;Lcnc;)V
    .locals 0

    invoke-direct {p0, p1}, Lcwa;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    iput-object p2, p0, Lcwf;->b:Lcnd;

    iput-object p3, p0, Lcwf;->c:Lcnc;

    return-void
.end method


# virtual methods
.method public synthetic a(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcwf;->b(I)Lcwe;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lcwe;
    .locals 6

    new-instance v0, Lcmf;

    iget-object v1, p0, Lcwf;->a:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {p0}, Lcwf;->f()Landroid/os/Bundle;

    move-result-object v3

    iget-object v4, p0, Lcwf;->b:Lcnd;

    iget-object v5, p0, Lcwf;->c:Lcnc;

    move v2, p1

    invoke-direct/range {v0 .. v5}, Lcmf;-><init>(Lcom/google/android/gms/common/data/DataHolder;ILandroid/os/Bundle;Lcnd;Lcnc;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "People:size="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcwf;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
