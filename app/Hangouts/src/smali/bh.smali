.class public final Lbh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Ljava/lang/CharSequence;

.field private final c:Landroid/app/PendingIntent;

.field private final d:Landroid/os/Bundle;

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 1506
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, Lbh;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;)V

    .line 1507
    return-void
.end method

.method private constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1518
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1519
    iput p1, p0, Lbh;->a:I

    .line 1520
    iput-object p2, p0, Lbh;->b:Ljava/lang/CharSequence;

    .line 1521
    iput-object p3, p0, Lbh;->c:Landroid/app/PendingIntent;

    .line 1522
    iput-object p4, p0, Lbh;->d:Landroid/os/Bundle;

    .line 1523
    return-void
.end method


# virtual methods
.method public a()Lbg;
    .locals 7

    .prologue
    .line 1578
    iget-object v0, p0, Lbh;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbh;->e:Ljava/util/ArrayList;

    iget-object v1, p0, Lbh;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcl;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcl;

    move-object v5, v0

    .line 1580
    :goto_0
    new-instance v0, Lbg;

    iget v1, p0, Lbh;->a:I

    iget-object v2, p0, Lbh;->b:Ljava/lang/CharSequence;

    iget-object v3, p0, Lbh;->c:Landroid/app/PendingIntent;

    iget-object v4, p0, Lbh;->d:Landroid/os/Bundle;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lbg;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Lcl;B)V

    return-object v0

    .line 1578
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public a(Lcl;)Lbh;
    .locals 1

    .prologue
    .line 1556
    iget-object v0, p0, Lbh;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1557
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbh;->e:Ljava/util/ArrayList;

    .line 1559
    :cond_0
    iget-object v0, p0, Lbh;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1560
    return-object p0
.end method
