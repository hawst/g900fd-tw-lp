.class public final Lbkq;
.super Lbnj;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field private final d:Z

.field private e:I

.field private final f:Lxm;

.field private g:I

.field private h:I

.field private final i:Z

.field private final j:Z


# direct methods
.method public constructor <init>(Lyj;Lxm;ZZZI)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lbnj;-><init>(Lyj;)V

    .line 55
    iput-object p2, p0, Lbkq;->f:Lxm;

    .line 56
    iput-boolean p3, p0, Lbkq;->i:Z

    .line 57
    iput-boolean p4, p0, Lbkq;->d:Z

    .line 58
    const/4 v0, 0x1

    iput v0, p0, Lbkq;->h:I

    .line 59
    iput-boolean p5, p0, Lbkq;->j:Z

    .line 60
    iput p6, p0, Lbkq;->g:I

    .line 61
    return-void
.end method


# virtual methods
.method public a()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x1

    .line 68
    iget-object v0, p0, Lbkq;->b:Lyj;

    iget-object v0, p0, Lbkq;->f:Lxm;

    .line 69
    invoke-static {v0}, Lf;->a(Lxm;)Ljava/util/ArrayList;

    move-result-object v2

    .line 71
    iget-object v0, p0, Lbkq;->b:Lyj;

    iget-object v0, p0, Lbkq;->f:Lxm;

    .line 72
    invoke-static {v0}, Lf;->b(Lxm;)Ljava/util/ArrayList;

    move-result-object v3

    .line 75
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    const/16 v4, 0x64

    if-le v0, v4, :cond_0

    .line 76
    const/4 v0, 0x4

    iput v0, p0, Lbkq;->h:I

    .line 102
    :goto_0
    return-void

    .line 83
    :cond_0
    iget-boolean v0, p0, Lbkq;->j:Z

    if-nez v0, :cond_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, v8, :cond_1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    :cond_1
    const/4 v0, 0x2

    :goto_1
    iput v0, p0, Lbkq;->e:I

    .line 86
    const/4 v4, 0x0

    .line 87
    iget v0, p0, Lbkq;->e:I

    if-ne v0, v8, :cond_2

    .line 88
    iget-object v0, p0, Lbkq;->f:Lxm;

    invoke-virtual {v0, v1}, Lxm;->a(I)Lxs;

    move-result-object v0

    .line 89
    if-eqz v0, :cond_2

    .line 90
    invoke-virtual {v0}, Lxs;->b()Lbcx;

    move-result-object v0

    invoke-virtual {v0}, Lbcx;->b()Ljava/util/List;

    move-result-object v4

    .line 94
    :cond_2
    new-instance v0, Lyt;

    iget-object v5, p0, Lbkq;->b:Lyj;

    invoke-direct {v0, v5}, Lyt;-><init>(Lyj;)V

    iget-boolean v5, p0, Lbkq;->i:Z

    iget v6, p0, Lbkq;->e:I

    iget v7, p0, Lbkq;->g:I

    iget-object v9, p0, Lbkq;->c:Lbnl;

    .line 95
    invoke-static/range {v0 .. v9}, Lyp;->a(Lyt;ZLjava/util/List;Ljava/util/List;Ljava/util/List;ZIIZLbnl;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbkq;->a:Ljava/lang/String;

    goto :goto_0

    :cond_3
    move v0, v8

    .line 83
    goto :goto_1
.end method

.method public b()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lbkq;->h:I

    return v0
.end method

.method public c()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 121
    new-instance v0, Lbkr;

    iget-object v1, p0, Lbkq;->a:Ljava/lang/String;

    iget-object v2, p0, Lbkq;->b:Lyj;

    iget v3, p0, Lbkq;->e:I

    iget-boolean v4, p0, Lbkq;->d:Z

    invoke-direct {v0, v1, v2, v3, v4}, Lbkr;-><init>(Ljava/lang/String;Lyj;IZ)V

    return-object v0
.end method
