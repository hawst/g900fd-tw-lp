.class final Lbuu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic a:Lbus;


# direct methods
.method constructor <init>(Lbus;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lbuu;->a:Lbus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 229
    instance-of v2, p2, Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 230
    check-cast p2, Ljava/lang/Boolean;

    invoke-static {p2, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v1

    .line 231
    iget-object v2, p0, Lbuu;->a:Lbus;

    new-instance v3, Lace;

    iget-object v4, p0, Lbuu;->a:Lbus;

    iget-object v4, v4, Lbus;->c:Landroid/app/Activity;

    invoke-direct {v3, v4}, Lace;-><init>(Landroid/content/Context;)V

    iput-object v3, v2, Lbus;->d:Lach;

    .line 233
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lbuu;->a:Lbus;

    iget-object v3, v3, Lbus;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 234
    new-instance v2, Lbuw;

    iget-object v3, p0, Lbuu;->a:Lbus;

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4, v1}, Lbuw;-><init>(Lbus;IZ)V

    .line 237
    iget-object v1, p0, Lbuu;->a:Lbus;

    .line 238
    iget-object v1, v1, Lbus;->d:Lach;

    .line 237
    invoke-static {v2, v1}, Lacc;->a(Laci;Lach;)Lacc;

    move-result-object v1

    .line 238
    invoke-virtual {v1}, Lacc;->a()V

    .line 250
    :cond_0
    :goto_0
    return v0

    .line 239
    :cond_1
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lbuu;->a:Lbus;

    iget-object v3, v3, Lbus;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 240
    new-instance v2, Lbuw;

    iget-object v3, p0, Lbuu;->a:Lbus;

    invoke-direct {v2, v3, v0, v1}, Lbuw;-><init>(Lbus;IZ)V

    .line 244
    iget-object v1, p0, Lbuu;->a:Lbus;

    .line 245
    iget-object v1, v1, Lbus;->d:Lach;

    .line 244
    invoke-static {v2, v1}, Lacc;->a(Laci;Lach;)Lacc;

    move-result-object v1

    .line 245
    invoke-virtual {v1}, Lacc;->a()V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 250
    goto :goto_0
.end method
