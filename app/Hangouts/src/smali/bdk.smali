.class public final Lbdk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lbdk;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 237
    new-instance v0, Lbdl;

    invoke-direct {v0}, Lbdl;-><init>()V

    sput-object v0, Lbdk;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ldui;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    if-eqz p1, :cond_4

    .line 48
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 49
    invoke-static {p2}, Lbdk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbdk;->b:Ljava/lang/String;

    .line 56
    :goto_0
    iget-object v0, p1, Ldui;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p1, Ldui;->c:Ljava/lang/String;

    .line 57
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 58
    iget-object v0, p1, Ldui;->c:Ljava/lang/String;

    iput-object v0, p0, Lbdk;->a:Ljava/lang/String;

    .line 63
    :goto_1
    iget-object v0, p1, Ldui;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const-string v0, "Babel"

    const-string v1, "Received empty gaiaid."

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 70
    :cond_0
    :goto_2
    return-void

    .line 50
    :cond_1
    iget-object v0, p1, Ldui;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p1, Ldui;->b:Ljava/lang/String;

    .line 51
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 52
    iget-object v0, p1, Ldui;->b:Ljava/lang/String;

    iput-object v0, p0, Lbdk;->b:Ljava/lang/String;

    goto :goto_0

    .line 54
    :cond_2
    iput-object v1, p0, Lbdk;->b:Ljava/lang/String;

    goto :goto_0

    .line 60
    :cond_3
    iput-object v1, p0, Lbdk;->a:Ljava/lang/String;

    goto :goto_1

    .line 67
    :cond_4
    iput-object v1, p0, Lbdk;->b:Ljava/lang/String;

    .line 68
    iput-object v1, p0, Lbdk;->a:Ljava/lang/String;

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lbdk;->a:Ljava/lang/String;

    .line 31
    invoke-static {p1}, Lbdk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbdk;->b:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lbdk;->a:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lbdk;->b:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lbdk;->a:Ljava/lang/String;

    .line 37
    if-eqz p3, :cond_0

    .line 40
    invoke-static {p3}, Lbdk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbdk;->b:Ljava/lang/String;

    .line 44
    :goto_0
    return-void

    .line 42
    :cond_0
    iput-object p2, p0, Lbdk;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Lbcx;)Lbdk;
    .locals 3

    .prologue
    .line 95
    new-instance v0, Lbdk;

    iget-object v1, p0, Lbcx;->a:Ljava/lang/String;

    iget-object v2, p0, Lbcx;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    if-nez p0, :cond_0

    .line 77
    const/4 v0, 0x0

    .line 79
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lbzd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a([Ldui;[Ldqh;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ldui;",
            "[",
            "Ldqh;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbdk;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 109
    if-nez p0, :cond_0

    .line 132
    :goto_0
    return-object v1

    .line 113
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 114
    array-length v6, p0

    move v5, v4

    :goto_1
    if-ge v5, v6, :cond_4

    aget-object v7, p0, v5

    .line 119
    if-eqz p1, :cond_2

    array-length v0, p1

    if-lez v0, :cond_2

    iget-object v0, v7, Ldui;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 121
    array-length v8, p1

    move v3, v4

    move-object v0, v1

    :goto_2
    if-ge v3, v8, :cond_3

    aget-object v9, p1, v3

    .line 122
    iget-object v10, v9, Ldqh;->b:Ldui;

    if-eqz v10, :cond_1

    iget-object v10, v9, Ldqh;->b:Ldui;

    iget-object v10, v10, Ldui;->c:Ljava/lang/String;

    if-eqz v10, :cond_1

    iget-object v10, v7, Ldui;->c:Ljava/lang/String;

    iget-object v11, v9, Ldqh;->b:Ldui;

    iget-object v11, v11, Ldui;->c:Ljava/lang/String;

    .line 123
    invoke-static {v10, v11}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    iget-object v10, v9, Ldqh;->e:Leir;

    if-eqz v10, :cond_1

    iget-object v10, v9, Ldqh;->e:Leir;

    iget-object v10, v10, Leir;->b:Ljava/lang/String;

    if-eqz v10, :cond_1

    .line 125
    iget-object v0, v9, Ldqh;->e:Leir;

    iget-object v0, v0, Leir;->b:Ljava/lang/String;

    .line 121
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    move-object v0, v1

    .line 130
    :cond_3
    new-instance v3, Lbdk;

    invoke-direct {v3, v7, v0}, Lbdk;-><init>(Ldui;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_1

    :cond_4
    move-object v1, v2

    .line 132
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 144
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 150
    :goto_0
    return v0

    :cond_0
    if-eqz p0, :cond_1

    invoke-virtual {p0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Lbdk;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 83
    new-instance v0, Lbdk;

    invoke-direct {v0, p0, v1, v1}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static c(Ljava/lang/String;)Lbdk;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 87
    new-instance v0, Lbdk;

    invoke-direct {v0, v1, p0, v1}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static d(Ljava/lang/String;)Lbdk;
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lbdk;

    invoke-direct {v0, p0}, Lbdk;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lbdk;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbdk;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lbdk;)Z
    .locals 4

    .prologue
    .line 162
    if-nez p1, :cond_0

    .line 163
    const/4 v0, 0x0

    .line 165
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lbdk;->b:Ljava/lang/String;

    iget-object v1, p0, Lbdk;->a:Ljava/lang/String;

    iget-object v2, p1, Lbdk;->b:Ljava/lang/String;

    iget-object v3, p1, Lbdk;->a:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lbdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lbdk;->b:Ljava/lang/String;

    iget-object v1, p0, Lbdk;->a:Ljava/lang/String;

    invoke-static {v0, v1, p1, p2}, Lbdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public b()Lbcn;
    .locals 1

    .prologue
    .line 103
    invoke-static {p0}, Lbcn;->a(Lbdk;)Lbcn;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 173
    iget-object v0, p0, Lbdk;->a:Ljava/lang/String;

    iget-object v1, p0, Lbdk;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2, v2}, Lbcx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ldui;
    .locals 2

    .prologue
    .line 206
    new-instance v0, Ldui;

    invoke-direct {v0}, Ldui;-><init>()V

    .line 207
    iget-object v1, p0, Lbdk;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 208
    iget-object v1, p0, Lbdk;->a:Ljava/lang/String;

    iput-object v1, v0, Ldui;->c:Ljava/lang/String;

    .line 211
    :cond_0
    iget-object v1, p0, Lbdk;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 212
    iget-object v1, p0, Lbdk;->b:Ljava/lang/String;

    iput-object v1, v0, Ldui;->b:Ljava/lang/String;

    .line 215
    :cond_1
    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 178
    if-eqz p1, :cond_0

    instance-of v0, p1, Lbdk;

    if-eqz v0, :cond_0

    .line 179
    check-cast p1, Lbdk;

    .line 180
    iget-object v0, p0, Lbdk;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p1, Lbdk;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    .line 182
    :goto_0
    iget-object v3, p0, Lbdk;->a:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, p1, Lbdk;->a:Ljava/lang/String;

    if-nez v3, :cond_3

    move v3, v1

    .line 184
    :goto_1
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    move v2, v1

    .line 186
    :cond_0
    return v2

    :cond_1
    move v0, v2

    .line 180
    goto :goto_0

    :cond_2
    iget-object v0, p0, Lbdk;->b:Ljava/lang/String;

    iget-object v3, p1, Lbdk;->b:Ljava/lang/String;

    .line 181
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_3
    move v3, v2

    .line 182
    goto :goto_1

    :cond_4
    iget-object v3, p0, Lbdk;->a:Ljava/lang/String;

    iget-object v4, p1, Lbdk;->a:Ljava/lang/String;

    .line 183
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 194
    const v2, 0x1475ca

    iget-object v0, p0, Lbdk;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 196
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lbdk;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 197
    return v0

    .line 194
    :cond_0
    iget-object v0, p0, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 196
    :cond_1
    iget-object v1, p0, Lbdk;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ParticipantId {chatId: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lbdk;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  gaiaId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 225
    iget-object v0, p0, Lbdk;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    move v3, v1

    .line 226
    :goto_0
    if-eqz v3, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 227
    if-eqz v3, :cond_0

    .line 228
    iget-object v0, p0, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 230
    :cond_0
    iget-object v0, p0, Lbdk;->b:Ljava/lang/String;

    if-eqz v0, :cond_4

    move v0, v1

    .line 231
    :goto_2
    if-eqz v0, :cond_5

    :goto_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 232
    if-eqz v0, :cond_1

    .line 233
    iget-object v0, p0, Lbdk;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 235
    :cond_1
    return-void

    :cond_2
    move v3, v2

    .line 225
    goto :goto_0

    :cond_3
    move v0, v2

    .line 226
    goto :goto_1

    :cond_4
    move v0, v2

    .line 230
    goto :goto_2

    :cond_5
    move v1, v2

    .line 231
    goto :goto_3
.end method
