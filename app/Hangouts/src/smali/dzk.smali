.class public final Ldzk;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldzk;


# instance fields
.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/String;

.field public d:Ldth;

.field public e:[Ldzo;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/Integer;

.field public i:Ldzl;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 442
    const/4 v0, 0x0

    new-array v0, v0, [Ldzk;

    sput-object v0, Ldzk;->a:[Ldzk;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 443
    invoke-direct {p0}, Lepn;-><init>()V

    .line 466
    iput-object v1, p0, Ldzk;->d:Ldth;

    .line 469
    sget-object v0, Ldzo;->a:[Ldzo;

    iput-object v0, p0, Ldzk;->e:[Ldzo;

    .line 472
    iput-object v1, p0, Ldzk;->f:Ljava/lang/Integer;

    .line 477
    iput-object v1, p0, Ldzk;->h:Ljava/lang/Integer;

    .line 480
    iput-object v1, p0, Ldzk;->i:Ldzl;

    .line 443
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 530
    iget-object v0, p0, Ldzk;->c:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 531
    const/4 v0, 0x1

    iget-object v2, p0, Ldzk;->c:Ljava/lang/String;

    .line 532
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 534
    :goto_0
    iget-object v2, p0, Ldzk;->d:Ldth;

    if-eqz v2, :cond_0

    .line 535
    const/4 v2, 0x2

    iget-object v3, p0, Ldzk;->d:Ldth;

    .line 536
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 538
    :cond_0
    iget-object v2, p0, Ldzk;->f:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 539
    const/4 v2, 0x3

    iget-object v3, p0, Ldzk;->f:Ljava/lang/Integer;

    .line 540
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 542
    :cond_1
    iget-object v2, p0, Ldzk;->g:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 543
    const/4 v2, 0x4

    iget-object v3, p0, Ldzk;->g:Ljava/lang/Boolean;

    .line 544
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 546
    :cond_2
    iget-object v2, p0, Ldzk;->h:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 547
    const/4 v2, 0x6

    iget-object v3, p0, Ldzk;->h:Ljava/lang/Integer;

    .line 548
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 550
    :cond_3
    iget-object v2, p0, Ldzk;->i:Ldzl;

    if-eqz v2, :cond_4

    .line 551
    const/4 v2, 0x7

    iget-object v3, p0, Ldzk;->i:Ldzl;

    .line 552
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 554
    :cond_4
    iget-object v2, p0, Ldzk;->b:Ljava/lang/Long;

    if-eqz v2, :cond_5

    .line 555
    const/16 v2, 0x8

    iget-object v3, p0, Ldzk;->b:Ljava/lang/Long;

    .line 556
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 558
    :cond_5
    iget-object v2, p0, Ldzk;->j:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 559
    const/16 v2, 0x9

    iget-object v3, p0, Ldzk;->j:Ljava/lang/String;

    .line 560
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 562
    :cond_6
    iget-object v2, p0, Ldzk;->e:[Ldzo;

    if-eqz v2, :cond_8

    .line 563
    iget-object v2, p0, Ldzk;->e:[Ldzo;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 564
    if-eqz v4, :cond_7

    .line 565
    const/16 v5, 0xb

    .line 566
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 563
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 570
    :cond_8
    iget-object v1, p0, Ldzk;->k:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 571
    const/16 v1, 0x63

    iget-object v2, p0, Ldzk;->k:Ljava/lang/String;

    .line 572
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 574
    :cond_9
    iget-object v1, p0, Ldzk;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 575
    iput v0, p0, Ldzk;->cachedSize:I

    .line 576
    return v0

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 439
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldzk;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldzk;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldzk;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldzk;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldzk;->d:Ldth;

    if-nez v0, :cond_2

    new-instance v0, Ldth;

    invoke-direct {v0}, Ldth;-><init>()V

    iput-object v0, p0, Ldzk;->d:Ldth;

    :cond_2
    iget-object v0, p0, Ldzk;->d:Ldth;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_3

    if-eq v0, v5, :cond_3

    if-eq v0, v6, :cond_3

    if-ne v0, v7, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldzk;->f:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldzk;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldzk;->g:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_5

    if-eq v0, v4, :cond_5

    if-eq v0, v5, :cond_5

    if-eq v0, v6, :cond_5

    if-eq v0, v7, :cond_5

    const/4 v2, 0x5

    if-ne v0, v2, :cond_6

    :cond_5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldzk;->h:Ljava/lang/Integer;

    goto :goto_0

    :cond_6
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldzk;->h:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ldzk;->i:Ldzl;

    if-nez v0, :cond_7

    new-instance v0, Ldzl;

    invoke-direct {v0}, Ldzl;-><init>()V

    iput-object v0, p0, Ldzk;->i:Ldzl;

    :cond_7
    iget-object v0, p0, Ldzk;->i:Ldzl;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldzk;->b:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldzk;->j:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_9
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldzk;->e:[Ldzo;

    if-nez v0, :cond_9

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldzo;

    iget-object v3, p0, Ldzk;->e:[Ldzo;

    if-eqz v3, :cond_8

    iget-object v3, p0, Ldzk;->e:[Ldzo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    iput-object v2, p0, Ldzk;->e:[Ldzo;

    :goto_2
    iget-object v2, p0, Ldzk;->e:[Ldzo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Ldzk;->e:[Ldzo;

    new-instance v3, Ldzo;

    invoke-direct {v3}, Ldzo;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldzk;->e:[Ldzo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_9
    iget-object v0, p0, Ldzk;->e:[Ldzo;

    array-length v0, v0

    goto :goto_1

    :cond_a
    iget-object v2, p0, Ldzk;->e:[Ldzo;

    new-instance v3, Ldzo;

    invoke-direct {v3}, Ldzo;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldzk;->e:[Ldzo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldzk;->k:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x30 -> :sswitch_5
        0x3a -> :sswitch_6
        0x40 -> :sswitch_7
        0x4a -> :sswitch_8
        0x5a -> :sswitch_9
        0x31a -> :sswitch_a
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 489
    iget-object v0, p0, Ldzk;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 490
    const/4 v0, 0x1

    iget-object v1, p0, Ldzk;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 492
    :cond_0
    iget-object v0, p0, Ldzk;->d:Ldth;

    if-eqz v0, :cond_1

    .line 493
    const/4 v0, 0x2

    iget-object v1, p0, Ldzk;->d:Ldth;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 495
    :cond_1
    iget-object v0, p0, Ldzk;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 496
    const/4 v0, 0x3

    iget-object v1, p0, Ldzk;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 498
    :cond_2
    iget-object v0, p0, Ldzk;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 499
    const/4 v0, 0x4

    iget-object v1, p0, Ldzk;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 501
    :cond_3
    iget-object v0, p0, Ldzk;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 502
    const/4 v0, 0x6

    iget-object v1, p0, Ldzk;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 504
    :cond_4
    iget-object v0, p0, Ldzk;->i:Ldzl;

    if-eqz v0, :cond_5

    .line 505
    const/4 v0, 0x7

    iget-object v1, p0, Ldzk;->i:Ldzl;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 507
    :cond_5
    iget-object v0, p0, Ldzk;->b:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 508
    const/16 v0, 0x8

    iget-object v1, p0, Ldzk;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 510
    :cond_6
    iget-object v0, p0, Ldzk;->j:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 511
    const/16 v0, 0x9

    iget-object v1, p0, Ldzk;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 513
    :cond_7
    iget-object v0, p0, Ldzk;->e:[Ldzo;

    if-eqz v0, :cond_9

    .line 514
    iget-object v1, p0, Ldzk;->e:[Ldzo;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_9

    aget-object v3, v1, v0

    .line 515
    if-eqz v3, :cond_8

    .line 516
    const/16 v4, 0xb

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 514
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 520
    :cond_9
    iget-object v0, p0, Ldzk;->k:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 521
    const/16 v0, 0x63

    iget-object v1, p0, Ldzk;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 523
    :cond_a
    iget-object v0, p0, Ldzk;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 525
    return-void
.end method
