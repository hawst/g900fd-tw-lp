.class public final Lcxl;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:I

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 24
    sput v0, Lcxl;->a:I

    .line 25
    sput v0, Lcxl;->b:I

    return-void
.end method

.method private static a(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 80
    const/4 v1, -0x1

    .line 81
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "/sys/devices/system/cpu/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcxl;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 82
    if-eqz v2, :cond_1

    .line 83
    const-string v3, "\\-"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 88
    array-length v3, v2

    if-ne v3, v0, :cond_0

    .line 99
    :goto_0
    return v0

    .line 90
    :cond_0
    array-length v0, v2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    .line 92
    const/4 v0, 0x0

    :try_start_0
    aget-object v0, v2, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 93
    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 94
    sub-int v0, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static a()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 19
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 20
    const-string v1, "randomize"

    invoke-static {v1}, Lcxc;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcxc;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 21
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v1

    const-wide v3, 0x3fb99999a0000000L    # 0.10000000149011612

    cmpg-double v1, v1, v3

    if-gtz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()I
    .locals 2

    .prologue
    .line 34
    sget v0, Lcxl;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 35
    const-string v0, "present"

    invoke-static {v0}, Lcxl;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcxl;->a:I

    .line 37
    :cond_0
    sget v0, Lcxl;->a:I

    return v0
.end method

.method private static b(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 109
    const/4 v0, -0x1

    .line 110
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcxl;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 112
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 115
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static c()I
    .locals 1

    .prologue
    .line 44
    const-string v0, "online"

    invoke-static {v0}, Lcxl;->a(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 125
    const/4 v0, 0x0

    .line 127
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, p0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 129
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    .line 132
    :goto_0
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 136
    :goto_1
    return-object v0

    :catch_0
    move-exception v2

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1

    .line 135
    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public static d()I
    .locals 2

    .prologue
    .line 51
    sget v0, Lcxl;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 54
    const-string v0, "cpuinfo_max_freq"

    invoke-static {v0}, Lcxl;->b(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcxl;->b:I

    .line 56
    :cond_0
    sget v0, Lcxl;->b:I

    return v0
.end method

.method public static e()I
    .locals 1

    .prologue
    .line 63
    const-string v0, "scaling_cur_freq"

    invoke-static {v0}, Lcxl;->b(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static f()I
    .locals 1

    .prologue
    .line 70
    const-string v0, "cpu_utilization"

    invoke-static {v0}, Lcxl;->b(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
