.class public final Ldof;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldof;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Long;

.field public h:Ljava/lang/Integer;

.field public i:Ldob;

.field public j:Ldob;

.field public k:Ljava/lang/Integer;

.field public l:Ldog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1432
    const/4 v0, 0x0

    new-array v0, v0, [Ldof;

    sput-object v0, Ldof;->a:[Ldof;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1433
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1549
    iput-object v0, p0, Ldof;->i:Ldob;

    .line 1552
    iput-object v0, p0, Ldof;->j:Ldob;

    .line 1557
    iput-object v0, p0, Ldof;->l:Ldog;

    .line 1433
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 1587
    const/16 v0, 0x13

    iget-object v1, p0, Ldof;->b:Ljava/lang/Integer;

    .line 1589
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1590
    const/16 v1, 0x14

    iget-object v2, p0, Ldof;->c:Ljava/lang/Integer;

    .line 1591
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1592
    const/16 v1, 0x15

    iget-object v2, p0, Ldof;->d:Ljava/lang/Integer;

    .line 1593
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1594
    const/16 v1, 0x16

    iget-object v2, p0, Ldof;->e:Ljava/lang/Long;

    .line 1595
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1596
    const/16 v1, 0x17

    iget-object v2, p0, Ldof;->f:Ljava/lang/Integer;

    .line 1597
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1598
    const/16 v1, 0x18

    iget-object v2, p0, Ldof;->g:Ljava/lang/Long;

    .line 1599
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1600
    const/16 v1, 0x19

    iget-object v2, p0, Ldof;->h:Ljava/lang/Integer;

    .line 1601
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1602
    iget-object v1, p0, Ldof;->i:Ldob;

    if-eqz v1, :cond_0

    .line 1603
    const/16 v1, 0x1a

    iget-object v2, p0, Ldof;->i:Ldob;

    .line 1604
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1606
    :cond_0
    iget-object v1, p0, Ldof;->j:Ldob;

    if-eqz v1, :cond_1

    .line 1607
    const/16 v1, 0x1b

    iget-object v2, p0, Ldof;->j:Ldob;

    .line 1608
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1610
    :cond_1
    iget-object v1, p0, Ldof;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 1611
    const/16 v1, 0x49

    iget-object v2, p0, Ldof;->k:Ljava/lang/Integer;

    .line 1612
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1614
    :cond_2
    iget-object v1, p0, Ldof;->l:Ldog;

    if-eqz v1, :cond_3

    .line 1615
    const/16 v1, 0x4a

    iget-object v2, p0, Ldof;->l:Ldog;

    .line 1616
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1618
    :cond_3
    iget-object v1, p0, Ldof;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1619
    iput v0, p0, Ldof;->cachedSize:I

    .line 1620
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1429
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldof;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldof;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldof;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldof;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldof;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldof;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldof;->e:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldof;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldof;->g:Ljava/lang/Long;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldof;->h:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Ldof;->i:Ldob;

    if-nez v0, :cond_2

    new-instance v0, Ldob;

    invoke-direct {v0}, Ldob;-><init>()V

    iput-object v0, p0, Ldof;->i:Ldob;

    :cond_2
    iget-object v0, p0, Ldof;->i:Ldob;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_9
    iget-object v0, p0, Ldof;->j:Ldob;

    if-nez v0, :cond_3

    new-instance v0, Ldob;

    invoke-direct {v0}, Ldob;-><init>()V

    iput-object v0, p0, Ldof;->j:Ldob;

    :cond_3
    iget-object v0, p0, Ldof;->j:Ldob;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldof;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Ldof;->l:Ldog;

    if-nez v0, :cond_4

    new-instance v0, Ldog;

    invoke-direct {v0}, Ldog;-><init>()V

    iput-object v0, p0, Ldof;->l:Ldog;

    :cond_4
    iget-object v0, p0, Ldof;->l:Ldog;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x98 -> :sswitch_1
        0xa0 -> :sswitch_2
        0xa8 -> :sswitch_3
        0xb0 -> :sswitch_4
        0xb8 -> :sswitch_5
        0xc0 -> :sswitch_6
        0xc8 -> :sswitch_7
        0xd2 -> :sswitch_8
        0xda -> :sswitch_9
        0x248 -> :sswitch_a
        0x252 -> :sswitch_b
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 1562
    const/16 v0, 0x13

    iget-object v1, p0, Ldof;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1563
    const/16 v0, 0x14

    iget-object v1, p0, Ldof;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1564
    const/16 v0, 0x15

    iget-object v1, p0, Ldof;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1565
    const/16 v0, 0x16

    iget-object v1, p0, Ldof;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 1566
    const/16 v0, 0x17

    iget-object v1, p0, Ldof;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1567
    const/16 v0, 0x18

    iget-object v1, p0, Ldof;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 1568
    const/16 v0, 0x19

    iget-object v1, p0, Ldof;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1569
    iget-object v0, p0, Ldof;->i:Ldob;

    if-eqz v0, :cond_0

    .line 1570
    const/16 v0, 0x1a

    iget-object v1, p0, Ldof;->i:Ldob;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1572
    :cond_0
    iget-object v0, p0, Ldof;->j:Ldob;

    if-eqz v0, :cond_1

    .line 1573
    const/16 v0, 0x1b

    iget-object v1, p0, Ldof;->j:Ldob;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1575
    :cond_1
    iget-object v0, p0, Ldof;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1576
    const/16 v0, 0x49

    iget-object v1, p0, Ldof;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1578
    :cond_2
    iget-object v0, p0, Ldof;->l:Ldog;

    if-eqz v0, :cond_3

    .line 1579
    const/16 v0, 0x4a

    iget-object v1, p0, Ldof;->l:Ldog;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1581
    :cond_3
    iget-object v0, p0, Ldof;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1583
    return-void
.end method
