.class public final Levc;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Levc;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lexp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 892
    const/4 v0, 0x0

    new-array v0, v0, [Levc;

    sput-object v0, Levc;->a:[Levc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 893
    invoke-direct {p0}, Lepn;-><init>()V

    .line 900
    const/4 v0, 0x0

    iput-object v0, p0, Levc;->d:Lexp;

    .line 893
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 920
    const/4 v0, 0x0

    .line 921
    iget-object v1, p0, Levc;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 922
    const/4 v0, 0x1

    iget-object v1, p0, Levc;->b:Ljava/lang/String;

    .line 923
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 925
    :cond_0
    iget-object v1, p0, Levc;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 926
    const/4 v1, 0x2

    iget-object v2, p0, Levc;->c:Ljava/lang/String;

    .line 927
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 929
    :cond_1
    iget-object v1, p0, Levc;->d:Lexp;

    if-eqz v1, :cond_2

    .line 930
    const/4 v1, 0x3

    iget-object v2, p0, Levc;->d:Lexp;

    .line 931
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 933
    :cond_2
    iget-object v1, p0, Levc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 934
    iput v0, p0, Levc;->cachedSize:I

    .line 935
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 889
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Levc;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Levc;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Levc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levc;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levc;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Levc;->d:Lexp;

    if-nez v0, :cond_2

    new-instance v0, Lexp;

    invoke-direct {v0}, Lexp;-><init>()V

    iput-object v0, p0, Levc;->d:Lexp;

    :cond_2
    iget-object v0, p0, Levc;->d:Lexp;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 905
    iget-object v0, p0, Levc;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 906
    const/4 v0, 0x1

    iget-object v1, p0, Levc;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 908
    :cond_0
    iget-object v0, p0, Levc;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 909
    const/4 v0, 0x2

    iget-object v1, p0, Levc;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 911
    :cond_1
    iget-object v0, p0, Levc;->d:Lexp;

    if-eqz v0, :cond_2

    .line 912
    const/4 v0, 0x3

    iget-object v1, p0, Levc;->d:Lexp;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 914
    :cond_2
    iget-object v0, p0, Levc;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 916
    return-void
.end method
