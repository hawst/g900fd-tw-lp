.class public final Ldtg;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldtg;


# instance fields
.field public b:Ldth;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1909
    const/4 v0, 0x0

    new-array v0, v0, [Ldtg;

    sput-object v0, Ldtg;->a:[Ldtg;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1910
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1921
    iput-object v0, p0, Ldtg;->b:Ldth;

    .line 1926
    iput-object v0, p0, Ldtg;->d:Ljava/lang/Integer;

    .line 1910
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1961
    const/4 v0, 0x0

    .line 1962
    iget-object v1, p0, Ldtg;->b:Ldth;

    if-eqz v1, :cond_0

    .line 1963
    const/4 v0, 0x1

    iget-object v1, p0, Ldtg;->b:Ldth;

    .line 1964
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1966
    :cond_0
    iget-object v1, p0, Ldtg;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1967
    const/4 v1, 0x2

    iget-object v2, p0, Ldtg;->e:Ljava/lang/String;

    .line 1968
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1970
    :cond_1
    iget-object v1, p0, Ldtg;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1971
    const/4 v1, 0x3

    iget-object v2, p0, Ldtg;->f:Ljava/lang/String;

    .line 1972
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1974
    :cond_2
    iget-object v1, p0, Ldtg;->g:[B

    if-eqz v1, :cond_3

    .line 1975
    const/4 v1, 0x4

    iget-object v2, p0, Ldtg;->g:[B

    .line 1976
    invoke-static {v1, v2}, Lepl;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 1978
    :cond_3
    iget-object v1, p0, Ldtg;->c:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1979
    const/4 v1, 0x5

    iget-object v2, p0, Ldtg;->c:Ljava/lang/String;

    .line 1980
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1982
    :cond_4
    iget-object v1, p0, Ldtg;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 1983
    const/4 v1, 0x6

    iget-object v2, p0, Ldtg;->d:Ljava/lang/Integer;

    .line 1984
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1986
    :cond_5
    iget-object v1, p0, Ldtg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1987
    iput v0, p0, Ldtg;->cachedSize:I

    .line 1988
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1906
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldtg;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldtg;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldtg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldtg;->b:Ldth;

    if-nez v0, :cond_2

    new-instance v0, Ldth;

    invoke-direct {v0}, Ldth;-><init>()V

    iput-object v0, p0, Ldtg;->b:Ldth;

    :cond_2
    iget-object v0, p0, Ldtg;->b:Ldth;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldtg;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldtg;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Ldtg;->g:[B

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldtg;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldtg;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldtg;->d:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1937
    iget-object v0, p0, Ldtg;->b:Ldth;

    if-eqz v0, :cond_0

    .line 1938
    const/4 v0, 0x1

    iget-object v1, p0, Ldtg;->b:Ldth;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1940
    :cond_0
    iget-object v0, p0, Ldtg;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1941
    const/4 v0, 0x2

    iget-object v1, p0, Ldtg;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1943
    :cond_1
    iget-object v0, p0, Ldtg;->f:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1944
    const/4 v0, 0x3

    iget-object v1, p0, Ldtg;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1946
    :cond_2
    iget-object v0, p0, Ldtg;->g:[B

    if-eqz v0, :cond_3

    .line 1947
    const/4 v0, 0x4

    iget-object v1, p0, Ldtg;->g:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 1949
    :cond_3
    iget-object v0, p0, Ldtg;->c:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1950
    const/4 v0, 0x5

    iget-object v1, p0, Ldtg;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1952
    :cond_4
    iget-object v0, p0, Ldtg;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 1953
    const/4 v0, 0x6

    iget-object v1, p0, Ldtg;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1955
    :cond_5
    iget-object v0, p0, Ldtg;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1957
    return-void
.end method
