.class public final Ldks;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldks;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:[Ldkt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 418
    const/4 v0, 0x0

    new-array v0, v0, [Ldks;

    sput-object v0, Ldks;->a:[Ldks;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 419
    invoke-direct {p0}, Lepn;-><init>()V

    .line 428
    const/4 v0, 0x0

    iput-object v0, p0, Ldks;->b:Ljava/lang/Integer;

    .line 431
    sget-object v0, Ldkt;->a:[Ldkt;

    iput-object v0, p0, Ldks;->c:[Ldkt;

    .line 419
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 453
    iget-object v0, p0, Ldks;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 454
    const/4 v0, 0x1

    iget-object v2, p0, Ldks;->b:Ljava/lang/Integer;

    .line 455
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 457
    :goto_0
    iget-object v2, p0, Ldks;->c:[Ldkt;

    if-eqz v2, :cond_1

    .line 458
    iget-object v2, p0, Ldks;->c:[Ldkt;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 459
    if-eqz v4, :cond_0

    .line 460
    const/4 v5, 0x2

    .line 461
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 458
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 465
    :cond_1
    iget-object v1, p0, Ldks;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 466
    iput v0, p0, Ldks;->cachedSize:I

    .line 467
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 415
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldks;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldks;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldks;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldks;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldks;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldks;->c:[Ldkt;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldkt;

    iget-object v3, p0, Ldks;->c:[Ldkt;

    if-eqz v3, :cond_4

    iget-object v3, p0, Ldks;->c:[Ldkt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Ldks;->c:[Ldkt;

    :goto_2
    iget-object v2, p0, Ldks;->c:[Ldkt;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ldks;->c:[Ldkt;

    new-instance v3, Ldkt;

    invoke-direct {v3}, Ldkt;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldks;->c:[Ldkt;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Ldks;->c:[Ldkt;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Ldks;->c:[Ldkt;

    new-instance v3, Ldkt;

    invoke-direct {v3}, Ldkt;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldks;->c:[Ldkt;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 436
    iget-object v0, p0, Ldks;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 437
    const/4 v0, 0x1

    iget-object v1, p0, Ldks;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 439
    :cond_0
    iget-object v0, p0, Ldks;->c:[Ldkt;

    if-eqz v0, :cond_2

    .line 440
    iget-object v1, p0, Ldks;->c:[Ldkt;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 441
    if-eqz v3, :cond_1

    .line 442
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 440
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 446
    :cond_2
    iget-object v0, p0, Ldks;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 448
    return-void
.end method
