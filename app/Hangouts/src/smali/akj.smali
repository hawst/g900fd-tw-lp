.class final Lakj;
.super Lakb;
.source "PG"

# interfaces
.implements Laki;


# instance fields
.field final synthetic a:Lakc;

.field private b:Landroid/view/View;


# direct methods
.method public constructor <init>(Lakc;Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 300
    iput-object p1, p0, Lakj;->a:Lakc;

    .line 301
    invoke-static {p1}, Lakc;->a(Lakc;)Lbyh;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lakb;-><init>(Landroid/content/res/Resources;Lbyh;)V

    .line 302
    invoke-static {p1}, Lakc;->b(Lakc;)Lakf;

    move-result-object v0

    invoke-virtual {v0, p0}, Lakf;->a(Laki;)V

    .line 303
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 306
    iput-object p1, p0, Lakj;->b:Landroid/view/View;

    .line 307
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 311
    iget-object v0, p0, Lakj;->a:Lakc;

    invoke-static {v0}, Lakc;->b(Lakc;)Lakf;

    move-result-object v0

    invoke-virtual {v0}, Lakf;->a()Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lakj;->a:Lakc;

    invoke-static {v1}, Lakc;->c(Lakc;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lakj;->a:Lakc;

    invoke-static {v0}, Lakc;->b(Lakc;)Lakf;

    move-result-object v0

    invoke-virtual {v0}, Lakf;->a()Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public j_()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 321
    iget-object v0, p0, Lakj;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lakj;->a:Lakc;

    invoke-static {v0}, Lakc;->b(Lakc;)Lakf;

    move-result-object v0

    invoke-virtual {v0}, Lakf;->a()Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 323
    iget-object v0, p0, Lakj;->b:Landroid/view/View;

    sget v1, Lg;->bM:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 324
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 325
    iget-object v0, p0, Lakj;->b:Landroid/view/View;

    sget v1, Lg;->gf:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 327
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 328
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 329
    const/4 v0, 0x0

    iput-object v0, p0, Lakj;->b:Landroid/view/View;

    .line 331
    invoke-virtual {p0}, Lakj;->notifyDataSetChanged()V

    .line 333
    :cond_0
    return-void
.end method
