.class public Lana;
.super Labq;
.source "PG"

# interfaces
.implements Ladc;
.implements Law;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Labq;",
        "Ladc;",
        "Law",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private Y:Ladb;

.field private Z:Landroid/view/ViewGroup;

.field private aa:Z

.field private ab:Landroid/database/Cursor;

.field private final ac:Lane;

.field private h:Land;

.field private i:Ladb;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Labq;-><init>()V

    .line 119
    new-instance v0, Lane;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lane;-><init>(Lana;B)V

    iput-object v0, p0, Lana;->ac:Lane;

    return-void
.end method

.method static synthetic a(Lana;)Land;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lana;->h:Land;

    return-object v0
.end method

.method private r()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 232
    iget-boolean v0, p0, Lana;->aa:Z

    if-nez v0, :cond_0

    .line 247
    :goto_0
    return-void

    .line 235
    :cond_0
    iget-object v0, p0, Lana;->Y:Ladb;

    if-eqz v0, :cond_1

    .line 237
    iget-object v0, p0, Lana;->Y:Ladb;

    invoke-virtual {v0, v1}, Ladb;->cancel(Z)Z

    .line 238
    iput-object v2, p0, Lana;->Y:Ladb;

    .line 240
    :cond_1
    invoke-virtual {p0}, Lana;->isAdded()Z

    move-result v0

    if-nez v0, :cond_2

    .line 241
    iput-boolean v1, p0, Lana;->aa:Z

    goto :goto_0

    .line 245
    :cond_2
    invoke-virtual {p0}, Lana;->getLoaderManager()Lav;

    move-result-object v0

    invoke-virtual {v0, v1, v2, p0}, Lav;->b(ILandroid/os/Bundle;Law;)Ldg;

    move-result-object v0

    invoke-virtual {v0}, Ldg;->q()V

    .line 246
    const/4 v0, 0x0

    iput-boolean v0, p0, Lana;->aa:Z

    goto :goto_0
.end method


# virtual methods
.method protected a(ILadk;)V
    .locals 2

    .prologue
    .line 332
    invoke-super {p0, p1, p2}, Labq;->a(ILadk;)V

    .line 333
    iget-object v0, p0, Lana;->e:Ladj;

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lana;->e:Ladj;

    invoke-virtual {v0}, Ladj;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 335
    iget-object v0, p0, Lana;->Z:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 340
    :cond_1
    iget-object v0, p0, Lana;->Z:Landroid/view/ViewGroup;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Land;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lana;->h:Land;

    .line 251
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 299
    iget-object v0, p0, Lana;->ab:Landroid/database/Cursor;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 300
    iput-object p1, p0, Lana;->ab:Landroid/database/Cursor;

    .line 301
    iget-object v0, p0, Lana;->e:Ladj;

    if-eqz v0, :cond_1

    .line 304
    new-instance v0, Laao;

    invoke-direct {v0, p1}, Laao;-><init>(Landroid/database/Cursor;)V

    .line 306
    iget-object v1, p0, Lana;->i:Ladb;

    if-eqz v1, :cond_0

    .line 307
    iget-object v1, p0, Lana;->i:Ladb;

    invoke-virtual {v0, v1}, Laao;->a(Ladb;)V

    .line 309
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lana;->a(ILadk;)V

    .line 312
    :cond_1
    return-void
.end method

.method public a(Lbsj;Ladb;)V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lana;->Y:Ladb;

    if-eq p2, v0, :cond_1

    .line 57
    const-string v0, "Babel"

    const-string v1, "Unexpected contact lookup load state."

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const-string v0, "Should not have two new items."

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    .line 74
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lana;->Y:Ladb;

    .line 62
    iget-object v0, p0, Lana;->c:Lbsj;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lana;->e:Ladj;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lana;->i:Ladb;

    if-eqz v0, :cond_2

    .line 64
    iget-object v0, p0, Lana;->i:Ladb;

    invoke-virtual {v0}, Ladb;->a()V

    .line 66
    :cond_2
    iput-object p2, p0, Lana;->i:Ladb;

    .line 67
    iget-object v0, p0, Lana;->e:Ladj;

    iget-object v1, p0, Lana;->i:Ladb;

    invoke-virtual {v0, v1}, Ladj;->a(Ladb;)V

    .line 70
    invoke-virtual {p0}, Lana;->b()Lcom/google/android/apps/hangouts/views/EsListView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 71
    invoke-virtual {p0}, Lana;->b()Lcom/google/android/apps/hangouts/views/EsListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/EsListView;->invalidateViews()V

    goto :goto_0
.end method

.method public a(Lbsj;Lcvx;Lcwf;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 259
    invoke-super {p0, p1, p2, p3}, Labq;->a(Lbsj;Lcvx;Lcwf;)V

    .line 261
    iget-object v0, p0, Lana;->c:Lbsj;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lana;->e:Ladj;

    if-eqz v0, :cond_2

    .line 266
    new-instance v0, Landroid/os/Debug$MemoryInfo;

    invoke-direct {v0}, Landroid/os/Debug$MemoryInfo;-><init>()V

    .line 267
    invoke-static {v0}, Landroid/os/Debug;->getMemoryInfo(Landroid/os/Debug$MemoryInfo;)V

    .line 268
    if-eqz p2, :cond_0

    .line 269
    invoke-virtual {v0}, Landroid/os/Debug$MemoryInfo;->getTotalPss()I

    move-result v0

    invoke-virtual {p2}, Lcvx;->a()I

    move-result v1

    div-int/lit8 v1, v1, 0xa

    sub-int/2addr v0, v1

    const/16 v1, 0x1000

    if-ge v0, v1, :cond_0

    .line 270
    const-string v0, "Babel"

    const-string v1, "Lots of contacts, or low memory, or both, clearing cache."

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    iget-object v0, p0, Lana;->i:Ladb;

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lana;->i:Ladb;

    invoke-virtual {v0}, Ladb;->a()V

    .line 273
    iput-object v2, p0, Lana;->i:Ladb;

    .line 274
    iget-object v0, p0, Lana;->e:Ladj;

    invoke-virtual {v0, v2}, Ladj;->a(Ladb;)V

    .line 278
    :cond_0
    iget-object v0, p0, Lana;->Y:Ladb;

    if-eqz v0, :cond_1

    .line 279
    iget-object v0, p0, Lana;->Y:Ladb;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ladb;->cancel(Z)Z

    .line 281
    :cond_1
    new-instance v0, Ladb;

    invoke-direct {v0, p2, p1, p0}, Ladb;-><init>(Lcvx;Lbsj;Ladc;)V

    iput-object v0, p0, Lana;->Y:Ladb;

    .line 282
    iget-object v0, p0, Lana;->Y:Ladb;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ladb;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 284
    :cond_2
    return-void
.end method

.method protected a()[Ladl;
    .locals 3

    .prologue
    .line 322
    const/4 v0, 0x1

    new-array v0, v0, [Ladl;

    const/4 v1, 0x0

    new-instance v2, Ladl;

    invoke-direct {v2}, Ladl;-><init>()V

    aput-object v2, v0, v1

    return-object v0
.end method

.method public e()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 216
    invoke-super {p0}, Labq;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 217
    const/4 v0, 0x0

    .line 222
    :goto_0
    return v0

    .line 220
    :cond_0
    iput-boolean v0, p0, Lana;->aa:Z

    .line 221
    invoke-direct {p0}, Lana;->r()V

    goto :goto_0
.end method

.method protected isEmpty()Z
    .locals 1

    .prologue
    .line 327
    const/4 v0, 0x0

    return v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 227
    invoke-super {p0, p1}, Labq;->onAttach(Landroid/app/Activity;)V

    .line 228
    invoke-direct {p0}, Lana;->r()V

    .line 229
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 194
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 195
    if-ne v0, v1, :cond_1

    .line 196
    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    .line 197
    instance-of v3, v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    if-eqz v3, :cond_0

    .line 198
    check-cast v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 199
    iget-object v0, v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;->targetView:Landroid/view/View;

    check-cast v0, Lcaw;

    .line 200
    invoke-virtual {v0}, Lcaw;->c()Ljava/lang/String;

    move-result-object v0

    .line 201
    if-eqz v0, :cond_0

    .line 202
    invoke-virtual {p0}, Lana;->f()Lyj;

    move-result-object v3

    new-array v4, v1, [Ljava/lang/String;

    aput-object v0, v4, v2

    invoke-static {v3, v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;[Ljava/lang/String;)V

    :cond_0
    move v0, v1

    .line 208
    :goto_0
    return v0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 185
    invoke-super {p0, p1, p2, p3}, Labq;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 186
    invoke-virtual {p0}, Lana;->b()Lcom/google/android/apps/hangouts/views/EsListView;

    move-result-object v0

    if-ne p2, v0, :cond_0

    .line 187
    const/4 v0, 0x1

    sget v1, Lh;->gv:I

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 188
    sget v0, Lh;->gz:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    .line 190
    :cond_0
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Ldg;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 288
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 289
    invoke-virtual {p0}, Lana;->f()Lyj;

    move-result-object v0

    invoke-static {v0, v1, v1}, Lf;->a(Lyj;ZI)Ldb;

    move-result-object v0

    .line 291
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 128
    sget v0, Lf;->gp:I

    invoke-super {p0, p1, p2, p3, v0}, Labq;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v1

    .line 130
    sget v0, Lg;->ea:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lana;->Z:Landroid/view/ViewGroup;

    .line 131
    iget-object v0, p0, Lana;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lana;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v2, p0, Lana;->ac:Lane;

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 137
    iget-object v0, p0, Lana;->f:Landroid/widget/TextView;

    new-instance v2, Lanb;

    invoke-direct {v2, p0}, Lanb;-><init>(Lana;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    :cond_0
    sget v0, Lg;->as:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 148
    new-instance v2, Lanc;

    invoke-direct {v2, p0}, Lanc;-><init>(Lana;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 158
    invoke-virtual {p0}, Lana;->b()Lcom/google/android/apps/hangouts/views/EsListView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lana;->registerForContextMenu(Landroid/view/View;)V

    .line 160
    return-object v1
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 171
    invoke-super {p0}, Labq;->onDestroy()V

    .line 172
    iget-object v0, p0, Lana;->Y:Ladb;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lana;->Y:Ladb;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ladb;->cancel(Z)Z

    .line 175
    iput-object v2, p0, Lana;->Y:Ladb;

    .line 177
    :cond_0
    iget-object v0, p0, Lana;->i:Ladb;

    if-eqz v0, :cond_1

    .line 178
    iget-object v0, p0, Lana;->i:Ladb;

    invoke-virtual {v0}, Ladb;->a()V

    .line 179
    iput-object v2, p0, Lana;->i:Ladb;

    .line 181
    :cond_1
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 165
    invoke-super {p0}, Labq;->onDestroyView()V

    .line 166
    iget-object v0, p0, Lana;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lana;->ac:Lane;

    invoke-static {v0, v1}, Lcxg;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 167
    return-void
.end method

.method public synthetic onLoadFinished(Ldg;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 42
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lana;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Ldg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 316
    return-void
.end method
