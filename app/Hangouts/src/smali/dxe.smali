.class public final Ldxe;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldxe;


# instance fields
.field public b:[Ldxc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24950
    const/4 v0, 0x0

    new-array v0, v0, [Ldxe;

    sput-object v0, Ldxe;->a:[Ldxe;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24951
    invoke-direct {p0}, Lepn;-><init>()V

    .line 24954
    sget-object v0, Ldxc;->a:[Ldxc;

    iput-object v0, p0, Ldxe;->b:[Ldxc;

    .line 24951
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 24973
    iget-object v1, p0, Ldxe;->b:[Ldxc;

    if-eqz v1, :cond_1

    .line 24974
    iget-object v2, p0, Ldxe;->b:[Ldxc;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 24975
    if-eqz v4, :cond_0

    .line 24976
    const/4 v5, 0x2

    .line 24977
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 24974
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 24981
    :cond_1
    iget-object v1, p0, Ldxe;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24982
    iput v0, p0, Ldxe;->cachedSize:I

    .line 24983
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 24947
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldxe;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldxe;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldxe;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldxe;->b:[Ldxc;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldxc;

    iget-object v3, p0, Ldxe;->b:[Ldxc;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldxe;->b:[Ldxc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldxe;->b:[Ldxc;

    :goto_2
    iget-object v2, p0, Ldxe;->b:[Ldxc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldxe;->b:[Ldxc;

    new-instance v3, Ldxc;

    invoke-direct {v3}, Ldxc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldxe;->b:[Ldxc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldxe;->b:[Ldxc;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldxe;->b:[Ldxc;

    new-instance v3, Ldxc;

    invoke-direct {v3}, Ldxc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldxe;->b:[Ldxc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 24959
    iget-object v0, p0, Ldxe;->b:[Ldxc;

    if-eqz v0, :cond_1

    .line 24960
    iget-object v1, p0, Ldxe;->b:[Ldxc;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 24961
    if-eqz v3, :cond_0

    .line 24962
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 24960
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 24966
    :cond_1
    iget-object v0, p0, Ldxe;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 24968
    return-void
.end method
