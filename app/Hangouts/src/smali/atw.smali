.class final Latw;
.super Latf;
.source "PG"


# instance fields
.field final synthetic b:Latu;


# direct methods
.method private constructor <init>(Latu;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Latw;->b:Latu;

    invoke-direct {p0, p1}, Latf;-><init>(Late;)V

    return-void
.end method

.method synthetic constructor <init>(Latu;B)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Latw;-><init>(Latu;)V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 3

    .prologue
    .line 90
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 91
    iget-object v0, p0, Latw;->b:Latu;

    iget-object v1, v0, Latu;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 92
    :try_start_0
    iget-object v0, p0, Latw;->b:Latu;

    iget v0, v0, Latu;->p:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 96
    iget-object v0, p0, Latw;->b:Latu;

    invoke-static {v0}, Latu;->c(Latu;)Z

    move-result v0

    .line 97
    iget-object v2, p0, Latw;->b:Latu;

    iget-object v2, v2, Latu;->e:Latd;

    invoke-virtual {v2}, Latd;->h()Laqv;

    move-result-object v2

    invoke-virtual {v2, v0}, Laqv;->d(Z)V

    .line 100
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    :cond_1
    return-void

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onAudioUpdated(Lcom/google/android/libraries/hangouts/video/AudioDeviceState;Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/hangouts/video/AudioDeviceState;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/libraries/hangouts/video/AudioDevice;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v1, p0, Latw;->b:Latu;

    sget-object v0, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->EARPIECE_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    if-ne p1, v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Latu;->b(Latu;Z)Z

    .line 74
    iget-object v0, p0, Latw;->b:Latu;

    invoke-static {v0}, Latu;->b(Latu;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    iget-object v0, p0, Latw;->b:Latu;

    iget-object v1, v0, Latu;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 76
    :try_start_0
    iget-object v0, p0, Latw;->b:Latu;

    iget v0, v0, Latu;->p:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 80
    iget-object v0, p0, Latw;->b:Latu;

    invoke-static {v0}, Latu;->c(Latu;)Z

    move-result v0

    .line 81
    iget-object v2, p0, Latw;->b:Latu;

    iget-object v2, v2, Latu;->e:Latd;

    invoke-virtual {v2}, Latd;->h()Laqv;

    move-result-object v2

    invoke-virtual {v2, v0}, Laqv;->d(Z)V

    .line 84
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    :cond_1
    return-void

    .line 73
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onEndpointEvent(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;)V
    .locals 2

    .prologue
    .line 60
    instance-of v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/AudioMuteEvent;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p2

    .line 62
    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/AudioMuteEvent;

    .line 63
    iget-object v1, p0, Latw;->b:Latu;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/AudioMuteEvent;->isMuted()Z

    move-result v0

    invoke-static {v1, v0}, Latu;->a(Latu;Z)V

    .line 65
    :cond_0
    instance-of v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/EnterEvent;

    if-nez v0, :cond_1

    instance-of v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/ExitEvent;

    if-eqz v0, :cond_2

    .line 66
    :cond_1
    iget-object v0, p0, Latw;->b:Latu;

    invoke-static {v0}, Latu;->a(Latu;)V

    .line 68
    :cond_2
    return-void
.end method
