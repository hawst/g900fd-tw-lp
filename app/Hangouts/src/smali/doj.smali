.class public final Ldoj;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldoj;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:[Ljava/lang/Integer;

.field public e:[Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 337
    const/4 v0, 0x0

    new-array v0, v0, [Ldoj;

    sput-object v0, Ldoj;->a:[Ldoj;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 338
    invoke-direct {p0}, Lepn;-><init>()V

    .line 345
    sget-object v0, Lept;->m:[Ljava/lang/Integer;

    iput-object v0, p0, Ldoj;->d:[Ljava/lang/Integer;

    .line 348
    sget-object v0, Lept;->m:[Ljava/lang/Integer;

    iput-object v0, p0, Ldoj;->e:[Ljava/lang/Integer;

    .line 338
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 375
    const/4 v0, 0x0

    .line 376
    iget-object v1, p0, Ldoj;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 377
    const/4 v0, 0x1

    iget-object v1, p0, Ldoj;->b:Ljava/lang/String;

    .line 378
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 380
    :cond_0
    iget-object v1, p0, Ldoj;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 381
    const/4 v1, 0x2

    iget-object v2, p0, Ldoj;->c:Ljava/lang/String;

    .line 382
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 384
    :cond_1
    iget-object v1, p0, Ldoj;->d:[Ljava/lang/Integer;

    if-eqz v1, :cond_2

    iget-object v1, p0, Ldoj;->d:[Ljava/lang/Integer;

    array-length v1, v1

    if-lez v1, :cond_2

    .line 385
    iget-object v1, p0, Ldoj;->d:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    .line 386
    add-int/2addr v0, v1

    .line 387
    iget-object v1, p0, Ldoj;->d:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 389
    :cond_2
    iget-object v1, p0, Ldoj;->e:[Ljava/lang/Integer;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ldoj;->e:[Ljava/lang/Integer;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 390
    iget-object v1, p0, Ldoj;->e:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    .line 391
    add-int/2addr v0, v1

    .line 392
    iget-object v1, p0, Ldoj;->e:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 394
    :cond_3
    iget-object v1, p0, Ldoj;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 395
    iput v0, p0, Ldoj;->cachedSize:I

    .line 396
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 334
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldoj;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldoj;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldoj;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldoj;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldoj;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1d

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldoj;->d:[Ljava/lang/Integer;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    iget-object v2, p0, Ldoj;->d:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldoj;->d:[Ljava/lang/Integer;

    :goto_1
    iget-object v1, p0, Ldoj;->d:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Ldoj;->d:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->h()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Ldoj;->d:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->h()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x25

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldoj;->e:[Ljava/lang/Integer;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    iget-object v2, p0, Ldoj;->e:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldoj;->e:[Ljava/lang/Integer;

    :goto_2
    iget-object v1, p0, Ldoj;->e:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Ldoj;->e:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->h()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v1, p0, Ldoj;->e:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->h()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 353
    iget-object v1, p0, Ldoj;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 354
    const/4 v1, 0x1

    iget-object v2, p0, Ldoj;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 356
    :cond_0
    iget-object v1, p0, Ldoj;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 357
    const/4 v1, 0x2

    iget-object v2, p0, Ldoj;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 359
    :cond_1
    iget-object v1, p0, Ldoj;->d:[Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 360
    iget-object v2, p0, Ldoj;->d:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 361
    const/4 v5, 0x3

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v5, v4}, Lepl;->b(II)V

    .line 360
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 364
    :cond_2
    iget-object v1, p0, Ldoj;->e:[Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 365
    iget-object v1, p0, Ldoj;->e:[Ljava/lang/Integer;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 366
    const/4 v4, 0x4

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Lepl;->b(II)V

    .line 365
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 369
    :cond_3
    iget-object v0, p0, Ldoj;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 371
    return-void
.end method
