.class public final Lept;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:I

.field static final b:I

.field static final c:I

.field static final d:I

.field public static final e:[I

.field public static final f:[J

.field public static final g:[F

.field public static final h:[D

.field public static final i:[Z

.field public static final j:[Ljava/lang/String;

.field public static final k:[[B

.field public static final l:[B

.field public static final m:[Ljava/lang/Integer;

.field public static final n:[Ljava/lang/Long;

.field public static final o:[Ljava/lang/Float;

.field public static final p:[Ljava/lang/Double;

.field public static final q:[Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 84
    const/16 v0, 0xb

    sput v0, Lept;->a:I

    .line 86
    const/16 v0, 0xc

    sput v0, Lept;->b:I

    .line 88
    const/16 v0, 0x10

    sput v0, Lept;->c:I

    .line 90
    const/16 v0, 0x1a

    sput v0, Lept;->d:I

    .line 93
    new-array v0, v1, [I

    sput-object v0, Lept;->e:[I

    .line 94
    new-array v0, v1, [J

    sput-object v0, Lept;->f:[J

    .line 95
    new-array v0, v1, [F

    sput-object v0, Lept;->g:[F

    .line 96
    new-array v0, v1, [D

    sput-object v0, Lept;->h:[D

    .line 97
    new-array v0, v1, [Z

    sput-object v0, Lept;->i:[Z

    .line 98
    new-array v0, v1, [Ljava/lang/String;

    sput-object v0, Lept;->j:[Ljava/lang/String;

    .line 99
    new-array v0, v1, [[B

    sput-object v0, Lept;->k:[[B

    .line 100
    new-array v0, v1, [B

    sput-object v0, Lept;->l:[B

    .line 102
    new-array v0, v1, [Ljava/lang/Integer;

    sput-object v0, Lept;->m:[Ljava/lang/Integer;

    .line 103
    new-array v0, v1, [Ljava/lang/Long;

    sput-object v0, Lept;->n:[Ljava/lang/Long;

    .line 104
    new-array v0, v1, [Ljava/lang/Float;

    sput-object v0, Lept;->o:[Ljava/lang/Float;

    .line 105
    new-array v0, v1, [Ljava/lang/Double;

    sput-object v0, Lept;->p:[Ljava/lang/Double;

    .line 106
    new-array v0, v1, [Ljava/lang/Boolean;

    sput-object v0, Lept;->q:[Ljava/lang/Boolean;

    return-void
.end method

.method static a(I)I
    .locals 1

    .prologue
    .line 65
    and-int/lit8 v0, p0, 0x7

    return v0
.end method

.method static a(II)I
    .locals 1

    .prologue
    .line 75
    shl-int/lit8 v0, p0, 0x3

    or-int/2addr v0, p1

    return v0
.end method

.method public static final a(Lepk;I)I
    .locals 3

    .prologue
    .line 166
    const/4 v0, 0x1

    .line 167
    invoke-virtual {p0}, Lepk;->q()I

    move-result v1

    .line 168
    invoke-virtual {p0, p1}, Lepk;->b(I)Z

    .line 169
    :goto_0
    invoke-virtual {p0}, Lepk;->p()I

    move-result v2

    if-lez v2, :cond_0

    .line 170
    invoke-virtual {p0}, Lepk;->a()I

    move-result v2

    .line 171
    if-ne v2, p1, :cond_0

    .line 172
    invoke-virtual {p0, p1}, Lepk;->b(I)Z

    .line 175
    add-int/lit8 v0, v0, 0x1

    .line 176
    goto :goto_0

    .line 177
    :cond_0
    invoke-virtual {p0, v1}, Lepk;->c(I)V

    .line 178
    return v0
.end method

.method public static a(Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Leps;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 346
    if-nez p0, :cond_0

    .line 354
    :goto_0
    return v0

    .line 350
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leps;

    .line 351
    iget v3, v0, Leps;->a:I

    invoke-static {v3}, Lepl;->i(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 352
    iget-object v0, v0, Leps;->b:[B

    array-length v0, v0

    add-int/2addr v0, v1

    move v1, v0

    .line 353
    goto :goto_1

    :cond_1
    move v0, v1

    .line 354
    goto :goto_0
.end method

.method private static a(ILjava/lang/Object;)Leps;
    .locals 4

    .prologue
    .line 286
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 288
    :try_start_0
    const-class v1, Ljava/lang/String;

    if-ne v0, v1, :cond_0

    .line 289
    check-cast p1, Ljava/lang/String;

    .line 290
    invoke-static {p1}, Lepl;->b(Ljava/lang/String;)I

    move-result v0

    new-array v1, v0, [B

    .line 291
    const/4 v0, 0x0

    array-length v2, v1

    invoke-static {v1, v0, v2}, Lepl;->a([BII)Lepl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lepl;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 292
    shl-int/lit8 v0, p0, 0x3

    or-int/lit8 v0, v0, 0x2

    .line 339
    :goto_0
    new-instance v2, Leps;

    invoke-direct {v2, v0, v1}, Leps;-><init>(I[B)V

    return-object v2

    .line 293
    :cond_0
    :try_start_1
    const-class v1, Ljava/lang/Integer;

    if-ne v0, v1, :cond_1

    .line 294
    check-cast p1, Ljava/lang/Integer;

    .line 295
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lepl;->f(I)I

    move-result v0

    new-array v1, v0, [B

    .line 296
    const/4 v0, 0x0

    array-length v2, v1

    invoke-static {v1, v0, v2}, Lepl;->a([BII)Lepl;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, Lepl;->b(I)V

    .line 297
    shl-int/lit8 v0, p0, 0x3

    .line 298
    goto :goto_0

    :cond_1
    const-class v1, Ljava/lang/Long;

    if-ne v0, v1, :cond_2

    .line 299
    check-cast p1, Ljava/lang/Long;

    .line 300
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lepl;->c(J)I

    move-result v0

    new-array v1, v0, [B

    .line 301
    const/4 v0, 0x0

    array-length v2, v1

    invoke-static {v1, v0, v2}, Lepl;->a([BII)Lepl;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lepl;->b(J)V

    .line 302
    shl-int/lit8 v0, p0, 0x3

    .line 303
    goto :goto_0

    :cond_2
    const-class v1, Ljava/lang/Boolean;

    if-ne v0, v1, :cond_3

    .line 304
    check-cast p1, Ljava/lang/Boolean;

    .line 305
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    const/4 v0, 0x1

    new-array v1, v0, [B

    .line 306
    const/4 v0, 0x0

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lepl;->a([BII)Lepl;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v2}, Lepl;->a(Z)V

    .line 307
    shl-int/lit8 v0, p0, 0x3

    .line 308
    goto :goto_0

    :cond_3
    const-class v1, Ljava/lang/Float;

    if-ne v0, v1, :cond_4

    .line 309
    check-cast p1, Ljava/lang/Float;

    .line 310
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    const/4 v0, 0x4

    new-array v1, v0, [B

    .line 311
    const/4 v0, 0x0

    const/4 v2, 0x4

    invoke-static {v1, v0, v2}, Lepl;->a([BII)Lepl;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v0, v2}, Lepl;->a(F)V

    .line 312
    shl-int/lit8 v0, p0, 0x3

    or-int/lit8 v0, v0, 0x5

    .line 313
    goto :goto_0

    :cond_4
    const-class v1, Ljava/lang/Double;

    if-ne v0, v1, :cond_5

    .line 314
    check-cast p1, Ljava/lang/Double;

    .line 315
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    const/16 v0, 0x8

    new-array v1, v0, [B

    .line 316
    const/4 v0, 0x0

    const/16 v2, 0x8

    invoke-static {v1, v0, v2}, Lepl;->a([BII)Lepl;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lepl;->a(D)V

    .line 317
    shl-int/lit8 v0, p0, 0x3

    or-int/lit8 v0, v0, 0x1

    .line 318
    goto/16 :goto_0

    :cond_5
    const-class v1, [B

    if-ne v0, v1, :cond_6

    .line 319
    check-cast p1, [B

    .line 320
    array-length v0, p1

    invoke-static {v0}, Lepl;->i(I)I

    move-result v0

    array-length v1, p1

    add-int/2addr v0, v1

    new-array v1, v0, [B

    .line 321
    const/4 v0, 0x0

    array-length v2, v1

    invoke-static {v1, v0, v2}, Lepl;->a([BII)Lepl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lepl;->a([B)V

    .line 322
    shl-int/lit8 v0, p0, 0x3

    or-int/lit8 v0, v0, 0x2

    .line 323
    goto/16 :goto_0

    :cond_6
    const-class v1, Lepr;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 324
    check-cast p1, Lepr;

    .line 326
    invoke-virtual {p1}, Lepr;->getSerializedSize()I

    move-result v0

    .line 327
    invoke-static {v0}, Lepl;->i(I)I

    move-result v1

    .line 328
    add-int/2addr v1, v0

    new-array v1, v1, [B

    .line 329
    const/4 v2, 0x0

    array-length v3, v1

    invoke-static {v1, v2, v3}, Lepl;->a([BII)Lepl;

    move-result-object v2

    .line 330
    invoke-virtual {v2, v0}, Lepl;->h(I)V

    .line 331
    invoke-static {p1}, Lepr;->toByteArray(Lepr;)[B

    move-result-object v0

    invoke-virtual {v2, v0}, Lepl;->c([B)V

    .line 332
    shl-int/lit8 v0, p0, 0x3

    or-int/lit8 v0, v0, 0x2

    .line 333
    goto/16 :goto_0

    .line 334
    :cond_7
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled extension field type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 338
    :catch_0
    move-exception v0

    .line 337
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Lepo;Ljava/util/List;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lepo",
            "<TT;>;",
            "Ljava/util/List",
            "<",
            "Leps;",
            ">;)TT;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 185
    if-nez p1, :cond_0

    move-object v0, v1

    .line 209
    :goto_0
    return-object v0

    .line 188
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 189
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leps;

    .line 190
    iget v4, v0, Leps;->a:I

    ushr-int/lit8 v4, v4, 0x3

    iget v5, p0, Lepo;->a:I

    if-ne v4, v5, :cond_1

    .line 191
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 194
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 195
    goto :goto_0

    .line 198
    :cond_3
    iget-boolean v0, p0, Lepo;->b:Z

    if-eqz v0, :cond_5

    .line 199
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 200
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leps;

    .line 201
    iget-object v3, p0, Lepo;->c:Ljava/lang/Class;

    iget-object v0, v0, Leps;->b:[B

    invoke-static {v3, v0}, Lept;->a(Ljava/lang/Class;[B)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 203
    :cond_4
    iget-object v0, p0, Lepo;->d:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 208
    :cond_5
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leps;

    .line 209
    iget-object v1, p0, Lepo;->c:Ljava/lang/Class;

    iget-object v0, v0, Leps;->b:[B

    invoke-static {v1, v0}, Lept;->a(Ljava/lang/Class;[B)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;[B)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;[B)TT;"
        }
    .end annotation

    .prologue
    .line 218
    array-length v0, p1

    if-nez v0, :cond_0

    .line 219
    const/4 v0, 0x0

    .line 241
    :goto_0
    return-object v0

    .line 221
    :cond_0
    const/4 v0, 0x0

    array-length v1, p1

    invoke-static {p1, v0, v1}, Lepk;->a([BII)Lepk;

    move-result-object v1

    .line 223
    :try_start_0
    const-class v0, Ljava/lang/String;

    if-ne p0, v0, :cond_1

    .line 224
    invoke-virtual {v1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 225
    :cond_1
    const-class v0, Ljava/lang/Integer;

    if-ne p0, v0, :cond_2

    .line 226
    invoke-virtual {v1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 227
    :cond_2
    const-class v0, Ljava/lang/Long;

    if-ne p0, v0, :cond_3

    .line 228
    invoke-virtual {v1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 229
    :cond_3
    const-class v0, Ljava/lang/Boolean;

    if-ne p0, v0, :cond_4

    .line 230
    invoke-virtual {v1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 231
    :cond_4
    const-class v0, Ljava/lang/Float;

    if-ne p0, v0, :cond_5

    .line 232
    invoke-virtual {v1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 233
    :cond_5
    const-class v0, Ljava/lang/Double;

    if-ne p0, v0, :cond_6

    .line 234
    invoke-virtual {v1}, Lepk;->b()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 235
    :cond_6
    const-class v0, [B

    if-ne p0, v0, :cond_7

    .line 236
    invoke-virtual {v1}, Lepk;->k()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 237
    :cond_7
    const-class v0, Lepr;

    invoke-virtual {v0, p0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    if-eqz v0, :cond_8

    .line 239
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepr;

    .line 240
    invoke-virtual {v1, v0}, Lepk;->a(Lepr;)V

    .line 241
    invoke-virtual {p0, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto/16 :goto_0

    .line 242
    :catch_0
    move-exception v0

    .line 243
    :try_start_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error creating instance of class "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 250
    :catch_1
    move-exception v0

    .line 251
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Error reading extension field"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 244
    :catch_2
    move-exception v0

    .line 245
    :try_start_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error creating instance of class "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 248
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled extension field type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
.end method

.method public static a(Lepo;Ljava/lang/Object;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lepo",
            "<TT;>;TT;",
            "Ljava/util/List",
            "<",
            "Leps;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 258
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 259
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leps;

    .line 260
    iget v2, p0, Lepo;->a:I

    iget v0, v0, Leps;->a:I

    ushr-int/lit8 v0, v0, 0x3

    if-ne v2, v0, :cond_0

    .line 261
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 264
    :cond_1
    if-nez p1, :cond_3

    .line 275
    :cond_2
    :goto_1
    return-void

    .line 268
    :cond_3
    instance-of v0, p1, Ljava/util/List;

    if-eqz v0, :cond_4

    .line 269
    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 270
    iget v2, p0, Lepo;->a:I

    invoke-static {v2, v1}, Lept;->a(ILjava/lang/Object;)Leps;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 273
    :cond_4
    iget v0, p0, Lepo;->a:I

    invoke-static {v0, p1}, Lept;->a(ILjava/lang/Object;)Leps;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static a(Ljava/util/List;Lepl;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Leps;",
            ">;",
            "Lepl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 362
    if-nez p0, :cond_1

    .line 369
    :cond_0
    return-void

    .line 365
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leps;

    .line 366
    iget v2, v0, Leps;->a:I

    ushr-int/lit8 v2, v2, 0x3

    iget v3, v0, Leps;->a:I

    and-int/lit8 v3, v3, 0x7

    invoke-virtual {p1, v2, v3}, Lepl;->g(II)V

    .line 367
    iget-object v0, v0, Leps;->b:[B

    invoke-virtual {p1, v0}, Lepl;->c([B)V

    goto :goto_0
.end method

.method public static a(Ljava/util/List;Lepk;I)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Leps;",
            ">;",
            "Lepk;",
            "I)Z"
        }
    .end annotation

    .prologue
    .line 141
    invoke-virtual {p1}, Lepk;->q()I

    move-result v0

    .line 142
    invoke-virtual {p1, p2}, Lepk;->b(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 143
    const/4 v0, 0x0

    .line 148
    :goto_0
    return v0

    .line 145
    :cond_0
    invoke-virtual {p1}, Lepk;->q()I

    move-result v1

    .line 146
    sub-int/2addr v1, v0

    invoke-virtual {p1, v0, v1}, Lepk;->a(II)[B

    move-result-object v0

    .line 147
    new-instance v1, Leps;

    invoke-direct {v1, p2, v0}, Leps;-><init>(I[B)V

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(I)I
    .locals 1

    .prologue
    .line 70
    ushr-int/lit8 v0, p0, 0x3

    return v0
.end method
