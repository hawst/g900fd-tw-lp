.class public final Ldkj;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldkj;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ldkk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    new-array v0, v0, [Ldkj;

    sput-object v0, Ldkj;->a:[Ldkj;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lepn;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Ldkj;->i:Ldkk;

    .line 16
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 60
    const/4 v0, 0x1

    iget-object v1, p0, Ldkj;->b:Ljava/lang/String;

    .line 62
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 63
    const/4 v1, 0x2

    iget-object v2, p0, Ldkj;->c:Ljava/lang/String;

    .line 64
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 65
    const/4 v1, 0x3

    iget-object v2, p0, Ldkj;->d:Ljava/lang/Boolean;

    .line 66
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 67
    const/4 v1, 0x4

    iget-object v2, p0, Ldkj;->e:Ljava/lang/Boolean;

    .line 68
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 69
    iget-object v1, p0, Ldkj;->f:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 70
    const/4 v1, 0x5

    iget-object v2, p0, Ldkj;->f:Ljava/lang/String;

    .line 71
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    :cond_0
    iget-object v1, p0, Ldkj;->g:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 74
    const/4 v1, 0x6

    iget-object v2, p0, Ldkj;->g:Ljava/lang/String;

    .line 75
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 77
    :cond_1
    iget-object v1, p0, Ldkj;->h:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 78
    const/4 v1, 0x7

    iget-object v2, p0, Ldkj;->h:Ljava/lang/String;

    .line 79
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    :cond_2
    iget-object v1, p0, Ldkj;->i:Ldkk;

    if-eqz v1, :cond_3

    .line 82
    const/16 v1, 0x8

    iget-object v2, p0, Ldkj;->i:Ldkk;

    .line 83
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 85
    :cond_3
    iget-object v1, p0, Ldkj;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 86
    iput v0, p0, Ldkj;->cachedSize:I

    .line 87
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 12
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldkj;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldkj;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldkj;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldkj;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldkj;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldkj;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldkj;->e:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldkj;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldkj;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldkj;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Ldkj;->i:Ldkk;

    if-nez v0, :cond_2

    new-instance v0, Ldkk;

    invoke-direct {v0}, Ldkk;-><init>()V

    iput-object v0, p0, Ldkj;->i:Ldkk;

    :cond_2
    iget-object v0, p0, Ldkj;->i:Ldkk;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 38
    const/4 v0, 0x1

    iget-object v1, p0, Ldkj;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 39
    const/4 v0, 0x2

    iget-object v1, p0, Ldkj;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 40
    const/4 v0, 0x3

    iget-object v1, p0, Ldkj;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 41
    const/4 v0, 0x4

    iget-object v1, p0, Ldkj;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 42
    iget-object v0, p0, Ldkj;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 43
    const/4 v0, 0x5

    iget-object v1, p0, Ldkj;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 45
    :cond_0
    iget-object v0, p0, Ldkj;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 46
    const/4 v0, 0x6

    iget-object v1, p0, Ldkj;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 48
    :cond_1
    iget-object v0, p0, Ldkj;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 49
    const/4 v0, 0x7

    iget-object v1, p0, Ldkj;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 51
    :cond_2
    iget-object v0, p0, Ldkj;->i:Ldkk;

    if-eqz v0, :cond_3

    .line 52
    const/16 v0, 0x8

    iget-object v1, p0, Ldkj;->i:Ldkk;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 54
    :cond_3
    iget-object v0, p0, Ldkj;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 56
    return-void
.end method
