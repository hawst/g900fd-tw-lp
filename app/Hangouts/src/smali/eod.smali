.class public final Leod;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leod;


# instance fields
.field public b:Leoc;

.field public c:Leni;

.field public d:Lekw;

.field public e:Lelv;

.field public f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Leod;

    sput-object v0, Leod;->a:[Leod;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 13
    iput-object v0, p0, Leod;->b:Leoc;

    .line 16
    iput-object v0, p0, Leod;->c:Leni;

    .line 19
    iput-object v0, p0, Leod;->d:Lekw;

    .line 22
    iput-object v0, p0, Leod;->e:Lelv;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 50
    const/4 v0, 0x0

    .line 51
    iget-object v1, p0, Leod;->b:Leoc;

    if-eqz v1, :cond_0

    .line 52
    const/4 v0, 0x1

    iget-object v1, p0, Leod;->b:Leoc;

    .line 53
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 55
    :cond_0
    iget-object v1, p0, Leod;->c:Leni;

    if-eqz v1, :cond_1

    .line 56
    const/4 v1, 0x2

    iget-object v2, p0, Leod;->c:Leni;

    .line 57
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59
    :cond_1
    iget-object v1, p0, Leod;->d:Lekw;

    if-eqz v1, :cond_2

    .line 60
    const/4 v1, 0x3

    iget-object v2, p0, Leod;->d:Lekw;

    .line 61
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    :cond_2
    iget-object v1, p0, Leod;->e:Lelv;

    if-eqz v1, :cond_3

    .line 64
    const/4 v1, 0x4

    iget-object v2, p0, Leod;->e:Lelv;

    .line 65
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    :cond_3
    iget-object v1, p0, Leod;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 68
    const/4 v1, 0x5

    iget-object v2, p0, Leod;->f:Ljava/lang/String;

    .line 69
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 71
    :cond_4
    iget-object v1, p0, Leod;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    iput v0, p0, Leod;->cachedSize:I

    .line 73
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leod;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leod;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leod;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Leod;->b:Leoc;

    if-nez v0, :cond_2

    new-instance v0, Leoc;

    invoke-direct {v0}, Leoc;-><init>()V

    iput-object v0, p0, Leod;->b:Leoc;

    :cond_2
    iget-object v0, p0, Leod;->b:Leoc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Leod;->c:Leni;

    if-nez v0, :cond_3

    new-instance v0, Leni;

    invoke-direct {v0}, Leni;-><init>()V

    iput-object v0, p0, Leod;->c:Leni;

    :cond_3
    iget-object v0, p0, Leod;->c:Leni;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Leod;->d:Lekw;

    if-nez v0, :cond_4

    new-instance v0, Lekw;

    invoke-direct {v0}, Lekw;-><init>()V

    iput-object v0, p0, Leod;->d:Lekw;

    :cond_4
    iget-object v0, p0, Leod;->d:Lekw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Leod;->e:Lelv;

    if-nez v0, :cond_5

    new-instance v0, Lelv;

    invoke-direct {v0}, Lelv;-><init>()V

    iput-object v0, p0, Leod;->e:Lelv;

    :cond_5
    iget-object v0, p0, Leod;->e:Lelv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leod;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Leod;->b:Leoc;

    if-eqz v0, :cond_0

    .line 30
    const/4 v0, 0x1

    iget-object v1, p0, Leod;->b:Leoc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 32
    :cond_0
    iget-object v0, p0, Leod;->c:Leni;

    if-eqz v0, :cond_1

    .line 33
    const/4 v0, 0x2

    iget-object v1, p0, Leod;->c:Leni;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 35
    :cond_1
    iget-object v0, p0, Leod;->d:Lekw;

    if-eqz v0, :cond_2

    .line 36
    const/4 v0, 0x3

    iget-object v1, p0, Leod;->d:Lekw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 38
    :cond_2
    iget-object v0, p0, Leod;->e:Lelv;

    if-eqz v0, :cond_3

    .line 39
    const/4 v0, 0x4

    iget-object v1, p0, Leod;->e:Lelv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 41
    :cond_3
    iget-object v0, p0, Leod;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 42
    const/4 v0, 0x5

    iget-object v1, p0, Leod;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 44
    :cond_4
    iget-object v0, p0, Leod;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 46
    return-void
.end method
