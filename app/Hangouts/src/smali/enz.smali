.class public final Lenz;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lenz;


# instance fields
.field public b:Leml;

.field public c:Lezc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lenz;

    sput-object v0, Lenz;->a:[Lenz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 13
    iput-object v0, p0, Lenz;->b:Leml;

    .line 16
    iput-object v0, p0, Lenz;->c:Lezc;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 33
    const/4 v0, 0x0

    .line 34
    iget-object v1, p0, Lenz;->b:Leml;

    if-eqz v1, :cond_0

    .line 35
    const/4 v0, 0x1

    iget-object v1, p0, Lenz;->b:Leml;

    .line 36
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 38
    :cond_0
    iget-object v1, p0, Lenz;->c:Lezc;

    if-eqz v1, :cond_1

    .line 39
    const/4 v1, 0x4

    iget-object v2, p0, Lenz;->c:Lezc;

    .line 40
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 42
    :cond_1
    iget-object v1, p0, Lenz;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43
    iput v0, p0, Lenz;->cachedSize:I

    .line 44
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lenz;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lenz;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lenz;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lenz;->b:Leml;

    if-nez v0, :cond_2

    new-instance v0, Leml;

    invoke-direct {v0}, Leml;-><init>()V

    iput-object v0, p0, Lenz;->b:Leml;

    :cond_2
    iget-object v0, p0, Lenz;->b:Leml;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lenz;->c:Lezc;

    if-nez v0, :cond_3

    new-instance v0, Lezc;

    invoke-direct {v0}, Lezc;-><init>()V

    iput-object v0, p0, Lenz;->c:Lezc;

    :cond_3
    iget-object v0, p0, Lenz;->c:Lezc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x22 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lenz;->b:Leml;

    if-eqz v0, :cond_0

    .line 22
    const/4 v0, 0x1

    iget-object v1, p0, Lenz;->b:Leml;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 24
    :cond_0
    iget-object v0, p0, Lenz;->c:Lezc;

    if-eqz v0, :cond_1

    .line 25
    const/4 v0, 0x4

    iget-object v1, p0, Lenz;->c:Lezc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 27
    :cond_1
    iget-object v0, p0, Lenz;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 29
    return-void
.end method
