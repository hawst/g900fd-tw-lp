.class public final Ldkn;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldkn;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ldko;

.field public d:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 694
    const/4 v0, 0x0

    new-array v0, v0, [Ldkn;

    sput-object v0, Ldkn;->a:[Ldkn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 695
    invoke-direct {p0}, Lepn;-><init>()V

    .line 765
    const/4 v0, 0x0

    iput-object v0, p0, Ldkn;->c:Ldko;

    .line 695
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 787
    const/4 v0, 0x0

    .line 788
    iget-object v1, p0, Ldkn;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 789
    const/4 v0, 0x1

    iget-object v1, p0, Ldkn;->b:Ljava/lang/String;

    .line 790
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 792
    :cond_0
    iget-object v1, p0, Ldkn;->c:Ldko;

    if-eqz v1, :cond_1

    .line 793
    const/4 v1, 0x2

    iget-object v2, p0, Ldkn;->c:Ldko;

    .line 794
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 796
    :cond_1
    iget-object v1, p0, Ldkn;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 797
    const/4 v1, 0x3

    iget-object v2, p0, Ldkn;->d:Ljava/lang/Integer;

    .line 798
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 800
    :cond_2
    iget-object v1, p0, Ldkn;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 801
    iput v0, p0, Ldkn;->cachedSize:I

    .line 802
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 691
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldkn;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldkn;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldkn;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldkn;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldkn;->c:Ldko;

    if-nez v0, :cond_2

    new-instance v0, Ldko;

    invoke-direct {v0}, Ldko;-><init>()V

    iput-object v0, p0, Ldkn;->c:Ldko;

    :cond_2
    iget-object v0, p0, Ldkn;->c:Ldko;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldkn;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 772
    iget-object v0, p0, Ldkn;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 773
    const/4 v0, 0x1

    iget-object v1, p0, Ldkn;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 775
    :cond_0
    iget-object v0, p0, Ldkn;->c:Ldko;

    if-eqz v0, :cond_1

    .line 776
    const/4 v0, 0x2

    iget-object v1, p0, Ldkn;->c:Ldko;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 778
    :cond_1
    iget-object v0, p0, Ldkn;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 779
    const/4 v0, 0x3

    iget-object v1, p0, Ldkn;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 781
    :cond_2
    iget-object v0, p0, Ldkn;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 783
    return-void
.end method
