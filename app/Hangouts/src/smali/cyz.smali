.class public final Lcyz;
.super Lczb;
.source "PG"


# instance fields
.field private c:Ljava/lang/String;
    .annotation runtime Ldbe;
        a = "refresh_token"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/api/client/http/HttpTransport;Lcom/google/api/client/json/JsonFactory;Lcom/google/api/client/http/GenericUrl;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 94
    const-string v0, "refresh_token"

    invoke-direct {p0, p1, p2, p3, v0}, Lczb;-><init>(Lcom/google/api/client/http/HttpTransport;Lcom/google/api/client/json/JsonFactory;Lcom/google/api/client/http/GenericUrl;Ljava/lang/String;)V

    .line 95
    invoke-static {p4}, Lg;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcyz;->c:Ljava/lang/String;

    .line 96
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/Object;)Lcyz;
    .locals 1

    .prologue
    .line 143
    invoke-super {p0, p1, p2}, Lczb;->a(Ljava/lang/String;Ljava/lang/Object;)Lczb;

    move-result-object v0

    check-cast v0, Lcyz;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/api/client/http/HttpExecuteInterceptor;)Lcyz;
    .locals 1

    .prologue
    .line 120
    invoke-super {p0, p1}, Lczb;->b(Lcom/google/api/client/http/HttpExecuteInterceptor;)Lczb;

    move-result-object v0

    check-cast v0, Lcyz;

    return-object v0
.end method

.method public a(Lcom/google/api/client/http/HttpRequestInitializer;)Lcyz;
    .locals 1

    .prologue
    .line 100
    invoke-super {p0, p1}, Lczb;->b(Lcom/google/api/client/http/HttpRequestInitializer;)Lczb;

    move-result-object v0

    check-cast v0, Lcyz;

    return-object v0
.end method

.method public bridge synthetic a(Lcom/google/api/client/http/GenericUrl;)Lczb;
    .locals 1

    .prologue
    .line 80
    invoke-super {p0, p1}, Lczb;->a(Lcom/google/api/client/http/GenericUrl;)Lczb;

    move-result-object v0

    check-cast v0, Lcyz;

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;)Lczb;
    .locals 1

    .prologue
    .line 80
    invoke-super {p0, p1}, Lczb;->a(Ljava/lang/String;)Lczb;

    move-result-object v0

    check-cast v0, Lcyz;

    return-object v0
.end method

.method public synthetic a(Ljava/lang/String;Ljava/lang/Object;)Lczb;
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Lcyz;->b(Ljava/lang/String;Ljava/lang/Object;)Lcyz;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lcom/google/api/client/http/HttpExecuteInterceptor;)Lczb;
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0, p1}, Lcyz;->a(Lcom/google/api/client/http/HttpExecuteInterceptor;)Lcyz;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lcom/google/api/client/http/HttpRequestInitializer;)Lczb;
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0, p1}, Lcyz;->a(Lcom/google/api/client/http/HttpRequestInitializer;)Lcyz;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/util/GenericData;
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Lcyz;->b(Ljava/lang/String;Ljava/lang/Object;)Lcyz;

    move-result-object v0

    return-object v0
.end method
