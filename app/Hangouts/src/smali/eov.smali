.class public final Leov;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leov;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Lemc;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Lemc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 484
    const/4 v0, 0x0

    new-array v0, v0, [Leov;

    sput-object v0, Leov;->a:[Leov;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 485
    invoke-direct {p0}, Lepn;-><init>()V

    .line 495
    iput-object v0, p0, Leov;->b:Ljava/lang/Integer;

    .line 498
    iput-object v0, p0, Leov;->c:Lemc;

    .line 505
    iput-object v0, p0, Leov;->f:Lemc;

    .line 485
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 531
    const/4 v0, 0x0

    .line 532
    iget-object v1, p0, Leov;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 533
    const/4 v0, 0x1

    iget-object v1, p0, Leov;->b:Ljava/lang/Integer;

    .line 534
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 536
    :cond_0
    iget-object v1, p0, Leov;->c:Lemc;

    if-eqz v1, :cond_1

    .line 537
    const/4 v1, 0x2

    iget-object v2, p0, Leov;->c:Lemc;

    .line 538
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 540
    :cond_1
    iget-object v1, p0, Leov;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 541
    const/4 v1, 0x3

    iget-object v2, p0, Leov;->d:Ljava/lang/String;

    .line 542
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 544
    :cond_2
    iget-object v1, p0, Leov;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 545
    const/4 v1, 0x4

    iget-object v2, p0, Leov;->e:Ljava/lang/String;

    .line 546
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 548
    :cond_3
    iget-object v1, p0, Leov;->f:Lemc;

    if-eqz v1, :cond_4

    .line 549
    const/4 v1, 0x5

    iget-object v2, p0, Leov;->f:Lemc;

    .line 550
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 552
    :cond_4
    iget-object v1, p0, Leov;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 553
    iput v0, p0, Leov;->cachedSize:I

    .line 554
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 481
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leov;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leov;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leov;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leov;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leov;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Leov;->c:Lemc;

    if-nez v0, :cond_4

    new-instance v0, Lemc;

    invoke-direct {v0}, Lemc;-><init>()V

    iput-object v0, p0, Leov;->c:Lemc;

    :cond_4
    iget-object v0, p0, Leov;->c:Lemc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leov;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leov;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Leov;->f:Lemc;

    if-nez v0, :cond_5

    new-instance v0, Lemc;

    invoke-direct {v0}, Lemc;-><init>()V

    iput-object v0, p0, Leov;->f:Lemc;

    :cond_5
    iget-object v0, p0, Leov;->f:Lemc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 510
    iget-object v0, p0, Leov;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 511
    const/4 v0, 0x1

    iget-object v1, p0, Leov;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 513
    :cond_0
    iget-object v0, p0, Leov;->c:Lemc;

    if-eqz v0, :cond_1

    .line 514
    const/4 v0, 0x2

    iget-object v1, p0, Leov;->c:Lemc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 516
    :cond_1
    iget-object v0, p0, Leov;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 517
    const/4 v0, 0x3

    iget-object v1, p0, Leov;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 519
    :cond_2
    iget-object v0, p0, Leov;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 520
    const/4 v0, 0x4

    iget-object v1, p0, Leov;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 522
    :cond_3
    iget-object v0, p0, Leov;->f:Lemc;

    if-eqz v0, :cond_4

    .line 523
    const/4 v0, 0x5

    iget-object v1, p0, Leov;->f:Lemc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 525
    :cond_4
    iget-object v0, p0, Leov;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 527
    return-void
.end method
