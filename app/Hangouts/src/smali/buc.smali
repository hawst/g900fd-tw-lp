.class public abstract Lbuc;
.super Landroid/preference/PreferenceFragment;
.source "PG"

# interfaces
.implements Lbtz;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field public a:Lbtf;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;)Landroid/preference/Preference;
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lbuc;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    return-object v0
.end method

.method public a()Landroid/preference/PreferenceScreen;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lbuc;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lbuc;->addPreferencesFromResource(I)V

    .line 47
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lbuc;->startActivity(Landroid/content/Intent;)V

    .line 72
    return-void
.end method

.method public a(Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 66
    invoke-virtual {p0, p1, p2}, Lbuc;->startActivityForResult(Landroid/content/Intent;I)V

    .line 67
    return-void
.end method

.method public a(Landroid/preference/Preference;)V
    .locals 1

    .prologue
    .line 76
    if-eqz p1, :cond_0

    .line 77
    invoke-virtual {p0}, Lbuc;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 79
    :cond_0
    return-void
.end method

.method public a(Landroid/preference/PreferenceScreen;)V
    .locals 0

    .prologue
    .line 61
    invoke-virtual {p0, p1}, Lbuc;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 62
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lbuc;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    .line 42
    return-void
.end method

.method public b()Landroid/preference/PreferenceManager;
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0}, Lbuc;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    return-object v0
.end method

.method public c()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0}, Lbuc;->getActivity()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 31
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDestroy()V

    .line 33
    iget-object v0, p0, Lbuc;->a:Lbtf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbuc;->a:Lbtf;

    invoke-virtual {v0}, Lbtf;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    new-instance v0, Landroid/app/backup/BackupManager;

    invoke-virtual {p0}, Lbuc;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/backup/BackupManager;-><init>(Landroid/content/Context;)V

    .line 35
    invoke-virtual {v0}, Landroid/app/backup/BackupManager;->dataChanged()V

    .line 37
    :cond_0
    return-void
.end method
