.class public final Lewd;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lewd;


# instance fields
.field public b:Levt;

.field public c:[Lexu;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Long;

.field public g:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    new-array v0, v0, [Lewd;

    sput-object v0, Lewd;->a:[Lewd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lepn;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lewd;->b:Levt;

    .line 22
    sget-object v0, Lexu;->a:[Lexu;

    iput-object v0, p0, Lewd;->c:[Lexu;

    .line 16
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 62
    iget-object v0, p0, Lewd;->b:Levt;

    if-eqz v0, :cond_5

    .line 63
    const/4 v0, 0x1

    iget-object v2, p0, Lewd;->b:Levt;

    .line 64
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 66
    :goto_0
    iget-object v2, p0, Lewd;->c:[Lexu;

    if-eqz v2, :cond_1

    .line 67
    iget-object v2, p0, Lewd;->c:[Lexu;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 68
    if-eqz v4, :cond_0

    .line 69
    const/4 v5, 0x2

    .line 70
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 67
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 74
    :cond_1
    const/4 v1, 0x3

    iget-object v2, p0, Lewd;->d:Ljava/lang/String;

    .line 75
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    iget-object v1, p0, Lewd;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 77
    const/4 v1, 0x4

    iget-object v2, p0, Lewd;->e:Ljava/lang/String;

    .line 78
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    :cond_2
    iget-object v1, p0, Lewd;->f:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 81
    const/4 v1, 0x5

    iget-object v2, p0, Lewd;->f:Ljava/lang/Long;

    .line 82
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    :cond_3
    iget-object v1, p0, Lewd;->g:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 85
    const/4 v1, 0x6

    iget-object v2, p0, Lewd;->g:Ljava/lang/Long;

    .line 86
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 88
    :cond_4
    iget-object v1, p0, Lewd;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    iput v0, p0, Lewd;->cachedSize:I

    .line 90
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 12
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lewd;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lewd;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lewd;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lewd;->b:Levt;

    if-nez v0, :cond_2

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Lewd;->b:Levt;

    :cond_2
    iget-object v0, p0, Lewd;->b:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lewd;->c:[Lexu;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lexu;

    iget-object v3, p0, Lewd;->c:[Lexu;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lewd;->c:[Lexu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lewd;->c:[Lexu;

    :goto_2
    iget-object v2, p0, Lewd;->c:[Lexu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lewd;->c:[Lexu;

    new-instance v3, Lexu;

    invoke-direct {v3}, Lexu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lewd;->c:[Lexu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lewd;->c:[Lexu;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lewd;->c:[Lexu;

    new-instance v3, Lexu;

    invoke-direct {v3}, Lexu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lewd;->c:[Lexu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewd;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewd;->e:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lewd;->f:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lewd;->g:Ljava/lang/Long;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 35
    iget-object v0, p0, Lewd;->b:Levt;

    if-eqz v0, :cond_0

    .line 36
    const/4 v0, 0x1

    iget-object v1, p0, Lewd;->b:Levt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 38
    :cond_0
    iget-object v0, p0, Lewd;->c:[Lexu;

    if-eqz v0, :cond_2

    .line 39
    iget-object v1, p0, Lewd;->c:[Lexu;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 40
    if-eqz v3, :cond_1

    .line 41
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 39
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 45
    :cond_2
    const/4 v0, 0x3

    iget-object v1, p0, Lewd;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 46
    iget-object v0, p0, Lewd;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 47
    const/4 v0, 0x4

    iget-object v1, p0, Lewd;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 49
    :cond_3
    iget-object v0, p0, Lewd;->f:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 50
    const/4 v0, 0x5

    iget-object v1, p0, Lewd;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 52
    :cond_4
    iget-object v0, p0, Lewd;->g:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 53
    const/4 v0, 0x6

    iget-object v1, p0, Lewd;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 55
    :cond_5
    iget-object v0, p0, Lewd;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 57
    return-void
.end method
