.class final Lbvt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:I

.field public b:Z

.field public c:Ljava/util/Timer;

.field public d:Lbwq;

.field public e:Lrc;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 241
    iput v1, p0, Lbvt;->a:I

    .line 242
    iput-boolean v1, p0, Lbvt;->b:Z

    .line 243
    iput-object v0, p0, Lbvt;->c:Ljava/util/Timer;

    .line 244
    iput-object v0, p0, Lbvt;->d:Lbwq;

    .line 245
    iput-object v0, p0, Lbvt;->e:Lrc;

    .line 246
    return-void
.end method

.method public static a(ZLrc;)V
    .locals 5

    .prologue
    .line 286
    invoke-static {}, Lbvq;->a()Lbvt;

    move-result-object v0

    iput-boolean p0, v0, Lbvt;->b:Z

    .line 287
    invoke-static {}, Lbvq;->a()Lbvt;

    move-result-object v0

    iput-object p1, v0, Lbvt;->e:Lrc;

    .line 288
    invoke-static {}, Lbvq;->a()Lbvt;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 289
    if-eqz p0, :cond_1

    invoke-static {}, Lbvq;->a()Lbvt;

    move-result-object v0

    iget-object v0, v0, Lbvt;->c:Ljava/util/Timer;

    if-nez v0, :cond_1

    .line 290
    invoke-static {}, Lbvq;->a()Lbvt;

    move-result-object v0

    iget-object v1, v0, Lbvt;->c:Ljava/util/Timer;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lbvt;->c:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    :cond_0
    new-instance v1, Ljava/util/Timer;

    const-string v2, "mms_apn_extension_timer"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Ljava/util/Timer;-><init>(Ljava/lang/String;Z)V

    iput-object v1, v0, Lbvt;->c:Ljava/util/Timer;

    :try_start_0
    iget-object v0, v0, Lbvt;->c:Ljava/util/Timer;

    new-instance v1, Lbvr;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lbvr;-><init>(B)V

    const-string v2, "babel_apn_extension_wait_in_millis"

    const-wide/16 v3, 0x7530

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 292
    :cond_1
    :goto_0
    return-void

    .line 290
    :catch_0
    move-exception v0

    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MmsSendReceiveManager.SendingStatus.startApnExtensionTimer: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lbvt;->c:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lbvt;->c:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 276
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lbvt;->c:Ljava/util/Timer;

    .line 277
    return-void
.end method
