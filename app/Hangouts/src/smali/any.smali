.class final Lany;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lanx;


# direct methods
.method constructor <init>(Lanx;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 407
    iput-object p1, p0, Lany;->b:Lanx;

    iput-object p2, p0, Lany;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 410
    iget-object v0, p0, Lany;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    sget v2, Lg;->ah:I

    if-ne v0, v2, :cond_1

    .line 411
    iget-object v0, p0, Lany;->b:Lanx;

    iget-object v0, v0, Lanx;->a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->b(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)Laod;

    move-result-object v0

    invoke-interface {v0}, Laod;->a()V

    .line 460
    :cond_0
    :goto_0
    return-void

    .line 412
    :cond_1
    iget-object v0, p0, Lany;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    sget v2, Lg;->eQ:I

    if-ne v0, v2, :cond_0

    .line 413
    iget-object v0, p0, Lany;->b:Lanx;

    iget-object v0, v0, Lanx;->a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->c(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)Labk;

    move-result-object v0

    invoke-virtual {v0}, Labk;->e()Lxm;

    move-result-object v2

    .line 414
    iget-object v0, p0, Lany;->b:Lanx;

    iget-object v0, v0, Lanx;->a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->d(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 453
    :pswitch_0
    iget-object v0, p0, Lany;->b:Lanx;

    iget-object v0, v0, Lanx;->a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    .line 454
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->c()Lyj;

    move-result-object v0

    iget-object v1, p0, Lany;->b:Lanx;

    iget-object v1, v1, Lanx;->a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    .line 455
    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->e(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)Ljava/lang/String;

    move-result-object v1

    .line 453
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;Lxm;)I

    .line 457
    iget-object v0, p0, Lany;->b:Lanx;

    iget-object v0, v0, Lanx;->a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->b(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)Laod;

    move-result-object v0

    invoke-interface {v0}, Laod;->b()V

    goto :goto_0

    .line 416
    :pswitch_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 417
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 419
    iget-object v0, p0, Lany;->b:Lanx;

    iget-object v0, v0, Lanx;->a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->b(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)Laod;

    move-result-object v0

    invoke-interface {v0}, Laod;->c()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "audience"

    .line 420
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lxm;

    .line 421
    if-eqz v0, :cond_2

    .line 422
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 430
    :cond_2
    iget-object v0, p0, Lany;->b:Lanx;

    iget-object v0, v0, Lanx;->a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-static {v1}, Lf;->a(Ljava/lang/Iterable;)Lxm;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->a(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;Lxm;)V

    goto :goto_0

    .line 434
    :pswitch_2
    new-instance v3, Ljava/util/ArrayList;

    .line 435
    invoke-virtual {v2}, Lxm;->b()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v1

    .line 436
    :goto_1
    invoke-virtual {v2}, Lxm;->b()I

    move-result v4

    if-ge v0, v4, :cond_3

    .line 437
    invoke-virtual {v2, v0}, Lxm;->a(I)Lxs;

    move-result-object v4

    invoke-virtual {v4}, Lxs;->h()Lbdh;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 436
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 439
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    .line 440
    invoke-virtual {v2}, Lxm;->d()I

    move-result v4

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 441
    :goto_2
    invoke-virtual {v2}, Lxm;->d()I

    move-result v4

    if-ge v1, v4, :cond_4

    .line 442
    invoke-virtual {v2, v1}, Lxm;->b(I)Lxo;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 441
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 445
    :cond_4
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v1

    invoke-virtual {v1, v3, v0}, Lapk;->a(Ljava/util/List;Ljava/util/List;)V

    .line 447
    iget-object v0, p0, Lany;->b:Lanx;

    iget-object v0, v0, Lanx;->a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->b(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)Laod;

    move-result-object v0

    invoke-interface {v0}, Laod;->b()V

    goto/16 :goto_0

    .line 450
    :pswitch_3
    iget-object v0, p0, Lany;->b:Lanx;

    iget-object v0, v0, Lanx;->a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-static {v0, v2}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->a(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;Lxm;)V

    goto/16 :goto_0

    .line 414
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
