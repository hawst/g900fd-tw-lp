.class public final Lbkt;
.super Lbnj;
.source "PG"


# instance fields
.field private final a:Ljava/lang/String;

.field private final d:J

.field private e:I


# direct methods
.method constructor <init>(Lyj;Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lbnj;-><init>(Lyj;)V

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lbkt;->e:I

    .line 33
    iput-object p2, p0, Lbkt;->a:Ljava/lang/String;

    .line 34
    iput-wide p3, p0, Lbkt;->d:J

    .line 35
    return-void
.end method

.method private b(I)V
    .locals 4

    .prologue
    .line 115
    new-instance v0, Ldqv;

    invoke-direct {v0}, Ldqv;-><init>()V

    .line 116
    new-instance v1, Ldvn;

    invoke-direct {v1}, Ldvn;-><init>()V

    iput-object v1, v0, Ldqv;->b:Ldvn;

    .line 117
    iget-object v1, v0, Ldqv;->b:Ldvn;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Ldvn;->b:Ljava/lang/Integer;

    .line 118
    new-instance v1, Lbgf;

    invoke-direct {v1, v0}, Lbgf;-><init>(Ldqv;)V

    .line 119
    new-instance v0, Lbos;

    iget v2, p0, Lbkt;->e:I

    const/4 v3, 0x1

    invoke-direct {v0, v2, v3, v1}, Lbos;-><init>(IILbfz;)V

    .line 121
    iget v1, p0, Lbkt;->e:I

    iget-object v2, p0, Lbkt;->b:Lyj;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(ILyj;Lbos;)V

    .line 122
    return-void
.end method


# virtual methods
.method public a()V
    .locals 12

    .prologue
    const-wide/16 v2, -0x1

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 43
    new-instance v4, Lyt;

    iget-object v0, p0, Lbkt;->b:Lyj;

    invoke-direct {v4, v0}, Lyt;-><init>(Lyj;)V

    .line 44
    iget-object v0, p0, Lbkt;->a:Ljava/lang/String;

    invoke-static {v0}, Lyt;->b(Ljava/lang/String;)Z

    move-result v5

    .line 47
    if-nez v5, :cond_1

    iget-object v0, p0, Lbkt;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lyt;->p(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v11, :cond_1

    .line 50
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lbkt;->b(I)V

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    if-eqz v5, :cond_6

    .line 55
    iget-object v0, p0, Lbkt;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lyt;->aa(Ljava/lang/String;)J

    move-result-wide v0

    .line 57
    :goto_1
    invoke-virtual {v4}, Lyt;->a()V

    .line 59
    :try_start_0
    iget-object v6, p0, Lbkt;->a:Ljava/lang/String;

    invoke-static {v6}, Lyt;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 61
    if-eqz v5, :cond_3

    .line 65
    iget-object v5, p0, Lbkt;->a:Ljava/lang/String;

    const-wide v6, 0x7fffffffffffffffL

    invoke-virtual {v4, v5, v6, v7}, Lyt;->e(Ljava/lang/String;J)Z

    .line 86
    :goto_2
    invoke-static {v4}, Lyp;->d(Lyt;)V

    .line 88
    invoke-virtual {v4}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    invoke-virtual {v4}, Lyt;->c()V

    .line 92
    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    .line 94
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_5

    .line 95
    sget-object v2, Landroid/provider/Telephony$Threads;->CONTENT_URI:Landroid/net/Uri;

    .line 96
    :goto_3
    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 97
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, v10, v10}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 99
    :cond_2
    iget-object v0, p0, Lbkt;->a:Ljava/lang/String;

    invoke-static {v0}, Lyt;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    invoke-direct {p0, v11}, Lbkt;->b(I)V

    goto :goto_0

    .line 73
    :cond_3
    :try_start_1
    iget-object v5, p0, Lbkt;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lyt;->q(Ljava/lang/String;)V

    .line 74
    iget-object v5, p0, Lbkt;->a:Ljava/lang/String;

    sget-wide v6, Lyp;->d:J

    invoke-virtual {v4, v5, v6, v7}, Lyt;->n(Ljava/lang/String;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 90
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lyt;->c()V

    throw v0

    .line 79
    :cond_4
    :try_start_2
    new-instance v5, Lbku;

    iget-object v6, p0, Lbkt;->a:Ljava/lang/String;

    iget-wide v7, p0, Lbkt;->d:J

    const/4 v9, 0x0

    invoke-direct {v5, v6, v7, v8, v9}, Lbku;-><init>(Ljava/lang/String;J[Ljava/lang/String;)V

    .line 82
    iget-object v6, p0, Lbkt;->c:Lbnl;

    invoke-virtual {v5, v6}, Lbku;->a(Lbnl;)V

    .line 84
    iget-object v5, p0, Lbkt;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lyt;->q(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 95
    :cond_5
    const-string v2, "content://mms-sms/"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "conversations"

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_3

    :cond_6
    move-wide v0, v2

    goto :goto_1
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 38
    iput p1, p0, Lbkt;->e:I

    .line 39
    return-void
.end method
