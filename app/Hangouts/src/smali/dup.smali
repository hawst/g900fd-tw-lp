.class public final Ldup;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldup;


# instance fields
.field public b:Ldum;

.field public c:Ldqp;

.field public d:Ldxo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24456
    const/4 v0, 0x0

    new-array v0, v0, [Ldup;

    sput-object v0, Ldup;->a:[Ldup;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24457
    invoke-direct {p0}, Lepn;-><init>()V

    .line 24460
    iput-object v0, p0, Ldup;->b:Ldum;

    .line 24463
    iput-object v0, p0, Ldup;->c:Ldqp;

    .line 24466
    iput-object v0, p0, Ldup;->d:Ldxo;

    .line 24457
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 24486
    const/4 v0, 0x0

    .line 24487
    iget-object v1, p0, Ldup;->b:Ldum;

    if-eqz v1, :cond_0

    .line 24488
    const/4 v0, 0x1

    iget-object v1, p0, Ldup;->b:Ldum;

    .line 24489
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 24491
    :cond_0
    iget-object v1, p0, Ldup;->c:Ldqp;

    if-eqz v1, :cond_1

    .line 24492
    const/4 v1, 0x2

    iget-object v2, p0, Ldup;->c:Ldqp;

    .line 24493
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24495
    :cond_1
    iget-object v1, p0, Ldup;->d:Ldxo;

    if-eqz v1, :cond_2

    .line 24496
    const/4 v1, 0x3

    iget-object v2, p0, Ldup;->d:Ldxo;

    .line 24497
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24499
    :cond_2
    iget-object v1, p0, Ldup;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24500
    iput v0, p0, Ldup;->cachedSize:I

    .line 24501
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 24453
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldup;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldup;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldup;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldup;->b:Ldum;

    if-nez v0, :cond_2

    new-instance v0, Ldum;

    invoke-direct {v0}, Ldum;-><init>()V

    iput-object v0, p0, Ldup;->b:Ldum;

    :cond_2
    iget-object v0, p0, Ldup;->b:Ldum;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldup;->c:Ldqp;

    if-nez v0, :cond_3

    new-instance v0, Ldqp;

    invoke-direct {v0}, Ldqp;-><init>()V

    iput-object v0, p0, Ldup;->c:Ldqp;

    :cond_3
    iget-object v0, p0, Ldup;->c:Ldqp;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldup;->d:Ldxo;

    if-nez v0, :cond_4

    new-instance v0, Ldxo;

    invoke-direct {v0}, Ldxo;-><init>()V

    iput-object v0, p0, Ldup;->d:Ldxo;

    :cond_4
    iget-object v0, p0, Ldup;->d:Ldxo;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 24471
    iget-object v0, p0, Ldup;->b:Ldum;

    if-eqz v0, :cond_0

    .line 24472
    const/4 v0, 0x1

    iget-object v1, p0, Ldup;->b:Ldum;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 24474
    :cond_0
    iget-object v0, p0, Ldup;->c:Ldqp;

    if-eqz v0, :cond_1

    .line 24475
    const/4 v0, 0x2

    iget-object v1, p0, Ldup;->c:Ldqp;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 24477
    :cond_1
    iget-object v0, p0, Ldup;->d:Ldxo;

    if-eqz v0, :cond_2

    .line 24478
    const/4 v0, 0x3

    iget-object v1, p0, Ldup;->d:Ldxo;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 24480
    :cond_2
    iget-object v0, p0, Ldup;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 24482
    return-void
.end method
