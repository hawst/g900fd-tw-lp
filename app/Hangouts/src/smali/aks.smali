.class public final Laks;
.super Lbor;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Laks;->a:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    invoke-direct {p0}, Lbor;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILyj;Lbea;Ljava/lang/Exception;)V
    .locals 6

    .prologue
    .line 110
    iget-object v0, p0, Laks;->a:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->getActivity()Ly;

    move-result-object v2

    .line 111
    iget-object v0, p0, Laks;->a:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->f(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)I

    move-result v0

    if-ne v0, p1, :cond_2

    instance-of v0, p3, Lbeo;

    if-eqz v0, :cond_2

    .line 112
    iget-object v0, p0, Laks;->a:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->g(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)V

    .line 113
    invoke-static {}, Lack;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lh;->jN:I

    .line 116
    :goto_0
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lf;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Laks;->a:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    iget-object v1, p0, Laks;->a:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->a(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;Landroid/view/View;)V

    .line 128
    :cond_0
    :goto_1
    return-void

    .line 113
    :cond_1
    sget v0, Lh;->jO:I

    goto :goto_0

    .line 118
    :cond_2
    iget-object v0, p0, Laks;->a:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->a(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-static {v0, p1}, Lf;->a(Landroid/util/SparseArray;I)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p3, Lbfu;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Laks;->a:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->d(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 121
    iget-object v1, p0, Laks;->a:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    invoke-static {v1, p1}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->a(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;I)V

    .line 122
    iget-object v1, p0, Laks;->a:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->e(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)Lakt;

    move-result-object v1

    invoke-virtual {v1}, Lakt;->notifyDataSetChanged()V

    .line 123
    invoke-static {}, Lack;->j()Z

    move-result v1

    if-eqz v1, :cond_3

    sget v1, Lh;->kx:I

    .line 126
    :goto_2
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v3, v1, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lf;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 123
    :cond_3
    sget v1, Lh;->ky:I

    goto :goto_2
.end method

.method public a(ILyj;Lbos;)V
    .locals 2

    .prologue
    .line 89
    invoke-virtual {p3}, Lbos;->c()Lbfz;

    move-result-object v0

    .line 90
    iget-object v1, p0, Laks;->a:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->f(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)I

    move-result v1

    if-ne v1, p1, :cond_1

    instance-of v1, v0, Lbhb;

    if-eqz v1, :cond_1

    .line 91
    iget-object v0, p0, Laks;->a:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->g(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)V

    .line 92
    iget-object v0, p0, Laks;->a:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    iget-object v1, p0, Laks;->a:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->a(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;Landroid/view/View;)V

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    instance-of v0, v0, Lbin;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Laks;->a:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->a(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 98
    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Laks;->a:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    invoke-static {v0, p1}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->a(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;I)V

    .line 100
    iget-object v0, p0, Laks;->a:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->e(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)Lakt;

    move-result-object v0

    invoke-virtual {v0}, Lakt;->notifyDataSetChanged()V

    .line 102
    iget-object v0, p0, Laks;->a:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    iget-object v1, p0, Laks;->a:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->a(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;Landroid/view/View;)V

    goto :goto_0
.end method
