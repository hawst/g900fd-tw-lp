.class final Lbnm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lyj;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lbcn;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lyj;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 201
    iput-object p1, p0, Lbnm;->a:Lyj;

    .line 202
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const-string v2, "batch_tag_pref"

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "batch_tag"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    const v3, 0x7fffffff

    if-ne v2, v3, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "batch_tag"

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    iput-object v2, p0, Lbnm;->b:Ljava/lang/String;

    .line 203
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbnm;->c:Ljava/util/Set;

    .line 204
    return-void

    .line 202
    :cond_0
    add-int/lit8 v0, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 251
    iget-object v0, p0, Lbnm;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 252
    new-instance v0, Lya;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lbnm;->c:Ljava/util/Set;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v2, p0, Lbnm;->b:Ljava/lang/String;

    iget-object v3, p0, Lbnm;->a:Lyj;

    invoke-direct {v0, v1, v2, v3}, Lya;-><init>(Ljava/util/List;Ljava/lang/String;Lyj;)V

    .line 255
    iget-object v1, p0, Lbnm;->a:Lyj;

    invoke-static {v1}, Lbrf;->a(Lyj;)Lbrf;

    move-result-object v1

    invoke-virtual {v1, v0}, Lbrf;->c(Lbrv;)V

    .line 256
    iget-object v0, p0, Lbnm;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 258
    :cond_0
    return-void
.end method

.method public a(Lyt;Ljava/lang/String;Ljava/util/List;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyt;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lbdk;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 215
    const/4 v0, 0x0

    .line 216
    invoke-virtual {p1}, Lyt;->a()V

    .line 218
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    .line 219
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v3, v0

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdk;

    .line 221
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    sub-long/2addr v5, v1

    .line 222
    invoke-static {}, Lzo;->b()J

    move-result-wide v7

    cmp-long v5, v5, v7

    if-ltz v5, :cond_1

    .line 223
    invoke-virtual {p1}, Lyt;->d()V

    .line 224
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    .line 227
    :cond_1
    iget-object v5, p0, Lbnm;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v5, p2}, Lyt;->a(Lbdk;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    .line 229
    if-eqz v5, :cond_0

    .line 230
    invoke-virtual {v0}, Lbdk;->b()Lbcn;

    move-result-object v5

    .line 231
    invoke-virtual {v5}, Lbcn;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 232
    const/4 v0, 0x1

    .line 233
    iget-object v3, p0, Lbnm;->c:Ljava/util/Set;

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move v3, v0

    goto :goto_0

    .line 235
    :cond_2
    const-string v0, "Babel"

    const-string v5, "RealTimeChatOperationState: invalid spec"

    invoke-static {v0, v5}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 241
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lyt;->c()V

    throw v0

    .line 239
    :cond_3
    :try_start_1
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 241
    invoke-virtual {p1}, Lyt;->c()V

    .line 243
    return v3
.end method
