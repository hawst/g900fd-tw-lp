.class public abstract enum Lecm;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lecm;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lecm;

.field public static final enum b:Lecm;

.field public static final enum c:Lecm;

.field private static final synthetic d:[Lecm;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 376
    new-instance v0, Lecn;

    const-string v1, "STRONG"

    invoke-direct {v0, v1}, Lecn;-><init>(Ljava/lang/String;)V

    sput-object v0, Lecm;->a:Lecm;

    .line 391
    new-instance v0, Leco;

    const-string v1, "SOFT"

    invoke-direct {v0, v1}, Leco;-><init>(Ljava/lang/String;)V

    sput-object v0, Lecm;->b:Lecm;

    .line 407
    new-instance v0, Lecp;

    const-string v1, "WEAK"

    invoke-direct {v0, v1}, Lecp;-><init>(Ljava/lang/String;)V

    sput-object v0, Lecm;->c:Lecm;

    .line 370
    const/4 v0, 0x3

    new-array v0, v0, [Lecm;

    const/4 v1, 0x0

    sget-object v2, Lecm;->a:Lecm;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lecm;->b:Lecm;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lecm;->c:Lecm;

    aput-object v2, v0, v1

    sput-object v0, Lecm;->d:[Lecm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 370
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 370
    invoke-direct {p0, p1, p2}, Lecm;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lecm;
    .locals 1

    .prologue
    .line 370
    const-class v0, Lecm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lecm;

    return-object v0
.end method

.method public static values()[Lecm;
    .locals 1

    .prologue
    .line 370
    sget-object v0, Lecm;->d:[Lecm;

    invoke-virtual {v0}, [Lecm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lecm;

    return-object v0
.end method


# virtual methods
.method abstract a(Lecj;Lcom/google/common/cache/LocalCache$ReferenceEntry;Ljava/lang/Object;I)Lecw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lecj",
            "<TK;TV;>;",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;TV;I)",
            "Lecw",
            "<TK;TV;>;"
        }
    .end annotation
.end method
