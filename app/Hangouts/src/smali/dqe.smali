.class public final Ldqe;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldqe;


# instance fields
.field public b:Ldqf;

.field public c:Ldqw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17189
    const/4 v0, 0x0

    new-array v0, v0, [Ldqe;

    sput-object v0, Ldqe;->a:[Ldqe;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17190
    invoke-direct {p0}, Lepn;-><init>()V

    .line 17193
    iput-object v0, p0, Ldqe;->b:Ldqf;

    .line 17196
    iput-object v0, p0, Ldqe;->c:Ldqw;

    .line 17190
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 17213
    const/4 v0, 0x0

    .line 17214
    iget-object v1, p0, Ldqe;->b:Ldqf;

    if-eqz v1, :cond_0

    .line 17215
    const/4 v0, 0x1

    iget-object v1, p0, Ldqe;->b:Ldqf;

    .line 17216
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 17218
    :cond_0
    iget-object v1, p0, Ldqe;->c:Ldqw;

    if-eqz v1, :cond_1

    .line 17219
    const/4 v1, 0x2

    iget-object v2, p0, Ldqe;->c:Ldqw;

    .line 17220
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17222
    :cond_1
    iget-object v1, p0, Ldqe;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17223
    iput v0, p0, Ldqe;->cachedSize:I

    .line 17224
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 17186
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldqe;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldqe;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldqe;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldqe;->b:Ldqf;

    if-nez v0, :cond_2

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldqe;->b:Ldqf;

    :cond_2
    iget-object v0, p0, Ldqe;->b:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldqe;->c:Ldqw;

    if-nez v0, :cond_3

    new-instance v0, Ldqw;

    invoke-direct {v0}, Ldqw;-><init>()V

    iput-object v0, p0, Ldqe;->c:Ldqw;

    :cond_3
    iget-object v0, p0, Ldqe;->c:Ldqw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 17201
    iget-object v0, p0, Ldqe;->b:Ldqf;

    if-eqz v0, :cond_0

    .line 17202
    const/4 v0, 0x1

    iget-object v1, p0, Ldqe;->b:Ldqf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 17204
    :cond_0
    iget-object v0, p0, Ldqe;->c:Ldqw;

    if-eqz v0, :cond_1

    .line 17205
    const/4 v0, 0x2

    iget-object v1, p0, Ldqe;->c:Ldqw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 17207
    :cond_1
    iget-object v0, p0, Ldqe;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 17209
    return-void
.end method
