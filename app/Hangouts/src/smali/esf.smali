.class public final Lesf;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lesf;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lesc;

.field public e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 389
    const/4 v0, 0x0

    new-array v0, v0, [Lesf;

    sput-object v0, Lesf;->a:[Lesf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 390
    invoke-direct {p0}, Lepn;-><init>()V

    .line 397
    const/4 v0, 0x0

    iput-object v0, p0, Lesf;->d:Lesc;

    .line 390
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 422
    const/4 v0, 0x0

    .line 423
    iget-object v1, p0, Lesf;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 424
    const/4 v0, 0x1

    iget-object v1, p0, Lesf;->b:Ljava/lang/String;

    .line 425
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 427
    :cond_0
    iget-object v1, p0, Lesf;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 428
    const/4 v1, 0x2

    iget-object v2, p0, Lesf;->c:Ljava/lang/String;

    .line 429
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 431
    :cond_1
    iget-object v1, p0, Lesf;->d:Lesc;

    if-eqz v1, :cond_2

    .line 432
    const/4 v1, 0x3

    iget-object v2, p0, Lesf;->d:Lesc;

    .line 433
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 435
    :cond_2
    iget-object v1, p0, Lesf;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 436
    const/4 v1, 0x4

    iget-object v2, p0, Lesf;->e:Ljava/lang/String;

    .line 437
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 439
    :cond_3
    iget-object v1, p0, Lesf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 440
    iput v0, p0, Lesf;->cachedSize:I

    .line 441
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 386
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lesf;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lesf;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lesf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lesf;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lesf;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lesf;->d:Lesc;

    if-nez v0, :cond_2

    new-instance v0, Lesc;

    invoke-direct {v0}, Lesc;-><init>()V

    iput-object v0, p0, Lesf;->d:Lesc;

    :cond_2
    iget-object v0, p0, Lesf;->d:Lesc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lesf;->e:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 404
    iget-object v0, p0, Lesf;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 405
    const/4 v0, 0x1

    iget-object v1, p0, Lesf;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 407
    :cond_0
    iget-object v0, p0, Lesf;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 408
    const/4 v0, 0x2

    iget-object v1, p0, Lesf;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 410
    :cond_1
    iget-object v0, p0, Lesf;->d:Lesc;

    if-eqz v0, :cond_2

    .line 411
    const/4 v0, 0x3

    iget-object v1, p0, Lesf;->d:Lesc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 413
    :cond_2
    iget-object v0, p0, Lesf;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 414
    const/4 v0, 0x4

    iget-object v1, p0, Lesf;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 416
    :cond_3
    iget-object v0, p0, Lesf;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 418
    return-void
.end method
