.class public final Ldvr;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldvr;


# instance fields
.field public b:Ldvm;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/lang/Boolean;

.field public h:[I

.field public i:[[B

.field public j:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19703
    const/4 v0, 0x0

    new-array v0, v0, [Ldvr;

    sput-object v0, Ldvr;->a:[Ldvr;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19704
    invoke-direct {p0}, Lepn;-><init>()V

    .line 19707
    const/4 v0, 0x0

    iput-object v0, p0, Ldvr;->b:Ldvm;

    .line 19720
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Ldvr;->h:[I

    .line 19723
    sget-object v0, Lept;->k:[[B

    iput-object v0, p0, Ldvr;->i:[[B

    .line 19704
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 19768
    iget-object v0, p0, Ldvr;->b:Ldvm;

    if-eqz v0, :cond_a

    .line 19769
    const/4 v0, 0x1

    iget-object v2, p0, Ldvr;->b:Ldvm;

    .line 19770
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 19772
    :goto_0
    iget-object v2, p0, Ldvr;->i:[[B

    if-eqz v2, :cond_1

    iget-object v2, p0, Ldvr;->i:[[B

    array-length v2, v2

    if-lez v2, :cond_1

    .line 19774
    iget-object v4, p0, Ldvr;->i:[[B

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    .line 19776
    invoke-static {v6}, Lepl;->b([B)I

    move-result v6

    add-int/2addr v3, v6

    .line 19774
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 19778
    :cond_0
    add-int/2addr v0, v3

    .line 19779
    iget-object v2, p0, Ldvr;->i:[[B

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 19781
    :cond_1
    iget-object v2, p0, Ldvr;->c:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 19782
    const/4 v2, 0x3

    iget-object v3, p0, Ldvr;->c:Ljava/lang/String;

    .line 19783
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 19785
    :cond_2
    iget-object v2, p0, Ldvr;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 19786
    const/4 v2, 0x4

    iget-object v3, p0, Ldvr;->d:Ljava/lang/Integer;

    .line 19787
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 19789
    :cond_3
    iget-object v2, p0, Ldvr;->j:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    .line 19790
    const/4 v2, 0x5

    iget-object v3, p0, Ldvr;->j:Ljava/lang/Boolean;

    .line 19791
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 19793
    :cond_4
    iget-object v2, p0, Ldvr;->f:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    .line 19794
    const/4 v2, 0x6

    iget-object v3, p0, Ldvr;->f:Ljava/lang/Boolean;

    .line 19795
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 19797
    :cond_5
    iget-object v2, p0, Ldvr;->g:Ljava/lang/Boolean;

    if-eqz v2, :cond_6

    .line 19798
    const/4 v2, 0x7

    iget-object v3, p0, Ldvr;->g:Ljava/lang/Boolean;

    .line 19799
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 19801
    :cond_6
    iget-object v2, p0, Ldvr;->h:[I

    if-eqz v2, :cond_8

    iget-object v2, p0, Ldvr;->h:[I

    array-length v2, v2

    if-lez v2, :cond_8

    .line 19803
    iget-object v3, p0, Ldvr;->h:[I

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_7

    aget v5, v3, v1

    .line 19805
    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 19803
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 19807
    :cond_7
    add-int/2addr v0, v2

    .line 19808
    iget-object v1, p0, Ldvr;->h:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 19810
    :cond_8
    iget-object v1, p0, Ldvr;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 19811
    const/16 v1, 0x9

    iget-object v2, p0, Ldvr;->e:Ljava/lang/Integer;

    .line 19812
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 19814
    :cond_9
    iget-object v1, p0, Ldvr;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19815
    iput v0, p0, Ldvr;->cachedSize:I

    .line 19816
    return v0

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 19700
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldvr;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldvr;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldvr;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldvr;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldvr;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldvr;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldvr;->i:[[B

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [[B

    iget-object v2, p0, Ldvr;->i:[[B

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldvr;->i:[[B

    :goto_1
    iget-object v1, p0, Ldvr;->i:[[B

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Ldvr;->i:[[B

    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Ldvr;->i:[[B

    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldvr;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldvr;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldvr;->j:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldvr;->f:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldvr;->g:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x40

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldvr;->h:[I

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [I

    iget-object v2, p0, Ldvr;->h:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldvr;->h:[I

    :goto_2
    iget-object v1, p0, Ldvr;->h:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Ldvr;->h:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v1, p0, Ldvr;->h:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldvr;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 19730
    iget-object v1, p0, Ldvr;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 19731
    const/4 v1, 0x1

    iget-object v2, p0, Ldvr;->b:Ldvm;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 19733
    :cond_0
    iget-object v1, p0, Ldvr;->i:[[B

    if-eqz v1, :cond_1

    .line 19734
    iget-object v2, p0, Ldvr;->i:[[B

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 19735
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->a(I[B)V

    .line 19734
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 19738
    :cond_1
    iget-object v1, p0, Ldvr;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 19739
    const/4 v1, 0x3

    iget-object v2, p0, Ldvr;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 19741
    :cond_2
    iget-object v1, p0, Ldvr;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 19742
    const/4 v1, 0x4

    iget-object v2, p0, Ldvr;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 19744
    :cond_3
    iget-object v1, p0, Ldvr;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 19745
    const/4 v1, 0x5

    iget-object v2, p0, Ldvr;->j:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 19747
    :cond_4
    iget-object v1, p0, Ldvr;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 19748
    const/4 v1, 0x6

    iget-object v2, p0, Ldvr;->f:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 19750
    :cond_5
    iget-object v1, p0, Ldvr;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 19751
    const/4 v1, 0x7

    iget-object v2, p0, Ldvr;->g:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 19753
    :cond_6
    iget-object v1, p0, Ldvr;->h:[I

    if-eqz v1, :cond_7

    iget-object v1, p0, Ldvr;->h:[I

    array-length v1, v1

    if-lez v1, :cond_7

    .line 19754
    iget-object v1, p0, Ldvr;->h:[I

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_7

    aget v3, v1, v0

    .line 19755
    const/16 v4, 0x8

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 19754
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 19758
    :cond_7
    iget-object v0, p0, Ldvr;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 19759
    const/16 v0, 0x9

    iget-object v1, p0, Ldvr;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 19761
    :cond_8
    iget-object v0, p0, Ldvr;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 19763
    return-void
.end method
