.class final Lcny;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lcnz;


# instance fields
.field private b:Z

.field private c:[I

.field private d:[Lcnz;

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcnz;

    invoke-direct {v0}, Lcnz;-><init>()V

    sput-object v0, Lcny;->a:Lcnz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcny;-><init>(B)V

    return-void
.end method

.method private constructor <init>(B)V
    .locals 3

    const/16 v2, 0xd

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcny;->b:Z

    new-array v0, v2, [I

    iput-object v0, p0, Lcny;->c:[I

    new-array v0, v2, [Lcnz;

    iput-object v0, p0, Lcny;->d:[Lcnz;

    iput v1, p0, Lcny;->e:I

    return-void
.end method

.method private c()V
    .locals 8

    const/4 v2, 0x0

    iget v3, p0, Lcny;->e:I

    iget-object v4, p0, Lcny;->c:[I

    iget-object v5, p0, Lcny;->d:[Lcnz;

    move v1, v2

    move v0, v2

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v6, v5, v1

    sget-object v7, Lcny;->a:Lcnz;

    if-eq v6, v7, :cond_1

    if-eq v1, v0, :cond_0

    aget v7, v4, v1

    aput v7, v4, v0

    aput-object v6, v5, v0

    const/4 v6, 0x0

    aput-object v6, v5, v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iput-boolean v2, p0, Lcny;->b:Z

    iput v0, p0, Lcny;->e:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-boolean v0, p0, Lcny;->b:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcny;->c()V

    :cond_0
    iget v0, p0, Lcny;->e:I

    return v0
.end method

.method public a(I)Lcnz;
    .locals 1

    iget-boolean v0, p0, Lcny;->b:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcny;->c()V

    :cond_0
    iget-object v0, p0, Lcny;->d:[Lcnz;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public b()Z
    .locals 1

    invoke-virtual {p0}, Lcny;->a()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcny;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcny;

    invoke-virtual {p0}, Lcny;->a()I

    move-result v2

    invoke-virtual {p1}, Lcny;->a()I

    move-result v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcny;->c:[I

    iget-object v4, p1, Lcny;->c:[I

    iget v5, p0, Lcny;->e:I

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_6

    aget v6, v3, v2

    aget v7, v4, v2

    if-eq v6, v7, :cond_5

    move v2, v1

    :goto_2
    if-eqz v2, :cond_4

    iget-object v3, p0, Lcny;->d:[Lcnz;

    iget-object v4, p1, Lcny;->d:[Lcnz;

    iget v5, p0, Lcny;->e:I

    move v2, v1

    :goto_3
    if-ge v2, v5, :cond_8

    aget-object v6, v3, v2

    aget-object v7, v4, v2

    invoke-virtual {v6, v7}, Lcnz;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_7

    move v2, v1

    :goto_4
    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_6
    move v2, v0

    goto :goto_2

    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_8
    move v2, v0

    goto :goto_4
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcny;->b:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcny;->c()V

    :cond_0
    const/16 v1, 0x11

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcny;->e:I

    if-ge v0, v2, :cond_1

    mul-int/lit8 v1, v1, 0x1f

    iget-object v2, p0, Lcny;->c:[I

    aget v2, v2, v0

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1f

    iget-object v2, p0, Lcny;->d:[Lcnz;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcnz;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method
