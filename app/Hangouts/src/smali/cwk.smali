.class public final Lcwk;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static c:Landroid/graphics/Bitmap;

.field private static d:I


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcgl;

.field private final e:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private f:Z

.field private g:Lcwl;

.field private final h:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcwl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, -0x1

    sput v0, Lcwk;->d:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcgl;)V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcwk;->e:Ljava/util/concurrent/ConcurrentHashMap;

    .line 91
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcwk;->h:Ljava/util/LinkedList;

    .line 95
    iput-object p1, p0, Lcwk;->a:Landroid/content/Context;

    .line 96
    iput-object p2, p0, Lcwk;->b:Lcgl;

    .line 97
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 205
    sget-object v0, Lcwk;->c:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 207
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->jh:I

    .line 206
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lf;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcwk;->c:Landroid/graphics/Bitmap;

    .line 209
    :cond_0
    sget-object v0, Lcwk;->c:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic a(Lcwk;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcwk;->e:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method static synthetic a(Lcwk;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 37
    if-eqz p2, :cond_1

    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aput-object v3, v1, v2

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Lcwk;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v2, v1, v4

    invoke-direct {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    sget v1, Lcwk;->d:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcwk;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x10e0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcwk;->d:I

    :cond_0
    invoke-virtual {v0, v4}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    sget v1, Lcwk;->d:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    :cond_1
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcwk;->h:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    iget-object v0, p0, Lcwk;->g:Lcwl;

    if-nez v0, :cond_0

    .line 168
    iget-object v0, p0, Lcwk;->h:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwl;

    iput-object v0, p0, Lcwk;->g:Lcwl;

    .line 169
    iget-object v0, p0, Lcwk;->g:Lcwl;

    invoke-virtual {v0}, Lcwl;->a()V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 103
    iget-object v0, p0, Lcwk;->g:Lcwl;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcwk;->g:Lcwl;

    iput-boolean v1, v0, Lcwl;->e:Z

    .line 106
    :cond_0
    iget-object v0, p0, Lcwk;->h:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 107
    iput-boolean v1, p0, Lcwk;->f:Z

    .line 108
    return-void
.end method

.method public a(Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 144
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 145
    const/4 v0, 0x0

    move v1, v0

    .line 147
    :goto_0
    iget-object v0, p0, Lcwk;->h:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 148
    iget-object v0, p0, Lcwk;->h:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwl;

    iget-object v0, v0, Lcwl;->a:Landroid/widget/ImageView;

    if-ne v0, p1, :cond_0

    .line 149
    iget-object v0, p0, Lcwk;->h:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 151
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 155
    :cond_1
    iget-object v0, p0, Lcwk;->g:Lcwl;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcwk;->g:Lcwl;

    iget-object v0, v0, Lcwl;->a:Landroid/widget/ImageView;

    if-ne v0, p1, :cond_2

    .line 156
    iget-object v0, p0, Lcwk;->g:Lcwl;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcwl;->e:Z

    .line 158
    :cond_2
    return-void
.end method

.method public a(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcwk;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    iget-object v0, p0, Lcwk;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 117
    invoke-virtual {p0, p1}, Lcwk;->a(Landroid/widget/ImageView;)V

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    iget-object v0, p0, Lcwk;->a:Landroid/content/Context;

    invoke-static {v0}, Lcwk;->a(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 121
    new-instance v0, Lcwl;

    invoke-direct {v0, p0, p1, p2, p3}, Lcwl;-><init>(Lcwk;Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcwk;->a(Landroid/widget/ImageView;)V

    iget-object v1, p0, Lcwk;->b:Lcgl;

    invoke-interface {v1}, Lcgl;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    iget-object v1, p0, Lcwk;->h:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcwk;->b()V

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;Lcwl;)V
    .locals 2

    .prologue
    .line 175
    :try_start_0
    iget-object v0, p0, Lcwk;->g:Lcwl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, p3, :cond_1

    .line 176
    iget-boolean v0, p0, Lcwk;->f:Z

    if-nez v0, :cond_0

    .line 199
    invoke-direct {p0}, Lcwk;->b()V

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcwk;->g:Lcwl;

    .line 181
    iget-boolean v0, p0, Lcwk;->f:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_2

    .line 198
    iget-boolean v0, p0, Lcwk;->f:Z

    if-nez v0, :cond_0

    .line 199
    invoke-direct {p0}, Lcwk;->b()V

    goto :goto_0

    .line 184
    :cond_2
    :try_start_2
    iget-object v0, p3, Lcwl;->a:Landroid/widget/ImageView;

    .line 185
    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p3, :cond_3

    iget-boolean v0, p3, Lcwl;->e:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_4

    .line 198
    :cond_3
    iget-boolean v0, p0, Lcwk;->f:Z

    if-nez v0, :cond_0

    .line 199
    invoke-direct {p0}, Lcwk;->b()V

    goto :goto_0

    .line 190
    :cond_4
    :try_start_3
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    if-nez p2, :cond_6

    .line 191
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Avatar loaded: status="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  pfd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 194
    :cond_6
    if-eqz p2, :cond_7

    .line 195
    new-instance v0, Lcwn;

    invoke-direct {v0, p0, p3, p2}, Lcwn;-><init>(Lcwk;Lcwl;Landroid/os/ParcelFileDescriptor;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcwn;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 198
    :cond_7
    iget-boolean v0, p0, Lcwk;->f:Z

    if-nez v0, :cond_0

    .line 199
    invoke-direct {p0}, Lcwk;->b()V

    goto :goto_0

    .line 198
    :catchall_0
    move-exception v0

    iget-boolean v1, p0, Lcwk;->f:Z

    if-nez v1, :cond_8

    .line 199
    invoke-direct {p0}, Lcwk;->b()V

    :cond_8
    throw v0
.end method
