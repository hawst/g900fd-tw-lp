.class public final Ldhg;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldhg;


# instance fields
.field public b:Ldhf;

.field public c:Ldhf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3347
    const/4 v0, 0x0

    new-array v0, v0, [Ldhg;

    sput-object v0, Ldhg;->a:[Ldhg;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3348
    invoke-direct {p0}, Lepn;-><init>()V

    .line 3351
    iput-object v0, p0, Ldhg;->b:Ldhf;

    .line 3354
    iput-object v0, p0, Ldhg;->c:Ldhf;

    .line 3348
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 3371
    const/4 v0, 0x0

    .line 3372
    iget-object v1, p0, Ldhg;->b:Ldhf;

    if-eqz v1, :cond_0

    .line 3373
    const/4 v0, 0x1

    iget-object v1, p0, Ldhg;->b:Ldhf;

    .line 3374
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3376
    :cond_0
    iget-object v1, p0, Ldhg;->c:Ldhf;

    if-eqz v1, :cond_1

    .line 3377
    const/4 v1, 0x2

    iget-object v2, p0, Ldhg;->c:Ldhf;

    .line 3378
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3380
    :cond_1
    iget-object v1, p0, Ldhg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3381
    iput v0, p0, Ldhg;->cachedSize:I

    .line 3382
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 3344
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldhg;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldhg;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldhg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldhg;->b:Ldhf;

    if-nez v0, :cond_2

    new-instance v0, Ldhf;

    invoke-direct {v0}, Ldhf;-><init>()V

    iput-object v0, p0, Ldhg;->b:Ldhf;

    :cond_2
    iget-object v0, p0, Ldhg;->b:Ldhf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldhg;->c:Ldhf;

    if-nez v0, :cond_3

    new-instance v0, Ldhf;

    invoke-direct {v0}, Ldhf;-><init>()V

    iput-object v0, p0, Ldhg;->c:Ldhf;

    :cond_3
    iget-object v0, p0, Ldhg;->c:Ldhf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 3359
    iget-object v0, p0, Ldhg;->b:Ldhf;

    if-eqz v0, :cond_0

    .line 3360
    const/4 v0, 0x1

    iget-object v1, p0, Ldhg;->b:Ldhf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 3362
    :cond_0
    iget-object v0, p0, Ldhg;->c:Ldhf;

    if-eqz v0, :cond_1

    .line 3363
    const/4 v0, 0x2

    iget-object v1, p0, Ldhg;->c:Ldhf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 3365
    :cond_1
    iget-object v0, p0, Ldhg;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 3367
    return-void
.end method
