.class public final Lbkg;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Z)V
    .locals 4

    .prologue
    .line 228
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lbkb;->a(Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 231
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 232
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 233
    const-string v2, "smsmms"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 235
    sget v2, Lh;->cz:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 236
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 237
    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 238
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 240
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v0

    .line 241
    if-eqz p0, :cond_0

    .line 242
    invoke-static {v0}, Lbwf;->c(Lyj;)V

    .line 251
    :goto_0
    invoke-static {}, Lcom/google/android/apps/hangouts/sms/SmsReceiver;->a()V

    .line 254
    invoke-static {}, Lbkb;->E()Lbkd;

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 255
    return-void

    .line 244
    :cond_0
    invoke-static {v0}, Lbwf;->a(Lyj;)V

    .line 249
    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;->b()V

    goto :goto_0
.end method

.method public static a()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 194
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    .line 196
    invoke-static {}, Lbkb;->D()Ljava/lang/Boolean;

    move-result-object v3

    if-nez v3, :cond_0

    .line 197
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 198
    const-string v4, "smsmms"

    invoke-virtual {v2, v4, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 200
    sget v5, Lh;->cz:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 201
    sget v6, Lf;->bM:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    .line 202
    invoke-interface {v4, v5, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v3}, Lbkb;->a(Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 205
    :cond_0
    invoke-static {}, Lbzd;->d()Z

    move-result v3

    if-nez v3, :cond_3

    .line 207
    invoke-static {}, Lbkb;->D()Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v2, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 208
    invoke-static {v1}, Lbkg;->a(Z)V

    .line 209
    invoke-static {}, Lbkb;->D()Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v2, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v2

    if-nez v2, :cond_2

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 221
    :cond_1
    :goto_1
    invoke-static {}, Lbkb;->D()Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    return v0

    :cond_2
    move v0, v1

    .line 209
    goto :goto_0

    .line 211
    :cond_3
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-lt v3, v4, :cond_1

    .line 215
    invoke-static {v2}, Landroid/provider/Telephony$Sms;->getDefaultSmsPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 216
    invoke-static {}, Lbkb;->D()Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v3, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v3

    if-eq v3, v2, :cond_1

    .line 217
    invoke-static {v2}, Lbkg;->a(Z)V

    .line 218
    invoke-static {}, Lbkb;->D()Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v3, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v3

    if-ne v3, v2, :cond_4

    :goto_2
    invoke-static {v0}, Lcwz;->a(Z)V

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method
