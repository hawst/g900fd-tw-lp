.class public final Lcaa;
.super Landroid/view/inputmethod/InputConnectionWrapper;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/views/AudienceTextView;

.field private b:Lcom/google/android/apps/hangouts/views/AudienceTextView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/views/AudienceTextView;Landroid/view/inputmethod/InputConnection;)V
    .locals 1

    .prologue
    .line 68
    iput-object p1, p0, Lcaa;->a:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    .line 69
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Landroid/view/inputmethod/InputConnectionWrapper;-><init>(Landroid/view/inputmethod/InputConnection;Z)V

    .line 70
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/hangouts/views/AudienceTextView;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcaa;->b:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    .line 74
    return-void
.end method

.method public deleteSurroundingText(II)Z
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcaa;->a:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->getSelectionStart()I

    move-result v0

    .line 79
    iget-object v1, p0, Lcaa;->a:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->getSelectionEnd()I

    move-result v1

    .line 80
    if-lez p1, :cond_0

    if-gtz p2, :cond_0

    if-gtz v0, :cond_0

    if-gtz v1, :cond_0

    .line 83
    iget-object v0, p0, Lcaa;->a:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->a(Lcom/google/android/apps/hangouts/views/AudienceTextView;)Lcab;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcaa;->b:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcaa;->a:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->a(Lcom/google/android/apps/hangouts/views/AudienceTextView;)Lcab;

    move-result-object v0

    iget-object v1, p0, Lcaa;->b:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    invoke-interface {v0}, Lcab;->a()V

    .line 85
    const/4 v0, 0x1

    .line 88
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/InputConnectionWrapper;->deleteSurroundingText(II)Z

    move-result v0

    goto :goto_0
.end method

.method public performEditorAction(I)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 94
    const/4 v0, 0x6

    if-ne p1, v0, :cond_0

    .line 95
    iget-object v0, p0, Lcaa;->a:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->showDropDown()V

    .line 96
    iget-object v0, p0, Lcaa;->a:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lame;

    .line 97
    invoke-virtual {v0, v1}, Lame;->d(I)I

    move-result v3

    .line 98
    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    .line 108
    :cond_0
    :goto_0
    return v1

    .line 102
    :cond_1
    invoke-virtual {v0, v3}, Lame;->b(I)Z

    move-result v3

    if-eqz v3, :cond_2

    move v1, v2

    .line 104
    :cond_2
    iget-object v3, p0, Lcaa;->a:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    invoke-virtual {v0, v1}, Lame;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->a(Lcom/google/android/apps/hangouts/views/AudienceTextView;Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 105
    iget-object v4, p0, Lcaa;->a:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    new-instance v5, Landroid/view/inputmethod/CompletionInfo;

    invoke-virtual {v0, v1}, Lame;->getItemId(I)J

    move-result-wide v6

    invoke-direct {v5, v6, v7, v1, v3}, Landroid/view/inputmethod/CompletionInfo;-><init>(JILjava/lang/CharSequence;)V

    invoke-virtual {v4, v5}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->onCommitCompletion(Landroid/view/inputmethod/CompletionInfo;)V

    move v1, v2

    .line 106
    goto :goto_0
.end method
