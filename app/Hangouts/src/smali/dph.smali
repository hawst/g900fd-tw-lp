.class public final Ldph;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldph;


# instance fields
.field public b:Ldvm;

.field public c:Ldrx;

.field public d:[Ldth;

.field public e:[B

.field public f:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5704
    const/4 v0, 0x0

    new-array v0, v0, [Ldph;

    sput-object v0, Ldph;->a:[Ldph;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5705
    invoke-direct {p0}, Lepn;-><init>()V

    .line 5708
    iput-object v0, p0, Ldph;->b:Ldvm;

    .line 5711
    iput-object v0, p0, Ldph;->c:Ldrx;

    .line 5714
    sget-object v0, Ldth;->a:[Ldth;

    iput-object v0, p0, Ldph;->d:[Ldth;

    .line 5705
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 5749
    iget-object v0, p0, Ldph;->b:Ldvm;

    if-eqz v0, :cond_5

    .line 5750
    const/4 v0, 0x1

    iget-object v2, p0, Ldph;->b:Ldvm;

    .line 5751
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5753
    :goto_0
    iget-object v2, p0, Ldph;->e:[B

    if-eqz v2, :cond_0

    .line 5754
    const/4 v2, 0x2

    iget-object v3, p0, Ldph;->e:[B

    .line 5755
    invoke-static {v2, v3}, Lepl;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 5757
    :cond_0
    iget-object v2, p0, Ldph;->d:[Ldth;

    if-eqz v2, :cond_2

    .line 5758
    iget-object v2, p0, Ldph;->d:[Ldth;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 5759
    if-eqz v4, :cond_1

    .line 5760
    const/4 v5, 0x3

    .line 5761
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 5758
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 5765
    :cond_2
    iget-object v1, p0, Ldph;->f:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 5766
    const/4 v1, 0x4

    iget-object v2, p0, Ldph;->f:Ljava/lang/Long;

    .line 5767
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 5769
    :cond_3
    iget-object v1, p0, Ldph;->c:Ldrx;

    if-eqz v1, :cond_4

    .line 5770
    const/4 v1, 0x5

    iget-object v2, p0, Ldph;->c:Ldrx;

    .line 5771
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5773
    :cond_4
    iget-object v1, p0, Ldph;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5774
    iput v0, p0, Ldph;->cachedSize:I

    .line 5775
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5701
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldph;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldph;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldph;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldph;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldph;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldph;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Ldph;->e:[B

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldph;->d:[Ldth;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldth;

    iget-object v3, p0, Ldph;->d:[Ldth;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldph;->d:[Ldth;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldph;->d:[Ldth;

    :goto_2
    iget-object v2, p0, Ldph;->d:[Ldth;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldph;->d:[Ldth;

    new-instance v3, Ldth;

    invoke-direct {v3}, Ldth;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldph;->d:[Ldth;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldph;->d:[Ldth;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldph;->d:[Ldth;

    new-instance v3, Ldth;

    invoke-direct {v3}, Ldth;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldph;->d:[Ldth;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldph;->f:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Ldph;->c:Ldrx;

    if-nez v0, :cond_6

    new-instance v0, Ldrx;

    invoke-direct {v0}, Ldrx;-><init>()V

    iput-object v0, p0, Ldph;->c:Ldrx;

    :cond_6
    iget-object v0, p0, Ldph;->c:Ldrx;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 5723
    iget-object v0, p0, Ldph;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 5724
    const/4 v0, 0x1

    iget-object v1, p0, Ldph;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5726
    :cond_0
    iget-object v0, p0, Ldph;->e:[B

    if-eqz v0, :cond_1

    .line 5727
    const/4 v0, 0x2

    iget-object v1, p0, Ldph;->e:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 5729
    :cond_1
    iget-object v0, p0, Ldph;->d:[Ldth;

    if-eqz v0, :cond_3

    .line 5730
    iget-object v1, p0, Ldph;->d:[Ldth;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 5731
    if-eqz v3, :cond_2

    .line 5732
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 5730
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5736
    :cond_3
    iget-object v0, p0, Ldph;->f:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 5737
    const/4 v0, 0x4

    iget-object v1, p0, Ldph;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 5739
    :cond_4
    iget-object v0, p0, Ldph;->c:Ldrx;

    if-eqz v0, :cond_5

    .line 5740
    const/4 v0, 0x5

    iget-object v1, p0, Ldph;->c:Ldrx;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5742
    :cond_5
    iget-object v0, p0, Ldph;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 5744
    return-void
.end method
