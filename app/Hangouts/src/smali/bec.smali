.class public Lbec;
.super Lbed;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Z

.field public final b:J


# direct methods
.method public constructor <init>(Ljava/lang/String;JZ)V
    .locals 0

    .prologue
    .line 4076
    invoke-direct {p0, p1}, Lbed;-><init>(Ljava/lang/String;)V

    .line 4077
    iput-wide p2, p0, Lbec;->b:J

    .line 4078
    iput-boolean p4, p0, Lbec;->a:Z

    .line 4079
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 4

    .prologue
    .line 4083
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 4084
    const-string v0, "Babel_RequestWriter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Archive conversation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbec;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lbec;->a:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4086
    :cond_0
    new-instance v1, Ldty;

    invoke-direct {v1}, Ldty;-><init>()V

    .line 4089
    invoke-virtual {p0, p1, p2}, Lbec;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v0

    iput-object v0, v1, Ldty;->b:Ldvm;

    .line 4090
    iget-object v0, p0, Lbec;->c:Ljava/lang/String;

    invoke-static {v0}, Lbea;->d(Ljava/lang/String;)Ldqf;

    move-result-object v0

    iput-object v0, v1, Ldty;->c:Ldqf;

    .line 4091
    iget-wide v2, p0, Lbec;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, Ldty;->e:Ljava/lang/Long;

    .line 4092
    iget-boolean v0, p0, Lbec;->a:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldty;->d:Ljava/lang/Integer;

    .line 4095
    return-object v1

    .line 4092
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4105
    const-string v0, "conversations/modifyconversationview"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4100
    const-string v0, "event_queue"

    return-object v0
.end method
