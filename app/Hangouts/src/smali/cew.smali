.class public final Lcew;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Lcdf;


# instance fields
.field private final a:Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

.field private final b:Landroid/view/View;

.field private final c:Landroid/view/View;

.field private final d:Landroid/widget/TextView;

.field private e:Lyj;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcew;-><init>(Landroid/content/Context;B)V

    .line 41
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcew;-><init>(Landroid/content/Context;C)V

    .line 45
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;C)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 48
    invoke-direct {p0, p1, v5, v4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 51
    sget v0, Lf;->eh:I

    invoke-virtual {v1, v0, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

    iput-object v0, p0, Lcew;->a:Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

    .line 53
    iget-object v0, p0, Lcew;->a:Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

    sget v2, Lcom/google/android/apps/hangouts/R$drawable;->cQ:I

    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->cP:I

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->a(II)V

    .line 55
    iget-object v0, p0, Lcew;->a:Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

    new-instance v2, Lcex;

    invoke-direct {v2, p0}, Lcex;-><init>(Lcew;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->a(Lcat;)V

    .line 68
    iget-object v0, p0, Lcew;->a:Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

    new-instance v2, Lcey;

    invoke-direct {v2, p0}, Lcey;-><init>(Lcew;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->a(Lcar;)V

    .line 84
    iget-object v0, p0, Lcew;->a:Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->setVisibility(I)V

    .line 85
    iget-object v0, p0, Lcew;->a:Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

    invoke-virtual {p0, v0}, Lcew;->addView(Landroid/view/View;)V

    .line 87
    sget v0, Lf;->gJ:I

    invoke-virtual {v1, v0, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcew;->b:Landroid/view/View;

    .line 88
    iget-object v0, p0, Lcew;->b:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcew;->addView(Landroid/view/View;)V

    .line 90
    iget-object v0, p0, Lcew;->b:Landroid/view/View;

    sget v1, Lg;->id:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcew;->d:Landroid/widget/TextView;

    .line 91
    iget-object v0, p0, Lcew;->b:Landroid/view/View;

    sget v1, Lg;->ft:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcew;->c:Landroid/view/View;

    .line 92
    iget-object v0, p0, Lcew;->c:Landroid/view/View;

    new-instance v1, Lcez;

    invoke-direct {v1, p0}, Lcez;-><init>(Lcew;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    return-void
.end method

.method static synthetic a(Lcew;)Landroid/view/View;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcew;->b:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lcew;)Lcom/google/android/apps/hangouts/views/AudioAttachmentView;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcew;->a:Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

    return-object v0
.end method

.method static synthetic c(Lcew;)Lyj;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcew;->e:Lyj;

    return-object v0
.end method

.method static synthetic d(Lcew;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcew;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 150
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 152
    return-void
.end method

.method public a(Lyj;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcas;)V
    .locals 8

    .prologue
    .line 108
    iput-object p1, p0, Lcew;->e:Lyj;

    .line 109
    iput-object p5, p0, Lcew;->f:Ljava/lang/String;

    .line 110
    iget-object v0, p0, Lcew;->a:Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

    invoke-virtual {v0, p2, p6, p7}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->a(Ljava/lang/String;Ljava/lang/String;Lcas;)V

    .line 113
    div-int/lit16 v0, p3, 0x3e8

    .line 114
    div-int/lit8 v1, v0, 0x3c

    .line 115
    rem-int/lit8 v0, v0, 0x3c

    .line 116
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 117
    invoke-virtual {p0}, Lcew;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 118
    if-lez v1, :cond_0

    .line 119
    sget v4, Lf;->hy:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v1, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    const-string v1, " "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    :cond_0
    sget v1, Lf;->hz:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v1, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 124
    const-string v0, " \u2022 "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    invoke-static {p4}, Lbzd;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    :cond_1
    iget-object v0, p0, Lcew;->d:Landroid/widget/TextView;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcew;->a:Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->d()V

    .line 103
    return-void
.end method

.method public d_(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcew;->a:Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->a(Ljava/lang/String;)V

    .line 145
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 132
    return-void
.end method

.method public f()V
    .locals 0

    .prologue
    .line 136
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 140
    return-void
.end method
