.class public abstract enum Leff;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Leff;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Leff;

.field public static final enum b:Leff;

.field public static final enum c:Leff;

.field public static final enum d:Leff;

.field public static final enum e:Leff;

.field public static final enum f:Leff;

.field public static final enum g:Leff;

.field public static final enum h:Leff;

.field static final i:[[Leff;

.field private static final synthetic j:[Leff;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 352
    new-instance v0, Lefg;

    const-string v1, "STRONG"

    invoke-direct {v0, v1}, Lefg;-><init>(Ljava/lang/String;)V

    sput-object v0, Leff;->a:Leff;

    .line 360
    new-instance v0, Lefh;

    const-string v1, "STRONG_EXPIRABLE"

    invoke-direct {v0, v1}, Lefh;-><init>(Ljava/lang/String;)V

    sput-object v0, Leff;->b:Leff;

    .line 376
    new-instance v0, Lefi;

    const-string v1, "STRONG_EVICTABLE"

    invoke-direct {v0, v1}, Lefi;-><init>(Ljava/lang/String;)V

    sput-object v0, Leff;->c:Leff;

    .line 392
    new-instance v0, Lefj;

    const-string v1, "STRONG_EXPIRABLE_EVICTABLE"

    invoke-direct {v0, v1}, Lefj;-><init>(Ljava/lang/String;)V

    sput-object v0, Leff;->d:Leff;

    .line 410
    new-instance v0, Lefk;

    const-string v1, "WEAK"

    invoke-direct {v0, v1}, Lefk;-><init>(Ljava/lang/String;)V

    sput-object v0, Leff;->e:Leff;

    .line 418
    new-instance v0, Lefl;

    const-string v1, "WEAK_EXPIRABLE"

    invoke-direct {v0, v1}, Lefl;-><init>(Ljava/lang/String;)V

    sput-object v0, Leff;->f:Leff;

    .line 434
    new-instance v0, Lefm;

    const-string v1, "WEAK_EVICTABLE"

    invoke-direct {v0, v1}, Lefm;-><init>(Ljava/lang/String;)V

    sput-object v0, Leff;->g:Leff;

    .line 450
    new-instance v0, Lefn;

    const-string v1, "WEAK_EXPIRABLE_EVICTABLE"

    invoke-direct {v0, v1}, Lefn;-><init>(Ljava/lang/String;)V

    sput-object v0, Leff;->h:Leff;

    .line 351
    const/16 v0, 0x8

    new-array v0, v0, [Leff;

    sget-object v1, Leff;->a:Leff;

    aput-object v1, v0, v3

    sget-object v1, Leff;->b:Leff;

    aput-object v1, v0, v4

    sget-object v1, Leff;->c:Leff;

    aput-object v1, v0, v5

    sget-object v1, Leff;->d:Leff;

    aput-object v1, v0, v6

    sget-object v1, Leff;->e:Leff;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Leff;->f:Leff;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Leff;->g:Leff;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Leff;->h:Leff;

    aput-object v2, v0, v1

    sput-object v0, Leff;->j:[Leff;

    .line 478
    new-array v0, v6, [[Leff;

    new-array v1, v7, [Leff;

    sget-object v2, Leff;->a:Leff;

    aput-object v2, v1, v3

    sget-object v2, Leff;->b:Leff;

    aput-object v2, v1, v4

    sget-object v2, Leff;->c:Leff;

    aput-object v2, v1, v5

    sget-object v2, Leff;->d:Leff;

    aput-object v2, v1, v6

    aput-object v1, v0, v3

    new-array v1, v3, [Leff;

    aput-object v1, v0, v4

    new-array v1, v7, [Leff;

    sget-object v2, Leff;->e:Leff;

    aput-object v2, v1, v3

    sget-object v2, Leff;->f:Leff;

    aput-object v2, v1, v4

    sget-object v2, Leff;->g:Leff;

    aput-object v2, v1, v5

    sget-object v2, Leff;->h:Leff;

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    sput-object v0, Leff;->i:[[Leff;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 351
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 351
    invoke-direct {p0, p1, p2}, Leff;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Legd;ZZ)Leff;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 486
    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz p2, :cond_0

    const/4 v0, 0x2

    :cond_0
    or-int/2addr v0, v1

    .line 487
    sget-object v1, Leff;->i:[[Leff;

    invoke-virtual {p0}, Legd;->ordinal()I

    move-result v2

    aget-object v1, v1, v2

    aget-object v0, v1, v0

    return-object v0

    :cond_1
    move v1, v0

    .line 486
    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Leff;
    .locals 1

    .prologue
    .line 351
    const-class v0, Leff;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Leff;

    return-object v0
.end method

.method public static values()[Leff;
    .locals 1

    .prologue
    .line 351
    sget-object v0, Leff;->j:[Leff;

    invoke-virtual {v0}, [Leff;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Leff;

    return-object v0
.end method


# virtual methods
.method a(Lega;Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lega",
            "<TK;TV;>;",
            "Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry",
            "<TK;TV;>;",
            "Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry",
            "<TK;TV;>;)",
            "Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 510
    invoke-interface {p2}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->getHash()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, p3}, Leff;->a(Lega;Ljava/lang/Object;ILcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;

    move-result-object v0

    return-object v0
.end method

.method abstract a(Lega;Ljava/lang/Object;ILcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lega",
            "<TK;TV;>;TK;I",
            "Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry",
            "<TK;TV;>;)",
            "Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method a(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry",
            "<TK;TV;>;",
            "Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 517
    invoke-interface {p1}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->getExpirationTime()J

    move-result-wide v0

    invoke-interface {p2, v0, v1}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->setExpirationTime(J)V

    .line 519
    invoke-interface {p1}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->getPreviousExpirable()Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/common/collect/MapMakerInternalMap;->a(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)V

    .line 520
    invoke-interface {p1}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->getNextExpirable()Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/common/collect/MapMakerInternalMap;->a(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)V

    .line 522
    invoke-static {p1}, Lcom/google/common/collect/MapMakerInternalMap;->d(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)V

    .line 523
    return-void
.end method

.method b(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry",
            "<TK;TV;>;",
            "Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 529
    invoke-interface {p1}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->getPreviousEvictable()Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/common/collect/MapMakerInternalMap;->b(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)V

    .line 530
    invoke-interface {p1}, Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;->getNextEvictable()Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/common/collect/MapMakerInternalMap;->b(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)V

    .line 532
    invoke-static {p1}, Lcom/google/common/collect/MapMakerInternalMap;->e(Lcom/google/common/collect/MapMakerInternalMap$ReferenceEntry;)V

    .line 533
    return-void
.end method
