.class public final Lbub;
.super Lbtf;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static final e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lbys;->m:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbub;->e:Z

    return-void
.end method

.method public constructor <init>(Lbtz;Landroid/app/Activity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lbtf;-><init>(Lbtz;Landroid/app/Activity;Ljava/lang/String;)V

    .line 32
    return-void
.end method


# virtual methods
.method protected a(IZLjava/lang/Integer;IIILandroid/content/SharedPreferences;)Landroid/preference/Preference;
    .locals 7

    .prologue
    .line 38
    iget-object v0, p0, Lbub;->a:Lbtz;

    iget-object v1, p0, Lbub;->b:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 40
    const/4 v4, 0x0

    .line 42
    if-eqz p3, :cond_0

    .line 43
    iget-object v0, p0, Lbub;->b:Landroid/content/res/Resources;

    .line 44
    const/4 v2, 0x0

    invoke-static {p3, v2}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 46
    :cond_0
    iget-object v0, p0, Lbub;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 47
    iget-object v0, p0, Lbub;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 49
    const-class v0, Lbuo;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setFragment(Ljava/lang/String;)V

    .line 50
    invoke-virtual {v1}, Landroid/preference/Preference;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 51
    const-string v2, "account_name"

    iget-object v3, p0, Lbub;->c:Lyj;

    invoke-virtual {v3}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    if-eqz v4, :cond_1

    .line 53
    const-string v2, "notification_pref_key"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    :cond_1
    const-string v2, "ringtone_pref_key"

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v2, "vibrate_pref_key"

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-string v2, "ringtone_type"

    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 59
    invoke-virtual {v1, p2}, Landroid/preference/Preference;->setEnabled(Z)V

    move-object v0, p0

    move-object v2, p7

    move v3, p2

    .line 62
    invoke-virtual/range {v0 .. v6}, Lbub;->a(Landroid/preference/Preference;Landroid/content/SharedPreferences;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    return-object v1
.end method

.method protected f()V
    .locals 3

    .prologue
    .line 70
    iget-object v0, p0, Lbub;->a:Lbtz;

    iget-object v1, p0, Lbub;->b:Landroid/content/res/Resources;

    sget v2, Lh;->kK:I

    .line 71
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 70
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;

    iput-object v0, p0, Lbub;->d:Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;

    .line 73
    const-string v0, "babel_richstatus"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbub;->c:Lyj;

    .line 75
    invoke-virtual {v0}, Lyj;->u()Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    iget-object v0, p0, Lbub;->d:Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;

    const-class v1, Lbur;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;->setFragment(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lbub;->d:Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_name"

    iget-object v2, p0, Lbub;->c:Lyj;

    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-virtual {p0}, Lbub;->i()V

    .line 82
    :goto_0
    return-void

    .line 80
    :cond_0
    iget-object v0, p0, Lbub;->a:Lbtz;

    iget-object v1, p0, Lbub;->d:Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;

    invoke-interface {v0, v1}, Lbtz;->a(Landroid/preference/Preference;)V

    goto :goto_0
.end method

.method protected g()V
    .locals 3

    .prologue
    .line 87
    iget-object v0, p0, Lbub;->a:Lbtz;

    iget-object v1, p0, Lbub;->b:Landroid/content/res/Resources;

    sget v2, Lh;->bu:I

    .line 88
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 87
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 90
    const-class v1, Lbud;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setFragment(Ljava/lang/String;)V

    .line 91
    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_name"

    iget-object v2, p0, Lbub;->c:Lyj;

    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    return-void
.end method
