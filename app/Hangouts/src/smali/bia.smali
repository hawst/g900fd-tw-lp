.class public Lbia;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final g:Z


# direct methods
.method private constructor <init>(Ldvz;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1478
    iget-object v0, p1, Ldvz;->b:Ldvn;

    const-wide/16 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 1479
    iget-object v0, p1, Ldvz;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1480
    iget-object v0, p1, Ldvz;->c:Ljava/lang/Integer;

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    .line 1481
    if-ne v0, v4, :cond_0

    .line 1482
    iput-boolean v4, p0, Lbia;->g:Z

    .line 1487
    :goto_0
    return-void

    .line 1486
    :cond_0
    iput-boolean v3, p0, Lbia;->g:Z

    goto :goto_0
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 1491
    new-instance v0, Ldvz;

    invoke-direct {v0}, Ldvz;-><init>()V

    .line 1492
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldvz;

    .line 1493
    iget-object v1, v0, Ldvz;->b:Ldvn;

    invoke-static {v1}, Lbia;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1494
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldvz;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 1496
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbia;

    invoke-direct {v1, v0}, Lbia;-><init>(Ldvz;)V

    move-object v0, v1

    goto :goto_0
.end method
