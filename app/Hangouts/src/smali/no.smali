.class public final Lno;
.super Landroid/view/ViewGroup;
.source "PG"


# instance fields
.field a:Lfr;

.field private final b:Lns;

.field private final c:Lnt;

.field private final d:Landroid/widget/LinearLayout;

.field private final e:Landroid/graphics/drawable/Drawable;

.field private final f:Landroid/widget/FrameLayout;

.field private final g:Landroid/widget/ImageView;

.field private final h:Landroid/widget/FrameLayout;

.field private final i:Landroid/widget/ImageView;

.field private final j:I

.field private final k:Landroid/database/DataSetObserver;

.field private final l:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private m:Loc;

.field private n:Landroid/widget/PopupWindow$OnDismissListener;

.field private o:Z

.field private p:I

.field private q:Z

.field private r:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lno;-><init>(Landroid/content/Context;B)V

    .line 189
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lno;-><init>(Landroid/content/Context;C)V

    .line 199
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;C)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 209
    invoke-direct {p0, p1, v2, v3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 122
    new-instance v0, Lnp;

    invoke-direct {v0, p0}, Lnp;-><init>(Lno;)V

    iput-object v0, p0, Lno;->k:Landroid/database/DataSetObserver;

    .line 136
    new-instance v0, Lnq;

    invoke-direct {v0, p0}, Lnq;-><init>(Lno;)V

    iput-object v0, p0, Lno;->l:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 170
    iput v1, p0, Lno;->p:I

    .line 211
    sget-object v0, Lle;->g:[I

    invoke-virtual {p1, v2, v0, v3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 214
    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lno;->p:I

    .line 218
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 221
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 223
    invoke-virtual {p0}, Lno;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 224
    sget v2, Lf;->ag:I

    invoke-virtual {v0, v2, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 226
    new-instance v0, Lnt;

    invoke-direct {v0, p0, v3}, Lnt;-><init>(Lno;B)V

    iput-object v0, p0, Lno;->c:Lnt;

    .line 228
    sget v0, Lf;->I:I

    invoke-virtual {p0, v0}, Lno;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lno;->d:Landroid/widget/LinearLayout;

    .line 229
    iget-object v0, p0, Lno;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lno;->e:Landroid/graphics/drawable/Drawable;

    .line 231
    sget v0, Lf;->J:I

    invoke-virtual {p0, v0}, Lno;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lno;->h:Landroid/widget/FrameLayout;

    .line 232
    iget-object v0, p0, Lno;->h:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lno;->c:Lnt;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 233
    iget-object v0, p0, Lno;->h:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lno;->c:Lnt;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 234
    iget-object v0, p0, Lno;->h:Landroid/widget/FrameLayout;

    sget v2, Lf;->N:I

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lno;->i:Landroid/widget/ImageView;

    .line 236
    sget v0, Lf;->K:I

    invoke-virtual {p0, v0}, Lno;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lno;->f:Landroid/widget/FrameLayout;

    .line 237
    iget-object v0, p0, Lno;->f:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lno;->c:Lnt;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 238
    iget-object v0, p0, Lno;->f:Landroid/widget/FrameLayout;

    sget v2, Lf;->N:I

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lno;->g:Landroid/widget/ImageView;

    .line 240
    iget-object v0, p0, Lno;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 242
    new-instance v0, Lns;

    invoke-direct {v0, p0, v3}, Lns;-><init>(Lno;B)V

    iput-object v0, p0, Lno;->b:Lns;

    .line 243
    iget-object v0, p0, Lno;->b:Lns;

    new-instance v1, Lnr;

    invoke-direct {v1, p0}, Lnr;-><init>(Lno;)V

    invoke-virtual {v0, v1}, Lns;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 251
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 252
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v1, v1, 0x2

    sget v2, Lf;->x:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lno;->j:I

    .line 254
    return-void
.end method

.method static synthetic a(Lno;)Lns;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lno;->b:Lns;

    return-object v0
.end method

.method static synthetic a(Lno;I)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lno;->c(I)V

    return-void
.end method

.method static synthetic a(Lno;Z)Z
    .locals 0

    .prologue
    .line 66
    iput-boolean p1, p0, Lno;->o:Z

    return p1
.end method

.method static synthetic b(Lno;)Loc;
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Lno;->c()Loc;

    move-result-object v0

    return-object v0
.end method

.method private c()Loc;
    .locals 2

    .prologue
    .line 482
    iget-object v0, p0, Lno;->m:Loc;

    if-nez v0, :cond_0

    .line 483
    new-instance v0, Loc;

    invoke-virtual {p0}, Lno;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Loc;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lno;->m:Loc;

    .line 484
    iget-object v0, p0, Lno;->m:Loc;

    iget-object v1, p0, Lno;->b:Lns;

    invoke-virtual {v0, v1}, Loc;->a(Landroid/widget/ListAdapter;)V

    .line 485
    iget-object v0, p0, Lno;->m:Loc;

    invoke-virtual {v0, p0}, Loc;->a(Landroid/view/View;)V

    .line 486
    iget-object v0, p0, Lno;->m:Loc;

    invoke-virtual {v0}, Loc;->b()V

    .line 487
    iget-object v0, p0, Lno;->m:Loc;

    iget-object v1, p0, Lno;->c:Lnt;

    invoke-virtual {v0, v1}, Loc;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 488
    iget-object v0, p0, Lno;->m:Loc;

    iget-object v1, p0, Lno;->c:Lnt;

    invoke-virtual {v0, v1}, Loc;->a(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 490
    :cond_0
    iget-object v0, p0, Lno;->m:Loc;

    return-object v0
.end method

.method private c(I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 325
    iget-object v0, p0, Lno;->b:Lns;

    invoke-virtual {v0}, Lns;->e()Lni;

    move-result-object v0

    if-nez v0, :cond_0

    .line 326
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No data model. Did you call #setDataModel?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 329
    :cond_0
    invoke-virtual {p0}, Lno;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v3, p0, Lno;->l:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 331
    iget-object v0, p0, Lno;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 334
    :goto_0
    iget-object v3, p0, Lno;->b:Lns;

    invoke-virtual {v3}, Lns;->c()I

    move-result v4

    .line 335
    if-eqz v0, :cond_5

    move v3, v1

    .line 336
    :goto_1
    const v5, 0x7fffffff

    if-eq p1, v5, :cond_6

    add-int/2addr v3, p1

    if-le v4, v3, :cond_6

    .line 338
    iget-object v3, p0, Lno;->b:Lns;

    invoke-virtual {v3, v1}, Lns;->a(Z)V

    .line 339
    iget-object v3, p0, Lno;->b:Lns;

    add-int/lit8 v4, p1, -0x1

    invoke-virtual {v3, v4}, Lns;->a(I)V

    .line 345
    :goto_2
    invoke-direct {p0}, Lno;->c()Loc;

    move-result-object v3

    .line 346
    invoke-virtual {v3}, Loc;->i()Z

    move-result v4

    if-nez v4, :cond_3

    .line 347
    iget-boolean v4, p0, Lno;->o:Z

    if-nez v4, :cond_1

    if-nez v0, :cond_7

    .line 348
    :cond_1
    iget-object v2, p0, Lno;->b:Lns;

    invoke-virtual {v2, v1, v0}, Lns;->a(ZZ)V

    .line 352
    :goto_3
    iget-object v0, p0, Lno;->b:Lns;

    invoke-virtual {v0}, Lns;->a()I

    move-result v0

    iget v2, p0, Lno;->j:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 353
    invoke-virtual {v3, v0}, Loc;->d(I)V

    .line 354
    invoke-virtual {v3}, Loc;->e()V

    .line 355
    iget-object v0, p0, Lno;->a:Lfr;

    if-eqz v0, :cond_2

    .line 356
    iget-object v0, p0, Lno;->a:Lfr;

    invoke-virtual {v0, v1}, Lfr;->a(Z)V

    .line 358
    :cond_2
    invoke-virtual {v3}, Loc;->k()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {p0}, Lno;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lf;->as:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 361
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 331
    goto :goto_0

    :cond_5
    move v3, v2

    .line 335
    goto :goto_1

    .line 341
    :cond_6
    iget-object v3, p0, Lno;->b:Lns;

    invoke-virtual {v3, v2}, Lns;->a(Z)V

    .line 342
    iget-object v3, p0, Lno;->b:Lns;

    invoke-virtual {v3, p1}, Lns;->a(I)V

    goto :goto_2

    .line 350
    :cond_7
    iget-object v0, p0, Lno;->b:Lns;

    invoke-virtual {v0, v2, v2}, Lns;->a(ZZ)V

    goto :goto_3
.end method

.method static synthetic c(Lno;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 66
    iget-object v0, p0, Lno;->b:Lns;

    invoke-virtual {v0}, Lns;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lno;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    :goto_0
    iget-object v0, p0, Lno;->b:Lns;

    invoke-virtual {v0}, Lns;->c()I

    move-result v0

    iget-object v1, p0, Lno;->b:Lns;

    invoke-virtual {v1}, Lns;->d()I

    move-result v1

    if-eq v0, v4, :cond_0

    if-le v0, v4, :cond_3

    if-lez v1, :cond_3

    :cond_0
    iget-object v0, p0, Lno;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lno;->b:Lns;

    invoke-virtual {v0}, Lns;->b()Landroid/content/pm/ResolveInfo;

    move-result-object v0

    invoke-virtual {p0}, Lno;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lno;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget v2, p0, Lno;->r:I

    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0}, Lno;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lno;->r:I

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lno;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_1
    iget-object v0, p0, Lno;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lno;->d:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lno;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_2
    return-void

    :cond_2
    iget-object v0, p0, Lno;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lno;->h:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lno;->d:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method

.method static synthetic d(Lno;)Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lno;->o:Z

    return v0
.end method

.method static synthetic e(Lno;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lno;->h:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic f(Lno;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lno;->f:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic g(Lno;)I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lno;->p:I

    return v0
.end method

.method static synthetic h(Lno;)Landroid/widget/PopupWindow$OnDismissListener;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lno;->n:Landroid/widget/PopupWindow$OnDismissListener;

    return-object v0
.end method

.method static synthetic i(Lno;)Landroid/database/DataSetObserver;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lno;->k:Landroid/database/DataSetObserver;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 293
    invoke-virtual {p0}, Lno;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 294
    iget-object v1, p0, Lno;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 295
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lno;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 280
    return-void
.end method

.method public a(Lfr;)V
    .locals 0

    .prologue
    .line 302
    iput-object p1, p0, Lno;->a:Lfr;

    .line 303
    return-void
.end method

.method public a(Lni;)V
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lno;->b:Lns;

    invoke-virtual {v0, p1}, Lns;->a(Lni;)V

    .line 261
    invoke-virtual {p0}, Lno;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    invoke-virtual {p0}, Lno;->a()Z

    .line 263
    invoke-virtual {p0}, Lno;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lno;->q:Z

    if-nez v0, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 263
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lno;->o:Z

    iget v0, p0, Lno;->p:I

    invoke-direct {p0, v0}, Lno;->c(I)V

    goto :goto_0
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 369
    invoke-virtual {p0}, Lno;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    invoke-direct {p0}, Lno;->c()Loc;

    move-result-object v0

    invoke-virtual {v0}, Loc;->f()V

    .line 371
    invoke-virtual {p0}, Lno;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 372
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 373
    iget-object v1, p0, Lno;->l:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 376
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 473
    iput p1, p0, Lno;->r:I

    .line 474
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 385
    invoke-direct {p0}, Lno;->c()Loc;

    move-result-object v0

    invoke-virtual {v0}, Loc;->i()Z

    move-result v0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 390
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 391
    iget-object v0, p0, Lno;->b:Lns;

    invoke-virtual {v0}, Lns;->e()Lni;

    move-result-object v0

    .line 392
    if-eqz v0, :cond_0

    .line 393
    iget-object v1, p0, Lno;->k:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lni;->registerObserver(Ljava/lang/Object;)V

    .line 395
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lno;->q:Z

    .line 396
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 400
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 401
    iget-object v0, p0, Lno;->b:Lns;

    invoke-virtual {v0}, Lns;->e()Lni;

    move-result-object v0

    .line 402
    if-eqz v0, :cond_0

    .line 403
    iget-object v1, p0, Lno;->k:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lni;->unregisterObserver(Ljava/lang/Object;)V

    .line 405
    :cond_0
    invoke-virtual {p0}, Lno;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 406
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 407
    iget-object v1, p0, Lno;->l:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 409
    :cond_1
    invoke-virtual {p0}, Lno;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 410
    invoke-virtual {p0}, Lno;->a()Z

    .line 412
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lno;->q:Z

    .line 413
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 431
    iget-object v0, p0, Lno;->d:Landroid/widget/LinearLayout;

    sub-int v1, p4, p2

    sub-int v2, p5, p3

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/widget/LinearLayout;->layout(IIII)V

    .line 432
    invoke-virtual {p0}, Lno;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 433
    invoke-virtual {p0}, Lno;->a()Z

    .line 435
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 417
    iget-object v0, p0, Lno;->d:Landroid/widget/LinearLayout;

    .line 421
    iget-object v1, p0, Lno;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    .line 422
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 425
    :cond_0
    invoke-virtual {p0, v0, p1, p2}, Lno;->measureChild(Landroid/view/View;II)V

    .line 426
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lno;->setMeasuredDimension(II)V

    .line 427
    return-void
.end method
