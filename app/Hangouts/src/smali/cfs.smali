.class public final enum Lcfs;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcfs;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lcfs;

.field public static final enum B:Lcfs;

.field public static final enum C:Lcfs;

.field public static final enum D:Lcfs;

.field public static final enum E:Lcfs;

.field public static final enum F:Lcfs;

.field public static final enum G:Lcfs;

.field public static final enum H:Lcfs;

.field public static final enum I:Lcfs;

.field public static final enum J:Lcfs;

.field public static final enum K:Lcfs;

.field public static final enum L:Lcfs;

.field public static final enum M:Lcfs;

.field public static final enum N:Lcfs;

.field public static final enum O:Lcfs;

.field public static final enum P:Lcfs;

.field public static final enum Q:Lcfs;

.field public static final enum R:Lcfs;

.field public static final enum S:Lcfs;

.field public static T:Ljava/lang/String;

.field public static U:Ljava/lang/String;

.field private static final synthetic W:[Lcfs;

.field public static final enum a:Lcfs;

.field public static final enum b:Lcfs;

.field public static final enum c:Lcfs;

.field public static final enum d:Lcfs;

.field public static final enum e:Lcfs;

.field public static final enum f:Lcfs;

.field public static final enum g:Lcfs;

.field public static final enum h:Lcfs;

.field public static final enum i:Lcfs;

.field public static final enum j:Lcfs;

.field public static final enum k:Lcfs;

.field public static final enum l:Lcfs;

.field public static final enum m:Lcfs;

.field public static final enum n:Lcfs;

.field public static final enum o:Lcfs;

.field public static final enum p:Lcfs;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum q:Lcfs;

.field public static final enum r:Lcfs;

.field public static final enum s:Lcfs;

.field public static final enum t:Lcfs;

.field public static final enum u:Lcfs;

.field public static final enum v:Lcfs;

.field public static final enum w:Lcfs;

.field public static final enum x:Lcfs;

.field public static final enum y:Lcfs;

.field public static final enum z:Lcfs;


# instance fields
.field private final V:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcfs;

    const-string v1, "SUCCESS"

    const-string v2, "Ok"

    invoke-direct {v0, v1, v4, v2}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->a:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "BAD_AUTHENTICATION"

    const-string v2, "BadAuthentication"

    invoke-direct {v0, v1, v5, v2}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->b:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "NEEDS_2F"

    const-string v2, "InvalidSecondFactor"

    invoke-direct {v0, v1, v6, v2}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->c:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "NOT_VERIFIED"

    const-string v2, "NotVerified"

    invoke-direct {v0, v1, v7, v2}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->d:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "TERMS_NOT_AGREED"

    const-string v2, "TermsNotAgreed"

    invoke-direct {v0, v1, v8, v2}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->e:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x5

    const-string v3, "Unknown"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->f:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "UNKNOWN_ERROR"

    const/4 v2, 0x6

    const-string v3, "UNKNOWN_ERR"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->g:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "ACCOUNT_DELETED"

    const/4 v2, 0x7

    const-string v3, "AccountDeleted"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->h:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "ACCOUNT_DISABLED"

    const/16 v2, 0x8

    const-string v3, "AccountDisabled"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->i:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "SERVICE_DISABLED"

    const/16 v2, 0x9

    const-string v3, "ServiceDisabled"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->j:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "SERVICE_UNAVAILABLE"

    const/16 v2, 0xa

    const-string v3, "ServiceUnavailable"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->k:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "CAPTCHA"

    const/16 v2, 0xb

    const-string v3, "CaptchaRequired"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->l:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "NETWORK_ERROR"

    const/16 v2, 0xc

    const-string v3, "NetworkError"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->m:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "USER_CANCEL"

    const/16 v2, 0xd

    const-string v3, "UserCancel"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->n:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "PERMISSION_DENIED"

    const/16 v2, 0xe

    const-string v3, "PermissionDenied"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->o:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "DEVICE_MANAGEMENT_REQUIRED"

    const/16 v2, 0xf

    const-string v3, "DeviceManagementRequiredOrSyncDisabled"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->p:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "DM_INTERNAL_ERROR"

    const/16 v2, 0x10

    const-string v3, "DeviceManagementInternalError"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->q:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "DM_SYNC_DISABLED"

    const/16 v2, 0x11

    const-string v3, "DeviceManagementSyncDisabled"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->r:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "DM_ADMIN_BLOCKED"

    const/16 v2, 0x12

    const-string v3, "DeviceManagementAdminBlocked"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->s:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "DM_ADMIN_PENDING_APPROVAL"

    const/16 v2, 0x13

    const-string v3, "DeviceManagementAdminPendingApproval"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->t:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "DM_STALE_SYNC_REQUIRED"

    const/16 v2, 0x14

    const-string v3, "DeviceManagementStaleSyncRequired"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->u:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "DM_DEACTIVATED"

    const/16 v2, 0x15

    const-string v3, "DeviceManagementDeactivated"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->v:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "DM_REQUIRED"

    const/16 v2, 0x16

    const-string v3, "DeviceManagementRequired"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->w:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "CLIENT_LOGIN_DISABLED"

    const/16 v2, 0x17

    const-string v3, "ClientLoginDisabled"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->x:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "NEED_PERMISSION"

    const/16 v2, 0x18

    const-string v3, "NeedPermission"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->y:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "BAD_PASSWORD"

    const/16 v2, 0x19

    const-string v3, "WeakPassword"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->z:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "ALREADY_HAS_GMAIL"

    const/16 v2, 0x1a

    const-string v3, "ALREADY_HAS_GMAIL"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->A:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "BAD_REQUEST"

    const/16 v2, 0x1b

    const-string v3, "BadRequest"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->B:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "BAD_USERNAME"

    const/16 v2, 0x1c

    const-string v3, "BadUsername"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->C:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "LOGIN_FAIL"

    const/16 v2, 0x1d

    const-string v3, "LoginFail"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->D:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "NOT_LOGGED_IN"

    const/16 v2, 0x1e

    const-string v3, "NotLoggedIn"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->E:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "NO_GMAIL"

    const/16 v2, 0x1f

    const-string v3, "NoGmail"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->F:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "REQUEST_DENIED"

    const/16 v2, 0x20

    const-string v3, "RequestDenied"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->G:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "SERVER_ERROR"

    const/16 v2, 0x21

    const-string v3, "ServerError"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->H:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "USERNAME_UNAVAILABLE"

    const/16 v2, 0x22

    const-string v3, "UsernameUnavailable"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->I:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "DELETED_GMAIL"

    const/16 v2, 0x23

    const-string v3, "DeletedGmail"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->J:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "SOCKET_TIMEOUT"

    const/16 v2, 0x24

    const-string v3, "SocketTimeout"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->K:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "EXISTING_USERNAME"

    const/16 v2, 0x25

    const-string v3, "ExistingUsername"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->L:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "NEEDS_BROWSER"

    const/16 v2, 0x26

    const-string v3, "NeedsBrowser"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->M:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "GPLUS_OTHER"

    const/16 v2, 0x27

    const-string v3, "GPlusOther"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->N:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "GPLUS_NICKNAME"

    const/16 v2, 0x28

    const-string v3, "GPlusNickname"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->O:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "GPLUS_INVALID_CHAR"

    const/16 v2, 0x29

    const-string v3, "GPlusInvalidChar"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->P:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "GPLUS_INTERSTITIAL"

    const/16 v2, 0x2a

    const-string v3, "GPlusInterstitial"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->Q:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "GPLUS_PROFILE_ERROR"

    const/16 v2, 0x2b

    const-string v3, "ProfileUpgradeError"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->R:Lcfs;

    new-instance v0, Lcfs;

    const-string v1, "INVALID_SCOPE"

    const/16 v2, 0x2c

    const-string v3, "INVALID_SCOPE"

    invoke-direct {v0, v1, v2, v3}, Lcfs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcfs;->S:Lcfs;

    const/16 v0, 0x2d

    new-array v0, v0, [Lcfs;

    sget-object v1, Lcfs;->a:Lcfs;

    aput-object v1, v0, v4

    sget-object v1, Lcfs;->b:Lcfs;

    aput-object v1, v0, v5

    sget-object v1, Lcfs;->c:Lcfs;

    aput-object v1, v0, v6

    sget-object v1, Lcfs;->d:Lcfs;

    aput-object v1, v0, v7

    sget-object v1, Lcfs;->e:Lcfs;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcfs;->f:Lcfs;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcfs;->g:Lcfs;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcfs;->h:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcfs;->i:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcfs;->j:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcfs;->k:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcfs;->l:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcfs;->m:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcfs;->n:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcfs;->o:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcfs;->p:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcfs;->q:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcfs;->r:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcfs;->s:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcfs;->t:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcfs;->u:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcfs;->v:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcfs;->w:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcfs;->x:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcfs;->y:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcfs;->z:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcfs;->A:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcfs;->B:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcfs;->C:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcfs;->D:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcfs;->E:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcfs;->F:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcfs;->G:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcfs;->H:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcfs;->I:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcfs;->J:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcfs;->K:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcfs;->L:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcfs;->M:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcfs;->N:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcfs;->O:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcfs;->P:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcfs;->Q:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcfs;->R:Lcfs;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcfs;->S:Lcfs;

    aput-object v2, v0, v1

    sput-object v0, Lcfs;->W:[Lcfs;

    const-string v0, "Error"

    sput-object v0, Lcfs;->T:Ljava/lang/String;

    const-string v0, "status"

    sput-object v0, Lcfs;->U:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcfs;->V:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcfs;
    .locals 1

    const-class v0, Lcfs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcfs;

    return-object v0
.end method

.method public static values()[Lcfs;
    .locals 1

    sget-object v0, Lcfs;->W:[Lcfs;

    invoke-virtual {v0}, [Lcfs;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcfs;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcfs;->V:Ljava/lang/String;

    return-object v0
.end method
