.class public final Ldoh;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldoh;


# instance fields
.field public A:Ljava/lang/Integer;

.field public B:Ljava/lang/Integer;

.field public C:Ljava/lang/Integer;

.field public D:Ljava/lang/Integer;

.field public E:Ljava/lang/Integer;

.field public F:Ljava/lang/Integer;

.field public G:Ljava/lang/String;

.field public H:Ljava/lang/String;

.field public I:Ljava/lang/Integer;

.field public J:Ljava/lang/Integer;

.field public K:Ljava/lang/Integer;

.field public L:Ljava/lang/Integer;

.field public M:Ljava/lang/Float;

.field public N:[Ljava/lang/Integer;

.field public O:[Ldoi;

.field public P:Ljava/lang/Integer;

.field public Q:Ljava/lang/Integer;

.field public R:Ljava/lang/Integer;

.field public S:Ljava/lang/Integer;

.field public T:Ljava/lang/Boolean;

.field public U:Ljava/lang/Boolean;

.field public V:Ljava/lang/Integer;

.field public W:Ljava/lang/Integer;

.field public X:Ljava/lang/Float;

.field public Y:Ljava/lang/Integer;

.field public Z:Ljava/lang/Integer;

.field public aa:Ljava/lang/Integer;

.field public ab:Ljava/lang/Integer;

.field public ac:Ljava/lang/Integer;

.field public ad:Ljava/lang/Integer;

.field public ae:Ljava/lang/Integer;

.field public af:Ljava/lang/Integer;

.field public ag:Ljava/lang/Integer;

.field public ah:Ljava/lang/Integer;

.field public ai:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/Long;

.field public i:Ljava/lang/Integer;

.field public j:Ljava/lang/Long;

.field public k:Ljava/lang/Integer;

.field public l:Ljava/lang/Integer;

.field public m:Ljava/lang/Integer;

.field public n:Ljava/lang/Integer;

.field public o:Ljava/lang/Integer;

.field public p:Ljava/lang/Integer;

.field public q:Ljava/lang/Integer;

.field public r:Ljava/lang/Integer;

.field public s:Ljava/lang/Integer;

.field public t:Ljava/lang/Float;

.field public u:Ljava/lang/Float;

.field public v:Ljava/lang/Float;

.field public w:Ljava/lang/Float;

.field public x:Ljava/lang/Integer;

.field public y:Ljava/lang/Integer;

.field public z:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 478
    const/4 v0, 0x0

    new-array v0, v0, [Ldoh;

    sput-object v0, Ldoh;->a:[Ldoh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 479
    invoke-direct {p0}, Lepn;-><init>()V

    .line 595
    iput-object v0, p0, Ldoh;->b:Ljava/lang/Integer;

    .line 616
    iput-object v0, p0, Ldoh;->l:Ljava/lang/Integer;

    .line 673
    sget-object v0, Lept;->m:[Ljava/lang/Integer;

    iput-object v0, p0, Ldoh;->N:[Ljava/lang/Integer;

    .line 676
    sget-object v0, Ldoi;->a:[Ldoi;

    iput-object v0, p0, Ldoh;->O:[Ldoi;

    .line 479
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 893
    const/16 v0, 0x8

    iget-object v1, p0, Ldoh;->b:Ljava/lang/Integer;

    .line 895
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 896
    const/16 v1, 0x9

    iget-object v2, p0, Ldoh;->c:Ljava/lang/Integer;

    .line 897
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 898
    const/16 v1, 0xa

    iget-object v2, p0, Ldoh;->d:Ljava/lang/Integer;

    .line 899
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 900
    const/16 v1, 0xb

    iget-object v2, p0, Ldoh;->e:Ljava/lang/Integer;

    .line 901
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 902
    const/16 v1, 0xc

    iget-object v2, p0, Ldoh;->f:Ljava/lang/Integer;

    .line 903
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 904
    const/16 v1, 0xd

    iget-object v2, p0, Ldoh;->g:Ljava/lang/Integer;

    .line 905
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 906
    const/16 v1, 0xe

    iget-object v2, p0, Ldoh;->h:Ljava/lang/Long;

    .line 907
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 908
    const/16 v1, 0xf

    iget-object v2, p0, Ldoh;->i:Ljava/lang/Integer;

    .line 909
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 910
    const/16 v1, 0x10

    iget-object v2, p0, Ldoh;->j:Ljava/lang/Long;

    .line 911
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 912
    const/16 v1, 0x11

    iget-object v2, p0, Ldoh;->k:Ljava/lang/Integer;

    .line 913
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 914
    iget-object v1, p0, Ldoh;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 915
    const/16 v1, 0x1f

    iget-object v2, p0, Ldoh;->l:Ljava/lang/Integer;

    .line 916
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 918
    :cond_0
    iget-object v1, p0, Ldoh;->m:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 919
    const/16 v1, 0x20

    iget-object v2, p0, Ldoh;->m:Ljava/lang/Integer;

    .line 920
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 922
    :cond_1
    iget-object v1, p0, Ldoh;->n:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 923
    const/16 v1, 0x21

    iget-object v2, p0, Ldoh;->n:Ljava/lang/Integer;

    .line 924
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 926
    :cond_2
    iget-object v1, p0, Ldoh;->o:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 927
    const/16 v1, 0x22

    iget-object v2, p0, Ldoh;->o:Ljava/lang/Integer;

    .line 928
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 930
    :cond_3
    iget-object v1, p0, Ldoh;->p:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 931
    const/16 v1, 0x23

    iget-object v2, p0, Ldoh;->p:Ljava/lang/Integer;

    .line 932
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 934
    :cond_4
    iget-object v1, p0, Ldoh;->q:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 935
    const/16 v1, 0x24

    iget-object v2, p0, Ldoh;->q:Ljava/lang/Integer;

    .line 936
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 938
    :cond_5
    iget-object v1, p0, Ldoh;->r:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 939
    const/16 v1, 0x25

    iget-object v2, p0, Ldoh;->r:Ljava/lang/Integer;

    .line 940
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 942
    :cond_6
    iget-object v1, p0, Ldoh;->s:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 943
    const/16 v1, 0x26

    iget-object v2, p0, Ldoh;->s:Ljava/lang/Integer;

    .line 944
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 946
    :cond_7
    iget-object v1, p0, Ldoh;->x:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 947
    const/16 v1, 0x2c

    iget-object v2, p0, Ldoh;->x:Ljava/lang/Integer;

    .line 948
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 950
    :cond_8
    iget-object v1, p0, Ldoh;->y:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 951
    const/16 v1, 0x2d

    iget-object v2, p0, Ldoh;->y:Ljava/lang/Integer;

    .line 952
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 954
    :cond_9
    iget-object v1, p0, Ldoh;->z:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    .line 955
    const/16 v1, 0x2e

    iget-object v2, p0, Ldoh;->z:Ljava/lang/Integer;

    .line 956
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 958
    :cond_a
    iget-object v1, p0, Ldoh;->A:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 959
    const/16 v1, 0x2f

    iget-object v2, p0, Ldoh;->A:Ljava/lang/Integer;

    .line 960
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 962
    :cond_b
    iget-object v1, p0, Ldoh;->B:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 963
    const/16 v1, 0x30

    iget-object v2, p0, Ldoh;->B:Ljava/lang/Integer;

    .line 964
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 966
    :cond_c
    iget-object v1, p0, Ldoh;->C:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 967
    const/16 v1, 0x31

    iget-object v2, p0, Ldoh;->C:Ljava/lang/Integer;

    .line 968
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 970
    :cond_d
    iget-object v1, p0, Ldoh;->D:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    .line 971
    const/16 v1, 0x32

    iget-object v2, p0, Ldoh;->D:Ljava/lang/Integer;

    .line 972
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 974
    :cond_e
    iget-object v1, p0, Ldoh;->G:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 975
    const/16 v1, 0x33

    iget-object v2, p0, Ldoh;->G:Ljava/lang/String;

    .line 976
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 978
    :cond_f
    iget-object v1, p0, Ldoh;->I:Ljava/lang/Integer;

    if-eqz v1, :cond_10

    .line 979
    const/16 v1, 0x35

    iget-object v2, p0, Ldoh;->I:Ljava/lang/Integer;

    .line 980
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 982
    :cond_10
    iget-object v1, p0, Ldoh;->J:Ljava/lang/Integer;

    if-eqz v1, :cond_11

    .line 983
    const/16 v1, 0x36

    iget-object v2, p0, Ldoh;->J:Ljava/lang/Integer;

    .line 984
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 986
    :cond_11
    iget-object v1, p0, Ldoh;->K:Ljava/lang/Integer;

    if-eqz v1, :cond_12

    .line 987
    const/16 v1, 0x37

    iget-object v2, p0, Ldoh;->K:Ljava/lang/Integer;

    .line 988
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 990
    :cond_12
    iget-object v1, p0, Ldoh;->L:Ljava/lang/Integer;

    if-eqz v1, :cond_13

    .line 991
    const/16 v1, 0x38

    iget-object v2, p0, Ldoh;->L:Ljava/lang/Integer;

    .line 992
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 994
    :cond_13
    iget-object v1, p0, Ldoh;->t:Ljava/lang/Float;

    if-eqz v1, :cond_14

    .line 995
    const/16 v1, 0x39

    iget-object v2, p0, Ldoh;->t:Ljava/lang/Float;

    .line 996
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 998
    :cond_14
    iget-object v1, p0, Ldoh;->v:Ljava/lang/Float;

    if-eqz v1, :cond_15

    .line 999
    const/16 v1, 0x3a

    iget-object v2, p0, Ldoh;->v:Ljava/lang/Float;

    .line 1000
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1002
    :cond_15
    iget-object v1, p0, Ldoh;->N:[Ljava/lang/Integer;

    if-eqz v1, :cond_16

    iget-object v1, p0, Ldoh;->N:[Ljava/lang/Integer;

    array-length v1, v1

    if-lez v1, :cond_16

    .line 1003
    iget-object v1, p0, Ldoh;->N:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    .line 1004
    add-int/2addr v0, v1

    .line 1005
    iget-object v1, p0, Ldoh;->N:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 1007
    :cond_16
    iget-object v1, p0, Ldoh;->O:[Ldoi;

    if-eqz v1, :cond_18

    .line 1008
    iget-object v2, p0, Ldoh;->O:[Ldoi;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_18

    aget-object v4, v2, v1

    .line 1009
    if-eqz v4, :cond_17

    .line 1010
    const/16 v5, 0x3d

    .line 1011
    invoke-static {v5, v4}, Lepl;->c(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1008
    :cond_17
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1015
    :cond_18
    iget-object v1, p0, Ldoh;->P:Ljava/lang/Integer;

    if-eqz v1, :cond_19

    .line 1016
    const/16 v1, 0x40

    iget-object v2, p0, Ldoh;->P:Ljava/lang/Integer;

    .line 1017
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1019
    :cond_19
    iget-object v1, p0, Ldoh;->Q:Ljava/lang/Integer;

    if-eqz v1, :cond_1a

    .line 1020
    const/16 v1, 0x41

    iget-object v2, p0, Ldoh;->Q:Ljava/lang/Integer;

    .line 1021
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1023
    :cond_1a
    iget-object v1, p0, Ldoh;->T:Ljava/lang/Boolean;

    if-eqz v1, :cond_1b

    .line 1024
    const/16 v1, 0x42

    iget-object v2, p0, Ldoh;->T:Ljava/lang/Boolean;

    .line 1025
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1027
    :cond_1b
    iget-object v1, p0, Ldoh;->U:Ljava/lang/Boolean;

    if-eqz v1, :cond_1c

    .line 1028
    const/16 v1, 0x43

    iget-object v2, p0, Ldoh;->U:Ljava/lang/Boolean;

    .line 1029
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1031
    :cond_1c
    iget-object v1, p0, Ldoh;->M:Ljava/lang/Float;

    if-eqz v1, :cond_1d

    .line 1032
    const/16 v1, 0x46

    iget-object v2, p0, Ldoh;->M:Ljava/lang/Float;

    .line 1033
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1035
    :cond_1d
    iget-object v1, p0, Ldoh;->V:Ljava/lang/Integer;

    if-eqz v1, :cond_1e

    .line 1036
    const/16 v1, 0x48

    iget-object v2, p0, Ldoh;->V:Ljava/lang/Integer;

    .line 1037
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1039
    :cond_1e
    iget-object v1, p0, Ldoh;->X:Ljava/lang/Float;

    if-eqz v1, :cond_1f

    .line 1040
    const/16 v1, 0x4b

    iget-object v2, p0, Ldoh;->X:Ljava/lang/Float;

    .line 1041
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1043
    :cond_1f
    iget-object v1, p0, Ldoh;->Y:Ljava/lang/Integer;

    if-eqz v1, :cond_20

    .line 1044
    const/16 v1, 0x51

    iget-object v2, p0, Ldoh;->Y:Ljava/lang/Integer;

    .line 1045
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1047
    :cond_20
    iget-object v1, p0, Ldoh;->Z:Ljava/lang/Integer;

    if-eqz v1, :cond_21

    .line 1048
    const/16 v1, 0x52

    iget-object v2, p0, Ldoh;->Z:Ljava/lang/Integer;

    .line 1049
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1051
    :cond_21
    iget-object v1, p0, Ldoh;->aa:Ljava/lang/Integer;

    if-eqz v1, :cond_22

    .line 1052
    const/16 v1, 0x53

    iget-object v2, p0, Ldoh;->aa:Ljava/lang/Integer;

    .line 1053
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1055
    :cond_22
    iget-object v1, p0, Ldoh;->ab:Ljava/lang/Integer;

    if-eqz v1, :cond_23

    .line 1056
    const/16 v1, 0x54

    iget-object v2, p0, Ldoh;->ab:Ljava/lang/Integer;

    .line 1057
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1059
    :cond_23
    iget-object v1, p0, Ldoh;->R:Ljava/lang/Integer;

    if-eqz v1, :cond_24

    .line 1060
    const/16 v1, 0x55

    iget-object v2, p0, Ldoh;->R:Ljava/lang/Integer;

    .line 1061
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1063
    :cond_24
    iget-object v1, p0, Ldoh;->S:Ljava/lang/Integer;

    if-eqz v1, :cond_25

    .line 1064
    const/16 v1, 0x56

    iget-object v2, p0, Ldoh;->S:Ljava/lang/Integer;

    .line 1065
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1067
    :cond_25
    iget-object v1, p0, Ldoh;->F:Ljava/lang/Integer;

    if-eqz v1, :cond_26

    .line 1068
    const/16 v1, 0x57

    iget-object v2, p0, Ldoh;->F:Ljava/lang/Integer;

    .line 1069
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1071
    :cond_26
    iget-object v1, p0, Ldoh;->ac:Ljava/lang/Integer;

    if-eqz v1, :cond_27

    .line 1072
    const/16 v1, 0x58

    iget-object v2, p0, Ldoh;->ac:Ljava/lang/Integer;

    .line 1073
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1075
    :cond_27
    iget-object v1, p0, Ldoh;->ad:Ljava/lang/Integer;

    if-eqz v1, :cond_28

    .line 1076
    const/16 v1, 0x59

    iget-object v2, p0, Ldoh;->ad:Ljava/lang/Integer;

    .line 1077
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1079
    :cond_28
    iget-object v1, p0, Ldoh;->ae:Ljava/lang/Integer;

    if-eqz v1, :cond_29

    .line 1080
    const/16 v1, 0x5a

    iget-object v2, p0, Ldoh;->ae:Ljava/lang/Integer;

    .line 1081
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1083
    :cond_29
    iget-object v1, p0, Ldoh;->af:Ljava/lang/Integer;

    if-eqz v1, :cond_2a

    .line 1084
    const/16 v1, 0x5b

    iget-object v2, p0, Ldoh;->af:Ljava/lang/Integer;

    .line 1085
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1087
    :cond_2a
    iget-object v1, p0, Ldoh;->ag:Ljava/lang/Integer;

    if-eqz v1, :cond_2b

    .line 1088
    const/16 v1, 0x5c

    iget-object v2, p0, Ldoh;->ag:Ljava/lang/Integer;

    .line 1089
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1091
    :cond_2b
    iget-object v1, p0, Ldoh;->ah:Ljava/lang/Integer;

    if-eqz v1, :cond_2c

    .line 1092
    const/16 v1, 0x5d

    iget-object v2, p0, Ldoh;->ah:Ljava/lang/Integer;

    .line 1093
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1095
    :cond_2c
    iget-object v1, p0, Ldoh;->u:Ljava/lang/Float;

    if-eqz v1, :cond_2d

    .line 1096
    const/16 v1, 0x5e

    iget-object v2, p0, Ldoh;->u:Ljava/lang/Float;

    .line 1097
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1099
    :cond_2d
    iget-object v1, p0, Ldoh;->w:Ljava/lang/Float;

    if-eqz v1, :cond_2e

    .line 1100
    const/16 v1, 0x5f

    iget-object v2, p0, Ldoh;->w:Ljava/lang/Float;

    .line 1101
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1103
    :cond_2e
    iget-object v1, p0, Ldoh;->ai:Ljava/lang/Integer;

    if-eqz v1, :cond_2f

    .line 1104
    const/16 v1, 0x60

    iget-object v2, p0, Ldoh;->ai:Ljava/lang/Integer;

    .line 1105
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1107
    :cond_2f
    iget-object v1, p0, Ldoh;->H:Ljava/lang/String;

    if-eqz v1, :cond_30

    .line 1108
    const/16 v1, 0x65

    iget-object v2, p0, Ldoh;->H:Ljava/lang/String;

    .line 1109
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1111
    :cond_30
    iget-object v1, p0, Ldoh;->W:Ljava/lang/Integer;

    if-eqz v1, :cond_31

    .line 1112
    const/16 v1, 0x66

    iget-object v2, p0, Ldoh;->W:Ljava/lang/Integer;

    .line 1113
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1115
    :cond_31
    iget-object v1, p0, Ldoh;->E:Ljava/lang/Integer;

    if-eqz v1, :cond_32

    .line 1116
    const/16 v1, 0x67

    iget-object v2, p0, Ldoh;->E:Ljava/lang/Integer;

    .line 1117
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1119
    :cond_32
    iget-object v1, p0, Ldoh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1120
    iput v0, p0, Ldoh;->cachedSize:I

    .line 1121
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 7

    .prologue
    const/16 v6, 0x3d

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 475
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldoh;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldoh;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldoh;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->g:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldoh;->h:Ljava/lang/Long;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldoh;->j:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_4

    if-eq v0, v4, :cond_4

    if-ne v0, v5, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_5
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->h()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->m:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->o:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->p:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->q:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->r:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->s:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->x:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->y:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->z:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_16
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->A:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_17
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->B:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->C:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_19
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->D:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_1a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldoh;->G:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_1b
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->I:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_1c
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->J:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_1d
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->K:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_1e
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->L:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_1f
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Ldoh;->t:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_20
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Ldoh;->v:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_21
    const/16 v0, 0x1e5

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldoh;->N:[Ljava/lang/Integer;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Integer;

    iget-object v3, p0, Ldoh;->N:[Ljava/lang/Integer;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldoh;->N:[Ljava/lang/Integer;

    :goto_1
    iget-object v2, p0, Ldoh;->N:[Ljava/lang/Integer;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ldoh;->N:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->h()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    iget-object v2, p0, Ldoh;->N:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->h()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_22
    const/16 v0, 0x1eb

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldoh;->O:[Ldoi;

    if-nez v0, :cond_8

    move v0, v1

    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Ldoi;

    iget-object v3, p0, Ldoh;->O:[Ldoi;

    if-eqz v3, :cond_7

    iget-object v3, p0, Ldoh;->O:[Ldoi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Ldoh;->O:[Ldoi;

    :goto_3
    iget-object v2, p0, Ldoh;->O:[Ldoi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Ldoh;->O:[Ldoi;

    new-instance v3, Ldoi;

    invoke-direct {v3}, Ldoi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldoh;->O:[Ldoi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2, v6}, Lepk;->a(Lepr;I)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_8
    iget-object v0, p0, Ldoh;->O:[Ldoi;

    array-length v0, v0

    goto :goto_2

    :cond_9
    iget-object v2, p0, Ldoh;->O:[Ldoi;

    new-instance v3, Ldoi;

    invoke-direct {v3}, Ldoi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldoh;->O:[Ldoi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0, v6}, Lepk;->a(Lepr;I)V

    goto/16 :goto_0

    :sswitch_23
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->P:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_24
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->Q:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_25
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldoh;->T:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_26
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldoh;->U:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_27
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Ldoh;->M:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_28
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->V:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_29
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Ldoh;->X:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_2a
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->Y:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_2b
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->Z:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_2c
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->aa:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_2d
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->ab:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_2e
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->R:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_2f
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->S:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_30
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->F:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_31
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->ac:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_32
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->ad:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_33
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->ae:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_34
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->af:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_35
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->ag:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_36
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->ah:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_37
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Ldoh;->u:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_38
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Ldoh;->w:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_39
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->ai:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_3a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldoh;->H:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_3b
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->W:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_3c
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoh;->E:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x40 -> :sswitch_1
        0x48 -> :sswitch_2
        0x50 -> :sswitch_3
        0x58 -> :sswitch_4
        0x60 -> :sswitch_5
        0x68 -> :sswitch_6
        0x70 -> :sswitch_7
        0x78 -> :sswitch_8
        0x80 -> :sswitch_9
        0x88 -> :sswitch_a
        0xf8 -> :sswitch_b
        0x105 -> :sswitch_c
        0x108 -> :sswitch_d
        0x110 -> :sswitch_e
        0x118 -> :sswitch_f
        0x120 -> :sswitch_10
        0x128 -> :sswitch_11
        0x130 -> :sswitch_12
        0x160 -> :sswitch_13
        0x168 -> :sswitch_14
        0x170 -> :sswitch_15
        0x178 -> :sswitch_16
        0x180 -> :sswitch_17
        0x188 -> :sswitch_18
        0x190 -> :sswitch_19
        0x19a -> :sswitch_1a
        0x1a8 -> :sswitch_1b
        0x1b0 -> :sswitch_1c
        0x1b8 -> :sswitch_1d
        0x1c0 -> :sswitch_1e
        0x1cd -> :sswitch_1f
        0x1d5 -> :sswitch_20
        0x1e5 -> :sswitch_21
        0x1eb -> :sswitch_22
        0x200 -> :sswitch_23
        0x208 -> :sswitch_24
        0x210 -> :sswitch_25
        0x218 -> :sswitch_26
        0x235 -> :sswitch_27
        0x240 -> :sswitch_28
        0x25d -> :sswitch_29
        0x288 -> :sswitch_2a
        0x290 -> :sswitch_2b
        0x298 -> :sswitch_2c
        0x2a0 -> :sswitch_2d
        0x2a8 -> :sswitch_2e
        0x2b0 -> :sswitch_2f
        0x2b8 -> :sswitch_30
        0x2c0 -> :sswitch_31
        0x2c8 -> :sswitch_32
        0x2d0 -> :sswitch_33
        0x2d8 -> :sswitch_34
        0x2e0 -> :sswitch_35
        0x2e8 -> :sswitch_36
        0x2f5 -> :sswitch_37
        0x2fd -> :sswitch_38
        0x300 -> :sswitch_39
        0x32a -> :sswitch_3a
        0x330 -> :sswitch_3b
        0x338 -> :sswitch_3c
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 721
    const/16 v1, 0x8

    iget-object v2, p0, Ldoh;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 722
    const/16 v1, 0x9

    iget-object v2, p0, Ldoh;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 723
    const/16 v1, 0xa

    iget-object v2, p0, Ldoh;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 724
    const/16 v1, 0xb

    iget-object v2, p0, Ldoh;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 725
    const/16 v1, 0xc

    iget-object v2, p0, Ldoh;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 726
    const/16 v1, 0xd

    iget-object v2, p0, Ldoh;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 727
    const/16 v1, 0xe

    iget-object v2, p0, Ldoh;->h:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->b(IJ)V

    .line 728
    const/16 v1, 0xf

    iget-object v2, p0, Ldoh;->i:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 729
    const/16 v1, 0x10

    iget-object v2, p0, Ldoh;->j:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->b(IJ)V

    .line 730
    const/16 v1, 0x11

    iget-object v2, p0, Ldoh;->k:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 731
    iget-object v1, p0, Ldoh;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 732
    const/16 v1, 0x1f

    iget-object v2, p0, Ldoh;->l:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 734
    :cond_0
    iget-object v1, p0, Ldoh;->m:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 735
    const/16 v1, 0x20

    iget-object v2, p0, Ldoh;->m:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->b(II)V

    .line 737
    :cond_1
    iget-object v1, p0, Ldoh;->n:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 738
    const/16 v1, 0x21

    iget-object v2, p0, Ldoh;->n:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 740
    :cond_2
    iget-object v1, p0, Ldoh;->o:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 741
    const/16 v1, 0x22

    iget-object v2, p0, Ldoh;->o:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 743
    :cond_3
    iget-object v1, p0, Ldoh;->p:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 744
    const/16 v1, 0x23

    iget-object v2, p0, Ldoh;->p:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 746
    :cond_4
    iget-object v1, p0, Ldoh;->q:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 747
    const/16 v1, 0x24

    iget-object v2, p0, Ldoh;->q:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 749
    :cond_5
    iget-object v1, p0, Ldoh;->r:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 750
    const/16 v1, 0x25

    iget-object v2, p0, Ldoh;->r:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 752
    :cond_6
    iget-object v1, p0, Ldoh;->s:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 753
    const/16 v1, 0x26

    iget-object v2, p0, Ldoh;->s:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 755
    :cond_7
    iget-object v1, p0, Ldoh;->x:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 756
    const/16 v1, 0x2c

    iget-object v2, p0, Ldoh;->x:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 758
    :cond_8
    iget-object v1, p0, Ldoh;->y:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 759
    const/16 v1, 0x2d

    iget-object v2, p0, Ldoh;->y:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 761
    :cond_9
    iget-object v1, p0, Ldoh;->z:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    .line 762
    const/16 v1, 0x2e

    iget-object v2, p0, Ldoh;->z:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 764
    :cond_a
    iget-object v1, p0, Ldoh;->A:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 765
    const/16 v1, 0x2f

    iget-object v2, p0, Ldoh;->A:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 767
    :cond_b
    iget-object v1, p0, Ldoh;->B:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 768
    const/16 v1, 0x30

    iget-object v2, p0, Ldoh;->B:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 770
    :cond_c
    iget-object v1, p0, Ldoh;->C:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 771
    const/16 v1, 0x31

    iget-object v2, p0, Ldoh;->C:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 773
    :cond_d
    iget-object v1, p0, Ldoh;->D:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    .line 774
    const/16 v1, 0x32

    iget-object v2, p0, Ldoh;->D:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 776
    :cond_e
    iget-object v1, p0, Ldoh;->G:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 777
    const/16 v1, 0x33

    iget-object v2, p0, Ldoh;->G:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 779
    :cond_f
    iget-object v1, p0, Ldoh;->I:Ljava/lang/Integer;

    if-eqz v1, :cond_10

    .line 780
    const/16 v1, 0x35

    iget-object v2, p0, Ldoh;->I:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 782
    :cond_10
    iget-object v1, p0, Ldoh;->J:Ljava/lang/Integer;

    if-eqz v1, :cond_11

    .line 783
    const/16 v1, 0x36

    iget-object v2, p0, Ldoh;->J:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 785
    :cond_11
    iget-object v1, p0, Ldoh;->K:Ljava/lang/Integer;

    if-eqz v1, :cond_12

    .line 786
    const/16 v1, 0x37

    iget-object v2, p0, Ldoh;->K:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 788
    :cond_12
    iget-object v1, p0, Ldoh;->L:Ljava/lang/Integer;

    if-eqz v1, :cond_13

    .line 789
    const/16 v1, 0x38

    iget-object v2, p0, Ldoh;->L:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 791
    :cond_13
    iget-object v1, p0, Ldoh;->t:Ljava/lang/Float;

    if-eqz v1, :cond_14

    .line 792
    const/16 v1, 0x39

    iget-object v2, p0, Ldoh;->t:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IF)V

    .line 794
    :cond_14
    iget-object v1, p0, Ldoh;->v:Ljava/lang/Float;

    if-eqz v1, :cond_15

    .line 795
    const/16 v1, 0x3a

    iget-object v2, p0, Ldoh;->v:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IF)V

    .line 797
    :cond_15
    iget-object v1, p0, Ldoh;->N:[Ljava/lang/Integer;

    if-eqz v1, :cond_16

    .line 798
    iget-object v2, p0, Ldoh;->N:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_16

    aget-object v4, v2, v1

    .line 799
    const/16 v5, 0x3c

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v5, v4}, Lepl;->b(II)V

    .line 798
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 802
    :cond_16
    iget-object v1, p0, Ldoh;->O:[Ldoi;

    if-eqz v1, :cond_18

    .line 803
    iget-object v1, p0, Ldoh;->O:[Ldoi;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_18

    aget-object v3, v1, v0

    .line 804
    if-eqz v3, :cond_17

    .line 805
    const/16 v4, 0x3d

    invoke-virtual {p1, v4, v3}, Lepl;->a(ILepr;)V

    .line 803
    :cond_17
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 809
    :cond_18
    iget-object v0, p0, Ldoh;->P:Ljava/lang/Integer;

    if-eqz v0, :cond_19

    .line 810
    const/16 v0, 0x40

    iget-object v1, p0, Ldoh;->P:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 812
    :cond_19
    iget-object v0, p0, Ldoh;->Q:Ljava/lang/Integer;

    if-eqz v0, :cond_1a

    .line 813
    const/16 v0, 0x41

    iget-object v1, p0, Ldoh;->Q:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 815
    :cond_1a
    iget-object v0, p0, Ldoh;->T:Ljava/lang/Boolean;

    if-eqz v0, :cond_1b

    .line 816
    const/16 v0, 0x42

    iget-object v1, p0, Ldoh;->T:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 818
    :cond_1b
    iget-object v0, p0, Ldoh;->U:Ljava/lang/Boolean;

    if-eqz v0, :cond_1c

    .line 819
    const/16 v0, 0x43

    iget-object v1, p0, Ldoh;->U:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 821
    :cond_1c
    iget-object v0, p0, Ldoh;->M:Ljava/lang/Float;

    if-eqz v0, :cond_1d

    .line 822
    const/16 v0, 0x46

    iget-object v1, p0, Ldoh;->M:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 824
    :cond_1d
    iget-object v0, p0, Ldoh;->V:Ljava/lang/Integer;

    if-eqz v0, :cond_1e

    .line 825
    const/16 v0, 0x48

    iget-object v1, p0, Ldoh;->V:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 827
    :cond_1e
    iget-object v0, p0, Ldoh;->X:Ljava/lang/Float;

    if-eqz v0, :cond_1f

    .line 828
    const/16 v0, 0x4b

    iget-object v1, p0, Ldoh;->X:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 830
    :cond_1f
    iget-object v0, p0, Ldoh;->Y:Ljava/lang/Integer;

    if-eqz v0, :cond_20

    .line 831
    const/16 v0, 0x51

    iget-object v1, p0, Ldoh;->Y:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 833
    :cond_20
    iget-object v0, p0, Ldoh;->Z:Ljava/lang/Integer;

    if-eqz v0, :cond_21

    .line 834
    const/16 v0, 0x52

    iget-object v1, p0, Ldoh;->Z:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 836
    :cond_21
    iget-object v0, p0, Ldoh;->aa:Ljava/lang/Integer;

    if-eqz v0, :cond_22

    .line 837
    const/16 v0, 0x53

    iget-object v1, p0, Ldoh;->aa:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 839
    :cond_22
    iget-object v0, p0, Ldoh;->ab:Ljava/lang/Integer;

    if-eqz v0, :cond_23

    .line 840
    const/16 v0, 0x54

    iget-object v1, p0, Ldoh;->ab:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 842
    :cond_23
    iget-object v0, p0, Ldoh;->R:Ljava/lang/Integer;

    if-eqz v0, :cond_24

    .line 843
    const/16 v0, 0x55

    iget-object v1, p0, Ldoh;->R:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 845
    :cond_24
    iget-object v0, p0, Ldoh;->S:Ljava/lang/Integer;

    if-eqz v0, :cond_25

    .line 846
    const/16 v0, 0x56

    iget-object v1, p0, Ldoh;->S:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 848
    :cond_25
    iget-object v0, p0, Ldoh;->F:Ljava/lang/Integer;

    if-eqz v0, :cond_26

    .line 849
    const/16 v0, 0x57

    iget-object v1, p0, Ldoh;->F:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 851
    :cond_26
    iget-object v0, p0, Ldoh;->ac:Ljava/lang/Integer;

    if-eqz v0, :cond_27

    .line 852
    const/16 v0, 0x58

    iget-object v1, p0, Ldoh;->ac:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 854
    :cond_27
    iget-object v0, p0, Ldoh;->ad:Ljava/lang/Integer;

    if-eqz v0, :cond_28

    .line 855
    const/16 v0, 0x59

    iget-object v1, p0, Ldoh;->ad:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 857
    :cond_28
    iget-object v0, p0, Ldoh;->ae:Ljava/lang/Integer;

    if-eqz v0, :cond_29

    .line 858
    const/16 v0, 0x5a

    iget-object v1, p0, Ldoh;->ae:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 860
    :cond_29
    iget-object v0, p0, Ldoh;->af:Ljava/lang/Integer;

    if-eqz v0, :cond_2a

    .line 861
    const/16 v0, 0x5b

    iget-object v1, p0, Ldoh;->af:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 863
    :cond_2a
    iget-object v0, p0, Ldoh;->ag:Ljava/lang/Integer;

    if-eqz v0, :cond_2b

    .line 864
    const/16 v0, 0x5c

    iget-object v1, p0, Ldoh;->ag:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 866
    :cond_2b
    iget-object v0, p0, Ldoh;->ah:Ljava/lang/Integer;

    if-eqz v0, :cond_2c

    .line 867
    const/16 v0, 0x5d

    iget-object v1, p0, Ldoh;->ah:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 869
    :cond_2c
    iget-object v0, p0, Ldoh;->u:Ljava/lang/Float;

    if-eqz v0, :cond_2d

    .line 870
    const/16 v0, 0x5e

    iget-object v1, p0, Ldoh;->u:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 872
    :cond_2d
    iget-object v0, p0, Ldoh;->w:Ljava/lang/Float;

    if-eqz v0, :cond_2e

    .line 873
    const/16 v0, 0x5f

    iget-object v1, p0, Ldoh;->w:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 875
    :cond_2e
    iget-object v0, p0, Ldoh;->ai:Ljava/lang/Integer;

    if-eqz v0, :cond_2f

    .line 876
    const/16 v0, 0x60

    iget-object v1, p0, Ldoh;->ai:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 878
    :cond_2f
    iget-object v0, p0, Ldoh;->H:Ljava/lang/String;

    if-eqz v0, :cond_30

    .line 879
    const/16 v0, 0x65

    iget-object v1, p0, Ldoh;->H:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 881
    :cond_30
    iget-object v0, p0, Ldoh;->W:Ljava/lang/Integer;

    if-eqz v0, :cond_31

    .line 882
    const/16 v0, 0x66

    iget-object v1, p0, Ldoh;->W:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 884
    :cond_31
    iget-object v0, p0, Ldoh;->E:Ljava/lang/Integer;

    if-eqz v0, :cond_32

    .line 885
    const/16 v0, 0x67

    iget-object v1, p0, Ldoh;->E:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 887
    :cond_32
    iget-object v0, p0, Ldoh;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 889
    return-void
.end method
