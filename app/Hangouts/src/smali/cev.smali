.class public final Lcev;
.super Lbor;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcev;->a:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

    invoke-direct {p0}, Lbor;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILyj;Lbea;Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 83
    iget-object v0, p0, Lcev;->a:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;->a(Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 84
    :try_start_0
    iget-object v0, p0, Lcev;->a:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;->b(Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;)Landroid/util/Pair;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcev;->a:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

    .line 85
    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;->b(Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 86
    iget-object v0, p0, Lcev;->a:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;->c(Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;)V

    .line 90
    :cond_0
    :goto_0
    const-string v0, "Voice request failed"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p4, v2, v3

    invoke-static {v0, v2}, Lf;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 91
    monitor-exit v1

    return-void

    .line 87
    :cond_1
    iget-object v0, p0, Lcev;->a:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;->d(Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;)I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 88
    iget-object v0, p0, Lcev;->a:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;->f(Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public a(ILyj;Lbos;)V
    .locals 4

    .prologue
    .line 58
    iget-object v0, p0, Lcev;->a:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;->a(Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 59
    :try_start_0
    invoke-virtual {p3}, Lbos;->c()Lbfz;

    move-result-object v1

    .line 61
    iget-object v0, p0, Lcev;->a:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;->b(Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;)Landroid/util/Pair;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcev;->a:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

    .line 62
    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;->b(Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    const/4 v3, 0x0

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    if-ne p1, v0, :cond_2

    .line 63
    iget-object v0, p0, Lcev;->a:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;->b(Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 65
    instance-of v3, v1, Lbgo;

    if-eqz v3, :cond_0

    .line 66
    check-cast v1, Lbgo;

    .line 67
    invoke-virtual {v1}, Lbgo;->h()Ljava/lang/String;

    move-result-object v1

    .line 68
    iget-object v3, p0, Lcev;->a:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

    invoke-static {v3, v0, v1, p2}, Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;->a(Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;Ljava/lang/String;Ljava/lang/String;Lyj;)V

    .line 70
    :cond_0
    iget-object v0, p0, Lcev;->a:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;->c(Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;)V

    .line 77
    :cond_1
    :goto_0
    monitor-exit v2

    return-void

    .line 71
    :cond_2
    iget-object v0, p0, Lcev;->a:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;->d(Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;)I

    move-result v0

    if-ne p1, v0, :cond_1

    instance-of v0, v1, Lbhd;

    if-eqz v0, :cond_1

    .line 73
    iget-object v0, p0, Lcev;->a:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;->e(Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p2}, Lyj;->ad()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    iget-object v0, p0, Lcev;->a:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;->f(Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;)V

    .line 75
    iget-object v0, p0, Lcev;->a:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;->g(Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method
