.class public final Levd;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Levd;


# instance fields
.field public b:Lexn;

.field public c:[Leve;

.field public d:Levt;

.field public e:[Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Levd;

    sput-object v0, Levd;->a:[Levd;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 112
    iput-object v1, p0, Levd;->b:Lexn;

    .line 115
    sget-object v0, Leve;->a:[Leve;

    iput-object v0, p0, Levd;->c:[Leve;

    .line 118
    iput-object v1, p0, Levd;->d:Levt;

    .line 121
    sget-object v0, Lept;->m:[Ljava/lang/Integer;

    iput-object v0, p0, Levd;->e:[Ljava/lang/Integer;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 156
    iget-object v0, p0, Levd;->b:Lexn;

    if-eqz v0, :cond_6

    .line 157
    const/4 v0, 0x1

    iget-object v2, p0, Levd;->b:Lexn;

    .line 158
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 160
    :goto_0
    iget-object v2, p0, Levd;->c:[Leve;

    if-eqz v2, :cond_1

    .line 161
    iget-object v3, p0, Levd;->c:[Leve;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 162
    if-eqz v5, :cond_0

    .line 163
    const/4 v6, 0x2

    .line 164
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 161
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 168
    :cond_1
    iget-object v2, p0, Levd;->d:Levt;

    if-eqz v2, :cond_2

    .line 169
    const/4 v2, 0x3

    iget-object v3, p0, Levd;->d:Levt;

    .line 170
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 172
    :cond_2
    iget-object v2, p0, Levd;->e:[Ljava/lang/Integer;

    if-eqz v2, :cond_4

    iget-object v2, p0, Levd;->e:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 174
    iget-object v3, p0, Levd;->e:[Ljava/lang/Integer;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    .line 176
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 174
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 178
    :cond_3
    add-int/2addr v0, v2

    .line 179
    iget-object v1, p0, Levd;->e:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 181
    :cond_4
    iget-object v1, p0, Levd;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 182
    const/4 v1, 0x5

    iget-object v2, p0, Levd;->f:Ljava/lang/Integer;

    .line 183
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 185
    :cond_5
    iget-object v1, p0, Levd;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 186
    iput v0, p0, Levd;->cachedSize:I

    .line 187
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Levd;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Levd;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Levd;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Levd;->b:Lexn;

    if-nez v0, :cond_2

    new-instance v0, Lexn;

    invoke-direct {v0}, Lexn;-><init>()V

    iput-object v0, p0, Levd;->b:Lexn;

    :cond_2
    iget-object v0, p0, Levd;->b:Lexn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Levd;->c:[Leve;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leve;

    iget-object v3, p0, Levd;->c:[Leve;

    if-eqz v3, :cond_3

    iget-object v3, p0, Levd;->c:[Leve;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Levd;->c:[Leve;

    :goto_2
    iget-object v2, p0, Levd;->c:[Leve;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Levd;->c:[Leve;

    new-instance v3, Leve;

    invoke-direct {v3}, Leve;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Levd;->c:[Leve;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Levd;->c:[Leve;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Levd;->c:[Leve;

    new-instance v3, Leve;

    invoke-direct {v3}, Leve;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Levd;->c:[Leve;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Levd;->d:Levt;

    if-nez v0, :cond_6

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Levd;->d:Levt;

    :cond_6
    iget-object v0, p0, Levd;->d:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x20

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Levd;->e:[Ljava/lang/Integer;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Integer;

    iget-object v3, p0, Levd;->e:[Ljava/lang/Integer;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Levd;->e:[Ljava/lang/Integer;

    :goto_3
    iget-object v2, p0, Levd;->e:[Ljava/lang/Integer;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Levd;->e:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_7
    iget-object v2, p0, Levd;->e:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Levd;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 128
    iget-object v1, p0, Levd;->b:Lexn;

    if-eqz v1, :cond_0

    .line 129
    const/4 v1, 0x1

    iget-object v2, p0, Levd;->b:Lexn;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 131
    :cond_0
    iget-object v1, p0, Levd;->c:[Leve;

    if-eqz v1, :cond_2

    .line 132
    iget-object v2, p0, Levd;->c:[Leve;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 133
    if-eqz v4, :cond_1

    .line 134
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 132
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 138
    :cond_2
    iget-object v1, p0, Levd;->d:Levt;

    if-eqz v1, :cond_3

    .line 139
    const/4 v1, 0x3

    iget-object v2, p0, Levd;->d:Levt;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 141
    :cond_3
    iget-object v1, p0, Levd;->e:[Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 142
    iget-object v1, p0, Levd;->e:[Ljava/lang/Integer;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 143
    const/4 v4, 0x4

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 142
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 146
    :cond_4
    iget-object v0, p0, Levd;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 147
    const/4 v0, 0x5

    iget-object v1, p0, Levd;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 149
    :cond_5
    iget-object v0, p0, Levd;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 151
    return-void
.end method
