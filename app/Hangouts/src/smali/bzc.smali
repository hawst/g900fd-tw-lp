.class public final Lbzc;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static a:Lcxq;


# instance fields
.field private final b:Lcye;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/Long;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    sget-object v0, Lbzc;->a:Lcxq;

    invoke-virtual {v0, p1, p2}, Lcxq;->a(Ljava/lang/String;Ljava/lang/String;)Lcye;

    move-result-object v0

    iput-object v0, p0, Lbzc;->b:Lcye;

    .line 62
    const/4 v1, 0x0

    .line 63
    const/4 v0, 0x0

    move v4, v0

    move-object v0, v1

    move v1, v4

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 64
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 65
    invoke-static {v2}, Ljava/lang/Character;->isLetter(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 66
    if-nez v0, :cond_0

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    :cond_0
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 71
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 70
    :cond_2
    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 75
    :cond_3
    if-eqz v0, :cond_4

    .line 76
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbzc;->f:Ljava/lang/String;

    .line 78
    :cond_4
    return-void
.end method


# virtual methods
.method public a(Lbzc;)I
    .locals 3

    .prologue
    .line 182
    iget-object v0, p0, Lbzc;->f:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p1, Lbzc;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 184
    :goto_0
    if-nez v0, :cond_2

    .line 185
    sget v0, Lcxs;->b:I

    .line 188
    :goto_1
    return v0

    .line 182
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbzc;->f:Ljava/lang/String;

    iget-object v1, p1, Lbzc;->f:Ljava/lang/String;

    .line 183
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 188
    :cond_2
    sget-object v0, Lbzc;->a:Lcxq;

    iget-object v1, p0, Lbzc;->b:Lcye;

    iget-object v2, p1, Lbzc;->b:Lcye;

    invoke-virtual {v0, v1, v2}, Lcxq;->a(Lcye;Lcye;)I

    move-result v0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0}, Lbzc;->d()Ljava/lang/String;

    move-result-object v0

    .line 107
    if-nez v0, :cond_0

    .line 111
    :goto_0
    sget-object v0, Lbzc;->a:Lcxq;

    invoke-virtual {v0, p1}, Lcxq;->c(Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_0
    move-object p1, v0

    goto :goto_0
.end method

.method public a()Lcye;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lbzc;->b:Lcye;

    return-object v0
.end method

.method public a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 155
    sget-object v0, Lbzc;->a:Lcxq;

    iget-object v1, p0, Lbzc;->b:Lcye;

    invoke-virtual {v0, v1, p1}, Lcxq;->a(Lcye;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lbzc;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 92
    sget-object v0, Lbzc;->a:Lcxq;

    iget-object v1, p0, Lbzc;->b:Lcye;

    invoke-virtual {v0, v1}, Lcxq;->b(Lcye;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lbzc;->c:Ljava/lang/Boolean;

    .line 95
    :cond_0
    iget-object v0, p0, Lbzc;->c:Ljava/lang/Boolean;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    return v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lbzc;->e:Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 120
    iget-object v0, p0, Lbzc;->b:Lcye;

    invoke-virtual {v0}, Lcye;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lbzc;->e:Ljava/lang/Long;

    .line 123
    :cond_0
    iget-object v0, p0, Lbzc;->e:Ljava/lang/Long;

    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    return-wide v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lbzc;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 135
    sget-object v0, Lbzc;->a:Lcxq;

    iget-object v1, p0, Lbzc;->b:Lcye;

    invoke-virtual {v0, v1}, Lcxq;->c(Lcye;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbzc;->d:Ljava/lang/String;

    .line 138
    :cond_0
    iget-object v0, p0, Lbzc;->d:Ljava/lang/String;

    return-object v0
.end method
