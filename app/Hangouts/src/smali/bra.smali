.class public final Lbra;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

.field private final b:Z

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Landroid/content/Context;

.field private f:I

.field private final g:Ljava/lang/Object;

.field private final h:Landroid/os/Handler;

.field private final i:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/service/AudioPlayerService;Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 88
    iput-object p1, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbra;->g:Ljava/lang/Object;

    .line 80
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lbra;->h:Landroid/os/Handler;

    .line 81
    new-instance v0, Lbrb;

    invoke-direct {v0, p0}, Lbrb;-><init>(Lbra;)V

    iput-object v0, p0, Lbra;->i:Ljava/lang/Runnable;

    .line 89
    iput-object p2, p0, Lbra;->e:Landroid/content/Context;

    .line 90
    iput-object p4, p0, Lbra;->c:Ljava/lang/String;

    .line 91
    iput v2, p0, Lbra;->f:I

    .line 92
    const-string v0, "audio_stream_url"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbra;->d:Ljava/lang/String;

    .line 93
    iget-object v0, p0, Lbra;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    const-string v0, "Babel"

    const-string v1, "Empty or missing AUDIO_STREAM_URL"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    iput-boolean v2, p0, Lbra;->b:Z

    .line 99
    :goto_0
    return-void

    .line 98
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbra;->b:Z

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lbra;->c:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbra;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lbra;->j()V

    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 169
    iget-object v1, p0, Lbra;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 170
    :try_start_0
    iget v0, p0, Lbra;->f:I

    if-nez v0, :cond_0

    .line 171
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    :goto_0
    return-void

    .line 173
    :cond_0
    monitor-exit v1

    .line 174
    const-string v0, "current_position"

    invoke-direct {p0, v0}, Lbra;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 175
    const-string v1, "position_in_milliseconds"

    iget-object v2, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->a(Lcom/google/android/apps/hangouts/service/AudioPlayerService;)Landroid/media/MediaPlayer;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 176
    const-string v1, "duration_in_milliseconds"

    iget-object v2, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->a(Lcom/google/android/apps/hangouts/service/AudioPlayerService;)Landroid/media/MediaPlayer;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 177
    iget-object v1, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 173
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private j()V
    .locals 5

    .prologue
    .line 181
    invoke-direct {p0}, Lbra;->i()V

    .line 182
    iget-object v1, p0, Lbra;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 183
    :try_start_0
    iget v0, p0, Lbra;->f:I

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lbra;->h:Landroid/os/Handler;

    iget-object v2, p0, Lbra;->i:Ljava/lang/Runnable;

    const-wide/16 v3, 0xfa

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 186
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->a(Lcom/google/android/apps/hangouts/service/AudioPlayerService;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 140
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lbra;->b:Z

    return v0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 111
    :try_start_0
    iget-object v0, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->a(Lcom/google/android/apps/hangouts/service/AudioPlayerService;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 112
    iget-object v0, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->a(Lcom/google/android/apps/hangouts/service/AudioPlayerService;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 113
    iget-object v0, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->a(Lcom/google/android/apps/hangouts/service/AudioPlayerService;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 114
    iget-object v0, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->a(Lcom/google/android/apps/hangouts/service/AudioPlayerService;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 115
    iget-object v0, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->a(Lcom/google/android/apps/hangouts/service/AudioPlayerService;)Landroid/media/MediaPlayer;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 116
    iget-object v0, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->a(Lcom/google/android/apps/hangouts/service/AudioPlayerService;)Landroid/media/MediaPlayer;

    move-result-object v0

    iget-object v1, p0, Lbra;->e:Landroid/content/Context;

    iget-object v2, p0, Lbra;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 117
    iget-object v0, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->a(Lcom/google/android/apps/hangouts/service/AudioPlayerService;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :goto_0
    return-void

    .line 119
    :catch_0
    move-exception v0

    iget-object v0, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->a(Lcom/google/android/apps/hangouts/service/AudioPlayerService;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 120
    iget-object v0, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    const-string v1, "play_stopped"

    invoke-direct {p0, v1}, Lbra;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lbra;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->a(Lcom/google/android/apps/hangouts/service/AudioPlayerService;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 130
    iget-object v0, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    const-string v1, "play_started"

    invoke-direct {p0, v1}, Lbra;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 131
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->a(Lcom/google/android/apps/hangouts/service/AudioPlayerService;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 135
    iget-object v0, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    const-string v1, "play_paused"

    invoke-direct {p0, v1}, Lbra;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 136
    return-void
.end method

.method public f()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 143
    const/4 v1, 0x0

    .line 144
    iget-object v2, p0, Lbra;->g:Ljava/lang/Object;

    monitor-enter v2

    .line 145
    :try_start_0
    iget v3, p0, Lbra;->f:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lbra;->f:I

    .line 146
    iget v3, p0, Lbra;->f:I

    if-ne v3, v0, :cond_1

    .line 149
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    if-eqz v0, :cond_0

    .line 151
    invoke-direct {p0}, Lbra;->j()V

    .line 155
    :goto_1
    return-void

    .line 149
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 153
    :cond_0
    invoke-direct {p0}, Lbra;->i()V

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public g()V
    .locals 3

    .prologue
    .line 158
    iget-object v1, p0, Lbra;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 159
    :try_start_0
    iget v0, p0, Lbra;->f:I

    if-lez v0, :cond_0

    .line 160
    iget v0, p0, Lbra;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbra;->f:I

    .line 165
    :goto_0
    monitor-exit v1

    return-void

    .line 162
    :cond_0
    const-string v0, "Babel"

    const-string v2, "unregisterForCurrentPositionNotification: currentPositionListenerCount <= 0!"

    invoke-static {v0, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 165
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 190
    iget-object v1, p0, Lbra;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 192
    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lbra;->f:I

    .line 193
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    iget-object v0, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->a(Lcom/google/android/apps/hangouts/service/AudioPlayerService;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 195
    iget-object v0, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->a(Lcom/google/android/apps/hangouts/service/AudioPlayerService;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 196
    iget-object v0, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    const-string v1, "play_stopped"

    invoke-direct {p0, v1}, Lbra;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 197
    iget-object v0, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->b(Lcom/google/android/apps/hangouts/service/AudioPlayerService;)Lbra;

    .line 198
    return-void

    .line 193
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 0

    .prologue
    .line 210
    invoke-virtual {p0}, Lbra;->h()V

    .line 211
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 3

    .prologue
    .line 215
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AudioPlayerService: MediaPlayer error. what: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " extra: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    invoke-virtual {p0}, Lbra;->h()V

    .line 217
    const/4 v0, 0x1

    return v0
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 3

    .prologue
    .line 202
    const-string v0, "ready_to_play"

    invoke-direct {p0, v0}, Lbra;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 203
    const-string v1, "duration_in_milliseconds"

    iget-object v2, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->a(Lcom/google/android/apps/hangouts/service/AudioPlayerService;)Landroid/media/MediaPlayer;

    move-result-object v2

    .line 204
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v2

    .line 203
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 205
    iget-object v1, p0, Lbra;->a:Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/hangouts/service/AudioPlayerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 206
    return-void
.end method
