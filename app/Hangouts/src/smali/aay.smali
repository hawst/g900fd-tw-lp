.class public final Laay;
.super Lt;
.source "PG"


# static fields
.field private static final a:Z


# instance fields
.field private b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lbys;->d:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Laay;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lt;-><init>()V

    .line 68
    return-void
.end method

.method public static a()Laay;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 297
    invoke-static {v0, v0}, Laay;->a(ZZ)Laay;

    move-result-object v0

    return-object v0
.end method

.method public static a(ZIIIZZZZ)Laay;
    .locals 4

    .prologue
    .line 274
    new-instance v0, Laay;

    invoke-direct {v0}, Laay;-><init>()V

    .line 278
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 279
    const-string v2, "exclude_enterprise"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 281
    const-string v2, "dialog_title_id"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 282
    const-string v2, "select_all_id"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 283
    const-string v2, "select_none_id"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 284
    const-string v2, "show_account_state"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 285
    const-string v2, "show_cancel"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 286
    const-string v2, "show_sms_accounts_only"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 287
    const-string v2, "exclude_plus_pages"

    invoke-virtual {v1, v2, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 288
    const-string v2, "include_sms_only_account"

    invoke-virtual {v1, v2, p7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 289
    invoke-virtual {v0, v1}, Laay;->setArguments(Landroid/os/Bundle;)V

    .line 291
    return-object v0
.end method

.method public static a(ZZ)Laay;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v1, -0x1

    .line 304
    const/4 v6, 0x0

    move v0, p0

    move v2, v1

    move v3, v1

    move v5, v4

    move v7, p1

    invoke-static/range {v0 .. v7}, Laay;->a(ZIIIZZZZ)Laay;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Laay;)Labd;
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Laay;->c()Labd;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Laay;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Laay;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 388
    invoke-static {p1}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 389
    if-eqz v0, :cond_0

    invoke-static {v0}, Lbkb;->f(Lyj;)I

    move-result v1

    const/16 v2, 0x66

    if-ne v1, v2, :cond_1

    .line 392
    :cond_0
    invoke-direct {p0}, Laay;->c()Labd;

    move-result-object v0

    invoke-interface {v0, p1}, Labd;->a_(Ljava/lang/String;)V

    .line 409
    :goto_0
    return-void

    .line 396
    :cond_1
    invoke-static {v0}, Lbkb;->l(Lyj;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 399
    invoke-static {v0}, Lbkb;->k(Lyj;)V

    .line 401
    :cond_2
    iput-object p1, p0, Laay;->b:Ljava/lang/String;

    .line 402
    const/4 v0, 0x0

    invoke-static {v0}, Lbbl;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 404
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 405
    const-string v1, "try_other_accounts"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 407
    const/16 v1, 0x7d0

    invoke-virtual {p0, v0, v1}, Laay;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private a(ZZZZ)V
    .locals 9

    .prologue
    .line 366
    invoke-virtual {p0}, Laay;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "dialog_title_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 367
    invoke-virtual {p0}, Laay;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "select_all_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 368
    invoke-virtual {p0}, Laay;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "select_none_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 369
    invoke-virtual {p0}, Laay;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "show_account_state"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 370
    invoke-virtual {p0}, Laay;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v5, "show_cancel"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    move v0, p1

    move v6, p2

    move v7, p3

    move v8, p4

    .line 371
    invoke-static/range {v0 .. v8}, Laaz;->a(ZIIIZZZZZ)Laaz;

    move-result-object v0

    .line 374
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Laaz;->setTargetFragment(Lt;I)V

    .line 375
    invoke-virtual {p0}, Laay;->getFragmentManager()Lae;

    move-result-object v1

    invoke-virtual {v1}, Lae;->a()Lao;

    move-result-object v1

    .line 376
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Laaz;->a(Lao;Ljava/lang/String;)I

    .line 377
    return-void
.end method

.method static synthetic b()Z
    .locals 1

    .prologue
    .line 44
    sget-boolean v0, Laay;->a:Z

    return v0
.end method

.method private c()Labd;
    .locals 1

    .prologue
    .line 424
    invoke-virtual {p0}, Laay;->getTargetFragment()Lt;

    move-result-object v0

    instance-of v0, v0, Labd;

    if-eqz v0, :cond_0

    .line 425
    invoke-virtual {p0}, Laay;->getTargetFragment()Lt;

    move-result-object v0

    check-cast v0, Labd;

    .line 427
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Laay;->getActivity()Ly;

    move-result-object v0

    check-cast v0, Labd;

    goto :goto_0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 413
    invoke-super {p0, p1, p2, p3}, Lt;->onActivityResult(IILandroid/content/Intent;)V

    .line 414
    const/16 v0, 0x7d0

    if-ne p1, v0, :cond_0

    .line 415
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 416
    invoke-direct {p0}, Laay;->c()Labd;

    move-result-object v0

    iget-object v1, p0, Laay;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Labd;->a_(Ljava/lang/String;)V

    .line 421
    :cond_0
    :goto_0
    return-void

    .line 418
    :cond_1
    invoke-direct {p0}, Laay;->c()Labd;

    move-result-object v0

    invoke-interface {v0}, Labd;->n_()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 310
    invoke-super {p0, p1}, Lt;->onCreate(Landroid/os/Bundle;)V

    .line 311
    if-nez p1, :cond_a

    .line 312
    invoke-virtual {p0}, Laay;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "exclude_enterprise"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 314
    invoke-virtual {p0}, Laay;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "show_sms_accounts_only"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 315
    invoke-virtual {p0}, Laay;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "sms_convs_only"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    .line 316
    :goto_0
    invoke-virtual {p0}, Laay;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v5, "exclude_plus_pages"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 318
    invoke-virtual {p0}, Laay;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v6, "include_sms_only_account"

    invoke-virtual {v3, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 319
    invoke-virtual {p0}, Laay;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v7, "select_none_id"

    invoke-virtual {v3, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 321
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v3

    invoke-virtual {v3}, Lyj;->u()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 322
    invoke-static {}, Lbzd;->d()Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v2

    :goto_1
    and-int/2addr v3, v6

    .line 324
    invoke-static {v4, v0, v5}, Lbkb;->a(ZZZ)Ljava/util/List;

    move-result-object v6

    .line 328
    if-nez v0, :cond_1

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    if-nez v8, :cond_7

    .line 330
    :cond_1
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v7

    invoke-static {v7}, Lbkb;->c(Lyj;)Lyj;

    move-result-object v7

    .line 331
    if-eqz v7, :cond_4

    .line 332
    invoke-static {v7}, Lbkb;->a(Lyj;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 333
    :goto_2
    if-nez v2, :cond_5

    .line 334
    const-string v0, "Babel"

    const-string v1, "Can\'t share -- no accounts and sms is disabled"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    const/4 v0, 0x0

    invoke-static {v0}, Lbbl;->b(Lyj;)Landroid/content/Intent;

    move-result-object v0

    .line 336
    invoke-virtual {p0, v0}, Laay;->startActivity(Landroid/content/Intent;)V

    .line 337
    invoke-direct {p0}, Laay;->c()Labd;

    move-result-object v0

    invoke-interface {v0}, Labd;->n_()V

    .line 362
    :goto_3
    return-void

    :cond_2
    move v0, v1

    .line 315
    goto :goto_0

    :cond_3
    move v3, v1

    .line 322
    goto :goto_1

    :cond_4
    move v2, v1

    .line 332
    goto :goto_2

    .line 339
    :cond_5
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_6

    .line 342
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v0

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Laay;->a(Ljava/lang/String;)V

    goto :goto_3

    .line 344
    :cond_6
    invoke-direct {p0, v4, v0, v5, v3}, Laay;->a(ZZZZ)V

    goto :goto_3

    .line 347
    :cond_7
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_9

    if-nez v3, :cond_9

    if-gez v7, :cond_9

    .line 350
    sget-boolean v0, Laay;->a:Z

    if-eqz v0, :cond_8

    .line 351
    const-string v0, "Babel"

    const-string v2, "AccountPickerFragment selecting only available account"

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    :cond_8
    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Laay;->a(Ljava/lang/String;)V

    goto :goto_3

    .line 356
    :cond_9
    invoke-direct {p0, v4, v1, v5, v3}, Laay;->a(ZZZZ)V

    goto :goto_3

    .line 360
    :cond_a
    const-string v0, "account_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laay;->b:Ljava/lang/String;

    goto :goto_3
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 381
    invoke-super {p0, p1}, Lt;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 382
    iget-object v0, p0, Laay;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 383
    const-string v0, "account_name"

    iget-object v1, p0, Laay;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    :cond_0
    return-void
.end method
