.class public final Lcnq;
.super Ljava/lang/Object;

# interfaces
.implements Lcfv;
.implements Lcfw;


# instance fields
.field private final a:La;

.field private b:Lcns;

.field private c:Z


# direct methods
.method public constructor <init>(La;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcnq;->a:La;

    iput-object v0, p0, Lcnq;->b:Lcns;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcnq;->c:Z

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcnq;->c:Z

    return-void
.end method

.method public a(Lcns;)V
    .locals 0

    iput-object p1, p0, Lcnq;->b:Lcns;

    return-void
.end method

.method public onConnected(Landroid/os/Bundle;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcnq;->b:Lcns;

    invoke-virtual {v0, v1}, Lcns;->a(Z)V

    iget-boolean v0, p0, Lcnq;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcnq;->a:La;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcnq;->a:La;

    :cond_0
    iput-boolean v1, p0, Lcnq;->c:Z

    return-void
.end method

.method public onConnectionFailed(Lcft;)V
    .locals 2

    iget-object v0, p0, Lcnq;->b:Lcns;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcns;->a(Z)V

    iget-boolean v0, p0, Lcnq;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcnq;->a:La;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcft;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcnq;->a:La;

    invoke-virtual {p1}, Lcft;->d()Landroid/app/PendingIntent;

    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcnq;->c:Z

    return-void

    :cond_1
    iget-object v0, p0, Lcnq;->a:La;

    goto :goto_0
.end method

.method public onDisconnected()V
    .locals 2

    iget-object v0, p0, Lcnq;->b:Lcns;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcns;->a(Z)V

    return-void
.end method
