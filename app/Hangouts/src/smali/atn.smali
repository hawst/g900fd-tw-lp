.class public final Latn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Larn;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Latn;->a:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;B)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0, p1}, Latn;-><init>(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;)V

    return-void
.end method

.method private c(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Latn;->a:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->b(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 106
    :try_start_0
    iget-object v0, p0, Latn;->a:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a(Ljava/lang/String;)Late;

    move-result-object v0

    .line 107
    if-nez v0, :cond_0

    .line 108
    const/4 v0, 0x0

    monitor-exit v1

    .line 110
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Late;->l()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 116
    invoke-direct {p0, p1}, Latn;->c(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v0

    .line 117
    if-eqz v0, :cond_0

    .line 118
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->remoteMute(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    .line 120
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 136
    invoke-direct {p0, p1}, Latn;->c(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_1

    .line 138
    iget-object v1, p0, Latn;->a:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->c(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;)Lapk;

    move-result-object v1

    invoke-virtual {v1}, Lapk;->c()Lapx;

    move-result-object v2

    .line 139
    if-eqz v2, :cond_1

    .line 140
    iget-object v1, p0, Latn;->a:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->c(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;)Lapk;

    move-result-object v1

    invoke-virtual {v1}, Lapk;->c()Lapx;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lapx;->F()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getEndpointMucJid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    :goto_0
    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v2, v0}, Lapx;->d(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    .line 141
    iget-object v0, p0, Latn;->a:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->d(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;)Lasx;

    move-result-object v0

    invoke-virtual {v0}, Lasx;->d()V

    .line 144
    :cond_1
    return-void

    .line 140
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
