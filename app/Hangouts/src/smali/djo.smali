.class public final Ldjo;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldjo;


# instance fields
.field public b:Ljava/lang/Boolean;

.field public c:Ldkj;

.field public d:Ldkj;

.field public e:Ldkj;

.field public f:Ldjh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 346
    const/4 v0, 0x0

    new-array v0, v0, [Ldjo;

    sput-object v0, Ldjo;->a:[Ldjo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 347
    invoke-direct {p0}, Lepn;-><init>()V

    .line 352
    iput-object v0, p0, Ldjo;->c:Ldkj;

    .line 355
    iput-object v0, p0, Ldjo;->d:Ldkj;

    .line 358
    iput-object v0, p0, Ldjo;->e:Ldkj;

    .line 361
    iput-object v0, p0, Ldjo;->f:Ldjh;

    .line 347
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 387
    const/4 v0, 0x0

    .line 388
    iget-object v1, p0, Ldjo;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 389
    const/4 v0, 0x5

    iget-object v1, p0, Ldjo;->b:Ljava/lang/Boolean;

    .line 390
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 392
    :cond_0
    iget-object v1, p0, Ldjo;->c:Ldkj;

    if-eqz v1, :cond_1

    .line 393
    const/4 v1, 0x6

    iget-object v2, p0, Ldjo;->c:Ldkj;

    .line 394
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 396
    :cond_1
    iget-object v1, p0, Ldjo;->d:Ldkj;

    if-eqz v1, :cond_2

    .line 397
    const/4 v1, 0x7

    iget-object v2, p0, Ldjo;->d:Ldkj;

    .line 398
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 400
    :cond_2
    iget-object v1, p0, Ldjo;->e:Ldkj;

    if-eqz v1, :cond_3

    .line 401
    const/16 v1, 0x8

    iget-object v2, p0, Ldjo;->e:Ldkj;

    .line 402
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 404
    :cond_3
    iget-object v1, p0, Ldjo;->f:Ldjh;

    if-eqz v1, :cond_4

    .line 405
    const/16 v1, 0x9

    iget-object v2, p0, Ldjo;->f:Ldjh;

    .line 406
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 408
    :cond_4
    iget-object v1, p0, Ldjo;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 409
    iput v0, p0, Ldjo;->cachedSize:I

    .line 410
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 343
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldjo;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldjo;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldjo;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldjo;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldjo;->c:Ldkj;

    if-nez v0, :cond_2

    new-instance v0, Ldkj;

    invoke-direct {v0}, Ldkj;-><init>()V

    iput-object v0, p0, Ldjo;->c:Ldkj;

    :cond_2
    iget-object v0, p0, Ldjo;->c:Ldkj;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldjo;->d:Ldkj;

    if-nez v0, :cond_3

    new-instance v0, Ldkj;

    invoke-direct {v0}, Ldkj;-><init>()V

    iput-object v0, p0, Ldjo;->d:Ldkj;

    :cond_3
    iget-object v0, p0, Ldjo;->d:Ldkj;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldjo;->e:Ldkj;

    if-nez v0, :cond_4

    new-instance v0, Ldkj;

    invoke-direct {v0}, Ldkj;-><init>()V

    iput-object v0, p0, Ldjo;->e:Ldkj;

    :cond_4
    iget-object v0, p0, Ldjo;->e:Ldkj;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldjo;->f:Ldjh;

    if-nez v0, :cond_5

    new-instance v0, Ldjh;

    invoke-direct {v0}, Ldjh;-><init>()V

    iput-object v0, p0, Ldjo;->f:Ldjh;

    :cond_5
    iget-object v0, p0, Ldjo;->f:Ldjh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x28 -> :sswitch_1
        0x32 -> :sswitch_2
        0x3a -> :sswitch_3
        0x42 -> :sswitch_4
        0x4a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, Ldjo;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 367
    const/4 v0, 0x5

    iget-object v1, p0, Ldjo;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 369
    :cond_0
    iget-object v0, p0, Ldjo;->c:Ldkj;

    if-eqz v0, :cond_1

    .line 370
    const/4 v0, 0x6

    iget-object v1, p0, Ldjo;->c:Ldkj;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 372
    :cond_1
    iget-object v0, p0, Ldjo;->d:Ldkj;

    if-eqz v0, :cond_2

    .line 373
    const/4 v0, 0x7

    iget-object v1, p0, Ldjo;->d:Ldkj;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 375
    :cond_2
    iget-object v0, p0, Ldjo;->e:Ldkj;

    if-eqz v0, :cond_3

    .line 376
    const/16 v0, 0x8

    iget-object v1, p0, Ldjo;->e:Ldkj;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 378
    :cond_3
    iget-object v0, p0, Ldjo;->f:Ldjh;

    if-eqz v0, :cond_4

    .line 379
    const/16 v0, 0x9

    iget-object v1, p0, Ldjo;->f:Ldjh;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 381
    :cond_4
    iget-object v0, p0, Ldjo;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 383
    return-void
.end method
