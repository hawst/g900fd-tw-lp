.class public final Ldyy;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldyy;


# instance fields
.field public b:Ldvm;

.field public c:Ljava/lang/String;

.field public d:Ldzg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1646
    const/4 v0, 0x0

    new-array v0, v0, [Ldyy;

    sput-object v0, Ldyy;->a:[Ldyy;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1647
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1650
    iput-object v0, p0, Ldyy;->b:Ldvm;

    .line 1655
    iput-object v0, p0, Ldyy;->d:Ldzg;

    .line 1647
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1675
    const/4 v0, 0x0

    .line 1676
    iget-object v1, p0, Ldyy;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 1677
    const/4 v0, 0x1

    iget-object v1, p0, Ldyy;->b:Ldvm;

    .line 1678
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1680
    :cond_0
    iget-object v1, p0, Ldyy;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1681
    const/4 v1, 0x2

    iget-object v2, p0, Ldyy;->c:Ljava/lang/String;

    .line 1682
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1684
    :cond_1
    iget-object v1, p0, Ldyy;->d:Ldzg;

    if-eqz v1, :cond_2

    .line 1685
    const/4 v1, 0x3

    iget-object v2, p0, Ldyy;->d:Ldzg;

    .line 1686
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1688
    :cond_2
    iget-object v1, p0, Ldyy;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1689
    iput v0, p0, Ldyy;->cachedSize:I

    .line 1690
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1643
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldyy;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldyy;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldyy;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldyy;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldyy;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldyy;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldyy;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldyy;->d:Ldzg;

    if-nez v0, :cond_3

    new-instance v0, Ldzg;

    invoke-direct {v0}, Ldzg;-><init>()V

    iput-object v0, p0, Ldyy;->d:Ldzg;

    :cond_3
    iget-object v0, p0, Ldyy;->d:Ldzg;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1660
    iget-object v0, p0, Ldyy;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 1661
    const/4 v0, 0x1

    iget-object v1, p0, Ldyy;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1663
    :cond_0
    iget-object v0, p0, Ldyy;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1664
    const/4 v0, 0x2

    iget-object v1, p0, Ldyy;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1666
    :cond_1
    iget-object v0, p0, Ldyy;->d:Ldzg;

    if-eqz v0, :cond_2

    .line 1667
    const/4 v0, 0x3

    iget-object v1, p0, Ldyy;->d:Ldzg;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1669
    :cond_2
    iget-object v0, p0, Ldyy;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1671
    return-void
.end method
