.class public final Leun;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leun;


# instance fields
.field public b:Lexn;

.field public c:[Leuk;

.field public d:[Leuo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 243
    const/4 v0, 0x0

    new-array v0, v0, [Leun;

    sput-object v0, Leun;->a:[Leun;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 244
    invoke-direct {p0}, Lepn;-><init>()V

    .line 360
    const/4 v0, 0x0

    iput-object v0, p0, Leun;->b:Lexn;

    .line 363
    sget-object v0, Leuk;->a:[Leuk;

    iput-object v0, p0, Leun;->c:[Leuk;

    .line 366
    sget-object v0, Leuo;->a:[Leuo;

    iput-object v0, p0, Leun;->d:[Leuo;

    .line 244
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 395
    iget-object v0, p0, Leun;->b:Lexn;

    if-eqz v0, :cond_4

    .line 396
    const/4 v0, 0x1

    iget-object v2, p0, Leun;->b:Lexn;

    .line 397
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 399
    :goto_0
    iget-object v2, p0, Leun;->c:[Leuk;

    if-eqz v2, :cond_1

    .line 400
    iget-object v3, p0, Leun;->c:[Leuk;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 401
    if-eqz v5, :cond_0

    .line 402
    const/4 v6, 0x2

    .line 403
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 400
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 407
    :cond_1
    iget-object v2, p0, Leun;->d:[Leuo;

    if-eqz v2, :cond_3

    .line 408
    iget-object v2, p0, Leun;->d:[Leuo;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 409
    if-eqz v4, :cond_2

    .line 410
    const/4 v5, 0x3

    .line 411
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 408
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 415
    :cond_3
    iget-object v1, p0, Leun;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 416
    iput v0, p0, Leun;->cachedSize:I

    .line 417
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 240
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Leun;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Leun;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Leun;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Leun;->b:Lexn;

    if-nez v0, :cond_2

    new-instance v0, Lexn;

    invoke-direct {v0}, Lexn;-><init>()V

    iput-object v0, p0, Leun;->b:Lexn;

    :cond_2
    iget-object v0, p0, Leun;->b:Lexn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leun;->c:[Leuk;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leuk;

    iget-object v3, p0, Leun;->c:[Leuk;

    if-eqz v3, :cond_3

    iget-object v3, p0, Leun;->c:[Leuk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Leun;->c:[Leuk;

    :goto_2
    iget-object v2, p0, Leun;->c:[Leuk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Leun;->c:[Leuk;

    new-instance v3, Leuk;

    invoke-direct {v3}, Leuk;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leun;->c:[Leuk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Leun;->c:[Leuk;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Leun;->c:[Leuk;

    new-instance v3, Leuk;

    invoke-direct {v3}, Leuk;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leun;->c:[Leuk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leun;->d:[Leuo;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Leuo;

    iget-object v3, p0, Leun;->d:[Leuo;

    if-eqz v3, :cond_6

    iget-object v3, p0, Leun;->d:[Leuo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Leun;->d:[Leuo;

    :goto_4
    iget-object v2, p0, Leun;->d:[Leuo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Leun;->d:[Leuo;

    new-instance v3, Leuo;

    invoke-direct {v3}, Leuo;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leun;->d:[Leuo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Leun;->d:[Leuo;

    array-length v0, v0

    goto :goto_3

    :cond_8
    iget-object v2, p0, Leun;->d:[Leuo;

    new-instance v3, Leuo;

    invoke-direct {v3}, Leuo;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leun;->d:[Leuo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 371
    iget-object v1, p0, Leun;->b:Lexn;

    if-eqz v1, :cond_0

    .line 372
    const/4 v1, 0x1

    iget-object v2, p0, Leun;->b:Lexn;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 374
    :cond_0
    iget-object v1, p0, Leun;->c:[Leuk;

    if-eqz v1, :cond_2

    .line 375
    iget-object v2, p0, Leun;->c:[Leuk;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 376
    if-eqz v4, :cond_1

    .line 377
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 375
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 381
    :cond_2
    iget-object v1, p0, Leun;->d:[Leuo;

    if-eqz v1, :cond_4

    .line 382
    iget-object v1, p0, Leun;->d:[Leuo;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 383
    if-eqz v3, :cond_3

    .line 384
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 382
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 388
    :cond_4
    iget-object v0, p0, Leun;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 390
    return-void
.end method
