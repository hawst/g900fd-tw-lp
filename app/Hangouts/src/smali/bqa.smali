.class public final Lbqa;
.super Lbnj;
.source "PG"


# instance fields
.field private final a:[B


# direct methods
.method constructor <init>(Lyj;[B)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lbnj;-><init>(Lyj;)V

    .line 26
    iput-object p2, p0, Lbqa;->a:[B

    .line 27
    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 31
    const/4 v2, 0x0

    .line 32
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "babel_hangout_invite_responds_to_mesi"

    invoke-static {v3, v4, v1}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 35
    if-eq v3, v1, :cond_0

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lbqa;->b:Lyj;

    .line 39
    invoke-virtual {v3}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    const-string v4, "@google.com"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move v0, v1

    .line 40
    :cond_1
    if-eqz v0, :cond_3

    .line 41
    new-instance v0, Lbdw;

    iget-object v1, p0, Lbqa;->a:[B

    invoke-direct {v0, v1}, Lbdw;-><init>([B)V

    .line 54
    :goto_0
    if-eqz v0, :cond_2

    .line 55
    iget-object v1, p0, Lbqa;->c:Lbnl;

    invoke-virtual {v1, v0}, Lbnl;->a(Lbea;)V

    .line 57
    :cond_2
    return-void

    .line 44
    :cond_3
    :try_start_0
    new-instance v0, Ldzk;

    invoke-direct {v0}, Ldzk;-><init>()V

    iget-object v1, p0, Lbqa;->a:[B

    invoke-static {v0, v1}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldzk;

    .line 46
    new-instance v1, Lbfk;

    iget-object v3, v0, Ldzk;->c:Ljava/lang/String;

    iget-object v0, v0, Ldzk;->h:Ljava/lang/Integer;

    .line 48
    const/4 v4, 0x0

    invoke-static {v0, v4}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    invoke-direct {v1, v3, v0}, Lbfk;-><init>(Ljava/lang/String;I)V
    :try_end_0
    .catch Lepq; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 51
    goto :goto_0

    .line 49
    :catch_0
    move-exception v0

    .line 50
    const-string v1, "Babel"

    const-string v3, "Parse failed"

    invoke-static {v1, v3, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v2

    goto :goto_0
.end method
