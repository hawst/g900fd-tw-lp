.class public final Lpm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Intent;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:[Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/Float;

.field private i:Z

.field private j:Z

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Z

.field private p:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lpm;->a:Landroid/content/Intent;

    .line 140
    iput-boolean v1, p0, Lpm;->j:Z

    iput-boolean v1, p0, Lpm;->o:Z

    iput-boolean v1, p0, Lpm;->p:Z

    .line 141
    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Ljava/lang/Class;B)V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0, p1, p2}, Lpm;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public a()Lpm;
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x1

    iput-boolean v0, p0, Lpm;->o:Z

    .line 242
    return-object p0
.end method

.method public a(F)Lpm;
    .locals 1

    .prologue
    .line 201
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpm;->h:Ljava/lang/Float;

    .line 202
    return-object p0
.end method

.method public a(IIII)Lpm;
    .locals 1

    .prologue
    .line 228
    const/4 v0, 0x1

    iput-boolean v0, p0, Lpm;->j:Z

    .line 229
    iput p1, p0, Lpm;->k:I

    .line 230
    iput p2, p0, Lpm;->l:I

    .line 231
    iput p3, p0, Lpm;->m:I

    .line 232
    iput p4, p0, Lpm;->n:I

    .line 233
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lpm;
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lpm;->c:Ljava/lang/String;

    .line 164
    return-object p0
.end method

.method public a(Z)Lpm;
    .locals 0

    .prologue
    .line 255
    iput-boolean p1, p0, Lpm;->p:Z

    .line 256
    return-object p0
.end method

.method public a([Ljava/lang/String;)Lpm;
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lpm;->f:[Ljava/lang/String;

    .line 176
    return-object p0
.end method

.method public b()Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 261
    iget-object v0, p0, Lpm;->a:Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 263
    iget-object v0, p0, Lpm;->a:Landroid/content/Intent;

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 265
    iget-object v0, p0, Lpm;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lpm;->a:Landroid/content/Intent;

    const-string v1, "photo_index"

    iget-object v2, p0, Lpm;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 269
    :cond_0
    iget-object v0, p0, Lpm;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 270
    iget-object v0, p0, Lpm;->a:Landroid/content/Intent;

    const-string v1, "initial_photo_uri"

    iget-object v2, p0, Lpm;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 273
    :cond_1
    iget-object v0, p0, Lpm;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lpm;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 274
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "specified both photo index and photo uri"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 278
    :cond_2
    iget-object v0, p0, Lpm;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 279
    iget-object v0, p0, Lpm;->a:Landroid/content/Intent;

    const-string v1, "photos_uri"

    iget-object v2, p0, Lpm;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 282
    :cond_3
    iget-object v0, p0, Lpm;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 283
    iget-object v0, p0, Lpm;->a:Landroid/content/Intent;

    const-string v1, "resolved_photo_uri"

    iget-object v2, p0, Lpm;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 286
    :cond_4
    iget-object v0, p0, Lpm;->f:[Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 287
    iget-object v0, p0, Lpm;->a:Landroid/content/Intent;

    const-string v1, "projection"

    iget-object v2, p0, Lpm;->f:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 290
    :cond_5
    iget-object v0, p0, Lpm;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 291
    iget-object v0, p0, Lpm;->a:Landroid/content/Intent;

    const-string v1, "thumbnail_uri"

    iget-object v2, p0, Lpm;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 294
    :cond_6
    iget-object v0, p0, Lpm;->h:Ljava/lang/Float;

    if-eqz v0, :cond_7

    .line 295
    iget-object v0, p0, Lpm;->a:Landroid/content/Intent;

    const-string v1, "max_scale"

    iget-object v2, p0, Lpm;->h:Ljava/lang/Float;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 298
    :cond_7
    iget-boolean v0, p0, Lpm;->i:Z

    if-ne v0, v3, :cond_8

    .line 299
    iget-object v0, p0, Lpm;->a:Landroid/content/Intent;

    const-string v1, "watch_network"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 302
    :cond_8
    iget-object v0, p0, Lpm;->a:Landroid/content/Intent;

    const-string v1, "scale_up_animation"

    iget-boolean v2, p0, Lpm;->j:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 303
    iget-boolean v0, p0, Lpm;->j:Z

    if-eqz v0, :cond_9

    .line 304
    iget-object v0, p0, Lpm;->a:Landroid/content/Intent;

    const-string v1, "start_x_extra"

    iget v2, p0, Lpm;->k:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 305
    iget-object v0, p0, Lpm;->a:Landroid/content/Intent;

    const-string v1, "start_y_extra"

    iget v2, p0, Lpm;->l:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 306
    iget-object v0, p0, Lpm;->a:Landroid/content/Intent;

    const-string v1, "start_width_extra"

    iget v2, p0, Lpm;->m:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 307
    iget-object v0, p0, Lpm;->a:Landroid/content/Intent;

    const-string v1, "start_height_extra"

    iget v2, p0, Lpm;->n:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 310
    :cond_9
    iget-object v0, p0, Lpm;->a:Landroid/content/Intent;

    const-string v1, "action_bar_hidden_initially"

    iget-boolean v2, p0, Lpm;->o:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 311
    iget-object v0, p0, Lpm;->a:Landroid/content/Intent;

    const-string v1, "display_thumbs_fullscreen"

    iget-boolean v2, p0, Lpm;->p:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 313
    iget-object v0, p0, Lpm;->a:Landroid/content/Intent;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lpm;
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lpm;->d:Ljava/lang/String;

    .line 170
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lpm;
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lpm;->e:Ljava/lang/String;

    .line 186
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lpm;
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lpm;->g:Ljava/lang/String;

    .line 194
    return-object p0
.end method
