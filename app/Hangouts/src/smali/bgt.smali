.class public Lbgt;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ldxs;)V
    .locals 6

    .prologue
    .line 4856
    iget-object v0, p1, Ldxs;->b:Ldvn;

    const-wide/16 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 4858
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbgt;->g:Ljava/util/Map;

    .line 4859
    iget-object v1, p1, Ldxs;->c:[Ldxp;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 4860
    iget-object v4, v3, Ldxp;->b:Ldxq;

    .line 4863
    iget-object v5, v3, Ldxp;->c:Ljava/lang/String;

    if-eqz v5, :cond_0

    if-eqz v4, :cond_0

    iget-object v5, v4, Ldxq;->c:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 4865
    iget-object v3, v3, Ldxp;->c:Ljava/lang/String;

    .line 4866
    iget-object v4, v4, Ldxq;->c:Ljava/lang/String;

    .line 4867
    iget-object v5, p0, Lbgt;->g:Ljava/util/Map;

    invoke-interface {v5, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4859
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4870
    :cond_1
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 4874
    new-instance v0, Ldxs;

    invoke-direct {v0}, Ldxs;-><init>()V

    .line 4875
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldxs;

    .line 4876
    iget-object v1, v0, Ldxs;->b:Ldvn;

    invoke-static {v1}, Lbgt;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4877
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldxs;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 4879
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbgt;

    invoke-direct {v1, v0}, Lbgt;-><init>(Ldxs;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public f()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4884
    iget-object v0, p0, Lbgt;->g:Ljava/util/Map;

    return-object v0
.end method
