.class public final Lesp;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lesp;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Lesq;

.field public d:Lesj;

.field public e:Lfac;

.field public f:Lesr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lesp;

    sput-object v0, Lesp;->a:[Lesp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 13
    iput-object v0, p0, Lesp;->b:Ljava/lang/Integer;

    .line 16
    iput-object v0, p0, Lesp;->c:Lesq;

    .line 19
    iput-object v0, p0, Lesp;->d:Lesj;

    .line 22
    iput-object v0, p0, Lesp;->e:Lfac;

    .line 25
    iput-object v0, p0, Lesp;->f:Lesr;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 51
    const/4 v0, 0x0

    .line 52
    iget-object v1, p0, Lesp;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 53
    const/4 v0, 0x1

    iget-object v1, p0, Lesp;->b:Ljava/lang/Integer;

    .line 54
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 56
    :cond_0
    iget-object v1, p0, Lesp;->d:Lesj;

    if-eqz v1, :cond_1

    .line 57
    const/4 v1, 0x3

    iget-object v2, p0, Lesp;->d:Lesj;

    .line 58
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60
    :cond_1
    iget-object v1, p0, Lesp;->e:Lfac;

    if-eqz v1, :cond_2

    .line 61
    const/4 v1, 0x4

    iget-object v2, p0, Lesp;->e:Lfac;

    .line 62
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64
    :cond_2
    iget-object v1, p0, Lesp;->f:Lesr;

    if-eqz v1, :cond_3

    .line 65
    const/4 v1, 0x5

    iget-object v2, p0, Lesp;->f:Lesr;

    .line 66
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 68
    :cond_3
    iget-object v1, p0, Lesp;->c:Lesq;

    if-eqz v1, :cond_4

    .line 69
    const/4 v1, 0x6

    iget-object v2, p0, Lesp;->c:Lesq;

    .line 70
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    :cond_4
    iget-object v1, p0, Lesp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    iput v0, p0, Lesp;->cachedSize:I

    .line 74
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lesp;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lesp;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lesp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3e9

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3ea

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3eb

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3ec

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lesp;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lesp;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lesp;->d:Lesj;

    if-nez v0, :cond_4

    new-instance v0, Lesj;

    invoke-direct {v0}, Lesj;-><init>()V

    iput-object v0, p0, Lesp;->d:Lesj;

    :cond_4
    iget-object v0, p0, Lesp;->d:Lesj;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lesp;->e:Lfac;

    if-nez v0, :cond_5

    new-instance v0, Lfac;

    invoke-direct {v0}, Lfac;-><init>()V

    iput-object v0, p0, Lesp;->e:Lfac;

    :cond_5
    iget-object v0, p0, Lesp;->e:Lfac;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lesp;->f:Lesr;

    if-nez v0, :cond_6

    new-instance v0, Lesr;

    invoke-direct {v0}, Lesr;-><init>()V

    iput-object v0, p0, Lesp;->f:Lesr;

    :cond_6
    iget-object v0, p0, Lesp;->f:Lesr;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lesp;->c:Lesq;

    if-nez v0, :cond_7

    new-instance v0, Lesq;

    invoke-direct {v0}, Lesq;-><init>()V

    iput-object v0, p0, Lesp;->c:Lesq;

    :cond_7
    iget-object v0, p0, Lesp;->c:Lesq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lesp;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x1

    iget-object v1, p0, Lesp;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 33
    :cond_0
    iget-object v0, p0, Lesp;->d:Lesj;

    if-eqz v0, :cond_1

    .line 34
    const/4 v0, 0x3

    iget-object v1, p0, Lesp;->d:Lesj;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 36
    :cond_1
    iget-object v0, p0, Lesp;->e:Lfac;

    if-eqz v0, :cond_2

    .line 37
    const/4 v0, 0x4

    iget-object v1, p0, Lesp;->e:Lfac;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 39
    :cond_2
    iget-object v0, p0, Lesp;->f:Lesr;

    if-eqz v0, :cond_3

    .line 40
    const/4 v0, 0x5

    iget-object v1, p0, Lesp;->f:Lesr;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 42
    :cond_3
    iget-object v0, p0, Lesp;->c:Lesq;

    if-eqz v0, :cond_4

    .line 43
    const/4 v0, 0x6

    iget-object v1, p0, Lesp;->c:Lesq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 45
    :cond_4
    iget-object v0, p0, Lesp;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 47
    return-void
.end method
