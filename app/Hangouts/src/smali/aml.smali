.class public final Laml;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:[Ljava/lang/String;

.field public static final c:[Ljava/lang/String;

.field public static final d:[Ljava/lang/String;

.field public static final e:[Ljava/lang/String;

.field public static final f:[Ljava/lang/String;

.field private static final g:[Ljava/lang/String;


# instance fields
.field private final h:I

.field private i:Ljava/lang/String;

.field private j:Lbdk;

.field private k:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcvx;

.field private m:Lcwf;

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lamd;

.field private p:Lbak;

.field private q:Lbak;

.field private r:Lbak;

.field private s:Lbak;

.field private t:Lbak;

.field private u:Lbak;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 58
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "gaia_id"

    aput-object v1, v0, v5

    const-string v1, "chat_id"

    aput-object v1, v0, v6

    const-string v1, "name"

    aput-object v1, v0, v7

    const-string v1, "profile_photo_url"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "packed_circle_ids"

    aput-object v2, v0, v1

    sput-object v0, Laml;->a:[Ljava/lang/String;

    .line 73
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "contact_details"

    aput-object v1, v0, v4

    sput-object v0, Laml;->b:[Ljava/lang/String;

    .line 80
    sget-object v0, Laml;->a:[Ljava/lang/String;

    array-length v0, v0

    sget-object v1, Laml;->b:[Ljava/lang/String;

    array-length v1, v1

    add-int/2addr v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Laml;->c:[Ljava/lang/String;

    .line 83
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "location"

    aput-object v1, v0, v4

    const-string v1, "organization"

    aput-object v1, v0, v5

    const-string v1, "role"

    aput-object v1, v0, v6

    sput-object v0, Laml;->d:[Ljava/lang/String;

    .line 92
    sget-object v0, Laml;->c:[Ljava/lang/String;

    array-length v0, v0

    sget-object v1, Laml;->d:[Ljava/lang/String;

    array-length v1, v1

    add-int/2addr v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Laml;->e:[Ljava/lang/String;

    .line 96
    sget-object v0, Laml;->a:[Ljava/lang/String;

    sget-object v1, Laml;->c:[Ljava/lang/String;

    sget-object v2, Laml;->a:[Ljava/lang/String;

    array-length v2, v2

    invoke-static {v0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 98
    sget-object v0, Laml;->b:[Ljava/lang/String;

    sget-object v1, Laml;->c:[Ljava/lang/String;

    sget-object v2, Laml;->a:[Ljava/lang/String;

    array-length v2, v2

    sget-object v3, Laml;->b:[Ljava/lang/String;

    array-length v3, v3

    invoke-static {v0, v4, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 100
    sget-object v0, Laml;->c:[Ljava/lang/String;

    sget-object v1, Laml;->e:[Ljava/lang/String;

    sget-object v2, Laml;->c:[Ljava/lang/String;

    array-length v2, v2

    invoke-static {v0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 102
    sget-object v0, Laml;->d:[Ljava/lang/String;

    sget-object v1, Laml;->e:[Ljava/lang/String;

    sget-object v2, Laml;->c:[Ljava/lang/String;

    array-length v2, v2

    sget-object v3, Laml;->d:[Ljava/lang/String;

    array-length v3, v3

    invoke-static {v0, v4, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 117
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "circle_id"

    aput-object v1, v0, v5

    const-string v1, "circle_type"

    aput-object v1, v0, v6

    const-string v1, "name"

    aput-object v1, v0, v7

    const-string v1, "circle_member_count"

    aput-object v1, v0, v8

    sput-object v0, Laml;->f:[Ljava/lang/String;

    .line 131
    new-array v0, v8, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v0, v4

    const-string v1, "name"

    aput-object v1, v0, v5

    const-string v1, "avatar_url"

    aput-object v1, v0, v6

    const-string v1, "contact_details"

    aput-object v1, v0, v7

    sput-object v0, Laml;->g:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lamd;I)V
    .locals 0

    .prologue
    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    iput-object p1, p0, Laml;->o:Lamd;

    .line 177
    iput p2, p0, Laml;->h:I

    .line 178
    return-void
.end method

.method private a(Lcwf;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v8, 0x0

    .line 450
    new-instance v0, Lbak;

    sget-object v2, Laml;->c:[Ljava/lang/String;

    invoke-direct {v0, v2}, Lbak;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Laml;->s:Lbak;

    .line 451
    new-instance v0, Lbak;

    sget-object v2, Laml;->g:[Ljava/lang/String;

    invoke-direct {v0, v2}, Lbak;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Laml;->t:Lbak;

    .line 453
    iget v0, p0, Laml;->h:I

    invoke-static {v0}, Lf;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 489
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 458
    :goto_0
    invoke-virtual {p1}, Lcwf;->a()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 459
    invoke-virtual {p1, v0}, Lcwf;->b(I)Lcwe;

    move-result-object v4

    .line 461
    invoke-interface {v4}, Lcwe;->e()Ljava/lang/String;

    move-result-object v5

    .line 462
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "g:"

    invoke-virtual {v5, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_2
    move v2, v3

    :goto_1
    invoke-static {v2}, Lcwz;->a(Z)V

    .line 463
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 464
    iget-object v2, p0, Laml;->j:Lbdk;

    iget-object v2, v2, Lbdk;->a:Ljava/lang/String;

    invoke-static {v2, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 465
    const-string v2, "Babel"

    const-string v4, "Ignore current user from local contacts"

    invoke-static {v2, v4}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    :cond_3
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v2, v1

    .line 462
    goto :goto_1

    .line 469
    :cond_5
    invoke-static {v5, v8, v8, v8}, Lbcx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 470
    iget-object v6, p0, Laml;->k:Ljava/util/HashSet;

    invoke-virtual {v6, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 471
    iget-object v6, p0, Laml;->k:Ljava/util/HashSet;

    invoke-virtual {v6, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 475
    iget-object v2, p0, Laml;->s:Lbak;

    const/4 v6, 0x7

    new-array v6, v6, [Ljava/lang/Object;

    .line 476
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    aput-object v5, v6, v3

    const/4 v5, 0x2

    aput-object v8, v6, v5

    const/4 v5, 0x3

    .line 479
    invoke-interface {v4}, Lcwe;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v5

    const/4 v5, 0x4

    .line 480
    invoke-interface {v4}, Lcwe;->h()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v5

    const/4 v5, 0x5

    aput-object v8, v6, v5

    const/4 v5, 0x6

    aput-object v8, v6, v5

    .line 475
    invoke-virtual {v2, v6}, Lbak;->a([Ljava/lang/Object;)V

    .line 483
    iget-object v2, p0, Laml;->o:Lamd;

    invoke-interface {v4}, Lcwe;->g()Ljava/lang/String;

    move-result-object v5

    .line 484
    invoke-interface {v4}, Lcwe;->i()[Ljava/lang/String;

    move-result-object v4

    .line 483
    invoke-virtual {v2, v5, v4}, Lamd;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    goto :goto_2

    .line 486
    :cond_6
    const-string v2, "Babel"

    const-string v4, "Ignoring a circle person without a gaiaId"

    invoke-static {v2, v4}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private g()V
    .locals 14

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 330
    iget-object v0, p0, Laml;->s:Lbak;

    if-eqz v0, :cond_1

    .line 353
    :cond_0
    :goto_0
    return-void

    .line 335
    :cond_1
    iget-object v0, p0, Laml;->p:Lbak;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Laml;->l:Lcvx;

    if-eqz v0, :cond_f

    .line 342
    iget-object v7, p0, Laml;->l:Lcvx;

    new-instance v0, Lbak;

    sget-object v1, Laml;->c:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lbak;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Laml;->s:Lbak;

    new-instance v0, Lbak;

    sget-object v1, Laml;->g:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lbak;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Laml;->t:Lbak;

    move v2, v3

    :goto_1
    invoke-virtual {v7}, Lcvx;->a()I

    move-result v0

    if-ge v2, v0, :cond_e

    invoke-virtual {v7, v2}, Lcvx;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvw;

    invoke-interface {v0}, Lcvw;->e()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "g:"

    invoke-virtual {v6, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    :cond_2
    move v1, v4

    :goto_2
    invoke-static {v1}, Lcwz;->a(Z)V

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Laml;->j:Lbdk;

    iget-object v1, v1, Lbdk;->a:Ljava/lang/String;

    invoke-static {v1, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v0, "Babel"

    const-string v1, "Ignore current user from local contacts"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    move v1, v3

    goto :goto_2

    :cond_5
    invoke-static {v6, v5, v5, v5}, Lbcx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v8, p0, Laml;->k:Ljava/util/HashSet;

    invoke-virtual {v8, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    iget v1, p0, Laml;->h:I

    invoke-static {v1}, Lf;->c(I)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_6
    :goto_4
    invoke-interface {v0}, Lcvw;->f()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    invoke-static {v0}, Laea;->a(Lcvw;)Laea;

    move-result-object v1

    iget v8, p0, Laml;->h:I

    invoke-static {v8}, Lf;->c(I)Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-virtual {v1}, Laea;->l()Z

    move-result v8

    if-eqz v8, :cond_3

    :cond_7
    iget-object v8, p0, Laml;->s:Lbak;

    const/4 v9, 0x7

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v3

    aput-object v6, v9, v4

    aput-object v5, v9, v12

    invoke-interface {v0}, Lcvw;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v9, v11

    const/4 v6, 0x4

    invoke-interface {v0}, Lcvw;->h()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v6

    const/4 v6, 0x5

    aput-object v5, v9, v6

    const/4 v6, 0x6

    invoke-virtual {v1}, Laea;->l()Z

    move-result v10

    if-eqz v10, :cond_9

    invoke-virtual {v1}, Laea;->o()[B

    move-result-object v1

    :goto_5
    aput-object v1, v9, v6

    invoke-virtual {v8, v9}, Lbak;->a([Ljava/lang/Object;)V

    iget-object v1, p0, Laml;->o:Lamd;

    invoke-interface {v0}, Lcvw;->g()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0}, Lcvw;->i()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v6, v0}, Lamd;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    goto :goto_3

    :cond_8
    iget-object v8, p0, Laml;->k:Ljava/util/HashSet;

    invoke-virtual {v8, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_9
    move-object v1, v5

    goto :goto_5

    :cond_a
    invoke-interface {v0}, Lcvw;->b()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v1}, Lbrz;->a(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v1

    move-object v13, v1

    move-object v1, v6

    move-object v6, v13

    :goto_6
    if-nez v1, :cond_b

    invoke-interface {v0}, Lcvw;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_b

    const-string v0, "Babel"

    const-string v1, "Find a local contact with empty contact idand qualified id"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_b
    invoke-static {v0}, Laea;->a(Lcvw;)Laea;

    move-result-object v8

    invoke-virtual {v8}, Laea;->g()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-gtz v9, :cond_c

    const-string v1, "Babel"

    invoke-static {v1, v11}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "Skipped empty contact"

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lcvw;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_c
    iget v9, p0, Laml;->h:I

    invoke-static {v9}, Lf;->c(I)Z

    move-result v9

    if-eqz v9, :cond_d

    invoke-virtual {v8}, Laea;->l()Z

    move-result v9

    if-eqz v9, :cond_3

    :cond_d
    iget-object v9, p0, Laml;->t:Lbak;

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/Object;

    aput-object v1, v10, v3

    invoke-interface {v0}, Lcvw;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v10, v4

    aput-object v6, v10, v12

    invoke-virtual {v8}, Laea;->o()[B

    move-result-object v0

    aput-object v0, v10, v11

    invoke-virtual {v9, v10}, Lbak;->a([Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 343
    :cond_e
    iget-object v0, p0, Laml;->l:Lcvx;

    invoke-virtual {v0}, Lcvx;->d()V

    .line 344
    iput-object v5, p0, Laml;->l:Lcvx;

    goto/16 :goto_0

    .line 348
    :cond_f
    iget-object v0, p0, Laml;->m:Lcwf;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Laml;->m:Lcwf;

    invoke-direct {p0, v0}, Laml;->a(Lcwf;)V

    .line 350
    iget-object v0, p0, Laml;->m:Lcwf;

    invoke-virtual {v0}, Lcwf;->d()V

    .line 351
    iput-object v5, p0, Laml;->m:Lcwf;

    goto/16 :goto_0

    :cond_10
    move-object v6, v5

    move-object v1, v5

    goto/16 :goto_6
.end method

.method private h()V
    .locals 11

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 551
    iget-object v0, p0, Laml;->u:Lbak;

    if-eqz v0, :cond_1

    .line 615
    :cond_0
    return-void

    .line 556
    :cond_1
    iget-object v0, p0, Laml;->p:Lbak;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laml;->s:Lbak;

    if-eqz v0, :cond_0

    .line 562
    iget-object v0, p0, Laml;->n:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 569
    new-instance v0, Lbak;

    sget-object v1, Laml;->e:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lbak;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Laml;->u:Lbak;

    .line 570
    new-instance v0, Lbak;

    sget-object v1, Laml;->e:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lbak;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Laml;->q:Lbak;

    .line 571
    iget-object v0, p0, Laml;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    move v3, v2

    :cond_2
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 572
    if-nez v0, :cond_3

    .line 573
    const-string v0, "Babel"

    const-string v4, "Ignore invalid participant entity from server"

    invoke-static {v0, v4}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 577
    :cond_3
    iget-object v4, p0, Laml;->j:Lbdk;

    iget-object v5, v0, Lbdh;->b:Lbdk;

    invoke-virtual {v4, v5}, Lbdk;->a(Lbdk;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 578
    const-string v0, "Babel"

    const-string v4, "Ignore current user from public search results."

    invoke-static {v0, v4}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 582
    :cond_4
    iget-object v4, p0, Laml;->k:Ljava/util/HashSet;

    iget-object v5, v0, Lbdh;->b:Lbdk;

    invoke-virtual {v5}, Lbdk;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 583
    iget-boolean v4, v0, Lbdh;->v:Z

    if-eqz v4, :cond_5

    .line 592
    add-int/lit8 v5, v1, 0x1

    .line 593
    iget-object v4, p0, Laml;->q:Lbak;

    move v10, v1

    move v1, v5

    move v5, v10

    .line 599
    :goto_1
    const/16 v7, 0xa

    new-array v7, v7, [Ljava/lang/Object;

    .line 600
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v2

    const/4 v5, 0x1

    iget-object v8, v0, Lbdh;->b:Lbdk;

    iget-object v8, v8, Lbdk;->a:Ljava/lang/String;

    aput-object v8, v7, v5

    const/4 v5, 0x2

    iget-object v8, v0, Lbdh;->b:Lbdk;

    iget-object v8, v8, Lbdk;->b:Ljava/lang/String;

    aput-object v8, v7, v5

    const/4 v5, 0x3

    iget-object v8, v0, Lbdh;->e:Ljava/lang/String;

    aput-object v8, v7, v5

    const/4 v5, 0x4

    iget-object v8, v0, Lbdh;->h:Ljava/lang/String;

    aput-object v8, v7, v5

    const/4 v5, 0x5

    aput-object v9, v7, v5

    const/4 v5, 0x6

    aput-object v9, v7, v5

    const/4 v5, 0x7

    iget-object v8, v0, Lbdh;->j:Ljava/lang/String;

    aput-object v8, v7, v5

    const/16 v5, 0x8

    iget-object v8, v0, Lbdh;->k:Ljava/lang/String;

    aput-object v8, v7, v5

    const/16 v5, 0x9

    iget-object v0, v0, Lbdh;->l:Ljava/lang/String;

    aput-object v0, v7, v5

    .line 599
    invoke-virtual {v4, v7}, Lbak;->a([Ljava/lang/Object;)V

    goto :goto_0

    .line 595
    :cond_5
    add-int/lit8 v5, v3, 0x1

    .line 596
    iget-object v4, p0, Laml;->u:Lbak;

    move v10, v3

    move v3, v5

    move v5, v10

    goto :goto_1
.end method


# virtual methods
.method public a()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 249
    iget v0, p0, Laml;->h:I

    invoke-static {v0}, Lf;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    const/4 v0, 0x0

    .line 254
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Laml;->p:Lbak;

    goto :goto_0
.end method

.method public a(Landroid/database/Cursor;)Ljava/util/Set;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 209
    new-instance v0, Lbak;

    sget-object v1, Laml;->c:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lbak;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Laml;->p:Lbak;

    .line 210
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Laml;->k:Ljava/util/HashSet;

    .line 211
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 213
    :cond_0
    new-instance v0, Lbdk;

    .line 214
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 215
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    iget-object v1, p0, Laml;->j:Lbdk;

    invoke-virtual {v1, v0}, Lbdk;->a(Lbdk;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 217
    const-string v0, "Babel"

    const-string v1, "Current user should not be part of suggested people"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 242
    :cond_1
    invoke-direct {p0}, Laml;->g()V

    .line 243
    invoke-direct {p0}, Laml;->h()V

    .line 245
    iget-object v0, p0, Laml;->k:Ljava/util/HashSet;

    return-object v0

    .line 223
    :cond_2
    iget-object v1, p0, Laml;->k:Ljava/util/HashSet;

    invoke-virtual {v0}, Lbdk;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 224
    const-string v0, "Babel"

    const-string v1, "Ignore duplicated suggested people result"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 228
    :cond_3
    iget-object v1, p0, Laml;->k:Ljava/util/HashSet;

    invoke-virtual {v0}, Lbdk;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 229
    iget-object v0, p0, Laml;->p:Lbak;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    .line 230
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    .line 231
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    .line 232
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    .line 233
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    .line 234
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const/4 v3, 0x0

    aput-object v3, v1, v2

    .line 229
    invoke-virtual {v0, v1}, Lbak;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Ljava/util/List;)Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 517
    iput-object p1, p0, Laml;->n:Ljava/util/List;

    .line 519
    invoke-direct {p0}, Laml;->h()V

    .line 521
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Laml;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    if-nez v0, :cond_1

    const-string v0, "Babel"

    const-string v3, "Ignore invalid participant entity from server"

    invoke-static {v0, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v3, v0, Lbdh;->b:Lbdk;

    invoke-virtual {v3}, Lbdk;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Laml;->j:Lbdk;

    iget-object v0, v0, Lbdh;->b:Lbdk;

    invoke-virtual {v4, v0}, Lbdk;->a(Lbdk;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Laml;->k:Ljava/util/HashSet;

    if-eqz v0, :cond_2

    iget-object v0, p0, Laml;->k:Ljava/util/HashSet;

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    invoke-virtual {v1, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-object v1
.end method

.method public a(Lbdk;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Laml;->j:Lbdk;

    .line 182
    return-void
.end method

.method public a(Lcvx;Lcwf;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 302
    iget-object v1, p0, Laml;->l:Lcvx;

    if-eqz v1, :cond_0

    .line 303
    iget-object v1, p0, Laml;->l:Lcvx;

    invoke-virtual {v1}, Lcvx;->d()V

    .line 306
    :cond_0
    iget-object v1, p0, Laml;->m:Lcwf;

    if-eqz v1, :cond_1

    .line 307
    iget-object v1, p0, Laml;->m:Lcwf;

    invoke-virtual {v1}, Lcwf;->d()V

    .line 310
    :cond_1
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object p2, v0

    .line 316
    :goto_0
    iput-object v0, p0, Laml;->l:Lcvx;

    .line 317
    iput-object p2, p0, Laml;->m:Lcwf;

    .line 319
    invoke-direct {p0}, Laml;->g()V

    .line 322
    invoke-direct {p0}, Laml;->h()V

    .line 323
    return-void

    :cond_2
    move-object v0, p1

    goto :goto_0
.end method

.method public a(Lcvz;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 258
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 259
    new-instance v0, Lbak;

    sget-object v2, Laml;->f:[Ljava/lang/String;

    invoke-direct {v0, v2}, Lbak;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Laml;->r:Lbak;

    .line 261
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcvz;->a()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 262
    invoke-virtual {p1, v0}, Lcvz;->b(I)Lcvy;

    move-result-object v2

    .line 265
    invoke-interface {v2}, Lcvy;->c()I

    move-result v3

    if-lez v3, :cond_0

    .line 266
    invoke-interface {v2}, Lcvy;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 267
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 271
    add-int/lit16 v4, v0, 0x400

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 272
    invoke-interface {v2}, Lcvy;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 273
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 274
    invoke-interface {v2}, Lcvy;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 275
    invoke-interface {v2}, Lcvy;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 277
    iget-object v2, p0, Laml;->r:Lbak;

    invoke-virtual {v2, v3}, Lbak;->a(Ljava/lang/Iterable;)V

    .line 261
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 281
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 185
    iget-object v0, p0, Laml;->i:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    :goto_0
    return-void

    .line 189
    :cond_0
    iput-object p1, p0, Laml;->i:Ljava/lang/String;

    .line 191
    iput-object v1, p0, Laml;->k:Ljava/util/HashSet;

    .line 192
    iput-object v1, p0, Laml;->m:Lcwf;

    .line 193
    iput-object v1, p0, Laml;->n:Ljava/util/List;

    .line 195
    iput-object v1, p0, Laml;->p:Lbak;

    .line 196
    iput-object v1, p0, Laml;->q:Lbak;

    .line 197
    iput-object v1, p0, Laml;->s:Lbak;

    .line 198
    iput-object v1, p0, Laml;->t:Lbak;

    .line 199
    iput-object v1, p0, Laml;->u:Lbak;

    goto :goto_0
.end method

.method public b()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 284
    iget v0, p0, Laml;->h:I

    invoke-static {v0}, Lf;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    const/4 v0, 0x0

    .line 289
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Laml;->r:Lbak;

    goto :goto_0
.end method

.method public c()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 495
    iget-object v0, p0, Laml;->s:Lbak;

    if-nez v0, :cond_0

    .line 496
    invoke-direct {p0}, Laml;->g()V

    .line 499
    :cond_0
    iget-object v0, p0, Laml;->s:Lbak;

    return-object v0
.end method

.method public d()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 506
    iget-object v0, p0, Laml;->t:Lbak;

    if-nez v0, :cond_0

    .line 507
    invoke-direct {p0}, Laml;->g()V

    .line 510
    :cond_0
    iget-object v0, p0, Laml;->t:Lbak;

    return-object v0
.end method

.method public e()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 621
    iget v0, p0, Laml;->h:I

    invoke-static {v0}, Lf;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 623
    const/4 v0, 0x0

    .line 629
    :goto_0
    return-object v0

    .line 625
    :cond_0
    iget-object v0, p0, Laml;->q:Lbak;

    if-nez v0, :cond_1

    .line 627
    invoke-direct {p0}, Laml;->h()V

    .line 629
    :cond_1
    iget-object v0, p0, Laml;->q:Lbak;

    goto :goto_0
.end method

.method public f()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 636
    iget v0, p0, Laml;->h:I

    invoke-static {v0}, Lf;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 638
    const/4 v0, 0x0

    .line 645
    :goto_0
    return-object v0

    .line 641
    :cond_0
    iget-object v0, p0, Laml;->u:Lbak;

    if-nez v0, :cond_1

    .line 642
    invoke-direct {p0}, Laml;->h()V

    .line 645
    :cond_1
    iget-object v0, p0, Laml;->u:Lbak;

    goto :goto_0
.end method
