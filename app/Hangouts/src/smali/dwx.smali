.class public final Ldwx;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldwx;


# instance fields
.field public b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25033
    const/4 v0, 0x0

    new-array v0, v0, [Ldwx;

    sput-object v0, Ldwx;->a:[Ldwx;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25034
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 2

    .prologue
    .line 25050
    const/4 v0, 0x0

    .line 25051
    iget-object v1, p0, Ldwx;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 25052
    const/4 v0, 0x1

    iget-object v1, p0, Ldwx;->b:Ljava/lang/String;

    .line 25053
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 25055
    :cond_0
    iget-object v1, p0, Ldwx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25056
    iput v0, p0, Ldwx;->cachedSize:I

    .line 25057
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 25030
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldwx;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldwx;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldwx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldwx;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 25041
    iget-object v0, p0, Ldwx;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 25042
    const/4 v0, 0x1

    iget-object v1, p0, Ldwx;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 25044
    :cond_0
    iget-object v0, p0, Ldwx;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 25046
    return-void
.end method
