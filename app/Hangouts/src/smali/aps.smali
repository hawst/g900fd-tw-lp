.class public final Laps;
.super Ls;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ls;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;I)Laps;
    .locals 3

    .prologue
    .line 27
    new-instance v0, Laps;

    invoke-direct {v0}, Laps;-><init>()V

    .line 30
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 31
    const-string v2, "key_error_message"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    const-string v2, "KEY_ERROR"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 33
    invoke-virtual {v0, v1}, Laps;->setArguments(Landroid/os/Bundle;)V

    .line 34
    return-object v0
.end method


# virtual methods
.method public f()Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 39
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Laps;->getActivity()Ly;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 40
    invoke-virtual {p0}, Laps;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v0, "key_error_message"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 41
    invoke-virtual {p0}, Laps;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 42
    sget v2, Lh;->hZ:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 43
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 40
    :cond_0
    const-string v0, "KEY_ERROR"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    sget v0, Lh;->dY:I

    sparse-switch v2, :sswitch_data_0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown/unexpected error code:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcwz;->a(Ljava/lang/String;)V

    :goto_1
    :sswitch_0
    invoke-virtual {p0}, Laps;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_1
    sget v0, Lh;->dS:I

    goto :goto_1

    :sswitch_2
    sget v0, Lh;->fg:I

    goto :goto_1

    :sswitch_3
    sget v0, Lh;->dQ:I

    goto :goto_1

    :sswitch_4
    sget v0, Lh;->dR:I

    goto :goto_1

    :sswitch_5
    sget v0, Lh;->dK:I

    goto :goto_1

    :sswitch_6
    sget v0, Lh;->dL:I

    goto :goto_1

    :sswitch_7
    sget v0, Lh;->dN:I

    goto :goto_1

    :sswitch_8
    sget v0, Lh;->dP:I

    goto :goto_1

    :sswitch_9
    const-string v2, "This should be handled differently"

    invoke-static {v2}, Lcwz;->a(Ljava/lang/String;)V

    goto :goto_1

    :sswitch_a
    sget v0, Lh;->dM:I

    goto :goto_1

    :sswitch_b
    sget v0, Lh;->ea:I

    goto :goto_1

    :sswitch_c
    sget v0, Lh;->dG:I

    goto :goto_1

    :sswitch_d
    sget v0, Lh;->dO:I

    goto :goto_1

    :sswitch_e
    sget v0, Lh;->dT:I

    goto :goto_1

    :sswitch_f
    sget v0, Lh;->dZ:I

    goto :goto_1

    :sswitch_10
    sget v0, Lh;->dU:I

    goto :goto_1

    :sswitch_11
    sget v0, Lh;->fr:I

    goto :goto_1

    :sswitch_12
    sget v0, Lh;->eb:I

    goto :goto_1

    :sswitch_13
    sget v0, Lh;->fg:I

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_4
        0x5 -> :sswitch_4
        0x6 -> :sswitch_4
        0x7 -> :sswitch_4
        0x8 -> :sswitch_4
        0x9 -> :sswitch_4
        0xa -> :sswitch_4
        0xb -> :sswitch_4
        0xc -> :sswitch_5
        0xd -> :sswitch_6
        0xe -> :sswitch_7
        0xf -> :sswitch_11
        0x10 -> :sswitch_1
        0x11 -> :sswitch_8
        0x12 -> :sswitch_8
        0x13 -> :sswitch_9
        0x14 -> :sswitch_a
        0x15 -> :sswitch_12
        0x16 -> :sswitch_12
        0x18 -> :sswitch_b
        0x1a -> :sswitch_0
        0x3e8 -> :sswitch_c
        0x3e9 -> :sswitch_d
        0x3ea -> :sswitch_e
        0x3eb -> :sswitch_12
        0x3ee -> :sswitch_f
        0x3ef -> :sswitch_0
        0x3f0 -> :sswitch_10
        0x3f1 -> :sswitch_11
        0x3f4 -> :sswitch_13
        0x3f5 -> :sswitch_3
        0x3f6 -> :sswitch_0
    .end sparse-switch
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Laps;->getActivity()Ly;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    .line 56
    if-eqz v0, :cond_0

    .line 57
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->p()V

    .line 59
    :cond_0
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 48
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 49
    invoke-virtual {p0, p1}, Laps;->onCancel(Landroid/content/DialogInterface;)V

    .line 50
    invoke-virtual {p0}, Laps;->a()V

    .line 51
    return-void
.end method
