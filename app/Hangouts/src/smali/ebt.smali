.class public abstract enum Lebt;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lebt;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lebt;

.field public static final enum b:Lebt;

.field public static final enum c:Lebt;

.field public static final enum d:Lebt;

.field public static final enum e:Lebt;

.field public static final enum f:Lebt;

.field public static final enum g:Lebt;

.field public static final enum h:Lebt;

.field static final i:[Lebt;

.field private static final synthetic j:[Lebt;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 441
    new-instance v0, Lebu;

    const-string v1, "STRONG"

    invoke-direct {v0, v1}, Lebu;-><init>(Ljava/lang/String;)V

    sput-object v0, Lebt;->a:Lebt;

    .line 449
    new-instance v0, Lebv;

    const-string v1, "STRONG_ACCESS"

    invoke-direct {v0, v1}, Lebv;-><init>(Ljava/lang/String;)V

    sput-object v0, Lebt;->b:Lebt;

    .line 465
    new-instance v0, Lebw;

    const-string v1, "STRONG_WRITE"

    invoke-direct {v0, v1}, Lebw;-><init>(Ljava/lang/String;)V

    sput-object v0, Lebt;->c:Lebt;

    .line 481
    new-instance v0, Lebx;

    const-string v1, "STRONG_ACCESS_WRITE"

    invoke-direct {v0, v1}, Lebx;-><init>(Ljava/lang/String;)V

    sput-object v0, Lebt;->d:Lebt;

    .line 499
    new-instance v0, Leby;

    const-string v1, "WEAK"

    invoke-direct {v0, v1}, Leby;-><init>(Ljava/lang/String;)V

    sput-object v0, Lebt;->e:Lebt;

    .line 507
    new-instance v0, Lebz;

    const-string v1, "WEAK_ACCESS"

    invoke-direct {v0, v1}, Lebz;-><init>(Ljava/lang/String;)V

    sput-object v0, Lebt;->f:Lebt;

    .line 523
    new-instance v0, Leca;

    const-string v1, "WEAK_WRITE"

    invoke-direct {v0, v1}, Leca;-><init>(Ljava/lang/String;)V

    sput-object v0, Lebt;->g:Lebt;

    .line 539
    new-instance v0, Lecb;

    const-string v1, "WEAK_ACCESS_WRITE"

    invoke-direct {v0, v1}, Lecb;-><init>(Ljava/lang/String;)V

    sput-object v0, Lebt;->h:Lebt;

    .line 440
    const/16 v0, 0x8

    new-array v0, v0, [Lebt;

    sget-object v1, Lebt;->a:Lebt;

    aput-object v1, v0, v3

    sget-object v1, Lebt;->b:Lebt;

    aput-object v1, v0, v4

    sget-object v1, Lebt;->c:Lebt;

    aput-object v1, v0, v5

    sget-object v1, Lebt;->d:Lebt;

    aput-object v1, v0, v6

    sget-object v1, Lebt;->e:Lebt;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lebt;->f:Lebt;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lebt;->g:Lebt;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lebt;->h:Lebt;

    aput-object v2, v0, v1

    sput-object v0, Lebt;->j:[Lebt;

    .line 567
    const/16 v0, 0x8

    new-array v0, v0, [Lebt;

    sget-object v1, Lebt;->a:Lebt;

    aput-object v1, v0, v3

    sget-object v1, Lebt;->b:Lebt;

    aput-object v1, v0, v4

    sget-object v1, Lebt;->c:Lebt;

    aput-object v1, v0, v5

    sget-object v1, Lebt;->d:Lebt;

    aput-object v1, v0, v6

    sget-object v1, Lebt;->e:Lebt;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lebt;->f:Lebt;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lebt;->g:Lebt;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lebt;->h:Lebt;

    aput-object v2, v0, v1

    sput-object v0, Lebt;->i:[Lebt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 440
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 440
    invoke-direct {p0, p1, p2}, Lebt;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lebt;
    .locals 1

    .prologue
    .line 440
    const-class v0, Lebt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lebt;

    return-object v0
.end method

.method public static values()[Lebt;
    .locals 1

    .prologue
    .line 440
    sget-object v0, Lebt;->j:[Lebt;

    invoke-virtual {v0}, [Lebt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lebt;

    return-object v0
.end method


# virtual methods
.method a(Lecj;Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;)Lcom/google/common/cache/LocalCache$ReferenceEntry;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lecj",
            "<TK;TV;>;",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;)",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 597
    invoke-interface {p2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getHash()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, p3}, Lebt;->a(Lecj;Ljava/lang/Object;ILcom/google/common/cache/LocalCache$ReferenceEntry;)Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    return-object v0
.end method

.method abstract a(Lecj;Ljava/lang/Object;ILcom/google/common/cache/LocalCache$ReferenceEntry;)Lcom/google/common/cache/LocalCache$ReferenceEntry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lecj",
            "<TK;TV;>;TK;I",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;)",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 604
    invoke-interface {p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getAccessTime()J

    move-result-wide v0

    invoke-interface {p2, v0, v1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->setAccessTime(J)V

    .line 606
    invoke-interface {p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getPreviousInAccessQueue()Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/common/cache/LocalCache;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;)V

    .line 607
    invoke-interface {p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNextInAccessQueue()Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/common/cache/LocalCache;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;)V

    .line 609
    invoke-static {p1}, Lcom/google/common/cache/LocalCache;->b(Lcom/google/common/cache/LocalCache$ReferenceEntry;)V

    .line 610
    return-void
.end method

.method b(Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 616
    invoke-interface {p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getWriteTime()J

    move-result-wide v0

    invoke-interface {p2, v0, v1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->setWriteTime(J)V

    .line 618
    invoke-interface {p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getPreviousInWriteQueue()Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/common/cache/LocalCache;->b(Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;)V

    .line 619
    invoke-interface {p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNextInWriteQueue()Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/common/cache/LocalCache;->b(Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;)V

    .line 621
    invoke-static {p1}, Lcom/google/common/cache/LocalCache;->c(Lcom/google/common/cache/LocalCache$ReferenceEntry;)V

    .line 622
    return-void
.end method
