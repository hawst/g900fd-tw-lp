.class public abstract Lacp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Laci;


# static fields
.field private static f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lacr;",
            ">;"
        }
    .end annotation
.end field

.field private static g:Lacs;

.field private static h:Ljava/lang/Object;


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:Ljava/lang/String;

.field protected c:Ljava/lang/String;

.field protected d:Lacj;

.field protected final e:Lacr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lacp;->f:Ljava/util/Map;

    .line 34
    const/4 v0, 0x0

    sput-object v0, Lacp;->g:Lacs;

    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lacp;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Lacq;

    invoke-direct {v0, p0}, Lacq;-><init>(Lacp;)V

    iput-object v0, p0, Lacp;->e:Lacr;

    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, Lacp;->d:Lacj;

    .line 127
    iput-object p1, p0, Lacp;->b:Ljava/lang/String;

    .line 128
    iput-object p2, p0, Lacp;->c:Ljava/lang/String;

    .line 129
    return-void
.end method

.method static synthetic a(Ljava/lang/String;)Lacr;
    .locals 1

    .prologue
    .line 26
    invoke-static {p0}, Lacp;->b(Ljava/lang/String;)Lacr;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Lacr;
    .locals 2

    .prologue
    .line 89
    sget-object v1, Lacp;->f:Ljava/util/Map;

    monitor-enter v1

    .line 90
    :try_start_0
    sget-object v0, Lacp;->f:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacr;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic i()V
    .locals 3

    .prologue
    .line 26
    sget-object v1, Lacp;->f:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lacp;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    sget-object v1, Lacp;->h:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    sget-object v0, Lacp;->g:Lacs;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    sget-object v2, Lacp;->g:Lacs;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    sput-object v0, Lacp;->g:Lacs;

    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(Lacj;)V
    .locals 0

    .prologue
    .line 203
    iput-object p1, p0, Lacp;->d:Lacj;

    .line 204
    return-void
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lacp;->d:Lacj;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lacp;->d:Lacj;

    invoke-interface {v0, p0}, Lacj;->a(Laci;)V

    .line 193
    :cond_0
    return-void
.end method

.method protected e()V
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lacp;->d:Lacj;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lacp;->d:Lacj;

    invoke-interface {v0, p0}, Lacj;->b(Laci;)V

    .line 199
    :cond_0
    return-void
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 207
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    .line 208
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 209
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()V
    .locals 6

    .prologue
    .line 134
    invoke-virtual {p0}, Lacp;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 137
    sget-object v1, Lacp;->h:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v2, Lacp;->g:Lacs;

    if-nez v2, :cond_0

    new-instance v2, Lacs;

    invoke-direct {v2}, Lacs;-><init>()V

    sput-object v2, Lacp;->g:Lacs;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lacp;->g:Lacs;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "com.google.android.apps.hangouts.SMS_STATUS"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lacp;->a:Ljava/lang/String;

    .line 144
    iget-object v1, p0, Lacp;->a:Ljava/lang/String;

    iget-object v2, p0, Lacp;->e:Lacr;

    sget-object v3, Lacp;->f:Ljava/util/Map;

    monitor-enter v3

    :try_start_1
    sget-object v4, Lacp;->f:Ljava/util/Map;

    invoke-interface {v4, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 146
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.apps.hangouts.SMS_STATUS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 147
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 148
    const-string v2, "request_id"

    iget-object v3, p0, Lacp;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 149
    const/4 v2, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v0, v2, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 152
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    .line 155
    :try_start_2
    iget-object v1, p0, Lacp;->b:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lacp;->c:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/os/BadParcelableException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 182
    :goto_0
    return-void

    .line 137
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 144
    :catchall_1
    move-exception v0

    monitor-exit v3

    throw v0

    .line 173
    :catch_0
    move-exception v0

    .line 172
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected telephony error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 174
    invoke-virtual {p0}, Lacp;->e()V

    goto :goto_0

    .line 161
    :catch_1
    move-exception v0

    .line 172
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected telephony error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 174
    invoke-virtual {p0}, Lacp;->e()V

    goto :goto_0

    .line 163
    :catch_2
    move-exception v0

    .line 172
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected telephony error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 174
    invoke-virtual {p0}, Lacp;->e()V

    goto :goto_0

    .line 165
    :catch_3
    move-exception v0

    .line 172
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected telephony error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 174
    invoke-virtual {p0}, Lacp;->e()V

    goto :goto_0

    .line 167
    :catch_4
    move-exception v0

    .line 172
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected telephony error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 174
    invoke-virtual {p0}, Lacp;->e()V

    goto/16 :goto_0

    .line 172
    :catchall_2
    move-exception v0

    throw v0

    .line 177
    :cond_1
    const-string v0, "Babel"

    const-string v1, "No telephony for sending offnetwork invite sms"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-virtual {p0}, Lacp;->e()V

    goto/16 :goto_0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lacp;->a:Ljava/lang/String;

    invoke-static {v0}, Lacp;->b(Ljava/lang/String;)Lacr;

    .line 187
    return-void
.end method
