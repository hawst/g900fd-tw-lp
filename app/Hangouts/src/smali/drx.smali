.class public final Ldrx;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldrx;


# instance fields
.field public b:Ldqf;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/Integer;

.field public e:Ldqw;

.field public f:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 879
    const/4 v0, 0x0

    new-array v0, v0, [Ldrx;

    sput-object v0, Ldrx;->a:[Ldrx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 880
    invoke-direct {p0}, Lepn;-><init>()V

    .line 883
    iput-object v0, p0, Ldrx;->b:Ldqf;

    .line 888
    iput-object v0, p0, Ldrx;->d:Ljava/lang/Integer;

    .line 891
    iput-object v0, p0, Ldrx;->e:Ldqw;

    .line 894
    iput-object v0, p0, Ldrx;->f:Ljava/lang/Integer;

    .line 880
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 920
    const/4 v0, 0x0

    .line 921
    iget-object v1, p0, Ldrx;->b:Ldqf;

    if-eqz v1, :cond_0

    .line 922
    const/4 v0, 0x1

    iget-object v1, p0, Ldrx;->b:Ldqf;

    .line 923
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 925
    :cond_0
    iget-object v1, p0, Ldrx;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 926
    const/4 v1, 0x2

    iget-object v2, p0, Ldrx;->c:Ljava/lang/Long;

    .line 927
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 929
    :cond_1
    iget-object v1, p0, Ldrx;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 930
    const/4 v1, 0x3

    iget-object v2, p0, Ldrx;->d:Ljava/lang/Integer;

    .line 931
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 933
    :cond_2
    iget-object v1, p0, Ldrx;->e:Ldqw;

    if-eqz v1, :cond_3

    .line 934
    const/4 v1, 0x4

    iget-object v2, p0, Ldrx;->e:Ldqw;

    .line 935
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 937
    :cond_3
    iget-object v1, p0, Ldrx;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 938
    const/4 v1, 0x5

    iget-object v2, p0, Ldrx;->f:Ljava/lang/Integer;

    .line 939
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 941
    :cond_4
    iget-object v1, p0, Ldrx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 942
    iput v0, p0, Ldrx;->cachedSize:I

    .line 943
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 876
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldrx;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldrx;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldrx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldrx;->b:Ldqf;

    if-nez v0, :cond_2

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldrx;->b:Ldqf;

    :cond_2
    iget-object v0, p0, Ldrx;->b:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldrx;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_3

    if-ne v0, v3, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldrx;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldrx;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldrx;->e:Ldqw;

    if-nez v0, :cond_5

    new-instance v0, Ldqw;

    invoke-direct {v0}, Ldqw;-><init>()V

    iput-object v0, p0, Ldrx;->e:Ldqw;

    :cond_5
    iget-object v0, p0, Ldrx;->e:Ldqw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_6

    if-eq v0, v2, :cond_6

    if-eq v0, v3, :cond_6

    const/16 v1, 0xb

    if-eq v0, v1, :cond_6

    const/4 v1, 0x3

    if-eq v0, v1, :cond_6

    const/4 v1, 0x4

    if-eq v0, v1, :cond_6

    const/4 v1, 0x5

    if-eq v0, v1, :cond_6

    const/4 v1, 0x6

    if-eq v0, v1, :cond_6

    const/4 v1, 0x7

    if-eq v0, v1, :cond_6

    const/16 v1, 0x8

    if-eq v0, v1, :cond_6

    const/16 v1, 0x9

    if-eq v0, v1, :cond_6

    const/16 v1, 0xa

    if-ne v0, v1, :cond_7

    :cond_6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldrx;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_7
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldrx;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 899
    iget-object v0, p0, Ldrx;->b:Ldqf;

    if-eqz v0, :cond_0

    .line 900
    const/4 v0, 0x1

    iget-object v1, p0, Ldrx;->b:Ldqf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 902
    :cond_0
    iget-object v0, p0, Ldrx;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 903
    const/4 v0, 0x2

    iget-object v1, p0, Ldrx;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 905
    :cond_1
    iget-object v0, p0, Ldrx;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 906
    const/4 v0, 0x3

    iget-object v1, p0, Ldrx;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 908
    :cond_2
    iget-object v0, p0, Ldrx;->e:Ldqw;

    if-eqz v0, :cond_3

    .line 909
    const/4 v0, 0x4

    iget-object v1, p0, Ldrx;->e:Ldqw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 911
    :cond_3
    iget-object v0, p0, Ldrx;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 912
    const/4 v0, 0x5

    iget-object v1, p0, Ldrx;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 914
    :cond_4
    iget-object v0, p0, Ldrx;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 916
    return-void
.end method
