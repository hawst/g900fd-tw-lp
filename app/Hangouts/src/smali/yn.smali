.class public final Lyn;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Z

.field private static b:I

.field private static c:I

.field private static d:I

.field private static e:I

.field private static f:Landroid/graphics/Bitmap;

.field private static g:Landroid/graphics/Bitmap;

.field private static h:Landroid/graphics/Bitmap;

.field private static i:Landroid/graphics/Bitmap;

.field private static j:Landroid/graphics/Bitmap;

.field private static k:Landroid/graphics/Bitmap;

.field private static l:Landroid/graphics/Bitmap;

.field private static m:Landroid/graphics/Bitmap;

.field private static n:Landroid/graphics/Bitmap;

.field private static o:Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lbys;->c:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lyn;->a:Z

    return-void
.end method

.method public static a()I
    .locals 2

    .prologue
    .line 55
    sget v0, Lyn;->b:I

    if-nez v0, :cond_0

    .line 56
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->dB:I

    .line 57
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lyn;->b:I

    .line 59
    :cond_0
    sget v0, Lyn;->b:I

    return v0
.end method

.method public static a(Lyj;Ljava/util/List;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyj;",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 208
    if-nez p1, :cond_1

    .line 230
    :cond_0
    return-void

    .line 212
    :cond_1
    if-eqz p2, :cond_3

    invoke-static {}, Lyn;->f()I

    move-result v0

    move v1, v0

    .line 214
    :goto_0
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 215
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    iget-object v0, v0, Lbdh;->h:Ljava/lang/String;

    .line 216
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 217
    sget-boolean v0, Lyn;->a:Z

    if-eqz v0, :cond_2

    .line 218
    const-string v3, "Babel_medialoader"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "Avatar url for contact is empty: "

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 219
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    iget-object v0, v0, Lbdh;->e:Ljava/lang/String;

    invoke-static {v0}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 218
    invoke-static {v3, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :cond_2
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 212
    :cond_3
    invoke-static {}, Lyn;->b()I

    move-result v0

    move v1, v0

    goto :goto_0

    .line 224
    :cond_4
    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v3

    new-instance v4, Lzx;

    new-instance v5, Lbyq;

    invoke-direct {v5, v0, p0}, Lbyq;-><init>(Ljava/lang/String;Lyj;)V

    .line 225
    invoke-virtual {v5, v1}, Lbyq;->a(I)Lbyq;

    move-result-object v0

    .line 226
    invoke-virtual {v0, v6}, Lbyq;->d(Z)Lbyq;

    move-result-object v0

    invoke-direct {v4, v0, v7, v6, v7}, Lzx;-><init>(Lbyq;Laac;ZLjava/lang/Object;)V

    .line 224
    invoke-virtual {v3, v4}, Lbsn;->c(Lbrv;)V

    goto :goto_2
.end method

.method public static final a(Landroid/graphics/Bitmap;)Z
    .locals 1

    .prologue
    .line 233
    if-eqz p0, :cond_1

    sget-object v0, Lyn;->f:Landroid/graphics/Bitmap;

    if-eq p0, v0, :cond_0

    sget-object v0, Lyn;->g:Landroid/graphics/Bitmap;

    if-eq p0, v0, :cond_0

    sget-object v0, Lyn;->h:Landroid/graphics/Bitmap;

    if-eq p0, v0, :cond_0

    sget-object v0, Lyn;->i:Landroid/graphics/Bitmap;

    if-eq p0, v0, :cond_0

    sget-object v0, Lyn;->j:Landroid/graphics/Bitmap;

    if-eq p0, v0, :cond_0

    sget-object v0, Lyn;->k:Landroid/graphics/Bitmap;

    if-eq p0, v0, :cond_0

    sget-object v0, Lyn;->l:Landroid/graphics/Bitmap;

    if-eq p0, v0, :cond_0

    sget-object v0, Lyn;->m:Landroid/graphics/Bitmap;

    if-eq p0, v0, :cond_0

    sget-object v0, Lyn;->n:Landroid/graphics/Bitmap;

    if-eq p0, v0, :cond_0

    sget-object v0, Lyn;->o:Landroid/graphics/Bitmap;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()I
    .locals 2

    .prologue
    .line 66
    sget v0, Lyn;->c:I

    if-nez v0, :cond_0

    .line 67
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->dl:I

    .line 68
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lyn;->c:I

    .line 70
    :cond_0
    sget v0, Lyn;->c:I

    return v0
.end method

.method public static c()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lyn;->f:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 77
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->j:I

    invoke-static {v0}, Lbyr;->a(I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lyn;->f:Landroid/graphics/Bitmap;

    .line 79
    :cond_0
    sget-object v0, Lyn;->f:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static d()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 84
    sget-object v0, Lyn;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 86
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->j:I

    .line 85
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lyn;->g:Landroid/graphics/Bitmap;

    .line 88
    :cond_0
    sget-object v0, Lyn;->g:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static e()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 94
    invoke-static {}, Lyn;->c()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static f()I
    .locals 2

    .prologue
    .line 99
    sget v0, Lyn;->d:I

    if-nez v0, :cond_0

    .line 100
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->dj:I

    .line 101
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lyn;->d:I

    .line 103
    :cond_0
    sget v0, Lyn;->d:I

    return v0
.end method

.method public static g()I
    .locals 2

    .prologue
    .line 108
    sget v0, Lyn;->e:I

    if-nez v0, :cond_0

    .line 109
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->da:I

    .line 110
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lyn;->e:I

    .line 112
    :cond_0
    sget v0, Lyn;->e:I

    return v0
.end method

.method public static h()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lyn;->h:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 118
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->l:I

    invoke-static {v0}, Lbyr;->a(I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lyn;->h:Landroid/graphics/Bitmap;

    .line 120
    :cond_0
    sget-object v0, Lyn;->h:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static i()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 125
    sget-object v0, Lyn;->i:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 127
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->l:I

    .line 126
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lyn;->i:Landroid/graphics/Bitmap;

    .line 129
    :cond_0
    sget-object v0, Lyn;->i:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static j()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 134
    invoke-static {}, Lyn;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static k()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 139
    invoke-static {}, Lyn;->i()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static l()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 144
    sget-object v0, Lyn;->j:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 145
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->m:I

    invoke-static {v0}, Lbyr;->a(I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lyn;->j:Landroid/graphics/Bitmap;

    .line 147
    :cond_0
    sget-object v0, Lyn;->j:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static m()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 152
    sget-object v0, Lyn;->k:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 154
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->m:I

    .line 153
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lyn;->k:Landroid/graphics/Bitmap;

    .line 156
    :cond_0
    sget-object v0, Lyn;->k:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static n()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 161
    sget-object v0, Lyn;->l:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 162
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->n:I

    invoke-static {v0}, Lbyr;->a(I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lyn;->l:Landroid/graphics/Bitmap;

    .line 165
    :cond_0
    sget-object v0, Lyn;->l:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static o()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 170
    sget-object v0, Lyn;->m:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 172
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->n:I

    .line 171
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lyn;->m:Landroid/graphics/Bitmap;

    .line 174
    :cond_0
    sget-object v0, Lyn;->m:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static p()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 179
    sget-object v0, Lyn;->n:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 180
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->k:I

    invoke-static {v0}, Lbyr;->a(I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lyn;->n:Landroid/graphics/Bitmap;

    .line 182
    :cond_0
    sget-object v0, Lyn;->n:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static q()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 187
    sget-object v0, Lyn;->o:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 188
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->cL:I

    invoke-static {v0}, Lbyr;->a(I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lyn;->o:Landroid/graphics/Bitmap;

    .line 190
    :cond_0
    sget-object v0, Lyn;->o:Landroid/graphics/Bitmap;

    return-object v0
.end method
