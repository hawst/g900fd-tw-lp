.class public final Ldbv;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ldbw;

.field private static final b:Ldbw;

.field private static final c:Ldbw;

.field private static final d:Ldbw;

.field private static final e:Ldbw;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 28
    new-instance v0, Ldbx;

    const-string v1, "-_.*"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Ldbx;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Ldbv;->a:Ldbw;

    .line 31
    new-instance v0, Ldbx;

    const-string v1, "-_.!~*\'()@:$&,;="

    invoke-direct {v0, v1, v3}, Ldbx;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Ldbv;->b:Ldbw;

    .line 34
    new-instance v0, Ldbx;

    const-string v1, "-_.!~*\'()@:$&,;=+/?"

    invoke-direct {v0, v1, v3}, Ldbx;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Ldbv;->c:Ldbw;

    .line 37
    new-instance v0, Ldbx;

    const-string v1, "-_.!~*\'():$&,;="

    invoke-direct {v0, v1, v3}, Ldbx;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Ldbv;->d:Ldbw;

    .line 40
    new-instance v0, Ldbx;

    const-string v1, "-_.!~*\'()@:$,;/?:"

    invoke-direct {v0, v1, v3}, Ldbx;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Ldbv;->e:Ldbw;

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Ldbv;->a:Ldbw;

    invoke-virtual {v0, p0}, Ldbw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 92
    :try_start_0
    const-string v0, "UTF-8"

    invoke-static {p0, v0}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 93
    :catch_0
    move-exception v0

    .line 95
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    sget-object v0, Ldbv;->b:Ldbw;

    invoke-virtual {v0, p0}, Ldbw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    sget-object v0, Ldbv;->c:Ldbw;

    invoke-virtual {v0, p0}, Ldbw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static e(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    sget-object v0, Ldbv;->d:Ldbw;

    invoke-virtual {v0, p0}, Ldbw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static f(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    sget-object v0, Ldbv;->e:Ldbw;

    invoke-virtual {v0, p0}, Ldbw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
