.class public final Lecj;
.super Ljava/util/concurrent/locks/ReentrantLock;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/locks/ReentrantLock;"
    }
.end annotation


# instance fields
.field final a:Lcom/google/common/cache/LocalCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/cache/LocalCache",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public volatile b:I

.field c:I

.field public d:I

.field e:I

.field public volatile f:Ljava/util/concurrent/atomic/AtomicReferenceArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReferenceArray",
            "<",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field final g:J

.field final h:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;"
        }
    .end annotation
.end field

.field final i:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<TV;>;"
        }
    .end annotation
.end field

.field final j:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field final k:Ljava/util/concurrent/atomic/AtomicInteger;

.field final l:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field final m:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field final n:La;


# direct methods
.method private a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;)Lcom/google/common/cache/LocalCache$ReferenceEntry;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;)",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2090
    invoke-interface {p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2104
    :cond_0
    :goto_0
    return-object v0

    .line 2095
    :cond_1
    invoke-interface {p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getValueReference()Lecw;

    move-result-object v1

    .line 2096
    invoke-interface {v1}, Lecw;->get()Ljava/lang/Object;

    move-result-object v2

    .line 2097
    if-nez v2, :cond_2

    invoke-interface {v1}, Lecw;->d()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2102
    :cond_2
    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v0, v0, Lcom/google/common/cache/LocalCache;->r:Lebt;

    invoke-virtual {v0, p0, p1, p2}, Lebt;->a(Lecj;Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;)Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    .line 2103
    iget-object v3, p0, Lecj;->i:Ljava/lang/ref/ReferenceQueue;

    invoke-interface {v1, v3, v2, v0}, Lecw;->a(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;Lcom/google/common/cache/LocalCache$ReferenceEntry;)Lecw;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->setValueReference(Lecw;)V

    goto :goto_0
.end method

.method private a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;Ljava/lang/Object;Lecw;Ledh;)Lcom/google/common/cache/LocalCache$ReferenceEntry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;TK;",
            "Lecw",
            "<TK;TV;>;",
            "Ledh;",
            ")",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 3201
    invoke-direct {p0, p3, p4, p5}, Lecj;->a(Ljava/lang/Object;Lecw;Ledh;)V

    .line 3202
    iget-object v0, p0, Lecj;->l:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 3203
    iget-object v0, p0, Lecj;->m:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 3205
    invoke-interface {p4}, Lecw;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3206
    const/4 v0, 0x0

    invoke-interface {p4, v0}, Lecw;->a(Ljava/lang/Object;)V

    .line 3209
    :goto_0
    return-object p1

    :cond_0
    invoke-direct {p0, p1, p2}, Lecj;->b(Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;)Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object p1

    goto :goto_0
.end method

.method private a(Ljava/lang/Object;IJ)Lcom/google/common/cache/LocalCache$ReferenceEntry;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "IJ)",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2671
    invoke-direct {p0, p1, p2}, Lecj;->e(Ljava/lang/Object;I)Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v1

    .line 2672
    if-nez v1, :cond_0

    .line 2678
    :goto_0
    return-object v0

    .line 2674
    :cond_0
    iget-object v2, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    invoke-virtual {v2, v1, p3, p4}, Lcom/google/common/cache/LocalCache;->b(Lcom/google/common/cache/LocalCache$ReferenceEntry;J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2675
    invoke-direct {p0, p3, p4}, Lecj;->a(J)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 2678
    goto :goto_0
.end method

.method private a(Ljava/lang/Object;ILcom/google/common/cache/LocalCache$ReferenceEntry;)Lcom/google/common/cache/LocalCache$ReferenceEntry;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;)",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 2081
    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v0, v0, Lcom/google/common/cache/LocalCache;->r:Lebt;

    invoke-static {p1}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p0, v1, p2, p3}, Lebt;->a(Lecj;Ljava/lang/Object;ILcom/google/common/cache/LocalCache$ReferenceEntry;)Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;ILebo;)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "Lebo",
            "<-TK;TV;>;)TV;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2334
    invoke-direct {p0, p1, p2}, Lecj;->d(Ljava/lang/Object;I)Lech;

    move-result-object v4

    .line 2336
    if-nez v4, :cond_0

    move-object v0, v6

    .line 2348
    :goto_0
    return-object v0

    .line 2340
    :cond_0
    invoke-virtual {v4, p1, p3}, Lech;->a(Ljava/lang/Object;Lebo;)Leht;

    move-result-object v5

    new-instance v0, Leck;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Leck;-><init>(Lecj;Ljava/lang/Object;ILech;Leht;)V

    sget-object v1, Lcom/google/common/cache/LocalCache;->b:Lehv;

    invoke-interface {v5, v0, v1}, Leht;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 2341
    invoke-interface {v5}, Leht;->isDone()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2343
    :try_start_0
    invoke-static {v5}, Lf;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_1
    move-object v0, v6

    .line 2348
    goto :goto_0
.end method

.method private a(J)V
    .locals 1

    .prologue
    .line 2559
    invoke-virtual {p0}, Lecj;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2561
    :try_start_0
    invoke-direct {p0, p1, p2}, Lecj;->b(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2563
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 2567
    :cond_0
    return-void

    .line 2563
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lecj;->unlock()V

    throw v0
.end method

.method private a(Lcom/google/common/cache/LocalCache$ReferenceEntry;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 3233
    sget-object v0, Ledh;->c:Ledh;

    invoke-direct {p0, p1, v0}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Ledh;)V

    .line 3234
    iget-object v0, p0, Lecj;->l:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 3235
    iget-object v0, p0, Lecj;->m:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 3236
    return-void
.end method

.method private a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Ledh;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;",
            "Ledh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2590
    invoke-interface {p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getHash()I

    invoke-interface {p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getValueReference()Lecw;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2}, Lecj;->a(Ljava/lang/Object;Lecw;Ledh;)V

    .line 2591
    return-void
.end method

.method private a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Ljava/lang/Object;J)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;TV;J)V"
        }
    .end annotation

    .prologue
    .line 2112
    invoke-interface {p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getValueReference()Lecw;

    move-result-object v1

    .line 2113
    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v0, v0, Lcom/google/common/cache/LocalCache;->k:La;

    invoke-interface {v0}, La;->j()I

    move-result v2

    .line 2114
    if-ltz v2, :cond_2

    const/4 v0, 0x1

    :goto_0
    const-string v3, "Weights must be non-negative"

    invoke-static {v0, v3}, Lm;->b(ZLjava/lang/Object;)V

    .line 2116
    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v0, v0, Lcom/google/common/cache/LocalCache;->i:Lecm;

    invoke-virtual {v0, p0, p1, p2, v2}, Lecm;->a(Lecj;Lcom/google/common/cache/LocalCache$ReferenceEntry;Ljava/lang/Object;I)Lecw;

    move-result-object v0

    .line 2118
    invoke-interface {p1, v0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->setValueReference(Lecw;)V

    .line 2119
    invoke-direct {p0}, Lecj;->e()V

    iget v0, p0, Lecj;->c:I

    add-int/2addr v0, v2

    iput v0, p0, Lecj;->c:I

    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    invoke-virtual {v0}, Lcom/google/common/cache/LocalCache;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, p3, p4}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->setAccessTime(J)V

    :cond_0
    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    invoke-virtual {v0}, Lcom/google/common/cache/LocalCache;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1, p3, p4}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->setWriteTime(J)V

    :cond_1
    iget-object v0, p0, Lecj;->m:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lecj;->l:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2120
    invoke-interface {v1, p2}, Lecw;->a(Ljava/lang/Object;)V

    .line 2121
    return-void

    .line 2114
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/Object;Lecw;Ledh;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lecw",
            "<TK;TV;>;",
            "Ledh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2596
    iget v0, p0, Lecj;->c:I

    invoke-interface {p2}, Lecw;->a()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lecj;->c:I

    .line 2597
    invoke-virtual {p3}, Ledh;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2598
    iget-object v0, p0, Lecj;->n:La;

    .line 2600
    :cond_0
    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v0, v0, Lcom/google/common/cache/LocalCache;->o:Ljava/util/Queue;

    sget-object v1, Lcom/google/common/cache/LocalCache;->u:Ljava/util/Queue;

    if-eq v0, v1, :cond_1

    .line 2601
    invoke-interface {p2}, Lecw;->get()Ljava/lang/Object;

    move-result-object v0

    .line 2602
    new-instance v1, Ledn;

    invoke-direct {v1, p1, v0, p3}, Ledn;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ledh;)V

    .line 2603
    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v0, v0, Lcom/google/common/cache/LocalCache;->o:Ljava/util/Queue;

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 2605
    :cond_1
    return-void
.end method

.method private a(Lcom/google/common/cache/LocalCache$ReferenceEntry;ILedh;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;I",
            "Ledh;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 3340
    iget v0, p0, Lecj;->b:I

    .line 3341
    iget-object v6, p0, Lecj;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3342
    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v7, p2, v0

    .line 3343
    invoke-virtual {v6, v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-object v2, v1

    .line 3345
    :goto_0
    if-eqz v2, :cond_1

    .line 3346
    if-ne v2, p1, :cond_0

    .line 3347
    iget v0, p0, Lecj;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lecj;->d:I

    .line 3348
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getValueReference()Lecw;

    move-result-object v4

    move-object v0, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;Ljava/lang/Object;Lecw;Ledh;)Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    .line 3350
    iget v1, p0, Lecj;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 3351
    invoke-virtual {v6, v7, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 3352
    iput v1, p0, Lecj;->b:I

    .line 3353
    const/4 v0, 0x1

    .line 3357
    :goto_1
    return v0

    .line 3345
    :cond_0
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNext()Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v2

    goto :goto_0

    .line 3357
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Ljava/lang/Object;ILech;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "Lech",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 3307
    invoke-virtual {p0}, Lecj;->lock()V

    .line 3309
    :try_start_0
    iget-object v3, p0, Lecj;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3310
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v4, p2, v0

    .line 3311
    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-object v2, v0

    .line 3313
    :goto_0
    if-eqz v2, :cond_3

    .line 3314
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v5

    .line 3315
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getHash()I

    move-result v6

    if-ne v6, p2, :cond_2

    if-eqz v5, :cond_2

    iget-object v6, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v6, v6, Lcom/google/common/cache/LocalCache;->f:Leba;

    invoke-virtual {v6, p1, v5}, Leba;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 3317
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getValueReference()Lecw;

    move-result-object v5

    .line 3318
    if-ne v5, p3, :cond_1

    .line 3319
    invoke-virtual {p3}, Lech;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3320
    invoke-virtual {p3}, Lech;->f()Lecw;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->setValueReference(Lecw;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3325
    :goto_1
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3334
    invoke-direct {p0}, Lecj;->h()V

    const/4 v0, 0x1

    :goto_2
    return v0

    .line 3322
    :cond_0
    :try_start_1
    invoke-direct {p0, v0, v2}, Lecj;->b(Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;)Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    .line 3323
    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 3333
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3334
    invoke-direct {p0}, Lecj;->h()V

    throw v0

    .line 3327
    :cond_1
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3334
    invoke-direct {p0}, Lecj;->h()V

    move v0, v1

    goto :goto_2

    .line 3313
    :cond_2
    :try_start_2
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNext()Lcom/google/common/cache/LocalCache$ReferenceEntry;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 3331
    :cond_3
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3334
    invoke-direct {p0}, Lecj;->h()V

    move v0, v1

    goto :goto_2
.end method

.method private a(Ljava/lang/Object;ILech;Ljava/lang/Object;)Z
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "Lech",
            "<TK;TV;>;TV;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3063
    invoke-virtual {p0}, Lecj;->lock()V

    .line 3065
    :try_start_0
    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v0, v0, Lcom/google/common/cache/LocalCache;->q:Lebm;

    invoke-virtual {v0}, Lebm;->a()J

    move-result-wide v5

    .line 3066
    invoke-direct {p0, v5, v6}, Lecj;->c(J)V

    .line 3068
    iget v0, p0, Lecj;->b:I

    add-int/lit8 v3, v0, 0x1

    .line 3069
    iget v0, p0, Lecj;->e:I

    if-le v3, v0, :cond_0

    .line 3070
    invoke-direct {p0}, Lecj;->g()V

    .line 3071
    iget v0, p0, Lecj;->b:I

    add-int/lit8 v3, v0, 0x1

    .line 3074
    :cond_0
    iget-object v7, p0, Lecj;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3075
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v8, p2, v0

    .line 3076
    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-object v4, v0

    .line 3078
    :goto_0
    if-eqz v4, :cond_5

    .line 3079
    invoke-interface {v4}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v9

    .line 3080
    invoke-interface {v4}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getHash()I

    move-result v10

    if-ne v10, p2, :cond_4

    if-eqz v9, :cond_4

    iget-object v10, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v10, v10, Lcom/google/common/cache/LocalCache;->f:Leba;

    invoke-virtual {v10, p1, v9}, Leba;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 3082
    invoke-interface {v4}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getValueReference()Lecw;

    move-result-object v0

    .line 3083
    invoke-interface {v0}, Lecw;->get()Ljava/lang/Object;

    move-result-object v7

    .line 3086
    if-eq p3, v0, :cond_1

    if-nez v7, :cond_3

    sget-object v8, Lcom/google/common/cache/LocalCache;->t:Lecw;

    if-eq v0, v8, :cond_3

    .line 3088
    :cond_1
    iget v0, p0, Lecj;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lecj;->d:I

    .line 3089
    invoke-virtual {p3}, Lech;->d()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3090
    if-nez v7, :cond_2

    sget-object v0, Ledh;->c:Ledh;

    .line 3092
    :goto_1
    invoke-direct {p0, p1, p3, v0}, Lecj;->a(Ljava/lang/Object;Lecw;Ledh;)V

    .line 3093
    add-int/lit8 v0, v3, -0x1

    .line 3095
    :goto_2
    invoke-direct {p0, v4, p4, v5, v6}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Ljava/lang/Object;J)V

    .line 3096
    iput v0, p0, Lecj;->b:I

    .line 3097
    invoke-direct {p0}, Lecj;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3098
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3117
    invoke-direct {p0}, Lecj;->h()V

    move v0, v2

    :goto_3
    return v0

    .line 3090
    :cond_2
    :try_start_1
    sget-object v0, Ledh;->b:Ledh;

    goto :goto_1

    .line 3102
    :cond_3
    new-instance v0, Lede;

    const/4 v2, 0x0

    invoke-direct {v0, p4, v2}, Lede;-><init>(Ljava/lang/Object;I)V

    .line 3103
    sget-object v2, Ledh;->b:Ledh;

    invoke-direct {p0, p1, v0, v2}, Lecj;->a(Ljava/lang/Object;Lecw;Ledh;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3104
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3117
    invoke-direct {p0}, Lecj;->h()V

    move v0, v1

    goto :goto_3

    .line 3078
    :cond_4
    :try_start_2
    invoke-interface {v4}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNext()Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v4

    goto :goto_0

    .line 3108
    :cond_5
    iget v1, p0, Lecj;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lecj;->d:I

    .line 3109
    invoke-direct {p0, p1, p2, v0}, Lecj;->a(Ljava/lang/Object;ILcom/google/common/cache/LocalCache$ReferenceEntry;)Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    .line 3110
    invoke-direct {p0, v0, p4, v5, v6}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Ljava/lang/Object;J)V

    .line 3111
    invoke-virtual {v7, v8, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 3112
    iput v3, p0, Lecj;->b:I

    .line 3113
    invoke-direct {p0}, Lecj;->f()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3114
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3117
    invoke-direct {p0}, Lecj;->h()V

    move v0, v2

    goto :goto_3

    .line 3116
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3117
    invoke-direct {p0}, Lecj;->h()V

    throw v0

    :cond_6
    move v0, v3

    goto :goto_2
.end method

.method private b(Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;)Lcom/google/common/cache/LocalCache$ReferenceEntry;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;)",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 3216
    iget v2, p0, Lecj;->b:I

    .line 3217
    invoke-interface {p2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNext()Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v1

    .line 3218
    :goto_0
    if-eq p1, p2, :cond_1

    .line 3219
    invoke-direct {p0, p1, v1}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;)Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    .line 3220
    if-eqz v0, :cond_0

    move v1, v2

    .line 3218
    :goto_1
    invoke-interface {p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNext()Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object p1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 3223
    :cond_0
    invoke-direct {p0, p1}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;)V

    .line 3224
    add-int/lit8 v0, v2, -0x1

    move-object v3, v1

    move v1, v0

    move-object v0, v3

    goto :goto_1

    .line 3227
    :cond_1
    iput v2, p0, Lecj;->b:I

    .line 3228
    return-object v1
.end method

.method private b(J)V
    .locals 3

    .prologue
    .line 2571
    invoke-direct {p0}, Lecj;->e()V

    .line 2574
    :cond_0
    iget-object v0, p0, Lecj;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/cache/LocalCache$ReferenceEntry;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    invoke-virtual {v1, v0, p1, p2}, Lcom/google/common/cache/LocalCache;->b(Lcom/google/common/cache/LocalCache$ReferenceEntry;J)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2575
    invoke-interface {v0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getHash()I

    move-result v1

    sget-object v2, Ledh;->d:Ledh;

    invoke-direct {p0, v0, v1, v2}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;ILedh;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2576
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2579
    :cond_1
    iget-object v0, p0, Lecj;->m:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/cache/LocalCache$ReferenceEntry;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    invoke-virtual {v1, v0, p1, p2}, Lcom/google/common/cache/LocalCache;->b(Lcom/google/common/cache/LocalCache$ReferenceEntry;J)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2580
    invoke-interface {v0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getHash()I

    move-result v1

    sget-object v2, Ledh;->d:Ledh;

    invoke-direct {p0, v0, v1, v2}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;ILedh;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2581
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2584
    :cond_2
    return-void
.end method

.method private b(Lcom/google/common/cache/LocalCache$ReferenceEntry;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;J)V"
        }
    .end annotation

    .prologue
    .line 2507
    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    invoke-virtual {v0}, Lcom/google/common/cache/LocalCache;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2508
    invoke-interface {p1, p2, p3}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->setAccessTime(J)V

    .line 2510
    :cond_0
    iget-object v0, p0, Lecj;->m:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2511
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 2411
    invoke-virtual {p0}, Lecj;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2413
    :try_start_0
    invoke-direct {p0}, Lecj;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2415
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 2418
    :cond_0
    return-void

    .line 2415
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lecj;->unlock()V

    throw v0
.end method

.method private c(J)V
    .locals 2

    .prologue
    .line 3395
    invoke-virtual {p0}, Lecj;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3397
    :try_start_0
    invoke-direct {p0}, Lecj;->d()V

    .line 3398
    invoke-direct {p0, p1, p2}, Lecj;->b(J)V

    .line 3399
    iget-object v0, p0, Lecj;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3401
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3404
    :cond_0
    return-void

    .line 3401
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lecj;->unlock()V

    throw v0
.end method

.method private d(Ljava/lang/Object;I)Lech;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I)",
            "Lech",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 2358
    invoke-virtual {p0}, Lecj;->lock()V

    .line 2361
    :try_start_0
    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v0, v0, Lcom/google/common/cache/LocalCache;->q:Lebm;

    invoke-virtual {v0}, Lebm;->a()J

    move-result-wide v2

    .line 2362
    invoke-direct {p0, v2, v3}, Lecj;->c(J)V

    .line 2364
    iget-object v4, p0, Lecj;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2365
    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v5, p2, v0

    .line 2366
    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-object v1, v0

    .line 2369
    :goto_0
    if-eqz v1, :cond_3

    .line 2370
    invoke-interface {v1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v6

    .line 2371
    invoke-interface {v1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getHash()I

    move-result v7

    if-ne v7, p2, :cond_2

    if-eqz v6, :cond_2

    iget-object v7, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v7, v7, Lcom/google/common/cache/LocalCache;->f:Leba;

    invoke-virtual {v7, p1, v6}, Leba;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2375
    invoke-interface {v1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getValueReference()Lecw;

    move-result-object v4

    .line 2376
    invoke-interface {v4}, Lecw;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getWriteTime()J

    move-result-wide v5

    sub-long/2addr v2, v5

    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-wide v5, v0, Lcom/google/common/cache/LocalCache;->n:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, v2, v5

    if-gez v0, :cond_1

    .line 2381
    :cond_0
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 2401
    invoke-direct {p0}, Lecj;->h()V

    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 2385
    :cond_1
    :try_start_1
    iget v0, p0, Lecj;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lecj;->d:I

    .line 2386
    new-instance v0, Lech;

    invoke-direct {v0, v4}, Lech;-><init>(Lecw;)V

    .line 2388
    invoke-interface {v1, v0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->setValueReference(Lecw;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2400
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 2401
    invoke-direct {p0}, Lecj;->h()V

    goto :goto_1

    .line 2369
    :cond_2
    :try_start_2
    invoke-interface {v1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNext()Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v1

    goto :goto_0

    .line 2393
    :cond_3
    iget v1, p0, Lecj;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lecj;->d:I

    .line 2394
    new-instance v1, Lech;

    invoke-direct {v1}, Lech;-><init>()V

    .line 2395
    invoke-direct {p0, p1, p2, v0}, Lecj;->a(Ljava/lang/Object;ILcom/google/common/cache/LocalCache$ReferenceEntry;)Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    .line 2396
    invoke-interface {v0, v1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->setValueReference(Lecw;)V

    .line 2397
    invoke-virtual {v4, v5, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2400
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 2401
    invoke-direct {p0}, Lecj;->h()V

    move-object v0, v1

    goto :goto_1

    .line 2400
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lecj;->unlock()V

    .line 2401
    invoke-direct {p0}, Lecj;->h()V

    throw v0
.end method

.method private d()V
    .locals 5

    .prologue
    const/16 v4, 0x10

    const/4 v2, 0x0

    .line 2426
    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    invoke-virtual {v0}, Lcom/google/common/cache/LocalCache;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v2

    .line 2427
    :goto_0
    iget-object v0, p0, Lecj;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/google/common/cache/LocalCache$ReferenceEntry;

    iget-object v3, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    invoke-virtual {v3, v0}, Lcom/google/common/cache/LocalCache;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;)V

    add-int/lit8 v0, v1, 0x1

    if-ne v0, v4, :cond_3

    .line 2429
    :cond_0
    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    invoke-virtual {v0}, Lcom/google/common/cache/LocalCache;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2430
    :cond_1
    iget-object v0, p0, Lecj;->i:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Lecw;

    iget-object v1, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    invoke-virtual {v1, v0}, Lcom/google/common/cache/LocalCache;->a(Lecw;)V

    add-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_1

    .line 2432
    :cond_2
    return-void

    :cond_3
    move v1, v0

    goto :goto_0
.end method

.method private e(Ljava/lang/Object;I)Lcom/google/common/cache/LocalCache$ReferenceEntry;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I)",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 2650
    iget-object v0, p0, Lecj;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int/2addr v1, p2

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/cache/LocalCache$ReferenceEntry;

    :goto_0
    if-eqz v0, :cond_2

    .line 2651
    invoke-interface {v0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getHash()I

    move-result v1

    if-ne v1, p2, :cond_0

    .line 2652
    invoke-interface {v0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 2656
    if-nez v1, :cond_1

    .line 2657
    invoke-direct {p0}, Lecj;->c()V

    .line 2650
    :cond_0
    invoke-interface {v0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNext()Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    goto :goto_0

    .line 2661
    :cond_1
    iget-object v2, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v2, v2, Lcom/google/common/cache/LocalCache;->f:Leba;

    invoke-virtual {v2, p1, v1}, Leba;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2666
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private e()V
    .locals 2

    .prologue
    .line 2542
    :cond_0
    :goto_0
    iget-object v0, p0, Lecj;->j:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/cache/LocalCache$ReferenceEntry;

    if-eqz v0, :cond_1

    .line 2547
    iget-object v1, p0, Lecj;->m:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2548
    iget-object v1, p0, Lecj;->m:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2551
    :cond_1
    return-void
.end method

.method private f()V
    .locals 4

    .prologue
    .line 2613
    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    invoke-virtual {v0}, Lcom/google/common/cache/LocalCache;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2624
    :cond_0
    return-void

    .line 2617
    :cond_1
    invoke-direct {p0}, Lecj;->e()V

    .line 2618
    :cond_2
    iget v0, p0, Lecj;->c:I

    int-to-long v0, v0

    iget-wide v2, p0, Lecj;->g:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 2619
    iget-object v0, p0, Lecj;->m:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/cache/LocalCache$ReferenceEntry;

    invoke-interface {v0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getValueReference()Lecw;

    move-result-object v2

    invoke-interface {v2}, Lecw;->a()I

    move-result v2

    if-lez v2, :cond_3

    .line 2620
    invoke-interface {v0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getHash()I

    move-result v1

    sget-object v2, Ledh;->e:Ledh;

    invoke-direct {p0, v0, v1, v2}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;ILedh;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2621
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2619
    :cond_4
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private g()V
    .locals 11

    .prologue
    .line 2850
    iget-object v7, p0, Lecj;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2851
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v8

    .line 2852
    const/high16 v0, 0x40000000    # 2.0f

    if-lt v8, v0, :cond_0

    .line 2915
    :goto_0
    return-void

    .line 2866
    :cond_0
    iget v5, p0, Lecj;->b:I

    .line 2867
    shl-int/lit8 v0, v8, 0x1

    new-instance v9, Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-direct {v9, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;-><init>(I)V

    .line 2868
    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, Lecj;->e:I

    .line 2869
    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v10, v0, -0x1

    .line 2870
    const/4 v0, 0x0

    move v6, v0

    :goto_1
    if-ge v6, v8, :cond_5

    .line 2873
    invoke-virtual {v7, v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/cache/LocalCache$ReferenceEntry;

    .line 2875
    if-eqz v0, :cond_7

    .line 2876
    invoke-interface {v0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNext()Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v3

    .line 2877
    invoke-interface {v0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getHash()I

    move-result v1

    and-int v2, v1, v10

    .line 2880
    if-nez v3, :cond_2

    .line 2881
    invoke-virtual {v9, v2, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move v1, v5

    .line 2870
    :cond_1
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v5, v1

    goto :goto_1

    :cond_2
    move-object v4, v0

    .line 2888
    :goto_3
    if-eqz v3, :cond_3

    .line 2889
    invoke-interface {v3}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getHash()I

    move-result v1

    and-int/2addr v1, v10

    .line 2890
    if-eq v1, v2, :cond_6

    move-object v2, v3

    .line 2888
    :goto_4
    invoke-interface {v3}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNext()Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v3

    move-object v4, v2

    move v2, v1

    goto :goto_3

    .line 2896
    :cond_3
    invoke-virtual {v9, v2, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move-object v2, v0

    move v1, v5

    .line 2899
    :goto_5
    if-eq v2, v4, :cond_1

    .line 2900
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getHash()I

    move-result v0

    and-int v3, v0, v10

    .line 2901
    invoke-virtual {v9, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/cache/LocalCache$ReferenceEntry;

    .line 2902
    invoke-direct {p0, v2, v0}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;)Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    .line 2903
    if-eqz v0, :cond_4

    .line 2904
    invoke-virtual {v9, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move v0, v1

    .line 2899
    :goto_6
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNext()Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v1

    move-object v2, v1

    move v1, v0

    goto :goto_5

    .line 2906
    :cond_4
    invoke-direct {p0, v2}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;)V

    .line 2907
    add-int/lit8 v0, v1, -0x1

    goto :goto_6

    .line 2913
    :cond_5
    iput-object v9, p0, Lecj;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2914
    iput v5, p0, Lecj;->b:I

    goto :goto_0

    :cond_6
    move v1, v2

    move-object v2, v4

    goto :goto_4

    :cond_7
    move v1, v5

    goto :goto_2
.end method

.method private h()V
    .locals 1

    .prologue
    .line 3408
    invoke-virtual {p0}, Lecj;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3409
    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    invoke-virtual {v0}, Lcom/google/common/cache/LocalCache;->k()V

    .line 3411
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/google/common/cache/LocalCache$ReferenceEntry;J)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;J)TV;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2686
    invoke-interface {p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 2687
    invoke-direct {p0}, Lecj;->c()V

    .line 2700
    :goto_0
    return-object v0

    .line 2690
    :cond_0
    invoke-interface {p1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getValueReference()Lecw;

    move-result-object v1

    invoke-interface {v1}, Lecw;->get()Ljava/lang/Object;

    move-result-object v1

    .line 2691
    if-nez v1, :cond_1

    .line 2692
    invoke-direct {p0}, Lecj;->c()V

    goto :goto_0

    .line 2696
    :cond_1
    iget-object v2, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    invoke-virtual {v2, p1, p2, p3}, Lcom/google/common/cache/LocalCache;->b(Lcom/google/common/cache/LocalCache$ReferenceEntry;J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2697
    invoke-direct {p0, p2, p3}, Lecj;->a(J)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 2700
    goto :goto_0
.end method

.method public a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I)TV;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2706
    :try_start_0
    iget v1, p0, Lecj;->b:I

    if-eqz v1, :cond_4

    .line 2707
    iget-object v1, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v1, v1, Lcom/google/common/cache/LocalCache;->q:Lebm;

    invoke-virtual {v1}, Lebm;->a()J

    move-result-wide v2

    .line 2708
    invoke-direct {p0, p1, p2, v2, v3}, Lecj;->a(Ljava/lang/Object;IJ)Lcom/google/common/cache/LocalCache$ReferenceEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 2709
    if-nez v4, :cond_0

    .line 2710
    invoke-virtual {p0}, Lecj;->b()V

    .line 2722
    :goto_0
    return-object v0

    .line 2713
    :cond_0
    :try_start_1
    invoke-interface {v4}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getValueReference()Lecw;

    move-result-object v1

    invoke-interface {v1}, Lecw;->get()Ljava/lang/Object;

    move-result-object v1

    .line 2714
    if-eqz v1, :cond_3

    .line 2715
    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    invoke-virtual {v0}, Lcom/google/common/cache/LocalCache;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4, v2, v3}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->setAccessTime(J)V

    :cond_1
    iget-object v0, p0, Lecj;->j:Ljava/util/Queue;

    invoke-interface {v0, v4}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2716
    invoke-interface {v4}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v0

    iget-object v5, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v5, v5, Lcom/google/common/cache/LocalCache;->s:Lebo;

    iget-object v6, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    invoke-virtual {v6}, Lcom/google/common/cache/LocalCache;->d()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v4}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getWriteTime()J

    move-result-wide v6

    sub-long/2addr v2, v6

    iget-object v6, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-wide v6, v6, Lcom/google/common/cache/LocalCache;->n:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_2

    invoke-interface {v4}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getValueReference()Lecw;

    move-result-object v2

    invoke-interface {v2}, Lecw;->c()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-direct {p0, v0, p2, v5}, Lecj;->a(Ljava/lang/Object;ILebo;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2722
    :goto_1
    invoke-virtual {p0}, Lecj;->b()V

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 2716
    goto :goto_1

    .line 2718
    :cond_3
    :try_start_2
    invoke-direct {p0}, Lecj;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2720
    :cond_4
    invoke-virtual {p0}, Lecj;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lecj;->b()V

    throw v0
.end method

.method a(Ljava/lang/Object;ILech;Leht;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "Lech",
            "<TK;TV;>;",
            "Leht",
            "<TV;>;)TV;"
        }
    .end annotation

    .prologue
    .line 2297
    const/4 v1, 0x0

    .line 2299
    :try_start_0
    invoke-static {p4}, Lf;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    .line 2300
    if-nez v1, :cond_1

    .line 2301
    new-instance v0, Lebp;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CacheLoader returned null for key "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lebp;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2307
    :catchall_0
    move-exception v0

    if-nez v1, :cond_0

    .line 2308
    iget-object v1, p0, Lecj;->n:La;

    invoke-virtual {p3}, Lech;->e()J

    .line 2309
    invoke-direct {p0, p1, p2, p3}, Lecj;->a(Ljava/lang/Object;ILech;)Z

    :cond_0
    throw v0

    .line 2303
    :cond_1
    :try_start_1
    iget-object v0, p0, Lecj;->n:La;

    invoke-virtual {p3}, Lech;->e()J

    .line 2304
    invoke-direct {p0, p1, p2, p3, v1}, Lecj;->a(Ljava/lang/Object;ILech;Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2307
    if-nez v1, :cond_2

    .line 2308
    iget-object v0, p0, Lecj;->n:La;

    invoke-virtual {p3}, Lech;->e()J

    .line 2309
    invoke-direct {p0, p1, p2, p3}, Lecj;->a(Ljava/lang/Object;ILech;)Z

    :cond_2
    return-object v1
.end method

.method public a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ITV;)TV;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2971
    invoke-virtual {p0}, Lecj;->lock()V

    .line 2973
    :try_start_0
    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v0, v0, Lcom/google/common/cache/LocalCache;->q:Lebm;

    invoke-virtual {v0}, Lebm;->a()J

    move-result-wide v7

    .line 2974
    invoke-direct {p0, v7, v8}, Lecj;->c(J)V

    .line 2976
    iget-object v9, p0, Lecj;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2977
    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v10, p2, v0

    .line 2978
    invoke-virtual {v9, v10}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-object v2, v1

    .line 2980
    :goto_0
    if-eqz v2, :cond_3

    .line 2981
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v3

    .line 2982
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getHash()I

    move-result v0

    if-ne v0, p2, :cond_2

    if-eqz v3, :cond_2

    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v0, v0, Lcom/google/common/cache/LocalCache;->f:Leba;

    invoke-virtual {v0, p1, v3}, Leba;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2984
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getValueReference()Lecw;

    move-result-object v4

    .line 2985
    invoke-interface {v4}, Lecw;->get()Ljava/lang/Object;

    move-result-object v0

    .line 2986
    if-nez v0, :cond_1

    .line 2987
    invoke-interface {v4}, Lecw;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2989
    iget v0, p0, Lecj;->b:I

    .line 2990
    iget v0, p0, Lecj;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lecj;->d:I

    .line 2991
    sget-object v5, Ledh;->c:Ledh;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;Ljava/lang/Object;Lecw;Ledh;)Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    .line 2993
    iget v1, p0, Lecj;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 2994
    invoke-virtual {v9, v10, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2995
    iput v1, p0, Lecj;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2997
    :cond_0
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3011
    invoke-direct {p0}, Lecj;->h()V

    move-object v0, v6

    :goto_1
    return-object v0

    .line 3000
    :cond_1
    :try_start_1
    iget v1, p0, Lecj;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lecj;->d:I

    .line 3001
    sget-object v1, Ledh;->b:Ledh;

    invoke-direct {p0, p1, v4, v1}, Lecj;->a(Ljava/lang/Object;Lecw;Ledh;)V

    .line 3002
    invoke-direct {p0, v2, p3, v7, v8}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Ljava/lang/Object;J)V

    .line 3003
    invoke-direct {p0}, Lecj;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3010
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3011
    invoke-direct {p0}, Lecj;->h()V

    goto :goto_1

    .line 2980
    :cond_2
    :try_start_2
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNext()Lcom/google/common/cache/LocalCache$ReferenceEntry;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 3008
    :cond_3
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3011
    invoke-direct {p0}, Lecj;->h()V

    move-object v0, v6

    goto :goto_1

    .line 3010
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3011
    invoke-direct {p0}, Lecj;->h()V

    throw v0
.end method

.method public a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ITV;Z)TV;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2775
    invoke-virtual {p0}, Lecj;->lock()V

    .line 2777
    :try_start_0
    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v0, v0, Lcom/google/common/cache/LocalCache;->q:Lebm;

    invoke-virtual {v0}, Lebm;->a()J

    move-result-wide v3

    .line 2778
    invoke-direct {p0, v3, v4}, Lecj;->c(J)V

    .line 2780
    iget v0, p0, Lecj;->b:I

    add-int/lit8 v0, v0, 0x1

    .line 2781
    iget v2, p0, Lecj;->e:I

    if-le v0, v2, :cond_0

    .line 2782
    invoke-direct {p0}, Lecj;->g()V

    .line 2783
    iget v0, p0, Lecj;->b:I

    .line 2786
    :cond_0
    iget-object v5, p0, Lecj;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2787
    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v6, p2, v0

    .line 2788
    invoke-virtual {v5, v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-object v2, v0

    .line 2791
    :goto_0
    if-eqz v2, :cond_5

    .line 2792
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v7

    .line 2793
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getHash()I

    move-result v8

    if-ne v8, p2, :cond_4

    if-eqz v7, :cond_4

    iget-object v8, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v8, v8, Lcom/google/common/cache/LocalCache;->f:Leba;

    invoke-virtual {v8, p1, v7}, Leba;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2797
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getValueReference()Lecw;

    move-result-object v5

    .line 2798
    invoke-interface {v5}, Lecw;->get()Ljava/lang/Object;

    move-result-object v0

    .line 2800
    if-nez v0, :cond_2

    .line 2801
    iget v0, p0, Lecj;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lecj;->d:I

    .line 2802
    invoke-interface {v5}, Lecw;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2803
    sget-object v0, Ledh;->c:Ledh;

    invoke-direct {p0, p1, v5, v0}, Lecj;->a(Ljava/lang/Object;Lecw;Ledh;)V

    .line 2804
    invoke-direct {p0, v2, p3, v3, v4}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Ljava/lang/Object;J)V

    .line 2805
    iget v0, p0, Lecj;->b:I

    .line 2810
    :goto_1
    iput v0, p0, Lecj;->b:I

    .line 2811
    invoke-direct {p0}, Lecj;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2812
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 2841
    invoke-direct {p0}, Lecj;->h()V

    move-object v0, v1

    :goto_2
    return-object v0

    .line 2807
    :cond_1
    :try_start_1
    invoke-direct {p0, v2, p3, v3, v4}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Ljava/lang/Object;J)V

    .line 2808
    iget v0, p0, Lecj;->b:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2813
    :cond_2
    if-eqz p4, :cond_3

    .line 2817
    invoke-direct {p0, v2, v3, v4}, Lecj;->b(Lcom/google/common/cache/LocalCache$ReferenceEntry;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2840
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 2841
    invoke-direct {p0}, Lecj;->h()V

    goto :goto_2

    .line 2821
    :cond_3
    :try_start_2
    iget v1, p0, Lecj;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lecj;->d:I

    .line 2822
    sget-object v1, Ledh;->b:Ledh;

    invoke-direct {p0, p1, v5, v1}, Lecj;->a(Ljava/lang/Object;Lecw;Ledh;)V

    .line 2823
    invoke-direct {p0, v2, p3, v3, v4}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Ljava/lang/Object;J)V

    .line 2824
    invoke-direct {p0}, Lecj;->f()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2840
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 2841
    invoke-direct {p0}, Lecj;->h()V

    goto :goto_2

    .line 2791
    :cond_4
    :try_start_3
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNext()Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v2

    goto :goto_0

    .line 2831
    :cond_5
    iget v2, p0, Lecj;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lecj;->d:I

    .line 2832
    invoke-direct {p0, p1, p2, v0}, Lecj;->a(Ljava/lang/Object;ILcom/google/common/cache/LocalCache$ReferenceEntry;)Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    .line 2833
    invoke-direct {p0, v0, p3, v3, v4}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Ljava/lang/Object;J)V

    .line 2834
    invoke-virtual {v5, v6, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2835
    iget v0, p0, Lecj;->b:I

    add-int/lit8 v0, v0, 0x1

    .line 2836
    iput v0, p0, Lecj;->b:I

    .line 2837
    invoke-direct {p0}, Lecj;->f()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2838
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 2841
    invoke-direct {p0}, Lecj;->h()V

    move-object v0, v1

    goto :goto_2

    .line 2840
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lecj;->unlock()V

    .line 2841
    invoke-direct {p0}, Lecj;->h()V

    throw v0
.end method

.method public a()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 3167
    iget v0, p0, Lecj;->b:I

    if-eqz v0, :cond_8

    .line 3168
    invoke-virtual {p0}, Lecj;->lock()V

    .line 3170
    :try_start_0
    iget-object v3, p0, Lecj;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move v2, v1

    .line 3171
    :goto_0
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 3172
    invoke-virtual {v3, v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/cache/LocalCache$ReferenceEntry;

    :goto_1
    if-eqz v0, :cond_1

    .line 3174
    invoke-interface {v0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getValueReference()Lecw;

    move-result-object v4

    invoke-interface {v4}, Lecw;->d()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3175
    sget-object v4, Ledh;->a:Ledh;

    invoke-direct {p0, v0, v4}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Ledh;)V

    .line 3172
    :cond_0
    invoke-interface {v0}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNext()Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    goto :goto_1

    .line 3171
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 3179
    :goto_2
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 3180
    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 3179
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3182
    :cond_3
    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    invoke-virtual {v0}, Lcom/google/common/cache/LocalCache;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    iget-object v0, p0, Lecj;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_4

    :cond_5
    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    invoke-virtual {v0}, Lcom/google/common/cache/LocalCache;->h()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    iget-object v0, p0, Lecj;->i:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_6

    .line 3183
    :cond_7
    iget-object v0, p0, Lecj;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 3184
    iget-object v0, p0, Lecj;->m:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 3185
    iget-object v0, p0, Lecj;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 3187
    iget v0, p0, Lecj;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lecj;->d:I

    .line 3188
    const/4 v0, 0x0

    iput v0, p0, Lecj;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3190
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3191
    invoke-direct {p0}, Lecj;->h()V

    .line 3194
    :cond_8
    return-void

    .line 3190
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3191
    invoke-direct {p0}, Lecj;->h()V

    throw v0
.end method

.method public a(Lcom/google/common/cache/LocalCache$ReferenceEntry;I)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;I)Z"
        }
    .end annotation

    .prologue
    .line 3242
    invoke-virtual {p0}, Lecj;->lock()V

    .line 3244
    :try_start_0
    iget v0, p0, Lecj;->b:I

    .line 3245
    iget-object v6, p0, Lecj;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3246
    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v7, p2, v0

    .line 3247
    invoke-virtual {v6, v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-object v2, v1

    .line 3249
    :goto_0
    if-eqz v2, :cond_1

    .line 3250
    if-ne v2, p1, :cond_0

    .line 3251
    iget v0, p0, Lecj;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lecj;->d:I

    .line 3252
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getValueReference()Lecw;

    move-result-object v4

    sget-object v5, Ledh;->c:Ledh;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;Ljava/lang/Object;Lecw;Ledh;)Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    .line 3254
    iget v1, p0, Lecj;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 3255
    invoke-virtual {v6, v7, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 3256
    iput v1, p0, Lecj;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3257
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3264
    invoke-direct {p0}, Lecj;->h()V

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 3249
    :cond_0
    :try_start_1
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNext()Lcom/google/common/cache/LocalCache$ReferenceEntry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 3261
    :cond_1
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3264
    invoke-direct {p0}, Lecj;->h()V

    const/4 v0, 0x0

    goto :goto_1

    .line 3263
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3264
    invoke-direct {p0}, Lecj;->h()V

    throw v0
.end method

.method public a(Ljava/lang/Object;ILecw;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "Lecw",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 3272
    invoke-virtual {p0}, Lecj;->lock()V

    .line 3274
    :try_start_0
    iget v1, p0, Lecj;->b:I

    .line 3275
    iget-object v6, p0, Lecj;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3276
    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int v7, p2, v1

    .line 3277
    invoke-virtual {v6, v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-object v2, v1

    .line 3279
    :goto_0
    if-eqz v2, :cond_4

    .line 3280
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v3

    .line 3281
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getHash()I

    move-result v4

    if-ne v4, p2, :cond_3

    if-eqz v3, :cond_3

    iget-object v4, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v4, v4, Lcom/google/common/cache/LocalCache;->f:Leba;

    invoke-virtual {v4, p1, v3}, Leba;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 3283
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getValueReference()Lecw;

    move-result-object v4

    .line 3284
    if-ne v4, p3, :cond_2

    .line 3285
    iget v0, p0, Lecj;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lecj;->d:I

    .line 3286
    sget-object v5, Ledh;->c:Ledh;

    move-object v0, p0

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;Ljava/lang/Object;Lecw;Ledh;)Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    .line 3288
    iget v1, p0, Lecj;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 3289
    invoke-virtual {v6, v7, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 3290
    iput v1, p0, Lecj;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3291
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3300
    invoke-virtual {p0}, Lecj;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3301
    invoke-direct {p0}, Lecj;->h()V

    :cond_0
    const/4 v0, 0x1

    :cond_1
    :goto_1
    return v0

    .line 3293
    :cond_2
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3300
    invoke-virtual {p0}, Lecj;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_1

    .line 3301
    invoke-direct {p0}, Lecj;->h()V

    goto :goto_1

    .line 3279
    :cond_3
    :try_start_1
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNext()Lcom/google/common/cache/LocalCache$ReferenceEntry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 3297
    :cond_4
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3300
    invoke-virtual {p0}, Lecj;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_1

    .line 3301
    invoke-direct {p0}, Lecj;->h()V

    goto :goto_1

    .line 3299
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3300
    invoke-virtual {p0}, Lecj;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_5

    .line 3301
    invoke-direct {p0}, Lecj;->h()V

    :cond_5
    throw v0
.end method

.method public a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ITV;TV;)Z"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2918
    invoke-virtual {p0}, Lecj;->lock()V

    .line 2920
    :try_start_0
    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v0, v0, Lcom/google/common/cache/LocalCache;->q:Lebm;

    invoke-virtual {v0}, Lebm;->a()J

    move-result-wide v7

    .line 2921
    invoke-direct {p0, v7, v8}, Lecj;->c(J)V

    .line 2923
    iget-object v9, p0, Lecj;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2924
    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v10, p2, v0

    .line 2925
    invoke-virtual {v9, v10}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-object v2, v1

    .line 2927
    :goto_0
    if-eqz v2, :cond_4

    .line 2928
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v3

    .line 2929
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getHash()I

    move-result v0

    if-ne v0, p2, :cond_3

    if-eqz v3, :cond_3

    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v0, v0, Lcom/google/common/cache/LocalCache;->f:Leba;

    invoke-virtual {v0, p1, v3}, Leba;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2931
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getValueReference()Lecw;

    move-result-object v4

    .line 2932
    invoke-interface {v4}, Lecw;->get()Ljava/lang/Object;

    move-result-object v0

    .line 2933
    if-nez v0, :cond_1

    .line 2934
    invoke-interface {v4}, Lecw;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2936
    iget v0, p0, Lecj;->b:I

    .line 2937
    iget v0, p0, Lecj;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lecj;->d:I

    .line 2938
    sget-object v5, Ledh;->c:Ledh;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;Ljava/lang/Object;Lecw;Ledh;)Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    .line 2940
    iget v1, p0, Lecj;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 2941
    invoke-virtual {v9, v10, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2942
    iput v1, p0, Lecj;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2944
    :cond_0
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 2965
    invoke-direct {p0}, Lecj;->h()V

    move v0, v6

    :goto_1
    return v0

    .line 2947
    :cond_1
    :try_start_1
    iget-object v1, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v1, v1, Lcom/google/common/cache/LocalCache;->g:Leba;

    invoke-virtual {v1, p3, v0}, Leba;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2948
    iget v0, p0, Lecj;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lecj;->d:I

    .line 2949
    sget-object v0, Ledh;->b:Ledh;

    invoke-direct {p0, p1, v4, v0}, Lecj;->a(Ljava/lang/Object;Lecw;Ledh;)V

    .line 2950
    invoke-direct {p0, v2, p4, v7, v8}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Ljava/lang/Object;J)V

    .line 2951
    invoke-direct {p0}, Lecj;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2952
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 2965
    invoke-direct {p0}, Lecj;->h()V

    const/4 v0, 0x1

    goto :goto_1

    .line 2956
    :cond_2
    :try_start_2
    invoke-direct {p0, v2, v7, v8}, Lecj;->b(Lcom/google/common/cache/LocalCache$ReferenceEntry;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2957
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 2965
    invoke-direct {p0}, Lecj;->h()V

    move v0, v6

    goto :goto_1

    .line 2927
    :cond_3
    :try_start_3
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNext()Lcom/google/common/cache/LocalCache$ReferenceEntry;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 2962
    :cond_4
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 2965
    invoke-direct {p0}, Lecj;->h()V

    move v0, v6

    goto :goto_1

    .line 2964
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lecj;->unlock()V

    .line 2965
    invoke-direct {p0}, Lecj;->h()V

    throw v0
.end method

.method b()V
    .locals 2

    .prologue
    .line 3365
    iget-object v0, p0, Lecj;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    and-int/lit8 v0, v0, 0x3f

    if-nez v0, :cond_0

    .line 3366
    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v0, v0, Lcom/google/common/cache/LocalCache;->q:Lebm;

    invoke-virtual {v0}, Lebm;->a()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lecj;->c(J)V

    invoke-direct {p0}, Lecj;->h()V

    .line 3368
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/Object;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2728
    :try_start_0
    iget v1, p0, Lecj;->b:I

    if-eqz v1, :cond_2

    .line 2729
    iget-object v1, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v1, v1, Lcom/google/common/cache/LocalCache;->q:Lebm;

    invoke-virtual {v1}, Lebm;->a()J

    move-result-wide v1

    .line 2730
    invoke-direct {p0, p1, p2, v1, v2}, Lecj;->a(Ljava/lang/Object;IJ)Lcom/google/common/cache/LocalCache$ReferenceEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2731
    if-nez v1, :cond_0

    .line 2732
    invoke-virtual {p0}, Lecj;->b()V

    .line 2739
    :goto_0
    return v0

    .line 2734
    :cond_0
    :try_start_1
    invoke-interface {v1}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getValueReference()Lecw;

    move-result-object v1

    invoke-interface {v1}, Lecw;->get()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 2739
    :cond_1
    invoke-virtual {p0}, Lecj;->b()V

    goto :goto_0

    .line 2737
    :cond_2
    invoke-virtual {p0}, Lecj;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lecj;->b()V

    throw v0
.end method

.method public b(Ljava/lang/Object;ILjava/lang/Object;)Z
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 3122
    invoke-virtual {p0}, Lecj;->lock()V

    .line 3124
    :try_start_0
    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v0, v0, Lcom/google/common/cache/LocalCache;->q:Lebm;

    invoke-virtual {v0}, Lebm;->a()J

    move-result-wide v0

    .line 3125
    invoke-direct {p0, v0, v1}, Lecj;->c(J)V

    .line 3127
    iget v0, p0, Lecj;->b:I

    .line 3128
    iget-object v7, p0, Lecj;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3129
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v8, p2, v0

    .line 3130
    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-object v2, v1

    .line 3132
    :goto_0
    if-eqz v2, :cond_4

    .line 3133
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v3

    .line 3134
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getHash()I

    move-result v0

    if-ne v0, p2, :cond_3

    if-eqz v3, :cond_3

    iget-object v0, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v0, v0, Lcom/google/common/cache/LocalCache;->f:Leba;

    invoke-virtual {v0, p1, v3}, Leba;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3136
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getValueReference()Lecw;

    move-result-object v4

    .line 3137
    invoke-interface {v4}, Lecw;->get()Ljava/lang/Object;

    move-result-object v0

    .line 3140
    iget-object v5, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v5, v5, Lcom/google/common/cache/LocalCache;->g:Leba;

    invoke-virtual {v5, p3, v0}, Leba;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3141
    sget-object v5, Ledh;->a:Ledh;

    .line 3149
    :goto_1
    iget v0, p0, Lecj;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lecj;->d:I

    move-object v0, p0

    .line 3150
    invoke-direct/range {v0 .. v5}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;Ljava/lang/Object;Lecw;Ledh;)Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    .line 3152
    iget v1, p0, Lecj;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 3153
    invoke-virtual {v7, v8, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 3154
    iput v1, p0, Lecj;->b:I

    .line 3155
    sget-object v0, Ledh;->a:Ledh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v5, v0, :cond_2

    const/4 v0, 0x1

    .line 3161
    :goto_2
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3162
    invoke-direct {p0}, Lecj;->h()V

    move v6, v0

    :goto_3
    return v6

    .line 3142
    :cond_0
    if-nez v0, :cond_1

    :try_start_1
    invoke-interface {v4}, Lecw;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3143
    sget-object v5, Ledh;->c:Ledh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 3146
    :cond_1
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3162
    invoke-direct {p0}, Lecj;->h()V

    goto :goto_3

    :cond_2
    move v0, v6

    .line 3155
    goto :goto_2

    .line 3132
    :cond_3
    :try_start_2
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNext()Lcom/google/common/cache/LocalCache$ReferenceEntry;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 3159
    :cond_4
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3162
    invoke-direct {p0}, Lecj;->h()V

    goto :goto_3

    .line 3161
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3162
    invoke-direct {p0}, Lecj;->h()V

    throw v0
.end method

.method public c(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I)TV;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 3017
    invoke-virtual {p0}, Lecj;->lock()V

    .line 3019
    :try_start_0
    iget-object v1, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v1, v1, Lcom/google/common/cache/LocalCache;->q:Lebm;

    invoke-virtual {v1}, Lebm;->a()J

    move-result-wide v1

    .line 3020
    invoke-direct {p0, v1, v2}, Lecj;->c(J)V

    .line 3022
    iget v1, p0, Lecj;->b:I

    .line 3023
    iget-object v7, p0, Lecj;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3024
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int v8, p2, v1

    .line 3025
    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-object v2, v1

    .line 3027
    :goto_0
    if-eqz v2, :cond_3

    .line 3028
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getKey()Ljava/lang/Object;

    move-result-object v3

    .line 3029
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getHash()I

    move-result v4

    if-ne v4, p2, :cond_2

    if-eqz v3, :cond_2

    iget-object v4, p0, Lecj;->a:Lcom/google/common/cache/LocalCache;

    iget-object v4, v4, Lcom/google/common/cache/LocalCache;->f:Leba;

    invoke-virtual {v4, p1, v3}, Leba;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 3031
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getValueReference()Lecw;

    move-result-object v4

    .line 3032
    invoke-interface {v4}, Lecw;->get()Ljava/lang/Object;

    move-result-object v6

    .line 3035
    if-eqz v6, :cond_0

    .line 3036
    sget-object v5, Ledh;->a:Ledh;

    .line 3044
    :goto_1
    iget v0, p0, Lecj;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lecj;->d:I

    move-object v0, p0

    .line 3045
    invoke-direct/range {v0 .. v5}, Lecj;->a(Lcom/google/common/cache/LocalCache$ReferenceEntry;Lcom/google/common/cache/LocalCache$ReferenceEntry;Ljava/lang/Object;Lecw;Ledh;)Lcom/google/common/cache/LocalCache$ReferenceEntry;

    move-result-object v0

    .line 3047
    iget v1, p0, Lecj;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 3048
    invoke-virtual {v7, v8, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 3049
    iput v1, p0, Lecj;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3056
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3057
    invoke-direct {p0}, Lecj;->h()V

    move-object v0, v6

    :goto_2
    return-object v0

    .line 3037
    :cond_0
    :try_start_1
    invoke-interface {v4}, Lecw;->d()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 3038
    sget-object v5, Ledh;->c:Ledh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 3041
    :cond_1
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3057
    invoke-direct {p0}, Lecj;->h()V

    goto :goto_2

    .line 3027
    :cond_2
    :try_start_2
    invoke-interface {v2}, Lcom/google/common/cache/LocalCache$ReferenceEntry;->getNext()Lcom/google/common/cache/LocalCache$ReferenceEntry;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 3054
    :cond_3
    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3057
    invoke-direct {p0}, Lecj;->h()V

    goto :goto_2

    .line 3056
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lecj;->unlock()V

    .line 3057
    invoke-direct {p0}, Lecj;->h()V

    throw v0
.end method
