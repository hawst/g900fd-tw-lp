.class public final Lckg;
.super Lchr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lchr",
        "<",
        "Lcka;",
        ">;"
    }
.end annotation


# instance fields
.field private final f:Lckk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lckk",
            "<",
            "Lcka;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lckd;

.field private final h:Lcko;

.field private final i:Lcju;

.field private final j:Lcjs;

.field private final k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcfv;Lcfw;Ljava/lang/String;)V
    .locals 3

    const/4 v1, 0x0

    new-array v0, v1, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lchr;-><init>(Landroid/content/Context;Lcfv;Lcfw;[Ljava/lang/String;)V

    new-instance v0, Lckh;

    invoke-direct {v0, p0, v1}, Lckh;-><init>(Lckg;B)V

    iput-object v0, p0, Lckg;->f:Lckk;

    new-instance v0, Lckd;

    iget-object v1, p0, Lckg;->f:Lckk;

    invoke-direct {v0, p1, v1}, Lckd;-><init>(Landroid/content/Context;Lckk;)V

    iput-object v0, p0, Lckg;->g:Lckd;

    iput-object p4, p0, Lckg;->k:Ljava/lang/String;

    new-instance v0, Lcko;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lckg;->f:Lckk;

    invoke-direct {v0, v1, v2}, Lcko;-><init>(Ljava/lang/String;Lckk;)V

    iput-object v0, p0, Lckg;->h:Lcko;

    iget-object v0, p0, Lckg;->f:Lckk;

    new-instance v1, Lcju;

    invoke-direct {v1, p1, v0}, Lcju;-><init>(Landroid/content/Context;Lckk;)V

    iput-object v1, p0, Lckg;->i:Lcju;

    iget-object v0, p0, Lckg;->f:Lckk;

    new-instance v1, Lcjs;

    invoke-direct {v1, p1, v0}, Lcjs;-><init>(Landroid/content/Context;Lckk;)V

    iput-object v1, p0, Lckg;->j:Lcjs;

    return-void
.end method

.method static synthetic a(Lckg;)V
    .locals 0

    invoke-virtual {p0}, Lckg;->h()V

    return-void
.end method


# virtual methods
.method protected synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lckb;->a(Landroid/os/IBinder;)Lcka;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcim;Lchv;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "client_name"

    iget-object v2, p0, Lckg;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x5cc600

    iget-object v2, p0, Lchr;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lcim;->e(Lcij;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/internal/nh;Lcof;)V
    .locals 2

    iget-object v1, p0, Lckg;->g:Lckd;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lckg;->g:Lckd;

    invoke-virtual {v0, p1, p2}, Lckd;->a(Lcom/google/android/gms/internal/nh;Lcof;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public b()V
    .locals 2

    iget-object v1, p0, Lckg;->g:Lckd;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lckg;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lckg;->g:Lckd;

    invoke-virtual {v0}, Lckd;->b()V

    iget-object v0, p0, Lckg;->g:Lckd;

    invoke-virtual {v0}, Lckd;->c()V

    :cond_0
    invoke-super {p0}, Lchr;->b()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.location.internal.GoogleLocationManagerService.START"

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.location.internal.IGoogleLocationManagerService"

    return-object v0
.end method

.method public j()Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lckg;->g:Lckd;

    invoke-virtual {v0}, Lckd;->a()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method
