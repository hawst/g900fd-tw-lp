.class public final Lasg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lasg;->a:Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;B)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lasg;-><init>(Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 52
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Lasg;->a:Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v1}, Landroid/hardware/Sensor;->getMaximumRange()F

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->a(Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;F)F

    .line 59
    iget-object v0, p0, Lasg;->a:Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->b(Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;F)F

    .line 60
    iget-object v0, p0, Lasg;->a:Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->c()V

    .line 61
    return-void
.end method
