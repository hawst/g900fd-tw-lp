.class final Lcml;
.super Lcvx;


# instance fields
.field private volatile b:Z

.field private final c:I

.field private d:Lcom/google/android/gms/common/data/DataHolder;

.field private e:Landroid/database/Cursor;

.field private f:Lclf;

.field private g:Lclf;

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private j:Landroid/content/Context;

.field private k:Lcmp;

.field private l:Lcmp;

.field private final m:Z

.field private n:Lcnd;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;Landroid/database/Cursor;Landroid/content/Context;ILclf;Lclf;Ljava/util/ArrayList;Ljava/util/HashMap;ILandroid/os/Bundle;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/data/DataHolder;",
            "Landroid/database/Cursor;",
            "Landroid/content/Context;",
            "I",
            "Lclf;",
            "Lclf;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcvx;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-static {p1}, Lg;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lg;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p8}, Lg;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p5}, Lclf;->a()I

    move-result v0

    if-ne p4, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lg;->b(Z)V

    invoke-virtual {p6}, Lclf;->a()I

    move-result v0

    if-ne p4, v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lg;->b(Z)V

    invoke-virtual {p7}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne p4, v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Lg;->b(Z)V

    iput-object p1, p0, Lcml;->d:Lcom/google/android/gms/common/data/DataHolder;

    iput-object p2, p0, Lcml;->e:Landroid/database/Cursor;

    iput p4, p0, Lcml;->c:I

    iput-object p7, p0, Lcml;->h:Ljava/util/ArrayList;

    iput-object p3, p0, Lcml;->j:Landroid/content/Context;

    iput-object p8, p0, Lcml;->i:Ljava/util/HashMap;

    new-instance v0, Lcmm;

    iget-object v1, p0, Lcml;->j:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcmm;-><init>(Lcml;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcml;->k:Lcmp;

    new-instance v0, Lcmn;

    iget-object v1, p0, Lcml;->j:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcmn;-><init>(Lcml;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcml;->l:Lcmp;

    iput-object p5, p0, Lcml;->f:Lclf;

    iput-object p6, p0, Lcml;->g:Lclf;

    and-int/lit8 v0, p9, 0x1

    if-eqz v0, :cond_0

    const-string v0, "PeopleAggregator"

    const-string v1, "PeopleExtraColumnBitmask.EMAILS is not supported in aggregation.  Ignored."

    const-string v2, "PeopleService"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    and-int/lit8 v0, p9, 0x2

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_3
    iput-boolean v0, p0, Lcml;->m:Z

    new-instance v0, Lcnd;

    invoke-direct {v0, p10}, Lcnd;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcml;->n:Lcnd;

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    goto :goto_3
.end method

.method static synthetic a(Lcml;)Lclf;
    .locals 1

    iget-object v0, p0, Lcml;->f:Lclf;

    return-object v0
.end method

.method static synthetic b(Lcml;)Lclf;
    .locals 1

    iget-object v0, p0, Lcml;->g:Lclf;

    return-object v0
.end method

.method static synthetic c(Lcml;)V
    .locals 0

    invoke-direct {p0}, Lcml;->g()V

    return-void
.end method

.method static synthetic d(Lcml;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    iget-object v0, p0, Lcml;->d:Lcom/google/android/gms/common/data/DataHolder;

    return-object v0
.end method

.method static synthetic e(Lcml;)Landroid/database/Cursor;
    .locals 1

    iget-object v0, p0, Lcml;->e:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic f(Lcml;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcml;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method private g()V
    .locals 2

    iget-boolean v0, p0, Lcml;->b:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already released"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method static synthetic g(Lcml;)Z
    .locals 1

    iget-boolean v0, p0, Lcml;->m:Z

    return v0
.end method

.method static synthetic h(Lcml;)Lcnd;
    .locals 1

    iget-object v0, p0, Lcml;->n:Lcnd;

    return-object v0
.end method

.method static synthetic i(Lcml;)Lcmp;
    .locals 1

    iget-object v0, p0, Lcml;->k:Lcmp;

    return-object v0
.end method

.method static synthetic j(Lcml;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcml;->i:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic k(Lcml;)Lcmp;
    .locals 1

    iget-object v0, p0, Lcml;->l:Lcmp;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    invoke-direct {p0}, Lcml;->g()V

    iget v0, p0, Lcml;->c:I

    return v0
.end method

.method public synthetic a(I)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcml;->g()V

    new-instance v0, Lcmo;

    invoke-direct {v0, p0, p1}, Lcmo;-><init>(Lcml;I)V

    return-object v0
.end method

.method public b()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcml;->b:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcml;->b:Z

    iget-object v0, p0, Lcml;->d:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->i()V

    iget-object v0, p0, Lcml;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iput-object v1, p0, Lcml;->d:Lcom/google/android/gms/common/data/DataHolder;

    iput-object v1, p0, Lcml;->e:Landroid/database/Cursor;

    iput-object v1, p0, Lcml;->f:Lclf;

    iput-object v1, p0, Lcml;->g:Lclf;

    iput-object v1, p0, Lcml;->h:Ljava/util/ArrayList;

    iput-object v1, p0, Lcml;->i:Ljava/util/HashMap;

    iput-object v1, p0, Lcml;->j:Landroid/content/Context;

    iput-object v1, p0, Lcml;->k:Lcmp;

    iput-object v1, p0, Lcml;->l:Lcmp;

    iput-object v1, p0, Lcml;->n:Lcnd;

    goto :goto_0
.end method
