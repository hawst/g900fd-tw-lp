.class public final Ldel;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldel;


# instance fields
.field public b:Ldem;

.field public c:Ldem;

.field public d:Ldem;

.field public e:Ldem;

.field public f:Ldem;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7146
    const/4 v0, 0x0

    new-array v0, v0, [Ldel;

    sput-object v0, Ldel;->a:[Ldel;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7147
    invoke-direct {p0}, Lepn;-><init>()V

    .line 7150
    iput-object v0, p0, Ldel;->b:Ldem;

    .line 7153
    iput-object v0, p0, Ldel;->c:Ldem;

    .line 7156
    iput-object v0, p0, Ldel;->d:Ldem;

    .line 7159
    iput-object v0, p0, Ldel;->e:Ldem;

    .line 7162
    iput-object v0, p0, Ldel;->f:Ldem;

    .line 7147
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 7188
    const/4 v0, 0x0

    .line 7189
    iget-object v1, p0, Ldel;->b:Ldem;

    if-eqz v1, :cond_0

    .line 7190
    const/4 v0, 0x1

    iget-object v1, p0, Ldel;->b:Ldem;

    .line 7191
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7193
    :cond_0
    iget-object v1, p0, Ldel;->c:Ldem;

    if-eqz v1, :cond_1

    .line 7194
    const/4 v1, 0x2

    iget-object v2, p0, Ldel;->c:Ldem;

    .line 7195
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7197
    :cond_1
    iget-object v1, p0, Ldel;->d:Ldem;

    if-eqz v1, :cond_2

    .line 7198
    const/4 v1, 0x3

    iget-object v2, p0, Ldel;->d:Ldem;

    .line 7199
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7201
    :cond_2
    iget-object v1, p0, Ldel;->e:Ldem;

    if-eqz v1, :cond_3

    .line 7202
    const/4 v1, 0x4

    iget-object v2, p0, Ldel;->e:Ldem;

    .line 7203
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7205
    :cond_3
    iget-object v1, p0, Ldel;->f:Ldem;

    if-eqz v1, :cond_4

    .line 7206
    const/4 v1, 0x5

    iget-object v2, p0, Ldel;->f:Ldem;

    .line 7207
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7209
    :cond_4
    iget-object v1, p0, Ldel;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7210
    iput v0, p0, Ldel;->cachedSize:I

    .line 7211
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 7143
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldel;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldel;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldel;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldel;->b:Ldem;

    if-nez v0, :cond_2

    new-instance v0, Ldem;

    invoke-direct {v0}, Ldem;-><init>()V

    iput-object v0, p0, Ldel;->b:Ldem;

    :cond_2
    iget-object v0, p0, Ldel;->b:Ldem;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldel;->c:Ldem;

    if-nez v0, :cond_3

    new-instance v0, Ldem;

    invoke-direct {v0}, Ldem;-><init>()V

    iput-object v0, p0, Ldel;->c:Ldem;

    :cond_3
    iget-object v0, p0, Ldel;->c:Ldem;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldel;->d:Ldem;

    if-nez v0, :cond_4

    new-instance v0, Ldem;

    invoke-direct {v0}, Ldem;-><init>()V

    iput-object v0, p0, Ldel;->d:Ldem;

    :cond_4
    iget-object v0, p0, Ldel;->d:Ldem;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldel;->e:Ldem;

    if-nez v0, :cond_5

    new-instance v0, Ldem;

    invoke-direct {v0}, Ldem;-><init>()V

    iput-object v0, p0, Ldel;->e:Ldem;

    :cond_5
    iget-object v0, p0, Ldel;->e:Ldem;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldel;->f:Ldem;

    if-nez v0, :cond_6

    new-instance v0, Ldem;

    invoke-direct {v0}, Ldem;-><init>()V

    iput-object v0, p0, Ldel;->f:Ldem;

    :cond_6
    iget-object v0, p0, Ldel;->f:Ldem;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 7167
    iget-object v0, p0, Ldel;->b:Ldem;

    if-eqz v0, :cond_0

    .line 7168
    const/4 v0, 0x1

    iget-object v1, p0, Ldel;->b:Ldem;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7170
    :cond_0
    iget-object v0, p0, Ldel;->c:Ldem;

    if-eqz v0, :cond_1

    .line 7171
    const/4 v0, 0x2

    iget-object v1, p0, Ldel;->c:Ldem;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7173
    :cond_1
    iget-object v0, p0, Ldel;->d:Ldem;

    if-eqz v0, :cond_2

    .line 7174
    const/4 v0, 0x3

    iget-object v1, p0, Ldel;->d:Ldem;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7176
    :cond_2
    iget-object v0, p0, Ldel;->e:Ldem;

    if-eqz v0, :cond_3

    .line 7177
    const/4 v0, 0x4

    iget-object v1, p0, Ldel;->e:Ldem;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7179
    :cond_3
    iget-object v0, p0, Ldel;->f:Ldem;

    if-eqz v0, :cond_4

    .line 7180
    const/4 v0, 0x5

    iget-object v1, p0, Ldel;->f:Ldem;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7182
    :cond_4
    iget-object v0, p0, Ldel;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 7184
    return-void
.end method
