.class public final Ldml;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldml;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5161
    const/4 v0, 0x0

    new-array v0, v0, [Ldml;

    sput-object v0, Ldml;->a:[Ldml;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5162
    invoke-direct {p0}, Lepn;-><init>()V

    .line 5175
    const/4 v0, 0x0

    iput-object v0, p0, Ldml;->d:Ljava/lang/Integer;

    .line 5162
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 5200
    const/4 v0, 0x0

    .line 5201
    iget-object v1, p0, Ldml;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5202
    const/4 v0, 0x1

    iget-object v1, p0, Ldml;->b:Ljava/lang/String;

    .line 5203
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5205
    :cond_0
    iget-object v1, p0, Ldml;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 5206
    const/4 v1, 0x2

    iget-object v2, p0, Ldml;->c:Ljava/lang/String;

    .line 5207
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5209
    :cond_1
    iget-object v1, p0, Ldml;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 5210
    const/4 v1, 0x3

    iget-object v2, p0, Ldml;->d:Ljava/lang/Integer;

    .line 5211
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5213
    :cond_2
    iget-object v1, p0, Ldml;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 5214
    const/4 v1, 0x4

    iget-object v2, p0, Ldml;->e:Ljava/lang/String;

    .line 5215
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5217
    :cond_3
    iget-object v1, p0, Ldml;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5218
    iput v0, p0, Ldml;->cachedSize:I

    .line 5219
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 5158
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldml;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldml;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldml;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldml;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldml;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldml;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldml;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldml;->e:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 5182
    iget-object v0, p0, Ldml;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 5183
    const/4 v0, 0x1

    iget-object v1, p0, Ldml;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 5185
    :cond_0
    iget-object v0, p0, Ldml;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 5186
    const/4 v0, 0x2

    iget-object v1, p0, Ldml;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 5188
    :cond_1
    iget-object v0, p0, Ldml;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 5189
    const/4 v0, 0x3

    iget-object v1, p0, Ldml;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 5191
    :cond_2
    iget-object v0, p0, Ldml;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 5192
    const/4 v0, 0x4

    iget-object v1, p0, Ldml;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 5194
    :cond_3
    iget-object v0, p0, Ldml;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 5196
    return-void
.end method
