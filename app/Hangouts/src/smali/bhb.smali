.class public Lbhb;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation
.end field

.field private final h:[B

.field private final i:Z

.field private final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation
.end field

.field private final k:[B

.field private final l:Z

.field private final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation
.end field

.field private final n:[B

.field private final o:Z


# direct methods
.method private constructor <init>(Ldst;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 1991
    iget-object v0, p1, Ldst;->b:Ldvn;

    const-wide/16 v3, -0x1

    invoke-direct {p0, v0, v3, v4}, Lbfz;-><init>(Ldvn;J)V

    .line 1993
    iget-object v0, p1, Ldst;->f:Ldsv;

    if-eqz v0, :cond_0

    iget-object v0, p1, Ldst;->f:Ldsv;

    iget-object v0, v0, Ldsv;->b:Ljava/lang/Boolean;

    .line 1994
    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lbhb;->i:Z

    .line 1995
    iget-boolean v0, p0, Lbhb;->i:Z

    if-nez v0, :cond_6

    .line 1996
    iget-object v0, p1, Ldst;->f:Ldsv;

    iget-object v0, v0, Ldsv;->c:[B

    iput-object v0, p0, Lbhb;->h:[B

    .line 1997
    iget-object v0, p1, Ldst;->f:Ldsv;

    iget-object v0, v0, Ldsv;->d:[Ldsu;

    invoke-static {v0}, Lbdh;->a([Ldsu;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbhb;->g:Ljava/util/List;

    .line 2004
    :goto_1
    iget-object v0, p1, Ldst;->g:Ldsv;

    if-eqz v0, :cond_1

    iget-object v0, p1, Ldst;->g:Ldsv;

    iget-object v0, v0, Ldsv;->b:Ljava/lang/Boolean;

    .line 2005
    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_1
    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lbhb;->l:Z

    .line 2006
    iget-boolean v0, p0, Lbhb;->l:Z

    if-nez v0, :cond_8

    .line 2007
    iget-object v0, p1, Ldst;->g:Ldsv;

    iget-object v0, v0, Ldsv;->c:[B

    iput-object v0, p0, Lbhb;->k:[B

    .line 2009
    iget-object v0, p1, Ldst;->g:Ldsv;

    iget-object v0, v0, Ldsv;->d:[Ldsu;

    invoke-static {v0}, Lbdh;->a([Ldsu;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbhb;->j:Ljava/util/List;

    .line 2016
    :goto_3
    iget-object v0, p1, Ldst;->j:Ldsv;

    if-eqz v0, :cond_2

    iget-object v0, p1, Ldst;->j:Ldsv;

    iget-object v0, v0, Ldsv;->b:Ljava/lang/Boolean;

    .line 2017
    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v1, v2

    :cond_3
    iput-boolean v1, p0, Lbhb;->o:Z

    .line 2018
    iget-boolean v0, p0, Lbhb;->o:Z

    if-nez v0, :cond_9

    .line 2019
    iget-object v0, p1, Ldst;->j:Ldsv;

    iget-object v0, v0, Ldsv;->c:[B

    iput-object v0, p0, Lbhb;->n:[B

    .line 2021
    iget-object v0, p1, Ldst;->j:Ldsv;

    iget-object v0, v0, Ldsv;->d:[Ldsu;

    invoke-static {v0}, Lbdh;->a([Ldsu;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbhb;->m:Ljava/util/List;

    .line 2028
    :goto_4
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_4

    .line 2029
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetSuggestedEntitiesResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2032
    :cond_4
    return-void

    :cond_5
    move v0, v1

    .line 1994
    goto :goto_0

    .line 2000
    :cond_6
    iput-object v5, p0, Lbhb;->h:[B

    .line 2001
    iput-object v5, p0, Lbhb;->g:Ljava/util/List;

    goto :goto_1

    :cond_7
    move v0, v1

    .line 2005
    goto :goto_2

    .line 2012
    :cond_8
    iput-object v5, p0, Lbhb;->k:[B

    .line 2013
    iput-object v5, p0, Lbhb;->j:Ljava/util/List;

    goto :goto_3

    .line 2024
    :cond_9
    iput-object v5, p0, Lbhb;->n:[B

    .line 2025
    iput-object v5, p0, Lbhb;->m:Ljava/util/List;

    goto :goto_4
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 2036
    new-instance v0, Ldst;

    invoke-direct {v0}, Ldst;-><init>()V

    .line 2037
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldst;

    .line 2038
    iget-object v1, v0, Ldst;->b:Ldvn;

    invoke-static {v1}, Lbhb;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2039
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldst;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 2041
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbhb;

    invoke-direct {v1, v0}, Lbhb;-><init>(Ldst;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 4

    .prologue
    .line 2048
    invoke-super {p0, p1, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 2050
    invoke-virtual {p1}, Lyt;->a()V

    .line 2052
    :try_start_0
    iget-boolean v0, p0, Lbhb;->i:Z

    if-nez v0, :cond_2

    .line 2053
    iget-object v0, p0, Lbhb;->g:Ljava/util/List;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lyt;->a(Ljava/util/List;I)V

    .line 2055
    invoke-virtual {p1}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v0

    const-string v1, "hash_favorites"

    iget-object v2, p0, Lbhb;->h:[B

    invoke-virtual {v0, v1, v2}, Lbsx;->a(Ljava/lang/String;[B)V

    .line 2057
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 2058
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Favorites size:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbhb;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2059
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Set hash for favorites:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbhb;->h:[B

    invoke-static {v2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2065
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lbhb;->l:Z

    if-nez v0, :cond_3

    .line 2066
    iget-object v0, p0, Lbhb;->j:Ljava/util/List;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lyt;->a(Ljava/util/List;I)V

    .line 2068
    invoke-virtual {p1}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v0

    const-string v1, "hash_people_you_hangout_with"

    iget-object v2, p0, Lbhb;->k:[B

    invoke-virtual {v0, v1, v2}, Lbsx;->a(Ljava/lang/String;[B)V

    .line 2071
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_1

    .line 2072
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Contacts you hangout with size:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbhb;->j:Ljava/util/List;

    .line 2073
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2072
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2074
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Set hash for people you hangout with:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbhb;->k:[B

    .line 2075
    invoke-static {v2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2074
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2081
    :cond_1
    :goto_1
    iget-boolean v0, p0, Lbhb;->o:Z

    if-nez v0, :cond_6

    .line 2082
    invoke-virtual {p1}, Lyt;->p()V

    .line 2083
    iget-object v0, p0, Lbhb;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 2084
    iget-object v2, v0, Lbdh;->b:Lbdk;

    iget-object v2, v2, Lbdk;->a:Ljava/lang/String;

    iget-object v3, v0, Lbdh;->e:Ljava/lang/String;

    iget-object v0, v0, Lbdh;->h:Ljava/lang/String;

    invoke-virtual {p1, v2, v3, v0}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 2102
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lyt;->c()V

    throw v0

    .line 2062
    :cond_2
    :try_start_1
    const-string v0, "Babel"

    const-string v1, "Hash matched for favorites. Skip updating"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2078
    :cond_3
    const-string v0, "Babel"

    const-string v1, "Hash matched for people you hangout with. Skip updating"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2087
    :cond_4
    invoke-virtual {p1}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v0

    const-string v1, "hash_dismissed_contacts"

    iget-object v2, p0, Lbhb;->n:[B

    invoke-virtual {v0, v1, v2}, Lbsx;->a(Ljava/lang/String;[B)V

    .line 2090
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_5

    .line 2091
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Dismissed contacts size:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbhb;->m:Ljava/util/List;

    .line 2092
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2091
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2093
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Set hash for dismissed contacts:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbhb;->n:[B

    .line 2094
    invoke-static {v2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2093
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2100
    :cond_5
    :goto_3
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2102
    invoke-virtual {p1}, Lyt;->c()V

    .line 2103
    return-void

    .line 2097
    :cond_6
    :try_start_2
    const-string v0, "Babel"

    const-string v1, "Hash matched for dismissed contacts. Skip updating"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method public f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2107
    iget-object v0, p0, Lbhb;->g:Ljava/util/List;

    return-object v0
.end method

.method public g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2111
    iget-object v0, p0, Lbhb;->j:Ljava/util/List;

    return-object v0
.end method
