.class public interface abstract Lbmt;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 60
    const/16 v0, 0x1e

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "author_first_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "author_full_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "author_chat_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "author_profile_photo_url"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "text"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "local_url"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "remote_url"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "conversation_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "conversation_name"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "generated_name"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "conversation_type"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "conversation_status"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "conversation_view"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "message_id"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "status"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "attachment_content_type"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "transport_type"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "transport_phone"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "author_gaia_id"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "new_conversation_name"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "participant_keys"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "merge_key"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "sms_type"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "chat_watermark"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "chat_ringtone_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "otr_status"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "call_media_type"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "wearable_watermark"

    aput-object v2, v0, v1

    sput-object v0, Lbmt;->a:[Ljava/lang/String;

    return-void
.end method
