.class public Lajq;
.super Lt;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lt;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateAnimation(IZI)Landroid/view/animation/Animation;
    .locals 3

    .prologue
    .line 29
    if-eqz p2, :cond_0

    .line 32
    invoke-virtual {p0}, Lajq;->getActivity()Ly;

    move-result-object v0

    const/high16 v1, 0x10a0000

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 33
    invoke-virtual {p0}, Lajq;->getActivity()Ly;

    move-result-object v1

    invoke-virtual {v1}, Ly;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->dL:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 35
    new-instance v1, Lajr;

    invoke-direct {v1, p0}, Lajr;-><init>(Lajq;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 57
    :goto_0
    return-object v0

    .line 54
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lt;->onCreateAnimation(IZI)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 23
    sget v0, Lf;->fd:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 24
    return-object v0
.end method
