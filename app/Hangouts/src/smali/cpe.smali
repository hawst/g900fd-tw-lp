.class public Lcpe;
.super Landroid/widget/FrameLayout;


# instance fields
.field private final a:Lcpg;

.field private b:Lcox;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcpg;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcpg;-><init>(Landroid/view/ViewGroup;Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V

    iput-object v0, p0, Lcpe;->a:Lcpg;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcpg;

    invoke-static {p1, p2}, Lcom/google/android/gms/maps/GoogleMapOptions;->a(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/google/android/gms/maps/GoogleMapOptions;

    move-result-object v1

    invoke-direct {v0, p0, p1, v1}, Lcpg;-><init>(Landroid/view/ViewGroup;Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V

    iput-object v0, p0, Lcpe;->a:Lcpg;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcpg;

    invoke-static {p1, p2}, Lcom/google/android/gms/maps/GoogleMapOptions;->a(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/google/android/gms/maps/GoogleMapOptions;

    move-result-object v1

    invoke-direct {v0, p0, p1, v1}, Lcpg;-><init>(Landroid/view/ViewGroup;Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V

    iput-object v0, p0, Lcpe;->a:Lcpg;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcpg;

    invoke-direct {v0, p0, p1, p2}, Lcpg;-><init>(Landroid/view/ViewGroup;Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V

    iput-object v0, p0, Lcpe;->a:Lcpg;

    return-void
.end method


# virtual methods
.method public final a()Lcox;
    .locals 2

    iget-object v0, p0, Lcpe;->b:Lcox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcpe;->b:Lcox;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcpe;->a:Lcpg;

    invoke-virtual {v0}, Lcpg;->g()V

    iget-object v0, p0, Lcpe;->a:Lcpg;

    invoke-virtual {v0}, Lcpg;->a()Lcit;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    :try_start_0
    new-instance v1, Lcox;

    iget-object v0, p0, Lcpe;->a:Lcpg;

    invoke-virtual {v0}, Lcpg;->a()Lcit;

    move-result-object v0

    check-cast v0, Lcpf;

    invoke-virtual {v0}, Lcpf;->f()Lcpy;

    move-result-object v0

    invoke-interface {v0}, Lcpy;->a()Lcpp;

    move-result-object v0

    invoke-direct {v1, v0}, Lcox;-><init>(Lcpp;)V

    iput-object v1, p0, Lcpe;->b:Lcox;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcpe;->b:Lcox;

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lv;

    invoke-direct {v1, v0}, Lv;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcpe;->a:Lcpg;

    invoke-virtual {v0, p1}, Lcpg;->b(Landroid/os/Bundle;)V

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcpe;->a:Lcpg;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcpg;->a(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcpe;->a:Lcpg;

    invoke-virtual {v0}, Lcpg;->a()Lcit;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcpe;->a:Lcpg;

    invoke-static {p0}, Lcpg;->b(Landroid/widget/FrameLayout;)V

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcpe;->a:Lcpg;

    invoke-virtual {v0}, Lcpg;->b()V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcpe;->a:Lcpg;

    invoke-virtual {v0}, Lcpg;->c()V

    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcpe;->a:Lcpg;

    invoke-virtual {v0}, Lcpg;->e()V

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcpe;->a:Lcpg;

    invoke-virtual {v0}, Lcpg;->f()V

    return-void
.end method
