.class public final Ldzh;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldzh;


# instance fields
.field public b:Ldvm;

.field public c:Ljava/lang/String;

.field public d:Ldzj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 699
    const/4 v0, 0x0

    new-array v0, v0, [Ldzh;

    sput-object v0, Ldzh;->a:[Ldzh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 700
    invoke-direct {p0}, Lepn;-><init>()V

    .line 703
    iput-object v0, p0, Ldzh;->b:Ldvm;

    .line 708
    iput-object v0, p0, Ldzh;->d:Ldzj;

    .line 700
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 728
    const/4 v0, 0x0

    .line 729
    iget-object v1, p0, Ldzh;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 730
    const/4 v0, 0x1

    iget-object v1, p0, Ldzh;->b:Ldvm;

    .line 731
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 733
    :cond_0
    iget-object v1, p0, Ldzh;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 734
    const/4 v1, 0x2

    iget-object v2, p0, Ldzh;->c:Ljava/lang/String;

    .line 735
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 737
    :cond_1
    iget-object v1, p0, Ldzh;->d:Ldzj;

    if-eqz v1, :cond_2

    .line 738
    const/4 v1, 0x3

    iget-object v2, p0, Ldzh;->d:Ldzj;

    .line 739
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 741
    :cond_2
    iget-object v1, p0, Ldzh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 742
    iput v0, p0, Ldzh;->cachedSize:I

    .line 743
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 696
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldzh;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldzh;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldzh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldzh;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldzh;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldzh;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldzh;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldzh;->d:Ldzj;

    if-nez v0, :cond_3

    new-instance v0, Ldzj;

    invoke-direct {v0}, Ldzj;-><init>()V

    iput-object v0, p0, Ldzh;->d:Ldzj;

    :cond_3
    iget-object v0, p0, Ldzh;->d:Ldzj;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 713
    iget-object v0, p0, Ldzh;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 714
    const/4 v0, 0x1

    iget-object v1, p0, Ldzh;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 716
    :cond_0
    iget-object v0, p0, Ldzh;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 717
    const/4 v0, 0x2

    iget-object v1, p0, Ldzh;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 719
    :cond_1
    iget-object v0, p0, Ldzh;->d:Ldzj;

    if-eqz v0, :cond_2

    .line 720
    const/4 v0, 0x3

    iget-object v1, p0, Ldzh;->d:Ldzj;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 722
    :cond_2
    iget-object v0, p0, Ldzh;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 724
    return-void
.end method
