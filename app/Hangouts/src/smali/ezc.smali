.class public final Lezc;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lezc;


# instance fields
.field public b:[Lezg;

.field public c:Ljava/lang/Long;

.field public d:Lezb;

.field public e:Ljava/lang/Integer;

.field public f:Lezd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lezc;

    sput-object v0, Lezc;->a:[Lezc;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 20
    sget-object v0, Lezg;->a:[Lezg;

    iput-object v0, p0, Lezc;->b:[Lezg;

    .line 25
    iput-object v1, p0, Lezc;->d:Lezb;

    .line 28
    iput-object v1, p0, Lezc;->e:Ljava/lang/Integer;

    .line 31
    iput-object v1, p0, Lezc;->f:Lezd;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 62
    iget-object v1, p0, Lezc;->b:[Lezg;

    if-eqz v1, :cond_1

    .line 63
    iget-object v2, p0, Lezc;->b:[Lezg;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 64
    if-eqz v4, :cond_0

    .line 65
    const/4 v5, 0x1

    .line 66
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 63
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 70
    :cond_1
    iget-object v1, p0, Lezc;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 71
    const/4 v1, 0x2

    iget-object v2, p0, Lezc;->c:Ljava/lang/Long;

    .line 72
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    :cond_2
    iget-object v1, p0, Lezc;->d:Lezb;

    if-eqz v1, :cond_3

    .line 75
    const/4 v1, 0x3

    iget-object v2, p0, Lezc;->d:Lezb;

    .line 76
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    :cond_3
    iget-object v1, p0, Lezc;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 79
    const/4 v1, 0x4

    iget-object v2, p0, Lezc;->e:Ljava/lang/Integer;

    .line 80
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 82
    :cond_4
    iget-object v1, p0, Lezc;->f:Lezd;

    if-eqz v1, :cond_5

    .line 83
    const/4 v1, 0x5

    iget-object v2, p0, Lezc;->f:Lezd;

    .line 84
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 86
    :cond_5
    iget-object v1, p0, Lezc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    iput v0, p0, Lezc;->cachedSize:I

    .line 88
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lezc;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lezc;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lezc;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lezc;->b:[Lezg;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lezg;

    iget-object v3, p0, Lezc;->b:[Lezg;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lezc;->b:[Lezg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lezc;->b:[Lezg;

    :goto_2
    iget-object v2, p0, Lezc;->b:[Lezg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lezc;->b:[Lezg;

    new-instance v3, Lezg;

    invoke-direct {v3}, Lezg;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lezc;->b:[Lezg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lezc;->b:[Lezg;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lezc;->b:[Lezg;

    new-instance v3, Lezg;

    invoke-direct {v3}, Lezg;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lezc;->b:[Lezg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lezc;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lezc;->d:Lezb;

    if-nez v0, :cond_5

    new-instance v0, Lezb;

    invoke-direct {v0}, Lezb;-><init>()V

    iput-object v0, p0, Lezc;->d:Lezb;

    :cond_5
    iget-object v0, p0, Lezc;->d:Lezb;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_6

    const/4 v2, 0x1

    if-eq v0, v2, :cond_6

    const/4 v2, 0x2

    if-eq v0, v2, :cond_6

    const/4 v2, 0x3

    if-ne v0, v2, :cond_7

    :cond_6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lezc;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lezc;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lezc;->f:Lezd;

    if-nez v0, :cond_8

    new-instance v0, Lezd;

    invoke-direct {v0}, Lezd;-><init>()V

    iput-object v0, p0, Lezc;->f:Lezd;

    :cond_8
    iget-object v0, p0, Lezc;->f:Lezd;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 36
    iget-object v0, p0, Lezc;->b:[Lezg;

    if-eqz v0, :cond_1

    .line 37
    iget-object v1, p0, Lezc;->b:[Lezg;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 38
    if-eqz v3, :cond_0

    .line 39
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 37
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 43
    :cond_1
    iget-object v0, p0, Lezc;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 44
    const/4 v0, 0x2

    iget-object v1, p0, Lezc;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 46
    :cond_2
    iget-object v0, p0, Lezc;->d:Lezb;

    if-eqz v0, :cond_3

    .line 47
    const/4 v0, 0x3

    iget-object v1, p0, Lezc;->d:Lezb;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 49
    :cond_3
    iget-object v0, p0, Lezc;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 50
    const/4 v0, 0x4

    iget-object v1, p0, Lezc;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 52
    :cond_4
    iget-object v0, p0, Lezc;->f:Lezd;

    if-eqz v0, :cond_5

    .line 53
    const/4 v0, 0x5

    iget-object v1, p0, Lezc;->f:Lezd;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 55
    :cond_5
    iget-object v0, p0, Lezc;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 57
    return-void
.end method
