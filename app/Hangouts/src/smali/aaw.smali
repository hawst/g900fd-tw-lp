.class public final Laaw;
.super Lalw;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lalw",
        "<",
        "Laax;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/View;Laax;)V
    .locals 1

    .prologue
    .line 29
    sget v0, Lg;->i:I

    invoke-direct {p0, p1, v0, p2}, Lalw;-><init>(Landroid/view/View;ILjava/lang/Object;)V

    .line 30
    return-void
.end method


# virtual methods
.method protected a()Z
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Laaw;->b:Ljava/lang/Object;

    check-cast v0, Laax;

    invoke-interface {v0}, Laax;->a()Lyj;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 35
    invoke-static {}, Lbkb;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laaw;->b:Ljava/lang/Object;

    check-cast v0, Laax;

    .line 36
    invoke-interface {v0}, Laax;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laaw;->b:Ljava/lang/Object;

    check-cast v0, Laax;

    .line 37
    invoke-interface {v0}, Laax;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Laaw;->b:Ljava/lang/Object;

    check-cast v0, Laax;

    .line 39
    invoke-interface {v0}, Laax;->d()I

    move-result v0

    .line 38
    invoke-static {v0}, Lf;->d(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b()V
    .locals 8

    .prologue
    .line 44
    iget-object v0, p0, Laaw;->b:Ljava/lang/Object;

    check-cast v0, Laax;

    invoke-interface {v0}, Laax;->a()Lyj;

    move-result-object v2

    .line 45
    iget-object v0, p0, Laaw;->a:Landroid/view/ViewGroup;

    sget v1, Lg;->j:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 47
    iget-object v1, p0, Laaw;->a:Landroid/view/ViewGroup;

    sget v3, Lg;->gE:I

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/hangouts/views/AvatarView;

    .line 49
    iget-object v3, p0, Laaw;->a:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lh;->kV:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 50
    invoke-virtual {v2}, Lyj;->f()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 49
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    invoke-virtual {v2}, Lyj;->F()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Ljava/lang/String;Lyj;)V

    .line 52
    return-void
.end method

.method protected c()V
    .locals 0

    .prologue
    .line 55
    return-void
.end method
