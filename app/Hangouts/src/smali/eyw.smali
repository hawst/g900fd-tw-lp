.class public final Leyw;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leyw;


# instance fields
.field public b:Leyy;

.field public c:Leyz;

.field public d:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 417
    const/4 v0, 0x0

    new-array v0, v0, [Leyw;

    sput-object v0, Leyw;->a:[Leyw;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 418
    invoke-direct {p0}, Lepn;-><init>()V

    .line 444
    iput-object v0, p0, Leyw;->b:Leyy;

    .line 447
    iput-object v0, p0, Leyw;->c:Leyz;

    .line 450
    iput-object v0, p0, Leyw;->d:Ljava/lang/Integer;

    .line 418
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 470
    const/4 v0, 0x0

    .line 471
    iget-object v1, p0, Leyw;->b:Leyy;

    if-eqz v1, :cond_0

    .line 472
    const/4 v0, 0x1

    iget-object v1, p0, Leyw;->b:Leyy;

    .line 473
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 475
    :cond_0
    iget-object v1, p0, Leyw;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 476
    const/4 v1, 0x2

    iget-object v2, p0, Leyw;->d:Ljava/lang/Integer;

    .line 477
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 479
    :cond_1
    iget-object v1, p0, Leyw;->c:Leyz;

    if-eqz v1, :cond_2

    .line 480
    const/4 v1, 0x3

    iget-object v2, p0, Leyw;->c:Leyz;

    .line 481
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 483
    :cond_2
    iget-object v1, p0, Leyw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 484
    iput v0, p0, Leyw;->cachedSize:I

    .line 485
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/16 v2, 0x2b

    .line 414
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leyw;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leyw;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leyw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Leyw;->b:Leyy;

    if-nez v0, :cond_2

    new-instance v0, Leyy;

    invoke-direct {v0}, Leyy;-><init>()V

    iput-object v0, p0, Leyw;->b:Leyy;

    :cond_2
    iget-object v0, p0, Leyw;->b:Leyy;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_3

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_3

    const/16 v1, 0xdc

    if-eq v0, v1, :cond_3

    const/16 v1, 0xdd

    if-eq v0, v1, :cond_3

    const/16 v1, 0xde

    if-eq v0, v1, :cond_3

    const/16 v1, 0xdf

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe0

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe1

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe2

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe3

    if-eq v0, v1, :cond_3

    const/16 v1, 0x12c

    if-eq v0, v1, :cond_3

    const/16 v1, 0x12e

    if-eq v0, v1, :cond_3

    const/16 v1, 0xf0

    if-eq v0, v1, :cond_3

    const/16 v1, 0xf1

    if-eq v0, v1, :cond_3

    const/16 v1, 0x138

    if-eq v0, v1, :cond_3

    const/16 v1, 0x13a

    if-eq v0, v1, :cond_3

    const/16 v1, 0x13b

    if-eq v0, v1, :cond_3

    const/16 v1, 0x13c

    if-eq v0, v1, :cond_3

    const/16 v1, 0x3a98

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyw;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyw;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_3
    iget-object v0, p0, Leyw;->c:Leyz;

    if-nez v0, :cond_5

    new-instance v0, Leyz;

    invoke-direct {v0}, Leyz;-><init>()V

    iput-object v0, p0, Leyw;->c:Leyz;

    :cond_5
    iget-object v0, p0, Leyw;->c:Leyz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 455
    iget-object v0, p0, Leyw;->b:Leyy;

    if-eqz v0, :cond_0

    .line 456
    const/4 v0, 0x1

    iget-object v1, p0, Leyw;->b:Leyy;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 458
    :cond_0
    iget-object v0, p0, Leyw;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 459
    const/4 v0, 0x2

    iget-object v1, p0, Leyw;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 461
    :cond_1
    iget-object v0, p0, Leyw;->c:Leyz;

    if-eqz v0, :cond_2

    .line 462
    const/4 v0, 0x3

    iget-object v1, p0, Leyw;->c:Leyz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 464
    :cond_2
    iget-object v0, p0, Leyw;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 466
    return-void
.end method
