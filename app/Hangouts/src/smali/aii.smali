.class public final Laii;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbcf;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

.field private final b:Lbcd;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)V
    .locals 2

    .prologue
    .line 1758
    iput-object p1, p0, Laii;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1759
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 1760
    new-instance v0, Lbcd;

    invoke-direct {v0, p0}, Lbcd;-><init>(Lbcf;)V

    iput-object v0, p0, Laii;->b:Lbcd;

    .line 1764
    :goto_0
    return-void

    .line 1762
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Laii;->b:Lbcd;

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/widget/AbsListView$MultiChoiceModeListener;
    .locals 1

    .prologue
    .line 1770
    iget-object v0, p0, Laii;->b:Lbcd;

    invoke-virtual {v0}, Lbcd;->a()Landroid/widget/AbsListView$MultiChoiceModeListener;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbcd;IZ)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1927
    iget-object v0, p0, Laii;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v2

    .line 1930
    invoke-virtual {v2, p2, p3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 1934
    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->size()I

    move-result v3

    move v0, v1

    .line 1935
    :goto_0
    if-ge v1, v3, :cond_1

    .line 1936
    invoke-virtual {v2, v1}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1937
    add-int/lit8 v0, v0, 0x1

    .line 1935
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1940
    :cond_1
    if-nez v0, :cond_2

    .line 1941
    iget-object v0, p0, Laii;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->j(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)Lbcd;

    move-result-object v0

    invoke-virtual {v0}, Lbcd;->c()V

    .line 1943
    :cond_2
    invoke-virtual {p1}, Lbcd;->b()V

    .line 1944
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 1911
    iget-object v0, p0, Laii;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->getCheckedItemIds()[J

    move-result-object v0

    .line 1912
    array-length v1, v0

    if-lez v1, :cond_0

    .line 1913
    iget-object v1, p0, Laii;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    iget-object v2, p0, Laii;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    iget-object v2, v2, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->g:Lbai;

    invoke-static {v1, v2, p1, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;Ljd;Landroid/view/MenuItem;[J)V

    .line 1916
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lbcd;Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 1775
    iget-object v0, p0, Laii;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-static {v0, p1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;Lbcd;)Lbcd;

    .line 1777
    iget-object v0, p0, Laii;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 1778
    sget v1, Lf;->gS:I

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1780
    const/4 v0, 0x1

    return v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 1921
    iget-object v0, p0, Laii;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;Lbcd;)Lbcd;

    .line 1922
    return-void
.end method

.method public b(Lbcd;Landroid/view/Menu;)Z
    .locals 24

    .prologue
    .line 1785
    const/4 v1, 0x0

    :goto_0
    invoke-interface/range {p2 .. p2}, Landroid/view/Menu;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1786
    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1785
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1789
    :cond_0
    const/4 v9, 0x0

    .line 1790
    const/4 v4, 0x0

    .line 1791
    const/4 v11, 0x1

    .line 1792
    const/4 v8, 0x1

    .line 1793
    const/4 v3, 0x1

    .line 1794
    const/4 v10, 0x1

    .line 1795
    const/4 v6, 0x0

    .line 1797
    move-object/from16 v0, p0

    iget-object v1, v0, Laii;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    iget-object v1, v1, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    check-cast v1, Lcom/google/android/apps/hangouts/listui/SwipeableListView;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v14

    .line 1798
    invoke-virtual {v14}, Landroid/util/SparseBooleanArray;->size()I

    move-result v15

    .line 1800
    move-object/from16 v0, p0

    iget-object v1, v0, Laii;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    iget-object v1, v1, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    check-cast v1, Lcom/google/android/apps/hangouts/listui/SwipeableListView;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v1, v0, Laii;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    iget-object v1, v1, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    check-cast v1, Lcom/google/android/apps/hangouts/listui/SwipeableListView;

    .line 1801
    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->getHeaderViewsCount()I

    move-result v1

    sub-int/2addr v2, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Laii;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    iget-object v1, v1, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    check-cast v1, Lcom/google/android/apps/hangouts/listui/SwipeableListView;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->getFooterViewsCount()I

    move-result v1

    sub-int v1, v2, v1

    .line 1802
    const/4 v2, 0x0

    .line 1803
    const/4 v5, 0x1

    .line 1804
    const/4 v7, 0x0

    .line 1805
    if-lez v1, :cond_18

    .line 1806
    move-object/from16 v0, p0

    iget-object v1, v0, Laii;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    iget-object v1, v1, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    check-cast v1, Lcom/google/android/apps/hangouts/listui/SwipeableListView;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->getHeaderViewsCount()I

    move-result v16

    .line 1807
    const/4 v1, 0x0

    move v13, v1

    move v1, v2

    move v2, v3

    move v3, v4

    move v4, v9

    :goto_1
    if-ge v13, v15, :cond_9

    .line 1808
    invoke-virtual {v14, v13}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v9

    if-eqz v9, :cond_17

    .line 1809
    add-int/lit8 v9, v1, 0x1

    .line 1810
    move-object/from16 v0, p0

    iget-object v1, v0, Laii;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    iget-object v1, v1, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    check-cast v1, Lcom/google/android/apps/hangouts/listui/SwipeableListView;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    .line 1811
    invoke-virtual {v14, v13}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v12

    sub-int v12, v12, v16

    .line 1810
    invoke-interface {v1, v12}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .line 1812
    instance-of v12, v1, Landroid/database/Cursor;

    if-eqz v12, :cond_16

    .line 1813
    check-cast v1, Landroid/database/Cursor;

    .line 1816
    if-eqz v1, :cond_16

    .line 1817
    const/4 v2, 0x1

    .line 1818
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 1819
    const/4 v2, 0x3

    .line 1820
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 1821
    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/16 v8, 0xa

    if-ne v2, v8, :cond_4

    const/4 v2, 0x1

    .line 1824
    :goto_2
    const/16 v8, 0x24

    .line 1825
    invoke-interface {v1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 1824
    invoke-static {v8}, Lf;->c(I)Z

    move-result v20

    .line 1826
    if-nez v20, :cond_15

    .line 1827
    const/4 v8, 0x0

    .line 1830
    :goto_3
    const-wide/16 v21, 0x2

    cmp-long v5, v18, v21

    if-nez v5, :cond_1

    if-eqz v20, :cond_14

    .line 1834
    :cond_1
    const/4 v12, 0x0

    .line 1836
    :goto_4
    const-wide/16 v21, 0x1

    cmp-long v5, v18, v21

    if-eqz v5, :cond_2

    if-nez v20, :cond_2

    .line 1837
    const/4 v10, 0x0

    .line 1839
    :cond_2
    if-eqz v2, :cond_5

    .line 1840
    const/4 v2, 0x1

    move v3, v2

    .line 1845
    :goto_5
    const/16 v2, 0xf

    .line 1846
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 1847
    const/4 v5, 0x2

    if-ne v2, v5, :cond_6

    const/4 v11, 0x1

    .line 1850
    :goto_6
    if-nez v11, :cond_7

    const/4 v2, 0x1

    move v5, v2

    .line 1853
    :goto_7
    move-object/from16 v0, p0

    iget-object v2, v0, Laii;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    iget-object v2, v2, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->g:Lbai;

    check-cast v2, Laip;

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    invoke-virtual {v2, v1}, Laip;->getItemViewType(I)I

    move-result v1

    .line 1854
    const/4 v2, 0x1

    if-eq v1, v2, :cond_3

    const/4 v2, 0x2

    if-ne v1, v2, :cond_13

    .line 1856
    :cond_3
    const/4 v1, 0x1

    move v2, v1

    .line 1861
    :goto_8
    invoke-static/range {v17 .. v17}, Lf;->f(Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v17

    move v6, v7

    :goto_9
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1862
    invoke-static {v1}, Lyt;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 1863
    add-int/lit8 v1, v6, 0x1

    :goto_a
    move v6, v1

    .line 1865
    goto :goto_9

    .line 1821
    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 1842
    :cond_5
    const/4 v2, 0x1

    move v4, v2

    goto :goto_5

    .line 1847
    :cond_6
    const/4 v11, 0x0

    goto :goto_6

    .line 1850
    :cond_7
    const/4 v2, 0x0

    move v5, v2

    goto :goto_7

    :cond_8
    move v7, v6

    move v1, v8

    move v6, v2

    move v8, v12

    move v2, v9

    move v9, v3

    move v3, v10

    move v10, v4

    move v4, v11

    .line 1807
    :goto_b
    add-int/lit8 v11, v13, 0x1

    move v13, v11

    move v11, v8

    move v8, v5

    move v5, v1

    move v1, v2

    move v2, v4

    move v4, v10

    move v10, v3

    move v3, v9

    goto/16 :goto_1

    :cond_9
    move v9, v1

    move v1, v10

    move v10, v6

    move v6, v4

    move v4, v3

    move v3, v8

    move v8, v7

    .line 1871
    :goto_c
    if-eqz v1, :cond_11

    const/4 v7, 0x1

    if-le v9, v7, :cond_11

    if-nez v5, :cond_11

    .line 1872
    const/4 v1, 0x0

    move v7, v1

    .line 1876
    :goto_d
    if-eqz v6, :cond_a

    if-nez v10, :cond_a

    const/4 v1, 0x1

    move v6, v1

    .line 1877
    :goto_e
    if-eqz v4, :cond_b

    if-nez v10, :cond_b

    const/4 v1, 0x1

    move v5, v1

    .line 1878
    :goto_f
    if-eqz v3, :cond_c

    if-nez v10, :cond_c

    const/4 v1, 0x1

    move v4, v1

    .line 1879
    :goto_10
    if-eqz v2, :cond_d

    if-nez v10, :cond_d

    const/4 v1, 0x1

    move v3, v1

    .line 1880
    :goto_11
    if-eqz v11, :cond_e

    if-nez v10, :cond_e

    const/4 v1, 0x1

    move v2, v1

    .line 1881
    :goto_12
    if-eqz v7, :cond_f

    if-nez v10, :cond_f

    const/4 v1, 0x1

    .line 1883
    :goto_13
    move-object/from16 v0, p0

    iget-object v7, v0, Laii;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-virtual {v7}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v10, Lh;->bq:I

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    .line 1884
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    .line 1883
    invoke-virtual {v7, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lbcd;->a(Ljava/lang/CharSequence;)V

    .line 1886
    sget v7, Lg;->fI:I

    move-object/from16 v0, p2

    invoke-interface {v0, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    .line 1887
    invoke-interface {v7, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1888
    sget v4, Lg;->fS:I

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 1889
    invoke-interface {v4, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1891
    sget v3, Lg;->fO:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 1892
    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1893
    sget v2, Lg;->fP:I

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1894
    sget v2, Lg;->fT:I

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1896
    sget v2, Lg;->fK:I

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1897
    sget v1, Lg;->fX:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 1898
    invoke-static {}, Lbtf;->k()Z

    move-result v1

    if-eqz v1, :cond_10

    const/4 v1, 0x1

    if-le v9, v1, :cond_10

    const/4 v1, 0x2

    if-ge v8, v1, :cond_10

    const/4 v1, 0x1

    .line 1897
    :goto_14
    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1902
    sget v1, Lg;->fR:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1903
    invoke-static {}, Lf;->w()Z

    move-result v2

    .line 1902
    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1905
    const/4 v1, 0x0

    return v1

    .line 1876
    :cond_a
    const/4 v1, 0x0

    move v6, v1

    goto/16 :goto_e

    .line 1877
    :cond_b
    const/4 v1, 0x0

    move v5, v1

    goto/16 :goto_f

    .line 1878
    :cond_c
    const/4 v1, 0x0

    move v4, v1

    goto/16 :goto_10

    .line 1879
    :cond_d
    const/4 v1, 0x0

    move v3, v1

    goto/16 :goto_11

    .line 1880
    :cond_e
    const/4 v1, 0x0

    move v2, v1

    goto/16 :goto_12

    .line 1881
    :cond_f
    const/4 v1, 0x0

    goto/16 :goto_13

    .line 1898
    :cond_10
    const/4 v1, 0x0

    goto :goto_14

    :cond_11
    move v7, v1

    goto/16 :goto_d

    :cond_12
    move v1, v6

    goto/16 :goto_a

    :cond_13
    move v2, v6

    goto/16 :goto_8

    :cond_14
    move v12, v11

    goto/16 :goto_4

    :cond_15
    move v8, v5

    goto/16 :goto_3

    :cond_16
    move v1, v5

    move v5, v8

    move v8, v11

    move/from16 v23, v2

    move v2, v9

    move v9, v3

    move v3, v10

    move v10, v4

    move/from16 v4, v23

    goto/16 :goto_b

    :cond_17
    move v9, v3

    move v3, v10

    move v10, v4

    move v4, v2

    move v2, v1

    move v1, v5

    move v5, v8

    move v8, v11

    goto/16 :goto_b

    :cond_18
    move v1, v10

    move v10, v6

    move v6, v9

    move v9, v2

    move v2, v3

    move v3, v8

    move v8, v7

    goto/16 :goto_c
.end method
