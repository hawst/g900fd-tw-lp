.class public final Lacc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# static fields
.field public static final a:Z


# instance fields
.field private b:Z

.field private c:Z

.field private volatile d:Z

.field private e:Lach;

.field private volatile f:Laci;

.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Laci;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lacj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lbys;->d:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lacc;->a:Z

    return-void
.end method

.method public constructor <init>(Lach;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 229
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lacc;->g:Ljava/util/List;

    .line 233
    new-instance v0, Lacd;

    invoke-direct {v0, p0}, Lacd;-><init>(Lacc;)V

    iput-object v0, p0, Lacc;->h:Lacj;

    .line 260
    iput-boolean v1, p0, Lacc;->d:Z

    .line 261
    iput-object p1, p0, Lacc;->e:Lach;

    .line 262
    iput-boolean v1, p0, Lacc;->b:Z

    .line 263
    const/4 v0, 0x1

    iput-boolean v0, p0, Lacc;->c:Z

    .line 264
    iget-object v0, p0, Lacc;->e:Lach;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lacc;->e:Lach;

    invoke-interface {v0, p0}, Lach;->a(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 267
    :cond_0
    return-void
.end method

.method public static a(Laci;Lach;)Lacc;
    .locals 1

    .prologue
    .line 254
    new-instance v0, Lacc;

    invoke-direct {v0, p1}, Lacc;-><init>(Lach;)V

    .line 255
    invoke-virtual {v0, p0}, Lacc;->a(Laci;)V

    .line 256
    return-object v0
.end method

.method private a(Laci;Z)V
    .locals 3

    .prologue
    .line 326
    sget-boolean v0, Lacc;->a:Z

    if-eqz v0, :cond_0

    .line 327
    if-nez p2, :cond_0

    .line 328
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BlockingChainedExecutor: request "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " failed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    :cond_0
    iget-object v0, p0, Lacc;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 332
    if-gez v0, :cond_3

    .line 334
    iget-object v0, p0, Lacc;->e:Lach;

    if-eqz v0, :cond_1

    .line 335
    iget-object v0, p0, Lacc;->e:Lach;

    invoke-interface {v0}, Lach;->a()V

    .line 337
    :cond_1
    const-string v0, "Babel"

    const-string v1, "BlockingChainedExecutor: can\'t find the request!"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    :cond_2
    :goto_0
    return-void

    .line 338
    :cond_3
    iget-object v1, p0, Lacc;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_6

    .line 340
    iget-object v0, p0, Lacc;->e:Lach;

    if-eqz v0, :cond_4

    .line 341
    iget-object v0, p0, Lacc;->e:Lach;

    invoke-interface {v0}, Lach;->a()V

    .line 344
    :cond_4
    if-eqz p2, :cond_5

    .line 345
    invoke-interface {p1}, Laci;->d()V

    goto :goto_0

    .line 347
    :cond_5
    invoke-interface {p1}, Laci;->c()V

    goto :goto_0

    .line 349
    :cond_6
    iget-boolean v1, p0, Lacc;->d:Z

    if-nez v1, :cond_2

    .line 351
    if-eqz p2, :cond_7

    iget-boolean v1, p0, Lacc;->b:Z

    if-nez v1, :cond_8

    :cond_7
    if-nez p2, :cond_2

    iget-boolean v1, p0, Lacc;->c:Z

    if-eqz v1, :cond_2

    .line 352
    :cond_8
    iget-object v1, p0, Lacc;->g:Ljava/util/List;

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laci;

    iput-object v0, p0, Lacc;->f:Laci;

    .line 353
    iget-object v0, p0, Lacc;->f:Laci;

    invoke-interface {v0}, Laci;->g()V

    .line 354
    iget-object v0, p0, Lacc;->e:Lach;

    if-eqz v0, :cond_9

    .line 356
    iget-object v0, p0, Lacc;->e:Lach;

    iget-object v1, p0, Lacc;->f:Laci;

    invoke-interface {v1}, Laci;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lach;->b(Ljava/lang/String;)V

    .line 358
    :cond_9
    sget-boolean v0, Lacc;->a:Z

    if-eqz v0, :cond_2

    .line 359
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BlockingChainedExecutor: handle request "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lacc;->f:Laci;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 303
    iget-object v0, p0, Lacc;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 304
    const-string v0, "Babel"

    const-string v1, "BlockingChainedExecutor: make sure you have requests to run!"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    :cond_0
    :goto_0
    return-void

    .line 308
    :cond_1
    iget-object v0, p0, Lacc;->g:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laci;

    iput-object v0, p0, Lacc;->f:Laci;

    .line 309
    iget-object v0, p0, Lacc;->e:Lach;

    if-eqz v0, :cond_2

    .line 310
    iget-object v0, p0, Lacc;->e:Lach;

    iget-object v1, p0, Lacc;->f:Laci;

    invoke-interface {v1}, Laci;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lach;->a(Ljava/lang/String;)V

    .line 313
    :cond_2
    iget-object v0, p0, Lacc;->f:Laci;

    invoke-interface {v0}, Laci;->g()V

    .line 314
    sget-boolean v0, Lacc;->a:Z

    if-eqz v0, :cond_0

    .line 315
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BlockingChainedExecutor: handle request "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lacc;->f:Laci;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Laci;)V
    .locals 2

    .prologue
    .line 284
    if-nez p1, :cond_0

    .line 285
    const-string v0, "Babel"

    const-string v1, "BlockingChainedExecutor: adding empty request"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :goto_0
    return-void

    .line 288
    :cond_0
    iget-object v0, p0, Lacc;->h:Lacj;

    invoke-interface {p1, v0}, Laci;->a(Lacj;)V

    .line 289
    iget-object v0, p0, Lacc;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected b(Laci;)V
    .locals 1

    .prologue
    .line 367
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lacc;->a(Laci;Z)V

    .line 368
    return-void
.end method

.method protected c(Laci;)V
    .locals 1

    .prologue
    .line 371
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lacc;->a(Laci;Z)V

    .line 372
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 321
    const/4 v0, 0x1

    iput-boolean v0, p0, Lacc;->d:Z

    .line 322
    iget-object v0, p0, Lacc;->f:Laci;

    invoke-interface {v0}, Laci;->h()V

    .line 323
    return-void
.end method
