.class public final Lbrx;
.super Landroid/database/AbstractCursor;
.source "PG"

# interfaces
.implements Ladk;


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field private a:Lcvx;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lbry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 52
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "aggregated_person"

    aput-object v2, v0, v1

    sput-object v0, Lbrx;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcvx;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbrx;->b:Ljava/util/ArrayList;

    .line 66
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    :goto_0
    return-void

    .line 70
    :cond_0
    iput-object p1, p0, Lbrx;->a:Lcvx;

    .line 71
    invoke-direct {p0}, Lbrx;->g()V

    goto :goto_0
.end method

.method private a(I)I
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lbrx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbry;

    iget v0, v0, Lbry;->a:I

    return v0
.end method

.method private g()V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 80
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 81
    iget-object v0, p0, Lbrx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 82
    iget-object v0, p0, Lbrx;->a:Lcvx;

    if-eqz v0, :cond_2

    move v2, v3

    .line 83
    :goto_0
    iget-object v0, p0, Lbrx;->a:Lcvx;

    invoke-virtual {v0}, Lcvx;->a()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 84
    iget-object v0, p0, Lbrx;->a:Lcvx;

    invoke-virtual {v0, v2}, Lcvx;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvw;

    .line 86
    invoke-interface {v0}, Lcvw;->d()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v3

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcwg;

    .line 87
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Lcvw;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v1}, Lcwg;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 88
    invoke-virtual {v5, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 89
    invoke-virtual {v5, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 90
    iget-object v1, p0, Lbrx;->b:Ljava/util/ArrayList;

    new-instance v7, Lbry;

    invoke-direct {v7, v2, v4}, Lbry;-><init>(II)V

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    :cond_0
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    .line 93
    goto :goto_1

    .line 83
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 96
    :cond_2
    return-void
.end method


# virtual methods
.method public a()Lcvw;
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lbrx;->a:Lcvx;

    if-nez v0, :cond_0

    .line 105
    const/4 v0, 0x0

    .line 108
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbrx;->a:Lcvx;

    invoke-virtual {p0}, Lbrx;->getPosition()I

    move-result v1

    invoke-direct {p0, v1}, Lbrx;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcvx;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvw;

    goto :goto_0
.end method

.method public a(Ladb;)V
    .locals 1

    .prologue
    .line 189
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b()Laea;
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    return-object v0
.end method

.method public c()Ladm;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 118
    invoke-virtual {p0}, Lbrx;->a()Lcvw;

    move-result-object v2

    .line 119
    if-eqz v2, :cond_1

    .line 120
    iget-object v0, p0, Lbrx;->b:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lbrx;->getPosition()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbry;

    iget v4, v0, Lbry;->b:I

    .line 127
    invoke-interface {v2}, Lcvw;->d()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v5

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcwg;

    .line 128
    add-int/lit8 v0, v3, 0x1

    if-ne v3, v4, :cond_0

    .line 129
    new-instance v0, Ladm;

    invoke-interface {v2}, Lcwg;->b()Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x0

    invoke-direct/range {v0 .. v5}, Ladm;-><init>(Ljava/lang/String;Ljava/lang/String;JI)V

    .line 134
    :goto_1
    return-object v0

    :cond_0
    move v3, v0

    .line 132
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 134
    goto :goto_1
.end method

.method public d()I
    .locals 1

    .prologue
    .line 194
    const/4 v0, 0x1

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x0

    return v0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lbrx;->b:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lbrx;->getPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbry;

    iget v0, v0, Lbry;->b:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    sget-object v0, Lbrx;->c:[Ljava/lang/String;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lbrx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getDouble(I)D
    .locals 1

    .prologue
    .line 179
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getFloat(I)F
    .locals 1

    .prologue
    .line 174
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getInt(I)I
    .locals 1

    .prologue
    .line 159
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getLong(I)J
    .locals 2

    .prologue
    .line 166
    if-nez p1, :cond_0

    .line 167
    invoke-virtual {p0}, Lbrx;->getPosition()I

    move-result v0

    invoke-direct {p0, v0}, Lbrx;->a(I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0

    .line 169
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getShort(I)S
    .locals 1

    .prologue
    .line 154
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public isNull(I)Z
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x0

    return v0
.end method
