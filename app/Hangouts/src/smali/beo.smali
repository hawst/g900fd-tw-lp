.class public Lbeo;
.super Lbep;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final h:[B

.field public final i:[B

.field public final j:[B


# direct methods
.method public constructor <init>(III[B[B[B)V
    .locals 0

    .prologue
    .line 2832
    invoke-direct {p0}, Lbep;-><init>()V

    .line 2833
    iput p1, p0, Lbeo;->a:I

    .line 2834
    iput p2, p0, Lbeo;->b:I

    .line 2835
    iput p3, p0, Lbeo;->c:I

    .line 2836
    iput-object p4, p0, Lbeo;->h:[B

    .line 2837
    iput-object p5, p0, Lbeo;->i:[B

    .line 2838
    iput-object p6, p0, Lbeo;->j:[B

    .line 2839
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 4

    .prologue
    .line 2843
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 2844
    const-string v0, "Babel_RequestWriter"

    const-string v1, "getSuggestedRequest()"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2847
    :cond_0
    new-instance v0, Ldsr;

    invoke-direct {v0}, Ldsr;-><init>()V

    .line 2850
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Ldsr;->e:Ljava/lang/Boolean;

    .line 2851
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2852
    invoke-virtual {p0, v1, v2, p1, p2}, Lbeo;->a(Ldpt;ZLjava/lang/String;I)Ldvm;

    move-result-object v1

    iput-object v1, v0, Ldsr;->b:Ldvm;

    .line 2854
    new-instance v1, Ldss;

    invoke-direct {v1}, Ldss;-><init>()V

    .line 2856
    iget v2, p0, Lbeo;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Ldss;->b:Ljava/lang/Integer;

    .line 2857
    iget-object v2, p0, Lbeo;->h:[B

    if-eqz v2, :cond_1

    .line 2858
    iget-object v2, p0, Lbeo;->h:[B

    iget-object v3, p0, Lbeo;->h:[B

    array-length v3, v3

    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v2

    iput-object v2, v1, Ldss;->c:[B

    .line 2860
    :cond_1
    iput-object v1, v0, Ldsr;->i:Ldss;

    .line 2862
    new-instance v1, Ldss;

    invoke-direct {v1}, Ldss;-><init>()V

    .line 2864
    iget v2, p0, Lbeo;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Ldss;->b:Ljava/lang/Integer;

    .line 2865
    iget-object v2, p0, Lbeo;->i:[B

    if-eqz v2, :cond_2

    .line 2866
    iget-object v2, p0, Lbeo;->i:[B

    iget-object v3, p0, Lbeo;->i:[B

    array-length v3, v3

    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v2

    iput-object v2, v1, Ldss;->c:[B

    .line 2869
    :cond_2
    iput-object v1, v0, Ldsr;->j:Ldss;

    .line 2871
    new-instance v1, Ldss;

    invoke-direct {v1}, Ldss;-><init>()V

    .line 2873
    iget v2, p0, Lbeo;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Ldss;->b:Ljava/lang/Integer;

    .line 2874
    iget-object v2, p0, Lbeo;->j:[B

    if-eqz v2, :cond_3

    .line 2875
    iget-object v2, p0, Lbeo;->j:[B

    iget-object v3, p0, Lbeo;->j:[B

    array-length v3, v3

    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v2

    iput-object v2, v1, Ldss;->c:[B

    .line 2878
    :cond_3
    iput-object v1, v0, Ldsr;->m:Ldss;

    .line 2880
    return-object v0
.end method

.method public a(Lyj;Lbph;)V
    .locals 4

    .prologue
    .line 2891
    invoke-static {p1}, Lbqh;->a(Lyj;)Lbqh;

    move-result-object v0

    .line 2892
    const-string v1, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SyncBaselineSuggestedContactsOperation failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2893
    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2892
    invoke-static {v1, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 2894
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbqh;->a(I)V

    .line 2895
    return-void
.end method

.method public a(Lbea;)Z
    .locals 2

    .prologue
    .line 2904
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2905
    const/4 v0, 0x1

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2885
    const-string v0, "contacts/getsuggestedentities"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2899
    const-string v0, "ui_queue"

    return-object v0
.end method
