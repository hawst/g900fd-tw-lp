.class public final Leza;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leza;


# instance fields
.field public b:Leys;

.field public c:Lepb;

.field public d:Lekc;

.field public e:Leiv;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Integer;

.field public h:Lfaa;

.field public i:Ljava/lang/Float;

.field public j:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Leza;

    sput-object v0, Leza;->a:[Leza;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 13
    iput-object v0, p0, Leza;->b:Leys;

    .line 16
    iput-object v0, p0, Leza;->c:Lepb;

    .line 19
    iput-object v0, p0, Leza;->d:Lekc;

    .line 22
    iput-object v0, p0, Leza;->e:Leiv;

    .line 27
    iput-object v0, p0, Leza;->g:Ljava/lang/Integer;

    .line 30
    iput-object v0, p0, Leza;->h:Lfaa;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 72
    const/4 v0, 0x0

    .line 73
    iget-object v1, p0, Leza;->b:Leys;

    if-eqz v1, :cond_0

    .line 74
    const/4 v0, 0x1

    iget-object v1, p0, Leza;->b:Leys;

    .line 75
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 77
    :cond_0
    iget-object v1, p0, Leza;->c:Lepb;

    if-eqz v1, :cond_1

    .line 78
    const/4 v1, 0x2

    iget-object v2, p0, Leza;->c:Lepb;

    .line 79
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    :cond_1
    iget-object v1, p0, Leza;->d:Lekc;

    if-eqz v1, :cond_2

    .line 82
    const/4 v1, 0x3

    iget-object v2, p0, Leza;->d:Lekc;

    .line 83
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 85
    :cond_2
    iget-object v1, p0, Leza;->e:Leiv;

    if-eqz v1, :cond_3

    .line 86
    const/4 v1, 0x4

    iget-object v2, p0, Leza;->e:Leiv;

    .line 87
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_3
    iget-object v1, p0, Leza;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 90
    const/4 v1, 0x5

    iget-object v2, p0, Leza;->f:Ljava/lang/Integer;

    .line 91
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    :cond_4
    iget-object v1, p0, Leza;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 94
    const/4 v1, 0x6

    iget-object v2, p0, Leza;->g:Ljava/lang/Integer;

    .line 95
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    :cond_5
    iget-object v1, p0, Leza;->h:Lfaa;

    if-eqz v1, :cond_6

    .line 98
    const/4 v1, 0x7

    iget-object v2, p0, Leza;->h:Lfaa;

    .line 99
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    :cond_6
    iget-object v1, p0, Leza;->i:Ljava/lang/Float;

    if-eqz v1, :cond_7

    .line 102
    const/16 v1, 0xa

    iget-object v2, p0, Leza;->i:Ljava/lang/Float;

    .line 103
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 105
    :cond_7
    iget-object v1, p0, Leza;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 106
    const/16 v1, 0xb

    iget-object v2, p0, Leza;->j:Ljava/lang/Boolean;

    .line 107
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 109
    :cond_8
    iget-object v1, p0, Leza;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 110
    iput v0, p0, Leza;->cachedSize:I

    .line 111
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leza;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leza;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leza;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Leza;->b:Leys;

    if-nez v0, :cond_2

    new-instance v0, Leys;

    invoke-direct {v0}, Leys;-><init>()V

    iput-object v0, p0, Leza;->b:Leys;

    :cond_2
    iget-object v0, p0, Leza;->b:Leys;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Leza;->c:Lepb;

    if-nez v0, :cond_3

    new-instance v0, Lepb;

    invoke-direct {v0}, Lepb;-><init>()V

    iput-object v0, p0, Leza;->c:Lepb;

    :cond_3
    iget-object v0, p0, Leza;->c:Lepb;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Leza;->d:Lekc;

    if-nez v0, :cond_4

    new-instance v0, Lekc;

    invoke-direct {v0}, Lekc;-><init>()V

    iput-object v0, p0, Leza;->d:Lekc;

    :cond_4
    iget-object v0, p0, Leza;->d:Lekc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Leza;->e:Leiv;

    if-nez v0, :cond_5

    new-instance v0, Leiv;

    invoke-direct {v0}, Leiv;-><init>()V

    iput-object v0, p0, Leza;->e:Leiv;

    :cond_5
    iget-object v0, p0, Leza;->e:Leiv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leza;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_6

    const/4 v1, 0x1

    if-eq v0, v1, :cond_6

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    :cond_6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leza;->g:Ljava/lang/Integer;

    goto :goto_0

    :cond_7
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leza;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Leza;->h:Lfaa;

    if-nez v0, :cond_8

    new-instance v0, Lfaa;

    invoke-direct {v0}, Lfaa;-><init>()V

    iput-object v0, p0, Leza;->h:Lfaa;

    :cond_8
    iget-object v0, p0, Leza;->h:Lfaa;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Leza;->i:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Leza;->j:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x55 -> :sswitch_8
        0x58 -> :sswitch_9
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Leza;->b:Leys;

    if-eqz v0, :cond_0

    .line 40
    const/4 v0, 0x1

    iget-object v1, p0, Leza;->b:Leys;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 42
    :cond_0
    iget-object v0, p0, Leza;->c:Lepb;

    if-eqz v0, :cond_1

    .line 43
    const/4 v0, 0x2

    iget-object v1, p0, Leza;->c:Lepb;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 45
    :cond_1
    iget-object v0, p0, Leza;->d:Lekc;

    if-eqz v0, :cond_2

    .line 46
    const/4 v0, 0x3

    iget-object v1, p0, Leza;->d:Lekc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 48
    :cond_2
    iget-object v0, p0, Leza;->e:Leiv;

    if-eqz v0, :cond_3

    .line 49
    const/4 v0, 0x4

    iget-object v1, p0, Leza;->e:Leiv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 51
    :cond_3
    iget-object v0, p0, Leza;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 52
    const/4 v0, 0x5

    iget-object v1, p0, Leza;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 54
    :cond_4
    iget-object v0, p0, Leza;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 55
    const/4 v0, 0x6

    iget-object v1, p0, Leza;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 57
    :cond_5
    iget-object v0, p0, Leza;->h:Lfaa;

    if-eqz v0, :cond_6

    .line 58
    const/4 v0, 0x7

    iget-object v1, p0, Leza;->h:Lfaa;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 60
    :cond_6
    iget-object v0, p0, Leza;->i:Ljava/lang/Float;

    if-eqz v0, :cond_7

    .line 61
    const/16 v0, 0xa

    iget-object v1, p0, Leza;->i:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 63
    :cond_7
    iget-object v0, p0, Leza;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 64
    const/16 v0, 0xb

    iget-object v1, p0, Leza;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 66
    :cond_8
    iget-object v0, p0, Leza;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 68
    return-void
.end method
