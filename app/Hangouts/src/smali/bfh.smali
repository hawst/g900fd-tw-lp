.class public Lbfh;
.super Lbep;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final a:I

.field private final b:Z


# direct methods
.method public constructor <init>(IZ)V
    .locals 0

    .prologue
    .line 2718
    invoke-direct {p0}, Lbep;-><init>()V

    .line 2719
    iput p1, p0, Lbfh;->a:I

    .line 2720
    iput-boolean p2, p0, Lbfh;->b:Z

    .line 2721
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2725
    new-instance v0, Ldwa;

    invoke-direct {v0}, Ldwa;-><init>()V

    .line 2726
    const/4 v1, 0x0

    .line 2727
    invoke-virtual {p0, v1, v3, p1, p2}, Lbfh;->a(Ldpt;ZLjava/lang/String;I)Ldvm;

    move-result-object v1

    iput-object v1, v0, Ldwa;->b:Ldvm;

    .line 2728
    new-instance v1, Ldpw;

    invoke-direct {v1}, Ldpw;-><init>()V

    .line 2729
    iget v2, p0, Lbfh;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Ldpw;->b:Ljava/lang/Integer;

    .line 2730
    iget-boolean v2, p0, Lbfh;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Ldpw;->c:Ljava/lang/Boolean;

    .line 2732
    new-array v2, v3, [Ldpw;

    iput-object v2, v0, Ldwa;->c:[Ldpw;

    .line 2733
    iget-object v2, v0, Ldwa;->c:[Ldpw;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    .line 2734
    return-object v0
.end method

.method public a(Lyj;Lbph;)V
    .locals 0

    .prologue
    .line 2744
    invoke-static {p2}, Lbkb;->a(Ljava/lang/Exception;)V

    .line 2745
    return-void
.end method

.method public a(Lbea;)Z
    .locals 2

    .prologue
    .line 2755
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2756
    invoke-virtual {p0, p1}, Lbfh;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2739
    const-string v0, "contacts/setconfigurationbit"

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2749
    check-cast p1, Lbfh;

    .line 2750
    iget v0, p0, Lbfh;->a:I

    iget v1, p1, Lbfh;->a:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lbfh;->b:Z

    iget-boolean v1, p1, Lbfh;->b:Z

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
