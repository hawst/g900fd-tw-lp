.class public final Lbdn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x3L


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:Z

.field public final e:Z

.field public final f:I

.field public final g:I

.field public final h:Ljava/lang/String;

.field public final i:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-boolean v0, p0, Lbdn;->a:Z

    .line 46
    iput-boolean v0, p0, Lbdn;->b:Z

    .line 47
    iput-boolean v0, p0, Lbdn;->c:Z

    .line 48
    iput-boolean v0, p0, Lbdn;->d:Z

    .line 49
    iput-boolean v0, p0, Lbdn;->e:Z

    .line 50
    iput v0, p0, Lbdn;->f:I

    .line 51
    iput v0, p0, Lbdn;->g:I

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lbdn;->h:Ljava/lang/String;

    .line 53
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbdn;->i:J

    .line 54
    return-void
.end method

.method public constructor <init>(Ldut;)V
    .locals 7

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iget-object v0, p1, Ldut;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 60
    iput-boolean v1, p0, Lbdn;->b:Z

    .line 61
    iget-object v0, p1, Ldut;->b:Ljava/lang/Boolean;

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbdn;->a:Z

    .line 64
    :cond_0
    iget-object v0, p1, Ldut;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 65
    iput-boolean v1, p0, Lbdn;->d:Z

    .line 66
    iget-object v0, p1, Ldut;->c:Ljava/lang/Boolean;

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbdn;->c:Z

    .line 70
    :cond_1
    iget-object v0, p1, Ldut;->f:Ldsz;

    if-eqz v0, :cond_a

    .line 72
    iget-object v0, p1, Ldut;->f:Ldsz;

    .line 73
    iget-object v4, v0, Ldsz;->b:Ljava/lang/Integer;

    if-eqz v4, :cond_9

    .line 74
    iget-object v4, v0, Ldsz;->b:Ljava/lang/Integer;

    invoke-static {v4, v3}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v4

    const/16 v5, 0x64

    if-ne v4, v5, :cond_2

    move v0, v1

    move v4, v1

    .line 83
    :goto_0
    iput v0, p0, Lbdn;->f:I

    .line 85
    iget-object v0, p1, Ldut;->e:Lduc;

    if-eqz v0, :cond_3

    .line 87
    iget-object v0, p1, Ldut;->e:Lduc;

    .line 88
    iget-object v0, v0, Lduc;->b:Ljava/lang/String;

    iput-object v0, p0, Lbdn;->h:Ljava/lang/String;

    move v4, v1

    .line 93
    :goto_1
    iget-object v0, p1, Ldut;->k:Ldto;

    if-eqz v0, :cond_4

    .line 94
    iget-object v0, p1, Ldut;->k:Ldto;

    .line 95
    iget-object v0, v0, Ldto;->b:Ljava/lang/Long;

    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v5

    iput-wide v5, p0, Lbdn;->i:J

    .line 100
    :goto_2
    iget-object v0, p1, Ldut;->g:Ldrc;

    if-eqz v0, :cond_8

    .line 102
    iget-object v0, p1, Ldut;->g:Ldrc;

    .line 103
    iget-object v4, v0, Ldrc;->b:Ljava/lang/Boolean;

    invoke-static {v4, v3}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 104
    iput v1, p0, Lbdn;->g:I

    .line 116
    :goto_3
    iput-boolean v1, p0, Lbdn;->e:Z

    .line 117
    return-void

    .line 77
    :cond_2
    iget-object v0, v0, Ldsz;->b:Ljava/lang/Integer;

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    const/16 v4, 0xc8

    if-ne v0, v4, :cond_9

    move v0, v2

    move v4, v1

    .line 79
    goto :goto_0

    .line 90
    :cond_3
    const-string v0, ""

    iput-object v0, p0, Lbdn;->h:Ljava/lang/String;

    goto :goto_1

    .line 97
    :cond_4
    const-wide/16 v5, 0x0

    iput-wide v5, p0, Lbdn;->i:J

    goto :goto_2

    .line 105
    :cond_5
    iget-object v4, v0, Ldrc;->d:Ljava/lang/Boolean;

    invoke-static {v4, v3}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 106
    iput v2, p0, Lbdn;->g:I

    goto :goto_3

    .line 107
    :cond_6
    iget-object v0, v0, Ldrc;->c:Ljava/lang/Boolean;

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 108
    const/4 v0, 0x3

    iput v0, p0, Lbdn;->g:I

    goto :goto_3

    .line 110
    :cond_7
    iput v3, p0, Lbdn;->g:I

    goto :goto_3

    .line 113
    :cond_8
    iput v3, p0, Lbdn;->g:I

    move v1, v4

    goto :goto_3

    :cond_9
    move v0, v3

    move v4, v1

    goto :goto_0

    :cond_a
    move v0, v3

    move v4, v3

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 142
    iget v0, p0, Lbdn;->g:I

    packed-switch v0, :pswitch_data_0

    .line 154
    const/4 v0, 0x0

    .line 156
    :goto_0
    return-object v0

    .line 144
    :pswitch_0
    sget v0, Lh;->d:I

    .line 156
    :goto_1
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 147
    :pswitch_1
    sget v0, Lh;->e:I

    goto :goto_1

    .line 150
    :pswitch_2
    sget v0, Lh;->c:I

    goto :goto_1

    .line 142
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 161
    iget v0, p0, Lbdn;->f:I

    packed-switch v0, :pswitch_data_0

    .line 170
    const/4 v0, 0x0

    .line 172
    :goto_0
    return-object v0

    .line 163
    :pswitch_0
    sget v0, Lh;->a:I

    .line 172
    :goto_1
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 166
    :pswitch_1
    sget v0, Lh;->b:I

    goto :goto_1

    .line 161
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lbdn;->h:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 177
    const/4 v0, 0x0

    .line 179
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbdn;->h:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 134
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "[Reachable:%b,%b], [Available:%b,%b], [HasRichStatus:%b], [InCall:%s], [DeviceStatus:%d], [Mood:%s], [LastSeen:%d]"

    const/16 v0, 0x9

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-boolean v4, p0, Lbdn;->b:Z

    .line 136
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x1

    iget-boolean v4, p0, Lbdn;->a:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x2

    iget-boolean v4, p0, Lbdn;->d:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x3

    iget-boolean v4, p0, Lbdn;->c:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x4

    iget-boolean v4, p0, Lbdn;->e:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x5

    iget v0, p0, Lbdn;->f:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "UNKNOWN"

    :goto_0
    aput-object v0, v3, v4

    const/4 v0, 0x6

    iget v4, p0, Lbdn;->g:I

    .line 137
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x7

    iget-object v4, p0, Lbdn;->h:Ljava/lang/String;

    aput-object v4, v3, v0

    const/16 v0, 0x8

    iget-wide v4, p0, Lbdn;->i:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v0

    .line 134
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 136
    :pswitch_0
    const-string v0, "NONE"

    goto :goto_0

    :pswitch_1
    const-string v0, "PSTN"

    goto :goto_0

    :pswitch_2
    const-string v0, "HANGOUT"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
