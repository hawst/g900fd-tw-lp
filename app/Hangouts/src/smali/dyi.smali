.class public final Ldyi;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldyi;


# instance fields
.field public b:Ldvn;

.field public c:Ldxv;

.field public d:Ldzg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1678
    const/4 v0, 0x0

    new-array v0, v0, [Ldyi;

    sput-object v0, Ldyi;->a:[Ldyi;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1679
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1682
    iput-object v0, p0, Ldyi;->b:Ldvn;

    .line 1685
    iput-object v0, p0, Ldyi;->c:Ldxv;

    .line 1688
    iput-object v0, p0, Ldyi;->d:Ldzg;

    .line 1679
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1708
    const/4 v0, 0x0

    .line 1709
    iget-object v1, p0, Ldyi;->b:Ldvn;

    if-eqz v1, :cond_0

    .line 1710
    const/4 v0, 0x1

    iget-object v1, p0, Ldyi;->b:Ldvn;

    .line 1711
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1713
    :cond_0
    iget-object v1, p0, Ldyi;->c:Ldxv;

    if-eqz v1, :cond_1

    .line 1714
    const/4 v1, 0x2

    iget-object v2, p0, Ldyi;->c:Ldxv;

    .line 1715
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1717
    :cond_1
    iget-object v1, p0, Ldyi;->d:Ldzg;

    if-eqz v1, :cond_2

    .line 1718
    const/4 v1, 0x3

    iget-object v2, p0, Ldyi;->d:Ldzg;

    .line 1719
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1721
    :cond_2
    iget-object v1, p0, Ldyi;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1722
    iput v0, p0, Ldyi;->cachedSize:I

    .line 1723
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1675
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldyi;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldyi;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldyi;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldyi;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldyi;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldyi;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldyi;->c:Ldxv;

    if-nez v0, :cond_3

    new-instance v0, Ldxv;

    invoke-direct {v0}, Ldxv;-><init>()V

    iput-object v0, p0, Ldyi;->c:Ldxv;

    :cond_3
    iget-object v0, p0, Ldyi;->c:Ldxv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldyi;->d:Ldzg;

    if-nez v0, :cond_4

    new-instance v0, Ldzg;

    invoke-direct {v0}, Ldzg;-><init>()V

    iput-object v0, p0, Ldyi;->d:Ldzg;

    :cond_4
    iget-object v0, p0, Ldyi;->d:Ldzg;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1693
    iget-object v0, p0, Ldyi;->b:Ldvn;

    if-eqz v0, :cond_0

    .line 1694
    const/4 v0, 0x1

    iget-object v1, p0, Ldyi;->b:Ldvn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1696
    :cond_0
    iget-object v0, p0, Ldyi;->c:Ldxv;

    if-eqz v0, :cond_1

    .line 1697
    const/4 v0, 0x2

    iget-object v1, p0, Ldyi;->c:Ldxv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1699
    :cond_1
    iget-object v0, p0, Ldyi;->d:Ldzg;

    if-eqz v0, :cond_2

    .line 1700
    const/4 v0, 0x3

    iget-object v1, p0, Ldyi;->d:Ldzg;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1702
    :cond_2
    iget-object v0, p0, Ldyi;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1704
    return-void
.end method
