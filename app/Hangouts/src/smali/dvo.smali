.class public final Ldvo;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldvo;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14894
    const/4 v0, 0x0

    new-array v0, v0, [Ldvo;

    sput-object v0, Ldvo;->a:[Ldvo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14895
    invoke-direct {p0}, Lepn;-><init>()V

    .line 14898
    const/4 v0, 0x0

    iput-object v0, p0, Ldvo;->b:Ljava/lang/Integer;

    .line 14895
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 14917
    const/4 v0, 0x0

    .line 14918
    iget-object v1, p0, Ldvo;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 14919
    const/4 v0, 0x1

    iget-object v1, p0, Ldvo;->b:Ljava/lang/Integer;

    .line 14920
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 14922
    :cond_0
    iget-object v1, p0, Ldvo;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 14923
    const/4 v1, 0x2

    iget-object v2, p0, Ldvo;->c:Ljava/lang/Boolean;

    .line 14924
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 14926
    :cond_1
    iget-object v1, p0, Ldvo;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14927
    iput v0, p0, Ldvo;->cachedSize:I

    .line 14928
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 14891
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldvo;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldvo;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldvo;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldvo;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldvo;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldvo;->c:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 14905
    iget-object v0, p0, Ldvo;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 14906
    const/4 v0, 0x1

    iget-object v1, p0, Ldvo;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 14908
    :cond_0
    iget-object v0, p0, Ldvo;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 14909
    const/4 v0, 0x2

    iget-object v1, p0, Ldvo;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 14911
    :cond_1
    iget-object v0, p0, Ldvo;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 14913
    return-void
.end method
