.class public abstract Lepr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public cachedSize:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lepr;->cachedSize:I

    return-void
.end method

.method public static final mergeFrom(Lepr;[B)Lepr;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lepr;",
            ">(TT;[B)TT;"
        }
    .end annotation

    .prologue
    .line 118
    const/4 v0, 0x0

    array-length v1, p1

    invoke-static {p0, p1, v0, v1}, Lepr;->mergeFrom(Lepr;[BII)Lepr;

    move-result-object v0

    return-object v0
.end method

.method public static final mergeFrom(Lepr;[BII)Lepr;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lepr;",
            ">(TT;[BII)TT;"
        }
    .end annotation

    .prologue
    .line 129
    :try_start_0
    invoke-static {p1, p2, p3}, Lepk;->a([BII)Lepk;

    move-result-object v0

    .line 130
    invoke-virtual {p0, v0}, Lepr;->mergeFrom(Lepk;)Lepr;

    .line 131
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lepk;->a(I)V
    :try_end_0
    .catch Lepq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 132
    return-object p0

    .line 133
    :catch_0
    move-exception v0

    throw v0

    .line 136
    :catch_1
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Reading from a byte array threw an IOException (should never happen)."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static final messageNanoEquals(Lepr;Lepr;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 146
    if-ne p0, p1, :cond_1

    .line 147
    const/4 v0, 0x1

    .line 163
    :cond_0
    :goto_0
    return v0

    .line 149
    :cond_1
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 152
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 155
    invoke-virtual {p0}, Lepr;->getSerializedSize()I

    move-result v1

    .line 156
    invoke-virtual {p1}, Lepr;->getSerializedSize()I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 159
    new-array v2, v1, [B

    .line 160
    new-array v3, v1, [B

    .line 161
    invoke-static {p0, v2, v0, v1}, Lepr;->toByteArray(Lepr;[BII)V

    .line 162
    invoke-static {p1, v3, v0, v1}, Lepr;->toByteArray(Lepr;[BII)V

    .line 163
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public static final toByteArray(Lepr;[BII)V
    .locals 3

    .prologue
    .line 103
    :try_start_0
    invoke-static {p1, p2, p3}, Lepl;->a([BII)Lepl;

    move-result-object v0

    .line 104
    invoke-virtual {p0, v0}, Lepr;->writeTo(Lepl;)V

    .line 105
    invoke-virtual {v0}, Lepl;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    return-void

    .line 106
    :catch_0
    move-exception v0

    .line 107
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Serializing to a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static final toByteArray(Lepr;)[B
    .locals 3

    .prologue
    .line 86
    invoke-virtual {p0}, Lepr;->getSerializedSize()I

    move-result v0

    new-array v0, v0, [B

    .line 87
    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {p0, v0, v1, v2}, Lepr;->toByteArray(Lepr;[BII)V

    .line 88
    return-object v0
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lepr;->cachedSize:I

    if-gez v0, :cond_0

    .line 54
    invoke-virtual {p0}, Lepr;->getSerializedSize()I

    .line 56
    :cond_0
    iget v0, p0, Lepr;->cachedSize:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 65
    iput v0, p0, Lepr;->cachedSize:I

    .line 66
    return v0
.end method

.method public abstract mergeFrom(Lepk;)Lepr;
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    invoke-static {p0}, Lf;->a(Lepr;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract writeTo(Lepl;)V
.end method
