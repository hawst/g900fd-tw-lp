.class public final Ldsm;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldsm;


# instance fields
.field public b:Ldvm;

.field public c:[Ldrp;

.field public d:[I

.field public e:Ldrp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20829
    const/4 v0, 0x0

    new-array v0, v0, [Ldsm;

    sput-object v0, Ldsm;->a:[Ldsm;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 20830
    invoke-direct {p0}, Lepn;-><init>()V

    .line 20833
    iput-object v1, p0, Ldsm;->b:Ldvm;

    .line 20836
    sget-object v0, Ldrp;->a:[Ldrp;

    iput-object v0, p0, Ldsm;->c:[Ldrp;

    .line 20839
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Ldsm;->d:[I

    .line 20842
    iput-object v1, p0, Ldsm;->e:Ldrp;

    .line 20830
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 20872
    iget-object v0, p0, Ldsm;->b:Ldvm;

    if-eqz v0, :cond_5

    .line 20873
    const/4 v0, 0x1

    iget-object v2, p0, Ldsm;->b:Ldvm;

    .line 20874
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 20876
    :goto_0
    iget-object v2, p0, Ldsm;->e:Ldrp;

    if-eqz v2, :cond_0

    .line 20877
    const/4 v2, 0x2

    iget-object v3, p0, Ldsm;->e:Ldrp;

    .line 20878
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 20880
    :cond_0
    iget-object v2, p0, Ldsm;->c:[Ldrp;

    if-eqz v2, :cond_2

    .line 20881
    iget-object v3, p0, Ldsm;->c:[Ldrp;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 20882
    if-eqz v5, :cond_1

    .line 20883
    const/4 v6, 0x3

    .line 20884
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 20881
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 20888
    :cond_2
    iget-object v2, p0, Ldsm;->d:[I

    if-eqz v2, :cond_4

    iget-object v2, p0, Ldsm;->d:[I

    array-length v2, v2

    if-lez v2, :cond_4

    .line 20890
    iget-object v3, p0, Ldsm;->d:[I

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_3

    aget v5, v3, v1

    .line 20892
    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 20890
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 20894
    :cond_3
    add-int/2addr v0, v2

    .line 20895
    iget-object v1, p0, Ldsm;->d:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 20897
    :cond_4
    iget-object v1, p0, Ldsm;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20898
    iput v0, p0, Ldsm;->cachedSize:I

    .line 20899
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 20826
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldsm;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldsm;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldsm;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldsm;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldsm;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldsm;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldsm;->e:Ldrp;

    if-nez v0, :cond_3

    new-instance v0, Ldrp;

    invoke-direct {v0}, Ldrp;-><init>()V

    iput-object v0, p0, Ldsm;->e:Ldrp;

    :cond_3
    iget-object v0, p0, Ldsm;->e:Ldrp;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldsm;->c:[Ldrp;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldrp;

    iget-object v3, p0, Ldsm;->c:[Ldrp;

    if-eqz v3, :cond_4

    iget-object v3, p0, Ldsm;->c:[Ldrp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Ldsm;->c:[Ldrp;

    :goto_2
    iget-object v2, p0, Ldsm;->c:[Ldrp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ldsm;->c:[Ldrp;

    new-instance v3, Ldrp;

    invoke-direct {v3}, Ldrp;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldsm;->c:[Ldrp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Ldsm;->c:[Ldrp;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Ldsm;->c:[Ldrp;

    new-instance v3, Ldrp;

    invoke-direct {v3}, Ldrp;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldsm;->c:[Ldrp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x20

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldsm;->d:[I

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [I

    iget-object v3, p0, Ldsm;->d:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldsm;->d:[I

    :goto_3
    iget-object v2, p0, Ldsm;->d:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Ldsm;->d:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_7
    iget-object v2, p0, Ldsm;->d:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 20847
    iget-object v1, p0, Ldsm;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 20848
    const/4 v1, 0x1

    iget-object v2, p0, Ldsm;->b:Ldvm;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 20850
    :cond_0
    iget-object v1, p0, Ldsm;->e:Ldrp;

    if-eqz v1, :cond_1

    .line 20851
    const/4 v1, 0x2

    iget-object v2, p0, Ldsm;->e:Ldrp;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 20853
    :cond_1
    iget-object v1, p0, Ldsm;->c:[Ldrp;

    if-eqz v1, :cond_3

    .line 20854
    iget-object v2, p0, Ldsm;->c:[Ldrp;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 20855
    if-eqz v4, :cond_2

    .line 20856
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 20854
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 20860
    :cond_3
    iget-object v1, p0, Ldsm;->d:[I

    if-eqz v1, :cond_4

    iget-object v1, p0, Ldsm;->d:[I

    array-length v1, v1

    if-lez v1, :cond_4

    .line 20861
    iget-object v1, p0, Ldsm;->d:[I

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget v3, v1, v0

    .line 20862
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 20861
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 20865
    :cond_4
    iget-object v0, p0, Ldsm;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 20867
    return-void
.end method
