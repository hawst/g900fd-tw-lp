.class public final Ldrp;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldrp;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20704
    const/4 v0, 0x0

    new-array v0, v0, [Ldrp;

    sput-object v0, Ldrp;->a:[Ldrp;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20705
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 20746
    const/4 v0, 0x0

    .line 20747
    iget-object v1, p0, Ldrp;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 20748
    const/4 v0, 0x1

    iget-object v1, p0, Ldrp;->b:Ljava/lang/String;

    .line 20749
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 20751
    :cond_0
    iget-object v1, p0, Ldrp;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 20752
    const/4 v1, 0x2

    iget-object v2, p0, Ldrp;->c:Ljava/lang/String;

    .line 20753
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20755
    :cond_1
    iget-object v1, p0, Ldrp;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 20756
    const/4 v1, 0x3

    iget-object v2, p0, Ldrp;->d:Ljava/lang/String;

    .line 20757
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20759
    :cond_2
    iget-object v1, p0, Ldrp;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 20760
    const/4 v1, 0x4

    iget-object v2, p0, Ldrp;->e:Ljava/lang/String;

    .line 20761
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20763
    :cond_3
    iget-object v1, p0, Ldrp;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 20764
    const/4 v1, 0x5

    iget-object v2, p0, Ldrp;->f:Ljava/lang/String;

    .line 20765
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20767
    :cond_4
    iget-object v1, p0, Ldrp;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 20768
    const/4 v1, 0x6

    iget-object v2, p0, Ldrp;->g:Ljava/lang/Boolean;

    .line 20769
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 20771
    :cond_5
    iget-object v1, p0, Ldrp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20772
    iput v0, p0, Ldrp;->cachedSize:I

    .line 20773
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 20701
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldrp;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldrp;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldrp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldrp;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldrp;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldrp;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldrp;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldrp;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldrp;->g:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 20722
    iget-object v0, p0, Ldrp;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 20723
    const/4 v0, 0x1

    iget-object v1, p0, Ldrp;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 20725
    :cond_0
    iget-object v0, p0, Ldrp;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 20726
    const/4 v0, 0x2

    iget-object v1, p0, Ldrp;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 20728
    :cond_1
    iget-object v0, p0, Ldrp;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 20729
    const/4 v0, 0x3

    iget-object v1, p0, Ldrp;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 20731
    :cond_2
    iget-object v0, p0, Ldrp;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 20732
    const/4 v0, 0x4

    iget-object v1, p0, Ldrp;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 20734
    :cond_3
    iget-object v0, p0, Ldrp;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 20735
    const/4 v0, 0x5

    iget-object v1, p0, Ldrp;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 20737
    :cond_4
    iget-object v0, p0, Ldrp;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 20738
    const/4 v0, 0x6

    iget-object v1, p0, Ldrp;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 20740
    :cond_5
    iget-object v0, p0, Ldrp;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 20742
    return-void
.end method
