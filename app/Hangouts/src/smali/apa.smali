.class public final Lapa;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static a:[I

.field private static b:I

.field private static c:[Lcom/google/android/libraries/hangouts/video/Size;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    sget-object v0, Lapa;->a:[I

    if-nez v0, :cond_0

    .line 30
    invoke-static {}, Lapa;->a()V

    .line 32
    :cond_0
    return-void
.end method

.method public static a(I)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, -0x1

    move v0, v1

    .line 36
    :goto_0
    sget v3, Lapa;->b:I

    if-ge v0, v3, :cond_3

    .line 37
    sget-object v3, Lapa;->a:[I

    aget v3, v3, v0

    if-ne v3, p0, :cond_0

    .line 42
    :goto_1
    if-ne v0, v2, :cond_1

    move v0, v2

    .line 50
    :goto_2
    return v0

    .line 36
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 46
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 47
    sget v2, Lapa;->b:I

    if-ne v0, v2, :cond_2

    .line 50
    :goto_3
    sget-object v0, Lapa;->a:[I

    aget v0, v0, v1

    goto :goto_2

    :cond_2
    move v1, v0

    goto :goto_3

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method private static a()V
    .locals 13

    .prologue
    const/4 v3, 0x0

    const/4 v12, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 101
    invoke-static {}, Lcwz;->a()V

    .line 104
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "gtalk_vc_force_camera_ids"

    invoke-static {v4, v5}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 107
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v5, "gtalk_vc_force_camera_dimensions"

    invoke-static {v0, v5}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 109
    if-eqz v4, :cond_4

    .line 110
    const-string v0, ","

    invoke-virtual {v4, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 111
    array-length v0, v1

    if-nez v0, :cond_2

    .line 112
    const-string v0, "Camera override empty, disabling"

    invoke-static {v0}, Lf;->j(Ljava/lang/String;)V

    .line 177
    :cond_0
    sget v0, Lapa;->b:I

    if-eqz v0, :cond_1

    if-eqz v7, :cond_1

    .line 178
    const-string v0, ","

    invoke-virtual {v7, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 179
    array-length v0, v1

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_8

    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Non-multiple of 2 length in gtalk_vc_force_camera_dimensions "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Babel"

    invoke-static {v1, v0}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    :cond_1
    return-void

    .line 114
    :cond_2
    sput v2, Lapa;->b:I

    .line 115
    array-length v0, v1

    .line 116
    new-array v0, v0, [I

    sput-object v0, Lapa;->a:[I

    .line 117
    array-length v4, v1

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v1, v0

    .line 119
    :try_start_0
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 120
    if-eq v6, v12, :cond_3

    .line 121
    sget-object v8, Lapa;->a:[I

    sget v9, Lapa;->b:I

    aput v6, v8, v9

    .line 122
    sget v6, Lapa;->b:I

    add-int/lit8 v6, v6, 0x1

    sput v6, Lapa;->b:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    :cond_3
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 125
    :catch_0
    move-exception v6

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "bad id "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " in "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Babel"

    invoke-static {v6, v5}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 130
    :cond_4
    new-instance v8, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v8}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 133
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/CameraInterface;->getInstance()Lcom/google/android/libraries/hangouts/video/CameraInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraInterface;->getCameraCount()I

    move-result v9

    .line 134
    new-array v0, v9, [I

    sput-object v0, Lapa;->a:[I

    .line 137
    sget-object v0, Lapa;->a:[I

    array-length v0, v0

    if-eq v9, v0, :cond_6

    move v0, v1

    .line 138
    :goto_2
    sput v2, Lapa;->b:I

    move v6, v2

    .line 140
    :goto_3
    const/4 v4, 0x3

    if-ge v6, v4, :cond_0

    move v5, v2

    .line 141
    :goto_4
    if-ge v5, v9, :cond_7

    .line 143
    :try_start_1
    invoke-static {v5, v8}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 149
    packed-switch v6, :pswitch_data_0

    .line 163
    iget v4, v8, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-eq v4, v1, :cond_b

    iget v4, v8, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-eqz v4, :cond_b

    move v4, v1

    .line 170
    :goto_5
    if-eqz v4, :cond_5

    .line 171
    sget-object v4, Lapa;->a:[I

    sget v10, Lapa;->b:I

    aput v5, v4, v10

    .line 172
    sget v4, Lapa;->b:I

    add-int/lit8 v4, v4, 0x1

    sput v4, Lapa;->b:I

    .line 141
    :cond_5
    :goto_6
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_4

    :cond_6
    move v0, v2

    .line 137
    goto :goto_2

    .line 144
    :catch_1
    move-exception v4

    .line 145
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Couldn\'t get camera info for camera "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-array v11, v1, [Ljava/lang/Object;

    aput-object v4, v11, v2

    invoke-static {v10, v11}, Lf;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_6

    .line 151
    :pswitch_0
    iget v4, v8, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v4, v1, :cond_b

    .line 152
    if-nez v0, :cond_b

    move v4, v1

    .line 153
    goto :goto_5

    .line 158
    :pswitch_1
    iget v4, v8, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-nez v4, :cond_b

    move v4, v1

    .line 159
    goto :goto_5

    .line 140
    :cond_7
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_3

    .line 184
    :cond_8
    sget v0, Lapa;->b:I

    array-length v4, v1

    div-int/lit8 v4, v4, 0x2

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 185
    if-lez v4, :cond_1

    .line 186
    new-array v0, v4, [Lcom/google/android/libraries/hangouts/video/Size;

    sput-object v0, Lapa;->c:[Lcom/google/android/libraries/hangouts/video/Size;

    .line 187
    :goto_7
    if-ge v2, v4, :cond_1

    .line 188
    :try_start_2
    new-instance v0, Lcom/google/android/libraries/hangouts/video/Size;

    mul-int/lit8 v5, v2, 0x2

    aget-object v5, v1, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    mul-int/lit8 v6, v2, 0x2

    add-int/lit8 v6, v6, 0x1

    aget-object v6, v1, v6

    .line 191
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-direct {v0, v5, v6}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    .line 195
    :goto_8
    iget v5, v0, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    if-eq v5, v12, :cond_9

    iget v5, v0, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    if-ne v5, v12, :cond_a

    :cond_9
    move-object v0, v3

    .line 198
    :cond_a
    sget-object v5, Lapa;->c:[Lcom/google/android/libraries/hangouts/video/Size;

    aput-object v0, v5, v2

    .line 187
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 193
    :catch_2
    move-exception v0

    move-object v0, v3

    goto :goto_8

    :cond_b
    move v4, v2

    goto :goto_5

    .line 149
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(I)Lcom/google/android/libraries/hangouts/video/Size;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lapa;->c:[Lcom/google/android/libraries/hangouts/video/Size;

    if-eqz v0, :cond_0

    sget-object v0, Lapa;->c:[Lcom/google/android/libraries/hangouts/video/Size;

    array-length v0, v0

    if-lt p0, v0, :cond_1

    .line 55
    :cond_0
    const/4 v0, 0x0

    .line 57
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lapa;->c:[Lcom/google/android/libraries/hangouts/video/Size;

    aget-object v0, v0, p0

    goto :goto_0
.end method
