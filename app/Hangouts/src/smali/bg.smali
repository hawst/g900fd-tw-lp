.class public final Lbg;
.super Lbx;
.source "PG"


# static fields
.field public static final d:Lby;


# instance fields
.field public a:I

.field public b:Ljava/lang/CharSequence;

.field public c:Landroid/app/PendingIntent;

.field private final e:Landroid/os/Bundle;

.field private final f:[Lcl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1700
    new-instance v0, Lby;

    invoke-direct {v0}, Lby;-><init>()V

    sput-object v0, Lbg;->d:Lby;

    return-void
.end method

.method public constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V
    .locals 6

    .prologue
    .line 1447
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lbg;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Lcl;)V

    .line 1448
    return-void
.end method

.method private constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Lcl;)V
    .locals 0

    .prologue
    .line 1451
    invoke-direct {p0}, Lbx;-><init>()V

    .line 1452
    iput p1, p0, Lbg;->a:I

    .line 1453
    iput-object p2, p0, Lbg;->b:Ljava/lang/CharSequence;

    .line 1454
    iput-object p3, p0, Lbg;->c:Landroid/app/PendingIntent;

    .line 1455
    if-eqz p4, :cond_0

    :goto_0
    iput-object p4, p0, Lbg;->e:Landroid/os/Bundle;

    .line 1456
    iput-object p5, p0, Lbg;->f:[Lcl;

    .line 1457
    return-void

    .line 1455
    :cond_0
    new-instance p4, Landroid/os/Bundle;

    invoke-direct {p4}, Landroid/os/Bundle;-><init>()V

    goto :goto_0
.end method

.method synthetic constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Lcl;B)V
    .locals 0

    .prologue
    .line 1428
    invoke-direct/range {p0 .. p5}, Lbg;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Lcl;)V

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 1461
    iget v0, p0, Lbg;->a:I

    return v0
.end method

.method protected b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1466
    iget-object v0, p0, Lbg;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method protected c()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 1471
    iget-object v0, p0, Lbg;->c:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public d()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1478
    iget-object v0, p0, Lbg;->e:Landroid/os/Bundle;

    return-object v0
.end method

.method public bridge synthetic e()[Lcr;
    .locals 1

    .prologue
    .line 1428
    iget-object v0, p0, Lbg;->f:[Lcl;

    return-object v0
.end method
