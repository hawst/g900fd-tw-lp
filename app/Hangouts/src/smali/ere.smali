.class public final Lere;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lere;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ldlb;

.field public g:Lesh;

.field public h:Ljava/lang/String;

.field public i:Lerh;

.field public j:Lerd;

.field public k:Ljava/lang/Boolean;

.field public l:Lerg;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1302
    const/4 v0, 0x0

    new-array v0, v0, [Lere;

    sput-object v0, Lere;->a:[Lere;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1303
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1314
    iput-object v0, p0, Lere;->f:Ldlb;

    .line 1317
    iput-object v0, p0, Lere;->g:Lesh;

    .line 1322
    iput-object v0, p0, Lere;->i:Lerh;

    .line 1325
    iput-object v0, p0, Lere;->j:Lerd;

    .line 1330
    iput-object v0, p0, Lere;->l:Lerg;

    .line 1303
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1389
    const/4 v0, 0x0

    .line 1390
    iget-object v1, p0, Lere;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1391
    const/4 v0, 0x1

    iget-object v1, p0, Lere;->b:Ljava/lang/String;

    .line 1392
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1394
    :cond_0
    iget-object v1, p0, Lere;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1395
    const/4 v1, 0x2

    iget-object v2, p0, Lere;->c:Ljava/lang/String;

    .line 1396
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1398
    :cond_1
    iget-object v1, p0, Lere;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1399
    const/4 v1, 0x3

    iget-object v2, p0, Lere;->e:Ljava/lang/String;

    .line 1400
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1402
    :cond_2
    iget-object v1, p0, Lere;->f:Ldlb;

    if-eqz v1, :cond_3

    .line 1403
    const/4 v1, 0x4

    iget-object v2, p0, Lere;->f:Ldlb;

    .line 1404
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1406
    :cond_3
    iget-object v1, p0, Lere;->h:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1407
    const/4 v1, 0x5

    iget-object v2, p0, Lere;->h:Ljava/lang/String;

    .line 1408
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1410
    :cond_4
    iget-object v1, p0, Lere;->d:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1411
    const/4 v1, 0x6

    iget-object v2, p0, Lere;->d:Ljava/lang/String;

    .line 1412
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1414
    :cond_5
    iget-object v1, p0, Lere;->j:Lerd;

    if-eqz v1, :cond_6

    .line 1415
    const/4 v1, 0x7

    iget-object v2, p0, Lere;->j:Lerd;

    .line 1416
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1418
    :cond_6
    iget-object v1, p0, Lere;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 1419
    const/16 v1, 0x8

    iget-object v2, p0, Lere;->k:Ljava/lang/Boolean;

    .line 1420
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1422
    :cond_7
    iget-object v1, p0, Lere;->l:Lerg;

    if-eqz v1, :cond_8

    .line 1423
    const/16 v1, 0x9

    iget-object v2, p0, Lere;->l:Lerg;

    .line 1424
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1426
    :cond_8
    iget-object v1, p0, Lere;->m:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 1427
    const/16 v1, 0xa

    iget-object v2, p0, Lere;->m:Ljava/lang/String;

    .line 1428
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1430
    :cond_9
    iget-object v1, p0, Lere;->n:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 1431
    const/16 v1, 0xb

    iget-object v2, p0, Lere;->n:Ljava/lang/String;

    .line 1432
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1434
    :cond_a
    iget-object v1, p0, Lere;->o:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 1435
    const/16 v1, 0xc

    iget-object v2, p0, Lere;->o:Ljava/lang/String;

    .line 1436
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1438
    :cond_b
    iget-object v1, p0, Lere;->i:Lerh;

    if-eqz v1, :cond_c

    .line 1439
    const/16 v1, 0xd

    iget-object v2, p0, Lere;->i:Lerh;

    .line 1440
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1442
    :cond_c
    iget-object v1, p0, Lere;->g:Lesh;

    if-eqz v1, :cond_d

    .line 1443
    const/16 v1, 0xe

    iget-object v2, p0, Lere;->g:Lesh;

    .line 1444
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1446
    :cond_d
    iget-object v1, p0, Lere;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1447
    iput v0, p0, Lere;->cachedSize:I

    .line 1448
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1299
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lere;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lere;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lere;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lere;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lere;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lere;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lere;->f:Ldlb;

    if-nez v0, :cond_2

    new-instance v0, Ldlb;

    invoke-direct {v0}, Ldlb;-><init>()V

    iput-object v0, p0, Lere;->f:Ldlb;

    :cond_2
    iget-object v0, p0, Lere;->f:Ldlb;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lere;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lere;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lere;->j:Lerd;

    if-nez v0, :cond_3

    new-instance v0, Lerd;

    invoke-direct {v0}, Lerd;-><init>()V

    iput-object v0, p0, Lere;->j:Lerd;

    :cond_3
    iget-object v0, p0, Lere;->j:Lerd;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lere;->k:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_9
    iget-object v0, p0, Lere;->l:Lerg;

    if-nez v0, :cond_4

    new-instance v0, Lerg;

    invoke-direct {v0}, Lerg;-><init>()V

    iput-object v0, p0, Lere;->l:Lerg;

    :cond_4
    iget-object v0, p0, Lere;->l:Lerg;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lere;->m:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lere;->n:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lere;->o:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lere;->i:Lerh;

    if-nez v0, :cond_5

    new-instance v0, Lerh;

    invoke-direct {v0}, Lerh;-><init>()V

    iput-object v0, p0, Lere;->i:Lerh;

    :cond_5
    iget-object v0, p0, Lere;->i:Lerh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Lere;->g:Lesh;

    if-nez v0, :cond_6

    new-instance v0, Lesh;

    invoke-direct {v0}, Lesh;-><init>()V

    iput-object v0, p0, Lere;->g:Lesh;

    :cond_6
    iget-object v0, p0, Lere;->g:Lesh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1341
    iget-object v0, p0, Lere;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1342
    const/4 v0, 0x1

    iget-object v1, p0, Lere;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1344
    :cond_0
    iget-object v0, p0, Lere;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1345
    const/4 v0, 0x2

    iget-object v1, p0, Lere;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1347
    :cond_1
    iget-object v0, p0, Lere;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1348
    const/4 v0, 0x3

    iget-object v1, p0, Lere;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1350
    :cond_2
    iget-object v0, p0, Lere;->f:Ldlb;

    if-eqz v0, :cond_3

    .line 1351
    const/4 v0, 0x4

    iget-object v1, p0, Lere;->f:Ldlb;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1353
    :cond_3
    iget-object v0, p0, Lere;->h:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1354
    const/4 v0, 0x5

    iget-object v1, p0, Lere;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1356
    :cond_4
    iget-object v0, p0, Lere;->d:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1357
    const/4 v0, 0x6

    iget-object v1, p0, Lere;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1359
    :cond_5
    iget-object v0, p0, Lere;->j:Lerd;

    if-eqz v0, :cond_6

    .line 1360
    const/4 v0, 0x7

    iget-object v1, p0, Lere;->j:Lerd;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1362
    :cond_6
    iget-object v0, p0, Lere;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 1363
    const/16 v0, 0x8

    iget-object v1, p0, Lere;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 1365
    :cond_7
    iget-object v0, p0, Lere;->l:Lerg;

    if-eqz v0, :cond_8

    .line 1366
    const/16 v0, 0x9

    iget-object v1, p0, Lere;->l:Lerg;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1368
    :cond_8
    iget-object v0, p0, Lere;->m:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 1369
    const/16 v0, 0xa

    iget-object v1, p0, Lere;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1371
    :cond_9
    iget-object v0, p0, Lere;->n:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 1372
    const/16 v0, 0xb

    iget-object v1, p0, Lere;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1374
    :cond_a
    iget-object v0, p0, Lere;->o:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 1375
    const/16 v0, 0xc

    iget-object v1, p0, Lere;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1377
    :cond_b
    iget-object v0, p0, Lere;->i:Lerh;

    if-eqz v0, :cond_c

    .line 1378
    const/16 v0, 0xd

    iget-object v1, p0, Lere;->i:Lerh;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1380
    :cond_c
    iget-object v0, p0, Lere;->g:Lesh;

    if-eqz v0, :cond_d

    .line 1381
    const/16 v0, 0xe

    iget-object v1, p0, Lere;->g:Lesh;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1383
    :cond_d
    iget-object v0, p0, Lere;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1385
    return-void
.end method
