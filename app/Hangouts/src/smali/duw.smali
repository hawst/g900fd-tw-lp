.class public final Lduw;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lduw;


# instance fields
.field public b:Ldui;

.field public c:Ldut;

.field public d:Lduu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13435
    const/4 v0, 0x0

    new-array v0, v0, [Lduw;

    sput-object v0, Lduw;->a:[Lduw;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 13436
    invoke-direct {p0}, Lepn;-><init>()V

    .line 13439
    iput-object v0, p0, Lduw;->b:Ldui;

    .line 13442
    iput-object v0, p0, Lduw;->c:Ldut;

    .line 13445
    iput-object v0, p0, Lduw;->d:Lduu;

    .line 13436
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 13465
    const/4 v0, 0x0

    .line 13466
    iget-object v1, p0, Lduw;->b:Ldui;

    if-eqz v1, :cond_0

    .line 13467
    const/4 v0, 0x1

    iget-object v1, p0, Lduw;->b:Ldui;

    .line 13468
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 13470
    :cond_0
    iget-object v1, p0, Lduw;->c:Ldut;

    if-eqz v1, :cond_1

    .line 13471
    const/4 v1, 0x2

    iget-object v2, p0, Lduw;->c:Ldut;

    .line 13472
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13474
    :cond_1
    iget-object v1, p0, Lduw;->d:Lduu;

    if-eqz v1, :cond_2

    .line 13475
    const/4 v1, 0x3

    iget-object v2, p0, Lduw;->d:Lduu;

    .line 13476
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13478
    :cond_2
    iget-object v1, p0, Lduw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13479
    iput v0, p0, Lduw;->cachedSize:I

    .line 13480
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 13432
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lduw;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lduw;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lduw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lduw;->b:Ldui;

    if-nez v0, :cond_2

    new-instance v0, Ldui;

    invoke-direct {v0}, Ldui;-><init>()V

    iput-object v0, p0, Lduw;->b:Ldui;

    :cond_2
    iget-object v0, p0, Lduw;->b:Ldui;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lduw;->c:Ldut;

    if-nez v0, :cond_3

    new-instance v0, Ldut;

    invoke-direct {v0}, Ldut;-><init>()V

    iput-object v0, p0, Lduw;->c:Ldut;

    :cond_3
    iget-object v0, p0, Lduw;->c:Ldut;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lduw;->d:Lduu;

    if-nez v0, :cond_4

    new-instance v0, Lduu;

    invoke-direct {v0}, Lduu;-><init>()V

    iput-object v0, p0, Lduw;->d:Lduu;

    :cond_4
    iget-object v0, p0, Lduw;->d:Lduu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 13450
    iget-object v0, p0, Lduw;->b:Ldui;

    if-eqz v0, :cond_0

    .line 13451
    const/4 v0, 0x1

    iget-object v1, p0, Lduw;->b:Ldui;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 13453
    :cond_0
    iget-object v0, p0, Lduw;->c:Ldut;

    if-eqz v0, :cond_1

    .line 13454
    const/4 v0, 0x2

    iget-object v1, p0, Lduw;->c:Ldut;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 13456
    :cond_1
    iget-object v0, p0, Lduw;->d:Lduu;

    if-eqz v0, :cond_2

    .line 13457
    const/4 v0, 0x3

    iget-object v1, p0, Lduw;->d:Lduu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 13459
    :cond_2
    iget-object v0, p0, Lduw;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 13461
    return-void
.end method
