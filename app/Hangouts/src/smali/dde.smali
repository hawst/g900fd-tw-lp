.class public final Ldde;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldde;


# instance fields
.field public b:Ldcw;

.field public c:Ldgr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 321
    const/4 v0, 0x0

    new-array v0, v0, [Ldde;

    sput-object v0, Ldde;->a:[Ldde;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 322
    invoke-direct {p0}, Lepn;-><init>()V

    .line 325
    iput-object v0, p0, Ldde;->b:Ldcw;

    .line 328
    iput-object v0, p0, Ldde;->c:Ldgr;

    .line 322
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 345
    const/4 v0, 0x0

    .line 346
    iget-object v1, p0, Ldde;->b:Ldcw;

    if-eqz v1, :cond_0

    .line 347
    const/4 v0, 0x1

    iget-object v1, p0, Ldde;->b:Ldcw;

    .line 348
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 350
    :cond_0
    iget-object v1, p0, Ldde;->c:Ldgr;

    if-eqz v1, :cond_1

    .line 351
    const/4 v1, 0x2

    iget-object v2, p0, Ldde;->c:Ldgr;

    .line 352
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 354
    :cond_1
    iget-object v1, p0, Ldde;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 355
    iput v0, p0, Ldde;->cachedSize:I

    .line 356
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 318
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldde;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldde;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldde;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldde;->b:Ldcw;

    if-nez v0, :cond_2

    new-instance v0, Ldcw;

    invoke-direct {v0}, Ldcw;-><init>()V

    iput-object v0, p0, Ldde;->b:Ldcw;

    :cond_2
    iget-object v0, p0, Ldde;->b:Ldcw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldde;->c:Ldgr;

    if-nez v0, :cond_3

    new-instance v0, Ldgr;

    invoke-direct {v0}, Ldgr;-><init>()V

    iput-object v0, p0, Ldde;->c:Ldgr;

    :cond_3
    iget-object v0, p0, Ldde;->c:Ldgr;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Ldde;->b:Ldcw;

    if-eqz v0, :cond_0

    .line 334
    const/4 v0, 0x1

    iget-object v1, p0, Ldde;->b:Ldcw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 336
    :cond_0
    iget-object v0, p0, Ldde;->c:Ldgr;

    if-eqz v0, :cond_1

    .line 337
    const/4 v0, 0x2

    iget-object v1, p0, Ldde;->c:Ldgr;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 339
    :cond_1
    iget-object v0, p0, Ldde;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 341
    return-void
.end method
