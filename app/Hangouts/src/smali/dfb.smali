.class public final Ldfb;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldfb;


# instance fields
.field public b:Ldfi;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/Boolean;

.field public h:[Ljava/lang/String;

.field public i:[Ldgl;

.field public j:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3306
    const/4 v0, 0x0

    new-array v0, v0, [Ldfb;

    sput-object v0, Ldfb;->a:[Ldfb;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3307
    invoke-direct {p0}, Lepn;-><init>()V

    .line 3317
    iput-object v1, p0, Ldfb;->b:Ldfi;

    .line 3330
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldfb;->h:[Ljava/lang/String;

    .line 3333
    sget-object v0, Ldgl;->a:[Ldgl;

    iput-object v0, p0, Ldfb;->i:[Ldgl;

    .line 3336
    iput-object v1, p0, Ldfb;->j:Ljava/lang/Integer;

    .line 3307
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 3381
    iget-object v0, p0, Ldfb;->b:Ldfi;

    if-eqz v0, :cond_a

    .line 3382
    const/4 v0, 0x1

    iget-object v2, p0, Ldfb;->b:Ldfi;

    .line 3383
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3385
    :goto_0
    iget-object v2, p0, Ldfb;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 3386
    const/4 v2, 0x2

    iget-object v3, p0, Ldfb;->c:Ljava/lang/String;

    .line 3387
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3389
    :cond_0
    iget-object v2, p0, Ldfb;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 3390
    const/4 v2, 0x3

    iget-object v3, p0, Ldfb;->d:Ljava/lang/String;

    .line 3391
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3393
    :cond_1
    iget-object v2, p0, Ldfb;->e:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 3394
    const/4 v2, 0x4

    iget-object v3, p0, Ldfb;->e:Ljava/lang/String;

    .line 3395
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3397
    :cond_2
    iget-object v2, p0, Ldfb;->f:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 3398
    const/4 v2, 0x5

    iget-object v3, p0, Ldfb;->f:Ljava/lang/String;

    .line 3399
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3401
    :cond_3
    iget-object v2, p0, Ldfb;->g:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    .line 3402
    const/4 v2, 0x6

    iget-object v3, p0, Ldfb;->g:Ljava/lang/Boolean;

    .line 3403
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 3405
    :cond_4
    iget-object v2, p0, Ldfb;->h:[Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Ldfb;->h:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 3407
    iget-object v4, p0, Ldfb;->h:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_5

    aget-object v6, v4, v2

    .line 3409
    invoke-static {v6}, Lepl;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 3407
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3411
    :cond_5
    add-int/2addr v0, v3

    .line 3412
    iget-object v2, p0, Ldfb;->h:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 3414
    :cond_6
    iget-object v2, p0, Ldfb;->i:[Ldgl;

    if-eqz v2, :cond_8

    .line 3415
    iget-object v2, p0, Ldfb;->i:[Ldgl;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 3416
    if-eqz v4, :cond_7

    .line 3417
    const/16 v5, 0x8

    .line 3418
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 3415
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3422
    :cond_8
    iget-object v1, p0, Ldfb;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 3423
    const/16 v1, 0x9

    iget-object v2, p0, Ldfb;->j:Ljava/lang/Integer;

    .line 3424
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3426
    :cond_9
    iget-object v1, p0, Ldfb;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3427
    iput v0, p0, Ldfb;->cachedSize:I

    .line 3428
    return v0

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 3303
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldfb;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldfb;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldfb;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldfb;->b:Ldfi;

    if-nez v0, :cond_2

    new-instance v0, Ldfi;

    invoke-direct {v0}, Ldfi;-><init>()V

    iput-object v0, p0, Ldfb;->b:Ldfi;

    :cond_2
    iget-object v0, p0, Ldfb;->b:Ldfi;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldfb;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldfb;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldfb;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldfb;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldfb;->g:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldfb;->h:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Ldfb;->h:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldfb;->h:[Ljava/lang/String;

    :goto_1
    iget-object v2, p0, Ldfb;->h:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Ldfb;->h:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v2, p0, Ldfb;->h:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldfb;->i:[Ldgl;

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Ldgl;

    iget-object v3, p0, Ldfb;->i:[Ldgl;

    if-eqz v3, :cond_4

    iget-object v3, p0, Ldfb;->i:[Ldgl;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Ldfb;->i:[Ldgl;

    :goto_3
    iget-object v2, p0, Ldfb;->i:[Ldgl;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ldfb;->i:[Ldgl;

    new-instance v3, Ldgl;

    invoke-direct {v3}, Ldgl;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldfb;->i:[Ldgl;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    iget-object v0, p0, Ldfb;->i:[Ldgl;

    array-length v0, v0

    goto :goto_2

    :cond_6
    iget-object v2, p0, Ldfb;->i:[Ldgl;

    new-instance v3, Ldgl;

    invoke-direct {v3}, Ldgl;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldfb;->i:[Ldgl;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_7

    const/4 v2, 0x2

    if-eq v0, v2, :cond_7

    const/4 v2, 0x3

    if-eq v0, v2, :cond_7

    const/4 v2, 0x4

    if-ne v0, v2, :cond_8

    :cond_7
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldfb;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_8
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldfb;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 3341
    iget-object v1, p0, Ldfb;->b:Ldfi;

    if-eqz v1, :cond_0

    .line 3342
    const/4 v1, 0x1

    iget-object v2, p0, Ldfb;->b:Ldfi;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 3344
    :cond_0
    iget-object v1, p0, Ldfb;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3345
    const/4 v1, 0x2

    iget-object v2, p0, Ldfb;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 3347
    :cond_1
    iget-object v1, p0, Ldfb;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 3348
    const/4 v1, 0x3

    iget-object v2, p0, Ldfb;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 3350
    :cond_2
    iget-object v1, p0, Ldfb;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 3351
    const/4 v1, 0x4

    iget-object v2, p0, Ldfb;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 3353
    :cond_3
    iget-object v1, p0, Ldfb;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 3354
    const/4 v1, 0x5

    iget-object v2, p0, Ldfb;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 3356
    :cond_4
    iget-object v1, p0, Ldfb;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 3357
    const/4 v1, 0x6

    iget-object v2, p0, Ldfb;->g:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 3359
    :cond_5
    iget-object v1, p0, Ldfb;->h:[Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 3360
    iget-object v2, p0, Ldfb;->h:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 3361
    const/4 v5, 0x7

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 3360
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3364
    :cond_6
    iget-object v1, p0, Ldfb;->i:[Ldgl;

    if-eqz v1, :cond_8

    .line 3365
    iget-object v1, p0, Ldfb;->i:[Ldgl;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    .line 3366
    if-eqz v3, :cond_7

    .line 3367
    const/16 v4, 0x8

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 3365
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3371
    :cond_8
    iget-object v0, p0, Ldfb;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 3372
    const/16 v0, 0x9

    iget-object v1, p0, Ldfb;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 3374
    :cond_9
    iget-object v0, p0, Ldfb;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 3376
    return-void
.end method
