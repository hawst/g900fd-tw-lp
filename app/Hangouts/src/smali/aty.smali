.class final Laty;
.super Latg;
.source "PG"


# instance fields
.field final synthetic f:Latu;


# direct methods
.method constructor <init>(Latu;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 232
    iput-object p1, p0, Laty;->f:Latu;

    .line 233
    invoke-direct {p0, p1, p2}, Latg;-><init>(Late;Landroid/content/Context;)V

    .line 234
    return-void
.end method


# virtual methods
.method protected a(I)I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 239
    iget-object v0, p0, Laty;->f:Latu;

    iget v0, v0, Latu;->F:I

    rem-int/lit16 v0, v0, 0xb4

    if-nez v0, :cond_1

    move v0, v1

    .line 240
    :goto_0
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->getDefaultAspectRatioSize(Z)Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v3

    .line 242
    if-eqz v0, :cond_4

    .line 243
    new-instance v0, Lcom/google/android/libraries/hangouts/video/Size;

    iget v4, v3, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    iget v3, v3, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    invoke-direct {v0, v4, v3}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    .line 247
    :goto_1
    iget-object v3, p0, Laty;->f:Latu;

    invoke-virtual {v3}, Latu;->A()Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    move-result-object v3

    .line 248
    iget-object v4, p0, Laty;->f:Latu;

    iget-boolean v4, v4, Latu;->J:Z

    if-eqz v4, :cond_0

    if-eqz v3, :cond_0

    .line 249
    invoke-virtual {v3}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->getRotatedFrameOutputSize()Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v3

    .line 250
    if-eqz v3, :cond_0

    move-object v0, v3

    .line 255
    :cond_0
    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 256
    iget v3, v0, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    if-lez v3, :cond_2

    move v3, v1

    :goto_2
    invoke-static {v3}, Lcwz;->a(Z)V

    .line 257
    iget v3, v0, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    if-lez v3, :cond_3

    :goto_3
    invoke-static {v1}, Lcwz;->a(Z)V

    .line 258
    iget v1, v0, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    int-to-float v1, v1

    iget v0, v0, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    int-to-float v0, v0

    div-float v0, v1, v0

    .line 259
    int-to-float v1, p1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0

    :cond_1
    move v0, v2

    .line 239
    goto :goto_0

    :cond_2
    move v3, v2

    .line 256
    goto :goto_2

    :cond_3
    move v1, v2

    .line 257
    goto :goto_3

    :cond_4
    move-object v0, v3

    goto :goto_1
.end method
