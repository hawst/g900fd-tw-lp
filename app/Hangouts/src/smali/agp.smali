.class public final Lagp;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lahp;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 0

    .prologue
    .line 7460
    iput-object p1, p0, Lagp;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private static varargs a([Lahp;)Ljava/lang/Void;
    .locals 14

    .prologue
    const/4 v4, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v6, 0x0

    .line 7463
    aget-object v9, p0, v8

    .line 7464
    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aa()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7465
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateContactStatTracker -- recipient: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7467
    :cond_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 7469
    iget-object v1, v9, Lahp;->c:Ljava/lang/String;

    if-nez v1, :cond_8

    .line 7470
    iput-boolean v7, v9, Lahp;->e:Z

    .line 7473
    iget-object v1, v9, Lahp;->b:Lbdk;

    iget-object v1, v1, Lbdk;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 7476
    invoke-static {}, Lbsc;->a()Lbsc;

    move-result-object v10

    .line 7479
    :try_start_0
    invoke-virtual {v10}, Lbsc;->c()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 7480
    iget-object v1, v9, Lahp;->b:Lbdk;

    iget-object v1, v1, Lbdk;->a:Ljava/lang/String;

    .line 7481
    invoke-virtual {v10, v1}, Lbsc;->a(Ljava/lang/String;)Laea;

    move-result-object v1

    .line 7483
    if-eqz v1, :cond_a

    .line 7486
    invoke-virtual {v1}, Laea;->k()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laef;

    .line 7487
    iget-object v1, v1, Laef;->a:Ljava/lang/String;

    move-object v5, v1

    .line 7488
    :goto_0
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 7491
    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aa()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 7492
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "updateContactStatTracker by gaia. email: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7496
    :cond_1
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "contact_id=? and data1=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v11, 0x0

    iget-wide v12, v9, Lahp;->a:J

    .line 7499
    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v11

    const/4 v11, 0x1

    aput-object v5, v4, v11

    const/4 v5, 0x0

    .line 7496
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 7507
    :goto_1
    invoke-virtual {v10}, Lbsc;->b()V

    .line 7525
    :goto_2
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 7526
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v9, Lahp;->c:Ljava/lang/String;

    .line 7527
    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aa()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 7528
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "updateContactStatTracker dataId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v9, Lahp;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 7533
    :cond_2
    if-eqz v1, :cond_3

    .line 7534
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 7537
    :cond_3
    iget-object v1, v9, Lahp;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 7538
    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aa()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 7539
    const-string v0, "Babel"

    const-string v1, "updateContactStatTracker -- null data id"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7555
    :cond_4
    :goto_3
    return-object v6

    .line 7507
    :catchall_0
    move-exception v0

    invoke-virtual {v10}, Lbsc;->b()V

    throw v0

    .line 7509
    :cond_5
    iget-object v1, v9, Lahp;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 7511
    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aa()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 7512
    const-string v1, "Babel"

    const-string v2, "updateContactStatTracker by phone number"

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7514
    :cond_6
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v8

    const-string v3, "contact_id=? and data1=?"

    new-array v4, v4, [Ljava/lang/String;

    iget-wide v10, v9, Lahp;->a:J

    .line 7517
    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    iget-object v5, v9, Lahp;->d:Ljava/lang/String;

    aput-object v5, v4, v7

    move-object v5, v6

    .line 7514
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_2

    .line 7533
    :catchall_1
    move-exception v0

    if-eqz v1, :cond_7

    .line 7534
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v0

    .line 7544
    :cond_8
    sget-object v1, Landroid/provider/ContactsContract$DataUsageFeedback;->FEEDBACK_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    iget-object v2, v9, Lahp;->c:Ljava/lang/String;

    .line 7545
    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "type"

    const-string v3, "short_text"

    .line 7546
    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 7548
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 7550
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {v0, v1, v2, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_9

    move v0, v7

    .line 7551
    :goto_4
    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aa()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 7552
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "updateContactStatTracker -- success: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " for uri: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_9
    move v0, v8

    .line 7550
    goto :goto_4

    :cond_a
    move-object v1, v6

    goto/16 :goto_1

    :cond_b
    move-object v5, v6

    goto/16 :goto_0
.end method


# virtual methods
.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7460
    check-cast p1, [Lahp;

    invoke-static {p1}, Lagp;->a([Lahp;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 7460
    return-void
.end method
