.class public final Ldtw;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldtw;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:[Ldui;

.field public d:Ljava/lang/Integer;

.field public e:[[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6034
    const/4 v0, 0x0

    new-array v0, v0, [Ldtw;

    sput-object v0, Ldtw;->a:[Ldtw;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6035
    invoke-direct {p0}, Lepn;-><init>()V

    .line 6038
    iput-object v1, p0, Ldtw;->b:Ljava/lang/Integer;

    .line 6041
    sget-object v0, Ldui;->a:[Ldui;

    iput-object v0, p0, Ldtw;->c:[Ldui;

    .line 6044
    iput-object v1, p0, Ldtw;->d:Ljava/lang/Integer;

    .line 6047
    sget-object v0, Lept;->k:[[B

    iput-object v0, p0, Ldtw;->e:[[B

    .line 6035
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 6077
    iget-object v0, p0, Ldtw;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 6078
    const/4 v0, 0x1

    iget-object v2, p0, Ldtw;->b:Ljava/lang/Integer;

    .line 6079
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6081
    :goto_0
    iget-object v2, p0, Ldtw;->e:[[B

    if-eqz v2, :cond_1

    iget-object v2, p0, Ldtw;->e:[[B

    array-length v2, v2

    if-lez v2, :cond_1

    .line 6083
    iget-object v4, p0, Ldtw;->e:[[B

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    .line 6085
    invoke-static {v6}, Lepl;->b([B)I

    move-result v6

    add-int/2addr v3, v6

    .line 6083
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 6087
    :cond_0
    add-int/2addr v0, v3

    .line 6088
    iget-object v2, p0, Ldtw;->e:[[B

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 6090
    :cond_1
    iget-object v2, p0, Ldtw;->c:[Ldui;

    if-eqz v2, :cond_3

    .line 6091
    iget-object v2, p0, Ldtw;->c:[Ldui;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 6092
    if-eqz v4, :cond_2

    .line 6093
    const/4 v5, 0x3

    .line 6094
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 6091
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 6098
    :cond_3
    iget-object v1, p0, Ldtw;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 6099
    const/4 v1, 0x4

    iget-object v2, p0, Ldtw;->d:Ljava/lang/Integer;

    .line 6100
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6102
    :cond_4
    iget-object v1, p0, Ldtw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6103
    iput v0, p0, Ldtw;->cachedSize:I

    .line 6104
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 6031
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldtw;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldtw;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldtw;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_2

    if-ne v0, v5, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldtw;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldtw;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldtw;->e:[[B

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [[B

    iget-object v3, p0, Ldtw;->e:[[B

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldtw;->e:[[B

    :goto_1
    iget-object v2, p0, Ldtw;->e:[[B

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldtw;->e:[[B

    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldtw;->e:[[B

    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldtw;->c:[Ldui;

    if-nez v0, :cond_6

    move v0, v1

    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Ldui;

    iget-object v3, p0, Ldtw;->c:[Ldui;

    if-eqz v3, :cond_5

    iget-object v3, p0, Ldtw;->c:[Ldui;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Ldtw;->c:[Ldui;

    :goto_3
    iget-object v2, p0, Ldtw;->c:[Ldui;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Ldtw;->c:[Ldui;

    new-instance v3, Ldui;

    invoke-direct {v3}, Ldui;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldtw;->c:[Ldui;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    iget-object v0, p0, Ldtw;->c:[Ldui;

    array-length v0, v0

    goto :goto_2

    :cond_7
    iget-object v2, p0, Ldtw;->c:[Ldui;

    new-instance v3, Ldui;

    invoke-direct {v3}, Ldui;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldtw;->c:[Ldui;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_8

    if-eq v0, v4, :cond_8

    if-ne v0, v5, :cond_9

    :cond_8
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldtw;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_9
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldtw;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 6052
    iget-object v1, p0, Ldtw;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 6053
    const/4 v1, 0x1

    iget-object v2, p0, Ldtw;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 6055
    :cond_0
    iget-object v1, p0, Ldtw;->e:[[B

    if-eqz v1, :cond_1

    .line 6056
    iget-object v2, p0, Ldtw;->e:[[B

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 6057
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->a(I[B)V

    .line 6056
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 6060
    :cond_1
    iget-object v1, p0, Ldtw;->c:[Ldui;

    if-eqz v1, :cond_3

    .line 6061
    iget-object v1, p0, Ldtw;->c:[Ldui;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 6062
    if-eqz v3, :cond_2

    .line 6063
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 6061
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 6067
    :cond_3
    iget-object v0, p0, Ldtw;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 6068
    const/4 v0, 0x4

    iget-object v1, p0, Ldtw;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 6070
    :cond_4
    iget-object v0, p0, Ldtw;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 6072
    return-void
.end method
