.class public Lccc;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static b:Lccc;


# instance fields
.field protected final a:Landroid/util/SparseIntArray;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    sput-object v0, Lccc;->b:Lccc;

    return-void
.end method

.method protected constructor <init>()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v1, Landroid/util/SparseIntArray;

    invoke-direct {v1}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v1, p0, Lccc;->a:Landroid/util/SparseIntArray;

    .line 43
    const-class v1, Lcom/google/android/apps/hangouts/R$drawable;

    invoke-virtual {v1}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v3

    array-length v4, v3

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 44
    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "emoji_u"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    const/4 v0, -0x1

    .line 47
    :try_start_0
    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v6, 0x7

    invoke-virtual {v1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const/16 v6, 0x10

    invoke-static {v1, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    move v1, v0

    .line 51
    :goto_1
    if-ltz v1, :cond_0

    .line 53
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v5, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v5, 0x0

    invoke-static {v0, v5}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    .line 54
    iget-object v5, p0, Lccc;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v5, v1, v0}, Landroid/util/SparseIntArray;->put(II)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1

    .line 43
    :cond_0
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 49
    :catch_0
    move-exception v1

    const-string v1, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Unable to parse resource: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    goto :goto_1

    .line 63
    :cond_1
    return-void

    :catch_1
    move-exception v0

    goto :goto_2

    .line 59
    :catch_2
    move-exception v0

    goto :goto_2
.end method

.method public static a()Lccc;
    .locals 2

    .prologue
    .line 30
    sget-object v0, Lccc;->b:Lccc;

    if-nez v0, :cond_0

    .line 31
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 32
    new-instance v0, Lccc;

    invoke-direct {v0}, Lccc;-><init>()V

    sput-object v0, Lccc;->b:Lccc;

    .line 37
    :cond_0
    :goto_0
    sget-object v0, Lccc;->b:Lccc;

    return-object v0

    .line 34
    :cond_1
    new-instance v0, Lccd;

    invoke-direct {v0}, Lccd;-><init>()V

    sput-object v0, Lccc;->b:Lccc;

    goto :goto_0
.end method


# virtual methods
.method public a(I)I
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lccc;->a:Landroid/util/SparseIntArray;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/CharSequence;Landroid/widget/TextView;)Landroid/text/SpannableString;
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Landroid/text/Spannable;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 84
    return-void
.end method

.method public a(Landroid/text/Spanned;)V
    .locals 0

    .prologue
    .line 95
    return-void
.end method
