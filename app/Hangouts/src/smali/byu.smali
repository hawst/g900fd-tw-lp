.class public abstract Lbyu;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Lyj;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field protected d:Z

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lyj;)V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lbyu;-><init>(Ljava/lang/String;Lyj;B)V

    .line 57
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lyj;B)V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lbyu;->e:Ljava/lang/String;

    .line 62
    iget-object v0, p0, Lbyu;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbyu;->e:Ljava/lang/String;

    const-string v1, "focus"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    :cond_0
    iput-object p2, p0, Lbyu;->a:Lyj;

    .line 65
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbyu;->d:Z

    .line 66
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lbyu;->e:Ljava/lang/String;

    return-object v0
.end method

.method public m()Lyj;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lbyu;->a:Lyj;

    return-object v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lbyu;->d:Z

    return v0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lbyu;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 97
    invoke-virtual {p0}, Lbyu;->a()V

    .line 99
    :cond_0
    iget-object v0, p0, Lbyu;->c:Ljava/lang/String;

    return-object v0
.end method

.method public p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lbyu;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 104
    invoke-virtual {p0}, Lbyu;->a()V

    .line 106
    :cond_0
    iget-object v0, p0, Lbyu;->b:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Base:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lbyu;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lbyu;->a:Lyj;

    if-nez v0, :cond_0

    const-string v0, "None"

    .line 83
    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Load:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbyu;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbyu;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " save:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lbyu;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 82
    :cond_0
    iget-object v0, p0, Lbyu;->a:Lyj;

    .line 83
    invoke-virtual {v0}, Lyj;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
