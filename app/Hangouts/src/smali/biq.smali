.class public Lbiq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final a:Z

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lbys;->h:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbiq;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1331
    return-void
.end method

.method public static a(Ljava/lang/String;Lyj;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lyj;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbiq;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1350
    :try_start_0
    new-instance v0, Ldpm;

    invoke-direct {v0}, Ldpm;-><init>()V

    const/4 v3, 0x0

    .line 1351
    invoke-static {p0, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v3

    .line 1350
    invoke-static {v0, v3}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldpm;
    :try_end_0
    .catch Lepq; {:try_start_0 .. :try_end_0} :catch_0

    .line 1356
    sget-boolean v3, Lbiq;->a:Z

    if-eqz v3, :cond_0

    .line 1357
    const-string v3, "Babel_protos"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ClientBatchUpdate from:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1360
    :cond_0
    iget-object v3, v0, Ldpm;->b:[Ldwv;

    array-length v3, v3

    .line 1361
    if-lez v3, :cond_2

    .line 1362
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1363
    :goto_0
    if-ge v2, v3, :cond_1

    .line 1364
    iget-object v4, v0, Ldpm;->b:[Ldwv;

    aget-object v4, v4, v2

    invoke-static {v4, p1, v1}, Lbiq;->a(Ldwv;Lyj;Ljava/util/ArrayList;)V

    .line 1363
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1352
    :catch_0
    move-exception v0

    .line 1353
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Problem parsing client update: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 1369
    :goto_1
    return-object v0

    :cond_1
    move-object v0, v1

    .line 1366
    goto :goto_1

    .line 1368
    :cond_2
    const-string v0, "Babel"

    const-string v2, "received a client update with no contents"

    invoke-static {v0, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 1369
    goto :goto_1
.end method

.method private static a(Lbis;Lbyc;Lyj;JI)V
    .locals 3

    .prologue
    .line 1375
    invoke-virtual {p1}, Lbyc;->a()Lbyc;

    move-result-object v0

    const-string v1, "server_update_field"

    .line 1376
    invoke-virtual {v0, v1}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    move-result-object v0

    .line 1377
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbyc;->b(Ljava/lang/String;)Lbyc;

    move-result-object v0

    .line 1378
    invoke-virtual {v0, p3, p4}, Lbyc;->a(J)Lbyc;

    move-result-object v0

    iget-wide v1, p0, Lbis;->e:J

    .line 1379
    invoke-virtual {v0, v1, v2}, Lbyc;->b(J)Lbyc;

    move-result-object v0

    .line 1380
    invoke-virtual {v0, p5}, Lbyc;->a(I)Lbyc;

    move-result-object v0

    iget-object v1, p0, Lbis;->c:Ljava/lang/String;

    .line 1381
    invoke-virtual {v0, v1}, Lbyc;->c(Ljava/lang/String;)Lbyc;

    move-result-object v0

    .line 1382
    invoke-virtual {v0, p2}, Lbyc;->a(Lyj;)Lbyc;

    move-result-object v0

    invoke-virtual {v0}, Lbyc;->b()V

    .line 1383
    return-void
.end method

.method private static a(Ldwv;Lyj;Ljava/util/ArrayList;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldwv;",
            "Lyj;",
            "Ljava/util/ArrayList",
            "<",
            "Lbiq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1391
    sget-boolean v0, Lbiq;->a:Z

    if-eqz v0, :cond_1

    .line 1394
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "received ClientStateUpdate "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1395
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1396
    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v5, "has"

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1397
    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_0

    .line 1398
    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getGenericReturnType()Ljava/lang/reflect/Type;

    move-result-object v0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-ne v0, v5, :cond_0

    .line 1400
    const/4 v0, 0x0

    :try_start_0
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {v4, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1401
    check-cast v0, Ljava/lang/Boolean;

    const/4 v5, 0x0

    invoke-static {v0, v5}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    .line 1402
    if-eqz v0, :cond_0

    .line 1403
    const-string v5, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "   "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ==> "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1395
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1405
    :catch_0
    move-exception v0

    .line 1406
    const-string v5, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "oops, couldn\'t invoke "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " gave exception "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1412
    :cond_1
    invoke-static {}, Lbya;->b()Z

    move-result v9

    .line 1413
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 1414
    invoke-virtual {p1}, Lyj;->c()Lbdk;

    move-result-object v10

    .line 1416
    if-eqz v9, :cond_1e

    .line 1417
    new-instance v1, Lbyc;

    invoke-direct {v1}, Lbyc;-><init>()V

    .line 1423
    :goto_2
    const/4 v2, 0x0

    .line 1424
    const-wide/16 v5, 0x0

    .line 1425
    const/4 v0, 0x0

    .line 1426
    iget-object v7, p0, Ldwv;->b:Ldww;

    if-eqz v7, :cond_3

    .line 1427
    iget-object v7, p0, Ldwv;->b:Ldww;

    .line 1428
    iget-object v2, v7, Ldww;->b:Ljava/lang/Integer;

    const/4 v5, 0x0

    invoke-static {v2, v5}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v2

    .line 1429
    iget-object v5, v7, Ldww;->d:Ljava/lang/Long;

    invoke-static {v5}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v5

    .line 1430
    new-instance v8, Lbir;

    invoke-direct {v8, v2}, Lbir;-><init>(I)V

    invoke-virtual {p2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1431
    iget-object v8, v7, Ldww;->c:Lduf;

    if-eqz v8, :cond_2

    .line 1432
    iget-object v7, v7, Ldww;->c:Lduf;

    .line 1434
    iget-object v8, v7, Lduf;->b:Ldrh;

    if-eqz v8, :cond_2

    .line 1435
    iget-object v0, v7, Lduf;->b:Ldrh;

    .line 1436
    iget-object v0, v0, Ldrh;->b:Ljava/lang/Boolean;

    const/4 v7, 0x0

    invoke-static {v0, v7}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    .line 1439
    :cond_2
    if-eqz v9, :cond_3

    .line 1440
    invoke-virtual {v1}, Lbyc;->a()Lbyc;

    move-result-object v7

    const-string v8, "server_update_field"

    .line 1441
    invoke-virtual {v7, v8}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    move-result-object v7

    const-string v8, "ClientStateUpdateHeader"

    .line 1442
    invoke-virtual {v7, v8}, Lbyc;->b(Ljava/lang/String;)Lbyc;

    move-result-object v7

    .line 1443
    invoke-virtual {v7, v3, v4}, Lbyc;->a(J)Lbyc;

    move-result-object v7

    .line 1444
    invoke-virtual {v7, v2}, Lbyc;->a(I)Lbyc;

    move-result-object v7

    .line 1445
    invoke-virtual {v7, v0}, Lbyc;->a(Z)Lbyc;

    move-result-object v7

    .line 1446
    invoke-virtual {v7, p1}, Lbyc;->a(Lyj;)Lbyc;

    move-result-object v7

    .line 1447
    invoke-virtual {v7}, Lbyc;->b()V

    :cond_3
    move-wide v7, v5

    move v6, v0

    move v0, v2

    .line 1451
    const-string v2, "Babel"

    const/4 v5, 0x3

    invoke-static {v2, v5}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1452
    const-string v2, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v11, "parseServerUpdates: acct="

    invoke-direct {v5, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, ", activeClientState is "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1457
    :cond_4
    const/4 v2, 0x1

    if-eq v0, v2, :cond_1f

    .line 1458
    invoke-static {p1}, Lbpx;->b(Lyj;)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 1459
    const-string v2, "Babel"

    const/4 v5, 0x3

    invoke-static {v2, v5}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1460
    const-string v2, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v10, "Overwrite active client state from server: "

    invoke-direct {v5, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1462
    :cond_5
    const/4 v0, 0x1

    move v5, v0

    .line 1464
    :goto_3
    if-eqz v6, :cond_6

    .line 1465
    const/4 v5, 0x2

    .line 1472
    :cond_6
    iget-object v0, p0, Ldwv;->c:Ldqg;

    if-nez v0, :cond_7

    iget-object v0, p0, Ldwv;->d:Ldrw;

    if-eqz v0, :cond_9

    iget-object v0, p0, Ldwv;->n:Ldqa;

    if-eqz v0, :cond_9

    iget-object v0, p0, Ldwv;->d:Ldrw;

    iget-object v0, v0, Ldrw;->b:Ldrr;

    iget-object v0, v0, Ldrr;->i:Ldtw;

    if-eqz v0, :cond_9

    .line 1475
    :cond_7
    new-instance v0, Lbjc;

    iget-object v2, p0, Ldwv;->n:Ldqa;

    invoke-direct {v0, v2}, Lbjc;-><init>(Ldqa;)V

    .line 1478
    if-eqz v9, :cond_8

    .line 1479
    invoke-virtual {v1}, Lbyc;->a()Lbyc;

    move-result-object v2

    const-string v10, "server_update_field"

    .line 1480
    invoke-virtual {v2, v10}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    move-result-object v2

    const-string v10, "Conversation"

    .line 1481
    invoke-virtual {v2, v10}, Lbyc;->b(Ljava/lang/String;)Lbyc;

    move-result-object v2

    .line 1482
    invoke-virtual {v2, v3, v4}, Lbyc;->a(J)Lbyc;

    move-result-object v2

    .line 1483
    invoke-virtual {v2, v5}, Lbyc;->a(I)Lbyc;

    move-result-object v2

    .line 1484
    invoke-virtual {v2, v6}, Lbyc;->a(Z)Lbyc;

    move-result-object v2

    iget-object v10, v0, Lbjc;->c:Ljava/lang/String;

    .line 1485
    invoke-virtual {v2, v10}, Lbyc;->c(Ljava/lang/String;)Lbyc;

    move-result-object v2

    .line 1486
    invoke-virtual {v2, p1}, Lbyc;->a(Lyj;)Lbyc;

    move-result-object v2

    .line 1487
    invoke-virtual {v2}, Lbyc;->b()V

    .line 1489
    :cond_8
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1491
    :cond_9
    iget-object v0, p0, Ldwv;->d:Ldrw;

    if-eqz v0, :cond_b

    .line 1492
    iget-object v0, p0, Ldwv;->d:Ldrw;

    iget-object v0, v0, Ldrw;->b:Ldrr;

    invoke-static {v0, v5, v7, v8}, Lbjg;->a(Ldrr;IJ)Lbjg;

    move-result-object v0

    .line 1494
    if-eqz v0, :cond_b

    .line 1495
    if-eqz v9, :cond_a

    .line 1496
    invoke-virtual {v1}, Lbyc;->a()Lbyc;

    move-result-object v2

    const-string v7, "server_update_field"

    .line 1497
    invoke-virtual {v2, v7}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    move-result-object v2

    .line 1498
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lbyc;->b(Ljava/lang/String;)Lbyc;

    move-result-object v2

    .line 1499
    invoke-virtual {v2, v3, v4}, Lbyc;->a(J)Lbyc;

    move-result-object v2

    iget-wide v7, v0, Lbjg;->e:J

    .line 1500
    invoke-virtual {v2, v7, v8}, Lbyc;->b(J)Lbyc;

    move-result-object v2

    .line 1501
    invoke-virtual {v2, v5}, Lbyc;->a(I)Lbyc;

    move-result-object v2

    .line 1502
    invoke-virtual {v2, v6}, Lbyc;->a(Z)Lbyc;

    move-result-object v2

    iget v7, v0, Lbjg;->g:I

    .line 1503
    invoke-virtual {v2, v7}, Lbyc;->b(I)Lbyc;

    move-result-object v2

    iget-object v7, v0, Lbjg;->c:Ljava/lang/String;

    .line 1504
    invoke-virtual {v2, v7}, Lbyc;->c(Ljava/lang/String;)Lbyc;

    move-result-object v2

    .line 1505
    invoke-virtual {v2, p1}, Lbyc;->a(Lyj;)Lbyc;

    move-result-object v2

    .line 1506
    invoke-virtual {v2}, Lbyc;->b()V

    .line 1508
    :cond_a
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1511
    :cond_b
    iget-object v0, p0, Ldwv;->e:Ldwf;

    if-eqz v0, :cond_d

    .line 1512
    new-instance v0, Lbjo;

    iget-object v2, p0, Ldwv;->e:Ldwf;

    invoke-direct {v0, v2}, Lbjo;-><init>(Ldwf;)V

    .line 1514
    if-eqz v9, :cond_c

    move-object v2, p1

    .line 1515
    invoke-static/range {v0 .. v5}, Lbiq;->a(Lbis;Lbyc;Lyj;JI)V

    .line 1518
    :cond_c
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1520
    :cond_d
    iget-object v0, p0, Ldwv;->f:Ldwo;

    if-eqz v0, :cond_f

    .line 1521
    new-instance v0, Lbjp;

    iget-object v2, p0, Ldwv;->f:Ldwo;

    invoke-direct {v0, v2}, Lbjp;-><init>(Ldwo;)V

    .line 1523
    if-eqz v9, :cond_e

    move-object v2, p1

    .line 1524
    invoke-static/range {v0 .. v5}, Lbiq;->a(Lbis;Lbyc;Lyj;JI)V

    .line 1527
    :cond_e
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1529
    :cond_f
    iget-object v0, p0, Ldwv;->i:Ldxu;

    if-eqz v0, :cond_11

    .line 1530
    new-instance v0, Lbjr;

    iget-object v2, p0, Ldwv;->i:Ldxu;

    const/4 v7, 0x0

    invoke-direct {v0, v2, v7}, Lbjr;-><init>(Ldxu;B)V

    .line 1532
    if-eqz v9, :cond_10

    move-object v2, p1

    .line 1533
    invoke-static/range {v0 .. v5}, Lbiq;->a(Lbis;Lbyc;Lyj;JI)V

    .line 1536
    :cond_10
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1538
    :cond_11
    iget-object v0, p0, Ldwv;->h:Ldvj;

    if-eqz v0, :cond_12

    .line 1539
    new-instance v0, Lbjl;

    iget-object v2, p0, Ldwv;->h:Ldvj;

    invoke-direct {v0, v2}, Lbjl;-><init>(Ldvj;)V

    .line 1540
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1542
    :cond_12
    iget-object v0, p0, Ldwv;->o:Ldvt;

    if-eqz v0, :cond_14

    .line 1543
    new-instance v0, Lbjn;

    iget-object v2, p0, Ldwv;->o:Ldvt;

    invoke-direct {v0, v2}, Lbjn;-><init>(Ldvt;)V

    .line 1545
    if-eqz v9, :cond_13

    .line 1546
    invoke-virtual {v1}, Lbyc;->a()Lbyc;

    move-result-object v2

    const-string v7, "server_update_field"

    .line 1547
    invoke-virtual {v2, v7}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    move-result-object v2

    const-string v7, "SelfPresenceNotification"

    .line 1548
    invoke-virtual {v2, v7}, Lbyc;->b(Ljava/lang/String;)Lbyc;

    move-result-object v2

    .line 1549
    invoke-virtual {v2, v3, v4}, Lbyc;->a(J)Lbyc;

    move-result-object v2

    .line 1550
    invoke-virtual {v2, v5}, Lbyc;->a(I)Lbyc;

    move-result-object v2

    .line 1551
    invoke-virtual {v2, v6}, Lbyc;->a(Z)Lbyc;

    move-result-object v2

    .line 1552
    invoke-virtual {v2, p1}, Lbyc;->a(Lyj;)Lbyc;

    move-result-object v2

    .line 1553
    invoke-virtual {v2}, Lbyc;->b()V

    .line 1555
    :cond_13
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1557
    :cond_14
    iget-object v0, p0, Ldwv;->j:Ldqm;

    if-eqz v0, :cond_16

    .line 1558
    new-instance v0, Lbjq;

    iget-object v2, p0, Ldwv;->j:Ldqm;

    invoke-direct {v0, v2}, Lbjq;-><init>(Ldqm;)V

    .line 1560
    if-eqz v9, :cond_15

    .line 1561
    invoke-virtual {v1}, Lbyc;->a()Lbyc;

    move-result-object v1

    const-string v2, "server_update_field"

    .line 1562
    invoke-virtual {v1, v2}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    move-result-object v1

    const-string v2, "ViewModificationNotification"

    .line 1563
    invoke-virtual {v1, v2}, Lbyc;->b(Ljava/lang/String;)Lbyc;

    move-result-object v1

    iget-object v2, v0, Lbjq;->b:Ljava/lang/String;

    .line 1564
    invoke-virtual {v1, v2}, Lbyc;->c(Ljava/lang/String;)Lbyc;

    move-result-object v1

    .line 1565
    invoke-virtual {v1, v3, v4}, Lbyc;->a(J)Lbyc;

    move-result-object v1

    .line 1566
    invoke-virtual {v1, v5}, Lbyc;->a(I)Lbyc;

    move-result-object v1

    .line 1567
    invoke-virtual {v1, v6}, Lbyc;->a(Z)Lbyc;

    move-result-object v1

    .line 1568
    invoke-virtual {v1, p1}, Lbyc;->a(Lyj;)Lbyc;

    move-result-object v1

    .line 1569
    invoke-virtual {v1}, Lbyc;->b()V

    .line 1571
    :cond_15
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1573
    :cond_16
    iget-object v0, p0, Ldwv;->g:Ldwc;

    if-eqz v0, :cond_17

    .line 1574
    new-instance v0, Lbjj;

    iget-object v1, p0, Ldwv;->g:Ldwc;

    invoke-direct {v0, v1}, Lbjj;-><init>(Ldwc;)V

    .line 1576
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1578
    :cond_17
    iget-object v0, p0, Ldwv;->l:Ldqt;

    if-eqz v0, :cond_18

    .line 1579
    new-instance v0, Lbje;

    iget-object v1, p0, Ldwv;->l:Ldqt;

    invoke-direct {v0, v1}, Lbje;-><init>(Ldqt;)V

    .line 1581
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1583
    :cond_18
    iget-object v0, p0, Ldwv;->q:Ldpn;

    if-eqz v0, :cond_19

    .line 1584
    new-instance v0, Lbit;

    iget-object v1, p0, Ldwv;->q:Ldpn;

    invoke-direct {v0, v1}, Lbit;-><init>(Ldpn;)V

    .line 1585
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1587
    :cond_19
    iget-object v0, p0, Ldwv;->u:Ldpz;

    if-eqz v0, :cond_1a

    .line 1588
    new-instance v0, Lbjb;

    iget-object v1, p0, Ldwv;->u:Ldpz;

    iget-object v2, p0, Ldwv;->b:Ldww;

    iget-object v2, v2, Ldww;->i:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lbjb;-><init>(Ldpz;Ljava/lang/String;)V

    .line 1590
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1592
    :cond_1a
    iget-object v0, p0, Ldwv;->t:Ldvp;

    if-eqz v0, :cond_1b

    .line 1593
    new-instance v0, Lbjm;

    iget-object v1, p0, Ldwv;->t:Ldvp;

    invoke-direct {v0, v1}, Lbjm;-><init>(Ldvp;)V

    .line 1596
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1598
    :cond_1b
    iget-object v0, p0, Ldwv;->k:Ldrk;

    if-eqz v0, :cond_1c

    .line 1599
    new-instance v0, Lbjf;

    iget-object v1, p0, Ldwv;->k:Ldrk;

    invoke-direct {v0, v1}, Lbjf;-><init>(Ldrk;)V

    .line 1601
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1604
    :cond_1c
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 1605
    const-string v0, "Babel"

    const-string v1, "Unexpected update type from babel server"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1607
    :cond_1d
    return-void

    .line 1419
    :cond_1e
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_1f
    move v5, v0

    goto/16 :goto_3
.end method

.method static synthetic a()Z
    .locals 1

    .prologue
    .line 77
    sget-boolean v0, Lbiq;->a:Z

    return v0
.end method
