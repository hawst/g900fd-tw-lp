.class public final Lett;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lett;


# instance fields
.field public b:[B

.field public c:Ljava/lang/String;

.field public d:[B

.field public e:Ljava/lang/Float;

.field public f:Levt;

.field public g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    new-array v0, v0, [Lett;

    sput-object v0, Lett;->a:[Lett;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Lepn;-><init>()V

    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lett;->f:Levt;

    .line 99
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 141
    const/4 v0, 0x0

    .line 142
    iget-object v1, p0, Lett;->b:[B

    if-eqz v1, :cond_0

    .line 143
    const/4 v0, 0x1

    iget-object v1, p0, Lett;->b:[B

    .line 144
    invoke-static {v0, v1}, Lepl;->b(I[B)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 146
    :cond_0
    iget-object v1, p0, Lett;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 147
    const/4 v1, 0x2

    iget-object v2, p0, Lett;->c:Ljava/lang/String;

    .line 148
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 150
    :cond_1
    iget-object v1, p0, Lett;->d:[B

    if-eqz v1, :cond_2

    .line 151
    const/4 v1, 0x3

    iget-object v2, p0, Lett;->d:[B

    .line 152
    invoke-static {v1, v2}, Lepl;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 154
    :cond_2
    iget-object v1, p0, Lett;->e:Ljava/lang/Float;

    if-eqz v1, :cond_3

    .line 155
    const/4 v1, 0x4

    iget-object v2, p0, Lett;->e:Ljava/lang/Float;

    .line 156
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 158
    :cond_3
    iget-object v1, p0, Lett;->f:Levt;

    if-eqz v1, :cond_4

    .line 159
    const/4 v1, 0x5

    iget-object v2, p0, Lett;->f:Levt;

    .line 160
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 162
    :cond_4
    iget-object v1, p0, Lett;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 163
    const/4 v1, 0x6

    iget-object v2, p0, Lett;->g:Ljava/lang/String;

    .line 164
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 166
    :cond_5
    iget-object v1, p0, Lett;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 167
    iput v0, p0, Lett;->cachedSize:I

    .line 168
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 95
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lett;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lett;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lett;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Lett;->b:[B

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lett;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Lett;->d:[B

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lett;->e:Ljava/lang/Float;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lett;->f:Levt;

    if-nez v0, :cond_2

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Lett;->f:Levt;

    :cond_2
    iget-object v0, p0, Lett;->f:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lett;->g:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x25 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lett;->b:[B

    if-eqz v0, :cond_0

    .line 118
    const/4 v0, 0x1

    iget-object v1, p0, Lett;->b:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 120
    :cond_0
    iget-object v0, p0, Lett;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 121
    const/4 v0, 0x2

    iget-object v1, p0, Lett;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 123
    :cond_1
    iget-object v0, p0, Lett;->d:[B

    if-eqz v0, :cond_2

    .line 124
    const/4 v0, 0x3

    iget-object v1, p0, Lett;->d:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 126
    :cond_2
    iget-object v0, p0, Lett;->e:Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 127
    const/4 v0, 0x4

    iget-object v1, p0, Lett;->e:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 129
    :cond_3
    iget-object v0, p0, Lett;->f:Levt;

    if-eqz v0, :cond_4

    .line 130
    const/4 v0, 0x5

    iget-object v1, p0, Lett;->f:Levt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 132
    :cond_4
    iget-object v0, p0, Lett;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 133
    const/4 v0, 0x6

    iget-object v1, p0, Lett;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 135
    :cond_5
    iget-object v0, p0, Lett;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 137
    return-void
.end method
