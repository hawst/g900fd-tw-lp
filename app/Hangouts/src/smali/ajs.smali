.class public Lajs;
.super Lt;
.source "PG"

# interfaces
.implements Lhz;


# instance fields
.field private Y:Landroid/widget/TextView;

.field private Z:Landroid/view/View;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private aa:Lcom/google/android/apps/hangouts/views/EmojiCategoryPageIndicatorView;

.field private final ab:Landroid/view/View$OnClickListener;

.field private final ac:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private final ad:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private final ae:Ljava/lang/Runnable;

.field private final af:Landroid/os/Handler;

.field private b:Laka;

.field private c:Landroid/support/v4/view/ViewPager;

.field private d:Lakc;

.field private e:Landroid/widget/TabHost;

.field private f:I

.field private g:Landroid/view/View;

.field private h:Landroid/widget/LinearLayout;

.field private i:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lt;-><init>()V

    .line 50
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    iput-object v0, p0, Lajs;->a:Ljava/util/Map;

    .line 66
    const/4 v0, -0x1

    iput v0, p0, Lajs;->f:I

    .line 74
    new-instance v0, Lajt;

    invoke-direct {v0, p0}, Lajt;-><init>(Lajs;)V

    iput-object v0, p0, Lajs;->ab:Landroid/view/View$OnClickListener;

    .line 83
    new-instance v0, Laju;

    invoke-direct {v0, p0}, Laju;-><init>(Lajs;)V

    iput-object v0, p0, Lajs;->ac:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 96
    new-instance v0, Lajv;

    invoke-direct {v0, p0}, Lajv;-><init>(Lajs;)V

    iput-object v0, p0, Lajs;->ad:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 105
    new-instance v0, Lajw;

    invoke-direct {v0, p0}, Lajw;-><init>(Lajs;)V

    iput-object v0, p0, Lajs;->ae:Ljava/lang/Runnable;

    .line 117
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lajs;->af:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lajs;)Laka;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lajs;->b:Laka;

    return-object v0
.end method

.method private a()Landroid/graphics/Point;
    .locals 4

    .prologue
    .line 163
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 164
    invoke-virtual {p0}, Lajs;->getActivity()Ly;

    move-result-object v1

    invoke-virtual {v1}, Ly;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 165
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xd

    if-lt v2, v3, :cond_0

    .line 166
    invoke-virtual {v1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 171
    :goto_0
    return-object v0

    .line 168
    :cond_0
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v2

    iput v2, v0, Landroid/graphics/Point;->x:I

    .line 169
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v1

    iput v1, v0, Landroid/graphics/Point;->y:I

    goto :goto_0
.end method

.method private a(IZ)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 315
    iget v0, p0, Lajs;->f:I

    if-ne v0, p1, :cond_1

    if-nez p2, :cond_1

    .line 332
    :cond_0
    :goto_0
    return-void

    .line 319
    :cond_1
    iput p1, p0, Lajs;->f:I

    .line 321
    iget-object v0, p0, Lajs;->d:Lakc;

    invoke-virtual {v0, p1}, Lakc;->c(I)I

    move-result v1

    .line 322
    iget-object v0, p0, Lajs;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->c()I

    move-result v0

    .line 323
    iget-object v2, p0, Lajs;->d:Lakc;

    .line 324
    invoke-virtual {v2, v0}, Lakc;->b(I)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    .line 323
    invoke-static {v0, v5}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    .line 325
    if-nez p2, :cond_2

    if-eq v0, p1, :cond_3

    .line 326
    :cond_2
    iget-object v0, p0, Lajs;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1, v5}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 328
    :cond_3
    if-nez p2, :cond_4

    iget-object v0, p0, Lajs;->e:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v0

    if-eq v0, p1, :cond_5

    .line 329
    :cond_4
    iget-object v0, p0, Lajs;->e:Landroid/widget/TabHost;

    invoke-virtual {v0, p1}, Landroid/widget/TabHost;->setCurrentTab(I)V

    .line 331
    :cond_5
    iget v0, p0, Lajs;->f:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-virtual {p0}, Lajs;->getActivity()Ly;

    move-result-object v3

    const-string v4, "recentEmoji"

    invoke-virtual {v3, v4, v5}, Ly;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "lastCategoryKey"

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    const-string v3, "Babel"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    const-string v5, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Emoji: Fragment write category "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " @"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " took: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sub-long v1, v3, v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static synthetic a(Lajs;IZ)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lajs;->a(IZ)V

    return-void
.end method

.method private a(Landroid/widget/TabHost;Ljava/lang/String;III)V
    .locals 6

    .prologue
    .line 181
    invoke-virtual {p1, p2}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    .line 182
    invoke-virtual {v0, p3}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    .line 183
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/widget/TabHost;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 184
    invoke-virtual {v1, p4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 185
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, 0x0

    const/4 v4, -0x2

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v2, v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 186
    invoke-virtual {p0}, Lajs;->getActivity()Ly;

    move-result-object v2

    invoke-virtual {v2}, Ly;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lf;->cX:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 188
    invoke-virtual {v1, v2, v2, v2, v2}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 189
    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;

    .line 190
    invoke-virtual {p1, v0}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 191
    iget-object v0, p0, Lajs;->a:Ljava/util/Map;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    return-void
.end method

.method static synthetic b(Lajs;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lajs;->c:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 289
    iget-object v0, p0, Lajs;->h:Landroid/widget/LinearLayout;

    .line 290
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 291
    invoke-virtual {p0}, Lajs;->getActivity()Ly;

    move-result-object v1

    invoke-virtual {v1}, Ly;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->cU:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 293
    int-to-float v1, v1

    const v2, 0x3e19999a    # 0.15f

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 294
    iget-object v1, p0, Lajs;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 295
    return-void
.end method

.method static synthetic c(Lajs;)Lakc;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lajs;->d:Lakc;

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 351
    iget-object v0, p0, Lajs;->aa:Lcom/google/android/apps/hangouts/views/EmojiCategoryPageIndicatorView;

    if-nez v0, :cond_0

    .line 362
    :goto_0
    return-void

    .line 354
    :cond_0
    iget-object v0, p0, Lajs;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->c()I

    move-result v0

    .line 355
    iget-object v1, p0, Lajs;->d:Lakc;

    .line 356
    invoke-virtual {v1, v0}, Lakc;->b(I)Landroid/util/Pair;

    move-result-object v1

    .line 357
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-static {v0, v4}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v2

    .line 358
    iget-object v3, p0, Lajs;->d:Lakc;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    .line 359
    invoke-static {v0, v4}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    invoke-virtual {v3, v0}, Lakc;->a(I)I

    move-result v0

    .line 360
    iget-object v1, p0, Lajs;->aa:Lcom/google/android/apps/hangouts/views/EmojiCategoryPageIndicatorView;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/apps/hangouts/views/EmojiCategoryPageIndicatorView;->a(IIF)V

    goto :goto_0
.end method

.method static synthetic d(Lajs;)Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lajs;->a()Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lajs;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lajs;->f:I

    return v0
.end method

.method static synthetic f(Lajs;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 42
    new-instance v0, Lakc;

    invoke-virtual {p0}, Lajs;->getActivity()Ly;

    move-result-object v1

    iget-object v2, p0, Lajs;->b:Laka;

    invoke-direct {p0}, Lajs;->a()Landroid/graphics/Point;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lajs;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getHeight()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lakc;-><init>(Landroid/content/Context;Laka;II)V

    iput-object v0, p0, Lajs;->d:Lakc;

    iget-object v0, p0, Lajs;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->a(Lhz;)V

    iget-object v0, p0, Lajs;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->e()V

    iget-object v0, p0, Lajs;->c:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lajs;->d:Lakc;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Lgq;)V

    iget-object v0, p0, Lajs;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->postInvalidate()V

    invoke-virtual {p0}, Lajs;->getActivity()Ly;

    move-result-object v0

    const-string v1, "recentEmoji"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ly;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "lastCategoryKey"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-direct {p0, v0, v5}, Lajs;->a(IZ)V

    return-void
.end method

.method static synthetic g(Lajs;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lajs;->ae:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic h(Lajs;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lajs;->af:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic i(Lajs;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lajs;->a:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic j(Lajs;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lajs;->c()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 394
    iget-object v0, p0, Lajs;->d:Lakc;

    .line 395
    invoke-virtual {v0, p1}, Lakc;->b(I)Landroid/util/Pair;

    move-result-object v0

    .line 396
    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    invoke-direct {p0, v0, v1}, Lajs;->a(IZ)V

    .line 398
    invoke-direct {p0}, Lajs;->c()V

    .line 399
    return-void
.end method

.method public a(IF)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 367
    iget-object v0, p0, Lajs;->d:Lakc;

    .line 368
    invoke-virtual {v0, p1}, Lakc;->b(I)Landroid/util/Pair;

    move-result-object v1

    .line 369
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-static {v0, v7}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v2

    .line 370
    iget-object v0, p0, Lajs;->d:Lakc;

    .line 371
    invoke-virtual {v0, v2}, Lakc;->a(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 370
    invoke-static {v0, v7}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v3

    .line 373
    iget-object v0, p0, Lajs;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->c()I

    move-result v0

    .line 374
    iget-object v4, p0, Lajs;->d:Lakc;

    .line 375
    invoke-virtual {v4, v0}, Lakc;->b(I)Landroid/util/Pair;

    move-result-object v4

    .line 376
    iget-object v0, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-static {v0, v7}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v5

    .line 377
    iget-object v6, p0, Lajs;->d:Lakc;

    iget-object v0, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    .line 378
    invoke-static {v0, v7}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    invoke-virtual {v6, v0}, Lakc;->a(I)I

    move-result v0

    .line 380
    iget v4, p0, Lajs;->f:I

    if-ne v2, v4, :cond_1

    .line 381
    iget-object v2, p0, Lajs;->aa:Lcom/google/android/apps/hangouts/views/EmojiCategoryPageIndicatorView;

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    .line 382
    invoke-static {v0, v7}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    .line 381
    invoke-virtual {v2, v3, v0, p2}, Lcom/google/android/apps/hangouts/views/EmojiCategoryPageIndicatorView;->a(IIF)V

    .line 390
    :cond_0
    :goto_0
    return-void

    .line 383
    :cond_1
    iget v1, p0, Lajs;->f:I

    if-le v2, v1, :cond_2

    .line 384
    iget-object v1, p0, Lajs;->aa:Lcom/google/android/apps/hangouts/views/EmojiCategoryPageIndicatorView;

    invoke-virtual {v1, v0, v5, p2}, Lcom/google/android/apps/hangouts/views/EmojiCategoryPageIndicatorView;->a(IIF)V

    goto :goto_0

    .line 386
    :cond_2
    iget v1, p0, Lajs;->f:I

    if-ge v2, v1, :cond_0

    .line 387
    iget-object v1, p0, Lajs;->aa:Lcom/google/android/apps/hangouts/views/EmojiCategoryPageIndicatorView;

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, p2, v2

    invoke-virtual {v1, v0, v5, v2}, Lcom/google/android/apps/hangouts/views/EmojiCategoryPageIndicatorView;->a(IIF)V

    goto :goto_0
.end method

.method public a(Laka;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lajs;->b:Laka;

    .line 129
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    .line 134
    invoke-virtual {p0}, Lajs;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->cU:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 136
    invoke-virtual {p0}, Lajs;->getView()Landroid/view/View;

    move-result-object v1

    .line 137
    if-eqz v1, :cond_0

    .line 138
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v2, v3, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 140
    :cond_0
    iget-object v0, p0, Lajs;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lajs;->ac:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 141
    iget-object v0, p0, Lajs;->h:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 142
    invoke-direct {p0}, Lajs;->b()V

    .line 144
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 121
    invoke-super {p0, p1}, Lt;->onCreate(Landroid/os/Bundle;)V

    .line 122
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Emoji: Fragment onCreate @"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v5, 0x0

    .line 200
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 202
    sget v0, Lf;->fe:I

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    .line 204
    sget v0, Lg;->bL:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TabHost;

    iput-object v0, p0, Lajs;->e:Landroid/widget/TabHost;

    .line 205
    iget-object v0, p0, Lajs;->e:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->setup()V

    .line 207
    iget-object v1, p0, Lajs;->e:Landroid/widget/TabHost;

    const-string v2, "Recent"

    sget v3, Lg;->gd:I

    sget v4, Lcom/google/android/apps/hangouts/R$drawable;->bq:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lajs;->a(Landroid/widget/TabHost;Ljava/lang/String;III)V

    .line 209
    iget-object v1, p0, Lajs;->e:Landroid/widget/TabHost;

    const-string v2, "Faces"

    sget v3, Lg;->bV:I

    sget v4, Lcom/google/android/apps/hangouts/R$drawable;->bo:I

    move-object v0, p0

    move v5, v9

    invoke-direct/range {v0 .. v5}, Lajs;->a(Landroid/widget/TabHost;Ljava/lang/String;III)V

    .line 211
    iget-object v1, p0, Lajs;->e:Landroid/widget/TabHost;

    const-string v2, "Objects"

    sget v3, Lg;->eP:I

    sget v4, Lcom/google/android/apps/hangouts/R$drawable;->bn:I

    const/4 v5, 0x2

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lajs;->a(Landroid/widget/TabHost;Ljava/lang/String;III)V

    .line 213
    iget-object v1, p0, Lajs;->e:Landroid/widget/TabHost;

    const-string v2, "Nature"

    sget v3, Lg;->eI:I

    sget v4, Lcom/google/android/apps/hangouts/R$drawable;->bm:I

    move-object v0, p0

    move v5, v10

    invoke-direct/range {v0 .. v5}, Lajs;->a(Landroid/widget/TabHost;Ljava/lang/String;III)V

    .line 215
    iget-object v1, p0, Lajs;->e:Landroid/widget/TabHost;

    const-string v2, "Places"

    sget v3, Lg;->fq:I

    sget v4, Lcom/google/android/apps/hangouts/R$drawable;->bp:I

    const/4 v5, 0x4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lajs;->a(Landroid/widget/TabHost;Ljava/lang/String;III)V

    .line 217
    iget-object v1, p0, Lajs;->e:Landroid/widget/TabHost;

    const-string v2, "Symbols"

    sget v3, Lg;->hk:I

    sget v4, Lcom/google/android/apps/hangouts/R$drawable;->br:I

    const/4 v5, 0x5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lajs;->a(Landroid/widget/TabHost;Ljava/lang/String;III)V

    .line 220
    iget-object v0, p0, Lajs;->e:Landroid/widget/TabHost;

    new-instance v1, Lajx;

    invoke-direct {v1, p0}, Lajx;-><init>(Lajs;)V

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 229
    iget-object v0, p0, Lajs;->e:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/TabWidget;->setStripEnabled(Z)V

    .line 231
    sget v0, Lg;->bR:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lajs;->c:Landroid/support/v4/view/ViewPager;

    .line 232
    iget-object v0, p0, Lajs;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lajs;->ad:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 234
    sget v0, Lg;->bK:I

    .line 235
    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/EmojiCategoryPageIndicatorView;

    iput-object v0, p0, Lajs;->aa:Lcom/google/android/apps/hangouts/views/EmojiCategoryPageIndicatorView;

    .line 236
    invoke-virtual {p0}, Lajs;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 237
    iget-object v0, p0, Lajs;->aa:Lcom/google/android/apps/hangouts/views/EmojiCategoryPageIndicatorView;

    .line 238
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/EmojiCategoryPageIndicatorView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 239
    sget v2, Lf;->cW:I

    .line 240
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 241
    iget-object v1, p0, Lajs;->aa:Lcom/google/android/apps/hangouts/views/EmojiCategoryPageIndicatorView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/hangouts/views/EmojiCategoryPageIndicatorView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 243
    sget v0, Lg;->bP:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lajs;->g:Landroid/view/View;

    .line 244
    iget-object v0, p0, Lajs;->g:Landroid/view/View;

    new-instance v1, Lajy;

    invoke-direct {v1, p0}, Lajy;-><init>(Lajs;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 263
    sget v0, Lg;->bJ:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lajs;->h:Landroid/widget/LinearLayout;

    .line 264
    invoke-direct {p0}, Lajs;->b()V

    .line 265
    sget v0, Lg;->bN:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lajs;->i:Landroid/widget/TextView;

    .line 266
    iget-object v0, p0, Lajs;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lajs;->ab:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 267
    sget v0, Lg;->bO:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lajs;->Y:Landroid/widget/TextView;

    .line 268
    iget-object v0, p0, Lajs;->Y:Landroid/widget/TextView;

    iget-object v1, p0, Lajs;->ab:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 269
    sget v0, Lg;->bQ:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lajs;->Z:Landroid/view/View;

    .line 270
    iget-object v0, p0, Lajs;->Z:Landroid/view/View;

    new-instance v1, Lajz;

    invoke-direct {v1, p0}, Lajz;-><init>(Lajs;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 279
    const-string v0, "Babel"

    invoke-static {v0, v10}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 281
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Emoji: Fragment onCreateView @"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " took: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long/2addr v0, v6

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    :cond_0
    return-object v8
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 156
    invoke-super {p0}, Lt;->onDestroyView()V

    .line 157
    iget-object v0, p0, Lajs;->c:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lajs;->ac:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-static {v0, v1}, Lcxg;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 158
    iget-object v0, p0, Lajs;->c:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lajs;->ad:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-static {v0, v1}, Lcxg;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 159
    return-void
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 148
    invoke-super {p0}, Lt;->onLowMemory()V

    .line 149
    iget-object v0, p0, Lajs;->d:Lakc;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lajs;->d:Lakc;

    invoke-virtual {v0}, Lakc;->e()V

    .line 152
    :cond_0
    return-void
.end method
