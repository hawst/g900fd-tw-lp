.class final Laet;
.super Landroid/widget/ArrayAdapter;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Laee;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Laer;

.field private b:Landroid/content/Context;

.field private c:Lyj;


# direct methods
.method public constructor <init>(Laer;Landroid/content/Context;Ljava/util/List;Lyj;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Laee;",
            ">;",
            "Lyj;",
            ")V"
        }
    .end annotation

    .prologue
    .line 135
    iput-object p1, p0, Laet;->a:Laer;

    .line 136
    sget v0, Lf;->ey:I

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 137
    iput-object p2, p0, Laet;->b:Landroid/content/Context;

    .line 138
    iput-object p4, p0, Laet;->c:Lyj;

    .line 139
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 143
    if-eqz p2, :cond_0

    instance-of v0, p2, Lceb;

    if-nez v0, :cond_1

    .line 145
    :cond_0
    iget-object v0, p0, Laet;->b:Landroid/content/Context;

    iget-object v1, p0, Laet;->a:Laer;

    .line 146
    invoke-virtual {v1}, Laer;->getTargetFragment()Lt;

    move-result-object v1

    const/4 v2, 0x1

    .line 145
    invoke-static {v0, v1, v3, v2}, Lceb;->createInstance(Landroid/content/Context;Lt;ZI)Lceb;

    move-result-object v0

    .line 151
    :goto_0
    invoke-virtual {p0, p1}, Laet;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Laee;

    .line 152
    invoke-virtual {v7}, Laee;->c()Lbdh;

    move-result-object v9

    .line 153
    if-nez v9, :cond_2

    .line 154
    const-string v0, "Babel"

    const-string v1, "Matching entity for contact detail item should not be null"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v8

    .line 180
    :goto_1
    return-object v0

    .line 148
    :cond_1
    check-cast p2, Lceb;

    move-object v0, p2

    goto :goto_0

    .line 158
    :cond_2
    invoke-virtual {v0}, Lceb;->reset()V

    .line 159
    invoke-static {v9}, Lbcx;->a(Lbdh;)Lbcx;

    move-result-object v1

    .line 160
    iget-object v2, v9, Lbdh;->h:Ljava/lang/String;

    iget-object v4, p0, Laet;->c:Lyj;

    move v5, v3

    move v6, v3

    invoke-virtual/range {v0 .. v6}, Lceb;->setInviteeId(Lbcx;Ljava/lang/String;ZLyj;ZZ)V

    .line 162
    iget-object v1, v9, Lbdh;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 163
    iget-object v1, v9, Lbdh;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lceb;->setContactName(Ljava/lang/String;)V

    .line 171
    :cond_3
    :goto_2
    invoke-virtual {v7}, Laee;->a()Ljava/lang/String;

    move-result-object v1

    .line 170
    invoke-virtual {v0, v8, v1}, Lceb;->setDetails(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    new-instance v1, Laeu;

    invoke-direct {v1, p0, v7}, Laeu;-><init>(Laet;Laee;)V

    invoke-virtual {v0, v1}, Lceb;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 164
    :cond_4
    iget-object v1, v9, Lbdh;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 165
    iget-object v1, v9, Lbdh;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lceb;->setContactName(Ljava/lang/String;)V

    goto :goto_2

    .line 166
    :cond_5
    iget-object v1, v9, Lbdh;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 167
    iget-object v1, v9, Lbdh;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lceb;->setContactName(Ljava/lang/String;)V

    goto :goto_2
.end method
