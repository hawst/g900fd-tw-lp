.class public final Lazf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/TextView;

.field final synthetic b:Landroid/widget/TextView;

.field final synthetic c:Lcom/google/android/apps/hangouts/phone/DebugActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/phone/DebugActivity;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lazf;->c:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    iput-object p2, p0, Lazf;->a:Landroid/widget/TextView;

    iput-object p3, p0, Lazf;->b:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/high16 v2, 0x40a00000    # 5.0f

    const/high16 v1, 0x3f800000    # 1.0f

    .line 142
    iget-object v0, p0, Lazf;->a:Landroid/widget/TextView;

    if-ne p1, v0, :cond_1

    .line 143
    iget-object v0, p0, Lazf;->c:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->c(Lcom/google/android/apps/hangouts/phone/DebugActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 145
    iget-object v0, p0, Lazf;->c:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->d(Lcom/google/android/apps/hangouts/phone/DebugActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 153
    :cond_0
    :goto_0
    iget-object v0, p0, Lazf;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->requestLayout()V

    .line 154
    return-void

    .line 147
    :cond_1
    iget-object v0, p0, Lazf;->b:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    .line 148
    iget-object v0, p0, Lazf;->c:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->c(Lcom/google/android/apps/hangouts/phone/DebugActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 150
    iget-object v0, p0, Lazf;->c:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->d(Lcom/google/android/apps/hangouts/phone/DebugActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    goto :goto_0
.end method
