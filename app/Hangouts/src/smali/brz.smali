.class public final Lbrz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcfv;
.implements Lcfw;


# static fields
.field private static final b:Z

.field private static volatile c:Lbrz;


# instance fields
.field a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lbsv;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/Object;

.field private e:I

.field private f:Lcvi;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lbys;->l:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbrz;->b:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbrz;->d:Ljava/lang/Object;

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lbrz;->e:I

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lbrz;->a:Ljava/util/Set;

    .line 80
    const-string v0, "Babel_medialoader"

    const-string v1, "GmsAvatarLoader Created."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    return-void
.end method

.method public static a()Lbrz;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lbrz;->c:Lbrz;

    if-nez v0, :cond_0

    .line 55
    new-instance v0, Lbrz;

    invoke-direct {v0}, Lbrz;-><init>()V

    sput-object v0, Lbrz;->c:Lbrz;

    .line 58
    :cond_0
    sget-object v0, Lbrz;->c:Lbrz;

    return-object v0
.end method

.method public static a(Ljava/lang/Long;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "gmscontact:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbrz;Lcft;Landroid/os/ParcelFileDescriptor;Lbsv;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-virtual {p1}, Lcft;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_2

    :cond_0
    sget-boolean v0, Lbrz;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "Babel_medialoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GmsAvatarLoader: Avatar loaded: status="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  pfd="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0, p3, v1}, Lbrz;->a(Lbsv;[B)V

    :goto_0
    return-void

    :cond_2
    new-instance v2, Ljava/io/FileInputStream;

    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    :try_start_0
    invoke-virtual {v2}, Ljava/io/FileInputStream;->available()I

    move-result v0

    new-array v0, v0, [B

    invoke-virtual {v2, v0}, Ljava/io/FileInputStream;->read([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_1
    invoke-direct {p0, p3, v0}, Lbrz;->a(Lbsv;[B)V

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "Babel_medialoader"

    const-string v3, "Error closing avatar file for contact."

    invoke-static {v2, v3, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_2
    const-string v3, "Babel_medialoader"

    const-string v4, "Error reading avatar file for contact."

    invoke-static {v3, v4, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    move-object v0, v1

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v2, "Babel_medialoader"

    const-string v3, "Error closing avatar file for contact."

    invoke-static {v2, v3, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :goto_2
    throw v0

    :catch_3
    move-exception v1

    const-string v2, "Babel_medialoader"

    const-string v3, "Error closing avatar file for contact."

    invoke-static {v2, v3, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method private a(Lbsv;[B)V
    .locals 3

    .prologue
    .line 269
    if-eqz p2, :cond_2

    .line 270
    new-instance v0, Lbsm;

    const-string v1, "image/jpeg"

    const/4 v2, 0x0

    invoke-direct {v0, p2, v1, v2}, Lbsm;-><init>([BLjava/lang/String;Z)V

    invoke-static {p1, v0}, Lbsn;->a(Lbsv;Lbsm;)V

    .line 275
    :goto_0
    iget-object v1, p0, Lbrz;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lbrz;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbrz;->e:I

    iget v0, p0, Lbrz;->e:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lbrz;->f:Lcvi;

    if-nez v0, :cond_3

    const-string v0, "Babel_medialoader"

    const-string v2, "GmsAvatarLoader trying to disconnect but is already null."

    invoke-static {v0, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lbrz;->f:Lcvi;

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 273
    :cond_2
    invoke-static {p1}, Lbsn;->b(Lbsv;)V

    goto :goto_0

    .line 275
    :cond_3
    :try_start_1
    iget-object v0, p0, Lbrz;->f:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lbrz;->f:Lcvi;

    invoke-virtual {v0}, Lcvi;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    iget-object v0, p0, Lbrz;->f:Lcvi;

    invoke-virtual {v0}, Lcvi;->b()V

    sget-boolean v0, Lbrz;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "Babel_medialoader"

    const-string v2, "GmsAvatarLoader disconnect. disconnecting people client."

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 72
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    const/4 v0, 0x0

    .line 76
    :goto_0
    return v0

    :cond_0
    const-string v0, "gmscontact:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private b(Lbsv;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 198
    invoke-virtual {p1}, Lbsv;->m()Lbyu;

    move-result-object v0

    invoke-virtual {v0}, Lbyu;->l()Ljava/lang/String;

    move-result-object v0

    .line 199
    invoke-static {v0}, Lbrz;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 200
    const-string v0, "Babel_medialoader"

    const-string v2, "GmsAvatarLoader: Not a valid gms url"

    invoke-static {v0, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    invoke-direct {p0, p1, v1}, Lbrz;->a(Lbsv;[B)V

    .line 235
    :goto_0
    return-void

    .line 205
    :cond_0
    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 206
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 207
    const-string v0, "Babel_medialoader"

    const-string v2, "GmsAvatarLoader: Empty contact id in gms url"

    invoke-static {v0, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    invoke-direct {p0, p1, v1}, Lbrz;->a(Lbsv;[B)V

    goto :goto_0

    .line 212
    :cond_1
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 219
    :goto_1
    if-nez v0, :cond_2

    .line 220
    const-string v0, "Babel_medialoader"

    const-string v2, "GmsAvatarLoader: Invalid contact id."

    invoke-static {v0, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    invoke-direct {p0, p1, v1}, Lbrz;->a(Lbsv;[B)V

    goto :goto_0

    .line 216
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_1

    .line 225
    :cond_2
    iget-object v1, p0, Lbrz;->f:Lcvi;

    new-instance v2, Lbsa;

    invoke-direct {v2, p0, p1}, Lbsa;-><init>(Lbrz;Lbsv;)V

    .line 233
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v3

    .line 225
    invoke-virtual {v1, v2, v3, v4}, Lcvi;->a(Lcvq;J)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lbsv;)V
    .locals 3

    .prologue
    .line 180
    invoke-static {}, Lcwz;->b()V

    .line 181
    iget-object v1, p0, Lbrz;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 182
    :try_start_0
    iget v0, p0, Lbrz;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbrz;->e:I

    iget-object v0, p0, Lbrz;->f:Lcvi;

    if-nez v0, :cond_0

    new-instance v0, Lcvi;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, p0, p0}, Lcvi;-><init>(Landroid/content/Context;Lcfv;Lcfw;)V

    iput-object v0, p0, Lbrz;->f:Lcvi;

    :cond_0
    iget-object v0, p0, Lbrz;->f:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lbrz;->f:Lcvi;

    invoke-virtual {v0}, Lcvi;->e()Z

    move-result v0

    if-nez v0, :cond_2

    sget-boolean v0, Lbrz;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "Babel_medialoader"

    const-string v2, "GmsAvatarLoader connectLocked. connecting peopleclient"

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lbrz;->f:Lcvi;

    invoke-virtual {v0}, Lcvi;->a()V

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_4

    .line 183
    invoke-direct {p0, p1}, Lbrz;->b(Lbsv;)V

    .line 191
    :goto_1
    monitor-exit v1

    return-void

    .line 182
    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    .line 185
    :cond_4
    const-string v0, "Babel_medialoader"

    const-string v2, "GmsAvatarLoader load. Not connected. Adding to queue."

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lbrz;->a:Ljava/util/Set;

    if-nez v0, :cond_5

    .line 187
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbrz;->a:Ljava/util/Set;

    .line 189
    :cond_5
    iget-object v0, p0, Lbrz;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 191
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onConnected(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 137
    iget-object v1, p0, Lbrz;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 138
    :try_start_0
    sget-boolean v0, Lbrz;->b:Z

    if-eqz v0, :cond_0

    .line 139
    const-string v2, "Babel_medialoader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "GmsAvatarLoader: People client onConnected. waiting = "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lbrz;->a:Ljava/util/Set;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 140
    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " numClient="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lbrz;->e:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 139
    invoke-static {v2, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :cond_0
    iget-object v0, p0, Lbrz;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsv;

    .line 145
    invoke-direct {p0, v0}, Lbrz;->b(Lbsv;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 149
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 139
    :cond_1
    :try_start_1
    iget-object v0, p0, Lbrz;->a:Ljava/util/Set;

    .line 140
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    goto :goto_0

    .line 147
    :cond_2
    iget-object v0, p0, Lbrz;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 148
    const/4 v0, 0x0

    iput-object v0, p0, Lbrz;->a:Ljava/util/Set;

    .line 149
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public onConnectionFailed(Lcft;)V
    .locals 4

    .prologue
    .line 164
    iget-object v1, p0, Lbrz;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 165
    :try_start_0
    const-string v0, "Babel_medialoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GmsAvatarLoader: People client connection failure: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    iget-object v0, p0, Lbrz;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsv;

    .line 168
    const/4 v3, 0x0

    invoke-direct {p0, v0, v3}, Lbrz;->a(Lbsv;[B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 170
    :cond_0
    :try_start_1
    iget-object v0, p0, Lbrz;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 171
    const/4 v0, 0x0

    iput-object v0, p0, Lbrz;->a:Ljava/util/Set;

    .line 172
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public onDisconnected()V
    .locals 0

    .prologue
    .line 157
    return-void
.end method
