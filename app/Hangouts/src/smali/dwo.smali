.class public final Ldwo;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldwo;


# instance fields
.field public b:Ldqf;

.field public c:Ldui;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8947
    const/4 v0, 0x0

    new-array v0, v0, [Ldwo;

    sput-object v0, Ldwo;->a:[Ldwo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8948
    invoke-direct {p0}, Lepn;-><init>()V

    .line 8951
    iput-object v0, p0, Ldwo;->b:Ldqf;

    .line 8954
    iput-object v0, p0, Ldwo;->c:Ldui;

    .line 8959
    iput-object v0, p0, Ldwo;->e:Ljava/lang/Integer;

    .line 8948
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 8982
    const/4 v0, 0x0

    .line 8983
    iget-object v1, p0, Ldwo;->b:Ldqf;

    if-eqz v1, :cond_0

    .line 8984
    const/4 v0, 0x1

    iget-object v1, p0, Ldwo;->b:Ldqf;

    .line 8985
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8987
    :cond_0
    iget-object v1, p0, Ldwo;->c:Ldui;

    if-eqz v1, :cond_1

    .line 8988
    const/4 v1, 0x2

    iget-object v2, p0, Ldwo;->c:Ldui;

    .line 8989
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8991
    :cond_1
    iget-object v1, p0, Ldwo;->d:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 8992
    const/4 v1, 0x3

    iget-object v2, p0, Ldwo;->d:Ljava/lang/Long;

    .line 8993
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 8995
    :cond_2
    iget-object v1, p0, Ldwo;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 8996
    const/4 v1, 0x4

    iget-object v2, p0, Ldwo;->e:Ljava/lang/Integer;

    .line 8997
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8999
    :cond_3
    iget-object v1, p0, Ldwo;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9000
    iput v0, p0, Ldwo;->cachedSize:I

    .line 9001
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 8944
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldwo;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldwo;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldwo;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldwo;->b:Ldqf;

    if-nez v0, :cond_2

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldwo;->b:Ldqf;

    :cond_2
    iget-object v0, p0, Ldwo;->b:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldwo;->c:Ldui;

    if-nez v0, :cond_3

    new-instance v0, Ldui;

    invoke-direct {v0}, Ldui;-><init>()V

    iput-object v0, p0, Ldwo;->c:Ldui;

    :cond_3
    iget-object v0, p0, Ldwo;->c:Ldui;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldwo;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwo;->e:Ljava/lang/Integer;

    goto :goto_0

    :cond_5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwo;->e:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 8964
    iget-object v0, p0, Ldwo;->b:Ldqf;

    if-eqz v0, :cond_0

    .line 8965
    const/4 v0, 0x1

    iget-object v1, p0, Ldwo;->b:Ldqf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 8967
    :cond_0
    iget-object v0, p0, Ldwo;->c:Ldui;

    if-eqz v0, :cond_1

    .line 8968
    const/4 v0, 0x2

    iget-object v1, p0, Ldwo;->c:Ldui;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 8970
    :cond_1
    iget-object v0, p0, Ldwo;->d:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 8971
    const/4 v0, 0x3

    iget-object v1, p0, Ldwo;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 8973
    :cond_2
    iget-object v0, p0, Ldwo;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 8974
    const/4 v0, 0x4

    iget-object v1, p0, Ldwo;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 8976
    :cond_3
    iget-object v0, p0, Ldwo;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 8978
    return-void
.end method
