.class public abstract Lakb;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field private final a:Lbyh;

.field private final b:Landroid/content/res/Resources;

.field private final c:Landroid/widget/AbsListView$LayoutParams;

.field private d:Lbyl;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lbyh;)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 33
    iput-object p1, p0, Lakb;->b:Landroid/content/res/Resources;

    .line 34
    iput-object p2, p0, Lakb;->a:Lbyh;

    .line 35
    sget v0, Lf;->cV:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 36
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v1, v0, v0}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    iput-object v1, p0, Lakb;->c:Landroid/widget/AbsListView$LayoutParams;

    .line 37
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lakb;->d:Lbyl;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lakb;->d:Lbyl;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbyl;->cancel(Z)Z

    .line 63
    :cond_0
    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 41
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lakb;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 42
    invoke-virtual {p0, p1}, Lakb;->getItemId(I)J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 45
    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0x2002

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 71
    if-nez p2, :cond_0

    .line 72
    new-instance p2, Landroid/widget/ImageView;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 73
    iget-object v0, p0, Lakb;->c:Landroid/widget/AbsListView$LayoutParams;

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 74
    invoke-virtual {p2, v2, v2, v2, v2}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 80
    :goto_0
    iget-object v0, p0, Lakb;->d:Lbyl;

    if-nez v0, :cond_3

    .line 81
    monitor-enter p0

    .line 82
    :try_start_0
    iget-object v0, p0, Lakb;->d:Lbyl;

    if-nez v0, :cond_2

    .line 83
    invoke-virtual {p0}, Lakb;->getCount()I

    move-result v0

    new-array v3, v0, [I

    move v1, v2

    .line 84
    :goto_1
    array-length v0, v3

    if-ge v1, v0, :cond_1

    .line 85
    invoke-virtual {p0, v1}, Lakb;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v4, 0x0

    invoke-static {v0, v4}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    aput v0, v3, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 76
    :cond_0
    check-cast p2, Landroid/widget/ImageView;

    .line 77
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 87
    :cond_1
    :try_start_1
    iget-object v0, p0, Lakb;->a:Lbyh;

    invoke-virtual {v0, v3}, Lbyh;->a([I)Lbyl;

    move-result-object v0

    iput-object v0, p0, Lakb;->d:Lbyl;

    .line 89
    :cond_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91
    :cond_3
    invoke-virtual {p0, p1}, Lakb;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-static {v0, v2}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    .line 92
    iget-object v1, p0, Lakb;->a:Lbyh;

    iget-object v2, p0, Lakb;->b:Landroid/content/res/Resources;

    iget-object v3, p0, Lakb;->d:Lbyl;

    invoke-virtual {v1, v2, v0, v3, p2}, Lbyh;->a(Landroid/content/res/Resources;ILbyl;Landroid/widget/ImageView;)V

    .line 94
    return-object p2

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lakb;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
