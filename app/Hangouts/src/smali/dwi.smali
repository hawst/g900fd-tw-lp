.class public final Ldwi;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldwi;


# instance fields
.field public b:Ldvm;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22234
    const/4 v0, 0x0

    new-array v0, v0, [Ldwi;

    sput-object v0, Ldwi;->a:[Ldwi;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22235
    invoke-direct {p0}, Lepn;-><init>()V

    .line 22238
    iput-object v0, p0, Ldwi;->b:Ldvm;

    .line 22243
    iput-object v0, p0, Ldwi;->d:Ljava/lang/Integer;

    .line 22235
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 22263
    const/4 v0, 0x0

    .line 22264
    iget-object v1, p0, Ldwi;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 22265
    const/4 v0, 0x1

    iget-object v1, p0, Ldwi;->b:Ldvm;

    .line 22266
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 22268
    :cond_0
    iget-object v1, p0, Ldwi;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 22269
    const/4 v1, 0x2

    iget-object v2, p0, Ldwi;->c:Ljava/lang/String;

    .line 22270
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22272
    :cond_1
    iget-object v1, p0, Ldwi;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 22273
    const/4 v1, 0x3

    iget-object v2, p0, Ldwi;->d:Ljava/lang/Integer;

    .line 22274
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 22276
    :cond_2
    iget-object v1, p0, Ldwi;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22277
    iput v0, p0, Ldwi;->cachedSize:I

    .line 22278
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 22231
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldwi;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldwi;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldwi;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldwi;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldwi;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldwi;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldwi;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwi;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwi;->d:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 22248
    iget-object v0, p0, Ldwi;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 22249
    const/4 v0, 0x1

    iget-object v1, p0, Ldwi;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 22251
    :cond_0
    iget-object v0, p0, Ldwi;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 22252
    const/4 v0, 0x2

    iget-object v1, p0, Ldwi;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 22254
    :cond_1
    iget-object v0, p0, Ldwi;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 22255
    const/4 v0, 0x3

    iget-object v1, p0, Ldwi;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 22257
    :cond_2
    iget-object v0, p0, Ldwi;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 22259
    return-void
.end method
