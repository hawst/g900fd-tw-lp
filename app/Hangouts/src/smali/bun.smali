.class final Lbun;
.super Lack;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lack",
        "<",
        "Lbfn;",
        "Lbii;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic d:Lbuk;

.field private final e:I

.field private final f:Z


# direct methods
.method protected constructor <init>(Lbuk;Z)V
    .locals 1

    .prologue
    .line 56
    iput-object p1, p0, Lbun;->d:Lbuk;

    invoke-direct {p0}, Lack;-><init>()V

    .line 57
    const/4 v0, 0x6

    iput v0, p0, Lbun;->e:I

    .line 58
    iput-boolean p2, p0, Lbun;->f:Z

    .line 59
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lbun;->d:Lbuk;

    iget-object v0, v0, Lbuk;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->kL:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lbos;)V
    .locals 3

    .prologue
    .line 72
    invoke-super {p0, p1}, Lack;->a(Lbos;)V

    .line 75
    invoke-virtual {p1}, Lbos;->c()Lbfz;

    move-result-object v0

    check-cast v0, Lbii;

    .line 77
    invoke-virtual {v0}, Lbii;->h()Ljava/lang/Boolean;

    move-result-object v1

    .line 78
    if-eqz v1, :cond_0

    .line 79
    iget-object v0, p0, Lbun;->d:Lbuk;

    iget-object v0, v0, Lbuk;->a:Lbtz;

    iget-object v2, p0, Lbun;->d:Lbuk;

    iget-object v2, v2, Lbuk;->e:Ljava/lang/String;

    invoke-interface {v0, v2}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 80
    const/4 v2, 0x0

    invoke-static {v1, v2}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 82
    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 89
    invoke-super {p0, p1}, Lack;->a(Ljava/lang/Exception;)V

    .line 90
    iget-object v0, p0, Lbun;->d:Lbuk;

    iget-object v0, v0, Lbuk;->a:Lbtz;

    iget-object v1, p0, Lbun;->d:Lbuk;

    iget-object v1, v1, Lbuk;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iget-boolean v1, p0, Lbun;->f:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 91
    return-void

    .line 90
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 3

    .prologue
    .line 63
    iget-object v0, p0, Lbun;->d:Lbuk;

    iget-object v0, v0, Lbuk;->b:Lyj;

    iget v1, p0, Lbun;->e:I

    iget-boolean v2, p0, Lbun;->f:Z

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lyj;IZ)I

    move-result v0

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 102
    invoke-static {}, Lbun;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 103
    iget-object v0, p0, Lbun;->d:Lbuk;

    iget-object v0, v0, Lbuk;->c:Landroid/app/Activity;

    sget v1, Lh;->kN:I

    invoke-static {v0, v1}, Lf;->a(Landroid/content/Context;I)V

    .line 107
    :goto_0
    return-void

    .line 105
    :cond_0
    iget-object v0, p0, Lbun;->d:Lbuk;

    iget-object v0, v0, Lbuk;->c:Landroid/app/Activity;

    sget v1, Lh;->kM:I

    invoke-static {v0, v1}, Lf;->a(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public e()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lbfn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    const-class v0, Lbfn;

    return-object v0
.end method

.method public f()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lbii;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    const-class v0, Lbii;

    return-object v0
.end method

.method public h()V
    .locals 0

    .prologue
    .line 95
    invoke-super {p0}, Lack;->h()V

    .line 98
    return-void
.end method
