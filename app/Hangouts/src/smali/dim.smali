.class public final Ldim;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldim;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 423
    const/4 v0, 0x0

    new-array v0, v0, [Ldim;

    sput-object v0, Ldim;->a:[Ldim;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 424
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 456
    const/4 v0, 0x1

    iget-object v1, p0, Ldim;->b:Ljava/lang/String;

    .line 458
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 459
    const/4 v1, 0x2

    iget-object v2, p0, Ldim;->c:Ljava/lang/String;

    .line 460
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 461
    iget-object v1, p0, Ldim;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 462
    const/4 v1, 0x3

    iget-object v2, p0, Ldim;->d:Ljava/lang/String;

    .line 463
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 465
    :cond_0
    iget-object v1, p0, Ldim;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 466
    const/4 v1, 0x4

    iget-object v2, p0, Ldim;->e:Ljava/lang/String;

    .line 467
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 469
    :cond_1
    iget-object v1, p0, Ldim;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 470
    const/4 v1, 0x5

    iget-object v2, p0, Ldim;->f:Ljava/lang/String;

    .line 471
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 473
    :cond_2
    iget-object v1, p0, Ldim;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 474
    iput v0, p0, Ldim;->cachedSize:I

    .line 475
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 420
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldim;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldim;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldim;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldim;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldim;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldim;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldim;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldim;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 439
    const/4 v0, 0x1

    iget-object v1, p0, Ldim;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 440
    const/4 v0, 0x2

    iget-object v1, p0, Ldim;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 441
    iget-object v0, p0, Ldim;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 442
    const/4 v0, 0x3

    iget-object v1, p0, Ldim;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 444
    :cond_0
    iget-object v0, p0, Ldim;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 445
    const/4 v0, 0x4

    iget-object v1, p0, Ldim;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 447
    :cond_1
    iget-object v0, p0, Ldim;->f:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 448
    const/4 v0, 0x5

    iget-object v1, p0, Ldim;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 450
    :cond_2
    iget-object v0, p0, Ldim;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 452
    return-void
.end method
