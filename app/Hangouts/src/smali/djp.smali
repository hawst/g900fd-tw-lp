.class public final Ldjp;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldjp;


# instance fields
.field public b:Ljava/lang/String;

.field public c:[Ldkj;

.field public d:Lerz;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Ldjp;

    sput-object v0, Ldjp;->a:[Ldjp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 15
    sget-object v0, Ldkj;->a:[Ldkj;

    iput-object v0, p0, Ldjp;->c:[Ldkj;

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Ldjp;->d:Lerz;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 53
    iget-object v0, p0, Ldjp;->b:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 54
    const/4 v0, 0x1

    iget-object v2, p0, Ldjp;->b:Ljava/lang/String;

    .line 55
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 57
    :goto_0
    iget-object v2, p0, Ldjp;->c:[Ldkj;

    if-eqz v2, :cond_1

    .line 58
    iget-object v2, p0, Ldjp;->c:[Ldkj;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 59
    if-eqz v4, :cond_0

    .line 60
    const/4 v5, 0x2

    .line 61
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 58
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 65
    :cond_1
    iget-object v1, p0, Ldjp;->d:Lerz;

    if-eqz v1, :cond_2

    .line 66
    const/4 v1, 0x3

    iget-object v2, p0, Ldjp;->d:Lerz;

    .line 67
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_2
    iget-object v1, p0, Ldjp;->e:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 70
    const/4 v1, 0x4

    iget-object v2, p0, Ldjp;->e:Ljava/lang/Long;

    .line 71
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    :cond_3
    iget-object v1, p0, Ldjp;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 74
    const/4 v1, 0x5

    iget-object v2, p0, Ldjp;->f:Ljava/lang/String;

    .line 75
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 77
    :cond_4
    iget-object v1, p0, Ldjp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    iput v0, p0, Ldjp;->cachedSize:I

    .line 79
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldjp;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldjp;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldjp;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldjp;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldjp;->c:[Ldkj;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldkj;

    iget-object v3, p0, Ldjp;->c:[Ldkj;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldjp;->c:[Ldkj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldjp;->c:[Ldkj;

    :goto_2
    iget-object v2, p0, Ldjp;->c:[Ldkj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldjp;->c:[Ldkj;

    new-instance v3, Ldkj;

    invoke-direct {v3}, Ldkj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldjp;->c:[Ldkj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldjp;->c:[Ldkj;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldjp;->c:[Ldkj;

    new-instance v3, Ldkj;

    invoke-direct {v3}, Ldkj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldjp;->c:[Ldkj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldjp;->d:Lerz;

    if-nez v0, :cond_5

    new-instance v0, Lerz;

    invoke-direct {v0}, Lerz;-><init>()V

    iput-object v0, p0, Ldjp;->d:Lerz;

    :cond_5
    iget-object v0, p0, Ldjp;->d:Lerz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldjp;->e:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldjp;->f:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 27
    iget-object v0, p0, Ldjp;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 28
    const/4 v0, 0x1

    iget-object v1, p0, Ldjp;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 30
    :cond_0
    iget-object v0, p0, Ldjp;->c:[Ldkj;

    if-eqz v0, :cond_2

    .line 31
    iget-object v1, p0, Ldjp;->c:[Ldkj;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 32
    if-eqz v3, :cond_1

    .line 33
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 31
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 37
    :cond_2
    iget-object v0, p0, Ldjp;->d:Lerz;

    if-eqz v0, :cond_3

    .line 38
    const/4 v0, 0x3

    iget-object v1, p0, Ldjp;->d:Lerz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 40
    :cond_3
    iget-object v0, p0, Ldjp;->e:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 41
    const/4 v0, 0x4

    iget-object v1, p0, Ldjp;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 43
    :cond_4
    iget-object v0, p0, Ldjp;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 44
    const/4 v0, 0x5

    iget-object v1, p0, Ldjp;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 46
    :cond_5
    iget-object v0, p0, Ldjp;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 48
    return-void
.end method
