.class public final Lepv;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lepv;


# instance fields
.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/String;

.field public d:[Lepy;

.field public e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x0

    new-array v0, v0, [Lepv;

    sput-object v0, Lepv;->a:[Lepv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 267
    invoke-direct {p0}, Lepn;-><init>()V

    .line 274
    sget-object v0, Lepy;->a:[Lepy;

    iput-object v0, p0, Lepv;->d:[Lepy;

    .line 267
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 304
    iget-object v0, p0, Lepv;->b:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 305
    const/4 v0, 0x1

    iget-object v2, p0, Lepv;->b:Ljava/lang/Long;

    .line 306
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lepl;->e(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 308
    :goto_0
    iget-object v2, p0, Lepv;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 309
    const/4 v2, 0x2

    iget-object v3, p0, Lepv;->c:Ljava/lang/String;

    .line 310
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 312
    :cond_0
    iget-object v2, p0, Lepv;->d:[Lepy;

    if-eqz v2, :cond_2

    .line 313
    iget-object v2, p0, Lepv;->d:[Lepy;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 314
    if-eqz v4, :cond_1

    .line 315
    const/4 v5, 0x3

    .line 316
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 313
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 320
    :cond_2
    iget-object v1, p0, Lepv;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 321
    const/4 v1, 0x4

    iget-object v2, p0, Lepv;->e:Ljava/lang/String;

    .line 322
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 324
    :cond_3
    iget-object v1, p0, Lepv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 325
    iput v0, p0, Lepv;->cachedSize:I

    .line 326
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 263
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lepv;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lepv;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lepv;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lepv;->b:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepv;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lepv;->d:[Lepy;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lepy;

    iget-object v3, p0, Lepv;->d:[Lepy;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lepv;->d:[Lepy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lepv;->d:[Lepy;

    :goto_2
    iget-object v2, p0, Lepv;->d:[Lepy;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lepv;->d:[Lepy;

    new-instance v3, Lepy;

    invoke-direct {v3}, Lepy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lepv;->d:[Lepy;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lepv;->d:[Lepy;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lepv;->d:[Lepy;

    new-instance v3, Lepy;

    invoke-direct {v3}, Lepy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lepv;->d:[Lepy;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepv;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 281
    iget-object v0, p0, Lepv;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 282
    const/4 v0, 0x1

    iget-object v1, p0, Lepv;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 284
    :cond_0
    iget-object v0, p0, Lepv;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 285
    const/4 v0, 0x2

    iget-object v1, p0, Lepv;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 287
    :cond_1
    iget-object v0, p0, Lepv;->d:[Lepy;

    if-eqz v0, :cond_3

    .line 288
    iget-object v1, p0, Lepv;->d:[Lepy;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 289
    if-eqz v3, :cond_2

    .line 290
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 288
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 294
    :cond_3
    iget-object v0, p0, Lepv;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 295
    const/4 v0, 0x4

    iget-object v1, p0, Lepv;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 297
    :cond_4
    iget-object v0, p0, Lepv;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 299
    return-void
.end method
