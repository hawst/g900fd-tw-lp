.class public final Laop;
.super Laoy;
.source "PG"


# instance fields
.field private i:I

.field private final j:Ljava/lang/String;


# direct methods
.method constructor <init>(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Laoy;-><init>(Lyj;Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Laop;->i:I

    .line 27
    iput-object p4, p0, Laop;->j:Ljava/lang/String;

    .line 28
    return-void
.end method

.method private b(I)V
    .locals 0

    .prologue
    .line 54
    iput p1, p0, Laop;->i:I

    .line 55
    invoke-direct {p0}, Laop;->l()V

    .line 56
    return-void
.end method

.method private l()V
    .locals 3

    .prologue
    .line 63
    iget-object v0, p0, Laop;->e:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Resolving abuse report for disposition: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Laop;->i:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iget v0, p0, Laop;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 72
    invoke-virtual {p0}, Laop;->g()V

    .line 73
    invoke-direct {p0}, Laop;->m()V

    goto :goto_0

    .line 74
    :cond_2
    iget v0, p0, Laop;->i:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 75
    invoke-direct {p0}, Laop;->o()V

    goto :goto_0
.end method

.method private m()V
    .locals 0

    .prologue
    .line 80
    invoke-virtual {p0}, Laop;->j()V

    .line 81
    invoke-virtual {p0}, Laop;->f()V

    .line 82
    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    .line 85
    iget v0, p0, Laop;->i:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 86
    invoke-virtual {p0}, Laop;->k()V

    .line 88
    :cond_0
    invoke-direct {p0}, Laop;->m()V

    .line 89
    return-void
.end method

.method private o()V
    .locals 3

    .prologue
    .line 129
    iget v0, p0, Laop;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 130
    iget-object v0, p0, Laop;->e:Ljava/lang/String;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 131
    iget-object v0, p0, Laop;->c:Ljava/lang/String;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 132
    iget-boolean v0, p0, Laop;->f:Z

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 133
    iget-boolean v0, p0, Laop;->g:Z

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 134
    iget-object v0, p0, Laop;->b:Lyj;

    iget-object v1, p0, Laop;->e:Ljava/lang/String;

    iget-object v2, p0, Laop;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Laop;->h:I

    .line 135
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Starting confirmation. Request ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Laop;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    return-void
.end method


# virtual methods
.method protected a(ILbga;)V
    .locals 2

    .prologue
    .line 111
    invoke-super {p0, p1, p2}, Laoy;->a(ILbga;)V

    .line 112
    iget-object v0, p0, Laop;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 113
    invoke-direct {p0}, Laop;->l()V

    .line 118
    :goto_0
    return-void

    .line 116
    :cond_0
    const-string v0, "Babel"

    const-string v1, "initialize abuse report response missing broadcast_id"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    invoke-direct {p0}, Laop;->n()V

    goto :goto_0
.end method

.method protected a(ILbgn;)V
    .locals 4

    .prologue
    .line 94
    invoke-super {p0, p1, p2}, Laoy;->a(ILbgn;)V

    .line 95
    invoke-virtual {p2}, Lbgn;->g()Ldxv;

    move-result-object v0

    .line 96
    iget-object v1, v0, Ldxv;->j:Ldya;

    .line 97
    if-eqz v1, :cond_0

    iget-object v2, v1, Ldya;->g:Ldxw;

    if-nez v2, :cond_2

    .line 98
    :cond_0
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "Unexpectedly null "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v1, :cond_1

    const-string v0, "recordingDetails"

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-direct {p0}, Laop;->n()V

    .line 106
    :goto_1
    return-void

    .line 98
    :cond_1
    const-string v0, "abuseRecording"

    goto :goto_0

    .line 103
    :cond_2
    iget-object v1, v1, Ldya;->g:Ldxw;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Ldxw;->b:Ljava/lang/Integer;

    .line 104
    iget-object v1, p0, Laop;->b:Lyj;

    .line 105
    invoke-virtual {p2}, Lbgn;->h()Ldzg;

    move-result-object v2

    .line 104
    invoke-static {v1, v0, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ldxv;Ldzg;)I

    move-result v0

    iput v0, p0, Laop;->h:I

    goto :goto_1
.end method

.method protected a(ILbhf;)V
    .locals 1

    .prologue
    .line 141
    invoke-super {p0, p1, p2}, Laoy;->a(ILbhf;)V

    .line 142
    invoke-virtual {p2}, Lbhf;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    invoke-direct {p0}, Laop;->o()V

    .line 148
    :goto_0
    return-void

    .line 145
    :cond_0
    invoke-virtual {p0}, Laop;->i()V

    .line 146
    invoke-direct {p0}, Laop;->m()V

    goto :goto_0
.end method

.method public a(ILyj;Lbea;Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 123
    invoke-super {p0, p1, p2, p3, p4}, Laoy;->a(ILyj;Lbea;Ljava/lang/Exception;)V

    .line 124
    invoke-direct {p0}, Laop;->n()V

    .line 125
    return-void
.end method

.method b()V
    .locals 4

    .prologue
    .line 31
    iget v0, p0, Laop;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 32
    iget-object v0, p0, Laop;->e:Ljava/lang/String;

    invoke-static {v0}, Lcwz;->a(Ljava/lang/Object;)V

    .line 33
    iget-boolean v0, p0, Laop;->f:Z

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 34
    iget-boolean v0, p0, Laop;->g:Z

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 35
    iget-object v0, p0, Laop;->b:Lyj;

    iget-object v1, p0, Laop;->c:Ljava/lang/String;

    iget-object v2, p0, Laop;->d:Ljava/lang/String;

    iget-object v3, p0, Laop;->j:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Laop;->h:I

    .line 37
    return-void
.end method

.method c()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Laop;->b(I)V

    .line 44
    return-void
.end method

.method d()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Laop;->b(I)V

    .line 51
    return-void
.end method

.method e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Laop;->j:Ljava/lang/String;

    return-object v0
.end method
