.class public final Lerl;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lerl;

.field public static final b:Lepo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepo",
            "<",
            "Lerl;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Leqc;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Lepu;

.field public l:Leqe;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lerl;

    sput-object v0, Lerl;->a:[Lerl;

    .line 13
    const v0, 0x19fc221

    new-instance v1, Lerm;

    invoke-direct {v1}, Lerm;-><init>()V

    .line 14
    invoke-static {v0, v1}, Lepo;->a(ILepp;)Lepo;

    move-result-object v0

    sput-object v0, Lerl;->b:Lepo;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 27
    iput-object v0, p0, Lerl;->h:Leqc;

    .line 34
    iput-object v0, p0, Lerl;->k:Lepu;

    .line 37
    iput-object v0, p0, Lerl;->l:Leqe;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 78
    const/4 v0, 0x0

    .line 79
    iget-object v1, p0, Lerl;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 80
    const/4 v0, 0x1

    iget-object v1, p0, Lerl;->c:Ljava/lang/String;

    .line 81
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 83
    :cond_0
    iget-object v1, p0, Lerl;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 84
    const/4 v1, 0x2

    iget-object v2, p0, Lerl;->d:Ljava/lang/String;

    .line 85
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    :cond_1
    iget-object v1, p0, Lerl;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 88
    const/4 v1, 0x3

    iget-object v2, p0, Lerl;->e:Ljava/lang/String;

    .line 89
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 91
    :cond_2
    iget-object v1, p0, Lerl;->j:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 92
    const/4 v1, 0x4

    iget-object v2, p0, Lerl;->j:Ljava/lang/String;

    .line 93
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    :cond_3
    iget-object v1, p0, Lerl;->g:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 96
    const/4 v1, 0x5

    iget-object v2, p0, Lerl;->g:Ljava/lang/String;

    .line 97
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 99
    :cond_4
    iget-object v1, p0, Lerl;->h:Leqc;

    if-eqz v1, :cond_5

    .line 100
    const/4 v1, 0x6

    iget-object v2, p0, Lerl;->h:Leqc;

    .line 101
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    :cond_5
    iget-object v1, p0, Lerl;->f:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 104
    const/4 v1, 0x7

    iget-object v2, p0, Lerl;->f:Ljava/lang/String;

    .line 105
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 107
    :cond_6
    iget-object v1, p0, Lerl;->i:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 108
    const/16 v1, 0xb

    iget-object v2, p0, Lerl;->i:Ljava/lang/String;

    .line 109
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 111
    :cond_7
    iget-object v1, p0, Lerl;->k:Lepu;

    if-eqz v1, :cond_8

    .line 112
    const/16 v1, 0xc

    iget-object v2, p0, Lerl;->k:Lepu;

    .line 113
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 115
    :cond_8
    iget-object v1, p0, Lerl;->l:Leqe;

    if-eqz v1, :cond_9

    .line 116
    const/16 v1, 0xd

    iget-object v2, p0, Lerl;->l:Leqe;

    .line 117
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 119
    :cond_9
    iget-object v1, p0, Lerl;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 120
    iput v0, p0, Lerl;->cachedSize:I

    .line 121
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lerl;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lerl;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lerl;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lerl;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lerl;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lerl;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lerl;->j:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lerl;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lerl;->h:Leqc;

    if-nez v0, :cond_2

    new-instance v0, Leqc;

    invoke-direct {v0}, Leqc;-><init>()V

    iput-object v0, p0, Lerl;->h:Leqc;

    :cond_2
    iget-object v0, p0, Lerl;->h:Leqc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lerl;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lerl;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    iget-object v0, p0, Lerl;->k:Lepu;

    if-nez v0, :cond_3

    new-instance v0, Lepu;

    invoke-direct {v0}, Lepu;-><init>()V

    iput-object v0, p0, Lerl;->k:Lepu;

    :cond_3
    iget-object v0, p0, Lerl;->k:Lepu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_a
    iget-object v0, p0, Lerl;->l:Leqe;

    if-nez v0, :cond_4

    new-instance v0, Leqe;

    invoke-direct {v0}, Leqe;-><init>()V

    iput-object v0, p0, Lerl;->l:Leqe;

    :cond_4
    iget-object v0, p0, Lerl;->l:Leqe;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x5a -> :sswitch_8
        0x62 -> :sswitch_9
        0x6a -> :sswitch_a
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lerl;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 43
    const/4 v0, 0x1

    iget-object v1, p0, Lerl;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 45
    :cond_0
    iget-object v0, p0, Lerl;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 46
    const/4 v0, 0x2

    iget-object v1, p0, Lerl;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 48
    :cond_1
    iget-object v0, p0, Lerl;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 49
    const/4 v0, 0x3

    iget-object v1, p0, Lerl;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 51
    :cond_2
    iget-object v0, p0, Lerl;->j:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 52
    const/4 v0, 0x4

    iget-object v1, p0, Lerl;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 54
    :cond_3
    iget-object v0, p0, Lerl;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 55
    const/4 v0, 0x5

    iget-object v1, p0, Lerl;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 57
    :cond_4
    iget-object v0, p0, Lerl;->h:Leqc;

    if-eqz v0, :cond_5

    .line 58
    const/4 v0, 0x6

    iget-object v1, p0, Lerl;->h:Leqc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 60
    :cond_5
    iget-object v0, p0, Lerl;->f:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 61
    const/4 v0, 0x7

    iget-object v1, p0, Lerl;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 63
    :cond_6
    iget-object v0, p0, Lerl;->i:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 64
    const/16 v0, 0xb

    iget-object v1, p0, Lerl;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 66
    :cond_7
    iget-object v0, p0, Lerl;->k:Lepu;

    if-eqz v0, :cond_8

    .line 67
    const/16 v0, 0xc

    iget-object v1, p0, Lerl;->k:Lepu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 69
    :cond_8
    iget-object v0, p0, Lerl;->l:Leqe;

    if-eqz v0, :cond_9

    .line 70
    const/16 v0, 0xd

    iget-object v1, p0, Lerl;->l:Leqe;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 72
    :cond_9
    iget-object v0, p0, Lerl;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 74
    return-void
.end method
