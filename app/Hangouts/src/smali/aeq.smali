.class final Laeq;
.super Lack;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lack",
        "<",
        "Lbel;",
        "Lbgs;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic d:Laen;

.field private final e:Lyj;

.field private final f:Laea;


# direct methods
.method public constructor <init>(Laen;Lyj;Laea;)V
    .locals 0

    .prologue
    .line 398
    iput-object p1, p0, Laeq;->d:Laen;

    invoke-direct {p0}, Lack;-><init>()V

    .line 399
    iput-object p2, p0, Laeq;->e:Lyj;

    .line 400
    iput-object p3, p0, Laeq;->f:Laea;

    .line 401
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 448
    iget-object v0, p0, Laeq;->d:Laen;

    invoke-static {v0}, Laen;->c(Laen;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->aV:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lbos;)V
    .locals 3

    .prologue
    .line 415
    invoke-super {p0, p1}, Lack;->a(Lbos;)V

    .line 417
    invoke-virtual {p1}, Lbos;->c()Lbfz;

    move-result-object v0

    check-cast v0, Lbgs;

    .line 418
    invoke-virtual {v0}, Lbgs;->f()Ljava/util/List;

    move-result-object v0

    .line 420
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 421
    const/4 v0, 0x0

    .line 424
    :cond_0
    iget-object v1, p0, Laeq;->d:Laen;

    iget-object v2, p0, Laeq;->f:Laea;

    invoke-static {v1, v2, v0}, Laen;->a(Laen;Laea;Ljava/util/List;)V

    .line 425
    return-void
.end method

.method protected a(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 429
    invoke-super {p0, p1}, Lack;->a(Ljava/lang/Exception;)V

    .line 431
    instance-of v0, p1, Laco;

    if-eqz v0, :cond_0

    .line 433
    iget-object v0, p0, Laeq;->d:Laen;

    invoke-static {v0}, Laen;->a(Laen;)V

    .line 437
    :goto_0
    return-void

    .line 435
    :cond_0
    iget-object v0, p0, Laeq;->d:Laen;

    invoke-static {v0}, Laen;->b(Laen;)V

    goto :goto_0
.end method

.method public b()I
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 405
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Laeq;->f:Laea;

    invoke-virtual {v0}, Laea;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Laeq;->f:Laea;

    invoke-virtual {v1}, Laea;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbcn;->a(Ljava/lang/String;)Lbcn;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Laeq;->d:Laen;

    invoke-static {v0}, Laen;->d(Laen;)I

    move-result v0

    invoke-static {v0}, Lf;->c(I)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    iget-object v0, p0, Laeq;->d:Laen;

    invoke-virtual {v0}, Laen;->a()Lyj;

    move-result-object v0

    invoke-virtual {v0}, Lyj;->N()Z

    move-result v6

    iget-object v0, p0, Laeq;->f:Laea;

    invoke-virtual {v0}, Laea;->g()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laee;

    instance-of v1, v0, Laef;

    if-eqz v1, :cond_8

    move-object v1, v0

    check-cast v1, Laef;

    iget-object v1, v1, Laef;->a:Ljava/lang/String;

    :goto_1
    instance-of v4, v0, Laeh;

    if-eqz v4, :cond_7

    check-cast v0, Laeh;

    iget-object v4, v0, Laeh;->a:Ljava/lang/String;

    invoke-static {v4}, Lbzd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v8, "Skipping phone number since it is not valid:"

    invoke-direct {v1, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-static {}, Laen;->d()Z

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "Babel"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Using E164 number "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " for "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v4}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v8, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move-object v4, v0

    :goto_2
    if-eqz v6, :cond_4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v3

    :goto_3
    invoke-static {v1, v4, v0}, Lbcn;->a(Ljava/lang/String;Ljava/lang/String;Z)Lbcn;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    .line 406
    :cond_5
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_6

    .line 407
    const/4 v0, -0x1

    .line 409
    :goto_4
    return v0

    :cond_6
    iget-object v0, p0, Laeq;->e:Lyj;

    invoke-static {v0, v5, v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/util/ArrayList;Ljava/lang/String;Z)I

    move-result v0

    goto :goto_4

    :cond_7
    move-object v4, v2

    goto :goto_2

    :cond_8
    move-object v1, v2

    goto :goto_1
.end method

.method public e()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lbel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 453
    const-class v0, Lbel;

    return-object v0
.end method

.method public f()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lbgs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 458
    const-class v0, Lbgs;

    return-object v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 441
    invoke-super {p0}, Lack;->h()V

    .line 443
    iget-object v0, p0, Laeq;->d:Laen;

    invoke-virtual {v0}, Laen;->c()V

    .line 444
    return-void
.end method
