.class final Lane;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Lana;

.field private b:I

.field private c:Z

.field private d:Z


# direct methods
.method private constructor <init>(Lana;)V
    .locals 1

    .prologue
    .line 89
    iput-object p1, p0, Lane;->a:Lana;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lane;->d:Z

    return-void
.end method

.method synthetic constructor <init>(Lana;B)V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lane;-><init>(Lana;)V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 96
    iget-boolean v0, p0, Lane;->c:Z

    if-nez v0, :cond_0

    .line 99
    iget-object v0, p0, Lane;->a:Lana;

    iget-object v0, v0, Lana;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v1

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 100
    iget-object v0, p0, Lane;->a:Lana;

    iget-object v0, v0, Lana;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lane;->b:I

    .line 101
    iput-boolean v1, p0, Lane;->c:Z

    .line 104
    :cond_0
    iget-object v0, p0, Lane;->a:Lana;

    invoke-virtual {v0}, Lana;->b()Lcom/google/android/apps/hangouts/views/EsListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/EsListView;->getMeasuredHeight()I

    move-result v0

    iget v3, p0, Lane;->b:I

    if-le v0, v3, :cond_3

    .line 105
    iget-boolean v0, p0, Lane;->d:Z

    if-nez v0, :cond_1

    .line 106
    iput-boolean v1, p0, Lane;->d:Z

    .line 107
    iget-object v0, p0, Lane;->a:Lana;

    iget-object v0, v0, Lana;->f:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->bh:I

    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 116
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 99
    goto :goto_0

    .line 111
    :cond_3
    iget-boolean v0, p0, Lane;->d:Z

    if-eqz v0, :cond_1

    .line 112
    iput-boolean v2, p0, Lane;->d:Z

    .line 113
    iget-object v0, p0, Lane;->a:Lana;

    iget-object v0, v0, Lana;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_1
.end method
