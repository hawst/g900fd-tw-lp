.class public final Laen;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Z


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lt;

.field private final d:Lyj;

.field private final e:Laea;

.field private final f:Laep;

.field private final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lbys;->d:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Laen;->a:Z

    return-void
.end method

.method public constructor <init>(Lt;Lyj;Laea;Laep;I)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Laen;->c:Lt;

    .line 65
    iput-object p2, p0, Laen;->d:Lyj;

    .line 66
    iput-object p3, p0, Laen;->e:Laea;

    .line 67
    iput-object p4, p0, Laen;->f:Laep;

    .line 68
    iget-object v0, p0, Laen;->c:Lt;

    invoke-virtual {v0}, Lt;->getActivity()Ly;

    move-result-object v0

    iput-object v0, p0, Laen;->b:Landroid/content/Context;

    .line 69
    iput p5, p0, Laen;->g:I

    .line 70
    return-void
.end method

.method private static a(Laea;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 220
    invoke-virtual {p0}, Laea;->g()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v1, v2

    move-object v3, v2

    move-object v4, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laee;

    .line 221
    instance-of v6, v0, Laef;

    if-eqz v6, :cond_0

    .line 222
    check-cast v0, Laef;

    iget-object v0, v0, Laef;->a:Ljava/lang/String;

    move-object v4, v0

    goto :goto_0

    .line 223
    :cond_0
    instance-of v6, v0, Laeh;

    if-eqz v6, :cond_1

    .line 224
    check-cast v0, Laeh;

    iget-object v0, v0, Laeh;->a:Ljava/lang/String;

    move-object v3, v0

    goto :goto_0

    .line 226
    :cond_1
    invoke-virtual {p0}, Laea;->e()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 228
    goto :goto_0

    .line 229
    :cond_2
    if-eqz v4, :cond_3

    .line 236
    :goto_1
    return-object v4

    .line 231
    :cond_3
    if-eqz v3, :cond_4

    move-object v4, v3

    .line 232
    goto :goto_1

    .line 233
    :cond_4
    if-eqz v1, :cond_5

    move-object v4, v1

    .line 234
    goto :goto_1

    :cond_5
    move-object v4, v2

    .line 236
    goto :goto_1
.end method

.method private a(Laea;Z)V
    .locals 4

    .prologue
    .line 278
    iget-object v0, p0, Laen;->c:Lt;

    invoke-virtual {v0}, Lt;->getFragmentManager()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->a()Lao;

    move-result-object v0

    .line 280
    invoke-static {p1, p2}, Laev;->a(Laea;Z)Laev;

    move-result-object v1

    .line 281
    iget-object v2, p0, Laen;->c:Lt;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Laev;->setTargetFragment(Lt;I)V

    .line 282
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Laev;->a(Lao;Ljava/lang/String;)I

    .line 283
    return-void
.end method

.method private a(Laee;Landroid/widget/Toast;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 312
    const/4 v1, 0x0

    .line 313
    if-nez p1, :cond_5

    .line 314
    iget v2, p0, Laen;->g:I

    invoke-static {v2}, Lf;->d(I)Z

    move-result v2

    if-eqz v2, :cond_2

    move v1, v0

    move-object v0, p1

    .line 326
    :goto_0
    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    .line 327
    invoke-virtual {p2}, Landroid/widget/Toast;->show()V

    .line 334
    :cond_0
    iget-object v1, p0, Laen;->f:Laep;

    if-eqz v1, :cond_1

    .line 335
    iget-object v1, p0, Laen;->f:Laep;

    iget-object v2, p0, Laen;->e:Laea;

    iget-object v3, p0, Laen;->e:Laea;

    .line 337
    invoke-virtual {v3}, Laea;->n()Laee;

    move-result-object v3

    if-nez v3, :cond_3

    .line 335
    :goto_1
    invoke-interface {v1, v2, v0}, Laep;->onContactLookupComplete(Laea;Laee;)V

    .line 340
    :cond_1
    return-void

    .line 319
    :cond_2
    iget-object v2, p0, Laen;->e:Laea;

    invoke-virtual {v2}, Laea;->h()Laeh;

    move-result-object p1

    .line 320
    if-nez p1, :cond_4

    move v1, v0

    move-object v0, p1

    .line 321
    goto :goto_0

    .line 337
    :cond_3
    iget-object v0, p0, Laen;->e:Laea;

    .line 338
    invoke-virtual {v0}, Laea;->n()Laee;

    move-result-object v0

    goto :goto_1

    :cond_4
    move-object v0, p1

    goto :goto_0

    :cond_5
    move-object v0, p1

    goto :goto_0
.end method

.method static synthetic a(Laen;)V
    .locals 4

    .prologue
    .line 40
    const/4 v0, 0x0

    iget-object v1, p0, Laen;->b:Landroid/content/Context;

    sget v2, Lh;->aX:I

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Laen;->a(Laee;Landroid/widget/Toast;)V

    return-void
.end method

.method static synthetic a(Laen;Laea;Ljava/util/List;)V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v4, 0x0

    .line 40
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    if-eqz p2, :cond_3

    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v3, v4

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzo;

    if-eqz v0, :cond_2

    iget-object v1, v0, Lbzo;->a:Ljava/io/Serializable;

    check-cast v1, Lbcn;

    iget-object v0, v0, Lbzo;->b:Ljava/io/Serializable;

    check-cast v0, [Lbdh;

    array-length v9, v0

    move v5, v4

    :goto_1
    if-ge v5, v9, :cond_2

    aget-object v10, v0, v5

    if-eqz v10, :cond_1

    iget-object v2, v10, Lbdh;->b:Lbdk;

    if-eqz v2, :cond_1

    iget-object v2, v10, Lbdh;->b:Lbdk;

    invoke-virtual {v2}, Lbdk;->a()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v10, Lbdh;->b:Lbdk;

    iget-object v2, v2, Lbdk;->a:Ljava/lang/String;

    invoke-interface {v7, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v10, Lbdh;->b:Lbdk;

    iget-object v2, v2, Lbdk;->a:Ljava/lang/String;

    invoke-interface {v7, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Laea;->g()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Laee;

    invoke-virtual {v2}, Laee;->b()Laee;

    move-result-object v2

    iget v11, p0, Laen;->g:I

    invoke-static {v11}, Lf;->c(I)Z

    move-result v11

    if-eqz v11, :cond_0

    instance-of v11, v2, Laeh;

    if-eqz v11, :cond_1

    :cond_0
    invoke-virtual {v2, v10, v1}, Laee;->a(Lbdh;Lbcn;)V

    invoke-static {p1}, Laen;->a(Laea;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lbdh;->b(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_3
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p1}, Laea;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Lookup a contact with gaia failed:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Laea;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    :goto_2
    return-void

    :cond_5
    iget-object v0, p0, Laen;->d:Lyj;

    invoke-virtual {v0}, Lyj;->u()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Laen;->e:Laea;

    invoke-direct {p0, v0}, Laen;->b(Laea;)V

    goto :goto_2

    :cond_6
    iget-object v0, p0, Laen;->b:Landroid/content/Context;

    sget v1, Lh;->aW:I

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-direct {p0, v12, v0}, Laen;->a(Laee;Landroid/widget/Toast;)V

    goto :goto_2

    :cond_7
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_9

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laee;

    instance-of v1, v0, Laeh;

    if-eqz v1, :cond_8

    iget-object v1, p0, Laen;->d:Lyj;

    invoke-virtual {v1}, Lyj;->v()Z

    move-result v1

    if-nez v1, :cond_8

    move-object v1, v0

    check-cast v1, Laeh;

    iget-object v1, v1, Laeh;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v0, p0, Laen;->e:Laea;

    invoke-direct {p0, v0}, Laen;->b(Laea;)V

    goto :goto_2

    :cond_8
    invoke-direct {p0, v0, v12}, Laen;->a(Laee;Landroid/widget/Toast;)V

    goto :goto_2

    :cond_9
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Laen;->c:Lt;

    invoke-virtual {v0}, Lt;->getFragmentManager()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->a()Lao;

    move-result-object v0

    new-instance v1, Laea;

    invoke-direct {v1, v6}, Laea;-><init>(Ljava/util/ArrayList;)V

    invoke-static {v1}, Laer;->a(Laea;)Laer;

    move-result-object v1

    iget-object v2, p0, Laen;->c:Lt;

    invoke-virtual {v1, v2, v4}, Laer;->setTargetFragment(Lt;I)V

    invoke-virtual {v1, v0, v12}, Laer;->a(Lao;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private b(Laea;)V
    .locals 4

    .prologue
    .line 257
    invoke-virtual {p1}, Laea;->p()Ljava/util/ArrayList;

    move-result-object v0

    .line 258
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 259
    iget-object v1, p0, Laen;->c:Lt;

    invoke-virtual {v1}, Lt;->getFragmentManager()Lae;

    move-result-object v1

    invoke-virtual {v1}, Lae;->a()Lao;

    move-result-object v1

    .line 261
    invoke-virtual {p1}, Laea;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Laej;->a(Ljava/lang/String;Ljava/util/ArrayList;)Laej;

    move-result-object v0

    .line 262
    iget-object v2, p0, Laen;->c:Lt;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Laej;->setTargetFragment(Lt;I)V

    .line 263
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Laej;->a(Lao;Ljava/lang/String;)I

    .line 269
    :goto_0
    return-void

    .line 265
    :cond_0
    const-string v0, "Babel"

    const-string v1, "displayContactInviteDialog: no valid contact to invite"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    iget-object v0, p0, Laen;->b:Landroid/content/Context;

    sget v1, Lh;->aO:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 267
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method static synthetic b(Laen;)V
    .locals 4

    .prologue
    .line 40
    const/4 v0, 0x0

    iget-object v1, p0, Laen;->b:Landroid/content/Context;

    sget v2, Lh;->aW:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Laen;->a(Laee;Landroid/widget/Toast;)V

    return-void
.end method

.method static synthetic c(Laen;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Laen;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Laen;)I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Laen;->g:I

    return v0
.end method

.method static synthetic d()Z
    .locals 1

    .prologue
    .line 40
    sget-boolean v0, Laen;->a:Z

    return v0
.end method


# virtual methods
.method public a()Lyj;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Laen;->d:Lyj;

    return-object v0
.end method

.method public a(Laee;)V
    .locals 1

    .prologue
    .line 347
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Laen;->a(Laee;Landroid/widget/Toast;)V

    .line 348
    return-void
.end method

.method public a(Laeh;)V
    .locals 5

    .prologue
    .line 376
    iget-object v0, p0, Laen;->d:Lyj;

    invoke-virtual {v0}, Lyj;->N()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 377
    new-instance v0, Laea;

    iget-object v1, p0, Laen;->e:Laea;

    invoke-virtual {v1}, Laea;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Laen;->e:Laea;

    .line 378
    invoke-virtual {v2}, Laea;->c()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Laeh;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Laea;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Iterable;)V

    .line 381
    new-instance v1, Laeq;

    iget-object v2, p0, Laen;->d:Lyj;

    invoke-direct {v1, p0, v2, v0}, Laeq;-><init>(Laen;Lyj;Laea;)V

    new-instance v0, Lacf;

    iget-object v2, p0, Laen;->c:Lt;

    .line 383
    invoke-virtual {v2}, Lt;->getFragmentManager()Lae;

    move-result-object v2

    invoke-direct {v0, v2}, Lacf;-><init>(Lae;)V

    .line 381
    invoke-static {v1, v0}, Lacc;->a(Laci;Lach;)Lacc;

    move-result-object v0

    .line 383
    invoke-virtual {v0}, Lacc;->a()V

    .line 387
    :goto_0
    return-void

    .line 385
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Laen;->a(Laee;Landroid/widget/Toast;)V

    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 82
    iget-object v0, p0, Laen;->c:Lt;

    check-cast v0, Laeo;

    .line 83
    if-nez v0, :cond_0

    .line 84
    const-string v0, "Babel"

    const-string v1, "targetFragment should implement ContactLookupContainer"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :goto_0
    return-void

    .line 87
    :cond_0
    invoke-interface {v0, p0}, Laeo;->a(Laen;)V

    .line 89
    iget v0, p0, Laen;->g:I

    invoke-static {v0}, Lf;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    iget-object v0, p0, Laen;->e:Laea;

    invoke-direct {p0, v0, v1}, Laen;->a(Laea;Z)V

    goto :goto_0

    .line 92
    :cond_1
    iget v0, p0, Laen;->g:I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 93
    invoke-static {}, Lbkb;->m()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Laen;->e:Laea;

    invoke-virtual {v0}, Laea;->l()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 95
    iget-object v0, p0, Laen;->e:Laea;

    invoke-virtual {v0}, Laea;->j()Ljava/util/ArrayList;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v1, :cond_3

    iget-object v1, p0, Laen;->e:Laea;

    invoke-virtual {v1}, Laea;->b()Z

    move-result v1

    if-nez v1, :cond_3

    .line 97
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laee;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Laen;->a(Laee;Landroid/widget/Toast;)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 92
    goto :goto_1

    .line 99
    :cond_3
    iget-object v0, p0, Laen;->e:Laea;

    invoke-direct {p0, v0, v2}, Laen;->a(Laea;Z)V

    goto :goto_0

    .line 104
    :cond_4
    new-instance v0, Laeq;

    iget-object v1, p0, Laen;->d:Lyj;

    iget-object v2, p0, Laen;->e:Laea;

    invoke-direct {v0, p0, v1, v2}, Laeq;-><init>(Laen;Lyj;Laea;)V

    new-instance v1, Lacf;

    iget-object v2, p0, Laen;->c:Lt;

    .line 106
    invoke-virtual {v2}, Lt;->getFragmentManager()Lae;

    move-result-object v2

    invoke-direct {v1, v2}, Lacf;-><init>(Lae;)V

    .line 104
    invoke-static {v0, v1}, Lacc;->a(Laci;Lach;)Lacc;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Lacc;->a()V

    goto :goto_0
.end method

.method public b(Laee;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 355
    instance-of v0, p1, Laef;

    if-eqz v0, :cond_2

    .line 356
    iget-object v0, p0, Laen;->c:Lt;

    invoke-virtual {v0}, Lt;->getActivity()Ly;

    move-result-object v2

    iget-object v3, p0, Laen;->d:Lyj;

    check-cast p1, Laef;

    iget-object v4, p1, Laef;->a:Ljava/lang/String;

    if-nez v4, :cond_1

    move-object v0, v1

    .line 357
    :goto_0
    invoke-virtual {v0}, Lalx;->a()V

    .line 370
    :cond_0
    :goto_1
    return-void

    .line 356
    :cond_1
    new-instance v0, Lalx;

    invoke-direct {v0, v2, v3, v4, v1}, Lalx;-><init>(Ly;Lyj;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 358
    :cond_2
    instance-of v0, p1, Laeh;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 359
    check-cast v0, Laeh;

    iget-object v0, v0, Laeh;->a:Ljava/lang/String;

    invoke-static {v0}, Laea;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 362
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 363
    iget-object v0, p0, Laen;->c:Lt;

    invoke-virtual {v0}, Lt;->getActivity()Ly;

    move-result-object v3

    iget-object v4, p0, Laen;->d:Lyj;

    if-nez v2, :cond_3

    .line 364
    :goto_2
    invoke-virtual {v1}, Lalx;->a()V

    goto :goto_1

    .line 363
    :cond_3
    new-instance v0, Lalx;

    invoke-direct {v0, v3, v4, v1, v2}, Lalx;-><init>(Ly;Lyj;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    goto :goto_2

    .line 366
    :cond_4
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onContactInviteConfirmed: invalid phone number "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    check-cast p1, Laeh;

    iget-object v2, p1, Laeh;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public c()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 305
    invoke-direct {p0, v0, v0}, Laen;->a(Laee;Landroid/widget/Toast;)V

    .line 306
    return-void
.end method
