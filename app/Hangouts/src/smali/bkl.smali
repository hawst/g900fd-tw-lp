.class public final Lbkl;
.super Lbnj;
.source "PG"


# instance fields
.field private final a:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Z

.field private final g:Z

.field private h:I


# direct methods
.method public constructor <init>(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lbnj;-><init>(Lyj;)V

    .line 33
    iput-object p2, p0, Lbkl;->a:Ljava/lang/String;

    .line 34
    iput-object p3, p0, Lbkl;->d:Ljava/lang/String;

    .line 35
    iput-object p4, p0, Lbkl;->e:Ljava/lang/String;

    .line 36
    iput-boolean p5, p0, Lbkl;->f:Z

    .line 37
    iput-boolean p6, p0, Lbkl;->g:Z

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lbkl;->h:I

    .line 39
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 47
    new-instance v0, Lyt;

    iget-object v1, p0, Lbkl;->b:Lyj;

    invoke-direct {v0, v1}, Lyt;-><init>(Lyj;)V

    .line 48
    iget-object v1, p0, Lbkl;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 49
    iget-object v0, p0, Lbkl;->c:Lbnl;

    new-instance v1, Lbde;

    iget-object v2, p0, Lbkl;->a:Ljava/lang/String;

    iget-object v3, p0, Lbkl;->e:Ljava/lang/String;

    iget-boolean v4, p0, Lbkl;->f:Z

    iget-boolean v5, p0, Lbkl;->g:Z

    invoke-direct {v1, v2, v3, v4, v5}, Lbde;-><init>(Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v0, v1}, Lbnl;->a(Lbea;)V

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    iget-object v1, p0, Lbkl;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 52
    iget-object v1, p0, Lbkl;->d:Ljava/lang/String;

    iget-object v2, p0, Lbkl;->e:Ljava/lang/String;

    iget-boolean v2, p0, Lbkl;->f:Z

    invoke-virtual {v0, v3, v1, v2}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 55
    new-instance v0, Lyt;

    .line 56
    invoke-static {}, Lbkb;->p()Lyj;

    move-result-object v1

    invoke-direct {v0, v1}, Lyt;-><init>(Lyj;)V

    .line 57
    iget-boolean v1, p0, Lbkl;->f:Z

    if-eqz v1, :cond_2

    .line 58
    iget-object v1, p0, Lbkl;->d:Ljava/lang/String;

    iget-object v2, p0, Lbkl;->e:Ljava/lang/String;

    invoke-virtual {v0, v3, v1, v2, v3}, Lyt;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :goto_1
    iget v0, p0, Lbkl;->h:I

    if-ltz v0, :cond_0

    .line 64
    new-instance v0, Ldcy;

    invoke-direct {v0}, Ldcy;-><init>()V

    .line 65
    new-instance v1, Lbhi;

    invoke-direct {v1, v0}, Lbhi;-><init>(Ldcy;)V

    .line 66
    new-instance v0, Lbos;

    iget v2, p0, Lbkl;->h:I

    const/4 v3, 0x1

    invoke-direct {v0, v2, v3, v1}, Lbos;-><init>(IILbfz;)V

    .line 68
    iget v1, p0, Lbkl;->h:I

    iget-object v2, p0, Lbkl;->b:Lyj;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(ILyj;Lbos;)V

    goto :goto_0

    .line 60
    :cond_2
    iget-object v1, p0, Lbkl;->d:Ljava/lang/String;

    invoke-virtual {v0, v3, v1}, Lyt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 42
    iput p1, p0, Lbkl;->h:I

    .line 43
    return-void
.end method
