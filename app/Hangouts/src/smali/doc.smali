.class public final Ldoc;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldoc;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:[Ldoe;

.field public h:Ljava/lang/Boolean;

.field public i:Ljava/lang/Integer;

.field public j:Ljava/lang/Integer;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/Integer;

.field public m:Ldod;

.field public n:Ljava/lang/Long;

.field public o:[Ldoj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 242
    const/4 v0, 0x0

    new-array v0, v0, [Ldoc;

    sput-object v0, Ldoc;->a:[Ldoc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 243
    invoke-direct {p0}, Lepn;-><init>()V

    .line 2014
    sget-object v0, Ldoe;->a:[Ldoe;

    iput-object v0, p0, Ldoc;->g:[Ldoe;

    .line 2027
    const/4 v0, 0x0

    iput-object v0, p0, Ldoc;->m:Ldod;

    .line 2032
    sget-object v0, Ldoj;->a:[Ldoj;

    iput-object v0, p0, Ldoc;->o:[Ldoj;

    .line 243
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2087
    const/4 v0, 0x1

    iget-object v2, p0, Ldoc;->b:Ljava/lang/String;

    .line 2089
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2090
    const/4 v2, 0x2

    iget-object v3, p0, Ldoc;->c:Ljava/lang/String;

    .line 2091
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2092
    const/4 v2, 0x3

    iget-object v3, p0, Ldoc;->d:Ljava/lang/Integer;

    .line 2093
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2094
    iget-object v2, p0, Ldoc;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 2095
    const/4 v2, 0x4

    iget-object v3, p0, Ldoc;->e:Ljava/lang/String;

    .line 2096
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2098
    :cond_0
    iget-object v2, p0, Ldoc;->g:[Ldoe;

    if-eqz v2, :cond_2

    .line 2099
    iget-object v3, p0, Ldoc;->g:[Ldoe;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 2100
    if-eqz v5, :cond_1

    .line 2101
    const/4 v6, 0x5

    .line 2102
    invoke-static {v6, v5}, Lepl;->c(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 2099
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2106
    :cond_2
    iget-object v2, p0, Ldoc;->h:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    .line 2107
    const/16 v2, 0x1c

    iget-object v3, p0, Ldoc;->h:Ljava/lang/Boolean;

    .line 2108
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2110
    :cond_3
    iget-object v2, p0, Ldoc;->i:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    .line 2111
    const/16 v2, 0x1d

    iget-object v3, p0, Ldoc;->i:Ljava/lang/Integer;

    .line 2112
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2114
    :cond_4
    iget-object v2, p0, Ldoc;->j:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    .line 2115
    const/16 v2, 0x1e

    iget-object v3, p0, Ldoc;->j:Ljava/lang/Integer;

    .line 2116
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2118
    :cond_5
    iget-object v2, p0, Ldoc;->n:Ljava/lang/Long;

    if-eqz v2, :cond_6

    .line 2119
    const/16 v2, 0x2a

    iget-object v3, p0, Ldoc;->n:Ljava/lang/Long;

    .line 2120
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2122
    :cond_6
    iget-object v2, p0, Ldoc;->f:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 2123
    const/16 v2, 0x2b

    iget-object v3, p0, Ldoc;->f:Ljava/lang/String;

    .line 2124
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2126
    :cond_7
    iget-object v2, p0, Ldoc;->o:[Ldoj;

    if-eqz v2, :cond_9

    .line 2127
    iget-object v2, p0, Ldoc;->o:[Ldoj;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_9

    aget-object v4, v2, v1

    .line 2128
    if-eqz v4, :cond_8

    .line 2129
    const/16 v5, 0x34

    .line 2130
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2127
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2134
    :cond_9
    iget-object v1, p0, Ldoc;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    .line 2135
    const/16 v1, 0x44

    iget-object v2, p0, Ldoc;->l:Ljava/lang/Integer;

    .line 2136
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2138
    :cond_a
    iget-object v1, p0, Ldoc;->m:Ldod;

    if-eqz v1, :cond_b

    .line 2139
    const/16 v1, 0x45

    iget-object v2, p0, Ldoc;->m:Ldod;

    .line 2140
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2142
    :cond_b
    iget-object v1, p0, Ldoc;->k:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 2143
    const/16 v1, 0x64

    iget-object v2, p0, Ldoc;->k:Ljava/lang/String;

    .line 2144
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2146
    :cond_c
    iget-object v1, p0, Ldoc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2147
    iput v0, p0, Ldoc;->cachedSize:I

    .line 2148
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v1, 0x0

    .line 239
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldoc;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldoc;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldoc;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldoc;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldoc;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoc;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldoc;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2b

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldoc;->g:[Ldoe;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldoe;

    iget-object v3, p0, Ldoc;->g:[Ldoe;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldoc;->g:[Ldoe;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldoc;->g:[Ldoe;

    :goto_2
    iget-object v2, p0, Ldoc;->g:[Ldoe;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldoc;->g:[Ldoe;

    new-instance v3, Ldoe;

    invoke-direct {v3}, Ldoe;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldoc;->g:[Ldoe;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2, v4}, Lepk;->a(Lepr;I)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldoc;->g:[Ldoe;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldoc;->g:[Ldoe;

    new-instance v3, Ldoe;

    invoke-direct {v3}, Ldoe;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldoc;->g:[Ldoe;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0, v4}, Lepk;->a(Lepr;I)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldoc;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoc;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoc;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldoc;->n:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldoc;->f:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    const/16 v0, 0x1a2

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldoc;->o:[Ldoj;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldoj;

    iget-object v3, p0, Ldoc;->o:[Ldoj;

    if-eqz v3, :cond_5

    iget-object v3, p0, Ldoc;->o:[Ldoj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Ldoc;->o:[Ldoj;

    :goto_4
    iget-object v2, p0, Ldoc;->o:[Ldoj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Ldoc;->o:[Ldoj;

    new-instance v3, Ldoj;

    invoke-direct {v3}, Ldoj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldoc;->o:[Ldoj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Ldoc;->o:[Ldoj;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Ldoc;->o:[Ldoj;

    new-instance v3, Ldoj;

    invoke-direct {v3}, Ldoj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldoc;->o:[Ldoj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoc;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Ldoc;->m:Ldod;

    if-nez v0, :cond_8

    new-instance v0, Ldod;

    invoke-direct {v0}, Ldod;-><init>()V

    iput-object v0, p0, Ldoc;->m:Ldod;

    :cond_8
    iget-object v0, p0, Ldoc;->m:Ldod;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldoc;->k:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2b -> :sswitch_5
        0xe0 -> :sswitch_6
        0xe8 -> :sswitch_7
        0xf0 -> :sswitch_8
        0x150 -> :sswitch_9
        0x15a -> :sswitch_a
        0x1a2 -> :sswitch_b
        0x220 -> :sswitch_c
        0x22a -> :sswitch_d
        0x322 -> :sswitch_e
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2037
    const/4 v1, 0x1

    iget-object v2, p0, Ldoc;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 2038
    const/4 v1, 0x2

    iget-object v2, p0, Ldoc;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 2039
    const/4 v1, 0x3

    iget-object v2, p0, Ldoc;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 2040
    iget-object v1, p0, Ldoc;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2041
    const/4 v1, 0x4

    iget-object v2, p0, Ldoc;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 2043
    :cond_0
    iget-object v1, p0, Ldoc;->g:[Ldoe;

    if-eqz v1, :cond_2

    .line 2044
    iget-object v2, p0, Ldoc;->g:[Ldoe;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 2045
    if-eqz v4, :cond_1

    .line 2046
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILepr;)V

    .line 2044
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2050
    :cond_2
    iget-object v1, p0, Ldoc;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 2051
    const/16 v1, 0x1c

    iget-object v2, p0, Ldoc;->h:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 2053
    :cond_3
    iget-object v1, p0, Ldoc;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 2054
    const/16 v1, 0x1d

    iget-object v2, p0, Ldoc;->i:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 2056
    :cond_4
    iget-object v1, p0, Ldoc;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 2057
    const/16 v1, 0x1e

    iget-object v2, p0, Ldoc;->j:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 2059
    :cond_5
    iget-object v1, p0, Ldoc;->n:Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 2060
    const/16 v1, 0x2a

    iget-object v2, p0, Ldoc;->n:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->b(IJ)V

    .line 2062
    :cond_6
    iget-object v1, p0, Ldoc;->f:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 2063
    const/16 v1, 0x2b

    iget-object v2, p0, Ldoc;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 2065
    :cond_7
    iget-object v1, p0, Ldoc;->o:[Ldoj;

    if-eqz v1, :cond_9

    .line 2066
    iget-object v1, p0, Ldoc;->o:[Ldoj;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_9

    aget-object v3, v1, v0

    .line 2067
    if-eqz v3, :cond_8

    .line 2068
    const/16 v4, 0x34

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 2066
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2072
    :cond_9
    iget-object v0, p0, Ldoc;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 2073
    const/16 v0, 0x44

    iget-object v1, p0, Ldoc;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 2075
    :cond_a
    iget-object v0, p0, Ldoc;->m:Ldod;

    if-eqz v0, :cond_b

    .line 2076
    const/16 v0, 0x45

    iget-object v1, p0, Ldoc;->m:Ldod;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 2078
    :cond_b
    iget-object v0, p0, Ldoc;->k:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 2079
    const/16 v0, 0x64

    iget-object v1, p0, Ldoc;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 2081
    :cond_c
    iget-object v0, p0, Ldoc;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 2083
    return-void
.end method
