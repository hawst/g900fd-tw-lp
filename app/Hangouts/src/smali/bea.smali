.class public abstract Lbea;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final d:Z

.field public static final e:Ljava/lang/String;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public f:I

.field public transient g:Lbci;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 205
    sget-object v0, Lbys;->h:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbea;->d:Z

    .line 243
    const-string v0, "oauth2:https://www.googleapis.com/auth/chat https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/hangouts https://www.googleapis.com/auth/identity.plus.page.impersonation https://www.googleapis.com/auth/chat.native"

    .line 246
    :try_start_0
    const-string v1, "com.google.android.apps.hangouts.defaultbuild.EsProvider"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 256
    :goto_0
    sput-object v0, Lbea;->e:Ljava/lang/String;

    .line 257
    return-void

    .line 250
    :catch_0
    move-exception v0

    const-string v0, "oauth2:https://www.googleapis.com/auth/chat https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/hangouts https://www.googleapis.com/auth/identity.plus.page.impersonation"

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 271
    const/4 v0, 0x0

    iput v0, p0, Lbea;->f:I

    .line 4621
    return-void
.end method

.method public static final a(Lyj;I)V
    .locals 2

    .prologue
    .line 420
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbme;

    invoke-static {v0, v1}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    invoke-interface {v0, p0}, Lbme;->a(Lyj;)Laux;

    move-result-object v0

    .line 421
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 422
    return-void
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 363
    invoke-static {}, Lbld;->a()J

    move-result-wide v0

    .line 364
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 365
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 366
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 368
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Ljava/lang/String;)Ldqf;
    .locals 1

    .prologue
    .line 202
    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object p0, v0, Ldqf;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public A_()Z
    .locals 1

    .prologue
    .line 306
    const/4 v0, 0x1

    return v0
.end method

.method public B_()Lcom/google/api/client/http/GenericUrl;
    .locals 5

    .prologue
    .line 314
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 315
    invoke-virtual {p0}, Lbea;->a()Ljava/lang/String;

    move-result-object v1

    .line 316
    invoke-static {v1}, Lf;->o(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 317
    new-instance v2, Lcom/google/api/client/http/GenericUrl;

    invoke-direct {v2, v1}, Lcom/google/api/client/http/GenericUrl;-><init>(Ljava/lang/String;)V

    .line 318
    invoke-virtual {p0, v0}, Lbea;->a(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    .line 319
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 320
    const-string v1, "trace"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "token:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    :cond_0
    return-object v2
.end method

.method public abstract a(Landroid/content/Context;Lbpp;Lbpk;)Lbfz;
.end method

.method protected a(Ldpt;ZLjava/lang/String;I)Ldvm;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 383
    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    .line 384
    iget-object v1, p0, Lbea;->g:Lbci;

    invoke-virtual {v1}, Lbci;->c()Ldpv;

    move-result-object v1

    iput-object v1, v0, Ldvm;->b:Ldpv;

    .line 386
    sget-boolean v1, Lbea;->d:Z

    if-eqz v1, :cond_0

    .line 387
    const-string v1, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Reporting "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for request: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 388
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 387
    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    :cond_0
    invoke-static {}, Lbld;->a()J

    move-result-wide v1

    .line 392
    cmp-long v3, v1, v4

    if-nez v3, :cond_1

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 393
    :cond_1
    new-instance v3, Ldps;

    invoke-direct {v3}, Ldps;-><init>()V

    iput-object v3, v0, Ldvm;->c:Ldps;

    .line 394
    cmp-long v3, v1, v4

    if-eqz v3, :cond_2

    .line 395
    iget-object v3, v0, Ldvm;->c:Ldps;

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Ldps;->c:Ljava/lang/String;

    .line 397
    iget-object v3, v0, Ldvm;->c:Ldps;

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Ldps;->d:Ljava/lang/String;

    .line 399
    :cond_2
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 402
    iget-object v1, v0, Ldvm;->c:Ldps;

    iput-object p3, v1, Ldps;->b:Ljava/lang/String;

    .line 406
    :cond_3
    if-eqz p1, :cond_4

    .line 407
    iput-object p1, v0, Ldvm;->d:Ldpt;

    .line 410
    :cond_4
    if-eqz p2, :cond_5

    .line 411
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ldvm;->e:Ljava/lang/String;

    .line 414
    :cond_5
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldvm;->g:Ljava/lang/Integer;

    .line 416
    return-object v0
.end method

.method public abstract a(Ljava/lang/String;I)Lepr;
.end method

.method protected a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 326
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://www.googleapis.com/chat/v1android/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbea;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 334
    const-string v0, "babel_apiary_trace_token"

    sget-object v1, Lbkj;->a:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 340
    iput p1, p0, Lbea;->f:I

    .line 341
    return-void
.end method

.method public a(Lyj;Lbph;)V
    .locals 0

    .prologue
    .line 4696
    return-void
.end method

.method public a(JI)Z
    .locals 1

    .prologue
    .line 4668
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lbea;)Z
    .locals 2

    .prologue
    .line 4709
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 4710
    const/4 v0, 0x0

    return v0
.end method

.method public b(JI)Lbph;
    .locals 1

    .prologue
    .line 4731
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Ljava/lang/String;I)Lcom/google/api/client/http/HttpContent;
    .locals 2

    .prologue
    .line 347
    invoke-virtual {p0, p1, p2}, Lbea;->a(Ljava/lang/String;I)Lepr;

    move-result-object v1

    .line 348
    if-eqz v1, :cond_0

    .line 349
    new-instance v0, Lbcy;

    invoke-direct {v0, v1}, Lbcy;-><init>(Lepr;)V

    .line 351
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public c(JI)Lbph;
    .locals 1

    .prologue
    .line 4740
    const/4 v0, 0x0

    return-object v0
.end method

.method protected c(Ljava/lang/String;I)Ldvm;
    .locals 2

    .prologue
    .line 356
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p1, p2}, Lbea;->a(Ldpt;ZLjava/lang/String;I)Ldvm;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4702
    const-string v0, "default_queue"

    return-object v0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 4744
    const/4 v0, 0x0

    return v0
.end method

.method public d()J
    .locals 3

    .prologue
    .line 4676
    const-string v0, "babel_pending_message_failure_duration"

    const-wide/32 v1, 0x124f80

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 289
    const/4 v0, 0x0

    return v0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 299
    const/4 v0, 0x0

    return v0
.end method

.method public p()V
    .locals 0

    .prologue
    .line 4719
    return-void
.end method

.method public q()V
    .locals 0

    .prologue
    .line 4723
    return-void
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 4735
    const/4 v0, 0x0

    return v0
.end method

.method public s()V
    .locals 1

    .prologue
    .line 4758
    invoke-static {}, Lbci;->a()Lbci;

    move-result-object v0

    iput-object v0, p0, Lbea;->g:Lbci;

    .line 4759
    return-void
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 4766
    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4727
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
