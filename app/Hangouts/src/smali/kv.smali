.class Lkv;
.super Lkd;
.source "PG"


# instance fields
.field a:Landroid/content/Context;

.field b:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

.field final c:Landroid/os/Handler;

.field private d:Landroid/content/Context;

.field private e:Lkj;

.field private f:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

.field private g:Landroid/support/v7/internal/widget/ActionBarContainer;

.field private h:Landroid/view/ViewGroup;

.field private i:Landroid/support/v7/internal/widget/ActionBarView;

.field private j:Landroid/support/v7/internal/widget/ActionBarContextView;

.field private k:Landroid/support/v7/internal/widget/ActionBarContainer;

.field private l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lkw;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lkw;

.field private n:I

.field private o:Z

.field private p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lkg;",
            ">;"
        }
    .end annotation
.end field

.field private q:I

.field private r:Z

.field private s:I

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Lke;


# direct methods
.method public constructor <init>(Lkj;Lke;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 106
    invoke-direct {p0}, Lkd;-><init>()V

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkv;->l:Ljava/util/ArrayList;

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lkv;->n:I

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkv;->p:Ljava/util/ArrayList;

    .line 92
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lkv;->c:Landroid/os/Handler;

    .line 95
    iput v2, p0, Lkv;->s:I

    .line 101
    iput-boolean v1, p0, Lkv;->w:Z

    .line 107
    iput-object p1, p0, Lkv;->e:Lkj;

    .line 108
    iput-object p1, p0, Lkv;->a:Landroid/content/Context;

    .line 109
    iput-object p2, p0, Lkv;->y:Lke;

    .line 110
    iget-object v3, p0, Lkv;->e:Lkj;

    sget v0, Lf;->C:I

    invoke-virtual {v3, v0}, Lkj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iput-object v0, p0, Lkv;->f:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v0, p0, Lkv;->f:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkv;->f:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->a(Lkd;)V

    :cond_0
    sget v0, Lf;->z:I

    invoke-virtual {v3, v0}, Lkj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarView;

    iput-object v0, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    sget v0, Lf;->G:I

    invoke-virtual {v3, v0}, Lkj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContextView;

    iput-object v0, p0, Lkv;->j:Landroid/support/v7/internal/widget/ActionBarContextView;

    sget v0, Lf;->B:I

    invoke-virtual {v3, v0}, Lkj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContainer;

    iput-object v0, p0, Lkv;->g:Landroid/support/v7/internal/widget/ActionBarContainer;

    sget v0, Lf;->U:I

    invoke-virtual {v3, v0}, Lkj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lkv;->h:Landroid/view/ViewGroup;

    iget-object v0, p0, Lkv;->h:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    iget-object v0, p0, Lkv;->g:Landroid/support/v7/internal/widget/ActionBarContainer;

    iput-object v0, p0, Lkv;->h:Landroid/view/ViewGroup;

    :cond_1
    sget v0, Lf;->S:I

    invoke-virtual {v3, v0}, Lkj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContainer;

    iput-object v0, p0, Lkv;->k:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-object v0, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lkv;->j:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lkv;->g:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-nez v0, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " can only be used with a compatible window decor layout"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    iget-object v3, p0, Lkv;->j:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/support/v7/internal/widget/ActionBarContextView;)V

    iget-object v0, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->j()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_0
    iput v0, p0, Lkv;->q:I

    iget-object v0, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->o()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_8

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    iput-boolean v1, p0, Lkv;->o:Z

    :cond_4
    iget-object v3, p0, Lkv;->a:Landroid/content/Context;

    invoke-static {v3}, Llf;->a(Landroid/content/Context;)Llf;

    move-result-object v3

    invoke-virtual {v3}, Llf;->e()Z

    move-result v4

    if-nez v4, :cond_5

    if-eqz v0, :cond_6

    :cond_5
    move v2, v1

    :cond_6
    invoke-virtual {p0, v2}, Lkv;->c(Z)V

    invoke-virtual {v3}, Llf;->c()Z

    move-result v0

    invoke-direct {p0, v0}, Lkv;->e(Z)V

    iget-object v0, p0, Lkv;->e:Lkj;

    invoke-virtual {v0}, Lkj;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkv;->a(Ljava/lang/CharSequence;)V

    .line 111
    return-void

    :cond_7
    move v0, v2

    .line 110
    goto :goto_0

    :cond_8
    move v0, v2

    goto :goto_1
.end method

.method private e(Z)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 155
    iput-boolean p1, p0, Lkv;->r:Z

    .line 157
    iget-boolean v0, p0, Lkv;->r:Z

    if-nez v0, :cond_1

    .line 158
    iget-object v0, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/support/v7/internal/widget/ScrollingTabContainerView;)V

    .line 159
    iget-object v0, p0, Lkv;->g:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-object v3, p0, Lkv;->b:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarContainer;->a(Landroid/support/v7/internal/widget/ScrollingTabContainerView;)V

    .line 164
    :goto_0
    invoke-virtual {p0}, Lkv;->j()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    .line 165
    :goto_1
    iget-object v3, p0, Lkv;->b:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    if-eqz v3, :cond_0

    .line 166
    if-eqz v0, :cond_3

    .line 167
    iget-object v3, p0, Lkv;->b:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {v3, v2}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    .line 172
    :cond_0
    :goto_2
    iget-object v3, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    iget-boolean v4, p0, Lkv;->r:Z

    if-nez v4, :cond_4

    if-eqz v0, :cond_4

    :goto_3
    invoke-virtual {v3, v1}, Landroid/support/v7/internal/widget/ActionBarView;->d(Z)V

    .line 173
    return-void

    .line 161
    :cond_1
    iget-object v0, p0, Lkv;->g:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarContainer;->a(Landroid/support/v7/internal/widget/ScrollingTabContainerView;)V

    .line 162
    iget-object v0, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    iget-object v3, p0, Lkv;->b:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/support/v7/internal/widget/ScrollingTabContainerView;)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 164
    goto :goto_1

    .line 169
    :cond_3
    iget-object v3, p0, Lkv;->b:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    goto :goto_2

    :cond_4
    move v1, v2

    .line 172
    goto :goto_3
.end method

.method private p()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 919
    iget-object v0, p0, Lkv;->b:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    if-eqz v0, :cond_0

    .line 937
    :goto_0
    return-void

    .line 923
    :cond_0
    new-instance v0, Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    iget-object v1, p0, Lkv;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;-><init>(Landroid/content/Context;)V

    .line 925
    iget-boolean v1, p0, Lkv;->r:Z

    if-eqz v1, :cond_1

    .line 926
    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    .line 927
    iget-object v1, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/support/v7/internal/widget/ScrollingTabContainerView;)V

    .line 936
    :goto_1
    iput-object v0, p0, Lkv;->b:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    goto :goto_0

    .line 929
    :cond_1
    invoke-virtual {p0}, Lkv;->j()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 930
    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    .line 934
    :goto_2
    iget-object v1, p0, Lkv;->g:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->a(Landroid/support/v7/internal/widget/ScrollingTabContainerView;)V

    goto :goto_1

    .line 932
    :cond_2
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    goto :goto_2
.end method

.method private q()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 980
    iget-boolean v0, p0, Lkv;->t:Z

    iget-boolean v3, p0, Lkv;->u:Z

    iget-boolean v4, p0, Lkv;->v:Z

    if-nez v4, :cond_2

    if-nez v0, :cond_0

    if-eqz v3, :cond_2

    :cond_0
    move v0, v1

    .line 982
    :goto_0
    if-eqz v0, :cond_3

    .line 983
    iget-boolean v0, p0, Lkv;->w:Z

    if-nez v0, :cond_1

    .line 984
    iput-boolean v2, p0, Lkv;->w:Z

    .line 985
    invoke-virtual {p0}, Lkv;->m()V

    .line 993
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 980
    goto :goto_0

    .line 988
    :cond_3
    iget-boolean v0, p0, Lkv;->w:Z

    if-eqz v0, :cond_1

    .line 989
    iput-boolean v1, p0, Lkv;->w:Z

    .line 990
    invoke-virtual {p0}, Lkv;->n()V

    goto :goto_1
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 240
    iget-object v1, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarView;->n()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 246
    :cond_0
    :goto_0
    return v0

    .line 242
    :pswitch_0
    iget-object v1, p0, Lkv;->m:Lkw;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lkv;->m:Lkw;

    invoke-virtual {v0}, Lkw;->a()I

    move-result v0

    goto :goto_0

    .line 244
    :pswitch_1
    iget-object v0, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->m()I

    move-result v0

    goto :goto_0

    .line 240
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->n()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 233
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setSelectedNavigationIndex not valid for current navigation mode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 227
    :pswitch_0
    iget-object v0, p0, Lkv;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkh;

    invoke-virtual {p0, v0}, Lkv;->b(Lkh;)V

    .line 231
    :goto_0
    return-void

    .line 230
    :pswitch_1
    iget-object v0, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->d(I)V

    goto :goto_0

    .line 225
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(II)V
    .locals 4

    .prologue
    .line 293
    iget-object v0, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->o()I

    move-result v0

    .line 294
    and-int/lit8 v1, p2, 0x4

    if-eqz v1, :cond_0

    .line 295
    const/4 v1, 0x1

    iput-boolean v1, p0, Lkv;->o:Z

    .line 297
    :cond_0
    iget-object v1, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    and-int v2, p1, p2

    xor-int/lit8 v3, p2, -0x1

    and-int/2addr v0, v3

    or-int/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarView;->b(I)V

    .line 298
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/graphics/drawable/Drawable;)V

    .line 215
    return-void
.end method

.method public a(Landroid/view/View;Lkf;)V
    .locals 1

    .prologue
    .line 186
    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 187
    iget-object v0, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/view/View;)V

    .line 189
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->a(Ljava/lang/CharSequence;)V

    .line 266
    return-void
.end method

.method public a(Lkg;)V
    .locals 1

    .prologue
    .line 579
    iget-object v0, p0, Lkv;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 580
    return-void
.end method

.method public a(Lkh;)V
    .locals 1

    .prologue
    .line 431
    invoke-virtual {p1}, Lkh;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lkv;->f(I)V

    .line 432
    return-void
.end method

.method public a(Lkh;Z)V
    .locals 3

    .prologue
    .line 406
    invoke-direct {p0}, Lkv;->p()V

    .line 407
    iget-object v0, p0, Lkv;->b:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->a(Lkh;Z)V

    .line 408
    iget-object v0, p0, Lkv;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    move-object v0, p1

    check-cast v0, Lkw;

    invoke-virtual {v0}, Lkw;->h()Lki;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Action Bar Tab must have a Callback"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0, v1}, Lkw;->b(I)V

    iget-object v2, p0, Lkv;->l:Ljava/util/ArrayList;

    invoke-virtual {v2, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v0, p0, Lkv;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lkv;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkw;

    invoke-virtual {v0, v1}, Lkw;->b(I)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 409
    :cond_1
    if-eqz p2, :cond_2

    .line 410
    invoke-virtual {p0, p1}, Lkv;->b(Lkh;)V

    .line 412
    :cond_2
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 312
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lkv;->a(II)V

    .line 313
    return-void

    .line 312
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->o()I

    move-result v0

    return v0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lkv;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkv;->a(Ljava/lang/CharSequence;)V

    .line 271
    return-void
.end method

.method public b(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->b(Landroid/graphics/drawable/Drawable;)V

    .line 530
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->c(Ljava/lang/CharSequence;)V

    .line 276
    return-void
.end method

.method public b(Lkh;)V
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 466
    invoke-virtual {p0}, Lkv;->j()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    .line 467
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lkh;->a()I

    move-result v0

    :cond_0
    iput v0, p0, Lkv;->n:I

    .line 493
    :cond_1
    :goto_0
    return-void

    .line 471
    :cond_2
    iget-object v1, p0, Lkv;->e:Lkj;

    invoke-virtual {v1}, Lkj;->e()Lae;

    move-result-object v1

    invoke-virtual {v1}, Lae;->a()Lao;

    move-result-object v1

    invoke-virtual {v1}, Lao;->a()Lao;

    move-result-object v1

    .line 474
    iget-object v2, p0, Lkv;->m:Lkw;

    if-ne v2, p1, :cond_4

    .line 475
    iget-object v0, p0, Lkv;->m:Lkw;

    if-eqz v0, :cond_3

    .line 476
    iget-object v0, p0, Lkv;->m:Lkw;

    invoke-virtual {v0}, Lkw;->h()Lki;

    move-result-object v0

    iget-object v2, p0, Lkv;->m:Lkw;

    invoke-interface {v0}, Lki;->b()V

    .line 477
    iget-object v0, p0, Lkv;->b:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {p1}, Lkh;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->b(I)V

    .line 490
    :cond_3
    :goto_1
    invoke-virtual {v1}, Lao;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 491
    invoke-virtual {v1}, Lao;->b()I

    goto :goto_0

    .line 480
    :cond_4
    iget-object v2, p0, Lkv;->b:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lkh;->a()I

    move-result v0

    :cond_5
    invoke-virtual {v2, v0}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->a(I)V

    .line 481
    iget-object v0, p0, Lkv;->m:Lkw;

    if-eqz v0, :cond_6

    .line 482
    iget-object v0, p0, Lkv;->m:Lkw;

    invoke-virtual {v0}, Lkw;->h()Lki;

    move-result-object v0

    iget-object v2, p0, Lkv;->m:Lkw;

    invoke-interface {v0}, Lki;->a()V

    .line 484
    :cond_6
    check-cast p1, Lkw;

    iput-object p1, p0, Lkv;->m:Lkw;

    .line 485
    iget-object v0, p0, Lkv;->m:Lkw;

    if-eqz v0, :cond_3

    .line 486
    iget-object v0, p0, Lkv;->m:Lkw;

    invoke-virtual {v0}, Lkw;->h()Lki;

    move-result-object v0

    iget-object v2, p0, Lkv;->m:Lkw;

    invoke-interface {v0, v2}, Lki;->a(Lkh;)V

    goto :goto_1
.end method

.method public b(Z)V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 322
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lkv;->a(II)V

    .line 323
    return-void

    .line 322
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Lkh;
    .locals 1

    .prologue
    .line 396
    new-instance v0, Lkw;

    invoke-direct {v0, p0}, Lkw;-><init>(Lkv;)V

    return-object v0
.end method

.method public c(I)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 367
    iget-object v1, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarView;->n()I

    move-result v1

    .line 368
    packed-switch v1, :pswitch_data_0

    .line 375
    :goto_0
    iget-object v1, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v1, p1}, Landroid/support/v7/internal/widget/ActionBarView;->c(I)V

    .line 376
    packed-switch p1, :pswitch_data_1

    .line 386
    :cond_0
    :goto_1
    iget-object v1, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    const/4 v2, 0x2

    if-ne p1, v2, :cond_1

    iget-boolean v2, p0, Lkv;->r:Z

    if-nez v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarView;->d(Z)V

    .line 387
    return-void

    .line 370
    :pswitch_0
    invoke-virtual {p0}, Lkv;->a()I

    move-result v1

    iput v1, p0, Lkv;->n:I

    .line 371
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lkv;->b(Lkh;)V

    .line 372
    iget-object v1, p0, Lkv;->b:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    goto :goto_0

    .line 378
    :pswitch_1
    invoke-direct {p0}, Lkv;->p()V

    .line 379
    iget-object v1, p0, Lkv;->b:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    .line 380
    iget v1, p0, Lkv;->n:I

    if-eq v1, v3, :cond_0

    .line 381
    iget v1, p0, Lkv;->n:I

    invoke-virtual {p0, v1}, Lkv;->a(I)V

    .line 382
    iput v3, p0, Lkv;->n:I

    goto :goto_1

    .line 368
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch

    .line 376
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_1
    .end packed-switch
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->c(Z)V

    .line 328
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lkv;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public d(I)Lkh;
    .locals 1

    .prologue
    .line 502
    iget-object v0, p0, Lkv;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkh;

    return-object v0
.end method

.method public d(Z)V
    .locals 1

    .prologue
    .line 996
    iput-boolean p1, p0, Lkv;->x:Z

    .line 997
    if-nez p1, :cond_0

    .line 998
    iget-object v0, p0, Lkv;->h:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->clearAnimation()V

    .line 999
    iget-object v0, p0, Lkv;->k:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_0

    .line 1000
    iget-object v0, p0, Lkv;->k:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->clearAnimation()V

    .line 1003
    :cond_0
    return-void
.end method

.method public e()I
    .locals 1

    .prologue
    .line 539
    iget-object v0, p0, Lkv;->g:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v0

    return v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 544
    iget-boolean v0, p0, Lkv;->t:Z

    if-eqz v0, :cond_0

    .line 545
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkv;->t:Z

    .line 546
    invoke-direct {p0}, Lkv;->q()V

    .line 548
    :cond_0
    return-void
.end method

.method public f(I)V
    .locals 4

    .prologue
    .line 436
    iget-object v0, p0, Lkv;->b:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    if-nez v0, :cond_1

    .line 457
    :cond_0
    :goto_0
    return-void

    .line 441
    :cond_1
    iget-object v0, p0, Lkv;->m:Lkw;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lkv;->m:Lkw;

    invoke-virtual {v0}, Lkw;->a()I

    move-result v0

    move v1, v0

    .line 443
    :goto_1
    iget-object v0, p0, Lkv;->b:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->d(I)V

    .line 444
    iget-object v0, p0, Lkv;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkw;

    .line 445
    if-eqz v0, :cond_2

    .line 446
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Lkw;->b(I)V

    .line 449
    :cond_2
    iget-object v0, p0, Lkv;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, p1

    .line 450
    :goto_2
    if-ge v2, v3, :cond_4

    .line 451
    iget-object v0, p0, Lkv;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkw;

    invoke-virtual {v0, v2}, Lkw;->b(I)V

    .line 450
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 441
    :cond_3
    iget v0, p0, Lkv;->n:I

    move v1, v0

    goto :goto_1

    .line 454
    :cond_4
    if-ne v1, p1, :cond_0

    .line 455
    iget-object v0, p0, Lkv;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p0, v0}, Lkv;->b(Lkh;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lkv;->l:Ljava/util/ArrayList;

    const/4 v1, 0x0

    add-int/lit8 v2, p1, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkw;

    goto :goto_3
.end method

.method public g()V
    .locals 1

    .prologue
    .line 559
    iget-boolean v0, p0, Lkv;->t:Z

    if-nez v0, :cond_0

    .line 560
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkv;->t:Z

    .line 561
    invoke-direct {p0}, Lkv;->q()V

    .line 563
    :cond_0
    return-void
.end method

.method public h()Landroid/content/Context;
    .locals 4

    .prologue
    .line 512
    iget-object v0, p0, Lkv;->d:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 513
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 514
    iget-object v1, p0, Lkv;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    .line 515
    sget v2, Lf;->h:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 516
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    .line 518
    if-eqz v0, :cond_1

    .line 519
    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lkv;->a:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lkv;->d:Landroid/content/Context;

    .line 524
    :cond_0
    :goto_0
    iget-object v0, p0, Lkv;->d:Landroid/content/Context;

    return-object v0

    .line 521
    :cond_1
    iget-object v0, p0, Lkv;->a:Landroid/content/Context;

    iput-object v0, p0, Lkv;->d:Landroid/content/Context;

    goto :goto_0
.end method

.method public i()V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lkv;->a:Landroid/content/Context;

    invoke-static {v0}, Llf;->a(Landroid/content/Context;)Llf;

    move-result-object v0

    invoke-virtual {v0}, Llf;->c()Z

    move-result v0

    invoke-direct {p0, v0}, Lkv;->e(Z)V

    .line 152
    return-void
.end method

.method public j()I
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lkv;->i:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->n()I

    move-result v0

    return v0
.end method

.method k()V
    .locals 1

    .prologue
    .line 551
    iget-boolean v0, p0, Lkv;->v:Z

    if-nez v0, :cond_0

    .line 552
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkv;->v:Z

    .line 553
    invoke-direct {p0}, Lkv;->q()V

    .line 555
    :cond_0
    return-void
.end method

.method l()V
    .locals 1

    .prologue
    .line 566
    iget-boolean v0, p0, Lkv;->v:Z

    if-eqz v0, :cond_0

    .line 567
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkv;->v:Z

    .line 568
    invoke-direct {p0}, Lkv;->q()V

    .line 570
    :cond_0
    return-void
.end method

.method public m()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1006
    iget-object v0, p0, Lkv;->h:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->clearAnimation()V

    .line 1007
    iget-object v0, p0, Lkv;->h:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 1026
    :cond_0
    :goto_0
    return-void

    .line 1011
    :cond_1
    invoke-virtual {p0}, Lkv;->o()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    .line 1013
    :goto_1
    if-eqz v0, :cond_2

    .line 1014
    iget-object v2, p0, Lkv;->a:Landroid/content/Context;

    sget v3, Lf;->b:I

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    .line 1015
    iget-object v3, p0, Lkv;->h:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1017
    :cond_2
    iget-object v2, p0, Lkv;->h:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1019
    iget-object v2, p0, Lkv;->k:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lkv;->k:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    .line 1020
    if-eqz v0, :cond_3

    .line 1021
    iget-object v0, p0, Lkv;->a:Landroid/content/Context;

    sget v2, Lf;->a:I

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1022
    iget-object v2, p0, Lkv;->k:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2, v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1024
    :cond_3
    iget-object v0, p0, Lkv;->k:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 1011
    goto :goto_1
.end method

.method public n()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 1029
    iget-object v0, p0, Lkv;->h:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->clearAnimation()V

    .line 1030
    iget-object v0, p0, Lkv;->h:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 1050
    :cond_0
    :goto_0
    return-void

    .line 1034
    :cond_1
    invoke-virtual {p0}, Lkv;->o()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    .line 1036
    :goto_1
    if-eqz v0, :cond_2

    .line 1037
    iget-object v1, p0, Lkv;->a:Landroid/content/Context;

    sget v2, Lf;->d:I

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 1038
    iget-object v2, p0, Lkv;->h:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1040
    :cond_2
    iget-object v1, p0, Lkv;->h:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1042
    iget-object v1, p0, Lkv;->k:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lkv;->k:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarContainer;->getVisibility()I

    move-result v1

    if-eq v1, v3, :cond_0

    .line 1043
    if-eqz v0, :cond_3

    .line 1044
    iget-object v0, p0, Lkv;->a:Landroid/content/Context;

    sget v1, Lf;->c:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1046
    iget-object v1, p0, Lkv;->k:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1048
    :cond_3
    iget-object v0, p0, Lkv;->k:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    goto :goto_0

    .line 1034
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method o()Z
    .locals 1

    .prologue
    .line 1053
    iget-boolean v0, p0, Lkv;->x:Z

    return v0
.end method
