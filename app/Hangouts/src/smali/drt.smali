.class public final Ldrt;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldrt;


# instance fields
.field public b:Ldtf;

.field public c:Ldtf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11439
    const/4 v0, 0x0

    new-array v0, v0, [Ldrt;

    sput-object v0, Ldrt;->a:[Ldrt;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11440
    invoke-direct {p0}, Lepn;-><init>()V

    .line 11443
    iput-object v0, p0, Ldrt;->b:Ldtf;

    .line 11446
    iput-object v0, p0, Ldrt;->c:Ldtf;

    .line 11440
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 11463
    const/4 v0, 0x0

    .line 11464
    iget-object v1, p0, Ldrt;->b:Ldtf;

    if-eqz v1, :cond_0

    .line 11465
    const/4 v0, 0x1

    iget-object v1, p0, Ldrt;->b:Ldtf;

    .line 11466
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 11468
    :cond_0
    iget-object v1, p0, Ldrt;->c:Ldtf;

    if-eqz v1, :cond_1

    .line 11469
    const/4 v1, 0x2

    iget-object v2, p0, Ldrt;->c:Ldtf;

    .line 11470
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11472
    :cond_1
    iget-object v1, p0, Ldrt;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11473
    iput v0, p0, Ldrt;->cachedSize:I

    .line 11474
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 11436
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldrt;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldrt;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldrt;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldrt;->b:Ldtf;

    if-nez v0, :cond_2

    new-instance v0, Ldtf;

    invoke-direct {v0}, Ldtf;-><init>()V

    iput-object v0, p0, Ldrt;->b:Ldtf;

    :cond_2
    iget-object v0, p0, Ldrt;->b:Ldtf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldrt;->c:Ldtf;

    if-nez v0, :cond_3

    new-instance v0, Ldtf;

    invoke-direct {v0}, Ldtf;-><init>()V

    iput-object v0, p0, Ldrt;->c:Ldtf;

    :cond_3
    iget-object v0, p0, Ldrt;->c:Ldtf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 11451
    iget-object v0, p0, Ldrt;->b:Ldtf;

    if-eqz v0, :cond_0

    .line 11452
    const/4 v0, 0x1

    iget-object v1, p0, Ldrt;->b:Ldtf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 11454
    :cond_0
    iget-object v0, p0, Ldrt;->c:Ldtf;

    if-eqz v0, :cond_1

    .line 11455
    const/4 v0, 0x2

    iget-object v1, p0, Ldrt;->c:Ldtf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 11457
    :cond_1
    iget-object v0, p0, Ldrt;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 11459
    return-void
.end method
