.class public final Ldrw;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldrw;


# instance fields
.field public b:Ldrr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25727
    const/4 v0, 0x0

    new-array v0, v0, [Ldrw;

    sput-object v0, Ldrw;->a:[Ldrw;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25728
    invoke-direct {p0}, Lepn;-><init>()V

    .line 25731
    const/4 v0, 0x0

    iput-object v0, p0, Ldrw;->b:Ldrr;

    .line 25728
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 2

    .prologue
    .line 25745
    const/4 v0, 0x0

    .line 25746
    iget-object v1, p0, Ldrw;->b:Ldrr;

    if-eqz v1, :cond_0

    .line 25747
    const/4 v0, 0x1

    iget-object v1, p0, Ldrw;->b:Ldrr;

    .line 25748
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 25750
    :cond_0
    iget-object v1, p0, Ldrw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25751
    iput v0, p0, Ldrw;->cachedSize:I

    .line 25752
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 25724
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldrw;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldrw;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldrw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldrw;->b:Ldrr;

    if-nez v0, :cond_2

    new-instance v0, Ldrr;

    invoke-direct {v0}, Ldrr;-><init>()V

    iput-object v0, p0, Ldrw;->b:Ldrr;

    :cond_2
    iget-object v0, p0, Ldrw;->b:Ldrr;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 25736
    iget-object v0, p0, Ldrw;->b:Ldrr;

    if-eqz v0, :cond_0

    .line 25737
    const/4 v0, 0x1

    iget-object v1, p0, Ldrw;->b:Ldrr;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 25739
    :cond_0
    iget-object v0, p0, Ldrw;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 25741
    return-void
.end method
