.class public final Lehu;
.super Ljava/util/concurrent/FutureTask;
.source "PG"

# interfaces
.implements Leht;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/FutureTask",
        "<TV;>;",
        "Leht",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final a:Lehm;


# direct methods
.method private constructor <init>(Ljava/lang/Runnable;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    .line 42
    new-instance v0, Lehm;

    invoke-direct {v0}, Lehm;-><init>()V

    iput-object v0, p0, Lehu;->a:Lehm;

    .line 77
    return-void
.end method

.method private constructor <init>(Ljava/util/concurrent/Callable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 42
    new-instance v0, Lehm;

    invoke-direct {v0}, Lehm;-><init>()V

    iput-object v0, p0, Lehu;->a:Lehm;

    .line 73
    return-void
.end method

.method public static a(Ljava/lang/Runnable;Ljava/lang/Object;)Lehu;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TV;)",
            "Lehu",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 68
    new-instance v0, Lehu;

    invoke-direct {v0, p0, p1}, Lehu;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Callable;)Lehu;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;)",
            "Lehu",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 52
    new-instance v0, Lehu;

    invoke-direct {v0, p0}, Lehu;-><init>(Ljava/util/concurrent/Callable;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lehu;->a:Lehm;

    invoke-virtual {v0, p1, p2}, Lehm;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 81
    return-void
.end method

.method protected done()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lehu;->a:Lehm;

    invoke-virtual {v0}, Lehm;->a()V

    .line 90
    return-void
.end method
