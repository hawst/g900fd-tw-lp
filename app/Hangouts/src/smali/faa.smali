.class public final Lfaa;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lfaa;


# instance fields
.field public b:[Leyr;

.field public c:[Leyr;

.field public d:[Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lfaa;

    sput-object v0, Lfaa;->a:[Lfaa;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 13
    sget-object v0, Leyr;->a:[Leyr;

    iput-object v0, p0, Lfaa;->b:[Leyr;

    .line 16
    sget-object v0, Leyr;->a:[Leyr;

    iput-object v0, p0, Lfaa;->c:[Leyr;

    .line 19
    sget-object v0, Lept;->q:[Ljava/lang/Boolean;

    iput-object v0, p0, Lfaa;->d:[Ljava/lang/Boolean;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 50
    iget-object v0, p0, Lfaa;->b:[Leyr;

    if-eqz v0, :cond_1

    .line 51
    iget-object v3, p0, Lfaa;->b:[Leyr;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 52
    if-eqz v5, :cond_0

    .line 53
    const/4 v6, 0x1

    .line 54
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 51
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 58
    :cond_2
    iget-object v2, p0, Lfaa;->c:[Leyr;

    if-eqz v2, :cond_4

    .line 59
    iget-object v2, p0, Lfaa;->c:[Leyr;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 60
    if-eqz v4, :cond_3

    .line 61
    const/4 v5, 0x2

    .line 62
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 59
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 66
    :cond_4
    iget-object v1, p0, Lfaa;->d:[Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lfaa;->d:[Ljava/lang/Boolean;

    array-length v1, v1

    if-lez v1, :cond_5

    .line 67
    iget-object v1, p0, Lfaa;->d:[Ljava/lang/Boolean;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    .line 68
    add-int/2addr v0, v1

    .line 69
    iget-object v1, p0, Lfaa;->d:[Ljava/lang/Boolean;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 71
    :cond_5
    iget-object v1, p0, Lfaa;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    iput v0, p0, Lfaa;->cachedSize:I

    .line 73
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lfaa;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lfaa;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lfaa;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lfaa;->b:[Leyr;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leyr;

    iget-object v3, p0, Lfaa;->b:[Leyr;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lfaa;->b:[Leyr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lfaa;->b:[Leyr;

    :goto_2
    iget-object v2, p0, Lfaa;->b:[Leyr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lfaa;->b:[Leyr;

    new-instance v3, Leyr;

    invoke-direct {v3}, Leyr;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lfaa;->b:[Leyr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lfaa;->b:[Leyr;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lfaa;->b:[Leyr;

    new-instance v3, Leyr;

    invoke-direct {v3}, Leyr;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lfaa;->b:[Leyr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lfaa;->c:[Leyr;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Leyr;

    iget-object v3, p0, Lfaa;->c:[Leyr;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lfaa;->c:[Leyr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Lfaa;->c:[Leyr;

    :goto_4
    iget-object v2, p0, Lfaa;->c:[Leyr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Lfaa;->c:[Leyr;

    new-instance v3, Leyr;

    invoke-direct {v3}, Leyr;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lfaa;->c:[Leyr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lfaa;->c:[Leyr;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lfaa;->c:[Leyr;

    new-instance v3, Leyr;

    invoke-direct {v3}, Leyr;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lfaa;->c:[Leyr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lfaa;->d:[Ljava/lang/Boolean;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Boolean;

    iget-object v3, p0, Lfaa;->d:[Ljava/lang/Boolean;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lfaa;->d:[Ljava/lang/Boolean;

    :goto_5
    iget-object v2, p0, Lfaa;->d:[Ljava/lang/Boolean;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Lfaa;->d:[Ljava/lang/Boolean;

    invoke-virtual {p1}, Lepk;->i()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_8
    iget-object v2, p0, Lfaa;->d:[Ljava/lang/Boolean;

    invoke-virtual {p1}, Lepk;->i()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 24
    iget-object v1, p0, Lfaa;->b:[Leyr;

    if-eqz v1, :cond_1

    .line 25
    iget-object v2, p0, Lfaa;->b:[Leyr;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 26
    if-eqz v4, :cond_0

    .line 27
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 25
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 31
    :cond_1
    iget-object v1, p0, Lfaa;->c:[Leyr;

    if-eqz v1, :cond_3

    .line 32
    iget-object v2, p0, Lfaa;->c:[Leyr;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 33
    if-eqz v4, :cond_2

    .line 34
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 32
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 38
    :cond_3
    iget-object v1, p0, Lfaa;->d:[Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 39
    iget-object v1, p0, Lfaa;->d:[Ljava/lang/Boolean;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 40
    const/4 v4, 0x3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {p1, v4, v3}, Lepl;->a(IZ)V

    .line 39
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 43
    :cond_4
    iget-object v0, p0, Lfaa;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 45
    return-void
.end method
