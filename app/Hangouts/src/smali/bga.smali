.class public Lbga;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:[B

.field private final h:[B


# direct methods
.method private constructor <init>(Ldyg;)V
    .locals 3

    .prologue
    .line 4018
    iget-object v0, p1, Ldyg;->b:Ldvn;

    const-wide/16 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 4019
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 4020
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AddBroadcastResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4022
    :cond_0
    iget-object v0, p1, Ldyg;->c:Ldxv;

    invoke-static {v0}, Ldxv;->toByteArray(Lepr;)[B

    move-result-object v0

    iput-object v0, p0, Lbga;->g:[B

    .line 4023
    iget-object v0, p1, Ldyg;->d:Ldzg;

    invoke-static {v0}, Ldzg;->toByteArray(Lepr;)[B

    move-result-object v0

    iput-object v0, p0, Lbga;->h:[B

    .line 4024
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 4028
    new-instance v0, Ldyg;

    invoke-direct {v0}, Ldyg;-><init>()V

    .line 4029
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldyg;

    .line 4030
    iget-object v1, v0, Ldyg;->b:Ldvn;

    invoke-static {v1}, Lbga;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4031
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldyg;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 4033
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbga;

    invoke-direct {v1, v0}, Lbga;-><init>(Ldyg;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4037
    invoke-virtual {p0}, Lbga;->g()Ldxv;

    move-result-object v0

    .line 4038
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Ldxv;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public g()Ldxv;
    .locals 3

    .prologue
    .line 4043
    :try_start_0
    new-instance v0, Ldxv;

    invoke-direct {v0}, Ldxv;-><init>()V

    iget-object v1, p0, Lbga;->g:[B

    invoke-static {v0, v1}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldxv;
    :try_end_0
    .catch Lepq; {:try_start_0 .. :try_end_0} :catch_0

    .line 4046
    :goto_0
    return-object v0

    .line 4044
    :catch_0
    move-exception v0

    .line 4045
    const-string v1, "Babel"

    const-string v2, "Parse failed"

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 4046
    const/4 v0, 0x0

    goto :goto_0
.end method
