.class public final Ldms;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldms;


# instance fields
.field public b:Ldgs;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2289
    const/4 v0, 0x0

    new-array v0, v0, [Ldms;

    sput-object v0, Ldms;->a:[Ldms;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2290
    invoke-direct {p0}, Lepn;-><init>()V

    .line 2293
    const/4 v0, 0x0

    iput-object v0, p0, Ldms;->b:Ldgs;

    .line 2290
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 2

    .prologue
    .line 2307
    const/4 v0, 0x0

    .line 2308
    iget-object v1, p0, Ldms;->b:Ldgs;

    if-eqz v1, :cond_0

    .line 2309
    const/4 v0, 0x1

    iget-object v1, p0, Ldms;->b:Ldgs;

    .line 2310
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2312
    :cond_0
    iget-object v1, p0, Ldms;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2313
    iput v0, p0, Ldms;->cachedSize:I

    .line 2314
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 2286
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldms;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldms;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldms;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldms;->b:Ldgs;

    if-nez v0, :cond_2

    new-instance v0, Ldgs;

    invoke-direct {v0}, Ldgs;-><init>()V

    iput-object v0, p0, Ldms;->b:Ldgs;

    :cond_2
    iget-object v0, p0, Ldms;->b:Ldgs;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 2298
    iget-object v0, p0, Ldms;->b:Ldgs;

    if-eqz v0, :cond_0

    .line 2299
    const/4 v0, 0x1

    iget-object v1, p0, Ldms;->b:Ldgs;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 2301
    :cond_0
    iget-object v0, p0, Ldms;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 2303
    return-void
.end method
