.class public final Ldco;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldco;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field public d:Ldcp;

.field public e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 284
    const/4 v0, 0x0

    new-array v0, v0, [Ldco;

    sput-object v0, Ldco;->a:[Ldco;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 285
    invoke-direct {p0}, Lepn;-><init>()V

    .line 297
    iput-object v0, p0, Ldco;->c:Ljava/lang/Integer;

    .line 300
    iput-object v0, p0, Ldco;->d:Ldcp;

    .line 285
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 325
    const/4 v0, 0x0

    .line 326
    iget-object v1, p0, Ldco;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 327
    const/4 v0, 0x1

    iget-object v1, p0, Ldco;->b:Ljava/lang/String;

    .line 328
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 330
    :cond_0
    iget-object v1, p0, Ldco;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 331
    const/4 v1, 0x2

    iget-object v2, p0, Ldco;->c:Ljava/lang/Integer;

    .line 332
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 334
    :cond_1
    iget-object v1, p0, Ldco;->d:Ldcp;

    if-eqz v1, :cond_2

    .line 335
    const/4 v1, 0x3

    iget-object v2, p0, Ldco;->d:Ldcp;

    .line 336
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 338
    :cond_2
    iget-object v1, p0, Ldco;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 339
    const/4 v1, 0x4

    iget-object v2, p0, Ldco;->e:Ljava/lang/String;

    .line 340
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 342
    :cond_3
    iget-object v1, p0, Ldco;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 343
    iput v0, p0, Ldco;->cachedSize:I

    .line 344
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 281
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldco;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldco;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldco;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldco;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldco;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldco;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldco;->d:Ldcp;

    if-nez v0, :cond_4

    new-instance v0, Ldcp;

    invoke-direct {v0}, Ldcp;-><init>()V

    iput-object v0, p0, Ldco;->d:Ldcp;

    :cond_4
    iget-object v0, p0, Ldco;->d:Ldcp;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldco;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Ldco;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 308
    const/4 v0, 0x1

    iget-object v1, p0, Ldco;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 310
    :cond_0
    iget-object v0, p0, Ldco;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 311
    const/4 v0, 0x2

    iget-object v1, p0, Ldco;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 313
    :cond_1
    iget-object v0, p0, Ldco;->d:Ldcp;

    if-eqz v0, :cond_2

    .line 314
    const/4 v0, 0x3

    iget-object v1, p0, Ldco;->d:Ldcp;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 316
    :cond_2
    iget-object v0, p0, Ldco;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 317
    const/4 v0, 0x4

    iget-object v1, p0, Ldco;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 319
    :cond_3
    iget-object v0, p0, Ldco;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 321
    return-void
.end method
