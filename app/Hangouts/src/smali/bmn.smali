.class public Lbmn;
.super Lbmf;
.source "PG"


# instance fields
.field protected l:J


# direct methods
.method public constructor <init>(Lyj;Lbmh;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 579
    invoke-direct {p0, p1, p2}, Lbmf;-><init>(Lyj;Lbmh;)V

    .line 581
    sget-boolean v0, Lbmn;->a:Z

    if-eqz v0, :cond_0

    .line 582
    const-string v0, "Babel"

    const-string v1, "[Notifications] Single Conversation"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    :cond_0
    iget-object v0, p2, Lbmh;->b:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmj;

    .line 585
    iget-object v1, v0, Lbmj;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lbmn;->a(Ljava/lang/String;)V

    .line 586
    iget-object v1, v0, Lbmj;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lbmn;->b(Ljava/lang/String;)V

    .line 588
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v3

    .line 589
    sget-boolean v1, Lbmn;->a:Z

    if-eqz v1, :cond_1

    .line 590
    const-string v1, "Babel"

    const-string v2, "single message needs notification"

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    :cond_1
    iget-object v1, v0, Lbmj;->h:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbml;

    .line 594
    iget-object v2, v1, Lbml;->c:Ljava/lang/String;

    iput-object v2, p0, Lbmn;->g:Ljava/lang/String;

    .line 595
    iget v2, v1, Lbml;->d:I

    iput v2, p0, Lbmn;->h:I

    .line 597
    iget-object v2, p0, Lbmn;->g:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, v1, Lbml;->b:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 600
    sget v2, Lh;->jS:I

    .line 601
    iget v4, p0, Lbmn;->h:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    .line 602
    sget v2, Lh;->jQ:I

    .line 611
    :cond_2
    :goto_0
    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lbmn;->e:Ljava/lang/CharSequence;

    .line 615
    :goto_1
    iget-boolean v2, v0, Lbmj;->c:Z

    if-eqz v2, :cond_7

    .line 618
    iget-object v2, p0, Lbmn;->e:Ljava/lang/CharSequence;

    iput-object v2, p0, Lbmn;->c:Ljava/lang/CharSequence;

    .line 619
    iget-object v2, v1, Lbml;->f:Ljava/lang/String;

    iput-object v2, p0, Lbmn;->b:Ljava/lang/String;

    .line 621
    iget-object v1, v1, Lbml;->e:Ljava/lang/String;

    iget-object v2, p0, Lbmn;->e:Ljava/lang/CharSequence;

    iget-object v3, p0, Lbmn;->g:Ljava/lang/String;

    iget v4, p0, Lbmn;->h:I

    invoke-static {v1, v2, v3, v4}, Lbne;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lbmn;->e:Ljava/lang/CharSequence;

    .line 624
    iget-object v1, v0, Lbmj;->f:Ljava/lang/String;

    iput-object v1, p0, Lbmn;->d:Ljava/lang/String;

    .line 631
    :goto_2
    iget-object v1, p0, Lbmn;->m:Lyj;

    iget-object v2, v0, Lbmj;->a:Ljava/lang/String;

    iget v3, v0, Lbmj;->e:I

    invoke-static {v1, v2, v3}, Lbbl;->a(Lyj;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    iput-object v1, p0, Lbmn;->f:Landroid/content/Intent;

    .line 633
    iget-object v1, p0, Lbmn;->f:Landroid/content/Intent;

    const-string v2, "reset_chat_notifications"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 634
    iget-object v1, p0, Lbmn;->f:Landroid/content/Intent;

    const-string v2, "conversation_opened_impression"

    const/16 v3, 0x665

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 636
    iget-object v1, p0, Lbmn;->f:Landroid/content/Intent;

    const/high16 v2, 0x24000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 638
    iget-wide v0, v0, Lbmj;->m:J

    iput-wide v0, p0, Lbmn;->l:J

    .line 639
    return-void

    .line 603
    :cond_3
    iget v4, p0, Lbmn;->h:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_4

    .line 604
    sget v2, Lh;->ke:I

    goto :goto_0

    .line 605
    :cond_4
    iget v4, p0, Lbmn;->h:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_5

    .line 606
    sget v2, Lh;->jT:I

    goto :goto_0

    .line 607
    :cond_5
    iget v4, p0, Lbmn;->h:I

    const/4 v5, 0x6

    if-ne v4, v5, :cond_2

    .line 608
    sget v2, Lh;->kd:I

    goto :goto_0

    .line 613
    :cond_6
    iget-object v2, v1, Lbml;->b:Ljava/lang/CharSequence;

    iput-object v2, p0, Lbmn;->e:Ljava/lang/CharSequence;

    goto :goto_1

    .line 628
    :cond_7
    iget-object v1, v0, Lbmj;->h:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbml;

    .line 629
    iget-object v1, v1, Lbml;->f:Ljava/lang/String;

    iput-object v1, p0, Lbmn;->d:Ljava/lang/String;

    goto :goto_2
.end method


# virtual methods
.method protected a(Lbk;)Lbu;
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 643
    iget-object v0, p0, Lbmn;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lbk;->a(Ljava/lang/CharSequence;)Lbk;

    move-result-object v0

    .line 644
    invoke-virtual {p0}, Lbmn;->n()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbk;->c(Ljava/lang/CharSequence;)Lbk;

    .line 647
    iget-object v0, p0, Lbmn;->j:Lbmh;

    iget-object v0, v0, Lbmh;->b:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmj;

    .line 648
    iget-object v3, v0, Lbmj;->h:Ljava/util/List;

    .line 649
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    .line 653
    iget-object v2, p0, Lbmn;->e:Ljava/lang/CharSequence;

    invoke-virtual {p1, v2}, Lbk;->b(Ljava/lang/CharSequence;)Lbk;

    .line 655
    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 656
    sget-boolean v1, Lbmn;->a:Z

    if-eqz v1, :cond_0

    .line 657
    const-string v1, "Babel"

    const-string v2, "SINGLE MESSAGE"

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    :cond_0
    iget-object v1, p0, Lbmn;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 660
    iget-object v1, p0, Lbmn;->g:Ljava/lang/String;

    const-string v2, "//"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 661
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "http:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbmn;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbmn;->g:Ljava/lang/String;

    .line 665
    :cond_1
    iget-object v1, p0, Lbmn;->g:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget v1, p0, Lbmn;->h:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    .line 667
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbml;

    .line 668
    iget-object v1, v1, Lbml;->e:Ljava/lang/String;

    .line 673
    iget v2, p0, Lbmn;->h:I

    .line 674
    invoke-static {v1, v2}, Lbne;->a(Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 679
    iget-boolean v3, v0, Lbmj;->c:Z

    if-nez v3, :cond_9

    .line 681
    iget v1, p0, Lbmn;->h:I

    invoke-static {v4, v1}, Lbne;->a(Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v1

    move-object v3, v4

    .line 684
    :goto_0
    invoke-virtual {p1, v1}, Lbk;->b(Ljava/lang/CharSequence;)Lbk;

    .line 685
    invoke-virtual {p1, v2}, Lbk;->c(Ljava/lang/CharSequence;)Lbk;

    .line 687
    new-instance v1, Lbi;

    invoke-direct {v1, p1}, Lbi;-><init>(Lbk;)V

    .line 688
    invoke-static {v3, v4, v4, v5}, Lbne;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbi;->a(Ljava/lang/CharSequence;)Lbi;

    move-result-object v1

    .line 731
    :goto_1
    iget-wide v2, v0, Lbmj;->g:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {p1, v2, v3}, Lbk;->a(J)Lbk;

    .line 732
    return-object v1

    .line 692
    :cond_2
    new-instance v1, Lbj;

    invoke-direct {v1, p1}, Lbj;-><init>(Lbk;)V

    iget-object v2, p0, Lbmn;->e:Ljava/lang/CharSequence;

    .line 693
    invoke-virtual {v1, v2}, Lbj;->b(Ljava/lang/CharSequence;)Lbj;

    move-result-object v1

    goto :goto_1

    .line 696
    :cond_3
    sget-boolean v1, Lbmn;->a:Z

    if-eqz v1, :cond_4

    .line 697
    const-string v1, "Babel"

    const-string v2, "MULTIPLE MESSAGE for the same conversation"

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    :cond_4
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 704
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_2
    if-ltz v2, :cond_8

    .line 705
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbmo;

    .line 706
    check-cast v1, Lbml;

    .line 707
    iget-object v6, v1, Lbml;->c:Ljava/lang/String;

    iput-object v6, p0, Lbmn;->g:Ljava/lang/String;

    .line 708
    iget v6, v1, Lbml;->d:I

    iput v6, p0, Lbmn;->h:I

    .line 709
    iget-object v6, v1, Lbml;->b:Ljava/lang/CharSequence;

    .line 710
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_5

    iget-object v7, p0, Lbmn;->g:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 711
    :cond_5
    iget-boolean v7, v0, Lbmj;->c:Z

    if-eqz v7, :cond_7

    .line 713
    iget-object v1, v1, Lbml;->e:Ljava/lang/String;

    iget-object v7, p0, Lbmn;->g:Ljava/lang/String;

    iget v8, p0, Lbmn;->h:I

    invoke-static {v1, v6, v7, v8}, Lbne;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 720
    :goto_3
    invoke-virtual {v5, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 721
    if-lez v2, :cond_6

    .line 722
    const/16 v1, 0xa

    invoke-virtual {v5, v1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 704
    :cond_6
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_2

    .line 717
    :cond_7
    iget-object v1, p0, Lbmn;->g:Ljava/lang/String;

    iget v7, p0, Lbmn;->h:I

    invoke-static {v4, v6, v1, v7}, Lbne;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_3

    .line 728
    :cond_8
    new-instance v1, Lbj;

    invoke-direct {v1, p1}, Lbj;-><init>(Lbk;)V

    .line 729
    invoke-virtual {v1, v5}, Lbj;->b(Ljava/lang/CharSequence;)Lbj;

    move-result-object v1

    goto :goto_1

    :cond_9
    move-object v3, v1

    move-object v1, v2

    goto/16 :goto_0
.end method
