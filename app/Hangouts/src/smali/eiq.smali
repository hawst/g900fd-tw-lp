.class public final Leiq;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leiq;


# instance fields
.field public b:Leir;

.field public c:Leit;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 303
    const/4 v0, 0x0

    new-array v0, v0, [Leiq;

    sput-object v0, Leiq;->a:[Leiq;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 304
    invoke-direct {p0}, Lepn;-><init>()V

    .line 307
    iput-object v0, p0, Leiq;->b:Leir;

    .line 310
    iput-object v0, p0, Leiq;->c:Leit;

    .line 304
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 337
    const/4 v0, 0x0

    .line 338
    iget-object v1, p0, Leiq;->b:Leir;

    if-eqz v1, :cond_0

    .line 339
    const/4 v0, 0x1

    iget-object v1, p0, Leiq;->b:Leir;

    .line 340
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 342
    :cond_0
    iget-object v1, p0, Leiq;->c:Leit;

    if-eqz v1, :cond_1

    .line 343
    const/4 v1, 0x2

    iget-object v2, p0, Leiq;->c:Leit;

    .line 344
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 346
    :cond_1
    iget-object v1, p0, Leiq;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 347
    const/4 v1, 0x3

    iget-object v2, p0, Leiq;->d:Ljava/lang/String;

    .line 348
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 350
    :cond_2
    iget-object v1, p0, Leiq;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 351
    const/4 v1, 0x4

    iget-object v2, p0, Leiq;->e:Ljava/lang/String;

    .line 352
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 354
    :cond_3
    iget-object v1, p0, Leiq;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 355
    iput v0, p0, Leiq;->cachedSize:I

    .line 356
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 300
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leiq;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leiq;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leiq;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Leiq;->b:Leir;

    if-nez v0, :cond_2

    new-instance v0, Leir;

    invoke-direct {v0}, Leir;-><init>()V

    iput-object v0, p0, Leiq;->b:Leir;

    :cond_2
    iget-object v0, p0, Leiq;->b:Leir;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Leiq;->c:Leit;

    if-nez v0, :cond_3

    new-instance v0, Leit;

    invoke-direct {v0}, Leit;-><init>()V

    iput-object v0, p0, Leiq;->c:Leit;

    :cond_3
    iget-object v0, p0, Leiq;->c:Leit;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leiq;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leiq;->e:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 319
    iget-object v0, p0, Leiq;->b:Leir;

    if-eqz v0, :cond_0

    .line 320
    const/4 v0, 0x1

    iget-object v1, p0, Leiq;->b:Leir;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 322
    :cond_0
    iget-object v0, p0, Leiq;->c:Leit;

    if-eqz v0, :cond_1

    .line 323
    const/4 v0, 0x2

    iget-object v1, p0, Leiq;->c:Leit;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 325
    :cond_1
    iget-object v0, p0, Leiq;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 326
    const/4 v0, 0x3

    iget-object v1, p0, Leiq;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 328
    :cond_2
    iget-object v0, p0, Leiq;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 329
    const/4 v0, 0x4

    iget-object v1, p0, Leiq;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 331
    :cond_3
    iget-object v0, p0, Leiq;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 333
    return-void
.end method
