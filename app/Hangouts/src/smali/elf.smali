.class public final Lelf;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lelf;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Lemw;

.field public d:Lekz;

.field public e:Leky;

.field public f:Lemh;

.field public g:[Lema;

.field public h:[Lemu;

.field public i:Lemk;

.field public j:[Leow;

.field public k:Lemo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lelf;

    sput-object v0, Lelf;->a:[Lelf;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 15
    iput-object v1, p0, Lelf;->c:Lemw;

    .line 18
    iput-object v1, p0, Lelf;->d:Lekz;

    .line 21
    iput-object v1, p0, Lelf;->e:Leky;

    .line 24
    iput-object v1, p0, Lelf;->f:Lemh;

    .line 27
    sget-object v0, Lema;->a:[Lema;

    iput-object v0, p0, Lelf;->g:[Lema;

    .line 30
    sget-object v0, Lemu;->a:[Lemu;

    iput-object v0, p0, Lelf;->h:[Lemu;

    .line 33
    iput-object v1, p0, Lelf;->i:Lemk;

    .line 36
    sget-object v0, Leow;->a:[Leow;

    iput-object v0, p0, Lelf;->j:[Leow;

    .line 39
    iput-object v1, p0, Lelf;->k:Lemo;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 93
    iget-object v0, p0, Lelf;->b:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 94
    const/4 v0, 0x1

    iget-object v2, p0, Lelf;->b:Ljava/lang/String;

    .line 95
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 97
    :goto_0
    iget-object v2, p0, Lelf;->d:Lekz;

    if-eqz v2, :cond_0

    .line 98
    const/4 v2, 0x2

    iget-object v3, p0, Lelf;->d:Lekz;

    .line 99
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 101
    :cond_0
    iget-object v2, p0, Lelf;->i:Lemk;

    if-eqz v2, :cond_1

    .line 102
    const/4 v2, 0x3

    iget-object v3, p0, Lelf;->i:Lemk;

    .line 103
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 105
    :cond_1
    iget-object v2, p0, Lelf;->c:Lemw;

    if-eqz v2, :cond_2

    .line 106
    const/4 v2, 0x4

    iget-object v3, p0, Lelf;->c:Lemw;

    .line 107
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 109
    :cond_2
    iget-object v2, p0, Lelf;->e:Leky;

    if-eqz v2, :cond_3

    .line 110
    const/4 v2, 0x5

    iget-object v3, p0, Lelf;->e:Leky;

    .line 111
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 113
    :cond_3
    iget-object v2, p0, Lelf;->f:Lemh;

    if-eqz v2, :cond_4

    .line 114
    const/4 v2, 0x7

    iget-object v3, p0, Lelf;->f:Lemh;

    .line 115
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 117
    :cond_4
    iget-object v2, p0, Lelf;->g:[Lema;

    if-eqz v2, :cond_6

    .line 118
    iget-object v3, p0, Lelf;->g:[Lema;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    .line 119
    if-eqz v5, :cond_5

    .line 120
    const/16 v6, 0x8

    .line 121
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 118
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 125
    :cond_6
    iget-object v2, p0, Lelf;->h:[Lemu;

    if-eqz v2, :cond_8

    .line 126
    iget-object v3, p0, Lelf;->h:[Lemu;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_8

    aget-object v5, v3, v2

    .line 127
    if-eqz v5, :cond_7

    .line 128
    const/16 v6, 0x9

    .line 129
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 126
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 133
    :cond_8
    iget-object v2, p0, Lelf;->j:[Leow;

    if-eqz v2, :cond_a

    .line 134
    iget-object v2, p0, Lelf;->j:[Leow;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_a

    aget-object v4, v2, v1

    .line 135
    if-eqz v4, :cond_9

    .line 136
    const/16 v5, 0xa

    .line 137
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 134
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 141
    :cond_a
    iget-object v1, p0, Lelf;->k:Lemo;

    if-eqz v1, :cond_b

    .line 142
    const/16 v1, 0xb

    iget-object v2, p0, Lelf;->k:Lemo;

    .line 143
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 145
    :cond_b
    iget-object v1, p0, Lelf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 146
    iput v0, p0, Lelf;->cachedSize:I

    .line 147
    return v0

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lelf;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lelf;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lelf;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lelf;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lelf;->d:Lekz;

    if-nez v0, :cond_2

    new-instance v0, Lekz;

    invoke-direct {v0}, Lekz;-><init>()V

    iput-object v0, p0, Lelf;->d:Lekz;

    :cond_2
    iget-object v0, p0, Lelf;->d:Lekz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lelf;->i:Lemk;

    if-nez v0, :cond_3

    new-instance v0, Lemk;

    invoke-direct {v0}, Lemk;-><init>()V

    iput-object v0, p0, Lelf;->i:Lemk;

    :cond_3
    iget-object v0, p0, Lelf;->i:Lemk;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lelf;->c:Lemw;

    if-nez v0, :cond_4

    new-instance v0, Lemw;

    invoke-direct {v0}, Lemw;-><init>()V

    iput-object v0, p0, Lelf;->c:Lemw;

    :cond_4
    iget-object v0, p0, Lelf;->c:Lemw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lelf;->e:Leky;

    if-nez v0, :cond_5

    new-instance v0, Leky;

    invoke-direct {v0}, Leky;-><init>()V

    iput-object v0, p0, Lelf;->e:Leky;

    :cond_5
    iget-object v0, p0, Lelf;->e:Leky;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lelf;->f:Lemh;

    if-nez v0, :cond_6

    new-instance v0, Lemh;

    invoke-direct {v0}, Lemh;-><init>()V

    iput-object v0, p0, Lelf;->f:Lemh;

    :cond_6
    iget-object v0, p0, Lelf;->f:Lemh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lelf;->g:[Lema;

    if-nez v0, :cond_8

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lema;

    iget-object v3, p0, Lelf;->g:[Lema;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lelf;->g:[Lema;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Lelf;->g:[Lema;

    :goto_2
    iget-object v2, p0, Lelf;->g:[Lema;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Lelf;->g:[Lema;

    new-instance v3, Lema;

    invoke-direct {v3}, Lema;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lelf;->g:[Lema;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lelf;->g:[Lema;

    array-length v0, v0

    goto :goto_1

    :cond_9
    iget-object v2, p0, Lelf;->g:[Lema;

    new-instance v3, Lema;

    invoke-direct {v3}, Lema;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lelf;->g:[Lema;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lelf;->h:[Lemu;

    if-nez v0, :cond_b

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lemu;

    iget-object v3, p0, Lelf;->h:[Lemu;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lelf;->h:[Lemu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    iput-object v2, p0, Lelf;->h:[Lemu;

    :goto_4
    iget-object v2, p0, Lelf;->h:[Lemu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_c

    iget-object v2, p0, Lelf;->h:[Lemu;

    new-instance v3, Lemu;

    invoke-direct {v3}, Lemu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lelf;->h:[Lemu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_b
    iget-object v0, p0, Lelf;->h:[Lemu;

    array-length v0, v0

    goto :goto_3

    :cond_c
    iget-object v2, p0, Lelf;->h:[Lemu;

    new-instance v3, Lemu;

    invoke-direct {v3}, Lemu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lelf;->h:[Lemu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    const/16 v0, 0x52

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lelf;->j:[Leow;

    if-nez v0, :cond_e

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Leow;

    iget-object v3, p0, Lelf;->j:[Leow;

    if-eqz v3, :cond_d

    iget-object v3, p0, Lelf;->j:[Leow;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_d
    iput-object v2, p0, Lelf;->j:[Leow;

    :goto_6
    iget-object v2, p0, Lelf;->j:[Leow;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_f

    iget-object v2, p0, Lelf;->j:[Leow;

    new-instance v3, Leow;

    invoke-direct {v3}, Leow;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lelf;->j:[Leow;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_e
    iget-object v0, p0, Lelf;->j:[Leow;

    array-length v0, v0

    goto :goto_5

    :cond_f
    iget-object v2, p0, Lelf;->j:[Leow;

    new-instance v3, Leow;

    invoke-direct {v3}, Leow;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lelf;->j:[Leow;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lelf;->k:Lemo;

    if-nez v0, :cond_10

    new-instance v0, Lemo;

    invoke-direct {v0}, Lemo;-><init>()V

    iput-object v0, p0, Lelf;->k:Lemo;

    :cond_10
    iget-object v0, p0, Lelf;->k:Lemo;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 44
    iget-object v1, p0, Lelf;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 45
    const/4 v1, 0x1

    iget-object v2, p0, Lelf;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 47
    :cond_0
    iget-object v1, p0, Lelf;->d:Lekz;

    if-eqz v1, :cond_1

    .line 48
    const/4 v1, 0x2

    iget-object v2, p0, Lelf;->d:Lekz;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 50
    :cond_1
    iget-object v1, p0, Lelf;->i:Lemk;

    if-eqz v1, :cond_2

    .line 51
    const/4 v1, 0x3

    iget-object v2, p0, Lelf;->i:Lemk;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 53
    :cond_2
    iget-object v1, p0, Lelf;->c:Lemw;

    if-eqz v1, :cond_3

    .line 54
    const/4 v1, 0x4

    iget-object v2, p0, Lelf;->c:Lemw;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 56
    :cond_3
    iget-object v1, p0, Lelf;->e:Leky;

    if-eqz v1, :cond_4

    .line 57
    const/4 v1, 0x5

    iget-object v2, p0, Lelf;->e:Leky;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 59
    :cond_4
    iget-object v1, p0, Lelf;->f:Lemh;

    if-eqz v1, :cond_5

    .line 60
    const/4 v1, 0x7

    iget-object v2, p0, Lelf;->f:Lemh;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 62
    :cond_5
    iget-object v1, p0, Lelf;->g:[Lema;

    if-eqz v1, :cond_7

    .line 63
    iget-object v2, p0, Lelf;->g:[Lema;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 64
    if-eqz v4, :cond_6

    .line 65
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 63
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 69
    :cond_7
    iget-object v1, p0, Lelf;->h:[Lemu;

    if-eqz v1, :cond_9

    .line 70
    iget-object v2, p0, Lelf;->h:[Lemu;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_9

    aget-object v4, v2, v1

    .line 71
    if-eqz v4, :cond_8

    .line 72
    const/16 v5, 0x9

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 70
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 76
    :cond_9
    iget-object v1, p0, Lelf;->j:[Leow;

    if-eqz v1, :cond_b

    .line 77
    iget-object v1, p0, Lelf;->j:[Leow;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_b

    aget-object v3, v1, v0

    .line 78
    if-eqz v3, :cond_a

    .line 79
    const/16 v4, 0xa

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 77
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 83
    :cond_b
    iget-object v0, p0, Lelf;->k:Lemo;

    if-eqz v0, :cond_c

    .line 84
    const/16 v0, 0xb

    iget-object v1, p0, Lelf;->k:Lemo;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 86
    :cond_c
    iget-object v0, p0, Lelf;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 88
    return-void
.end method
