.class final Lamh;
.super Landroid/widget/Filter;
.source "PG"


# instance fields
.field final synthetic a:Lame;


# direct methods
.method constructor <init>(Lame;)V
    .locals 0

    .prologue
    .line 382
    iput-object p1, p0, Lamh;->a:Lame;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method public convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 448
    check-cast p1, Landroid/database/Cursor;

    .line 450
    const-string v0, "name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 451
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 452
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 454
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 4

    .prologue
    .line 395
    new-instance v0, Lamj;

    invoke-direct {v0}, Lamj;-><init>()V

    .line 396
    iget-object v1, p0, Lamh;->a:Lame;

    invoke-static {v1}, Lame;->d(Lame;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lami;

    invoke-direct {v2, p0, p1, v0}, Lami;-><init>(Lamh;Ljava/lang/CharSequence;Lamj;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 414
    :try_start_0
    const-string v1, "babel_people_search_list_filter_timeout"

    const/4 v2, 0x3

    .line 415
    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v1

    int-to-long v1, v1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 414
    invoke-virtual {v0, v1, v2, v3}, Lamj;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 422
    const-string v0, "Babel"

    const-string v1, "PeopleSearchListAdapter: filter loading takes too long"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 430
    :cond_0
    :goto_0
    new-instance v0, Landroid/widget/Filter$FilterResults;

    invoke-direct {v0}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 431
    const/4 v1, 0x1

    iput v1, v0, Landroid/widget/Filter$FilterResults;->count:I

    .line 432
    return-object v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 1

    .prologue
    .line 439
    iget-object v0, p0, Lamh;->a:Lame;

    invoke-static {v0}, Lame;->e(Lame;)V

    .line 440
    return-void
.end method
