.class final Lbpo;
.super Ljava/lang/Thread;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

.field final synthetic b:Lbpn;


# direct methods
.method constructor <init>(Lbpn;Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;)V
    .locals 0

    .prologue
    .line 1135
    iput-object p1, p0, Lbpo;->b:Lbpn;

    iput-object p2, p0, Lbpo;->a:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 27

    .prologue
    .line 1141
    move-object/from16 v0, p0

    iget-object v0, v0, Lbpo;->b:Lbpn;

    move-object/from16 v20, v0

    :goto_0
    invoke-virtual/range {v20 .. v20}, Lbpn;->d()Lbpp;

    move-result-object v21

    if-eqz v21, :cond_18

    move-object/from16 v0, v21

    iget-object v2, v0, Lbpp;->c:Ljava/lang/String;

    invoke-static {v2}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-virtual/range {v20 .. v21}, Lbpn;->a(Lbpp;)V

    goto :goto_0

    :cond_0
    invoke-virtual/range {v21 .. v21}, Lbpp;->a()Lbea;

    move-result-object v14

    const/4 v3, 0x0

    instance-of v4, v14, Lbfb;

    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v5

    if-eqz v5, :cond_1c

    if-eqz v4, :cond_1c

    move-object v3, v14

    check-cast v3, Lbfb;

    invoke-virtual {v3}, Lbfb;->h()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v16, v3

    :goto_1
    invoke-virtual/range {v20 .. v21}, Lbpn;->b(Lbpp;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual/range {v21 .. v21}, Lbpp;->d()Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_1
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "Babel_RequestWriter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "write request: removing expired or replaced item: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    if-nez v3, :cond_3

    move-object/from16 v0, v20

    iget-object v2, v0, Lbpn;->h:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    sget-object v2, Lbph;->a:Lbph;

    move-object/from16 v0, v21

    invoke-static {v0, v2}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Lbpp;Lbph;)V

    :cond_3
    invoke-virtual/range {v20 .. v21}, Lbpn;->a(Lbpp;)V

    goto :goto_0

    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    move-object/from16 v0, v20

    iget-object v3, v0, Lbpn;->h:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-virtual {v3}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->b()Z

    move-result v3

    if-eqz v3, :cond_19

    const/16 v17, 0x0

    :try_start_0
    invoke-virtual {v14}, Lbea;->t()Z

    move-result v3

    if-eqz v3, :cond_8

    move-object/from16 v0, v20

    iget-object v3, v0, Lbpn;->h:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->c(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;)Z

    move-result v3

    if-nez v3, :cond_8

    new-instance v2, Lbph;

    const/16 v3, 0x65

    invoke-direct {v2, v3}, Lbph;-><init>(I)V

    throw v2
    :try_end_0
    .catch Lbph; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :catch_0
    move-exception v2

    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-string v5, "Babel_RequestWriter"

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "error sending %s; took %d ms (error code == %d)"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v21, v8, v9

    const/4 v9, 0x1

    sub-long v3, v3, v22

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v8, v9

    const/4 v3, 0x2

    invoke-virtual {v2}, Lbph;->e()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v8, v3

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v20

    iget-object v4, v0, Lbpn;->d:Ljava/lang/Object;

    monitor-enter v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    move-object/from16 v0, v20

    iget-object v3, v0, Lbpn;->h:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-virtual {v3}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->h()V

    move-object/from16 v0, v20

    iget-object v3, v0, Lbpn;->f:Ljava/lang/String;

    if-eqz v3, :cond_5

    move-object/from16 v0, v20

    iget-object v3, v0, Lbpn;->f:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Ljava/lang/String;)V

    :cond_5
    move-object/from16 v0, v20

    iget-object v3, v0, Lbpn;->f:Ljava/lang/String;

    if-nez v3, :cond_13

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lbpp;->a(Lbph;)Z

    move-result v3

    if-eqz v3, :cond_13

    const/4 v3, 0x1

    :goto_2
    if-nez v3, :cond_14

    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "Babel_RequestWriter"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "not retrying "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; error code == "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lbph;->e()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    move-object/from16 v0, v20

    iget-object v3, v0, Lbpn;->h:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    move-object/from16 v0, v21

    invoke-static {v0, v2}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Lbpp;Lbph;)V

    invoke-virtual/range {v20 .. v21}, Lbpn;->a(Lbpp;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    move/from16 v3, v17

    :goto_3
    const/4 v2, 0x0

    :try_start_3
    move-object/from16 v0, v20

    iput-object v2, v0, Lbpn;->e:Lbpp;

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_7

    move-object/from16 v0, v20

    iget-object v4, v0, Lbpn;->d:Ljava/lang/Object;

    monitor-enter v4

    :try_start_4
    move-object/from16 v0, v20

    iget-object v2, v0, Lbpn;->h:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-virtual {v2}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->c()V

    if-eqz v3, :cond_7

    invoke-virtual/range {v20 .. v20}, Lbpn;->a()V

    :cond_7
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v4

    throw v2

    :cond_8
    :try_start_5
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->k()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v3, v5, v7

    if-lez v3, :cond_a

    move-object/from16 v0, v20

    iget-object v3, v0, Lbpn;->h:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->d(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;)Ljava/util/Random;

    move-result-object v3

    const/16 v5, 0x64

    invoke-virtual {v3, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    int-to-long v5, v3

    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->k()J

    move-result-wide v7

    cmp-long v3, v5, v7

    if-gez v3, :cond_a

    new-instance v2, Lbph;

    const/16 v3, 0x66

    const/4 v4, 0x0

    const-wide/16 v5, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lbph;-><init>(ILjava/lang/Exception;JZLjava/lang/String;)V

    throw v2
    :try_end_5
    .catch Lbph; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v2

    :goto_4
    move-object/from16 v0, v20

    iget-object v3, v0, Lbpn;->d:Ljava/lang/Object;

    monitor-enter v3

    :try_start_6
    move-object/from16 v0, v20

    iget-object v4, v0, Lbpn;->h:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-virtual {v4}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->c()V

    if-eqz v17, :cond_9

    invoke-virtual/range {v20 .. v20}, Lbpn;->a()V

    :cond_9
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_5

    throw v2

    :cond_a
    if-eqz v16, :cond_b

    :try_start_7
    const-string v3, "Babel_Stress"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Sending stress message in RequestWriter:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d(I)V

    move-object/from16 v0, v20

    iget-object v3, v0, Lbpn;->h:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    move-object/from16 v0, v20

    iget-object v5, v0, Lbpn;->h:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-static {v5}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;)Lbpk;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v14, v3, v0, v5}, Lbea;->a(Landroid/content/Context;Lbpp;Lbpk;)Lbfz;

    move-result-object v24

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v25

    if-eqz v4, :cond_c

    move-object v0, v14

    check-cast v0, Lbfb;

    move-object v15, v0

    const-wide/16 v3, 0x3e8

    mul-long v3, v3, v22

    if-eqz v24, :cond_10

    const-wide/16 v5, 0x3e8

    mul-long v5, v5, v25

    move-wide/from16 v18, v5

    :goto_5
    iget-object v5, v15, Lbfb;->i:Ljava/lang/String;

    invoke-static {v5}, Lyt;->d(Ljava/lang/String;)J

    move-result-wide v10

    const/4 v5, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    iget-object v12, v15, Lbfb;->c:Ljava/lang/String;

    const/4 v13, 0x0

    invoke-static/range {v2 .. v13}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    const-wide/16 v3, 0x0

    cmp-long v3, v18, v3

    if-lez v3, :cond_c

    const/16 v5, 0xc

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    iget-object v12, v15, Lbfb;->c:Ljava/lang/String;

    const/4 v13, 0x0

    move-wide/from16 v3, v18

    invoke-static/range {v2 .. v13}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    :cond_c
    if-eqz v24, :cond_12

    const-wide/16 v2, 0x3e8

    mul-long v2, v2, v22

    move-object/from16 v0, v24

    invoke-virtual {v0, v2, v3}, Lbfz;->a(J)V

    const-wide/16 v2, 0x3e8

    mul-long v2, v2, v25

    move-object/from16 v0, v24

    invoke-virtual {v0, v2, v3}, Lbfz;->b(J)V

    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v2

    if-eqz v2, :cond_d

    sub-long v3, v25, v22

    move-object/from16 v0, v21

    iget-wide v5, v0, Lbpp;->i:J

    sub-long v5, v25, v5

    const-string v7, "Babel_RequestWriter"

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "%ssent %s; took %d ms (%d total); errorDescription: %s; debug_url: %s"

    const/4 v2, 0x6

    new-array v10, v2, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-wide/16 v12, 0x7d0

    cmp-long v2, v5, v12

    if-lez v2, :cond_11

    const-string v2, "!!!!! "

    :goto_6
    aput-object v2, v10, v11

    const/4 v2, 0x1

    aput-object v21, v10, v2

    const/4 v2, 0x2

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v10, v2

    const/4 v2, 0x3

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v10, v2

    const/4 v2, 0x4

    move-object/from16 v0, v24

    iget-object v3, v0, Lbfz;->c:Lbht;

    iget-object v3, v3, Lbht;->a:Ljava/lang/String;

    aput-object v3, v10, v2

    const/4 v2, 0x5

    move-object/from16 v0, v24

    iget-object v3, v0, Lbfz;->c:Lbht;

    iget-object v3, v3, Lbht;->c:Ljava/lang/String;

    aput-object v3, v10, v2

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v7, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    if-eqz v16, :cond_e

    const-string v2, "Babel_Stress"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Sending stress message succeeded in RequestWriter:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Lbfz;->a(Lbea;)V

    move-object/from16 v0, v21

    iget-object v2, v0, Lbpp;->c:Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;Lbpp;Lbfz;)V

    move-object/from16 v0, v20

    iget-object v2, v0, Lbpn;->h:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    const-wide/16 v3, 0x1f4

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;J)V

    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v2

    if-eqz v2, :cond_f

    const-string v2, "Babel_RequestWriter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "request successful. removing from queue: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    const-wide/16 v2, 0x1f4

    move-object/from16 v0, v20

    iput-wide v2, v0, Lbpn;->g:J

    invoke-virtual/range {v20 .. v21}, Lbpn;->a(Lbpp;)V
    :try_end_7
    .catch Lbph; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-object/from16 v0, v20

    iget-object v3, v0, Lbpn;->d:Ljava/lang/Object;

    monitor-enter v3

    :try_start_8
    move-object/from16 v0, v20

    iget-object v2, v0, Lbpn;->h:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-virtual {v2}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->c()V

    monitor-exit v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto/16 :goto_0

    :catchall_2
    move-exception v2

    monitor-exit v3

    throw v2

    :cond_10
    const-wide/16 v5, 0x0

    move-wide/from16 v18, v5

    goto/16 :goto_5

    :cond_11
    :try_start_9
    const-string v2, ""

    goto/16 :goto_6

    :cond_12
    new-instance v2, Lbph;

    const/16 v3, 0x6f

    const-string v4, "Empty response received"

    invoke-direct {v2, v3, v4}, Lbph;-><init>(ILjava/lang/String;)V

    throw v2
    :try_end_9
    .catch Lbph; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :cond_13
    const/4 v3, 0x0

    goto/16 :goto_2

    :cond_14
    if-eqz v16, :cond_15

    :try_start_a
    const-string v3, "Babel_Stress"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Sending stress message failed with error="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lbph;->e()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", will retry:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_15
    invoke-static {v2}, Lbph;->a(Lbph;)J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v3, v5, v7

    if-lez v3, :cond_16

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v2}, Lbph;->a(Lbph;)J

    move-result-wide v7

    add-long/2addr v5, v7

    move-object/from16 v0, v21

    iput-wide v5, v0, Lbpp;->h:J

    :cond_16
    move-object/from16 v0, v20

    iget-object v3, v0, Lbpn;->h:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;)Lbpk;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Lbpk;->c(Lbpp;)V

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v5, 0xe10

    invoke-virtual {v3, v5, v6}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v5

    move-object/from16 v0, v20

    iget-wide v7, v0, Lbpn;->g:J

    const/4 v3, 0x1

    shl-long/2addr v7, v3

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v5

    move-object/from16 v0, v20

    iput-wide v5, v0, Lbpn;->g:J

    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v3

    if-eqz v3, :cond_17

    const-string v3, "Babel_RequestWriter"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "pausing queue after failed request:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; error code:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lbph;->e()I

    move-result v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    :cond_17
    const/4 v3, 0x1

    goto/16 :goto_3

    :catchall_3
    move-exception v2

    move/from16 v3, v17

    :goto_7
    :try_start_b
    monitor-exit v4

    throw v2
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    :catchall_4
    move-exception v2

    move/from16 v17, v3

    goto/16 :goto_4

    :catchall_5
    move-exception v2

    monitor-exit v3

    throw v2

    :cond_18
    move-object/from16 v0, v20

    iget-object v3, v0, Lbpn;->d:Ljava/lang/Object;

    monitor-enter v3

    :try_start_c
    move-object/from16 v0, v20

    iget-boolean v2, v0, Lbpn;->c:Z

    if-eqz v2, :cond_1b

    monitor-exit v3
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_6

    :cond_19
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v2

    if-eqz v2, :cond_1a

    const-string v2, "Babel_RequestWriter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "### queue \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    iget-object v4, v0, Lbpn;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' is finished"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1142
    :cond_1a
    return-void

    .line 1141
    :cond_1b
    monitor-exit v3

    goto/16 :goto_0

    :catchall_6
    move-exception v2

    monitor-exit v3

    throw v2

    :catchall_7
    move-exception v2

    goto :goto_7

    :cond_1c
    move-object/from16 v16, v3

    goto/16 :goto_1
.end method
