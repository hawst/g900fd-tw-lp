.class public final Lcvi;
.super Ljava/lang/Object;

# interfaces
.implements Lft;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:Lclj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcfv;Lcfw;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcvi;-><init>(Landroid/content/Context;Lcfv;Lcfw;B)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcfv;Lcfw;B)V
    .locals 2

    new-instance v0, Lclj;

    const-string v1, "117"

    invoke-direct {v0, p1, p2, p3, v1}, Lclj;-><init>(Landroid/content/Context;Lcfv;Lcfw;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcvi;-><init>(Lclj;)V

    return-void
.end method

.method private constructor <init>(Lclj;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcvi;->a:Lclj;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcvi;->a:Lclj;

    invoke-virtual {v0}, Lclj;->a()V

    return-void
.end method

.method public a(Lcvn;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcvi;->a:Lclj;

    new-instance v1, Lcvl;

    invoke-direct {v1, p0, p1}, Lcvl;-><init>(Lcvi;Lcvn;)V

    invoke-virtual {v0, v1, p2, p3, p4}, Lclj;->a(Lcgk;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Lcvo;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcvi;->a:Lclj;

    new-instance v1, Lcvs;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcvs;-><init>(Lcvo;B)V

    invoke-virtual {v0, v1, p2, p3}, Lclj;->a(Lcgk;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Lcvp;)V
    .locals 1

    iget-object v0, p0, Lcvi;->a:Lclj;

    invoke-virtual {v0, p1}, Lclj;->a(Lcvp;)V

    return-void
.end method

.method public a(Lcvq;J)V
    .locals 2

    iget-object v0, p0, Lcvi;->a:Lclj;

    new-instance v1, Lcvk;

    invoke-direct {v1, p0, p1}, Lcvk;-><init>(Lcvi;Lcvq;)V

    invoke-virtual {v0, v1, p2, p3}, Lclj;->a(Lcgk;J)Lcig;

    return-void
.end method

.method public a(Lcvr;Ljava/lang/String;Ljava/lang/String;Lcvm;)V
    .locals 2

    iget-object v0, p0, Lcvi;->a:Lclj;

    new-instance v1, Lcvj;

    invoke-direct {v1, p0, p1}, Lcvj;-><init>(Lcvi;Lcvr;)V

    invoke-virtual {v0, v1, p2, p3, p4}, Lclj;->a(Lcgk;Ljava/lang/String;Ljava/lang/String;Lcvm;)V

    return-void
.end method

.method public a(Lcvp;Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 2

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "account must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcvi;->a:Lclj;

    invoke-virtual {v0, p1, p2, p3, p4}, Lclj;->a(Lcvp;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 1

    iget-object v0, p0, Lcvi;->a:Lclj;

    invoke-virtual {v0, p1, p2, p3, p4}, Lclj;->a(Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcvi;->a:Lclj;

    invoke-virtual {v0}, Lclj;->b()V

    return-void
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lcvi;->a:Lclj;

    invoke-virtual {v0}, Lclj;->c()Z

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, Lcvi;->a:Lclj;

    invoke-virtual {v0}, Lclj;->g()Z

    move-result v0

    return v0
.end method
