.class public final Ldqz;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldqz;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23202
    const/4 v0, 0x0

    new-array v0, v0, [Ldqz;

    sput-object v0, Ldqz;->a:[Ldqz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23203
    invoke-direct {p0}, Lepn;-><init>()V

    .line 23211
    iput-object v0, p0, Ldqz;->b:Ljava/lang/Integer;

    .line 23214
    iput-object v0, p0, Ldqz;->c:Ljava/lang/Integer;

    .line 23203
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 23231
    const/4 v0, 0x0

    .line 23232
    iget-object v1, p0, Ldqz;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 23233
    const/4 v0, 0x1

    iget-object v1, p0, Ldqz;->b:Ljava/lang/Integer;

    .line 23234
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 23236
    :cond_0
    iget-object v1, p0, Ldqz;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 23237
    const/4 v1, 0x2

    iget-object v2, p0, Ldqz;->c:Ljava/lang/Integer;

    .line 23238
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 23240
    :cond_1
    iget-object v1, p0, Ldqz;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23241
    iput v0, p0, Ldqz;->cachedSize:I

    .line 23242
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 23199
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldqz;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldqz;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldqz;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_2

    if-ne v0, v3, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqz;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqz;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_4

    if-ne v0, v3, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqz;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqz;->c:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 23219
    iget-object v0, p0, Ldqz;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 23220
    const/4 v0, 0x1

    iget-object v1, p0, Ldqz;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 23222
    :cond_0
    iget-object v0, p0, Ldqz;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 23223
    const/4 v0, 0x2

    iget-object v1, p0, Ldqz;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 23225
    :cond_1
    iget-object v0, p0, Ldqz;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 23227
    return-void
.end method
