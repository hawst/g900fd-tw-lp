.class public final Lly;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lmp;


# instance fields
.field a:Landroid/content/Context;

.field b:Landroid/view/LayoutInflater;

.field c:Lma;

.field d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

.field e:I

.field f:I

.field g:Llz;

.field private h:I

.field private i:Lmq;


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput p1, p0, Lly;->f:I

    .line 79
    iput p2, p0, Lly;->e:I

    .line 80
    return-void
.end method

.method static synthetic a(Lly;)I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lly;->h:I

    return v0
.end method


# virtual methods
.method public a()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lly;->g:Llz;

    if-nez v0, :cond_0

    .line 128
    new-instance v0, Llz;

    invoke-direct {v0, p0}, Llz;-><init>(Lly;)V

    iput-object v0, p0, Lly;->g:Llz;

    .line 130
    :cond_0
    iget-object v0, p0, Lly;->g:Llz;

    return-object v0
.end method

.method public a(Landroid/view/ViewGroup;)Lmr;
    .locals 3

    .prologue
    .line 101
    iget-object v0, p0, Lly;->g:Llz;

    if-nez v0, :cond_0

    .line 102
    new-instance v0, Llz;

    invoke-direct {v0, p0}, Llz;-><init>(Lly;)V

    iput-object v0, p0, Lly;->g:Llz;

    .line 105
    :cond_0
    iget-object v0, p0, Lly;->g:Llz;

    invoke-virtual {v0}, Llz;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 106
    iget-object v0, p0, Lly;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    if-nez v0, :cond_1

    .line 107
    iget-object v0, p0, Lly;->b:Landroid/view/LayoutInflater;

    sget v1, Lf;->ai:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    iput-object v0, p0, Lly;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    .line 109
    iget-object v0, p0, Lly;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    iget-object v1, p0, Lly;->g:Llz;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/ExpandedMenuView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 110
    iget-object v0, p0, Lly;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/view/menu/ExpandedMenuView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 112
    :cond_1
    iget-object v0, p0, Lly;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    .line 116
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Lma;)V
    .locals 2

    .prologue
    .line 84
    iget v0, p0, Lly;->e:I

    if-eqz v0, :cond_2

    .line 85
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget v1, p0, Lly;->e:I

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lly;->a:Landroid/content/Context;

    .line 86
    iget-object v0, p0, Lly;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lly;->b:Landroid/view/LayoutInflater;

    .line 93
    :cond_0
    :goto_0
    iput-object p2, p0, Lly;->c:Lma;

    .line 94
    iget-object v0, p0, Lly;->g:Llz;

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lly;->g:Llz;

    invoke-virtual {v0}, Llz;->notifyDataSetChanged()V

    .line 97
    :cond_1
    return-void

    .line 87
    :cond_2
    iget-object v0, p0, Lly;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 88
    iput-object p1, p0, Lly;->a:Landroid/content/Context;

    .line 89
    iget-object v0, p0, Lly;->b:Landroid/view/LayoutInflater;

    if-nez v0, :cond_0

    .line 90
    iget-object v0, p0, Lly;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lly;->b:Landroid/view/LayoutInflater;

    goto :goto_0
.end method

.method public a(Lma;Z)V
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lly;->i:Lmq;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lly;->i:Lmq;

    invoke-interface {v0, p1, p2}, Lmq;->a(Lma;Z)V

    .line 160
    :cond_0
    return-void
.end method

.method public a(Lmq;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lly;->i:Lmq;

    .line 141
    return-void
.end method

.method public a(Lmu;)Z
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p1}, Lmu;->hasVisibleItems()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 152
    :goto_0
    return v0

    .line 148
    :cond_0
    new-instance v0, Lmd;

    invoke-direct {v0, p1}, Lmd;-><init>(Lma;)V

    invoke-virtual {v0}, Lmd;->a()V

    .line 149
    iget-object v0, p0, Lly;->i:Lmq;

    if-eqz v0, :cond_1

    .line 150
    iget-object v0, p0, Lly;->i:Lmq;

    invoke-interface {v0, p1}, Lmq;->b(Lma;)Z

    .line 152
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(Lme;)Z
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x0

    return v0
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lly;->g:Llz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lly;->g:Llz;

    invoke-virtual {v0}, Llz;->notifyDataSetChanged()V

    .line 136
    :cond_0
    return-void
.end method

.method public c(Lme;)Z
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x0

    return v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x0

    return v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 175
    iget-object v0, p0, Lly;->c:Lma;

    iget-object v1, p0, Lly;->g:Llz;

    invoke-virtual {v1, p3}, Llz;->a(I)Lme;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lma;->a(Landroid/view/MenuItem;I)Z

    .line 176
    return-void
.end method
