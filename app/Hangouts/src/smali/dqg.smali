.class public final Ldqg;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldqg;


# instance fields
.field public b:Ldqa;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25663
    const/4 v0, 0x0

    new-array v0, v0, [Ldqg;

    sput-object v0, Ldqg;->a:[Ldqg;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25664
    invoke-direct {p0}, Lepn;-><init>()V

    .line 25667
    const/4 v0, 0x0

    iput-object v0, p0, Ldqg;->b:Ldqa;

    .line 25664
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 2

    .prologue
    .line 25681
    const/4 v0, 0x0

    .line 25682
    iget-object v1, p0, Ldqg;->b:Ldqa;

    if-eqz v1, :cond_0

    .line 25683
    const/4 v0, 0x1

    iget-object v1, p0, Ldqg;->b:Ldqa;

    .line 25684
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 25686
    :cond_0
    iget-object v1, p0, Ldqg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25687
    iput v0, p0, Ldqg;->cachedSize:I

    .line 25688
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 25660
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldqg;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldqg;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldqg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldqg;->b:Ldqa;

    if-nez v0, :cond_2

    new-instance v0, Ldqa;

    invoke-direct {v0}, Ldqa;-><init>()V

    iput-object v0, p0, Ldqg;->b:Ldqa;

    :cond_2
    iget-object v0, p0, Ldqg;->b:Ldqa;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 25672
    iget-object v0, p0, Ldqg;->b:Ldqa;

    if-eqz v0, :cond_0

    .line 25673
    const/4 v0, 0x1

    iget-object v1, p0, Ldqg;->b:Ldqa;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 25675
    :cond_0
    iget-object v0, p0, Ldqg;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 25677
    return-void
.end method
