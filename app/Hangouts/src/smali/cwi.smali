.class public final Lcwi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/accounts/OnAccountsUpdateListener;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcwc;",
            ">;"
        }
    .end annotation
.end field

.field private c:[Landroid/accounts/Account;

.field private d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcwc;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private f:Landroid/accounts/AccountManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcwi;->a:Landroid/content/Context;

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcwi;->d:Ljava/util/HashMap;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcwi;->b:Ljava/util/ArrayList;

    .line 42
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcwi;->e:Z

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcwi;->f:Landroid/accounts/AccountManager;

    invoke-virtual {v0, p0}, Landroid/accounts/AccountManager;->removeOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;)V

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcwi;->e:Z

    .line 59
    iget-object v0, p0, Lcwi;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 60
    iget-object v0, p0, Lcwi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 62
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Iterable;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcwc;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcwc;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 76
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    .line 78
    :cond_0
    invoke-direct {p0}, Lcwi;->a()V

    .line 97
    :cond_1
    :goto_1
    iget-object v0, p0, Lcwi;->b:Ljava/util/ArrayList;

    return-object v0

    :cond_2
    move v0, v1

    .line 76
    goto :goto_0

    .line 80
    :cond_3
    iget-object v0, p0, Lcwi;->f:Landroid/accounts/AccountManager;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcwi;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lcwi;->f:Landroid/accounts/AccountManager;

    iget-object v0, p0, Lcwi;->f:Landroid/accounts/AccountManager;

    const-string v3, "com.google"

    invoke-virtual {v0, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcwi;->c:[Landroid/accounts/Account;

    :cond_4
    iget-boolean v0, p0, Lcwi;->e:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcwi;->f:Landroid/accounts/AccountManager;

    const/4 v3, 0x0

    invoke-virtual {v0, p0, v3, v2}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    iput-boolean v2, p0, Lcwi;->e:Z

    .line 81
    :cond_5
    iget-object v0, p0, Lcwi;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    if-eqz p1, :cond_6

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwc;

    iget-object v3, p0, Lcwi;->d:Ljava/util/HashMap;

    invoke-interface {v0}, Lcwc;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 84
    :cond_6
    iget-object v0, p0, Lcwi;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 85
    invoke-direct {p0}, Lcwi;->a()V

    goto :goto_1

    .line 88
    :cond_7
    iget-object v0, p0, Lcwi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 89
    iget-object v2, p0, Lcwi;->c:[Landroid/accounts/Account;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 90
    iget-object v4, p0, Lcwi;->d:Ljava/util/HashMap;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwc;

    .line 91
    if-eqz v0, :cond_8

    .line 92
    iget-object v4, p0, Lcwi;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3
.end method

.method public onAccountsUpdated([Landroid/accounts/Account;)V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcwi;->f:Landroid/accounts/AccountManager;

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcwi;->c:[Landroid/accounts/Account;

    .line 67
    iget-object v0, p0, Lcwi;->b:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Lcwi;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    .line 68
    return-void
.end method
