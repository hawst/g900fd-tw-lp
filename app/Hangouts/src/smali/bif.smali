.class public Lbif;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method private constructor <init>(Ldwj;)V
    .locals 3

    .prologue
    .line 3836
    invoke-direct {p0}, Lbfz;-><init>()V

    .line 3837
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 3838
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SetHangoutNotificationStatusResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3841
    :cond_0
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 3845
    new-instance v0, Ldwj;

    invoke-direct {v0}, Ldwj;-><init>()V

    .line 3846
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldwj;

    .line 3847
    iget-object v1, v0, Ldwj;->b:Ldvn;

    invoke-static {v1}, Lbif;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3848
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldwj;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 3850
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbif;

    invoke-direct {v1, v0}, Lbif;-><init>(Ldwj;)V

    move-object v0, v1

    goto :goto_0
.end method
