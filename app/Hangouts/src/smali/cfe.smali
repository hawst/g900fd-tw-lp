.class public final Lcfe;
.super Lcbq;
.source "PG"

# interfaces
.implements Landroid/widget/RemoteViewsService$RemoteViewsFactory;


# instance fields
.field private final j:Landroid/content/Context;

.field private final k:I

.field private l:Z

.field private m:Landroid/database/Cursor;

.field private final n:Landroid/appwidget/AppWidgetManager;

.field private o:Landroid/net/Uri;

.field private p:Lyj;

.field private q:Landroid/widget/RemoteViews;

.field private r:I

.field private s:I

.field private t:Z

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private final x:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 107
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcbq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 95
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcfe;->x:Ljava/lang/Object;

    .line 109
    iput-object p1, p0, Lcfe;->j:Landroid/content/Context;

    .line 110
    const-string v0, "appWidgetId"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcfe;->k:I

    .line 112
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    iput-object v0, p0, Lcfe;->n:Landroid/appwidget/AppWidgetManager;

    .line 113
    const-string v0, "account_name"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 114
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    iput-object v0, p0, Lcfe;->p:Lyj;

    .line 115
    invoke-static {}, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BabelRemoteViewsFactory intent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "widget id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcfe;->k:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mAccount: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcfe;->p:Lyj;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    :cond_0
    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 14

    .prologue
    const/4 v2, 0x0

    const v3, 0x7fffffff

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 412
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v0

    iget-object v1, p0, Lcfe;->p:Lyj;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Lyj;)J

    move-result-wide v4

    .line 415
    const/16 v0, 0x18

    .line 416
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 417
    const/16 v1, 0x13

    .line 418
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 422
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 424
    :cond_0
    iget-object v0, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v1, Lg;->dP:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 425
    iget-object v0, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v1, Lg;->dO:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 472
    :goto_0
    return-void

    .line 428
    :cond_1
    const-string v2, "\\|"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 429
    const-string v0, "\\|"

    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 431
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 434
    array-length v0, v6

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    move v1, v3

    :goto_1
    if-ltz v2, :cond_5

    .line 435
    aget-object v9, v6, v2

    .line 439
    array-length v0, v7

    if-ge v2, v0, :cond_4

    aget-object v0, v7, v2

    .line 441
    :goto_2
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 443
    if-ne v1, v3, :cond_6

    cmp-long v0, v4, v10

    if-ltz v0, :cond_6

    .line 444
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 447
    :goto_3
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 448
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 449
    const-string v1, ", "

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 451
    :cond_2
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 434
    :cond_3
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 439
    :cond_4
    const-string v0, "0"

    goto :goto_2

    .line 455
    :cond_5
    iget-object v0, p0, Lcfe;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 458
    iget-object v2, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v3, Lg;->dP:I

    sget v4, Lh;->gr:I

    new-array v5, v13, [Ljava/lang/Object;

    const/16 v6, 0x16

    .line 461
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 460
    invoke-static {v6}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->b(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v12

    .line 459
    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 458
    invoke-virtual {v2, v3, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 465
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 466
    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-direct {v2, v13}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 468
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 466
    invoke-virtual {v0, v2, v12, v1, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 469
    iget-object v1, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v2, Lg;->dO:I

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 471
    iget-object v0, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v1, Lg;->G:I

    sget v2, Lcom/google/android/apps/hangouts/R$drawable;->bM:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_3
.end method

.method private r()I
    .locals 2

    .prologue
    .line 250
    iget-object v0, p0, Lcfe;->m:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/16 v1, 0x19

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method private s()Landroid/widget/RemoteViews;
    .locals 4

    .prologue
    .line 580
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcfe;->j:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sget v2, Lf;->gQ:I

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 582
    sget v1, Lg;->eh:I

    iget-object v2, p0, Lcfe;->j:Landroid/content/Context;

    sget v3, Lh;->oy:I

    .line 583
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 582
    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 584
    iget-object v1, p0, Lcfe;->p:Lyj;

    invoke-static {v1}, Lbbl;->c(Lyj;)Landroid/content/Intent;

    move-result-object v1

    .line 585
    sget v2, Lg;->io:I

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    .line 586
    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 721
    iget-object v0, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v1, Lg;->eD:I

    invoke-virtual {v0, v1, p1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 722
    return-void
.end method

.method public a(II)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 698
    iput p2, p0, Lcfe;->r:I

    .line 700
    iget-object v0, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v1, Lg;->aM:I

    invoke-virtual {v0, v1, p1}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 701
    iget-object v0, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v1, Lg;->hw:I

    invoke-virtual {v0, v1, p1}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 702
    iget-object v0, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v1, Lg;->eD:I

    invoke-virtual {v0, v1, p1}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 704
    iget v0, p0, Lcfe;->r:I

    if-nez v0, :cond_0

    .line 705
    iget-object v0, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v1, Lg;->ir:I

    invoke-virtual {v0, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 706
    iget-object v0, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v1, Lg;->ip:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 711
    :goto_0
    return-void

    .line 708
    :cond_0
    iget-object v0, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v1, Lg;->ir:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 709
    iget-object v0, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v1, Lg;->ip:I

    invoke-virtual {v0, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0
.end method

.method protected a(ILandroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 632
    invoke-super {p0, p1, p2}, Lcbq;->a(ILandroid/database/Cursor;)V

    .line 633
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 774
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 778
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 728
    if-eqz p1, :cond_0

    iget v0, p0, Lcfe;->r:I

    if-nez v0, :cond_2

    .line 729
    :cond_0
    iget-object v0, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v1, Lg;->eD:I

    invoke-virtual {v0, v1, p1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 730
    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcfe;->v:Ljava/lang/String;

    .line 737
    :goto_1
    return-void

    .line 730
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 732
    :cond_2
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 733
    new-instance v1, Landroid/text/style/StyleSpan;

    iget v2, p0, Lcfe;->r:I

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 734
    iget-object v1, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v2, Lg;->eD:I

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 735
    invoke-virtual {v0}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfe;->v:Ljava/lang/String;

    goto :goto_1
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 750
    return-void
.end method

.method public a(ZLandroid/graphics/Bitmap;ZLjava/lang/Object;)V
    .locals 5

    .prologue
    .line 646
    if-eqz p4, :cond_2

    .line 647
    invoke-static {}, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 648
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setAvatarAnimateImageBitmap avatarLoadedToken: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isDefaultAvatar: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mCurrentPosition: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcfe;->s:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bitmap: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " width: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 651
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " height: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 652
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " threadId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 653
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 648
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    :cond_0
    check-cast p4, Ljava/lang/Integer;

    const/4 v0, 0x0

    invoke-static {p4, v0}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    iget v1, p0, Lcfe;->s:I

    if-ne v0, v1, :cond_2

    .line 656
    iget-object v0, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v1, Lg;->G:I

    invoke-virtual {v0, v1, p2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 657
    if-eqz p2, :cond_2

    if-nez p3, :cond_2

    .line 658
    iget-object v1, p0, Lcfe;->x:Ljava/lang/Object;

    monitor-enter v1

    .line 659
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcfe;->t:Z

    .line 660
    iget-object v0, p0, Lcfe;->x:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 661
    invoke-static {}, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 662
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setAvatarAnimateImageBitmap mAvatarLock.notifyAll threadId: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 663
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getId()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 662
    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 669
    :cond_2
    return-void

    .line 665
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 715
    iget-object v0, p0, Lcfe;->u:Ljava/lang/String;

    return-object v0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 746
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 798
    if-eqz p1, :cond_0

    iget v0, p0, Lcfe;->r:I

    if-nez v0, :cond_2

    .line 799
    :cond_0
    iget-object v0, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v1, Lg;->hw:I

    invoke-virtual {v0, v1, p1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 800
    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcfe;->w:Ljava/lang/String;

    .line 807
    :goto_1
    return-void

    .line 800
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 802
    :cond_2
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 803
    new-instance v1, Landroid/text/style/StyleSpan;

    iget v2, p0, Lcfe;->r:I

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 804
    iget-object v1, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v2, Lg;->hw:I

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 805
    invoke-virtual {v0}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfe;->w:Ljava/lang/String;

    goto :goto_1
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 680
    if-eqz p1, :cond_0

    .line 681
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 685
    :cond_0
    if-eqz p1, :cond_1

    iget v0, p0, Lcfe;->r:I

    if-nez v0, :cond_2

    .line 686
    :cond_1
    iget-object v0, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v1, Lg;->aM:I

    invoke-virtual {v0, v1, p1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 687
    iput-object p1, p0, Lcfe;->u:Ljava/lang/String;

    .line 694
    :goto_0
    return-void

    .line 689
    :cond_2
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 690
    new-instance v1, Landroid/text/style/StyleSpan;

    iget v2, p0, Lcfe;->r:I

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 691
    iget-object v1, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v2, Lg;->aM:I

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 692
    invoke-virtual {v0}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfe;->u:Ljava/lang/String;

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 758
    const/4 v0, 0x0

    return v0
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 754
    return-void
.end method

.method public c(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 878
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 790
    const/16 v0, 0x8

    return v0
.end method

.method public d(I)V
    .locals 0

    .prologue
    .line 770
    return-void
.end method

.method public e()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 763
    iget-object v0, p0, Lcfe;->v:Ljava/lang/String;

    return-object v0
.end method

.method public e(I)V
    .locals 0

    .prologue
    .line 782
    return-void
.end method

.method public f()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 741
    const/4 v0, 0x0

    return-object v0
.end method

.method public f(I)V
    .locals 0

    .prologue
    .line 786
    return-void
.end method

.method public g()I
    .locals 1

    .prologue
    .line 816
    const/4 v0, 0x0

    return v0
.end method

.method public g(I)V
    .locals 2

    .prologue
    .line 811
    iget-object v0, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v1, Lg;->hw:I

    invoke-virtual {v0, v1, p1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 812
    return-void
.end method

.method public getCount()I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 235
    invoke-static {}, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;->b()Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 236
    :try_start_0
    iget-object v2, p0, Lcfe;->m:Landroid/database/Cursor;

    if-nez v2, :cond_0

    .line 237
    monitor-exit v3

    .line 241
    :goto_0
    return v0

    .line 239
    :cond_0
    invoke-direct {p0}, Lcfe;->r()I

    move-result v4

    .line 240
    iget-object v2, p0, Lcfe;->m:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-ge v4, v2, :cond_2

    move v2, v1

    :goto_1
    iput-boolean v2, p0, Lcfe;->l:Z

    .line 241
    iget-boolean v2, p0, Lcfe;->l:Z

    if-eqz v2, :cond_1

    move v0, v1

    :cond_1
    add-int/2addr v0, v4

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 242
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_2
    move v2, v0

    .line 240
    goto :goto_1
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 606
    int-to-long v0, p1

    return-wide v0
.end method

.method public getLoadingView()Landroid/widget/RemoteViews;
    .locals 4

    .prologue
    .line 591
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcfe;->j:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sget v2, Lf;->gQ:I

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 593
    sget v1, Lg;->eh:I

    iget-object v2, p0, Lcfe;->j:Landroid/content/Context;

    sget v3, Lh;->fX:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 595
    return-object v0
.end method

.method public getViewAt(I)Landroid/widget/RemoteViews;
    .locals 14

    .prologue
    const/4 v13, 0x3

    const/4 v7, 0x4

    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 258
    invoke-static {}, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;->b()Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8

    .line 260
    :try_start_0
    iget-object v2, p0, Lcfe;->m:Landroid/database/Cursor;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcfe;->l:Z

    if-eqz v2, :cond_1

    .line 261
    invoke-direct {p0}, Lcfe;->r()I

    move-result v2

    if-lt p1, v2, :cond_1

    .line 262
    :cond_0
    invoke-direct {p0}, Lcfe;->s()Landroid/widget/RemoteViews;

    move-result-object v0

    monitor-exit v8

    .line 386
    :goto_0
    return-object v0

    .line 265
    :cond_1
    iget-object v2, p0, Lcfe;->m:Landroid/database/Cursor;

    invoke-interface {v2, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 268
    invoke-direct {p0}, Lcfe;->s()Landroid/widget/RemoteViews;

    move-result-object v0

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 387
    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0

    .line 272
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 273
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "getViewAt position: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " mCurrentPosition: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v5, p0, Lcfe;->s:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " threadId: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 274
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->getId()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 273
    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    :cond_3
    iget-object v2, p0, Lcfe;->m:Landroid/database/Cursor;

    const/16 v3, 0xe

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-ne v3, v4, :cond_5

    move v3, v1

    .line 279
    :goto_1
    packed-switch v3, :pswitch_data_0

    .line 282
    sget v2, Lf;->gN:I

    .line 293
    :goto_2
    new-instance v5, Landroid/widget/RemoteViews;

    iget-object v6, p0, Lcfe;->j:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v5, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    .line 295
    invoke-virtual {p0}, Lcfe;->onFinishInflate()V

    .line 298
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v2, v5, :cond_7

    .line 299
    iget-object v2, p0, Lcfe;->n:Landroid/appwidget/AppWidgetManager;

    iget v5, p0, Lcfe;->k:I

    invoke-virtual {v2, v5}, Landroid/appwidget/AppWidgetManager;->getAppWidgetOptions(I)Landroid/os/Bundle;

    move-result-object v2

    const-string v5, "widgetSizeKey"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    move v6, v2

    .line 302
    :goto_3
    iget-object v5, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v9, Lg;->G:I

    if-eq v6, v13, :cond_4

    if-ne v6, v7, :cond_8

    :cond_4
    move v2, v1

    :goto_4
    invoke-virtual {v5, v9, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 308
    if-ne v3, v4, :cond_9

    .line 309
    iget-object v0, p0, Lcfe;->m:Landroid/database/Cursor;

    invoke-direct {p0, v0}, Lcfe;->a(Landroid/database/Cursor;)V

    .line 312
    iget-object v0, p0, Lcfe;->p:Lyj;

    .line 313
    invoke-static {v0}, Lbbl;->e(Lyj;)Landroid/content/Intent;

    move-result-object v0

    .line 314
    iget-object v1, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v2, Lg;->im:I

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    .line 315
    iget-object v0, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    monitor-exit v8

    goto/16 :goto_0

    .line 277
    :cond_5
    const/16 v3, 0x16

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-le v2, v0, :cond_6

    move v3, v4

    goto :goto_1

    :cond_6
    move v3, v0

    goto :goto_1

    .line 285
    :pswitch_0
    sget v2, Lf;->gO:I

    goto :goto_2

    .line 288
    :pswitch_1
    sget v2, Lf;->gP:I

    goto :goto_2

    :cond_7
    move v6, v7

    .line 299
    goto :goto_3

    .line 302
    :cond_8
    const/16 v2, 0x8

    goto :goto_4

    .line 320
    :cond_9
    iget-object v9, p0, Lcfe;->x:Ljava/lang/Object;

    monitor-enter v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 321
    const/4 v2, 0x0

    :try_start_2
    iput-boolean v2, p0, Lcfe;->t:Z

    .line 322
    const/4 v2, 0x0

    iput v2, p0, Lcfe;->r:I

    .line 323
    iput p1, p0, Lcfe;->s:I

    .line 324
    const/4 v2, 0x0

    iput-object v2, p0, Lcfe;->u:Ljava/lang/String;

    .line 325
    const/4 v2, 0x0

    iput-object v2, p0, Lcfe;->v:Ljava/lang/String;

    .line 326
    const/4 v2, 0x0

    iput-object v2, p0, Lcfe;->w:Ljava/lang/String;

    .line 328
    if-ne v3, v0, :cond_13

    .line 329
    iget-object v3, p0, Lcfe;->m:Landroid/database/Cursor;

    const/4 v2, 0x3

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-ne v2, v4, :cond_c

    :goto_5
    invoke-virtual {p0}, Lcfe;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-eqz v0, :cond_12

    const/4 v0, 0x7

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x6

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v1, 0x12

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_f

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_d

    sget v1, Lh;->fJ:I

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lf;->ho:I

    add-int/lit8 v5, v0, 0x1

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v10, v11

    invoke-virtual {v4, v2, v5, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_6
    iget-object v2, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v4, Lg;->dT:I

    invoke-virtual {v2, v4, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    iget-object v1, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v2, Lg;->dQ:I

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    :goto_7
    const/16 v0, 0x14

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {p0, v1}, Lcfe;->a(Ljava/lang/Object;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcfe;->p:Lyj;

    invoke-virtual {p0, v0, v1, v2}, Lcfe;->a(Ljava/util/List;ILyj;)V

    .line 334
    new-instance v2, Lbdk;

    iget-object v0, p0, Lcfe;->m:Landroid/database/Cursor;

    const/16 v1, 0x10

    .line 335
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcfe;->m:Landroid/database/Cursor;

    const/16 v3, 0x11

    .line 337
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    iget-object v0, p0, Lcfe;->m:Landroid/database/Cursor;

    const/4 v1, 0x4

    .line 341
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 343
    iget-object v0, p0, Lcfe;->p:Lyj;

    iget-object v1, p0, Lcfe;->a:Ljava/lang/String;

    iget v3, p0, Lcfe;->b:I

    .line 344
    invoke-static/range {v0 .. v5}, Lbbl;->a(Lyj;Ljava/lang/String;Lbdk;IJ)Landroid/content/Intent;

    move-result-object v0

    .line 350
    iget-object v1, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v2, Lg;->il:I

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    .line 362
    :goto_8
    if-eq v6, v13, :cond_a

    if-ne v6, v7, :cond_b

    :cond_a
    iget-boolean v0, p0, Lcfe;->t:Z

    if-nez v0, :cond_b

    .line 364
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-wide v0

    .line 366
    :try_start_3
    iget-object v2, p0, Lcfe;->x:Ljava/lang/Object;

    const-wide/16 v3, 0xfa

    invoke-virtual {v2, v3, v4}, Ljava/lang/Object;->wait(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 370
    :goto_9
    :try_start_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 371
    sub-long v4, v2, v0

    const-wide/16 v6, 0xfa

    cmp-long v4, v4, v6

    if-ltz v4, :cond_14

    .line 372
    invoke-static {}, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;->a()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 373
    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "TIMED OUT on position: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " time: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sub-long v0, v2, v0

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " threadId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 375
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 373
    invoke-static {v4, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    :cond_b
    :goto_a
    monitor-exit v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 386
    :try_start_5
    iget-object v0, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    monitor-exit v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    :cond_c
    move v0, v1

    .line 329
    goto/16 :goto_5

    :cond_d
    if-nez v0, :cond_e

    :try_start_6
    sget v0, Lh;->fO:I

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_6

    :cond_e
    sget v2, Lf;->hq:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v5, v10

    invoke-virtual {v4, v2, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_6

    :cond_f
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_10

    sget v1, Lf;->ho:I

    add-int/lit8 v5, v0, 0x1

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v10, v11

    invoke-virtual {v4, v1, v5, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v2

    goto/16 :goto_6

    :cond_10
    if-nez v0, :cond_11

    sget v0, Lh;->fN:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v1, v5, v10

    invoke-virtual {v4, v0, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v2

    goto/16 :goto_6

    :cond_11
    sget v5, Lf;->hp:I

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object v1, v10, v11

    invoke-virtual {v4, v5, v0, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v2

    goto/16 :goto_6

    :cond_12
    iget-object v0, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v1, Lg;->dT:I

    const/16 v2, 0x12

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const/16 v0, 0x8

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_1

    :pswitch_2
    iget-object v0, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v1, Lg;->dQ:I

    sget v2, Lh;->fK:I

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_7

    .line 385
    :catchall_1
    move-exception v0

    :try_start_7
    monitor-exit v9

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 329
    :pswitch_3
    :try_start_8
    iget-object v0, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v1, Lg;->dQ:I

    sget v2, Lh;->fL:I

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_7

    :pswitch_4
    iget-object v0, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v1, Lg;->dQ:I

    sget v2, Lh;->fM:I

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 353
    :cond_13
    iget-object v0, p0, Lcfe;->m:Landroid/database/Cursor;

    iget-object v1, p0, Lcfe;->p:Lyj;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {p0, v0, v1, v2, v3}, Lcfe;->a(Landroid/database/Cursor;Lyj;ZLjava/lang/Object;)V

    .line 357
    iget-object v0, p0, Lcfe;->p:Lyj;

    iget-object v1, p0, Lcfe;->a:Ljava/lang/String;

    iget v2, p0, Lcfe;->b:I

    invoke-static {v0, v1, v2}, Lbbl;->a(Lyj;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 359
    iget-object v1, p0, Lcfe;->q:Landroid/widget/RemoteViews;

    sget v2, Lg;->ij:I

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    goto/16 :goto_8

    .line 368
    :catch_0
    move-exception v2

    const-string v2, "Babel"

    const-string v3, "getViewAt: loading avatar interrupted"

    invoke-static {v2, v3}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 378
    :cond_14
    invoke-static {}, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;->a()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 379
    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Didn\'t time out on position: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " time: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sub-long v0, v2, v0

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " threadId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 381
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 379
    invoke-static {v4, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_a

    .line 279
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 329
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 600
    const/4 v0, 0x5

    return v0
.end method

.method public h()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 821
    iget-object v0, p0, Lcfe;->w:Ljava/lang/String;

    return-object v0
.end method

.method public h(I)V
    .locals 0

    .prologue
    .line 831
    return-void
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 611
    const/4 v0, 0x1

    return v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 836
    const/16 v0, 0x8

    return v0
.end method

.method public i(I)V
    .locals 0

    .prologue
    .line 841
    return-void
.end method

.method public j()I
    .locals 1

    .prologue
    .line 846
    const/16 v0, 0x8

    return v0
.end method

.method public j(I)V
    .locals 0

    .prologue
    .line 851
    return-void
.end method

.method public k()I
    .locals 1

    .prologue
    .line 866
    const/16 v0, 0x8

    return v0
.end method

.method public k(I)V
    .locals 0

    .prologue
    .line 861
    return-void
.end method

.method protected l()V
    .locals 0

    .prologue
    .line 888
    return-void
.end method

.method public l(I)V
    .locals 0

    .prologue
    .line 871
    return-void
.end method

.method public m(I)V
    .locals 0

    .prologue
    .line 875
    return-void
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 892
    const/4 v0, 0x0

    return v0
.end method

.method public n(I)V
    .locals 0

    .prologue
    .line 883
    return-void
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 124
    iget-object v0, p0, Lcfe;->p:Lyj;

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 125
    :goto_0
    if-eqz v0, :cond_0

    .line 127
    iget-object v1, p0, Lcfe;->j:Landroid/content/Context;

    iget v2, p0, Lcfe;->k:I

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 132
    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcfe;->j:Landroid/content/Context;

    iget v2, p0, Lcfe;->k:I

    .line 133
    invoke-static {v1, v2, v0}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->b(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 134
    :cond_1
    iget-object v1, p0, Lcfe;->j:Landroid/content/Context;

    iget v2, p0, Lcfe;->k:I

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->c(Landroid/content/Context;ILjava/lang/String;)V

    .line 136
    :cond_2
    return-void

    .line 124
    :cond_3
    iget-object v0, p0, Lcfe;->p:Lyj;

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onDataSetChanged()V
    .locals 11

    .prologue
    .line 150
    invoke-static {}, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;->b()Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6

    .line 151
    :try_start_0
    iget-object v0, p0, Lcfe;->m:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcfe;->m:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 153
    const/4 v0, 0x0

    iput-object v0, p0, Lcfe;->m:Landroid/database/Cursor;

    .line 155
    :cond_0
    invoke-static {}, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 156
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BabelRemoteViewsFactory.onDataSetChanged account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcfe;->p:Lyj;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :cond_1
    iget-object v0, p0, Lcfe;->p:Lyj;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcfe;->j:Landroid/content/Context;

    iget v1, p0, Lcfe;->k:I

    iget-object v2, p0, Lcfe;->p:Lyj;

    .line 165
    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    .line 164
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->b(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 166
    :cond_2
    iget-object v0, p0, Lcfe;->j:Landroid/content/Context;

    iget v1, p0, Lcfe;->k:I

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 168
    invoke-static {}, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 169
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BabelRemoteViewsFactory.onDataSetChanged account: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcfe;->p:Lyj;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not valid, widgetId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcfe;->k:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " accountName:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    :cond_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 174
    invoke-static {}, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 175
    const-string v0, "Babel"

    const-string v1, "BabelRemoteViewsFactory.onDataSetChanged empty bail"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :cond_4
    iget-object v0, p0, Lcfe;->j:Landroid/content/Context;

    iget v1, p0, Lcfe;->k:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->c(Landroid/content/Context;ILjava/lang/String;)V

    .line 178
    monitor-exit v6

    .line 197
    :goto_0
    return-void

    .line 180
    :cond_5
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    iput-object v0, p0, Lcfe;->p:Lyj;

    .line 181
    invoke-static {}, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 182
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BabelRemoteViewsFactory.onDataSetChanged getAccountByName: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcfe;->p:Lyj;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    :cond_6
    iget-object v0, p0, Lcfe;->p:Lyj;

    if-nez v0, :cond_7

    .line 186
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 197
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    .line 190
    :cond_7
    :try_start_1
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v7

    .line 192
    :try_start_2
    invoke-static {}, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "queryAllConversations account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcfe;->p:Lyj;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->d:Landroid/net/Uri;

    iget-object v1, p0, Lcfe;->p:Lyj;

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Landroid/net/Uri;Lyj;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcfe;->o:Landroid/net/Uri;

    iget-object v0, p0, Lcfe;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcfe;->o:Landroid/net/Uri;

    sget-object v2, Laiq;->a:[Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "(%s >= 0 OR %s IS NULL) AND %s = %d AND %s > 0"

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "is_pending_leave"

    aput-object v10, v5, v9

    const/4 v9, 0x1

    const-string v10, "is_pending_leave"

    aput-object v10, v5, v9

    const/4 v9, 0x2

    const-string v10, "view"

    aput-object v10, v5, v9

    const/4 v9, 0x3

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v5, v9

    const/4 v9, 0x4

    const-string v10, "sort_timestamp"

    aput-object v10, v5, v9

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "call_media_type DESC, sort_timestamp DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "queryAllConversations cursor count: "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v1, :cond_a

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    iput-object v1, p0, Lcfe;->m:Landroid/database/Cursor;

    .line 193
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcfe;->j:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sget v2, Lf;->gL:I

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iget-object v1, p0, Lcfe;->n:Landroid/appwidget/AppWidgetManager;

    iget v2, p0, Lcfe;->k:I

    invoke-virtual {v1, v2, v0}, Landroid/appwidget/AppWidgetManager;->partiallyUpdateAppWidget(ILandroid/widget/RemoteViews;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 195
    :try_start_3
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 197
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 192
    :cond_a
    :try_start_4
    const-string v0, "null"
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    .line 195
    :catchall_1
    move-exception v0

    :try_start_5
    invoke-static {v7, v8}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 140
    invoke-static {}, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;->b()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 141
    :try_start_0
    iget-object v0, p0, Lcfe;->m:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcfe;->m:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 142
    iget-object v0, p0, Lcfe;->m:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 143
    const/4 v0, 0x0

    iput-object v0, p0, Lcfe;->m:Landroid/database/Cursor;

    .line 145
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onFinishInflate()V
    .locals 0

    .prologue
    .line 623
    invoke-super {p0}, Lcbq;->onFinishInflate()V

    .line 624
    return-void
.end method

.method protected q()Z
    .locals 1

    .prologue
    .line 673
    const/4 v0, 0x1

    return v0
.end method
