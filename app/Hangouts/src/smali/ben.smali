.class public Lben;
.super Lbep;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final a:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 2607
    invoke-direct {p0}, Lbep;-><init>()V

    .line 2608
    iput-boolean p1, p0, Lben;->a:Z

    .line 2609
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 21

    .prologue
    .line 2613
    sget-boolean v3, Lbea;->d:Z

    if-eqz v3, :cond_0

    .line 2614
    const-string v3, "Babel_RequestWriter"

    const-string v4, "GetSelfInfoRequest build protobuf."

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2616
    :cond_0
    new-instance v16, Ldsp;

    invoke-direct/range {v16 .. v16}, Ldsp;-><init>()V

    .line 2617
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 2618
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v3, v4, v1, v2}, Lben;->a(Ldpt;ZLjava/lang/String;I)Ldvm;

    move-result-object v3

    move-object/from16 v0, v16

    iput-object v3, v0, Ldsp;->b:Ldvm;

    .line 2619
    const/4 v3, 0x1

    new-array v0, v3, [I

    move-object/from16 v17, v0

    const/4 v3, 0x0

    const/4 v4, 0x7

    aput v4, v17, v3

    .line 2621
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 2622
    const/4 v3, 0x5

    .line 2623
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    .line 2624
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x2

    .line 2625
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x3

    .line 2626
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x4

    .line 2627
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v8, 0x6

    .line 2628
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/16 v9, 0x8

    .line 2629
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const/16 v10, 0x9

    .line 2630
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const/16 v11, 0xa

    .line 2631
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    const/16 v12, 0xb

    .line 2632
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    const/16 v13, 0xc

    .line 2633
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    const/16 v14, 0xd

    .line 2634
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    const/4 v15, 0x4

    new-array v15, v15, [Ljava/lang/Integer;

    const/16 v19, 0x0

    const/16 v20, 0xe

    .line 2635
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v15, v19

    const/16 v19, 0x1

    const/16 v20, 0xf

    .line 2636
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v15, v19

    const/16 v19, 0x2

    const/16 v20, 0x10

    .line 2637
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v15, v19

    const/16 v19, 0x3

    const/16 v20, 0x12

    .line 2638
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v15, v19

    .line 2622
    invoke-static/range {v3 .. v15}, Leeg;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Leeg;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2639
    const-string v3, "babel_enable_call_me_maybe"

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2641
    const/16 v3, 0x11

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2643
    :cond_1
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v3

    new-array v5, v3, [I

    .line 2644
    const/4 v3, 0x0

    move v4, v3

    :goto_0
    array-length v3, v5

    if-ge v4, v3, :cond_2

    .line 2645
    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    const/4 v6, 0x0

    invoke-static {v3, v6}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v3

    aput v3, v5, v4

    .line 2644
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 2647
    :cond_2
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Ldsp;->c:[I

    .line 2648
    move-object/from16 v0, v16

    iput-object v5, v0, Ldsp;->d:[I

    .line 2649
    return-object v16
.end method

.method public a(Lyj;Lbph;)V
    .locals 0

    .prologue
    .line 2702
    invoke-static {p1, p2}, Lbkb;->b(Lyj;Ljava/lang/Exception;)V

    .line 2703
    return-void
.end method

.method public a(JI)Z
    .locals 1

    .prologue
    .line 2654
    iget-boolean v0, p0, Lben;->a:Z

    return v0
.end method

.method public a(Lbea;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2678
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2680
    check-cast p1, Lben;

    .line 2681
    iget-boolean v1, p0, Lben;->a:Z

    iget-boolean v2, p1, Lben;->a:Z

    if-eq v1, v2, :cond_0

    .line 2685
    iget-boolean v1, p0, Lben;->a:Z

    if-nez v1, :cond_2

    .line 2691
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    sget-boolean v1, Lbea;->d:Z

    if-eqz v1, :cond_1

    .line 2692
    const-string v1, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Replacing a GetSelfInfoRequest. Old withRetry="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p1, Lben;->a:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". New withRetry="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lben;->a:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2696
    :cond_1
    return v0

    .line 2685
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2707
    const-string v0, "contacts/getselfinfo"

    return-object v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 2669
    iget-boolean v0, p0, Lben;->a:Z

    if-eqz v0, :cond_0

    .line 2670
    invoke-static {}, Lbkb;->c()J

    move-result-wide v0

    .line 2673
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 2664
    iget-boolean v0, p0, Lben;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
