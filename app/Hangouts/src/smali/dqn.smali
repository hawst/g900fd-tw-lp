.class public final Ldqn;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldqn;


# instance fields
.field public b:Ldvm;

.field public c:Ljava/lang/Integer;

.field public d:Ldsb;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:[Ldth;

.field public i:Ldqw;

.field public j:Ldte;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2737
    const/4 v0, 0x0

    new-array v0, v0, [Ldqn;

    sput-object v0, Ldqn;->a:[Ldqn;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2738
    invoke-direct {p0}, Lepn;-><init>()V

    .line 2741
    iput-object v1, p0, Ldqn;->b:Ldvm;

    .line 2744
    iput-object v1, p0, Ldqn;->c:Ljava/lang/Integer;

    .line 2747
    iput-object v1, p0, Ldqn;->d:Ldsb;

    .line 2756
    sget-object v0, Ldth;->a:[Ldth;

    iput-object v0, p0, Ldqn;->h:[Ldth;

    .line 2759
    iput-object v1, p0, Ldqn;->i:Ldqw;

    .line 2762
    iput-object v1, p0, Ldqn;->j:Ldte;

    .line 2738
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2805
    iget-object v0, p0, Ldqn;->b:Ldvm;

    if-eqz v0, :cond_9

    .line 2806
    const/4 v0, 0x1

    iget-object v2, p0, Ldqn;->b:Ldvm;

    .line 2807
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2809
    :goto_0
    iget-object v2, p0, Ldqn;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 2810
    const/4 v2, 0x2

    iget-object v3, p0, Ldqn;->c:Ljava/lang/Integer;

    .line 2811
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2813
    :cond_0
    iget-object v2, p0, Ldqn;->e:Ljava/lang/Long;

    if-eqz v2, :cond_1

    .line 2814
    const/4 v2, 0x3

    iget-object v3, p0, Ldqn;->e:Ljava/lang/Long;

    .line 2815
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2817
    :cond_1
    iget-object v2, p0, Ldqn;->f:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 2818
    const/4 v2, 0x4

    iget-object v3, p0, Ldqn;->f:Ljava/lang/String;

    .line 2819
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2821
    :cond_2
    iget-object v2, p0, Ldqn;->h:[Ldth;

    if-eqz v2, :cond_4

    .line 2822
    iget-object v2, p0, Ldqn;->h:[Ldth;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 2823
    if-eqz v4, :cond_3

    .line 2824
    const/4 v5, 0x5

    .line 2825
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2822
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2829
    :cond_4
    iget-object v1, p0, Ldqn;->i:Ldqw;

    if-eqz v1, :cond_5

    .line 2830
    const/16 v1, 0xb

    iget-object v2, p0, Ldqn;->i:Ldqw;

    .line 2831
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2833
    :cond_5
    iget-object v1, p0, Ldqn;->j:Ldte;

    if-eqz v1, :cond_6

    .line 2834
    const/16 v1, 0xc

    iget-object v2, p0, Ldqn;->j:Ldte;

    .line 2835
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2837
    :cond_6
    iget-object v1, p0, Ldqn;->d:Ldsb;

    if-eqz v1, :cond_7

    .line 2838
    const/16 v1, 0xd

    iget-object v2, p0, Ldqn;->d:Ldsb;

    .line 2839
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2841
    :cond_7
    iget-object v1, p0, Ldqn;->g:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 2842
    const/16 v1, 0xe

    iget-object v2, p0, Ldqn;->g:Ljava/lang/String;

    .line 2843
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2845
    :cond_8
    iget-object v1, p0, Ldqn;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2846
    iput v0, p0, Ldqn;->cachedSize:I

    .line 2847
    return v0

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2734
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldqn;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldqn;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldqn;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldqn;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldqn;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldqn;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v2, 0x1

    if-eq v0, v2, :cond_3

    const/4 v2, 0x2

    if-ne v0, v2, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqn;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqn;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldqn;->e:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldqn;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldqn;->h:[Ldth;

    if-nez v0, :cond_6

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldth;

    iget-object v3, p0, Ldqn;->h:[Ldth;

    if-eqz v3, :cond_5

    iget-object v3, p0, Ldqn;->h:[Ldth;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Ldqn;->h:[Ldth;

    :goto_2
    iget-object v2, p0, Ldqn;->h:[Ldth;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Ldqn;->h:[Ldth;

    new-instance v3, Ldth;

    invoke-direct {v3}, Ldth;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldqn;->h:[Ldth;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_6
    iget-object v0, p0, Ldqn;->h:[Ldth;

    array-length v0, v0

    goto :goto_1

    :cond_7
    iget-object v2, p0, Ldqn;->h:[Ldth;

    new-instance v3, Ldth;

    invoke-direct {v3}, Ldth;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldqn;->h:[Ldth;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Ldqn;->i:Ldqw;

    if-nez v0, :cond_8

    new-instance v0, Ldqw;

    invoke-direct {v0}, Ldqw;-><init>()V

    iput-object v0, p0, Ldqn;->i:Ldqw;

    :cond_8
    iget-object v0, p0, Ldqn;->i:Ldqw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Ldqn;->j:Ldte;

    if-nez v0, :cond_9

    new-instance v0, Ldte;

    invoke-direct {v0}, Ldte;-><init>()V

    iput-object v0, p0, Ldqn;->j:Ldte;

    :cond_9
    iget-object v0, p0, Ldqn;->j:Ldte;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Ldqn;->d:Ldsb;

    if-nez v0, :cond_a

    new-instance v0, Ldsb;

    invoke-direct {v0}, Ldsb;-><init>()V

    iput-object v0, p0, Ldqn;->d:Ldsb;

    :cond_a
    iget-object v0, p0, Ldqn;->d:Ldsb;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldqn;->g:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x5a -> :sswitch_6
        0x62 -> :sswitch_7
        0x6a -> :sswitch_8
        0x72 -> :sswitch_9
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 2767
    iget-object v0, p0, Ldqn;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 2768
    const/4 v0, 0x1

    iget-object v1, p0, Ldqn;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 2770
    :cond_0
    iget-object v0, p0, Ldqn;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 2771
    const/4 v0, 0x2

    iget-object v1, p0, Ldqn;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 2773
    :cond_1
    iget-object v0, p0, Ldqn;->e:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2774
    const/4 v0, 0x3

    iget-object v1, p0, Ldqn;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 2776
    :cond_2
    iget-object v0, p0, Ldqn;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 2777
    const/4 v0, 0x4

    iget-object v1, p0, Ldqn;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 2779
    :cond_3
    iget-object v0, p0, Ldqn;->h:[Ldth;

    if-eqz v0, :cond_5

    .line 2780
    iget-object v1, p0, Ldqn;->h:[Ldth;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 2781
    if-eqz v3, :cond_4

    .line 2782
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 2780
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2786
    :cond_5
    iget-object v0, p0, Ldqn;->i:Ldqw;

    if-eqz v0, :cond_6

    .line 2787
    const/16 v0, 0xb

    iget-object v1, p0, Ldqn;->i:Ldqw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 2789
    :cond_6
    iget-object v0, p0, Ldqn;->j:Ldte;

    if-eqz v0, :cond_7

    .line 2790
    const/16 v0, 0xc

    iget-object v1, p0, Ldqn;->j:Ldte;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 2792
    :cond_7
    iget-object v0, p0, Ldqn;->d:Ldsb;

    if-eqz v0, :cond_8

    .line 2793
    const/16 v0, 0xd

    iget-object v1, p0, Ldqn;->d:Ldsb;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 2795
    :cond_8
    iget-object v0, p0, Ldqn;->g:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 2796
    const/16 v0, 0xe

    iget-object v1, p0, Ldqn;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 2798
    :cond_9
    iget-object v0, p0, Ldqn;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 2800
    return-void
.end method
