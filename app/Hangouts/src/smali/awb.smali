.class public final Lawb;
.super Lack;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lack",
        "<",
        "Lbel;",
        "Lbgs;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic d:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;)V
    .locals 0

    .prologue
    .line 563
    iput-object p1, p0, Lawb;->d:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    invoke-direct {p0}, Lack;-><init>()V

    .line 564
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 568
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a(Lbos;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 583
    invoke-super {p0, p1}, Lack;->a(Lbos;)V

    .line 585
    invoke-virtual {p1}, Lbos;->c()Lbfz;

    move-result-object v0

    check-cast v0, Lbgs;

    .line 586
    invoke-virtual {v0}, Lbgs;->f()Ljava/util/List;

    move-result-object v6

    .line 588
    if-eqz v6, :cond_0

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    .line 589
    :goto_0
    iget-object v0, p0, Lawb;->d:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->f(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-eq v1, v0, :cond_1

    .line 590
    const-string v0, "Babel"

    const-string v1, "BabelGatewayActivity: Entity lookup returned wrong number of entities"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    iget-object v0, p0, Lawb;->d:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    sget v1, Lh;->cE:I

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->a(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;I)V

    .line 611
    :goto_1
    return-void

    :cond_0
    move v1, v2

    .line 588
    goto :goto_0

    .line 595
    :cond_1
    iget-object v0, p0, Lawb;->d:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    new-array v4, v1, [Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->a(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;[Ljava/lang/String;)[Ljava/lang/String;

    move v5, v2

    .line 596
    :goto_2
    if-ge v5, v1, :cond_7

    .line 598
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzo;

    .line 599
    if-nez v0, :cond_2

    move-object v4, v3

    .line 601
    :goto_3
    if-eqz v4, :cond_3

    aget-object v0, v4, v2

    .line 602
    :goto_4
    if-nez v0, :cond_4

    .line 603
    const-string v0, "Babel"

    const-string v1, "BabelGatewayActivity: Could not resolve some gaiaId\'s"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    iget-object v0, p0, Lawb;->d:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    sget v1, Lh;->cE:I

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->a(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;I)V

    goto :goto_1

    .line 599
    :cond_2
    iget-object v0, v0, Lbzo;->b:Ljava/io/Serializable;

    check-cast v0, [Lbdh;

    move-object v4, v0

    goto :goto_3

    :cond_3
    move-object v0, v3

    .line 601
    goto :goto_4

    .line 607
    :cond_4
    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    array-length v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v7, v4}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 608
    iget-object v4, p0, Lawb;->d:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    invoke-static {v4}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->g(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;)[Ljava/lang/String;

    move-result-object v4

    iget-object v7, v0, Lbdh;->e:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    iget-object v0, v0, Lbdh;->e:Ljava/lang/String;

    :goto_5
    aput-object v0, v4, v5

    .line 596
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_2

    .line 608
    :cond_5
    iget-object v7, v0, Lbdh;->f:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_6

    iget-object v0, v0, Lbdh;->f:Ljava/lang/String;

    goto :goto_5

    :cond_6
    move-object v0, v3

    goto :goto_5

    .line 610
    :cond_7
    iget-object v0, p0, Lawb;->d:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->h(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;)V

    goto :goto_1
.end method

.method protected a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 615
    invoke-super {p0, p1}, Lack;->a(Ljava/lang/Exception;)V

    .line 616
    iget-object v0, p0, Lawb;->d:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    sget v1, Lh;->cE:I

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->a(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;I)V

    .line 617
    return-void
.end method

.method public b()I
    .locals 5

    .prologue
    .line 573
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 574
    iget-object v0, p0, Lawb;->d:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->f(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 575
    invoke-static {v4}, Lbcn;->a(Ljava/lang/String;)Lbcn;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 574
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 577
    :cond_0
    iget-object v0, p0, Lawb;->d:Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->c(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;)Lyj;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/util/ArrayList;Ljava/lang/String;Z)I

    move-result v0

    return v0
.end method

.method public e()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lbel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 631
    const-class v0, Lbel;

    return-object v0
.end method

.method public f()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lbgs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 636
    const-class v0, Lbgs;

    return-object v0
.end method
