.class public final Ldzy;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldzy;


# instance fields
.field public b:Ldzx;

.field public c:Ldoc;

.field public d:Ldok;

.field public e:Ldol;

.field public f:Ldon;

.field public g:Ldoq;

.field public h:Ldos;

.field public i:Ldzz;

.field public j:Ldzw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Ldzy;

    sput-object v0, Ldzy;->a:[Ldzy;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 13
    iput-object v0, p0, Ldzy;->b:Ldzx;

    .line 16
    iput-object v0, p0, Ldzy;->c:Ldoc;

    .line 19
    iput-object v0, p0, Ldzy;->d:Ldok;

    .line 22
    iput-object v0, p0, Ldzy;->e:Ldol;

    .line 25
    iput-object v0, p0, Ldzy;->f:Ldon;

    .line 28
    iput-object v0, p0, Ldzy;->g:Ldoq;

    .line 31
    iput-object v0, p0, Ldzy;->h:Ldos;

    .line 34
    iput-object v0, p0, Ldzy;->i:Ldzz;

    .line 37
    iput-object v0, p0, Ldzy;->j:Ldzw;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 75
    const/4 v0, 0x0

    .line 76
    iget-object v1, p0, Ldzy;->b:Ldzx;

    if-eqz v1, :cond_0

    .line 77
    const/4 v0, 0x1

    iget-object v1, p0, Ldzy;->b:Ldzx;

    .line 78
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 80
    :cond_0
    iget-object v1, p0, Ldzy;->c:Ldoc;

    if-eqz v1, :cond_1

    .line 81
    const/4 v1, 0x2

    iget-object v2, p0, Ldzy;->c:Ldoc;

    .line 82
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    :cond_1
    iget-object v1, p0, Ldzy;->d:Ldok;

    if-eqz v1, :cond_2

    .line 85
    const/4 v1, 0x3

    iget-object v2, p0, Ldzy;->d:Ldok;

    .line 86
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 88
    :cond_2
    iget-object v1, p0, Ldzy;->e:Ldol;

    if-eqz v1, :cond_3

    .line 89
    const/4 v1, 0x4

    iget-object v2, p0, Ldzy;->e:Ldol;

    .line 90
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    :cond_3
    iget-object v1, p0, Ldzy;->f:Ldon;

    if-eqz v1, :cond_4

    .line 93
    const/4 v1, 0x5

    iget-object v2, p0, Ldzy;->f:Ldon;

    .line 94
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    :cond_4
    iget-object v1, p0, Ldzy;->g:Ldoq;

    if-eqz v1, :cond_5

    .line 97
    const/4 v1, 0x6

    iget-object v2, p0, Ldzy;->g:Ldoq;

    .line 98
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 100
    :cond_5
    iget-object v1, p0, Ldzy;->h:Ldos;

    if-eqz v1, :cond_6

    .line 101
    const/4 v1, 0x7

    iget-object v2, p0, Ldzy;->h:Ldos;

    .line 102
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 104
    :cond_6
    iget-object v1, p0, Ldzy;->i:Ldzz;

    if-eqz v1, :cond_7

    .line 105
    const/16 v1, 0x8

    iget-object v2, p0, Ldzy;->i:Ldzz;

    .line 106
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 108
    :cond_7
    iget-object v1, p0, Ldzy;->j:Ldzw;

    if-eqz v1, :cond_8

    .line 109
    const/16 v1, 0x9

    iget-object v2, p0, Ldzy;->j:Ldzw;

    .line 110
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 112
    :cond_8
    iget-object v1, p0, Ldzy;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 113
    iput v0, p0, Ldzy;->cachedSize:I

    .line 114
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldzy;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldzy;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldzy;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldzy;->b:Ldzx;

    if-nez v0, :cond_2

    new-instance v0, Ldzx;

    invoke-direct {v0}, Ldzx;-><init>()V

    iput-object v0, p0, Ldzy;->b:Ldzx;

    :cond_2
    iget-object v0, p0, Ldzy;->b:Ldzx;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldzy;->c:Ldoc;

    if-nez v0, :cond_3

    new-instance v0, Ldoc;

    invoke-direct {v0}, Ldoc;-><init>()V

    iput-object v0, p0, Ldzy;->c:Ldoc;

    :cond_3
    iget-object v0, p0, Ldzy;->c:Ldoc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldzy;->d:Ldok;

    if-nez v0, :cond_4

    new-instance v0, Ldok;

    invoke-direct {v0}, Ldok;-><init>()V

    iput-object v0, p0, Ldzy;->d:Ldok;

    :cond_4
    iget-object v0, p0, Ldzy;->d:Ldok;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldzy;->e:Ldol;

    if-nez v0, :cond_5

    new-instance v0, Ldol;

    invoke-direct {v0}, Ldol;-><init>()V

    iput-object v0, p0, Ldzy;->e:Ldol;

    :cond_5
    iget-object v0, p0, Ldzy;->e:Ldol;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldzy;->f:Ldon;

    if-nez v0, :cond_6

    new-instance v0, Ldon;

    invoke-direct {v0}, Ldon;-><init>()V

    iput-object v0, p0, Ldzy;->f:Ldon;

    :cond_6
    iget-object v0, p0, Ldzy;->f:Ldon;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ldzy;->g:Ldoq;

    if-nez v0, :cond_7

    new-instance v0, Ldoq;

    invoke-direct {v0}, Ldoq;-><init>()V

    iput-object v0, p0, Ldzy;->g:Ldoq;

    :cond_7
    iget-object v0, p0, Ldzy;->g:Ldoq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Ldzy;->h:Ldos;

    if-nez v0, :cond_8

    new-instance v0, Ldos;

    invoke-direct {v0}, Ldos;-><init>()V

    iput-object v0, p0, Ldzy;->h:Ldos;

    :cond_8
    iget-object v0, p0, Ldzy;->h:Ldos;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Ldzy;->i:Ldzz;

    if-nez v0, :cond_9

    new-instance v0, Ldzz;

    invoke-direct {v0}, Ldzz;-><init>()V

    iput-object v0, p0, Ldzy;->i:Ldzz;

    :cond_9
    iget-object v0, p0, Ldzy;->i:Ldzz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Ldzy;->j:Ldzw;

    if-nez v0, :cond_a

    new-instance v0, Ldzw;

    invoke-direct {v0}, Ldzw;-><init>()V

    iput-object v0, p0, Ldzy;->j:Ldzw;

    :cond_a
    iget-object v0, p0, Ldzy;->j:Ldzw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Ldzy;->b:Ldzx;

    if-eqz v0, :cond_0

    .line 43
    const/4 v0, 0x1

    iget-object v1, p0, Ldzy;->b:Ldzx;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 45
    :cond_0
    iget-object v0, p0, Ldzy;->c:Ldoc;

    if-eqz v0, :cond_1

    .line 46
    const/4 v0, 0x2

    iget-object v1, p0, Ldzy;->c:Ldoc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 48
    :cond_1
    iget-object v0, p0, Ldzy;->d:Ldok;

    if-eqz v0, :cond_2

    .line 49
    const/4 v0, 0x3

    iget-object v1, p0, Ldzy;->d:Ldok;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 51
    :cond_2
    iget-object v0, p0, Ldzy;->e:Ldol;

    if-eqz v0, :cond_3

    .line 52
    const/4 v0, 0x4

    iget-object v1, p0, Ldzy;->e:Ldol;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 54
    :cond_3
    iget-object v0, p0, Ldzy;->f:Ldon;

    if-eqz v0, :cond_4

    .line 55
    const/4 v0, 0x5

    iget-object v1, p0, Ldzy;->f:Ldon;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 57
    :cond_4
    iget-object v0, p0, Ldzy;->g:Ldoq;

    if-eqz v0, :cond_5

    .line 58
    const/4 v0, 0x6

    iget-object v1, p0, Ldzy;->g:Ldoq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 60
    :cond_5
    iget-object v0, p0, Ldzy;->h:Ldos;

    if-eqz v0, :cond_6

    .line 61
    const/4 v0, 0x7

    iget-object v1, p0, Ldzy;->h:Ldos;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 63
    :cond_6
    iget-object v0, p0, Ldzy;->i:Ldzz;

    if-eqz v0, :cond_7

    .line 64
    const/16 v0, 0x8

    iget-object v1, p0, Ldzy;->i:Ldzz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 66
    :cond_7
    iget-object v0, p0, Ldzy;->j:Ldzw;

    if-eqz v0, :cond_8

    .line 67
    const/16 v0, 0x9

    iget-object v1, p0, Ldzy;->j:Ldzw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 69
    :cond_8
    iget-object v0, p0, Ldzy;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 71
    return-void
.end method
