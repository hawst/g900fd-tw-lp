.class public final Lccp;
.super Lcct;
.source "PG"


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field private d:Lccu;

.field private e:Lcom/google/android/apps/hangouts/views/MessageListItemView;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lccp;-><init>(Landroid/content/Context;B)V

    .line 27
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcct;-><init>(Landroid/content/Context;)V

    .line 31
    return-void
.end method

.method static synthetic a(Lccp;)Lcom/google/android/apps/hangouts/views/MessageListItemView;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lccp;->e:Lcom/google/android/apps/hangouts/views/MessageListItemView;

    return-object v0
.end method

.method static synthetic b(Lccp;)Lccu;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lccp;->d:Lccu;

    return-object v0
.end method

.method static synthetic c(Lccp;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lccp;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lccp;->a:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public a(Lyj;ZLjava/lang/String;IILccu;Lcom/google/android/apps/hangouts/views/MessageListItemView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0, p3}, Lccp;->a(Ljava/lang/String;)V

    .line 38
    iput-object p9, p0, Lccp;->b:Ljava/lang/String;

    .line 39
    iput-object p6, p0, Lccp;->d:Lccu;

    .line 40
    iput-object p7, p0, Lccp;->e:Lcom/google/android/apps/hangouts/views/MessageListItemView;

    .line 41
    const-string v0, "image/gif"

    invoke-virtual {v0, p8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lccp;->f:Z

    .line 43
    iget-boolean v0, p0, Lccp;->f:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 44
    :cond_0
    new-instance v0, Lccq;

    invoke-direct {v0, p0}, Lccq;-><init>(Lccp;)V

    invoke-virtual {p0, v0}, Lccp;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    :cond_1
    sget v0, Lh;->fE:I

    invoke-virtual {p0, v0}, Lccp;->a(I)V

    .line 57
    invoke-super/range {p0 .. p5}, Lcct;->a(Lyj;ZLjava/lang/String;II)V

    .line 58
    return-void
.end method

.method protected a()Z
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    return v0
.end method
