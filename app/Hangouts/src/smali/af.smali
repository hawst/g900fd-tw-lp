.class final Laf;
.super Lae;
.source "PG"


# static fields
.field static final A:Landroid/view/animation/Interpolator;

.field static final B:Landroid/view/animation/Interpolator;

.field static final C:Landroid/view/animation/Interpolator;

.field static a:Z

.field static final b:Z

.field static final z:Landroid/view/animation/Interpolator;


# instance fields
.field c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field d:[Ljava/lang/Runnable;

.field e:Z

.field f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lt;",
            ">;"
        }
    .end annotation
.end field

.field g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lt;",
            ">;"
        }
    .end annotation
.end field

.field h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lo;",
            ">;"
        }
    .end annotation
.end field

.field j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lt;",
            ">;"
        }
    .end annotation
.end field

.field k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lo;",
            ">;"
        }
    .end annotation
.end field

.field l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "La;",
            ">;"
        }
    .end annotation
.end field

.field n:I

.field o:Ly;

.field p:Lad;

.field q:Lt;

.field r:Z

.field s:Z

.field t:Z

.field u:Ljava/lang/String;

.field v:Z

.field w:Landroid/os/Bundle;

.field x:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field y:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/high16 v4, 0x40200000    # 2.5f

    const/high16 v3, 0x3fc00000    # 1.5f

    .line 406
    sput-boolean v0, Laf;->a:Z

    .line 409
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    sput-boolean v0, Laf;->b:Z

    .line 742
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v4}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Laf;->z:Landroid/view/animation/Interpolator;

    .line 743
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Laf;->A:Landroid/view/animation/Interpolator;

    .line 744
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v4}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    sput-object v0, Laf;->B:Landroid/view/animation/Interpolator;

    .line 745
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v3}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    sput-object v0, Laf;->C:Landroid/view/animation/Interpolator;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 405
    invoke-direct {p0}, Lae;-><init>()V

    .line 432
    const/4 v0, 0x0

    iput v0, p0, Laf;->n:I

    .line 444
    iput-object v1, p0, Laf;->w:Landroid/os/Bundle;

    .line 445
    iput-object v1, p0, Laf;->x:Landroid/util/SparseArray;

    .line 447
    new-instance v0, Lag;

    invoke-direct {v0, p0}, Lag;-><init>(Laf;)V

    iput-object v0, p0, Laf;->y:Ljava/lang/Runnable;

    return-void
.end method

.method private static a(FF)Landroid/view/animation/Animation;
    .locals 3

    .prologue
    .line 765
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p0, p1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 766
    sget-object v1, Laf;->A:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 767
    const-wide/16 v1, 0xdc

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 768
    return-object v0
.end method

.method private static a(FFFF)Landroid/view/animation/Animation;
    .locals 12

    .prologue
    const-wide/16 v10, 0xdc

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    .line 751
    new-instance v9, Landroid/view/animation/AnimationSet;

    const/4 v0, 0x0

    invoke-direct {v9, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 752
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v1, p0

    move v2, p1

    move v3, p0

    move v4, p1

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 754
    sget-object v1, Laf;->z:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 755
    invoke-virtual {v0, v10, v11}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 756
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 757
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p2, p3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 758
    sget-object v1, Laf;->A:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 759
    invoke-virtual {v0, v10, v11}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 760
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 761
    return-object v9
.end method

.method private a(Lt;IZI)Landroid/view/animation/Animation;
    .locals 6

    .prologue
    const v5, 0x3f79999a    # 0.975f

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 773
    iget v0, p1, Lt;->P:I

    invoke-virtual {p1, p2, p3, v0}, Lt;->onCreateAnimation(IZI)Landroid/view/animation/Animation;

    move-result-object v0

    .line 775
    if-eqz v0, :cond_1

    .line 827
    :cond_0
    :goto_0
    return-object v0

    .line 779
    :cond_1
    iget v0, p1, Lt;->P:I

    if-eqz v0, :cond_2

    .line 780
    iget-object v0, p0, Laf;->o:Ly;

    iget v2, p1, Lt;->P:I

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 781
    if-nez v0, :cond_0

    .line 786
    :cond_2
    if-nez p2, :cond_3

    move-object v0, v1

    .line 787
    goto :goto_0

    .line 790
    :cond_3
    const/4 v0, -0x1

    sparse-switch p2, :sswitch_data_0

    .line 791
    :goto_1
    if-gez v0, :cond_7

    move-object v0, v1

    .line 792
    goto :goto_0

    .line 790
    :sswitch_0
    if-eqz p3, :cond_4

    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, 0x2

    goto :goto_1

    :sswitch_1
    if-eqz p3, :cond_5

    const/4 v0, 0x3

    goto :goto_1

    :cond_5
    const/4 v0, 0x4

    goto :goto_1

    :sswitch_2
    if-eqz p3, :cond_6

    const/4 v0, 0x5

    goto :goto_1

    :cond_6
    const/4 v0, 0x6

    goto :goto_1

    .line 795
    :cond_7
    packed-switch v0, :pswitch_data_0

    .line 810
    if-nez p4, :cond_8

    iget-object v0, p0, Laf;->o:Ly;

    invoke-virtual {v0}, Ly;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 811
    iget-object v0, p0, Laf;->o:Ly;

    invoke-virtual {v0}, Ly;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget p4, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 813
    :cond_8
    if-nez p4, :cond_9

    move-object v0, v1

    .line 814
    goto :goto_0

    .line 797
    :pswitch_0
    iget-object v0, p0, Laf;->o:Ly;

    const/high16 v0, 0x3f900000    # 1.125f

    invoke-static {v0, v3, v4, v3}, Laf;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 799
    :pswitch_1
    iget-object v0, p0, Laf;->o:Ly;

    invoke-static {v3, v5, v3, v4}, Laf;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 801
    :pswitch_2
    iget-object v0, p0, Laf;->o:Ly;

    invoke-static {v5, v3, v4, v3}, Laf;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 803
    :pswitch_3
    iget-object v0, p0, Laf;->o:Ly;

    const v0, 0x3f89999a    # 1.075f

    invoke-static {v3, v0, v3, v4}, Laf;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 805
    :pswitch_4
    iget-object v0, p0, Laf;->o:Ly;

    invoke-static {v4, v3}, Laf;->a(FF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 807
    :pswitch_5
    iget-object v0, p0, Laf;->o:Ly;

    invoke-static {v3, v4}, Laf;->a(FF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    :cond_9
    move-object v0, v1

    .line 827
    goto :goto_0

    .line 790
    :sswitch_data_0
    .sparse-switch
        0x1001 -> :sswitch_0
        0x1003 -> :sswitch_2
        0x2002 -> :sswitch_1
    .end sparse-switch

    .line 795
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private a(Landroid/os/Bundle;Ljava/lang/String;)Lt;
    .locals 5

    .prologue
    const/4 v0, -0x1

    .line 575
    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 576
    if-ne v1, v0, :cond_1

    .line 577
    const/4 v0, 0x0

    .line 588
    :cond_0
    :goto_0
    return-object v0

    .line 579
    :cond_1
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_2

    .line 580
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fragment no longer exists for key "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Laf;->a(Ljava/lang/RuntimeException;)V

    .line 583
    :cond_2
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 584
    if-nez v0, :cond_0

    .line 585
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Fragment no longer exists for key "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": index "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2}, Laf;->a(Ljava/lang/RuntimeException;)V

    goto :goto_0
.end method

.method private a(ILo;)V
    .locals 3

    .prologue
    .line 1416
    monitor-enter p0

    .line 1417
    :try_start_0
    iget-object v0, p0, Laf;->k:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1418
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laf;->k:Ljava/util/ArrayList;

    .line 1420
    :cond_0
    iget-object v0, p0, Laf;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1421
    if-ge p1, v0, :cond_2

    .line 1422
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Setting back stack index "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1423
    :cond_1
    iget-object v0, p0, Laf;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1437
    :goto_0
    monitor-exit p0

    return-void

    .line 1425
    :cond_2
    :goto_1
    if-ge v0, p1, :cond_4

    .line 1426
    iget-object v1, p0, Laf;->k:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1427
    iget-object v1, p0, Laf;->l:Ljava/util/ArrayList;

    if-nez v1, :cond_3

    .line 1428
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Laf;->l:Ljava/util/ArrayList;

    .line 1430
    :cond_3
    sget-boolean v1, Laf;->a:Z

    .line 1431
    iget-object v1, p0, Laf;->l:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1432
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1434
    :cond_4
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Adding back stack index "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " with "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1435
    :cond_5
    iget-object v0, p0, Laf;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1437
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Ljava/lang/RuntimeException;)V
    .locals 5

    .prologue
    .line 455
    const-string v0, "FragmentManager"

    invoke-virtual {p1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    const-string v0, "FragmentManager"

    const-string v1, "Activity state:"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    new-instance v0, Lev;

    const-string v1, "FragmentManager"

    invoke-direct {v0, v1}, Lev;-><init>(Ljava/lang/String;)V

    .line 458
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 459
    iget-object v0, p0, Laf;->o:Ly;

    if-eqz v0, :cond_0

    .line 461
    :try_start_0
    iget-object v0, p0, Laf;->o:Ly;

    const-string v2, "  "

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v1, v4}, Ly;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 472
    :goto_0
    throw p1

    .line 462
    :catch_0
    move-exception v0

    .line 463
    const-string v1, "FragmentManager"

    const-string v2, "Failed dumping state"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 467
    :cond_0
    :try_start_1
    const-string v0, "  "

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {p0, v0, v2, v1, v3}, Laf;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 468
    :catch_1
    move-exception v0

    .line 469
    const-string v1, "FragmentManager"

    const-string v2, "Failed dumping state"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private c(Lt;)V
    .locals 2

    .prologue
    .line 1606
    iget-object v0, p1, Lt;->S:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1619
    :cond_0
    :goto_0
    return-void

    .line 1609
    :cond_1
    iget-object v0, p0, Laf;->x:Landroid/util/SparseArray;

    if-nez v0, :cond_2

    .line 1610
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Laf;->x:Landroid/util/SparseArray;

    .line 1614
    :goto_1
    iget-object v0, p1, Lt;->S:Landroid/view/View;

    iget-object v1, p0, Laf;->x:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 1615
    iget-object v0, p0, Laf;->x:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1616
    iget-object v0, p0, Laf;->x:Landroid/util/SparseArray;

    iput-object v0, p1, Lt;->n:Landroid/util/SparseArray;

    .line 1617
    const/4 v0, 0x0

    iput-object v0, p0, Laf;->x:Landroid/util/SparseArray;

    goto :goto_0

    .line 1612
    :cond_2
    iget-object v0, p0, Laf;->x:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    goto :goto_1
.end method

.method public static d(I)I
    .locals 1

    .prologue
    .line 2053
    const/4 v0, 0x0

    .line 2054
    sparse-switch p0, :sswitch_data_0

    .line 2065
    :goto_0
    return v0

    .line 2056
    :sswitch_0
    const/16 v0, 0x2002

    .line 2057
    goto :goto_0

    .line 2059
    :sswitch_1
    const/16 v0, 0x1001

    .line 2060
    goto :goto_0

    .line 2062
    :sswitch_2
    const/16 v0, 0x1003

    goto :goto_0

    .line 2054
    :sswitch_data_0
    .sparse-switch
        0x1001 -> :sswitch_0
        0x1003 -> :sswitch_2
        0x2002 -> :sswitch_1
    .end sparse-switch
.end method

.method private e(I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1103
    invoke-virtual {p0, p1, v0, v0, v0}, Laf;->a(IIIZ)V

    .line 1104
    return-void
.end method

.method private u()V
    .locals 3

    .prologue
    .line 1359
    iget-boolean v0, p0, Laf;->s:Z

    if-eqz v0, :cond_0

    .line 1360
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can not perform this action after onSaveInstanceState"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1363
    :cond_0
    iget-object v0, p0, Laf;->u:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1364
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not perform this action inside of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Laf;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1367
    :cond_1
    return-void
.end method

.method private v()V
    .locals 2

    .prologue
    .line 1508
    iget-object v0, p0, Laf;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1509
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Laf;->m:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1510
    iget-object v1, p0, Laf;->m:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 1509
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1513
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lo;)I
    .locals 3

    .prologue
    .line 1396
    monitor-enter p0

    .line 1397
    :try_start_0
    iget-object v0, p0, Laf;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laf;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_3

    .line 1398
    :cond_0
    iget-object v0, p0, Laf;->k:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1399
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laf;->k:Ljava/util/ArrayList;

    .line 1401
    :cond_1
    iget-object v0, p0, Laf;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1402
    sget-boolean v1, Laf;->a:Z

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Setting back stack index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1403
    :cond_2
    iget-object v1, p0, Laf;->k:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1404
    monitor-exit p0

    .line 1410
    :goto_0
    return v0

    .line 1407
    :cond_3
    iget-object v0, p0, Laf;->l:Ljava/util/ArrayList;

    iget-object v1, p0, Laf;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1408
    sget-boolean v1, Laf;->a:Z

    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Adding back stack index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1409
    :cond_4
    iget-object v1, p0, Laf;->k:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1410
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1412
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()Lao;
    .locals 1

    .prologue
    .line 477
    new-instance v0, Lo;

    invoke-direct {v0, p0}, Lo;-><init>(Laf;)V

    return-object v0
.end method

.method public a(I)Lt;
    .locals 3

    .prologue
    .line 1303
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 1305
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 1306
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 1307
    if-eqz v0, :cond_1

    iget v2, v0, Lt;->F:I

    if-ne v2, p1, :cond_1

    .line 1321
    :cond_0
    :goto_1
    return-object v0

    .line 1305
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1312
    :cond_2
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 1314
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_4

    .line 1315
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 1316
    if-eqz v0, :cond_3

    iget v2, v0, Lt;->F:I

    if-eq v2, p1, :cond_0

    .line 1314
    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 1321
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)Lt;
    .locals 3

    .prologue
    .line 1325
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    .line 1327
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 1328
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 1329
    if-eqz v0, :cond_1

    iget-object v2, v0, Lt;->H:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1343
    :cond_0
    :goto_1
    return-object v0

    .line 1327
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1334
    :cond_2
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    if-eqz p1, :cond_4

    .line 1336
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_4

    .line 1337
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 1338
    if-eqz v0, :cond_3

    iget-object v2, v0, Lt;->H:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1336
    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 1343
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method a(IIIZ)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1107
    iget-object v0, p0, Laf;->o:Ly;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 1108
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No activity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1111
    :cond_0
    if-nez p4, :cond_2

    iget v0, p0, Laf;->n:I

    if-ne v0, p1, :cond_2

    .line 1137
    :cond_1
    :goto_0
    return-void

    .line 1115
    :cond_2
    iput p1, p0, Laf;->n:I

    .line 1116
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    move v6, v5

    move v7, v5

    .line 1118
    :goto_1
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_3

    .line 1119
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lt;

    .line 1120
    if-eqz v1, :cond_5

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    .line 1121
    invoke-virtual/range {v0 .. v5}, Laf;->a(Lt;IIIZ)V

    .line 1122
    iget-object v0, v1, Lt;->V:Lax;

    if-eqz v0, :cond_5

    .line 1123
    iget-object v0, v1, Lt;->V:Lax;

    invoke-virtual {v0}, Lax;->a()Z

    move-result v0

    or-int/2addr v7, v0

    move v1, v7

    .line 1118
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v7, v1

    goto :goto_1

    .line 1128
    :cond_3
    if-nez v7, :cond_4

    .line 1129
    invoke-virtual {p0}, Laf;->g()V

    .line 1132
    :cond_4
    iget-boolean v0, p0, Laf;->r:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Laf;->o:Ly;

    if-eqz v0, :cond_1

    iget v0, p0, Laf;->n:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 1133
    iget-object v0, p0, Laf;->o:Ly;

    invoke-virtual {v0}, Ly;->u_()V

    .line 1134
    iput-boolean v5, p0, Laf;->r:Z

    goto :goto_0

    :cond_5
    move v1, v7

    goto :goto_2
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 1945
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 1946
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1947
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 1948
    if-eqz v0, :cond_0

    .line 1949
    invoke-virtual {v0, p1}, Lt;->a(Landroid/content/res/Configuration;)V

    .line 1946
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1953
    :cond_1
    return-void
.end method

.method a(Landroid/os/Parcelable;Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcelable;",
            "Ljava/util/ArrayList",
            "<",
            "Lt;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 1773
    if-nez p1, :cond_1

    .line 1880
    :cond_0
    :goto_0
    return-void

    .line 1774
    :cond_1
    check-cast p1, Laj;

    .line 1775
    iget-object v0, p1, Laj;->a:[Lam;

    if-eqz v0, :cond_0

    .line 1779
    if-eqz p2, :cond_4

    move v1, v2

    .line 1780
    :goto_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 1781
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 1782
    sget-boolean v3, Laf;->a:Z

    if-eqz v3, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "restoreAllState: re-attaching retained "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1783
    :cond_2
    iget-object v3, p1, Laj;->a:[Lam;

    iget v4, v0, Lt;->o:I

    aget-object v3, v3, v4

    .line 1784
    iput-object v0, v3, Lam;->k:Lt;

    .line 1785
    iput-object v6, v0, Lt;->n:Landroid/util/SparseArray;

    .line 1786
    iput v2, v0, Lt;->A:I

    .line 1787
    iput-boolean v2, v0, Lt;->y:Z

    .line 1788
    iput-boolean v2, v0, Lt;->u:Z

    .line 1789
    iput-object v6, v0, Lt;->r:Lt;

    .line 1790
    iget-object v4, v3, Lam;->j:Landroid/os/Bundle;

    if-eqz v4, :cond_3

    .line 1791
    iget-object v4, v3, Lam;->j:Landroid/os/Bundle;

    iget-object v5, p0, Laf;->o:Ly;

    invoke-virtual {v5}, Ly;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 1792
    iget-object v3, v3, Lam;->j:Landroid/os/Bundle;

    const-string v4, "android:view_state"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v3

    iput-object v3, v0, Lt;->n:Landroid/util/SparseArray;

    .line 1780
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1800
    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Laj;->a:[Lam;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    .line 1801
    iget-object v0, p0, Laf;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    .line 1802
    iget-object v0, p0, Laf;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_5
    move v0, v2

    .line 1804
    :goto_2
    iget-object v1, p1, Laj;->a:[Lam;

    array-length v1, v1

    if-ge v0, v1, :cond_9

    .line 1805
    iget-object v1, p1, Laj;->a:[Lam;

    aget-object v1, v1, v0

    .line 1806
    if-eqz v1, :cond_7

    .line 1807
    iget-object v3, p0, Laf;->o:Ly;

    iget-object v4, p0, Laf;->q:Lt;

    invoke-virtual {v1, v3, v4}, Lam;->a(Ly;Lt;)Lt;

    move-result-object v3

    .line 1808
    sget-boolean v4, Laf;->a:Z

    if-eqz v4, :cond_6

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "restoreAllState: active #"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1809
    :cond_6
    iget-object v4, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1813
    iput-object v6, v1, Lam;->k:Lt;

    .line 1804
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1815
    :cond_7
    iget-object v1, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1816
    iget-object v1, p0, Laf;->h:Ljava/util/ArrayList;

    if-nez v1, :cond_8

    .line 1817
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Laf;->h:Ljava/util/ArrayList;

    .line 1819
    :cond_8
    sget-boolean v1, Laf;->a:Z

    .line 1820
    iget-object v1, p0, Laf;->h:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1825
    :cond_9
    if-eqz p2, :cond_c

    move v3, v2

    .line 1826
    :goto_4
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_c

    .line 1827
    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 1828
    iget v1, v0, Lt;->s:I

    if-ltz v1, :cond_a

    .line 1829
    iget v1, v0, Lt;->s:I

    iget-object v4, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_b

    .line 1830
    iget-object v1, p0, Laf;->f:Ljava/util/ArrayList;

    iget v4, v0, Lt;->s:I

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lt;

    iput-object v1, v0, Lt;->r:Lt;

    .line 1826
    :cond_a
    :goto_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 1832
    :cond_b
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Re-attaching retained fragment "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " target no longer exists: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, v0, Lt;->s:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1834
    iput-object v6, v0, Lt;->r:Lt;

    goto :goto_5

    .line 1841
    :cond_c
    iget-object v0, p1, Laj;->b:[I

    if-eqz v0, :cond_10

    .line 1842
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Laj;->b:[I

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    move v1, v2

    .line 1843
    :goto_6
    iget-object v0, p1, Laj;->b:[I

    array-length v0, v0

    if-ge v1, v0, :cond_11

    .line 1844
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    iget-object v3, p1, Laj;->b:[I

    aget v3, v3, v1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 1845
    if-nez v0, :cond_d

    .line 1846
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "No instantiated fragment for index #"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p1, Laj;->b:[I

    aget v5, v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v3}, Laf;->a(Ljava/lang/RuntimeException;)V

    .line 1849
    :cond_d
    const/4 v3, 0x1

    iput-boolean v3, v0, Lt;->u:Z

    .line 1850
    sget-boolean v3, Laf;->a:Z

    if-eqz v3, :cond_e

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "restoreAllState: added #"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1851
    :cond_e
    iget-object v3, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 1852
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already added!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1854
    :cond_f
    iget-object v3, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1843
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 1857
    :cond_10
    iput-object v6, p0, Laf;->g:Ljava/util/ArrayList;

    .line 1861
    :cond_11
    iget-object v0, p1, Laj;->c:[Lq;

    if-eqz v0, :cond_14

    .line 1862
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Laj;->c:[Lq;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Laf;->i:Ljava/util/ArrayList;

    move v0, v2

    .line 1863
    :goto_7
    iget-object v1, p1, Laj;->c:[Lq;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1864
    iget-object v1, p1, Laj;->c:[Lq;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Lq;->a(Laf;)Lo;

    move-result-object v1

    .line 1865
    sget-boolean v3, Laf;->a:Z

    if-eqz v3, :cond_12

    .line 1866
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "restoreAllState: back stack #"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " (index "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lo;->o:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1868
    new-instance v3, Lev;

    const-string v4, "FragmentManager"

    invoke-direct {v3, v4}, Lev;-><init>(Ljava/lang/String;)V

    .line 1869
    new-instance v4, Ljava/io/PrintWriter;

    invoke-direct {v4, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 1870
    const-string v3, "  "

    invoke-virtual {v1, v3, v4, v2}, Lo;->a(Ljava/lang/String;Ljava/io/PrintWriter;Z)V

    .line 1872
    :cond_12
    iget-object v3, p0, Laf;->i:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1873
    iget v3, v1, Lo;->o:I

    if-ltz v3, :cond_13

    .line 1874
    iget v3, v1, Lo;->o:I

    invoke-direct {p0, v3, v1}, Laf;->a(ILo;)V

    .line 1863
    :cond_13
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 1878
    :cond_14
    iput-object v6, p0, Laf;->i:Ljava/util/ArrayList;

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/Runnable;Z)V
    .locals 2

    .prologue
    .line 1377
    if-nez p2, :cond_0

    .line 1378
    invoke-direct {p0}, Laf;->u()V

    .line 1380
    :cond_0
    monitor-enter p0

    .line 1381
    :try_start_0
    iget-boolean v0, p0, Laf;->t:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Laf;->o:Ly;

    if-nez v0, :cond_2

    .line 1382
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity has been destroyed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1392
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1384
    :cond_2
    :try_start_1
    iget-object v0, p0, Laf;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    .line 1385
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laf;->c:Ljava/util/ArrayList;

    .line 1387
    :cond_3
    iget-object v0, p0, Laf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1388
    iget-object v0, p0, Laf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 1389
    iget-object v0, p0, Laf;->o:Ly;

    iget-object v0, v0, Ly;->a:Landroid/os/Handler;

    iget-object v1, p0, Laf;->y:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1390
    iget-object v0, p0, Laf;->o:Ly;

    iget-object v0, v0, Ly;->a:Landroid/os/Handler;

    iget-object v1, p0, Laf;->y:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1392
    :cond_4
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 631
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "    "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 634
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 635
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 636
    if-lez v4, :cond_1

    .line 637
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Active Fragments in "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 638
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 639
    const-string v0, ":"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 640
    :goto_0
    if-ge v2, v4, :cond_1

    .line 641
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 642
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 643
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 644
    if-eqz v0, :cond_0

    .line 645
    invoke-virtual {v0, v3, p2, p3, p4}, Lt;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 640
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 651
    :cond_1
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 652
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 653
    if-lez v4, :cond_2

    .line 654
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Added Fragments:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 655
    :goto_1
    if-ge v2, v4, :cond_2

    .line 656
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 657
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 658
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Lt;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 655
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 663
    :cond_2
    iget-object v0, p0, Laf;->j:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 664
    iget-object v0, p0, Laf;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 665
    if-lez v4, :cond_3

    .line 666
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Fragments Created Menus:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 667
    :goto_2
    if-ge v2, v4, :cond_3

    .line 668
    iget-object v0, p0, Laf;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 669
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 670
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Lt;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 667
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 675
    :cond_3
    iget-object v0, p0, Laf;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 676
    iget-object v0, p0, Laf;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 677
    if-lez v4, :cond_4

    .line 678
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Back Stack:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 679
    :goto_3
    if-ge v2, v4, :cond_4

    .line 680
    iget-object v0, p0, Laf;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo;

    .line 681
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 682
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Lo;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 683
    invoke-virtual {v0, v3, p3}, Lo;->a(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 679
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 688
    :cond_4
    monitor-enter p0

    .line 689
    :try_start_0
    iget-object v0, p0, Laf;->k:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    .line 690
    iget-object v0, p0, Laf;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 691
    if-lez v3, :cond_5

    .line 692
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Back Stack Indices:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 693
    :goto_4
    if-ge v2, v3, :cond_5

    .line 694
    iget-object v0, p0, Laf;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo;

    .line 695
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v4, "  #"

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 696
    const-string v4, ": "

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 693
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 701
    :cond_5
    iget-object v0, p0, Laf;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    iget-object v0, p0, Laf;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 702
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mAvailBackStackIndices: "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 703
    iget-object v0, p0, Laf;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 705
    :cond_6
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 707
    iget-object v0, p0, Laf;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    .line 708
    iget-object v0, p0, Laf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 709
    if-lez v2, :cond_7

    .line 710
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Pending Actions:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 711
    :goto_5
    if-ge v1, v2, :cond_7

    .line 712
    iget-object v0, p0, Laf;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 713
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v3, "  #"

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(I)V

    .line 714
    const-string v3, ": "

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 711
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 705
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 719
    :cond_7
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "FragmentManager misc state:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 720
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mActivity="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Laf;->o:Ly;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 721
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mContainer="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Laf;->p:Lad;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 722
    iget-object v0, p0, Laf;->q:Lt;

    if-eqz v0, :cond_8

    .line 723
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mParent="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Laf;->q:Lt;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 725
    :cond_8
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mCurState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Laf;->n:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 726
    const-string v0, " mStateSaved="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Laf;->s:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 727
    const-string v0, " mDestroyed="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Laf;->t:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 728
    iget-boolean v0, p0, Laf;->r:Z

    if-eqz v0, :cond_9

    .line 729
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mNeedMenuInvalidate="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 730
    iget-boolean v0, p0, Laf;->r:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 732
    :cond_9
    iget-object v0, p0, Laf;->u:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 733
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mNoTransactionsBecause="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 734
    iget-object v0, p0, Laf;->u:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 736
    :cond_a
    iget-object v0, p0, Laf;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_b

    iget-object v0, p0, Laf;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_b

    .line 737
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mAvailIndices: "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 738
    iget-object v0, p0, Laf;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 740
    :cond_b
    return-void
.end method

.method public a(Lt;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 831
    iget-boolean v0, p1, Lt;->T:Z

    if-eqz v0, :cond_0

    .line 832
    iget-boolean v0, p0, Laf;->e:Z

    if-eqz v0, :cond_1

    .line 834
    const/4 v0, 0x1

    iput-boolean v0, p0, Laf;->v:Z

    .line 840
    :cond_0
    :goto_0
    return-void

    .line 837
    :cond_1
    iput-boolean v3, p1, Lt;->T:Z

    .line 838
    iget v2, p0, Laf;->n:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Laf;->a(Lt;IIIZ)V

    goto :goto_0
.end method

.method public a(Lt;II)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 1207
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "remove: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " nesting="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p1, Lt;->A:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1208
    :cond_0
    iget v0, p1, Lt;->A:I

    if-lez v0, :cond_5

    move v0, v1

    :goto_0
    if-nez v0, :cond_6

    move v0, v1

    .line 1209
    :goto_1
    iget-boolean v2, p1, Lt;->J:Z

    if-eqz v2, :cond_1

    if-eqz v0, :cond_4

    .line 1210
    :cond_1
    iget-object v2, p0, Laf;->g:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    .line 1211
    iget-object v2, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1213
    :cond_2
    iget-boolean v2, p1, Lt;->M:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p1, Lt;->N:Z

    if-eqz v2, :cond_3

    .line 1214
    iput-boolean v1, p0, Laf;->r:Z

    .line 1216
    :cond_3
    iput-boolean v5, p1, Lt;->u:Z

    .line 1217
    iput-boolean v1, p1, Lt;->v:Z

    .line 1218
    if-eqz v0, :cond_7

    move v2, v5

    :goto_2
    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Laf;->a(Lt;IIIZ)V

    .line 1221
    :cond_4
    return-void

    :cond_5
    move v0, v5

    .line 1208
    goto :goto_0

    :cond_6
    move v0, v5

    goto :goto_1

    :cond_7
    move v2, v1

    .line 1218
    goto :goto_2
.end method

.method a(Lt;IIIZ)V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 845
    iget-boolean v0, p1, Lt;->u:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lt;->J:Z

    if-eqz v0, :cond_1

    :cond_0
    if-le p2, v5, :cond_1

    move p2, v5

    .line 848
    :cond_1
    iget-boolean v0, p1, Lt;->v:Z

    if-eqz v0, :cond_2

    iget v0, p1, Lt;->j:I

    if-le p2, v0, :cond_2

    .line 850
    iget p2, p1, Lt;->j:I

    .line 854
    :cond_2
    iget-boolean v0, p1, Lt;->T:Z

    if-eqz v0, :cond_3

    iget v0, p1, Lt;->j:I

    if-ge v0, v9, :cond_3

    if-le p2, v6, :cond_3

    move p2, v6

    .line 857
    :cond_3
    iget v0, p1, Lt;->j:I

    if-ge v0, p2, :cond_1e

    .line 861
    iget-boolean v0, p1, Lt;->x:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p1, Lt;->y:Z

    if-nez v0, :cond_4

    .line 1096
    :goto_0
    return-void

    .line 864
    :cond_4
    iget-object v0, p1, Lt;->k:Landroid/view/View;

    if-eqz v0, :cond_5

    .line 869
    iput-object v7, p1, Lt;->k:Landroid/view/View;

    .line 870
    iget v2, p1, Lt;->l:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Laf;->a(Lt;IIIZ)V

    .line 872
    :cond_5
    iget v0, p1, Lt;->j:I

    packed-switch v0, :pswitch_data_0

    .line 1095
    :cond_6
    :goto_1
    iput p2, p1, Lt;->j:I

    goto :goto_0

    .line 874
    :pswitch_0
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "moveto CREATED: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 875
    :cond_7
    iget-object v0, p1, Lt;->m:Landroid/os/Bundle;

    if-eqz v0, :cond_9

    .line 876
    iget-object v0, p1, Lt;->m:Landroid/os/Bundle;

    const-string v1, "android:view_state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v0

    iput-object v0, p1, Lt;->n:Landroid/util/SparseArray;

    .line 878
    iget-object v0, p1, Lt;->m:Landroid/os/Bundle;

    const-string v1, "android:target_state"

    invoke-direct {p0, v0, v1}, Laf;->a(Landroid/os/Bundle;Ljava/lang/String;)Lt;

    move-result-object v0

    iput-object v0, p1, Lt;->r:Lt;

    .line 880
    iget-object v0, p1, Lt;->r:Lt;

    if-eqz v0, :cond_8

    .line 881
    iget-object v0, p1, Lt;->m:Landroid/os/Bundle;

    const-string v1, "android:target_req_state"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p1, Lt;->t:I

    .line 884
    :cond_8
    iget-object v0, p1, Lt;->m:Landroid/os/Bundle;

    const-string v1, "android:user_visible_hint"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p1, Lt;->U:Z

    .line 886
    iget-boolean v0, p1, Lt;->U:Z

    if-nez v0, :cond_9

    .line 887
    iput-boolean v5, p1, Lt;->T:Z

    .line 888
    if-le p2, v6, :cond_9

    move p2, v6

    .line 893
    :cond_9
    iget-object v0, p0, Laf;->o:Ly;

    iput-object v0, p1, Lt;->C:Ly;

    .line 894
    iget-object v0, p0, Laf;->q:Lt;

    iput-object v0, p1, Lt;->E:Lt;

    .line 895
    iget-object v0, p0, Laf;->q:Lt;

    if-eqz v0, :cond_a

    iget-object v0, p0, Laf;->q:Lt;

    iget-object v0, v0, Lt;->D:Laf;

    :goto_2
    iput-object v0, p1, Lt;->B:Laf;

    .line 897
    iput-boolean v3, p1, Lt;->O:Z

    .line 898
    iget-object v0, p0, Laf;->o:Ly;

    invoke-virtual {p1, v0}, Lt;->onAttach(Landroid/app/Activity;)V

    .line 899
    iget-boolean v0, p1, Lt;->O:Z

    if-nez v0, :cond_b

    .line 900
    new-instance v0, Lct;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onAttach()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lct;-><init>(Ljava/lang/String;)V

    throw v0

    .line 895
    :cond_a
    iget-object v0, p0, Laf;->o:Ly;

    iget-object v0, v0, Ly;->b:Laf;

    goto :goto_2

    .line 903
    :cond_b
    iget-object v0, p1, Lt;->E:Lt;

    if-nez v0, :cond_c

    .line 904
    iget-object v0, p0, Laf;->o:Ly;

    invoke-virtual {v0, p1}, Ly;->a(Lt;)V

    .line 907
    :cond_c
    iget-boolean v0, p1, Lt;->L:Z

    if-nez v0, :cond_d

    .line 908
    iget-object v0, p1, Lt;->m:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Lt;->a(Landroid/os/Bundle;)V

    .line 910
    :cond_d
    iput-boolean v3, p1, Lt;->L:Z

    .line 911
    iget-boolean v0, p1, Lt;->x:Z

    if-eqz v0, :cond_f

    .line 915
    iget-object v0, p1, Lt;->m:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Lt;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p1, Lt;->m:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v7, v1}, Lt;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lt;->R:Landroid/view/View;

    .line 917
    iget-object v0, p1, Lt;->R:Landroid/view/View;

    if-eqz v0, :cond_17

    .line 918
    iget-object v0, p1, Lt;->R:Landroid/view/View;

    iput-object v0, p1, Lt;->S:Landroid/view/View;

    .line 919
    iget-object v0, p1, Lt;->R:Landroid/view/View;

    invoke-static {v0}, Lbc;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p1, Lt;->R:Landroid/view/View;

    .line 920
    iget-boolean v0, p1, Lt;->I:Z

    if-eqz v0, :cond_e

    iget-object v0, p1, Lt;->R:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 921
    :cond_e
    iget-object v0, p1, Lt;->R:Landroid/view/View;

    iget-object v1, p1, Lt;->m:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Lt;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 927
    :cond_f
    :goto_3
    :pswitch_1
    if-le p2, v5, :cond_1a

    .line 928
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_10

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "moveto ACTIVITY_CREATED: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 929
    :cond_10
    iget-boolean v0, p1, Lt;->x:Z

    if-nez v0, :cond_15

    .line 931
    iget v0, p1, Lt;->G:I

    if-eqz v0, :cond_33

    .line 932
    iget-object v0, p0, Laf;->p:Lad;

    iget v1, p1, Lt;->G:I

    invoke-interface {v0, v1}, Lad;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 933
    if-nez v0, :cond_11

    iget-boolean v1, p1, Lt;->z:Z

    if-nez v1, :cond_11

    .line 934
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "No view found for id 0x"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p1, Lt;->G:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lt;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget v8, p1, Lt;->G:I

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ") for fragment "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Laf;->a(Ljava/lang/RuntimeException;)V

    .line 941
    :cond_11
    :goto_4
    iput-object v0, p1, Lt;->Q:Landroid/view/ViewGroup;

    .line 942
    iget-object v1, p1, Lt;->m:Landroid/os/Bundle;

    invoke-virtual {p1, v1}, Lt;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p1, Lt;->m:Landroid/os/Bundle;

    invoke-virtual {p1, v1, v0, v2}, Lt;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p1, Lt;->R:Landroid/view/View;

    .line 944
    iget-object v1, p1, Lt;->R:Landroid/view/View;

    if-eqz v1, :cond_18

    .line 945
    iget-object v1, p1, Lt;->R:Landroid/view/View;

    iput-object v1, p1, Lt;->S:Landroid/view/View;

    .line 946
    iget-object v1, p1, Lt;->R:Landroid/view/View;

    invoke-static {v1}, Lbc;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p1, Lt;->R:Landroid/view/View;

    .line 947
    if-eqz v0, :cond_13

    .line 948
    invoke-direct {p0, p1, p3, v5, p4}, Laf;->a(Lt;IZI)Landroid/view/animation/Animation;

    move-result-object v1

    .line 950
    if-eqz v1, :cond_12

    .line 951
    iget-object v2, p1, Lt;->R:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 953
    :cond_12
    iget-object v1, p1, Lt;->R:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 955
    :cond_13
    iget-boolean v0, p1, Lt;->I:Z

    if-eqz v0, :cond_14

    iget-object v0, p1, Lt;->R:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 956
    :cond_14
    iget-object v0, p1, Lt;->R:Landroid/view/View;

    iget-object v1, p1, Lt;->m:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Lt;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 962
    :cond_15
    :goto_5
    iget-object v0, p1, Lt;->m:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Lt;->b(Landroid/os/Bundle;)V

    .line 963
    iget-object v0, p1, Lt;->R:Landroid/view/View;

    if-eqz v0, :cond_19

    .line 964
    iget-object v0, p1, Lt;->m:Landroid/os/Bundle;

    iget-object v1, p1, Lt;->n:Landroid/util/SparseArray;

    if-eqz v1, :cond_16

    iget-object v1, p1, Lt;->S:Landroid/view/View;

    iget-object v2, p1, Lt;->n:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    iput-object v7, p1, Lt;->n:Landroid/util/SparseArray;

    :cond_16
    iput-boolean v3, p1, Lt;->O:Z

    invoke-virtual {p1, v0}, Lt;->onViewStateRestored(Landroid/os/Bundle;)V

    iget-boolean v0, p1, Lt;->O:Z

    if-nez v0, :cond_19

    new-instance v0, Lct;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onViewStateRestored()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lct;-><init>(Ljava/lang/String;)V

    throw v0

    .line 923
    :cond_17
    iput-object v7, p1, Lt;->S:Landroid/view/View;

    goto/16 :goto_3

    .line 958
    :cond_18
    iput-object v7, p1, Lt;->S:Landroid/view/View;

    goto :goto_5

    .line 966
    :cond_19
    iput-object v7, p1, Lt;->m:Landroid/os/Bundle;

    .line 970
    :cond_1a
    :pswitch_2
    if-le p2, v6, :cond_1c

    .line 971
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_1b

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "moveto STARTED: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 972
    :cond_1b
    invoke-virtual {p1}, Lt;->i()V

    .line 975
    :cond_1c
    :pswitch_3
    if-le p2, v9, :cond_6

    .line 976
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_1d

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "moveto RESUMED: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 977
    :cond_1d
    iput-boolean v5, p1, Lt;->w:Z

    .line 978
    invoke-virtual {p1}, Lt;->i_()V

    .line 979
    iput-object v7, p1, Lt;->m:Landroid/os/Bundle;

    .line 980
    iput-object v7, p1, Lt;->n:Landroid/util/SparseArray;

    goto/16 :goto_1

    .line 983
    :cond_1e
    iget v0, p1, Lt;->j:I

    if-le v0, p2, :cond_6

    .line 984
    iget v0, p1, Lt;->j:I

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_1

    .line 1047
    :cond_1f
    :goto_6
    :pswitch_4
    if-gtz p2, :cond_6

    .line 1048
    iget-boolean v0, p0, Laf;->t:Z

    if-eqz v0, :cond_20

    .line 1049
    iget-object v0, p1, Lt;->k:Landroid/view/View;

    if-eqz v0, :cond_20

    .line 1056
    iget-object v0, p1, Lt;->k:Landroid/view/View;

    .line 1057
    iput-object v7, p1, Lt;->k:Landroid/view/View;

    .line 1058
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 1061
    :cond_20
    iget-object v0, p1, Lt;->k:Landroid/view/View;

    if-eqz v0, :cond_2b

    .line 1066
    iput p2, p1, Lt;->l:I

    move p2, v5

    .line 1067
    goto/16 :goto_1

    .line 986
    :pswitch_5
    const/4 v0, 0x5

    if-ge p2, v0, :cond_22

    .line 987
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_21

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "movefrom RESUMED: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 988
    :cond_21
    invoke-virtual {p1}, Lt;->l()V

    .line 989
    iput-boolean v3, p1, Lt;->w:Z

    .line 992
    :cond_22
    :pswitch_6
    if-ge p2, v9, :cond_24

    .line 993
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_23

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "movefrom STARTED: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 994
    :cond_23
    invoke-virtual {p1}, Lt;->m()V

    .line 997
    :cond_24
    :pswitch_7
    if-ge p2, v6, :cond_26

    .line 998
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_25

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "movefrom STOPPED: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 999
    :cond_25
    invoke-virtual {p1}, Lt;->n()V

    .line 1002
    :cond_26
    :pswitch_8
    const/4 v0, 0x2

    if-ge p2, v0, :cond_1f

    .line 1003
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_27

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "movefrom ACTIVITY_CREATED: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1004
    :cond_27
    iget-object v0, p1, Lt;->R:Landroid/view/View;

    if-eqz v0, :cond_28

    .line 1007
    iget-object v0, p0, Laf;->o:Ly;

    invoke-virtual {v0}, Ly;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_28

    iget-object v0, p1, Lt;->n:Landroid/util/SparseArray;

    if-nez v0, :cond_28

    .line 1008
    invoke-direct {p0, p1}, Laf;->c(Lt;)V

    .line 1011
    :cond_28
    invoke-virtual {p1}, Lt;->o()V

    .line 1012
    iget-object v0, p1, Lt;->R:Landroid/view/View;

    if-eqz v0, :cond_2a

    iget-object v0, p1, Lt;->Q:Landroid/view/ViewGroup;

    if-eqz v0, :cond_2a

    .line 1014
    iget v0, p0, Laf;->n:I

    if-lez v0, :cond_32

    iget-boolean v0, p0, Laf;->t:Z

    if-nez v0, :cond_32

    .line 1015
    invoke-direct {p0, p1, p3, v3, p4}, Laf;->a(Lt;IZI)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1018
    :goto_7
    if-eqz v0, :cond_29

    .line 1020
    iget-object v1, p1, Lt;->R:Landroid/view/View;

    iput-object v1, p1, Lt;->k:Landroid/view/View;

    .line 1021
    iput p2, p1, Lt;->l:I

    .line 1022
    new-instance v1, Lai;

    invoke-direct {v1, p0, p1}, Lai;-><init>(Laf;Lt;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1038
    iget-object v1, p1, Lt;->R:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1040
    :cond_29
    iget-object v0, p1, Lt;->Q:Landroid/view/ViewGroup;

    iget-object v1, p1, Lt;->R:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1042
    :cond_2a
    iput-object v7, p1, Lt;->Q:Landroid/view/ViewGroup;

    .line 1043
    iput-object v7, p1, Lt;->R:Landroid/view/View;

    .line 1044
    iput-object v7, p1, Lt;->S:Landroid/view/View;

    goto/16 :goto_6

    .line 1069
    :cond_2b
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_2c

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "movefrom CREATED: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1070
    :cond_2c
    iget-boolean v0, p1, Lt;->L:Z

    if-nez v0, :cond_2d

    .line 1071
    invoke-virtual {p1}, Lt;->p()V

    .line 1074
    :cond_2d
    iput-boolean v3, p1, Lt;->O:Z

    .line 1075
    invoke-virtual {p1}, Lt;->onDetach()V

    .line 1076
    iget-boolean v0, p1, Lt;->O:Z

    if-nez v0, :cond_2e

    .line 1077
    new-instance v0, Lct;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDetach()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lct;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1080
    :cond_2e
    if-nez p5, :cond_6

    .line 1081
    iget-boolean v0, p1, Lt;->L:Z

    if-nez v0, :cond_31

    .line 1082
    iget v0, p1, Lt;->o:I

    if-ltz v0, :cond_6

    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_2f

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Freeing fragment index "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2f
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    iget v1, p1, Lt;->o:I

    invoke-virtual {v0, v1, v7}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Laf;->h:Ljava/util/ArrayList;

    if-nez v0, :cond_30

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laf;->h:Ljava/util/ArrayList;

    :cond_30
    iget-object v0, p0, Laf;->h:Ljava/util/ArrayList;

    iget v1, p1, Lt;->o:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Laf;->o:Ly;

    iget-object v1, p1, Lt;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ly;->a(Ljava/lang/String;)V

    invoke-virtual {p1}, Lt;->g_()V

    goto/16 :goto_1

    .line 1084
    :cond_31
    iput-object v7, p1, Lt;->C:Ly;

    .line 1085
    iput-object v7, p1, Lt;->E:Lt;

    .line 1086
    iput-object v7, p1, Lt;->B:Laf;

    .line 1087
    iput-object v7, p1, Lt;->D:Laf;

    goto/16 :goto_1

    :cond_32
    move-object v0, v7

    goto/16 :goto_7

    :cond_33
    move-object v0, v7

    goto/16 :goto_4

    .line 872
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 984
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method public a(Lt;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1185
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1186
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    .line 1188
    :cond_0
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "add: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1189
    :cond_1
    iget v0, p1, Lt;->o:I

    if-gez v0, :cond_4

    iget-object v0, p0, Laf;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Laf;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_5

    :cond_2
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    :cond_3
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Laf;->q:Lt;

    invoke-virtual {p1, v0, v1}, Lt;->a(ILt;)V

    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Allocated fragment index "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1190
    :cond_4
    iget-boolean v0, p1, Lt;->J:Z

    if-nez v0, :cond_8

    .line 1191
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1192
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment already added: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1189
    :cond_5
    iget-object v0, p0, Laf;->h:Ljava/util/ArrayList;

    iget-object v1, p0, Laf;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Laf;->q:Lt;

    invoke-virtual {p1, v0, v1}, Lt;->a(ILt;)V

    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    iget v1, p1, Lt;->o:I

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1194
    :cond_6
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1195
    iput-boolean v2, p1, Lt;->u:Z

    .line 1196
    const/4 v0, 0x0

    iput-boolean v0, p1, Lt;->v:Z

    .line 1197
    iget-boolean v0, p1, Lt;->M:Z

    if-eqz v0, :cond_7

    iget-boolean v0, p1, Lt;->N:Z

    if-eqz v0, :cond_7

    .line 1198
    iput-boolean v2, p0, Laf;->r:Z

    .line 1200
    :cond_7
    if-eqz p2, :cond_8

    .line 1201
    invoke-virtual {p0, p1}, Laf;->b(Lt;)V

    .line 1204
    :cond_8
    return-void
.end method

.method public a(Ly;Lad;Lt;)V
    .locals 2

    .prologue
    .line 1884
    iget-object v0, p0, Laf;->o:Ly;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already attached"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1885
    :cond_0
    iput-object p1, p0, Laf;->o:Ly;

    .line 1886
    iput-object p2, p0, Laf;->p:Lad;

    .line 1887
    iput-object p3, p0, Laf;->q:Lt;

    .line 1888
    return-void
.end method

.method a(II)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1524
    iget-object v0, p0, Laf;->i:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1583
    :cond_0
    :goto_0
    return v3

    .line 1527
    :cond_1
    if-gez p1, :cond_3

    and-int/lit8 v0, p2, 0x1

    if-nez v0, :cond_3

    .line 1528
    iget-object v0, p0, Laf;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 1529
    if-ltz v0, :cond_0

    .line 1532
    iget-object v1, p0, Laf;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo;

    .line 1533
    invoke-virtual {v0, v2}, Lo;->a(Z)V

    .line 1581
    :cond_2
    invoke-direct {p0}, Laf;->v()V

    move v3, v2

    .line 1583
    goto :goto_0

    .line 1536
    :cond_3
    const/4 v0, -0x1

    .line 1537
    if-ltz p1, :cond_7

    .line 1540
    iget-object v0, p0, Laf;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    .line 1541
    :goto_1
    if-ltz v1, :cond_5

    .line 1542
    iget-object v0, p0, Laf;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo;

    .line 1543
    if-ltz p1, :cond_4

    iget v0, v0, Lo;->o:I

    if-eq p1, v0, :cond_5

    .line 1547
    :cond_4
    add-int/lit8 v1, v1, -0x1

    .line 1550
    goto :goto_1

    .line 1551
    :cond_5
    if-ltz v1, :cond_0

    .line 1554
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_6

    .line 1555
    add-int/lit8 v1, v1, -0x1

    .line 1557
    :goto_2
    if-ltz v1, :cond_6

    .line 1558
    iget-object v0, p0, Laf;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo;

    .line 1559
    if-ltz p1, :cond_6

    iget v0, v0, Lo;->o:I

    if-ne p1, v0, :cond_6

    .line 1561
    add-int/lit8 v1, v1, -0x1

    .line 1562
    goto :goto_2

    :cond_6
    move v0, v1

    .line 1568
    :cond_7
    iget-object v1, p0, Laf;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1571
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1573
    iget-object v1, p0, Laf;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_3
    if-le v1, v0, :cond_8

    .line 1574
    iget-object v4, p0, Laf;->i:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1573
    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    .line 1576
    :cond_8
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v6, v0, -0x1

    move v4, v3

    .line 1577
    :goto_4
    if-gt v4, v6, :cond_2

    .line 1578
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Popping back stack state: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1579
    :cond_9
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo;

    if-ne v4, v6, :cond_a

    move v1, v2

    :goto_5
    invoke-virtual {v0, v1}, Lo;->a(Z)V

    .line 1577
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_4

    :cond_a
    move v1, v3

    .line 1579
    goto :goto_5
.end method

.method public a(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2000
    iget-object v1, p0, Laf;->g:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    move v1, v0

    move v2, v0

    .line 2001
    :goto_0
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2002
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 2003
    if-eqz v0, :cond_0

    .line 2004
    invoke-virtual {v0, p1}, Lt;->a(Landroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2005
    const/4 v2, 0x1

    .line 2001
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v2, v0

    .line 2010
    :cond_2
    return v2
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1968
    const/4 v1, 0x0

    .line 1969
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    move v3, v4

    move v2, v4

    .line 1970
    :goto_0
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 1971
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 1972
    if-eqz v0, :cond_1

    .line 1973
    invoke-virtual {v0, p1, p2}, Lt;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1974
    const/4 v2, 0x1

    .line 1975
    if-nez v1, :cond_0

    .line 1976
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1978
    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    move v0, v2

    .line 1970
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    :cond_2
    move v2, v4

    .line 1984
    :cond_3
    iget-object v0, p0, Laf;->j:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    .line 1985
    :goto_1
    iget-object v0, p0, Laf;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_6

    .line 1986
    iget-object v0, p0, Laf;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 1987
    if-eqz v1, :cond_4

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 1988
    :cond_4
    invoke-virtual {v0}, Lt;->onDestroyOptionsMenu()V

    .line 1985
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1993
    :cond_6
    iput-object v1, p0, Laf;->j:Ljava/util/ArrayList;

    .line 1995
    return v2
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2014
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    move v1, v2

    .line 2015
    :goto_0
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2016
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 2017
    if-eqz v0, :cond_1

    .line 2018
    invoke-virtual {v0, p1}, Lt;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2019
    const/4 v2, 0x1

    .line 2024
    :cond_0
    return v2

    .line 2015
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public b(I)V
    .locals 3

    .prologue
    .line 519
    if-gez p1, :cond_0

    .line 520
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 522
    :cond_0
    new-instance v0, Lah;

    invoke-direct {v0, p0, p1}, Lah;-><init>(Laf;I)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Laf;->a(Ljava/lang/Runnable;Z)V

    .line 527
    return-void
.end method

.method public b(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 2042
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 2043
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2044
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 2045
    if-eqz v0, :cond_0

    .line 2046
    invoke-virtual {v0, p1}, Lt;->b(Landroid/view/Menu;)V

    .line 2043
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2050
    :cond_1
    return-void
.end method

.method b(Lo;)V
    .locals 1

    .prologue
    .line 1516
    iget-object v0, p0, Laf;->i:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1517
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laf;->i:Ljava/util/ArrayList;

    .line 1519
    :cond_0
    iget-object v0, p0, Laf;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1520
    invoke-direct {p0}, Laf;->v()V

    .line 1521
    return-void
.end method

.method b(Lt;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1099
    iget v2, p0, Laf;->n:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Laf;->a(Lt;IIIZ)V

    .line 1100
    return-void
.end method

.method public b(Lt;II)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1224
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "hide: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1225
    :cond_0
    iget-boolean v0, p1, Lt;->I:Z

    if-nez v0, :cond_4

    .line 1226
    iput-boolean v2, p1, Lt;->I:Z

    .line 1227
    iget-object v0, p1, Lt;->R:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 1228
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Laf;->a(Lt;IZI)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1230
    if-eqz v0, :cond_1

    .line 1231
    iget-object v1, p1, Lt;->R:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1233
    :cond_1
    iget-object v0, p1, Lt;->R:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1235
    :cond_2
    iget-boolean v0, p1, Lt;->u:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Lt;->M:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Lt;->N:Z

    if-eqz v0, :cond_3

    .line 1236
    iput-boolean v2, p0, Laf;->r:Z

    .line 1238
    :cond_3
    invoke-virtual {p1, v2}, Lt;->onHiddenChanged(Z)V

    .line 1240
    :cond_4
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 482
    invoke-virtual {p0}, Laf;->h()Z

    move-result v0

    return v0
.end method

.method public b(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2028
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    move v1, v2

    .line 2029
    :goto_0
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2030
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 2031
    if-eqz v0, :cond_1

    .line 2032
    invoke-virtual {v0, p1}, Lt;->b(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2033
    const/4 v2, 0x1

    .line 2038
    :cond_0
    return v2

    .line 2029
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 593
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    return-object v0
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 1441
    monitor-enter p0

    .line 1442
    :try_start_0
    iget-object v0, p0, Laf;->k:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1443
    iget-object v0, p0, Laf;->l:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1444
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laf;->l:Ljava/util/ArrayList;

    .line 1446
    :cond_0
    sget-boolean v0, Laf;->a:Z

    .line 1447
    iget-object v0, p0, Laf;->l:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1448
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(Lt;II)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1243
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "show: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1244
    :cond_0
    iget-boolean v0, p1, Lt;->I:Z

    if-eqz v0, :cond_4

    .line 1245
    iput-boolean v2, p1, Lt;->I:Z

    .line 1246
    iget-object v0, p1, Lt;->R:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 1247
    invoke-direct {p0, p1, p2, v3, p3}, Laf;->a(Lt;IZI)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1249
    if-eqz v0, :cond_1

    .line 1250
    iget-object v1, p1, Lt;->R:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1252
    :cond_1
    iget-object v0, p1, Lt;->R:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1254
    :cond_2
    iget-boolean v0, p1, Lt;->u:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Lt;->M:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Lt;->N:Z

    if-eqz v0, :cond_3

    .line 1255
    iput-boolean v3, p0, Laf;->r:Z

    .line 1257
    :cond_3
    invoke-virtual {p1, v2}, Lt;->onHiddenChanged(Z)V

    .line 1259
    :cond_4
    return-void
.end method

.method public d(Lt;II)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    .line 1262
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "detach: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1263
    :cond_0
    iget-boolean v0, p1, Lt;->J:Z

    if-nez v0, :cond_4

    .line 1264
    iput-boolean v2, p1, Lt;->J:Z

    .line 1265
    iget-boolean v0, p1, Lt;->u:Z

    if-eqz v0, :cond_4

    .line 1267
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 1268
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "remove from detach: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1269
    :cond_1
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1271
    :cond_2
    iget-boolean v0, p1, Lt;->M:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Lt;->N:Z

    if-eqz v0, :cond_3

    .line 1272
    iput-boolean v2, p0, Laf;->r:Z

    .line 1274
    :cond_3
    iput-boolean v5, p1, Lt;->u:Z

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    .line 1275
    invoke-virtual/range {v0 .. v5}, Laf;->a(Lt;IIIZ)V

    .line 1278
    :cond_4
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 611
    iget-boolean v0, p0, Laf;->t:Z

    return v0
.end method

.method public e(Lt;II)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 1281
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "attach: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1282
    :cond_0
    iget-boolean v0, p1, Lt;->J:Z

    if-eqz v0, :cond_5

    .line 1283
    iput-boolean v5, p1, Lt;->J:Z

    .line 1284
    iget-boolean v0, p1, Lt;->u:Z

    if-nez v0, :cond_5

    .line 1285
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1286
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    .line 1288
    :cond_1
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1289
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment already added: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1291
    :cond_2
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "add from attach: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1292
    :cond_3
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1293
    iput-boolean v2, p1, Lt;->u:Z

    .line 1294
    iget-boolean v0, p1, Lt;->M:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p1, Lt;->N:Z

    if-eqz v0, :cond_4

    .line 1295
    iput-boolean v2, p0, Laf;->r:Z

    .line 1297
    :cond_4
    iget v2, p0, Laf;->n:I

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Laf;->a(Lt;IIIZ)V

    .line 1300
    :cond_5
    return-void
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 496
    invoke-direct {p0}, Laf;->u()V

    .line 497
    invoke-virtual {p0}, Laf;->b()Z

    .line 498
    iget-object v0, p0, Laf;->o:Ly;

    iget-object v0, v0, Ly;->a:Landroid/os/Handler;

    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Laf;->a(II)Z

    move-result v0

    return v0
.end method

.method g()V
    .locals 2

    .prologue
    .line 1140
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1148
    :cond_0
    return-void

    .line 1142
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1143
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 1144
    if-eqz v0, :cond_2

    .line 1145
    invoke-virtual {p0, v0}, Laf;->a(Lt;)V

    .line 1142
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public h()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1455
    iget-boolean v1, p0, Laf;->e:Z

    if-eqz v1, :cond_0

    .line 1456
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Recursive entry to executePendingTransactions"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1459
    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    iget-object v3, p0, Laf;->o:Ly;

    iget-object v3, v3, Ly;->a:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v1, v3, :cond_1

    .line 1460
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must be called from main thread of process"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v1, v2

    .line 1468
    :goto_0
    monitor-enter p0

    .line 1469
    :try_start_0
    iget-object v3, p0, Laf;->c:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    iget-object v3, p0, Laf;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_4

    .line 1470
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1491
    iget-boolean v0, p0, Laf;->v:Z

    if-eqz v0, :cond_9

    move v3, v2

    move v4, v2

    .line 1493
    :goto_1
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_8

    .line 1494
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 1495
    if-eqz v0, :cond_3

    iget-object v5, v0, Lt;->V:Lax;

    if-eqz v5, :cond_3

    .line 1496
    iget-object v0, v0, Lt;->V:Lax;

    invoke-virtual {v0}, Lax;->a()Z

    move-result v0

    or-int/2addr v4, v0

    .line 1493
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 1473
    :cond_4
    :try_start_1
    iget-object v1, p0, Laf;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1474
    iget-object v1, p0, Laf;->d:[Ljava/lang/Runnable;

    if-eqz v1, :cond_5

    iget-object v1, p0, Laf;->d:[Ljava/lang/Runnable;

    array-length v1, v1

    if-ge v1, v3, :cond_6

    .line 1475
    :cond_5
    new-array v1, v3, [Ljava/lang/Runnable;

    iput-object v1, p0, Laf;->d:[Ljava/lang/Runnable;

    .line 1477
    :cond_6
    iget-object v1, p0, Laf;->c:Ljava/util/ArrayList;

    iget-object v4, p0, Laf;->d:[Ljava/lang/Runnable;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1478
    iget-object v1, p0, Laf;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1479
    iget-object v1, p0, Laf;->o:Ly;

    iget-object v1, v1, Ly;->a:Landroid/os/Handler;

    iget-object v4, p0, Laf;->y:Ljava/lang/Runnable;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1480
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1482
    iput-boolean v0, p0, Laf;->e:Z

    move v1, v2

    .line 1483
    :goto_2
    if-ge v1, v3, :cond_7

    .line 1484
    iget-object v4, p0, Laf;->d:[Ljava/lang/Runnable;

    aget-object v4, v4, v1

    invoke-interface {v4}, Ljava/lang/Runnable;->run()V

    .line 1485
    iget-object v4, p0, Laf;->d:[Ljava/lang/Runnable;

    const/4 v5, 0x0

    aput-object v5, v4, v1

    .line 1483
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1480
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1487
    :cond_7
    iput-boolean v2, p0, Laf;->e:Z

    move v1, v0

    .line 1489
    goto :goto_0

    .line 1499
    :cond_8
    if-nez v4, :cond_9

    .line 1500
    iput-boolean v2, p0, Laf;->v:Z

    .line 1501
    invoke-virtual {p0}, Laf;->g()V

    .line 1504
    :cond_9
    return v1
.end method

.method i()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1587
    const/4 v1, 0x0

    .line 1588
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 1589
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 1590
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 1591
    if-eqz v0, :cond_1

    iget-boolean v2, v0, Lt;->K:Z

    if-eqz v2, :cond_1

    .line 1592
    if-nez v1, :cond_0

    .line 1593
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1595
    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1596
    const/4 v2, 0x1

    iput-boolean v2, v0, Lt;->L:Z

    .line 1597
    iget-object v2, v0, Lt;->r:Lt;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lt;->r:Lt;

    iget v2, v2, Lt;->o:I

    :goto_1
    iput v2, v0, Lt;->s:I

    .line 1598
    sget-boolean v2, Laf;->a:Z

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "retainNonConfig: keeping retained "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1589
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1597
    :cond_2
    const/4 v2, -0x1

    goto :goto_1

    .line 1602
    :cond_3
    return-object v1
.end method

.method j()Landroid/os/Parcelable;
    .locals 14

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 1657
    invoke-virtual {p0}, Laf;->h()Z

    .line 1659
    sget-boolean v0, Laf;->b:Z

    if-eqz v0, :cond_0

    .line 1669
    iput-boolean v3, p0, Laf;->s:Z

    .line 1672
    :cond_0
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    .line 1767
    :cond_1
    :goto_0
    return-object v2

    .line 1677
    :cond_2
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 1678
    new-array v7, v6, [Lam;

    move v5, v4

    move v1, v4

    .line 1680
    :goto_1
    if-ge v5, v6, :cond_10

    .line 1681
    iget-object v0, p0, Laf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 1682
    if-eqz v0, :cond_19

    .line 1683
    iget v1, v0, Lt;->o:I

    if-gez v1, :cond_3

    .line 1684
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Failure saving state: active "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " has cleared index: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v0, Lt;->o:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v1, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Laf;->a(Ljava/lang/RuntimeException;)V

    .line 1691
    :cond_3
    new-instance v8, Lam;

    invoke-direct {v8, v0}, Lam;-><init>(Lt;)V

    .line 1692
    aput-object v8, v7, v5

    .line 1694
    iget v1, v0, Lt;->j:I

    if-lez v1, :cond_f

    iget-object v1, v8, Lam;->j:Landroid/os/Bundle;

    if-nez v1, :cond_f

    .line 1695
    iget-object v1, p0, Laf;->w:Landroid/os/Bundle;

    if-nez v1, :cond_4

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, p0, Laf;->w:Landroid/os/Bundle;

    :cond_4
    iget-object v1, p0, Laf;->w:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lt;->c(Landroid/os/Bundle;)V

    iget-object v1, p0, Laf;->w:Landroid/os/Bundle;

    invoke-virtual {v1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_18

    iget-object v1, p0, Laf;->w:Landroid/os/Bundle;

    iput-object v2, p0, Laf;->w:Landroid/os/Bundle;

    :goto_2
    iget-object v9, v0, Lt;->R:Landroid/view/View;

    if-eqz v9, :cond_5

    invoke-direct {p0, v0}, Laf;->c(Lt;)V

    :cond_5
    iget-object v9, v0, Lt;->n:Landroid/util/SparseArray;

    if-eqz v9, :cond_7

    if-nez v1, :cond_6

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    :cond_6
    const-string v9, "android:view_state"

    iget-object v10, v0, Lt;->n:Landroid/util/SparseArray;

    invoke-virtual {v1, v9, v10}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    :cond_7
    iget-boolean v9, v0, Lt;->U:Z

    if-nez v9, :cond_9

    if-nez v1, :cond_8

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    :cond_8
    const-string v9, "android:user_visible_hint"

    iget-boolean v10, v0, Lt;->U:Z

    invoke-virtual {v1, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_9
    iput-object v1, v8, Lam;->j:Landroid/os/Bundle;

    .line 1697
    iget-object v1, v0, Lt;->r:Lt;

    if-eqz v1, :cond_d

    .line 1698
    iget-object v1, v0, Lt;->r:Lt;

    iget v1, v1, Lt;->o:I

    if-gez v1, :cond_a

    .line 1699
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Failure saving state: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " has target not in fragment manager: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v0, Lt;->r:Lt;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v1, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Laf;->a(Ljava/lang/RuntimeException;)V

    .line 1703
    :cond_a
    iget-object v1, v8, Lam;->j:Landroid/os/Bundle;

    if-nez v1, :cond_b

    .line 1704
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, v8, Lam;->j:Landroid/os/Bundle;

    .line 1706
    :cond_b
    iget-object v1, v8, Lam;->j:Landroid/os/Bundle;

    const-string v9, "android:target_state"

    iget-object v10, v0, Lt;->r:Lt;

    iget v11, v10, Lt;->o:I

    if-gez v11, :cond_c

    new-instance v11, Ljava/lang/IllegalStateException;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Fragment "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " is not currently in the FragmentManager"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v11}, Laf;->a(Ljava/lang/RuntimeException;)V

    :cond_c
    iget v10, v10, Lt;->o:I

    invoke-virtual {v1, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1708
    iget v1, v0, Lt;->t:I

    if-eqz v1, :cond_d

    .line 1709
    iget-object v1, v8, Lam;->j:Landroid/os/Bundle;

    const-string v9, "android:target_req_state"

    iget v10, v0, Lt;->t:I

    invoke-virtual {v1, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1719
    :cond_d
    :goto_3
    sget-boolean v1, Laf;->a:Z

    if-eqz v1, :cond_e

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v9, "Saved state of "

    invoke-direct {v1, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v8, Lam;->j:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_e
    move v0, v3

    .line 1680
    :goto_4
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v1, v0

    goto/16 :goto_1

    .line 1716
    :cond_f
    iget-object v1, v0, Lt;->m:Landroid/os/Bundle;

    iput-object v1, v8, Lam;->j:Landroid/os/Bundle;

    goto :goto_3

    .line 1724
    :cond_10
    if-nez v1, :cond_11

    .line 1725
    sget-boolean v0, Laf;->a:Z

    goto/16 :goto_0

    .line 1733
    :cond_11
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_14

    .line 1734
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1735
    if-lez v5, :cond_14

    .line 1736
    new-array v1, v5, [I

    move v3, v4

    .line 1737
    :goto_5
    if-ge v3, v5, :cond_15

    .line 1738
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    iget v0, v0, Lt;->o:I

    aput v0, v1, v3

    .line 1739
    aget v0, v1, v3

    if-gez v0, :cond_12

    .line 1740
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "Failure saving state: active "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " has cleared index: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v8, v1, v3

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Laf;->a(Ljava/lang/RuntimeException;)V

    .line 1744
    :cond_12
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_13

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "saveAllState: adding fragment #"

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ": "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1737
    :cond_13
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    :cond_14
    move-object v1, v2

    .line 1751
    :cond_15
    iget-object v0, p0, Laf;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_17

    .line 1752
    iget-object v0, p0, Laf;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1753
    if-lez v5, :cond_17

    .line 1754
    new-array v2, v5, [Lq;

    move v3, v4

    .line 1755
    :goto_6
    if-ge v3, v5, :cond_17

    .line 1756
    new-instance v4, Lq;

    iget-object v0, p0, Laf;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo;

    invoke-direct {v4, v0}, Lq;-><init>(Lo;)V

    aput-object v4, v2, v3

    .line 1757
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_16

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "saveAllState: adding back stack #"

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ": "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Laf;->i:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1755
    :cond_16
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_6

    .line 1763
    :cond_17
    new-instance v0, Laj;

    invoke-direct {v0}, Laj;-><init>()V

    .line 1764
    iput-object v7, v0, Laj;->a:[Lam;

    .line 1765
    iput-object v1, v0, Laj;->b:[I

    .line 1766
    iput-object v2, v0, Laj;->c:[Lq;

    move-object v2, v0

    .line 1767
    goto/16 :goto_0

    :cond_18
    move-object v1, v2

    goto/16 :goto_2

    :cond_19
    move v0, v1

    goto/16 :goto_4
.end method

.method public k()V
    .locals 1

    .prologue
    .line 1895
    const/4 v0, 0x0

    iput-boolean v0, p0, Laf;->s:Z

    .line 1896
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Laf;->e(I)V

    .line 1897
    return-void
.end method

.method public l()V
    .locals 1

    .prologue
    .line 1900
    const/4 v0, 0x0

    iput-boolean v0, p0, Laf;->s:Z

    .line 1901
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Laf;->e(I)V

    .line 1902
    return-void
.end method

.method public m()V
    .locals 1

    .prologue
    .line 1905
    const/4 v0, 0x0

    iput-boolean v0, p0, Laf;->s:Z

    .line 1906
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Laf;->e(I)V

    .line 1907
    return-void
.end method

.method public n()V
    .locals 1

    .prologue
    .line 1910
    const/4 v0, 0x0

    iput-boolean v0, p0, Laf;->s:Z

    .line 1911
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Laf;->e(I)V

    .line 1912
    return-void
.end method

.method public noteStateNotSaved()V
    .locals 1

    .prologue
    .line 1891
    const/4 v0, 0x0

    iput-boolean v0, p0, Laf;->s:Z

    .line 1892
    return-void
.end method

.method public o()V
    .locals 1

    .prologue
    .line 1915
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Laf;->e(I)V

    .line 1916
    return-void
.end method

.method public p()V
    .locals 1

    .prologue
    .line 1922
    const/4 v0, 0x1

    iput-boolean v0, p0, Laf;->s:Z

    .line 1924
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Laf;->e(I)V

    .line 1925
    return-void
.end method

.method public q()V
    .locals 1

    .prologue
    .line 1928
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Laf;->e(I)V

    .line 1929
    return-void
.end method

.method public r()V
    .locals 1

    .prologue
    .line 1932
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Laf;->e(I)V

    .line 1933
    return-void
.end method

.method public s()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1936
    const/4 v0, 0x1

    iput-boolean v0, p0, Laf;->t:Z

    .line 1937
    invoke-virtual {p0}, Laf;->h()Z

    .line 1938
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Laf;->e(I)V

    .line 1939
    iput-object v1, p0, Laf;->o:Ly;

    .line 1940
    iput-object v1, p0, Laf;->p:Lad;

    .line 1941
    iput-object v1, p0, Laf;->q:Lt;

    .line 1942
    return-void
.end method

.method public t()V
    .locals 2

    .prologue
    .line 1956
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 1957
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1958
    iget-object v0, p0, Laf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 1959
    if-eqz v0, :cond_0

    .line 1960
    invoke-virtual {v0}, Lt;->k()V

    .line 1957
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1964
    :cond_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 616
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 617
    const-string v1, "FragmentManager{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 618
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 619
    const-string v1, " in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 620
    iget-object v1, p0, Laf;->q:Lt;

    if-eqz v1, :cond_0

    .line 621
    iget-object v1, p0, Laf;->q:Lt;

    invoke-static {v1, v0}, Lf;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 625
    :goto_0
    const-string v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 626
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 623
    :cond_0
    iget-object v1, p0, Laf;->o:Ly;

    invoke-static {v1, v0}, Lf;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    goto :goto_0
.end method
