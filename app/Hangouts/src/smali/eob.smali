.class public final Leob;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leob;


# instance fields
.field public b:Lemz;

.field public c:Leok;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 437
    const/4 v0, 0x0

    new-array v0, v0, [Leob;

    sput-object v0, Leob;->a:[Leob;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 438
    invoke-direct {p0}, Lepn;-><init>()V

    .line 441
    iput-object v0, p0, Leob;->b:Lemz;

    .line 444
    iput-object v0, p0, Leob;->c:Leok;

    .line 438
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 461
    const/4 v0, 0x0

    .line 462
    iget-object v1, p0, Leob;->b:Lemz;

    if-eqz v1, :cond_0

    .line 463
    const/4 v0, 0x1

    iget-object v1, p0, Leob;->b:Lemz;

    .line 464
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 466
    :cond_0
    iget-object v1, p0, Leob;->c:Leok;

    if-eqz v1, :cond_1

    .line 467
    const/4 v1, 0x2

    iget-object v2, p0, Leob;->c:Leok;

    .line 468
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 470
    :cond_1
    iget-object v1, p0, Leob;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 471
    iput v0, p0, Leob;->cachedSize:I

    .line 472
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 434
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leob;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leob;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leob;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Leob;->b:Lemz;

    if-nez v0, :cond_2

    new-instance v0, Lemz;

    invoke-direct {v0}, Lemz;-><init>()V

    iput-object v0, p0, Leob;->b:Lemz;

    :cond_2
    iget-object v0, p0, Leob;->b:Lemz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Leob;->c:Leok;

    if-nez v0, :cond_3

    new-instance v0, Leok;

    invoke-direct {v0}, Leok;-><init>()V

    iput-object v0, p0, Leob;->c:Leok;

    :cond_3
    iget-object v0, p0, Leob;->c:Leok;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 449
    iget-object v0, p0, Leob;->b:Lemz;

    if-eqz v0, :cond_0

    .line 450
    const/4 v0, 0x1

    iget-object v1, p0, Leob;->b:Lemz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 452
    :cond_0
    iget-object v0, p0, Leob;->c:Leok;

    if-eqz v0, :cond_1

    .line 453
    const/4 v0, 0x2

    iget-object v1, p0, Leob;->c:Leok;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 455
    :cond_1
    iget-object v0, p0, Leob;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 457
    return-void
.end method
