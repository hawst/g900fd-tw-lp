.class public final Ldeo;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldeo;


# instance fields
.field public b:[Ldgh;

.field public c:[Ldgf;

.field public d:[Ldge;

.field public e:[Ldgg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6746
    const/4 v0, 0x0

    new-array v0, v0, [Ldeo;

    sput-object v0, Ldeo;->a:[Ldeo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6747
    invoke-direct {p0}, Lepn;-><init>()V

    .line 6750
    sget-object v0, Ldgh;->a:[Ldgh;

    iput-object v0, p0, Ldeo;->b:[Ldgh;

    .line 6753
    sget-object v0, Ldgf;->a:[Ldgf;

    iput-object v0, p0, Ldeo;->c:[Ldgf;

    .line 6756
    sget-object v0, Ldge;->a:[Ldge;

    iput-object v0, p0, Ldeo;->d:[Ldge;

    .line 6759
    sget-object v0, Ldgg;->a:[Ldgg;

    iput-object v0, p0, Ldeo;->e:[Ldgg;

    .line 6747
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 6799
    iget-object v0, p0, Ldeo;->b:[Ldgh;

    if-eqz v0, :cond_1

    .line 6800
    iget-object v3, p0, Ldeo;->b:[Ldgh;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 6801
    if-eqz v5, :cond_0

    .line 6802
    const/4 v6, 0x1

    .line 6803
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 6800
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 6807
    :cond_2
    iget-object v2, p0, Ldeo;->c:[Ldgf;

    if-eqz v2, :cond_4

    .line 6808
    iget-object v3, p0, Ldeo;->c:[Ldgf;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 6809
    if-eqz v5, :cond_3

    .line 6810
    const/4 v6, 0x2

    .line 6811
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 6808
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 6815
    :cond_4
    iget-object v2, p0, Ldeo;->d:[Ldge;

    if-eqz v2, :cond_6

    .line 6816
    iget-object v3, p0, Ldeo;->d:[Ldge;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    .line 6817
    if-eqz v5, :cond_5

    .line 6818
    const/4 v6, 0x3

    .line 6819
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 6816
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 6823
    :cond_6
    iget-object v2, p0, Ldeo;->e:[Ldgg;

    if-eqz v2, :cond_8

    .line 6824
    iget-object v2, p0, Ldeo;->e:[Ldgg;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 6825
    if-eqz v4, :cond_7

    .line 6826
    const/4 v5, 0x4

    .line 6827
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 6824
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 6831
    :cond_8
    iget-object v1, p0, Ldeo;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6832
    iput v0, p0, Ldeo;->cachedSize:I

    .line 6833
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6743
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldeo;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldeo;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldeo;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldeo;->b:[Ldgh;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldgh;

    iget-object v3, p0, Ldeo;->b:[Ldgh;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldeo;->b:[Ldgh;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldeo;->b:[Ldgh;

    :goto_2
    iget-object v2, p0, Ldeo;->b:[Ldgh;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldeo;->b:[Ldgh;

    new-instance v3, Ldgh;

    invoke-direct {v3}, Ldgh;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldeo;->b:[Ldgh;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldeo;->b:[Ldgh;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldeo;->b:[Ldgh;

    new-instance v3, Ldgh;

    invoke-direct {v3}, Ldgh;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldeo;->b:[Ldgh;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldeo;->c:[Ldgf;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldgf;

    iget-object v3, p0, Ldeo;->c:[Ldgf;

    if-eqz v3, :cond_5

    iget-object v3, p0, Ldeo;->c:[Ldgf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Ldeo;->c:[Ldgf;

    :goto_4
    iget-object v2, p0, Ldeo;->c:[Ldgf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Ldeo;->c:[Ldgf;

    new-instance v3, Ldgf;

    invoke-direct {v3}, Ldgf;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldeo;->c:[Ldgf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Ldeo;->c:[Ldgf;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Ldeo;->c:[Ldgf;

    new-instance v3, Ldgf;

    invoke-direct {v3}, Ldgf;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldeo;->c:[Ldgf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldeo;->d:[Ldge;

    if-nez v0, :cond_9

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Ldge;

    iget-object v3, p0, Ldeo;->d:[Ldge;

    if-eqz v3, :cond_8

    iget-object v3, p0, Ldeo;->d:[Ldge;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    iput-object v2, p0, Ldeo;->d:[Ldge;

    :goto_6
    iget-object v2, p0, Ldeo;->d:[Ldge;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Ldeo;->d:[Ldge;

    new-instance v3, Ldge;

    invoke-direct {v3}, Ldge;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldeo;->d:[Ldge;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    iget-object v0, p0, Ldeo;->d:[Ldge;

    array-length v0, v0

    goto :goto_5

    :cond_a
    iget-object v2, p0, Ldeo;->d:[Ldge;

    new-instance v3, Ldge;

    invoke-direct {v3}, Ldge;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldeo;->d:[Ldge;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldeo;->e:[Ldgg;

    if-nez v0, :cond_c

    move v0, v1

    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Ldgg;

    iget-object v3, p0, Ldeo;->e:[Ldgg;

    if-eqz v3, :cond_b

    iget-object v3, p0, Ldeo;->e:[Ldgg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    iput-object v2, p0, Ldeo;->e:[Ldgg;

    :goto_8
    iget-object v2, p0, Ldeo;->e:[Ldgg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    iget-object v2, p0, Ldeo;->e:[Ldgg;

    new-instance v3, Ldgg;

    invoke-direct {v3}, Ldgg;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldeo;->e:[Ldgg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_c
    iget-object v0, p0, Ldeo;->e:[Ldgg;

    array-length v0, v0

    goto :goto_7

    :cond_d
    iget-object v2, p0, Ldeo;->e:[Ldgg;

    new-instance v3, Ldgg;

    invoke-direct {v3}, Ldgg;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldeo;->e:[Ldgg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 6764
    iget-object v1, p0, Ldeo;->b:[Ldgh;

    if-eqz v1, :cond_1

    .line 6765
    iget-object v2, p0, Ldeo;->b:[Ldgh;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 6766
    if-eqz v4, :cond_0

    .line 6767
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 6765
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 6771
    :cond_1
    iget-object v1, p0, Ldeo;->c:[Ldgf;

    if-eqz v1, :cond_3

    .line 6772
    iget-object v2, p0, Ldeo;->c:[Ldgf;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 6773
    if-eqz v4, :cond_2

    .line 6774
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 6772
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6778
    :cond_3
    iget-object v1, p0, Ldeo;->d:[Ldge;

    if-eqz v1, :cond_5

    .line 6779
    iget-object v2, p0, Ldeo;->d:[Ldge;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 6780
    if-eqz v4, :cond_4

    .line 6781
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 6779
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 6785
    :cond_5
    iget-object v1, p0, Ldeo;->e:[Ldgg;

    if-eqz v1, :cond_7

    .line 6786
    iget-object v1, p0, Ldeo;->e:[Ldgg;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 6787
    if-eqz v3, :cond_6

    .line 6788
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 6786
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 6792
    :cond_7
    iget-object v0, p0, Ldeo;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 6794
    return-void
.end method
