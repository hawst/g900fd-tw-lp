.class public final Laof;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Z

.field private final b:Landroid/content/Context;

.field private final c:Laok;

.field private d:Landroid/media/ToneGenerator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v1, Laok;

    invoke-direct {v1}, Laok;-><init>()V

    iput-object v1, p0, Laof;->c:Laok;

    .line 40
    iput-object p1, p0, Laof;->b:Landroid/content/Context;

    .line 42
    iget-object v1, p0, Laof;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "dtmf_tone"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Laof;->a:Z

    .line 47
    :try_start_0
    new-instance v0, Landroid/media/ToneGenerator;

    const/16 v1, 0x8

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Landroid/media/ToneGenerator;-><init>(II)V

    iput-object v0, p0, Laof;->d:Landroid/media/ToneGenerator;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    :goto_1
    :try_start_1
    iget-object v0, p0, Laof;->c:Laok;

    iget-object v1, p0, Laof;->b:Landroid/content/Context;

    iget-object v2, p0, Laof;->b:Landroid/content/Context;

    .line 57
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lf;->bH:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    .line 56
    invoke-virtual {v0, v1, v2}, Laok;->a(Landroid/content/Context;Z)V
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 63
    :goto_2
    iget-object v0, p0, Laof;->c:Laok;

    invoke-virtual {v0}, Laok;->a()V

    .line 64
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 48
    :catch_0
    move-exception v0

    .line 49
    const-string v1, "Babel_dialer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception caught while creating local tone generator: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Laof;->d:Landroid/media/ToneGenerator;

    goto :goto_1

    .line 58
    :catch_1
    move-exception v0

    .line 59
    const-string v1, "Babel_dialer"

    const-string v2, "Vibrate control bool missing."

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Laof;->d:Landroid/media/ToneGenerator;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Laof;->d:Landroid/media/ToneGenerator;

    invoke-virtual {v0}, Landroid/media/ToneGenerator;->release()V

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Laof;->d:Landroid/media/ToneGenerator;

    .line 74
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 82
    iget-boolean v0, p0, Laof;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Laof;->d:Landroid/media/ToneGenerator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laof;->d:Landroid/media/ToneGenerator;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, Landroid/media/ToneGenerator;->startTone(II)Z

    .line 83
    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Laof;->a:Z

    if-nez v0, :cond_1

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    iget-object v0, p0, Laof;->d:Landroid/media/ToneGenerator;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Laof;->d:Landroid/media/ToneGenerator;

    invoke-virtual {v0}, Landroid/media/ToneGenerator;->stopTone()V

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Laof;->c:Laok;

    invoke-virtual {v0}, Laok;->b()V

    .line 110
    return-void
.end method
