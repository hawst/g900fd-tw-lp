.class public final Lbcp;
.super Lbep;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private final l:J

.field private m:Ljava/lang/String;

.field private final n:J

.field private o:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Lbep;-><init>()V

    .line 33
    iput-object p1, p0, Lbcp;->a:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lbcp;->b:Ljava/lang/String;

    .line 35
    iput-object p3, p0, Lbcp;->c:Ljava/lang/String;

    .line 36
    iput-object p4, p0, Lbcp;->h:Ljava/lang/String;

    .line 37
    iput-object p5, p0, Lbcp;->i:Ljava/lang/String;

    .line 38
    iput-object p6, p0, Lbcp;->j:Ljava/lang/String;

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lbcp;->m:Ljava/lang/String;

    .line 40
    iput-wide p8, p0, Lbcp;->l:J

    .line 41
    iput-wide p10, p0, Lbcp;->n:J

    .line 42
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbcp;->o:J

    .line 43
    iput-object p7, p0, Lbcp;->k:Ljava/lang/String;

    .line 44
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(J)V
    .locals 0

    .prologue
    .line 110
    iput-wide p1, p0, Lbcp;->o:J

    .line 111
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lbcp;->m:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    const-string v0, "photo_queue"

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x1

    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lbcp;->a:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lbcp;->h:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lbcp;->i:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lbcp;->j:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lbcp;->m:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lbcp;->k:Ljava/lang/String;

    return-object v0
.end method

.method public l()J
    .locals 2

    .prologue
    .line 94
    iget-wide v0, p0, Lbcp;->l:J

    return-wide v0
.end method

.method public m()J
    .locals 2

    .prologue
    .line 98
    iget-wide v0, p0, Lbcp;->n:J

    return-wide v0
.end method

.method public n()J
    .locals 2

    .prologue
    .line 102
    iget-wide v0, p0, Lbcp;->o:J

    return-wide v0
.end method
