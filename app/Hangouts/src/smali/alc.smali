.class public final Lalc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)V
    .locals 0

    .prologue
    .line 720
    iput-object p1, p0, Lalc;->a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 723
    iget-object v0, p0, Lalc;->a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mInviterCircleIds:Ljava/util/HashSet;

    if-nez v0, :cond_0

    .line 739
    :goto_0
    return-void

    .line 727
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 729
    iget-object v0, p0, Lalc;->a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mInviterCircleIds:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 731
    iget-object v1, p0, Lalc;->a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    iget-object v1, v1, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mMyCircles:Ljava/util/Map;

    .line 732
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    .line 733
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 735
    :cond_1
    iget-object v0, p0, Lalc;->a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-static {}, Lf;->z()Lchg;

    move-result-object v1

    iget-object v3, p0, Lalc;->a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    .line 736
    invoke-static {v3}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->f(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Lyj;

    move-result-object v3

    invoke-virtual {v3}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Lchg;->c(Ljava/lang/String;)Lchg;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "g:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lalc;->a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    .line 737
    invoke-static {v4}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->a(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Lbdk;

    move-result-object v4

    iget-object v4, v4, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Lchg;->a(Ljava/lang/String;)Lchg;

    move-result-object v1

    .line 738
    invoke-interface {v1, v2}, Lchg;->a(Ljava/util/List;)Lchg;

    move-result-object v1

    invoke-interface {v1}, Lchg;->a()Landroid/content/Intent;

    move-result-object v1

    const/16 v2, 0x65

    .line 735
    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
