.class public Lbhg;
.super Lbgj;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method private constructor <init>(Ldub;)V
    .locals 7

    .prologue
    .line 3471
    iget-object v1, p1, Ldub;->b:Ldvn;

    iget-object v0, p1, Ldub;->c:Ldrr;

    iget-object v0, v0, Ldrr;->d:Ljava/lang/Long;

    .line 3472
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v2

    iget-object v0, p1, Ldub;->c:Ldrr;

    iget-object v0, v0, Ldrr;->o:Ljava/lang/Long;

    .line 3473
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v4

    iget-object v0, p1, Ldub;->c:Ldrr;

    iget-object v6, v0, Ldrr;->e:Ljava/lang/String;

    move-object v0, p0

    .line 3471
    invoke-direct/range {v0 .. v6}, Lbgj;-><init>(Ldvn;JJLjava/lang/String;)V

    .line 3475
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 3476
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ModifyOtrStatusResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3478
    :cond_0
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 3482
    new-instance v0, Ldub;

    invoke-direct {v0}, Ldub;-><init>()V

    .line 3483
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldub;

    .line 3484
    iget-object v1, v0, Ldub;->b:Ldvn;

    invoke-static {v1}, Lbhg;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3485
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldub;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 3487
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbhg;

    invoke-direct {v1, v0}, Lbhg;-><init>(Ldub;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic a(Lyt;Lbnl;)V
    .locals 0

    .prologue
    .line 3467
    invoke-super {p0, p1, p2}, Lbgj;->a(Lyt;Lbnl;)V

    return-void
.end method
