.class public final Lejn;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lejn;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Lejt;

.field public d:Lejl;

.field public e:Lejc;

.field public f:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 964
    const/4 v0, 0x0

    new-array v0, v0, [Lejn;

    sput-object v0, Lejn;->a:[Lejn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 965
    invoke-direct {p0}, Lepn;-><init>()V

    .line 989
    iput-object v0, p0, Lejn;->b:Ljava/lang/Integer;

    .line 992
    iput-object v0, p0, Lejn;->c:Lejt;

    .line 995
    iput-object v0, p0, Lejn;->d:Lejl;

    .line 998
    iput-object v0, p0, Lejn;->e:Lejc;

    .line 965
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1026
    const/4 v0, 0x0

    .line 1027
    iget-object v1, p0, Lejn;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1028
    const/4 v0, 0x1

    iget-object v1, p0, Lejn;->b:Ljava/lang/Integer;

    .line 1029
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1031
    :cond_0
    iget-object v1, p0, Lejn;->c:Lejt;

    if-eqz v1, :cond_1

    .line 1032
    const/4 v1, 0x2

    iget-object v2, p0, Lejn;->c:Lejt;

    .line 1033
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1035
    :cond_1
    iget-object v1, p0, Lejn;->d:Lejl;

    if-eqz v1, :cond_2

    .line 1036
    const/4 v1, 0x3

    iget-object v2, p0, Lejn;->d:Lejl;

    .line 1037
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1039
    :cond_2
    iget-object v1, p0, Lejn;->e:Lejc;

    if-eqz v1, :cond_3

    .line 1040
    const/4 v1, 0x4

    iget-object v2, p0, Lejn;->e:Lejc;

    .line 1041
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1043
    :cond_3
    iget-object v1, p0, Lejn;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 1044
    const/4 v1, 0x5

    iget-object v2, p0, Lejn;->f:Ljava/lang/Boolean;

    .line 1045
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1047
    :cond_4
    iget-object v1, p0, Lejn;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1048
    iput v0, p0, Lejn;->cachedSize:I

    .line 1049
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 961
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lejn;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lejn;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lejn;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lejn;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lejn;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lejn;->c:Lejt;

    if-nez v0, :cond_4

    new-instance v0, Lejt;

    invoke-direct {v0}, Lejt;-><init>()V

    iput-object v0, p0, Lejn;->c:Lejt;

    :cond_4
    iget-object v0, p0, Lejn;->c:Lejt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lejn;->d:Lejl;

    if-nez v0, :cond_5

    new-instance v0, Lejl;

    invoke-direct {v0}, Lejl;-><init>()V

    iput-object v0, p0, Lejn;->d:Lejl;

    :cond_5
    iget-object v0, p0, Lejn;->d:Lejl;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Lejn;->e:Lejc;

    if-nez v0, :cond_6

    new-instance v0, Lejc;

    invoke-direct {v0}, Lejc;-><init>()V

    iput-object v0, p0, Lejn;->e:Lejc;

    :cond_6
    iget-object v0, p0, Lejn;->e:Lejc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lejn;->f:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1005
    iget-object v0, p0, Lejn;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1006
    const/4 v0, 0x1

    iget-object v1, p0, Lejn;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1008
    :cond_0
    iget-object v0, p0, Lejn;->c:Lejt;

    if-eqz v0, :cond_1

    .line 1009
    const/4 v0, 0x2

    iget-object v1, p0, Lejn;->c:Lejt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1011
    :cond_1
    iget-object v0, p0, Lejn;->d:Lejl;

    if-eqz v0, :cond_2

    .line 1012
    const/4 v0, 0x3

    iget-object v1, p0, Lejn;->d:Lejl;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1014
    :cond_2
    iget-object v0, p0, Lejn;->e:Lejc;

    if-eqz v0, :cond_3

    .line 1015
    const/4 v0, 0x4

    iget-object v1, p0, Lejn;->e:Lejc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1017
    :cond_3
    iget-object v0, p0, Lejn;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 1018
    const/4 v0, 0x5

    iget-object v1, p0, Lejn;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 1020
    :cond_4
    iget-object v0, p0, Lejn;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1022
    return-void
.end method
