.class public final Lbbz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/net/Uri;

.field final synthetic b:Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 514
    iput-object p1, p0, Lbbz;->b:Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;

    iput-object p2, p0, Lbbz;->a:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 517
    new-instance v0, Lts;

    invoke-direct {v0}, Lts;-><init>()V

    .line 518
    new-instance v2, Luc;

    invoke-direct {v2}, Luc;-><init>()V

    .line 524
    :try_start_0
    iget-object v3, p0, Lbbz;->b:Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;

    iget-object v4, p0, Lbbz;->a:Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static {v3, v4, v5, v2, v6}, Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;->a(Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;Landroid/net/Uri;ILtu;Z)Z
    :try_end_0
    .catch Luk; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 539
    :goto_0
    if-nez v0, :cond_0

    .line 540
    iget-object v0, p0, Lbbz;->b:Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;->f(Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lbca;

    invoke-direct {v1, p0}, Lbca;-><init>(Lbbz;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 550
    :goto_1
    return-void

    :catch_0
    move-exception v3

    .line 528
    :try_start_1
    invoke-virtual {v2}, Luc;->c()I

    move-result v3

    .line 531
    iget-object v4, p0, Lbbz;->b:Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;

    iget-object v5, p0, Lbbz;->a:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-static {v4, v5, v3, v0, v6}, Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;->a(Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;Landroid/net/Uri;ILtu;Z)Z
    :try_end_1
    .catch Luk; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    goto :goto_0

    .line 533
    :catch_1
    move-exception v0

    .line 535
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Must not reach here. "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0

    .line 549
    :cond_0
    iget-object v0, p0, Lbbz;->b:Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;

    iget-object v1, p0, Lbbz;->a:Landroid/net/Uri;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;->a(Lcom/google/android/apps/hangouts/phone/ViewVCardActivity;Landroid/net/Uri;Luc;)V

    goto :goto_1
.end method
