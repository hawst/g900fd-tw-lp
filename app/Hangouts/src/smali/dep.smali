.class public final Ldep;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldep;


# instance fields
.field public b:Ldei;

.field public c:Ldei;

.field public d:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1728
    const/4 v0, 0x0

    new-array v0, v0, [Ldep;

    sput-object v0, Ldep;->a:[Ldep;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1729
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1732
    iput-object v0, p0, Ldep;->b:Ldei;

    .line 1735
    iput-object v0, p0, Ldep;->c:Ldei;

    .line 1729
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1757
    const/4 v0, 0x0

    .line 1758
    iget-object v1, p0, Ldep;->b:Ldei;

    if-eqz v1, :cond_0

    .line 1759
    const/4 v0, 0x1

    iget-object v1, p0, Ldep;->b:Ldei;

    .line 1760
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1762
    :cond_0
    iget-object v1, p0, Ldep;->c:Ldei;

    if-eqz v1, :cond_1

    .line 1763
    const/4 v1, 0x2

    iget-object v2, p0, Ldep;->c:Ldei;

    .line 1764
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1766
    :cond_1
    iget-object v1, p0, Ldep;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 1767
    const/4 v1, 0x3

    iget-object v2, p0, Ldep;->d:Ljava/lang/Boolean;

    .line 1768
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1770
    :cond_2
    iget-object v1, p0, Ldep;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1771
    iput v0, p0, Ldep;->cachedSize:I

    .line 1772
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1725
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldep;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldep;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldep;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldep;->b:Ldei;

    if-nez v0, :cond_2

    new-instance v0, Ldei;

    invoke-direct {v0}, Ldei;-><init>()V

    iput-object v0, p0, Ldep;->b:Ldei;

    :cond_2
    iget-object v0, p0, Ldep;->b:Ldei;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldep;->c:Ldei;

    if-nez v0, :cond_3

    new-instance v0, Ldei;

    invoke-direct {v0}, Ldei;-><init>()V

    iput-object v0, p0, Ldep;->c:Ldei;

    :cond_3
    iget-object v0, p0, Ldep;->c:Ldei;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldep;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1742
    iget-object v0, p0, Ldep;->b:Ldei;

    if-eqz v0, :cond_0

    .line 1743
    const/4 v0, 0x1

    iget-object v1, p0, Ldep;->b:Ldei;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1745
    :cond_0
    iget-object v0, p0, Ldep;->c:Ldei;

    if-eqz v0, :cond_1

    .line 1746
    const/4 v0, 0x2

    iget-object v1, p0, Ldep;->c:Ldei;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1748
    :cond_1
    iget-object v0, p0, Ldep;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1749
    const/4 v0, 0x3

    iget-object v1, p0, Ldep;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 1751
    :cond_2
    iget-object v0, p0, Ldep;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1753
    return-void
.end method
