.class public final Lauh;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Z


# instance fields
.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private final f:Ljava/nio/FloatBuffer;

.field private final g:Landroid/graphics/Rect;

.field private final h:Lcom/google/android/libraries/hangouts/video/FloatRect;

.field private final i:[F

.field private final j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lbys;->e:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lauh;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lauh;->g:Landroid/graphics/Rect;

    .line 37
    new-instance v0, Lcom/google/android/libraries/hangouts/video/FloatRect;

    invoke-direct {v0}, Lcom/google/android/libraries/hangouts/video/FloatRect;-><init>()V

    iput-object v0, p0, Lauh;->h:Lcom/google/android/libraries/hangouts/video/FloatRect;

    .line 39
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lauh;->i:[F

    .line 63
    iput-object p1, p0, Lauh;->j:Ljava/lang/String;

    .line 64
    iget-object v0, p0, Lauh;->i:[F

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 65
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lauh;->f:Ljava/nio/FloatBuffer;

    .line 66
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 69
    const-string v0, "attribute vec4 vPosition;\nvarying vec2 v_texCoord;\nvoid main() {\n  gl_Position = vPosition;\n}\n"

    const-string v1, "precision mediump float;\nvoid main() {\n  gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);\n}\n"

    invoke-static {v0, v1}, Lf;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lauh;->b:I

    .line 70
    iget v0, p0, Lauh;->b:I

    const-string v1, "vPosition"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lauh;->e:I

    .line 71
    return-void
.end method

.method public a(II)V
    .locals 0

    .prologue
    .line 74
    iput p1, p0, Lauh;->c:I

    .line 75
    iput p2, p0, Lauh;->d:I

    .line 76
    return-void
.end method

.method public a(Landroid/graphics/Rect;)V
    .locals 10

    .prologue
    const/4 v9, 0x6

    const/4 v8, 0x4

    const/4 v1, 0x2

    const/high16 v6, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 90
    iget v0, p0, Lauh;->c:I

    if-eqz v0, :cond_0

    iget v0, p0, Lauh;->d:I

    if-nez v0, :cond_1

    .line 91
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SolidRenderer not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 136
    :goto_0
    return-void

    .line 96
    :cond_2
    iget-object v0, p0, Lauh;->g:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 97
    iget-object v0, p0, Lauh;->g:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 99
    iget-object v0, p0, Lauh;->h:Lcom/google/android/libraries/hangouts/video/FloatRect;

    const/high16 v2, -0x40800000    # -1.0f

    iget-object v4, p0, Lauh;->g:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    iget v5, p0, Lauh;->c:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    mul-float/2addr v4, v6

    add-float/2addr v2, v4

    iput v2, v0, Lcom/google/android/libraries/hangouts/video/FloatRect;->left:F

    .line 101
    iget-object v0, p0, Lauh;->h:Lcom/google/android/libraries/hangouts/video/FloatRect;

    const/high16 v2, 0x3f800000    # 1.0f

    iget-object v4, p0, Lauh;->g:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    iget v5, p0, Lauh;->d:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    mul-float/2addr v4, v6

    sub-float/2addr v2, v4

    iput v2, v0, Lcom/google/android/libraries/hangouts/video/FloatRect;->top:F

    .line 103
    iget-object v0, p0, Lauh;->h:Lcom/google/android/libraries/hangouts/video/FloatRect;

    const/high16 v2, -0x40800000    # -1.0f

    iget-object v4, p0, Lauh;->g:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    iget v5, p0, Lauh;->c:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    mul-float/2addr v4, v6

    add-float/2addr v2, v4

    iput v2, v0, Lcom/google/android/libraries/hangouts/video/FloatRect;->right:F

    .line 105
    iget-object v0, p0, Lauh;->h:Lcom/google/android/libraries/hangouts/video/FloatRect;

    const/high16 v2, 0x3f800000    # 1.0f

    iget-object v4, p0, Lauh;->g:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    iget v5, p0, Lauh;->d:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    mul-float/2addr v4, v6

    sub-float/2addr v2, v4

    iput v2, v0, Lcom/google/android/libraries/hangouts/video/FloatRect;->bottom:F

    .line 107
    iget-object v0, p0, Lauh;->h:Lcom/google/android/libraries/hangouts/video/FloatRect;

    iget-object v2, p0, Lauh;->i:[F

    iget v4, v0, Lcom/google/android/libraries/hangouts/video/FloatRect;->left:F

    aput v4, v2, v3

    const/4 v4, 0x1

    iget v5, v0, Lcom/google/android/libraries/hangouts/video/FloatRect;->bottom:F

    aput v5, v2, v4

    iget v4, v0, Lcom/google/android/libraries/hangouts/video/FloatRect;->right:F

    aput v4, v2, v1

    const/4 v4, 0x3

    iget v5, v0, Lcom/google/android/libraries/hangouts/video/FloatRect;->bottom:F

    aput v5, v2, v4

    iget v4, v0, Lcom/google/android/libraries/hangouts/video/FloatRect;->right:F

    aput v4, v2, v8

    const/4 v4, 0x5

    iget v5, v0, Lcom/google/android/libraries/hangouts/video/FloatRect;->top:F

    aput v5, v2, v4

    iget v4, v0, Lcom/google/android/libraries/hangouts/video/FloatRect;->left:F

    aput v4, v2, v9

    const/4 v4, 0x7

    iget v0, v0, Lcom/google/android/libraries/hangouts/video/FloatRect;->top:F

    aput v0, v2, v4

    .line 109
    sget-boolean v0, Lauh;->a:Z

    if-eqz v0, :cond_3

    .line 110
    const-string v0, "Babel"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s Pixels=%d,%d:%d,%d View=%d:%d Vertices=%.2f,%.2f:%.2f,%.2f)"

    const/16 v5, 0xb

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lauh;->j:Ljava/lang/String;

    aput-object v6, v5, v3

    const/4 v6, 0x1

    iget v7, p1, Landroid/graphics/Rect;->left:I

    .line 112
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    iget v6, p1, Landroid/graphics/Rect;->top:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v6, 0x3

    iget v7, p1, Landroid/graphics/Rect;->right:I

    .line 113
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    const/4 v6, 0x5

    iget v7, p0, Lauh;->c:I

    .line 114
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    iget v6, p0, Lauh;->d:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    const/4 v6, 0x7

    iget-object v7, p0, Lauh;->h:Lcom/google/android/libraries/hangouts/video/FloatRect;

    iget v7, v7, Lcom/google/android/libraries/hangouts/video/FloatRect;->left:F

    .line 115
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x8

    iget-object v7, p0, Lauh;->h:Lcom/google/android/libraries/hangouts/video/FloatRect;

    iget v7, v7, Lcom/google/android/libraries/hangouts/video/FloatRect;->top:F

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x9

    iget-object v7, p0, Lauh;->h:Lcom/google/android/libraries/hangouts/video/FloatRect;

    iget v7, v7, Lcom/google/android/libraries/hangouts/video/FloatRect;->right:F

    .line 116
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0xa

    iget-object v7, p0, Lauh;->h:Lcom/google/android/libraries/hangouts/video/FloatRect;

    iget v7, v7, Lcom/google/android/libraries/hangouts/video/FloatRect;->bottom:F

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v5, v6

    .line 110
    invoke-static {v2, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_3
    iget-object v0, p0, Lauh;->f:Ljava/nio/FloatBuffer;

    iget-object v2, p0, Lauh;->i:[F

    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 121
    :cond_4
    iget v0, p0, Lauh;->b:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 122
    iget v0, p0, Lauh;->c:I

    iget v2, p0, Lauh;->d:I

    invoke-static {v3, v3, v0, v2}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 123
    const-string v0, "glViewport"

    invoke-static {v0}, Lf;->s(Ljava/lang/String;)V

    .line 125
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 128
    iget v0, p0, Lauh;->e:I

    const/16 v2, 0x1406

    iget-object v5, p0, Lauh;->f:Ljava/nio/FloatBuffer;

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 130
    iget v0, p0, Lauh;->e:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 131
    const-string v0, "setup"

    invoke-static {v0}, Lf;->s(Ljava/lang/String;)V

    .line 134
    invoke-static {v9, v3, v8}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 135
    const-string v0, "glDrawArrays"

    invoke-static {v0}, Lf;->s(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
