.class public final Lbck;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lbjc;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbjg;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Z

.field public final e:[B

.field public final f:J

.field public final g:J

.field public final h:J


# direct methods
.method private constructor <init>(Ldql;ZJ)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iget-object v3, p1, Ldql;->b:Ldqf;

    iget-object v3, v3, Ldqf;->b:Ljava/lang/String;

    iput-object v3, p0, Lbck;->a:Ljava/lang/String;

    .line 34
    iget-object v3, p1, Ldql;->d:Ldqa;

    if-eqz v3, :cond_0

    .line 35
    new-instance v3, Lbjc;

    iget-object v4, p1, Ldql;->d:Ldqa;

    invoke-direct {v3, v4, v0}, Lbjc;-><init>(Ldqa;B)V

    iput-object v3, p0, Lbck;->b:Lbjc;

    .line 39
    :goto_0
    iget-object v3, p1, Ldql;->c:Ljava/lang/Long;

    if-eqz v3, :cond_1

    .line 40
    iget-object v3, p1, Ldql;->c:Ljava/lang/Long;

    invoke-static {v3}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v3

    iput-wide v3, p0, Lbck;->g:J

    .line 44
    :goto_1
    iget-object v3, p1, Ldql;->e:[Ldrr;

    invoke-static {v3, p2, p3, p4}, Lbjg;->a([Ldrr;ZJ)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lbck;->c:Ljava/util/List;

    .line 46
    iget-object v3, p1, Ldql;->g:Ljava/lang/Boolean;

    invoke-static {v3, v0}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v3

    iput-boolean v3, p0, Lbck;->d:Z

    .line 49
    iget-object v3, p1, Ldql;->f:Ldrv;

    if-eqz v3, :cond_3

    .line 50
    iget-object v3, p1, Ldql;->f:Ldrv;

    .line 52
    iget-object v4, v3, Ldrv;->c:[B

    array-length v4, v4

    if-lez v4, :cond_2

    .line 53
    iget-object v4, v3, Ldrv;->c:[B

    iget-object v5, v3, Ldrv;->c:[B

    array-length v5, v5

    invoke-static {v4, v5}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v4

    iput-object v4, p0, Lbck;->e:[B

    .line 55
    iget-object v3, v3, Ldrv;->b:Ljava/lang/Long;

    invoke-static {v3}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v3

    iput-wide v3, p0, Lbck;->f:J

    .line 68
    :goto_2
    iget-object v3, p1, Ldql;->h:[Ldqs;

    array-length v3, v3

    .line 69
    :goto_3
    if-ge v0, v3, :cond_4

    .line 70
    iget-object v4, p1, Ldql;->h:[Ldqs;

    aget-object v4, v4, v0

    .line 71
    iget-object v4, v4, Ldqs;->d:Ljava/lang/Long;

    .line 72
    invoke-static {v4}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v4

    .line 71
    invoke-static {v1, v2, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    .line 69
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 37
    :cond_0
    iput-object v5, p0, Lbck;->b:Lbjc;

    goto :goto_0

    .line 42
    :cond_1
    iput-wide v1, p0, Lbck;->g:J

    goto :goto_1

    .line 58
    :cond_2
    iput-object v5, p0, Lbck;->e:[B

    .line 59
    iput-wide v1, p0, Lbck;->f:J

    goto :goto_2

    .line 62
    :cond_3
    iput-object v5, p0, Lbck;->e:[B

    .line 63
    iput-wide v1, p0, Lbck;->f:J

    goto :goto_2

    .line 74
    :cond_4
    iput-wide v1, p0, Lbck;->h:J

    .line 75
    return-void
.end method

.method public static a([BZJ)Lbck;
    .locals 2

    .prologue
    .line 79
    if-eqz p0, :cond_0

    .line 81
    :try_start_0
    new-instance v1, Lbck;

    new-instance v0, Ldql;

    invoke-direct {v0}, Ldql;-><init>()V

    .line 82
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldql;

    invoke-direct {v1, v0, p1, p2, p3}, Lbck;-><init>(Ldql;ZJ)V
    :try_end_0
    .catch Lepq; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 87
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/util/ArrayList;ZJ)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[B>;ZJ)",
            "Ljava/util/List",
            "<",
            "Lbck;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 94
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 95
    invoke-static {v0, p1, p2, p3}, Lbck;->a([BZJ)Lbck;

    move-result-object v0

    .line 96
    if-eqz v0, :cond_0

    .line 97
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 100
    :cond_1
    return-object v1
.end method
