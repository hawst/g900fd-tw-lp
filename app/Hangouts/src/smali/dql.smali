.class public final Ldql;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldql;


# instance fields
.field public b:Ldqf;

.field public c:Ljava/lang/Long;

.field public d:Ldqa;

.field public e:[Ldrr;

.field public f:Ldrv;

.field public g:Ljava/lang/Boolean;

.field public h:[Ldqs;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9346
    const/4 v0, 0x0

    new-array v0, v0, [Ldql;

    sput-object v0, Ldql;->a:[Ldql;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9347
    invoke-direct {p0}, Lepn;-><init>()V

    .line 9350
    iput-object v1, p0, Ldql;->b:Ldqf;

    .line 9355
    iput-object v1, p0, Ldql;->d:Ldqa;

    .line 9358
    sget-object v0, Ldrr;->a:[Ldrr;

    iput-object v0, p0, Ldql;->e:[Ldrr;

    .line 9361
    iput-object v1, p0, Ldql;->f:Ldrv;

    .line 9366
    sget-object v0, Ldqs;->a:[Ldqs;

    iput-object v0, p0, Ldql;->h:[Ldqs;

    .line 9347
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 9407
    iget-object v0, p0, Ldql;->b:Ldqf;

    if-eqz v0, :cond_8

    .line 9408
    const/4 v0, 0x1

    iget-object v2, p0, Ldql;->b:Ldqf;

    .line 9409
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9411
    :goto_0
    iget-object v2, p0, Ldql;->d:Ldqa;

    if-eqz v2, :cond_0

    .line 9412
    const/4 v2, 0x2

    iget-object v3, p0, Ldql;->d:Ldqa;

    .line 9413
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 9415
    :cond_0
    iget-object v2, p0, Ldql;->e:[Ldrr;

    if-eqz v2, :cond_2

    .line 9416
    iget-object v3, p0, Ldql;->e:[Ldrr;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 9417
    if-eqz v5, :cond_1

    .line 9418
    const/4 v6, 0x3

    .line 9419
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 9416
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 9423
    :cond_2
    iget-object v2, p0, Ldql;->g:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    .line 9424
    const/4 v2, 0x4

    iget-object v3, p0, Ldql;->g:Ljava/lang/Boolean;

    .line 9425
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 9427
    :cond_3
    iget-object v2, p0, Ldql;->f:Ldrv;

    if-eqz v2, :cond_4

    .line 9428
    const/4 v2, 0x5

    iget-object v3, p0, Ldql;->f:Ldrv;

    .line 9429
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 9431
    :cond_4
    iget-object v2, p0, Ldql;->c:Ljava/lang/Long;

    if-eqz v2, :cond_5

    .line 9432
    const/4 v2, 0x6

    iget-object v3, p0, Ldql;->c:Ljava/lang/Long;

    .line 9433
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 9435
    :cond_5
    iget-object v2, p0, Ldql;->h:[Ldqs;

    if-eqz v2, :cond_7

    .line 9436
    iget-object v2, p0, Ldql;->h:[Ldqs;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 9437
    if-eqz v4, :cond_6

    .line 9438
    const/4 v5, 0x7

    .line 9439
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 9436
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 9443
    :cond_7
    iget-object v1, p0, Ldql;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9444
    iput v0, p0, Ldql;->cachedSize:I

    .line 9445
    return v0

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 9343
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldql;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldql;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldql;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldql;->b:Ldqf;

    if-nez v0, :cond_2

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldql;->b:Ldqf;

    :cond_2
    iget-object v0, p0, Ldql;->b:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldql;->d:Ldqa;

    if-nez v0, :cond_3

    new-instance v0, Ldqa;

    invoke-direct {v0}, Ldqa;-><init>()V

    iput-object v0, p0, Ldql;->d:Ldqa;

    :cond_3
    iget-object v0, p0, Ldql;->d:Ldqa;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldql;->e:[Ldrr;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldrr;

    iget-object v3, p0, Ldql;->e:[Ldrr;

    if-eqz v3, :cond_4

    iget-object v3, p0, Ldql;->e:[Ldrr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Ldql;->e:[Ldrr;

    :goto_2
    iget-object v2, p0, Ldql;->e:[Ldrr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ldql;->e:[Ldrr;

    new-instance v3, Ldrr;

    invoke-direct {v3}, Ldrr;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldql;->e:[Ldrr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Ldql;->e:[Ldrr;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Ldql;->e:[Ldrr;

    new-instance v3, Ldrr;

    invoke-direct {v3}, Ldrr;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldql;->e:[Ldrr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldql;->g:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Ldql;->f:Ldrv;

    if-nez v0, :cond_7

    new-instance v0, Ldrv;

    invoke-direct {v0}, Ldrv;-><init>()V

    iput-object v0, p0, Ldql;->f:Ldrv;

    :cond_7
    iget-object v0, p0, Ldql;->f:Ldrv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldql;->c:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldql;->h:[Ldqs;

    if-nez v0, :cond_9

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldqs;

    iget-object v3, p0, Ldql;->h:[Ldqs;

    if-eqz v3, :cond_8

    iget-object v3, p0, Ldql;->h:[Ldqs;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    iput-object v2, p0, Ldql;->h:[Ldqs;

    :goto_4
    iget-object v2, p0, Ldql;->h:[Ldqs;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Ldql;->h:[Ldqs;

    new-instance v3, Ldqs;

    invoke-direct {v3}, Ldqs;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldql;->h:[Ldqs;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_9
    iget-object v0, p0, Ldql;->h:[Ldqs;

    array-length v0, v0

    goto :goto_3

    :cond_a
    iget-object v2, p0, Ldql;->h:[Ldqs;

    new-instance v3, Ldqs;

    invoke-direct {v3}, Ldqs;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldql;->h:[Ldqs;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 9371
    iget-object v1, p0, Ldql;->b:Ldqf;

    if-eqz v1, :cond_0

    .line 9372
    const/4 v1, 0x1

    iget-object v2, p0, Ldql;->b:Ldqf;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 9374
    :cond_0
    iget-object v1, p0, Ldql;->d:Ldqa;

    if-eqz v1, :cond_1

    .line 9375
    const/4 v1, 0x2

    iget-object v2, p0, Ldql;->d:Ldqa;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 9377
    :cond_1
    iget-object v1, p0, Ldql;->e:[Ldrr;

    if-eqz v1, :cond_3

    .line 9378
    iget-object v2, p0, Ldql;->e:[Ldrr;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 9379
    if-eqz v4, :cond_2

    .line 9380
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 9378
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 9384
    :cond_3
    iget-object v1, p0, Ldql;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 9385
    const/4 v1, 0x4

    iget-object v2, p0, Ldql;->g:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 9387
    :cond_4
    iget-object v1, p0, Ldql;->f:Ldrv;

    if-eqz v1, :cond_5

    .line 9388
    const/4 v1, 0x5

    iget-object v2, p0, Ldql;->f:Ldrv;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 9390
    :cond_5
    iget-object v1, p0, Ldql;->c:Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 9391
    const/4 v1, 0x6

    iget-object v2, p0, Ldql;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->a(IJ)V

    .line 9393
    :cond_6
    iget-object v1, p0, Ldql;->h:[Ldqs;

    if-eqz v1, :cond_8

    .line 9394
    iget-object v1, p0, Ldql;->h:[Ldqs;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    .line 9395
    if-eqz v3, :cond_7

    .line 9396
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 9394
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 9400
    :cond_8
    iget-object v0, p0, Ldql;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 9402
    return-void
.end method
