.class public final Ldzn;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldzn;


# instance fields
.field public b:Ldvn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 877
    const/4 v0, 0x0

    new-array v0, v0, [Ldzn;

    sput-object v0, Ldzn;->a:[Ldzn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 878
    invoke-direct {p0}, Lepn;-><init>()V

    .line 881
    const/4 v0, 0x0

    iput-object v0, p0, Ldzn;->b:Ldvn;

    .line 878
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 2

    .prologue
    .line 895
    const/4 v0, 0x0

    .line 896
    iget-object v1, p0, Ldzn;->b:Ldvn;

    if-eqz v1, :cond_0

    .line 897
    const/4 v0, 0x1

    iget-object v1, p0, Ldzn;->b:Ldvn;

    .line 898
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 900
    :cond_0
    iget-object v1, p0, Ldzn;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 901
    iput v0, p0, Ldzn;->cachedSize:I

    .line 902
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 874
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldzn;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldzn;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldzn;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldzn;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldzn;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldzn;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 886
    iget-object v0, p0, Ldzn;->b:Ldvn;

    if-eqz v0, :cond_0

    .line 887
    const/4 v0, 0x1

    iget-object v1, p0, Ldzn;->b:Ldvn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 889
    :cond_0
    iget-object v0, p0, Ldzn;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 891
    return-void
.end method
