.class public final Ldmg;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldmg;


# instance fields
.field public A:Ljava/lang/Integer;

.field public B:Ljava/lang/Integer;

.field public C:Ldlk;

.field public D:Ljava/lang/Integer;

.field public E:Ldmq;

.field public F:Ldmf;

.field public G:Ldmr;

.field public H:Ljava/lang/Boolean;

.field public I:Ldmj;

.field public J:Ldmi;

.field public K:Ljava/lang/Boolean;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/Integer;

.field public j:Ljava/lang/Integer;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:[Ldlm;

.field public n:Ljava/lang/Integer;

.field public o:Ldln;

.field public p:Ldkp;

.field public q:Ldlw;

.field public r:Ljava/lang/Boolean;

.field public s:Ljava/lang/Integer;

.field public t:[Ldmh;

.field public u:Ljava/lang/Long;

.field public v:Ljava/lang/Boolean;

.field public w:Ljava/lang/Integer;

.field public x:Ljava/lang/Integer;

.field public y:Ljava/lang/Integer;

.field public z:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5602
    const/4 v0, 0x0

    new-array v0, v0, [Ldmg;

    sput-object v0, Ldmg;->a:[Ldmg;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5603
    invoke-direct {p0}, Lepn;-><init>()V

    .line 5859
    iput-object v1, p0, Ldmg;->b:Ljava/lang/Integer;

    .line 5862
    iput-object v1, p0, Ldmg;->c:Ljava/lang/Integer;

    .line 5883
    sget-object v0, Ldlm;->a:[Ldlm;

    iput-object v0, p0, Ldmg;->m:[Ldlm;

    .line 5886
    iput-object v1, p0, Ldmg;->n:Ljava/lang/Integer;

    .line 5889
    iput-object v1, p0, Ldmg;->o:Ldln;

    .line 5892
    iput-object v1, p0, Ldmg;->p:Ldkp;

    .line 5895
    iput-object v1, p0, Ldmg;->q:Ldlw;

    .line 5900
    iput-object v1, p0, Ldmg;->s:Ljava/lang/Integer;

    .line 5903
    sget-object v0, Ldmh;->a:[Ldmh;

    iput-object v0, p0, Ldmg;->t:[Ldmh;

    .line 5910
    iput-object v1, p0, Ldmg;->w:Ljava/lang/Integer;

    .line 5919
    iput-object v1, p0, Ldmg;->A:Ljava/lang/Integer;

    .line 5922
    iput-object v1, p0, Ldmg;->B:Ljava/lang/Integer;

    .line 5925
    iput-object v1, p0, Ldmg;->C:Ldlk;

    .line 5930
    iput-object v1, p0, Ldmg;->E:Ldmq;

    .line 5933
    iput-object v1, p0, Ldmg;->F:Ldmf;

    .line 5936
    iput-object v1, p0, Ldmg;->G:Ldmr;

    .line 5941
    iput-object v1, p0, Ldmg;->I:Ldmj;

    .line 5944
    iput-object v1, p0, Ldmg;->J:Ldmi;

    .line 5603
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 6069
    const/4 v0, 0x1

    iget-object v2, p0, Ldmg;->b:Ljava/lang/Integer;

    .line 6071
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6072
    const/4 v2, 0x2

    iget-object v3, p0, Ldmg;->c:Ljava/lang/Integer;

    .line 6073
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 6074
    iget-object v2, p0, Ldmg;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 6075
    const/4 v2, 0x3

    iget-object v3, p0, Ldmg;->d:Ljava/lang/String;

    .line 6076
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6078
    :cond_0
    iget-object v2, p0, Ldmg;->e:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 6079
    const/4 v2, 0x4

    iget-object v3, p0, Ldmg;->e:Ljava/lang/String;

    .line 6080
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6082
    :cond_1
    iget-object v2, p0, Ldmg;->f:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 6083
    const/4 v2, 0x5

    iget-object v3, p0, Ldmg;->f:Ljava/lang/String;

    .line 6084
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6086
    :cond_2
    iget-object v2, p0, Ldmg;->i:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 6087
    const/4 v2, 0x6

    iget-object v3, p0, Ldmg;->i:Ljava/lang/Integer;

    .line 6088
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 6090
    :cond_3
    iget-object v2, p0, Ldmg;->k:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 6091
    const/4 v2, 0x7

    iget-object v3, p0, Ldmg;->k:Ljava/lang/String;

    .line 6092
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6094
    :cond_4
    iget-object v2, p0, Ldmg;->l:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 6095
    const/16 v2, 0x8

    iget-object v3, p0, Ldmg;->l:Ljava/lang/String;

    .line 6096
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6098
    :cond_5
    iget-object v2, p0, Ldmg;->m:[Ldlm;

    if-eqz v2, :cond_7

    .line 6099
    iget-object v3, p0, Ldmg;->m:[Ldlm;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 6100
    if-eqz v5, :cond_6

    .line 6101
    const/16 v6, 0x9

    .line 6102
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 6099
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 6106
    :cond_7
    iget-object v2, p0, Ldmg;->n:Ljava/lang/Integer;

    if-eqz v2, :cond_8

    .line 6107
    const/16 v2, 0xa

    iget-object v3, p0, Ldmg;->n:Ljava/lang/Integer;

    .line 6108
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 6110
    :cond_8
    iget-object v2, p0, Ldmg;->o:Ldln;

    if-eqz v2, :cond_9

    .line 6111
    const/16 v2, 0xb

    iget-object v3, p0, Ldmg;->o:Ldln;

    .line 6112
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6114
    :cond_9
    iget-object v2, p0, Ldmg;->p:Ldkp;

    if-eqz v2, :cond_a

    .line 6115
    const/16 v2, 0xc

    iget-object v3, p0, Ldmg;->p:Ldkp;

    .line 6116
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6118
    :cond_a
    iget-object v2, p0, Ldmg;->q:Ldlw;

    if-eqz v2, :cond_b

    .line 6119
    const/16 v2, 0xd

    iget-object v3, p0, Ldmg;->q:Ldlw;

    .line 6120
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6122
    :cond_b
    iget-object v2, p0, Ldmg;->r:Ljava/lang/Boolean;

    if-eqz v2, :cond_c

    .line 6123
    const/16 v2, 0xe

    iget-object v3, p0, Ldmg;->r:Ljava/lang/Boolean;

    .line 6124
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 6126
    :cond_c
    iget-object v2, p0, Ldmg;->s:Ljava/lang/Integer;

    if-eqz v2, :cond_d

    .line 6127
    const/16 v2, 0xf

    iget-object v3, p0, Ldmg;->s:Ljava/lang/Integer;

    .line 6128
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 6130
    :cond_d
    iget-object v2, p0, Ldmg;->t:[Ldmh;

    if-eqz v2, :cond_f

    .line 6131
    iget-object v2, p0, Ldmg;->t:[Ldmh;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_f

    aget-object v4, v2, v1

    .line 6132
    if-eqz v4, :cond_e

    .line 6133
    const/16 v5, 0x10

    .line 6134
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 6131
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6138
    :cond_f
    iget-object v1, p0, Ldmg;->u:Ljava/lang/Long;

    if-eqz v1, :cond_10

    .line 6139
    const/16 v1, 0x11

    iget-object v2, p0, Ldmg;->u:Ljava/lang/Long;

    .line 6140
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 6142
    :cond_10
    iget-object v1, p0, Ldmg;->v:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    .line 6143
    const/16 v1, 0x12

    iget-object v2, p0, Ldmg;->v:Ljava/lang/Boolean;

    .line 6144
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6146
    :cond_11
    iget-object v1, p0, Ldmg;->w:Ljava/lang/Integer;

    if-eqz v1, :cond_12

    .line 6147
    const/16 v1, 0x13

    iget-object v2, p0, Ldmg;->w:Ljava/lang/Integer;

    .line 6148
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6150
    :cond_12
    iget-object v1, p0, Ldmg;->x:Ljava/lang/Integer;

    if-eqz v1, :cond_13

    .line 6151
    const/16 v1, 0x14

    iget-object v2, p0, Ldmg;->x:Ljava/lang/Integer;

    .line 6152
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6154
    :cond_13
    iget-object v1, p0, Ldmg;->A:Ljava/lang/Integer;

    if-eqz v1, :cond_14

    .line 6155
    const/16 v1, 0x15

    iget-object v2, p0, Ldmg;->A:Ljava/lang/Integer;

    .line 6156
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6158
    :cond_14
    iget-object v1, p0, Ldmg;->B:Ljava/lang/Integer;

    if-eqz v1, :cond_15

    .line 6159
    const/16 v1, 0x16

    iget-object v2, p0, Ldmg;->B:Ljava/lang/Integer;

    .line 6160
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6162
    :cond_15
    iget-object v1, p0, Ldmg;->C:Ldlk;

    if-eqz v1, :cond_16

    .line 6163
    const/16 v1, 0x17

    iget-object v2, p0, Ldmg;->C:Ldlk;

    .line 6164
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6166
    :cond_16
    iget-object v1, p0, Ldmg;->D:Ljava/lang/Integer;

    if-eqz v1, :cond_17

    .line 6167
    const/16 v1, 0x18

    iget-object v2, p0, Ldmg;->D:Ljava/lang/Integer;

    .line 6168
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6170
    :cond_17
    iget-object v1, p0, Ldmg;->E:Ldmq;

    if-eqz v1, :cond_18

    .line 6171
    const/16 v1, 0x19

    iget-object v2, p0, Ldmg;->E:Ldmq;

    .line 6172
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6174
    :cond_18
    iget-object v1, p0, Ldmg;->F:Ldmf;

    if-eqz v1, :cond_19

    .line 6175
    const/16 v1, 0x1a

    iget-object v2, p0, Ldmg;->F:Ldmf;

    .line 6176
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6178
    :cond_19
    iget-object v1, p0, Ldmg;->G:Ldmr;

    if-eqz v1, :cond_1a

    .line 6179
    const/16 v1, 0x1b

    iget-object v2, p0, Ldmg;->G:Ldmr;

    .line 6180
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6182
    :cond_1a
    iget-object v1, p0, Ldmg;->g:Ljava/lang/String;

    if-eqz v1, :cond_1b

    .line 6183
    const/16 v1, 0x1c

    iget-object v2, p0, Ldmg;->g:Ljava/lang/String;

    .line 6184
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6186
    :cond_1b
    iget-object v1, p0, Ldmg;->z:Ljava/lang/Boolean;

    if-eqz v1, :cond_1c

    .line 6187
    const/16 v1, 0x1d

    iget-object v2, p0, Ldmg;->z:Ljava/lang/Boolean;

    .line 6188
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6190
    :cond_1c
    iget-object v1, p0, Ldmg;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_1d

    .line 6191
    const/16 v1, 0x1e

    iget-object v2, p0, Ldmg;->j:Ljava/lang/Integer;

    .line 6192
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6194
    :cond_1d
    iget-object v1, p0, Ldmg;->y:Ljava/lang/Integer;

    if-eqz v1, :cond_1e

    .line 6195
    const/16 v1, 0x1f

    iget-object v2, p0, Ldmg;->y:Ljava/lang/Integer;

    .line 6196
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6198
    :cond_1e
    iget-object v1, p0, Ldmg;->H:Ljava/lang/Boolean;

    if-eqz v1, :cond_1f

    .line 6199
    const/16 v1, 0x20

    iget-object v2, p0, Ldmg;->H:Ljava/lang/Boolean;

    .line 6200
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6202
    :cond_1f
    iget-object v1, p0, Ldmg;->I:Ldmj;

    if-eqz v1, :cond_20

    .line 6203
    const/16 v1, 0x21

    iget-object v2, p0, Ldmg;->I:Ldmj;

    .line 6204
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6206
    :cond_20
    iget-object v1, p0, Ldmg;->h:Ljava/lang/String;

    if-eqz v1, :cond_21

    .line 6207
    const/16 v1, 0x22

    iget-object v2, p0, Ldmg;->h:Ljava/lang/String;

    .line 6208
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6210
    :cond_21
    iget-object v1, p0, Ldmg;->J:Ldmi;

    if-eqz v1, :cond_22

    .line 6211
    const/16 v1, 0x23

    iget-object v2, p0, Ldmg;->J:Ldmi;

    .line 6212
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6214
    :cond_22
    iget-object v1, p0, Ldmg;->K:Ljava/lang/Boolean;

    if-eqz v1, :cond_23

    .line 6215
    const/16 v1, 0x24

    iget-object v2, p0, Ldmg;->K:Ljava/lang/Boolean;

    .line 6216
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6218
    :cond_23
    iget-object v1, p0, Ldmg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6219
    iput v0, p0, Ldmg;->cachedSize:I

    .line 6220
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 5599
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldmg;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldmg;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldmg;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/16 v2, 0xf

    if-eq v0, v2, :cond_2

    const/16 v2, 0x10

    if-eq v0, v2, :cond_2

    const/16 v2, 0x12

    if-eq v0, v2, :cond_2

    const/16 v2, 0x13

    if-eq v0, v2, :cond_2

    const/16 v2, 0x14

    if-eq v0, v2, :cond_2

    const/16 v2, 0x15

    if-eq v0, v2, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    if-eq v0, v7, :cond_2

    const/4 v2, 0x6

    if-eq v0, v2, :cond_2

    const/4 v2, 0x7

    if-eq v0, v2, :cond_2

    const/16 v2, 0x8

    if-eq v0, v2, :cond_2

    const/16 v2, 0xa

    if-eq v0, v2, :cond_2

    const/16 v2, 0xb

    if-eq v0, v2, :cond_2

    const/16 v2, 0xc

    if-eq v0, v2, :cond_2

    const/16 v2, 0xd

    if-eq v0, v2, :cond_2

    const/16 v2, 0xe

    if-eq v0, v2, :cond_2

    const/16 v2, 0x9

    if-eq v0, v2, :cond_2

    const/16 v2, 0x11

    if-ne v0, v2, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmg;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmg;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_4

    if-ne v0, v5, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmg;->c:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_5
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmg;->c:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldmg;->d:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldmg;->e:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldmg;->f:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmg;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldmg;->k:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldmg;->l:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_9
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldmg;->m:[Ldlm;

    if-nez v0, :cond_7

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldlm;

    iget-object v3, p0, Ldmg;->m:[Ldlm;

    if-eqz v3, :cond_6

    iget-object v3, p0, Ldmg;->m:[Ldlm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Ldmg;->m:[Ldlm;

    :goto_2
    iget-object v2, p0, Ldmg;->m:[Ldlm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Ldmg;->m:[Ldlm;

    new-instance v3, Ldlm;

    invoke-direct {v3}, Ldlm;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldmg;->m:[Ldlm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    iget-object v0, p0, Ldmg;->m:[Ldlm;

    array-length v0, v0

    goto :goto_1

    :cond_8
    iget-object v2, p0, Ldmg;->m:[Ldlm;

    new-instance v3, Ldlm;

    invoke-direct {v3}, Ldlm;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldmg;->m:[Ldlm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_9

    if-eq v0, v4, :cond_9

    if-eq v0, v5, :cond_9

    if-eq v0, v6, :cond_9

    if-ne v0, v7, :cond_a

    :cond_9
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmg;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_a
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmg;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Ldmg;->o:Ldln;

    if-nez v0, :cond_b

    new-instance v0, Ldln;

    invoke-direct {v0}, Ldln;-><init>()V

    iput-object v0, p0, Ldmg;->o:Ldln;

    :cond_b
    iget-object v0, p0, Ldmg;->o:Ldln;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Ldmg;->p:Ldkp;

    if-nez v0, :cond_c

    new-instance v0, Ldkp;

    invoke-direct {v0}, Ldkp;-><init>()V

    iput-object v0, p0, Ldmg;->p:Ldkp;

    :cond_c
    iget-object v0, p0, Ldmg;->p:Ldkp;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Ldmg;->q:Ldlw;

    if-nez v0, :cond_d

    new-instance v0, Ldlw;

    invoke-direct {v0}, Ldlw;-><init>()V

    iput-object v0, p0, Ldmg;->q:Ldlw;

    :cond_d
    iget-object v0, p0, Ldmg;->q:Ldlw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldmg;->r:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_e

    if-eq v0, v5, :cond_e

    if-ne v0, v6, :cond_f

    :cond_e
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmg;->s:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_f
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmg;->s:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_10
    const/16 v0, 0x82

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldmg;->t:[Ldmh;

    if-nez v0, :cond_11

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldmh;

    iget-object v3, p0, Ldmg;->t:[Ldmh;

    if-eqz v3, :cond_10

    iget-object v3, p0, Ldmg;->t:[Ldmh;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_10
    iput-object v2, p0, Ldmg;->t:[Ldmh;

    :goto_4
    iget-object v2, p0, Ldmg;->t:[Ldmh;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_12

    iget-object v2, p0, Ldmg;->t:[Ldmh;

    new-instance v3, Ldmh;

    invoke-direct {v3}, Ldmh;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldmg;->t:[Ldmh;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_11
    iget-object v0, p0, Ldmg;->t:[Ldmh;

    array-length v0, v0

    goto :goto_3

    :cond_12
    iget-object v2, p0, Ldmg;->t:[Ldmh;

    new-instance v3, Ldmh;

    invoke-direct {v3}, Ldmh;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldmg;->t:[Ldmh;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldmg;->u:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldmg;->v:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_13

    if-eq v0, v4, :cond_13

    if-ne v0, v5, :cond_14

    :cond_13
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmg;->w:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_14
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmg;->w:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmg;->x:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-nez v0, :cond_15

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmg;->A:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_15
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmg;->A:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_16
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_16

    if-ne v0, v4, :cond_17

    :cond_16
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmg;->B:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_17
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmg;->B:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_17
    iget-object v0, p0, Ldmg;->C:Ldlk;

    if-nez v0, :cond_18

    new-instance v0, Ldlk;

    invoke-direct {v0}, Ldlk;-><init>()V

    iput-object v0, p0, Ldmg;->C:Ldlk;

    :cond_18
    iget-object v0, p0, Ldmg;->C:Ldlk;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmg;->D:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_19
    iget-object v0, p0, Ldmg;->E:Ldmq;

    if-nez v0, :cond_19

    new-instance v0, Ldmq;

    invoke-direct {v0}, Ldmq;-><init>()V

    iput-object v0, p0, Ldmg;->E:Ldmq;

    :cond_19
    iget-object v0, p0, Ldmg;->E:Ldmq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_1a
    iget-object v0, p0, Ldmg;->F:Ldmf;

    if-nez v0, :cond_1a

    new-instance v0, Ldmf;

    invoke-direct {v0}, Ldmf;-><init>()V

    iput-object v0, p0, Ldmg;->F:Ldmf;

    :cond_1a
    iget-object v0, p0, Ldmg;->F:Ldmf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_1b
    iget-object v0, p0, Ldmg;->G:Ldmr;

    if-nez v0, :cond_1b

    new-instance v0, Ldmr;

    invoke-direct {v0}, Ldmr;-><init>()V

    iput-object v0, p0, Ldmg;->G:Ldmr;

    :cond_1b
    iget-object v0, p0, Ldmg;->G:Ldmr;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_1c
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldmg;->g:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_1d
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldmg;->z:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_1e
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmg;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_1f
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmg;->y:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_20
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldmg;->H:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_21
    iget-object v0, p0, Ldmg;->I:Ldmj;

    if-nez v0, :cond_1c

    new-instance v0, Ldmj;

    invoke-direct {v0}, Ldmj;-><init>()V

    iput-object v0, p0, Ldmg;->I:Ldmj;

    :cond_1c
    iget-object v0, p0, Ldmg;->I:Ldmj;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_22
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldmg;->h:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_23
    iget-object v0, p0, Ldmg;->J:Ldmi;

    if-nez v0, :cond_1d

    new-instance v0, Ldmi;

    invoke-direct {v0}, Ldmi;-><init>()V

    iput-object v0, p0, Ldmg;->J:Ldmi;

    :cond_1d
    iget-object v0, p0, Ldmg;->J:Ldmi;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_24
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldmg;->K:Ljava/lang/Boolean;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x82 -> :sswitch_10
        0x88 -> :sswitch_11
        0x90 -> :sswitch_12
        0x98 -> :sswitch_13
        0xa0 -> :sswitch_14
        0xa8 -> :sswitch_15
        0xb0 -> :sswitch_16
        0xba -> :sswitch_17
        0xc0 -> :sswitch_18
        0xca -> :sswitch_19
        0xd2 -> :sswitch_1a
        0xda -> :sswitch_1b
        0xe2 -> :sswitch_1c
        0xe8 -> :sswitch_1d
        0xf0 -> :sswitch_1e
        0xf8 -> :sswitch_1f
        0x100 -> :sswitch_20
        0x10a -> :sswitch_21
        0x112 -> :sswitch_22
        0x11a -> :sswitch_23
        0x120 -> :sswitch_24
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 5951
    const/4 v1, 0x1

    iget-object v2, p0, Ldmg;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 5952
    const/4 v1, 0x2

    iget-object v2, p0, Ldmg;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 5953
    iget-object v1, p0, Ldmg;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5954
    const/4 v1, 0x3

    iget-object v2, p0, Ldmg;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 5956
    :cond_0
    iget-object v1, p0, Ldmg;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 5957
    const/4 v1, 0x4

    iget-object v2, p0, Ldmg;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 5959
    :cond_1
    iget-object v1, p0, Ldmg;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 5960
    const/4 v1, 0x5

    iget-object v2, p0, Ldmg;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 5962
    :cond_2
    iget-object v1, p0, Ldmg;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 5963
    const/4 v1, 0x6

    iget-object v2, p0, Ldmg;->i:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 5965
    :cond_3
    iget-object v1, p0, Ldmg;->k:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 5966
    const/4 v1, 0x7

    iget-object v2, p0, Ldmg;->k:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 5968
    :cond_4
    iget-object v1, p0, Ldmg;->l:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 5969
    const/16 v1, 0x8

    iget-object v2, p0, Ldmg;->l:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 5971
    :cond_5
    iget-object v1, p0, Ldmg;->m:[Ldlm;

    if-eqz v1, :cond_7

    .line 5972
    iget-object v2, p0, Ldmg;->m:[Ldlm;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 5973
    if-eqz v4, :cond_6

    .line 5974
    const/16 v5, 0x9

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 5972
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5978
    :cond_7
    iget-object v1, p0, Ldmg;->n:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 5979
    const/16 v1, 0xa

    iget-object v2, p0, Ldmg;->n:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 5981
    :cond_8
    iget-object v1, p0, Ldmg;->o:Ldln;

    if-eqz v1, :cond_9

    .line 5982
    const/16 v1, 0xb

    iget-object v2, p0, Ldmg;->o:Ldln;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 5984
    :cond_9
    iget-object v1, p0, Ldmg;->p:Ldkp;

    if-eqz v1, :cond_a

    .line 5985
    const/16 v1, 0xc

    iget-object v2, p0, Ldmg;->p:Ldkp;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 5987
    :cond_a
    iget-object v1, p0, Ldmg;->q:Ldlw;

    if-eqz v1, :cond_b

    .line 5988
    const/16 v1, 0xd

    iget-object v2, p0, Ldmg;->q:Ldlw;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 5990
    :cond_b
    iget-object v1, p0, Ldmg;->r:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    .line 5991
    const/16 v1, 0xe

    iget-object v2, p0, Ldmg;->r:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 5993
    :cond_c
    iget-object v1, p0, Ldmg;->s:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 5994
    const/16 v1, 0xf

    iget-object v2, p0, Ldmg;->s:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 5996
    :cond_d
    iget-object v1, p0, Ldmg;->t:[Ldmh;

    if-eqz v1, :cond_f

    .line 5997
    iget-object v1, p0, Ldmg;->t:[Ldmh;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_f

    aget-object v3, v1, v0

    .line 5998
    if-eqz v3, :cond_e

    .line 5999
    const/16 v4, 0x10

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 5997
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 6003
    :cond_f
    iget-object v0, p0, Ldmg;->u:Ljava/lang/Long;

    if-eqz v0, :cond_10

    .line 6004
    const/16 v0, 0x11

    iget-object v1, p0, Ldmg;->u:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 6006
    :cond_10
    iget-object v0, p0, Ldmg;->v:Ljava/lang/Boolean;

    if-eqz v0, :cond_11

    .line 6007
    const/16 v0, 0x12

    iget-object v1, p0, Ldmg;->v:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 6009
    :cond_11
    iget-object v0, p0, Ldmg;->w:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    .line 6010
    const/16 v0, 0x13

    iget-object v1, p0, Ldmg;->w:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 6012
    :cond_12
    iget-object v0, p0, Ldmg;->x:Ljava/lang/Integer;

    if-eqz v0, :cond_13

    .line 6013
    const/16 v0, 0x14

    iget-object v1, p0, Ldmg;->x:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 6015
    :cond_13
    iget-object v0, p0, Ldmg;->A:Ljava/lang/Integer;

    if-eqz v0, :cond_14

    .line 6016
    const/16 v0, 0x15

    iget-object v1, p0, Ldmg;->A:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 6018
    :cond_14
    iget-object v0, p0, Ldmg;->B:Ljava/lang/Integer;

    if-eqz v0, :cond_15

    .line 6019
    const/16 v0, 0x16

    iget-object v1, p0, Ldmg;->B:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 6021
    :cond_15
    iget-object v0, p0, Ldmg;->C:Ldlk;

    if-eqz v0, :cond_16

    .line 6022
    const/16 v0, 0x17

    iget-object v1, p0, Ldmg;->C:Ldlk;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6024
    :cond_16
    iget-object v0, p0, Ldmg;->D:Ljava/lang/Integer;

    if-eqz v0, :cond_17

    .line 6025
    const/16 v0, 0x18

    iget-object v1, p0, Ldmg;->D:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 6027
    :cond_17
    iget-object v0, p0, Ldmg;->E:Ldmq;

    if-eqz v0, :cond_18

    .line 6028
    const/16 v0, 0x19

    iget-object v1, p0, Ldmg;->E:Ldmq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6030
    :cond_18
    iget-object v0, p0, Ldmg;->F:Ldmf;

    if-eqz v0, :cond_19

    .line 6031
    const/16 v0, 0x1a

    iget-object v1, p0, Ldmg;->F:Ldmf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6033
    :cond_19
    iget-object v0, p0, Ldmg;->G:Ldmr;

    if-eqz v0, :cond_1a

    .line 6034
    const/16 v0, 0x1b

    iget-object v1, p0, Ldmg;->G:Ldmr;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6036
    :cond_1a
    iget-object v0, p0, Ldmg;->g:Ljava/lang/String;

    if-eqz v0, :cond_1b

    .line 6037
    const/16 v0, 0x1c

    iget-object v1, p0, Ldmg;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 6039
    :cond_1b
    iget-object v0, p0, Ldmg;->z:Ljava/lang/Boolean;

    if-eqz v0, :cond_1c

    .line 6040
    const/16 v0, 0x1d

    iget-object v1, p0, Ldmg;->z:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 6042
    :cond_1c
    iget-object v0, p0, Ldmg;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_1d

    .line 6043
    const/16 v0, 0x1e

    iget-object v1, p0, Ldmg;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 6045
    :cond_1d
    iget-object v0, p0, Ldmg;->y:Ljava/lang/Integer;

    if-eqz v0, :cond_1e

    .line 6046
    const/16 v0, 0x1f

    iget-object v1, p0, Ldmg;->y:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 6048
    :cond_1e
    iget-object v0, p0, Ldmg;->H:Ljava/lang/Boolean;

    if-eqz v0, :cond_1f

    .line 6049
    const/16 v0, 0x20

    iget-object v1, p0, Ldmg;->H:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 6051
    :cond_1f
    iget-object v0, p0, Ldmg;->I:Ldmj;

    if-eqz v0, :cond_20

    .line 6052
    const/16 v0, 0x21

    iget-object v1, p0, Ldmg;->I:Ldmj;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6054
    :cond_20
    iget-object v0, p0, Ldmg;->h:Ljava/lang/String;

    if-eqz v0, :cond_21

    .line 6055
    const/16 v0, 0x22

    iget-object v1, p0, Ldmg;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 6057
    :cond_21
    iget-object v0, p0, Ldmg;->J:Ldmi;

    if-eqz v0, :cond_22

    .line 6058
    const/16 v0, 0x23

    iget-object v1, p0, Ldmg;->J:Ldmi;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6060
    :cond_22
    iget-object v0, p0, Ldmg;->K:Ljava/lang/Boolean;

    if-eqz v0, :cond_23

    .line 6061
    const/16 v0, 0x24

    iget-object v1, p0, Ldmg;->K:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 6063
    :cond_23
    iget-object v0, p0, Ldmg;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 6065
    return-void
.end method
