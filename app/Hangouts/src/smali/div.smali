.class public final Ldiv;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldiv;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:[Ldkj;

.field public g:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Ldiv;

    sput-object v0, Ldiv;->a:[Ldiv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 21
    sget-object v0, Ldkj;->a:[Ldkj;

    iput-object v0, p0, Ldiv;->f:[Ldkj;

    .line 24
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldiv;->g:[Ljava/lang/String;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 58
    iget-object v0, p0, Ldiv;->b:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 59
    const/4 v0, 0x1

    iget-object v2, p0, Ldiv;->b:Ljava/lang/String;

    .line 60
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 62
    :goto_0
    iget-object v2, p0, Ldiv;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 63
    const/4 v2, 0x2

    iget-object v3, p0, Ldiv;->c:Ljava/lang/String;

    .line 64
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 66
    :cond_0
    const/4 v2, 0x3

    iget-object v3, p0, Ldiv;->d:Ljava/lang/String;

    .line 67
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 68
    iget-object v2, p0, Ldiv;->f:[Ldkj;

    if-eqz v2, :cond_2

    .line 69
    iget-object v3, p0, Ldiv;->f:[Ldkj;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 70
    if-eqz v5, :cond_1

    .line 71
    const/4 v6, 0x4

    .line 72
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 69
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 76
    :cond_2
    iget-object v2, p0, Ldiv;->g:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Ldiv;->g:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 78
    iget-object v3, p0, Ldiv;->g:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    .line 80
    invoke-static {v5}, Lepl;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 78
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 82
    :cond_3
    add-int/2addr v0, v2

    .line 83
    iget-object v1, p0, Ldiv;->g:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 85
    :cond_4
    iget-object v1, p0, Ldiv;->e:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 86
    const/4 v1, 0x6

    iget-object v2, p0, Ldiv;->e:Ljava/lang/String;

    .line 87
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_5
    iget-object v1, p0, Ldiv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    iput v0, p0, Ldiv;->cachedSize:I

    .line 91
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldiv;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldiv;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldiv;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldiv;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldiv;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldiv;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldiv;->f:[Ldkj;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldkj;

    iget-object v3, p0, Ldiv;->f:[Ldkj;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldiv;->f:[Ldkj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldiv;->f:[Ldkj;

    :goto_2
    iget-object v2, p0, Ldiv;->f:[Ldkj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldiv;->f:[Ldkj;

    new-instance v3, Ldkj;

    invoke-direct {v3}, Ldkj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldiv;->f:[Ldkj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldiv;->f:[Ldkj;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldiv;->f:[Ldkj;

    new-instance v3, Ldkj;

    invoke-direct {v3}, Ldkj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldiv;->f:[Ldkj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldiv;->g:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Ldiv;->g:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldiv;->g:[Ljava/lang/String;

    :goto_3
    iget-object v2, p0, Ldiv;->g:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldiv;->g:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    iget-object v2, p0, Ldiv;->g:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldiv;->e:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 29
    iget-object v1, p0, Ldiv;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30
    const/4 v1, 0x1

    iget-object v2, p0, Ldiv;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 32
    :cond_0
    iget-object v1, p0, Ldiv;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 33
    const/4 v1, 0x2

    iget-object v2, p0, Ldiv;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 35
    :cond_1
    const/4 v1, 0x3

    iget-object v2, p0, Ldiv;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 36
    iget-object v1, p0, Ldiv;->f:[Ldkj;

    if-eqz v1, :cond_3

    .line 37
    iget-object v2, p0, Ldiv;->f:[Ldkj;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 38
    if-eqz v4, :cond_2

    .line 39
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 37
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 43
    :cond_3
    iget-object v1, p0, Ldiv;->g:[Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 44
    iget-object v1, p0, Ldiv;->g:[Ljava/lang/String;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 45
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Lepl;->a(ILjava/lang/String;)V

    .line 44
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 48
    :cond_4
    iget-object v0, p0, Ldiv;->e:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 49
    const/4 v0, 0x6

    iget-object v1, p0, Ldiv;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 51
    :cond_5
    iget-object v0, p0, Ldiv;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 53
    return-void
.end method
