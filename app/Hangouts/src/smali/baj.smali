.class public Lbaj;
.super Ldb;
.source "PG"


# instance fields
.field private u:Z

.field private final v:Ldh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">.dh;"
        }
    .end annotation
.end field

.field private final w:Lyj;

.field private final x:Landroid/net/Uri;

.field private y:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lyj;)V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lbaj;-><init>(Landroid/content/Context;Lyj;B)V

    .line 37
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lyj;B)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0, p1}, Ldb;-><init>(Landroid/content/Context;)V

    .line 24
    new-instance v0, Ldh;

    invoke-direct {v0, p0}, Ldh;-><init>(Ldg;)V

    iput-object v0, p0, Lbaj;->v:Ldh;

    .line 44
    iput-object p2, p0, Lbaj;->w:Lyj;

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lbaj;->x:Landroid/net/Uri;

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lyj;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 53
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lbaj;-><init>(Landroid/content/Context;Lyj;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;B)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lyj;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;B)V
    .locals 7

    .prologue
    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    .line 62
    invoke-direct/range {v0 .. v6}, Ldb;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    new-instance v0, Ldh;

    invoke-direct {v0, p0}, Ldh;-><init>(Ldg;)V

    iput-object v0, p0, Lbaj;->v:Ldh;

    .line 63
    iput-object p2, p0, Lbaj;->w:Lyj;

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lbaj;->x:Landroid/net/Uri;

    .line 65
    return-void
.end method


# virtual methods
.method public A()Lyj;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lbaj;->w:Lyj;

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lbaj;->y:Z

    if-nez v0, :cond_0

    .line 131
    invoke-super {p0, p1}, Ldb;->a(Landroid/database/Cursor;)V

    .line 133
    :cond_0
    return-void
.end method

.method public synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lbaj;->b(Landroid/database/Cursor;)V

    return-void
.end method

.method public b(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 158
    invoke-super {p0, p1}, Ldb;->b(Landroid/database/Cursor;)V

    .line 171
    invoke-virtual {p0}, Lbaj;->y()V

    .line 172
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lbaj;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 181
    invoke-super {p0}, Ldb;->b()Z

    move-result v0

    return v0
.end method

.method public synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lbaj;->f()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final f()Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    .line 108
    invoke-virtual {p0}, Lbaj;->j()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_0

    .line 109
    iput-boolean v5, p0, Lbaj;->y:Z

    .line 121
    :goto_0
    return-object v0

    .line 114
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lbaj;->z()Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 115
    :catch_0
    move-exception v1

    .line 116
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "loadInBackground for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbaj;->j()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " failed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 117
    iput-boolean v5, p0, Lbaj;->y:Z

    goto :goto_0
.end method

.method protected g()V
    .locals 4

    .prologue
    .line 76
    invoke-super {p0}, Ldb;->g()V

    .line 77
    iget-boolean v0, p0, Lbaj;->u:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lbaj;->x:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {p0}, Lbaj;->k()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lbaj;->x:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lbaj;->v:Ldh;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbaj;->u:Z

    .line 82
    :cond_0
    return-void
.end method

.method protected i()V
    .locals 0

    .prologue
    .line 152
    invoke-virtual {p0}, Lbaj;->t()V

    .line 153
    invoke-super {p0}, Ldb;->i()V

    .line 154
    return-void
.end method

.method protected t()V
    .locals 2

    .prologue
    .line 140
    invoke-super {p0}, Ldb;->t()V

    .line 141
    iget-boolean v0, p0, Lbaj;->u:Z

    if-eqz v0, :cond_0

    .line 142
    invoke-virtual {p0}, Lbaj;->k()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lbaj;->v:Ldh;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 143
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbaj;->u:Z

    .line 145
    :cond_0
    return-void
.end method

.method public z()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 90
    invoke-super {p0}, Ldb;->f()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
