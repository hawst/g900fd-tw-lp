.class public final Lakt;
.super Ljd;
.source "PG"


# instance fields
.field final synthetic j:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lakt;->j:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    .line 225
    invoke-direct {p0, p2}, Ljd;-><init>(Landroid/content/Context;)V

    .line 226
    iput-object p2, p0, Lakt;->d:Landroid/content/Context;

    .line 227
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 270
    invoke-static {p1}, Lccn;->a(Landroid/content/Context;)Lccn;

    move-result-object v0

    .line 271
    iget-object v1, p0, Lakt;->j:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->h(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)Lcco;

    move-result-object v1

    invoke-virtual {v0, v1}, Lccn;->a(Lcco;)V

    .line 272
    return-object v0
.end method

.method public a(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 277
    check-cast p1, Lccn;

    .line 278
    const/4 v0, 0x3

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lccn;->a(Ljava/lang/String;)V

    .line 279
    const/4 v0, 0x4

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lakt;->j:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    .line 280
    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->c(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)Lyj;

    move-result-object v1

    .line 279
    invoke-virtual {p1, v0, v1}, Lccn;->a(Ljava/lang/String;Lyj;)V

    .line 281
    new-instance v0, Lbdk;

    const/4 v1, 0x2

    .line 282
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    .line 283
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    invoke-virtual {p1, v0}, Lccn;->a(Lbdk;)V

    .line 285
    return-void
.end method

.method public b(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 264
    invoke-super {p0, p1}, Ljd;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 265
    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 236
    invoke-virtual {p0}, Lakt;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 237
    if-nez p2, :cond_0

    iget-object v0, p0, Lakt;->d:Landroid/content/Context;

    invoke-virtual {p0}, Lakt;->a()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p3}, Lakt;->a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 243
    :cond_0
    :goto_0
    return-object p2

    .line 241
    :cond_1
    invoke-super {p0, p1, p2, p3}, Ljd;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 242
    sget v0, Lg;->hW:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    move-object v1, v2

    check-cast v1, Lccn;

    invoke-virtual {v1}, Lccn;->a()Lbdk;

    move-result-object v1

    iget-object v1, v1, Lbdk;->a:Ljava/lang/String;

    iget-object v3, p0, Lakt;->j:Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->a(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)Landroid/util/SparseArray;

    move-result-object v3

    invoke-static {v3, v1}, Lf;->a(Landroid/util/SparseArray;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    sget v1, Lh;->nz:I

    move v3, v1

    :goto_1
    if-nez v4, :cond_3

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(I)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    move-object p2, v2

    .line 243
    goto :goto_0

    .line 242
    :cond_2
    sget v1, Lh;->ny:I

    move v3, v1

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 290
    invoke-virtual {p0}, Lakt;->a()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_0

    .line 291
    const/4 v0, 0x1

    .line 293
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Ljd;->isEmpty()Z

    move-result v0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 259
    const/4 v0, 0x0

    return v0
.end method
