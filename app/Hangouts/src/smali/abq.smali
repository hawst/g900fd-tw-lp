.class public abstract Labq;
.super Lakl;
.source "PG"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lbsl;
.implements Lcfv;
.implements Lcfw;


# static fields
.field public static final a:Z


# instance fields
.field private Y:Lcom/google/android/apps/hangouts/views/EsListView;

.field private Z:Z

.field protected b:Lcvi;

.field protected c:Lbsj;

.field protected d:Lcvx;

.field protected e:Ladj;

.field protected f:Landroid/widget/TextView;

.field protected g:Z

.field private h:Ljava/lang/String;

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lbys;->d:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Labq;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Lakl;-><init>()V

    .line 66
    iput-boolean v0, p0, Labq;->g:Z

    .line 74
    iput v0, p0, Labq;->i:I

    return-void
.end method

.method private r()V
    .locals 4

    .prologue
    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Labq;->c:Lbsj;

    .line 172
    iget-object v0, p0, Labq;->b:Lcvi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Labq;->b:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Labq;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    new-instance v0, Lbsj;

    iget-object v1, p0, Labq;->b:Lcvi;

    invoke-virtual {p0}, Labq;->f()Lyj;

    move-result-object v2

    iget-object v3, p0, Labq;->h:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p0, v3}, Lbsj;-><init>(Lcvi;Lyj;Lbsl;Ljava/lang/String;)V

    iput-object v0, p0, Labq;->c:Lbsj;

    .line 174
    iget-object v0, p0, Labq;->c:Lbsj;

    invoke-virtual {v0}, Lbsj;->a()V

    .line 176
    :cond_0
    return-void
.end method

.method private s()V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Labq;->d:Lcvx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Labq;->d:Lcvx;

    invoke-virtual {v0}, Lcvx;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 184
    iget-object v0, p0, Labq;->d:Lcvx;

    invoke-virtual {v0}, Lcvx;->d()V

    .line 185
    const/4 v0, 0x0

    iput-object v0, p0, Labq;->d:Lcvx;

    .line 187
    :cond_0
    return-void
.end method

.method private t()V
    .locals 5

    .prologue
    .line 307
    new-instance v0, Ladj;

    invoke-virtual {p0}, Labq;->getActivity()Ly;

    move-result-object v1

    invoke-virtual {p0}, Labq;->f()Lyj;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ladj;-><init>(Landroid/content/Context;Lyj;)V

    iput-object v0, p0, Labq;->e:Ladj;

    .line 308
    invoke-virtual {p0}, Labq;->a()[Ladl;

    move-result-object v1

    .line 309
    array-length v0, v1

    iput v0, p0, Labq;->i:I

    .line 310
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 311
    iget-object v4, p0, Labq;->e:Ladj;

    invoke-virtual {v4, v3}, Ladj;->a(Lpj;)V

    .line 310
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 317
    :cond_0
    iget-object v0, p0, Labq;->Y:Lcom/google/android/apps/hangouts/views/EsListView;

    if-eqz v0, :cond_1

    .line 318
    iget-object v0, p0, Labq;->Y:Lcom/google/android/apps/hangouts/views/EsListView;

    iget-object v1, p0, Labq;->e:Ladj;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/EsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 320
    :cond_1
    return-void
.end method


# virtual methods
.method protected a(ILadk;)V
    .locals 2

    .prologue
    .line 326
    iget-object v0, p0, Labq;->e:Ladj;

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Labq;->e:Ladj;

    .line 328
    invoke-virtual {v0, p1}, Ladj;->a(I)Lpj;

    move-result-object v0

    check-cast v0, Ladl;

    .line 329
    iget-object v1, p0, Labq;->e:Ladj;

    invoke-virtual {v1, p1, p2}, Ladj;->a(ILandroid/database/Cursor;)V

    .line 330
    iget-object v1, p0, Labq;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ladl;->a(Ljava/lang/CharSequence;)V

    .line 332
    iget-object v0, p0, Labq;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 333
    iget-object v0, p0, Labq;->e:Ladj;

    invoke-virtual {v0}, Ladj;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 334
    iget-object v0, p0, Labq;->f:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 340
    :cond_0
    :goto_0
    return-void

    .line 336
    :cond_1
    iget-object v0, p0, Labq;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Labs;)V
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, Labq;->Y:Lcom/google/android/apps/hangouts/views/EsListView;

    new-instance v1, Labr;

    invoke-direct {v1, p0, p1}, Labr;-><init>(Labq;Labs;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/EsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 287
    return-void
.end method

.method public a(Lbsj;Lcvx;Lcwf;)V
    .locals 2

    .prologue
    .line 295
    iget-object v0, p0, Labq;->c:Lbsj;

    if-ne p1, v0, :cond_1

    .line 296
    invoke-direct {p0}, Labq;->s()V

    .line 297
    iput-object p2, p0, Labq;->d:Lcvx;

    .line 300
    iget-object v0, p0, Labq;->e:Ladj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Labq;->e:Ladj;

    invoke-virtual {v0}, Ladj;->g()Lyj;

    move-result-object v0

    invoke-virtual {p0}, Labq;->f()Lyj;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 301
    :cond_0
    invoke-direct {p0}, Labq;->t()V

    .line 304
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 146
    invoke-static {}, Lcwz;->a()V

    .line 147
    iget-object v0, p0, Labq;->e:Ladj;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Labq;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 151
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Labq;->h:Ljava/lang/String;

    .line 152
    iget-object v0, p0, Labq;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Labq;->b(Ljava/lang/CharSequence;)V

    .line 155
    :cond_0
    return-void

    .line 151
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected abstract a()[Ladl;
.end method

.method protected b()Lcom/google/android/apps/hangouts/views/EsListView;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Labq;->Y:Lcom/google/android/apps/hangouts/views/EsListView;

    return-object v0
.end method

.method protected b(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0}, Labq;->r()V

    .line 167
    return-void
.end method

.method protected c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Labq;->h:Ljava/lang/String;

    return-object v0
.end method

.method protected d()Z
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x1

    return v0
.end method

.method public e()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 212
    iget-object v1, p0, Labq;->e:Ladj;

    if-eqz v1, :cond_0

    iget-object v1, p0, Labq;->e:Ladj;

    invoke-virtual {v1}, Ladj;->g()Lyj;

    move-result-object v1

    invoke-virtual {p0}, Labq;->f()Lyj;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 213
    const/4 v0, 0x0

    .line 222
    :goto_0
    return v0

    .line 216
    :cond_0
    invoke-direct {p0}, Labq;->t()V

    .line 218
    invoke-virtual {p0}, Labq;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 219
    invoke-direct {p0}, Labq;->r()V

    .line 221
    :cond_1
    iput-boolean v0, p0, Labq;->g:Z

    goto :goto_0
.end method

.method protected f()Lyj;
    .locals 2

    .prologue
    .line 268
    invoke-virtual {p0}, Labq;->getActivity()Ly;

    move-result-object v0

    check-cast v0, Lakn;

    .line 270
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lakn;->k()Lyj;

    move-result-object v1

    if-nez v1, :cond_1

    .line 271
    :cond_0
    const-string v0, "Babel"

    const-string v1, "Account is no longer valid in BasePhoneContactListFragment."

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    const/4 v0, 0x0

    .line 275
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lakn;->k()Lyj;

    move-result-object v0

    goto :goto_0
.end method

.method public onConnected(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 227
    sget-boolean v0, Labq;->a:Z

    if-eqz v0, :cond_0

    .line 228
    const-string v0, "Babel"

    const-string v1, "People client connected."

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    :cond_0
    invoke-virtual {p0}, Labq;->getActivity()Ly;

    move-result-object v0

    if-nez v0, :cond_2

    .line 234
    const-string v0, "Babel"

    const-string v1, "People client connected but MakePhoneCallFragment is detached."

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    :cond_1
    :goto_0
    return-void

    .line 238
    :cond_2
    iget-object v0, p0, Labq;->b:Lcvi;

    invoke-static {v0}, Lf;->a(Lcvi;)V

    .line 245
    iget-boolean v0, p0, Labq;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Labq;->c:Lbsj;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Labq;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 246
    invoke-direct {p0}, Labq;->r()V

    goto :goto_0
.end method

.method public onConnectionFailed(Lcft;)V
    .locals 3

    .prologue
    .line 264
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "People client connection failure: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 89
    invoke-super {p0, p1}, Lakl;->onCreate(Landroid/os/Bundle;)V

    .line 91
    new-instance v0, Lcvi;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0, p0}, Lcvi;-><init>(Landroid/content/Context;Lcfv;Lcfw;)V

    iput-object v0, p0, Labq;->b:Lcvi;

    .line 93
    iget-object v0, p0, Labq;->b:Lcvi;

    invoke-virtual {v0}, Lcvi;->a()V

    .line 94
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;
    .locals 3

    .prologue
    .line 102
    invoke-super {p0, p1, p2, p3, p4}, Lakl;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v1

    .line 105
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/EsListView;

    iput-object v0, p0, Labq;->Y:Lcom/google/android/apps/hangouts/views/EsListView;

    .line 106
    iget-object v0, p0, Labq;->Y:Lcom/google/android/apps/hangouts/views/EsListView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/hangouts/views/EsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 107
    sget v0, Lg;->eL:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Labq;->f:Landroid/widget/TextView;

    .line 109
    iget-object v0, p0, Labq;->e:Ladj;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Labq;->Y:Lcom/google/android/apps/hangouts/views/EsListView;

    iget-object v2, p0, Labq;->e:Ladj;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/EsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 111
    iget-object v0, p0, Labq;->e:Ladj;

    invoke-virtual {v0}, Ladj;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 112
    iget-object v0, p0, Labq;->f:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 118
    :cond_0
    :goto_0
    iget-boolean v0, p0, Labq;->Z:Z

    if-eqz v0, :cond_1

    .line 119
    invoke-virtual {p0}, Labq;->q()V

    .line 122
    :cond_1
    return-object v1

    .line 114
    :cond_2
    iget-object v0, p0, Labq;->f:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 127
    invoke-super {p0}, Lakl;->onDestroy()V

    .line 128
    invoke-direct {p0}, Labq;->s()V

    .line 129
    iput-object v1, p0, Labq;->c:Lbsj;

    .line 131
    iget-object v0, p0, Labq;->b:Lcvi;

    if-eqz v0, :cond_1

    iget-object v0, p0, Labq;->b:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Labq;->b:Lcvi;

    invoke-virtual {v0}, Lcvi;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 132
    :cond_0
    iget-object v0, p0, Labq;->b:Lcvi;

    invoke-virtual {v0}, Lcvi;->b()V

    .line 133
    iput-object v1, p0, Labq;->b:Lcvi;

    .line 137
    :cond_1
    iput-object v1, p0, Labq;->e:Ladj;

    .line 138
    return-void
.end method

.method public onDisconnected()V
    .locals 2

    .prologue
    .line 252
    sget-boolean v0, Labq;->a:Z

    if-eqz v0, :cond_0

    .line 253
    const-string v0, "Babel"

    const-string v1, "People client disconnected."

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    :cond_0
    iget-object v0, p0, Labq;->c:Lbsj;

    if-eqz v0, :cond_1

    .line 257
    const-string v0, "Babel"

    const-string v1, "Resetting mGmsPeopleLoader after people client disconnect"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const/4 v0, 0x0

    iput-object v0, p0, Labq;->c:Lbsj;

    .line 260
    :cond_1
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 365
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 3

    .prologue
    .line 347
    packed-switch p2, :pswitch_data_0

    .line 353
    :goto_0
    return-void

    .line 350
    :pswitch_0
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    .line 351
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 352
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0

    .line 347
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public q()V
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Labq;->Y:Lcom/google/android/apps/hangouts/views/EsListView;

    if-nez v0, :cond_0

    .line 370
    const/4 v0, 0x1

    iput-boolean v0, p0, Labq;->Z:Z

    .line 375
    :goto_0
    return-void

    .line 373
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Labq;->Z:Z

    .line 374
    iget-object v0, p0, Labq;->Y:Lcom/google/android/apps/hangouts/views/EsListView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/EsListView;->setSelectionAfterHeaderView()V

    goto :goto_0
.end method
