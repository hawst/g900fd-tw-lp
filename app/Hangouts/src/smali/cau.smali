.class public Lcau;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field a:Lcav;

.field private b:Landroid/widget/TextView;

.field private c:Lcom/google/android/apps/hangouts/views/AvatarView;

.field private d:Landroid/widget/Button;

.field private e:Ljava/lang/String;

.field private f:Lbdk;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcau;-><init>(Landroid/content/Context;B)V

    .line 50
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 2

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lf;->el:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 57
    sget v0, Lg;->hT:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcau;->d:Landroid/widget/Button;

    .line 58
    iget-object v0, p0, Lcau;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    sget v0, Lg;->eH:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcau;->b:Landroid/widget/TextView;

    .line 60
    sget v0, Lg;->E:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/AvatarView;

    iput-object v0, p0, Lcau;->c:Lcom/google/android/apps/hangouts/views/AvatarView;

    .line 61
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcau;
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcau;

    invoke-direct {v0, p0}, Lcau;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public a()Lbdk;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcau;->f:Lbdk;

    return-object v0
.end method

.method public a(Lbdk;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcau;->f:Lbdk;

    .line 72
    return-void
.end method

.method public a(Lcav;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcau;->a:Lcav;

    .line 68
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 79
    iput-object p1, p0, Lcau;->e:Ljava/lang/String;

    .line 80
    iget-object v0, p0, Lcau;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    return-void
.end method

.method public a(Ljava/lang/String;Lyj;)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcau;->c:Lcom/google/android/apps/hangouts/views/AvatarView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Ljava/lang/String;Lyj;)V

    .line 89
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcau;->e:Ljava/lang/String;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcau;->a:Lcav;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcau;->d:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 98
    iget-object v0, p0, Lcau;->a:Lcav;

    invoke-interface {v0, p0}, Lcav;->a(Lcau;)V

    .line 102
    :cond_0
    return-void
.end method
