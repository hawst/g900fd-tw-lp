.class public final Ldpq;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldpq;


# instance fields
.field public b:Ldtx;

.field public c:[Ldru;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5604
    const/4 v0, 0x0

    new-array v0, v0, [Ldpq;

    sput-object v0, Ldpq;->a:[Ldpq;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5605
    invoke-direct {p0}, Lepn;-><init>()V

    .line 5608
    const/4 v0, 0x0

    iput-object v0, p0, Ldpq;->b:Ldtx;

    .line 5611
    sget-object v0, Ldru;->a:[Ldru;

    iput-object v0, p0, Ldpq;->c:[Ldru;

    .line 5605
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 5633
    iget-object v1, p0, Ldpq;->c:[Ldru;

    if-eqz v1, :cond_1

    .line 5634
    iget-object v2, p0, Ldpq;->c:[Ldru;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 5635
    if-eqz v4, :cond_0

    .line 5636
    const/4 v5, 0x2

    .line 5637
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 5634
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5641
    :cond_1
    iget-object v1, p0, Ldpq;->b:Ldtx;

    if-eqz v1, :cond_2

    .line 5642
    const/4 v1, 0x3

    iget-object v2, p0, Ldpq;->b:Ldtx;

    .line 5643
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5645
    :cond_2
    iget-object v1, p0, Ldpq;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5646
    iput v0, p0, Ldpq;->cachedSize:I

    .line 5647
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5601
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldpq;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldpq;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldpq;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldpq;->c:[Ldru;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldru;

    iget-object v3, p0, Ldpq;->c:[Ldru;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldpq;->c:[Ldru;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldpq;->c:[Ldru;

    :goto_2
    iget-object v2, p0, Ldpq;->c:[Ldru;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldpq;->c:[Ldru;

    new-instance v3, Ldru;

    invoke-direct {v3}, Ldru;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldpq;->c:[Ldru;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldpq;->c:[Ldru;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldpq;->c:[Ldru;

    new-instance v3, Ldru;

    invoke-direct {v3}, Ldru;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldpq;->c:[Ldru;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldpq;->b:Ldtx;

    if-nez v0, :cond_5

    new-instance v0, Ldtx;

    invoke-direct {v0}, Ldtx;-><init>()V

    iput-object v0, p0, Ldpq;->b:Ldtx;

    :cond_5
    iget-object v0, p0, Ldpq;->b:Ldtx;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 5616
    iget-object v0, p0, Ldpq;->c:[Ldru;

    if-eqz v0, :cond_1

    .line 5617
    iget-object v1, p0, Ldpq;->c:[Ldru;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 5618
    if-eqz v3, :cond_0

    .line 5619
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 5617
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5623
    :cond_1
    iget-object v0, p0, Ldpq;->b:Ldtx;

    if-eqz v0, :cond_2

    .line 5624
    const/4 v0, 0x3

    iget-object v1, p0, Ldpq;->b:Ldtx;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5626
    :cond_2
    iget-object v0, p0, Ldpq;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 5628
    return-void
.end method
