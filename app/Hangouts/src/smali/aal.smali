.class public final Laal;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Z

.field private static final b:Les;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Les",
            "<",
            "Ljava/lang/String;",
            "Laal;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Lyj;

.field private final d:Les;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Les",
            "<",
            "Ljava/lang/String;",
            "Laam;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lbyz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbyz",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lbys;->c:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Laal;->a:Z

    .line 36
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    sput-object v0, Laal;->b:Les;

    return-void
.end method

.method private constructor <init>(Lyj;)V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    iput-object v0, p0, Laal;->d:Les;

    .line 108
    new-instance v0, Lbyz;

    invoke-direct {v0}, Lbyz;-><init>()V

    iput-object v0, p0, Laal;->e:Lbyz;

    .line 85
    iput-object p1, p0, Laal;->c:Lyj;

    .line 86
    return-void
.end method

.method public static a(Lyj;)Laal;
    .locals 5

    .prologue
    .line 46
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    .line 48
    sget-object v2, Laal;->b:Les;

    monitor-enter v2

    .line 49
    :try_start_0
    sget-object v0, Laal;->b:Les;

    invoke-virtual {v0, v1}, Les;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laal;

    .line 51
    if-nez v0, :cond_1

    .line 52
    sget-boolean v0, Laal;->a:Z

    if-eqz v0, :cond_0

    .line 53
    const-string v0, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Adding participant cache for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    :cond_0
    new-instance v0, Laal;

    invoke-direct {v0, p0}, Laal;-><init>(Lyj;)V

    .line 57
    sget-object v3, Laal;->b:Les;

    invoke-virtual {v3, v1, v0}, Les;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    return-object v0

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public static a(Lyj;Lbdh;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 174
    if-eqz p0, :cond_0

    if-eqz p2, :cond_0

    .line 175
    invoke-static {p0}, Laal;->a(Lyj;)Laal;

    move-result-object v1

    invoke-virtual {p1}, Lbdh;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, p2, p1, v0}, Laal;->a(Ljava/lang/String;Lbdh;Z)Z

    .line 177
    :cond_0
    return-void

    .line 175
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lbdk;Z)Lbdh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 122
    iget-object v2, p0, Laal;->d:Les;

    monitor-enter v2

    .line 123
    :try_start_0
    iget-object v0, p0, Laal;->e:Lbyz;

    invoke-virtual {v0, p1}, Lbyz;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 124
    if-eqz v0, :cond_2

    .line 125
    iget-object v3, p0, Laal;->d:Les;

    invoke-virtual {v3, v0}, Les;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laam;

    .line 127
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    if-nez v0, :cond_1

    .line 129
    if-eqz p2, :cond_0

    .line 130
    invoke-static {}, Lcwz;->b()V

    .line 131
    invoke-static {p1}, Lbcn;->a(Lbdk;)Lbcn;

    move-result-object v0

    .line 132
    new-instance v1, Lyt;

    iget-object v2, p0, Laal;->c:Lyj;

    invoke-direct {v1, v2}, Lyt;-><init>(Lyj;)V

    .line 133
    invoke-virtual {v1, v0}, Lyt;->a(Lbcn;)Lbdh;

    move-result-object v0

    .line 141
    :goto_1
    return-object v0

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 136
    :cond_0
    iget-object v0, p0, Laal;->c:Lyj;

    invoke-static {p1, v0}, Lbrf;->a(Lbdk;Lyj;)V

    move-object v0, v1

    goto :goto_1

    .line 139
    :cond_1
    iget-object v0, v0, Laam;->a:Lbdh;

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Z)Lbdh;
    .locals 3

    .prologue
    .line 153
    const/4 v1, 0x0

    .line 155
    iget-object v2, p0, Laal;->d:Les;

    monitor-enter v2

    .line 156
    :try_start_0
    iget-object v0, p0, Laal;->d:Les;

    invoke-virtual {v0, p1}, Les;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laam;

    .line 157
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 158
    if-nez v0, :cond_1

    .line 159
    if-eqz p2, :cond_0

    .line 160
    invoke-static {}, Lcwz;->b()V

    .line 161
    new-instance v0, Lyt;

    iget-object v1, p0, Laal;->c:Lyj;

    invoke-direct {v0, v1}, Lyt;-><init>(Lyj;)V

    .line 162
    invoke-virtual {v0, p1}, Lyt;->I(Ljava/lang/String;)Lbdh;

    move-result-object v0

    .line 170
    :goto_0
    return-object v0

    .line 157
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 165
    :cond_0
    iget-object v0, p0, Laal;->c:Lyj;

    invoke-static {p1, v0}, Lbrf;->a(Ljava/lang/String;Lyj;)V

    move-object v0, v1

    goto :goto_0

    .line 168
    :cond_1
    iget-object v0, v0, Laam;->a:Lbdh;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lbdh;Z)Z
    .locals 12

    .prologue
    .line 188
    const/4 v1, 0x0

    .line 189
    if-eqz p2, :cond_c

    if-eqz p1, :cond_c

    .line 190
    const/4 v2, 0x0

    .line 191
    iget-object v10, p0, Laal;->d:Les;

    monitor-enter v10

    .line 192
    :try_start_0
    iget-object v0, p0, Laal;->d:Les;

    invoke-virtual {v0, p1}, Les;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laam;

    .line 197
    if-eqz v0, :cond_b

    .line 198
    iget-object v3, v0, Laam;->a:Lbdh;

    iget-object v3, v3, Lbdh;->b:Lbdk;

    if-eqz v3, :cond_7

    .line 199
    iget-object v3, v0, Laam;->a:Lbdh;

    iget-object v3, v3, Lbdh;->b:Lbdk;

    iget-object v4, p2, Lbdh;->b:Lbdk;

    invoke-virtual {v3, v4}, Lbdk;->a(Lbdk;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 200
    iget-object v0, v0, Laam;->a:Lbdh;

    .line 208
    :goto_0
    if-nez v0, :cond_a

    .line 209
    iget-object v0, p2, Lbdh;->b:Lbdk;

    if-nez v0, :cond_8

    const/4 v0, 0x0

    :goto_1
    iget-object v1, p2, Lbdh;->b:Lbdk;

    if-nez v1, :cond_9

    const/4 v1, 0x0

    :goto_2
    iget-object v2, p2, Lbdh;->d:Ljava/lang/String;

    iget-object v3, p2, Lbdh;->c:Ljava/lang/String;

    iget-object v4, p2, Lbdh;->e:Ljava/lang/String;

    iget-object v5, p2, Lbdh;->f:Ljava/lang/String;

    iget-object v6, p2, Lbdh;->g:Ljava/lang/String;

    iget-object v7, p2, Lbdh;->h:Ljava/lang/String;

    iget-object v8, p2, Lbdh;->p:Ljava/lang/String;

    iget-object v9, p2, Lbdh;->i:Ljava/lang/Boolean;

    invoke-static/range {v0 .. v9}, Lbdh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lbdh;

    move-result-object v0

    .line 215
    invoke-virtual {p2}, Lbdh;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbdh;->a(Ljava/lang/String;)V

    .line 216
    new-instance v1, Laam;

    invoke-direct {v1, p0, v0}, Laam;-><init>(Laal;Lbdh;)V

    .line 217
    iget-object v2, p0, Laal;->d:Les;

    invoke-virtual {v2, p1, v1}, Les;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    iget-object v1, p0, Laal;->e:Lbyz;

    iget-object v2, v0, Lbdh;->b:Lbdk;

    invoke-virtual {v1, v2, p1}, Lbyz;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    const/4 v1, 0x1

    move-object v11, v0

    move v0, v1

    move-object v1, v11

    .line 222
    :goto_3
    if-eqz p3, :cond_0

    invoke-virtual {v1}, Lbdh;->g()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 223
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lbdh;->a(Ljava/lang/String;)V

    .line 225
    :cond_0
    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227
    if-nez v0, :cond_6

    .line 230
    iget-object v2, p2, Lbdh;->f:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p2, Lbdh;->f:Ljava/lang/String;

    iget-object v3, v1, Lbdh;->f:Ljava/lang/String;

    .line 231
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 232
    const/4 v0, 0x1

    .line 233
    iget-object v2, p2, Lbdh;->f:Ljava/lang/String;

    iput-object v2, v1, Lbdh;->f:Ljava/lang/String;

    .line 236
    :cond_1
    iget-object v2, p2, Lbdh;->e:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p2, Lbdh;->e:Ljava/lang/String;

    iget-object v3, v1, Lbdh;->e:Ljava/lang/String;

    .line 237
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 238
    const/4 v0, 0x1

    .line 239
    iget-object v2, p2, Lbdh;->e:Ljava/lang/String;

    iput-object v2, v1, Lbdh;->e:Ljava/lang/String;

    .line 242
    :cond_2
    iget-object v2, p2, Lbdh;->g:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p2, Lbdh;->g:Ljava/lang/String;

    iget-object v3, v1, Lbdh;->g:Ljava/lang/String;

    .line 243
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 244
    const/4 v0, 0x1

    .line 245
    iget-object v2, p2, Lbdh;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lbdh;->b(Ljava/lang/String;)V

    .line 248
    :cond_3
    iget-object v2, p2, Lbdh;->p:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p2, Lbdh;->p:Ljava/lang/String;

    iget-object v3, v1, Lbdh;->p:Ljava/lang/String;

    .line 249
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 250
    const/4 v0, 0x1

    .line 251
    iget-object v2, p2, Lbdh;->p:Ljava/lang/String;

    iput-object v2, v1, Lbdh;->p:Ljava/lang/String;

    .line 254
    :cond_4
    iget-object v2, p2, Lbdh;->h:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p2, Lbdh;->h:Ljava/lang/String;

    iget-object v3, v1, Lbdh;->h:Ljava/lang/String;

    .line 255
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 256
    const/4 v0, 0x1

    .line 257
    iget-object v2, p2, Lbdh;->h:Ljava/lang/String;

    iput-object v2, v1, Lbdh;->h:Ljava/lang/String;

    .line 262
    :cond_5
    iget-object v2, p2, Lbdh;->i:Ljava/lang/Boolean;

    if-eqz v2, :cond_6

    iget-object v2, p2, Lbdh;->i:Ljava/lang/Boolean;

    iget-object v3, v1, Lbdh;->i:Ljava/lang/Boolean;

    if-eq v2, v3, :cond_6

    .line 263
    const/4 v0, 0x1

    .line 264
    iget-object v2, p2, Lbdh;->i:Ljava/lang/Boolean;

    iput-object v2, v1, Lbdh;->i:Ljava/lang/Boolean;

    .line 268
    :cond_6
    :goto_4
    return v0

    .line 202
    :cond_7
    :try_start_1
    iget-object v3, v0, Laam;->a:Lbdh;

    iget-object v3, v3, Lbdh;->d:Ljava/lang/String;

    if-eqz v3, :cond_b

    .line 203
    iget-object v3, v0, Laam;->a:Lbdh;

    iget-object v3, v3, Lbdh;->d:Ljava/lang/String;

    iget-object v4, p2, Lbdh;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 204
    iget-object v0, v0, Laam;->a:Lbdh;

    goto/16 :goto_0

    .line 209
    :cond_8
    iget-object v0, p2, Lbdh;->b:Lbdk;

    iget-object v0, v0, Lbdk;->a:Ljava/lang/String;

    goto/16 :goto_1

    :cond_9
    iget-object v1, p2, Lbdh;->b:Lbdk;

    iget-object v1, v1, Lbdk;->b:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 225
    :catchall_0
    move-exception v0

    monitor-exit v10

    throw v0

    :cond_a
    move-object v11, v0

    move v0, v1

    move-object v1, v11

    goto/16 :goto_3

    :cond_b
    move-object v0, v2

    goto/16 :goto_0

    :cond_c
    move v0, v1

    goto :goto_4
.end method
