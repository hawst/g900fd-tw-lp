.class public abstract Lbux;
.super Landroid/preference/PreferenceActivity;
.source "PG"

# interfaces
.implements Lbtz;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;)Landroid/preference/Preference;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lbux;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    return-object v0
.end method

.method public a()Landroid/preference/PreferenceScreen;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lbux;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lbux;->addPreferencesFromResource(I)V

    .line 27
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lbux;->startActivity(Landroid/content/Intent;)V

    .line 52
    return-void
.end method

.method public a(Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 46
    invoke-virtual {p0, p1, p2}, Lbux;->startActivityForResult(Landroid/content/Intent;I)V

    .line 47
    return-void
.end method

.method public a(Landroid/preference/Preference;)V
    .locals 1

    .prologue
    .line 56
    if-eqz p1, :cond_0

    .line 57
    invoke-virtual {p0}, Lbux;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 59
    :cond_0
    return-void
.end method

.method public a(Landroid/preference/PreferenceScreen;)V
    .locals 0

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lbux;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 42
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lbux;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    .line 22
    return-void
.end method

.method public b()Landroid/preference/PreferenceManager;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lbux;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    return-object v0
.end method

.method public c()Landroid/app/Activity;
    .locals 0

    .prologue
    .line 68
    return-object p0
.end method
