.class public final Ldnf;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldnf;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1574
    const/4 v0, 0x0

    new-array v0, v0, [Ldnf;

    sput-object v0, Ldnf;->a:[Ldnf;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1575
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1606
    const/4 v0, 0x0

    .line 1607
    iget-object v1, p0, Ldnf;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1608
    const/4 v0, 0x1

    iget-object v1, p0, Ldnf;->b:Ljava/lang/Integer;

    .line 1609
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1611
    :cond_0
    iget-object v1, p0, Ldnf;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1612
    const/4 v1, 0x2

    iget-object v2, p0, Ldnf;->c:Ljava/lang/String;

    .line 1613
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1615
    :cond_1
    iget-object v1, p0, Ldnf;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1616
    const/4 v1, 0x3

    iget-object v2, p0, Ldnf;->d:Ljava/lang/String;

    .line 1617
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1619
    :cond_2
    iget-object v1, p0, Ldnf;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1620
    const/4 v1, 0x5

    iget-object v2, p0, Ldnf;->e:Ljava/lang/Integer;

    .line 1621
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1623
    :cond_3
    iget-object v1, p0, Ldnf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1624
    iput v0, p0, Ldnf;->cachedSize:I

    .line 1625
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1571
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldnf;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldnf;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldnf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldnf;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnf;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnf;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldnf;->e:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x28 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1588
    iget-object v0, p0, Ldnf;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1589
    const/4 v0, 0x1

    iget-object v1, p0, Ldnf;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1591
    :cond_0
    iget-object v0, p0, Ldnf;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1592
    const/4 v0, 0x2

    iget-object v1, p0, Ldnf;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1594
    :cond_1
    iget-object v0, p0, Ldnf;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1595
    const/4 v0, 0x3

    iget-object v1, p0, Ldnf;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1597
    :cond_2
    iget-object v0, p0, Ldnf;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1598
    const/4 v0, 0x5

    iget-object v1, p0, Ldnf;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1600
    :cond_3
    iget-object v0, p0, Ldnf;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1602
    return-void
.end method
