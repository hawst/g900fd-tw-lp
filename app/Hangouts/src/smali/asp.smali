.class public final Lasp;
.super Lapo;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/hangout/ToastView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/hangouts/hangout/ToastView;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lasp;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    invoke-direct {p0}, Lapo;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/hangouts/hangout/ToastView;B)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lasp;-><init>(Lcom/google/android/apps/hangouts/hangout/ToastView;)V

    return-void
.end method


# virtual methods
.method public g()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lasp;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    sget v1, Lh;->dC:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/hangout/ToastView;->b(I)V

    .line 59
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lasp;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    sget v1, Lh;->dB:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/hangout/ToastView;->b(I)V

    .line 64
    return-void
.end method

.method public onCommonNotificationReceived(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 69
    if-nez p2, :cond_0

    .line 70
    iget-object v0, p0, Lasp;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    new-instance v1, Lasv;

    iget-object v2, p0, Lasp;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3, p4}, Lasv;-><init>(Lcom/google/android/apps/hangouts/hangout/ToastView;ILjava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/hangout/ToastView;->a(Lcom/google/android/apps/hangouts/hangout/ToastView;Lasw;)V

    .line 72
    :cond_0
    return-void
.end method

.method public onEndpointEvent(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;)V
    .locals 4

    .prologue
    .line 30
    instance-of v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/EnterEvent;

    if-eqz v0, :cond_1

    .line 31
    iget-object v0, p0, Lasp;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    check-cast p2, Lcom/google/android/libraries/hangouts/video/endpoint/EnterEvent;

    invoke-static {p1, p2}, Lcom/google/android/apps/hangouts/hangout/ToastView;->a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EnterEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lasp;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    new-instance v1, Lasm;

    iget-object v2, p0, Lasp;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    invoke-direct {v1, v2, p1}, Lasm;-><init>(Lcom/google/android/apps/hangouts/hangout/ToastView;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/hangout/ToastView;->a(Lcom/google/android/apps/hangouts/hangout/ToastView;Lasw;)V

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 34
    :cond_1
    instance-of v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/ExitEvent;

    if-eqz v0, :cond_2

    .line 40
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isConnecting()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfUser()Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    iget-object v0, p0, Lasp;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    new-instance v1, Lasn;

    iget-object v2, p0, Lasp;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    invoke-direct {v1, v2, p1}, Lasn;-><init>(Lcom/google/android/apps/hangouts/hangout/ToastView;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/hangout/ToastView;->a(Lcom/google/android/apps/hangouts/hangout/ToastView;Lasw;)V

    goto :goto_0

    .line 43
    :cond_2
    instance-of v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/AudioMuteEvent;

    if-eqz v0, :cond_3

    .line 44
    check-cast p2, Lcom/google/android/libraries/hangouts/video/endpoint/AudioMuteEvent;

    .line 45
    invoke-virtual {p2}, Lcom/google/android/libraries/hangouts/video/endpoint/AudioMuteEvent;->isRemoteAction()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lasp;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    new-instance v1, Lass;

    iget-object v2, p0, Lasp;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    .line 47
    invoke-virtual {p2}, Lcom/google/android/libraries/hangouts/video/endpoint/AudioMuteEvent;->getMuter()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v3

    invoke-direct {v1, v2, p1, v3}, Lass;-><init>(Lcom/google/android/apps/hangouts/hangout/ToastView;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    .line 46
    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/hangout/ToastView;->a(Lcom/google/android/apps/hangouts/hangout/ToastView;Lasw;)V

    goto :goto_0

    .line 49
    :cond_3
    instance-of v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/MediaBlockEvent;

    if-eqz v0, :cond_0

    .line 50
    check-cast p2, Lcom/google/android/libraries/hangouts/video/endpoint/MediaBlockEvent;

    .line 51
    iget-object v0, p0, Lasp;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    new-instance v1, Lasq;

    iget-object v2, p0, Lasp;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    .line 52
    invoke-virtual {p2}, Lcom/google/android/libraries/hangouts/video/endpoint/MediaBlockEvent;->getBlocker()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v3

    invoke-direct {v1, v2, p1, v3}, Lasq;-><init>(Lcom/google/android/apps/hangouts/hangout/ToastView;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    .line 51
    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/hangout/ToastView;->a(Lcom/google/android/apps/hangouts/hangout/ToastView;Lasw;)V

    goto :goto_0
.end method

.method public onPresenterUpdate(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 4

    .prologue
    .line 76
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    :goto_0
    return-void

    .line 81
    :cond_0
    if-eqz p1, :cond_1

    .line 82
    iget-object v0, p0, Lasp;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    new-instance v1, Last;

    iget-object v2, p0, Lasp;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    invoke-direct {v1, v2, p1, p2}, Last;-><init>(Lcom/google/android/apps/hangouts/hangout/ToastView;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/hangout/ToastView;->a(Lcom/google/android/apps/hangouts/hangout/ToastView;Lasw;)V

    goto :goto_0

    .line 86
    :cond_1
    iget-object v0, p0, Lasp;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/ToastView;->a(Lcom/google/android/apps/hangouts/hangout/ToastView;)Lapk;

    move-result-object v0

    invoke-virtual {v0}, Lapk;->d()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getLastPresenter()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v0

    .line 87
    iget-object v1, p0, Lasp;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    new-instance v2, Lasu;

    iget-object v3, p0, Lasp;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    invoke-direct {v2, v3, v0, p2}, Lasu;-><init>(Lcom/google/android/apps/hangouts/hangout/ToastView;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/hangout/ToastView;->a(Lcom/google/android/apps/hangouts/hangout/ToastView;Lasw;)V

    goto :goto_0
.end method
