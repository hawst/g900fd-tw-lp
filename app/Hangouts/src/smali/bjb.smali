.class public final Lbjb;
.super Lbiq;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ldpz;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1248
    invoke-direct {p0}, Lbiq;-><init>()V

    .line 1249
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lbjb;->b:Ljava/util/List;

    .line 1250
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lbjb;->c:Ljava/util/List;

    .line 1251
    iput-object p2, p0, Lbjb;->d:Ljava/lang/String;

    .line 1253
    iget-object v2, p1, Ldpz;->b:[Ldui;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 1254
    iget-object v5, p0, Lbjb;->b:Ljava/util/List;

    iget-object v4, v4, Ldui;->c:Ljava/lang/String;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1253
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1258
    :cond_0
    iget-object v1, p1, Ldpz;->c:[Ldui;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1259
    iget-object v4, p0, Lbjb;->c:Ljava/util/List;

    iget-object v3, v3, Ldui;->c:Ljava/lang/String;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1258
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1261
    :cond_1
    return-void
.end method
