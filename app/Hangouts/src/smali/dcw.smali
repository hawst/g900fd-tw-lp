.class public final Ldcw;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldcw;


# instance fields
.field public b:Letl;

.field public c:Letn;

.field public d:Ljava/lang/String;

.field public e:Leyp;

.field public f:Ldcb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    new-array v0, v0, [Ldcw;

    sput-object v0, Ldcw;->a:[Ldcw;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 113
    invoke-direct {p0}, Lepn;-><init>()V

    .line 116
    iput-object v0, p0, Ldcw;->b:Letl;

    .line 119
    iput-object v0, p0, Ldcw;->c:Letn;

    .line 124
    iput-object v0, p0, Ldcw;->e:Leyp;

    .line 127
    iput-object v0, p0, Ldcw;->f:Ldcb;

    .line 113
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 153
    const/4 v0, 0x0

    .line 154
    iget-object v1, p0, Ldcw;->b:Letl;

    if-eqz v1, :cond_0

    .line 155
    const/4 v0, 0x1

    iget-object v1, p0, Ldcw;->b:Letl;

    .line 156
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 158
    :cond_0
    iget-object v1, p0, Ldcw;->c:Letn;

    if-eqz v1, :cond_1

    .line 159
    const/4 v1, 0x2

    iget-object v2, p0, Ldcw;->c:Letn;

    .line 160
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 162
    :cond_1
    iget-object v1, p0, Ldcw;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 163
    const/4 v1, 0x3

    iget-object v2, p0, Ldcw;->d:Ljava/lang/String;

    .line 164
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 166
    :cond_2
    iget-object v1, p0, Ldcw;->e:Leyp;

    if-eqz v1, :cond_3

    .line 167
    const/4 v1, 0x4

    iget-object v2, p0, Ldcw;->e:Leyp;

    .line 168
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    :cond_3
    iget-object v1, p0, Ldcw;->f:Ldcb;

    if-eqz v1, :cond_4

    .line 171
    const/4 v1, 0x5

    iget-object v2, p0, Ldcw;->f:Ldcb;

    .line 172
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    :cond_4
    iget-object v1, p0, Ldcw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 175
    iput v0, p0, Ldcw;->cachedSize:I

    .line 176
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 109
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldcw;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldcw;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldcw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldcw;->b:Letl;

    if-nez v0, :cond_2

    new-instance v0, Letl;

    invoke-direct {v0}, Letl;-><init>()V

    iput-object v0, p0, Ldcw;->b:Letl;

    :cond_2
    iget-object v0, p0, Ldcw;->b:Letl;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldcw;->c:Letn;

    if-nez v0, :cond_3

    new-instance v0, Letn;

    invoke-direct {v0}, Letn;-><init>()V

    iput-object v0, p0, Ldcw;->c:Letn;

    :cond_3
    iget-object v0, p0, Ldcw;->c:Letn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldcw;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldcw;->e:Leyp;

    if-nez v0, :cond_4

    new-instance v0, Leyp;

    invoke-direct {v0}, Leyp;-><init>()V

    iput-object v0, p0, Ldcw;->e:Leyp;

    :cond_4
    iget-object v0, p0, Ldcw;->e:Leyp;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldcw;->f:Ldcb;

    if-nez v0, :cond_5

    new-instance v0, Ldcb;

    invoke-direct {v0}, Ldcb;-><init>()V

    iput-object v0, p0, Ldcw;->f:Ldcb;

    :cond_5
    iget-object v0, p0, Ldcw;->f:Ldcb;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Ldcw;->b:Letl;

    if-eqz v0, :cond_0

    .line 133
    const/4 v0, 0x1

    iget-object v1, p0, Ldcw;->b:Letl;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 135
    :cond_0
    iget-object v0, p0, Ldcw;->c:Letn;

    if-eqz v0, :cond_1

    .line 136
    const/4 v0, 0x2

    iget-object v1, p0, Ldcw;->c:Letn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 138
    :cond_1
    iget-object v0, p0, Ldcw;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 139
    const/4 v0, 0x3

    iget-object v1, p0, Ldcw;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 141
    :cond_2
    iget-object v0, p0, Ldcw;->e:Leyp;

    if-eqz v0, :cond_3

    .line 142
    const/4 v0, 0x4

    iget-object v1, p0, Ldcw;->e:Leyp;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 144
    :cond_3
    iget-object v0, p0, Ldcw;->f:Ldcb;

    if-eqz v0, :cond_4

    .line 145
    const/4 v0, 0x5

    iget-object v1, p0, Ldcw;->f:Ldcb;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 147
    :cond_4
    iget-object v0, p0, Ldcw;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 149
    return-void
.end method
