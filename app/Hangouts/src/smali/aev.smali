.class public final Laev;
.super Ls;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private Y:Laea;

.field private Z:Z

.field private aa:Laen;

.field private ab:Laei;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ls;-><init>()V

    .line 64
    return-void
.end method

.method public static a(Laea;Z)Laev;
    .locals 3

    .prologue
    .line 51
    new-instance v0, Laev;

    invoke-direct {v0}, Laev;-><init>()V

    .line 52
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 53
    const-string v2, "contact_details"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 54
    const-string v2, "phone_only"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 55
    invoke-virtual {v0, v1}, Laev;->setArguments(Landroid/os/Bundle;)V

    .line 56
    return-object v0
.end method


# virtual methods
.method public f()Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 71
    invoke-virtual {p0}, Laev;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "contact_details"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Laea;

    iput-object v0, p0, Laev;->Y:Laea;

    .line 72
    invoke-virtual {p0}, Laev;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "phone_only"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Laev;->Z:Z

    .line 77
    invoke-virtual {p0}, Laev;->getTargetFragment()Lt;

    move-result-object v0

    check-cast v0, Laeo;

    .line 78
    if-eqz v0, :cond_0

    .line 79
    invoke-interface {v0}, Laeo;->c()Laen;

    move-result-object v0

    iput-object v0, p0, Laev;->aa:Laen;

    .line 82
    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Laev;->getActivity()Ly;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 83
    invoke-virtual {p0}, Laev;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 84
    sget v2, Lf;->eA:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 86
    sget v0, Lg;->aC:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 89
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 90
    iget-boolean v4, p0, Laev;->Z:Z

    if-nez v4, :cond_1

    iget-object v4, p0, Laev;->Y:Laea;

    invoke-virtual {v4}, Laea;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 91
    new-instance v4, Laeg;

    iget-object v5, p0, Laev;->Y:Laea;

    invoke-virtual {v5}, Laea;->c()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Laeg;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    :cond_1
    iget-object v4, p0, Laev;->Y:Laea;

    invoke-virtual {v4}, Laea;->j()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 95
    new-instance v4, Laei;

    invoke-virtual {p0}, Laev;->getActivity()Ly;

    move-result-object v5

    iget-object v6, p0, Laev;->Y:Laea;

    invoke-direct {v4, v5, v3, v6, v7}, Laei;-><init>(Landroid/content/Context;Ljava/util/List;Laea;Z)V

    iput-object v4, p0, Laev;->ab:Laei;

    .line 97
    iget-object v3, p0, Laev;->ab:Laei;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 98
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 100
    sget v0, Lh;->iv:I

    new-array v3, v7, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Laev;->Y:Laea;

    invoke-virtual {v5}, Laea;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v0, v3}, Laev;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 101
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 103
    invoke-virtual {p0}, Laev;->d()V

    .line 104
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Laev;->ab:Laei;

    invoke-virtual {v0, p3}, Laei;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laee;

    .line 113
    invoke-virtual {p0}, Laev;->b()Landroid/app/Dialog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 114
    invoke-virtual {p0}, Laev;->b()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 117
    :cond_0
    iget-object v1, p0, Laev;->aa:Laen;

    if-eqz v1, :cond_1

    .line 118
    instance-of v1, v0, Laeh;

    if-eqz v1, :cond_2

    .line 119
    iget-object v2, p0, Laev;->aa:Laen;

    move-object v1, v0

    check-cast v1, Laeh;

    invoke-virtual {v2, v1}, Laen;->a(Laeh;)V

    .line 126
    :cond_1
    :goto_0
    iget-object v1, p0, Laev;->Y:Laea;

    invoke-virtual {v1, v0}, Laea;->a(Laee;)V

    .line 127
    return-void

    .line 120
    :cond_2
    instance-of v1, v0, Laeg;

    if-eqz v1, :cond_1

    .line 121
    iget-object v2, p0, Laev;->aa:Laen;

    move-object v1, v0

    check-cast v1, Laeg;

    invoke-virtual {v2, v1}, Laen;->a(Laee;)V

    goto :goto_0
.end method
