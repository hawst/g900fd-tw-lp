.class public final Lcbf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)V
    .locals 0

    .prologue
    .line 522
    iput-object p1, p0, Lcbf;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 525
    invoke-static {}, Lccc;->a()Lccc;

    move-result-object v0

    iget-object v1, p0, Lcbf;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->g(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lccc;->a(Landroid/text/Spannable;Landroid/widget/TextView;)V

    .line 526
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 531
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 545
    iget-object v0, p0, Lcbf;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcbf;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->h(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lcbm;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 546
    iget-object v0, p0, Lcbf;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->h(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lcbm;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcbm;->a(Ljava/lang/CharSequence;III)V

    .line 547
    iget-object v0, p0, Lcbf;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcbf;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->h(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lcbm;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 548
    return-void
.end method
