.class public final Lcao;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/content/Context;

.field b:I

.field private c:Lcas;

.field private final d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Landroid/media/AudioManager;

.field private i:Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

.field private j:Z

.field private k:I

.field private l:I

.field private m:Landroid/content/BroadcastReceiver;

.field private n:Landroid/os/PowerManager;

.field private o:Z

.field private p:Landroid/os/PowerManager$WakeLock;

.field private final q:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/views/AudioAttachmentView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    invoke-static {p1}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->a(Lcom/google/android/apps/hangouts/views/AudioAttachmentView;)Lcas;

    move-result-object v0

    iput-object v0, p0, Lcao;->c:Lcas;

    .line 170
    invoke-virtual {p1}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcao;->a:Landroid/content/Context;

    .line 171
    iput-object p2, p0, Lcao;->d:Ljava/lang/String;

    .line 172
    iput-object p3, p0, Lcao;->g:Ljava/lang/String;

    .line 173
    iget-object v0, p0, Lcao;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "power"

    .line 174
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcao;->n:Landroid/os/PowerManager;

    .line 175
    iget-object v0, p0, Lcao;->n:Landroid/os/PowerManager;

    const v2, 0x2000000a

    const-string v3, "Babel"

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcao;->p:Landroid/os/PowerManager$WakeLock;

    .line 177
    iget-object v0, p0, Lcao;->a:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcao;->h:Landroid/media/AudioManager;

    .line 178
    iget-object v0, p0, Lcao;->h:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcao;->j:Z

    .line 179
    iput v1, p0, Lcao;->b:I

    .line 181
    new-instance v0, Lcap;

    invoke-direct {v0, p0}, Lcap;-><init>(Lcao;)V

    iput-object v0, p0, Lcao;->q:Ljava/lang/Runnable;

    .line 194
    return-void

    :cond_0
    move v0, v1

    .line 178
    goto :goto_0
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 209
    iget v0, p0, Lcao;->b:I

    if-eq v0, p1, :cond_1

    .line 210
    invoke-static {}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    const-string v0, "setState"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "new state:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcao;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcao;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    :cond_0
    iput p1, p0, Lcao;->b:I

    .line 214
    iget-object v0, p0, Lcao;->i:Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

    if-eqz v0, :cond_1

    .line 215
    iget-object v0, p0, Lcao;->i:Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->b(Lcom/google/android/apps/hangouts/views/AudioAttachmentView;)V

    .line 218
    :cond_1
    return-void
.end method

.method private static c(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 548
    packed-switch p0, :pswitch_data_0

    .line 557
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UNKNOWN:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 549
    :pswitch_0
    const-string v0, "IDLE"

    goto :goto_0

    .line 550
    :pswitch_1
    const-string v0, "PREFETCH"

    goto :goto_0

    .line 551
    :pswitch_2
    const-string v0, "FETCH_FOR_PLAY"

    goto :goto_0

    .line 552
    :pswitch_3
    const-string v0, "PREPARING"

    goto :goto_0

    .line 553
    :pswitch_4
    const-string v0, "READY"

    goto :goto_0

    .line 554
    :pswitch_5
    const-string v0, "PLAYING"

    goto :goto_0

    .line 555
    :pswitch_6
    const-string v0, "PAUSED"

    goto :goto_0

    .line 548
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private j()V
    .locals 1

    .prologue
    .line 258
    iget-boolean v0, p0, Lcao;->o:Z

    if-nez v0, :cond_0

    .line 259
    iget-object v0, p0, Lcao;->p:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 260
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcao;->o:Z

    .line 262
    :cond_0
    return-void
.end method

.method private k()V
    .locals 1

    .prologue
    .line 265
    iget-boolean v0, p0, Lcao;->o:Z

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lcao;->p:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 267
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcao;->o:Z

    .line 269
    :cond_0
    return-void
.end method

.method private l()V
    .locals 3

    .prologue
    .line 279
    const-string v0, "sendPrepare"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcao;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcao;->a(I)Landroid/content/Intent;

    move-result-object v0

    .line 281
    const-string v1, "audio_stream_url"

    iget-object v2, p0, Lcao;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 282
    iget-object v1, p0, Lcao;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 283
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    .line 286
    const-string v0, "sendPlay"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcao;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    iget-object v0, p0, Lcao;->a:Landroid/content/Context;

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcao;->a(I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 288
    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    .line 291
    const-string v0, "sendPause"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcao;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    iget-object v0, p0, Lcao;->a:Landroid/content/Context;

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcao;->a(I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 293
    return-void
.end method

.method private o()V
    .locals 4

    .prologue
    .line 434
    const-string v0, "activateSelf"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcao;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    iget-object v0, p0, Lcao;->c:Lcas;

    invoke-interface {v0}, Lcas;->a()Lcao;

    move-result-object v0

    .line 436
    if-eqz v0, :cond_1

    .line 439
    invoke-virtual {v0}, Lcao;->b()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    .line 440
    invoke-direct {v0}, Lcao;->n()V

    .line 443
    :cond_0
    invoke-direct {v0}, Lcao;->p()V

    .line 446
    :cond_1
    iget-object v0, p0, Lcao;->c:Lcas;

    invoke-interface {v0, p0}, Lcas;->a(Lcao;)V

    .line 448
    iget-object v0, p0, Lcao;->h:Landroid/media/AudioManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    .line 449
    iget-object v0, p0, Lcao;->h:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcao;->a(Z)V

    .line 454
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcao;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+playId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcao;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcao;->e:Ljava/lang/String;

    .line 455
    iget v0, p0, Lcao;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcao;->f:I

    .line 456
    iget-object v0, p0, Lcao;->m:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_2

    new-instance v0, Lcaq;

    invoke-direct {v0, p0}, Lcaq;-><init>(Lcao;)V

    iput-object v0, p0, Lcao;->m:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "ready_to_play"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "play_started"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "play_paused"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "play_stopped"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "current_position"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcao;->a:Landroid/content/Context;

    iget-object v2, p0, Lcao;->m:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 457
    :cond_2
    invoke-direct {p0}, Lcao;->j()V

    .line 458
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcao;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iget-object v1, p0, Lcao;->q:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 459
    return-void

    .line 449
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 462
    const-string v0, "resetSelf"

    const-string v2, ""

    invoke-virtual {p0, v0, v2}, Lcao;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    iget-object v0, p0, Lcao;->m:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcao;->a:Landroid/content/Context;

    iget-object v2, p0, Lcao;->m:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v3, p0, Lcao;->m:Landroid/content/BroadcastReceiver;

    .line 464
    :cond_0
    iget-object v0, p0, Lcao;->c:Lcas;

    invoke-interface {v0}, Lcas;->a()Lcao;

    move-result-object v0

    if-ne v0, p0, :cond_1

    .line 465
    iget-object v0, p0, Lcao;->c:Lcas;

    invoke-interface {v0, v3}, Lcas;->a(Lcao;)V

    .line 467
    :cond_1
    iget-object v0, p0, Lcao;->h:Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    .line 468
    iget-object v0, p0, Lcao;->h:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcao;->a(Z)V

    .line 469
    invoke-direct {p0}, Lcao;->k()V

    .line 470
    invoke-direct {p0, v1}, Lcao;->b(I)V

    .line 471
    return-void

    :cond_2
    move v0, v1

    .line 468
    goto :goto_0
.end method


# virtual methods
.method public a(I)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 272
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcao;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/apps/hangouts/service/AudioPlayerService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 273
    const-string v1, "op"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 274
    const-string v1, "play_id"

    iget-object v2, p0, Lcao;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 275
    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcao;->d:Ljava/lang/String;

    return-object v0
.end method

.method public a(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x6

    const/4 v0, 0x0

    .line 364
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 367
    const-string v2, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 368
    const-string v1, "state"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 369
    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, v0}, Lcao;->a(Z)V

    .line 391
    :cond_1
    :goto_0
    return-void

    .line 375
    :cond_2
    iget-object v2, p0, Lcao;->e:Ljava/lang/String;

    const-string v3, "play_id"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 378
    const-string v2, "ready_to_play"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 379
    const-string v1, "onReadyToPlay"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lcao;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "duration_in_milliseconds"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcao;->k:I

    iput v0, p0, Lcao;->l:I

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcao;->b(I)V

    invoke-virtual {p0}, Lcao;->h()V

    goto :goto_0

    .line 380
    :cond_3
    const-string v2, "play_started"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 381
    const-string v0, "onPlayStarted"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcao;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "sendRegister"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcao;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcao;->a:Landroid/content/Context;

    invoke-virtual {p0, v4}, Lcao;->a(I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcao;->b(I)V

    iget-object v0, p0, Lcao;->c:Lcas;

    invoke-interface {v0}, Lcas;->b()V

    invoke-direct {p0}, Lcao;->j()V

    goto :goto_0

    .line 382
    :cond_4
    const-string v2, "play_paused"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 383
    const-string v0, "onPlayPaused"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcao;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "sendUnregister"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcao;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcao;->a:Landroid/content/Context;

    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Lcao;->a(I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-direct {p0, v4}, Lcao;->b(I)V

    iget-object v0, p0, Lcao;->c:Lcas;

    invoke-interface {v0}, Lcas;->b()V

    invoke-direct {p0}, Lcao;->k()V

    goto/16 :goto_0

    .line 384
    :cond_5
    const-string v2, "play_stopped"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 385
    const-string v0, "onPlayStopped"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcao;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcao;->p()V

    goto/16 :goto_0

    .line 386
    :cond_6
    const-string v2, "current_position"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 387
    const-string v1, "position_in_milliseconds"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcao;->l:I

    const-string v1, "duration_in_milliseconds"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcao;->k:I

    iget-object v0, p0, Lcao;->i:Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcao;->i:Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->b(Lcom/google/android/apps/hangouts/views/AudioAttachmentView;)V

    goto/16 :goto_0

    .line 389
    :cond_7
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Received unrecognized broadcast action: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public a(Lcar;)V
    .locals 2

    .prologue
    .line 221
    const-string v0, "preloadAudioDataSourceIfNecessary"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcao;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Lcao;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 223
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcao;->b(I)V

    .line 224
    invoke-interface {p1}, Lcar;->a()V

    .line 226
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/hangouts/views/AudioAttachmentView;)V
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lcao;->i:Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

    .line 202
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 522
    invoke-static {}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 523
    const-string v0, "setAudioDataSource"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "new audioStreamUrl is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " this.audioStreamUrl is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcao;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcao;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 545
    :goto_0
    return-void

    .line 529
    :cond_1
    iget v0, p0, Lcao;->b:I

    packed-switch v0, :pswitch_data_0

    .line 543
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected state in setAudioDataSource: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcao;->b:I

    invoke-static {v2}, Lcao;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 531
    :pswitch_0
    iput-object p1, p0, Lcao;->g:Ljava/lang/String;

    goto :goto_0

    .line 534
    :pswitch_1
    iput-object p1, p0, Lcao;->g:Ljava/lang/String;

    .line 535
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcao;->b(I)V

    goto :goto_0

    .line 538
    :pswitch_2
    iput-object p1, p0, Lcao;->g:Ljava/lang/String;

    .line 539
    invoke-direct {p0}, Lcao;->l()V

    .line 540
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcao;->b(I)V

    goto :goto_0

    .line 529
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 562
    invoke-static {}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 563
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " playId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcao;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcao;->b:I

    invoke-static {v2}, Lcao;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcao;->h:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 230
    iput-boolean p1, p0, Lcao;->j:Z

    .line 231
    iget-object v0, p0, Lcao;->i:Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcao;->i:Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->b(Lcom/google/android/apps/hangouts/views/AudioAttachmentView;)V

    .line 234
    :cond_0
    iget-object v0, p0, Lcao;->c:Lcas;

    invoke-interface {v0}, Lcas;->b()V

    .line 235
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 205
    iget v0, p0, Lcao;->b:I

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 237
    iget-boolean v0, p0, Lcao;->j:Z

    return v0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 246
    iget-boolean v0, p0, Lcao;->j:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcao;->h:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcao;->b()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 250
    iget v0, p0, Lcao;->k:I

    return v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 254
    iget v0, p0, Lcao;->l:I

    return v0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 322
    const-string v0, "stopPlayback"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcao;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    invoke-direct {p0}, Lcao;->p()V

    .line 330
    const-string v0, "sendStop"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcao;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcao;->a:Landroid/content/Context;

    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lcao;->a(I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 331
    return-void
.end method

.method public h()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 474
    const-string v0, "playAudio"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcao;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    iget v0, p0, Lcao;->b:I

    packed-switch v0, :pswitch_data_0

    .line 510
    :pswitch_0
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected state in playAudio: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcao;->b:I

    invoke-static {v2}, Lcao;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    :goto_0
    :pswitch_1
    return-void

    .line 477
    :pswitch_2
    iget-object v0, p0, Lcao;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 478
    invoke-direct {p0}, Lcao;->o()V

    .line 479
    invoke-direct {p0}, Lcao;->l()V

    .line 480
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcao;->b(I)V

    goto :goto_0

    .line 482
    :cond_0
    iget-object v0, p0, Lcao;->i:Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcao;->i:Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->c(Lcom/google/android/apps/hangouts/views/AudioAttachmentView;)Lcar;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 483
    invoke-direct {p0}, Lcao;->o()V

    .line 484
    iget-object v0, p0, Lcao;->i:Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->c(Lcom/google/android/apps/hangouts/views/AudioAttachmentView;)Lcar;

    move-result-object v0

    invoke-interface {v0}, Lcar;->a()V

    .line 485
    invoke-direct {p0, v2}, Lcao;->b(I)V

    goto :goto_0

    .line 487
    :cond_1
    const-string v0, "Babel"

    const-string v1, "No audioUrl, and no audioUrlFetcher."

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcao;->b(I)V

    goto :goto_0

    .line 493
    :pswitch_3
    invoke-direct {p0}, Lcao;->o()V

    .line 495
    invoke-direct {p0, v2}, Lcao;->b(I)V

    goto :goto_0

    .line 504
    :pswitch_4
    invoke-direct {p0}, Lcao;->m()V

    goto :goto_0

    .line 507
    :pswitch_5
    invoke-direct {p0}, Lcao;->m()V

    goto :goto_0

    .line 475
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public i()V
    .locals 2

    .prologue
    .line 515
    const-string v0, "pauseAudio"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcao;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    iget v0, p0, Lcao;->b:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 517
    invoke-direct {p0}, Lcao;->n()V

    .line 519
    :cond_0
    return-void
.end method
