.class final Lcwn;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lcwl;

.field final b:Landroid/os/ParcelFileDescriptor;

.field final synthetic c:Lcwk;


# direct methods
.method constructor <init>(Lcwk;Lcwl;Landroid/os/ParcelFileDescriptor;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcwn;->c:Lcwk;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 217
    iput-object p2, p0, Lcwn;->a:Lcwl;

    .line 218
    iput-object p3, p0, Lcwn;->b:Landroid/os/ParcelFileDescriptor;

    .line 219
    return-void
.end method

.method private varargs a()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 222
    :try_start_0
    iget-object v0, p0, Lcwn;->b:Landroid/os/ParcelFileDescriptor;

    invoke-static {v0}, Lf;->a(Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 225
    if-nez v0, :cond_1

    .line 226
    iget-object v0, p0, Lcwn;->c:Lcwk;

    iget-object v0, v0, Lcwk;->a:Landroid/content/Context;

    invoke-static {v0}, Lcwk;->a(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    .line 230
    :goto_0
    iget-object v0, p0, Lcwn;->c:Lcwk;

    invoke-static {v0}, Lcwk;->a(Lcwk;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    iget-object v2, p0, Lcwn;->a:Lcwl;

    iget-object v2, v2, Lcwl;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    iget-object v0, p0, Lcwn;->b:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_0

    .line 234
    :try_start_1
    iget-object v0, p0, Lcwn;->b:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 240
    :cond_0
    :goto_1
    return-object v1

    .line 228
    :cond_1
    :try_start_2
    invoke-static {v0}, Lf;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 235
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    goto :goto_1

    .line 232
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcwn;->b:Landroid/os/ParcelFileDescriptor;

    if-eqz v1, :cond_2

    .line 234
    :try_start_3
    iget-object v1, p0, Lcwn;->b:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 237
    :cond_2
    :goto_2
    throw v0

    .line 235
    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    goto :goto_2
.end method


# virtual methods
.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 212
    invoke-direct {p0}, Lcwn;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 212
    check-cast p1, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcwn;->a:Lcwl;

    iget-object v0, v0, Lcwl;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcwn;->a:Lcwl;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcwn;->c:Lcwk;

    iget-object v1, p0, Lcwn;->a:Lcwl;

    iget-object v1, v1, Lcwl;->a:Landroid/widget/ImageView;

    invoke-static {v0, v1, p1}, Lcwk;->a(Lcwk;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method
