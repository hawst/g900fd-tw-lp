.class public final Laer;
.super Ls;
.source "PG"


# instance fields
.field private Y:Laea;

.field private Z:Laen;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ls;-><init>()V

    .line 130
    return-void
.end method

.method public static a(Laea;)Laer;
    .locals 3

    .prologue
    .line 52
    new-instance v0, Laer;

    invoke-direct {v0}, Laer;-><init>()V

    .line 55
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 56
    const-string v2, "contact_details"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 57
    invoke-virtual {v0, v1}, Laer;->setArguments(Landroid/os/Bundle;)V

    .line 58
    return-object v0
.end method

.method static synthetic a(Laer;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Laer;->q()V

    return-void
.end method

.method static synthetic a(Laer;Laee;)V
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Laer;->b()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Laer;->a()V

    :cond_0
    iget-object v0, p0, Laer;->Z:Laen;

    if-eqz v0, :cond_1

    iget-object v0, p0, Laer;->Z:Laen;

    invoke-virtual {v0, p1}, Laen;->a(Laee;)V

    :cond_1
    return-void
.end method

.method private q()V
    .locals 1

    .prologue
    .line 108
    invoke-virtual {p0}, Laer;->b()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 109
    invoke-virtual {p0}, Laer;->a()V

    .line 112
    :cond_0
    iget-object v0, p0, Laer;->Z:Laen;

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Laer;->Z:Laen;

    invoke-virtual {v0}, Laen;->c()V

    .line 115
    :cond_1
    return-void
.end method


# virtual methods
.method public f()Landroid/app/Dialog;
    .locals 7

    .prologue
    .line 67
    invoke-virtual {p0}, Laer;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "contact_details"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Laea;

    iput-object v0, p0, Laer;->Y:Laea;

    .line 72
    invoke-virtual {p0}, Laer;->getTargetFragment()Lt;

    move-result-object v0

    check-cast v0, Laeo;

    .line 73
    if-eqz v0, :cond_0

    .line 74
    invoke-interface {v0}, Laeo;->c()Laen;

    move-result-object v0

    iput-object v0, p0, Laer;->Z:Laen;

    .line 77
    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Laer;->getActivity()Ly;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 78
    invoke-virtual {p0}, Laer;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 79
    sget v2, Lf;->eC:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 81
    sget v0, Lg;->aE:I

    .line 82
    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 83
    new-instance v3, Laet;

    .line 84
    invoke-virtual {p0}, Laer;->getActivity()Ly;

    move-result-object v4

    iget-object v5, p0, Laer;->Y:Laea;

    invoke-virtual {v5}, Laea;->g()Ljava/util/ArrayList;

    move-result-object v5

    iget-object v6, p0, Laer;->Z:Laen;

    .line 85
    invoke-virtual {v6}, Laen;->a()Lyj;

    move-result-object v6

    invoke-direct {v3, p0, v4, v5, v6}, Laet;-><init>(Laer;Landroid/content/Context;Ljava/util/List;Lyj;)V

    .line 87
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 89
    sget v0, Lh;->bb:I

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 90
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v2, 0x1040000

    .line 91
    invoke-virtual {p0, v2}, Laer;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Laes;

    invoke-direct {v3, p0}, Laes;-><init>(Laer;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 99
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Laer;->q()V

    .line 105
    return-void
.end method
