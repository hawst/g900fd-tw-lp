.class public final Lduv;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lduv;


# instance fields
.field public b:[Lduw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13533
    const/4 v0, 0x0

    new-array v0, v0, [Lduv;

    sput-object v0, Lduv;->a:[Lduv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13534
    invoke-direct {p0}, Lepn;-><init>()V

    .line 13537
    sget-object v0, Lduw;->a:[Lduw;

    iput-object v0, p0, Lduv;->b:[Lduw;

    .line 13534
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 13556
    iget-object v1, p0, Lduv;->b:[Lduw;

    if-eqz v1, :cond_1

    .line 13557
    iget-object v2, p0, Lduv;->b:[Lduw;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 13558
    if-eqz v4, :cond_0

    .line 13559
    const/4 v5, 0x1

    .line 13560
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 13557
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 13564
    :cond_1
    iget-object v1, p0, Lduv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13565
    iput v0, p0, Lduv;->cachedSize:I

    .line 13566
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 13530
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lduv;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lduv;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lduv;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lduv;->b:[Lduw;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lduw;

    iget-object v3, p0, Lduv;->b:[Lduw;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lduv;->b:[Lduw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lduv;->b:[Lduw;

    :goto_2
    iget-object v2, p0, Lduv;->b:[Lduw;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lduv;->b:[Lduw;

    new-instance v3, Lduw;

    invoke-direct {v3}, Lduw;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lduv;->b:[Lduw;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lduv;->b:[Lduw;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lduv;->b:[Lduw;

    new-instance v3, Lduw;

    invoke-direct {v3}, Lduw;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lduv;->b:[Lduw;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 13542
    iget-object v0, p0, Lduv;->b:[Lduw;

    if-eqz v0, :cond_1

    .line 13543
    iget-object v1, p0, Lduv;->b:[Lduw;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 13544
    if-eqz v3, :cond_0

    .line 13545
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 13543
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 13549
    :cond_1
    iget-object v0, p0, Lduv;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 13551
    return-void
.end method
