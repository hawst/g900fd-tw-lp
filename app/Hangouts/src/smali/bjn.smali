.class public final Lbjn;
.super Lbiq;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field public final b:J

.field public final c:Z

.field public final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ldvt;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1174
    invoke-direct {p0}, Lbiq;-><init>()V

    .line 1175
    iget-object v0, p1, Ldvt;->d:Ldrg;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbjn;->c:Z

    .line 1176
    iget-boolean v0, p0, Lbjn;->c:Z

    if-eqz v0, :cond_3

    .line 1177
    iget-object v0, p1, Ldvt;->d:Ldrg;

    iget-object v0, v0, Ldrg;->c:Ljava/lang/Long;

    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    iput-wide v0, p0, Lbjn;->b:J

    .line 1182
    :goto_1
    const-string v0, ""

    .line 1183
    iget-object v1, p1, Ldvt;->g:Ldvq;

    if-eqz v1, :cond_0

    .line 1184
    iget-object v1, p1, Ldvt;->g:Ldvq;

    .line 1185
    iget-object v2, v1, Ldvq;->b:Ldue;

    if-eqz v2, :cond_0

    iget-object v2, v1, Ldvq;->b:Ldue;

    iget-object v2, v2, Ldue;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1186
    iget-object v0, v1, Ldvq;->b:Ldue;

    iget-object v0, v0, Ldue;->b:Ljava/lang/String;

    .line 1189
    :cond_0
    iput-object v0, p0, Lbjn;->d:Ljava/lang/String;

    .line 1190
    sget-boolean v0, Lbiq;->a:Z

    if-eqz v0, :cond_1

    .line 1191
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "got SelfPresenceNotification "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lbjn;->c:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lbjn;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mood: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbjn;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1194
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 1175
    goto :goto_0

    .line 1180
    :cond_3
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbjn;->b:J

    goto :goto_1
.end method
