.class public final Lbqc;
.super Lbnj;
.source "PG"


# instance fields
.field private final a:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:I


# direct methods
.method public constructor <init>(Lyj;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lbnj;-><init>(Lyj;)V

    .line 17
    iput-object p2, p0, Lbqc;->a:Ljava/lang/String;

    .line 18
    iput-object p3, p0, Lbqc;->d:Ljava/lang/String;

    .line 19
    iput p4, p0, Lbqc;->e:I

    .line 20
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 24
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SetMessageFailedOperation: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbqc;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbqc;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lbqc;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    new-instance v1, Lyt;

    iget-object v0, p0, Lbqc;->b:Lyj;

    invoke-direct {v1, v0}, Lyt;-><init>(Lyj;)V

    .line 28
    invoke-virtual {v1}, Lyt;->a()V

    .line 30
    :try_start_0
    iget-object v0, p0, Lbqc;->a:Ljava/lang/String;

    iget-object v2, p0, Lbqc;->d:Ljava/lang/String;

    iget v3, p0, Lbqc;->e:I

    invoke-static {v1, v0, v2, v3}, Lyp;->a(Lyt;Ljava/lang/String;Ljava/lang/String;I)V

    .line 32
    iget-object v0, p0, Lbqc;->a:Ljava/lang/String;

    iget-object v2, p0, Lbqc;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;)Z

    .line 33
    invoke-virtual {v1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 35
    invoke-virtual {v1}, Lyt;->c()V

    .line 36
    return-void

    .line 35
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lyt;->c()V

    throw v0
.end method
