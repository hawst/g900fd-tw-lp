.class public final Ldwa;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldwa;


# instance fields
.field public b:Ldvm;

.field public c:[Ldpw;

.field public d:Ldqz;

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22951
    const/4 v0, 0x0

    new-array v0, v0, [Ldwa;

    sput-object v0, Ldwa;->a:[Ldwa;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22952
    invoke-direct {p0}, Lepn;-><init>()V

    .line 22955
    iput-object v1, p0, Ldwa;->b:Ldvm;

    .line 22958
    sget-object v0, Ldpw;->a:[Ldpw;

    iput-object v0, p0, Ldwa;->c:[Ldpw;

    .line 22961
    iput-object v1, p0, Ldwa;->d:Ldqz;

    .line 22966
    iput-object v1, p0, Ldwa;->f:Ljava/lang/Integer;

    .line 22952
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 22997
    iget-object v0, p0, Ldwa;->b:Ldvm;

    if-eqz v0, :cond_5

    .line 22998
    const/4 v0, 0x1

    iget-object v2, p0, Ldwa;->b:Ldvm;

    .line 22999
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 23001
    :goto_0
    iget-object v2, p0, Ldwa;->c:[Ldpw;

    if-eqz v2, :cond_1

    .line 23002
    iget-object v2, p0, Ldwa;->c:[Ldpw;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 23003
    if-eqz v4, :cond_0

    .line 23004
    const/4 v5, 0x2

    .line 23005
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 23002
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 23009
    :cond_1
    iget-object v1, p0, Ldwa;->d:Ldqz;

    if-eqz v1, :cond_2

    .line 23010
    const/4 v1, 0x3

    iget-object v2, p0, Ldwa;->d:Ldqz;

    .line 23011
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23013
    :cond_2
    iget-object v1, p0, Ldwa;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 23014
    const/4 v1, 0x4

    iget-object v2, p0, Ldwa;->e:Ljava/lang/Boolean;

    .line 23015
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 23017
    :cond_3
    iget-object v1, p0, Ldwa;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 23018
    const/4 v1, 0x5

    iget-object v2, p0, Ldwa;->f:Ljava/lang/Integer;

    .line 23019
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 23021
    :cond_4
    iget-object v1, p0, Ldwa;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23022
    iput v0, p0, Ldwa;->cachedSize:I

    .line 23023
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 22948
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldwa;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldwa;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldwa;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldwa;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldwa;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldwa;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldwa;->c:[Ldpw;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldpw;

    iget-object v3, p0, Ldwa;->c:[Ldpw;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldwa;->c:[Ldpw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldwa;->c:[Ldpw;

    :goto_2
    iget-object v2, p0, Ldwa;->c:[Ldpw;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldwa;->c:[Ldpw;

    new-instance v3, Ldpw;

    invoke-direct {v3}, Ldpw;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldwa;->c:[Ldpw;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldwa;->c:[Ldpw;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldwa;->c:[Ldpw;

    new-instance v3, Ldpw;

    invoke-direct {v3}, Ldpw;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldwa;->c:[Ldpw;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldwa;->d:Ldqz;

    if-nez v0, :cond_6

    new-instance v0, Ldqz;

    invoke-direct {v0}, Ldqz;-><init>()V

    iput-object v0, p0, Ldwa;->d:Ldqz;

    :cond_6
    iget-object v0, p0, Ldwa;->d:Ldqz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldwa;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_7

    const/4 v2, 0x1

    if-eq v0, v2, :cond_7

    const/4 v2, 0x2

    if-ne v0, v2, :cond_8

    :cond_7
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwa;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_8
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwa;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 22971
    iget-object v0, p0, Ldwa;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 22972
    const/4 v0, 0x1

    iget-object v1, p0, Ldwa;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 22974
    :cond_0
    iget-object v0, p0, Ldwa;->c:[Ldpw;

    if-eqz v0, :cond_2

    .line 22975
    iget-object v1, p0, Ldwa;->c:[Ldpw;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 22976
    if-eqz v3, :cond_1

    .line 22977
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 22975
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 22981
    :cond_2
    iget-object v0, p0, Ldwa;->d:Ldqz;

    if-eqz v0, :cond_3

    .line 22982
    const/4 v0, 0x3

    iget-object v1, p0, Ldwa;->d:Ldqz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 22984
    :cond_3
    iget-object v0, p0, Ldwa;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 22985
    const/4 v0, 0x4

    iget-object v1, p0, Ldwa;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 22987
    :cond_4
    iget-object v0, p0, Ldwa;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 22988
    const/4 v0, 0x5

    iget-object v1, p0, Ldwa;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 22990
    :cond_5
    iget-object v0, p0, Ldwa;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 22992
    return-void
.end method
