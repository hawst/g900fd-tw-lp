.class public final Lech;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lecw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lecw",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field volatile a:Lecw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lecw",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final b:Lehy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lehy",
            "<TV;>;"
        }
    .end annotation
.end field

.field final c:Lebk;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3423
    invoke-static {}, Lcom/google/common/cache/LocalCache;->i()Lecw;

    move-result-object v0

    invoke-direct {p0, v0}, Lech;-><init>(Lecw;)V

    .line 3424
    return-void
.end method

.method public constructor <init>(Lecw;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lecw",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 3426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3419
    invoke-static {}, Lehy;->a()Lehy;

    move-result-object v0

    iput-object v0, p0, Lech;->b:Lehy;

    .line 3420
    new-instance v0, Lebk;

    invoke-direct {v0}, Lebk;-><init>()V

    iput-object v0, p0, Lech;->c:Lebk;

    .line 3427
    iput-object p1, p0, Lech;->a:Lecw;

    .line 3428
    return-void
.end method

.method private static a(Lehy;Ljava/lang/Throwable;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lehy",
            "<*>;",
            "Ljava/lang/Throwable;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 3452
    :try_start_0
    invoke-virtual {p0, p1}, Lehy;->a(Ljava/lang/Throwable;)Z
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 3455
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 3439
    iget-object v0, p0, Lech;->a:Lecw;

    invoke-interface {v0}, Lecw;->a()I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;Lcom/google/common/cache/LocalCache$ReferenceEntry;)Lecw;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TV;>;TV;",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;)",
            "Lecw",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 3520
    return-object p0
.end method

.method public a(Ljava/lang/Object;Lebo;)Leht;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lebo",
            "<-TK;TV;>;)",
            "Leht",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 3479
    iget-object v0, p0, Lech;->c:Lebk;

    invoke-virtual {v0}, Lebk;->a()Lebk;

    .line 3480
    iget-object v0, p0, Lech;->a:Lecw;

    invoke-interface {v0}, Lecw;->get()Ljava/lang/Object;

    move-result-object v0

    .line 3482
    if-nez v0, :cond_2

    .line 3483
    :try_start_0
    invoke-virtual {p2}, Lebo;->a()Ljava/lang/Object;

    move-result-object v0

    .line 3484
    invoke-virtual {p0, v0}, Lech;->b(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lech;->b:Lehy;

    .line 3494
    :cond_0
    :goto_0
    return-object v0

    .line 3484
    :cond_1
    invoke-static {v0}, Leho;->a(Ljava/lang/Object;)Leht;

    move-result-object v0

    goto :goto_0

    .line 3486
    :cond_2
    invoke-virtual {p2, p1, v0}, Lebo;->a(Ljava/lang/Object;Ljava/lang/Object;)Leht;

    move-result-object v0

    .line 3488
    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Leho;->a(Ljava/lang/Object;)Leht;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 3490
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 3491
    instance-of v0, v1, Ljava/lang/InterruptedException;

    if-eqz v0, :cond_3

    .line 3492
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 3494
    :cond_3
    invoke-virtual {p0, v1}, Lech;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lech;->b:Lehy;

    goto :goto_0

    :cond_4
    invoke-static {}, Lehy;->a()Lehy;

    move-result-object v0

    invoke-static {v0, v1}, Lech;->a(Lehy;Ljava/lang/Throwable;)Z

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 3466
    if-eqz p1, :cond_0

    .line 3469
    invoke-virtual {p0, p1}, Lech;->b(Ljava/lang/Object;)Z

    .line 3476
    :goto_0
    return-void

    .line 3472
    :cond_0
    invoke-static {}, Lcom/google/common/cache/LocalCache;->i()Lecw;

    move-result-object v0

    iput-object v0, p0, Lech;->a:Lecw;

    goto :goto_0
.end method

.method public a(Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 3447
    iget-object v0, p0, Lech;->b:Lehy;

    invoke-static {v0, p1}, Lech;->a(Lehy;Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method

.method public b()Lcom/google/common/cache/LocalCache$ReferenceEntry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/cache/LocalCache$ReferenceEntry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 3515
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)Z"
        }
    .end annotation

    .prologue
    .line 3443
    iget-object v0, p0, Lech;->b:Lehy;

    invoke-virtual {v0, p1}, Lehy;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 3431
    const/4 v0, 0x1

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 3435
    iget-object v0, p0, Lech;->a:Lecw;

    invoke-interface {v0}, Lecw;->d()Z

    move-result v0

    return v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 3499
    iget-object v0, p0, Lech;->c:Lebk;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, Lebk;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public f()Lecw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lecw",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 3511
    iget-object v0, p0, Lech;->a:Lecw;

    return-object v0
.end method

.method public get()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 3507
    iget-object v0, p0, Lech;->a:Lecw;

    invoke-interface {v0}, Lecw;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
