.class public final Ldux;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldux;


# instance fields
.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14803
    const/4 v0, 0x0

    new-array v0, v0, [Ldux;

    sput-object v0, Ldux;->a:[Ldux;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14804
    invoke-direct {p0}, Lepn;-><init>()V

    .line 14817
    const/4 v0, 0x0

    iput-object v0, p0, Ldux;->c:Ljava/lang/Integer;

    .line 14804
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 14834
    const/4 v0, 0x0

    .line 14835
    iget-object v1, p0, Ldux;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 14836
    const/4 v0, 0x1

    iget-object v1, p0, Ldux;->b:Ljava/lang/Long;

    .line 14837
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lepl;->d(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 14839
    :cond_0
    iget-object v1, p0, Ldux;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 14840
    const/4 v1, 0x2

    iget-object v2, p0, Ldux;->c:Ljava/lang/Integer;

    .line 14841
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14843
    :cond_1
    iget-object v1, p0, Ldux;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14844
    iput v0, p0, Ldux;->cachedSize:I

    .line 14845
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 14800
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldux;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldux;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldux;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldux;->b:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0x14

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x28

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldux;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldux;->c:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 14822
    iget-object v0, p0, Ldux;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 14823
    const/4 v0, 0x1

    iget-object v1, p0, Ldux;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 14825
    :cond_0
    iget-object v0, p0, Ldux;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 14826
    const/4 v0, 0x2

    iget-object v1, p0, Ldux;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 14828
    :cond_1
    iget-object v0, p0, Ldux;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 14830
    return-void
.end method
