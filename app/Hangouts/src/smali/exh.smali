.class public final Lexh;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lexh;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/Boolean;

.field public m:Ljava/lang/Integer;

.field public n:Lexi;

.field public o:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lexh;

    sput-object v0, Lexh;->a:[Lexh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 136
    const/4 v0, 0x0

    iput-object v0, p0, Lexh;->n:Lexi;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 191
    const/4 v0, 0x0

    .line 192
    iget-object v1, p0, Lexh;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 193
    const/4 v0, 0x1

    iget-object v1, p0, Lexh;->b:Ljava/lang/String;

    .line 194
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 196
    :cond_0
    iget-object v1, p0, Lexh;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 197
    const/4 v1, 0x2

    iget-object v2, p0, Lexh;->c:Ljava/lang/Integer;

    .line 198
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 200
    :cond_1
    iget-object v1, p0, Lexh;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 201
    const/4 v1, 0x3

    iget-object v2, p0, Lexh;->d:Ljava/lang/Integer;

    .line 202
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 204
    :cond_2
    iget-object v1, p0, Lexh;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 205
    const/4 v1, 0x4

    iget-object v2, p0, Lexh;->e:Ljava/lang/String;

    .line 206
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 208
    :cond_3
    iget-object v1, p0, Lexh;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 209
    const/4 v1, 0x5

    iget-object v2, p0, Lexh;->f:Ljava/lang/String;

    .line 210
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 212
    :cond_4
    iget-object v1, p0, Lexh;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 213
    const/4 v1, 0x6

    iget-object v2, p0, Lexh;->g:Ljava/lang/String;

    .line 214
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 216
    :cond_5
    iget-object v1, p0, Lexh;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 217
    const/4 v1, 0x7

    iget-object v2, p0, Lexh;->h:Ljava/lang/String;

    .line 218
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 220
    :cond_6
    iget-object v1, p0, Lexh;->i:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 221
    const/16 v1, 0x8

    iget-object v2, p0, Lexh;->i:Ljava/lang/String;

    .line 222
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 224
    :cond_7
    iget-object v1, p0, Lexh;->j:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 225
    const/16 v1, 0x9

    iget-object v2, p0, Lexh;->j:Ljava/lang/String;

    .line 226
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 228
    :cond_8
    iget-object v1, p0, Lexh;->k:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 229
    const/16 v1, 0xa

    iget-object v2, p0, Lexh;->k:Ljava/lang/String;

    .line 230
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 232
    :cond_9
    iget-object v1, p0, Lexh;->l:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 233
    const/16 v1, 0xb

    iget-object v2, p0, Lexh;->l:Ljava/lang/Boolean;

    .line 234
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 236
    :cond_a
    iget-object v1, p0, Lexh;->m:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 237
    const/16 v1, 0xc

    iget-object v2, p0, Lexh;->m:Ljava/lang/Integer;

    .line 238
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 240
    :cond_b
    iget-object v1, p0, Lexh;->n:Lexi;

    if-eqz v1, :cond_c

    .line 241
    const/16 v1, 0xd

    iget-object v2, p0, Lexh;->n:Lexi;

    .line 242
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 244
    :cond_c
    iget-object v1, p0, Lexh;->o:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 245
    const/16 v1, 0xe

    iget-object v2, p0, Lexh;->o:Ljava/lang/String;

    .line 246
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 248
    :cond_d
    iget-object v1, p0, Lexh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 249
    iput v0, p0, Lexh;->cachedSize:I

    .line 250
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lexh;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lexh;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lexh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexh;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lexh;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lexh;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexh;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexh;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexh;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexh;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexh;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexh;->j:Ljava/lang/String;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexh;->k:Ljava/lang/String;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lexh;->l:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lexh;->m:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_d
    iget-object v0, p0, Lexh;->n:Lexi;

    if-nez v0, :cond_2

    new-instance v0, Lexi;

    invoke-direct {v0}, Lexi;-><init>()V

    iput-object v0, p0, Lexh;->n:Lexi;

    :cond_2
    iget-object v0, p0, Lexh;->n:Lexi;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexh;->o:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lexh;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 144
    const/4 v0, 0x1

    iget-object v1, p0, Lexh;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 146
    :cond_0
    iget-object v0, p0, Lexh;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 147
    const/4 v0, 0x2

    iget-object v1, p0, Lexh;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 149
    :cond_1
    iget-object v0, p0, Lexh;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 150
    const/4 v0, 0x3

    iget-object v1, p0, Lexh;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 152
    :cond_2
    iget-object v0, p0, Lexh;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 153
    const/4 v0, 0x4

    iget-object v1, p0, Lexh;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 155
    :cond_3
    iget-object v0, p0, Lexh;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 156
    const/4 v0, 0x5

    iget-object v1, p0, Lexh;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 158
    :cond_4
    iget-object v0, p0, Lexh;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 159
    const/4 v0, 0x6

    iget-object v1, p0, Lexh;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 161
    :cond_5
    iget-object v0, p0, Lexh;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 162
    const/4 v0, 0x7

    iget-object v1, p0, Lexh;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 164
    :cond_6
    iget-object v0, p0, Lexh;->i:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 165
    const/16 v0, 0x8

    iget-object v1, p0, Lexh;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 167
    :cond_7
    iget-object v0, p0, Lexh;->j:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 168
    const/16 v0, 0x9

    iget-object v1, p0, Lexh;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 170
    :cond_8
    iget-object v0, p0, Lexh;->k:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 171
    const/16 v0, 0xa

    iget-object v1, p0, Lexh;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 173
    :cond_9
    iget-object v0, p0, Lexh;->l:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 174
    const/16 v0, 0xb

    iget-object v1, p0, Lexh;->l:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 176
    :cond_a
    iget-object v0, p0, Lexh;->m:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 177
    const/16 v0, 0xc

    iget-object v1, p0, Lexh;->m:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 179
    :cond_b
    iget-object v0, p0, Lexh;->n:Lexi;

    if-eqz v0, :cond_c

    .line 180
    const/16 v0, 0xd

    iget-object v1, p0, Lexh;->n:Lexi;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 182
    :cond_c
    iget-object v0, p0, Lexh;->o:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 183
    const/16 v0, 0xe

    iget-object v1, p0, Lexh;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 185
    :cond_d
    iget-object v0, p0, Lexh;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 187
    return-void
.end method
