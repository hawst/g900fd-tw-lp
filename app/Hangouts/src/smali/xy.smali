.class public final Lxy;
.super Lzx;
.source "PG"

# interfaces
.implements Laac;


# static fields
.field private static final f:Z

.field private static g:I

.field private static final r:D

.field private static final s:D


# instance fields
.field private h:Landroid/graphics/Bitmap;

.field private i:Landroid/graphics/Canvas;

.field private j:Z

.field private k:Z

.field private l:I

.field private m:I

.field private n:I

.field private final o:[Lzx;

.field private p:Landroid/graphics/Rect;

.field private q:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    .line 34
    sget-object v0, Lbys;->c:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lxy;->f:Z

    .line 36
    const/4 v0, -0x1

    sput v0, Lxy;->g:I

    .line 283
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    add-double/2addr v0, v2

    div-double v0, v2, v0

    sput-wide v0, Lxy;->r:D

    .line 284
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    .line 285
    invoke-static {v2, v3}, Ljava/lang/Math;->acos(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x4010000000000000L    # 4.0

    div-double/2addr v0, v2

    sput-wide v0, Lxy;->s:D

    .line 284
    return-void
.end method

.method private constructor <init>(Lxz;ILaac;Ljava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 101
    invoke-direct {p0, p1, p3, v1, p4}, Lzx;-><init>(Lbyq;Laac;ZLjava/lang/Object;)V

    .line 83
    iput-boolean v0, p0, Lxy;->j:Z

    .line 84
    iput-boolean v1, p0, Lxy;->k:Z

    .line 88
    iput v0, p0, Lxy;->l:I

    .line 92
    const/4 v0, 0x4

    new-array v0, v0, [Lzx;

    iput-object v0, p0, Lxy;->o:[Lzx;

    .line 103
    iput p2, p0, Lxy;->n:I

    .line 104
    sget v0, Lxy;->g:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 105
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->cO:I

    .line 106
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lxy;->g:I

    .line 108
    :cond_0
    return-void
.end method

.method public static a(Ljava/util/List;ILyj;ILjava/lang/String;Laac;Ljava/lang/Object;Ljava/lang/String;ZZ)Lzx;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Lyj;",
            "I",
            "Ljava/lang/String;",
            "Laac;",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            "ZZ)",
            "Lzx;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 503
    if-eqz p0, :cond_5

    .line 504
    sget-boolean v1, Lxy;->f:Z

    if-eqz v1, :cond_0

    .line 505
    const-string v1, "Babel_medialoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "makeImageRequest oldKey="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " urls="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "defaultAvatars="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v1, p1

    if-nez v1, :cond_2

    .line 543
    :cond_1
    :goto_0
    return-object v0

    .line 513
    :cond_2
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v4, :cond_3

    if-nez p1, :cond_3

    .line 514
    new-instance v1, Lbyq;

    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0, p2}, Lbyq;-><init>(Ljava/lang/String;Lyj;)V

    .line 515
    invoke-virtual {v1, p3}, Lbyq;->a(I)Lbyq;

    move-result-object v0

    invoke-virtual {v0, v4}, Lbyq;->d(Z)Lbyq;

    move-result-object v0

    .line 516
    invoke-virtual {v0, v4}, Lbyq;->b(Z)Lbyq;

    move-result-object v0

    invoke-virtual {v0, p8}, Lbyq;->e(Z)Lbyq;

    move-result-object v0

    invoke-virtual {v0, p9}, Lbyq;->f(Z)Lbyq;

    move-result-object v1

    .line 517
    new-instance v0, Lzx;

    invoke-direct {v0, v1, p5, v4, p6}, Lzx;-><init>(Lbyq;Laac;ZLjava/lang/Object;)V

    .line 519
    invoke-virtual {v0, v5}, Lzx;->b(Z)V

    goto :goto_0

    .line 522
    :cond_3
    new-instance v1, Lxz;

    invoke-direct {v1, p0, p2, p4, p3}, Lxz;-><init>(Ljava/util/List;Lyj;Ljava/lang/String;I)V

    .line 524
    invoke-virtual {v1, p8}, Lxz;->e(Z)Lbyq;

    move-result-object v2

    invoke-virtual {v2, p9}, Lbyq;->f(Z)Lbyq;

    .line 525
    invoke-virtual {v1}, Lxz;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 526
    sget-boolean v1, Lxy;->f:Z

    if-eqz v1, :cond_1

    .line 527
    const-string v1, "Babel_medialoader"

    const-string v2, "makeImageRequest MATCH multiple"

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 531
    :cond_4
    new-instance v0, Lxy;

    invoke-direct {v0, v1, p1, p5, p6}, Lxy;-><init>(Lxz;ILaac;Ljava/lang/Object;)V

    .line 533
    sget-boolean v1, Lxy;->f:Z

    if-eqz v1, :cond_1

    .line 534
    const-string v1, "Babel_medialoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "makeImageRequest create new AvatarImageRequest="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 536
    invoke-virtual {v0}, Lxy;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 534
    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 541
    :cond_5
    const-string v1, "Babel_medialoader"

    const-string v2, "makeImageRequest imageUrls are null and no default avatars used, returning null."

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Landroid/graphics/Bitmap;I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 292
    if-ltz p2, :cond_4

    iget v0, p0, Lxy;->l:I

    if-ge p2, v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 294
    iget-object v0, p0, Lxy;->p:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    .line 296
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lxy;->p:Landroid/graphics/Rect;

    .line 299
    :cond_0
    iget-object v0, p0, Lxy;->q:Landroid/graphics/Rect;

    if-nez v0, :cond_1

    .line 301
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lxy;->q:Landroid/graphics/Rect;

    .line 305
    :cond_1
    invoke-virtual {p0, p1}, Lxy;->b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 307
    iget-object v0, p0, Lxy;->b:Lbyu;

    check-cast v0, Lxz;

    .line 308
    invoke-virtual {v0}, Lxz;->c()I

    move-result v0

    .line 309
    div-int/lit8 v3, v0, 0x2

    .line 310
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 311
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 315
    iget-object v6, p0, Lxy;->p:Landroid/graphics/Rect;

    invoke-static {v6, v4, v5}, Lxy;->a(Landroid/graphics/Rect;II)V

    .line 321
    iget v4, p0, Lxy;->l:I

    packed-switch v4, :pswitch_data_0

    .line 406
    :goto_1
    :pswitch_0
    sget-boolean v1, Lxy;->f:Z

    if-eqz v1, :cond_2

    .line 407
    const-string v1, "Babel_medialoader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "AvatarImageRequest  drew onto composite position="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " compound:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 408
    invoke-virtual {p0}, Lxy;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 407
    invoke-static {v1, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    :cond_2
    iget-object v1, p0, Lxy;->h:Landroid/graphics/Bitmap;

    if-nez v1, :cond_3

    .line 413
    invoke-static {}, Lbyr;->a()Lbxs;

    move-result-object v1

    invoke-virtual {v1, v0, v0}, Lbxs;->b(II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lxy;->h:Landroid/graphics/Bitmap;

    .line 415
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lxy;->h:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lxy;->i:Landroid/graphics/Canvas;

    .line 419
    :cond_3
    iget-object v0, p0, Lxy;->i:Landroid/graphics/Canvas;

    iget-object v1, p0, Lxy;->p:Landroid/graphics/Rect;

    iget-object v3, p0, Lxy;->q:Landroid/graphics/Rect;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 420
    return-void

    :cond_4
    move v0, v1

    .line 292
    goto/16 :goto_0

    .line 324
    :pswitch_1
    packed-switch p2, :pswitch_data_1

    goto :goto_1

    .line 326
    :pswitch_2
    iget-object v1, p0, Lxy;->q:Landroid/graphics/Rect;

    invoke-static {v1, v0, v0}, Lxy;->a(Landroid/graphics/Rect;II)V

    goto :goto_1

    .line 345
    :pswitch_3
    sget-wide v3, Lxy;->r:D

    int-to-double v5, v0

    mul-double/2addr v3, v5

    double-to-int v3, v3

    .line 346
    sub-int v4, v0, v3

    .line 347
    iget-object v5, p0, Lxy;->q:Landroid/graphics/Rect;

    invoke-virtual {v5, v1, v1, v3, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 348
    packed-switch p2, :pswitch_data_2

    goto :goto_1

    .line 353
    :pswitch_4
    iget-object v1, p0, Lxy;->q:Landroid/graphics/Rect;

    invoke-virtual {v1, v4, v4}, Landroid/graphics/Rect;->offset(II)V

    goto :goto_1

    .line 374
    :pswitch_5
    iget-object v4, p0, Lxy;->q:Landroid/graphics/Rect;

    invoke-virtual {v4, v1, v1, v3, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 376
    int-to-double v4, v0

    sget-wide v6, Lxy;->s:D

    mul-double/2addr v4, v6

    double-to-int v4, v4

    .line 377
    packed-switch p2, :pswitch_data_3

    goto :goto_1

    .line 380
    :pswitch_6
    iget-object v1, p0, Lxy;->q:Landroid/graphics/Rect;

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Rect;->offset(II)V

    goto :goto_1

    .line 385
    :pswitch_7
    iget-object v5, p0, Lxy;->q:Landroid/graphics/Rect;

    sub-int/2addr v3, v4

    invoke-virtual {v5, v1, v3}, Landroid/graphics/Rect;->offset(II)V

    goto/16 :goto_1

    .line 390
    :pswitch_8
    iget-object v1, p0, Lxy;->q:Landroid/graphics/Rect;

    sub-int v4, v3, v4

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Rect;->offset(II)V

    goto/16 :goto_1

    .line 401
    :pswitch_9
    iget-object v3, p0, Lxy;->q:Landroid/graphics/Rect;

    div-int/lit8 v4, v0, 0x2

    sub-int v5, v0, v4

    invoke-virtual {v3, v1, v1, v4, v4}, Landroid/graphics/Rect;->set(IIII)V

    packed-switch p2, :pswitch_data_4

    goto/16 :goto_1

    :pswitch_a
    invoke-virtual {v3, v1, v1}, Landroid/graphics/Rect;->offset(II)V

    goto/16 :goto_1

    :pswitch_b
    invoke-virtual {v3, v5, v1}, Landroid/graphics/Rect;->offset(II)V

    goto/16 :goto_1

    :pswitch_c
    invoke-virtual {v3, v1, v5}, Landroid/graphics/Rect;->offset(II)V

    goto/16 :goto_1

    :pswitch_d
    invoke-virtual {v3, v5, v5}, Landroid/graphics/Rect;->offset(II)V

    goto/16 :goto_1

    .line 321
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_9
    .end packed-switch

    .line 324
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    .line 348
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 377
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 401
    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method private static a(Landroid/graphics/Rect;II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 445
    if-le p1, p2, :cond_0

    .line 446
    sub-int v0, p1, p2

    div-int/lit8 v0, v0, 0x2

    .line 447
    add-int v1, v0, p2

    invoke-virtual {p0, v0, v2, v1, p2}, Landroid/graphics/Rect;->set(IIII)V

    .line 452
    :goto_0
    return-void

    .line 449
    :cond_0
    sub-int v0, p2, p1

    div-int/lit8 v0, v0, 0x2

    .line 450
    add-int v1, v0, p1

    invoke-virtual {p0, v2, v0, p1, v1}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method private o()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 427
    iget v0, p0, Lxy;->m:I

    if-nez v0, :cond_0

    .line 428
    new-instance v0, Lbzn;

    iget-object v1, p0, Lxy;->h:Landroid/graphics/Bitmap;

    .line 429
    invoke-virtual {p0}, Lxy;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbzn;-><init>(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 430
    invoke-virtual {v0}, Lbzn;->a()V

    .line 431
    invoke-static {}, Lbyr;->a()Lbxs;

    move-result-object v1

    invoke-virtual {p0}, Lxy;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lbxs;->a(Ljava/lang/String;Lbzn;)Lbzn;

    .line 432
    invoke-static {p0, v0}, Lbsn;->a(Lbsv;Lbsu;)V

    .line 433
    iput-object v3, p0, Lxy;->h:Landroid/graphics/Bitmap;

    .line 434
    iput-object v3, p0, Lxy;->i:Landroid/graphics/Canvas;

    .line 436
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lbzn;Lbyd;ZLzx;Z)V
    .locals 4

    .prologue
    .line 220
    invoke-static {p2}, Lcwz;->a(Ljava/lang/Object;)V

    .line 221
    invoke-static {}, Lcwz;->b()V

    .line 222
    sget-boolean v0, Lxy;->f:Z

    if-eqz v0, :cond_0

    .line 223
    const-string v0, "Babel_medialoader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AvatarImageRequest setImageBitmap for request "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 224
    invoke-virtual {p4}, Lzx;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " success="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " loadedFromCache="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " compound:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 225
    invoke-virtual {p0}, Lxy;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 223
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    :cond_0
    if-nez p3, :cond_2

    .line 231
    invoke-static {}, Lyn;->c()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 238
    :goto_0
    iget-object v2, p0, Lxy;->o:[Lzx;

    monitor-enter v2

    .line 239
    :try_start_0
    iget-boolean v1, p0, Lxy;->j:Z

    if-eqz v1, :cond_3

    .line 240
    if-eqz p1, :cond_1

    .line 241
    invoke-virtual {p1}, Lbzn;->b()V

    .line 243
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 278
    :goto_1
    return-void

    .line 233
    :cond_2
    invoke-virtual {p1}, Lbzn;->e()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 247
    :cond_3
    const/4 v1, 0x0

    :goto_2
    :try_start_1
    iget v3, p0, Lxy;->l:I

    if-ge v1, v3, :cond_4

    .line 249
    iget-object v3, p0, Lxy;->o:[Lzx;

    aget-object v3, v3, v1

    if-eq v3, p4, :cond_4

    .line 250
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 253
    :cond_4
    iget v3, p0, Lxy;->l:I

    if-lt v1, v3, :cond_6

    .line 254
    if-eqz p1, :cond_5

    .line 255
    invoke-virtual {p1}, Lbzn;->b()V

    .line 257
    :cond_5
    const-string v0, "Babel"

    const-string v1, "Received image that was not part of the requested set"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 278
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 262
    :cond_6
    :try_start_2
    invoke-direct {p0, v0, v1}, Lxy;->a(Landroid/graphics/Bitmap;I)V

    .line 264
    if-eqz p1, :cond_7

    .line 265
    invoke-virtual {p1}, Lbzn;->b()V

    .line 269
    :cond_7
    iget-object v0, p0, Lxy;->o:[Lzx;

    const/4 v3, 0x0

    aput-object v3, v0, v1

    .line 270
    iget v0, p0, Lxy;->m:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lxy;->m:I

    .line 271
    sget-boolean v0, Lxy;->f:Z

    if-eqz v0, :cond_8

    .line 272
    const-string v0, "Babel_medialoader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "AvatarImageRequest pending size="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lxy;->m:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " request removed="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " compound:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 273
    invoke-virtual {p0}, Lxy;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 272
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    :cond_8
    invoke-direct {p0}, Lxy;->o()V

    .line 278
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 116
    iput-boolean p1, p0, Lxy;->k:Z

    .line 117
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 134
    invoke-super {p0}, Lzx;->b()V

    .line 135
    iget-object v1, p0, Lxy;->o:[Lzx;

    monitor-enter v1

    .line 136
    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Lxy;->j:Z

    .line 137
    :goto_0
    iget v2, p0, Lxy;->l:I

    if-ge v0, v2, :cond_1

    .line 138
    iget-object v2, p0, Lxy;->o:[Lzx;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 139
    iget-object v2, p0, Lxy;->o:[Lzx;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lzx;->b()V

    .line 140
    iget-object v2, p0, Lxy;->o:[Lzx;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    .line 137
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 143
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lxy;->m:I

    .line 144
    iget-object v0, p0, Lxy;->h:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 145
    invoke-static {}, Lbyr;->a()Lbxs;

    move-result-object v0

    iget-object v2, p0, Lxy;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2}, Lbxs;->a(Landroid/graphics/Bitmap;)V

    .line 146
    const/4 v0, 0x0

    iput-object v0, p0, Lxy;->h:Landroid/graphics/Bitmap;

    .line 147
    const/4 v0, 0x0

    iput-object v0, p0, Lxy;->i:Landroid/graphics/Canvas;

    .line 149
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public c_()Lbsm;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v2, 0x0

    .line 158
    sget-boolean v0, Lxy;->f:Z

    if-eqz v0, :cond_0

    .line 159
    const-string v0, "Babel_medialoader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "AvatarImageRequest getBytes for request "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lxy;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    :cond_0
    invoke-static {}, Lcwz;->b()V

    .line 162
    iget-object v0, p0, Lxy;->b:Lbyu;

    check-cast v0, Lxz;

    .line 164
    invoke-virtual {v0}, Lxz;->c()I

    move-result v4

    .line 165
    iget-object v1, v0, Lxz;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    .line 167
    iget-object v6, p0, Lxy;->o:[Lzx;

    monitor-enter v6

    .line 168
    :try_start_0
    iget-boolean v1, p0, Lxy;->j:Z

    if-eqz v1, :cond_1

    .line 169
    monitor-exit v6

    .line 209
    :goto_0
    return-object v10

    .line 171
    :cond_1
    iget v1, p0, Lxy;->n:I

    add-int/2addr v1, v5

    const/4 v3, 0x4

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lxy;->l:I

    move v3, v2

    .line 177
    :goto_1
    const/4 v1, 0x4

    invoke-static {v5, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 178
    iget-object v1, v0, Lxz;->a:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 179
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 180
    new-instance v7, Lbyq;

    invoke-virtual {p0}, Lxy;->l()Lyj;

    move-result-object v8

    invoke-direct {v7, v1, v8}, Lbyq;-><init>(Ljava/lang/String;Lyj;)V

    .line 181
    invoke-virtual {v7, v4}, Lbyq;->a(I)Lbyq;

    move-result-object v1

    const/4 v7, 0x1

    invoke-virtual {v1, v7}, Lbyq;->d(Z)Lbyq;

    move-result-object v1

    iget-boolean v7, p0, Lxy;->k:Z

    invoke-virtual {v1, v7}, Lbyq;->b(Z)Lbyq;

    move-result-object v1

    .line 182
    new-instance v7, Lzx;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v7, v1, p0, v8, v9}, Lzx;-><init>(Lbyq;Laac;ZLjava/lang/Object;)V

    .line 184
    invoke-virtual {p0}, Lxy;->d()Z

    move-result v1

    invoke-virtual {v7, v1}, Lzx;->b(Z)V

    .line 185
    sget-boolean v1, Lxy;->f:Z

    if-eqz v1, :cond_2

    .line 186
    const-string v1, "Babel_medialoader"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "AvatarImageRequest creating ImageRequest "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 187
    invoke-virtual {v7}, Lzx;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " compound:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lxy;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 186
    invoke-static {v1, v8}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    :cond_2
    iget-object v1, p0, Lxy;->o:[Lzx;

    iget v8, p0, Lxy;->m:I

    aput-object v7, v1, v8

    .line 190
    iget v1, p0, Lxy;->m:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lxy;->m:I

    .line 177
    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_4
    move v0, v2

    .line 195
    :goto_2
    iget v1, p0, Lxy;->m:I

    if-ge v0, v1, :cond_6

    .line 196
    iget-object v1, p0, Lxy;->o:[Lzx;

    aget-object v1, v1, v0

    .line 197
    invoke-virtual {v1}, Lzx;->e()Z

    move-result v2

    if-nez v2, :cond_5

    .line 198
    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v2

    invoke-virtual {v2, v1}, Lbsn;->c(Lbrv;)V

    .line 195
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 201
    :cond_6
    invoke-static {}, Lyn;->c()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 203
    iget v0, p0, Lxy;->m:I

    :goto_3
    iget v2, p0, Lxy;->l:I

    if-ge v0, v2, :cond_7

    .line 204
    invoke-direct {p0, v1, v0}, Lxy;->a(Landroid/graphics/Bitmap;I)V

    .line 203
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 207
    :cond_7
    invoke-direct {p0}, Lxy;->o()V

    .line 208
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method
