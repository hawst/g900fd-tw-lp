.class public final Ldgb;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldgb;


# instance fields
.field public b:Ldng;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Boolean;

.field public e:[Ldmz;

.field public f:[Ldmz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7359
    const/4 v0, 0x0

    new-array v0, v0, [Ldgb;

    sput-object v0, Ldgb;->a:[Ldgb;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7360
    invoke-direct {p0}, Lepn;-><init>()V

    .line 7363
    const/4 v0, 0x0

    iput-object v0, p0, Ldgb;->b:Ldng;

    .line 7370
    sget-object v0, Ldmz;->a:[Ldmz;

    iput-object v0, p0, Ldgb;->e:[Ldmz;

    .line 7373
    sget-object v0, Ldmz;->a:[Ldmz;

    iput-object v0, p0, Ldgb;->f:[Ldmz;

    .line 7360
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 7408
    iget-object v0, p0, Ldgb;->b:Ldng;

    if-eqz v0, :cond_6

    .line 7409
    const/4 v0, 0x1

    iget-object v2, p0, Ldgb;->b:Ldng;

    .line 7410
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7412
    :goto_0
    iget-object v2, p0, Ldgb;->c:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    .line 7413
    const/4 v2, 0x2

    iget-object v3, p0, Ldgb;->c:Ljava/lang/Boolean;

    .line 7414
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 7416
    :cond_0
    iget-object v2, p0, Ldgb;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 7417
    const/4 v2, 0x3

    iget-object v3, p0, Ldgb;->d:Ljava/lang/Boolean;

    .line 7418
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 7420
    :cond_1
    iget-object v2, p0, Ldgb;->e:[Ldmz;

    if-eqz v2, :cond_3

    .line 7421
    iget-object v3, p0, Ldgb;->e:[Ldmz;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 7422
    if-eqz v5, :cond_2

    .line 7423
    const/4 v6, 0x4

    .line 7424
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 7421
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 7428
    :cond_3
    iget-object v2, p0, Ldgb;->f:[Ldmz;

    if-eqz v2, :cond_5

    .line 7429
    iget-object v2, p0, Ldgb;->f:[Ldmz;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 7430
    if-eqz v4, :cond_4

    .line 7431
    const/4 v5, 0x5

    .line 7432
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 7429
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 7436
    :cond_5
    iget-object v1, p0, Ldgb;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7437
    iput v0, p0, Ldgb;->cachedSize:I

    .line 7438
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 7356
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldgb;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldgb;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldgb;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldgb;->b:Ldng;

    if-nez v0, :cond_2

    new-instance v0, Ldng;

    invoke-direct {v0}, Ldng;-><init>()V

    iput-object v0, p0, Ldgb;->b:Ldng;

    :cond_2
    iget-object v0, p0, Ldgb;->b:Ldng;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldgb;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldgb;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldgb;->e:[Ldmz;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldmz;

    iget-object v3, p0, Ldgb;->e:[Ldmz;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldgb;->e:[Ldmz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldgb;->e:[Ldmz;

    :goto_2
    iget-object v2, p0, Ldgb;->e:[Ldmz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldgb;->e:[Ldmz;

    new-instance v3, Ldmz;

    invoke-direct {v3}, Ldmz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgb;->e:[Ldmz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldgb;->e:[Ldmz;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldgb;->e:[Ldmz;

    new-instance v3, Ldmz;

    invoke-direct {v3}, Ldmz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgb;->e:[Ldmz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldgb;->f:[Ldmz;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldmz;

    iget-object v3, p0, Ldgb;->f:[Ldmz;

    if-eqz v3, :cond_6

    iget-object v3, p0, Ldgb;->f:[Ldmz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Ldgb;->f:[Ldmz;

    :goto_4
    iget-object v2, p0, Ldgb;->f:[Ldmz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Ldgb;->f:[Ldmz;

    new-instance v3, Ldmz;

    invoke-direct {v3}, Ldmz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgb;->f:[Ldmz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Ldgb;->f:[Ldmz;

    array-length v0, v0

    goto :goto_3

    :cond_8
    iget-object v2, p0, Ldgb;->f:[Ldmz;

    new-instance v3, Ldmz;

    invoke-direct {v3}, Ldmz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgb;->f:[Ldmz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 7378
    iget-object v1, p0, Ldgb;->b:Ldng;

    if-eqz v1, :cond_0

    .line 7379
    const/4 v1, 0x1

    iget-object v2, p0, Ldgb;->b:Ldng;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 7381
    :cond_0
    iget-object v1, p0, Ldgb;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 7382
    const/4 v1, 0x2

    iget-object v2, p0, Ldgb;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 7384
    :cond_1
    iget-object v1, p0, Ldgb;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 7385
    const/4 v1, 0x3

    iget-object v2, p0, Ldgb;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 7387
    :cond_2
    iget-object v1, p0, Ldgb;->e:[Ldmz;

    if-eqz v1, :cond_4

    .line 7388
    iget-object v2, p0, Ldgb;->e:[Ldmz;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 7389
    if-eqz v4, :cond_3

    .line 7390
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 7388
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 7394
    :cond_4
    iget-object v1, p0, Ldgb;->f:[Ldmz;

    if-eqz v1, :cond_6

    .line 7395
    iget-object v1, p0, Ldgb;->f:[Ldmz;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 7396
    if-eqz v3, :cond_5

    .line 7397
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 7395
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 7401
    :cond_6
    iget-object v0, p0, Ldgb;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 7403
    return-void
.end method
