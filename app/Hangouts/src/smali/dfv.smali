.class public final Ldfv;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldfv;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:[Ljava/lang/String;

.field public d:Ldfx;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5027
    const/4 v0, 0x0

    new-array v0, v0, [Ldfv;

    sput-object v0, Ldfv;->a:[Ldfv;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5028
    invoke-direct {p0}, Lepn;-><init>()V

    .line 5341
    iput-object v1, p0, Ldfv;->b:Ljava/lang/Integer;

    .line 5344
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldfv;->c:[Ljava/lang/String;

    .line 5347
    iput-object v1, p0, Ldfv;->d:Ldfx;

    .line 5028
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 5370
    iget-object v0, p0, Ldfv;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 5371
    const/4 v0, 0x1

    iget-object v2, p0, Ldfv;->b:Ljava/lang/Integer;

    .line 5372
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5374
    :goto_0
    iget-object v2, p0, Ldfv;->c:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Ldfv;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 5376
    iget-object v3, p0, Ldfv;->c:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 5378
    invoke-static {v5}, Lepl;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 5376
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 5380
    :cond_0
    add-int/2addr v0, v2

    .line 5381
    iget-object v1, p0, Ldfv;->c:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5383
    :cond_1
    iget-object v1, p0, Ldfv;->d:Ldfx;

    if-eqz v1, :cond_2

    .line 5384
    const/4 v1, 0x3

    iget-object v2, p0, Ldfv;->d:Ldfx;

    .line 5385
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5387
    :cond_2
    iget-object v1, p0, Ldfv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5388
    iput v0, p0, Ldfv;->cachedSize:I

    .line 5389
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 5024
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldfv;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldfv;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldfv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldfv;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldfv;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldfv;->c:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Ldfv;->c:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldfv;->c:[Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Ldfv;->c:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Ldfv;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iget-object v1, p0, Ldfv;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldfv;->d:Ldfx;

    if-nez v0, :cond_5

    new-instance v0, Ldfx;

    invoke-direct {v0}, Ldfx;-><init>()V

    iput-object v0, p0, Ldfv;->d:Ldfx;

    :cond_5
    iget-object v0, p0, Ldfv;->d:Ldfx;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 5352
    iget-object v0, p0, Ldfv;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 5353
    const/4 v0, 0x1

    iget-object v1, p0, Ldfv;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 5355
    :cond_0
    iget-object v0, p0, Ldfv;->c:[Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 5356
    iget-object v1, p0, Ldfv;->c:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 5357
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->a(ILjava/lang/String;)V

    .line 5356
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5360
    :cond_1
    iget-object v0, p0, Ldfv;->d:Ldfx;

    if-eqz v0, :cond_2

    .line 5361
    const/4 v0, 0x3

    iget-object v1, p0, Ldfv;->d:Ldfx;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5363
    :cond_2
    iget-object v0, p0, Ldfv;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 5365
    return-void
.end method
