.class public final Ldpz;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldpz;


# instance fields
.field public b:[Ldui;

.field public c:[Ldui;

.field public d:[Ldsf;

.field public e:Ldsg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26019
    const/4 v0, 0x0

    new-array v0, v0, [Ldpz;

    sput-object v0, Ldpz;->a:[Ldpz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26020
    invoke-direct {p0}, Lepn;-><init>()V

    .line 26023
    sget-object v0, Ldui;->a:[Ldui;

    iput-object v0, p0, Ldpz;->b:[Ldui;

    .line 26026
    sget-object v0, Ldui;->a:[Ldui;

    iput-object v0, p0, Ldpz;->c:[Ldui;

    .line 26029
    sget-object v0, Ldsf;->a:[Ldsf;

    iput-object v0, p0, Ldpz;->d:[Ldsf;

    .line 26032
    const/4 v0, 0x0

    iput-object v0, p0, Ldpz;->e:Ldsg;

    .line 26020
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 26068
    iget-object v0, p0, Ldpz;->b:[Ldui;

    if-eqz v0, :cond_1

    .line 26069
    iget-object v3, p0, Ldpz;->b:[Ldui;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 26070
    if-eqz v5, :cond_0

    .line 26071
    const/4 v6, 0x1

    .line 26072
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 26069
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 26076
    :cond_2
    iget-object v2, p0, Ldpz;->c:[Ldui;

    if-eqz v2, :cond_4

    .line 26077
    iget-object v3, p0, Ldpz;->c:[Ldui;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 26078
    if-eqz v5, :cond_3

    .line 26079
    const/4 v6, 0x2

    .line 26080
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 26077
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 26084
    :cond_4
    iget-object v2, p0, Ldpz;->d:[Ldsf;

    if-eqz v2, :cond_6

    .line 26085
    iget-object v2, p0, Ldpz;->d:[Ldsf;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 26086
    if-eqz v4, :cond_5

    .line 26087
    const/4 v5, 0x3

    .line 26088
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 26085
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 26092
    :cond_6
    iget-object v1, p0, Ldpz;->e:Ldsg;

    if-eqz v1, :cond_7

    .line 26093
    const/4 v1, 0x4

    iget-object v2, p0, Ldpz;->e:Ldsg;

    .line 26094
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26096
    :cond_7
    iget-object v1, p0, Ldpz;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26097
    iput v0, p0, Ldpz;->cachedSize:I

    .line 26098
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 26016
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldpz;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldpz;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldpz;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldpz;->b:[Ldui;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldui;

    iget-object v3, p0, Ldpz;->b:[Ldui;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldpz;->b:[Ldui;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldpz;->b:[Ldui;

    :goto_2
    iget-object v2, p0, Ldpz;->b:[Ldui;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldpz;->b:[Ldui;

    new-instance v3, Ldui;

    invoke-direct {v3}, Ldui;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldpz;->b:[Ldui;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldpz;->b:[Ldui;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldpz;->b:[Ldui;

    new-instance v3, Ldui;

    invoke-direct {v3}, Ldui;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldpz;->b:[Ldui;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldpz;->c:[Ldui;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldui;

    iget-object v3, p0, Ldpz;->c:[Ldui;

    if-eqz v3, :cond_5

    iget-object v3, p0, Ldpz;->c:[Ldui;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Ldpz;->c:[Ldui;

    :goto_4
    iget-object v2, p0, Ldpz;->c:[Ldui;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Ldpz;->c:[Ldui;

    new-instance v3, Ldui;

    invoke-direct {v3}, Ldui;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldpz;->c:[Ldui;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Ldpz;->c:[Ldui;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Ldpz;->c:[Ldui;

    new-instance v3, Ldui;

    invoke-direct {v3}, Ldui;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldpz;->c:[Ldui;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldpz;->d:[Ldsf;

    if-nez v0, :cond_9

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Ldsf;

    iget-object v3, p0, Ldpz;->d:[Ldsf;

    if-eqz v3, :cond_8

    iget-object v3, p0, Ldpz;->d:[Ldsf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    iput-object v2, p0, Ldpz;->d:[Ldsf;

    :goto_6
    iget-object v2, p0, Ldpz;->d:[Ldsf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Ldpz;->d:[Ldsf;

    new-instance v3, Ldsf;

    invoke-direct {v3}, Ldsf;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldpz;->d:[Ldsf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    iget-object v0, p0, Ldpz;->d:[Ldsf;

    array-length v0, v0

    goto :goto_5

    :cond_a
    iget-object v2, p0, Ldpz;->d:[Ldsf;

    new-instance v3, Ldsf;

    invoke-direct {v3}, Ldsf;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldpz;->d:[Ldsf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Ldpz;->e:Ldsg;

    if-nez v0, :cond_b

    new-instance v0, Ldsg;

    invoke-direct {v0}, Ldsg;-><init>()V

    iput-object v0, p0, Ldpz;->e:Ldsg;

    :cond_b
    iget-object v0, p0, Ldpz;->e:Ldsg;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 26037
    iget-object v1, p0, Ldpz;->b:[Ldui;

    if-eqz v1, :cond_1

    .line 26038
    iget-object v2, p0, Ldpz;->b:[Ldui;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 26039
    if-eqz v4, :cond_0

    .line 26040
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 26038
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 26044
    :cond_1
    iget-object v1, p0, Ldpz;->c:[Ldui;

    if-eqz v1, :cond_3

    .line 26045
    iget-object v2, p0, Ldpz;->c:[Ldui;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 26046
    if-eqz v4, :cond_2

    .line 26047
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 26045
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 26051
    :cond_3
    iget-object v1, p0, Ldpz;->d:[Ldsf;

    if-eqz v1, :cond_5

    .line 26052
    iget-object v1, p0, Ldpz;->d:[Ldsf;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 26053
    if-eqz v3, :cond_4

    .line 26054
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 26052
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 26058
    :cond_5
    iget-object v0, p0, Ldpz;->e:Ldsg;

    if-eqz v0, :cond_6

    .line 26059
    const/4 v0, 0x4

    iget-object v1, p0, Ldpz;->e:Ldsg;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26061
    :cond_6
    iget-object v0, p0, Ldpz;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 26063
    return-void
.end method
