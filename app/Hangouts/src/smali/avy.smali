.class public final Lavy;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Z

.field private static final b:[I

.field private static c:Lavy;


# instance fields
.field private final d:Lavz;

.field private final e:Z

.field private final f:[J

.field private g:I

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x5

    .line 50
    sget-object v0, Lbys;->g:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lavy;->a:Z

    .line 54
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    .line 73
    sput-object v0, Lavy;->b:[I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 74
    return-void

    .line 54
    nop

    :array_0
    .array-data 4
        0x2
        0x1
        0x1
        0x1
        0x3
    .end array-data
.end method

.method private constructor <init>(Lavz;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    const/4 v0, 0x5

    new-array v0, v0, [J

    iput-object v0, p0, Lavy;->f:[J

    .line 70
    const/4 v0, 0x0

    iput v0, p0, Lavy;->h:I

    .line 78
    iput-object p1, p0, Lavy;->d:Lavz;

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lavy;->e:Z

    .line 80
    return-void
.end method

.method public static a()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 90
    invoke-static {}, Lavy;->l()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 91
    const-string v3, "instance"

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 92
    if-nez v1, :cond_2

    .line 94
    new-instance v0, Lavy;

    new-instance v1, Lavz;

    invoke-direct {v1, v2}, Lavz;-><init>(B)V

    invoke-direct {v0, v1}, Lavy;-><init>(Lavz;)V

    sput-object v0, Lavy;->c:Lavy;

    .line 107
    :cond_0
    :goto_0
    sget-boolean v0, Lavy;->a:Z

    if-eqz v0, :cond_1

    .line 108
    sget-object v0, Lavy;->c:Lavy;

    if-nez v0, :cond_6

    .line 109
    const-string v0, "Babel"

    const-string v1, "AppRatingTracker not enabled"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_1
    :goto_1
    return-void

    .line 96
    :cond_2
    const/16 v3, 0x2c

    invoke-static {v3}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v1, v3, v2

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_4

    .line 97
    :cond_3
    if-nez v0, :cond_5

    .line 99
    new-instance v0, Lavy;

    new-instance v1, Lavz;

    invoke-direct {v1, v2}, Lavz;-><init>(B)V

    invoke-direct {v0, v1}, Lavy;-><init>(Lavz;)V

    sput-object v0, Lavy;->c:Lavy;

    goto :goto_0

    .line 96
    :cond_4
    new-instance v0, Lavy;

    new-instance v1, Lavz;

    invoke-direct {v1, v2}, Lavz;-><init>(B)V

    invoke-direct {v0, v1}, Lavy;-><init>(Lavz;)V

    const/4 v1, 0x1

    aget-object v1, v3, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lavy;->h:I

    const/4 v1, 0x2

    aget-object v1, v3, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lavy;->g:I

    move v1, v2

    :goto_2
    const/4 v4, 0x5

    if-ge v1, v4, :cond_3

    iget-object v4, v0, Lavy;->f:[J

    add-int/lit8 v5, v1, 0x3

    aget-object v5, v3, v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    aput-wide v5, v4, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 100
    :cond_5
    iget v1, v0, Lavy;->h:I

    if-nez v1, :cond_0

    .line 103
    sput-object v0, Lavy;->c:Lavy;

    goto :goto_0

    .line 111
    :cond_6
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AppRatingTracker points: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lavy;->c:Lavy;

    iget v2, v2, Lavy;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static a(I)V
    .locals 7

    .prologue
    .line 165
    sget-object v0, Lavy;->c:Lavy;

    if-eqz v0, :cond_0

    .line 166
    sget-object v0, Lavy;->c:Lavy;

    invoke-direct {v0}, Lavy;->j()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lavy;->d:Lavz;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v3, v0, Lavy;->f:[J

    aget-wide v3, v3, p0

    const-wide/32 v5, 0x1b77400

    add-long/2addr v3, v5

    cmp-long v3, v1, v3

    if-ltz v3, :cond_0

    iget-object v3, v0, Lavy;->f:[J

    aput-wide v1, v3, p0

    iget v1, v0, Lavy;->g:I

    sget-object v2, Lavy;->b:[I

    aget v2, v2, p0

    add-int/2addr v1, v2

    iput v1, v0, Lavy;->g:I

    invoke-direct {v0}, Lavy;->k()V

    .line 168
    :cond_0
    return-void
.end method

.method public static b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 117
    const-string v0, "babel_app_rating_enabled"

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 120
    if-eqz v0, :cond_1

    sget-object v0, Lavy;->c:Lavy;

    if-eqz v0, :cond_1

    sget-object v0, Lavy;->c:Lavy;

    invoke-direct {v0}, Lavy;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 123
    const-string v2, "connectivity"

    .line 124
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 125
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 126
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 128
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 126
    goto :goto_0

    :cond_1
    move v0, v1

    .line 128
    goto :goto_0
.end method

.method public static c()V
    .locals 2

    .prologue
    .line 133
    sget-object v0, Lavy;->c:Lavy;

    if-eqz v0, :cond_0

    .line 134
    sget-object v0, Lavy;->c:Lavy;

    const/4 v1, 0x1

    iput v1, v0, Lavy;->h:I

    invoke-direct {v0}, Lavy;->k()V

    .line 136
    :cond_0
    return-void
.end method

.method public static d()V
    .locals 2

    .prologue
    .line 139
    sget-object v0, Lavy;->c:Lavy;

    if-eqz v0, :cond_0

    .line 140
    sget-object v0, Lavy;->c:Lavy;

    const/4 v1, 0x2

    iput v1, v0, Lavy;->h:I

    invoke-direct {v0}, Lavy;->k()V

    .line 142
    :cond_0
    return-void
.end method

.method public static e()V
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    invoke-static {v0}, Lavy;->a(I)V

    .line 146
    return-void
.end method

.method public static f()V
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x1

    invoke-static {v0}, Lavy;->a(I)V

    .line 150
    return-void
.end method

.method public static g()V
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x2

    invoke-static {v0}, Lavy;->a(I)V

    .line 154
    return-void
.end method

.method public static h()V
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x3

    invoke-static {v0}, Lavy;->a(I)V

    .line 158
    return-void
.end method

.method public static i()V
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x4

    invoke-static {v0}, Lavy;->a(I)V

    .line 162
    return-void
.end method

.method private j()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 173
    iget v2, p0, Lavy;->h:I

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    iget v2, p0, Lavy;->g:I

    const-string v3, "babel_app_rating_required_points_value"

    const/16 v4, 0xf

    invoke-static {v3, v4}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v3

    if-lt v2, v3, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private k()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/16 v6, 0x2c

    .line 211
    iget-boolean v1, p0, Lavy;->e:Z

    if-eqz v1, :cond_2

    .line 212
    invoke-static {}, Lavy;->l()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 213
    const-string v2, "instance"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v4, p0, Lavy;->h:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v4, p0, Lavy;->g:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_0
    const/4 v4, 0x5

    if-ge v0, v4, :cond_1

    iget-object v4, p0, Lavy;->f:[J

    aget-wide v4, v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v4, 0x4

    if-ge v0, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 214
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 215
    sget-boolean v0, Lavy;->a:Z

    if-eqz v0, :cond_2

    .line 216
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Updating AppRatingTracker points: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lavy;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    :cond_2
    return-void
.end method

.method private static l()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 255
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "app_rating_tracker"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method
