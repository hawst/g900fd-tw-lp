.class final Low;
.super Loc;
.source "PG"

# interfaces
.implements Loy;


# instance fields
.field f:Landroid/widget/ListAdapter;

.field final synthetic g:Lot;

.field private h:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lot;Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 698
    iput-object p1, p0, Low;->g:Lot;

    .line 699
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, p3}, Loc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 701
    invoke-virtual {p0, p1}, Low;->a(Landroid/view/View;)V

    .line 702
    invoke-virtual {p0}, Low;->b()V

    .line 703
    invoke-virtual {p0}, Low;->a()V

    .line 705
    new-instance v0, Lnx;

    new-instance v1, Lox;

    invoke-direct {v1, p0, p1}, Lox;-><init>(Low;Lot;)V

    invoke-direct {v0, p1, v1}, Lnx;-><init>(Lnu;Lnw;)V

    .line 716
    invoke-virtual {p0, v0}, Low;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 717
    return-void
.end method


# virtual methods
.method public a(Landroid/widget/ListAdapter;)V
    .locals 0

    .prologue
    .line 721
    invoke-super {p0, p1}, Loc;->a(Landroid/widget/ListAdapter;)V

    .line 722
    iput-object p1, p0, Low;->f:Landroid/widget/ListAdapter;

    .line 723
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 731
    iput-object p1, p0, Low;->h:Ljava/lang/CharSequence;

    .line 732
    return-void
.end method

.method public e()V
    .locals 6

    .prologue
    .line 736
    iget-object v0, p0, Low;->g:Lot;

    invoke-virtual {v0}, Lot;->getPaddingLeft()I

    move-result v1

    .line 737
    iget-object v0, p0, Low;->g:Lot;

    iget v0, v0, Lot;->E:I

    const/4 v2, -0x2

    if-ne v0, v2, :cond_1

    .line 738
    iget-object v0, p0, Low;->g:Lot;

    invoke-virtual {v0}, Lot;->getWidth()I

    move-result v2

    .line 739
    iget-object v0, p0, Low;->g:Lot;

    invoke-virtual {v0}, Lot;->getPaddingRight()I

    move-result v3

    .line 740
    iget-object v4, p0, Low;->g:Lot;

    iget-object v0, p0, Low;->f:Landroid/widget/ListAdapter;

    check-cast v0, Landroid/widget/SpinnerAdapter;

    invoke-virtual {p0}, Low;->c()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Lot;->a(Landroid/widget/SpinnerAdapter;Landroid/graphics/drawable/Drawable;)I

    move-result v0

    sub-int/2addr v2, v1

    sub-int/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v0}, Low;->d(I)V

    .line 750
    :goto_0
    invoke-virtual {p0}, Low;->c()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 751
    const/4 v0, 0x0

    .line 752
    if-eqz v2, :cond_0

    .line 753
    iget-object v0, p0, Low;->g:Lot;

    invoke-static {v0}, Lot;->a(Lot;)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 754
    iget-object v0, p0, Low;->g:Lot;

    invoke-static {v0}, Lot;->a(Lot;)Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->left:I

    neg-int v0, v0

    .line 756
    :cond_0
    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Low;->a(I)V

    .line 757
    invoke-virtual {p0}, Low;->g()V

    .line 758
    invoke-super {p0}, Loc;->e()V

    .line 759
    invoke-virtual {p0}, Low;->k()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 760
    iget-object v0, p0, Low;->g:Lot;

    invoke-virtual {v0}, Lot;->g()I

    move-result v0

    invoke-virtual {p0, v0}, Low;->e(I)V

    .line 761
    return-void

    .line 743
    :cond_1
    iget-object v0, p0, Low;->g:Lot;

    iget v0, v0, Lot;->E:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    .line 744
    iget-object v0, p0, Low;->g:Lot;

    invoke-virtual {v0}, Lot;->getWidth()I

    move-result v0

    .line 745
    iget-object v2, p0, Low;->g:Lot;

    invoke-virtual {v2}, Lot;->getPaddingRight()I

    move-result v2

    .line 746
    sub-int/2addr v0, v1

    sub-int/2addr v0, v2

    invoke-virtual {p0, v0}, Low;->d(I)V

    goto :goto_0

    .line 748
    :cond_2
    iget-object v0, p0, Low;->g:Lot;

    iget v0, v0, Lot;->E:I

    invoke-virtual {p0, v0}, Low;->d(I)V

    goto :goto_0
.end method
