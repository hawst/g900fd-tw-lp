.class public final Lcnn;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcno;",
            ">;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcnn;-><init>(B)V

    return-void
.end method

.method private constructor <init>(B)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcnn;->a:Ljava/util/ArrayList;

    const/16 v0, 0x64

    iput v0, p0, Lcnn;->b:I

    return-void
.end method


# virtual methods
.method public a()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcno;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcnn;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method public a(Lcom/google/android/gms/internal/qr;Lcom/google/android/gms/internal/qn;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcnn;->a:Ljava/util/ArrayList;

    new-instance v1, Lcno;

    invoke-direct {v1, p1, p2, v2}, Lcno;-><init>(Lcom/google/android/gms/internal/qr;Lcom/google/android/gms/internal/qn;B)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    iget-object v0, p0, Lcnn;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcnn;->b:I

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcnn;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcnn;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lcnn;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    return v0
.end method
