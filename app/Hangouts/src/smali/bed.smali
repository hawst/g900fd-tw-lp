.class public abstract Lbed;
.super Lbep;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final c:Ljava/lang/String;

.field protected transient h:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 723
    invoke-direct {p0}, Lbep;-><init>()V

    .line 721
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbed;->h:Z

    .line 724
    iput-object p1, p0, Lbed;->c:Ljava/lang/String;

    .line 725
    if-eqz p1, :cond_0

    .line 726
    invoke-static {p1}, Lyt;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 727
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot use client generated conversation id:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 730
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lyj;Lbph;)V
    .locals 3

    .prologue
    .line 749
    iget-object v0, p0, Lbed;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lbph;->e()I

    move-result v0

    const/16 v1, 0x71

    if-ne v0, v1, :cond_0

    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ERROR_RESPONSE_INVALID_CONVERSATION for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbed;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lbed;->c:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Lyj;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbed;->h:Z

    .line 750
    :cond_0
    return-void
.end method
