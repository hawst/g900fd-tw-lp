.class public final Lwq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field private final a:F

.field private final b:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const v0, 0x3f59999a    # 0.85f

    iput v0, p0, Lwq;->a:F

    .line 49
    const v0, 0x3f19999a    # 0.6f

    iput v0, p0, Lwq;->b:F

    .line 50
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 2

    .prologue
    .line 54
    iget v0, p0, Lwq;->a:F

    iget v1, p0, Lwq;->b:F

    invoke-static {p1, v0, v1}, Lwp;->a(FFF)F

    move-result v0

    return v0
.end method
