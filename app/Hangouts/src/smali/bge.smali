.class public Lbge;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ldys;)V
    .locals 3

    .prologue
    .line 3805
    invoke-direct {p0}, Lbfz;-><init>()V

    .line 3806
    iget-object v0, p1, Ldys;->c:Ldyq;

    if-eqz v0, :cond_1

    .line 3807
    iget-object v0, p1, Ldys;->c:Ldyq;

    iget-object v0, v0, Ldyq;->b:Ljava/lang/String;

    iput-object v0, p0, Lbge;->g:Ljava/lang/String;

    .line 3811
    :goto_0
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 3812
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CreateHangoutIdResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3814
    :cond_0
    return-void

    .line 3809
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lbge;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 3818
    new-instance v0, Ldys;

    invoke-direct {v0}, Ldys;-><init>()V

    .line 3819
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldys;

    .line 3820
    iget-object v1, v0, Ldys;->b:Ldvn;

    invoke-static {v1}, Lbge;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3821
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldys;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 3823
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbge;

    invoke-direct {v1, v0}, Lbge;-><init>(Ldys;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3828
    iget-object v0, p0, Lbge;->g:Ljava/lang/String;

    return-object v0
.end method
