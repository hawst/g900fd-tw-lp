.class public final Lern;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lern;

.field public static final b:Lepo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepo",
            "<",
            "Lern;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Lepo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepo",
            "<",
            "Lern;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Leqc;

.field public j:Ljava/lang/String;

.field public k:[Lepu;

.field public l:Lepu;

.field public m:[Lepu;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Lepu;

.field public q:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const v1, 0x25e8557

    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lern;

    sput-object v0, Lern;->a:[Lern;

    .line 13
    new-instance v0, Lero;

    invoke-direct {v0}, Lero;-><init>()V

    .line 14
    invoke-static {v1, v0}, Lepo;->a(ILepp;)Lepo;

    move-result-object v0

    sput-object v0, Lern;->b:Lepo;

    .line 17
    new-instance v0, Lerp;

    invoke-direct {v0}, Lerp;-><init>()V

    .line 18
    invoke-static {v1, v0}, Lepo;->a(ILepp;)Lepo;

    move-result-object v0

    sput-object v0, Lern;->c:Lepo;

    .line 17
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 31
    iput-object v1, p0, Lern;->i:Leqc;

    .line 36
    sget-object v0, Lepu;->a:[Lepu;

    iput-object v0, p0, Lern;->k:[Lepu;

    .line 39
    iput-object v1, p0, Lern;->l:Lepu;

    .line 42
    sget-object v0, Lepu;->a:[Lepu;

    iput-object v0, p0, Lern;->m:[Lepu;

    .line 49
    iput-object v1, p0, Lern;->p:Lepu;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 113
    iget-object v0, p0, Lern;->d:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 114
    const/4 v0, 0x1

    iget-object v2, p0, Lern;->d:Ljava/lang/String;

    .line 115
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 117
    :goto_0
    iget-object v2, p0, Lern;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 118
    const/4 v2, 0x2

    iget-object v3, p0, Lern;->e:Ljava/lang/String;

    .line 119
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 121
    :cond_0
    iget-object v2, p0, Lern;->f:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 122
    const/4 v2, 0x3

    iget-object v3, p0, Lern;->f:Ljava/lang/String;

    .line 123
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 125
    :cond_1
    iget-object v2, p0, Lern;->g:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 126
    const/4 v2, 0x4

    iget-object v3, p0, Lern;->g:Ljava/lang/String;

    .line 127
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 129
    :cond_2
    iget-object v2, p0, Lern;->h:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 130
    const/4 v2, 0x5

    iget-object v3, p0, Lern;->h:Ljava/lang/String;

    .line 131
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 133
    :cond_3
    iget-object v2, p0, Lern;->i:Leqc;

    if-eqz v2, :cond_4

    .line 134
    const/4 v2, 0x6

    iget-object v3, p0, Lern;->i:Leqc;

    .line 135
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 137
    :cond_4
    iget-object v2, p0, Lern;->j:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 138
    const/4 v2, 0x7

    iget-object v3, p0, Lern;->j:Ljava/lang/String;

    .line 139
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 141
    :cond_5
    iget-object v2, p0, Lern;->k:[Lepu;

    if-eqz v2, :cond_7

    .line 142
    iget-object v3, p0, Lern;->k:[Lepu;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 143
    if-eqz v5, :cond_6

    .line 144
    const/16 v6, 0x8

    .line 145
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 142
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 149
    :cond_7
    iget-object v2, p0, Lern;->l:Lepu;

    if-eqz v2, :cond_8

    .line 150
    const/16 v2, 0x9

    iget-object v3, p0, Lern;->l:Lepu;

    .line 151
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 153
    :cond_8
    iget-object v2, p0, Lern;->m:[Lepu;

    if-eqz v2, :cond_a

    .line 154
    iget-object v2, p0, Lern;->m:[Lepu;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_a

    aget-object v4, v2, v1

    .line 155
    if-eqz v4, :cond_9

    .line 156
    const/16 v5, 0xb

    .line 157
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 154
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 161
    :cond_a
    iget-object v1, p0, Lern;->n:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 162
    const/16 v1, 0xc

    iget-object v2, p0, Lern;->n:Ljava/lang/String;

    .line 163
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 165
    :cond_b
    iget-object v1, p0, Lern;->o:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 166
    const/16 v1, 0x4b

    iget-object v2, p0, Lern;->o:Ljava/lang/String;

    .line 167
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 169
    :cond_c
    iget-object v1, p0, Lern;->p:Lepu;

    if-eqz v1, :cond_d

    .line 170
    const/16 v1, 0xb9

    iget-object v2, p0, Lern;->p:Lepu;

    .line 171
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 173
    :cond_d
    iget-object v1, p0, Lern;->q:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 174
    const/16 v1, 0xfe

    iget-object v2, p0, Lern;->q:Ljava/lang/String;

    .line 175
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 177
    :cond_e
    iget-object v1, p0, Lern;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 178
    iput v0, p0, Lern;->cachedSize:I

    .line 179
    return v0

    :cond_f
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lern;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lern;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lern;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lern;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lern;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lern;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lern;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lern;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lern;->i:Leqc;

    if-nez v0, :cond_2

    new-instance v0, Leqc;

    invoke-direct {v0}, Leqc;-><init>()V

    iput-object v0, p0, Lern;->i:Leqc;

    :cond_2
    iget-object v0, p0, Lern;->i:Leqc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lern;->j:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lern;->k:[Lepu;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lepu;

    iget-object v3, p0, Lern;->k:[Lepu;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lern;->k:[Lepu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lern;->k:[Lepu;

    :goto_2
    iget-object v2, p0, Lern;->k:[Lepu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lern;->k:[Lepu;

    new-instance v3, Lepu;

    invoke-direct {v3}, Lepu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lern;->k:[Lepu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lern;->k:[Lepu;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lern;->k:[Lepu;

    new-instance v3, Lepu;

    invoke-direct {v3}, Lepu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lern;->k:[Lepu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lern;->l:Lepu;

    if-nez v0, :cond_6

    new-instance v0, Lepu;

    invoke-direct {v0}, Lepu;-><init>()V

    iput-object v0, p0, Lern;->l:Lepu;

    :cond_6
    iget-object v0, p0, Lern;->l:Lepu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_a
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lern;->m:[Lepu;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lepu;

    iget-object v3, p0, Lern;->m:[Lepu;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lern;->m:[Lepu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Lern;->m:[Lepu;

    :goto_4
    iget-object v2, p0, Lern;->m:[Lepu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Lern;->m:[Lepu;

    new-instance v3, Lepu;

    invoke-direct {v3}, Lepu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lern;->m:[Lepu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    iget-object v0, p0, Lern;->m:[Lepu;

    array-length v0, v0

    goto :goto_3

    :cond_9
    iget-object v2, p0, Lern;->m:[Lepu;

    new-instance v3, Lepu;

    invoke-direct {v3}, Lepu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lern;->m:[Lepu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lern;->n:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lern;->o:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lern;->p:Lepu;

    if-nez v0, :cond_a

    new-instance v0, Lepu;

    invoke-direct {v0}, Lepu;-><init>()V

    iput-object v0, p0, Lern;->p:Lepu;

    :cond_a
    iget-object v0, p0, Lern;->p:Lepu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lern;->q:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x25a -> :sswitch_c
        0x5ca -> :sswitch_d
        0x7f2 -> :sswitch_e
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 56
    iget-object v1, p0, Lern;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 57
    const/4 v1, 0x1

    iget-object v2, p0, Lern;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 59
    :cond_0
    iget-object v1, p0, Lern;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 60
    const/4 v1, 0x2

    iget-object v2, p0, Lern;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 62
    :cond_1
    iget-object v1, p0, Lern;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 63
    const/4 v1, 0x3

    iget-object v2, p0, Lern;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 65
    :cond_2
    iget-object v1, p0, Lern;->g:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 66
    const/4 v1, 0x4

    iget-object v2, p0, Lern;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 68
    :cond_3
    iget-object v1, p0, Lern;->h:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 69
    const/4 v1, 0x5

    iget-object v2, p0, Lern;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 71
    :cond_4
    iget-object v1, p0, Lern;->i:Leqc;

    if-eqz v1, :cond_5

    .line 72
    const/4 v1, 0x6

    iget-object v2, p0, Lern;->i:Leqc;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 74
    :cond_5
    iget-object v1, p0, Lern;->j:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 75
    const/4 v1, 0x7

    iget-object v2, p0, Lern;->j:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 77
    :cond_6
    iget-object v1, p0, Lern;->k:[Lepu;

    if-eqz v1, :cond_8

    .line 78
    iget-object v2, p0, Lern;->k:[Lepu;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 79
    if-eqz v4, :cond_7

    .line 80
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 78
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 84
    :cond_8
    iget-object v1, p0, Lern;->l:Lepu;

    if-eqz v1, :cond_9

    .line 85
    const/16 v1, 0x9

    iget-object v2, p0, Lern;->l:Lepu;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 87
    :cond_9
    iget-object v1, p0, Lern;->m:[Lepu;

    if-eqz v1, :cond_b

    .line 88
    iget-object v1, p0, Lern;->m:[Lepu;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_b

    aget-object v3, v1, v0

    .line 89
    if-eqz v3, :cond_a

    .line 90
    const/16 v4, 0xb

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 88
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 94
    :cond_b
    iget-object v0, p0, Lern;->n:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 95
    const/16 v0, 0xc

    iget-object v1, p0, Lern;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 97
    :cond_c
    iget-object v0, p0, Lern;->o:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 98
    const/16 v0, 0x4b

    iget-object v1, p0, Lern;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 100
    :cond_d
    iget-object v0, p0, Lern;->p:Lepu;

    if-eqz v0, :cond_e

    .line 101
    const/16 v0, 0xb9

    iget-object v1, p0, Lern;->p:Lepu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 103
    :cond_e
    iget-object v0, p0, Lern;->q:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 104
    const/16 v0, 0xfe

    iget-object v1, p0, Lern;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 106
    :cond_f
    iget-object v0, p0, Lern;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 108
    return-void
.end method
