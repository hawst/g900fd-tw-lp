.class public final Ldhc;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldhc;


# instance fields
.field public b:[Ljava/lang/Integer;

.field public c:[Ljava/lang/Integer;

.field public d:[Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6315
    const/4 v0, 0x0

    new-array v0, v0, [Ldhc;

    sput-object v0, Ldhc;->a:[Ldhc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6316
    invoke-direct {p0}, Lepn;-><init>()V

    .line 6319
    sget-object v0, Lept;->m:[Ljava/lang/Integer;

    iput-object v0, p0, Ldhc;->b:[Ljava/lang/Integer;

    .line 6322
    sget-object v0, Lept;->m:[Ljava/lang/Integer;

    iput-object v0, p0, Ldhc;->c:[Ljava/lang/Integer;

    .line 6325
    sget-object v0, Lept;->m:[Ljava/lang/Integer;

    iput-object v0, p0, Ldhc;->d:[Ljava/lang/Integer;

    .line 6316
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 6352
    iget-object v0, p0, Ldhc;->b:[Ljava/lang/Integer;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ldhc;->b:[Ljava/lang/Integer;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 6354
    iget-object v3, p0, Ldhc;->b:[Ljava/lang/Integer;

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 6356
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 6354
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6359
    :cond_0
    iget-object v0, p0, Ldhc;->b:[Ljava/lang/Integer;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    .line 6361
    :goto_1
    iget-object v2, p0, Ldhc;->c:[Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ldhc;->c:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 6363
    iget-object v4, p0, Ldhc;->c:[Ljava/lang/Integer;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 6365
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Lepl;->f(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 6363
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 6367
    :cond_1
    add-int/2addr v0, v3

    .line 6368
    iget-object v2, p0, Ldhc;->c:[Ljava/lang/Integer;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 6370
    :cond_2
    iget-object v2, p0, Ldhc;->d:[Ljava/lang/Integer;

    if-eqz v2, :cond_4

    iget-object v2, p0, Ldhc;->d:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 6372
    iget-object v3, p0, Ldhc;->d:[Ljava/lang/Integer;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    .line 6374
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 6372
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 6376
    :cond_3
    add-int/2addr v0, v2

    .line 6377
    iget-object v1, p0, Ldhc;->d:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6379
    :cond_4
    iget-object v1, p0, Ldhc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6380
    iput v0, p0, Ldhc;->cachedSize:I

    .line 6381
    return v0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 6312
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldhc;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldhc;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldhc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldhc;->b:[Ljava/lang/Integer;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    iget-object v2, p0, Ldhc;->b:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldhc;->b:[Ljava/lang/Integer;

    :goto_1
    iget-object v1, p0, Ldhc;->b:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Ldhc;->b:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Ldhc;->b:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldhc;->c:[Ljava/lang/Integer;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    iget-object v2, p0, Ldhc;->c:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldhc;->c:[Ljava/lang/Integer;

    :goto_2
    iget-object v1, p0, Ldhc;->c:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Ldhc;->c:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v1, p0, Ldhc;->c:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldhc;->d:[Ljava/lang/Integer;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    iget-object v2, p0, Ldhc;->d:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldhc;->d:[Ljava/lang/Integer;

    :goto_3
    iget-object v1, p0, Ldhc;->d:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Ldhc;->d:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    iget-object v1, p0, Ldhc;->d:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 6330
    iget-object v1, p0, Ldhc;->b:[Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 6331
    iget-object v2, p0, Ldhc;->b:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 6332
    const/4 v5, 0x1

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v5, v4}, Lepl;->a(II)V

    .line 6331
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 6335
    :cond_0
    iget-object v1, p0, Ldhc;->c:[Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 6336
    iget-object v2, p0, Ldhc;->c:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 6337
    const/4 v5, 0x2

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v5, v4}, Lepl;->a(II)V

    .line 6336
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6340
    :cond_1
    iget-object v1, p0, Ldhc;->d:[Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 6341
    iget-object v1, p0, Ldhc;->d:[Ljava/lang/Integer;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 6342
    const/4 v4, 0x3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 6341
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 6345
    :cond_2
    iget-object v0, p0, Ldhc;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 6347
    return-void
.end method
