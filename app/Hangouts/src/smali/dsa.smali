.class public final Ldsa;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldsa;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2331
    const/4 v0, 0x0

    new-array v0, v0, [Ldsa;

    sput-object v0, Ldsa;->a:[Ldsa;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2332
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2363
    const/4 v0, 0x0

    .line 2364
    iget-object v1, p0, Ldsa;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2365
    const/4 v0, 0x1

    iget-object v1, p0, Ldsa;->c:Ljava/lang/String;

    .line 2366
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2368
    :cond_0
    iget-object v1, p0, Ldsa;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 2369
    const/4 v1, 0x2

    iget-object v2, p0, Ldsa;->d:Ljava/lang/Boolean;

    .line 2370
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2372
    :cond_1
    iget-object v1, p0, Ldsa;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2373
    const/4 v1, 0x3

    iget-object v2, p0, Ldsa;->b:Ljava/lang/String;

    .line 2374
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2376
    :cond_2
    iget-object v1, p0, Ldsa;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 2377
    const/4 v1, 0x4

    iget-object v2, p0, Ldsa;->e:Ljava/lang/Boolean;

    .line 2378
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2380
    :cond_3
    iget-object v1, p0, Ldsa;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2381
    iput v0, p0, Ldsa;->cachedSize:I

    .line 2382
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 2328
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldsa;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldsa;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldsa;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldsa;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldsa;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldsa;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldsa;->e:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 2345
    iget-object v0, p0, Ldsa;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2346
    const/4 v0, 0x1

    iget-object v1, p0, Ldsa;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 2348
    :cond_0
    iget-object v0, p0, Ldsa;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 2349
    const/4 v0, 0x2

    iget-object v1, p0, Ldsa;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 2351
    :cond_1
    iget-object v0, p0, Ldsa;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2352
    const/4 v0, 0x3

    iget-object v1, p0, Ldsa;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 2354
    :cond_2
    iget-object v0, p0, Ldsa;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 2355
    const/4 v0, 0x4

    iget-object v1, p0, Ldsa;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 2357
    :cond_3
    iget-object v0, p0, Ldsa;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 2359
    return-void
.end method
