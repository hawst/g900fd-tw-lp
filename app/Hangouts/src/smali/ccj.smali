.class public final Lccj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/views/FadeImageView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/views/FadeImageView;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lccj;->a:Lcom/google/android/apps/hangouts/views/FadeImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lccj;->a:Lcom/google/android/apps/hangouts/views/FadeImageView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/FadeImageView;->a(Lcom/google/android/apps/hangouts/views/FadeImageView;)Landroid/view/animation/Animation;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 61
    iget-object v0, p0, Lccj;->a:Lcom/google/android/apps/hangouts/views/FadeImageView;

    iget-object v1, p0, Lccj;->a:Lcom/google/android/apps/hangouts/views/FadeImageView;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/FadeImageView;->b(Lcom/google/android/apps/hangouts/views/FadeImageView;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/FadeImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 62
    iget-object v0, p0, Lccj;->a:Lcom/google/android/apps/hangouts/views/FadeImageView;

    iget-object v1, p0, Lccj;->a:Lcom/google/android/apps/hangouts/views/FadeImageView;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/FadeImageView;->c(Lcom/google/android/apps/hangouts/views/FadeImageView;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/FadeImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 63
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 55
    return-void
.end method
