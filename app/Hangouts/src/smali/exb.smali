.class public final Lexb;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lexb;


# instance fields
.field public b:Letw;

.field public c:[Levu;

.field public d:Levu;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lexb;

    sput-object v0, Lexb;->a:[Lexb;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 13
    iput-object v1, p0, Lexb;->b:Letw;

    .line 16
    sget-object v0, Levu;->a:[Levu;

    iput-object v0, p0, Lexb;->c:[Levu;

    .line 19
    iput-object v1, p0, Lexb;->d:Levu;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 54
    iget-object v0, p0, Lexb;->b:Letw;

    if-eqz v0, :cond_5

    .line 55
    const/4 v0, 0x1

    iget-object v2, p0, Lexb;->b:Letw;

    .line 56
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 58
    :goto_0
    iget-object v2, p0, Lexb;->c:[Levu;

    if-eqz v2, :cond_1

    .line 59
    iget-object v2, p0, Lexb;->c:[Levu;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 60
    if-eqz v4, :cond_0

    .line 61
    const/4 v5, 0x2

    .line 62
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 59
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 66
    :cond_1
    iget-object v1, p0, Lexb;->d:Levu;

    if-eqz v1, :cond_2

    .line 67
    const/4 v1, 0x3

    iget-object v2, p0, Lexb;->d:Levu;

    .line 68
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    :cond_2
    iget-object v1, p0, Lexb;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 71
    const/4 v1, 0x4

    iget-object v2, p0, Lexb;->e:Ljava/lang/String;

    .line 72
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    :cond_3
    iget-object v1, p0, Lexb;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 75
    const/4 v1, 0x5

    iget-object v2, p0, Lexb;->f:Ljava/lang/String;

    .line 76
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    :cond_4
    iget-object v1, p0, Lexb;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    iput v0, p0, Lexb;->cachedSize:I

    .line 80
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lexb;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lexb;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lexb;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lexb;->b:Letw;

    if-nez v0, :cond_2

    new-instance v0, Letw;

    invoke-direct {v0}, Letw;-><init>()V

    iput-object v0, p0, Lexb;->b:Letw;

    :cond_2
    iget-object v0, p0, Lexb;->b:Letw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lexb;->c:[Levu;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Levu;

    iget-object v3, p0, Lexb;->c:[Levu;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lexb;->c:[Levu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lexb;->c:[Levu;

    :goto_2
    iget-object v2, p0, Lexb;->c:[Levu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lexb;->c:[Levu;

    new-instance v3, Levu;

    invoke-direct {v3}, Levu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lexb;->c:[Levu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lexb;->c:[Levu;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lexb;->c:[Levu;

    new-instance v3, Levu;

    invoke-direct {v3}, Levu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lexb;->c:[Levu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lexb;->d:Levu;

    if-nez v0, :cond_6

    new-instance v0, Levu;

    invoke-direct {v0}, Levu;-><init>()V

    iput-object v0, p0, Lexb;->d:Levu;

    :cond_6
    iget-object v0, p0, Lexb;->d:Levu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexb;->e:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexb;->f:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 28
    iget-object v0, p0, Lexb;->b:Letw;

    if-eqz v0, :cond_0

    .line 29
    const/4 v0, 0x1

    iget-object v1, p0, Lexb;->b:Letw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 31
    :cond_0
    iget-object v0, p0, Lexb;->c:[Levu;

    if-eqz v0, :cond_2

    .line 32
    iget-object v1, p0, Lexb;->c:[Levu;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 33
    if-eqz v3, :cond_1

    .line 34
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 32
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 38
    :cond_2
    iget-object v0, p0, Lexb;->d:Levu;

    if-eqz v0, :cond_3

    .line 39
    const/4 v0, 0x3

    iget-object v1, p0, Lexb;->d:Levu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 41
    :cond_3
    iget-object v0, p0, Lexb;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 42
    const/4 v0, 0x4

    iget-object v1, p0, Lexb;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 44
    :cond_4
    iget-object v0, p0, Lexb;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 45
    const/4 v0, 0x5

    iget-object v1, p0, Lexb;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 47
    :cond_5
    iget-object v0, p0, Lexb;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 49
    return-void
.end method
