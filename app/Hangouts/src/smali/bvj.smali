.class public final Lbvj;
.super Lbvh;
.source "PG"


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:I

.field public static final c:I

.field public static final d:I

.field public static final e:I

.field public static final f:I

.field public static final g:I

.field public static final h:I

.field public static final i:I

.field public static final j:I

.field public static final k:I

.field public static final l:I

.field private static x:I


# instance fields
.field private A:I

.field private B:I

.field private C:Ljava/lang/String;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private F:Z

.field public m:J

.field public n:I

.field public o:Ljava/lang/String;

.field public p:I

.field public q:J

.field public r:J

.field public s:I

.field public t:I

.field public u:Z

.field public v:Z

.field public w:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbvk;",
            ">;"
        }
    .end annotation
.end field

.field private y:J

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 175
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "msg_box"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "sub"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "sub_cs"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "m_size"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "date"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "thread_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "pri"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "st"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "read"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "seen"

    aput-object v2, v0, v1

    sput-object v0, Lbvj;->a:[Ljava/lang/String;

    .line 189
    sput v3, Lbvj;->x:I

    sput v4, Lbvj;->x:I

    sput v3, Lbvj;->b:I

    .line 190
    sget v0, Lbvj;->x:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbvj;->x:I

    sput v0, Lbvj;->c:I

    .line 191
    sget v0, Lbvj;->x:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbvj;->x:I

    sput v0, Lbvj;->d:I

    .line 192
    sget v0, Lbvj;->x:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbvj;->x:I

    sput v0, Lbvj;->e:I

    .line 193
    sget v0, Lbvj;->x:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbvj;->x:I

    sput v0, Lbvj;->f:I

    .line 194
    sget v0, Lbvj;->x:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbvj;->x:I

    sput v0, Lbvj;->g:I

    .line 195
    sget v0, Lbvj;->x:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbvj;->x:I

    sput v0, Lbvj;->h:I

    .line 196
    sget v0, Lbvj;->x:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbvj;->x:I

    sput v0, Lbvj;->i:I

    .line 197
    sget v0, Lbvj;->x:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbvj;->x:I

    sput v0, Lbvj;->j:I

    .line 198
    sget v0, Lbvj;->x:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbvj;->x:I

    sput v0, Lbvj;->k:I

    .line 199
    sget v0, Lbvj;->x:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbvj;->x:I

    sput v0, Lbvj;->l:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 174
    invoke-direct {p0}, Lbvh;-><init>()V

    .line 213
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbvj;->w:Ljava/util/List;

    .line 214
    iput-boolean v1, p0, Lbvj;->z:Z

    .line 215
    iput v1, p0, Lbvj;->A:I

    .line 216
    iput v1, p0, Lbvj;->B:I

    .line 217
    iput-object v2, p0, Lbvj;->C:Ljava/lang/String;

    .line 218
    iput-object v2, p0, Lbvj;->D:Ljava/lang/String;

    .line 219
    iput-object v2, p0, Lbvj;->E:Ljava/lang/String;

    .line 220
    iput-boolean v1, p0, Lbvj;->F:Z

    return-void
.end method

.method public static a(Landroid/database/Cursor;)Lbvj;
    .locals 9

    .prologue
    const-wide/16 v7, 0x3e8

    const/4 v2, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 260
    new-instance v3, Lbvj;

    invoke-direct {v3}, Lbvj;-><init>()V

    .line 261
    sget v0, Lbvj;->b:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v3, Lbvj;->m:J

    sget v0, Lbvj;->c:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v3, Lbvj;->n:I

    sget v0, Lbvj;->d:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lbvj;->o:Ljava/lang/String;

    sget v0, Lbvj;->e:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v3, Lbvj;->p:I

    iget-object v0, v3, Lbvj;->o:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v3, Lbvj;->o:Ljava/lang/String;

    const/4 v4, 0x4

    invoke-static {v0, v4}, Lf;->b(Ljava/lang/String;I)[B

    move-result-object v0

    iget v4, v3, Lbvj;->p:I

    invoke-static {v0, v4}, Lf;->a([BI)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lbvj;->o:Ljava/lang/String;

    :cond_0
    sget v0, Lbvj;->f:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v3, Lbvj;->y:J

    sget v0, Lbvj;->g:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    mul-long/2addr v4, v7

    mul-long/2addr v4, v7

    iput-wide v4, v3, Lbvj;->q:J

    sget v0, Lbvj;->h:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v3, Lbvj;->r:J

    sget v0, Lbvj;->i:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v3, Lbvj;->s:I

    sget v0, Lbvj;->j:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v3, Lbvj;->t:I

    sget v0, Lbvj;->k:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, v3, Lbvj;->u:Z

    sget v0, Lbvj;->l:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v2, v1

    :cond_1
    iput-boolean v2, v3, Lbvj;->v:Z

    iget-object v0, v3, Lbvj;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iput-boolean v1, v3, Lbvj;->z:Z

    iput v1, v3, Lbvj;->A:I

    iput v1, v3, Lbvj;->B:I

    iput-object v6, v3, Lbvj;->C:Ljava/lang/String;

    iput-object v6, v3, Lbvj;->D:Ljava/lang/String;

    iput-object v6, v3, Lbvj;->E:Ljava/lang/String;

    iput-boolean v1, v3, Lbvj;->F:Z

    .line 262
    return-object v3

    :cond_2
    move v0, v2

    .line 261
    goto :goto_0
.end method

.method private m()V
    .locals 14

    .prologue
    .line 365
    iget-boolean v0, p0, Lbvj;->z:Z

    if-eqz v0, :cond_1

    .line 446
    :cond_0
    :goto_0
    return-void

    .line 368
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbvj;->z:Z

    .line 369
    const/4 v5, 0x0

    .line 370
    const/4 v4, 0x0

    .line 373
    const/4 v3, 0x0

    .line 374
    const/4 v2, 0x0

    .line 375
    const-wide/16 v0, 0x0

    .line 376
    iget-object v6, p0, Lbvj;->w:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-wide v12, v0

    move v1, v2

    move v2, v3

    move-object v3, v4

    move-object v4, v5

    move-wide v5, v12

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvk;

    .line 377
    invoke-virtual {v0}, Lbvk;->a()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 378
    iget v8, p0, Lbvj;->A:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lbvj;->A:I

    .line 381
    iget-object v8, p0, Lbvj;->C:Ljava/lang/String;

    if-nez v8, :cond_3

    .line 383
    iget-object v8, v0, Lbvk;->j:Ljava/lang/String;

    iput-object v8, p0, Lbvj;->C:Ljava/lang/String;

    .line 431
    :cond_2
    :goto_2
    iget-wide v8, v0, Lbvk;->n:J

    add-long/2addr v5, v8

    .line 432
    goto :goto_1

    .line 387
    :cond_3
    if-nez v4, :cond_4

    .line 388
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 389
    iget-object v8, p0, Lbvj;->C:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 390
    iget-object v8, p0, Lbvj;->C:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 393
    :cond_4
    iget-object v8, v0, Lbvk;->j:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 394
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_5

    .line 395
    const-string v8, " "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 397
    :cond_5
    iget-object v8, v0, Lbvk;->j:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 400
    :cond_6
    invoke-virtual {v0}, Lbvk;->b()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 401
    iget v8, p0, Lbvj;->B:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lbvj;->B:I

    .line 402
    invoke-virtual {v0}, Lbvk;->c()Z

    move-result v8

    if-eqz v8, :cond_7

    .line 403
    const/4 v8, 0x1

    iput-boolean v8, p0, Lbvj;->F:Z

    .line 405
    :cond_7
    iget-object v8, p0, Lbvj;->D:Ljava/lang/String;

    if-nez v8, :cond_8

    .line 407
    invoke-virtual {v0}, Lbvk;->d()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbvj;->D:Ljava/lang/String;

    .line 408
    iget-object v1, v0, Lbvk;->i:Ljava/lang/String;

    iput-object v1, p0, Lbvj;->E:Ljava/lang/String;

    .line 409
    iget v2, v0, Lbvk;->l:I

    .line 410
    iget v1, v0, Lbvk;->m:I

    goto :goto_2

    .line 414
    :cond_8
    if-nez v3, :cond_9

    .line 415
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 416
    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-object v10, p0, Lbvj;->D:Ljava/lang/String;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, p0, Lbvj;->E:Ljava/lang/String;

    aput-object v10, v8, v9

    const/4 v9, 0x2

    .line 419
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    .line 420
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    .line 416
    invoke-static {v8}, Lf;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422
    :cond_9
    const/16 v8, 0x7c

    .line 423
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x4

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    .line 425
    invoke-virtual {v0}, Lbvk;->d()Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v11}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    iget-object v11, v0, Lbvk;->i:Ljava/lang/String;

    aput-object v11, v9, v10

    const/4 v10, 0x2

    iget v11, v0, Lbvk;->l:I

    .line 427
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x3

    iget v11, v0, Lbvk;->m:I

    .line 428
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    .line 424
    invoke-static {v9}, Lf;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 433
    :cond_a
    iget v0, p0, Lbvj;->B:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_b

    .line 434
    const-string v0, "multipart/mixed"

    iput-object v0, p0, Lbvj;->E:Ljava/lang/String;

    .line 436
    :cond_b
    if-eqz v4, :cond_c

    .line 437
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbvj;->C:Ljava/lang/String;

    .line 439
    :cond_c
    if-eqz v3, :cond_d

    .line 440
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbvj;->D:Ljava/lang/String;

    .line 442
    :cond_d
    iget-wide v0, p0, Lbvj;->y:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 443
    iget-object v0, p0, Lbvj;->o:Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lbvj;->o:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    array-length v0, v0

    int-to-long v0, v0

    :goto_3
    iput-wide v0, p0, Lbvj;->y:J

    .line 444
    iget-wide v0, p0, Lbvj;->y:J

    add-long/2addr v0, v5

    iput-wide v0, p0, Lbvj;->y:J

    goto/16 :goto_0

    .line 443
    :cond_e
    const-wide/16 v0, 0x0

    goto :goto_3
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 455
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lbvk;)V
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lbvj;->w:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 270
    return-void
.end method

.method public b()J
    .locals 2

    .prologue
    .line 450
    iget-wide v0, p0, Lbvj;->m:J

    return-wide v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 460
    iget-wide v0, p0, Lbvj;->q:J

    return-wide v0
.end method

.method public d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbvk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 273
    iget-object v0, p0, Lbvj;->w:Ljava/util/List;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 277
    iget-boolean v0, p0, Lbvj;->z:Z

    if-nez v0, :cond_0

    .line 278
    invoke-direct {p0}, Lbvj;->m()V

    .line 280
    :cond_0
    iget-object v0, p0, Lbvj;->C:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 281
    const-string v0, ""

    .line 283
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lbvj;->C:Ljava/lang/String;

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 287
    iget-boolean v0, p0, Lbvj;->z:Z

    if-nez v0, :cond_0

    .line 288
    invoke-direct {p0}, Lbvj;->m()V

    .line 290
    :cond_0
    iget-object v0, p0, Lbvj;->D:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 294
    iget-boolean v0, p0, Lbvj;->z:Z

    if-nez v0, :cond_0

    .line 295
    invoke-direct {p0}, Lbvj;->m()V

    .line 297
    :cond_0
    iget-object v0, p0, Lbvj;->E:Ljava/lang/String;

    return-object v0
.end method

.method public h()J
    .locals 2

    .prologue
    .line 301
    iget-boolean v0, p0, Lbvj;->z:Z

    if-nez v0, :cond_0

    .line 302
    invoke-direct {p0}, Lbvj;->m()V

    .line 304
    :cond_0
    iget-wide v0, p0, Lbvj;->y:J

    return-wide v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 308
    iget-boolean v0, p0, Lbvj;->z:Z

    if-nez v0, :cond_0

    .line 309
    invoke-direct {p0}, Lbvj;->m()V

    .line 311
    :cond_0
    iget-boolean v0, p0, Lbvj;->F:Z

    return v0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 315
    iget-boolean v0, p0, Lbvj;->z:Z

    if-nez v0, :cond_0

    .line 316
    invoke-direct {p0}, Lbvj;->m()V

    .line 318
    :cond_0
    iget v0, p0, Lbvj;->A:I

    return v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 322
    iget-boolean v0, p0, Lbvj;->z:Z

    if-nez v0, :cond_0

    .line 323
    invoke-direct {p0}, Lbvj;->m()V

    .line 325
    :cond_0
    iget v0, p0, Lbvj;->B:I

    return v0
.end method

.method public l()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbiw;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v10, 0x0

    .line 332
    iget-boolean v0, p0, Lbvj;->z:Z

    if-nez v0, :cond_0

    .line 333
    invoke-direct {p0}, Lbvj;->m()V

    .line 336
    :cond_0
    iget v0, p0, Lbvj;->B:I

    if-lez v0, :cond_3

    .line 337
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 338
    iget-object v0, p0, Lbvj;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lbvk;

    .line 339
    invoke-virtual {v6}, Lbvk;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 340
    new-instance v0, Lbiy;

    new-array v1, v7, [I

    aput v10, v1, v10

    .line 345
    invoke-virtual {v6}, Lbvk;->d()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    iget v4, v6, Lbvk;->l:I

    iget v5, v6, Lbvk;->m:I

    iget-object v6, v6, Lbvk;->i:Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Lbiy;-><init>([ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    .line 340
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move-object v2, v8

    .line 357
    :cond_3
    return-object v2
.end method
