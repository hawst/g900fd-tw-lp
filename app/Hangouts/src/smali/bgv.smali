.class public Lbgv;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ldyz;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3673
    invoke-direct {p0}, Lbfz;-><init>()V

    .line 3674
    iget-object v0, p1, Ldyz;->c:Ldyq;

    if-eqz v0, :cond_3

    .line 3675
    iget-object v0, p1, Ldyz;->c:Ldyq;

    .line 3676
    iget-object v1, v0, Ldyq;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 3677
    iget-object v1, v0, Ldyq;->b:Ljava/lang/String;

    iput-object v1, p0, Lbgv;->g:Ljava/lang/String;

    .line 3678
    iget-object v1, v0, Ldyq;->j:Ldqf;

    if-eqz v1, :cond_1

    .line 3679
    iget-object v0, v0, Ldyq;->j:Ldqf;

    iget-object v0, v0, Ldqf;->b:Ljava/lang/String;

    iput-object v0, p0, Lbgv;->h:Ljava/lang/String;

    .line 3694
    :goto_0
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 3695
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetHangoutInfoResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3697
    :cond_0
    return-void

    .line 3681
    :cond_1
    const-string v0, "Hangout has no associated conversation"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    .line 3682
    iput-object v2, p0, Lbgv;->h:Ljava/lang/String;

    goto :goto_0

    .line 3685
    :cond_2
    const-string v0, "ServerResponse has Hangout but no hangoutId"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    .line 3686
    :cond_3
    iput-object v2, p0, Lbgv;->g:Ljava/lang/String;

    .line 3692
    iput-object v2, p0, Lbgv;->h:Ljava/lang/String;

    goto :goto_0
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 3701
    new-instance v0, Ldyz;

    invoke-direct {v0}, Ldyz;-><init>()V

    .line 3702
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldyz;

    .line 3703
    iget-object v1, v0, Ldyz;->b:Ldvn;

    invoke-static {v1}, Lbgv;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3704
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldyz;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 3706
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbgv;

    invoke-direct {v1, v0}, Lbgv;-><init>(Ldyz;)V

    move-object v0, v1

    goto :goto_0
.end method
