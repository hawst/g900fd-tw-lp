.class public final Lbue;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Lyj;

.field b:Landroid/app/Activity;

.field private final c:Lbtz;

.field private d:Ljava/lang/String;

.field private e:Landroid/content/res/Resources;

.field private f:Lach;

.field private g:Laak;

.field private final h:Landroid/preference/Preference$OnPreferenceChangeListener;


# direct methods
.method public constructor <init>(Lbtz;)V
    .locals 1

    .prologue
    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lbuf;

    invoke-direct {v0, p0}, Lbuf;-><init>(Lbue;)V

    iput-object v0, p0, Lbue;->g:Laak;

    .line 70
    new-instance v0, Lbuh;

    invoke-direct {v0, p0}, Lbuh;-><init>(Lbue;)V

    iput-object v0, p0, Lbue;->h:Landroid/preference/Preference$OnPreferenceChangeListener;

    .line 158
    iput-object p1, p0, Lbue;->c:Lbtz;

    .line 159
    iget-object v0, p0, Lbue;->c:Lbtz;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lbue;->c:Lbtz;

    invoke-interface {v0}, Lbtz;->c()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lbue;->b:Landroid/app/Activity;

    .line 161
    iget-object v0, p0, Lbue;->b:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lbue;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lbue;->e:Landroid/content/res/Resources;

    .line 165
    :cond_0
    return-void
.end method

.method private a(Landroid/preference/ListPreference;)V
    .locals 1

    .prologue
    .line 222
    if-eqz p1, :cond_0

    .line 223
    invoke-virtual {p1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 224
    iget-object v0, p0, Lbue;->h:Landroid/preference/Preference$OnPreferenceChangeListener;

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 226
    :cond_0
    return-void
.end method

.method static synthetic a(Lbue;Landroid/preference/ListPreference;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 44
    invoke-virtual {p1}, Landroid/preference/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbue;->e:Landroid/content/res/Resources;

    sget v2, Lh;->ak:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v4, Laaj;

    invoke-virtual {p1}, Landroid/preference/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0, v3, v3}, Laaj;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    if-nez v4, :cond_1

    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ChatAclSettingsUi.setRemoteChatAclSetting: invalid preference key "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/preference/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/preference/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Laai;->a(Ljava/lang/String;)Laaj;

    move-result-object v4

    goto :goto_0

    :cond_1
    iput-object p2, v4, Laaj;->d:Ljava/lang/String;

    new-instance v0, Lace;

    iget-object v1, p0, Lbue;->b:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lace;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lbue;->f:Lach;

    new-instance v0, Lbuj;

    iget-object v1, p0, Lbue;->b:Landroid/app/Activity;

    iget-object v2, p0, Lbue;->a:Lyj;

    invoke-virtual {p1}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v5

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lbuj;-><init>(Landroid/content/Context;Lyj;Landroid/preference/ListPreference;Laaj;Ljava/lang/String;)V

    iget-object v1, p0, Lbue;->f:Lach;

    invoke-static {v0, v1}, Lacc;->a(Laci;Lach;)Lacc;

    move-result-object v0

    invoke-virtual {v0}, Lacc;->a()V

    goto :goto_1
.end method

.method private e()Z
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lbue;->d:Ljava/lang/String;

    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    iput-object v0, p0, Lbue;->a:Lyj;

    .line 195
    iget-object v0, p0, Lbue;->a:Lyj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbue;->a:Lyj;

    invoke-static {v0}, Lbkb;->f(Lyj;)I

    move-result v0

    const/16 v1, 0x66

    if-eq v0, v1, :cond_1

    .line 197
    :cond_0
    const-string v0, "Babel"

    const-string v1, "Chat acl settings page resumed with invalid account. Go to Babel home"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lbue;->b:Landroid/app/Activity;

    invoke-static {v0}, Lbtf;->a(Landroid/app/Activity;)V

    .line 199
    const/4 v0, 0x0

    .line 202
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 190
    invoke-direct {p0}, Lbue;->e()Z

    .line 191
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lbue;->b:Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 169
    const-string v0, "Babel"

    const-string v1, "ChatAclSettingsUi: empty context. Abort."

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    if-nez p1, :cond_2

    .line 173
    const-string v0, "Babel"

    const-string v1, "ChatAclSettingsUi: no account specified. Abort."

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    iget-object v0, p0, Lbue;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 177
    :cond_2
    iput-object p1, p0, Lbue;->d:Ljava/lang/String;

    .line 178
    invoke-direct {p0}, Lbue;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lbue;->c:Lbtz;

    iget-object v1, p0, Lbue;->a:Lyj;

    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lf;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/String;)V

    .line 183
    iget-object v0, p0, Lbue;->g:Laak;

    invoke-static {v0}, Laai;->a(Laak;)V

    .line 184
    iget-object v0, p0, Lbue;->a:Lyj;

    invoke-static {v0}, Laai;->a(Lyj;)V

    .line 185
    iget-object v0, p0, Lbue;->c:Lbtz;

    sget v1, Lf;->ic:I

    invoke-interface {v0, v1}, Lbtz;->a(I)V

    .line 186
    invoke-virtual {p0}, Lbue;->d()V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lbue;->a:Lyj;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lbue;->g:Laak;

    invoke-static {v0}, Laai;->b(Laak;)V

    .line 209
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lbue;->f:Lach;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lbue;->f:Lach;

    invoke-interface {v0}, Lach;->a()V

    .line 214
    const/4 v0, 0x0

    iput-object v0, p0, Lbue;->f:Lach;

    .line 216
    :cond_0
    return-void
.end method

.method d()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 309
    iget-object v0, p0, Lbue;->e:Landroid/content/res/Resources;

    sget v1, Lh;->ak:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 311
    iget-object v1, p0, Lbue;->c:Lbtz;

    invoke-interface {v1, v0}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    .line 312
    if-eqz v0, :cond_0

    .line 313
    iget-object v1, p0, Lbue;->a:Lyj;

    invoke-static {v1}, Laai;->c(Lyj;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 314
    invoke-direct {p0, v0}, Lbue;->a(Landroid/preference/ListPreference;)V

    .line 317
    :cond_0
    iget-object v0, p0, Lbue;->c:Lbtz;

    iget-object v1, p0, Lbue;->e:Landroid/content/res/Resources;

    sget v2, Lh;->oZ:I

    .line 318
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 317
    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 320
    if-eqz v0, :cond_1

    .line 321
    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    .line 322
    if-nez v0, :cond_2

    const-string v0, "Babel"

    const-string v1, "ChatAclSettingsUi.loadCirclePreferences: circle category not found"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    :cond_1
    :goto_0
    return-void

    .line 322
    :cond_2
    iget-object v1, p0, Lbue;->a:Lyj;

    invoke-static {v1}, Laai;->b(Lyj;)Ljava/util/List;

    move-result-object v4

    new-instance v1, Lbui;

    invoke-direct {v1, p0}, Lbui;-><init>(Lbue;)V

    invoke-static {v4, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const-string v1, "babel_invite_settings_off_mode"

    invoke-static {v1, v7}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lbue;->e:Landroid/content/res/Resources;

    sget v2, Lf;->bk:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lbue;->e:Landroid/content/res/Resources;

    sget v3, Lf;->bm:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    move-object v3, v2

    move-object v2, v1

    :goto_1
    if-eqz v3, :cond_3

    if-nez v2, :cond_5

    :cond_3
    const-string v0, "Babel"

    const-string v1, "ChatAclSettingsUi.loadCirclePreferences:Can not load preference entries or values from resources"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lbue;->e:Landroid/content/res/Resources;

    sget v2, Lf;->bj:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lbue;->e:Landroid/content/res/Resources;

    sget v3, Lf;->bl:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    move-object v3, v2

    move-object v2, v1

    goto :goto_1

    :cond_5
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_6

    iget-object v0, p0, Lbue;->c:Lbtz;

    iget-object v1, p0, Lbue;->e:Landroid/content/res/Resources;

    sget v2, Lh;->oZ:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    iget-object v0, p0, Lbue;->c:Lbtz;

    iget-object v1, p0, Lbue;->c:Lbtz;

    iget-object v2, p0, Lbue;->e:Landroid/content/res/Resources;

    sget v3, Lh;->oZ:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lbtz;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-interface {v0, v1}, Lbtz;->a(Landroid/preference/Preference;)V

    goto :goto_0

    :cond_6
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laaj;

    new-instance v5, Landroid/preference/ListPreference;

    iget-object v6, p0, Lbue;->b:Landroid/app/Activity;

    invoke-direct {v5, v6}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v5}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    iget-object v6, v1, Laaj;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/preference/ListPreference;->setKey(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    invoke-virtual {v5, v2}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    iget-object v6, v1, Laaj;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v6, v1, Laaj;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v5, v7}, Landroid/preference/ListPreference;->setPersistent(Z)V

    iget-object v1, v1, Laaj;->d:Ljava/lang/String;

    invoke-virtual {v5, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    invoke-direct {p0, v5}, Lbue;->a(Landroid/preference/ListPreference;)V

    goto :goto_2
.end method
