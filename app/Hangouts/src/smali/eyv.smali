.class public final Leyv;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leyv;


# instance fields
.field public b:Leyy;

.field public c:Leyy;

.field public d:Leyz;

.field public e:Leyz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x0

    new-array v0, v0, [Leyv;

    sput-object v0, Leyv;->a:[Leyv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 175
    invoke-direct {p0}, Lepn;-><init>()V

    .line 178
    iput-object v0, p0, Leyv;->b:Leyy;

    .line 181
    iput-object v0, p0, Leyv;->c:Leyy;

    .line 184
    iput-object v0, p0, Leyv;->d:Leyz;

    .line 187
    iput-object v0, p0, Leyv;->e:Leyz;

    .line 175
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 210
    const/4 v0, 0x0

    .line 211
    iget-object v1, p0, Leyv;->b:Leyy;

    if-eqz v1, :cond_0

    .line 212
    const/4 v0, 0x1

    iget-object v1, p0, Leyv;->b:Leyy;

    .line 213
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 215
    :cond_0
    iget-object v1, p0, Leyv;->c:Leyy;

    if-eqz v1, :cond_1

    .line 216
    const/4 v1, 0x2

    iget-object v2, p0, Leyv;->c:Leyy;

    .line 217
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 219
    :cond_1
    iget-object v1, p0, Leyv;->d:Leyz;

    if-eqz v1, :cond_2

    .line 220
    const/4 v1, 0x3

    iget-object v2, p0, Leyv;->d:Leyz;

    .line 221
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 223
    :cond_2
    iget-object v1, p0, Leyv;->e:Leyz;

    if-eqz v1, :cond_3

    .line 224
    const/4 v1, 0x4

    iget-object v2, p0, Leyv;->e:Leyz;

    .line 225
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 227
    :cond_3
    iget-object v1, p0, Leyv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 228
    iput v0, p0, Leyv;->cachedSize:I

    .line 229
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 171
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leyv;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leyv;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leyv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Leyv;->b:Leyy;

    if-nez v0, :cond_2

    new-instance v0, Leyy;

    invoke-direct {v0}, Leyy;-><init>()V

    iput-object v0, p0, Leyv;->b:Leyy;

    :cond_2
    iget-object v0, p0, Leyv;->b:Leyy;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Leyv;->c:Leyy;

    if-nez v0, :cond_3

    new-instance v0, Leyy;

    invoke-direct {v0}, Leyy;-><init>()V

    iput-object v0, p0, Leyv;->c:Leyy;

    :cond_3
    iget-object v0, p0, Leyv;->c:Leyy;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Leyv;->d:Leyz;

    if-nez v0, :cond_4

    new-instance v0, Leyz;

    invoke-direct {v0}, Leyz;-><init>()V

    iput-object v0, p0, Leyv;->d:Leyz;

    :cond_4
    iget-object v0, p0, Leyv;->d:Leyz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Leyv;->e:Leyz;

    if-nez v0, :cond_5

    new-instance v0, Leyz;

    invoke-direct {v0}, Leyz;-><init>()V

    iput-object v0, p0, Leyv;->e:Leyz;

    :cond_5
    iget-object v0, p0, Leyv;->e:Leyz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Leyv;->b:Leyy;

    if-eqz v0, :cond_0

    .line 193
    const/4 v0, 0x1

    iget-object v1, p0, Leyv;->b:Leyy;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 195
    :cond_0
    iget-object v0, p0, Leyv;->c:Leyy;

    if-eqz v0, :cond_1

    .line 196
    const/4 v0, 0x2

    iget-object v1, p0, Leyv;->c:Leyy;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 198
    :cond_1
    iget-object v0, p0, Leyv;->d:Leyz;

    if-eqz v0, :cond_2

    .line 199
    const/4 v0, 0x3

    iget-object v1, p0, Leyv;->d:Leyz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 201
    :cond_2
    iget-object v0, p0, Leyv;->e:Leyz;

    if-eqz v0, :cond_3

    .line 202
    const/4 v0, 0x4

    iget-object v1, p0, Leyv;->e:Leyz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 204
    :cond_3
    iget-object v0, p0, Leyv;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 206
    return-void
.end method
