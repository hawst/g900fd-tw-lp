.class public final Lbkn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lbkn;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lbkn;


# instance fields
.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 41
    new-instance v0, Lbkn;

    const/4 v1, 0x1

    const/4 v4, 0x0

    const/16 v8, 0x3d

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v8}, Lbkn;-><init>(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lbkn;->a:Lbkn;

    .line 166
    new-instance v0, Lbko;

    invoke-direct {v0}, Lbko;-><init>()V

    sput-object v0, Lbkn;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput p1, p0, Lbkn;->b:I

    .line 58
    iput-object p2, p0, Lbkn;->c:Ljava/lang/String;

    .line 59
    iput-object p3, p0, Lbkn;->d:Ljava/lang/String;

    .line 60
    iput p4, p0, Lbkn;->e:I

    .line 61
    iput-object p5, p0, Lbkn;->f:Ljava/lang/String;

    .line 62
    iput-object p6, p0, Lbkn;->g:Ljava/lang/String;

    .line 63
    iput-object p7, p0, Lbkn;->h:Ljava/lang/String;

    .line 64
    iput p8, p0, Lbkn;->i:I

    .line 65
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lbkn;->b:I

    .line 181
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbkn;->c:Ljava/lang/String;

    .line 182
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbkn;->d:Ljava/lang/String;

    .line 183
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lbkn;->e:I

    .line 184
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbkn;->f:Ljava/lang/String;

    .line 185
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbkn;->g:Ljava/lang/String;

    .line 186
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbkn;->h:Ljava/lang/String;

    .line 187
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lbkn;->i:I

    .line 188
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lbkn;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public a(Lyj;)Landroid/content/Intent;
    .locals 14

    .prologue
    const/4 v5, 0x0

    const/4 v9, 0x1

    .line 131
    invoke-virtual {p0}, Lbkn;->c()Ljava/lang/String;

    move-result-object v1

    .line 132
    if-eqz v1, :cond_0

    move v0, v9

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 134
    new-instance v10, Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    .line 135
    invoke-static {v1}, Lbzd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v10, v0, v9, v1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 136
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 137
    invoke-virtual {p0}, Lbkn;->a()Lbdh;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 140
    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v13

    .line 141
    iget v0, p0, Lbkn;->e:I

    if-ne v0, v9, :cond_1

    move-object v0, p0

    .line 140
    :goto_1
    invoke-static {v13, v9, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;ILbkn;)I

    .line 144
    iget v4, p0, Lbkn;->i:I

    .line 145
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    move-object v0, v10

    move-object v1, v11

    move-object v2, v12

    move v3, v9

    .line 143
    invoke-static/range {v0 .. v6}, Lbbl;->a(Lcom/google/android/libraries/hangouts/video/HangoutRequest;Ljava/util/ArrayList;Ljava/util/ArrayList;ZIJ)Landroid/content/Intent;

    move-result-object v0

    .line 146
    return-object v0

    .line 132
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 141
    :cond_1
    new-instance v0, Lbkn;

    iget v1, p0, Lbkn;->b:I

    iget-object v2, p0, Lbkn;->c:Ljava/lang/String;

    iget-object v3, p0, Lbkn;->d:Ljava/lang/String;

    iget v4, p0, Lbkn;->e:I

    const/16 v8, 0x3d

    move-object v6, v5

    move-object v7, v5

    invoke-direct/range {v0 .. v8}, Lbkn;-><init>(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1
.end method

.method public a()Lbdh;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 85
    iget-object v0, p0, Lbkn;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v1

    .line 86
    :goto_0
    iget-object v0, p0, Lbkn;->c:Ljava/lang/String;

    .line 87
    new-instance v4, Ljava/util/Random;

    invoke-direct {v4}, Ljava/util/Random;-><init>()V

    invoke-virtual {v4}, Ljava/util/Random;->nextLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    const-wide v6, 0xffffffffffL

    rem-long/2addr v4, v6

    const-string v6, "2-%010x@%s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v7, v3

    const-string v3, "pstn-conference.google.com"

    aput-object v3, v7, v1

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lbkn;->g:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v4, p0, Lbkn;->f:Ljava/lang/String;

    :goto_1
    iget v5, p0, Lbkn;->e:I

    invoke-static/range {v0 .. v5}, Lbdh;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;I)Lbdh;

    move-result-object v0

    .line 90
    return-object v0

    :cond_0
    move v2, v3

    .line 85
    goto :goto_0

    .line 87
    :cond_1
    iget-object v4, p0, Lbkn;->c:Ljava/lang/String;

    goto :goto_1
.end method

.method public b()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lbkn;->b:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lbkn;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lbkn;->d:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x0

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lbkn;->e:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 156
    iget v0, p0, Lbkn;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 157
    iget-object v0, p0, Lbkn;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 158
    iget-object v0, p0, Lbkn;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 159
    iget v0, p0, Lbkn;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 160
    iget-object v0, p0, Lbkn;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lbkn;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lbkn;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 163
    iget v0, p0, Lbkn;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 164
    return-void
.end method
