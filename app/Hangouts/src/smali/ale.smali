.class public final Lale;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field private a:I

.field private b:I

.field private c:Landroid/widget/AbsListView$LayoutParams;

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/res/Resources;)V
    .locals 2

    .prologue
    .line 101
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 86
    const/4 v0, -0x1

    iput v0, p0, Lale;->a:I

    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lale;->c:Landroid/widget/AbsListView$LayoutParams;

    .line 102
    sget v0, Lf;->do:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 103
    sget v1, Lf;->dn:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lale;->b:I

    .line 105
    iget v1, p0, Lale;->b:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 107
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v1, v0, v0}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    iput-object v1, p0, Lale;->c:Landroid/widget/AbsListView$LayoutParams;

    .line 108
    return-void
.end method

.method public static a(Landroid/content/res/Resources;)Lale;
    .locals 3

    .prologue
    .line 92
    new-instance v0, Lale;

    invoke-direct {v0, p0}, Lale;-><init>(Landroid/content/res/Resources;)V

    .line 93
    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;->a()V

    .line 96
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lale;->d:Ljava/util/ArrayList;

    .line 97
    iget-object v1, v0, Lale;->d:Ljava/util/ArrayList;

    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;->b()Ljava/util/LinkedHashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 98
    return-object v0
.end method

.method public static synthetic a(Lale;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lale;->d:Ljava/util/ArrayList;

    return-object v0
.end method

.method private b(I)J
    .locals 2

    .prologue
    .line 128
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lale;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 129
    iget-object v0, p0, Lale;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    .line 134
    :goto_0
    invoke-static {}, Lccc;->a()Lccc;

    move-result-object v1

    invoke-virtual {v1, v0}, Lccc;->a(I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0

    .line 132
    :cond_0
    const/16 v0, 0x2002

    goto :goto_0
.end method


# virtual methods
.method public a(I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 193
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 194
    invoke-virtual {p0}, Lale;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_1

    :goto_1
    invoke-static {v1}, Lcwz;->a(Z)V

    .line 196
    iput p1, p0, Lale;->a:I

    .line 197
    return-void

    :cond_0
    move v0, v2

    .line 193
    goto :goto_0

    :cond_1
    move v1, v2

    .line 194
    goto :goto_1
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 115
    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;->b()Ljava/util/LinkedHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 123
    invoke-direct {p0, p1}, Lale;->b(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 142
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 167
    if-nez p2, :cond_2

    .line 168
    new-instance p2, Landroid/widget/ImageView;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 169
    iget-object v0, p0, Lale;->c:Landroid/widget/AbsListView$LayoutParams;

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 170
    iget v0, p0, Lale;->b:I

    iget v1, p0, Lale;->b:I

    iget v2, p0, Lale;->b:I

    iget v3, p0, Lale;->b:I

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 175
    :goto_0
    invoke-direct {p0, p1}, Lale;->b(I)J

    move-result-wide v0

    long-to-int v0, v0

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 176
    const/4 v0, 0x0

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lale;->getCount()I

    move-result v1

    if-ge p1, v1, :cond_0

    iget-object v0, p0, Lale;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;->b()Ljava/util/LinkedHashMap;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_0
    if-nez v0, :cond_1

    const-string v0, ""

    :cond_1
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 178
    iget v0, p0, Lale;->a:I

    if-ne p1, v0, :cond_3

    .line 179
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->cB:I

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 184
    :goto_1
    return-object p2

    .line 172
    :cond_2
    check-cast p2, Landroid/widget/ImageView;

    goto :goto_0

    .line 181
    :cond_3
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->cA:I

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0}, Lale;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
