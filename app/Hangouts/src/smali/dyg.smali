.class public final Ldyg;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldyg;


# instance fields
.field public b:Ldvn;

.field public c:Ldxv;

.field public d:Ldzg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1281
    const/4 v0, 0x0

    new-array v0, v0, [Ldyg;

    sput-object v0, Ldyg;->a:[Ldyg;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1282
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1285
    iput-object v0, p0, Ldyg;->b:Ldvn;

    .line 1288
    iput-object v0, p0, Ldyg;->c:Ldxv;

    .line 1291
    iput-object v0, p0, Ldyg;->d:Ldzg;

    .line 1282
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1311
    const/4 v0, 0x0

    .line 1312
    iget-object v1, p0, Ldyg;->b:Ldvn;

    if-eqz v1, :cond_0

    .line 1313
    const/4 v0, 0x1

    iget-object v1, p0, Ldyg;->b:Ldvn;

    .line 1314
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1316
    :cond_0
    iget-object v1, p0, Ldyg;->c:Ldxv;

    if-eqz v1, :cond_1

    .line 1317
    const/4 v1, 0x2

    iget-object v2, p0, Ldyg;->c:Ldxv;

    .line 1318
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1320
    :cond_1
    iget-object v1, p0, Ldyg;->d:Ldzg;

    if-eqz v1, :cond_2

    .line 1321
    const/4 v1, 0x3

    iget-object v2, p0, Ldyg;->d:Ldzg;

    .line 1322
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1324
    :cond_2
    iget-object v1, p0, Ldyg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1325
    iput v0, p0, Ldyg;->cachedSize:I

    .line 1326
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1278
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldyg;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldyg;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldyg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldyg;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldyg;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldyg;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldyg;->c:Ldxv;

    if-nez v0, :cond_3

    new-instance v0, Ldxv;

    invoke-direct {v0}, Ldxv;-><init>()V

    iput-object v0, p0, Ldyg;->c:Ldxv;

    :cond_3
    iget-object v0, p0, Ldyg;->c:Ldxv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldyg;->d:Ldzg;

    if-nez v0, :cond_4

    new-instance v0, Ldzg;

    invoke-direct {v0}, Ldzg;-><init>()V

    iput-object v0, p0, Ldyg;->d:Ldzg;

    :cond_4
    iget-object v0, p0, Ldyg;->d:Ldzg;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1296
    iget-object v0, p0, Ldyg;->b:Ldvn;

    if-eqz v0, :cond_0

    .line 1297
    const/4 v0, 0x1

    iget-object v1, p0, Ldyg;->b:Ldvn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1299
    :cond_0
    iget-object v0, p0, Ldyg;->c:Ldxv;

    if-eqz v0, :cond_1

    .line 1300
    const/4 v0, 0x2

    iget-object v1, p0, Ldyg;->c:Ldxv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1302
    :cond_1
    iget-object v0, p0, Ldyg;->d:Ldzg;

    if-eqz v0, :cond_2

    .line 1303
    const/4 v0, 0x3

    iget-object v1, p0, Ldyg;->d:Ldzg;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1305
    :cond_2
    iget-object v0, p0, Ldyg;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1307
    return-void
.end method
