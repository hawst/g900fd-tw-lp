.class public final Levf;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Levf;


# instance fields
.field public b:[Levh;

.field public c:Levh;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Levg;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Levf;

    sput-object v0, Levf;->a:[Levf;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 289
    sget-object v0, Levh;->a:[Levh;

    iput-object v0, p0, Levf;->b:[Levh;

    .line 292
    iput-object v1, p0, Levf;->c:Levh;

    .line 315
    iput-object v1, p0, Levf;->n:Levg;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 380
    iget-object v1, p0, Levf;->b:[Levh;

    if-eqz v1, :cond_1

    .line 381
    iget-object v2, p0, Levf;->b:[Levh;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 382
    if-eqz v4, :cond_0

    .line 383
    const/4 v5, 0x1

    .line 384
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 381
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 388
    :cond_1
    iget-object v1, p0, Levf;->c:Levh;

    if-eqz v1, :cond_2

    .line 389
    const/4 v1, 0x2

    iget-object v2, p0, Levf;->c:Levh;

    .line 390
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 392
    :cond_2
    iget-object v1, p0, Levf;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 393
    const/4 v1, 0x3

    iget-object v2, p0, Levf;->d:Ljava/lang/String;

    .line 394
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 396
    :cond_3
    iget-object v1, p0, Levf;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 397
    const/4 v1, 0x4

    iget-object v2, p0, Levf;->e:Ljava/lang/String;

    .line 398
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 400
    :cond_4
    iget-object v1, p0, Levf;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 401
    const/16 v1, 0xb

    iget-object v2, p0, Levf;->f:Ljava/lang/String;

    .line 402
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 404
    :cond_5
    iget-object v1, p0, Levf;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 405
    const/16 v1, 0xc

    iget-object v2, p0, Levf;->g:Ljava/lang/String;

    .line 406
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 408
    :cond_6
    iget-object v1, p0, Levf;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 409
    const/16 v1, 0xd

    iget-object v2, p0, Levf;->h:Ljava/lang/String;

    .line 410
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 412
    :cond_7
    iget-object v1, p0, Levf;->i:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 413
    const/16 v1, 0xe

    iget-object v2, p0, Levf;->i:Ljava/lang/String;

    .line 414
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 416
    :cond_8
    iget-object v1, p0, Levf;->j:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 417
    const/16 v1, 0xf

    iget-object v2, p0, Levf;->j:Ljava/lang/String;

    .line 418
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 420
    :cond_9
    iget-object v1, p0, Levf;->k:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 421
    const/16 v1, 0x10

    iget-object v2, p0, Levf;->k:Ljava/lang/String;

    .line 422
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 424
    :cond_a
    iget-object v1, p0, Levf;->n:Levg;

    if-eqz v1, :cond_b

    .line 425
    const/16 v1, 0x11

    iget-object v2, p0, Levf;->n:Levg;

    .line 426
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 428
    :cond_b
    iget-object v1, p0, Levf;->o:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 429
    const/16 v1, 0x12

    iget-object v2, p0, Levf;->o:Ljava/lang/String;

    .line 430
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 432
    :cond_c
    iget-object v1, p0, Levf;->p:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    .line 433
    const/16 v1, 0x13

    iget-object v2, p0, Levf;->p:Ljava/lang/Boolean;

    .line 434
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 436
    :cond_d
    iget-object v1, p0, Levf;->l:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 437
    const/16 v1, 0x14

    iget-object v2, p0, Levf;->l:Ljava/lang/String;

    .line 438
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 440
    :cond_e
    iget-object v1, p0, Levf;->m:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 441
    const/16 v1, 0x15

    iget-object v2, p0, Levf;->m:Ljava/lang/String;

    .line 442
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 444
    :cond_f
    iget-object v1, p0, Levf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 445
    iput v0, p0, Levf;->cachedSize:I

    .line 446
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Levf;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Levf;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Levf;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Levf;->b:[Levh;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Levh;

    iget-object v3, p0, Levf;->b:[Levh;

    if-eqz v3, :cond_2

    iget-object v3, p0, Levf;->b:[Levh;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Levf;->b:[Levh;

    :goto_2
    iget-object v2, p0, Levf;->b:[Levh;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Levf;->b:[Levh;

    new-instance v3, Levh;

    invoke-direct {v3}, Levh;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Levf;->b:[Levh;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Levf;->b:[Levh;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Levf;->b:[Levh;

    new-instance v3, Levh;

    invoke-direct {v3}, Levh;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Levf;->b:[Levh;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Levf;->c:Levh;

    if-nez v0, :cond_5

    new-instance v0, Levh;

    invoke-direct {v0}, Levh;-><init>()V

    iput-object v0, p0, Levf;->c:Levh;

    :cond_5
    iget-object v0, p0, Levf;->c:Levh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levf;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levf;->e:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levf;->f:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levf;->g:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levf;->h:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levf;->i:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levf;->j:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levf;->k:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Levf;->n:Levg;

    if-nez v0, :cond_6

    new-instance v0, Levg;

    invoke-direct {v0}, Levg;-><init>()V

    iput-object v0, p0, Levf;->n:Levg;

    :cond_6
    iget-object v0, p0, Levf;->n:Levg;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levf;->o:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Levf;->p:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levf;->l:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levf;->m:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x5a -> :sswitch_5
        0x62 -> :sswitch_6
        0x6a -> :sswitch_7
        0x72 -> :sswitch_8
        0x7a -> :sswitch_9
        0x82 -> :sswitch_a
        0x8a -> :sswitch_b
        0x92 -> :sswitch_c
        0x98 -> :sswitch_d
        0xa2 -> :sswitch_e
        0xaa -> :sswitch_f
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 324
    iget-object v0, p0, Levf;->b:[Levh;

    if-eqz v0, :cond_1

    .line 325
    iget-object v1, p0, Levf;->b:[Levh;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 326
    if-eqz v3, :cond_0

    .line 327
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 325
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 331
    :cond_1
    iget-object v0, p0, Levf;->c:Levh;

    if-eqz v0, :cond_2

    .line 332
    const/4 v0, 0x2

    iget-object v1, p0, Levf;->c:Levh;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 334
    :cond_2
    iget-object v0, p0, Levf;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 335
    const/4 v0, 0x3

    iget-object v1, p0, Levf;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 337
    :cond_3
    iget-object v0, p0, Levf;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 338
    const/4 v0, 0x4

    iget-object v1, p0, Levf;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 340
    :cond_4
    iget-object v0, p0, Levf;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 341
    const/16 v0, 0xb

    iget-object v1, p0, Levf;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 343
    :cond_5
    iget-object v0, p0, Levf;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 344
    const/16 v0, 0xc

    iget-object v1, p0, Levf;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 346
    :cond_6
    iget-object v0, p0, Levf;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 347
    const/16 v0, 0xd

    iget-object v1, p0, Levf;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 349
    :cond_7
    iget-object v0, p0, Levf;->i:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 350
    const/16 v0, 0xe

    iget-object v1, p0, Levf;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 352
    :cond_8
    iget-object v0, p0, Levf;->j:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 353
    const/16 v0, 0xf

    iget-object v1, p0, Levf;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 355
    :cond_9
    iget-object v0, p0, Levf;->k:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 356
    const/16 v0, 0x10

    iget-object v1, p0, Levf;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 358
    :cond_a
    iget-object v0, p0, Levf;->n:Levg;

    if-eqz v0, :cond_b

    .line 359
    const/16 v0, 0x11

    iget-object v1, p0, Levf;->n:Levg;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 361
    :cond_b
    iget-object v0, p0, Levf;->o:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 362
    const/16 v0, 0x12

    iget-object v1, p0, Levf;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 364
    :cond_c
    iget-object v0, p0, Levf;->p:Ljava/lang/Boolean;

    if-eqz v0, :cond_d

    .line 365
    const/16 v0, 0x13

    iget-object v1, p0, Levf;->p:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 367
    :cond_d
    iget-object v0, p0, Levf;->l:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 368
    const/16 v0, 0x14

    iget-object v1, p0, Levf;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 370
    :cond_e
    iget-object v0, p0, Levf;->m:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 371
    const/16 v0, 0x15

    iget-object v1, p0, Levf;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 373
    :cond_f
    iget-object v0, p0, Levf;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 375
    return-void
.end method
