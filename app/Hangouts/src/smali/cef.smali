.class public final Lcef;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/views/RichStatusView;

.field private final b:Lceh;

.field private final c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/views/RichStatusView;Lceh;Z)V
    .locals 0

    .prologue
    .line 353
    iput-object p1, p0, Lcef;->a:Lcom/google/android/apps/hangouts/views/RichStatusView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354
    iput-object p2, p0, Lcef;->b:Lceh;

    .line 355
    iput-boolean p3, p0, Lcef;->c:Z

    .line 356
    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 360
    iget-object v0, p0, Lcef;->b:Lceh;

    iget-object v0, v0, Lceh;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/hangouts/views/RichStatusView;->d()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 362
    iget-boolean v0, p0, Lcef;->c:Z

    if-nez v0, :cond_0

    .line 363
    iget-object v0, p0, Lcef;->b:Lceh;

    iget-object v0, v0, Lceh;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 365
    iget-object v0, p0, Lcef;->a:Lcom/google/android/apps/hangouts/views/RichStatusView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/RichStatusView;->a(Lcom/google/android/apps/hangouts/views/RichStatusView;)V

    .line 367
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 375
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 371
    return-void
.end method
