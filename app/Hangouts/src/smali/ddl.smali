.class public final Lddl;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lddl;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:[Lddm;

.field public e:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    new-array v0, v0, [Lddl;

    sput-object v0, Lddl;->a:[Lddl;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 107
    invoke-direct {p0}, Lepn;-><init>()V

    .line 110
    iput-object v0, p0, Lddl;->b:Ljava/lang/Integer;

    .line 113
    iput-object v0, p0, Lddl;->c:Ljava/lang/Integer;

    .line 116
    sget-object v0, Lddm;->a:[Lddm;

    iput-object v0, p0, Lddl;->d:[Lddm;

    .line 107
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 146
    iget-object v0, p0, Lddl;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 147
    const/4 v0, 0x1

    iget-object v2, p0, Lddl;->b:Ljava/lang/Integer;

    .line 148
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 150
    :goto_0
    iget-object v2, p0, Lddl;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 151
    const/4 v2, 0x2

    iget-object v3, p0, Lddl;->c:Ljava/lang/Integer;

    .line 152
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 154
    :cond_0
    iget-object v2, p0, Lddl;->d:[Lddm;

    if-eqz v2, :cond_2

    .line 155
    iget-object v2, p0, Lddl;->d:[Lddm;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 156
    if-eqz v4, :cond_1

    .line 157
    const/4 v5, 0x3

    .line 158
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 155
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 162
    :cond_2
    iget-object v1, p0, Lddl;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 163
    const/4 v1, 0x4

    iget-object v2, p0, Lddl;->e:Ljava/lang/Boolean;

    .line 164
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 166
    :cond_3
    iget-object v1, p0, Lddl;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 167
    iput v0, p0, Lddl;->cachedSize:I

    .line 168
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 8

    .prologue
    const/16 v7, 0x28

    const/16 v6, 0x1e

    const/16 v5, 0x14

    const/16 v4, 0xa

    const/4 v1, 0x0

    .line 103
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lddl;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lddl;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lddl;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    if-eq v0, v7, :cond_2

    const/16 v2, 0x32

    if-ne v0, v2, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lddl;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lddl;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_4

    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    if-eq v0, v6, :cond_4

    if-eq v0, v7, :cond_4

    const/16 v2, 0x32

    if-ne v0, v2, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lddl;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_5
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lddl;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lddl;->d:[Lddm;

    if-nez v0, :cond_7

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lddm;

    iget-object v3, p0, Lddl;->d:[Lddm;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lddl;->d:[Lddm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Lddl;->d:[Lddm;

    :goto_2
    iget-object v2, p0, Lddl;->d:[Lddm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Lddl;->d:[Lddm;

    new-instance v3, Lddm;

    invoke-direct {v3}, Lddm;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lddl;->d:[Lddm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lddl;->d:[Lddm;

    array-length v0, v0

    goto :goto_1

    :cond_8
    iget-object v2, p0, Lddl;->d:[Lddm;

    new-instance v3, Lddm;

    invoke-direct {v3}, Lddm;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lddl;->d:[Lddm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lddl;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 123
    iget-object v0, p0, Lddl;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 124
    const/4 v0, 0x1

    iget-object v1, p0, Lddl;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 126
    :cond_0
    iget-object v0, p0, Lddl;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 127
    const/4 v0, 0x2

    iget-object v1, p0, Lddl;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 129
    :cond_1
    iget-object v0, p0, Lddl;->d:[Lddm;

    if-eqz v0, :cond_3

    .line 130
    iget-object v1, p0, Lddl;->d:[Lddm;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 131
    if-eqz v3, :cond_2

    .line 132
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 130
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 136
    :cond_3
    iget-object v0, p0, Lddl;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 137
    const/4 v0, 0x4

    iget-object v1, p0, Lddl;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 139
    :cond_4
    iget-object v0, p0, Lddl;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 141
    return-void
.end method
