.class public final Lbaz;
.super Ls;
.source "PG"


# instance fields
.field private Y:Landroid/app/Dialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 130
    invoke-direct {p0}, Ls;-><init>()V

    .line 131
    return-void
.end method

.method public constructor <init>(I)V
    .locals 3

    .prologue
    .line 147
    invoke-direct {p0}, Ls;-><init>()V

    .line 148
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 149
    const-string v1, "error_code"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 150
    const-string v1, "request_code"

    const/16 v2, 0x3e9

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 151
    invoke-virtual {p0, v0}, Lbaz;->setArguments(Landroid/os/Bundle;)V

    .line 152
    return-void
.end method


# virtual methods
.method public f()Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 162
    invoke-virtual {p0}, Lbaz;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "error_code"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 163
    invoke-virtual {p0}, Lbaz;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "request_code"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 166
    invoke-virtual {p0}, Lbaz;->getActivity()Ly;

    move-result-object v2

    .line 165
    invoke-static {v0, v2, v1}, Lcfz;->a(ILandroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lbaz;->Y:Landroid/app/Dialog;

    .line 168
    iget-object v0, p0, Lbaz;->Y:Landroid/app/Dialog;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lbaz;->Y:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 188
    invoke-virtual {p0}, Lbaz;->e()V

    .line 191
    :cond_0
    invoke-super {p0, p1}, Ls;->onActivityCreated(Landroid/os/Bundle;)V

    .line 192
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0}, Lbaz;->a()V

    .line 176
    invoke-virtual {p0}, Lbaz;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->finish()V

    .line 177
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 135
    invoke-super {p0, p1}, Ls;->onCreate(Landroid/os/Bundle;)V

    .line 138
    if-eqz p1, :cond_0

    .line 139
    invoke-virtual {p0}, Lbaz;->a()V

    .line 141
    :cond_0
    return-void
.end method
