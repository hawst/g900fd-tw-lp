.class public final Lcbd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)V
    .locals 0

    .prologue
    .line 347
    iput-object p1, p0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 350
    iget-object v1, p0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->c(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lyj;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lt;

    move-result-object v1

    if-nez v1, :cond_1

    .line 509
    :cond_0
    :goto_0
    return-void

    .line 355
    :cond_1
    invoke-static {}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->p()Z

    move-result v1

    .line 356
    if-eqz v1, :cond_8

    .line 358
    iget-object v2, p0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->c(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lyj;

    move-result-object v2

    invoke-virtual {v2}, Lyj;->u()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->c(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lyj;

    move-result-object v2

    invoke-virtual {v2}, Lyj;->l()Z

    move-result v2

    if-nez v2, :cond_4

    .line 385
    :cond_2
    :goto_1
    iget-object v1, p0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lt;

    move-result-object v1

    invoke-virtual {v1}, Lt;->getActivity()Ly;

    move-result-object v1

    .line 386
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 387
    iget-object v3, p0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->e(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lbxn;

    move-result-object v3

    if-nez v3, :cond_3

    .line 388
    iget-object v3, p0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    new-instance v4, Lbxn;

    invoke-direct {v4, v1}, Lbxn;-><init>(Landroid/content/Context;)V

    invoke-static {v3, v4}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(Lcom/google/android/apps/hangouts/views/ComposeMessageView;Lbxn;)Lbxn;

    .line 391
    :cond_3
    iget-object v1, p0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    .line 392
    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->f(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lcbl;

    move-result-object v1

    invoke-interface {v1}, Lcbl;->a()I

    move-result v1

    .line 391
    invoke-static {v1}, Lf;->b(I)Z

    move-result v1

    .line 393
    iget-object v3, p0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->e(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lbxn;

    move-result-object v3

    .line 396
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->n()Z

    move-result v4

    .line 393
    invoke-virtual {v3, v0, v1, v4}, Lbxn;->a(ZZZ)V

    .line 397
    iget-object v0, p0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->e(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lbxn;

    move-result-object v0

    new-instance v1, Lcbe;

    invoke-direct {v1, p0}, Lcbe;-><init>(Lcbd;)V

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 508
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 364
    :cond_4
    iget-object v2, p0, Lcbd;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v2}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 365
    const/4 v3, 0x0

    invoke-static {v3}, Lbbl;->f(Lyj;)Landroid/content/Intent;

    move-result-object v3

    .line 366
    invoke-virtual {v2, v3, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    .line 367
    if-eqz v3, :cond_5

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_6

    :cond_5
    move v1, v0

    .line 374
    :cond_6
    if-eqz v1, :cond_7

    .line 376
    invoke-static {}, Lbbl;->i()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "com.google.android.apps.plus"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 375
    invoke-virtual {v2, v3, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 379
    if-eqz v2, :cond_7

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_2

    :cond_7
    move v0, v1

    goto/16 :goto_1

    :cond_8
    move v0, v1

    goto/16 :goto_1
.end method
