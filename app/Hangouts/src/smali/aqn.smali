.class public final Laqn;
.super Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/hangouts/video/SafeAsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Laqq;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Z

.field final synthetic c:Z

.field final synthetic d:Lcom/google/android/apps/hangouts/hangout/IncomingRing;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/hangout/IncomingRing;Ljava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 637
    iput-object p1, p0, Laqn;->d:Lcom/google/android/apps/hangouts/hangout/IncomingRing;

    iput-object p2, p0, Laqn;->a:Ljava/lang/String;

    iput-boolean p3, p0, Laqn;->b:Z

    iput-boolean p4, p0, Laqn;->c:Z

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected synthetic doInBackgroundTimed([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 637
    new-instance v0, Laqq;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Laqq;-><init>(B)V

    iget-object v1, p0, Laqn;->d:Lcom/google/android/apps/hangouts/hangout/IncomingRing;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->b(Lcom/google/android/apps/hangouts/hangout/IncomingRing;)Lyj;

    move-result-object v1

    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lf;->k(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v1

    iget-object v2, p0, Laqn;->a:Ljava/lang/String;

    iget-boolean v3, p0, Laqn;->b:Z

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Laqq;->b:Z

    invoke-static {}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->q()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "shouldVibrate="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, v0, Laqq;->b:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " silent="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Laqn;->c:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Laqn;->d:Lcom/google/android/apps/hangouts/hangout/IncomingRing;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->c(Lcom/google/android/apps/hangouts/hangout/IncomingRing;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v0, Laqq;->a:Landroid/net/Uri;

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 637
    check-cast p1, Laqq;

    iget-object v0, p0, Laqn;->d:Lcom/google/android/apps/hangouts/hangout/IncomingRing;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->d(Lcom/google/android/apps/hangouts/hangout/IncomingRing;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Laqq;->a:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laqn;->d:Lcom/google/android/apps/hangouts/hangout/IncomingRing;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->e(Lcom/google/android/apps/hangouts/hangout/IncomingRing;)Lbyv;

    move-result-object v0

    iget-object v1, p1, Laqq;->a:Landroid/net/Uri;

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2, v3, v4}, Lbyv;->a(Landroid/net/Uri;ZIF)V

    :cond_0
    iget-boolean v0, p1, Laqq;->b:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Laqn;->c:Z

    if-nez v0, :cond_1

    iget-object v1, p0, Laqn;->d:Lcom/google/android/apps/hangouts/hangout/IncomingRing;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v2, "vibrator"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->a(Lcom/google/android/apps/hangouts/hangout/IncomingRing;Landroid/os/Vibrator;)Landroid/os/Vibrator;

    iget-object v0, p0, Laqn;->d:Lcom/google/android/apps/hangouts/hangout/IncomingRing;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f(Lcom/google/android/apps/hangouts/hangout/IncomingRing;)Landroid/os/Vibrator;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->r()[J

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    :cond_1
    return-void
.end method
