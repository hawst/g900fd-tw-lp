.class public final Lvd;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Queue",
            "<",
            "Lva;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lva;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/concurrent/PriorityBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/PriorityBlockingQueue",
            "<",
            "Lva;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/concurrent/PriorityBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/PriorityBlockingQueue",
            "<",
            "Lva;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Luo;

.field private final g:Luu;

.field private final h:Lvh;

.field private i:[Luv;

.field private j:Luq;


# direct methods
.method public constructor <init>(Luo;Luu;I)V
    .locals 3

    .prologue
    .line 114
    new-instance v0, Lvh;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, v1}, Lvh;-><init>(Landroid/os/Handler;)V

    invoke-direct {p0, p1, p2, p3, v0}, Lvd;-><init>(Luo;Luu;ILvh;)V

    .line 116
    return-void
.end method

.method private constructor <init>(Luo;Luu;ILvh;)V
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lvd;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lvd;->b:Ljava/util/Map;

    .line 62
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lvd;->c:Ljava/util/Set;

    .line 65
    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object v0, p0, Lvd;->d:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 69
    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object v0, p0, Lvd;->e:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 100
    iput-object p1, p0, Lvd;->f:Luo;

    .line 101
    iput-object p2, p0, Lvd;->g:Luu;

    .line 102
    new-array v0, p3, [Luv;

    iput-object v0, p0, Lvd;->i:[Luv;

    .line 103
    iput-object p4, p0, Lvd;->h:Lvh;

    .line 104
    return-void
.end method


# virtual methods
.method public a(Lva;)Lva;
    .locals 5

    .prologue
    .line 219
    invoke-virtual {p1, p0}, Lva;->a(Lvd;)V

    .line 220
    iget-object v1, p0, Lvd;->c:Ljava/util/Set;

    monitor-enter v1

    .line 221
    :try_start_0
    iget-object v0, p0, Lvd;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 222
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    iget-object v0, p0, Lvd;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lva;->a:Ljava/lang/Integer;

    .line 226
    const-string v0, "add-to-queue"

    invoke-virtual {p1, v0}, Lva;->a(Ljava/lang/String;)V

    .line 229
    iget-boolean v0, p1, Lva;->b:Z

    if-nez v0, :cond_0

    .line 230
    iget-object v0, p0, Lvd;->e:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 254
    :goto_0
    return-object p1

    .line 222
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 235
    :cond_0
    iget-object v1, p0, Lvd;->b:Ljava/util/Map;

    monitor-enter v1

    .line 236
    :try_start_1
    invoke-virtual {p1}, Lva;->d()Ljava/lang/String;

    move-result-object v2

    .line 237
    iget-object v0, p0, Lvd;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 239
    iget-object v0, p0, Lvd;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    .line 240
    if-nez v0, :cond_1

    .line 241
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 243
    :cond_1
    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 244
    iget-object v3, p0, Lvd;->b:Ljava/util/Map;

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    sget-boolean v0, Lvm;->b:Z

    if-eqz v0, :cond_2

    .line 246
    const-string v0, "Request for cacheKey=%s is in flight, putting on hold."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-static {v0, v3}, Lvm;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 254
    :cond_2
    :goto_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 255
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 251
    :cond_3
    :try_start_2
    iget-object v0, p0, Lvd;->b:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    iget-object v0, p0, Lvd;->d:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1
.end method

.method public a()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 132
    iget-object v0, p0, Lvd;->j:Luq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lvd;->j:Luq;

    invoke-virtual {v0}, Luq;->a()V

    :cond_0
    move v0, v1

    :goto_0
    iget-object v2, p0, Lvd;->i:[Luv;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lvd;->i:[Luv;

    aget-object v2, v2, v0

    if-eqz v2, :cond_1

    iget-object v2, p0, Lvd;->i:[Luv;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Luv;->a()V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 134
    :cond_2
    new-instance v0, Luq;

    iget-object v2, p0, Lvd;->d:Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v3, p0, Lvd;->e:Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v4, p0, Lvd;->f:Luo;

    iget-object v5, p0, Lvd;->h:Lvh;

    invoke-direct {v0, v2, v3, v4, v5}, Luq;-><init>(Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/BlockingQueue;Luo;Lvh;)V

    iput-object v0, p0, Lvd;->j:Luq;

    .line 135
    iget-object v0, p0, Lvd;->j:Luq;

    invoke-virtual {v0}, Luq;->start()V

    .line 138
    :goto_1
    iget-object v0, p0, Lvd;->i:[Luv;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 139
    new-instance v0, Luv;

    iget-object v2, p0, Lvd;->e:Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v3, p0, Lvd;->g:Luu;

    iget-object v4, p0, Lvd;->f:Luo;

    iget-object v5, p0, Lvd;->h:Lvh;

    invoke-direct {v0, v2, v3, v4, v5}, Luv;-><init>(Ljava/util/concurrent/BlockingQueue;Luu;Luo;Lvh;)V

    .line 141
    iget-object v2, p0, Lvd;->i:[Luv;

    aput-object v0, v2, v1

    .line 142
    invoke-virtual {v0}, Luv;->start()V

    .line 138
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 144
    :cond_3
    return-void
.end method

.method public b()Luo;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lvd;->f:Luo;

    return-object v0
.end method

.method b(Lva;)V
    .locals 7

    .prologue
    .line 267
    iget-object v1, p0, Lvd;->c:Ljava/util/Set;

    monitor-enter v1

    .line 268
    :try_start_0
    iget-object v0, p0, Lvd;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 269
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271
    iget-boolean v0, p1, Lva;->b:Z

    if-eqz v0, :cond_2

    .line 272
    iget-object v1, p0, Lvd;->b:Ljava/util/Map;

    monitor-enter v1

    .line 273
    :try_start_1
    invoke-virtual {p1}, Lva;->d()Ljava/lang/String;

    move-result-object v2

    .line 274
    iget-object v0, p0, Lvd;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    .line 275
    if-eqz v0, :cond_1

    .line 276
    sget-boolean v3, Lvm;->b:Z

    if-eqz v3, :cond_0

    .line 277
    const-string v3, "Releasing %d waiting requests for cacheKey=%s."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Lvm;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 282
    :cond_0
    iget-object v2, p0, Lvd;->d:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/PriorityBlockingQueue;->addAll(Ljava/util/Collection;)Z

    .line 284
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 286
    :cond_2
    return-void

    .line 269
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 284
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method
