.class public final Ldvi;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldvi;


# instance fields
.field public b:Ldvn;

.field public c:Ldrr;

.field public d:Ldqa;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6557
    const/4 v0, 0x0

    new-array v0, v0, [Ldvi;

    sput-object v0, Ldvi;->a:[Ldvi;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6558
    invoke-direct {p0}, Lepn;-><init>()V

    .line 6561
    iput-object v0, p0, Ldvi;->b:Ldvn;

    .line 6564
    iput-object v0, p0, Ldvi;->c:Ldrr;

    .line 6567
    iput-object v0, p0, Ldvi;->d:Ldqa;

    .line 6558
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 6597
    const/4 v0, 0x0

    .line 6598
    iget-object v1, p0, Ldvi;->b:Ldvn;

    if-eqz v1, :cond_0

    .line 6599
    const/4 v0, 0x1

    iget-object v1, p0, Ldvi;->b:Ldvn;

    .line 6600
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6602
    :cond_0
    iget-object v1, p0, Ldvi;->e:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 6603
    const/4 v1, 0x2

    iget-object v2, p0, Ldvi;->e:Ljava/lang/Long;

    .line 6604
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 6606
    :cond_1
    iget-object v1, p0, Ldvi;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 6607
    const/4 v1, 0x3

    iget-object v2, p0, Ldvi;->f:Ljava/lang/String;

    .line 6608
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6610
    :cond_2
    iget-object v1, p0, Ldvi;->c:Ldrr;

    if-eqz v1, :cond_3

    .line 6611
    const/4 v1, 0x4

    iget-object v2, p0, Ldvi;->c:Ldrr;

    .line 6612
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6614
    :cond_3
    iget-object v1, p0, Ldvi;->d:Ldqa;

    if-eqz v1, :cond_4

    .line 6615
    const/4 v1, 0x5

    iget-object v2, p0, Ldvi;->d:Ldqa;

    .line 6616
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6618
    :cond_4
    iget-object v1, p0, Ldvi;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6619
    iput v0, p0, Ldvi;->cachedSize:I

    .line 6620
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6554
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldvi;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldvi;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldvi;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldvi;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldvi;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldvi;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldvi;->e:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldvi;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldvi;->c:Ldrr;

    if-nez v0, :cond_3

    new-instance v0, Ldrr;

    invoke-direct {v0}, Ldrr;-><init>()V

    iput-object v0, p0, Ldvi;->c:Ldrr;

    :cond_3
    iget-object v0, p0, Ldvi;->c:Ldrr;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldvi;->d:Ldqa;

    if-nez v0, :cond_4

    new-instance v0, Ldqa;

    invoke-direct {v0}, Ldqa;-><init>()V

    iput-object v0, p0, Ldvi;->d:Ldqa;

    :cond_4
    iget-object v0, p0, Ldvi;->d:Ldqa;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 6576
    iget-object v0, p0, Ldvi;->b:Ldvn;

    if-eqz v0, :cond_0

    .line 6577
    const/4 v0, 0x1

    iget-object v1, p0, Ldvi;->b:Ldvn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6579
    :cond_0
    iget-object v0, p0, Ldvi;->e:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 6580
    const/4 v0, 0x2

    iget-object v1, p0, Ldvi;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 6582
    :cond_1
    iget-object v0, p0, Ldvi;->f:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 6583
    const/4 v0, 0x3

    iget-object v1, p0, Ldvi;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 6585
    :cond_2
    iget-object v0, p0, Ldvi;->c:Ldrr;

    if-eqz v0, :cond_3

    .line 6586
    const/4 v0, 0x4

    iget-object v1, p0, Ldvi;->c:Ldrr;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6588
    :cond_3
    iget-object v0, p0, Ldvi;->d:Ldqa;

    if-eqz v0, :cond_4

    .line 6589
    const/4 v0, 0x5

    iget-object v1, p0, Ldvi;->d:Ldqa;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6591
    :cond_4
    iget-object v0, p0, Ldvi;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 6593
    return-void
.end method
