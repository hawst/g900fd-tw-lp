.class public final Lbwt;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Z


# instance fields
.field private b:I

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lbys;->n:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbwt;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-virtual {p0}, Lbwt;->a()V

    .line 52
    return-void
.end method

.method private static a(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 180
    if-nez p0, :cond_0

    .line 181
    const-string v0, "<none>"

    .line 201
    :goto_0
    return-object v0

    .line 183
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 184
    and-int/lit8 v1, p0, 0x1

    if-lez v1, :cond_1

    .line 185
    const-string v1, "RECIPIENTS_REQUIRE_MMS | "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    :cond_1
    and-int/lit8 v1, p0, 0x2

    if-lez v1, :cond_2

    .line 188
    const-string v1, "HAS_SUBJECT | "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    :cond_2
    and-int/lit8 v1, p0, 0x4

    if-lez v1, :cond_3

    .line 191
    const-string v1, "HAS_ATTACHMENT | "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    :cond_3
    and-int/lit8 v1, p0, 0x8

    if-lez v1, :cond_4

    .line 194
    const-string v1, "LENGTH_REQUIRES_MMS | "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    :cond_4
    and-int/lit8 v1, p0, 0x10

    if-lez v1, :cond_5

    .line 197
    const-string v1, "MULTIPLE_RECIPIENTS | "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    :cond_5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x3

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 201
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(IZZ)V
    .locals 4

    .prologue
    .line 212
    invoke-static {}, Lbvm;->a()Lsm;

    move-result-object v0

    invoke-virtual {v0}, Lsm;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    iget v0, p0, Lbwt;->b:I

    .line 219
    if-eqz p2, :cond_3

    .line 220
    iget v1, p0, Lbwt;->b:I

    or-int/2addr v1, p1

    iput v1, p0, Lbwt;->b:I

    .line 227
    :goto_1
    if-eqz p3, :cond_2

    .line 228
    if-nez v0, :cond_4

    iget v1, p0, Lbwt;->b:I

    if-eqz v1, :cond_4

    .line 229
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    sget v2, Lh;->bs:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 235
    :cond_2
    :goto_2
    iget v1, p0, Lbwt;->b:I

    if-eq v0, v1, :cond_0

    .line 236
    sget-boolean v0, Lbwt;->a:Z

    if-eqz v0, :cond_0

    .line 237
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateState: oldState: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 238
    invoke-static {p1}, Lbwt;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " new state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lbwt;->b:I

    invoke-static {v2}, Lbwt;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 237
    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 222
    :cond_3
    iget v1, p0, Lbwt;->b:I

    xor-int/lit8 v2, p1, -0x1

    and-int/2addr v1, v2

    iput v1, p0, Lbwt;->b:I

    goto :goto_1

    .line 230
    :cond_4
    if-eqz v0, :cond_2

    iget v1, p0, Lbwt;->b:I

    goto :goto_2
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 58
    iput v1, p0, Lbwt;->c:I

    .line 59
    iput-object v0, p0, Lbwt;->d:Ljava/lang/String;

    .line 60
    iput v1, p0, Lbwt;->b:I

    .line 61
    iput-object v0, p0, Lbwt;->e:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v2, v0, v1}, Lbwt;->a(IZZ)V

    .line 62
    return-void

    :cond_0
    move v0, v1

    .line 61
    goto :goto_0
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 103
    iput-object p1, p0, Lbwt;->d:Ljava/lang/String;

    .line 104
    const/4 v1, 0x2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, v0, p2}, Lbwt;->a(IZZ)V

    .line 105
    return-void

    .line 104
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 156
    const/16 v2, 0x10

    if-eqz p1, :cond_0

    .line 157
    invoke-static {}, Lbvx;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 156
    :goto_0
    invoke-direct {p0, v2, v0, v1}, Lbwt;->a(IZZ)V

    .line 159
    return-void

    :cond_0
    move v0, v1

    .line 157
    goto :goto_0
.end method

.method public a(ZZ)V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x4

    invoke-direct {p0, v0, p1, p2}, Lbwt;->a(IZZ)V

    .line 81
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    iput v0, p0, Lbwt;->c:I

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lbwt;->d:Ljava/lang/String;

    .line 75
    iget v0, p0, Lbwt;->b:I

    and-int/lit8 v0, v0, 0x11

    iput v0, p0, Lbwt;->b:I

    .line 76
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 176
    const/16 v0, 0x8

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, v1}, Lbwt;->a(IZZ)V

    .line 177
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lbwt;->d:Ljava/lang/String;

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lbwt;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbwt;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 141
    invoke-static {}, Lbvm;->a()Lsm;

    move-result-object v0

    invoke-virtual {v0}, Lsm;->k()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 142
    invoke-direct {p0, v2, v1, v1}, Lbwt;->a(IZZ)V

    .line 146
    :goto_0
    return-void

    .line 144
    :cond_0
    invoke-direct {p0, v2, v2, v1}, Lbwt;->a(IZZ)V

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lbwt;->b:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
