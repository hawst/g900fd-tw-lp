.class public final Ldeq;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldeq;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Float;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2863
    const/4 v0, 0x0

    new-array v0, v0, [Ldeq;

    sput-object v0, Ldeq;->a:[Ldeq;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2864
    invoke-direct {p0}, Lepn;-><init>()V

    .line 2877
    const/4 v0, 0x0

    iput-object v0, p0, Ldeq;->b:Ljava/lang/Integer;

    .line 2864
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2896
    const/4 v0, 0x0

    .line 2897
    iget-object v1, p0, Ldeq;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 2898
    const/4 v0, 0x1

    iget-object v1, p0, Ldeq;->b:Ljava/lang/Integer;

    .line 2899
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2901
    :cond_0
    iget-object v1, p0, Ldeq;->c:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 2902
    const/4 v1, 0x2

    iget-object v2, p0, Ldeq;->c:Ljava/lang/Float;

    .line 2903
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 2905
    :cond_1
    iget-object v1, p0, Ldeq;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2906
    iput v0, p0, Ldeq;->cachedSize:I

    .line 2907
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 2860
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldeq;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldeq;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldeq;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldeq;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldeq;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Ldeq;->c:Ljava/lang/Float;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 2884
    iget-object v0, p0, Ldeq;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2885
    const/4 v0, 0x1

    iget-object v1, p0, Ldeq;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 2887
    :cond_0
    iget-object v0, p0, Ldeq;->c:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 2888
    const/4 v0, 0x2

    iget-object v1, p0, Ldeq;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 2890
    :cond_1
    iget-object v0, p0, Ldeq;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 2892
    return-void
.end method
