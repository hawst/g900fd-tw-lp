.class public final Lddu;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lddu;


# instance fields
.field public b:[Lddr;

.field public c:[Lddr;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:[Lddv;

.field public g:Lddt;

.field public h:Ldds;

.field public i:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 467
    const/4 v0, 0x0

    new-array v0, v0, [Lddu;

    sput-object v0, Lddu;->a:[Lddu;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 468
    invoke-direct {p0}, Lepn;-><init>()V

    .line 544
    sget-object v0, Lddr;->a:[Lddr;

    iput-object v0, p0, Lddu;->b:[Lddr;

    .line 547
    sget-object v0, Lddr;->a:[Lddr;

    iput-object v0, p0, Lddu;->c:[Lddr;

    .line 554
    sget-object v0, Lddv;->a:[Lddv;

    iput-object v0, p0, Lddu;->f:[Lddv;

    .line 557
    iput-object v1, p0, Lddu;->g:Lddt;

    .line 560
    iput-object v1, p0, Lddu;->h:Ldds;

    .line 468
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 610
    iget-object v0, p0, Lddu;->b:[Lddr;

    if-eqz v0, :cond_1

    .line 611
    iget-object v3, p0, Lddu;->b:[Lddr;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 612
    if-eqz v5, :cond_0

    .line 613
    const/4 v6, 0x1

    .line 614
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 611
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 618
    :cond_2
    iget-object v2, p0, Lddu;->c:[Lddr;

    if-eqz v2, :cond_4

    .line 619
    iget-object v3, p0, Lddu;->c:[Lddr;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 620
    if-eqz v5, :cond_3

    .line 621
    const/4 v6, 0x2

    .line 622
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 619
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 626
    :cond_4
    iget-object v2, p0, Lddu;->d:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 627
    const/4 v2, 0x3

    iget-object v3, p0, Lddu;->d:Ljava/lang/String;

    .line 628
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 630
    :cond_5
    iget-object v2, p0, Lddu;->e:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 631
    const/4 v2, 0x4

    iget-object v3, p0, Lddu;->e:Ljava/lang/String;

    .line 632
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 634
    :cond_6
    iget-object v2, p0, Lddu;->f:[Lddv;

    if-eqz v2, :cond_8

    .line 635
    iget-object v2, p0, Lddu;->f:[Lddv;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 636
    if-eqz v4, :cond_7

    .line 637
    const/4 v5, 0x5

    .line 638
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 635
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 642
    :cond_8
    iget-object v1, p0, Lddu;->g:Lddt;

    if-eqz v1, :cond_9

    .line 643
    const/4 v1, 0x6

    iget-object v2, p0, Lddu;->g:Lddt;

    .line 644
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 646
    :cond_9
    iget-object v1, p0, Lddu;->h:Ldds;

    if-eqz v1, :cond_a

    .line 647
    const/4 v1, 0x7

    iget-object v2, p0, Lddu;->h:Ldds;

    .line 648
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 650
    :cond_a
    iget-object v1, p0, Lddu;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 651
    const/16 v1, 0x8

    iget-object v2, p0, Lddu;->i:Ljava/lang/Boolean;

    .line 652
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 654
    :cond_b
    iget-object v1, p0, Lddu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 655
    iput v0, p0, Lddu;->cachedSize:I

    .line 656
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 464
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lddu;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lddu;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lddu;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lddu;->b:[Lddr;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lddr;

    iget-object v3, p0, Lddu;->b:[Lddr;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lddu;->b:[Lddr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lddu;->b:[Lddr;

    :goto_2
    iget-object v2, p0, Lddu;->b:[Lddr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lddu;->b:[Lddr;

    new-instance v3, Lddr;

    invoke-direct {v3}, Lddr;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lddu;->b:[Lddr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lddu;->b:[Lddr;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lddu;->b:[Lddr;

    new-instance v3, Lddr;

    invoke-direct {v3}, Lddr;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lddu;->b:[Lddr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lddu;->c:[Lddr;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lddr;

    iget-object v3, p0, Lddu;->c:[Lddr;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lddu;->c:[Lddr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Lddu;->c:[Lddr;

    :goto_4
    iget-object v2, p0, Lddu;->c:[Lddr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Lddu;->c:[Lddr;

    new-instance v3, Lddr;

    invoke-direct {v3}, Lddr;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lddu;->c:[Lddr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lddu;->c:[Lddr;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lddu;->c:[Lddr;

    new-instance v3, Lddr;

    invoke-direct {v3}, Lddr;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lddu;->c:[Lddr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lddu;->d:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lddu;->e:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lddu;->f:[Lddv;

    if-nez v0, :cond_9

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lddv;

    iget-object v3, p0, Lddu;->f:[Lddv;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lddu;->f:[Lddv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    iput-object v2, p0, Lddu;->f:[Lddv;

    :goto_6
    iget-object v2, p0, Lddu;->f:[Lddv;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Lddu;->f:[Lddv;

    new-instance v3, Lddv;

    invoke-direct {v3}, Lddv;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lddu;->f:[Lddv;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    iget-object v0, p0, Lddu;->f:[Lddv;

    array-length v0, v0

    goto :goto_5

    :cond_a
    iget-object v2, p0, Lddu;->f:[Lddv;

    new-instance v3, Lddv;

    invoke-direct {v3}, Lddv;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lddu;->f:[Lddv;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lddu;->g:Lddt;

    if-nez v0, :cond_b

    new-instance v0, Lddt;

    invoke-direct {v0}, Lddt;-><init>()V

    iput-object v0, p0, Lddu;->g:Lddt;

    :cond_b
    iget-object v0, p0, Lddu;->g:Lddt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lddu;->h:Ldds;

    if-nez v0, :cond_c

    new-instance v0, Ldds;

    invoke-direct {v0}, Ldds;-><init>()V

    iput-object v0, p0, Lddu;->h:Ldds;

    :cond_c
    iget-object v0, p0, Lddu;->h:Ldds;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lddu;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 567
    iget-object v1, p0, Lddu;->b:[Lddr;

    if-eqz v1, :cond_1

    .line 568
    iget-object v2, p0, Lddu;->b:[Lddr;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 569
    if-eqz v4, :cond_0

    .line 570
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 568
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 574
    :cond_1
    iget-object v1, p0, Lddu;->c:[Lddr;

    if-eqz v1, :cond_3

    .line 575
    iget-object v2, p0, Lddu;->c:[Lddr;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 576
    if-eqz v4, :cond_2

    .line 577
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 575
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 581
    :cond_3
    iget-object v1, p0, Lddu;->d:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 582
    const/4 v1, 0x3

    iget-object v2, p0, Lddu;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 584
    :cond_4
    iget-object v1, p0, Lddu;->e:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 585
    const/4 v1, 0x4

    iget-object v2, p0, Lddu;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 587
    :cond_5
    iget-object v1, p0, Lddu;->f:[Lddv;

    if-eqz v1, :cond_7

    .line 588
    iget-object v1, p0, Lddu;->f:[Lddv;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 589
    if-eqz v3, :cond_6

    .line 590
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 588
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 594
    :cond_7
    iget-object v0, p0, Lddu;->g:Lddt;

    if-eqz v0, :cond_8

    .line 595
    const/4 v0, 0x6

    iget-object v1, p0, Lddu;->g:Lddt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 597
    :cond_8
    iget-object v0, p0, Lddu;->h:Ldds;

    if-eqz v0, :cond_9

    .line 598
    const/4 v0, 0x7

    iget-object v1, p0, Lddu;->h:Ldds;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 600
    :cond_9
    iget-object v0, p0, Lddu;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 601
    const/16 v0, 0x8

    iget-object v1, p0, Lddu;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 603
    :cond_a
    iget-object v0, p0, Lddu;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 605
    return-void
.end method
