.class public final Ldwr;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldwr;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8276
    const/4 v0, 0x0

    new-array v0, v0, [Ldwr;

    sput-object v0, Ldwr;->a:[Ldwr;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8277
    invoke-direct {p0}, Lepn;-><init>()V

    .line 8286
    const/4 v0, 0x0

    iput-object v0, p0, Ldwr;->b:Ljava/lang/Integer;

    .line 8277
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 8305
    const/4 v0, 0x0

    .line 8306
    iget-object v1, p0, Ldwr;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 8307
    const/4 v0, 0x1

    iget-object v1, p0, Ldwr;->b:Ljava/lang/Integer;

    .line 8308
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8310
    :cond_0
    iget-object v1, p0, Ldwr;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 8311
    const/4 v1, 0x2

    iget-object v2, p0, Ldwr;->c:Ljava/lang/Long;

    .line 8312
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 8314
    :cond_1
    iget-object v1, p0, Ldwr;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8315
    iput v0, p0, Ldwr;->cachedSize:I

    .line 8316
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 8273
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldwr;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldwr;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldwr;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwr;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwr;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldwr;->c:Ljava/lang/Long;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 8293
    iget-object v0, p0, Ldwr;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 8294
    const/4 v0, 0x1

    iget-object v1, p0, Ldwr;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 8296
    :cond_0
    iget-object v0, p0, Ldwr;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 8297
    const/4 v0, 0x2

    iget-object v1, p0, Ldwr;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 8299
    :cond_1
    iget-object v0, p0, Ldwr;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 8301
    return-void
.end method
