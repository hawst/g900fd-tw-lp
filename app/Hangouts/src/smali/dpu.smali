.class public final Ldpu;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldpu;


# instance fields
.field public b:Ldps;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16198
    const/4 v0, 0x0

    new-array v0, v0, [Ldpu;

    sput-object v0, Ldpu;->a:[Ldpu;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16199
    invoke-direct {p0}, Lepn;-><init>()V

    .line 16202
    iput-object v0, p0, Ldpu;->b:Ldps;

    .line 16205
    iput-object v0, p0, Ldpu;->c:Ljava/lang/Integer;

    .line 16199
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 16227
    const/4 v0, 0x0

    .line 16228
    iget-object v1, p0, Ldpu;->b:Ldps;

    if-eqz v1, :cond_0

    .line 16229
    const/4 v0, 0x1

    iget-object v1, p0, Ldpu;->b:Ldps;

    .line 16230
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 16232
    :cond_0
    iget-object v1, p0, Ldpu;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 16233
    const/4 v1, 0x2

    iget-object v2, p0, Ldpu;->c:Ljava/lang/Integer;

    .line 16234
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 16236
    :cond_1
    iget-object v1, p0, Ldpu;->d:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 16237
    const/4 v1, 0x3

    iget-object v2, p0, Ldpu;->d:Ljava/lang/Long;

    .line 16238
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 16240
    :cond_2
    iget-object v1, p0, Ldpu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16241
    iput v0, p0, Ldpu;->cachedSize:I

    .line 16242
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 16195
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldpu;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldpu;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldpu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldpu;->b:Ldps;

    if-nez v0, :cond_2

    new-instance v0, Ldps;

    invoke-direct {v0}, Ldps;-><init>()V

    iput-object v0, p0, Ldpu;->b:Ldps;

    :cond_2
    iget-object v0, p0, Ldpu;->b:Ldps;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_3

    const/16 v1, 0xa

    if-eq v0, v1, :cond_3

    const/16 v1, 0x14

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_3

    const/16 v1, 0x28

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpu;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpu;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldpu;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 16212
    iget-object v0, p0, Ldpu;->b:Ldps;

    if-eqz v0, :cond_0

    .line 16213
    const/4 v0, 0x1

    iget-object v1, p0, Ldpu;->b:Ldps;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 16215
    :cond_0
    iget-object v0, p0, Ldpu;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 16216
    const/4 v0, 0x2

    iget-object v1, p0, Ldpu;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 16218
    :cond_1
    iget-object v0, p0, Ldpu;->d:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 16219
    const/4 v0, 0x3

    iget-object v1, p0, Ldpu;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 16221
    :cond_2
    iget-object v0, p0, Ldpu;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 16223
    return-void
.end method
