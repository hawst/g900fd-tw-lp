.class public Lbew;
.super Lbei;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Lbdk;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2089
    invoke-direct {p0, p2, p1}, Lbei;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2090
    const/4 v0, 0x0

    iput-object v0, p0, Lbew;->a:Lbdk;

    .line 2091
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lbew;
    .locals 1

    .prologue
    .line 2095
    new-instance v0, Lbew;

    invoke-direct {v0, p0, p1}, Lbew;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 3

    .prologue
    .line 2100
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 2101
    const-string v0, "Babel_RequestWriter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RemoveUserRequest build protobuf convID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbew;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " clientGeneratedId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbew;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " participantId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbew;->a:Lbdk;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2106
    :cond_0
    new-instance v0, Ldrx;

    invoke-direct {v0}, Ldrx;-><init>()V

    .line 2108
    iget-object v1, p0, Lbew;->i:Ljava/lang/String;

    .line 2109
    invoke-static {v1}, Lyt;->c(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Ldrx;->c:Ljava/lang/Long;

    .line 2110
    iget-object v1, p0, Lbew;->c:Ljava/lang/String;

    invoke-static {v1}, Lbea;->d(Ljava/lang/String;)Ldqf;

    move-result-object v1

    iput-object v1, v0, Ldrx;->b:Ldqf;

    .line 2111
    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldrx;->f:Ljava/lang/Integer;

    .line 2114
    new-instance v1, Ldvf;

    invoke-direct {v1}, Ldvf;-><init>()V

    .line 2115
    iput-object v0, v1, Ldvf;->c:Ldrx;

    .line 2116
    iget-object v0, p0, Lbew;->a:Lbdk;

    if-eqz v0, :cond_1

    .line 2117
    iget-object v0, p0, Lbew;->a:Lbdk;

    invoke-virtual {v0}, Lbdk;->d()Ldui;

    move-result-object v0

    iput-object v0, v1, Ldvf;->d:Ldui;

    .line 2119
    :cond_1
    invoke-virtual {p0, p1, p2}, Lbew;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v0

    iput-object v0, v1, Ldvf;->b:Ldvm;

    .line 2122
    return-object v1
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2127
    const-string v0, "conversations/removeuser"

    return-object v0
.end method
