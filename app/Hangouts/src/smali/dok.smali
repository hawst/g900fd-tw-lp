.class public final Ldok;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldok;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ldor;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3638
    const/4 v0, 0x0

    new-array v0, v0, [Ldok;

    sput-object v0, Ldok;->a:[Ldok;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3639
    invoke-direct {p0}, Lepn;-><init>()V

    .line 3642
    iput-object v0, p0, Ldok;->b:Ljava/lang/Integer;

    .line 3645
    iput-object v0, p0, Ldok;->c:Ldor;

    .line 3639
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 3677
    const/4 v0, 0x0

    .line 3678
    iget-object v1, p0, Ldok;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 3679
    const/4 v0, 0x1

    iget-object v1, p0, Ldok;->b:Ljava/lang/Integer;

    .line 3680
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3682
    :cond_0
    iget-object v1, p0, Ldok;->c:Ldor;

    if-eqz v1, :cond_1

    .line 3683
    const/4 v1, 0x2

    iget-object v2, p0, Ldok;->c:Ldor;

    .line 3684
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3686
    :cond_1
    iget-object v1, p0, Ldok;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 3687
    const/4 v1, 0x3

    iget-object v2, p0, Ldok;->d:Ljava/lang/String;

    .line 3688
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3690
    :cond_2
    iget-object v1, p0, Ldok;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 3691
    const/4 v1, 0x4

    iget-object v2, p0, Ldok;->e:Ljava/lang/String;

    .line 3692
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3694
    :cond_3
    iget-object v1, p0, Ldok;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 3695
    const/4 v1, 0x5

    iget-object v2, p0, Ldok;->f:Ljava/lang/Integer;

    .line 3696
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3698
    :cond_4
    iget-object v1, p0, Ldok;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3699
    iput v0, p0, Ldok;->cachedSize:I

    .line 3700
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 3635
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldok;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldok;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldok;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/16 v1, 0x64

    if-eq v0, v1, :cond_2

    const/16 v1, 0x65

    if-eq v0, v1, :cond_2

    const/16 v1, 0x66

    if-eq v0, v1, :cond_2

    const/16 v1, 0x67

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_2

    const/16 v1, 0xdd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xca

    if-eq v0, v1, :cond_2

    const/16 v1, 0xcb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xcc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xcd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xce

    if-eq v0, v1, :cond_2

    const/16 v1, 0xcf

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd0

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd1

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd2

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd3

    if-eq v0, v1, :cond_2

    const/16 v1, 0xde

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd4

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd5

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd6

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd7

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd8

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xda

    if-eq v0, v1, :cond_2

    const/16 v1, 0xdb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xdc

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x130

    if-eq v0, v1, :cond_2

    const/16 v1, 0x131

    if-eq v0, v1, :cond_2

    const/16 v1, 0x132

    if-eq v0, v1, :cond_2

    const/16 v1, 0x133

    if-eq v0, v1, :cond_2

    const/16 v1, 0x134

    if-eq v0, v1, :cond_2

    const/16 v1, 0x135

    if-eq v0, v1, :cond_2

    const/16 v1, 0x136

    if-eq v0, v1, :cond_2

    const/16 v1, 0x137

    if-eq v0, v1, :cond_2

    const/16 v1, 0x138

    if-eq v0, v1, :cond_2

    const/16 v1, 0x139

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13c

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldok;->b:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldok;->b:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_2
    iget-object v0, p0, Ldok;->c:Ldor;

    if-nez v0, :cond_4

    new-instance v0, Ldor;

    invoke-direct {v0}, Ldor;-><init>()V

    iput-object v0, p0, Ldok;->c:Ldor;

    :cond_4
    iget-object v0, p0, Ldok;->c:Ldor;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldok;->d:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldok;->e:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldok;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 3656
    iget-object v0, p0, Ldok;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 3657
    const/4 v0, 0x1

    iget-object v1, p0, Ldok;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 3659
    :cond_0
    iget-object v0, p0, Ldok;->c:Ldor;

    if-eqz v0, :cond_1

    .line 3660
    const/4 v0, 0x2

    iget-object v1, p0, Ldok;->c:Ldor;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 3662
    :cond_1
    iget-object v0, p0, Ldok;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3663
    const/4 v0, 0x3

    iget-object v1, p0, Ldok;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3665
    :cond_2
    iget-object v0, p0, Ldok;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 3666
    const/4 v0, 0x4

    iget-object v1, p0, Ldok;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3668
    :cond_3
    iget-object v0, p0, Ldok;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 3669
    const/4 v0, 0x5

    iget-object v1, p0, Ldok;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 3671
    :cond_4
    iget-object v0, p0, Ldok;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 3673
    return-void
.end method
