.class public final Lapp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lapu;
.implements Law;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lapu;",
        "Law",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field private final b:Lapk;

.field private c:Lapq;

.field private d:Landroid/content/Context;

.field private e:Lkd;

.field private f:Lyj;

.field private g:Lapv;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkd;Lyj;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    iput-object v0, p0, Lapp;->b:Lapk;

    .line 63
    iput-object p1, p0, Lapp;->d:Landroid/content/Context;

    .line 64
    iput-object p2, p0, Lapp;->e:Lkd;

    .line 65
    iput-object p3, p0, Lapp;->f:Lyj;

    .line 66
    iput-object p4, p0, Lapp;->a:Ljava/lang/String;

    .line 68
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 70
    iget-object v0, p0, Lapp;->e:Lkd;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lkd;->a(Z)V

    .line 71
    iget-object v0, p0, Lapp;->e:Lkd;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lkd;->a(II)V

    .line 73
    :cond_0
    return-void
.end method

.method static synthetic a(Lapp;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 27
    iget-object v0, p0, Lapp;->b:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lapx;->J()I

    move-result v2

    if-ne v2, v3, :cond_2

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lapx;->S()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v3, :cond_3

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laqd;

    invoke-virtual {v0}, Laqd;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Laqd;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbzd;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Laqd;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbzd;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-eqz v1, :cond_1

    invoke-direct {p0, v1, v0}, Lapp;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    invoke-virtual {v0}, Laqd;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbzd;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lapp;->b()V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lapp;->b()V

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lapp;->e:Lkd;

    invoke-virtual {v0, p1}, Lkd;->a(Ljava/lang/CharSequence;)V

    .line 189
    iget-object v0, p0, Lapp;->e:Lkd;

    invoke-virtual {v0, p2}, Lkd;->b(Ljava/lang/CharSequence;)V

    .line 190
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 194
    return-void
.end method

.method public a(Lapv;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 77
    iput-object p1, p0, Lapp;->g:Lapv;

    .line 78
    new-instance v0, Lapq;

    invoke-direct {v0, p0, v2}, Lapq;-><init>(Lapp;B)V

    iput-object v0, p0, Lapp;->c:Lapq;

    .line 79
    iget-object v0, p0, Lapp;->b:Lapk;

    iget-object v1, p0, Lapp;->c:Lapq;

    invoke-virtual {v0, v1}, Lapk;->a(Lapo;)V

    .line 80
    iget-object v0, p0, Lapp;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {p1}, Lapv;->f()Lav;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1, p0}, Lav;->a(ILandroid/os/Bundle;Law;)Ldg;

    .line 85
    :cond_0
    iget-object v0, p0, Lapp;->c:Lapq;

    invoke-virtual {v0}, Lapq;->i()V

    .line 86
    return-void
.end method

.method b()V
    .locals 3

    .prologue
    .line 107
    iget-object v0, p0, Lapp;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lapp;->g:Lapv;

    invoke-virtual {v0}, Lapv;->f()Lav;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lav;->b(ILandroid/os/Bundle;Law;)Ldg;

    .line 111
    :cond_0
    return-void
.end method

.method public m_()V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lapp;->c:Lapq;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lapp;->b:Lapk;

    iget-object v1, p0, Lapp;->c:Lapq;

    invoke-virtual {v0, v1}, Lapk;->b(Lapo;)V

    .line 141
    const/4 v0, 0x0

    iput-object v0, p0, Lapp;->c:Lapq;

    .line 143
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 198
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Ldg;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 90
    if-eqz p1, :cond_1

    .line 103
    :cond_0
    :goto_0
    return-object v7

    .line 93
    :cond_1
    iget-object v0, p0, Lapp;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 96
    const-string v5, "conversation_id=?"

    .line 97
    new-instance v0, Lbaj;

    iget-object v1, p0, Lapp;->d:Landroid/content/Context;

    iget-object v2, p0, Lapp;->f:Lyj;

    sget-object v3, Lcom/google/android/apps/hangouts/content/EsProvider;->e:Landroid/net/Uri;

    iget-object v4, p0, Lapp;->g:Lapv;

    .line 99
    invoke-virtual {v4}, Lapv;->a()Lyj;

    move-result-object v4

    .line 98
    invoke-static {v3, v4}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Landroid/net/Uri;Lyj;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lahe;->a:[Ljava/lang/String;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v8, 0x0

    iget-object v9, p0, Lapp;->a:Ljava/lang/String;

    aput-object v9, v6, v8

    invoke-direct/range {v0 .. v7}, Lbaj;-><init>(Landroid/content/Context;Lyj;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v7, v0

    .line 103
    goto :goto_0
.end method

.method public synthetic onLoadFinished(Ldg;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 27
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p1}, Ldg;->l()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lapp;->b:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lapx;->J()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    :cond_0
    if-eqz p2, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x4

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lapp;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method public onLoaderReset(Ldg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 202
    return-void
.end method
