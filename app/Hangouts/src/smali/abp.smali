.class public final Labp;
.super Lqj;
.source "PG"

# interfaces
.implements Laac;


# instance fields
.field private aj:Landroid/graphics/drawable/Drawable;

.field private ak:Lyj;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lqj;-><init>()V

    return-void
.end method

.method public static b(Landroid/content/Intent;IZ)Labp;
    .locals 2

    .prologue
    .line 75
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 76
    const-string v1, "arg-intent"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 77
    const-string v1, "arg-position"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 78
    const-string v1, "arg-show-spinner"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 79
    new-instance v1, Labp;

    invoke-direct {v1}, Labp;-><init>()V

    .line 80
    invoke-virtual {v1, v0}, Labp;->setArguments(Landroid/os/Bundle;)V

    .line 81
    return-object v1
.end method


# virtual methods
.method public a(Lbzn;Lbyd;ZLzx;Z)V
    .locals 3

    .prologue
    .line 118
    invoke-static {p2}, Lcwz;->a(Ljava/lang/Object;)V

    .line 119
    invoke-static {p1}, Lcwz;->a(Ljava/lang/Object;)V

    .line 121
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    if-eqz p3, :cond_0

    sget v0, Lh;->nl:I

    :goto_0
    const/4 v2, 0x0

    .line 120
    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 123
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 124
    return-void

    .line 121
    :cond_0
    sget v0, Lh;->nk:I

    goto :goto_0
.end method

.method public a(Ldg;Lqn;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Lqn;",
            ">;",
            "Lqn;",
            ")V"
        }
    .end annotation

    .prologue
    .line 102
    iget v0, p2, Lqn;->c:I

    if-nez v0, :cond_1

    .line 103
    invoke-super {p0, p1, p2}, Lqj;->a(Ldg;Lqn;)V

    .line 104
    invoke-virtual {p1}, Ldg;->l()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 105
    invoke-virtual {p0}, Labp;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p2, v0}, Lqn;->a(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Labp;->aj:Landroid/graphics/drawable/Drawable;

    .line 106
    iget-object v0, p0, Labp;->aj:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Lccm;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Labp;->aj:Landroid/graphics/drawable/Drawable;

    check-cast v0, Lccm;

    invoke-virtual {v0}, Lccm;->a()V

    .line 109
    :cond_0
    invoke-virtual {p0}, Labp;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->u_()V

    .line 112
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 38
    invoke-super {p0, p1}, Lqj;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Labp;->setHasOptionsMenu(Z)V

    .line 40
    invoke-virtual {p0}, Labp;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    iput-object v0, p0, Labp;->ak:Lyj;

    .line 43
    iget-object v0, p0, Labp;->ak:Lyj;

    if-nez v0, :cond_0

    .line 44
    invoke-virtual {p0}, Labp;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->finish()V

    .line 46
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 50
    sget v0, Lf;->hc:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 51
    return-void
.end method

.method public synthetic onLoadFinished(Ldg;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 31
    check-cast p2, Lqn;

    invoke-virtual {p0, p1, p2}, Labp;->a(Ldg;Lqn;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 61
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lg;->gx:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Labp;->aj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 62
    new-instance v1, Lbyq;

    iget-object v0, p0, Labp;->a:Ljava/lang/String;

    iget-object v2, p0, Labp;->ak:Lyj;

    invoke-direct {v1, v0, v2}, Lbyq;-><init>(Ljava/lang/String;Lyj;)V

    .line 63
    invoke-virtual {v1}, Lbyq;->b()Lbyq;

    .line 64
    iget-object v0, p0, Labp;->c:Landroid/content/Intent;

    const-string v2, "content_type"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 65
    new-instance v0, Lzx;

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lzx;-><init>(Lbyq;Laac;Ljava/lang/String;ZZLjava/lang/Object;)V

    .line 66
    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v1

    invoke-virtual {v1, v0}, Lbsn;->c(Lbrv;)V

    .line 69
    :goto_0
    return v5

    :cond_0
    invoke-super {p0, p1}, Lqj;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v5

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 94
    invoke-super {p0}, Lqj;->onPause()V

    .line 95
    iget-object v0, p0, Labp;->aj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Labp;->aj:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Lccm;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Labp;->aj:Landroid/graphics/drawable/Drawable;

    check-cast v0, Lccm;

    invoke-virtual {v0}, Lccm;->b()V

    .line 98
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 55
    sget v0, Lg;->gx:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 56
    iget-object v0, p0, Labp;->aj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 57
    return-void

    .line 56
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 86
    invoke-super {p0}, Lqj;->onResume()V

    .line 87
    iget-object v0, p0, Labp;->aj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Labp;->aj:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Lccm;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Labp;->aj:Landroid/graphics/drawable/Drawable;

    check-cast v0, Lccm;

    invoke-virtual {v0}, Lccm;->a()V

    .line 90
    :cond_0
    return-void
.end method
