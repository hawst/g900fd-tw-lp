.class public final Lbyv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# instance fields
.field private final a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lbyx;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/os/Looper;

.field private c:Ljava/lang/String;

.field private d:Lbyw;

.field private e:Lbyy;

.field private final f:Ljava/lang/Object;

.field private g:Landroid/media/MediaPlayer;

.field private h:Landroid/os/PowerManager$WakeLock;

.field private i:Landroid/media/AudioManager;

.field private j:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lbyv;->a:Ljava/util/LinkedList;

    .line 243
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbyv;->f:Ljava/lang/Object;

    .line 250
    const/4 v0, 0x2

    iput v0, p0, Lbyv;->j:I

    .line 258
    iput-object p1, p0, Lbyv;->c:Ljava/lang/String;

    .line 261
    return-void
.end method

.method static synthetic a(Lbyv;Landroid/media/AudioManager;)Landroid/media/AudioManager;
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lbyv;->i:Landroid/media/AudioManager;

    return-object p1
.end method

.method static synthetic a(Lbyv;)Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lbyv;->g:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic a(Lbyv;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lbyv;->g:Landroid/media/MediaPlayer;

    return-object p1
.end method

.method static synthetic a(Lbyv;Landroid/os/Looper;)Landroid/os/Looper;
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lbyv;->b:Landroid/os/Looper;

    return-object p1
.end method

.method static synthetic a(Lbyv;Lbyx;)V
    .locals 5

    .prologue
    .line 43
    :try_start_0
    iget-object v1, p0, Lbyv;->f:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lbyv;->b:Landroid/os/Looper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbyv;->b:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getState()Ljava/lang/Thread$State;

    move-result-object v0

    sget-object v2, Ljava/lang/Thread$State;->TERMINATED:Ljava/lang/Thread$State;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lbyv;->b:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    :cond_0
    new-instance v0, Lbyy;

    invoke-direct {v0, p0, p1}, Lbyy;-><init>(Lbyv;Lbyx;)V

    iput-object v0, p0, Lbyv;->e:Lbyy;

    iget-object v2, p0, Lbyv;->e:Lbyy;

    monitor-enter v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v0, p0, Lbyv;->e:Lbyy;

    invoke-virtual {v0}, Lbyy;->start()V

    iget-object v0, p0, Lbyv;->e:Lbyy;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p1, Lbyx;->f:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lbyv;->c:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Notification sound delayed by "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "msecs"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :cond_1
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v2

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1

    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lbyv;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "error loading sound for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lbyx;->b:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private a(Lbyx;)V
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lbyv;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 320
    iget-object v0, p0, Lbyv;->d:Lbyw;

    if-nez v0, :cond_1

    .line 321
    iget-object v0, p0, Lbyv;->h:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbyv;->h:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 322
    :cond_0
    new-instance v0, Lbyw;

    invoke-direct {v0, p0}, Lbyw;-><init>(Lbyv;)V

    iput-object v0, p0, Lbyv;->d:Lbyw;

    .line 323
    iget-object v0, p0, Lbyv;->d:Lbyw;

    invoke-virtual {v0}, Lbyw;->start()V

    .line 325
    :cond_1
    return-void
.end method

.method static synthetic b(Lbyv;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lbyv;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lbyv;Lbyx;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 43
    iget-object v0, p0, Lbyv;->g:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p1, Lbyx;->f:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lbyv;->c:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Notification stop delayed by "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "msecs"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lbyv;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    iget-object v0, p0, Lbyv;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    iput-object v5, p0, Lbyv;->g:Landroid/media/MediaPlayer;

    iget-boolean v0, p1, Lbyx;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbyv;->i:Landroid/media/AudioManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbyv;->i:Landroid/media/AudioManager;

    invoke-virtual {v0, v5}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    :cond_1
    iput-object v5, p0, Lbyv;->i:Landroid/media/AudioManager;

    iget-object v0, p0, Lbyv;->b:Landroid/os/Looper;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbyv;->b:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getState()Ljava/lang/Thread$State;

    move-result-object v0

    sget-object v1, Ljava/lang/Thread$State;->TERMINATED:Ljava/lang/Thread$State;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lbyv;->b:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    :cond_2
    return-void
.end method

.method static synthetic c(Lbyv;)Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lbyv;->a:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic d(Lbyv;)Lbyw;
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lbyv;->d:Lbyw;

    return-object v0
.end method

.method static synthetic e(Lbyv;)V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lbyv;->h:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbyv;->h:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 294
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbyv;->a(Z)V

    .line 295
    return-void
.end method

.method public a(Landroid/net/Uri;ZIF)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 279
    new-instance v0, Lbyx;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lbyx;-><init>(B)V

    .line 280
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, v0, Lbyx;->f:J

    .line 281
    iput v3, v0, Lbyx;->a:I

    .line 282
    iput-object p1, v0, Lbyx;->b:Landroid/net/Uri;

    .line 283
    iput-boolean p2, v0, Lbyx;->c:Z

    .line 284
    iput p3, v0, Lbyx;->d:I

    .line 285
    iput p4, v0, Lbyx;->e:F

    .line 286
    iget-object v1, p0, Lbyv;->a:Ljava/util/LinkedList;

    monitor-enter v1

    .line 287
    :try_start_0
    invoke-direct {p0, v0}, Lbyv;->a(Lbyx;)V

    .line 288
    const/4 v0, 0x1

    iput v0, p0, Lbyv;->j:I

    .line 289
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public a(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 304
    iget-object v1, p0, Lbyv;->a:Ljava/util/LinkedList;

    monitor-enter v1

    .line 307
    :try_start_0
    iget v0, p0, Lbyv;->j:I

    if-eq v0, v2, :cond_0

    .line 308
    new-instance v0, Lbyx;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lbyx;-><init>(B)V

    .line 309
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v0, Lbyx;->f:J

    .line 310
    const/4 v2, 0x2

    iput v2, v0, Lbyx;->a:I

    .line 311
    iput-boolean p1, v0, Lbyx;->g:Z

    .line 312
    invoke-direct {p0, v0}, Lbyv;->a(Lbyx;)V

    .line 313
    const/4 v0, 0x2

    iput v0, p0, Lbyv;->j:I

    .line 315
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 224
    iget-object v0, p0, Lbyv;->i:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lbyv;->i:Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 228
    :cond_0
    iget-object v1, p0, Lbyv;->a:Ljava/util/LinkedList;

    monitor-enter v1

    .line 229
    :try_start_0
    iget-object v0, p0, Lbyv;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 230
    iget-object v2, p0, Lbyv;->f:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 231
    :try_start_1
    iget-object v0, p0, Lbyv;->b:Landroid/os/Looper;

    if-eqz v0, :cond_1

    .line 232
    iget-object v0, p0, Lbyv;->b:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 234
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lbyv;->e:Lbyy;

    .line 235
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237
    :cond_2
    :try_start_2
    monitor-exit v1

    return-void

    .line 235
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 237
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method
