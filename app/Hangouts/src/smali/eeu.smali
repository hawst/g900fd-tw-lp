.class abstract enum Leeu;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Leeu;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Leeu;

.field public static final enum b:Leeu;

.field public static final enum c:Leeu;

.field public static final enum d:Leeu;

.field public static final enum e:Leeu;

.field private static final synthetic f:[Leeu;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 691
    new-instance v0, Leev;

    const-string v1, "EXPLICIT"

    invoke-direct {v0, v1}, Leev;-><init>(Ljava/lang/String;)V

    sput-object v0, Leeu;->a:Leeu;

    .line 705
    new-instance v0, Leew;

    const-string v1, "REPLACED"

    invoke-direct {v0, v1}, Leew;-><init>(Ljava/lang/String;)V

    sput-object v0, Leeu;->b:Leeu;

    .line 717
    new-instance v0, Leex;

    const-string v1, "COLLECTED"

    invoke-direct {v0, v1}, Leex;-><init>(Ljava/lang/String;)V

    sput-object v0, Leeu;->c:Leeu;

    .line 729
    new-instance v0, Leey;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1}, Leey;-><init>(Ljava/lang/String;)V

    sput-object v0, Leeu;->d:Leeu;

    .line 741
    new-instance v0, Leez;

    const-string v1, "SIZE"

    invoke-direct {v0, v1}, Leez;-><init>(Ljava/lang/String;)V

    sput-object v0, Leeu;->e:Leeu;

    .line 686
    const/4 v0, 0x5

    new-array v0, v0, [Leeu;

    const/4 v1, 0x0

    sget-object v2, Leeu;->a:Leeu;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Leeu;->b:Leeu;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Leeu;->c:Leeu;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Leeu;->d:Leeu;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Leeu;->e:Leeu;

    aput-object v2, v0, v1

    sput-object v0, Leeu;->f:[Leeu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 686
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 686
    invoke-direct {p0, p1, p2}, Leeu;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Leeu;
    .locals 1

    .prologue
    .line 686
    const-class v0, Leeu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Leeu;

    return-object v0
.end method

.method public static values()[Leeu;
    .locals 1

    .prologue
    .line 686
    sget-object v0, Leeu;->f:[Leeu;

    invoke-virtual {v0}, [Leeu;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Leeu;

    return-object v0
.end method
