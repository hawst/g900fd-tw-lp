.class public final Ldiy;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldiy;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/Long;

.field public e:[Leqq;

.field public f:Ljava/lang/Integer;

.field public g:[Leqy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Ldiy;

    sput-object v0, Ldiy;->a:[Ldiy;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Ldiy;->b:Ljava/lang/Integer;

    .line 28
    sget-object v0, Leqq;->a:[Leqq;

    iput-object v0, p0, Ldiy;->e:[Leqq;

    .line 33
    sget-object v0, Leqy;->a:[Leqy;

    iput-object v0, p0, Ldiy;->g:[Leqy;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 71
    iget-object v0, p0, Ldiy;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 72
    const/4 v0, 0x1

    iget-object v2, p0, Ldiy;->b:Ljava/lang/Integer;

    .line 73
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 75
    :goto_0
    iget-object v2, p0, Ldiy;->c:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 76
    const/4 v2, 0x2

    iget-object v3, p0, Ldiy;->c:Ljava/lang/Long;

    .line 77
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 79
    :cond_0
    iget-object v2, p0, Ldiy;->d:Ljava/lang/Long;

    if-eqz v2, :cond_1

    .line 80
    const/4 v2, 0x3

    iget-object v3, p0, Ldiy;->d:Ljava/lang/Long;

    .line 81
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 83
    :cond_1
    iget-object v2, p0, Ldiy;->e:[Leqq;

    if-eqz v2, :cond_3

    .line 84
    iget-object v3, p0, Ldiy;->e:[Leqq;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 85
    if-eqz v5, :cond_2

    .line 86
    const/4 v6, 0x4

    .line 87
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 84
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 91
    :cond_3
    iget-object v2, p0, Ldiy;->f:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    .line 92
    const/4 v2, 0x5

    iget-object v3, p0, Ldiy;->f:Ljava/lang/Integer;

    .line 93
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 95
    :cond_4
    iget-object v2, p0, Ldiy;->g:[Leqy;

    if-eqz v2, :cond_6

    .line 96
    iget-object v2, p0, Ldiy;->g:[Leqy;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 97
    if-eqz v4, :cond_5

    .line 98
    const/4 v5, 0x6

    .line 99
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 96
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 103
    :cond_6
    iget-object v1, p0, Ldiy;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 104
    iput v0, p0, Ldiy;->cachedSize:I

    .line 105
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldiy;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldiy;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldiy;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-ne v0, v2, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldiy;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldiy;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldiy;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldiy;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldiy;->e:[Leqq;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leqq;

    iget-object v3, p0, Ldiy;->e:[Leqq;

    if-eqz v3, :cond_4

    iget-object v3, p0, Ldiy;->e:[Leqq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Ldiy;->e:[Leqq;

    :goto_2
    iget-object v2, p0, Ldiy;->e:[Leqq;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ldiy;->e:[Leqq;

    new-instance v3, Leqq;

    invoke-direct {v3}, Leqq;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldiy;->e:[Leqq;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Ldiy;->e:[Leqq;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Ldiy;->e:[Leqq;

    new-instance v3, Leqq;

    invoke-direct {v3}, Leqq;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldiy;->e:[Leqq;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldiy;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldiy;->g:[Leqy;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Leqy;

    iget-object v3, p0, Ldiy;->g:[Leqy;

    if-eqz v3, :cond_7

    iget-object v3, p0, Ldiy;->g:[Leqy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Ldiy;->g:[Leqy;

    :goto_4
    iget-object v2, p0, Ldiy;->g:[Leqy;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Ldiy;->g:[Leqy;

    new-instance v3, Leqy;

    invoke-direct {v3}, Leqy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldiy;->g:[Leqy;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    iget-object v0, p0, Ldiy;->g:[Leqy;

    array-length v0, v0

    goto :goto_3

    :cond_9
    iget-object v2, p0, Ldiy;->g:[Leqy;

    new-instance v3, Leqy;

    invoke-direct {v3}, Leqy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldiy;->g:[Leqy;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 38
    iget-object v1, p0, Ldiy;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 39
    const/4 v1, 0x1

    iget-object v2, p0, Ldiy;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 41
    :cond_0
    iget-object v1, p0, Ldiy;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 42
    const/4 v1, 0x2

    iget-object v2, p0, Ldiy;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->b(IJ)V

    .line 44
    :cond_1
    iget-object v1, p0, Ldiy;->d:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 45
    const/4 v1, 0x3

    iget-object v2, p0, Ldiy;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->b(IJ)V

    .line 47
    :cond_2
    iget-object v1, p0, Ldiy;->e:[Leqq;

    if-eqz v1, :cond_4

    .line 48
    iget-object v2, p0, Ldiy;->e:[Leqq;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 49
    if-eqz v4, :cond_3

    .line 50
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 48
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 54
    :cond_4
    iget-object v1, p0, Ldiy;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 55
    const/4 v1, 0x5

    iget-object v2, p0, Ldiy;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 57
    :cond_5
    iget-object v1, p0, Ldiy;->g:[Leqy;

    if-eqz v1, :cond_7

    .line 58
    iget-object v1, p0, Ldiy;->g:[Leqy;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 59
    if-eqz v3, :cond_6

    .line 60
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 58
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 64
    :cond_7
    iget-object v0, p0, Ldiy;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 66
    return-void
.end method
