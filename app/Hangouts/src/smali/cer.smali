.class public final Lcer;
.super Lcct;
.source "PG"

# interfaces
.implements Lcdf;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Lyj;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcer;-><init>(Landroid/content/Context;B)V

    .line 32
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcct;-><init>(Landroid/content/Context;)V

    .line 36
    return-void
.end method

.method static synthetic a(Lcer;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcer;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcer;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcer;->c()V

    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 86
    iget-object v0, p0, Lcer;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcer;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcer;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcer;->e:Lyj;

    iget-object v1, p0, Lcer;->a:Ljava/lang/String;

    iget-object v2, p0, Lcer;->b:Ljava/lang/String;

    const-string v3, "video/*"

    invoke-static {v0, v1, v2, v3}, Lyp;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcer;->d:Ljava/lang/String;

    .line 90
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 100
    return-void
.end method

.method public a(Lyj;ZLjava/lang/String;Lt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 40
    iput-object p1, p0, Lcer;->e:Lyj;

    .line 41
    iput-object p6, p0, Lcer;->a:Ljava/lang/String;

    .line 42
    iput-object p5, p0, Lcer;->b:Ljava/lang/String;

    .line 43
    invoke-virtual {p0, p7}, Lcer;->d_(Ljava/lang/String;)V

    .line 45
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcer;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 46
    :cond_0
    invoke-direct {p0}, Lcer;->c()V

    .line 49
    :cond_1
    new-instance v0, Lces;

    invoke-direct {v0, p0, p4}, Lces;-><init>(Lcer;Lt;)V

    invoke-virtual {p0, v0}, Lcer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    sget v0, Lh;->or:I

    invoke-virtual {p0, v0}, Lcer;->a(I)V

    .line 76
    iget-object v0, p0, Lcer;->e:Lyj;

    invoke-super {p0, v0, p2, p3}, Lcct;->a(Lyj;ZLjava/lang/String;)V

    .line 77
    return-void
.end method

.method public d_(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcer;->d:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 81
    invoke-super {p0}, Lcct;->g()V

    .line 82
    invoke-direct {p0}, Lcer;->c()V

    .line 83
    return-void
.end method
