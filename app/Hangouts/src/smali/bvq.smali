.class public final Lbvq;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Z

.field private static final b:Lbvt;

.field private static final c:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lbys;->n:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbvq;->a:Z

    .line 321
    new-instance v0, Lbvt;

    invoke-direct {v0}, Lbvt;-><init>()V

    sput-object v0, Lbvq;->b:Lbvt;

    .line 649
    const/4 v0, 0x2

    .line 650
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lbvq;->c:Ljava/lang/Integer;

    .line 649
    return-void
.end method

.method static synthetic a(Landroid/content/Intent;)I
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 56
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v0, v2, :cond_0

    const-string v0, "networkType"

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const-string v0, "networkInfo"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Lbvs;
    .locals 3

    .prologue
    .line 325
    new-instance v0, Lbvs;

    invoke-direct {v0}, Lbvs;-><init>()V

    .line 326
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 327
    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 328
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 329
    return-object v0
.end method

.method static synthetic a()Lbvt;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lbvq;->b:Lbvt;

    return-object v0
.end method

.method private static a(Ljava/lang/String;I)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation

    .prologue
    .line 737
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 739
    if-eqz p1, :cond_3

    .line 740
    :try_start_0
    invoke-static {p0}, Ljava/net/InetAddress;->getAllByName(Ljava/lang/String;)[Ljava/net/InetAddress;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_3

    aget-object v4, v2, v0

    .line 741
    sget-boolean v5, Lbvq;->a:Z

    if-eqz v5, :cond_0

    .line 742
    const-string v5, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "MMS host resolved address "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 744
    :cond_0
    and-int/lit8 v5, p1, 0x2

    if-eqz v5, :cond_2

    instance-of v5, v4, Ljava/net/Inet6Address;

    if-eqz v5, :cond_2

    .line 747
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 740
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 748
    :cond_2
    and-int/lit8 v5, p1, 0x1

    if-eqz v5, :cond_1

    instance-of v5, v4, Ljava/net/Inet4Address;

    if-eqz v5, :cond_1

    .line 751
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 760
    :catch_0
    move-exception v0

    .line 761
    new-instance v1, Lbvu;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to resolve "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lbvu;-><init>(Ljava/lang/String;)V

    throw v1

    .line 755
    :cond_3
    :try_start_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_4

    .line 756
    new-instance v0, Lbvu;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to resolve "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for allowed address types, addressTypes="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbvu;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_0

    .line 759
    :cond_4
    return-object v1
.end method

.method public static a(Landroid/content/Context;Lbwq;Ljava/lang/String;)Lri;
    .locals 2

    .prologue
    .line 359
    if-nez p2, :cond_0

    .line 360
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Empty URL to retrieve"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 362
    :cond_0
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v0, v1}, Lbvq;->a(Landroid/content/Context;Lbwq;Ljava/lang/String;I[B)Lri;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Lbwq;Ljava/lang/String;I[B)Lri;
    .locals 14

    .prologue
    .line 377
    sget-boolean v1, Lbvq;->a:Z

    if-eqz v1, :cond_0

    .line 378
    const-string v2, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "MmsSendReceiveManager.executeMmsRequest: requestUrl="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ",requestMethod="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ",requestData="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz p4, :cond_2

    move-object/from16 v0, p4

    array-length v1, v0

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    :cond_0
    if-nez p1, :cond_1

    .line 384
    const/4 v1, 0x0

    invoke-static {p0, v1}, Lbwq;->a(Landroid/content/Context;Ljava/lang/String;)Lbwq;

    move-result-object p1

    .line 386
    :cond_1
    invoke-virtual {p1}, Lbwq;->b()Z

    move-result v1

    if-nez v1, :cond_3

    .line 387
    new-instance v1, Lbvp;

    const/16 v2, 0x87

    const-string v3, "No available APN to use"

    invoke-direct {v1, v2, v3}, Lbvp;-><init>(ILjava/lang/String;)V

    throw v1

    .line 378
    :cond_2
    const/4 v1, -0x1

    goto :goto_0

    .line 391
    :cond_3
    const/4 v3, 0x0

    .line 392
    const-string v1, "connectivity"

    .line 393
    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Landroid/net/ConnectivityManager;

    .line 400
    invoke-virtual {p1}, Lbwq;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lbwr;

    .line 401
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "MmsSendReceiveManager: try APN "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    if-nez p2, :cond_e

    .line 404
    iget-object v2, v11, Lbwr;->b:Ljava/lang/String;

    .line 409
    :goto_2
    sget-boolean v1, Lbvq;->a:Z

    if-eqz v1, :cond_4

    const-string v1, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "resolveDestination url: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " with apn "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v11}, Lbwr;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, v11, Lbwr;->d:Ljava/lang/String;

    :goto_3
    invoke-static {v9}, Lbvq;->b(Landroid/net/ConnectivityManager;)I

    move-result v4

    const-string v5, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "ensureRouteToHost: addressTypes="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, v4}, Lbvq;->a(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    move-object v1, v3

    :goto_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Ljava/net/InetAddress;

    .line 410
    const-string v1, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MmsSendReceiveManager: try inet addr "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    :try_start_0
    invoke-static {v9, v10}, Lbvq;->a(Landroid/net/ConnectivityManager;Ljava/net/InetAddress;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 413
    new-instance v1, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cannot establish route to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 450
    :catch_0
    move-exception v1

    .line 452
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MmsSendReceiveManager: MMS HTTP request failed with exception for addr="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " apn="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 409
    :cond_5
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_3

    .line 421
    :cond_6
    :try_start_1
    invoke-virtual {v11}, Lbwr;->b()Z

    move-result v5

    iget-object v6, v11, Lbwr;->d:Ljava/lang/String;

    iget v7, v11, Lbwr;->f:I

    instance-of v8, v10, Ljava/net/Inet6Address;

    move-object v1, p0

    move-object/from16 v3, p4

    move/from16 v4, p3

    .line 415
    invoke-static/range {v1 .. v8}, Lsk;->a(Landroid/content/Context;Ljava/lang/String;[BIZLjava/lang/String;IZ)[B

    move-result-object v4

    .line 431
    invoke-virtual {p1, v11}, Lbwq;->a(Lbwr;)V

    .line 432
    if-eqz v4, :cond_a

    array-length v1, v4
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    if-lez v1, :cond_a

    .line 433
    const/4 v3, 0x0

    .line 435
    :try_start_2
    new-instance v1, Lrt;

    invoke-direct {v1, v4}, Lrt;-><init>([B)V

    invoke-virtual {v1}, Lrt;->a()Lri;
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v3

    .line 441
    :goto_5
    const/4 v1, 0x2

    move/from16 v0, p3

    if-ne v0, v1, :cond_7

    .line 442
    :try_start_3
    invoke-static {}, Lbvx;->i()Z

    move-result v1

    if-eqz v1, :cond_7

    if-eqz v3, :cond_7

    .line 443
    instance-of v1, v3, Lsa;

    if-nez v1, :cond_8

    const-string v1, "Babel"

    const-string v4, "dumpPdu: not RetrieveConf"

    invoke-static {v1, v4}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    :cond_7
    :goto_6
    return-object v3

    .line 436
    :catch_1
    move-exception v1

    .line 437
    const-string v5, "Babel"

    const-string v6, "MmsSendReceiveManager: Parsing retrieved PDU failure"

    invoke-static {v5, v6, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 443
    :cond_8
    move-object v0, v3

    check-cast v0, Lsa;

    move-object v1, v0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mmsdump-"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v6, Ljava/lang/String;

    invoke-virtual {v1}, Lsa;->j()[B

    move-result-object v1

    invoke-direct {v6, v1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v5, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    invoke-direct {v5, v6, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v5}, Ljava/io/File;->delete()Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_9
    :try_start_4
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    new-instance v5, Ljava/io/BufferedOutputStream;

    invoke-direct {v5, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :try_start_5
    invoke-virtual {v5, v4}, Ljava/io/BufferedOutputStream;->write([B)V

    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_6

    :catch_2
    move-exception v1

    :try_start_7
    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "dumpPdu: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    goto :goto_6

    :catchall_0
    move-exception v1

    :try_start_8
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V

    throw v1
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    .line 449
    :cond_a
    const/4 v3, 0x0

    goto :goto_6

    :cond_b
    move-object v3, v1

    .line 459
    goto/16 :goto_1

    .line 460
    :cond_c
    if-eqz v3, :cond_d

    .line 461
    new-instance v1, Lbvu;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "MMS HTTP request failed: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lbvu;-><init>(Ljava/lang/String;)V

    throw v1

    .line 463
    :cond_d
    new-instance v1, Lbvu;

    const-string v2, "MMS HTTP request failed"

    invoke-direct {v1, v2}, Lbvu;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_e
    move-object/from16 v2, p2

    goto/16 :goto_2
.end method

.method public static a(Landroid/content/Context;Lbwq;Lri;Ljava/lang/String;)Lri;
    .locals 2

    .prologue
    .line 347
    new-instance v0, Lrn;

    invoke-direct {v0, p0, p2}, Lrn;-><init>(Landroid/content/Context;Lri;)V

    invoke-virtual {v0}, Lrn;->a()[B

    move-result-object v0

    .line 348
    if-eqz v0, :cond_0

    array-length v1, v0

    if-gtz v1, :cond_1

    .line 349
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Empty or zero length PDU data"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 351
    :cond_1
    const/4 v1, 0x1

    invoke-static {p0, p1, p3, v1, v0}, Lbvq;->a(Landroid/content/Context;Lbwq;Ljava/lang/String;I[B)Lri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lbvs;)V
    .locals 0

    .prologue
    .line 334
    if-eqz p1, :cond_0

    .line 335
    invoke-virtual {p0, p1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 337
    :cond_0
    return-void
.end method

.method public static a(Landroid/net/ConnectivityManager;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 211
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 212
    const-string v2, "getMobileDataEnabled"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 213
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 215
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    .line 216
    invoke-virtual {v0, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 215
    const/4 v2, 0x0

    invoke-static {v0, v2}, Lf;->a(Ljava/lang/Boolean;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 219
    :goto_0
    return v0

    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0
.end method

.method private static a(Landroid/net/ConnectivityManager;Ljava/net/InetAddress;)Z
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 660
    const-string v0, "Babel"

    invoke-static {v0, v8}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 661
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "requestRouteToHostAddress "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "requestRouteToHostAddress"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, Ljava/net/InetAddress;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 667
    if-eqz v0, :cond_1

    .line 668
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Lbvq;->c:Ljava/lang/Integer;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    .line 669
    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 668
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 675
    :goto_0
    return v0

    .line 671
    :catch_0
    move-exception v0

    .line 672
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Call hidden requestRouteToHostAddress failed with "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    :cond_1
    invoke-virtual {p1}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v0

    aget-byte v1, v0, v8

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    aget-byte v2, v0, v6

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    aget-byte v2, v0, v7

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    aget-byte v0, v0, v5

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v1

    .line 675
    invoke-virtual {p0, v6, v0}, Landroid/net/ConnectivityManager;->requestRouteToHost(II)Z

    move-result v0

    goto :goto_0
.end method

.method private static b(Landroid/net/ConnectivityManager;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 707
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "getLinkProperties"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 708
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Lbvq;->c:Ljava/lang/Integer;

    aput-object v4, v2, v3

    invoke-virtual {v0, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 709
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MMS link: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getAddresses"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 711
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    .line 712
    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 713
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 714
    instance-of v3, v0, Ljava/net/Inet4Address;

    if-eqz v3, :cond_0

    .line 715
    or-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 716
    :cond_0
    instance-of v0, v0, Ljava/net/Inet6Address;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_2

    .line 717
    or-int/lit8 v0, v1, 0x2

    :goto_1
    move v1, v0

    .line 719
    goto :goto_0

    .line 723
    :catch_0
    move-exception v0

    const/4 v1, 0x3

    .line 725
    :cond_1
    return v1

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;)Lbwq;
    .locals 11

    .prologue
    .line 523
    const-string v0, "Babel"

    const-string v1, "MmsSendReceiveManager.acquireMmsNetwork"

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    sget-object v4, Lbvq;->b:Lbvt;

    monitor-enter v4

    .line 525
    :try_start_0
    sget-object v0, Lbvq;->b:Lbvt;

    const/4 v1, 0x0

    iput-object v1, v0, Lbvt;->e:Lrc;

    .line 526
    sget-object v0, Lbvq;->b:Lbvt;

    iget v1, v0, Lbvt;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lbvt;->a:I

    .line 527
    invoke-static {p0}, Lbvq;->e(Landroid/content/Context;)V

    .line 528
    sget-object v0, Lbvq;->b:Lbvt;

    iget-boolean v0, v0, Lbvt;->b:Z

    if-eqz v0, :cond_0

    .line 530
    sget-object v0, Lbvq;->b:Lbvt;

    iget-object v0, v0, Lbvt;->d:Lbwq;

    monitor-exit v4

    .line 565
    :goto_0
    return-object v0

    .line 533
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    .line 534
    const-string v0, "babel_mms_network_acquire_timeout_in_millis"

    const-wide/32 v1, 0x2bf20

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 537
    const-string v0, "babel_mms_network_acquire_wait_interval"

    const-wide/16 v7, 0x3a98

    invoke-static {v0, v7, v8}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v7

    move-wide v0, v2

    .line 541
    :goto_1
    const-wide/16 v9, 0x0

    cmp-long v9, v0, v9

    if-lez v9, :cond_5

    .line 543
    :try_start_1
    sget-object v9, Lbvq;->b:Lbvt;

    invoke-static {v0, v1, v7, v8}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    invoke-virtual {v9, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 548
    :goto_2
    :try_start_2
    sget-object v0, Lbvq;->b:Lbvt;

    iget-boolean v0, v0, Lbvt;->b:Z

    if-nez v0, :cond_3

    sget-object v0, Lbvq;->b:Lbvt;

    iget-object v0, v0, Lbvt;->e:Lrc;

    if-eqz v0, :cond_3

    .line 550
    sget-object v0, Lbvq;->b:Lbvt;

    iget-object v0, v0, Lbvt;->e:Lrc;

    instance-of v0, v0, Lbvu;

    if-eqz v0, :cond_1

    .line 551
    sget-object v0, Lbvq;->b:Lbvt;

    iget-object v0, v0, Lbvt;->e:Lrc;

    check-cast v0, Lbvu;

    throw v0

    .line 572
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 545
    :catch_0
    move-exception v0

    :try_start_3
    const-string v0, "Babel"

    const-string v1, "MmsSendReceiveManager: acquire network wait interrupted"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 553
    :cond_1
    sget-object v0, Lbvq;->b:Lbvt;

    iget-object v0, v0, Lbvt;->e:Lrc;

    instance-of v0, v0, Lbvp;

    if-eqz v0, :cond_2

    .line 554
    sget-object v0, Lbvq;->b:Lbvt;

    iget-object v0, v0, Lbvt;->e:Lrc;

    check-cast v0, Lbvp;

    throw v0

    .line 556
    :cond_2
    const-string v0, "Babel"

    const-string v1, "MmsSendReceiveManager: unknown exception"

    sget-object v2, Lbvq;->b:Lbvt;

    iget-object v2, v2, Lbvt;->e:Lrc;

    invoke-static {v0, v1, v2}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 558
    new-instance v0, Lbvu;

    sget-object v1, Lbvq;->b:Lbvt;

    iget-object v1, v1, Lbvt;->e:Lrc;

    invoke-direct {v0, v1}, Lbvu;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 563
    :cond_3
    invoke-static {p0}, Lbvq;->e(Landroid/content/Context;)V

    .line 564
    sget-object v0, Lbvq;->b:Lbvt;

    iget-boolean v0, v0, Lbvt;->b:Z

    if-eqz v0, :cond_4

    .line 565
    sget-object v0, Lbvq;->b:Lbvt;

    iget-object v0, v0, Lbvt;->d:Lbwq;

    monitor-exit v4

    goto :goto_0

    .line 568
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, v5

    sub-long v0, v2, v0

    goto :goto_1

    .line 571
    :cond_5
    new-instance v0, Lbvu;

    const-string v1, "Acquiring MMS network timed out"

    invoke-direct {v0, v1}, Lbvu;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public static c(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 579
    sget-object v1, Lbvq;->b:Lbvt;

    monitor-enter v1

    .line 580
    :try_start_0
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MmsSendReceiveManager.releaseMmsNetwork senders="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lbvq;->b:Lbvt;

    iget v3, v3, Lbvt;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    sget-object v0, Lbvq;->b:Lbvt;

    iget v2, v0, Lbvt;->a:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Lbvt;->a:I

    .line 583
    sget-object v0, Lbvq;->b:Lbvt;

    iget v0, v0, Lbvt;->a:I

    if-gtz v0, :cond_0

    .line 584
    sget-object v0, Lbvq;->b:Lbvt;

    invoke-virtual {v0}, Lbvt;->a()V

    .line 585
    const-string v0, "Babel"

    const-string v2, "MmsSendReceiveManager.endMmsConnectivity"

    invoke-static {v0, v2}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    const/4 v2, 0x0

    const-string v3, "enableMMS"

    invoke-virtual {v0, v2, v3}, Landroid/net/ConnectivityManager;->stopUsingNetworkFeature(ILjava/lang/String;)I

    .line 586
    sget-object v0, Lbvq;->b:Lbvt;

    const/4 v2, 0x0

    iput-boolean v2, v0, Lbvt;->b:Z

    .line 587
    sget-object v0, Lbvq;->b:Lbvt;

    const/4 v2, 0x0

    iput-object v2, v0, Lbvt;->d:Lbwq;

    .line 589
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic d(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 56
    invoke-static {p0}, Lbvq;->e(Landroid/content/Context;)V

    return-void
.end method

.method private static e(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 505
    const-string v0, "Babel"

    const-string v1, "MmsSendReceiveManager.extendMmsNetworkConnectivity"

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    const-string v0, "Babel"

    const-string v1, "MmsSendReceiveManager.beginMmsConnectivity"

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    const-string v1, "enableMMS"

    invoke-virtual {v0, v2, v1}, Landroid/net/ConnectivityManager;->startUsingNetworkFeature(ILjava/lang/String;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v1, Lbvu;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot establish MMS connectivity: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lbws;->a:[Ljava/lang/String;

    array-length v3, v3

    if-lt v0, v3, :cond_0

    sget-object v0, Lbws;->a:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :cond_0
    sget-object v3, Lbws;->a:[Ljava/lang/String;

    aget-object v0, v3, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lbvu;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    if-nez v0, :cond_1

    .line 508
    sget-object v0, Lbvq;->b:Lbvt;

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbvt;->a(ZLrc;)V

    .line 512
    :goto_0
    return-void

    .line 510
    :cond_1
    sget-object v0, Lbvq;->b:Lbvt;

    iput-boolean v2, v0, Lbvt;->b:Z

    goto :goto_0

    .line 506
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
