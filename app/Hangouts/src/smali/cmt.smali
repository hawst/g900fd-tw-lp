.class public abstract Lcmt;
.super Ljava/lang/Object;


# static fields
.field static volatile i:Z


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final b:Z

.field protected final c:I

.field protected final d:Landroid/os/Bundle;

.field protected final e:Landroid/os/Bundle;

.field protected final f:Z

.field protected final g:Ljava/lang/String;

.field protected final h:Lcmh;

.field private final j:Ljava/lang/Object;

.field private final k:Lcmx;

.field private l:Z

.field private m:Lcft;

.field private n:Lcom/google/android/gms/common/data/DataHolder;

.field private o:Lcom/google/android/gms/common/data/DataHolder;

.field private p:Z

.field private q:Landroid/database/Cursor;

.field private r:Ljava/lang/Exception;

.field private s:Z

.field private final t:Ljava/text/Collator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcmt;->i:Z

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcmx;ZILandroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcmt;->j:Ljava/lang/Object;

    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lcmt;->t:Ljava/text/Collator;

    iput-object p1, p0, Lcmt;->a:Landroid/content/Context;

    iput-object p2, p0, Lcmt;->k:Lcmx;

    iput-boolean p3, p0, Lcmt;->b:Z

    iput p4, p0, Lcmt;->c:I

    iput-object p5, p0, Lcmt;->d:Landroid/os/Bundle;

    iput-object p6, p0, Lcmt;->e:Landroid/os/Bundle;

    invoke-static {p7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcmt;->f:Z

    iget-boolean v0, p0, Lcmt;->f:Z

    if-eqz v0, :cond_1

    :goto_1
    iput-object p7, p0, Lcmt;->g:Ljava/lang/String;

    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v1, "aggregator"

    new-instance v0, Lcmh;

    invoke-direct {v0, v1}, Lcmh;-><init>(Ljava/lang/String;)V

    :goto_2
    iput-object v0, p0, Lcmt;->h:Lcmh;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 p7, 0x0

    goto :goto_1

    :cond_2
    sget-object v0, Lcmi;->a:Lcmi;

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Lcmx;ZILandroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Lcmt;
    .locals 8

    const/4 v7, 0x0

    const/4 v3, 0x0

    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcmy;

    move-object v1, p0

    move-object v2, p1

    move v4, v3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Lcmy;-><init>(Landroid/content/Context;Lcmx;ZILandroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Search aggregation doesn\'t support filtering by gaia-id"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Lcmz;

    move-object v1, p0

    move-object v2, p1

    move v4, v3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcmz;-><init>(Landroid/content/Context;Lcmx;ZILandroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcmt;)V
    .locals 4

    iget-object v0, p0, Lcmt;->m:Lcft;

    invoke-virtual {v0}, Lcft;->b()Z

    move-result v0

    invoke-static {v0}, Lg;->b(Z)V

    iget-object v0, p0, Lcmt;->h:Lcmh;

    const-string v1, "agg start"

    invoke-virtual {v0, v1}, Lcmh;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcmt;->q:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcmt;->q:Landroid/database/Cursor;

    :goto_0
    new-instance v1, Lcmw;

    iget-object v2, p0, Lcmt;->n:Lcom/google/android/gms/common/data/DataHolder;

    invoke-direct {v1, v2}, Lcmw;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    new-instance v2, Lcmw;

    iget-object v3, p0, Lcmt;->o:Lcom/google/android/gms/common/data/DataHolder;

    invoke-direct {v2, v3}, Lcmw;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {p0, v1, v2, v0}, Lcmt;->a(Lcmw;Lcmw;Landroid/database/Cursor;)Lcml;

    move-result-object v0

    iget-object v1, p0, Lcmt;->h:Lcmh;

    const-string v2, "agg finish"

    invoke-virtual {v1, v2}, Lcmh;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcmt;->h:Lcmh;

    invoke-virtual {v1}, Lcmh;->a()V

    iget-object v1, p0, Lcmt;->k:Lcmx;

    const/4 v2, 0x0

    invoke-interface {v1, v2, v0}, Lcmx;->a(ILcvx;)V

    return-void

    :cond_0
    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v1, Lcmq;->a:[Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected static a(Lcmw;Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcmw;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcmw;->a(I)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcmw;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "gaia_id"

    invoke-virtual {p0, v0}, Lcmw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcmw;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static a(Z)V
    .locals 0

    sput-boolean p0, Lcmt;->i:Z

    return-void
.end method

.method static synthetic b(Lcmt;)V
    .locals 0

    invoke-direct {p0}, Lcmt;->e()V

    return-void
.end method

.method private d()V
    .locals 3

    :try_start_0
    new-instance v0, Lcmv;

    invoke-direct {v0, p0}, Lcmv;-><init>(Lcmt;)V

    invoke-virtual {v0}, Lcmv;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "PeopleAggregator"

    const-string v2, "Unable to start thread"

    invoke-static {v1, v2, v0}, Lf;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcmt;->a(Landroid/database/Cursor;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private e()V
    .locals 3

    iget-object v1, p0, Lcmt;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcmt;->l:Z

    invoke-static {v0}, Lg;->b(Z)V

    iget-boolean v0, p0, Lcmt;->p:Z

    invoke-static {v0}, Lg;->b(Z)V

    iget-object v0, p0, Lcmt;->n:Lcom/google/android/gms/common/data/DataHolder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcmt;->n:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->i()V

    :cond_0
    iget-object v0, p0, Lcmt;->o:Lcom/google/android/gms/common/data/DataHolder;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcmt;->o:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->i()V

    :cond_1
    iget-object v0, p0, Lcmt;->q:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcmt;->q:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    iget-boolean v0, p0, Lcmt;->s:Z

    if-eqz v0, :cond_3

    monitor-exit v1

    :goto_0
    return-void

    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcmt;->s:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcmt;->k:Lcmx;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcmx;->a(ILcvx;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private f()V
    .locals 3

    iget-object v1, p0, Lcmt;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcmt;->l:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcmt;->p:Z

    if-nez v0, :cond_1

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return-void

    :cond_1
    monitor-exit v1

    iget-object v0, p0, Lcmt;->m:Lcft;

    invoke-virtual {v0}, Lcft;->b()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcmt;->e()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    :try_start_1
    new-instance v0, Lcmu;

    invoke-direct {v0, p0}, Lcmu;-><init>(Lcmt;)V

    invoke-virtual {v0}, Lcmu;->start()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "PeopleAggregator"

    const-string v2, "Unable to start thread"

    invoke-static {v1, v2, v0}, Lf;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lcmt;->e()V

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/database/Cursor;Lcmj;Lcle;Ljava/util/HashMap;)I
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lcmj;",
            "Lcle;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    const/4 v3, 0x0

    const-wide/16 v1, -0x1

    const/4 v0, -0x1

    const/4 v4, -0x1

    invoke-interface {p1, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    new-instance v6, Ljava/util/ArrayList;

    const/4 v4, 0x3

    invoke-direct {v6, v4}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v7, Ljava/util/ArrayList;

    const/4 v4, 0x6

    invoke-direct {v7, v4}, Ljava/util/ArrayList;-><init>(I)V

    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x0

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    cmp-long v8, v4, v1

    if-eqz v8, :cond_4

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    add-int/lit8 v3, v3, 0x1

    move-wide v9, v4

    move v4, v3

    move-wide v2, v9

    :goto_1
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v5, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p2, v0, v1}, Lcmj;->a(Ljava/lang/String;I)V

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p3, v5, v0}, Lcle;->a(Ljava/lang/Integer;Ljava/lang/String;)V

    :cond_1
    move v0, v1

    move-wide v9, v2

    move-wide v1, v9

    move v3, v4

    goto :goto_0

    :cond_2
    return v3

    :cond_3
    move v0, v1

    move-wide v9, v2

    move-wide v1, v9

    move v3, v4

    goto :goto_0

    :cond_4
    move v4, v3

    move-wide v9, v1

    move-wide v2, v9

    move v1, v0

    goto :goto_1
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcmt;->t:Ljava/text/Collator;

    invoke-virtual {v0, p1, p2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method protected abstract a(Lcmw;Lcmw;Landroid/database/Cursor;)Lcml;
.end method

.method protected a()Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    iget-object v0, p0, Lcmt;->o:Lcom/google/android/gms/common/data/DataHolder;

    return-object v0
.end method

.method a(Landroid/database/Cursor;Ljava/lang/Exception;)V
    .locals 2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcmt;->h:Lcmh;

    const-string v1, "contacts loaded"

    invoke-virtual {v0, v1}, Lcmh;->a(Ljava/lang/String;)V

    :goto_0
    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Contacts loaded.  exception="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez p1, :cond_2

    const/4 v0, -0x1

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v1, p0, Lcmt;->j:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcmt;->p:Z

    iput-object p1, p0, Lcmt;->q:Landroid/database/Cursor;

    iput-object p2, p0, Lcmt;->r:Ljava/lang/Exception;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcmt;->f()V

    return-void

    :cond_1
    iget-object v0, p0, Lcmt;->h:Lcmh;

    const-string v1, "contacts load failure"

    invoke-virtual {v0, v1}, Lcmh;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public a(Lcft;[Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcft;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcmt;->h:Lcmh;

    const-string v1, "people loaded"

    invoke-virtual {v0, v1}, Lcmh;->a(Ljava/lang/String;)V

    :goto_0
    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "People loaded.  status="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p2, :cond_0

    array-length v0, p2

    const/4 v2, 0x2

    if-lt v0, v2, :cond_0

    aget-object v0, p2, v3

    if-nez v0, :cond_4

    :cond_0
    const/4 v0, -0x1

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v1, p0, Lcmt;->j:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcmt;->l:Z

    iput-object p1, p0, Lcmt;->m:Lcft;

    iget-object v0, p0, Lcmt;->m:Lcft;

    invoke-virtual {v0}, Lcft;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    aget-object v0, p2, v0

    iput-object v0, p0, Lcmt;->n:Lcom/google/android/gms/common/data/DataHolder;

    const/4 v0, 0x1

    aget-object v0, p2, v0

    iput-object v0, p0, Lcmt;->o:Lcom/google/android/gms/common/data/DataHolder;

    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-boolean v0, p0, Lcmt;->f:Z

    if-nez v0, :cond_5

    invoke-direct {p0}, Lcmt;->f()V

    :goto_2
    return-void

    :cond_3
    iget-object v0, p0, Lcmt;->h:Lcmh;

    const-string v1, "people load failure"

    invoke-virtual {v0, v1}, Lcmh;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    aget-object v0, p2, v3

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->g()I

    move-result v0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_5
    iget-object v0, p0, Lcmt;->m:Lcft;

    invoke-virtual {v0}, Lcft;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcmt;->d()V

    goto :goto_2

    :cond_6
    iget-object v1, p0, Lcmt;->j:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcmt;->p:Z

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-direct {p0}, Lcmt;->e()V

    goto :goto_2

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public b()V
    .locals 1

    iget-boolean v0, p0, Lcmt;->f:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcmt;->d()V

    :cond_0
    return-void
.end method

.method protected b(Lcmw;Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcmw;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Lcmw;->a(I)V

    :goto_0
    invoke-virtual {p1}, Lcmw;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "value"

    invoke-virtual {p1, v0}, Lcmw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-virtual {p1, v1}, Lcmw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected abstract c()Landroid/database/Cursor;
.end method
