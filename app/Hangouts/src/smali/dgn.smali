.class public final Ldgn;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldgn;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:[Ldgj;

.field public d:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 834
    const/4 v0, 0x0

    new-array v0, v0, [Ldgn;

    sput-object v0, Ldgn;->a:[Ldgn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 835
    invoke-direct {p0}, Lepn;-><init>()V

    .line 848
    const/4 v0, 0x0

    iput-object v0, p0, Ldgn;->b:Ljava/lang/Integer;

    .line 851
    sget-object v0, Ldgj;->a:[Ldgj;

    iput-object v0, p0, Ldgn;->c:[Ldgj;

    .line 835
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 875
    const/4 v0, 0x1

    iget-object v1, p0, Ldgn;->b:Ljava/lang/Integer;

    .line 877
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 878
    iget-object v1, p0, Ldgn;->c:[Ldgj;

    if-eqz v1, :cond_1

    .line 879
    iget-object v2, p0, Ldgn;->c:[Ldgj;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 880
    if-eqz v4, :cond_0

    .line 881
    const/4 v5, 0x2

    .line 882
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 879
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 886
    :cond_1
    iget-object v1, p0, Ldgn;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 887
    const/4 v1, 0x3

    iget-object v2, p0, Ldgn;->d:Ljava/lang/Boolean;

    .line 888
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 890
    :cond_2
    iget-object v1, p0, Ldgn;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 891
    iput v0, p0, Ldgn;->cachedSize:I

    .line 892
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 831
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldgn;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldgn;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldgn;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/4 v2, 0x6

    if-ne v0, v2, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgn;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldgn;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldgn;->c:[Ldgj;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldgj;

    iget-object v3, p0, Ldgn;->c:[Ldgj;

    if-eqz v3, :cond_4

    iget-object v3, p0, Ldgn;->c:[Ldgj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Ldgn;->c:[Ldgj;

    :goto_2
    iget-object v2, p0, Ldgn;->c:[Ldgj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ldgn;->c:[Ldgj;

    new-instance v3, Ldgj;

    invoke-direct {v3}, Ldgj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgn;->c:[Ldgj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Ldgn;->c:[Ldgj;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Ldgn;->c:[Ldgj;

    new-instance v3, Ldgj;

    invoke-direct {v3}, Ldgj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldgn;->c:[Ldgj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldgn;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 858
    const/4 v0, 0x1

    iget-object v1, p0, Ldgn;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 859
    iget-object v0, p0, Ldgn;->c:[Ldgj;

    if-eqz v0, :cond_1

    .line 860
    iget-object v1, p0, Ldgn;->c:[Ldgj;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 861
    if-eqz v3, :cond_0

    .line 862
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 860
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 866
    :cond_1
    iget-object v0, p0, Ldgn;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 867
    const/4 v0, 0x3

    iget-object v1, p0, Ldgn;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 869
    :cond_2
    iget-object v0, p0, Ldgn;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 871
    return-void
.end method
