.class public final Lbqp;
.super Lbqr;
.source "PG"


# instance fields
.field final a:Ljava/lang/String;

.field final b:I


# direct methods
.method public constructor <init>(Lbjj;)V
    .locals 1

    .prologue
    .line 119
    invoke-direct {p0}, Lbqr;-><init>()V

    .line 120
    iget-object v0, p1, Lbjj;->b:Ljava/lang/String;

    iput-object v0, p0, Lbqp;->a:Ljava/lang/String;

    .line 121
    iget v0, p1, Lbjj;->c:I

    iput v0, p0, Lbqp;->b:I

    .line 122
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 124
    invoke-direct {p0}, Lbqr;-><init>()V

    .line 125
    iput-object p1, p0, Lbqp;->a:Ljava/lang/String;

    .line 126
    iput p2, p0, Lbqp;->b:I

    .line 127
    return-void
.end method

.method private b(Lyt;)V
    .locals 3

    .prologue
    .line 168
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateConversationNotificationLevelLocally conversationId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbqp;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " notificationLevel: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lbqp;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :cond_0
    invoke-virtual {p1}, Lyt;->a()V

    .line 177
    :try_start_0
    iget-object v0, p0, Lbqp;->a:Ljava/lang/String;

    iget v1, p0, Lbqp;->b:I

    invoke-virtual {p1, v0, v1}, Lyt;->a(Ljava/lang/String;I)V

    .line 178
    invoke-direct {p0, p1}, Lbqp;->c(Lyt;)V

    .line 179
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    invoke-virtual {p1}, Lyt;->c()V

    .line 182
    return-void

    .line 181
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lyt;->c()V

    throw v0
.end method

.method private c(Lyt;)V
    .locals 3

    .prologue
    .line 186
    iget-object v0, p0, Lbqp;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lyt;->ai(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 187
    iget v2, p0, Lbqp;->b:I

    invoke-virtual {p1, v0, v2}, Lyt;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 189
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lyt;)V
    .locals 0

    .prologue
    .line 158
    invoke-direct {p0, p1}, Lbqp;->b(Lyt;)V

    .line 159
    return-void
.end method

.method public a(Lyt;Lbnl;)V
    .locals 3

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lbqp;->b(Lyt;)V

    .line 133
    iget-object v0, p0, Lbqp;->a:Ljava/lang/String;

    invoke-static {v0}, Lyt;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    iget v0, p0, Lbqp;->b:I

    sparse-switch v0, :sswitch_data_0

    .line 153
    :goto_0
    return-void

    .line 136
    :sswitch_0
    iget-object v0, p0, Lbqp;->a:Ljava/lang/String;

    sget-wide v1, Lyp;->f:J

    invoke-virtual {p1, v0, v1, v2}, Lyt;->n(Ljava/lang/String;J)V

    .line 138
    iget-object v0, p0, Lbqp;->a:Ljava/lang/String;

    sget-wide v1, Lyp;->g:J

    invoke-virtual {p1, v0, v1, v2}, Lyt;->m(Ljava/lang/String;J)V

    goto :goto_0

    .line 143
    :sswitch_1
    iget-object v0, p0, Lbqp;->a:Ljava/lang/String;

    sget-wide v1, Lyp;->f:J

    invoke-virtual {p1, v0, v1, v2}, Lyt;->m(Ljava/lang/String;J)V

    .line 145
    iget-object v0, p0, Lbqp;->a:Ljava/lang/String;

    sget-wide v1, Lyp;->g:J

    invoke-virtual {p1, v0, v1, v2}, Lyt;->n(Ljava/lang/String;J)V

    goto :goto_0

    .line 150
    :cond_0
    new-instance v0, Lbes;

    iget-object v1, p0, Lbqp;->a:Ljava/lang/String;

    iget v2, p0, Lbqp;->b:I

    invoke-direct {v0, v1, v2}, Lbes;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p2, v0}, Lbnl;->a(Lbea;)V

    goto :goto_0

    .line 134
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_1
    .end sparse-switch
.end method
