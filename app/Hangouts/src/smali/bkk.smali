.class public final Lbkk;
.super Lxu;
.source "PG"


# static fields
.field private static final a:Z

.field private static b:Lbkk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lbys;->k:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbkk;->a:Z

    .line 16
    const/4 v0, 0x0

    sput-object v0, Lbkk;->b:Lbkk;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lxu;-><init>()V

    return-void
.end method

.method public static c()Lbkk;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lbkk;->b:Lbkk;

    if-nez v0, :cond_0

    .line 22
    new-instance v0, Lbkk;

    invoke-direct {v0}, Lbkk;-><init>()V

    sput-object v0, Lbkk;->b:Lbkk;

    .line 24
    :cond_0
    sget-object v0, Lbkk;->b:Lbkk;

    return-object v0
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 29
    sget-boolean v0, Lbkk;->a:Z

    if-eqz v0, :cond_0

    .line 30
    const-string v0, "Babel"

    const-string v1, "The first activity of Hangout got started."

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    :cond_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/BackgroundWarmSyncService;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 33
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/BackgroundWarmSyncService;->a(Landroid/content/Context;Z)V

    .line 35
    :cond_1
    return-void
.end method

.method protected b()V
    .locals 2

    .prologue
    .line 39
    sget-boolean v0, Lbkk;->a:Z

    if-eqz v0, :cond_0

    .line 40
    const-string v0, "Babel"

    const-string v1, "The last activity of Hangout got stopped."

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    :cond_0
    const-string v0, "babel_background_polling"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/BackgroundWarmSyncService;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 46
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/BackgroundWarmSyncService;->a(Landroid/content/Context;Z)V

    .line 48
    :cond_1
    return-void
.end method
