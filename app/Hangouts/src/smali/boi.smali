.class public final Lboi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lyj;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:I

.field final synthetic h:I

.field final synthetic i:Ljava/lang/String;

.field final synthetic j:Ljava/lang/String;

.field final synthetic k:Z

.field final synthetic l:Lcom/google/android/gms/maps/model/MarkerOptions;

.field final synthetic m:Lcom/google/android/gms/maps/model/CameraPosition;

.field final synthetic n:J


# direct methods
.method public constructor <init>(ILyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;ZLcom/google/android/gms/maps/model/MarkerOptions;Lcom/google/android/gms/maps/model/CameraPosition;J)V
    .locals 0

    .prologue
    .line 1873
    iput p1, p0, Lboi;->a:I

    iput-object p2, p0, Lboi;->b:Lyj;

    iput-object p3, p0, Lboi;->c:Ljava/lang/String;

    iput-object p4, p0, Lboi;->d:Ljava/lang/String;

    iput-object p5, p0, Lboi;->e:Ljava/lang/String;

    iput-object p6, p0, Lboi;->f:Ljava/lang/String;

    iput p7, p0, Lboi;->g:I

    iput p8, p0, Lboi;->h:I

    iput-object p9, p0, Lboi;->i:Ljava/lang/String;

    iput-object p10, p0, Lboi;->j:Ljava/lang/String;

    iput-boolean p11, p0, Lboi;->k:Z

    iput-object p12, p0, Lboi;->l:Lcom/google/android/gms/maps/model/MarkerOptions;

    iput-object p13, p0, Lboi;->m:Lcom/google/android/gms/maps/model/CameraPosition;

    iput-wide p14, p0, Lboi;->n:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 17

    .prologue
    .line 1876
    const/4 v1, 0x1

    move/from16 v16, v1

    :goto_0
    move-object/from16 v0, p0

    iget v1, v0, Lboi;->a:I

    move/from16 v0, v16

    if-gt v0, v1, :cond_0

    .line 1878
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lbqz;

    .line 1877
    invoke-static {v1, v2}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbqz;

    move-object/from16 v0, p0

    iget-object v2, v0, Lboi;->b:Lyj;

    move-object/from16 v0, p0

    iget-object v3, v0, Lboi;->c:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lboi;->d:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "--"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lboi;->e:Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lboi;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lboi;->g:I

    move-object/from16 v0, p0

    iget v9, v0, Lboi;->h:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lboi;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lboi;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lboi;->k:Z

    move-object/from16 v0, p0

    iget-object v13, v0, Lboi;->l:Lcom/google/android/gms/maps/model/MarkerOptions;

    move-object/from16 v0, p0

    iget-object v14, v0, Lboi;->m:Lcom/google/android/gms/maps/model/CameraPosition;

    const/4 v15, 0x0

    .line 1879
    invoke-interface/range {v1 .. v15}, Lbqz;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;Ljava/lang/String;ZLcom/google/android/gms/maps/model/MarkerOptions;Lcom/google/android/gms/maps/model/CameraPosition;I)V

    .line 1884
    :try_start_0
    move-object/from16 v0, p0

    iget-wide v1, v0, Lboi;->n:J

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1876
    :goto_1
    add-int/lit8 v1, v16, 0x1

    move/from16 v16, v1

    goto :goto_0

    .line 1888
    :cond_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_1
.end method
