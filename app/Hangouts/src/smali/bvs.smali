.class public final Lbvs;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    .line 104
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    const-string v0, "connectivity"

    .line 108
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 109
    invoke-static {p2}, Lbvq;->a(Landroid/content/Intent;)I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 115
    invoke-static {}, Lbvq;->a()Lbvt;

    move-result-object v1

    monitor-enter v1

    .line 116
    :try_start_0
    invoke-static {}, Lbvq;->a()Lbvt;

    move-result-object v2

    iget v2, v2, Lbvt;->a:I

    if-gtz v2, :cond_2

    .line 117
    const-string v0, "Babel"

    const-string v2, "MmsSendReceiveManager: no sender, skip checking"

    invoke-static {v0, v2}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    monitor-exit v1

    .line 122
    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 124
    if-eqz v1, :cond_0

    .line 125
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MmsSendReceiveManager: MMS connectivity change "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const-string v2, "2GVoiceCallEnded"

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getReason()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 132
    :try_start_1
    invoke-static {}, Lbvq;->a()Lbvt;

    move-result-object v1

    monitor-enter v1
    :try_end_1
    .catch Lrc; {:try_start_1 .. :try_end_1} :catch_0

    .line 133
    :try_start_2
    invoke-static {p1}, Lbvq;->d(Landroid/content/Context;)V

    .line 134
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1

    throw v0
    :try_end_3
    .catch Lrc; {:try_start_3 .. :try_end_3} :catch_0

    .line 138
    :catch_0
    move-exception v0

    .line 136
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MmsSendReceiveManager.ConnectivityBroadcastReceiver: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 141
    :cond_3
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v2

    .line 142
    sget-object v3, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v2, v3, :cond_4

    .line 144
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getExtraInfo()Ljava/lang/String;

    move-result-object v0

    .line 143
    invoke-static {p1, v0}, Lbwq;->a(Landroid/content/Context;Ljava/lang/String;)Lbwq;

    move-result-object v0

    .line 149
    invoke-virtual {v0}, Lbwq;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    invoke-static {}, Lbvq;->a()Lbvt;

    move-result-object v1

    monitor-enter v1

    .line 151
    :try_start_4
    invoke-static {}, Lbvq;->a()Lbvt;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lbvt;->a(ZLrc;)V

    .line 152
    invoke-static {}, Lbvq;->a()Lbvt;

    move-result-object v2

    iput-object v0, v2, Lbvt;->d:Lbwq;

    .line 153
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0

    .line 155
    :cond_4
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v2

    sget-object v3, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v2, v3, :cond_5

    .line 156
    invoke-static {v0}, Lbvq;->a(Landroid/net/ConnectivityManager;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 159
    invoke-static {}, Lbvq;->a()Lbvt;

    move-result-object v1

    monitor-enter v1

    .line 160
    :try_start_5
    invoke-static {}, Lbvq;->a()Lbvt;

    const/4 v0, 0x0

    new-instance v2, Lbvp;

    const/16 v3, 0x7b

    const-string v4, "Mobile data is disabled and can not connect MMS"

    invoke-direct {v2, v3, v4}, Lbvp;-><init>(ILjava/lang/String;)V

    invoke-static {v0, v2}, Lbvt;->a(ZLrc;)V

    .line 166
    invoke-static {}, Lbvq;->a()Lbvt;

    move-result-object v0

    const/4 v2, 0x0

    iput-object v2, v0, Lbvt;->d:Lbwq;

    .line 167
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    goto/16 :goto_0

    :catchall_3
    move-exception v0

    monitor-exit v1

    throw v0

    .line 170
    :cond_5
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    :try_start_6
    invoke-static {}, Lbvq;->a()Lbvt;

    move-result-object v1

    monitor-enter v1
    :try_end_6
    .catch Lrc; {:try_start_6 .. :try_end_6} :catch_1

    .line 173
    :try_start_7
    invoke-static {p1}, Lbvq;->d(Landroid/content/Context;)V

    .line 174
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    goto/16 :goto_0

    :catchall_4
    move-exception v0

    :try_start_8
    monitor-exit v1

    throw v0
    :try_end_8
    .catch Lrc; {:try_start_8 .. :try_end_8} :catch_1

    .line 178
    :catch_1
    move-exception v0

    .line 176
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MmsSendReceiveManager.ConnectivityBroadcastReceiver: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
