.class public final Ladj;
.super Lpi;
.source "PG"


# instance fields
.field private final a:Lyj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lyj;)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lpi;-><init>(Landroid/content/Context;)V

    .line 148
    iput-object p2, p0, Ladj;->a:Lyj;

    .line 149
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;II)Landroid/view/View;
    .locals 2

    .prologue
    .line 268
    new-instance v0, Lcaw;

    iget-object v1, p0, Ladj;->a:Lyj;

    invoke-direct {v0, p1, v1}, Lcaw;-><init>(Landroid/content/Context;Lyj;)V

    return-object v0
.end method

.method public a(Ladb;)V
    .locals 3

    .prologue
    .line 272
    invoke-virtual {p0}, Ladj;->d()I

    move-result v2

    .line 273
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 274
    invoke-virtual {p0, v1}, Ladj;->c(I)Landroid/database/Cursor;

    move-result-object v0

    .line 275
    if-eqz v0, :cond_0

    .line 276
    check-cast v0, Ladk;

    invoke-interface {v0, p1}, Ladk;->a(Ladb;)V

    .line 273
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 279
    :cond_1
    return-void
.end method

.method public a(Landroid/view/View;ILandroid/database/Cursor;I)V
    .locals 10

    .prologue
    move-object v0, p1

    .line 161
    check-cast v0, Lcaw;

    .line 162
    check-cast p3, Ladk;

    .line 163
    if-nez p3, :cond_0

    .line 235
    :goto_0
    return-void

    .line 167
    :cond_0
    invoke-interface {p3}, Ladk;->a()Lcvw;

    move-result-object v7

    .line 168
    invoke-interface {p3}, Ladk;->c()Ladm;

    move-result-object v8

    .line 169
    invoke-interface {p3}, Ladk;->e()I

    move-result v5

    .line 170
    invoke-interface {p3}, Ladk;->d()I

    move-result v6

    .line 172
    invoke-virtual {p0, p2}, Ladj;->a(I)Lpj;

    move-result-object v1

    check-cast v1, Ladl;

    .line 173
    invoke-virtual {v1}, Ladl;->a()Ljava/lang/CharSequence;

    move-result-object v1

    .line 174
    const/16 v9, 0xf

    .line 175
    const/4 v2, 0x1

    if-ne v6, v2, :cond_1

    .line 176
    const/4 v9, 0x7

    .line 180
    :cond_1
    if-nez v1, :cond_4

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, v1}, Lcaw;->a(Ljava/lang/String;)V

    .line 181
    if-eqz v7, :cond_7

    .line 182
    invoke-interface {p3}, Ladk;->f()Z

    move-result v1

    if-nez v1, :cond_2

    .line 185
    and-int/lit8 v9, v9, -0x4

    .line 190
    :cond_2
    const/4 v2, 0x0

    if-nez v8, :cond_5

    const/4 v1, 0x0

    move-object v4, v1

    :goto_2
    invoke-interface {v7}, Lcvw;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v7}, Lcvw;->f()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    invoke-interface {v7}, Lcvw;->h()Ljava/lang/String;

    move-result-object v3

    move-object v1, v2

    :goto_3
    invoke-interface {v7}, Lcvw;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-static {v7}, Laea;->c(Lcvw;)Ljava/lang/String;

    move-result-object v2

    :cond_3
    invoke-static {v7, v4}, Laea;->a(Lcvw;Ljava/lang/String;)Laea;

    move-result-object v7

    const/4 v5, 0x0

    move-object v4, v3

    invoke-virtual/range {v0 .. v9}, Lcaw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILaea;Ladm;I)V

    goto :goto_0

    .line 180
    :cond_4
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 190
    :cond_5
    invoke-virtual {v8}, Ladm;->b()Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    goto :goto_2

    :cond_6
    invoke-interface {v7}, Lcvw;->b()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Lbrz;->a(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v3

    move-object v1, v2

    goto :goto_3

    .line 195
    :cond_7
    invoke-interface {p3}, Ladk;->b()Laea;

    move-result-object v7

    .line 197
    packed-switch v5, :pswitch_data_0

    .line 231
    const-string v0, "Babel"

    const-string v1, "CallContactsAdapter is given an unsupported contact type!"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 199
    :pswitch_0
    const/4 v1, 0x1

    if-ne v6, v1, :cond_8

    .line 203
    const-string v1, "Babel"

    const-string v2, "Local contacts not returning AggregatedPerson!"

    invoke-static {v1, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    :cond_8
    const/4 v1, 0x0

    .line 210
    const/4 v2, 0x2

    if-ne v6, v2, :cond_9

    .line 212
    const/4 v2, 0x0

    invoke-interface {p3, v2}, Ladk;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 223
    :goto_4
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v9}, Lcaw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILaea;Ladm;I)V

    goto/16 :goto_0

    .line 217
    :cond_9
    invoke-virtual {v7}, Laea;->j()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laeh;

    .line 218
    invoke-virtual {v1}, Laeh;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbzd;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 219
    const/4 v1, 0x2

    invoke-interface {p3, v1}, Ladk;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 221
    or-int/lit8 v9, v9, 0x10

    goto :goto_4

    .line 197
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public g()Lyj;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Ladj;->a:Lyj;

    return-object v0
.end method
