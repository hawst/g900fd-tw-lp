.class public final Lagc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/LayoutTransition$TransitionListener;


# instance fields
.field final synthetic a:Landroid/view/ViewGroup;

.field final synthetic b:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 5789
    iput-object p1, p0, Lagc;->b:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iput-object p2, p0, Lagc;->a:Landroid/view/ViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public endTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 5793
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 5794
    iget-object v0, p0, Lagc;->a:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 5808
    :goto_0
    return-void

    .line 5801
    :cond_0
    iget-object v0, p0, Lagc;->b:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->q(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lagd;

    invoke-direct {v1, p0}, Lagd;-><init>(Lagc;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public startTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 5813
    return-void
.end method
