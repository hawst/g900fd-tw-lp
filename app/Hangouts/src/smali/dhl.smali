.class public final Ldhl;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldhl;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/Integer;

.field public k:Ldhm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3045
    const/4 v0, 0x0

    new-array v0, v0, [Ldhl;

    sput-object v0, Ldhl;->a:[Ldhl;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3046
    invoke-direct {p0}, Lepn;-><init>()V

    .line 3067
    const/4 v0, 0x0

    iput-object v0, p0, Ldhl;->k:Ldhm;

    .line 3046
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 3106
    const/4 v0, 0x1

    iget-object v1, p0, Ldhl;->c:Ljava/lang/String;

    .line 3108
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3109
    iget-object v1, p0, Ldhl;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3110
    const/4 v1, 0x2

    iget-object v2, p0, Ldhl;->d:Ljava/lang/String;

    .line 3111
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3113
    :cond_0
    iget-object v1, p0, Ldhl;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3114
    const/4 v1, 0x3

    iget-object v2, p0, Ldhl;->e:Ljava/lang/String;

    .line 3115
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3117
    :cond_1
    iget-object v1, p0, Ldhl;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 3118
    const/4 v1, 0x4

    iget-object v2, p0, Ldhl;->f:Ljava/lang/String;

    .line 3119
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3121
    :cond_2
    iget-object v1, p0, Ldhl;->g:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 3122
    const/4 v1, 0x5

    iget-object v2, p0, Ldhl;->g:Ljava/lang/String;

    .line 3123
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3125
    :cond_3
    iget-object v1, p0, Ldhl;->i:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 3126
    const/4 v1, 0x6

    iget-object v2, p0, Ldhl;->i:Ljava/lang/String;

    .line 3127
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3129
    :cond_4
    iget-object v1, p0, Ldhl;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 3130
    const/4 v1, 0x7

    iget-object v2, p0, Ldhl;->j:Ljava/lang/Integer;

    .line 3131
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3133
    :cond_5
    iget-object v1, p0, Ldhl;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 3134
    const/16 v1, 0x8

    iget-object v2, p0, Ldhl;->h:Ljava/lang/String;

    .line 3135
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3137
    :cond_6
    iget-object v1, p0, Ldhl;->b:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 3138
    const/16 v1, 0x9

    iget-object v2, p0, Ldhl;->b:Ljava/lang/String;

    .line 3139
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3141
    :cond_7
    iget-object v1, p0, Ldhl;->k:Ldhm;

    if-eqz v1, :cond_8

    .line 3142
    const/16 v1, 0xa

    iget-object v2, p0, Ldhl;->k:Ldhm;

    .line 3143
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3145
    :cond_8
    iget-object v1, p0, Ldhl;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3146
    iput v0, p0, Ldhl;->cachedSize:I

    .line 3147
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 3042
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldhl;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldhl;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldhl;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldhl;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldhl;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldhl;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldhl;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldhl;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldhl;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldhl;->j:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldhl;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldhl;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_a
    iget-object v0, p0, Ldhl;->k:Ldhm;

    if-nez v0, :cond_2

    new-instance v0, Ldhm;

    invoke-direct {v0}, Ldhm;-><init>()V

    iput-object v0, p0, Ldhl;->k:Ldhm;

    :cond_2
    iget-object v0, p0, Ldhl;->k:Ldhm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 3072
    const/4 v0, 0x1

    iget-object v1, p0, Ldhl;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3073
    iget-object v0, p0, Ldhl;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3074
    const/4 v0, 0x2

    iget-object v1, p0, Ldhl;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3076
    :cond_0
    iget-object v0, p0, Ldhl;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3077
    const/4 v0, 0x3

    iget-object v1, p0, Ldhl;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3079
    :cond_1
    iget-object v0, p0, Ldhl;->f:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3080
    const/4 v0, 0x4

    iget-object v1, p0, Ldhl;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3082
    :cond_2
    iget-object v0, p0, Ldhl;->g:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 3083
    const/4 v0, 0x5

    iget-object v1, p0, Ldhl;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3085
    :cond_3
    iget-object v0, p0, Ldhl;->i:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 3086
    const/4 v0, 0x6

    iget-object v1, p0, Ldhl;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3088
    :cond_4
    iget-object v0, p0, Ldhl;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 3089
    const/4 v0, 0x7

    iget-object v1, p0, Ldhl;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 3091
    :cond_5
    iget-object v0, p0, Ldhl;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 3092
    const/16 v0, 0x8

    iget-object v1, p0, Ldhl;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3094
    :cond_6
    iget-object v0, p0, Ldhl;->b:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 3095
    const/16 v0, 0x9

    iget-object v1, p0, Ldhl;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3097
    :cond_7
    iget-object v0, p0, Ldhl;->k:Ldhm;

    if-eqz v0, :cond_8

    .line 3098
    const/16 v0, 0xa

    iget-object v1, p0, Ldhl;->k:Ldhm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 3100
    :cond_8
    iget-object v0, p0, Ldhl;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 3102
    return-void
.end method
