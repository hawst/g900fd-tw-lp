.class public final Ldqw;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldqw;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Leir;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2056
    const/4 v0, 0x0

    new-array v0, v0, [Ldqw;

    sput-object v0, Ldqw;->a:[Ldqw;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2057
    invoke-direct {p0}, Lepn;-><init>()V

    .line 2060
    iput-object v0, p0, Ldqw;->b:Ljava/lang/Integer;

    .line 2063
    iput-object v0, p0, Ldqw;->c:Leir;

    .line 2057
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2080
    const/4 v0, 0x0

    .line 2081
    iget-object v1, p0, Ldqw;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 2082
    const/4 v0, 0x1

    iget-object v1, p0, Ldqw;->b:Ljava/lang/Integer;

    .line 2083
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2085
    :cond_0
    iget-object v1, p0, Ldqw;->c:Leir;

    if-eqz v1, :cond_1

    .line 2086
    const/4 v1, 0x2

    iget-object v2, p0, Ldqw;->c:Leir;

    .line 2087
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2089
    :cond_1
    iget-object v1, p0, Ldqw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2090
    iput v0, p0, Ldqw;->cachedSize:I

    .line 2091
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 2053
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldqw;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldqw;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldqw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqw;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqw;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldqw;->c:Leir;

    if-nez v0, :cond_4

    new-instance v0, Leir;

    invoke-direct {v0}, Leir;-><init>()V

    iput-object v0, p0, Ldqw;->c:Leir;

    :cond_4
    iget-object v0, p0, Ldqw;->c:Leir;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 2068
    iget-object v0, p0, Ldqw;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2069
    const/4 v0, 0x1

    iget-object v1, p0, Ldqw;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 2071
    :cond_0
    iget-object v0, p0, Ldqw;->c:Leir;

    if-eqz v0, :cond_1

    .line 2072
    const/4 v0, 0x2

    iget-object v1, p0, Ldqw;->c:Leir;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 2074
    :cond_1
    iget-object v0, p0, Ldqw;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 2076
    return-void
.end method
