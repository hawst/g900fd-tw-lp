.class public final Lbrf;
.super Lbrr;
.source "PG"


# static fields
.field private static final b:Z

.field private static final e:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lbrf;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Lyj;

.field private d:Lyt;

.field private final f:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lbys;->l:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbrf;->b:Z

    .line 104
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lbrf;->e:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method private constructor <init>(Lyj;)V
    .locals 1

    .prologue
    .line 147
    invoke-direct {p0}, Lbrr;-><init>()V

    .line 153
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbrf;->f:Ljava/lang/Object;

    .line 148
    iput-object p1, p0, Lbrf;->c:Lyj;

    .line 149
    const/4 v0, 0x0

    iput-object v0, p0, Lbrf;->d:Lyt;

    .line 150
    return-void
.end method

.method public static a(Lyj;)Lbrf;
    .locals 4

    .prologue
    .line 112
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    .line 113
    sget-object v0, Lbrf;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrf;

    .line 115
    if-nez v0, :cond_1

    .line 116
    sget-boolean v0, Lbrf;->b:Z

    if-eqz v0, :cond_0

    .line 117
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Adding contact loader for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    :cond_0
    new-instance v0, Lbrf;

    invoke-direct {v0, p0}, Lbrf;-><init>(Lyj;)V

    .line 125
    sget-object v2, Lbrf;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v0, Lbrf;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrf;

    .line 129
    :cond_1
    return-object v0
.end method

.method public static a(Lbdk;Lyj;Lbri;)Lyb;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 443
    invoke-virtual {p0}, Lbdk;->b()Lbcn;

    move-result-object v2

    .line 444
    invoke-virtual {v2}, Lbcn;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 445
    const-string v1, "Babel"

    const-string v2, "lookup spec for participantId invalid"

    new-instance v3, Ljava/lang/Throwable;

    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v2, v3}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 452
    :cond_0
    :goto_0
    return-object v0

    .line 448
    :cond_1
    new-instance v1, Lyb;

    invoke-direct {v1, v2, p2}, Lyb;-><init>(Lbcn;Lbri;)V

    .line 449
    invoke-static {p1}, Lbrf;->a(Lyj;)Lbrf;

    move-result-object v2

    invoke-virtual {v2, v1}, Lbrf;->a(Lbrv;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 452
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;ZLyj;Lbrj;)Lyb;
    .locals 2

    .prologue
    .line 465
    invoke-static {p3}, Lcwz;->b(Ljava/lang/Object;)V

    .line 466
    new-instance v0, Laan;

    invoke-direct {v0, p0, p1, p3}, Laan;-><init>(Ljava/lang/String;ZLbrj;)V

    .line 467
    invoke-static {p2}, Lbrf;->a(Lyj;)Lbrf;

    move-result-object v1

    invoke-virtual {v1, v0}, Lbrf;->a(Lbrv;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 468
    const/4 v0, 0x0

    .line 470
    :cond_0
    return-object v0
.end method

.method private a(Lbcn;Lbdh;)V
    .locals 3

    .prologue
    .line 281
    sget-boolean v0, Lbrf;->b:Z

    if-eqz v0, :cond_0

    .line 282
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Contact info ready: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    :cond_0
    invoke-virtual {p1}, Lbcn;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbrf;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 295
    :goto_0
    return-void

    .line 289
    :cond_1
    new-instance v0, Lbrg;

    invoke-direct {v0, p0, p1, p2}, Lbrg;-><init>(Lbrf;Lbcn;Lbdh;)V

    invoke-virtual {p0, v0}, Lbrf;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static a(Lbdk;Lyj;)V
    .locals 4

    .prologue
    .line 480
    invoke-static {p0}, Lbcn;->a(Lbdk;)Lbcn;

    move-result-object v0

    .line 481
    invoke-virtual {v0}, Lbcn;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 482
    const-string v0, "Babel"

    const-string v1, "participantId not valid for DB lookup"

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1, v2}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 492
    :goto_0
    return-void

    .line 485
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 486
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 488
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lbcn;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "#"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 489
    new-instance v2, Lya;

    invoke-direct {v2, v1, v0, p1}, Lya;-><init>(Ljava/util/List;Ljava/lang/String;Lyj;)V

    .line 491
    invoke-static {p1}, Lbrf;->a(Lyj;)Lbrf;

    move-result-object v0

    invoke-virtual {v0, v2}, Lbrf;->c(Lbrv;)V

    goto :goto_0
.end method

.method static synthetic a(Lbrf;Lbcn;)V
    .locals 3

    .prologue
    .line 54
    invoke-virtual {p1}, Lbcn;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbrf;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrv;

    check-cast v0, Lyb;

    invoke-virtual {v0}, Lyb;->a()Lbri;

    move-result-object v2

    invoke-interface {v2, v0}, Lbri;->a(Lyb;)V

    goto :goto_1

    :cond_2
    sget-boolean v0, Lbrf;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "Babel"

    invoke-virtual {p0}, Lbrf;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lbrf;Lbcn;Lbdh;)V
    .locals 8

    .prologue
    .line 54
    invoke-virtual {p1}, Lbcn;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbrf;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p2, Lbdh;->e:Ljava/lang/String;

    iget-object v2, p2, Lbdh;->h:Ljava/lang/String;

    iget-object v6, p2, Lbdh;->f:Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrv;

    instance-of v3, v0, Laan;

    if-eqz v3, :cond_3

    move-object v4, v0

    check-cast v4, Laan;

    invoke-virtual {v4}, Laan;->a()Lbri;

    move-result-object v0

    check-cast v0, Lbrj;

    iget v3, p2, Lbdh;->t:I

    iget-object v5, p0, Lbrf;->c:Lyj;

    invoke-interface/range {v0 .. v5}, Lbrj;->a(Ljava/lang/String;Ljava/lang/String;ILaan;Lyj;)V

    goto :goto_1

    :cond_3
    instance-of v3, v0, Lyb;

    if-eqz v3, :cond_2

    move-object v3, v0

    check-cast v3, Lyb;

    invoke-virtual {v3}, Lyb;->a()Lbri;

    move-result-object v0

    iget-object v5, p0, Lbrf;->c:Lyj;

    move-object v4, v6

    invoke-interface/range {v0 .. v5}, Lbri;->a(Ljava/lang/String;Ljava/lang/String;Lyb;Ljava/lang/String;Lyj;)V

    goto :goto_1

    :cond_4
    sget-boolean v0, Lbrf;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "Babel"

    invoke-virtual {p0}, Lbrf;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 136
    sget-boolean v0, Lbrf;->b:Z

    if-eqz v0, :cond_0

    .line 137
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Removing contact loader for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_0
    sget-object v0, Lbrf;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    return-void
.end method

.method public static a(Ljava/lang/String;Lyj;)V
    .locals 2

    .prologue
    .line 501
    new-instance v0, Lyg;

    invoke-direct {v0, p0, p1}, Lyg;-><init>(Ljava/lang/String;Lyj;)V

    .line 503
    invoke-static {p1}, Lbrf;->a(Lyj;)Lbrf;

    move-result-object v1

    invoke-virtual {v1, v0}, Lbrf;->c(Lbrv;)V

    .line 504
    return-void
.end method

.method public static a(Lyj;Lbgs;)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 221
    invoke-static {p0}, Lbrf;->a(Lyj;)Lbrf;

    move-result-object v4

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p1}, Lbgs;->g()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v4, v3}, Lbrf;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    sget-boolean v6, Lbrf;->b:Z

    if-eqz v6, :cond_0

    const-string v6, "Babel"

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "Clearing %d request from queue: %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v9, v1

    aput-object v3, v9, v2

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-nez v3, :cond_4

    invoke-virtual {p1}, Lbgs;->i()Z

    move-result v0

    if-nez v0, :cond_4

    :goto_1
    invoke-direct {v4}, Lbrf;->c()V

    iget-object v0, v4, Lbrf;->d:Lyt;

    invoke-virtual {v0}, Lyt;->a()V

    :try_start_0
    invoke-virtual {p1}, Lbgs;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzo;

    const/4 v3, 0x0

    iget-object v1, v0, Lbzo;->b:Ljava/io/Serializable;

    if-eqz v1, :cond_8

    iget-object v1, v0, Lbzo;->b:Ljava/io/Serializable;

    check-cast v1, [Lbdh;

    array-length v1, v1

    if-lez v1, :cond_8

    iget-object v1, v0, Lbzo;->b:Ljava/io/Serializable;

    check-cast v1, [Lbdh;

    const/4 v3, 0x0

    aget-object v1, v1, v3

    :goto_3
    if-eqz v1, :cond_5

    iget-object v3, v4, Lbrf;->d:Lyt;

    const/4 v7, 0x1

    invoke-virtual {v3, v1, v7}, Lyt;->a(Lbdh;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v1, Lbdh;->b:Lbdk;

    invoke-virtual {v5, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_2
    if-eqz v2, :cond_1

    iget-object v0, v0, Lbzo;->a:Ljava/io/Serializable;

    check-cast v0, Lbcn;

    invoke-direct {v4, v0, v1}, Lbrf;->a(Lbcn;Lbdh;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    iget-object v1, v4, Lbrf;->d:Lyt;

    invoke-virtual {v1}, Lyt;->c()V

    throw v0

    :cond_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    :cond_4
    move v2, v1

    goto :goto_1

    :cond_5
    if-eqz v2, :cond_1

    :try_start_1
    iget-object v0, v0, Lbzo;->a:Ljava/io/Serializable;

    check-cast v0, Lbcn;

    invoke-virtual {v4, v0}, Lbrf;->a(Lbcn;)V

    goto :goto_2

    :cond_6
    iget-object v0, v4, Lbrf;->d:Lyt;

    invoke-virtual {v0}, Lyt;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, v4, Lbrf;->d:Lyt;

    invoke-virtual {v0}, Lyt;->c()V

    invoke-virtual {v5}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, v4, Lbrf;->d:Lyt;

    invoke-virtual {v0, v5}, Lyt;->a(Ljava/util/HashSet;)V

    iget-object v0, v4, Lbrf;->d:Lyt;

    invoke-static {v0}, Lyp;->b(Lyt;)V

    iget-object v0, v4, Lbrf;->d:Lyt;

    invoke-static {v0}, Lyp;->d(Lyt;)V

    .line 222
    :cond_7
    return-void

    :cond_8
    move-object v1, v3

    goto :goto_3
.end method

.method private c()V
    .locals 3

    .prologue
    .line 160
    iget-object v1, p0, Lbrf;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 161
    :try_start_0
    iget-object v0, p0, Lbrf;->d:Lyt;

    if-nez v0, :cond_0

    .line 162
    new-instance v0, Lyt;

    iget-object v2, p0, Lbrf;->c:Lyj;

    invoke-direct {v0, v2}, Lyt;-><init>(Lyj;)V

    iput-object v0, p0, Lbrf;->d:Lyt;

    .line 164
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    const-string v0, "ContactLoader"

    return-object v0
.end method

.method public a(Lbcn;)V
    .locals 3

    .prologue
    .line 302
    const-string v0, "Babel"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 303
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Contact info failed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    :cond_0
    invoke-virtual {p1}, Lbcn;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbrf;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 316
    :goto_0
    return-void

    .line 310
    :cond_1
    new-instance v0, Lbrh;

    invoke-direct {v0, p0, p1}, Lbrh;-><init>(Lbrf;Lbcn;)V

    invoke-virtual {p0, v0}, Lbrf;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected a(Ljava/util/ArrayList;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 320
    invoke-direct {p0}, Lbrf;->c()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, v0}, Lbrf;->d(Ljava/lang/String;)Lbrv;

    move-result-object v1

    if-nez v1, :cond_1

    sget-boolean v1, Lbrf;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "No Contact Requests for key: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    instance-of v0, v1, Laan;

    if-eqz v0, :cond_4

    invoke-static {}, Lcwz;->b()V

    move-object v0, v1

    check-cast v0, Laan;

    invoke-static {}, Lcwz;->b()V

    invoke-static {}, Lbsc;->a()Lbsc;

    move-result-object v5

    :try_start_0
    iget-object v1, p0, Lbrf;->d:Lyt;

    invoke-virtual {v0}, Laan;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v5}, Lyt;->a(Ljava/lang/String;Lbsc;)Laea;

    move-result-object v6

    invoke-virtual {v0}, Laan;->e()Ljava/lang/String;

    move-result-object v7

    if-nez v6, :cond_2

    const-string v1, ""

    move-object v3, v1

    :goto_1
    if-nez v6, :cond_3

    move-object v1, v2

    :goto_2
    invoke-static {v7, v3, v1}, Lbdh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbdh;

    move-result-object v1

    invoke-virtual {v0}, Laan;->d()Lbcn;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lbrf;->a(Lbcn;Lbdh;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v5}, Lbsc;->b()V

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-virtual {v6}, Laea;->e()Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    goto :goto_1

    :cond_3
    invoke-virtual {v6}, Laea;->f()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lbsc;->b()V

    throw v0

    :cond_4
    instance-of v0, v1, Lyg;

    if-eqz v0, :cond_5

    check-cast v1, Lyg;

    iget-object v0, p0, Lbrf;->d:Lyt;

    invoke-virtual {v1}, Lyg;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lyt;->I(Ljava/lang/String;)Lbdh;

    goto/16 :goto_0

    :cond_5
    instance-of v0, v1, Lya;

    if-eqz v0, :cond_a

    check-cast v1, Lya;

    iget-object v0, p0, Lbrf;->d:Lyt;

    invoke-virtual {v0}, Lyt;->a()V

    :try_start_2
    invoke-virtual {v1}, Lya;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbcn;

    iget-object v6, p0, Lbrf;->d:Lyt;

    invoke-virtual {v6, v0}, Lyt;->a(Lbcn;)Lbdh;

    move-result-object v6

    if-eqz v6, :cond_6

    invoke-virtual {v6}, Lbdh;->g()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_6

    invoke-direct {p0, v0, v6}, Lbrf;->a(Lbcn;Lbdh;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_3

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lbrf;->d:Lyt;

    invoke-virtual {v1}, Lyt;->c()V

    throw v0

    :cond_6
    :try_start_3
    invoke-virtual {v0}, Lbcn;->a()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    invoke-virtual {p0, v0}, Lbrf;->a(Lbcn;)V

    goto :goto_3

    :cond_8
    iget-object v0, p0, Lbrf;->d:Lyt;

    invoke-virtual {v0}, Lyt;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    iget-object v0, p0, Lbrf;->d:Lyt;

    invoke-virtual {v0}, Lyt;->c()V

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    sget-boolean v0, Lbrf;->b:Z

    if-eqz v0, :cond_9

    const-string v0, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Sending batch request to server: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    iget-object v0, p0, Lbrf;->c:Lyj;

    invoke-virtual {v1}, Lya;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v3, v1, v8}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/util/ArrayList;Ljava/lang/String;Z)I

    goto/16 :goto_0

    :cond_a
    instance-of v0, v1, Lyb;

    if-eqz v0, :cond_0

    check-cast v1, Lyb;

    invoke-virtual {v1}, Lyb;->d()Lbcn;

    move-result-object v0

    iget-object v1, p0, Lbrf;->d:Lyt;

    invoke-virtual {v1, v0}, Lyt;->a(Lbcn;)Lbdh;

    move-result-object v1

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lbdh;->g()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_e

    :cond_b
    sget-boolean v5, Lbrf;->b:Z

    if-eqz v5, :cond_c

    if-nez v1, :cond_d

    const-string v1, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Contact info not in database, try server: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    :goto_4
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lbrf;->c:Lyj;

    invoke-static {v0, v3, v2, v8}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/util/ArrayList;Ljava/lang/String;Z)I

    goto/16 :goto_0

    :cond_d
    const-string v5, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Contact info is not in database: but we have already requested it: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lbdh;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_e
    sget-boolean v3, Lbrf;->b:Z

    if-eqz v3, :cond_f

    const-string v3, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Contact info is in the database: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lbcn;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " entity: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lbdh;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    invoke-direct {p0, v0, v1}, Lbrf;->a(Lbcn;Lbdh;)V

    goto/16 :goto_0

    .line 321
    :cond_10
    return-void
.end method
