.class public Lbih;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method private constructor <init>(Ldwl;)V
    .locals 3

    .prologue
    .line 3540
    iget-object v0, p1, Ldwl;->b:Ldvn;

    const-wide/16 v1, 0x0

    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 3541
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 3542
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SetMoodResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3544
    :cond_0
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 3548
    new-instance v0, Ldwl;

    invoke-direct {v0}, Ldwl;-><init>()V

    .line 3549
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldwl;

    .line 3550
    iget-object v1, v0, Ldwl;->b:Ldvn;

    invoke-static {v1}, Lbih;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3551
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldwl;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 3553
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbih;

    invoke-direct {v1, v0}, Lbih;-><init>(Ldwl;)V

    move-object v0, v1

    goto :goto_0
.end method
