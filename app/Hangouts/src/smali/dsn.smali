.class public final Ldsn;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldsn;


# instance fields
.field public b:Ldvn;

.field public c:[Ldro;

.field public d:[Ldso;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20977
    const/4 v0, 0x0

    new-array v0, v0, [Ldsn;

    sput-object v0, Ldsn;->a:[Ldsn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20978
    invoke-direct {p0}, Lepn;-><init>()V

    .line 21081
    const/4 v0, 0x0

    iput-object v0, p0, Ldsn;->b:Ldvn;

    .line 21084
    sget-object v0, Ldro;->a:[Ldro;

    iput-object v0, p0, Ldsn;->c:[Ldro;

    .line 21087
    sget-object v0, Ldso;->a:[Ldso;

    iput-object v0, p0, Ldsn;->d:[Ldso;

    .line 20978
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 21116
    iget-object v0, p0, Ldsn;->b:Ldvn;

    if-eqz v0, :cond_4

    .line 21117
    const/4 v0, 0x1

    iget-object v2, p0, Ldsn;->b:Ldvn;

    .line 21118
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 21120
    :goto_0
    iget-object v2, p0, Ldsn;->c:[Ldro;

    if-eqz v2, :cond_1

    .line 21121
    iget-object v3, p0, Ldsn;->c:[Ldro;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 21122
    if-eqz v5, :cond_0

    .line 21123
    const/4 v6, 0x2

    .line 21124
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 21121
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 21128
    :cond_1
    iget-object v2, p0, Ldsn;->d:[Ldso;

    if-eqz v2, :cond_3

    .line 21129
    iget-object v2, p0, Ldsn;->d:[Ldso;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 21130
    if-eqz v4, :cond_2

    .line 21131
    const/4 v5, 0x3

    .line 21132
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 21129
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 21136
    :cond_3
    iget-object v1, p0, Ldsn;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21137
    iput v0, p0, Ldsn;->cachedSize:I

    .line 21138
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 20974
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldsn;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldsn;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldsn;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldsn;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldsn;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldsn;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldsn;->c:[Ldro;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldro;

    iget-object v3, p0, Ldsn;->c:[Ldro;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldsn;->c:[Ldro;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldsn;->c:[Ldro;

    :goto_2
    iget-object v2, p0, Ldsn;->c:[Ldro;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldsn;->c:[Ldro;

    new-instance v3, Ldro;

    invoke-direct {v3}, Ldro;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldsn;->c:[Ldro;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldsn;->c:[Ldro;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldsn;->c:[Ldro;

    new-instance v3, Ldro;

    invoke-direct {v3}, Ldro;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldsn;->c:[Ldro;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldsn;->d:[Ldso;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldso;

    iget-object v3, p0, Ldsn;->d:[Ldso;

    if-eqz v3, :cond_6

    iget-object v3, p0, Ldsn;->d:[Ldso;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Ldsn;->d:[Ldso;

    :goto_4
    iget-object v2, p0, Ldsn;->d:[Ldso;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Ldsn;->d:[Ldso;

    new-instance v3, Ldso;

    invoke-direct {v3}, Ldso;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldsn;->d:[Ldso;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Ldsn;->d:[Ldso;

    array-length v0, v0

    goto :goto_3

    :cond_8
    iget-object v2, p0, Ldsn;->d:[Ldso;

    new-instance v3, Ldso;

    invoke-direct {v3}, Ldso;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldsn;->d:[Ldso;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 21092
    iget-object v1, p0, Ldsn;->b:Ldvn;

    if-eqz v1, :cond_0

    .line 21093
    const/4 v1, 0x1

    iget-object v2, p0, Ldsn;->b:Ldvn;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 21095
    :cond_0
    iget-object v1, p0, Ldsn;->c:[Ldro;

    if-eqz v1, :cond_2

    .line 21096
    iget-object v2, p0, Ldsn;->c:[Ldro;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 21097
    if-eqz v4, :cond_1

    .line 21098
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 21096
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 21102
    :cond_2
    iget-object v1, p0, Ldsn;->d:[Ldso;

    if-eqz v1, :cond_4

    .line 21103
    iget-object v1, p0, Ldsn;->d:[Ldso;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 21104
    if-eqz v3, :cond_3

    .line 21105
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 21103
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 21109
    :cond_4
    iget-object v0, p0, Ldsn;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 21111
    return-void
.end method
