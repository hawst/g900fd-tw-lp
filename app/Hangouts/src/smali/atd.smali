.class public final Latd;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/hangout/renderer/GLView;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Latd;->a:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Latd;->a:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Latd;->a:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->b(Lcom/google/android/apps/hangouts/hangout/renderer/GLView;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 103
    return-void
.end method

.method public b()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Latd;->a:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method b(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Latd;->a:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->queueEvent(Ljava/lang/Runnable;)V

    .line 107
    return-void
.end method

.method c()Lapv;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Latd;->a:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->a(Lcom/google/android/apps/hangouts/hangout/renderer/GLView;)Lapv;

    move-result-object v0

    return-object v0
.end method

.method d()Ly;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Latd;->a:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->a(Lcom/google/android/apps/hangouts/hangout/renderer/GLView;)Lapv;

    move-result-object v0

    invoke-virtual {v0}, Lapv;->b()Ly;

    move-result-object v0

    return-object v0
.end method

.method e()Lcom/google/android/libraries/hangouts/video/RendererManager;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Latd;->a:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->c(Lcom/google/android/apps/hangouts/hangout/renderer/GLView;)Lcom/google/android/libraries/hangouts/video/RendererManager;

    move-result-object v0

    return-object v0
.end method

.method f()Lcom/google/android/libraries/hangouts/video/DecoderManager;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Latd;->a:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->d(Lcom/google/android/apps/hangouts/hangout/renderer/GLView;)Lcom/google/android/libraries/hangouts/video/DecoderManager;

    move-result-object v0

    return-object v0
.end method

.method g()Lauj;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Latd;->a:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->e(Lcom/google/android/apps/hangouts/hangout/renderer/GLView;)Lauj;

    move-result-object v0

    return-object v0
.end method

.method public h()Laqv;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Latd;->a:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->f(Lcom/google/android/apps/hangouts/hangout/renderer/GLView;)Laqv;

    move-result-object v0

    return-object v0
.end method
