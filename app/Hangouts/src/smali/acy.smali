.class public abstract Lacy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field protected d:Landroid/view/View;

.field final synthetic e:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;Landroid/view/ViewGroup;II)V
    .locals 2

    .prologue
    .line 72
    iput-object p1, p0, Lacy;->e:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    invoke-virtual {p1}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, p3, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 74
    invoke-virtual {p2, p4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lacy;->d:Landroid/view/View;

    .line 75
    iget-object v0, p0, Lacy;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 76
    return-void
.end method


# virtual methods
.method abstract a()Z
.end method

.method protected b()V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lacy;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 83
    iget-object v0, p0, Lacy;->e:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->a(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)I

    move-result v0

    if-lez v0, :cond_0

    .line 84
    iget-object v0, p0, Lacy;->e:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->b(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)I

    .line 86
    :cond_0
    iget-object v0, p0, Lacy;->e:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->b()V

    .line 87
    return-void
.end method
