.class public final Lbcd;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field final a:Lbcf;

.field b:Landroid/view/ActionMode;

.field private final c:Landroid/widget/AbsListView$MultiChoiceModeListener;


# direct methods
.method public constructor <init>(Lbcf;)V
    .locals 2

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    iput-object p1, p0, Lbcd;->a:Lbcf;

    .line 135
    new-instance v0, Lbce;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lbce;-><init>(Lbcd;B)V

    iput-object v0, p0, Lbcd;->c:Landroid/widget/AbsListView$MultiChoiceModeListener;

    .line 136
    return-void
.end method


# virtual methods
.method public a()Landroid/widget/AbsListView$MultiChoiceModeListener;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lbcd;->c:Landroid/widget/AbsListView$MultiChoiceModeListener;

    return-object v0
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 5

    .prologue
    .line 151
    iget-object v0, p0, Lbcd;->b:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 152
    if-eqz p1, :cond_1

    .line 157
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 158
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 160
    iget-object v1, p0, Lbcd;->b:Landroid/view/ActionMode;

    invoke-virtual {v1, v0}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 162
    :cond_1
    iget-object v0, p0, Lbcd;->b:Landroid/view/ActionMode;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lbcd;->b:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lbcd;->b:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    .line 174
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lbcd;->b:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lbcd;->b:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 183
    :cond_0
    return-void
.end method
