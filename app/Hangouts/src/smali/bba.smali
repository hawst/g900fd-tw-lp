.class public final Lbba;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Landroid/app/Activity;

.field private b:Landroid/os/Parcel;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lbba;->a:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4

    .prologue
    .line 69
    :try_start_0
    sget-object v0, Lf;->iw:Landroid/os/Parcel;

    iput-object v0, p0, Lbba;->b:Landroid/os/Parcel;

    .line 70
    iget-object v0, p0, Lbba;->b:Landroid/os/Parcel;

    if-nez v0, :cond_0

    .line 71
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    iput-object v0, p0, Lbba;->b:Landroid/os/Parcel;

    .line 74
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lbba;->b:Landroid/os/Parcel;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {p2, v0, v1, v2, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    iget-object v0, p0, Lbba;->a:Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 80
    :goto_0
    return-void

    .line 75
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79
    iget-object v0, p0, Lbba;->a:Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbba;->a:Landroid/app/Activity;

    invoke-virtual {v1, p0}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    throw v0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lbba;->b:Landroid/os/Parcel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbba;->b:Landroid/os/Parcel;

    sget-object v1, Lf;->iw:Landroid/os/Parcel;

    if-eq v0, v1, :cond_0

    .line 86
    iget-object v0, p0, Lbba;->b:Landroid/os/Parcel;

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lbba;->b:Landroid/os/Parcel;

    .line 89
    :cond_0
    return-void
.end method
