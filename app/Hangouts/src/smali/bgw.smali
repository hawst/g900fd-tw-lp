.class public Lbgw;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbdk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ldyx;)V
    .locals 5

    .prologue
    .line 3735
    invoke-direct {p0}, Lbfz;-><init>()V

    .line 3736
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbgw;->g:Ljava/util/List;

    .line 3737
    iget-object v1, p1, Ldyx;->c:[Ldyt;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 3738
    iget-object v4, p0, Lbgw;->g:Ljava/util/List;

    iget-object v3, v3, Ldyt;->d:Ljava/lang/String;

    invoke-static {v3}, Lbdk;->b(Ljava/lang/String;)Lbdk;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3737
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3740
    :cond_0
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_1

    .line 3741
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetHangoutParticipantsResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3744
    :cond_1
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 3748
    new-instance v0, Ldyx;

    invoke-direct {v0}, Ldyx;-><init>()V

    .line 3749
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldyx;

    .line 3750
    iget-object v1, v0, Ldyx;->b:Ldvn;

    invoke-static {v1}, Lbgw;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3751
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldyx;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 3753
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbgw;

    invoke-direct {v1, v0}, Lbgw;-><init>(Ldyx;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3762
    iget-object v0, p0, Lbgw;->b:Lbea;

    check-cast v0, Lbdv;

    iget-object v0, v0, Lbdv;->b:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbdk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3766
    iget-object v0, p0, Lbgw;->g:Ljava/util/List;

    return-object v0
.end method
