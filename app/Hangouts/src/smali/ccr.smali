.class public final Lccr;
.super Lcct;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:D

.field private d:D


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lccr;-><init>(Landroid/content/Context;B)V

    .line 35
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lccr;-><init>(Landroid/content/Context;C)V

    .line 39
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;C)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcct;-><init>(Landroid/content/Context;)V

    .line 43
    return-void
.end method

.method static synthetic a(Lccr;)D
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lccr;->b:D

    return-wide v0
.end method

.method static synthetic b(Lccr;)D
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lccr;->d:D

    return-wide v0
.end method

.method static synthetic c(Lccr;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lccr;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 85
    return-void
.end method

.method public a(Lyj;ZLjava/lang/String;Ljava/lang/String;DDLt;)V
    .locals 6

    .prologue
    const/16 v4, 0x190

    .line 47
    iput-object p4, p0, Lccr;->a:Ljava/lang/String;

    .line 48
    iput-wide p5, p0, Lccr;->b:D

    .line 49
    iput-wide p7, p0, Lccr;->d:D

    .line 51
    new-instance v0, Lccs;

    invoke-direct {v0, p0, p9}, Lccs;-><init>(Lccr;Lt;)V

    invoke-virtual {p0, v0}, Lccr;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    sget v0, Lh;->fY:I

    invoke-virtual {p0, v0}, Lccr;->a(I)V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v5, v4

    .line 74
    invoke-super/range {v0 .. v5}, Lcct;->a(Lyj;ZLjava/lang/String;II)V

    .line 76
    return-void
.end method

.method protected a()Z
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method
