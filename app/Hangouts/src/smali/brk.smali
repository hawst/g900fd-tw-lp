.class public final Lbrk;
.super Lbrr;
.source "PG"


# static fields
.field private static final g:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lbrk;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lyj;

.field private c:Lyt;

.field private final d:Lbor;

.field private volatile e:Z

.field private final f:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lbrk;->g:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method private constructor <init>(Lyj;)V
    .locals 1

    .prologue
    .line 150
    invoke-direct {p0}, Lbrr;-><init>()V

    .line 39
    new-instance v0, Lbrl;

    invoke-direct {v0, p0}, Lbrl;-><init>(Lbrk;)V

    iput-object v0, p0, Lbrk;->d:Lbor;

    .line 69
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lbrk;->f:Ljava/util/concurrent/ConcurrentHashMap;

    .line 151
    iput-object p1, p0, Lbrk;->b:Lyj;

    .line 152
    const/4 v0, 0x0

    iput-object v0, p0, Lbrk;->c:Lyt;

    .line 153
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbrk;->e:Z

    .line 154
    return-void
.end method

.method public static a(Lyj;)Lbrk;
    .locals 4

    .prologue
    .line 82
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    .line 83
    sget-object v0, Lbrk;->g:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrk;

    .line 85
    if-nez v0, :cond_1

    .line 86
    const-string v0, "Babel"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Adding contact loader for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :cond_0
    new-instance v0, Lbrk;

    invoke-direct {v0, p0}, Lbrk;-><init>(Lyj;)V

    .line 95
    sget-object v2, Lbrk;->g:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lbrk;->g:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrk;

    .line 99
    :cond_1
    return-object v0
.end method

.method static synthetic a(Lbrk;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lbrk;->f:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method static synthetic a(Lbrk;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lbrk;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrv;

    check-cast v0, Lyf;

    iget-object v0, v0, Lyf;->b:Lbrn;

    invoke-interface {v0}, Lbrn;->b()V

    goto :goto_0
.end method

.method static synthetic a(Lbrk;Ljava/lang/String;Lyv;)V
    .locals 2

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lbrk;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrv;

    check-cast v0, Lyf;

    iget-object v0, v0, Lyf;->b:Lbrn;

    invoke-interface {v0, p2}, Lbrn;->a(Lyv;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 137
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Removing conversation loader for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    :cond_0
    sget-object v0, Lbrk;->g:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    return-void
.end method

.method static synthetic b(Lbrk;)V
    .locals 1

    .prologue
    .line 22
    iget-boolean v0, p0, Lbrk;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbrk;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbrk;->d:Lbor;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbrk;->e:Z

    :cond_0
    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    const-string v0, "ConversationLoader"

    return-object v0
.end method

.method protected a(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 172
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 173
    iget-object v2, p0, Lbrk;->c:Lyt;

    if-nez v2, :cond_0

    new-instance v2, Lyt;

    iget-object v3, p0, Lbrk;->b:Lyj;

    invoke-direct {v2, v3}, Lyt;-><init>(Lyj;)V

    iput-object v2, p0, Lbrk;->c:Lyt;

    :cond_0
    iget-object v2, p0, Lbrk;->c:Lyt;

    invoke-virtual {v2, v0}, Lyt;->e(Ljava/lang/String;)Lyv;

    move-result-object v2

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lbrk;->e:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lbrk;->d:Lbor;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbor;)V

    const/4 v2, 0x1

    iput-boolean v2, p0, Lbrk;->e:Z

    :cond_1
    iget-object v2, p0, Lbrk;->b:Lyj;

    invoke-static {v2, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->n(Lyj;Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Lbrk;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    new-instance v3, Lbrm;

    invoke-direct {v3, p0, v0, v2}, Lbrm;-><init>(Lbrk;Ljava/lang/String;Lyv;)V

    invoke-virtual {p0, v3}, Lbrk;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 175
    :cond_3
    return-void
.end method
