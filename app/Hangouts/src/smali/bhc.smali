.class public Lbhc;
.super Lbfz;
.source "PG"


# static fields
.field private static final i:Landroid/util/SparseBooleanArray;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1282
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    .line 1283
    sput-object v0, Lbhc;->i:Landroid/util/SparseBooleanArray;

    const/16 v1, 0x12

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 1284
    sget-object v0, Lbhc;->i:Landroid/util/SparseBooleanArray;

    const/16 v1, 0x16

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 1285
    sget-object v0, Lbhc;->i:Landroid/util/SparseBooleanArray;

    const/16 v1, 0x24

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 1286
    return-void
.end method

.method private constructor <init>(Lddc;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1289
    iget-object v0, p1, Lddc;->b:Ldcw;

    invoke-direct {p0, v0}, Lbfz;-><init>(Ldcw;)V

    .line 1271
    iput-object v2, p0, Lbhc;->g:Ljava/lang/String;

    .line 1272
    iput-object v2, p0, Lbhc;->h:Ljava/lang/String;

    .line 1290
    iget-object v0, p1, Lddc;->c:Ldeb;

    iget-object v0, v0, Ldeb;->b:Ldhe;

    .line 1291
    if-eqz v0, :cond_0

    .line 1292
    iget-object v2, v0, Ldhe;->i:Ljava/lang/String;

    iput-object v2, p0, Lbhc;->g:Ljava/lang/String;

    .line 1293
    iget-object v0, v0, Ldhe;->r:Ldhn;

    .line 1294
    if-eqz v0, :cond_0

    .line 1295
    iget-object v2, v0, Ldhn;->d:[Ldho;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 1297
    iget-object v5, v4, Ldho;->e:Ljava/lang/String;

    .line 1298
    iget-object v4, v4, Ldho;->b:Ljava/lang/Integer;

    invoke-static {v4, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v4

    .line 1299
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    sget-object v6, Lbhc;->i:Landroid/util/SparseBooleanArray;

    .line 1300
    invoke-virtual {v6, v4, v1}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 1299
    invoke-static {v4, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1301
    iput-object v5, p0, Lbhc;->h:Ljava/lang/String;

    .line 1307
    :cond_0
    return-void

    .line 1296
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 1311
    new-instance v0, Lddc;

    invoke-direct {v0}, Lddc;-><init>()V

    .line 1312
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Lddc;

    .line 1313
    iget-object v1, v0, Lddc;->b:Ldcw;

    invoke-static {v1}, Lbhc;->a(Ldcw;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1314
    new-instance v1, Lbgk;

    iget-object v0, v0, Lddc;->b:Ldcw;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldcw;)V

    move-object v0, v1

    .line 1316
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbhc;

    invoke-direct {v1, v0}, Lbhc;-><init>(Lddc;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 4

    .prologue
    .line 1323
    invoke-super {p0, p1, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 1324
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 1325
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetVideoDataResponse.processResponse: retrieved video with id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbhc;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and has stream url of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbhc;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1328
    :cond_0
    iget-object v0, p0, Lbhc;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lbhc;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1329
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x14

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 1330
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 1331
    iget-object v2, p0, Lbhc;->g:Ljava/lang/String;

    iget-object v3, p0, Lbhc;->h:Ljava/lang/String;

    invoke-virtual {p1, v2, v3, v0, v1}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;J)V

    .line 1332
    invoke-static {p1}, Lyp;->c(Lyt;)V

    .line 1334
    :cond_1
    return-void
.end method
